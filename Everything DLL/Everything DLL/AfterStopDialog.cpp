#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

// old gui version

extern HMODULE hDllModule;
void updateAfterStopLockedControls(HWND hWnd, RunNextSettings &rns);

//------------------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK AfterStopSettingsDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static RunNextSettings * rns = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (!bgBrush)
					bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				
				EMButton * embOK = GetEMButtonInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_OK));
				EMButton * embCancel = GetEMButtonInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_CANCEL));
				EMButton * embSaveFolder = GetEMButtonInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_CHOOSE_FOLDER));
				EMText * emtNextCam = GetEMTextInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_TEXT_NEXT_CAM));
				EMText * emtDescending = GetEMTextInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_TEXT_DESCENDING));
				EMText * emtCopyPost = GetEMTextInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_TEXT_COPY_POST_BLEND_FINAL));
				EMText * emtDelBuffs = GetEMTextInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_TEXT_DELETE_CAM_BUFFERS));
				EMText * emtSaveImage = GetEMTextInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_TEXT_SAVE_IMAGE));
				EMText * emtChangeSunsky = GetEMTextInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_TEXT_CHANGE_SUNSKY));
				EMText * emtMinutes = GetEMTextInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_TEXT_MINUTES));
				EMCheckBox * emcNextCam  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_NEXT_CAM));
				EMCheckBox * emcCamDesc  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_CAM_DESC));
				EMCheckBox * emcCopyPost = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_COPY_POST_BLEND_FINAL));
				EMCheckBox * emcDelBuffs = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_DEL_CAM_BUFS));
				EMCheckBox * emcSaveImg  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_SAVE_IMAGE));
				EMCheckBox * emcChangeSunsky = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_CHANGE_SUNSKY));
				EMComboBox * emcFileFormat = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_FILE_FORMAT));
				EMEditSpin * emesSunsky = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_AFTERSTOP_SUNSKY_MINUTES));
				GlobalWindowSettings::colorSchemes.apply(embOK);
				GlobalWindowSettings::colorSchemes.apply(embCancel);
				GlobalWindowSettings::colorSchemes.apply(embSaveFolder);
				GlobalWindowSettings::colorSchemes.apply(emtNextCam, true, false);
				GlobalWindowSettings::colorSchemes.apply(emtDescending, true, false);
				GlobalWindowSettings::colorSchemes.apply(emtCopyPost, true, false);
				GlobalWindowSettings::colorSchemes.apply(emtDelBuffs, true, false);
				GlobalWindowSettings::colorSchemes.apply(emtSaveImage, true, false);
				GlobalWindowSettings::colorSchemes.apply(emtChangeSunsky, true, false);
				GlobalWindowSettings::colorSchemes.apply(emtMinutes, true, false);
				GlobalWindowSettings::colorSchemes.apply(emcNextCam);
				GlobalWindowSettings::colorSchemes.apply(emcCamDesc);
				GlobalWindowSettings::colorSchemes.apply(emcCopyPost);
				GlobalWindowSettings::colorSchemes.apply(emcDelBuffs);
				GlobalWindowSettings::colorSchemes.apply(emcSaveImg);
				GlobalWindowSettings::colorSchemes.apply(emcChangeSunsky);
				GlobalWindowSettings::colorSchemes.apply(emcFileFormat);
				GlobalWindowSettings::colorSchemes.apply(emesSunsky);


				if (!lParam)
				{
					EndDialog(hWnd, (INT_PTR)NULL);
					break;
				}

				emcFileFormat->deleteItems();
				emcFileFormat->addItem("PNG");
				emcFileFormat->addItem("JPG");
				emcFileFormat->addItem("EXR");

				rns = (RunNextSettings*)lParam;

				emcNextCam->selected = rns->nextCam;	
				InvalidateRect(emcNextCam->hwnd, NULL, false);
				emcCamDesc->selected = rns->nCamDesc;	
				InvalidateRect(emcCamDesc->hwnd, NULL, false);
				emcCopyPost->selected = rns->copyPost;	
				InvalidateRect(emcCopyPost->hwnd, NULL, false);
				emcDelBuffs->selected = rns->delBuffs;	
				InvalidateRect(emcDelBuffs->hwnd, NULL, false);
				emcSaveImg->selected = rns->saveImg;	
				InvalidateRect(emcSaveImg->hwnd, NULL, false);
				emcChangeSunsky->selected = rns->changeSunsky;	
				InvalidateRect(emcChangeSunsky->hwnd, NULL, false);
				emcFileFormat->selected = rns->fileFormat;	
				InvalidateRect(emcFileFormat->hwnd, NULL, false);
				emesSunsky->setValuesToInt(rns->sunskyMins, -1440, 1440, 1, 0.1f);

				updateAfterStopLockedControls(hWnd, *rns);

			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_AFTERSTOP_OK:
						{
							RunNextSettings * rememberit = rns;
							rns = NULL;
							EndDialog(hWnd, (INT_PTR)rememberit);
						}
						break;
					case IDC_AFTERSTOP_CANCEL:
						{
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;
					case IDC_AFTERSTOP_NEXT_CAM:
						{
							CHECK(rns);
							CHECK(wmEvent==BN_CLICKED);
							EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, wmId));
							rns->nextCam = emc->selected;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_CAM_DESC:
						{
							CHECK(rns);
							CHECK(wmEvent==BN_CLICKED);
							EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, wmId));
							rns->nCamDesc = emc->selected;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_COPY_POST_BLEND_FINAL:
						{
							CHECK(rns);
							CHECK(wmEvent==BN_CLICKED);
							EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, wmId));
							rns->copyPost = emc->selected;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_DEL_CAM_BUFS:
						{
							CHECK(rns);
							CHECK(wmEvent==BN_CLICKED);
							EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, wmId));
							rns->delBuffs = emc->selected;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_SAVE_IMAGE:
						{
							CHECK(rns);
							CHECK(wmEvent==BN_CLICKED);
							EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, wmId));
							rns->saveImg = emc->selected;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_CHANGE_SUNSKY:
						{
							CHECK(rns);
							CHECK(wmEvent==BN_CLICKED);
							EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, wmId));
							rns->changeSunsky = emc->selected;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_CHOOSE_FOLDER:
						{
							CHECK(rns);
							CHECK(wmEvent==BN_CLICKED);
							EMButton * emb = GetEMButtonInstance(GetDlgItem(hWnd, wmId));

							char * newdir = saveDirectoryDialog(hWnd, "Choose folder for images", rns->saveFolder);
							if (!newdir)
								break;
							if (rns->saveFolder)
								free(rns->saveFolder);
							rns->saveFolder = newdir;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_FILE_FORMAT:
						{
							CHECK(rns);
							CHECK(wmEvent==CBN_SELCHANGE);
							EMComboBox * emcb = GetEMComboBoxInstance(GetDlgItem(hWnd, wmId));
							rns->fileFormat = emcb->selected;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;
					case IDC_AFTERSTOP_SUNSKY_MINUTES:
						{
							CHECK(rns);
							CHECK(wmEvent==WM_VSCROLL);
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, wmId));
							rns->sunskyMins = emes->intValue;
							updateAfterStopLockedControls(hWnd, *rns);
						}
						break;

				}	// end switch
			}		// end WM_COMMAND
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------------------------------------

bool RunNextSettings::copyFrom(RunNextSettings * other)
{
	CHECK(other);
	if (saveFolder)
		free(saveFolder);

	active = other->active;
	nextCam = other->nextCam;
	nCamDesc = other->nCamDesc;
	copyPost = other->copyPost;
	delBuffs = other->delBuffs;
	saveImg = other->saveImg;
	changeSunsky = other->changeSunsky;
	sunskyMins = other->sunskyMins;
	fileFormat = other->fileFormat;
	saveFolder = copyString(other->saveFolder);

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------

RunNextSettings * RunNextSettings::getCopy()
{
	RunNextSettings * res = new RunNextSettings();
	res->copyFrom(this);
	return res;
}

//------------------------------------------------------------------------------------------------------------------------------

void updateAfterStopLockedControls(HWND hWnd, RunNextSettings &rns)
{
	bool nc = rns.nextCam;
	HWND hactual;
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_NEXT_CAM);					EnableWindow(hactual, true);				InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_CAM_DESC);					EnableWindow(hactual, nc);					InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_COPY_POST_BLEND_FINAL);	EnableWindow(hactual, nc);					InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_DEL_CAM_BUFS);				EnableWindow(hactual, nc);					InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_SAVE_IMAGE);				EnableWindow(hactual, true);				InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_CHANGE_SUNSKY);			EnableWindow(hactual, true);				InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_FILE_FORMAT);				EnableWindow(hactual, rns.saveImg);			InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_CHOOSE_FOLDER);			EnableWindow(hactual, rns.saveImg);			InvalidateRect(hactual, NULL, false);
	hactual = GetDlgItem(hWnd, IDC_AFTERSTOP_SUNSKY_MINUTES);			EnableWindow(hactual, rns.changeSunsky);	InvalidateRect(hactual, NULL, false);
}

//------------------------------------------------------------------------------------------------------------------------------
