#define _CRT_RAND_S
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "log.h"
#include "MatEditor.h"
#include "XML_IO.h"

extern HMODULE hDllModule;

EMEditSimple * getBlendNameControl(HWND hDlg, int num);
EMText * getBlendTextControl(HWND hDlg, int num);
void exportBlendNames(HWND hWnd, BlendSettings * blend);
void importBlendNames(HWND hWnd, BlendSettings * blend);
void updateUItoBlend(HWND hWnd, BlendSettings * blend);

//------------------------------------------------------------------------------------------------------------------------------------

void runBlendNamesDialog(HWND hWnd, BlendSettings * blend)
{
	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->runDialogsUnicode)
	{
		DialogBoxParamW(hDllModule, 
			MAKEINTRESOURCEW(IDD_BLEND_LAYERS), hWnd, BlendNamesDlgProc,(LPARAM)(blend));
	}
	else
	{
		DialogBoxParam(hDllModule, 
			MAKEINTRESOURCE(IDD_BLEND_LAYERS), hWnd, BlendNamesDlgProc,(LPARAM)(blend));
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK BlendNamesDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
char tbuf[256];
sprintf_s(tbuf, 256, "blend msg: %x", message);
Logger::add(tbuf);

	static HBRUSH bgBrush = 0;
	static BlendSettings * blend = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				blend = (BlendSettings *)lParam;
				if (!blend)
					EndDialog(hWnd, (INT_PTR)1);
					
				//for (int i=0; i<16; i++)
				for (int i=15; i>=0; i--)
				{
					EMEditSimple * emedit = getBlendNameControl(hWnd, i);
					EMText * emtext = getBlendTextControl(hWnd, i);

					GlobalWindowSettings::colorSchemes.apply(emtext, true);
					GlobalWindowSettings::colorSchemes.apply(emedit);
		
					SetWindowPos(emtext->hwnd,  HWND_TOP,  8,  8+18*i+1,   48, 13,   SWP_NOOWNERZORDER);
					SetWindowPos(emedit->hwnd,  HWND_TOP, 60,  8+18*i,    300, 15,   SWP_NOOWNERZORDER);
					SetWindowPos(emedit->hEdit, HWND_TOP,  1,  1,         298, 13,   SWP_NOOWNERZORDER);

					emedit->setText(blend->names[i]);
				}

				EMButton * emok   = GetEMButtonInstance(GetDlgItem(hWnd, IDC_BLEND_OK));
				EMButton * emload = GetEMButtonInstance(GetDlgItem(hWnd, IDC_BLEND_IMPORT));
				EMButton * emsave = GetEMButtonInstance(GetDlgItem(hWnd, IDC_BLEND_EXPORT));
				GlobalWindowSettings::colorSchemes.apply(emok);
				GlobalWindowSettings::colorSchemes.apply(emload);
				GlobalWindowSettings::colorSchemes.apply(emsave);
				SetWindowPos(emok->hwnd,   HWND_TOP,  46,  8+18*16+10,    80, 22,   SWP_NOOWNERZORDER);
				SetWindowPos(emload->hwnd, HWND_TOP, 146,  8+18*16+10,    80, 22,   SWP_NOOWNERZORDER);
				SetWindowPos(emsave->hwnd, HWND_TOP, 246,  8+18*16+10,    80, 22,   SWP_NOOWNERZORDER);

			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_BLEND_IMPORT:
						{
							CHECK(wmEvent==BN_CLICKED);
							updateUItoBlend(hWnd, blend);
							importBlendNames(hWnd, blend);
						}
						break;
					case IDC_BLEND_EXPORT:
						{
							CHECK(wmEvent==BN_CLICKED);
							updateUItoBlend(hWnd, blend);
							exportBlendNames(hWnd, blend);
						}
						break;
					case IDC_BLEND_OK:
						{
							CHECK(wmEvent==BN_CLICKED);

							updateUItoBlend(hWnd, blend);

							EndDialog(hWnd, (INT_PTR)1);
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------------------------------------------

EMText * getBlendTextControl(HWND hDlg, int num)
{
	EMText * res = NULL;
	switch (num)
	{
		case 0: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT1));		break;
		case 1: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT2));		break;
		case 2: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT3));		break;
		case 3: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT4));		break;
		case 4: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT5));		break;
		case 5: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT6));		break;
		case 6: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT7));		break;
		case 7: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT8));		break;
		case 8: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT9));		break;
		case 9: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT10));	break;
		case 10: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT11));	break;
		case 11: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT12));	break;
		case 12: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT13));	break;
		case 13: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT14));	break;
		case 14: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT15));	break;
		case 15: res = GetEMTextInstance(GetDlgItem(hDlg, IDC_BLEND_TEXT16));	break;
	}
	return res;
}

//------------------------------------------------------------------------------------------------------------------------------------

EMEditSimple * getBlendNameControl(HWND hDlg, int num)
{
	EMEditSimple * res = NULL;
	switch (num)
	{
		case 0: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME1));	break;
		case 1: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME2));	break;
		case 2: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME3));	break;
		case 3: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME4));	break;
		case 4: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME5));	break;
		case 5: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME6));	break;
		case 6: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME7));	break;
		case 7: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME8));	break;
		case 8: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME9));	break;
		case 9: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME10));	break;
		case 10: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME11));	break;
		case 11: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME12));	break;
		case 12: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME13));	break;
		case 13: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME14));	break;
		case 14: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME15));	break;
		case 15: res = GetEMEditSimpleInstance(GetDlgItem(hDlg, IDC_BLEND_NAME16));	break;
	}
	return res;
}

//------------------------------------------------------------------------------------------------------------------------------------

void updateUItoBlend(HWND hWnd, BlendSettings * blend)
{
	if (!blend)
		return;

	for (int i=0; i<16; i++)
	{
		EMEditSimple * emes = getBlendNameControl(hWnd, i);
		if (!emes)
			continue;
		char * name = emes->getText();
		if (blend)
			blend->setName(i, name);
		if (name)
			free(name);
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

void exportBlendNames(HWND hWnd, BlendSettings * blend)
{
	if (!blend)
	{
		MessageBox(0, "Internal error, blend settings is NULL.\nNot saved.", "Error", 0);
		return;
	}

	char * filename;
	filename = saveFileDialog(hWnd, "NOX XML blend layer names (*.nxb)\0*.nxb\0", "Save blend layer names", "nxb", 1);
	if (!filename)
		return;

	XMLScene xscene;
	bool saved = xscene.saveBlendSettings(filename, *blend, true);
	
	if (saved)
		MessageBox(hWnd, "XML saved", "", 0);
	else
		MessageBox(hWnd, "XML not saved", "", 0);
}

//------------------------------------------------------------------------------------------------------------------------------------

void importBlendNames(HWND hWnd, BlendSettings * blend)
{
	if (!blend)
	{
		MessageBox(0, "Internal error, blend settings is NULL.\nNot saved.", "Error", 0);
		return;
	}

	char * filename;
	filename = openFileDialog(hWnd, "NOX XML blend layer names (*.nxb)\0*.nxb\0", "Load blend layer names", "nxb", 1);
	if (!filename)
		return;

	XMLScene xscene;
	int ok = xscene.loadBlendSettings(filename, *blend, true);
	if (ok)
	{
		for (int i=0; i<16; i++)
		{
			EMEditSimple * emedit = getBlendNameControl(hWnd, i);
			emedit->setText(blend->names[i]);
		}
	}
	else
	{
		int err = xscene.errLine;
		char buf[256];
		if (err>0)
			sprintf_s(buf, 256, "Something is wrong with loading blend layer names.\nWatch line %d", err); 
		else
			sprintf_s(buf, 256, "Something is wrong with XML while loading blend layer names.\nError code: %d", err); 
		MessageBox(0, buf, "", 0);
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

