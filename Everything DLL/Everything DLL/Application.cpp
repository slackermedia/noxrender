#define _WIN32_WINNT 0x0500
#define _CRT_RAND_S
#include <windows.h>
#include <shlobj.h>
#include "resource.h"
#include "RendererMainWindow.h"
#include "MatEditor.h"
#include "EMControls.h"
#include "EM2Controls.h"
#include "ColorSchemes.h"
#include "raytracer.h"
#include "Dialogs.h"
#include "FastMath.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "CrashLog\CrashLog.h"
#include "embree.h"

//------------------------------------------------------

#pragma message("Checking versions...")

#ifndef NV_MAJOR
	#error NV_MAJOR not defined
#endif

#ifndef NV_MINOR
	#error NV_MINOR not defined
#endif

#ifndef NV_BUILD
	#error NV_BUILD not defined
#endif

#ifndef NV_COMP_NO
	#error NV_COMP_NO not defined
#endif

#ifndef NOX_VER_MAJOR
	#error NOX_VER_MAJOR not defined
#endif

#ifndef NOX_VER_MINOR
	#error NOX_VER_MINOR not defined
#endif

#ifndef NOX_VER_BUILD
	#error NOX_VER_BUILD not defined
#endif

#ifndef NOX_VER_COMP_NO
	#error NOX_VER_COMP_NO not defined
#endif

#if NOX_VER_MAJOR != NV_MAJOR
	#pragma message("Versions do not match!")
	#error Major Version not the same
#endif

#if NOX_VER_MINOR != NV_MINOR
	#pragma message("Versions do not match!")
	#error Minor Version not the same
#endif

#if NOX_VER_BUILD != NV_BUILD
	#pragma message("Versions do not match!")
	#error Build Version not the same
#endif

#if NOX_VER_COMP_NO != NV_COMP_NO
	#pragma message("Versions do not match!")
	#error Compilation Number Version not the same
#endif

#pragma message("Version seems to be OK")

//------------------------------------------------------

bool doRegistryStuffOnStartup(bool showLog);
bool dirExists(char * dirName);
bool fileExists(char * filename);
int fileSize(char * filename);
char * joinStrings(char * str1, char * str2);
char * joinStrings(char * str1, char * str2, char * str3);
bool isOpenCLSupported();

char  * defaultDirectory = NULL;
char * userDirectory = NULL;
extern HMODULE hDllModule;
ThemeManager GlobalWindowSettings::colorSchemes;
ThemeManager gTheme;
HBITMAP gIcons = 0;


bool SUPPORT_SSE1   = false;
bool SUPPORT_SSE2   = false;
bool SUPPORT_SSE3   = false;
bool SUPPORT_SSSE3  = false;
bool SUPPORT_SSE4_1 = false;
bool SUPPORT_SSE4_2 = false;


void checkProcessor(bool &s_sse1, bool &s_sse2, bool &s_sse3, bool &s_ssse3, bool &s_sse4_1, bool &s_sse4_2)
{
	#ifdef _WIN64
		int abcd[4];
		int num = 1;
		__cpuid(abcd, num);
		s_sse1   = ((abcd[3] & (1<<25)) > 0);
		s_sse2   = ((abcd[3] & (1<<26)) > 0);
		s_sse3   = ((abcd[2] & (1<<0))  > 0);
		s_ssse3  = ((abcd[2] & (1<<9))  > 0);
		s_sse4_1 = ((abcd[2] & (1<<19)) > 0);
		s_sse4_2 = ((abcd[2] & (1<<20)) > 0);

	#else
		unsigned int a,b,c,d;

		__asm
		{
			mov eax, 1
			cpuid
			mov a, eax
			mov b, ebx
			mov c, ecx
			mov d, edx
		}

		s_sse1   = ((d & (1<<25)) > 0);
		s_sse2   = ((d & (1<<26)) > 0);
		s_sse3   = ((c & (1<<0))  > 0);
		s_ssse3  = ((c & (1<<9))  > 0);
		s_sse4_1 = ((c & (1<<19)) > 0);
		s_sse4_2 = ((c & (1<<20)) > 0);
	#endif
}

void InitializeApplication(bool showLog)
{
	CrashLog * crash = CrashLog::getInstance();
	
	static bool initialized = false;
	if (initialized)
		return;
	initialized = true;

	InitEMButton();
	InitEMImgButton();
	InitEMImgStateButton();
	InitEMPView();
	InitEMComboBox();
	InitEMCheckBox();
	InitEMEditTrackBar();
	InitEMEditSpin();
	InitEMFresnel();
	InitEMColorPicker();
	InitEMColorShow();
	InitEMGroupBar();
	InitEMCameraView();
	InitEMEnvironment();
	InitEMProgressBar();
	InitEMStatusBar();
	InitEMTabs();
	InitEMListView();
	InitEMText();
	InitEMCurve();
	InitEMTemperaturePicker();
	InitEMLine();
	InitEMEditSimple();
	InitEMPages();
	InitEMScrollBar();
	InitEMImageList();
	InitEMAberration();
	InitEMGraphList();

	InitEM2Button();
	InitEM2Tabs();
	InitEM2Text();
	InitEM2GroupBar();
	InitEM2CheckBox();
	InitEM2ListView();
	InitEM2EditSpin();
	InitEM2EditTrackBar();
	InitEM2ComboBox();
	InitEM2ColorShow();
	InitEM2EditSimple();
	InitEM2ImgButton();
	InitEM2Line();
	InitEM2Busy();
	InitEM2TrackBar();
	InitEM2Color2D();
	InitEM2ScrollBar();

	InitLicense2WindowClass();

	srand((unsigned int)time(0));
	FastMath::initialize();
	
	bool legacymode = checkLegacyModeFromRegistry();
	#ifdef USE_EMBREE
	if (embreeSystemMinimum())
	{
		if (legacymode)
			Raytracer::getInstance()->use_embree = false;
		initEmbree();
	}
	#endif

	checkProcessor(SUPPORT_SSE1, SUPPORT_SSE2, SUPPORT_SSE3, SUPPORT_SSSE3, SUPPORT_SSE4_1, SUPPORT_SSE4_2);

	char buffer[2048];

	// load path from registry and check dirs existance
	doRegistryStuffOnStartup(showLog);

	int v1 = NOX_VER_MAJOR;
	int v2 = NOX_VER_MINOR;
	int v3 = NOX_VER_BUILD;
	int v4 = NOX_VER_COMP_NO;
	char vbuf[128];
	#ifdef _WIN64
		sprintf_s(vbuf, 128, "NOX Version %d.%d.%d.%d x64", v1, v2, v3, v4);
	#else
		sprintf_s(vbuf, 128, "NOX Version %d.%d.%d.%d x86", v1, v2, v3, v4);
	#endif
	Logger::add(vbuf);

	sprintf_s(buffer, 2048, "hDllModule address: %lld", (INT_PTR)hDllModule);
	Logger::add(buffer);

	if (legacymode)
		Logger::add("Legacy mode set from registry");


	// load themes
	sprintf_s(buffer, 2048, "%s\\settings\\themes\\default theme.xml", defaultDirectory);
	if (showLog)
	{
		Logger::add("Loading color scheme from file:");
		Logger::add(buffer);
	}
	bool res = false;
	ThemeManager ttheme;
	res = ttheme.loadFromFile(buffer);
	if (res)
	{
		gTheme = ttheme;
		RendererMainWindow::theme = ttheme;
		GlobalWindowSettings::colorSchemes = ttheme;
		if (showLog)
			Logger::add("Loaded successfully.");

		// load material editor icons
		sprintf_s(buffer, 2048, "%s\\settings\\themes\\%s", defaultDirectory, ttheme.MaterialWindowIcons);
		if (buffer)
		{
			if (showLog)
			{
				Logger::add("Trying to load icons from file:");
				Logger::add(buffer);
			}
			gIcons = (HBITMAP)LoadImage(hDllModule, 
						buffer, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			if (!gIcons)
			{
				if (showLog)
					Logger::add("Load failed, using default icons.");
				gIcons = (HBITMAP)LoadImage(hDllModule, 
							MAKEINTRESOURCE(IDB_ICONS), IMAGE_BITMAP, 0, 0, 0);
			}
			else
			{
				if (showLog)
					Logger::add("Loaded successfully.");
			}
		}
		else
		{
			if (showLog)
				Logger::add("Icons file not given, using default icons.");
			gIcons = (HBITMAP)LoadImage(hDllModule, 
						MAKEINTRESOURCE(IDB_ICONS), IMAGE_BITMAP, 0, 0, 0);
		}

	}
	else
	{
		if (showLog)
			Logger::add("Failed");
		if (showLog)
			Logger::add("No theme file, using internal icons.");
		gIcons = (HBITMAP)LoadImage(hDllModule, 
					MAKEINTRESOURCE(IDB_ICONS), IMAGE_BITMAP, 0, 0, 0);
	}

	MatEditorWindow::localmatlib.initialize();
}


char * copyDirPathWithoutLastBackslash(char * path, bool correctRoot)
{
	if (!path)
		return NULL;

	char * res = copyString(path, 2);
	if (!res)
		return NULL;

	int l = (int)strlen(res)-1;
	while (l>=0)
	{
		if (res[l]=='\\'  ||  res[l]=='/')
			res[l] = 0;
		else
			break;
		l--;
	}

	if (correctRoot)
	{
		l = (int)strlen(res);
		if (l==2  &&  res[1]==':')
		{
			res[2] = '\\';
			res[3] = 0;
		}
	}

	return res;
}

char * getOnlyFile(char * filename)
{
	CHECK(filename);
	int l = (int)strlen(filename);
	char * b = (char*)malloc(l+1);
	if (b)
	{
		char * tpos;
		tpos = filename;
		for (int i=0; i<l; i++)
		{
			if (filename[i] == 92     ||     filename[i] == 47)
				tpos = &(filename[i+1]);
		}
		sprintf_s(b, l+1, "%s", tpos);
		return b;
	}
	return NULL;
}

char * addOneToFilename(char * filename)
{
	if (!filename)
		return NULL;

	int l = (int)strlen(filename);
	char * res = (char *)malloc(l+3);
	if (!res)
		return NULL;

	char * kr = strrchr(filename, '.');
	if (kr==NULL)
	{
		sprintf_s(res, l+3, "%s1", filename);
		return res;
	}
	else
	{
		int lbezkropki = (int)(kr - filename);
		int lpokropce = l - lbezkropki - 1;
		sprintf_s(res, l+3, "%s", filename);
		sprintf_s(res+lbezkropki, l+3-lbezkropki,"1.%s", filename+lbezkropki+1);
		return res;
	}
}

bool dirExists(char * dirName)
{
	if (!dirName)
		return false;

	bool res;
	HANDLE hDir;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hDir = FindFirstFile(dirName, &fd);
	
	if (hDir == INVALID_HANDLE_VALUE   ||   !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hDir != INVALID_HANDLE_VALUE)
		FindClose(hDir);

	return res;
}


bool fileExists(char * filename)		// will return false on dir
{
	if (!filename)
		return false;

	bool res;
	HANDLE hFile;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hFile = FindFirstFile(filename, &fd);
	
	if (hFile == INVALID_HANDLE_VALUE   ||   (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hFile != INVALID_HANDLE_VALUE)
		FindClose(hFile);

	return res;
}

int fileSize(char * filename)
{
	HANDLE hfile = CreateFile(filename, FILE_READ_DATA, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (!hfile)
		return -1;
	int res = GetFileSize(hfile, NULL);
	CloseHandle(hfile);
	return res;
}

bool verifyUserFolder()
{
	TCHAR szPath[MAX_PATH];
	SHGetSpecialFolderPath(0, szPath, CSIDL_LOCAL_APPDATA, FALSE);
	// in blender use this: os.environ['LOCALAPPDATA']
	bool dirok1 = false;
	if (dirExists(szPath))
	{
		char * dir1 = joinStrings(szPath, "\\Evermotion");
		char * dir2 = joinStrings(szPath, "\\Evermotion\\NOX");
		if (!dirExists(dir1))
			CreateDirectory(dir1, NULL);
		if (!dirExists(dir2))
			CreateDirectory(dir2, NULL);
		if (dirExists(dir2))
			dirok1 = true;
		userDirectory = dir2;
		free(dir1);
	}
	if (!dirok1)
	{
		userDirectory = copyString(defaultDirectory);
	}
	return true;
}

bool doRegistryStuffOnStartup(bool showLog)
{
	// get default directory
	DWORD rsize = 2048;
	char buffer[2048];
	char buffer2[2048];
	char buffer3[2048];
	LONG regres, regres2=1;
	HKEY key1, key2, key3, key4;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	regres2 = regres;
	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres = RegQueryValueEx(key3, "directory", NULL, &type, (BYTE*)buffer, &rsize);

		buffer3[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "userdirectory", NULL, &type, (BYTE*)buffer3, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres != ERROR_SUCCESS)
	{
		regres = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SOFTWARE", 0, KEY_READ, &key1);
		if (regres == ERROR_SUCCESS)
			regres = RegOpenKeyEx(key1, "Microsoft", 0, KEY_READ, &key2);
		if (regres == ERROR_SUCCESS)
			regres = RegOpenKeyEx(key2, "Windows", 0, KEY_READ, &key3);
		if (regres == ERROR_SUCCESS)
			regres = RegOpenKeyEx(key3, "CurrentVersion", 0, KEY_READ, &key4);
		
		if (regres == ERROR_SUCCESS)
		{	// get program files x86 if 64 bit system dir
			rsize = 2048;
			regres = RegQueryValueEx(key4, "ProgramFilesDir (x86)", NULL, &type, (BYTE*)buffer, &rsize);
			if (regres != ERROR_SUCCESS)
			{	// get program files - normal way otherwise
				rsize = 2048;
				regres = RegQueryValueEx(key4, "ProgramFilesDir", NULL, &type, (BYTE*)buffer, &rsize);
				if (regres != ERROR_SUCCESS)
				{
					sprintf_s(buffer, rsize, "C:\\");
				}
			}
			// close reg keys
			RegCloseKey(key4);
			RegCloseKey(key3);
			RegCloseKey(key2);
			RegCloseKey(key1);
		}

		size_t l = strlen(buffer);
		if (l > 0)
		{
			if (buffer[l-1] == '\\'  ||  buffer[l-1] == '/')
				buffer[l-1] = 0;
		}

		sprintf_s(buffer2, 2048, "%s\\Evermotion\\Renderer", buffer);

		regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_WRITE, &key1);
		if (regres == ERROR_SUCCESS)
			regres = RegCreateKey(key1, "Evermotion", &key2);
		if (regres == ERROR_SUCCESS)
			regres = RegCreateKey(key2, "Renderer", &key3);

		if (regres == ERROR_SUCCESS)
			regres = RegSetValueEx(key3, "directory", 0, REG_SZ, (BYTE*)buffer2, 2048);

		RegCloseKey(key3);
		RegCloseKey(key2);
		RegCloseKey(key1);
		
		sprintf_s(buffer, 2048, "%s", buffer2);
	}

	defaultDirectory = (char *)malloc(strlen(buffer)+1);
	sprintf_s(defaultDirectory, strlen(buffer)+1, "%s", buffer);

	if (regres2 == ERROR_SUCCESS)
	{
		userDirectory = copyString(buffer3);
		if (!dirExists(userDirectory))
			regres2 = 1;
	}

	if (regres2 != ERROR_SUCCESS)
	{
		verifyUserFolder();

		regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_WRITE, &key1);
		if (regres == ERROR_SUCCESS)
			regres = RegCreateKey(key1, "Evermotion", &key2);
		if (regres == ERROR_SUCCESS)
			regres = RegCreateKey(key2, "Renderer", &key3);

		if (regres == ERROR_SUCCESS)
			regres = RegSetValueEx(key3, "userdirectory", 0, REG_SZ, (BYTE*)userDirectory, (DWORD)strlen(userDirectory));

		RegCloseKey(key3);
		RegCloseKey(key2);
		RegCloseKey(key1);
	}

	char * logdir = joinStrings(userDirectory, "\\logs");
	if (!dirExists(logdir))
		CreateDirectory(logdir, NULL);
	Logger::setLogDir(logdir);
	if (showLog)
	{
		#ifdef _WIN64
			Logger::add("Running 64 bits.");
		#else
			Logger::add("Running 32 bits.");
		#endif
		Logger::add("Default directory from registry:");
		Logger::add(buffer);
	}

	// check if directories exists
	if (showLog)
		Logger::add("Checking directories...");
	bool dirOK = true;
	char dirName[2048];

	sprintf_s(dirName, 2048, "%s", buffer);
	if (!dirExists(dirName))
	{
		dirOK = false;
		if (showLog)
		{
			Logger::add("Couldn't find directory:");
			Logger::add(dirName);
		}
	}

	CrashLog * clog = CrashLog::getInstance();
	if (clog)
		clog->setCrashLogFolder(logdir);

	#ifdef _WIN64
		sprintf_s(dirName, 2048, "%s\\dll\\64", buffer);
	#else
		sprintf_s(dirName, 2048, "%s\\dll\\32", buffer);
	#endif
	if (clog)
		clog->setSymbolPath(dirName);

	sprintf_s(dirName, 2048, "%s\\bin\\32\\NOXReportCrash.exe", buffer);
	char buuuff[2048];
	sprintf_s(buuuff, 2048, "\"%s\\bin\\32\\NOXReportCrash.exe\" -sendreport", buffer);
	if (clog)
		clog->setCrashRunApp(dirName, buuuff);


	sprintf_s(dirName, 2048, "%s\\settings", buffer);
	if (!dirExists(dirName))
	{
		dirOK = false;
		if (showLog)
		{
			Logger::add("Couldn't find directory:");
			Logger::add(dirName);
		}
	}
	sprintf_s(dirName, 2048, "%s\\settings\\themes", buffer);
	if (!dirExists(dirName))
	{
		dirOK = false;
		if (showLog)
		{
			Logger::add("Couldn't find directory:");
			Logger::add(dirName);
		}
	}
	sprintf_s(dirName, 2048, "%s\\materials", userDirectory);
	if (!dirExists(dirName))
	{
		dirOK = false;
		Logger::add("Couldn't find directory (will create):");
		Logger::add(dirName);
		if (!CreateDirectory(dirName, NULL))
		{
			if (showLog)
				Logger::add("  ... creation failed");
		}
	}
	sprintf_s(dirName, 2048, "%s\\exported scenes", userDirectory);
	if (!dirExists(dirName))
	{
		dirOK = false;
		Logger::add("Couldn't find directory (will create):");
		Logger::add(dirName);
		if (!CreateDirectory(dirName, NULL))
		{
			if (showLog)
				Logger::add("  ... creation failed");
		}
	}
	for (int i=0; i<23; i++)
	{
		sprintf_s(dirName, 2048, "%s\\materials\\%s", userDirectory, MatCategory::getCategoryName(i));
		if (!dirExists(dirName))
		{
			dirOK = false;
			if (showLog)
			{
				Logger::add("Couldn't find directory (will create):");
				Logger::add(dirName);
			}
			if (!CreateDirectory(dirName, NULL))
			{
				if (showLog)
					Logger::add("  ... creation failed");
			}
		}
	}

	if (dirOK)
	{
		if (showLog)
			Logger::add("Directories OK.");
	}

	MatEditorWindow::onlinematlib.remember = loadUserPassFromRegistry(&(MatEditorWindow::onlinematlib.username), &(MatEditorWindow::onlinematlib.passwordhash), 13);

	return true;
}



//-----------------------------------------------------------------------------------------------------------------------

char * openFileDialog(HWND hwnd, char * loadFilter, char * dialogTitle, char * defaultExtension, int extensionDefaultIndex, char * initDir)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hDllModule;
	ofn.lpstrFilter = loadFilter;
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = extensionDefaultIndex;
	ofn.lpstrFile = (char *)malloc(1024);
	ofn.lpstrFile[0] = 0;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	if (initDir)
		ofn.lpstrInitialDir = initDir;
	else
		ofn.lpstrInitialDir = "";
	ofn.lpstrTitle = dialogTitle;
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = defaultExtension;
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	bool res = (GetOpenFileName(&ofn) == TRUE);
	if (res)
	{
		return ofn.lpstrFile;
	}
	else
	{
		free(ofn.lpstrFile);
		return NULL;
	}
	
	return NULL;
}

//-----------------------------------------------------------------------------------------------------------------------

INT CALLBACK BrowseCallbackProc(HWND hwnd, 
                                UINT uMsg,
                                LPARAM lp, 
                                LPARAM pData) 
{
	switch(uMsg) 
	{
		case BFFM_INITIALIZED: 
			if (pData)
				SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)pData);
			break;
	}
	return 0;
}

char * openDirectoryDialog(HWND hwnd, char* title, char * startDir)
{
	char * res = (char *)malloc(MAX_PATH);
	res[0] = 0;
	BROWSEINFO bi;
	bi.hwndOwner = hwnd;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = res;
	bi.lpszTitle = title;
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;
	bi.lpfn = BrowseCallbackProc;
	bi.lParam = (LPARAM)startDir;
	bi.iImage = 0;

	HRESULT hres = CoInitialize(NULL);
	
	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	if (pidl)
	{
		BOOL bRet = SHGetPathFromIDList(pidl,res);
		if (bRet)
			return res;
	}
	else
		return NULL;

	CoUninitialize();

	free(res);
	return NULL;
}

//-----------------------------------------------------------------------------------------------------------------------

char * saveDirectoryDialog(HWND hwnd, char* title, char * startDir)
{
	char * res = (char *)malloc(MAX_PATH);
	res[0] = 0;
	BROWSEINFO bi;
	bi.hwndOwner = hwnd;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = res;
	bi.lpszTitle = title;
	bi.ulFlags = BIF_RETURNONLYFSDIRS;
	bi.lpfn = BrowseCallbackProc;
	bi.lParam = (LPARAM)startDir;
	bi.iImage = 0;

	HRESULT hres = CoInitialize(NULL);
	
	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	if (pidl)
	{
		BOOL bRet = SHGetPathFromIDList(pidl,res);
		if (bRet)
			return res;
	}
	else
		return NULL;

	CoUninitialize();

	free(res);
	return NULL;
}

//-----------------------------------------------------------------------------------------------------------------------

char * saveFileDialog(HWND hwnd, char * saveFilter, char * dialogTitle, char * defaultExtension, int extensionDefaultIndex)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hDllModule;
	ofn.lpstrFilter = saveFilter;
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = extensionDefaultIndex;
	ofn.lpstrFile = (char *)malloc(1024);
	ofn.lpstrFile[0] = 0;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = "";
	ofn.lpstrTitle = dialogTitle;
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = defaultExtension;
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT;

	bool res = (GetSaveFileName(&ofn) == TRUE);
	if (res)
	{
		return ofn.lpstrFile;
	}
	else
	{
		free(ofn.lpstrFile);
		return NULL;
	}
	
	return NULL;
}


BOOL CALLBACK OpenFileHookProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch (message) 
	{
		case WM_INITDIALOG:
		{
			HWND hHelp = GetDlgItem(hDlg, 1038);
			SetWindowText(hHelp, "Options");
			return TRUE;
		}
		case WM_SIZE: 
		{
			HWND hHelp = GetDlgItem(hDlg, 1038);
			SetWindowText(hHelp, "Options");
		}
		break;
		case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case 1038: 
				{
				}
				break;
			}
		}
		break;
		default:
		break;
	}
	return FALSE;
}

char * saveFileDialogWithOptions(HWND hwnd, char * saveFilter, char * dialogTitle, char * defaultExtension, int extensionDefaultIndex)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hDllModule;
	ofn.lpstrFilter = saveFilter;
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = extensionDefaultIndex;
	ofn.lpstrFile = (char *)malloc(1024);
	ofn.lpstrFile[0] = 0;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = "";
	ofn.lpstrTitle = dialogTitle;
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = defaultExtension;
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT;

	ofn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT  | 		 OFN_ENABLEHOOK | OFN_EXPLORER | OFN_SHOWHELP | OFN_ENABLESIZING;
	ofn.lpfnHook           = (LPOFNHOOKPROC)OpenFileHookProc;

	bool res = (GetSaveFileName(&ofn) == TRUE);
	if (res)
	{
		return ofn.lpstrFile;
	}
	else
	{
		free(ofn.lpstrFile);
		return NULL;
	}
	
	return NULL;
}


void disableControl(HWND hDlg, int itemID)
{
	HWND hItem = GetDlgItem(hDlg, itemID);
	EnableWindow(hItem, false);
	if (hItem)
		InvalidateRect(hItem, NULL, false);
}

void enableControl(HWND hDlg, int itemID)
{
	HWND hItem = GetDlgItem(hDlg, itemID);
	EnableWindow(hItem, true);
	if (hItem)
		InvalidateRect(hItem, NULL, false);
}

void setControlEnabled(bool enabled, HWND hDlg, int itemID)
{
	HWND hItem = GetDlgItem(hDlg, itemID);
	EnableWindow(hItem, enabled);
	if (hItem)
		InvalidateRect(hItem, NULL, false);
}

char * getDirectory(char * filename)	// will cut last "\"
{
	if (!filename)
		return NULL;
	int s = (int)strlen(filename) + 1;
	if (s < 1)
		return NULL;
	
	char * fname = strrchr(filename, '\\');
	if (!fname)
		return NULL;

	char * res = (char*)malloc(s);
	if (!res)
		return NULL;

	sprintf_s(res, s, "%s", filename);
	res[fname-filename] = 0;
	
	return res;
}

typedef BOOL (WINAPI *LPFN_GLPI)(
    PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, 
	PDWORD);

DWORD HelperCountSetBits(ULONG_PTR bitMask)
{
    DWORD LSHIFT = sizeof(ULONG_PTR)*8 - 1;
    DWORD bitSetCount = 0;
    ULONG_PTR bitTest = (ULONG_PTR)1 << LSHIFT;    
    DWORD i;
    
    for (i = 0; i <= LSHIFT; ++i)
    {
        bitSetCount += ((bitMask & bitTest)?1:0);
        bitTest/=2;
    }

    return bitSetCount;
}

bool getProcessorInfoAdvanced(int &numCPUs, int &numCores, int &numThreads)
{
    LPFN_GLPI glpi;
    BOOL done = FALSE;
    PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;
    PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
    DWORD returnLength = 0;
    DWORD logicalProcessorCount = 0;
    DWORD numaNodeCount = 0;
    DWORD processorCoreCount = 0;
    DWORD processorL1CacheCount = 0;
    DWORD processorL2CacheCount = 0;
    DWORD processorL3CacheCount = 0;
    DWORD processorPackageCount = 0;
    DWORD byteOffset = 0;
    PCACHE_DESCRIPTOR Cache;

    glpi = (LPFN_GLPI) GetProcAddress(GetModuleHandle(TEXT("kernel32")), "GetLogicalProcessorInformation");
    if (!glpi) 
        return false;

    while (!done)
    {
        DWORD rc = glpi(buffer, &returnLength);
        if (FALSE == rc) 
        {
            if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) 
            {
                if (buffer) 
                    free(buffer);
                buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(
                        returnLength);
                if (!buffer) 
					return false;
            } 
            else 
				return false;
        } 
        else
        {
            done = TRUE;
        }
    }

    ptr = buffer;

    while (byteOffset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= returnLength) 
    {
        switch (ptr->Relationship) 
        {
        case RelationNumaNode:
            numaNodeCount++;
            break;
        case RelationProcessorCore:
            processorCoreCount++;
            logicalProcessorCount += HelperCountSetBits(ptr->ProcessorMask);
            break;
        case RelationCache:
            Cache = &ptr->Cache;
            if (Cache->Level == 1)
            {
                processorL1CacheCount++;
            }
            else if (Cache->Level == 2)
            {
                processorL2CacheCount++;
            }
            else if (Cache->Level == 3)
            {
                processorL3CacheCount++;
            }
            break;

        case RelationProcessorPackage:
            processorPackageCount++;
            break;
        default:
            break;
        }
        byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
        ptr++;
    }

	numCPUs = processorPackageCount;
	numCores = processorCoreCount;
	numThreads = logicalProcessorCount;
    
    free(buffer);
	return true;
}


char * getProcessorInfo(bool addnumcores)
{
	char info[2048];
	char info2[2048];
	char * res = NULL;
	LONG regres;
	HKEY key1, key2, key3, key4, key5;
	DWORD type;

	SYSTEM_INFO si;
	GetSystemInfo(&si);
	if (si.dwNumberOfProcessors < 1)
		si.dwNumberOfProcessors = 1;


	regres = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "DESCRIPTION", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "System", 0, KEY_READ, &key3);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key3, "CentralProcessor", 0, KEY_READ, &key4);

	if (regres == ERROR_SUCCESS)
	{
		regres = RegOpenKeyEx(key4, "0", 0, KEY_READ, &key5);
		if (regres == ERROR_SUCCESS)
		{
			info[0] = 0;
			DWORD rsize = 2048;
			regres = RegQueryValueEx(key5, "ProcessorNameString", NULL, &type, (BYTE*)info, &rsize);
		}
	}

	if (regres == ERROR_SUCCESS)
	{
		int s1 = (int)strlen(info)+1;
		if (addnumcores)
			sprintf_s(info2, 2048, "%s, %d cores", info, si.dwNumberOfProcessors);
		else
			sprintf_s(info2, 2048, "%s", info);
		int s2 = (int)strlen(info2)+1;
		res = (char *)malloc(s2);
		if (res)
			sprintf_s(res, s2, "%s", info2);
	}

	RegCloseKey(key5);
	RegCloseKey(key4);
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	return res;
}

DWORDLONG getMemorySize()
{
	MEMORYSTATUSEX stat;
	stat.dwLength = sizeof(stat);
	GlobalMemoryStatusEx (&stat);
	DWORDLONG res = stat.ullTotalPhys;
	return res;
}

char * getFormatedMemorySize()
{
	char memFrm[64];
	DWORDLONG m64 = getMemorySize();
	double m = (double)m64;
	if (m > 1023)
	{
		if (m > 1048575)
		{
			if (m > 1073741823)
				sprintf_s(memFrm, 64, "%1.2f GB", m/1073741824.0);
			else
				sprintf_s(memFrm, 64, "%1.2f MB", m/1048576.0);
		}
		else
			sprintf_s(memFrm, 64, "%1.2f KB", m/1024.0);
	}
	else
		sprintf_s(memFrm, 64, "%d B", m);

	int s = (int)sizeof(memFrm)+1;
	char * res = (char*)malloc(s);
	if (res)
		sprintf_s(res, s, "%s", memFrm);
	
	return res;
}

char * copyString(char * src, unsigned int addSpace)
{
	if (!src  &&  addSpace==0)
		return NULL;
	int l = src ? (int)strlen(src) : 0;
	char * res = (char *)malloc(l+1+addSpace);
	res[0] = 0;
	if (src)
		strcpy_s(res, l+1+addSpace, src);
	return res;
}

char * joinStrings(char * str1, char * str2)
{
	if (!str1)
		if (!str2)
			return NULL;
		else
			return copyString(str2);
	else
		if (!str2)
			return copyString(str1);
	int l1 = (int)strlen(str1);
	int l2 = (int)strlen(str2);
	int ll = l1+l2+2;
	char * res = (char *)malloc(ll);
	if (!res)
		return NULL;
	sprintf_s(res, ll, "%s%s", str1, str2);
	return res;
}

char * joinStrings(char * str1, char * str2, char * str3)
{
	if (!str1)
		return joinStrings(str2, str3);
	if (!str2)
		return joinStrings(str1, str3);
	if (!str3)
		return joinStrings(str1, str2);
	int l1 = (int)strlen(str1);
	int l2 = (int)strlen(str2);
	int l3 = (int)strlen(str3);
	int ll = l1+l2+l3+1;
	char * res = (char *)malloc(ll);
	if (!res)
		return NULL;
	sprintf_s(res, ll, "%s%s%s", str1, str2, str3);
	return res;

}

bool formatStringFromUnsignedInteger(char * buf, int bufSize, int numDigits, unsigned int number)
{
	int needed = 1;
	long long tmp = 10;
	while (tmp<=number)
	{
		tmp *= 10;
		needed++;
	}
	if (needed+1>bufSize)
		return false;

	int numzeros = max(0, numDigits-needed);
	for (int i=0; i<numzeros; i++)
		sprintf_s(buf+i, bufSize-i, "0");
	sprintf_s(buf+numzeros, bufSize-numzeros, "%d", number);
	return true;
}

bool correctFilename(char * filename)
{
	if (!filename)
		return false;
	if (strlen(filename) < 1)
		return false;

	char invalidChars[] = {'\\', '/', '*', ':', '"', '<', '>', '?', '|'};

	bool nameOK = false;
	char * c = NULL;
	while (!nameOK)
	{
		nameOK = true;
		for (int i=0; i<9; i++)
		{
			char * c = strchr(filename, invalidChars[i]);
			if (c)
			{
				nameOK = false;
				*c = '_';
			}
		}
	}
	return true;
}

char * createMD5(char * txt)
{
	HCRYPTPROV hCryptProv;
	HCRYPTHASH hHash;
	BYTE *pData;
	DWORD dwHashLength;
	DWORD dwLength;

	CHECK(txt);
	int l = (int)strlen(txt);

	if (CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
	{
		if(CryptCreateHash(hCryptProv, CALG_MD5, 0, 0, &hHash))
		{
			if(CryptHashData(hHash, (BYTE*)txt, l, 0))
			{
				CryptGetHashParam(hHash, HP_HASHSIZE, (BYTE*)&dwHashLength, &dwLength, 0);
				pData = (BYTE*)malloc(dwHashLength+1);
				CryptGetHashParam(hHash, HP_HASHVAL, pData, &dwHashLength, 0);
				CryptDestroyHash(hHash);
				CryptReleaseContext(hCryptProv, 0);

				char * res = (char*)malloc(2*dwHashLength + 1);
				for (unsigned int i=0; i<dwHashLength; i++)
				{
					char c1 = pData[i]/16;
					char c2 = pData[i]%16;
					res[2*i  ] = c1>9?'a'+c1-10:'0'+c1;
					res[2*i+1] = c2>9?'a'+c2-10:'0'+c2;
				}
				res[2*dwHashLength] = 0;
				free(pData);

				return (char*)res;
			}
		}
	}

	return NULL;
//}
}

bool saveUserPassToRegistry(char * user, char * password, char salt)
{
	CHECK(user);
	CHECK(password);
	CHECK(strlen(user));
	CHECK(strlen(password)>1);
	CHECK(strlen(password)%2==0);

	//--------------------------------------------


	int pl = (int)strlen(password);
	_strlwr_s(password, pl+1);
	int ul = (int)strlen(user);
	unsigned char * userxorpass = (unsigned char*)malloc(pl+2);
	unsigned char * userxorpassxorsalt = (unsigned char*)malloc(pl+2);
	unsigned char * dec = (unsigned char*)malloc(pl+2);
	for (int i=0; i<pl/2; i++)
	{
		char c1 = password[i*2];
		char c2 = password[i*2+1];
		if (c1<'0'  ||  (c1>'9' && c1<'a')  ||  c1>'f')
			return false;
		if (c2<'0'  ||  (c2>'9' && c2<'a')  ||  c2>'f')
			return false;
		int d1 = (c1<='9')? c1-'0' : c1-'a'+10;
		int d2 = (c2<='9')? c2-'0' : c2-'a'+10;
		dec[i] = 16*d1+d2;
	}
	dec[pl/2] = 0;

	for (int i=0; i<pl/2; i++)
		userxorpass[i] = dec[i] ^ user[i%ul];
	userxorpass[pl/2] = 0;

	for (int i=0; i<pl/2; i++)
		userxorpassxorsalt[i] = userxorpass[i] ^ salt;
	userxorpassxorsalt[pl/2] = 0;

	// check after xor salt
	char * pass4 = (char*)malloc(pl+2);
	for (int i=0; i<pl/2; i++)
	{
		char c1 = userxorpassxorsalt[i]/16;
		char c2 = userxorpassxorsalt[i]%16;
		pass4[2*i  ] = c1>9?'a'+c1-10:'0'+c1;
		pass4[2*i+1] = c2>9?'a'+c2-10:'0'+c2;
	}
	pass4[pl] = 0;

	//--------------------------------------------

	LONG regres;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_WRITE, &key3);

	if (regres == ERROR_SUCCESS)
		regres = RegSetValueEx(key3, "username", 0, REG_SZ, (BYTE*)user, (DWORD)strlen(user));

	if (regres == ERROR_SUCCESS)
		regres = RegSetValueEx(key3, "password", 0, REG_SZ, (BYTE*)(char*)pass4, (DWORD)strlen((char*)pass4));

	free(userxorpass);
	free(userxorpassxorsalt);
	free(dec);
	free(pass4);

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
		return true;

	return false;
}

bool loadUserPassFromRegistry(char ** user, char ** password, char salt)
{
	LONG regres;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	char ubuffer[2048];
	char cbuffer[2048];
	ubuffer[0] = 0;
	cbuffer[0] = 0;
	DWORD ursize = 2048;
	DWORD crsize = 2048;
	DWORD utype;
	DWORD ctype;

	if (regres == ERROR_SUCCESS)
		regres = RegQueryValueEx(key3, "username", NULL, &utype, (BYTE*)ubuffer, &ursize);
	if (regres == ERROR_SUCCESS)
		regres = RegQueryValueEx(key3, "password", NULL, &ctype, (BYTE*)cbuffer, &crsize);

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres != ERROR_SUCCESS)
		return false;

	CHECK(utype==REG_SZ);
	CHECK(ctype==REG_SZ);
	
	//---------------------------------------

	int pl = (int)strlen(cbuffer);
	CHECK(pl>1);
	_strlwr_s(cbuffer, pl+1);
	int ul = (int)strlen(ubuffer);
	unsigned char * userxorpass = (unsigned char*)malloc(pl+2);
	unsigned char * userxorpassxorsalt = (unsigned char*)malloc(pl+2);
	unsigned char * dec = (unsigned char*)malloc(pl+2);
	for (int i=0; i<pl/2; i++)
	{
		char c1 = cbuffer[i*2];
		char c2 = cbuffer[i*2+1];
		if (c1<'0'  ||  (c1>'9' && c1<'a')  ||  c1>'f')
			return false;
		if (c2<'0'  ||  (c2>'9' && c2<'a')  ||  c2>'f')
			return false;
		int d1 = (c1<='9')? c1-'0' : c1-'a'+10;
		int d2 = (c2<='9')? c2-'0' : c2-'a'+10;
		dec[i] = 16*d1+d2;
	}
	dec[pl/2] = 0;

	for (int i=0; i<pl/2; i++)
		userxorpass[i] = dec[i] ^ ubuffer[i%ul];
	userxorpass[pl/2] = 0;

	for (int i=0; i<pl/2; i++)
		userxorpassxorsalt[i] = userxorpass[i] ^ salt;
	userxorpassxorsalt[pl/2] = 0;

	// check after xor salt
	char * pass4 = (char*)malloc(pl+2);
	for (int i=0; i<pl/2; i++)
	{
		char c1 = userxorpassxorsalt[i]/16;
		char c2 = userxorpassxorsalt[i]%16;
		pass4[2*i  ] = c1>9?'a'+c1-10:'0'+c1;
		pass4[2*i+1] = c2>9?'a'+c2-10:'0'+c2;
	}
	pass4[pl] = 0;

	*user = copyString(ubuffer);
	*password = copyString(pass4);
	CHECK(*user);
	CHECK(*password);

	free(userxorpass);
	free(userxorpassxorsalt);
	free(dec);
	free(pass4);

	return true;
}

bool delUserPassFromRegistry()
{
	LONG regres;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_SET_VALUE, &key3);

	if (regres != ERROR_SUCCESS)
		return false;

	regres = RegDeleteValue(key3, "username");
	regres = RegDeleteValue(key3, "password");

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	return true;
}

char * getTempDirectory()
{
	char temppath[2048];
	int ll = GetTempPath(2048, temppath);
	if (ll > 2048)
		return NULL;
	if (ll < 3)
		return NULL;

	char temppathlong[2048];
	int ll2 = GetLongPathName(temppath, temppathlong, 2048);
	if (ll2 > 2048)
		return NULL;

	if (temppathlong[ll2-1] == '\\'  ||  temppathlong[ll2-1] == '/')
		temppathlong[ll2-1] = 0;

	if (!dirExists(temppathlong))
		return NULL;

	char * res = copyString(temppathlong);
	return res;
}

char * randTempFilename(char * prefix)
{
	char tempRes[2048];
	char * temppathlong = getTempDirectory();
	if (!temppathlong)
		return NULL;

	char * gprefix = "nox";
	if (prefix)
	{
		int l = (int)strlen(prefix);
		if (l>0 && l<4)
			gprefix = prefix;
	}


	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(temppathlong, "nox", unique, tempRes);

	if (strlen(tempRes) < strlen(temppathlong))
	{
		free(temppathlong);
		return NULL;
	}

	int ll3 = (int)strlen(tempRes)+1;
	char * result = (char*)malloc(ll3);
	if (!result)
	{
		free(temppathlong);
		return NULL;
	}
	sprintf_s(result, ll3, "%s", tempRes);

	return result;
}

bool checkLegacyModeFromRegistry()
{
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres, regres2;
	HKEY key1, key2, key3;
	DWORD type;
	bool result = false;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "autoupdate" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "legacymode", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				result = (val==1);
			}
		}
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	return result;
}

bool checkBetaTesterStatusFromRegistry()
{
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres, regres2;
	HKEY key1, key2, key3;
	DWORD type;
	bool result = false;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "autoupdate" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "betatester", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				result = (val==1);
			}
		}

	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	return result;
}

bool checkAutoupdateStatusFromRegistry()
{
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres, regres2;
	HKEY key1, key2, key3;
	DWORD type;
	bool result = false;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "autoupdate" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "autoupdate", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				result = (val==1);
			}
		}

	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	return result;
}

bool checkLRegStatus()
{
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres, regres2;
	HKEY key1, key2, key3;
	DWORD type;
	bool result = false;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "autoupdate" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "noxlogo", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				result = (val==1);
			}
		}

	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	return result;
}

bool setAutoupdateStatusInRegistry(bool on)
{
	LONG regres, regres2;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_WRITE, &key3);

	// set value
	if (regres == ERROR_SUCCESS)
	{
		DWORD val = (on ? 1 : 0);
		regres2 = RegSetValueEx(key3, "autoupdate", 0, REG_DWORD, (BYTE*)&val, 4);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS   &&   regres2 == ERROR_SUCCESS)
		return true;
	else
		return false;
}


void invalidateWithParentRedraw(HWND hWnd)
{
	RECT wrect;
	GetWindowRect(hWnd, &wrect);
	POINT p;
	p.x = wrect.left;	p.y = wrect.top;
	ScreenToClient(GetParent(hWnd), &p);
	wrect.right = wrect.right + p.x - wrect.left;
	wrect.bottom = wrect.bottom + p.y - wrect.top;
	wrect.left = p.x;
	wrect.top = p.y;
	InvalidateRect(GetParent(hWnd), &wrect, false);
}

bool isFileExtSceneNox(char * filename)
{
	if (!filename)
		return false;
	int l = (int)strlen(filename);
	if (l<5)
		return false;
	if (filename[l-4]!='.')
		return false;
	if (filename[l-3]!='n'  &&  filename[l-3]!='N')
		return false;
	if (filename[l-2]!='o'  &&  filename[l-2]!='O')
		return false;
	if (filename[l-1]!='x'  &&  filename[l-1]!='X')
		return false;

	return true;
}

bool isFileExtSceneNxs(char * filename)
{
	if (!filename)
		return false;
	int l = (int)strlen(filename);
	if (l<5)
		return false;
	if (filename[l-4]!='.')
		return false;
	if (filename[l-3]!='n'  &&  filename[l-3]!='N')
		return false;
	if (filename[l-2]!='x'  &&  filename[l-2]!='X')
		return false;
	if (filename[l-1]!='s'  &&  filename[l-1]!='S')
		return false;

	return true;
}

bool isFileExtPreset(char * filename)
{
	if (!filename)
		return false;
	int l = (int)strlen(filename);
	if (l<5)
		return false;
	if (filename[l-4]!='.')
		return false;
	if (filename[l-3]!='n'  &&  filename[l-3]!='N')
		return false;
	if (filename[l-2]!='p'  &&  filename[l-2]!='P')
		return false;
	if (filename[l-1]!='r'  &&  filename[l-1]!='R')
		return false;

	return true;
}

bool isFileExtMaterial(char * filename)
{
	if (!filename)
		return false;
	int l = (int)strlen(filename);
	if (l<5)
		return false;
	if (filename[l-4]!='.')
		return false;
	if (filename[l-3]!='n'  &&  filename[l-3]!='N')
		return false;
	if (filename[l-2]!='x'  &&  filename[l-2]!='X')
		return false;
	if (filename[l-1]!='m'  &&  filename[l-1]!='M')
		return false;

	return true;
}

