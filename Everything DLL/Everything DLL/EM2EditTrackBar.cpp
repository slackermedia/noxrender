#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"

//#define GUI2_USE_GDIPLUS

extern HMODULE hDllModule;
extern _locale_t noxLocale;
extern _locale_t noxLocaleComa;

#define BN_X 9
#define BN_Y 29
#define BN_W 13
#define BN_H 13
#define BM_X 29
#define BM_Y 29
#define BM_W 13
#define BM_H 13
#define BC_X 49
#define BC_Y 29
#define BC_W 13
#define BC_H 13
#define BD_X 69
#define BD_Y 29
#define BD_W 13
#define BD_H 13


//------------------------------------------------------------------------------------------------

EM2EditTrackBar::EM2EditTrackBar(HWND hWnd) 
{
	colEditBackground = RGB(0x3f,0x3f,0x3f);
	colEditText = NGCOL_LIGHT_GRAY;

	colDisabledEditBorderOuter = RGB(0x17,0x17,0x17);
	colDisabledEditText = NGCOL_DARK_GRAY;
	colDisabledEditText = NGCOL_TEXT_DISABLED;
	colDisabledEditBackground = RGB(0x28,0x28,0x28);

	colDisabledPathBg = NGCOL_DARK_GRAY;
	colDisabledPathBorder = RGB(16,16,16);
	alphaDisabledPath = 128;
	alphaDisabledPathBorder = 128;

	colEditBorderOuter = RGB(0x10,0x10,0x10);
	colEditBorderLeft = RGB(0x3f,0x3f,0x3f);
	colEditBorderTop = RGB(0x3f,0x3f,0x3f);
	colEditBorderRight = RGB(0x47,0x47,0x47);
	colEditBorderBottom = RGB(0x4e,0x4e,0x4e);

	colPathBackgroundBefore = NGCOL_YELLOW;
	colPathBackgroundAfter = RGB(0,128,255);
	colPathBorder = RGB(0,0,0);
	alphaPathBefore = 64;
	alphaPathAfter = 64;
	alphaPathBorder = 96;

	hEditBrush = CreateSolidBrush(colEditBackground);
	hEditDisabledBrush = CreateSolidBrush(colDisabledEditBackground);
	hwnd = hWnd;
	hEdit = 0;
	allowFloat = true;
	nowScrolling = false;
	nowChanged = false;
	nowMouseOver = false;
	useCursors = true;

	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfont = true;
	bgImage = 0;
	bgShiftX = bgShiftY = 0;


	minIntValue = 0;
	maxIntValue = 100;
	minFloatValue = 0;
	maxFloatValue = 100;
	intValue = minIntValue;
	floatValue = minFloatValue;
	floatDefaultValue = 0.0f;
	intDefaultValue = 0;
	intFastValue = 5;
	floatFastValue = 5;
	floatAfterDot = 2;

	spaceAfterEdit = 5;
	height = 18;
	editRectWidth = 64;
	pathHeight = 1;
	pathXStart = BN_W/2 + spaceAfterEdit;
	sliderWidth = 8;
	sliderHeight = 16;
	sliderPos = 0;
	resize(NULL);
	updateMouseRectangles();
}

//------------------------------------------------------------------------------------------------

EM2EditTrackBar::~EM2EditTrackBar() 
{
	if (hEditBrush)
		DeleteObject(hEditBrush);
	hEditBrush = 0;
	if  (hFont  &&  autodelfont)
		DeleteObject(hFont);
	hFont = 0;
}

//------------------------------------------------------------------------------------------------

void InitEM2EditTrackBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2EditTrackBar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2EditTrackBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2EditTrackBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//------------------------------------------------------------------------------------------------

EM2EditTrackBar * GetEM2EditTrackBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2EditTrackBar * emetb = (EM2EditTrackBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2EditTrackBar * emetb = (EM2EditTrackBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emetb;
}

//------------------------------------------------------------------------------------------------

void SetEM2EditTrackBarInstance(HWND hwnd, EM2EditTrackBar *emetb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emetb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emetb);
	#endif
}

//------------------------------------------------------------------------------------------------

typedef LRESULT (WINAPI * EDITPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EDITPROC OldTrackBar2Proc;

//------------------------------------------------------------------------------------------------

void verifyEditTrackbar2ControlData(HWND hwnd, int &lastIntValue, float &lastFloatValue)
{
	char * addr;
	char * addr1;
	char * addr2;
	char buff[512];
	memset(buff, 0, 512);
	((WORD*)buff)[0] = 512;
	int cpied = (int)SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
	if (cpied>=0)
		buff[cpied]=0;
	HWND hFazia = GetParent(hwnd);
	EM2EditTrackBar * emetb;
	emetb = GetEM2EditTrackBarInstance(hFazia);
	bool notify  = false;
	if (emetb->allowFloat)
	{
		float a = (float)_strtod_l(buff, &addr1, noxLocale);
		float b = (float)_strtod_l(buff, &addr2, noxLocaleComa);
		if (addr1 == buff   &&   addr2 == buff)
		{	// conversion error (to float)
			a = lastFloatValue;
			emetb->floatToEditbox(a);
		}
		else
		{
			if (addr2>addr1)
				a = b;
			if (a > emetb->maxFloatValue)
				a = emetb->maxFloatValue;
			if (a < emetb->minFloatValue)
				a = emetb->minFloatValue;
			if (lastFloatValue != a)
				if (fabs(lastFloatValue-a) > pow(0.1f, emetb->floatAfterDot))
					notify = true;
			emetb->floatToEditbox(a);
			lastFloatValue = a;
			emetb->floatValue = a;
		}
		emetb->sliderPos = (int)((a-emetb->minFloatValue)/(float)(emetb->maxFloatValue-emetb->minFloatValue)*emetb->pathLength);
		if (emetb->sliderPos > emetb->pathLength)
			emetb->sliderPos = emetb->pathLength;
		if (emetb->sliderPos < 0)
			emetb->sliderPos = 0;
	}
	else
	{
		int a = (int)_strtoi64(buff, &addr, 10);
		if (addr == buff)
		{	// conversion error (to int)
			a = lastIntValue;
			emetb->intToEditbox(a);
		}
		else
		{
			if (a > emetb->maxIntValue)
				a = emetb->maxIntValue;
			if (a < emetb->minIntValue)
				a = emetb->minIntValue;
			emetb->intToEditbox(a);
			if (lastIntValue != a)
				notify = true;
			lastIntValue = a;
			emetb->intValue = a;
		}
		emetb->sliderPos = (int)((a-emetb->minIntValue)/(float)(emetb->maxIntValue-emetb->minIntValue)*emetb->pathLength);
		if (emetb->sliderPos > emetb->pathLength)
			emetb->sliderPos = emetb->pathLength;
		if (emetb->sliderPos < 0)
			emetb->sliderPos = 0;
	}
	RECT rect;
	GetClientRect(hFazia, &rect);
	InvalidateRect(hFazia, &rect, FALSE);
	if (notify)
		emetb->notifyParent();
}

//------------------------------------------------------------------------------------------------

LRESULT CALLBACK EM2EditTrackBarEditProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static int lastIntValue = 0;
	static float lastFloatValue = 0;

	switch (msg)
	{
	case 65001:
		{
			lastIntValue = (int&)wParam;
			lastFloatValue = (float&)lParam;
		}
		break;
	case WM_CHAR:
	//case WM_KEYDOWN:
		{
			if (wParam == VK_RETURN)
			{
				verifyEditTrackbar2ControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_RETURN), (LPARAM)hwnd);
				SendMessage(hwnd, EM_SETSEL, 0, -1);
				return TRUE;
			}
			if (wParam == VK_ESCAPE)
			{
				HWND hFazia = GetParent(hwnd);
				EM2EditTrackBar * emetb = GetEM2EditTrackBarInstance(hFazia);
				if (emetb->allowFloat)
				{
					emetb->setFloatValue(lastFloatValue);
				}
				else
				{
					emetb->setIntValue(lastIntValue);
				}
				return TRUE;
			}
			if (wParam == VK_TAB)
			{
				verifyEditTrackbar2ControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				if (GetKeyState(VK_SHIFT)&0x8000)
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB_SHIFT), (LPARAM)hwnd);
				else
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB), (LPARAM)hwnd);

				return TRUE;
			}
		}
		break;
	case WM_KILLFOCUS:
		{
			verifyEditTrackbar2ControlData(hwnd, lastIntValue, lastFloatValue);
		}
		break;
	case WM_SETFOCUS:
		{
			char * addr;
			char buff[512];
			((WORD*)buff)[0] = 512;
			SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
			HWND hFazia = GetParent(hwnd);
			EM2EditTrackBar * emetb;
			emetb = GetEM2EditTrackBarInstance(hFazia);
			if (emetb->allowFloat)
			{
				float a = (float)strtod(buff, &addr);
				if (addr == buff)
				{	// conversion error (to float)
				}
				else
				{
					if (a > emetb->maxFloatValue)
						a = emetb->maxFloatValue;
					if (a < emetb->minFloatValue)
						a = emetb->minFloatValue;
					lastFloatValue = a;
				}
			}
			else
			{
				int a = (int)_strtoi64(buff, &addr, 10);
				if (addr == buff)
				{	// conversion error (to int)
				}
				else
				{
					if (a > emetb->maxIntValue)
						a = emetb->maxIntValue;
					if (a < emetb->minIntValue)
						a = emetb->minIntValue;
					lastIntValue = a;
				}
			}
			SendMessage(hwnd, EM_SETSEL, 0, -1);
		}
		break;
	case WM_NCCALCSIZE:
		{
			if (lParam)
			{
				NCCALCSIZE_PARAMS * params = (NCCALCSIZE_PARAMS *)lParam;
				RECT trect = params->rgrc[0];
				HDC hdc = GetDC(hwnd);
				
				EM2EditTrackBar * emet = GetEM2EditTrackBarInstance(GetParent(hwnd));
				HFONT hfont = emet ? emet->getFont() : (HFONT)GetStockObject(DEFAULT_GUI_FONT);
				HFONT hOldFont = (HFONT)SelectObject(hdc, hfont);
				
				DrawText(hdc, "Ky", 2, &trect, DT_CALCRECT | DT_LEFT);

				SelectObject(hdc, hOldFont);
				ReleaseDC(hwnd, hdc);

				int p = ((params->rgrc[0].bottom - params->rgrc[0].top) - (trect.bottom-trect.top));
				if (p>0)
					params->rgrc[0].top += p/2;
				if (p<0)
					params->rgrc[0].bottom += -p;
			}
			return 0;
		}
		break;
	default:
		return CallWindowProc(OldTrackBar2Proc, hwnd, msg, wParam, lParam);
	}

	return CallWindowProc(OldTrackBar2Proc, hwnd, msg, wParam, lParam);
}

//------------------------------------------------------------------------------------------------

LRESULT CALLBACK EM2EditTrackBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2EditTrackBar * emetb;
	emetb = GetEM2EditTrackBarInstance(hwnd);
	static int lastPosX;
	static int lastBarPos;

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2EditTrackBar * emetb1 = new EM2EditTrackBar(hwnd);
				SetEM2EditTrackBarInstance(hwnd, (EM2EditTrackBar *)emetb1);
				emetb1->hEdit = CreateWindow("EDIT", "", 
					WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VISIBLE | ES_CENTER | WS_CLIPCHILDREN
					| ES_MULTILINE | ES_WANTRETURN,
					1, 1, emetb1->editRectWidth-2, emetb1->height-2,
					hwnd, (HMENU)0, NULL, 0);
				SendMessage(emetb1->hEdit, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

				#ifdef _WIN64
					OldTrackBar2Proc = (EDITPROC) SetWindowLongPtr(emetb1->hEdit, GWLP_WNDPROC, (LONG_PTR)EM2EditTrackBarEditProc) ;
				#else
					OldTrackBar2Proc = (EDITPROC)(HANDLE)(LONG_PTR) SetWindowLong(emetb1->hEdit, GWL_WNDPROC, (LONG)(LONG_PTR)EM2EditTrackBarEditProc) ;
				#endif

				if (emetb1->allowFloat)
					emetb1->floatToEditbox(emetb1->minFloatValue);
				else
					emetb1->intToEditbox(emetb1->minIntValue);

			}
		break;
		case WM_SIZE:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				rect.left = 0;
				rect.top = 0;
				rect.right = (int)(short)LOWORD(lParam);
				rect.bottom = (int)(short)HIWORD(lParam);

				emetb->resize(&rect);
			}
			break;
		case WM_CLOSE:
			{
				return TRUE;
			}
			break;
		case WM_NCDESTROY:
			{
				EM2EditTrackBar * emetb1;
				emetb1 = GetEM2EditTrackBarInstance(hwnd);
				delete emetb1;
				SetEM2EditTrackBarInstance(hwnd, 0);
			}
			break;
		case WM_CTLCOLORSTATIC:
		case WM_CTLCOLOREDIT:
			{
				HDC hdc1 = (HDC)wParam;
				DWORD style;
				style = GetWindowLong(hwnd, GWL_STYLE);
				bool disabled = ((style & WS_DISABLED) > 0);
				if (!disabled)
				{
					SetTextColor(hdc1, emetb->colEditText);
					SetBkColor(hdc1, emetb->colEditBackground);
				}
				else
				{
					SetTextColor(hdc1, emetb->colDisabledEditText);
					SetBkColor(hdc1, emetb->colDisabledEditBackground);
				}
				if (disabled)
					return (INT_PTR)(emetb->hEditDisabledBrush);
				else
					return (INT_PTR)(emetb->hEditBrush);
			}
			break;
		case WM_ENABLE:
			{
				if (GetFocus()==emetb->hEdit)
					SetFocus(0);
				SendMessage(emetb->hEdit, EM_SETREADONLY, wParam?FALSE:TRUE, 0);

			}
			break;
		case WM_PAINT:
			{
				if (!emetb)
					break;

				RECT rect;
				HDC hdc1, thdc;
				PAINTSTRUCT ps;
				HPEN hOldPen;

				bool disabled = (((GetWindowLong(hwnd, GWL_STYLE)) & WS_DISABLED) > 0);

				// get DC and create backbuffer DC
				hdc1 = BeginPaint(hwnd, &ps);
				GetClientRect(hwnd, &rect);
				thdc = CreateCompatibleDC(hdc1); 
				HBITMAP hbmp = CreateCompatibleBitmap(hdc1, rect.right, rect.bottom);
				HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

				// draw background
				if (emetb->bgImage)
				{
					HDC thdc2 = CreateCompatibleDC(thdc);			// image buffer thdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc2, emetb->bgImage);
					BitBlt(thdc, 0, 0, rect.right, rect.bottom, thdc2, emetb->bgShiftX, emetb->bgShiftY, SRCCOPY);
					SelectObject(thdc2, oldBitmap);
					DeleteDC(thdc2);
					SetBkMode(thdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(thdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(thdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(thdc, hOldBrush));
					DeleteObject(SelectObject(thdc, hOldPen2));
					SetBkMode(thdc, OPAQUE);
				}

				// draw editbox background - its client rect might be shifted
				RECT erect = { 1,1, emetb->editRectWidth-1, emetb->height-1 };
				HBRUSH bgeBr = CreateSolidBrush(disabled ? emetb->colDisabledEditBackground : emetb->colEditBackground);
				FillRect(thdc, &erect, bgeBr);
				DeleteObject(bgeBr);

				// draw editbox border
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, disabled ? emetb->colDisabledEditBorderOuter : emetb->colEditBorderOuter));
				HBRUSH hOldBrush = (HBRUSH)SelectObject(thdc, GetStockObject(NULL_BRUSH));
				Rectangle(thdc, 0,0, emetb->editRectWidth, rect.bottom);
				DeleteObject(SelectObject(thdc, hOldBrush));
				// inner
				if (!disabled)
				{
					MoveToEx(thdc, 1, rect.bottom-2, NULL);
					DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colEditBorderLeft)));
					LineTo(thdc, 1, 1);
					DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colEditBorderTop)));
					LineTo(thdc, emetb->editRectWidth-2, 1);
					LineTo(thdc, emetb->editRectWidth-2, 2);
					DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colEditBorderRight)));
					LineTo(thdc, emetb->editRectWidth-2, rect.bottom-2);
					DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colEditBorderBottom)));
					LineTo(thdc, 1, rect.bottom-2);
					DeleteObject(SelectObject(thdc, hOldPen));
				}

				// path
				int vcenter = emetb->height/2;
				//#define GUI2_USE_GDIPLUS
				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(thdc);
						COLORREF bgl = disabled ? emetb->colDisabledPathBg : emetb->colPathBackgroundBefore;
						COLORREF bgr = disabled ? emetb->colDisabledPathBg : emetb->colPathBackgroundAfter;
						COLORREF bgb = disabled ? emetb->colDisabledPathBorder : emetb->colPathBorder;
						Gdiplus::SolidBrush brushL(Gdiplus::Color(disabled ? emetb->alphaDisabledPath : emetb->alphaPathBefore, GetRValue(bgl), GetGValue(bgl), GetBValue(bgl)));
						Gdiplus::SolidBrush brushR(Gdiplus::Color(disabled ? emetb->alphaDisabledPath : emetb->alphaPathAfter, GetRValue(bgr), GetGValue(bgr), GetBValue(bgr)));
						if (emetb->pathHeight > 0)
						{
							if (emetb->sliderPos > 1)
								graphics.FillRectangle(&brushL, 
											emetb->pathXStart + emetb->editRectWidth,
											vcenter-emetb->pathHeight/2,
											emetb->sliderPos,
											emetb->pathHeight);
							if (emetb->sliderPos < emetb->pathLength-1)
								graphics.FillRectangle(&brushR, 
											emetb->pathXStart + emetb->editRectWidth + emetb->sliderPos,
											vcenter-emetb->pathHeight/2,
											emetb->pathXEnd + 1 - emetb->pathXStart - emetb->sliderPos,
											emetb->pathHeight);
						}

						Gdiplus::Pen bpen(Gdiplus::Color(disabled ? emetb->alphaDisabledPathBorder : emetb->alphaPathBorder, GetRValue(bgb), GetGValue(bgb), GetBValue(bgb)));
						// top
						graphics.DrawLine(&bpen, Gdiplus::Point(emetb->pathXStart + emetb->editRectWidth, vcenter-emetb->pathHeight/2-1),
									Gdiplus::Point(emetb->pathXEnd+ emetb->editRectWidth, vcenter-emetb->pathHeight/2-1));
						// bottom
						graphics.DrawLine(&bpen, Gdiplus::Point(emetb->pathXEnd+ emetb->editRectWidth, vcenter-emetb->pathHeight/2+emetb->pathHeight),
									Gdiplus::Point(emetb->pathXStart + emetb->editRectWidth, vcenter-emetb->pathHeight/2+emetb->pathHeight));
						if (emetb->pathHeight>1)
						{
							// left
							graphics.DrawLine(&bpen, Gdiplus::Point(emetb->pathXStart + emetb->editRectWidth-1, vcenter-emetb->pathHeight/2+emetb->pathHeight-1),
										Gdiplus::Point(emetb->pathXStart + emetb->editRectWidth-1, vcenter-emetb->pathHeight/2));
							// right
							graphics.DrawLine(&bpen, Gdiplus::Point(emetb->pathXEnd+ emetb->editRectWidth+1, vcenter-emetb->pathHeight/2),
										Gdiplus::Point(emetb->pathXEnd+ emetb->editRectWidth+1, vcenter-emetb->pathHeight/2+emetb->pathHeight-1));
						}
						if (emetb->pathHeight==1)
						{
							Gdiplus::SolidBrush brushpen(Gdiplus::Color(disabled ? emetb->alphaDisabledPathBorder : emetb->alphaPathBorder, GetRValue(bgb), GetGValue(bgb), GetBValue(bgb)));
							graphics.FillRectangle(&brushpen,  emetb->pathXStart + emetb->editRectWidth-1, vcenter-emetb->pathHeight/2+emetb->pathHeight-1, 1,1);
							graphics.FillRectangle(&brushpen,  emetb->pathXEnd+ emetb->editRectWidth+1, vcenter-emetb->pathHeight/2, 1,1);
						}
					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					COLORREF colBefore = getMixedColor(NGCOL_TEXT_BACKGROUND, disabled ? emetb->alphaDisabledPath : emetb->alphaPathBefore, disabled ? emetb->colDisabledPathBg : emetb->colPathBackgroundBefore);
					COLORREF colAfter  = getMixedColor(NGCOL_TEXT_BACKGROUND, disabled ? emetb->alphaDisabledPath : emetb->alphaPathAfter , disabled ? emetb->colDisabledPathBg : emetb->colPathBackgroundAfter);
					COLORREF colBorder = getMixedColor(NGCOL_TEXT_BACKGROUND, disabled ? emetb->alphaDisabledPathBorder : emetb->alphaPathBorder, disabled ? emetb->colDisabledPathBorder : emetb->colPathBorder);

					if (emetb->pathHeight > 0)
					{
						if (emetb->sliderPos > 1)
						{
							RECT rectBefore;
							rectBefore.left = emetb->pathXStart + emetb->editRectWidth;
							rectBefore.right = emetb->pathXStart + emetb->editRectWidth + emetb->sliderPos;
							rectBefore.top = vcenter-emetb->pathHeight/2;
							rectBefore.bottom = vcenter-emetb->pathHeight/2 + emetb->pathHeight;
							HBRUSH hBrBefore = CreateSolidBrush(colBefore);
							FillRect(thdc, &rectBefore, hBrBefore);
							DeleteObject(hBrBefore);
						}
						if (emetb->sliderPos < emetb->pathLength-1)
						{
							RECT rectAfter;
							rectAfter.left = emetb->pathXStart + emetb->editRectWidth + emetb->sliderPos;
							rectAfter.right = emetb->pathXEnd + emetb->editRectWidth + 1;
							rectAfter.top = vcenter-emetb->pathHeight/2;
							rectAfter.bottom = vcenter-emetb->pathHeight/2 + emetb->pathHeight;
							HBRUSH hBrAfter = CreateSolidBrush(colAfter);
							FillRect(thdc, &rectAfter, hBrAfter);
							DeleteObject(hBrAfter);
						}
					}
					HPEN hOldPen2 = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colPathBorder));
					MoveToEx(thdc, emetb->pathXStart + emetb->editRectWidth, vcenter-emetb->pathHeight/2-1, NULL);
					LineTo(thdc, emetb->pathXEnd+ emetb->editRectWidth, vcenter-emetb->pathHeight/2-1);
					LineTo(thdc, emetb->pathXEnd+ emetb->editRectWidth+1, vcenter-emetb->pathHeight/2);
					LineTo(thdc, emetb->pathXEnd+ emetb->editRectWidth+1, vcenter-emetb->pathHeight/2+emetb->pathHeight/2);
					LineTo(thdc, emetb->pathXEnd+ emetb->editRectWidth, vcenter-emetb->pathHeight/2+emetb->pathHeight/2+1);
					LineTo(thdc, emetb->pathXStart + emetb->editRectWidth, vcenter-emetb->pathHeight/2+emetb->pathHeight/2+1);
					LineTo(thdc, emetb->pathXStart + emetb->editRectWidth-1, vcenter-emetb->pathHeight/2+emetb->pathHeight/2);
					LineTo(thdc, emetb->pathXStart + emetb->editRectWidth-1, vcenter-emetb->pathHeight/2);
					LineTo(thdc, emetb->pathXStart + emetb->editRectWidth, vcenter-emetb->pathHeight/2-1);
					DeleteObject(SelectObject(thdc, hOldPen2));
				#endif


				// draw slider
				int bpos_u = vcenter - BN_H/2;
				int bpos_l = emetb->pathXStart + emetb->editRectWidth + emetb->sliderPos - BN_W/2;
				HBITMAP hBArrrows = EM2Elements::getInstance()->getHBitmap();
				HDC thdc2 = CreateCompatibleDC(thdc);
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc2, hBArrrows);
				BLENDFUNCTION blendFunction;
				blendFunction.BlendOp = AC_SRC_OVER;
				blendFunction.BlendFlags = 0;
				blendFunction.SourceConstantAlpha = 255;
				blendFunction.AlphaFormat = AC_SRC_ALPHA;
				if (disabled)
					AlphaBlend(thdc, bpos_l, bpos_u,  BD_W,BD_H ,thdc2, BD_X,BD_Y,BD_W,BD_H, blendFunction);
				else
					if (emetb->nowScrolling)
						AlphaBlend(thdc, bpos_l, bpos_u,  BC_W,BC_H ,thdc2, BC_X,BC_Y,BC_W,BC_H, blendFunction);
					else
						if (emetb->nowMouseOver)
							AlphaBlend(thdc, bpos_l, bpos_u,  BM_W,BM_H ,thdc2, BM_X,BM_Y,BM_W,BM_H, blendFunction);
						else
							AlphaBlend(thdc, bpos_l, bpos_u,  BN_W,BN_H ,thdc2, BN_X,BN_Y,BN_W,BN_H, blendFunction);
				SelectObject(thdc2, oldBitmap);
				DeleteDC(thdc2);

				// copy from back buffer and free resources
				BitBlt(hdc1, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);
				SelectObject(thdc, oldBMP);
				DeleteObject(hbmp);
				DeleteDC(thdc);
				EndPaint(hwnd, &ps);

				InvalidateRect(emetb->hEdit, NULL, false);
				ShowWindow(emetb->hEdit, SW_SHOW);
				UpdateWindow(emetb->hEdit);
			}
			break;
		case WM_MOUSEMOVE:
			{
				if (!GetCapture())
					SetCapture(hwnd);
				if (GetCapture() == hwnd)
				{
					RECT rect;
					GetClientRect(hwnd, &rect);
					rect.left = emetb->editRectWidth;
					POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };

					if (emetb->useCursors)
						if (PtInRect(&emetb->rectSlider, pt)  ||  PtInRect(&emetb->rectBefore, pt)  ||  PtInRect(&emetb->rectAfter, pt)  ||  emetb->nowScrolling)
							SetCursor(LoadCursor (NULL, IDC_HAND));
						else
							SetCursor(LoadCursor (NULL, IDC_ARROW));
					if (PtInRect(&rect, pt)  || emetb->nowScrolling)
					{
						if (emetb->nowScrolling)
						{
							emetb->sliderPos = lastBarPos + (short)LOWORD(lParam) - lastPosX;
							if (emetb->sliderPos > emetb->pathLength)
								emetb->sliderPos = emetb->pathLength;
							if (emetb->sliderPos < 0)
								emetb->sliderPos = 0;
							float newVal;
							if (emetb->allowFloat)
							{
								newVal = emetb->sliderPos / (float) emetb->pathLength * (emetb->maxFloatValue-emetb->minFloatValue) + emetb->minFloatValue;
								emetb->floatToEditbox(newVal);
								emetb->floatValue = newVal;
							}
							else
							{
								newVal = emetb->sliderPos / (float) emetb->pathLength * (emetb->maxIntValue-emetb->minIntValue) + emetb->minIntValue;
								int iVal = (int)floorf(newVal+0.4f);
								emetb->sliderPos = (int)((iVal-emetb->minIntValue)/(float)(emetb->maxIntValue-emetb->minIntValue)*emetb->pathLength);
								emetb->intToEditbox(iVal);
								emetb->intValue = iVal;
							}
							SendMessage(emetb->hEdit, 65001, (WPARAM&)emetb->intValue, (LPARAM&)emetb->floatValue);
							emetb->updateMouseRectangles();
							GetClientRect(hwnd, &rect);
							InvalidateRect(hwnd, &rect, FALSE);
							emetb->notifyParent();
						}
						else
						{
							if (PtInRect(&emetb->rectSlider, pt))
							{
								if(!emetb->nowMouseOver)
									InvalidateRect(hwnd, NULL, false);
								emetb->nowMouseOver = true;
							}
							else
							{
								if(emetb->nowMouseOver)
									InvalidateRect(hwnd, NULL, false);
								emetb->nowMouseOver = false;
							}
						}
					}
					else
					{
						emetb->nowMouseOver = false;
						InvalidateRect(hwnd, NULL, false);
						ReleaseCapture();
					}
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emetb->nowMouseOver = false;
				emetb->nowScrolling = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{

				RECT rect;
				GetClientRect(hwnd, &rect);
				rect.left = emetb->editRectWidth;
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				if (PtInRect(&rect, pt))
				{
					SetFocus(hwnd);
					bool changed = false;

					if (PtInRect(&emetb->rectSlider, pt))
					{
						emetb->nowScrolling = true;
					}
					else
					{
						if (PtInRect(&emetb->rectBefore, pt))
						{
							changed = true;
							if (emetb->allowFloat)
								emetb->floatValue -= emetb->floatFastValue;
							else
								emetb->intValue -= emetb->intFastValue;
						}
						else
							if (PtInRect(&emetb->rectAfter, pt))
							{
								changed = true;
								if (emetb->allowFloat)
									emetb->floatValue += emetb->floatFastValue;
								else
									emetb->intValue += emetb->intFastValue;
							}
					}

					if (changed)
					{
						if (emetb->allowFloat)
						{
							if (emetb->floatValue > emetb->maxFloatValue)
								emetb->floatValue = emetb->maxFloatValue;
							if (emetb->floatValue < emetb->minFloatValue)
								emetb->floatValue = emetb->minFloatValue;
							emetb->floatToEditbox(emetb->floatValue);
							emetb->sliderPos = (int)((emetb->floatValue-emetb->minFloatValue)/(float)(emetb->maxFloatValue-emetb->minFloatValue)*emetb->pathLength);
						}
						else
						{
							if (emetb->intValue > emetb->maxIntValue)
								emetb->intValue = emetb->maxIntValue;
							if (emetb->intValue < emetb->minIntValue)
								emetb->intValue = emetb->minIntValue;
							emetb->intToEditbox(emetb->intValue);
							emetb->sliderPos = (int)((emetb->intValue-emetb->minIntValue)/(float)(emetb->maxIntValue-emetb->minIntValue)*emetb->pathLength);
						}

						if (emetb->sliderPos > emetb->pathLength)
							emetb->sliderPos = emetb->pathLength;
						if (emetb->sliderPos < 0)
							emetb->sliderPos = 0;

						SendMessage(emetb->hEdit, 65001, (WPARAM&)emetb->intValue, (LPARAM&)emetb->floatValue);
						emetb->updateMouseRectangles();
						InvalidateRect(hwnd, NULL, FALSE);
						emetb->notifyParent();
					}

					if (emetb->nowScrolling)
					{
						lastBarPos = emetb->sliderPos;
						lastPosX = pt.x;
						InvalidateRect(hwnd, NULL, FALSE);
						emetb->notifyParent();
					}
				}

			}
			break;
		case WM_LBUTTONUP:
			{
				emetb->nowScrolling = false;
				emetb->nowMouseOver = false;
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				if (GetCapture() == hwnd)
					ReleaseCapture();
				InvalidateRect(hwnd, NULL, FALSE);
			}
			break;
		case WM_RBUTTONUP:
			{
				bool notify = false;
				if (emetb->allowFloat)
				{
					notify = (emetb->floatDefaultValue != emetb->floatValue);
					emetb->setFloatValue(emetb->floatDefaultValue);
				}
				else
				{
					notify = (emetb->intDefaultValue != emetb->intValue);
					emetb->setIntValue(emetb->intDefaultValue);
				}
				if (notify)
					emetb->notifyParent();
			}
			break;
		case WM_SETFOCUS:
			{
				SetFocus(emetb->hEdit);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
		
				LONG editID = GetWindowLong(emetb->hEdit, GWL_ID);
				if (wmId == editID  &&   wmEvent == ES_RETURN)
				{
				}
				if (wmId == editID  &&   wmEvent == ES_TAB)
				{
					HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, false);
					if (hNext != hwnd)
						SetFocus(hNext);
				}
				if (wmId == editID  &&   wmEvent == ES_TAB_SHIFT)
				{
					HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, true);
					if (hNext != hwnd)
						SetFocus(hNext);
				}
			}
			break;	// end WM_COMMAND
		default:
			break;
		}

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::floatToEditbox(float value)
{
	char eValue[64];
	char temp[64];
	sprintf_s(temp, 64, "%%.%df", floatAfterDot);
	sprintf_s(eValue, 64, temp, value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::intToEditbox(int value)
{
	char eValue[64];
	sprintf_s(eValue, 64, "%d", value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::resize(RECT * nrect)
{
	RECT rect;
	if (nrect)
		rect = *nrect;
	else
		GetClientRect(hwnd, &rect);
	pathXStart = BN_W/2 + spaceAfterEdit;
	pathLength = rect.right - pathXStart - editRectWidth - (BN_W+1)/2;
	pathXEnd = pathLength + pathXStart;

	height = rect.bottom;

	SetWindowPos(hEdit, HWND_TOP, 2, 2, editRectWidth-4, height-4, SWP_NOZORDER | SWP_FRAMECHANGED);

	if (allowFloat)
		sliderPos = (int)((floatValue - minFloatValue)/(float)(maxFloatValue - minFloatValue)*pathLength);
	else
		sliderPos = (int)((intValue - minIntValue)/(float)(maxIntValue - minIntValue)*pathLength);

	if (sliderPos > pathLength)
		sliderPos = pathLength;
	if (sliderPos < 0)
		sliderPos = 0;

	updateMouseRectangles();
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::setEditBoxWidth(int w, int spaceAfter)
{
	editRectWidth = w;
	spaceAfterEdit = spaceAfter;
	SetWindowPos(hEdit, HWND_TOP, 1, 1, editRectWidth-2, height-2, 0);
	resize(NULL);
	updateMouseRectangles();
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::updateMouseRectangles()
{
	RECT crect;
	GetClientRect(hwnd, &crect);
	int left = editRectWidth + spaceAfterEdit + BN_W/2;
	int vcenter = crect.bottom/2;
	int up = vcenter - BN_H/2;
	int down = up + BN_H;

	rectBefore.left = left;
	rectBefore.top = up;
	rectBefore.bottom = down;
	rectBefore.right = max(left+sliderPos-BN_W/2, left);

	rectAfter.right = crect.right - (BN_W+1)/2;
	rectAfter.top = up;
	rectAfter.bottom = down;
	rectAfter.left = min(left+sliderPos+(BN_W+1)/2, rectAfter.right);

	rectSlider.top = up;
	rectSlider.bottom = down;
	rectSlider.left = left+sliderPos-BN_W/2;
	rectSlider.right = left+sliderPos+(BN_W+1)/2;
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::setValuesToInt(int value, int minVal, int maxVal, int defaultValue, int fastValue)
{
	intFastValue = fastValue;
	minIntValue = minVal;
	maxIntValue = maxVal;
	intDefaultValue = min(maxIntValue, max(minIntValue, defaultValue));

	allowFloat = false;

	setIntValue(value);
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::setValuesToFloat(float value, float minVal, float maxVal, float defaultValue, float fastValue)
{
	minFloatValue = minVal;
	maxFloatValue = maxVal;
	floatFastValue = fastValue;
	floatDefaultValue = min(maxFloatValue, max(defaultValue, value));

	allowFloat = true;

	setFloatValue(value);
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::setIntValue(int value)
{
	if (allowFloat)
		return;
	intValue = min(maxIntValue, max(minIntValue, value));

	intToEditbox(intValue);

	sliderPos = (int)((value - minIntValue)/(float)(maxIntValue - minIntValue)*pathLength);
	if (sliderPos > pathLength)
		sliderPos = pathLength;
	if (sliderPos < 0)
		sliderPos = 0;

	updateMouseRectangles();

	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

//----------------------------------------------------------------------

void EM2EditTrackBar::setFloatValue(float value)
{
	if (!allowFloat)
		return;
	floatValue = min(maxFloatValue, max(minFloatValue, value));
	floatToEditbox(floatValue);

	sliderPos = (int)((value - minFloatValue)/(float)(maxFloatValue - minFloatValue)*pathLength);
	if (sliderPos > pathLength)
		sliderPos = pathLength;
	if (sliderPos < 0)
		sliderPos = 0;

	updateMouseRectangles();

	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

//----------------------------------------------------------------------

void EM2EditTrackBar::notifyParent()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_HSCROLL), (LPARAM)hwnd);
}

//------------------------------------------------------------------------------------------------

bool EM2EditTrackBar::setFont(HFONT hNewFont, bool autodel)
{
	if (autodelfont)
		DeleteObject(hFont);
	hFont = hNewFont;
	autodelfont = autodel;
	SendMessage(hEdit, WM_SETFONT, (WPARAM)(hFont?hFont:GetStockObject(DEFAULT_GUI_FONT)), TRUE);
	SetWindowPos(hEdit, HWND_TOP, 0,0,0,0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED);
	return true;
}

//------------------------------------------------------------------------------------------------

HFONT EM2EditTrackBar::getFont()
{
	return (hFont ? hFont : (HFONT)GetStockObject(DEFAULT_GUI_FONT));
	return hFont;
}

//------------------------------------------------------------------------------------------------

void EM2EditTrackBar::setAlign(int newalign)
{
	DWORD style = GetWindowLong(hEdit, GWL_STYLE);
	style &= ~(ES_CENTER|ES_LEFT|ES_RIGHT);
	switch (newalign)
	{
		case ALIGN_LEFT:	style |= ES_LEFT;	break;
		case ALIGN_CENTER:	style |= ES_CENTER;	break;
		case ALIGN_RIGHT:	style |= ES_RIGHT;	break;
		default :			style |= ES_LEFT;	break;
	}
	SetWindowLong(hEdit, GWL_STYLE, style);
}

//----------------------------------------------------------------------
