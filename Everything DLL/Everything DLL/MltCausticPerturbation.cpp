#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

//#define DEBUG_LOG2
//#define FAIL_LOG_CAUSTIC

//--------------------------------------------------------------------------------------------------------------------------------------
bool Metropolis::perturbateCaustic()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	// analyze current path
	bool gotSpec = false;
	bool gotLight = false;
	bool gotNonSpec = false;
	int pertStart = 1;
	int pertStop = 1;
	csInd = 0;

	if (numEP<1)
		return false;
	CHECK(ePath[1].mlay);
	float r1u=0, r1v=0;
	ePath[1].tri->evalTexUV(ePath[1].u, ePath[1].v, r1u, r1v);
	float r1 = ePath[1].mlay->getRough(r1u, r1v);
	if (r1==0)	// pure specular seen directly from camera
		return false;

	bool causticFromSun = false;
	// eye path
	for (int i=2; i<=numEP; i++)
	{
		MatLayer * mlay = ePath[i].mlay;
		CHECK(mlay);

		pertStart++;

		float tu=0,tv=0;
		if (ePath[i].tri)
			ePath[i].tri->evalTexUV(ePath[i].u, ePath[i].v, tu,tv);
		float rough = ePath[i].mlay->getRough(tu, tv);
		bool glossspec = (rough<=maxCausticRough);

		if (!gotSpec  &&  !glossspec)
		{	// *DDE  <-  not a caustic from eye
			pertStop = i;
		}

		if (gotSpec && !glossspec)
		{	// *DS*E  <--  that's it - perturbation is possible
			gotNonSpec = true;
			break;
		}

		if (glossspec)
			gotSpec = true;
	}

	// light path
	if (!gotSpec   ||   !gotNonSpec)
	{
		for (int i=numLP; i>=0; i--)
		{
			MatLayer * mlay = lPath[i].mlay;
			if (!mlay)
			{
				if (i==0  &&  (sourceSky  ||  sourceSun))
				{
					gotNonSpec = true;
					causticFromSun = true;
					pertStart++;
					break;
				}
				else
					return false;
			}

			float tu=0,tv=0;
			if (lPath[i].tri)
				lPath[i].tri->evalTexUV(lPath[i].u, lPath[i].v, tu,tv);
			float rough = lPath[i].mlay->getRough(tu, tv);
			bool glossspec =	rough<=maxCausticRough  &&  mlay->type==MatLayer::TYPE_SHADE;
			pertStart++;

			if (!gotSpec  && !glossspec)
			{	// *DDE  <-  not a caustic
				pertStop = numLP-i+1+numEP;
			}

			if (gotSpec && !glossspec)
			{	// that's it - perturbation is possible
				gotNonSpec = true;
				break;
			}

			if (glossspec)
				gotSpec = true;
		}
	}

	if (!gotSpec  ||  !gotNonSpec)
		return false;

	#ifdef FAIL_LOG_CAUSTIC
		if (causticFromSun)
			Logger::add("caustic is possible try2");
	#endif

	// perturbate first direction
	MLTVertex * pVert = NULL;
	if (pertStart > numEP)
		pVert = &(lCandidate[numLP-(pertStart-numEP-1)]);
	else
		pVert = &(eCandidate[pertStart]);

	Vector3d olddir;
	if (pertStart > numEP)
		olddir = pVert->outDir;
	else
		olddir = pVert->inDir;


	bool lightStart = false;
	if (numLP-(pertStart-numEP-1) == 0) 
		lightStart = true;

	//-------------------------------
	if (lightStart)
	{
		if (pVert->mlay)
		{
			#define LTRIES 20
			int ilm = ss->findLightMesh(lTriID);
			if (ilm>=0)
			{
				LightMesh * lm = (*ss->lightMeshes)[ilm];
				float sdist = BIGFLOAT;
				float su,sv;
				int sitri;
				Triangle * sltri;
				Vector3d snormal;
				Point3d spoint;
				bool gotOne = false;
				Matrix4d sInstMatr;
				int sInstID;

				int ltries = LTRIES + sysRej;

				for (int i=0; i<ltries; i++)
				{
					float ppp;
					int citri = lm->randomTriangle(ppp);
					if (citri<0  ||  citri >= scene->triangles.objCount)
						continue;
					Triangle * nltri = &(scene->triangles[citri]);
					MeshInstance * mInst = &(scene->instances[lm->instNum]);
					MaterialNox * cMat = scene->mats[nltri->matInInst];

					if (cMat != lPath[0].mat)
						continue;

					Point3d npoint;
					float nu,nv;
					nltri->randomPoint(nu,nv, npoint);
					Matrix4d instMatr = mInst->matrix_transform;
					npoint = instMatr * npoint;
					Vector3d nn = (instMatr.getMatrixForNormals() * nltri->evalNormal(nu,nv)).getNormalized();
					if (nn * lPath[0].outDir < 0)
						continue;

					float ndist = (lPath[0].pos - npoint).normalize_return_old_length();
					if (ndist < sdist)
					{
						spoint = npoint;
						sdist = ndist;
						snormal = nn;
						su = nu;
						sv = nv;
						sitri = citri;
						sltri = nltri;
						sInstMatr = instMatr;
						sInstID = lm->instNum;
						gotOne = true;
					}
				}

				if (gotOne)
				{
					lCandidate[0].pos = spoint;
					lCandidate[0].normal = snormal;
					lCandidate[0].tri = sltri;
					lCandidate[0].u = su;
					lCandidate[0].v = sv;
					lCandidate[0].instanceID = sInstID;
					lCandidate[0].inst_mat = sInstMatr;
					clTriID = sitri;
				}
			}
		}
		else
		{	// perturbate from SUN
			if (sourceSun)
			{
				Vector3d tvec = Vector3d::random();
				Vector3d pvec = tvec ^ pVert->outDir;
				pvec.normalize();
				float pdist = 0.1f;
				if (sysRej > 0)
					pdist /= sysRej;
				Vector3d oldStartDir = lPath[0].outDir;
				oldStartDir = lPath[0].outDir * -1;
				oldStartDir = ss->sunsky->getRandomPerturbedSunDirection() * -1;
				olddir = oldStartDir;
				Point3d oldpos = ePath[numEP].pos +  oldStartDir * -1000;
				Point3d newpos = oldpos + pvec*pdist;
				lCandidate[0].pos = newpos;
				lCandidate[0].outDir = lCandidate[0].inDir = lCandidate[0].normal = ss->sunsky->getRandomPerturbedSunDirection() * -1;	//oldStartDir;
				cSourceSun = true;
				cSourceSky = false;
			}
			else
			{
				Vector3d tvec = Vector3d::random();
				Vector3d pvec = tvec ^ pVert->outDir;
				pvec.normalize();
				float pdist = 0.1f;
				if (sysRej > 0)
					pdist /= sysRej;
				Vector3d oldStartDir = lPath[0].outDir;
				olddir = oldStartDir;
				Point3d oldpos = ePath[numEP].pos +  oldStartDir * -1000;
				Point3d newpos = oldpos + pvec*pdist;
				lCandidate[0].pos = newpos;
				lCandidate[0].outDir = lCandidate[0].inDir = lCandidate[0].normal = oldStartDir;
				cSourceSun = false;
				cSourceSky = true;
			}

		}
	}
	//-------------------------------

	// rand phi and theta
	float phi = Raytracer::getRandomGeneratorForThread()->getRandomFloat() * 2 * PI;			// 0 - 2PI
	float rnd = Raytracer::getRandomGeneratorForThread()->getRandomFloat();

			float temppcR1 = pcR1;
			if (sysRej > 0)
				temppcR1 = pcR1 / (float)sysRej;
			temppcR1 = max(0.001f, temppcR1);

			float temppcR2 = pcR2;
			if (sysRej > 0)
				temppcR2 = pcR2 / (float)sysRej;
			temppcR2 = max(0.002f, temppcR2);

	float theta = temppcR2 * exp(-log(temppcR2/temppcR1)*rnd);



	// eval dir
	Vector3d aa = Vector3d::random();
	aa = aa ^ olddir;
	aa.normalize();
	Vector3d newdir = olddir*cos(theta) + aa*sin(theta);
	newdir.normalize();

	if (causticFromSun)
		newdir = olddir;

	// fill first vertex.....
	if (pertStart > numEP)
	{
		pVert->outCosine = pVert->normal * newdir;
		pVert->outDir = newdir;
	}
	else
	{
		pVert->inCosine = pVert->normal * newdir;
		pVert->inDir = newdir;
	}


	Vector3d cDir = newdir;
	Point3d cPoint;
	if (pVert->normal * newdir > 0)
		cPoint = pVert->pos + pVert->normal * 0.0001f;		// 0.1mm
	else
		cPoint = pVert->pos + pVert->normal * -0.0001f;
	MLTVertex * curPath = NULL;
	MLTVertex * origPath = NULL;

	csInd = pertStart;

	// MAIN LOOP
	for (int i=pertStart-1; i>=pertStop; i--)
	{
		bool lSide;
		bool nowConnection = (i==numEP);
		if (i > numEP)
		{
			curPath  = &(lCandidate[numLP-(i-numEP-1)]);
			origPath = &(lPath[numLP-(i-numEP-1)]);
			lSide = true;
		}
		else
		{
			curPath  = &(eCandidate[i]);
			origPath = &(ePath[i]);
			lSide = false;
		}

		// send a ray
		float u=0, v=0;
		int chosen = -1, inst_id = -1;
		Matrix4d inst_matr;
		float bigf = BIGFLOAT;

		Color4 att;
		float d = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time, cPoint, cDir, bigf, inst_matr, inst_id, chosen, u, v, att, ss, true);			// find an intersection
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.001f)
			return false;	// nothing intersected

		if (nowConnection)
			connectionAttenuationCandidate = att;
		else
		{
			if (lSide)
				curPath->atten = att;
			else
				eCandidate[i+1].atten = att;
		}
		
		Triangle * tri  = &(scene->triangles[chosen]);
		MaterialNox * mat  = scene->mats[scene->instances[inst_id].materials[tri->matInInst]];
		MatLayer * mlay = NULL;

		if (mat == origPath->mat)
			mlay = origPath->mlay;
		else
			return false;

		// fill current vertex
		curPath->normal = (inst_matr.getMatrixForNormals() * tri->evalNormal(u,v)).getNormalized();
		curPath->u = u;
		curPath->v = v;
		curPath->mat = mat;
		curPath->mlay = mlay;
		curPath->pos = inst_matr * tri->getPointFromUV(u,v);
		curPath->tri = tri;
		curPath->instanceID = inst_id;
		curPath->inst_mat = inst_matr;
		curPath->rough = 1.0f;
		curPath->fetchRoughness();

		if (i<=numEP)
		{
			curPath->outDir = cDir * -1;
			curPath->outCosine = cDir*curPath->normal*-1;
		}
		else
		{
			curPath->inDir = cDir * -1;
			curPath->inCosine = cDir*curPath->normal*-1;
		}

		float pdf;
		HitData hd;
		hd.in = cDir * -1;
		hd.normal_shade = curPath->normal;
		tri->evalTexUV(u,v, hd.tU,hd.tV);
		hd.normal_geom = (inst_matr.getMatrixForNormals() * tri->normal_geom).getNormalized();
		hd.adjustNormal();
		tri->getTangentSpaceSmooth(inst_matr, u,v, hd.dir_U, hd.dir_V);

		Vector3d oldDir = ( (i<=numEP) ? origPath->inDir : origPath->outDir );
		float oldCos = origPath->normal * oldDir;
		bool gotValidDir = false;
		int nTry = 0;
		while (!gotValidDir)
		{
			hd.clearFlagInvalidRandom();
			cDir = mlay->randomNewDirection(hd, pdf);
			float newCos = cDir * curPath->normal;
			if (newCos*oldCos > 0   &&   !hd.isFlagInvalidRandom())
				gotValidDir = true;
			if (nTry++ > 150)
				return false;
		}

		hd.out = cDir;
		float ccos = curPath->normal * cDir;

		if (i==pertStop)
		{	// last reflection -> to camera/next vert
			int jEye = pertStop-1;
			MLTVertex * v2 = jEye<=numEP ? &(eCandidate[jEye]):&(lCandidate[numLP-(jEye-numEP-1)]);
			cDir = v2->pos - curPath->pos;
			cDir.normalize();
			hd.out = cDir;
			ccos = curPath->normal * cDir;
		}

		if (i<=numEP)
		{
			curPath->inDir = cDir;
			curPath->inCosine = curPath->normal * cDir;
		}
		else
		{
			curPath->outDir = cDir;
			curPath->outCosine = curPath->normal * cDir;
		}

		if (fabs(curPath->inCosine) < 0.0001f   ||   fabs(curPath->outCosine) < 0.0001f)
		{
			#ifdef FAIL_LOG_CAUSTIC
				Logger::add("bad angle");
			#endif
			return false;
		}


		if (ccos < 0)
			cPoint = curPath->pos + curPath->normal * -0.0001f;
		else
			cPoint = curPath->pos + curPath->normal * 0.0001f;
		
		curPath->brdf = mlay->getBRDF(hd);
		if (curPath->brdf.isInf()   ||   curPath->brdf.isNaN())
			return false;
		if (curPath->brdf.isBlack())
			return false;
	}

	// join to eye
	int jEye = pertStop-1;
	MLTVertex * v1 = curPath;
	MLTVertex * v2 = jEye<=numEP ? &(eCandidate[jEye]):&(lCandidate[numLP-(jEye-numEP-1)]);
	Vector3d dir12 = v2->pos - v1->pos;
	dir12.normalize();
	Vector3d dir21 = dir12 * -1.0f;
	v2->outDir = dir21;
	if (jEye==0)
		v2->inDir = v2->normal = dir21;
	if (pertStop<=numEP)
	{
		v1->inDir = dir12;
		v1->inCosine = v1->inDir * v1->normal;
	}
	else
	{
		v1->outDir = dir12;
		v1->outCosine = v1->outDir * v1->normal;
	}

	bool inFrust = true;
	if (jEye==0)
		inFrust = cam->getCoordsFromDirReturnInFrustum(dir21, eCandidate[0].pos, pCX, pCY);
	#ifdef FAIL_LOG_CAUSTIC
		Logger::add("0");
	#endif
	if (!inFrust)
		return false;
	else
	{
		pCX *= cam->aa;
		pCY *= cam->aa;
	}

	// check visibility
	Color4 visCol = visibilityTestFakeGlassAndOpacity(v1,v2);
	bool visOK = !visCol.isBlack();
	
	causticPertStart = pertStart;
	causticPertStop  = pertStop;

	return visOK;
}

//--------------------------------------------------------------------------------------------------------------------------------------

float Metropolis::tentativeTransitionForPertCaustic(bool forward)
{
	MLTVertex * v1 = NULL;
	MLTVertex * v2 = NULL;
	MLTVertex * lCur = NULL;
	MLTVertex * eCur = NULL;

	if (forward)
	{
		if (causticPertStop > numEP)
			v1 = &(lCandidate[numLP-(causticPertStop-numEP-1)]);	// shouldn't happen
		else
			v1 = &(eCandidate[causticPertStop]);
		if (causticPertStop-1 > numEP)
			v2 = &(lCandidate[numLP-(causticPertStop-1-numEP-1)]);	// shouldn't happen
		else
			v2 = &(eCandidate[causticPertStop-1]);

		lCur = lCandidate;
		eCur = eCandidate;
	}
	else
	{
		if (causticPertStop > numEP)
			v1 = &(lPath[numLP-(causticPertStop-numEP-1)]);		// shouldn't happen
		else
			v1 = &(ePath[causticPertStop]);
		if (causticPertStop-1 > numEP)
			v2 = &(lPath[numLP-(causticPertStop-1-numEP-1)]);	// shouldn't happen
		else
			v2 = &(ePath[causticPertStop-1]);
		lCur = lPath;
		eCur = ePath;
	}

	Vector3d dirEL = v1->pos - v2->pos;
	float d2 = dirEL.normalize_return_old_length();
	d2 = d2*d2;
	if (d2<0.001f)
		return 0;
	Vector3d dirLE = dirEL*-1;
	float cosEL = fabs(dirEL*v2->normal);
	if (causticPertStop==1)
		cosEL = 1.0f;
	float cosLE = fabs(dirLE*v1->normal);
	if (cosEL < 0.001f    ||   cosLE < 0.001f)
		return 0;

	float G = cosLE * cosEL / d2;

	v2->outDir = dirEL;
	if (causticPertStop > numEP)
	{
		v1->outDir = dirLE;
		v1->outCosine = dirLE * v1->normal;
	}
	else
	{
		v1->inDir = dirLE;
		v1->inCosine = dirLE * v1->normal;
	}

	float den = 1.0f;
	for (int i=causticPertStart; i>causticPertStop; i--)
	{
		HitData hd;
		MLTVertex * v1 = NULL;

		if (i > numEP)
		{
			v1 = &(lCur[numLP-(i-numEP-1)]);
			hd.in = v1->inDir;
			hd.out = v1->outDir;
		}
		else
		{
			v1 = &(eCur[i]);
			hd.in = v1->outDir;
			hd.out = v1->inDir;
		}

		hd.normal_shade = v1->normal;
		Triangle * tri  = v1->tri;
		MatLayer * mlay = v1->mlay;
		if (!tri || !mlay)
		{
			if (i==causticPertStart)
			{
			}
			else
				return 0;
		}
		else
		{
			tri->evalTexUV(v1->u, v1->v,  hd.tU, hd.tV);
			hd.normal_geom = (v1->inst_mat.getMatrixForNormals() * tri->normal_geom).getNormalized();
			hd.adjustNormal();
			tri->getTangentSpaceSmooth(v1->inst_mat, v1->u, v1->v, hd.dir_U, hd.dir_V);
			float pdf = mlay->getProbability(hd);
			if (pdf < 0.001f)
				return 0;

			float tu=0,tv=0;
			if (v1->tri)
				v1->tri->evalTexUV(v1->u, v1->v, tu,tv);
			float rough = v1->mlay->getRough(tu, tv);
			if (rough>0)
			{
				float tcos = fabs(hd.out*hd.normal_shade);
				if (tcos<0.001f)
					return 0.0f;
				den *= pdf / tcos;
			}
			else
				den *= pdf;
		}
	}
	
	if (den <= 0)
		return 0;

	// nominator
	Color4 brdf = Color4(1,1,1);
	for (int i=causticPertStart; i>=causticPertStop; i--)
	{
		bool lSide;
		HitData hd;
		MLTVertex * v1 = NULL;

		if (i > numEP)
		{
			v1 = &(lCur[numLP-(i-numEP-1)]);
			hd.in = v1->inDir;
			hd.out = v1->outDir;
			lSide = true;
		}
		else
		{
			v1 = &(eCur[i]);
			hd.in = v1->outDir;
			hd.out = v1->inDir;
			lSide = false;
		}

		Color4 tbrdf = Color4(1,1,1);

		hd.normal_shade = v1->normal;
		Triangle * tri  = v1->tri;
		MatLayer * mlay = v1->mlay;
		if (!tri || !mlay)
		{
			if (i==causticPertStart)
			{
			}
			else
				return 0;
		}
		else
		{
			hd.normal_geom = (v1->inst_mat.getMatrixForNormals() * tri->normal_geom).getNormalized();
			hd.adjustNormal();
			tri->evalTexUV(v1->u, v1->v,  hd.tU, hd.tV);
			tri->getTangentSpaceSmooth(v1->inst_mat, v1->u, v1->v, hd.dir_U, hd.dir_V);

			if (mlay->type == MatLayer::TYPE_EMITTER)
				tbrdf = mlay->getLightColor(hd.tU, hd.tV) * mlay->getIlluminance(ss, forward ? clTriID : lTriID);
			else
				tbrdf = mlay->getBRDF(hd);
		}

		if (tbrdf.isBlack())
			return 0;

		if (tbrdf.isInf()  ||  tbrdf.isNaN())
			return 0;

		if (i==numCEP)
			brdf *= (forward ? connectionAttenuationCandidate : connectionAttenuation);
		brdf *= v1->atten;
		brdf *= tbrdf;

	}

	if (causticPertStop > 1)
	{
	}

	float nom = (0.299f * brdf.r  +  0.587f * brdf.g  +  0.114f * brdf.b);
	if (nom == 0)
		return 0;

	return G*nom/den;
}

//--------------------------------------------------------------------------------------------------------------------------------------

