//#define _WIN32_WINNT 0x501
#include "max2013predef.h"
#include "NOXMaterial.h"
#include "3dsmaxport.h"

void fillLayersList(HWND hLay, NOXMaterial * nmat);
INT_PTR CALLBACK NOXMatParamDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);

NOXMatParamDialog::NOXMatParamDialog(NOXMaterial * cMat, IMtlParams *imp, HINSTANCE hInstance)
{
	eMat = cMat;
	hInst = hInstance;
	reenableAccels = true;

	hMatPanel = imp->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_NOX_MAT_DIALOG),
			NOXMatParamDlgProc,
			_T("NOX Material"),
			(LPARAM)this);
}

NOXMatParamDialog::~NOXMatParamDialog()
{
}

void NOXMatParamDialog::backFromMatEditor()
{
	eMat->getTexturesFromNOX();
	eMat->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
	GetCOREInterface()->RedrawViews(GetCOREInterface()->GetTime());
	if (reenableAccels)
	{
		EnableAccelerators();
	}


	// tutaj update paramblocka z nowych danych
	//eMat->pb_test->SetValue(4, GetCOREInterface()->GetTime(), _T("dupa"));

}

Class_ID NOXMatParamDialog::ClassID()
{
	return Class_ID(0x576d0830, 0x52062e91);
}

void NOXMatParamDialog::SetThing(ReferenceTarget *m)
{
	eMat = (NOXMaterial *)m;
}

ReferenceTarget * NOXMatParamDialog::GetThing()
{
	return (ReferenceTarget *)eMat;
}

void NOXMatParamDialog::SetTime(TimeValue t)
{
}

void NOXMatParamDialog::ReloadDialog()
{
}

void NOXMatParamDialog::DeleteThis()
{
}

void NOXMatParamDialog::ActivateDlg(BOOL onOff)
{
}

INT_PTR CALLBACK NOXMatParamDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	NOXMatParamDialog *dlg = DLGetWindowLongPtr<NOXMatParamDialog*>(hWnd);
	switch (msg) 
	{
		case WM_INITDIALOG:
			{
				dlg = (NOXMatParamDialog*)lParam;
				DLSetWindowLongPtr(hWnd, lParam);
				HWND hCombo  = GetDlgItem(hWnd, IDC_NOX_MAT_PICK_TEX);
				HWND hCombo2 = GetDlgItem(hWnd, IDC_NOX_MAT_LAYER_TEX);
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("none"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("roughness"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("reflection 0"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("reflection 90"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("weight"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("light"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("normal"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("transmission"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("anisotropy"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("aniso angle"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("opacity"));
				SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)_T("displacement"));
				SendMessage( hCombo, CB_SETCURSEL, 0, 0);
				fillLayersList(GetDlgItem(hWnd, IDC_NOX_MAT_LAYER_TEX), dlg->eMat);
				int nL = dlg->eMat->mat.getLayersNumber();
				int l = max(0, min(nL-1, dlg->eMat->curLayerNumber));
				int t = max(0, min(11, dlg->eMat->curTextureNumber));
				SendMessage( hCombo2, CB_SETCURSEL, l, 0);
				SendMessage( hCombo,  CB_SETCURSEL, t, 0);
			}
			break;
		case WM_DESTROY:
			break;      
		case WM_COMMAND:
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_NOX_MAT_RUN_EDITOR:
				{
					if (wmEvent == BN_CLICKED)
					{
						if (dlg)
						{
							HWND hCombo = GetDlgItem(hWnd, IDC_NOX_MAT_PICK_TEX);
							dlg->eMat->lastTextureNumber = (int)SendMessage( hCombo, CB_GETCURSEL, 0, 0);
							HWND hCombo2 = GetDlgItem(hWnd, IDC_NOX_MAT_LAYER_TEX);
							dlg->eMat->lastLayerNumber = (int)SendMessage( hCombo2, CB_GETCURSEL, 0, 0);

							dlg->reenableAccels = (TRUE == AcceleratorsEnabled());
							DisableAccelerators();

							bool accept = dlg->eMat->mat.runEditor2(dlg->hInst, hWnd);

							dlg->eMat->mat.backFromEditor(accept);
							dlg->eMat->updateToParamBlock();
						}
					}
					if (wmEvent == 6666)
					{
						if (dlg)
						{

							fillLayersList(GetDlgItem(hWnd, IDC_NOX_MAT_LAYER_TEX), dlg->eMat);
							dlg->backFromMatEditor();

							int nL = dlg->eMat->mat.getLayersNumber();
							HWND hCombo2 = GetDlgItem(hWnd, IDC_NOX_MAT_LAYER_TEX);
							HWND hCombo = GetDlgItem(hWnd, IDC_NOX_MAT_PICK_TEX);
							int l = min(nL-1, dlg->eMat->lastLayerNumber);
							SendMessage( hCombo2, CB_SETCURSEL, l, 0);
							SendMessage( hCombo,  CB_SETCURSEL, dlg->eMat->lastTextureNumber, 0);
							dlg->eMat->changeTexture(dlg->eMat->lastTextureNumber, l);
						}
					}
					
				}
				break;
				case IDC_NOX_MAT_BLEND_NAMES:
				{
					if (wmEvent == BN_CLICKED)
					{
						TrMaterial tr;
						tr.runBlendLayersDialog(hWnd);
					}
				}
				break;
				case IDC_NOX_MAT_LAYER_TEX:
				case IDC_NOX_MAT_PICK_TEX:
				{
					if (wmEvent != CBN_SELCHANGE)
						break;

					HWND hCombo = GetDlgItem(hWnd, IDC_NOX_MAT_PICK_TEX);
					int res = (int)SendMessage( hCombo, CB_GETCURSEL, 0, 0);
					res = min(11, max(0, res));
					HWND hCombo2 = GetDlgItem(hWnd, IDC_NOX_MAT_LAYER_TEX);
					int nL = (int)SendMessage( hCombo2, CB_GETCURSEL, 0, 0);
					dlg->eMat->curTextureNumber = res;
					dlg->eMat->curLayerNumber = nL;
					dlg->eMat->changeTexture(res, nL);
				}
				break;
			}; 
			break;

		default:
			return FALSE;
	}  
	return TRUE;
}

void fillLayersList(HWND hLay, NOXMaterial * nmat)
{
	int nL = nmat->mat.getLayersNumber();
	SendMessage(hLay, CB_RESETCONTENT, 0, 0);
	TCHAR buf[32];
	for (int i=1; i<=nL; i++)
	{
		_stprintf_s(buf, 32, _T("Layer %d"), i);
		SendMessage(hLay, CB_ADDSTRING, 0, (LPARAM)buf);
	}
}


INT_PTR CALLBACK NOXMatTexUpdateParamDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch (msg) 
	{
		case WM_INITDIALOG:
			{
				HWND hProgress =     GetDlgItem(hWnd, IDC_NOX_MAT_TEX_UPDATE_PROGRESSBAR);
				HWND hTextProgress = GetDlgItem(hWnd, IDC_NOX_MAT_TEX_UPDATE_TEXT);
				SendMessage(hProgress, PBM_SETRANGE, 0, (LPARAM) MAKELPARAM (0, 1));
				SendMessage(hProgress, PBM_SETPOS, 0, 0);
				SetWindowText(hTextProgress, _T("Updating textures..."));
			}
			break;
		case WM_DESTROY:
			break;
		default:
			return FALSE;
	}  
	return TRUE;
}

void NOXMaterial::setTexUpdateProgress(int nom, int den)
{
	if (!hTUInfoWindow)
		return;
	HWND hProgress =     GetDlgItem(hTUInfoWindow, IDC_NOX_MAT_TEX_UPDATE_PROGRESSBAR);
	HWND hTextProgress = GetDlgItem(hTUInfoWindow, IDC_NOX_MAT_TEX_UPDATE_TEXT);
	if (hProgress)
	{
		SendMessage(hProgress, PBM_SETRANGE, 0, (LPARAM) MAKELPARAM (0, den));
		SendMessage(hProgress, PBM_SETPOS, nom, 0);
	}
	
	if (hTextProgress)
	{
		TCHAR buf[128];
		_stprintf_s(buf, 128, _T("Updating texture %d of %d..."), nom, den);
		SetWindowText(hTextProgress, buf);
	}
	
	InvalidateRect(hProgress, NULL, false);
	InvalidateRect(hTextProgress, NULL, false);
	UpdateWindow(hProgress);
	UpdateWindow(hTextProgress);
}
