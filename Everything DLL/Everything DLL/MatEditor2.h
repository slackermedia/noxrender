#ifndef __MAT_EDITOR_2_MAIN_H
#define __MAT_EDITOR_2_MAIN_H

// this is new material editor header (used from version 0.40)

#include "DLL.h"
#include <windows.h>
#include "noxfonts.h"
#include "raytracer.h"

//----------------------------------------------------------------------

class MatEditor2
{
public:
	static const int MODE_ALONE = 0;
	static const int MODE_RENDERER = 1;
	static const int MODE_3DSMAX = 2;

	static INT_PTR CALLBACK MatEditor2WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static DWORD WINAPI RefreshThreadProc(LPVOID lpParameter);
	static DWORD WINAPI TextureWatcherThreadProc(LPVOID lpParameter);

	HBITMAP hBmpBg;
	NOXFontManager * fonts;
	bool isMatLibOpened;
	bool isMatEditOpened;
	bool refreshThreadRunning;
	bool needToUpdateDisplacement;
	bool saveOnOK;

	DECLDIR bool createWindow(HINSTANCE hInst, HWND parent, int mode, char * fileToLoad);
	DECLDIR static MatEditor2 * getInstance();
	DECLDIR HWND getHWND();
	DECLDIR void setSaveMode(bool saveOnOKButton);
	DECLDIR static void setLegacyMode(bool legacy);

	static bool acceptMaterial();
	bool initControls();
	bool updateBgShifts();
	bool postWindowOpen();
	bool updateMaterialToEditableMaterial();
	bool changeScene(int id);
	int whichSceneIsBestForMat(MaterialNox * mat);
	bool copyBufferWindowToMaterial();
	bool copyBufferMaterialToWindow();

	bool startRendering();
	bool stopRendering();
	bool refreshPreview();
	void startRefreshThread(float rtime);
	void stopRefreshThread();
	void startTextureWatcherThread();


	bool fillLayersList();
	bool updateCurrentLayerPropsOnList();
	bool layerAdd();
	bool layerDel();
	bool layerClone();

	bool layerToGUI(int num);
	bool guiToLayer(int num);
	bool layerCurrentToGUI();
	bool guiToLayerCurrent();
	bool matToGUI();
	bool guiToMat();
	bool updateDisplacement();

	bool runTextureDialog(TextureInstance * tex, bool normalsmode);
	bool updateTexButtons();

	bool loadMaterial(char * filename);
	bool loadMaterial();
	bool saveMaterial();


	bool showMatlib(bool on);
	bool setPositions(bool matLibOn);

	MaterialNox * getEditedMaterial();
	MatLayer * getEditedMatLayer();

	bool updateControlLocks(bool nowrendering);

private:
	char * autoLoadFilename;

	static MatEditor2 * mewInstance;
	static bool isWindowClassRegistered;
	ATOM registerMyClass(HINSTANCE hInst);
	HWND hMain;
	MatEditor2();
public:
	~MatEditor2();

};



//----------------------------------------------------------------------

#endif
