#define _CRT_RAND_S
#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <wingdi.h>
#include "BenchmarkWindow.h"
#include "resource.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include "log.h"

int MessageBoxNox(HWND hParent, char *messageText, char *titleText, HFONT hFontText, HFONT hFontButton);

bool setLicenseTextInControl(HWND hWnd);
int getControlLines(HWND hWnd);
int getTextLines(HWND hWnd);

#define lCenter 158
#define rCenter 782

extern HMODULE hDllModule;
extern char * bench_license_text;
HBITMAP loadWindowsImage(char * filename);
void invalidateWithParentRedraw(HWND hWnd);

HINSTANCE			BenchmarkWindow::hInstance2 = 0;
HMODULE				BenchmarkWindow::hModule = 0;
HBRUSH				BenchmarkWindow::backgroundBrush = 0;
HBITMAP				BenchmarkWindow::hBmpBg = 0;
HBITMAP				BenchmarkWindow::hBmpBgEmpty = 0;
HBITMAP				BenchmarkWindow::hBmpBgLic = 0;
HBITMAP				BenchmarkWindow::hBmpButtons = 0;
ThemeManager		BenchmarkWindow::theme;
HWND				BenchmarkWindow::hMain = 0;
HWND				BenchmarkWindow::hImage = 0;
HWND				BenchmarkWindow::hStartStop = 0;
HWND				BenchmarkWindow::hResultValue = 0;
HWND				BenchmarkWindow::hSendResult = 0;
HWND				BenchmarkWindow::hViewResults = 0;
HWND				BenchmarkWindow::hCompRes = 0;
HWND				BenchmarkWindow::hCpuText = 0;
HWND				BenchmarkWindow::hCpuValue = 0;
HWND				BenchmarkWindow::hCoresThreadsText = 0;
HWND				BenchmarkWindow::hCoresThreadsValue = 0;
HWND				BenchmarkWindow::hVersion = 0;
HWND				BenchmarkWindow::hAbout = 0;
HWND				BenchmarkWindow::hProgress = 0;
HWND				BenchmarkWindow::hLoadingTxt = 0;
HWND				BenchmarkWindow::hCrEngine = 0;
HWND				BenchmarkWindow::hCrBuild = 0;
HWND				BenchmarkWindow::hCrCredits = 0;
HWND				BenchmarkWindow::hCrCode = 0;
HWND				BenchmarkWindow::hCrCodeName = 0;
HWND				BenchmarkWindow::hCrGui = 0;
HWND				BenchmarkWindow::hCrGuiName = 0;
HWND				BenchmarkWindow::hCrScene = 0;
HWND				BenchmarkWindow::hCrSceneName = 0;
HWND				BenchmarkWindow::hCrScene2 = 0;
HWND				BenchmarkWindow::hCrScene2Name = 0;
HWND				BenchmarkWindow::hCrContactUs = 0;
HWND				BenchmarkWindow::hCrEmail = 0;
HWND				BenchmarkWindow::hCrLogo = 0;
HWND				BenchmarkWindow::hLicText = 0;
HWND 				BenchmarkWindow::hLicAccept = 0;
HWND 				BenchmarkWindow::hLicScroll = 0;
bool				BenchmarkWindow::refreshThrRunningB = false;
int					BenchmarkWindow::refreshTimeB = 10;
int					BenchmarkWindow::license_pos = 0;
int					BenchmarkWindow::license_height = 0;
bool				BenchmarkWindow::stoppingPassOn = false;
bool				BenchmarkWindow::about_mode = false;
bool				BenchmarkWindow::license_mode = false;
bool				BenchmarkWindow::loading_mode = false;
int					BenchmarkWindow::cmode = NBMODE_NORMAL;
bool				BenchmarkWindow::dll_ok = true;
bool				BenchmarkWindow::scene_hash_ok = true;
float				BenchmarkWindow::bResult = 0;
unsigned int		BenchmarkWindow::bTimeStart = 0;
BenchResult			BenchmarkWindow::bsRes;
HFONT				BenchmarkWindow::hFontGui = 0;
HFONT				BenchmarkWindow::hFontYourScore = 0;
HFONT				BenchmarkWindow::hFontResult = 0;
HFONT				BenchmarkWindow::hFontHardware = 0;
HFONT				BenchmarkWindow::hFontCPU[CPUFONTTRIES];
HFONT				BenchmarkWindow::hFontCPUName[CPUFONTTRIES];
HFONT				BenchmarkWindow::hFontCoresThreads[CPUFONTTRIES];
HFONT				BenchmarkWindow::hFontCoresThreadsValue[CPUFONTTRIES];
HFONT				BenchmarkWindow::hFontResultsComparison = 0;
HFONT				BenchmarkWindow::hFontVisitResultsPage = 0;
HFONT				BenchmarkWindow::hFontEngineVersion = 0;
HFONT				BenchmarkWindow::hFontAbout = 0;
HFONT				BenchmarkWindow::hFontBars = 0;
int					BenchmarkWindow::cpuFontChosen = 0;
HFONT				BenchmarkWindow::hFontCrEngine = 0;
HFONT 				BenchmarkWindow::hFontCrBuild = 0;
HFONT 				BenchmarkWindow::hFontCrCredits = 0;
HFONT 				BenchmarkWindow::hFontCrCode = 0;
HFONT 				BenchmarkWindow::hFontCrCodeValue = 0;
HFONT 				BenchmarkWindow::hFontCrGui = 0;
HFONT 				BenchmarkWindow::hFontCrGuiValue = 0;
HFONT 				BenchmarkWindow::hFontCrScene = 0;
HFONT 				BenchmarkWindow::hFontCrSceneValue = 0;
HFONT 				BenchmarkWindow::hFontCrContactUs = 0;
HFONT 				BenchmarkWindow::hFontCrEmail = 0;
HFONT 				BenchmarkWindow::hFontLicense = 0;
HFONT 				BenchmarkWindow::hFontProgress = 0;
HFONT 				BenchmarkWindow::hFontMessageText = 0;
HFONT 				BenchmarkWindow::hFontMessageButton = 0;

char *				BenchmarkWindow::cpuNameTextFormatted = NULL;
char *				BenchmarkWindow::coresThreadsCountText = NULL;
char *				BenchmarkWindow::versionText = NULL;


//----------------------------------------------------------------------------------------------------------------

BenchmarkWindow::BenchmarkWindow(HINSTANCE hInst)
{
	hInstance2 = hInst;
	{
		hModule = hDllModule;
		MyRegisterClass(hInst);
		LicenseRegisterClass(hModule);
	}
	hMain = 0;
}

//----------------------------------------------------------------------------------------------------------------

BenchmarkWindow::~BenchmarkWindow()
{
}

//----------------------------------------------------------------------------------------------------------------

ATOM BenchmarkWindow::MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	HICON sIcon = LoadIcon(hDllModule, MAKEINTRESOURCE(IDI_ICON1));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= BenchmarkWindow::WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;	//??
	wcex.hIcon			= sIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)CreateSolidBrush(RGB(40,40,40));
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "NOXBenchmarkWindow";
	wcex.hIconSm		= sIcon;
	return RegisterClassEx(&wcex);
}

//----------------------------------------------------------------------------------------------------------------

ATOM BenchmarkWindow::LicenseRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	HICON sIcon = NULL;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= BenchmarkWindow::LicParentWindowProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)CreateSolidBrush(RGB(40,40,40));
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "NOXBenchmarkLicenseWindow";
	wcex.hIconSm		= NULL;
	return RegisterClassEx(&wcex);
}

//----------------------------------------------------------------------------------------------------------------

HWND BenchmarkWindow::getHWND()
{
	return hMain;
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::createWindow(HWND hParent, int vMajor, int vMinor, int vBuild, int vComp)
{
	Logger::add("createWindow start");
	int wx = 1280;
	int wy = 800;
	int cx = GetSystemMetrics(SM_CXSCREEN);
	int cy = GetSystemMetrics(SM_CYSCREEN);
	while(rand_s(&bsRes.session_id));

	Logger::add("loading background window");
	hBmpBg = loadHBitmapFromBinaryResource(hInstance2, 103);		// background in exe
	hBmpBgLic = loadHBitmapFromBinaryResource(hInstance2, 109);		// background license in exe
	hBmpBgEmpty = loadHBitmapFromBinaryResource(hInstance2, 111);	// background with no special texts
	hBmpButtons = loadHBitmapFromBinaryResource(hInstance2, 105);	// buttons in exe
	ImageByteBuffer * imgLogo = new ImageByteBuffer();
	imgLogo->loadFromResource(hInstance2, 107);

	Raytracer * rtr = Raytracer::getInstance();
	Logger::add("loading fonts");
	loadFonts();

	Logger::add("creating window and controls");
	hMain = CreateWindow("NOXBenchmarkWindow", "NOX Benchmark 1.0", WS_OVERLAPPEDWINDOW ,
		  (cx-wx)/2, (cy-wy)/2,   wx-1,wy,    hParent, NULL, hInstance2, NULL);


	hImage		= CreateWindow("EMPView", "", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						400, 150,  640,480,   hMain, (HMENU)1300, hModule, NULL);
	ShowWindow(hImage, SW_HIDE);

	hStartStop	= CreateWindow("EMImgButton", "Start", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						672,614,  241,43,   hMain, (HMENU)1302, hModule, NULL);
	EnableWindow(hStartStop, FALSE);


	hResultValue = CreateWindow("STATIC", "----", WS_CHILD | WS_VISIBLE | SS_CENTER, 
						lCenter-100,176,  200,60,   hMain, (HMENU)1352, hModule, NULL);
	SendMessage(hResultValue, WM_SETFONT, (WPARAM)hFontResult, 1);

	hSendResult = CreateWindow("EMImgButton", "Send result", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						99,253,  117,43,   hMain, (HMENU)1303, hModule, NULL);	
	
	hViewResults = CreateWindow("EMText", "VISIT RESULTS PAGE", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						164,722,  130,20,   hMain, (HMENU)1304, hModule, NULL);
	hCompRes = CreateWindow("EMGraphList", "", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						33,531,  247,185,   hMain, (HMENU)1305, hModule, NULL);
	hAbout = CreateWindow("EMText", "ABOUT", WS_CHILD | WS_VISIBLE , 
						1162,751,  100,15,   hMain, (HMENU)1353, hModule, NULL);

	char tversion[256];
	sprintf_s(tversion, 256, "ENGINE: %d.%d.%d", Raytracer::getVersionMajor(), Raytracer::getVersionMinor(), Raytracer::getVersionBuild());
	versionText = copyString(tversion);
	hVersion = CreateWindow("STATIC", versionText, WS_CHILD | WS_VISIBLE , 
						1093,600,  180,15,   hMain, (HMENU)1352, hModule, NULL);
	SendMessage(hVersion, WM_SETFONT, (WPARAM)hFontEngineVersion, 1);

	hProgress = CreateWindow("EMProgressBar", "LOADING...", WS_CHILD | WS_VISIBLE , 
						rCenter-206,350,  2*206,19,   hMain, (HMENU)1359, hModule, NULL);

	EMProgressBar * empb = GetEMProgressBarInstance(hProgress);
	empb->useNewStyle = true;
	empb->hFont = hFontProgress;
	empb->align = EMProgressBar::ALIGN_CENTER_CONTROL;
	Logger::add("Applying styles");

	EMPView * empvimg = GetEMPViewInstance(hImage);
	empvimg->showBorder = true;
	theme.EMPViewBackground = theme.WindowBackground;
	EMImgButton * embStart = GetEMImgButtonInstance(hStartStop);
	EMImgButton * embSendRes = GetEMImgButtonInstance(hSendResult);
	EMText * emtVisitResults = GetEMTextInstance(hViewResults);
	EMText * emtAbout = GetEMTextInstance(hAbout);

	theme.apply(GetEMProgressBarInstance(hProgress));
	theme.apply(empvimg);
	theme.apply(emtAbout, false, false);
	theme.apply(embStart);
	theme.apply(embSendRes);
	theme.apply(emtVisitResults, false, false);
	empvimg->dontLetUserChangeZoom = true;
	empvimg->colBorder = RGB(78,78,78);

	emtVisitResults->hFont = hFontVisitResultsPage;
	emtVisitResults->colText = RGB(207, 207, 207);
	emtVisitResults->colLink = RGB(207, 207, 207);
	emtVisitResults->colLinkMouseOver = RGB(207,140,10);
	emtVisitResults->linkAddress = copyString("http://www.evermotion.org/nox/noxBenchmarkResults");
	emtVisitResults->isItHyperLink = true;
	emtVisitResults->drawTransparent = true;

	emtAbout->hFont = hFontAbout;
	emtAbout->colText = RGB(127,123,121);
	emtAbout->colLink = RGB(127,123,121);
	emtAbout->colLinkMouseOver = RGB(207,140,10);
	emtAbout->drawTransparent = true;
	emtAbout->align = EMText::ALIGN_RIGHT;
	emtAbout->isItHyperLink = true;
	

	embSendRes->showBorder = false;
	embSendRes->setToolTip("Upload your result to server.");
	embSendRes->bmAll = &(hBmpButtons);
	embSendRes->pNx = embSendRes->pMx = embSendRes->pCx = embSendRes->pDx = 482;
	embSendRes->pNy = 0;
	embSendRes->pMy = 43;
	embSendRes->pCy = 86;
	embSendRes->pDy = 129;

	embStart->showBorder = false;
	embStart->setToolTip("Start benchmark.");
	embStart->bmAll = &(hBmpButtons);
	embStart->pNx = embStart->pMx = embStart->pCx = embStart->pDx = 0;
	embStart->pNy = 0;
	embStart->pMy = 43;
	embStart->pCy = 86;
	embStart->pDy = 129;

	EMGraphList * emglres = GetEMGraphListInstance(hCompRes);	// these are random values
	emglres->addData(EMGraphList::Record("INTEL CORE I7-3930K",		21.2f,	RGB(30,45,74),		RGB(3,18,47),	RGB(64,64,64), RGB(207,207,207)));
	emglres->addData(EMGraphList::Record("AMD FX-8350",				14.5f,	RGB(30,45,74),		RGB(3,18,47),	RGB(64,64,64), RGB(207,207,207)));
	emglres->addData(EMGraphList::Record("INTEL PENTIUM 4",			2.3f,	RGB(30,45,74),		RGB(3,18,47),	RGB(64,64,64), RGB(207,207,207)));
	emglres->addData(EMGraphList::Record("INTEL CORE2 QUAD 9300",	8.9f,	RGB(30,45,74),		RGB(3,18,47),	RGB(64,64,64), RGB(207,207,207)));
	emglres->addData(EMGraphList::Record("AMD ATHLON 64 3200",		1.9f,	RGB(30,45,74),		RGB(3,18,47),	RGB(64,64,64), RGB(207,207,207)));
	emglres->bgTransparent = true;
	emglres->hFont = hFontBars;

	bool sortok = emglres->sortIDs(true);
	if (!sortok)
		Logger::add("results sort failed");

	SetWindowPos(hMain, HWND_TOP, 0, 0, wx, wy, SWP_NOZORDER | SWP_NOMOVE);

	// ---- CPU recognition ----
	Logger::add("cpu recognition");
	char * cpuname = getProcessorInfo(false);
	if (!cpuname)
		cpuname = copyString("Unknown CPU");
	char * newcpuname = formatCpuName(cpuname);
	free(cpuname);
	cpuname = newcpuname;
	int numCPU, numCores, numThreads;
	char * crthr = (char*)malloc(64);
	if (getProcessorInfoAdvanced(numCPU, numCores, numThreads))
	{
		if (numCPU>1)
		{
			int l = (int)strlen(cpuname)+10;
			char * newname = (char *)malloc(l);
			sprintf_s(newname, l, "%dx %s", numCPU, cpuname);
			free(cpuname);
			cpuname = newname;
		}
		
		sprintf_s(crthr, 64, "%d / %d", numCores, numThreads);

		bsRes.cpuname = copyString(cpuname);
		bsRes.num_cores = numCores;
		bsRes.num_threads = numThreads;
		bsRes.num_cpus = numCPU;
	}
	else
	{
		sprintf_s(crthr, 64, "%d / %d", numThreads, numThreads);

		bsRes.cpuname = copyString(cpuname);
		bsRes.num_threads = numThreads;
		bsRes.num_cores = 0;
		bsRes.num_cpus = 0;
	}

	_strupr_s(cpuname, strlen(cpuname)+1);
	cpuNameTextFormatted = cpuname;
	coresThreadsCountText = crthr;

	hCpuText = CreateWindow("STATIC", cputext, WS_CHILD | WS_VISIBLE,
						lCenter-100,376,  200,60,   hMain, (HMENU)1352, hModule, NULL);
	hCpuValue = CreateWindow("STATIC", cpuNameTextFormatted, WS_CHILD | WS_VISIBLE, 
						lCenter,376,  200,60,   hMain, (HMENU)1352, hModule, NULL);
	SendMessage(hCpuText , WM_SETFONT, (WPARAM)hFontCPU[cpuFontChosen], 1);
	SendMessage(hCpuValue , WM_SETFONT, (WPARAM)hFontCPUName[cpuFontChosen], 1);

	hCoresThreadsText = CreateWindow("STATIC", coresthreadstext, WS_CHILD | WS_VISIBLE,
						lCenter-130,406,  200,60,   hMain, (HMENU)1352, hModule, NULL);
	hCoresThreadsValue = CreateWindow("STATIC", coresThreadsCountText, WS_CHILD | WS_VISIBLE, 
						lCenter+50,406,  200,60,   hMain, (HMENU)1352, hModule, NULL);
	SendMessage(hCoresThreadsText , WM_SETFONT, (WPARAM)hFontCoresThreads[cpuFontChosen], 1);
	SendMessage(hCoresThreadsValue , WM_SETFONT, (WPARAM)hFontCoresThreadsValue[cpuFontChosen], 1);

	char bufaboutengine[256];
	sprintf_s(bufaboutengine, 256, "ENGINE: %d.%d.%d", Raytracer::getVersionMajor(), Raytracer::getVersionMinor(), Raytracer::getVersionBuild());
	hCrEngine = CreateWindow("STATIC", bufaboutengine, WS_CHILD | WS_VISIBLE | SS_CENTER,
						rCenter-200,257,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrBuild = CreateWindow("STATIC", "BUILD: 2013-03-08", WS_CHILD | WS_VISIBLE | SS_CENTER,
						rCenter-200,289,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrCredits = CreateWindow("STATIC", "CREDITS", WS_CHILD | WS_VISIBLE | SS_CENTER,
						rCenter-200,344,  400,30,   hMain, (HMENU)1352, hModule, NULL);
	hCrCode  = CreateWindow("STATIC", text_code, WS_CHILD | WS_VISIBLE,
						rCenter-200,397,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrCodeName = CreateWindow("STATIC", text_code_value, WS_CHILD | WS_VISIBLE,
						rCenter-140,397,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrGui = CreateWindow("STATIC", text_gui, WS_CHILD | WS_VISIBLE,
						rCenter-200,435,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrGuiName = CreateWindow("STATIC", text_gui_value, WS_CHILD | WS_VISIBLE,
						rCenter-140,435,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrScene = CreateWindow("STATIC", text_scene, WS_CHILD | WS_VISIBLE,
						rCenter-200,475,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrSceneName = CreateWindow("STATIC", text_scene_value, WS_CHILD | WS_VISIBLE,
						rCenter-140,475,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrScene2 = CreateWindow("STATIC", text_scene2, WS_CHILD | WS_VISIBLE,
						rCenter-200,515,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrScene2Name = CreateWindow("STATIC", text_scene2_value, WS_CHILD | WS_VISIBLE,
						rCenter-140,515,  400,20,   hMain, (HMENU)1352, hModule, NULL);
	hCrContactUs = CreateWindow("STATIC", "CONTACT US:", WS_CHILD | WS_VISIBLE | SS_CENTER,
						rCenter-200,567,  400,30,   hMain, (HMENU)1352, hModule, NULL);
	hCrEmail = CreateWindow("EMText", "marchewa@evermotion.org", WS_CHILD | WS_VISIBLE,
						rCenter-200,616,  400,20,   hMain, (HMENU)1359, hModule, NULL);
	EMText * emtemail = GetEMTextInstance(hCrEmail);
	theme.apply(emtemail, false, false);
	emtemail->colLink = RGB(207, 207, 207);
	emtemail->colLinkMouseOver = RGB(207,140,10);
	emtemail->isItHyperLink = true;
	emtemail->setHyperLink("mailto:marchewa@evermotion.org");
	emtemail->align = EMText::ALIGN_CENTER;
	emtemail->drawTransparent = true;
	emtemail->hFont = hFontCrEmail;
	
	hCrLogo = CreateWindow("EMPView", "", WS_CHILD | WS_VISIBLE | SS_CENTER,
						rCenter-imgLogo->width/2,129,  imgLogo->width,imgLogo->height,   hMain, (HMENU)1358, hModule, NULL);
	EMPView * empvlogo = GetEMPViewInstance(hCrLogo);
	empvlogo->byteBuff = imgLogo;
	empvlogo->opacityToParent = true;
	empvlogo->useImageOpacity = true;
	//hBmpLogo

	SendMessage(hCrEngine ,		WM_SETFONT, (WPARAM)hFontCrEngine, 1);
	SendMessage(hCrBuild ,		WM_SETFONT, (WPARAM)hFontCrBuild, 1);
	SendMessage(hCrCredits ,	WM_SETFONT, (WPARAM)hFontCrCredits, 1);
	SendMessage(hCrCode ,		WM_SETFONT, (WPARAM)hFontCrCode, 1);
	SendMessage(hCrCodeName ,	WM_SETFONT, (WPARAM)hFontCrCodeValue, 1);
	SendMessage(hCrGui ,		WM_SETFONT, (WPARAM)hFontCrGui, 1);
	SendMessage(hCrGuiName ,	WM_SETFONT, (WPARAM)hFontCrGuiValue, 1);
	SendMessage(hCrScene ,		WM_SETFONT, (WPARAM)hFontCrScene, 1);
	SendMessage(hCrSceneName ,	WM_SETFONT, (WPARAM)hFontCrSceneValue, 1);
	SendMessage(hCrScene2 ,		WM_SETFONT, (WPARAM)hFontCrScene, 1);
	SendMessage(hCrScene2Name ,	WM_SETFONT, (WPARAM)hFontCrSceneValue, 1);
	SendMessage(hCrContactUs ,	WM_SETFONT, (WPARAM)hFontCrContactUs, 1);
	SendMessage(hCrEmail ,		WM_SETFONT, (WPARAM)hFontCrEmail, 1);


	updateCpuCoresThreadsPositions();
	updateFrameVersionPositions();
	updateCreditsPositions();

	/// --------------- LICENSE
		hLicAccept = CreateWindow("EMImgButton", "Accept", WS_CHILD,
							648,661,  288,43,   hMain, (HMENU)1305, hModule, NULL);
		hLicScroll = CreateWindow("EMScrollBar", "", WS_CHILD,
							1093,139,  19,488,   hMain, (HMENU)1306, hModule, NULL);
		hLicText = CreateWindow("NOXBenchmarkLicenseWindow", "", WS_CHILD,
							523,139,  570,488,   hMain, (HMENU)1308, hModule, NULL);
	// ---- END LICENSE

	Logger::add("show window");
	ShowWindow(hMain, SW_SHOW);
	UpdateWindow(hMain);

	#ifdef CHECK_BENCHMARK_SCENE
		Logger::add("checking scene");
		char schash[33];
		bool schok = evalSceneHash("scene1\\scene.nox", schash);
		if (schok)
		{
			scene_hash_ok = false;
			if (!strcmp("4bcffbded9d2b56425ba95a52cffb3f3", schash))
				scene_hash_ok = true;
		}
		if (!scene_hash_ok)
			MessageBoxNox(hMain, "NOX Bench has detected that scene has been replaced.\nYou can run benchmark, however you won't be able to upload your result.", 
					"Warning", hFontMessageText, hFontMessageButton);
	#endif

	// ---- LOAD SCENE
	startLoadingThread();
	Logger::add("createWindow done");

	EnableWindow(hStartStop, TRUE);
	InvalidateRect(hStartStop, NULL, FALSE);

	dll_ok = true;
	if (vMajor!=NOX_VER_MAJOR  ||  vMinor!=NOX_VER_MINOR  ||  vBuild!=NOX_VER_BUILD  ||  vComp!=NOX_VER_COMP_NO)
	{
		dll_ok = false;
		MessageBoxNox(hMain, "NOX Bench has detected that dll version does not match.\nYou can run benchmark, however you won't be able to upload your result.", "Warning", hFontMessageText, hFontMessageButton);
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::startRender()
{
	EnableWindow(hStartStop, false);
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Camera * cam = sc->getActiveCamera();
	cam->blendBits = rtr->curScenePtr->evalBlendBits();
	cam->staticRegion.overallHits = 0;
	cam->bucket_hwnd_image = hImage;

	sc->sscene.random_type = Camera::RND_TYPE_BUCKETS;
	sc->sscene.stop_after_pass = true;

	sc->sscene.samples_per_pixel_in_pass = 100;
	sc->sscene.pt_gi_samples = 4;

	bool initOK = cam->initializeCamera(true);
	if (!initOK)
	{
		EnableWindow(hStartStop, true);
		return false;
	}
	

	EnableWindow(hSendResult , FALSE);
	InvalidateRect(hSendResult, NULL, FALSE);

	EMPView * empv = GetEMPViewInstance(hImage);
	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();
	empv->byteBuff->allocBuffer(cam->width, cam->height);
	empv->byteBuff->clearBuffer();
	empv->otherSource = cam->imgBuff;

	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	int prn = rtr->curScenePtr->thrM->getProcessorsNumber();
	if (prn < 1)
		prn = 1;

	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		MaterialNox * m = rtr->curScenePtr->mats[i];
		if (m)
			m->prepareToRender();
	}

	cam->rendTime = 0;
	cam->startTick = GetTickCount();
	cam->nowRendering = true;

	if (cam->buckets)
	{
		empv->showBuckets = true;
		cam->buckets->nThr = prn;
		cam->buckets->coordsThreads = (int *)malloc(cam->buckets->nThr*sizeof(int)*4);
		for (int i=0; i<cam->buckets->nThr*4; i++)
			cam->buckets->coordsThreads[i] = -1;
		empv->bucketsCoords = cam->buckets->coordsThreads;
		empv->nBuckets = cam->buckets->nThr;

		cam->buckets->stopOnLast = true;
	}
	stoppingPassOn = false;

	bTimeStart = GetTickCount();

	sc->thrM->registerStoppedCallback(stoppedRenderingCallback);
	sc->thrM->initThreads(prn, false);
	sc->thrM->runThreads();
	sc->nowRendering = true;

	EMImgButton * embss = GetEMImgButtonInstance(hStartStop);
	embss->showBorder = false;
	embss->setToolTip("Abort benchmark.");
	embss->bmAll = &(hBmpButtons);
	embss->pNx = embss->pMx = embss->pCx = embss->pDx = 241;
	embss->pNy = 0;
	embss->pMy = 43;
	embss->pCy = 86;
	embss->pDy = 129;
	EnableWindow(hStartStop, true);

	updateFrameVersionPositions();
	InvalidateRect(hMain, NULL, false);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::stopRender(bool byUser)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	
	sc->thrM->stopThreads();
	sc->thrM->resumeThreads();
	sc->thrM->disposeThreads();

	if (!byUser)
	{
		unsigned int sTime = GetTickCount();
		unsigned int dTime = sTime - bTimeStart;
		bResult = 10000000.0f/dTime;
		char buf[256];
		sprintf_s(buf, 256, "%.1f", bResult);
		SetWindowText(hResultValue, buf);
		invalidateWithParentRedraw(hResultValue);
		addResToBars(bResult);

		if (scene_hash_ok  &&  dll_ok)
		{
			EnableWindow(hSendResult , TRUE);
			InvalidateRect(hSendResult, NULL, FALSE);
			InvalidateRect(hMain, NULL, FALSE);
		}
	}

	sc->nowRendering = false;
	Camera * cam = sc->getActiveCamera();
	cam->nowRendering = false;
	int ss = (GetTickCount() - cam->startTick)/1000;
	if (ss < 0)
	{
	}
	cam->rendTime += ss;
	if (cam->buckets)
	{
		cam->buckets->nThr = 0;
		if (cam->buckets->coordsThreads)
			free(cam->buckets->coordsThreads);
		cam->buckets->coordsThreads = NULL;
	}

	EMPView * empv = GetEMPViewInstance(hImage);
	empv->bucketsCoords = NULL;
	empv->nBuckets = 0;
	empv->showBuckets = false;

	EMImgButton * embss = GetEMImgButtonInstance(hStartStop);
	embss->showBorder = false;
	embss->setToolTip("Start benchmark.");
	embss->bmAll = &(hBmpButtons);
	embss->pNx = embss->pMx = embss->pCx = embss->pDx = 0;
	embss->pNy = 0;
	embss->pMy = 43;
	embss->pCy = 86;
	embss->pDy = 129;
	EnableWindow(hStartStop, true);
	InvalidateRect(hStartStop, NULL, false);

	InvalidateRect(hImage, NULL, false);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::runRefreshThread()
{
	refreshThrRunningB = true;
	DWORD threadID;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RefreshThreadProc), (LPVOID)0, 0, &threadID);
}

//----------------------------------------------------------------------------------------------------------------

DWORD WINAPI BenchmarkWindow::RefreshThreadProc(LPVOID lpParameter)
{
	clock_t lTime = clock();
	clock_t cTime;
	double diff;

	while (refreshThrRunningB)
	{
		Sleep(200);
		cTime = clock();
		diff  = (cTime - lTime)/(double)CLOCKS_PER_SEC;
		if (diff > refreshTimeB)
		{
			//if (refreshTimeOn)
				refreshRenderedImage();
			lTime = clock();
		}
	}
	return 0;
}

//----------------------------------------------------------------------------------------------------------------

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

bool BenchmarkWindow::refreshRenderedImage(RedrawRegion * rgn)
{
	EMPView * empv = GetEMPViewInstance(hImage);
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(rtr->curScenePtr);
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	CHECK(cam);

	static bool nowRefreshing = false;
	if(nowRefreshing)
		return false;
	nowRefreshing = true;
	updateRenderedImage(empv, true, false, rgn, true);
	nowRefreshing = false;
	InvalidateRect(hImage, NULL, false);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::stoppedRenderingCallback(void)
{
	stoppingPassOn = true;
	SendMessage(hMain, WM_TIMER, TIMER_STOP_ID, 0);
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::sendResultToServer()
{
	BenchResult bres = bsRes;
	bres.nox_ver_major = NOX_VER_MAJOR;
	bres.nox_ver_minor = NOX_VER_MINOR;
	bres.nox_ver_build = NOX_VER_BUILD;
	bres.result = (float)bResult;

	DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_UPLOAD_BENCHRES), hMain, UploadBenchResDlgProc,(LPARAM)(&bres));

	return true;
}

//----------------------------------------------------------------------------------------------------------------

HFONT loadFontShort(char * name, int size)
{
	if (!name)
		return 0;

	LOGFONT lf;
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight = -MulDiv(size, 96, 72);
	lf.lfWeight = FW_NORMAL;
	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	lf.lfQuality = CLEARTYPE_QUALITY;
	sprintf_s(lf.lfFaceName, LF_FACESIZE, name);

	return CreateFontIndirect(&lf);
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::loadFonts()
{
	int nResults = AddFontResourceEx(font1Filename,
		FR_PRIVATE,
		NULL);

	LOGFONT lf;
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight = -MulDiv(20, 96, 72);
	lf.lfWeight = FW_NORMAL;
	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	sprintf_s(lf.lfFaceName, LF_FACESIZE, font1Name);

	hFontGui = CreateFontIndirect(&lf);
	if (!hFontGui)
		return false;
	
	hFontYourScore = loadFontShort(font1Name, 18);
	hFontResult = loadFontShort(font1Name, 36);
	hFontHardware = loadFontShort(font1Name, 18);
	for (int i=0; i<CPUFONTTRIES; i++)
	{
		hFontCPU[i] = loadFontShort(font1Name, 11-i);
		hFontCPUName[i] = loadFontShort(font1Name, 11-i);
		hFontCoresThreads[i] = loadFontShort(font1Name, 11-i);
		hFontCoresThreadsValue[i] = loadFontShort(font1Name, 11-i);
	}
	hFontResultsComparison = loadFontShort(font1Name, 24);
	hFontVisitResultsPage = loadFontShort(font1Name, 9);
	hFontEngineVersion = loadFontShort(font1Name, 10);
	hFontAbout = loadFontShort(font1Name, 10);
	hFontBars = loadFontShort(font1Name, 8);
	hFontProgress = loadFontShort(font1Name, 8);
	hFontLicense = loadFontShort("Arial", 10);

	hFontCrEngine = loadFontShort(font1Name, 10);
	hFontCrBuild = loadFontShort(font1Name, 10);
	hFontCrCredits = loadFontShort(font1Name, 18);
	hFontCrCode = loadFontShort(font1Name, 12);
	hFontCrCodeValue = loadFontShort(font1Name, 12);
	hFontCrGui = loadFontShort(font1Name, 12);
	hFontCrGuiValue = loadFontShort(font1Name, 12);
	hFontCrScene = loadFontShort(font1Name, 12);
	hFontCrSceneValue = loadFontShort(font1Name, 12);
	hFontCrContactUs = loadFontShort(font1Name, 18);
	hFontCrEmail = loadFontShort(font1Name, 11);

	hFontMessageText = loadFontShort(font1Name, 10);
	hFontMessageButton = loadFontShort(font1Name, 10);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::unloadFonts()
{
	DeleteObject(hFontYourScore);
	DeleteObject(hFontResult);
	DeleteObject(hFontHardware);
	for (int i=0; i<CPUFONTTRIES; i++)
	{
		DeleteObject(hFontCPU);
		DeleteObject(hFontCPUName);
		DeleteObject(hFontCoresThreads);
		DeleteObject(hFontCoresThreadsValue);
	}
	DeleteObject(hFontResultsComparison);
	DeleteObject(hFontVisitResultsPage);
	DeleteObject(hFontEngineVersion);
	DeleteObject(hFontAbout);
	DeleteObject(hFontBars);
	DeleteObject(hFontProgress);
	DeleteObject(hFontLicense);

	DeleteObject(hFontCrEngine);
	DeleteObject(hFontCrBuild);
	DeleteObject(hFontCrCredits);
	DeleteObject(hFontCrCode);
	DeleteObject(hFontCrCodeValue);
	DeleteObject(hFontCrGui);
	DeleteObject(hFontCrGuiValue);
	DeleteObject(hFontCrScene);
	DeleteObject(hFontCrSceneValue);
	DeleteObject(hFontCrContactUs);
	DeleteObject(hFontCrEmail);

	DeleteObject(hFontMessageText);
	DeleteObject(hFontMessageButton);


	DeleteObject(hFontGui);
	BOOL rOK = RemoveFontResourceEx(font1Filename, FR_PRIVATE, NULL);
}

//----------------------------------------------------------------------------------------------------------------

int BenchmarkWindow::getTextWidth(char * txt, HFONT hFont)
{
	if (!txt)
		return 0;
	int l = (int)strlen(txt);
	if (l<1)
		return 0;

	HDC hdc = GetWindowDC(hMain);
	if (!hdc)
		return 0;

	HFONT oldFont = (HFONT)SelectObject(hdc, hFont);
	SIZE sz;
	GetTextExtentPoint32(hdc, txt, l, &sz);
	SelectObject(hdc, oldFont);
	ReleaseDC(hMain, hdc);

	return sz.cx;
}

//----------------------------------------------------------------------------------------------------------------

// helper... will free str or return same address if substr not found
char * helperCutSubstring(char * str, char * substr)	
{
	char * str2 = str;
	while ( strstr(str2, substr) )
	{
		int l = (int)strlen(str2)+2;
		int sl = (int)strlen(substr);
		char * buf = (char *)malloc(l);
		char * p = strstr(str2, substr);
		if (p>str2)
		{
			memcpy_s(buf, l, str2, (p-str2));
			buf[p-str2] = 0;
		}
		if ((p-str2)+sl<l)
		{
			sprintf_s(buf+(p-str2), l-(p-str2), "%s", str2+(p-str2+sl) );
		}
		free(str2);
		str2 = buf;
	}
	return str2;
}

//----------------------------------------------------------------------------------------------------------------

char * BenchmarkWindow::formatCpuName(char * cpu)
{
	if (!cpu)
		return NULL;
	if (strlen(cpu)<1)
		return NULL;

	char * cpu2 = copyString(cpu);

	cpu2 = helperCutSubstring(cpu2, "(R)");
	cpu2 = helperCutSubstring(cpu2, "(r)");
	cpu2 = helperCutSubstring(cpu2, "(TM)");
	cpu2 = helperCutSubstring(cpu2, "(tm)");
	cpu2 = helperCutSubstring(cpu2, "CPU");
	cpu2 = helperCutSubstring(cpu2, "cpu");
	cpu2 = helperCutSubstring(cpu2, "processor");
	cpu2 = helperCutSubstring(cpu2, "Processor");
	cpu2 = helperCutSubstring(cpu2, "PROCESSOR");

	char * substring = "  ";
	while ( strstr(cpu2, substring) )
	{
		int l = (int)strlen(cpu2)+2;
		int sl = (int)strlen(substring);
		char * buf = (char *)malloc(l);
		char * p = strstr(cpu2, substring);
		if (p>cpu2)
		{
			memcpy_s(buf, l, cpu2, (p-cpu2));
			buf[p-cpu2] = 0;
		}
		if ((p-cpu2)+sl<l)
		{
			sprintf_s(buf+(p-cpu2), l-(p-cpu2), " %s", cpu2+(p-cpu2+sl) );
		}
		free(cpu2);
		cpu2 = buf;
	}

	return cpu2;
	return copyString(cpu);
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::updateCpuCoresThreadsPositions()
{
	int s1,s2,s3,s4;
	for (int i=0; i<CPUFONTTRIES; i++)
	{
		cpuFontChosen = i;
		s1 = getTextWidth(cputext, hFontCPU[cpuFontChosen]);
		s2 = getTextWidth(cpuNameTextFormatted, hFontCPUName[cpuFontChosen]);
		s3 = getTextWidth(coresthreadstext, hFontCoresThreads[cpuFontChosen]);
		s4 = getTextWidth(coresThreadsCountText, hFontCoresThreadsValue[cpuFontChosen]);
		char buf[64];
		sprintf_s(buf, 64, "cpu width %d", (s1+s2));
		Logger::add(buf);
		int maxwidth = 294;
		if ((s1+s2<=maxwidth)   &&   (s3+s4<maxwidth))
			break;
	}

	SendMessage(hCpuText ,				WM_SETFONT, (WPARAM)hFontCPU[cpuFontChosen],				1);
	SendMessage(hCpuValue ,				WM_SETFONT, (WPARAM)hFontCPUName[cpuFontChosen],			1);
	SendMessage(hCoresThreadsText ,		WM_SETFONT, (WPARAM)hFontCoresThreads[cpuFontChosen],		1);
	SendMessage(hCoresThreadsValue ,	WM_SETFONT, (WPARAM)hFontCoresThreadsValue[cpuFontChosen],	1);


	int c1x = lCenter - (s1+s2)/2;
	int c2x = c1x + s1;
	int c3x = lCenter - (s3+s4)/2;
	int c4x = c3x + s3;
	SetWindowPos(hCpuText,				HWND_TOP, c1x, 387, s1, 20, SWP_NOZORDER);
	SetWindowPos(hCpuValue,				HWND_TOP, c2x, 387, s2, 20, SWP_NOZORDER);
	SetWindowPos(hCoresThreadsText,		HWND_TOP, c3x, 424, s3, 20, SWP_NOZORDER);
	SetWindowPos(hCoresThreadsValue,	HWND_TOP, c4x, 424, s4, 20, SWP_NOZORDER);
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::updateFrameVersionPositions()
{
	int s1 = getTextWidth(versionText, hFontEngineVersion);
	int cx = 477+315;
	int cy = 358;
	int fx, fy;
	EMPView * empv = GetEMPViewInstance(hImage);
	if (empv  &&  empv->byteBuff)
	{
		fx = empv->byteBuff->width;
		fy = empv->byteBuff->height;
	}
	else
	{
		RECT frect;
		GetClientRect(hImage, &frect);
		fx = frect.right;
		fy = frect.bottom;
	}

	SetWindowPos(hImage, HWND_TOP, cx-fx/2, cy-fy/2, fx, fy, SWP_NOZORDER);
	SetWindowPos(hVersion, HWND_TOP, cx+fx/2-s1, cy+fy/2+9, s1, 15, SWP_NOZORDER);
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::updateCreditsPositions()
{
	int s1,s2,s3,s4,s5,s6,s7,s8;
	s1 = getTextWidth(text_code, hFontCrCode);
	s2 = getTextWidth(text_code_value, hFontCrCodeValue);
	s3 = getTextWidth(text_gui, hFontCrGui);
	s4 = getTextWidth(text_gui_value, hFontCrGuiValue);
	s5 = getTextWidth(text_scene, hFontCrScene);
	s6 = getTextWidth(text_scene_value, hFontCrSceneValue);
	s7 = getTextWidth(text_scene2, hFontCrScene);
	s8 = getTextWidth(text_scene2_value, hFontCrSceneValue);

	SendMessage(hCrCode,		WM_SETFONT, (WPARAM)hFontCrCode,		1);
	SendMessage(hCrCodeName,	WM_SETFONT, (WPARAM)hFontCrCodeValue,	1);
	SendMessage(hCrGui,			WM_SETFONT, (WPARAM)hFontCrGui,			1);
	SendMessage(hCrGuiName,		WM_SETFONT, (WPARAM)hFontCrGuiValue,	1);
	SendMessage(hCrScene,		WM_SETFONT, (WPARAM)hFontCrScene,		1);
	SendMessage(hCrSceneName,	WM_SETFONT, (WPARAM)hFontCrSceneValue,	1);
	SendMessage(hCrScene2,		WM_SETFONT, (WPARAM)hFontCrScene,		1);
	SendMessage(hCrScene2Name,	WM_SETFONT, (WPARAM)hFontCrSceneValue,	1);

	int c1x = rCenter - (s1+s2)/2;
	int c2x = c1x + s1;
	int c3x = rCenter - (s3+s4)/2;
	int c4x = c3x + s3;
	int c5x = rCenter - (s5+s6)/2;
	int c6x = c5x + s5;
	int c7x = rCenter - (s7+s8)/2;
	int c8x = c7x + s7;
	SetWindowPos(hCrCode,		HWND_TOP, c1x, 393, s1, 20, SWP_NOZORDER);
	SetWindowPos(hCrCodeName,	HWND_TOP, c2x, 393, s2, 20, SWP_NOZORDER);
	SetWindowPos(hCrGui,		HWND_TOP, c3x, 431, s3, 20, SWP_NOZORDER);
	SetWindowPos(hCrGuiName,	HWND_TOP, c4x, 431, s4, 20, SWP_NOZORDER);
	SetWindowPos(hCrScene,		HWND_TOP, c5x, 470, s5, 20, SWP_NOZORDER);
	SetWindowPos(hCrSceneName,	HWND_TOP, c6x, 470, s6, 20, SWP_NOZORDER);
	SetWindowPos(hCrScene2,		HWND_TOP, c7x, 509, s7, 20, SWP_NOZORDER);
	SetWindowPos(hCrScene2Name,	HWND_TOP, c8x, 509, s8, 20, SWP_NOZORDER);
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::addResToBars(float f)
{
	EMGraphList * emgl = GetEMGraphListInstance(hCompRes);
	CHECK(emgl);
	if (emgl->entries.objCount>5)
	{
		emgl->entries.trimLastObjects(emgl->entries.objCount-5);
		emgl->entries.createArray();
	}
	emgl->resetSortedIDs();
	emgl->addData(EMGraphList::Record("YOUR SCORE", f, RGB(207,140,10), RGB(132,101,8), RGB(32,32,32), RGB(1,1,1)));
	emgl->sortIDs(true);
	invalidateWithParentRedraw(hCompRes);
	return true;
}

////----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::setMode(int newmode)
{
	if (newmode==NBMODE_NORMAL  ||  newmode==NBMODE_RENDERING)
		ShowWindow(hImage, SW_SHOW);
	else
		ShowWindow(hImage, SW_HIDE);

	if (newmode==NBMODE_NORMAL  ||  newmode==NBMODE_RENDERING)
		ShowWindow(hVersion, SW_SHOW);
	else
		ShowWindow(hVersion, SW_HIDE);

	if (newmode==NBMODE_ABOUT)
		GetEMTextInstance(hAbout)->changeCaption("BACK");
	else
		GetEMTextInstance(hAbout)->changeCaption("ABOUT");

	if (newmode==NBMODE_LICENSE  ||  newmode==NBMODE_LOADING)
		ShowWindow(hAbout, SW_HIDE);
	else
		ShowWindow(hAbout, SW_SHOW);


	int cropt = (newmode==NBMODE_ABOUT) ? SW_SHOW : SW_HIDE;
	ShowWindow(hCrEngine,		cropt);
	ShowWindow(hCrEngine,		cropt);
	ShowWindow(hCrBuild,		cropt);
	ShowWindow(hCrCredits,		cropt);
	ShowWindow(hCrCode,			cropt);
	ShowWindow(hCrCodeName,		cropt);
	ShowWindow(hCrGui,			cropt);
	ShowWindow(hCrGuiName,		cropt);
	ShowWindow(hCrScene,		cropt);
	ShowWindow(hCrSceneName,	cropt);
	ShowWindow(hCrScene2,		cropt);
	ShowWindow(hCrScene2Name,	cropt);
	ShowWindow(hCrContactUs,	cropt);
	ShowWindow(hCrEmail,		cropt);
	ShowWindow(hCrLogo,			cropt);

	int licopt = (newmode==NBMODE_LICENSE) ? SW_SHOW : SW_HIDE;
	ShowWindow(hLicText,		licopt);
	ShowWindow(hLicAccept,		licopt);
	ShowWindow(hLicScroll,		licopt);

	if (newmode==NBMODE_RENDERING)
	{
		EMImgButton * embStart = GetEMImgButtonInstance(hStartStop);
		embStart->setToolTip("Abort benchmark.");
		embStart->bmAll = &(hBmpButtons);
		embStart->pNx = embStart->pMx = embStart->pCx = embStart->pDx = 241;
		embStart->pNy = 0;
		embStart->pMy = 43;
		embStart->pCy = 86;
		embStart->pDy = 129;
	}
	else
	{
		EMImgButton * embStart = GetEMImgButtonInstance(hStartStop);
		embStart->setToolTip("Start benchmark.");
		embStart->bmAll = &(hBmpButtons);
		embStart->pNx = embStart->pMx = embStart->pCx = embStart->pDx = 0;
		embStart->pNy = 0;
		embStart->pMy = 43;
		embStart->pCy = 86;
		embStart->pDy = 129;
	}

	if (newmode==NBMODE_NORMAL  ||  newmode==NBMODE_RENDERING)
		ShowWindow(hStartStop, SW_SHOW);
	else
		ShowWindow(hStartStop, SW_HIDE);

	if (newmode==NBMODE_LOADING)
	{
		ShowWindow(hProgress, SW_SHOW);
	}
	else
	{
		ShowWindow(hProgress, SW_HIDE);
	}

	InvalidateRect(hMain, NULL, false);
	cmode = newmode;

	if (newmode==NBMODE_LOADING  ||  newmode==NBMODE_LICENSE)
		EnableWindow(hCompRes, FALSE);
	else
		EnableWindow(hCompRes, TRUE);
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::licenseAlreadyAccepted()
{
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get NoxBench key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "NoxBench", 0, KEY_READ, &key3);

	// try to load "accepted" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres = RegQueryValueEx(key3, "accepted", NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS  &&  type==REG_DWORD  &&  *(int*)buffer==1)
		return true;
	else
		return false;
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::addLicenseAccepted()
{
	DWORD rsize = 2048;
	LONG regres;
	HKEY key1, key2, key3;

	// get NoxBench key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ | KEY_CREATE_SUB_KEY, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ | KEY_CREATE_SUB_KEY, &key2);
	if (regres != ERROR_SUCCESS)
		regres = RegCreateKey(key1, "Evermotion", &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "NoxBench", 0, KEY_READ | KEY_SET_VALUE, &key3);
	if (regres != ERROR_SUCCESS)
		regres = RegCreateKey(key2, "NoxBench", &key3);

	bool result;
	if (regres == ERROR_SUCCESS)
	{	// if OK
		int accval = 1;
		regres = RegSetValueEx(key3, "accepted", 0, REG_DWORD, (BYTE*)(&accval), 4);
		if (regres != ERROR_SUCCESS)
			result = false;
		else
			result = true;
	}
	else
	{
		result = false;
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	return result;
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::evalSceneHash(char * filename, char * hash)
{
	if (!hash)
		return false;

	#define MD5_BUFSIZE 1024
	#define MD5_LEN 16
	DWORD dwStatus = 0;
    BOOL bResult = FALSE;
    HCRYPTPROV hProv = 0;
    HCRYPTHASH hHash = 0;
    HANDLE hFile = NULL;
    BYTE rgbFile[MD5_BUFSIZE];
    DWORD cbRead = 0;
    BYTE rgbHash[MD5_LEN];
    DWORD cbHash = 0;
    CHAR rgbDigits[] = "0123456789abcdef";

    hFile = CreateFile(filename,
        GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        FILE_FLAG_SEQUENTIAL_SCAN,
        NULL);

    if (INVALID_HANDLE_VALUE == hFile)
    {
        dwStatus = GetLastError();
        return false;
    }

    if (!CryptAcquireContext(&hProv,
        NULL,
        NULL,
        PROV_RSA_FULL,
        CRYPT_VERIFYCONTEXT))
    {
        dwStatus = GetLastError();
        CloseHandle(hFile);
        return false;
    }

    if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
    {
        dwStatus = GetLastError();
        CloseHandle(hFile);
        CryptReleaseContext(hProv, 0);
        return false;
    }

    while (bResult = ReadFile(hFile, rgbFile, MD5_BUFSIZE, 
        &cbRead, NULL))
    {
        if (0 == cbRead)
        {
            break;
        }

        if (!CryptHashData(hHash, rgbFile, cbRead, 0))
        {
            dwStatus = GetLastError();
            CryptReleaseContext(hProv, 0);
            CryptDestroyHash(hHash);
            CloseHandle(hFile);
            return false;
        }
    }

    if (!bResult)
    {
        dwStatus = GetLastError();
        CryptReleaseContext(hProv, 0);
        CryptDestroyHash(hHash);
        CloseHandle(hFile);
        return false;
    }

    cbHash = MD5_LEN;
    if (!CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
        dwStatus = GetLastError();

    CryptDestroyHash(hHash);
    CryptReleaseContext(hProv, 0);
    CloseHandle(hFile);

	if (dwStatus==ERROR_SUCCESS)
	{
        for (DWORD i = 0; i < cbHash; i++)
        {
            hash[2*i+0] = rgbDigits[rgbHash[i] >> 4];
            hash[2*i+1] = rgbDigits[rgbHash[i] & 0xf];
        }
		hash[32] = 0;
		return true;
	}

	return false;
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::checkAndAddLicense()
{
	bool licAccepted = false;
	#ifdef CHECK_BENCHMARK_LICENSE
		licAccepted = licenseAlreadyAccepted();
	#endif
	if (!licAccepted)
	{
		// eval text size for scroll size
		HDC tthdc = GetWindowDC(hMain);
		HFONT oldFont = (HFONT)SelectObject(tthdc, hFontLicense);
		RECT rectlic;
		DrawText(tthdc, bench_license_text, (int)strlen(bench_license_text), &rectlic, DT_CALCRECT);
		license_height = rectlic.bottom;
		SelectObject(tthdc, oldFont);
		ReleaseDC(hMain, tthdc);

		EMScrollBar * emsb = GetEMScrollBarInstance(hLicScroll);
		int wl = getTextLines(hLicText);
		emsb->size_screen = 488;
		emsb->size_work = rectlic.bottom;
		emsb->workarea_pos = 0;
		emsb->updateSize();
		emsb->updateRegions();
		emsb->useNewStyle = true;

		EMImgButton * embAccept = GetEMImgButtonInstance(hLicAccept);
		embAccept->showBorder = false;
		embAccept->setToolTip("Do you agree to given license?");
		embAccept->bmAll = &(hBmpButtons);
		embAccept->pNx = embAccept->pMx = embAccept->pCx = embAccept->pDx = 599;
		embAccept->pNy = 0;
		embAccept->pMy = 43;
		embAccept->pCy = 86;
		embAccept->pDy = 129;

		setMode(NBMODE_LICENSE);
	}
}



