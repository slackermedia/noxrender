#ifndef __RENDERER_MAIN2_WINDOW__
#define __RENDERER_MAIN2_WINDOW__

// this is new version of main renderer window (from v. 0.40)

#include "DLL.h"
#include "noxfonts.h"
#include "Colors.h"
#include <windows.h>
#include "dialogs.h"

#define TIMER_STOP_ID 101
#define TIMER_AUTOSAVE_ID 102
#define TIMER_POSTPROCESS_ID 103

#define IDC_REND2_TABS 1000
#define IDC_REND2_BUTTON_SHOWHIDE_PANEL 1001
#define IDC_REND2_BUTTON_SHOWHIDE_REGIONS 1004
#define IDC_REND2_VIEWPORT 1002
#define IDC_REND2_MENU_SCENE 1003
#define IDC_REND2_MENU_HELP 1005
#define IDC_REND2_MENU_PRESETS 1006

#define IDC_REND2_STEXT_1_WELCOME 1101
#define IDC_REND2_STEXT_2_NOXNAME 1102
#define IDC_REND2_STEXT_3_LOAD_LINK 1103
#define IDC_REND2_STEXT_4_NOX_SCENE 1104
#define IDC_REND2_STEXT_5_OPEN_LINK 1105
#define IDC_REND2_STEXT_6_SAMPLESCENES 1106
#define IDC_REND2_STEXT_7_OR 1107

#define ID_MENU_HELP_ABOUT 0
#define ID_MENU_HELP_LICENSE 1
#define ID_MENU_HELP_NOX_FORUM 2
#define ID_MENU_HELP_NOX_PROFILE 3

#define ID_MENU_SCENE_LOAD 0
#define ID_MENU_SCENE_MERGE 1
#define ID_MENU_SCENE_SAVE 2
#define ID_MENU_SCENE_SAVE_WITH_TEXTURES 3
#define ID_MENU_SCENE_SAVE_IMAGE 4
#define ID_MENU_SCENE_SEPARATOR_1 5
#define ID_MENU_SCENE_LOAD_PRESET 6
#define ID_MENU_SCENE_SAVE_PRESET 7
#define ID_MENU_SCENE_SCENE_INFO 8

#define RIGHT_PANEL_WIDTH 379
#define TABS_WIDTH 83
#define MAT_PROPS_MAX 7

class Camera;

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
HBITMAP loadImageMakeTiled(char * filename, int w, int h);
HBITMAP loadImageFromResMakeTiled(HMODULE hMod, WORD idRes, int w, int h);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);

//------------------------------------------------------------------------------------------------

class RM2MatControls
{
public:
	HWND hPrev;
	HWND hName;
	HWND hProps[MAT_PROPS_MAX];
	int numProps;
	RM2MatControls() {	numProps = 0; hPrev = hName = 0;
						for (int i=0; i<MAT_PROPS_MAX; i++)	hProps[i] = 0; }
};

//------------------------------------------------------------------------------------------------

class RendererMain2
{
public:
	HINSTANCE hInstance;
	HBITMAP hBmpBg;
	HBITMAP hDiaph;
	HBITMAP hLogoGradient;
	int bgShiftXRightPanel;
	int bgShiftXTopPanel;
	int bgShiftXRegionsPanel;
	int bgShiftYRegionsPanel;
	HANDLE hStopThreadId;

	static ATOM registerMyClass(HINSTANCE hInstance);
	DECLDIR static bool create(bool asPlugin, HWND hParent, HINSTANCE hInst);
	DECLDIR static RendererMain2 * getInstance();
	DECLDIR HWND getHWND();
	DECLDIR static void setLegacyMode(bool legacy);

	NOXFontManager * fonts;

	int sceneID;
	bool cantloadscene;
	bool showstartgui;

	bool rightPanelOpened;
	bool rightButtonShowing;
	bool regionsPanelOpened;
	RECT vpRect;
	
	HWND hViewport;
	HWND hTabs;
	HWND hButtonMain;
	HWND hButtonRegions;
	HWND hTabRender;
	HWND hTabCamera;
	HWND hTabMats;
	HWND hTabEnv;
	HWND hTabBlend;
	HWND hTabPost;
	HWND hTabCorrection;
	HWND hTabFakeDof;
	HWND hTabEffects;
	HWND hTabOptions;
	HWND hTopPanel;
	HWND hBottomPanel;
	HWND hRegionsPanel;

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcRender(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcCamera(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcMaterials(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcEnvironment(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcBlend(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcPost(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcCorrection(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcFakeDof(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcEffects(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcOptions(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcTopPanel(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcBottomPanel(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK WndProcRegions(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	static DWORD WINAPI RefreshThreadProc(LPVOID lpParameter);
	static DWORD WINAPI CounterRefreshThreadProc(LPVOID lpParameter);
	static DWORD WINAPI LoadSceneThreadProc(LPVOID lpParameter);
	static DWORD WINAPI MergeSceneThreadProc(LPVOID lpParameter);
	static DWORD WINAPI AutoStopThreadProc(LPVOID lpParameter);
	static DWORD WINAPI TextureWatcherThreadProc(LPVOID lpParameter);

	static void stoppedRenderingCallback(void);
	LONG_PTR processResizeMessage(WPARAM wParam, LPARAM lParam);
	void openCloseRightPanel(bool open);
	void openCloseRegionsPanel(bool open);
	void tabsWereChanged();
	void updateBgShiftsPanelRender();
	void updateBgShiftsPanelCamera();
	void updateBgShiftsPanelMats();
	void updateBgShiftsPanelEnv();
	void updateBgShiftsPanelBlend();
	void updateBgShiftsPanelPost();
	void updateBgShiftsPanelCorrection();
	void updateBgShiftsPanelFakeDof();
	void updateBgShiftsPanelEffects();
	void updateBgShiftsPanelOptions();
	void updateBgShiftsPanelTop();
	void updateBgShiftsPanelBottom();
	void updateBgShiftsRegions();

	// render tab
	bool fillRenderTab(Camera * cam);
	void updateTimersFromGUItoRtr();
	void updateTimersFromRtrToGUI();	// gui itself not updated, call fillRenderTab
	bool doAfterRenderingStuff();

	// camera tab
	bool fillCameraTabAll();
	bool fillCameraData(Camera * cam);
	bool changeCamera(int id, bool updListPos);
	bool changeCameraOnThread();
	bool updateCamerasStateOnList();
	bool changeRenderResFromCam(Camera * cam);

	// materials tab
	RM2MatControls * matcontrol;
	int numMatControls;
	bool matControlsCreateMatControls();
	bool matControlsUpdateMats();
	bool matControlsUpdateOneMat(int matnum);
	bool matControlsCreateOneMatProps(int matnum);
	bool matControlsDeleteAll();
	bool matControlsLockUnlockAll(bool enabled);
	bool runMaterialEditor(int matnum);

	// layers tab
	bool fillLayersTabAll();
	bool layers2lockUnlockOnlyUsedLayers(Camera * cam);

	// env tab
	bool fillEnvTabAll();
	bool copySunskyToEnvControl();
	bool updateSunskyAzimuthAltitude();
	bool updateSunskyDateFromGui();

	// post tab
	bool autoRedrawPost;
	int autoRedrawTimer;
	bool fillPostTabAll(ImageModifier * iMod);
	bool fillCorrectionTabAll(ImageModifier * iMod);

	// fakedof tab
	bool fillFakeDOFTabAll(FinalModifier * fMod);
	bool showZDepth();
	bool runShowZDepthThread();
	bool evalDepthMapOnThread();
	bool drawDofRange();
	bool drawDofRangeOnThread();
	bool turnOffDofDephtModes();


	// effects tab
	bool fillEffectsTabAll(Camera * cam, FinalModifier * fMod);

	bool runRedrawFinalThread();
	static DWORD WINAPI RedrawFinalThreadProc(LPVOID lpParameter);

	// options tab
	bool fillOptionsTabAll();

	bool fillPresetsCombo(HWND hCombo);

	//
	void createFirstScene();
	bool loadScene(char * filename);
	bool saveScene(char * filename, bool withTextures, bool silentMode);
	bool saveImage(char * filename);
	bool mergeSceneAsync(char * filename);
	bool mergeScene(char * filename);
	bool loadSceneAsync(char * filename, bool andStartRendering=false);
	bool savePreset(char * filename);
	bool loadPreset(HWND hWnd, char * filename, bool forceUseAll);	// main thread only
	bool loadSceneInNewWindow(char * filename);
	bool callLoadSceneDialog(HWND hWnd, char * folder);
	DECLDIR static bool tryLoadSceneDroppedAsync(char * filename, bool andStartRendering=false);

	bool startRender(bool resume);
	bool stopRender();
	bool refreshRender(RedrawRegion * rgn);
	bool refreshRenderOnThread();
	bool runAutoRefreshThread();
	bool runAutoStopThread();
	bool runTextureWatcherThread();
	void refreshCounterInStatus(bool average);
	void refreshTimeInStatus();
	void showRegionModeInStatus(bool on);
	void showPickModeInStatus(bool on);
	void showRefreshingInStatus(bool show);
	bool fillStatusBar();
	bool udpateRegionPanelModeFromViewport();
	bool showAllPanelsAfterSceneLoaded();


	bool stoppingPassOn;

	struct
	{
		bool refreshOn;
		int refreshSec;
		bool autosaveOn;
		int autosaveMin;
		bool stopOn;
		int stopSec;
	} timers;
	unsigned long long totalPixelSamples;
	RunNextSettings runNext;

	void lockNOXStart();
	void unlockAfterSceneLoaded();
	void unlockAfterSceneLoadFailed();
	void lockOnStartRender();
	void unlockOnStopRender();
	void updateLocksTabRender(bool nowrendering);
	void updateLocksTabCamera(bool nowrendering);
	void updateLocksTabMaterials(bool nowrendering);
	void updateLocksTabEnvironment(bool nowrendering);
	void updateLocksTabLayers(bool nowrendering);
	void updateLocksTabPost(bool nowrendering);
	void updateLocksTabCorrection(bool nowrendering);
	void updateLocksTabFakeDof(bool nowrendering);
	void updateLocksTabEffects(bool nowrendering);
	void updateLocksTabOptions(bool nowrendering);

	struct 
	{
		HWND ht1_welcome;
		HWND ht2_noxname;
		HWND ht3_loadLink;
		HWND ht4_noxScene;
		HWND ht5_openLink;
		HWND ht6_SampleScenes;
		HWND ht7_or;
	} start_texts;


private:
	static RendererMain2 * rmwInstance;
	static bool isWindowClassRegistered;
	HWND hMain;
	RendererMain2();
	~RendererMain2();
};

//------------------------------------------------------------------------------------------------

#endif
