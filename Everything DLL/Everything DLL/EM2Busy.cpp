#include <windows.h>
#include <GdiPlus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;

//---------------------------------------------------------------------

void InitEM2Busy()
{
    WNDCLASSEX wc;
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2Busy";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2BusyProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2Busy *);
    wc.hIconSm        = 0;
    RegisterClassEx(&wc);
}

//---------------------------------------------------------------------

EM2Busy * GetEM2BusyInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2Busy * emcs = (EM2Busy *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2Busy * emcs = (EM2Busy *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcs;
}

//---------------------------------------------------------------------

void SetEM2BusyInstance(HWND hwnd, EM2Busy *emcs)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcs);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcs);
	#endif
}

//---------------------------------------------------------------------

EM2Busy::EM2Busy(HWND hWnd)
{
	hwnd = hWnd;
	bgImage = 0;
	bgShiftX = bgShiftY = 0;

	colNormal = RGB(150,150,150);
	colActive = RGB(200,200,200);
	alphaNormal = 48;
	alphaActive = 72;

	boxSize = 5;
	boxSizeActive = 7;
	boxDist = 4;
	boxLeftMargin = 1;
	boxTopMargin = 1;
	numBoxes = 10;

	current = 1;
	nowAnim = false;
	timer = 200;
	timerID = 0;
}

//---------------------------------------------------------------------

EM2Busy::~EM2Busy()
{
	stopAnim();
}

//---------------------------------------------------------------------

LRESULT CALLBACK EM2BusyProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2Busy* emb = GetEM2BusyInstance(hwnd);
	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			{
				EM2Busy * emb = new EM2Busy(hwnd);
				SetEM2BusyInstance(hwnd, emb);
			}
			break;
		case WM_DESTROY:
			{
				delete emb;
				SetEM2BusyInstance(hwnd, 0);
			}
			break;
		case WM_TIMER:
			{
				if (wParam == 101)
				{
					if (!emb)
						break;
					emb->current = (emb->current+1) % emb->numBoxes;
					InvalidateRect(hwnd, NULL, false);
				}
			}
			break;
		case WM_PAINT:
			{
				if (!emb) 
					break;
				GetClientRect(hwnd, &rect);
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) != 0);

				PAINTSTRUCT ps;
				HDC ohdc = BeginPaint(hwnd, &ps);

				HDC hdc = CreateCompatibleDC(ohdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(hdc, backBMP);

				if (emb->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emb->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emb->bgShiftX, emb->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
				}

				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);
						Gdiplus::SolidBrush brush1(Gdiplus::Color(emb->alphaNormal, GetRValue(emb->colNormal), GetGValue(emb->colNormal), GetBValue(emb->colNormal)));
						Gdiplus::SolidBrush brush2(Gdiplus::Color(emb->alphaActive, GetRValue(emb->colActive), GetGValue(emb->colActive), GetBValue(emb->colActive)));
						int diff = ((int)emb->boxSize - (int)emb->boxSizeActive)/2;
						for (int i=0; i<(int)emb->numBoxes; i++)
						{
							Gdiplus::SolidBrush * br = (i==emb->current) ? &brush2 : &brush1;
							int d = (i==emb->current) ? diff : 0;
							int s = (i==emb->current) ? emb->boxSizeActive : emb->boxSize;
							int x = emb->boxLeftMargin + d + i*(emb->boxSize+emb->boxDist);
							int y = emb->boxTopMargin + d;
							graphics.FillRectangle(br, x, y, s, s);
						}
					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					COLORREF colN = getMixedColor(NGCOL_BG_MEDIUM, emb->alphaNormal, emb->colNormal);
					COLORREF colA = getMixedColor(NGCOL_BG_MEDIUM, emb->alphaActive, emb->colActive);
					HBRUSH hBrNormal = CreateSolidBrush(colN);
					HBRUSH hBrActive = CreateSolidBrush(colA);
					int diff = ((int)emb->boxSize - (int)emb->boxSizeActive)/2;

					for (int i=0; i<(int)emb->numBoxes; i++)
					{
						HBRUSH hBr = (i==emb->current) ? hBrActive : hBrNormal;
						int d = (i==emb->current) ? diff : 0;
						int s = (i==emb->current) ? emb->boxSizeActive : emb->boxSize;
						int x = emb->boxLeftMargin + d + i*(emb->boxSize+emb->boxDist);
						int y = emb->boxTopMargin + d;
						RECT frect = { x, y, x+s, y+s };
						FillRect(hdc, &frect, hBr);
					}
					DeleteObject(hBrNormal);
					DeleteObject(hBrActive);
				#endif

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY);

				SelectObject(hdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(hdc); 

				EndPaint(hwnd, &ps);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//---------------------------------------------------------------------

bool EM2Busy::startAnim()
{
	stopAnim();
	current = -1;
	timerID = SetTimer(hwnd, 101, timer, NULL);
	nowAnim = (timerID!=0);
	InvalidateRect(hwnd, NULL, false);
	return true;
}

//---------------------------------------------------------------------

bool EM2Busy::stopAnim()
{
	if (timerID)
		KillTimer(hwnd, 101);
	timerID = 0;
	current = -1;
	nowAnim = false;
	InvalidateRect(hwnd, NULL, false);
	return true;
}

//---------------------------------------------------------------------
