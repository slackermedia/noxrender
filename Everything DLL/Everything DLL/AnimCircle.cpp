#include <Windows.h>
#include "EM2Controls.h"

LRESULT CALLBACK AnimCircleWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void InitAnimCircleWindowClass();
extern HMODULE hDllModule;
HBITMAP hCircleScreen = 0;
bool circleActive = false;
HWND hCircleParent = 0;

//------------------------------------------------------------------------------------------------

bool openAnimCircleDialog(HWND hParent, int cposx, int cposy)
{
	static bool initialized = false;
	if (!initialized)
	{
		InitAnimCircleWindowClass();
		initialized = true;
	}

	int px = cposx-55/2;
	int py = cposy-55/2;
	circleActive = true;
	ReleaseCapture();
	EnableWindow(hParent, FALSE);
	HWND hAnim = CreateWindowEx(WS_EX_TOPMOST ,
				"AnimCircleWindow", "", WS_VISIBLE | WS_CHILD , px, py, 55, 55, hParent, (HMENU)13240, hDllModule, 0);
	hCircleParent = hParent;

	RECT wRect;
	GetWindowRect(hParent, &wRect);
	POINT pt = {0,0};
	ClientToScreen(hParent, &pt);

	HDC hNoDC = GetDC(NULL);
	HDC thdc = CreateCompatibleDC(hNoDC);
	HBITMAP backBMP = CreateCompatibleBitmap(hNoDC, 55, 55);
	HBITMAP hOldBmp = (HBITMAP)SelectObject(thdc, backBMP);
	BitBlt(thdc, 0,0, 55, 55, hNoDC, px+pt.x, py+pt.y, SRCCOPY);
	SelectObject(thdc, hOldBmp);
	DeleteDC(thdc);
	ReleaseDC(NULL, hNoDC);
	if (hCircleScreen)
		DeleteObject(hCircleScreen);
	hCircleScreen = backBMP;

	return true;
}

//------------------------------------------------------------------------------------------------

void InitAnimCircleWindowClass()
{
	WNDCLASSEX wc;
	wc.cbSize         = sizeof(wc);
	wc.lpszClassName  = "AnimCircleWindow";
	wc.hInstance      = hDllModule;
	wc.lpfnWndProc    = AnimCircleWndProc;
	wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
	wc.hIcon          = 0;
	wc.lpszMenuName   = 0;
	wc.hbrBackground  = (HBRUSH)0;
	wc.style          = 0;
	wc.cbClsExtra     = 0;
	wc.cbWndExtra     = 0;
	wc.hIconSm        = 0;
	RegisterClassEx(&wc);
}

//------------------------------------------------------------------------------------------------

LRESULT CALLBACK AnimCircleWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;

	switch (message)
	{
		case WM_CREATE:
			{
				SetTimer(hWnd, 101, 100, 0);
				SetTimer(hWnd, 102, 10, 0);
			}
			break;
		case WM_CLOSE:
			{
				DestroyWindow(hWnd);
			}
			break;
		case WM_DESTROY:
			{
			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect;

				GetClientRect(hWnd, &rect);
				HDC ohdc = BeginPaint(hWnd, &ps);

				HDC hdc = CreateCompatibleDC(ohdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(hdc, backBMP);

				BLENDFUNCTION blendFunction;
				blendFunction.BlendOp = AC_SRC_OVER;
				blendFunction.BlendFlags = 0;
				blendFunction.SourceConstantAlpha = 255;
				blendFunction.AlphaFormat = AC_SRC_ALPHA;

				HDC thdc1 = CreateCompatibleDC(hdc);			// background buffer hdc
				HBITMAP oldBitmap1 = (HBITMAP)SelectObject(thdc1, hCircleScreen);
				BitBlt(hdc, 0, 0, 55, 55, thdc1, 0, 0, SRCCOPY);
				SelectObject(thdc1, oldBitmap1);
				DeleteDC(thdc1);

				static int i=0;
				i = (i+1)%16;
				EM2Elements * emel = EM2Elements::getInstance();
				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emel->getCircleBitmap());
				AlphaBlend(hdc, 0,0, 55,55, thdc,  i*55,0, 55,55, blendFunction);

				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				// done
				AlphaBlend(ohdc, rect.left, rect.top,  rect.right-rect.left, rect.bottom-rect.top ,hdc,  rect.left, rect.top,  rect.right-rect.left, rect.bottom-rect.top, blendFunction);

				SelectObject(hdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(hdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_TIMER:
			{
				if (wParam == 102)
					if (!circleActive)
					{
						EnableWindow(hCircleParent, TRUE);
						DestroyWindow(hWnd);
					}
				if (wParam == 101)
					InvalidateRect(hWnd, NULL, false);
			}
			break;


		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------
