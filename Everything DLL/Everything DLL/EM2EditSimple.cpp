#include <windows.h>
#include <stdio.h>
#include "EM2Controls.h"
#include "log.h"

extern HMODULE hDllModule;

//---------------------------------------------------------------------

EM2EditSimple::EM2EditSimple(HWND hWnd)
{
	colBackground = NGCOL_BG_MEDIUM;
	colDisabledBackground = NGCOL_BG_MEDIUM;
	colText = NGCOL_LIGHT_GRAY;
	colDisabledText = RGB(160,160,160);
	colDisabledText = NGCOL_TEXT_DISABLED;
	colBorderLeft = RGB(180,100,100);
	colBorderUp = RGB(180,180,100);
	colBorderRight = RGB(100,180,100);
	colBorderDown = RGB(100,180,180);
	colTextMouseOver = NGCOL_YELLOW;
	colTextEdit = NGCOL_LIGHT_GRAY;

	colBackgroundEdit = RGB(100, 100,100);

	hwnd = hWnd;
	hEdit = 0;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;

	hFontMain = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hFontEdit = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfontmain = true;
	autodelfontedit = true;

	isMouseOver = false;
	margin = 4;
	align = ALIGN_LEFT;

	hBrush = CreateSolidBrush(colBackgroundEdit);
	hDisabledBrush = CreateSolidBrush(colDisabledBackground);
}

//---------------------------------------------------------------------

EM2EditSimple::~EM2EditSimple()
{
	if (hBrush)
		DeleteObject(hBrush);
	hBrush = NULL;
	if (hDisabledBrush)
		DeleteObject(hDisabledBrush);
	hDisabledBrush = NULL;

	if  (hFontMain  &&  autodelfontmain)
		DeleteObject(hFontMain);
	hFontMain = 0;
	if  (hFontEdit  &&  autodelfontedit)
		DeleteObject(hFontEdit);
	hFontEdit = 0;

}

//---------------------------------------------------------------------

void InitEM2EditSimple()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2EditSimple";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2EditSimpleProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2EditSimple *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);


    WNDCLASSEX wcp;
    
    wcp.cbSize         = sizeof(wcp);
    wcp.lpszClassName  = "EM2EditPassword";
    wcp.hInstance      = hDllModule;
    wcp.lpfnWndProc    = EM2EditSimpleProc;
    wcp.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wcp.hIcon          = 0;
    wcp.lpszMenuName   = 0;
    wcp.hbrBackground  = (HBRUSH)0;
    wcp.style          = 0;
    wcp.cbClsExtra     = 0;
    wcp.cbWndExtra     = sizeof(EM2EditSimple *);
    wcp.hIconSm        = 0;

    RegisterClassEx(&wcp);

}

//---------------------------------------------------------------------

EM2EditSimple * GetEM2EditSimpleInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2EditSimple * emes = (EM2EditSimple *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2EditSimple * emes = (EM2EditSimple *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emes;
}

//---------------------------------------------------------------------

void SetEM2EditSimpleInstance(HWND hwnd, EM2EditSimple *emes)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emes);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emes);
	#endif
}

//---------------------------------------------------------------------

typedef LRESULT (WINAPI * EDITPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EDITPROC OldEditSimple2Proc;

//---------------------------------------------------------------------

LRESULT CALLBACK EM2EditSimpleEditProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_CHAR:
		//case WM_KEYDOWN:
			{
				if (wParam == VK_RETURN)
				{
					LONG wID;
					wID = GetWindowLong(hwnd, GWL_ID);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_RETURN), (LPARAM)hwnd);
					SendMessage(hwnd, EM_SETSEL, 0, -1);
					return TRUE;
				}
				if (wParam == VK_ESCAPE)
				{
					SendMessage(hwnd, EM_SETSEL, -1, 0);
					return TRUE;
				}
				if (wParam == VK_TAB)
				{
					LONG wID = GetWindowLong(hwnd, GWL_ID);
					if (GetKeyState(VK_SHIFT)&0x8000)
						SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB_SHIFT), (LPARAM)hwnd);
					else
						SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB), (LPARAM)hwnd);
					return TRUE;
				}
			}
			break;
		case WM_KILLFOCUS:
			{
				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_LOST_FOCUS), (LPARAM)hwnd);
			}
			break;
		case WM_SETFOCUS:
			{
				SendMessage(hwnd, EM_SETSEL, 0, -1);
			}
			break;
		case WM_NCCALCSIZE:
			{
				if (lParam)
				{
					NCCALCSIZE_PARAMS * params = (NCCALCSIZE_PARAMS *)lParam;
					RECT trect = params->rgrc[0];
					HDC hdc = GetDC(hwnd);

					EM2EditSimple * emes = GetEM2EditSimpleInstance(GetParent(hwnd));
					HFONT hfont = emes ? emes->getFontEdit() : (HFONT)GetStockObject(DEFAULT_GUI_FONT);
					HFONT hOldFont = (HFONT)SelectObject(hdc, hfont);

					DrawText(hdc, "Ky", 2, &trect, DT_CALCRECT | DT_LEFT);
					
					SelectObject(hdc, hOldFont);
					ReleaseDC(hwnd, hdc);

					int p = ((params->rgrc[0].bottom - params->rgrc[0].top) - (trect.bottom-trect.top));
					if (p>0)
						params->rgrc[0].top += p/2;
					if (p<0)
					{
						params->rgrc[0].bottom += -p;
					}
				}
				return WVR_ALIGNBOTTOM;
			}
			break;
	}
	return CallWindowProc(OldEditSimple2Proc, hwnd, msg, wParam, lParam);
}

//---------------------------------------------------------------------

LRESULT CALLBACK EM2EditSimpleProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2EditSimple * emes = GetEM2EditSimpleInstance(hwnd);
	RECT rect;

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2EditSimple * emes1 = new EM2EditSimple(hwnd);
				SetEM2EditSimpleInstance(hwnd, (EM2EditSimple *)emes1);
				RECT rect;
				GetClientRect(emes1->hwnd, &rect);

				bool isPass = false;
				char cname[256];
				if (GetClassName(hwnd, cname, 256))
				{
					if (!strcmp(cname, "EM2EditPassword"))
						isPass = true;
				}

				char txt[2048];
				txt[0] = 0;
				GetWindowText(hwnd, txt, 2048);

				if (isPass)
					emes1->hEdit = CreateWindow("EDIT", txt, 
								WS_CHILD | WS_TABSTOP | ES_AUTOHSCROLL 
								| ES_PASSWORD | ES_WANTRETURN,
								1, 1, rect.right-2, rect.bottom-2,
								hwnd, (HMENU)0, NULL, 0);
				else
					emes1->hEdit = CreateWindow("EDIT", txt, 
								WS_CHILD | WS_TABSTOP | ES_AUTOHSCROLL 
								| ES_MULTILINE | ES_WANTRETURN,
								1, 1, rect.right-2, rect.bottom-2,
								hwnd, (HMENU)0, NULL, 0);

				SendMessage(emes1->hEdit, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

				#ifdef _WIN64
					OldEditSimple2Proc = (EDITPROC) SetWindowLongPtr(emes1->hEdit, GWLP_WNDPROC, (LONG_PTR)EM2EditSimpleEditProc) ;
				#else
					OldEditSimple2Proc = (EDITPROC) (HANDLE)(LONG_PTR)SetWindowLong(emes1->hEdit, GWL_WNDPROC, (LONG)(LONG_PTR)EM2EditSimpleEditProc) ;
				#endif

				emes1->setAlign(emes1->getAlign());
				SetWindowPos(emes1->hEdit, HWND_TOP, 1, 1, rect.right-2, rect.bottom-2, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			break;
		case WM_ERASEBKGND: 
			return FALSE;
		case WM_SIZE:
			{
				SetWindowPos(emes->hEdit, HWND_TOP, 1, 1, (int)(short)LOWORD(lParam)-2, (int)(short)HIWORD(lParam)-2, SWP_NOZORDER | SWP_FRAMECHANGED);
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_CLOSE:
			{
				return TRUE;
			}
			break;
		case WM_DESTROY:
			{
				EM2EditSimple * emes1;
				emes1 = GetEM2EditSimpleInstance(hwnd);
				delete emes1;
				SetEM2EditSimpleInstance(hwnd, 0);
			}
		break;
		case WM_CTLCOLOREDIT:
		{
			HDC hdc1 = (HDC)wParam;
			DWORD style;
			style = GetWindowLong(hwnd, GWL_STYLE);
			bool disabled = ((style & WS_DISABLED) > 0);
			if (!disabled)
			{
				SetTextColor(hdc1, emes->colTextEdit);
				SetBkColor(hdc1, emes->colBackgroundEdit);
			}
			else
			{
				SetTextColor(hdc1, emes->colDisabledText);
				SetBkColor(hdc1, emes->colDisabledBackground);
			}
			if (disabled)
				return (INT_PTR)(emes->hDisabledBrush);
			else
				return (INT_PTR)(emes->hBrush);
		}
		break;
		case WM_ENABLE:
			{
				if (GetFocus()==emes->hEdit)
					SetFocus(0);
				SendMessage(emes->hEdit, EM_SETREADONLY, wParam?FALSE:TRUE, 0);
			}
			break;
		case WM_PAINT:
		{
			if (!emes) 
				break;

			PAINTSTRUCT ps;
			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// create back buffer
			HDC hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);

			bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

			if (emes->bgImage)
			{
				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP * curBitmap = &emes->bgImage;
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, *curBitmap);
				BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emes->bgShiftX, emes->bgShiftY, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);
				SetBkMode(hdc, TRANSPARENT);
			}
			else
			{
				HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
				HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, hOldBrush));
				DeleteObject(SelectObject(hdc, hOldPen2));
				SetBkMode(hdc, OPAQUE);
			}



			if (emes->hasFocus())	// draw edit
			{
				RECT iRect = { 1, 1, rect.right-1, rect.bottom-1 };
				FillRect(hdc, &iRect, emes->hBrush);
			}
			else	// draw text
			{
				emes->rectText.left = 0;
				emes->rectText.top = 0;
				emes->rectText.right = rect.right;
				emes->rectText.bottom = rect.bottom;
				char * txt = emes->getText();
				if (txt)
				{
					int ll = (int)strlen(txt);
					int posx = 0;
					int posy = 0;

					HFONT hOldFont = (HFONT)SelectObject(hdc, emes->getFontMain());

					SIZE sz;
					GetTextExtentPoint32(hdc, txt, ll, &sz);
					posy = (rect.bottom-rect.top-sz.cy)/2;
					int margin = 4;
					switch (emes->getAlign())
					{
						case EM2EditSimple::ALIGN_LEFT:		posx = margin;	break;
						case EM2EditSimple::ALIGN_CENTER:	posx = (rect.right-rect.left-sz.cx)/2;	break;
						case EM2EditSimple::ALIGN_RIGHT:	posx = rect.right - sz.cx - margin;	break;
					}

					SetTextColor(hdc, disabled ? emes->colDisabledText : (emes->isMouseOver ? emes->colTextMouseOver : emes->colText));
					ExtTextOut(hdc, posx, posy, ETO_CLIPPED, &rect, txt, ll, 0);

					SelectObject(hdc, hOldFont);
				}
			}

			// draw editbox border
			POINT eBorder1[] = {
				{0,   rect.bottom-1 },
				{0,   0},
				{rect.right,   0}
			};

			POINT eBorder2[] = {
				{rect.right-1,   1},
				{rect.right-1,   rect.bottom-1},
				{0, rect.bottom-1}
			};

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);

			InvalidateRect(emes->hEdit, NULL, false);
		}
		break;
		case WM_LBUTTONDOWN:
			{
				POINT pt = { (short)LOWORD(lParam), pt.y = (short)HIWORD(lParam) };
				if (PtInRect(&emes->rectText, pt))
				{
					ShowWindow(emes->hEdit, TRUE);
					SetFocus(emes->hEdit);
					ShowCaret(emes->hEdit);
					ReleaseCapture();
					emes->isMouseOver = false;
				}
			}
			break;
		case WM_MOUSEMOVE:
			{
				GetClientRect(hwnd, &rect);
				POINT pt = { (short)LOWORD(lParam), pt.y = (short)HIWORD(lParam) };
				if (PtInRect(&rect, pt)  &&  !emes->hasFocus())
				{
					if (!GetCapture())
						SetCapture(hwnd);

					if (!emes->isMouseOver)
						InvalidateRect(hwnd, NULL, false);
					emes->isMouseOver = true;
				}
				else
				{
					if (emes->isMouseOver)
						InvalidateRect(hwnd, NULL, false);
					emes->isMouseOver = false;
					ReleaseCapture();
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emes->isMouseOver = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_SETFOCUS:
			{
				ShowWindow(emes->hEdit, TRUE);
				SetFocus(emes->hEdit);
				ShowCaret(emes->hEdit);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				
				LONG editID = GetWindowLong(emes->hEdit, GWL_ID);
				if (wmId == editID  &&   wmEvent == ES_RETURN)
				{
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), ES_RETURN), (LPARAM)hwnd);

					SetFocus(0);
					ShowWindow(emes->hEdit, FALSE);
					InvalidateRect(hwnd, NULL, false);
					break;
				}
				if (wmId == editID  &&   wmEvent == ES_LOST_FOCUS)
				{
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), ES_LOST_FOCUS), (LPARAM)hwnd);
					ShowWindow(emes->hEdit, FALSE);
					InvalidateRect(hwnd, NULL, false);
					break;
				}
				if (wmId == editID  &&   wmEvent == ES_TAB)
				{
					HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, false);
					if (hNext != hwnd)
						SetFocus(hNext);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), ES_LOST_FOCUS), (LPARAM)hwnd);
				}
				if (wmId == editID  &&   wmEvent == ES_TAB_SHIFT)
				{
					HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, true);
					if (hNext != hwnd)
						SetFocus(hNext);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), ES_LOST_FOCUS), (LPARAM)hwnd);
				}

			}
			break;
	
		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//---------------------------------------------------------------------

char * EM2EditSimple::getText()
{
	if (!hEdit)
		return NULL;
	int l = GetWindowTextLength(hEdit);
	char * res = (char *)malloc(l+2);
	int ok = GetWindowText(hEdit, res, l+2);
	if (ok<1)
		return NULL;

	return res;
}

//---------------------------------------------------------------------

bool EM2EditSimple::setText(char * text)
{
	if(!hEdit)
		return false;
	if (!text)
		return false;
	int ok = SetWindowText(hEdit, text);
	if (!ok)
		return false;
	InvalidateRect(hwnd, NULL, false);

	return true;
}

//---------------------------------------------------------------------

bool EM2EditSimple::hasFocus()
{
	if (!hEdit)
		return false;
	return (GetFocus()==hEdit);
}

//---------------------------------------------------------------------

bool EM2EditSimple::setFontMain(HFONT hNewFont, bool autodel)
{
	if (autodelfontmain)
		DeleteObject(hFontMain);
	hFontMain = hNewFont;
	autodelfontmain = autodel;
	return true;
}

//----------------------------------------------------------------------

bool EM2EditSimple::setFontEdit(HFONT hNewFont, bool autodel)
{
	if (autodelfontedit)
		DeleteObject(hFontEdit);
	hFontEdit = hNewFont;
	autodelfontedit = autodel;
	SendMessage(hEdit, WM_SETFONT, (WPARAM)(hNewFont?hNewFont:GetStockObject(DEFAULT_GUI_FONT)), TRUE);
	SetWindowPos(hEdit, HWND_TOP, 0,0,0,0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED);
	return true;
}

//----------------------------------------------------------------------

HFONT EM2EditSimple::getFontMain()
{
	return (hFontMain ? hFontMain : (HFONT)GetStockObject(DEFAULT_GUI_FONT));
	return hFontMain;
}

//----------------------------------------------------------------------

HFONT EM2EditSimple::getFontEdit()
{
	return (hFontEdit ? hFontEdit : (HFONT)GetStockObject(DEFAULT_GUI_FONT));
	return hFontEdit;
}

//----------------------------------------------------------------------

int EM2EditSimple::getAlign()
{
	return align;
}

//----------------------------------------------------------------------

void EM2EditSimple::setAlign(int newalign)
{
	if (newalign == ALIGN_CENTER   ||   newalign == ALIGN_RIGHT)
		align = newalign;
	else
		align = ALIGN_LEFT;

	DWORD style = GetWindowLong(hEdit, GWL_STYLE);
	style &= ~(ES_CENTER|ES_LEFT|ES_RIGHT);
	switch (align)
	{
		case ALIGN_LEFT:	style |= ES_LEFT;	break;
		case ALIGN_CENTER:	style |= ES_CENTER;	break;
		case ALIGN_RIGHT:	style |= ES_RIGHT;	break;
	}
	SetWindowLong(hEdit, GWL_STYLE, style);
}

//----------------------------------------------------------------------

void EM2EditSimple::setEditBgColor(COLORREF newcol)
{
	colBackgroundEdit = newcol;
	if (hBrush)
		DeleteObject(hBrush);
	hBrush = CreateSolidBrush(colBackgroundEdit);
}

//----------------------------------------------------------------------
