#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

//#define DEBUG_STATS
//#define DEBUG_LOG2

int Metropolis::prMutLens       = 200;
int Metropolis::prMutBidir      = 200;
int Metropolis::prMutPertLens   = 200;
int Metropolis::prMutPertCaust  = 200;
int Metropolis::prMutPertChain  = 200;

float Metropolis::plR1 = 30.0f;
float Metropolis::plR2 = 100.0f;
float Metropolis::pcR1 = 1.0f * PI/180.0f;
float Metropolis::pcR2 = 8.0f * PI/180.0f;
float Metropolis::pch1R1 = 3.0f;
float Metropolis::pch1R2 = 7.0f;
float Metropolis::pch2R1 = 0.1f * PI/180.0f;
float Metropolis::pch2R2 = 4.0f * PI/180.0f;

bool Metropolis::discardDirect = true;


//---------------------------------------------------------------------------------------------------------------------------------------------

Metropolis::Metropolis(SceneStatic * st, Camera * camera, bool * working)
{
	ss = st;
	running = working;
	cam = camera;
	numLP = 0;
	numEP = 0;
	kd  = 0;
	kde = 0;
	kdl = 0;
	ka  = 0;
	kae = 0;
	kal = 0;
	lensMutNum = 0;
	for (int i=0; i<MLT_MAX_VERTICES; i++)
	{
		lCnt[i] = 0;
		eCnt[i] = 0;
		nadd[i] = 0;
		ndel[i] = 0;
		KAAL[i] = 0;
		KAAE[i] = 0;
		KDDL[i] = 0;
		KDDE[i] = 0;
		nEdg[2*i] = 0;
		nEdg[2*i+1] = 0;
	}
	accCnt = 0;
	rejCnt = 0;
	rejNonVis = 0;
	rndLightPdf = 0;
	cInd = 0;
	maxCausticRough = 0.05f;

	sourceSun = false;
	sourceSky = false;
	cSourceSun = false;
	cSourceSky = false;
	connectionAttenuation = Color4(1,1,1);
	connectionAttenuationCandidate = Color4(1,1,1);
}

Metropolis::~Metropolis()
{
}

#define LLL 10
#define EEE 10

#define NSEEDS 5000
//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::initializePaths()
{
	sumweights = 0.0f;
	sumprobes = 0;

	for (int i=0; i<MLT_MAX_VERTICES*2; i++)
	{
		lweights[i] = 0;
		lprobes[i] = 0;
	}

	MltSeed * seeds = (MltSeed *)_aligned_malloc(sizeof(MltSeed)*NSEEDS, 16);

	if (!seeds)
		MessageBox(0, "Allocation error, may crash.", "Error", 0);

	for (int i=0; i<NSEEDS; i++)
	{
		new (&(seeds[i])) MltSeed(ss, cam);		/// constructor on manual allocated space
		float x,y;
		cam->getHaltonCoords(x, y);
		seeds[i].createSeed(x, y, 1);
		
		int a = seeds[i].numE + seeds[i].numL;
		lprobes[a]++;
		lweights[a] += seeds[i].weight;

		sumweights += seeds[i].weight;
	}

	float a = getMarsaglia()/(float)(unsigned int)(MAXMARS);
	a *= (float)sumweights;

	int ch = 0;
	while (seeds[ch].weight < a   &&   ch<NSEEDS)
	{
		a -= seeds[ch].weight;
		ch++;
	}

	char bbb[256];
	sprintf_s(bbb, 256, "ch=%d   sumweights=%f", ch, sumweights);
	Logger::add(bbb);


	for (int i=0; i<=seeds[ch].numE; i++)
		ePath[i] = seeds[ch].ePath[i];
	for (int i=0; i<=seeds[ch].numL; i++)
		lPath[i] = seeds[ch].lPath[i];
	numLP  = seeds[ch].numL;
	numEP  = seeds[ch].numE;
	lTriID = seeds[ch].lightTriID;

	sourceSky = seeds[ch].useSky;
	sourceSun = seeds[ch].useSun;
	connectionAttenuation = seeds[ch].connAtt;


	weightBias = (float)(sumweights/(float)NSEEDS);
	sumProbes = 0;

	sumprobes = NSEEDS;

	float weightBias2 = 0;
	for (int i=0; i<MLT_MAX_VERTICES*2; i++)
	{
		if (lprobes[i]>0)
			weightBias2 += (float)(lweights[i]/lprobes[i]);
	}

	float weightBias3 = 0;
	long long maxprobes = 0;
	for (int i=0; i<MLT_MAX_VERTICES*2; i++)
		maxprobes = max(maxprobes, lprobes[i]);
	weightBias3 = (float)(sumweights / (float)maxprobes);


	_aligned_free(seeds);

	return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::createInitialPaths()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	Point3d eyeOrigin = cam->position;
	Vector3d eyeDirection = cam->direction;

	float x,y;
	cam->getHaltonCoords(x, y);
	eyeDirection = cam->getDirection(x,y, eyeOrigin, 0);
	numLP = 0;
	numEP = 0;

	// EYE PATH
	ePath[0].pos    = eyeOrigin;
	ePath[0].normal = eyeDirection;
	ePath[0].inDir  = eyeDirection;
	ePath[0].outDir = eyeDirection;
	ePath[0].inCosine  = 1;
	ePath[0].outCosine = 1;
	ePath[0].tri = NULL;
	ePath[0].u = 0;	
	ePath[0].v = 0;
	ePath[0].mat = NULL;
	ePath[0].mlay = NULL;
	ePath[0].inRadiance = Color4(1,1,1);
	alphaE[0] = Color4(1,1,1);
	alphaE[1] = Color4(1,1,1);

	bool eOK = createNextVertices(eyeOrigin, eyeDirection, false, EEE, true);
	
	// LIGHT PATH
	float u,v;
	Triangle * ltri = NULL;
	MaterialNox * mat = NULL;
	MatLayer * mlay = NULL;

	bool gotLight = randomNewLightPoint(&ltri, &mat, &mlay, lPath[0].instanceID, lPath[0].inst_mat, u, v, rndLightPdf);
	if (!gotLight)
		return false;

	lTriID = clTriID;	// it changed candidate

	Vector3d normal = (lPath[0].inst_mat.getMatrixForNormals() * ltri->evalNormal(u,v)).getNormalized();
	float tu = ltri->UV1.x * (1-u-v) + ltri->UV2.x * u + ltri->UV3.x * v;
	float tv = ltri->UV1.y * (1-u-v) + ltri->UV2.y * u + ltri->UV3.y * v;
	lPath[0].pos = lPath[0].inst_mat * ltri->getPointFromUV(u,v);

	lPath[0].normal		= normal;
	lPath[0].inDir		= normal;
	lPath[0].outDir		= normal;
	lPath[0].inCosine	= 1.0f;
	lPath[0].outCosine	= 1.0f;
	lPath[0].mat		= mat;
	lPath[0].mlay		= mlay;
	lPath[0].tri		= ltri;
	lPath[0].u			= u;
	lPath[0].v			= v;

	HitData hd;
	hd.in = lPath[0].normal;
	hd.normal_shade = lPath[0].normal;
	hd.normal_geom = (lPath[0].inst_mat.getMatrixForNormals() * (ltri ? ltri->normal_geom : normal)).getNormalized();
	if (lPath[0].tri)
	{
		lPath[0].tri->evalTexUV(lPath[0].u, lPath[0].v, hd.tU, hd.tV);
		lPath[0].tri->getTangentSpaceSmooth(lPath[0].inst_mat, lPath[0].u, lPath[0].v, hd.dir_U, hd.dir_V);
	}
	else
		hd.tU = hd.tV = 0;

	hd.clearFlagsAll();
	hd.adjustNormal();
	normal = hd.normal_shade;

	float pdf_choose_light_dir = 0;
	Vector3d newdir = lPath[0].mlay->randomNewDirectionLambert2(hd, pdf_choose_light_dir);
	Point3d newpos = lPath[0].pos + lPath[0].normal * 0.01f;								

	lPath[0].outDir = newdir;																
	lPath[0].outCosine = newdir * lPath[0].normal;											
	lPath[0].outCosine = max(0, lPath[0].outCosine);

	alphaL[0] = lPath[0].mlay->gColor * lPath[0].mlay->power * lPath[0].tri->getArea();		
	alphaL[1] = alphaL[0] * (lPath[0].outCosine/pdf_choose_light_dir);

	bool lOK = createNextVertices(newpos, newdir, true, LLL, true);							

	return (eOK && lOK);
}

bool Metropolis::createNextCandidateVertices(const Vector3d *dir, const bool &isItLight, const int &steps, const bool &stopRandom)
{
	// dir may be null if we want brdf to random direction
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	MLTVertex * curpath;
	MLTVertex * nextpath;

	int cur = isItLight ? numCLP : numCEP;						
	MLTVertex * chosenPath = isItLight ? lCandidate : eCandidate;
	Color4 * chosenAlpha = isItLight ? alphaCL : alphaCE;		

	// MAIN LOOP -------------
	int end = cur+steps;
	int start = cur;
	while (cur < end)
	{
		#ifdef DEBUG_LOG2
			Logger::add("1");
		#endif
		
		curpath  = &(chosenPath[cur]);
		nextpath = &(chosenPath[cur+1]);

		HitData hd;
		hd.in = curpath->inDir;
		hd.normal_shade = curpath->normal;
		if (curpath->tri)
		{
			curpath->tri->evalTexUV(curpath->u, curpath->v, hd.tU, hd.tV);
			hd.normal_geom = (curpath->inst_mat.getMatrixForNormals() * curpath->tri->normal_geom).getNormalized();
			curpath->tri->getTangentSpaceSmooth(curpath->inst_mat, curpath->u, curpath->v, hd.dir_U, hd.dir_V);
		}
		else
		{
			hd.tU = hd.tV = 0;
			hd.normal_geom = curpath->normal;
		}

		hd.adjustNormal();	// done

		#ifdef DEBUG_LOG2
			Logger::add("2");
		#endif

		if (cur==0)
		{
			if (isItLight)
			{
				curpath->mat = scene->mats[scene->instances[curpath->instanceID].materials[curpath->tri->matInInst]];
				float elp, ldp;
				curpath->mlay = curpath->mat->getRandomEmissionLayer(hd.tU, hd.tV, elp);

				curpath->outDir = curpath->mlay->randomNewDirectionLambert2(hd, ldp);
				if (ldp < 0.001f)
					return false;
				curpath->outDir.normalize();
				curpath->outCosine = curpath->outDir * curpath->normal;
				curpath->brdf = curpath->mlay->gColor * curpath->mlay->power;
			}
			else
			{
				curpath->mlay = NULL;
				curpath->tri = NULL;
				curpath->mat = NULL;
				curpath->outDir.normalize();
				curpath->normal = curpath->outDir;
				curpath->inDir = curpath->outDir;
				curpath->outCosine = curpath->inCosine = 1; 
				curpath->brdf = Color4(1,1,1);
			}
		}
		else
		{
			if (!curpath->tri)
				return false;

			curpath->mat = scene->mats[scene->instances[curpath->instanceID].materials[curpath->tri->matInInst]];
			float mlp;
			curpath->mlay = curpath->mat->getRandomShadeLayer(hd.tU, hd.tV, mlp);
			if (!curpath->mlay   ||   mlp < 0.001f)
				return false;
			MatLayer * mlay = curpath->mlay;
			float p;

			if (mlay->getRough(hd.tU, hd.tV)== 0)
				tempwasspecular = true;

			if (cur==start   &&   dir)
			{
				hd.out = curpath->outDir = *dir;
				p = mlay->getProbability(hd);
			}
			else
			{
				hd.clearFlagInvalidRandom();
				curpath->outDir = mlay->randomNewDirection(hd, p);
				if (hd.isFlagInvalidRandom())
					return false;
				curpath->outDir.normalize();
				hd.out = curpath->outDir;
			}

			if (p < 0.001f)
				return false;

			curpath->outCosine = fabs(curpath->normal * curpath->outDir);
			curpath->brdf = curpath->mlay->getBRDF(hd);
			if (curpath->brdf.isBlack())
			{
				#ifdef DEBUG_LOG2
					Logger::add("candidate brdf is black");
				#endif
				return false;
			}
		}
		
		Point3d cPoint;
		if (curpath->outDir * curpath->normal > 0)
			cPoint = curpath->pos + curpath->normal * 0.01f;
		else
			cPoint = curpath->pos + curpath->normal * -0.01f;

		// send a ray
		float u=0, v=0;
		int chosen = -1;
		float bigf = BIGFLOAT;
		float prLay;
		float d = ss->bvh.intersectForTrisNonSSE(sample_time, cPoint, curpath->outDir, nextpath->inst_mat, bigf, nextpath->instanceID, chosen, u,v);
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.001f)
			return false;

		Triangle * tri = &(scene->triangles[chosen]);

		// fill path vertex
		nextpath->pos		= nextpath->inst_mat * tri->getPointFromUV(u,v);
		nextpath->normal	= (nextpath->inst_mat.getMatrixForNormals() * tri->evalNormal(u,v)).getNormalized();
		nextpath->inDir		= curpath->outDir * -1;
		nextpath->inCosine	= fabs(nextpath->inDir * nextpath->normal);	

		nextpath->u			= u;
		nextpath->v			= v;
		nextpath->tri		= tri;
		nextpath->mlay		= NULL;
		nextpath->outCosine	= 1;
		nextpath->outDir	= nextpath->normal;
		
		nextpath->mat  = scene->mats[scene->instances[nextpath->instanceID].materials[tri->matInInst]];
		if (!nextpath->mat)
			return false;

		if (nextpath->mat->hasShade)
			nextpath->mlay = nextpath->mat->getRandomShadeLayer(u,v,prLay);	
		else
			return false;
		if (!nextpath->mlay)
			return false;

		if (isItLight)
			numCLP++;
		else
			numCEP++;
		cur++;
	}
	
	return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::createNextVertices(const Point3d &orig, const Vector3d &dir, const bool &isItLight, const int &steps, const bool &stopRandom)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	MLTVertex * prevpath;
	MLTVertex * curpath;
	MLTVertex * nextpath;
	Color4 * alpha;
	Color4 * lastalpha;
	Color4 * nextalpha;

	int cur = isItLight ? numLP : numEP;
	MLTVertex * chosenPath = isItLight ? lPath : ePath;	
	Color4 * chosenAlpha = isItLight ? alphaL : alphaE;	
	Point3d cPoint = orig;
	Vector3d cDir = dir;

	// MAIN LOOP
	int end = cur+steps;
	bool first = true;
	while (cur < end)
	{
		if (isItLight)
			first = false;
		float pcontinue = 1.0f;
		if (stopRandom)
		{
			int stop = Raytracer::getInstance()->getRandomIntThread(10000)%2;
			if (stop == 0)
				if (!first)
					return true;
			pcontinue = 0.8f;
		}

		cur++;

		prevpath = &(chosenPath[cur-1]);
		curpath  = &(chosenPath[cur]);
		nextpath = &(chosenPath[cur+1]);
		lastalpha = &(chosenAlpha[cur-1]);
		alpha     = &(chosenAlpha[cur]);
		nextalpha = &(chosenAlpha[cur+1]);

		// send a ray
		float u=0, v=0;
		int chosen = -1;
		float bigf = BIGFLOAT;
		float prLay;
		float d = ss->bvh.intersectForTrisNonSSE(sample_time, cPoint, cDir, curpath->inst_mat, bigf, curpath->instanceID, chosen, u,v);
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.001f)
			if (first)
				return false;
			else
				return true;

		Triangle * tri = &(scene->triangles[chosen]);

		curpath->pos		= curpath->inst_mat * tri->getPointFromUV(u,v);	
		curpath->normal		= curpath->inst_mat.getMatrixForNormals() * tri->evalNormal(u,v);
		curpath->normal.normalize();														
		curpath->inDir		= cDir * -1;													
		curpath->inCosine	= fabs(curpath->inDir * curpath->normal);						
		curpath->u			= u;
		curpath->v			= v;
		curpath->tri		= tri;
		curpath->mlay		= NULL;
		curpath->outCosine	= 1;
		curpath->outDir		= curpath->normal;

		curpath->mat  = scene->mats[scene->instances[curpath->instanceID].materials[tri->matInInst]];
		if (!curpath->mat)
			return false;

		float tu, tv;
		tri->evalTexUV(u,v, tu, tv);
		curpath->mlay = curpath->mat->getRandomShadeLayer(tu,tv,prLay);	
		if (!curpath->mlay)
			return false;

		HitData hd;
		hd.tU = tu;
		hd.tV = tv;
		hd.in = curpath->inDir;
		hd.normal_shade = curpath->normal;
		hd.normal_geom = (curpath->inst_mat.getMatrixForNormals() * tri->normal_geom).getNormalized();
		hd.adjustNormal();	
		curpath->tri->getTangentSpaceSmooth(curpath->inst_mat, curpath->u, curpath->v, hd.dir_U, hd.dir_V);

		float dirprob;
		hd.clearFlagInvalidRandom();
		Vector3d newdir = curpath->mlay->randomNewDirection(hd, dirprob);
		if (hd.isFlagInvalidRandom())
			return false;
		newdir.normalize();
		if (dirprob < 0.001f)
			return false;
		curpath->outDir = newdir;
		curpath->outCosine = fabs(curpath->outDir * curpath->normal);
		Color4 brdf = curpath->mlay->getBRDF(hd);
		*nextalpha = *alpha * brdf * (curpath->outCosine / dirprob);

		if (curpath->outDir * curpath->normal > 0)
			cPoint = curpath->pos + curpath->normal * 0.01f;
		else
			cPoint = curpath->pos + curpath->normal * -0.01f;
		cDir = curpath->outDir;

		if (isItLight)
			numLP++;
		else
			numEP++;

		first = false;
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

void Metropolis::recalculateContributions(int lStart, int lEnd, int eStart, int eEnd)	// not used?
{
	int l,e;
	int temp;
	Point3d p1, p2;
	Vector3d dir1, dir2;
	float dist, d;
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	for (l=lStart; l<=lEnd; l++)		// for each light path vertex
	{
		for (e=eStart; e<=eEnd; e++)	// for each eye path vertex
		{
			p1 = lPath[l].pos;
			p2 = ePath[e].pos;
			dir1 = p2-p1;
			dist = dir1.normalize_return_old_length();
			dir2 = -dir1;

			Vector3d normal = lPath[l].normal;
			Point3d iPoint = p1;
			float ttu = 0; float ttv = 0;
			float mdist = dist - 0.2f;

			Matrix4d tempmatrix;
			if (dir1 * normal > 0)
				d = ss->bvh.intersectForTrisNonSSE(sample_time, iPoint + normal*0.01f, dir1, tempmatrix, mdist, temp, temp, ttu,ttv);
			else
				d = ss->bvh.intersectForTrisNonSSE(sample_time, iPoint + normal*-0.01f, dir1, tempmatrix, mdist, temp, temp, ttu,ttv);

			if (d > 0)
			{	// shadowed
				uC[l][e] = Color4(0,0,0);
				w [l][e] = 0;
			}
			else
			{
				float k = fabs(lPath[l].normal * dir1) * (ePath[e].normal * dir1);
				if (e > 0)
					k = fabs(k);
				k = max(k, 0);			

				uC[l][e] = alphaL[l] * alphaE[e];
				uC[l][e] *= k;
				uC[l][e] *= 1/(dist*dist);

				Color4 brdf;
				if (l > 0)
				{	// no brdf on light
					if (!lPath[l].mlay)
						brdf = Color4(0,0,0);
					else
					{
						HitData hd;
						hd.in = lPath[l].inDir;
						hd.normal_shade = lPath[l].normal;
						hd.out = dir1;

						if (lPath[l].tri)
						{
							lPath[l].tri->evalTexUV(lPath[l].u, lPath[l].v, hd.tU, hd.tV);
							hd.normal_geom = lPath[l].tri->normal_geom;
						}
						else
						{
							hd.tU = hd.tV = 0;
							hd.normal_geom = lPath[l].normal;
						}
						hd.adjustNormal();	// done

						brdf = lPath[l].mlay->getBRDF(hd);
					}
					uC[l][e] *= brdf;
				}

				if (e > 0)
				{	// camera is not a triangle
					Point2d tUV1(0,0);
					if (!ePath[e].mlay)
						brdf = Color4(0,0,0);
					else
					{
						HitData hd;
						hd.in = ePath[e].inDir;
						hd.normal_shade = ePath[e].normal;
						hd.out = dir2;

						if (ePath[e].tri)
						{
							ePath[e].tri->evalTexUV(ePath[e].u, ePath[e].v, hd.tU, hd.tV);
							hd.normal_geom = ePath[e].tri->normal_geom;
							hd.adjustNormal();
						}
						else
							hd.tU = hd.tV = 0;
						brdf = ePath[e].mlay->getBRDF(hd);
					}
					uC[l][e] *= brdf;
				}
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------------

void Metropolis::recalculateWeights(int lStart, int lEnd, int eStart, int eEnd)	// not used
{
	int i,j,kk,ii,jj;
	float p[MLT_MAX_VERTICES*2];
	float sp;

	float pLE[MLT_MAX_VERTICES];
	float pEL[MLT_MAX_VERTICES];

	MLTVertex * ncur;
	MLTVertex * nnext;
	Point3d pprev, pcur, pnext;
	Vector3d in;
	Vector3d out;
	float d1, d2;

	for (ii=lStart; ii<=lEnd; ii++)			// for each light path vertex
	{
		for (jj=eStart; jj<=eEnd; jj++)		// for each eye path vertex
		{
			w[ii][jj] = 1;
			kk = ii + jj + 1;

			// LIGHT TO EYE PATH
			pLE[0] = 1;
			HitData hd;
			hd.normal_shade = lPath[0].normal;
			hd.in = lPath[0].normal;
			hd.out = lPath[0].outDir;
			if (lPath[0].tri)
			{
				lPath[0].tri->evalTexUV(lPath[0].u, lPath[0].v, hd.tU, hd.tV);
				hd.normal_geom = lPath[0].tri->normal_geom;
				hd.adjustNormal();	// done
			}
			else
				hd.tU = hd.tV = 0;


			pLE[1] = lPath[0].mlay->getProbabilityLambert(hd);
			d2 = (lPath[1].pos-lPath[0].pos).lengthSqr();
			pLE[1] *= lPath[0].outCosine;

			for (i=1; i<kk; i++)
			{
				if (i < ii+2)
					pprev = lPath[i-1].pos;
				else
					pprev = ePath[kk-i+1].pos;
				
				if (i < ii+1)
				{
					ncur = &(lPath[i]);
					pcur = lPath[i].pos;
				}
				else
				{
					ncur = &(ePath[kk-i]);
					pcur = ePath[kk-i].pos;
				}

				if (i < ii)	
				{
					nnext = &(lPath[i+1]);
					pnext = lPath[i+1].pos;
				}
				else
				{
					nnext = &(ePath[kk-i-1]);
					pnext = ePath[kk-i-1].pos;
				}


				in = pprev - pcur;
				d1  = in.normalize_return_old_length();
				out = pnext - pcur;
				d2  = out.normalize_return_old_length();

				hd.in = in;
				hd.out = out;
				hd.normal_shade = ncur->normal;
				hd.tU = hd.tV = 0;
				if (ncur->tri)
				{
					ncur->tri->evalTexUV(ncur->u, ncur->v, hd.tU, hd.tV);
					hd.normal_geom = ncur->tri->normal_geom;
					hd.adjustNormal();
				}

				pLE[i+1] = ncur->mlay->getProbability(hd);
				pLE[i+1] *= max(0, -nnext->normal * out);
			}


			// EYE TO LIGHT PATH
			pEL[0] = 1;
			pEL[1] = 1;
			d2 = (ePath[1].pos - ePath[0].pos).lengthSqr();

			for (i=1; i<kk; i++)				
			{
				if (i < jj+2)					
					pprev = ePath[i-1].pos;
				else
					pprev = lPath[kk-i+1].pos;

				if (i < jj+1)					
				{
					ncur = &(ePath[i]);
					pcur = ePath[i].pos;
				}
				else
				{
					ncur = &(lPath[kk-i]);
					pcur = lPath[kk-i].pos;
				}

				if (i < jj)						
				{
					nnext = &(ePath[i+1]);
					pnext = ePath[i+1].pos;
				}
				else
				{
					nnext = &(lPath[kk-i-1]);
					pnext = lPath[kk-i-1].pos;
				}


				in = pprev - pcur;							
				d1  = in.normalize_return_old_length();

				out = pnext - pcur;							
				d2  = out.normalize_return_old_length();

				hd.in = in;									
				hd.out = out;
				hd.normal_shade = ncur->normal;
				hd.tU = hd.tV = 0;
				if (ncur->tri)
				{
					ncur->tri->evalTexUV(ncur->u, ncur->v, hd.tU, hd.tV);
					hd.normal_geom = ncur->tri->normal_geom;
					hd.adjustNormal();
				}

				pEL[i+1] = ncur->mlay->getProbability(hd);	
				pEL[i+1] *= max(0, -nnext->normal * out);	
			}

			sp = 0;
			for (i=0; i<kk; i++)	// enumerate all paths to find its probabilities
			{
				p[i] = 1;
				for (j=0; j<kk; j++)
				{
					if (j < i)
						p[i] *= pLE[j];
					else
						p[i] *= pEL[kk-j+1];
				}
				sp += p[i] * p[i];
			}
			w[ii][jj] = p[ii]*p[ii]/sp;
			if (sp<0.0001f)
				w[ii][jj] = 0;
			if (p[ii] < 0.00001f)
				w[ii][jj] = 0;
			if (w[ii][jj] > 100)
				w[ii][jj] = 0;
			if (w[ii][jj] < 0)
				w[ii][jj] = 0;
			w[00][jj] = 1;
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::mutateRandom()
{
	return true;
}

//--------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::testMutateRandomWholeNewPaths()
{
	bool ok = createInitialPaths();
			for (int i=0; i<=numEP; i++)
				eCandidate[i] = ePath[i];
			for (int i=0; i<=numLP; i++)
				lCandidate[i] = lPath[i];
			numCLP = numLP;
			numCEP = numEP;
	return ok;
}

//--------------------------------------------------------------------------------------------------------------------------------------

Color4 Metropolis::calculateContribution()
{
	Color4 lC = Color4(1,1,1);
	if (lPath[0].mlay == 0)
	{
		if (!ss->useSunSky)
			return Color4(0,0,0);
		if (sourceSun)
		{
			lC = ss->sunsky->getSunColor();
		}
		else
		{
			Vector3d skyDir;
			if (numLP > 0)
				skyDir = lPath[1].inDir;
			else
				skyDir = ePath[numEP].outDir;
			lC = ss->sunsky->getColor(skyDir);
		}
	}
	else
	{
		Vector3d pvec1 = ePath[numEP].pos - lPath[numLP].pos;
		pvec1.normalize();
		lPath[numLP].outDir = pvec1;
		lPath[numLP].outCosine = fabs(lPath[numLP].outDir * lPath[numLP].normal);
		pvec1.invert();
		ePath[numEP].outDir = pvec1;
		ePath[numEP].outCosine = fabs(ePath[numEP].outDir * ePath[numEP].normal);

		float tlu, tlv;
		lPath[0].tri->evalTexUV(lPath[0].u, lPath[0].v, tlu, tlv);
		lC = lPath[0].mlay->getLightColor(tlu, tlv) * lPath[0].mlay->getIlluminance(ss, lTriID) * max(0,lPath[0].outDir * lPath[0].normal);
	}

	Color4 eC = Color4(1,1,1);

	for (int i=1; i<=numLP; i++)
	{	
		Vector3d pvec = lPath[i-1].pos - lPath[i].pos;
		float d2 = pvec.lengthSqr();
		lPath[i].outCosine = fabs(lPath[i].outDir * lPath[i].normal);
		lC *= lPath[i].outCosine;
		HitData hd;
		hd.in = lPath[i].inDir;
		hd.out = lPath[i].outDir;
		hd.normal_shade = lPath[i].normal;
		hd.lastDist = sqrt(d2);
		if (lPath[i].tri)
		{
			lPath[i].tri->evalTexUV(lPath[i].u, lPath[i].v, hd.tU, hd.tV);
			hd.normal_geom = (lPath[i].inst_mat.getMatrixForNormals() * lPath[i].tri->normal_geom).getNormalized();
			hd.adjustNormal();	// done
			lPath[i].tri->getTangentSpaceSmooth(lPath[i].inst_mat, lPath[i].u, lPath[i].v, hd.dir_U, hd.dir_V);
		}
		else
			hd.tU = hd.tV = 0;

		Color4 brdf = lPath[i].mlay->getBRDF(hd);
		if (brdf.isBlack())
			return brdf;
		lC *= brdf;
		lC *= lPath[i].atten;
	}


	for (int i=1; i<=numEP; i++)
	{
		if (!ePath[i].mlay)
			return Color4(0,0,0);
		Vector3d pvec = ePath[i-1].pos - ePath[i].pos;
		float d2 = pvec.lengthSqr();
		ePath[i].outCosine = fabs(ePath[i].outDir * ePath[i].normal);
		eC *= fabs(ePath[i].outCosine);

		HitData hd;
		hd.in = ePath[i].inDir;
		hd.out = ePath[i].outDir;
		hd.normal_shade = ePath[i].normal;
		hd.lastDist = sqrt(d2);
		if (ePath[i].tri)
		{
			ePath[i].tri->evalTexUV(ePath[i].u, ePath[i].v, hd.tU, hd.tV);
			hd.normal_geom = (ePath[i].inst_mat.getMatrixForNormals() * ePath[i].tri->normal_geom).getNormalized();
			hd.adjustNormal();
			ePath[i].tri->getTangentSpaceSmooth(ePath[i].inst_mat, ePath[i].u, ePath[i].v, hd.dir_U, hd.dir_V);
		}
		else
			hd.tU = hd.tV = 0;

		Color4 brdf = ePath[i].mlay->getBRDF(hd);

		if (brdf.isBlack())
			return brdf;
		eC *= brdf;
		eC *= ePath[i].atten;
	}


	Vector3d pvec = ePath[numEP].pos - lPath[numLP].pos;
	float d2 = pvec.lengthSqr();

	return lC * eC * connectionAttenuation * (1.0f/d2);
}

//--------------------------------------------------------------------------------------------------------------------------------------

Color4 Metropolis::visibilityTestFakeGlassAndOpacity(MLTVertex * v1, MLTVertex * v2)
{
	Scene * scPtr = Raytracer::getInstance()->curScenePtr;
	int maxtri = scPtr->triangles.objCount - 1;
	Point3d pt1 = v1->pos;
	Point3d pt2 = v2->pos;
	Vector3d dir12 = pt2 - pt1;
	Vector3d norm = v1->normal;
	Point3d cPoint;
	if (dir12 * norm >= 0)
		cPoint = pt1 + norm * 0.0001f;
	else
		cPoint = pt1 + norm * -0.0001f;

	float visib = 1.0f;
	Color4 attenuation = Color4(1,1,1);
	bool shadow = true;
	do 
	{
		Vector3d tempDirToPt2 = pt2 - cPoint;
		float ldist = tempDirToPt2.normalize_return_old_length();
		ldist -= 0.0002f;

		int chosen = -1, inst_id=-1;
		float u,v;
		Matrix4d tmatr;
		float nd = ss->bvh.intersectForTrisNonSSE(sample_time, cPoint, tempDirToPt2, tmatr, ldist, inst_id, chosen, u,v);
		if (chosen < 0   ||   chosen > maxtri    ||   nd < 0)
			return attenuation;
		else
		{	// hit sth - check material
			Triangle * ftri = &(scPtr->triangles[chosen]);
			MaterialNox * fmat = scPtr->mats[scPtr->instances[inst_id].materials[ftri->matInInst]];
			HitData fhd;
			ftri->evalTexUV(u,v, fhd.tU, fhd.tV);
			float opac = fmat->getOpacity(fhd.tU, fhd.tV);
			if (!fmat->hasFakeGlass   &&   opac>=1.0f)
				return Color4(0,0,0);

			fhd.clearFlagsAll();
			fhd.in = tempDirToPt2 * -1;
			fhd.out = tempDirToPt2;
			fhd.normal_shade = (tmatr.getMatrixForNormals() * ftri->evalNormal(u, v)).getNormalized();
			fhd.normal_geom = (tmatr.getMatrixForNormals() * ftri->normal_geom).getNormalized();
			fhd.adjustNormal();
			ftri->getTangentSpaceSmooth(tmatr, u,v, fhd.dir_U, fhd.dir_V);

			float opac1;
			Color4 att = fmat->evalAllLayersFakeGlassAndOpacityAttenuation(fhd, opac1);
			if (att.isBlack())
				return Color4(0,0,0);
			attenuation *= att;

			cPoint = tmatr * ftri->getPointFromUV(u,v);
			cPoint = cPoint + fhd.normal_shade * ((tempDirToPt2*fhd.normal_shade>0) ? 0.0001f : -0.0001f);

		}
	} while (shadow);

	return attenuation;
}

//--------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::visibilityTest()
{
	Point3d ptL = lCandidate[numCLP].pos;
	Point3d ptE = eCandidate[numCEP].pos;
	Vector3d dirLP = ptE - ptL;
	float d = dirLP.normalize_return_old_length() - 0.02f;
	Vector3d dirPL = dirLP * -1.0f;

	Vector3d lnorm = lCandidate[numCLP].normal;
	Point3d cPoint;
	if (dirLP * lnorm >= 0)
		cPoint = ptL + lnorm * 0.01f;
	else
		cPoint = ptL + lnorm * -0.01f;

	int chosen = -1, inst_id=-1;
	float u,v;
	Matrix4d tmatr;
	float nd = ss->bvh.intersectForTrisNonSSE(sample_time, cPoint, dirLP, tmatr, d, inst_id, chosen, u,v);
	if (chosen < 0   ||   chosen >= Raytracer::getInstance()->curScenePtr->triangles.objCount   ||   nd < 0)
		return true;
	else
		return false;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::visibilityTest(MLTVertex * v1, MLTVertex * v2)
{
	Point3d pt1 = v1->pos;
	Point3d pt2 = v2->pos;
	Vector3d dir12 = pt2 - pt1;
	float d = dir12.normalize_return_old_length() - 0.0002f;
	Vector3d dir21 = dir12 * -1.0f;

	Vector3d norm = v1->normal;
	Point3d cPoint;
	if (dir12 * norm >= 0)
		cPoint = pt1 + norm * 0.0001f;
	else
		cPoint = pt1 + norm * -0.0001f;

	Vector3d tempDirToLight = pt2 - cPoint;
	float ldist = tempDirToLight.normalize_return_old_length();
	ldist -= 0.0002f;

	int chosen = -1, inst_id;
	float u,v;
	Matrix4d tmatr;
	float nd = ss->bvh.intersectForTrisNonSSE(sample_time, cPoint, dir12, tmatr, ldist, inst_id, chosen, u,v);
	if (chosen < 0   ||   chosen >= Raytracer::getInstance()->curScenePtr->triangles.objCount   ||   nd < 0)
		return true;
	else
		return false;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::randomNewLightPoint(Triangle ** tri, MaterialNox ** mat, MatLayer ** mlay, int &instID, Matrix4d &instMatr, float &u, float &v, float &pdf)
{
	if (!tri  ||  !mat  ||  !mlay)
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	int l,ll;
	float pdf_choose_lightmesh, pdf_choose_light_triangle, pdf_choose_layer;
	ll = ss->randomLightMesh(pdf_choose_lightmesh);
	LightMesh *lm = (*ss->lightMeshes)[ll];											
	if (ll >= 0 && lm)
		l = lm->randomTriangle(pdf_choose_light_triangle);
	else
		return false;

	instID = lm->instNum;
	instMatr = sc->instances[instID].matrix_transform;

	MaterialNox * cmat = NULL;	
	MatLayer * cmlay = NULL;
	Triangle * ltri = &(sc->triangles[l]);
	if (!ltri)
		return false;

	float cu,cv;
	Point3d lpos;
	ltri->randomPoint(cu, cv, lpos);
	lpos = instMatr * lpos;

	float tu = ltri->UV1.x * (1-cu-cv) + ltri->UV2.x * cu + ltri->UV3.x * cv;
	float tv = ltri->UV1.y * (1-cu-cv) + ltri->UV2.y * cu + ltri->UV3.y * cv;

	cmat = sc->mats[sc->instances[instID].materials[ltri->matInInst]];
	if (!cmat)
		return false;
	cmlay = cmat->getRandomEmissionLayer(tu, tv, pdf_choose_layer);
	if (!cmlay)
		return false;

	clTriID = l;
	*tri = ltri;
	*mat = cmat;
	*mlay = cmlay;
	u = cu;
	v = cv;

	return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

void Metropolis::logWholePathDebug(bool candidate)
{
	char buf[8192];
	if (candidate)
		Logger::add("WHOLE CANDIDATE PATH:");
	else
		Logger::add("WHOLE PATH:");
	sprintf_s(buf, 8129, "numLP=%d  numEP=%d    numCLP=%d  numCEP=%d", numLP, numEP, numCLP, numCEP);
	Logger::add(buf);
	
	int nL = candidate ? numCLP : numLP;
	int nE = candidate ? numCEP : numEP;

	Point3d lastpos;
	Vector3d tempdir;
	
	for (int i=0; i<=nL; i++)
	{
		MLTVertex * v = candidate ? &(lCandidate[i]) : &(lPath[i]) ;
		float r = -1;
		if (v->mlay  &&  v->tri)
		{
			float tu,tv;
			v->tri->evalTexUV(v->u, v->v, tu, tv);
			r = v->mlay->getRough(tu,tv);
		}
		//if (v->use_opacity)
		//	r = -2;

		float ddd = 0;
		if (i>0)
			ddd = (v->pos - lastpos).normalize_return_old_length();
		lastpos = v->pos;


		sprintf_s(buf, 8129, "LP%d: \npos:  %f  %f  %f\nin:  %f  %f  %f\nout:  %f  %f  %f\nnormal:  %f  %f  %f\ninCos: %f   outCos: %f\nlastdist: %f\nrough: %f", i,
			v->pos.x, v->pos.y, v->pos.z,
			v->inDir.x, v->inDir.y, v->inDir.z,
			v->outDir.x, v->outDir.y, v->outDir.z,
			v->normal.x, v->normal.y, v->normal.z,
			v->inCosine, v->outCosine, ddd, r
			);
		Logger::add(buf);
	}

	for (int i=nE; i>=0; i--)
	//for (int i=0; i<=nE; i++)
	{
		MLTVertex * v = candidate ? &(eCandidate[i]) : &(ePath[i]);
		float r = -1;
		if (v->mlay  &&  v->tri)
		{
			float tu,tv;
			v->tri->evalTexUV(v->u, v->v, tu, tv);
			r = v->mlay->getRough(tu,tv);
		}

		float ddd = 0;
		//if (i>0)
			ddd = (v->pos - lastpos).normalize_return_old_length();
		lastpos = v->pos;

		sprintf_s(buf, 8129, "EP%d: \npos:  %f  %f  %f\nin:  %f  %f  %f\nout:  %f  %f  %f\nnormal:  %f  %f  %f\ninCos: %f   outCos: %f\nlastdist: %f\nrough: %f", i,
			v->pos.x, v->pos.y, v->pos.z,
			v->inDir.x, v->inDir.y, v->inDir.z,
			v->outDir.x, v->outDir.y, v->outDir.z,
			v->normal.x, v->normal.y, v->normal.z,
			v->inCosine, v->outCosine, ddd, r 
			);
		Logger::add(buf);
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::copySeedToCandidate(MltSeed * seed)
{
	CHECK(seed);

	for (int i=0; i<=seed->numE; i++)
		eCandidate[i] = seed->ePath[i];
	for (int i=0; i<=seed->numL; i++)
		lCandidate[i] = seed->lPath[i];
	numCLP = seed->numL;
	numCEP = seed->numE;
	clTriID = seed->lightTriID;

	cSourceSky = seed->useSky;
	cSourceSun = seed->useSun;
	connectionAttenuationCandidate = seed->connAtt;

	return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

int Metropolis::randomMutationType	()
{
	int lll = Raytracer::getRandomGeneratorForThread()->getRandomInt(100000)%1000;

	if (lll < prMutBidir)
		return MLT_MUT_BIDIR;
	lll -= prMutBidir;
	
	if (lll < prMutPertLens)
		return MLT_MUT_PERT_LENS;
	lll -= prMutPertLens;

	if (lll < prMutLens)
		return MLT_MUT_LENS;
	lll -= prMutLens;

	if (lll < prMutPertCaust)
		return MLT_MUT_PERT_CAUSTIC;
	lll -= prMutPertCaust;

	if (lll < prMutPertChain)
		return MLT_MUT_PERT_CHAIN;
	lll -= prMutPertChain;

	return MLT_MUT_BIDIR;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::isPathValid(bool forward)
{
	bool pvalid = true;
	int nL, nE;
	MLTVertex * eCur;
	MLTVertex * lCur;
	if (forward)
	{
		nL = numCLP;
		nE = numCEP;
		eCur = eCandidate;
		lCur = lCandidate;
	}
	else
	{
		nL = numLP;
		nE = numEP;
		eCur = ePath;
		lCur = lPath;
	}

	// LIGHT PATH
	for (int i=1; i<=nL; i++)
	{
		MLTVertex * v1 = &(lCur[i-1]);
		MLTVertex * v2 = &(lCur[i]);
		MLTVertex * v3 = &(lCur[i+1]);
		if (i==nL)
			v3 = &(eCur[nE]);

		Vector3d vInDir  = v1->pos - v2->pos;
		Vector3d vOutDir = v3->pos - v2->pos;
		float distIn  = vInDir.normalize_return_old_length();
		float distOut = vOutDir.normalize_return_old_length();
		float inC = vInDir*v2->inDir;
		if (inC>1.002  ||  inC <0.998f)
		{
			if (distIn > 0.002)
				pvalid = false;
		}

		float outC = vOutDir*v2->outDir;
		if (outC>1.002  ||  outC <0.998f)
		{
			if (distOut > 0.002)
				pvalid = false;
		}
	}
	
	// EYE PATH
	for (int i=1; i<=nE; i++)
	{
		MLTVertex * v1 = &(eCur[i-1]);
		MLTVertex * v2 = &(eCur[i]);
		MLTVertex * v3 = &(eCur[i+1]);
		if (i==nE)
			v3 = &(lCur[nL]);

		Vector3d vInDir  = v1->pos - v2->pos;
		Vector3d vOutDir = v3->pos - v2->pos;
		float distIn  = vInDir.normalize_return_old_length();
		float distOut = vOutDir.normalize_return_old_length();
		float inC = vInDir*v2->inDir;
		if (inC>1.002  ||  inC <0.998f)
		{
			if (distIn > 0.002)
				pvalid = false;
		}

		float outC = vOutDir*v2->outDir;
		if (outC>1.002  ||  outC <0.998f)
		{
			if (distOut > 0.002)
				pvalid = false;
		}
	}

	return pvalid;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

void Metropolis::printHeckbertPath(bool forward)
{
	int eNum = forward ? numCEP : numEP;
	int lNum = forward ? numCLP : numLP;
	MLTVertex * eCur = forward ? eCandidate : ePath;
	MLTVertex * lCur = forward ? lCandidate : lPath;

	int t=0;
	char buf[128];

	for (int i=0; i<=lNum; i++)
	{
		int inc=1;
		char aaa[8];
		MatLayer * mlay = lCur[i].mlay;
		if (mlay)
		{
			{
				if (mlay->type == MatLayer::TYPE_EMITTER)
					sprintf_s(aaa, 8, "L");
				else
				{
					float tu, tv;
					lCur[i].tri->evalTexUV(lCur[i].u, lCur[i].v, tu, tv);
					float rough = mlay->getRough(tu,tv);
					if (rough>0)
					{
						if (rough > maxCausticRough)
							sprintf_s(aaa, 8, "D");
						else
							sprintf_s(aaa, 8, "G");
					}
					else
						sprintf_s(aaa, 8, "S");
				}
			}

		}
		else
		{
			if (i==0)
			{
				if (forward)
				{
					if (cSourceSun)
						sprintf_s(aaa, 8, "[SUN]");
					if (cSourceSky)
						sprintf_s(aaa, 8, "[SKY]");
				}
				else
				{
					if (sourceSun)
						sprintf_s(aaa, 8, "[SUN]");
					if (sourceSky)
						sprintf_s(aaa, 8, "[SKY]");
				}
				inc = 5;
			}
			else
			{
				sprintf_s(aaa, 8, "[NULL]");
				inc = 6;
			}
		}
		sprintf_s(buf+t, 128-t, "%s ", aaa);
		t+=inc;
	}

	sprintf_s(buf+t, 128-t, " ");
	t++;

	for (int i=eNum; i>=0; i--)
	{
		char aaa[8];
		MatLayer * mlay = eCur[i].mlay;
		if (mlay)
		{
			{
				if (mlay->type == MatLayer::TYPE_EMITTER)
					sprintf_s(aaa, 8, "L");
				else
				{
					float tu, tv;
					eCur[i].tri->evalTexUV(eCur[i].u, eCur[i].v, tu, tv);
					float rough = mlay->getRough(tu,tv);
					if (rough>0)
					{
						if (rough > maxCausticRough)
							sprintf_s(aaa, 8, "D");
						else
							sprintf_s(aaa, 8, "G");
					}
					else
						sprintf_s(aaa, 8, "S");
				}
			}
		}
		else
		{
			if (i==0)
				sprintf_s(aaa, 8, "E");
			else
				sprintf_s(aaa, 8, "0");
		}
		sprintf_s(buf+t, 128-t, "%s ", aaa);
		t++;
	}
	Logger::add(buf);
}

//---------------------------------------------------------------------------------------------------------------------------------------------

void MLTVertex::fillHitData(HitData &hd, bool forward)
{
	if (forward)
	{
		hd.in  = inDir;
		hd.out = outDir;
	}
	else
	{
		hd.in  = outDir;
		hd.out = inDir;
	}
	hd.normal_shade = normal;
	if (tri)
	{
		hd.normal_geom = (inst_mat.getMatrixForNormals() * tri->normal_geom).getNormalized();
		hd.adjustNormal();
		tri->evalTexUV(u,v, hd.tU, hd.tV);
		tri->getTangentSpaceSmooth(inst_mat, u, v, hd.dir_U, hd.dir_V);
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------------

char * Metropolis::getHeckbertPath(bool forward)
{
	int eNum = forward ? numCEP : numEP;
	int lNum = forward ? numCLP : numLP;
	MLTVertex * eCur = forward ? eCandidate : ePath;
	MLTVertex * lCur = forward ? lCandidate : lPath;

	int t=0;
	char buf[128];

	for (int i=0; i<=lNum; i++)
	{
		char aaa[8];
		MatLayer * mlay = lCur[i].mlay;
		if (mlay)
		{
			{
				if (mlay->type == MatLayer::TYPE_EMITTER)
					sprintf_s(aaa, 8, "L");
				else
				{
					float tu, tv;
					lCur[i].tri->evalTexUV(lCur[i].u, lCur[i].v, tu, tv);
					float rough = mlay->getRough(tu,tv);
					if (rough>0)
					{
						if (rough > maxCausticRough)
							sprintf_s(aaa, 8, "D");
						else
							sprintf_s(aaa, 8, "G");
					}
					else
						sprintf_s(aaa, 8, "S");
				}
			}
		}
		else
			sprintf_s(aaa, 8, "0");
		sprintf_s(buf+t, 128-t, "%s ", aaa);
		t++;
	}

	sprintf_s(buf+t, 128-t, " ");
	t++;

	for (int i=eNum; i>=0; i--)
	{
		char aaa[8];
		MatLayer * mlay = eCur[i].mlay;
		if (mlay)
		{
			{
				if (mlay->type == MatLayer::TYPE_EMITTER)
					sprintf_s(aaa, 8, "L");
				else
				{
					float tu, tv;
					eCur[i].tri->evalTexUV(eCur[i].u, eCur[i].v, tu, tv);
					float rough = mlay->getRough(tu,tv);
					if (rough>0)
					{
						if (rough> maxCausticRough)
							sprintf_s(aaa, 8, "D");
						else
							sprintf_s(aaa, 8, "G");
					}
					else
						sprintf_s(aaa, 8, "S");
				}
			}
		}
		else
		{
			if (i==0)
				sprintf_s(aaa, 8, "E");
			else
				sprintf_s(aaa, 8, "0");
		}
		sprintf_s(buf+t, 128-t, "%s ", aaa);
		t++;
	}

	int ll = (int)strlen(buf)+1;
	char * res = (char*)malloc(ll);
	sprintf_s(res, ll, "%s", buf);
	return res;
}

//---------------------------------------------------------------------------------------------------------------------------------------------

bool MLTVertex::fetchRoughness()
{
	if (!tri  ||  !mlay)
		return false;
	float tu=0, tv=0;
	tri->evalTexUV(u,v, tu,tv);
	rough = mlay->getRough(tu,tv);
	return true;
}

//---------------------------------------------------------------------------------------------------------------------------------------------
