#include "raytracer.h"
#include "log.h"

Bucket::Bucket()
{
	tile_size_x = 20;
	tile_size_y = 20;
	res_x = 800;
	res_y = 600;
	tx_min = 0;
	tx_max = 0;
	ty_min = 0;
	ty_max = 0;

	bdir = DIR_UP;
	max_dir_up = -1;
	max_dir_down = 1;
	max_dir_left = -1;
	max_dir_right = 1;
	cbx = 0;
	cby = 1;
	totalBucketsOnImage = 1;
	stopOnLast = true;

	coordsThreads = NULL;
	nThr = 0;

}


bool Bucket::getCoordinates(int tx, int ty, int &x1, int &y1, int &x2, int &y2)
{
	x1 = s0x + tx*tile_size_x;
	y1 = s0y + ty*tile_size_y;
	x2 = x1 + tile_size_x - 1;
	y2 = y1 + tile_size_y - 1;
	if (x2<0 || y2<0)
		return false;
	if (x1>=res_x|| y1>=res_y)
		return false;
	x1 = max(0, x1);
	y1 = max(0, y1);
	x2 = min(res_x-1, x2);
	y2 = min(res_y-1, y2);

	return true;
}

bool Bucket::randomPixel(int tx, int ty, float &x, float &y)
{
	return true;
}

bool Bucket::getNewTile(int &tx, int &ty)
{
	static bool now_used = false;
	while (now_used)
		Sleep(1);
	now_used = true;

	if (curTaken == totalBucketsOnImage)
	{
		if (stopOnLast)
		{
			curTaken = 0;		// counter
			now_used = false;
			return false;
		}

		bdir = DIR_UP;		// direction for change
		max_dir_up = -1;	// limiter up
		max_dir_down = 1;	// limiter down
		max_dir_left = -1;	// limiter left
		max_dir_right = 1;	// limiter right
		cbx = 0;			// current (last) bucket coord x (0,0 at center)
		cby = 1;			// current (last) bucket coord y (0,0 at center)
		curTaken = 0;		// counter
	}

	bool not_chosen = true;
	int tries = 0;
	while (not_chosen)
	{
		switch (bdir)
		{
			case DIR_UP:
				cby--;
				if (cby==max_dir_up)
				{
					max_dir_up = max(max_dir_up-1, ty_min-1);
					bdir = DIR_LEFT;
				}
				break;
			case DIR_LEFT:
				cbx--;
				if (cbx==max_dir_left)
				{
					max_dir_left = max(max_dir_left-1, tx_min-1);
					bdir = DIR_DOWN;
				}
				break;
			case DIR_DOWN:
				cby++;
				if (cby==max_dir_down)
				{
					max_dir_down = min(max_dir_down+1, ty_max+1);
					bdir = DIR_RIGHT;
				}
				break;
			case DIR_RIGHT:
				cbx++;
				if (cbx==max_dir_right)
				{
					max_dir_right = min(max_dir_right+1, tx_max+1);
					bdir = DIR_UP;
				}
				break;
		}
		if (cby>=ty_min  &&  cby<=ty_max  &&  cbx>=tx_min  &&  cbx<=tx_max)
			not_chosen = false;

		if (tries++ > totalBucketsOnImage+20)
		{
			Logger::add("buckets hang");

			if (stopOnLast)
			{
				curTaken = 0;		// counter
				now_used = false;
				return false;
			}

			bdir = DIR_UP;		// direction for change
			max_dir_up = -1;	// limiter up
			max_dir_down = 1;	// limiter down
			max_dir_left = -1;	// limiter left
			max_dir_right = 1;	// limiter right
			cbx = 0;			// current (last) bucket coord x (0,0 at center)
			cby = 0;			// current (last) bucket coord y (0,0 at center)
			curTaken = 0;		// counter
			break;
		}

	}

	curTaken++;
	tx = cbx;
	ty = cby;

	now_used = false;
	return true;
}

bool Bucket::reset(int iw, int ih, int bw, int bh)
{
	int tsx = tile_size_x = max(8, bw);
	int tsy = tile_size_y = max(8, bh);
	if (iw<8 || ih<8)
	{
		return false;
	}
	res_x = iw;
	res_y = ih;

	s0x = (res_x-tsx)/2;
	s0y = (res_y-tsy)/2;
	tx_min = (int)(-ceil(s0x/(float)tsx));
	ty_min = (int)(-ceil(s0y/(float)tsy));
	tx_max = (int)( ceil((iw-s0x)/(float)tsx))-1;
	ty_max = (int)( ceil((ih-s0y)/(float)tsy))-1;

	totalBucketsOnImage = (tx_max-tx_min+1) * (ty_max-ty_min+1);

	bdir = DIR_UP;
	max_dir_up = -1;
	max_dir_down = 1;
	max_dir_left = -1;
	max_dir_right = 1;
	cbx = 0;
	cby = 1;

	return true;
}
