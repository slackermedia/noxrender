#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>

extern HMODULE hDllModule;
#define ROUND(a) (int)floor(a+0.5)

INT_PTR CALLBACK RendererMainWindow::EnvironmentWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				hEnvironment = hWnd;
				Raytracer * rtr = Raytracer::getInstance();

				HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
				SetWindowPos(hEnv, HWND_TOP, 0,0, 362, 182, SWP_NOOWNERZORDER | SWP_NOMOVE);

				EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
				emen->sunsky.copyFrom(rtr->curScenePtr->sscene.sunsky);
				emen->bmEarth = (HBITMAP)LoadImage(hDllModule, 
							MAKEINTRESOURCE(IDB_ENV_EARTH_IMG), IMAGE_BITMAP, 0, 0, 0);

				// try to get timezone shift
				TIME_ZONE_INFORMATION tzi;
				if (GetTimeZoneInformation(&tzi))
					emen->sunsky.setStandardMeridianForTimeZone(tzi.Bias*PI/720);

				// set current date and time for sun sky initialization
				SYSTEMTIME st;
				GetLocalTime(&st);
				emen->sunsky.setDate((int)st.wMonth, (int)st.wDay, (int)st.wHour, (float)(int)st.wMinute);

				
				// generate shadowed earth image
				emen->evalSunPosition();
				emen->evalShadow();

				EMComboBox * emmodel = GetEMComboBoxInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_MODEL));
				emmodel->deleteItems();
				emmodel->addItem("Preetham");
				emmodel->addItem("Hosek-Wilkie");
				emmodel->selected = 0;

				theme.apply(GetEMCheckBoxInstance(    GetDlgItem(hWnd, IDC_REND_ENV_USE_SUNSKY)));
				theme.apply(GetEMEditSpinInstance(    GetDlgItem(hWnd, IDC_REND_ENV_LONGITUDE)));
				theme.apply(GetEMEditSpinInstance(    GetDlgItem(hWnd, IDC_REND_ENV_LATITUDE)));
				theme.apply(GetEMEditSpinInstance(    GetDlgItem(hWnd, IDC_REND_ENV_MONTH)));
				theme.apply(GetEMEditSpinInstance(    GetDlgItem(hWnd, IDC_REND_ENV_DAY)));
				theme.apply(GetEMEditSpinInstance(    GetDlgItem(hWnd, IDC_REND_ENV_HOUR)));
				theme.apply(GetEMEditSpinInstance(    GetDlgItem(hWnd, IDC_REND_ENV_MINUTE)));
				theme.apply(GetEMEditSpinInstance(    GetDlgItem(hWnd, IDC_REND_ENV_GMT)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_TURBIDITY)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_ALBEDO)));
				theme.apply(GetEMColorShowInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_SUNCOLOR)));
				theme.apply(GetEMCheckBoxInstance(    GetDlgItem(hWnd, IDC_REND_ENV_SUN_ON)));
				theme.apply(GetEMCheckBoxInstance(    GetDlgItem(hWnd, IDC_REND_ENV_SUN_OTHER_LAYER)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_SUN_SIZE)));
				theme.apply(GetEMButtonInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_RESET_SUN_SIZE)));
				theme.apply(GetEMComboBoxInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_MODEL)));


				EMCheckBox * emusess =	GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_ENV_USE_SUNSKY));
				EMEditSpin * emlong =	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_LONGITUDE));
				EMEditSpin * emlat =	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_LATITUDE));
				EMEditSpin * emmonth =	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MONTH));
				EMEditSpin * emday = 	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_DAY));
				EMEditSpin * emhour = 	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_HOUR));
				EMEditSpin * emminute =	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MINUTE));
				EMEditSpin * emgmt = 	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_GMT));
				EMColorShow * emcs =    GetEMColorShowInstance(GetDlgItem(hWnd, IDC_REND_ENV_SUNCOLOR));
				EMEditTrackBar * emturb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_TURBIDITY));
				EMEditTrackBar * emalb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_ALBEDO));
				EMEditTrackBar * emsunsize = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_SUN_SIZE));
				emturb->setEditBoxWidth(32);
				emalb->setEditBoxWidth(32);
				emsunsize->floatAfterDot = 4;
				emsunsize->setEditBoxWidth(48);

				EMGroupBar * emgr1 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_GR1));
				EMGroupBar * emgr2 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_GR2));
				EMGroupBar * emgr3 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_GR3));
				EMGroupBar * emgr4 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_ENV_GR4));
				theme.apply(emgr1);
				theme.apply(emgr2);
				theme.apply(emgr3);
				theme.apply(emgr4);


				EMEditSpin * emesMapEV = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MAP_POWER));
				EMEditSpin * emesMapBlendLayer =  GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MAP_LAYER));
				EMEditSpin * emesAzimuth =  GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MAP_AZIMUTH));
				EMCheckBox * emcbMapEnabled = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_ENV_USE_MAP));
				emesMapEV->setValuesToFloat(rtr->curScenePtr->env.powerEV, NOX_ENV_MAP_EV_MIN, NOX_ENV_MAP_EV_MAX, 0.5f, 0.05f);
				emesMapBlendLayer->setValuesToInt(rtr->curScenePtr->env.blendlayer, NOX_ENV_MAP_BLEND_MIN, NOX_ENV_MAP_BLEND_MAX, 1, 0.1f);
				emcbMapEnabled->selected = rtr->curScenePtr->env.enabled;
				emesAzimuth->setValuesToFloat(rtr->curScenePtr->env.azimuth_shift, NOX_ENV_MAP_AZIMUTH_MIN, NOX_ENV_MAP_AZIMUTH_MAX, 5.0f, 0.2f);

				theme.apply(GetEMButtonInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_TEXTURE)));
				theme.apply(GetEMGroupBarInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_GR5)));
				theme.apply(GetEMEditSpinInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_MAP_POWER)));
				theme.apply(GetEMEditSpinInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_MAP_LAYER)));
				theme.apply(GetEMCheckBoxInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_USE_MAP)));
				theme.apply(emesAzimuth);

				// fill sun sky controls from EMEnvironment->sunsky
				fillSunSkyControls();
				Raytracer::getInstance()->curScenePtr->sscene.sunsky->copyFrom(&emen->sunsky);

				// set sun color in control
				Color4 sc = emen->sunsky.getSunColor();
				sc.clamp();
				emcs->color = sc;
				InvalidateRect(emcs->hwnd, NULL, false);
			}
			break;
		case WM_DESTROY:
			{
				HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
				EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
				DeleteObject(emen->bmEarth);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_ENV_MAP:
						{
							if (wmEvent != EN_POSITION_CHANGED)
								break;
							HWND hLong = GetDlgItem(hWnd, IDC_REND_ENV_LONGITUDE);
							EMEditSpin * emeslong = GetEMEditSpinInstance(hLong);
							HWND hLat = GetDlgItem(hWnd, IDC_REND_ENV_LATITUDE);
							EMEditSpin * emeslat = GetEMEditSpinInstance(hLat);
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							if (!emen)
								break;

							emeslong->setValuesToInt(ROUND(-emen->sunsky.getLongitude()*180/PI) , NOX_ENV_LONG_MIN, NOX_ENV_LONG_MAX, 1, 0.2f);
							emeslat->setValuesToInt (ROUND(-emen->sunsky.getLatitude()*180/PI)  , NOX_ENV_LAT_MIN,  NOX_ENV_LAT_MAX,  1, 0.2f);
							changeSunPosAndDateFromControls();
							fillSunSkyControls();
						}
						break;

					case IDC_REND_ENV_LONGITUDE:
						{
							if (wmEvent != WM_VSCROLL)
								break;

							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							changeSunPosAndDateFromControls();
							updateAzimuthAltitude();
							InvalidateRect(hEnv, NULL, false);
						}
						break;
					case IDC_REND_ENV_LATITUDE:
						{
							if (wmEvent != WM_VSCROLL)
								break;

							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							changeSunPosAndDateFromControls();
							updateAzimuthAltitude();							
							InvalidateRect(hEnv, NULL, false);
						}
						break;
					case IDC_REND_ENV_MONTH:
						{
							if (wmEvent != WM_VSCROLL)
								break;

							HWND hMonth = GetDlgItem(hWnd, IDC_REND_ENV_MONTH);
							EMEditSpin * emesmonth = GetEMEditSpinInstance(hMonth);
							HWND hDay= GetDlgItem(hWnd, IDC_REND_ENV_DAY);
							EMEditSpin * emesday = GetEMEditSpinInstance(hDay);

							int maxdays;
							switch (emesmonth->intValue)
							{
								case 1:  maxdays=31; break;
								case 2:  maxdays=28; break;
								case 3:  maxdays=31; break;
								case 4:  maxdays=30; break;
								case 5:  maxdays=31; break;
								case 6:  maxdays=30; break;
								case 7:  maxdays=31; break;
								case 8:  maxdays=31; break;
								case 9:  maxdays=30; break;
								case 10: maxdays=31; break;
								case 11: maxdays=30; break;
								case 12: maxdays=31; break;
								default: maxdays=1; break;
							}

							// number of days in month
							emesday->setValuesToInt(emesday->intValue, 1, maxdays, 1, 0.1f);

							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->evalShadow();

							changeSunPosAndDateFromControls();
							updateAzimuthAltitude();
						}
						break;
					case IDC_REND_ENV_DAY:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							changeSunPosAndDateFromControls();
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->evalShadow();
							updateAzimuthAltitude();
						}
						break;
					case IDC_REND_ENV_HOUR:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							changeSunPosAndDateFromControls();
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->evalShadow();
							updateAzimuthAltitude();
						}
						break;
					case IDC_REND_ENV_MINUTE:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							changeSunPosAndDateFromControls();
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->evalShadow();
							updateAzimuthAltitude();
						}
						break;
					case IDC_REND_ENV_GMT:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							changeSunPosAndDateFromControls();
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->evalShadow();
							updateAzimuthAltitude();
						}
						break;
					case IDC_REND_ENV_TURBIDITY:
						{
							if (wmEvent != WM_HSCROLL)
								break;

							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							HWND hTur = GetDlgItem(hWnd, IDC_REND_ENV_TURBIDITY);
							EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hTur);
							emen->sunsky.setSunDir(emen->sunsky.sunDir, emetb->intValue/10.0f);

							EMColorShow * emcs =    GetEMColorShowInstance(	  GetDlgItem(hWnd, IDC_REND_ENV_SUNCOLOR));
							Color4 sc = emen->sunsky.getSunColor();
							sc.clamp();
							emcs->color = sc;
							InvalidateRect(emcs->hwnd, NULL, false);
						}
						break;
					case IDC_REND_ENV_SUN_SIZE:
						{
							HWND hSize = GetDlgItem(hWnd, IDC_REND_ENV_SUN_SIZE);
							EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hSize);
							float angle = emetb->floatValue;
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunSize = angle;
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->maxCosSunSize = cos(angle/2*PI/180.0f);
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunSizeSteradians = 2*PI*(1-cos(angle/2.0f *PI/180.0f));
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->sunsky.sunSize = emetb->floatValue;
							emen->sunsky.maxCosSunSize = cos(emetb->floatValue/2*PI/180.0f);
						}
						break;
					case IDC_REND_ENV_RESET_SUN_SIZE:
						{
							HWND hSize = GetDlgItem(hWnd, IDC_REND_ENV_SUN_SIZE);
							EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hSize);
							float newval = 32.0f/60.0f;
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunSize = newval;
							emetb->setValuesToFloat(newval, NOX_ENV_SUN_SIZE_MIN,  NOX_ENV_SUN_SIZE_MAX,  0.01f);
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->maxCosSunSize = cos(emetb->floatValue/2*PI/180.0f);
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunSizeSteradians = 2*PI*(1-cos(newval/2.0f *PI/180.0f));
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->sunsky.sunSize = newval;
							emen->sunsky.maxCosSunSize = cos(emetb->floatValue/2*PI/180.0f);
						}
						break;
					case IDC_REND_ENV_SUN_ON:
						{
							HWND hSunON = GetDlgItem(hWnd, IDC_REND_ENV_SUN_ON);
							EMCheckBox * emcb = GetEMCheckBoxInstance(hSunON);
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunOn = emcb->selected;
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->sunsky.sunOn = emcb->selected;
						}
						break;
					case IDC_REND_ENV_SUN_OTHER_LAYER:
						{
							HWND hOtherLayer = GetDlgItem(hWnd, IDC_REND_ENV_SUN_OTHER_LAYER);
							EMCheckBox * emcb = GetEMCheckBoxInstance(hOtherLayer);
							Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunOtherBlendLayer = emcb->selected;
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							emen->sunsky.sunOtherBlendLayer = emcb->selected;
						}
						break;

					case IDC_REND_ENV_ALBEDO:
						{
							if (wmEvent != WM_HSCROLL)
								break;
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							HWND hAe = GetDlgItem(hWnd, IDC_REND_ENV_ALBEDO);
							EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hAe);
							emen->sunsky.albedo = emetb->floatValue;
							emen->sunsky.updateHosekData();
						}
						break;
					case IDC_REND_ENV_MODEL:
						{
							HWND hModel = GetDlgItem(hWnd, IDC_REND_ENV_MODEL);
							EMComboBox * emModel = GetEMComboBoxInstance(hModel);
							CHECK(emModel);
							HWND hEnv = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
							EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
							CHECK(emen);
							if (wmEvent==CBN_SELCHANGE)
							{
								if (emModel->selected==1)
									emen->sunsky.use_hosek = true;
								else
									emen->sunsky.use_hosek = false;
							}
						}
						break;
					case IDC_REND_ENV_USE_SUNSKY:
						{
							HWND hUse = GetDlgItem(hWnd, IDC_REND_ENV_USE_SUNSKY);
							EMCheckBox * emcb = GetEMCheckBoxInstance(hUse);
							Raytracer::getInstance()->curScenePtr->sscene.useSunSky = emcb->selected;
						}
						break;
					case IDC_REND_ENV_TEXTURE:
						{
							CHECK(wmEvent==BN_CLICKED);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(rtr->curScenePtr->env.tex);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = (Texture *)DialogBoxParam(hDllModule, 
								MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								rtr->curScenePtr->env.enabled = texres->bmp.isValid();

								texres->freeAllBuffers();
								delete texres;

							}

							HWND hMapON = GetDlgItem(hWnd, IDC_REND_ENV_USE_MAP);
							EMCheckBox * emcb = GetEMCheckBoxInstance(hMapON);
							emcb->selected = rtr->curScenePtr->env.enabled;
							InvalidateRect(hMapON, NULL, false);

							EMButton * emtex = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_ENV_TEXTURE));
							CHECK(emtex);
							emtex->selected = rtr->curScenePtr->env.tex.isValid();
							InvalidateRect(emtex->hwnd, NULL, false);
						}
						break;
					case IDC_REND_ENV_MAP_POWER:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MAP_POWER));
							Raytracer::getInstance()->curScenePtr->env.powerEV = emes->floatValue;
						}
						break;
					case IDC_REND_ENV_MAP_LAYER:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MAP_LAYER));
							Raytracer::getInstance()->curScenePtr->env.blendlayer = emes->intValue;
						}
						break;
					case IDC_REND_ENV_USE_MAP:
						{
							HWND hMapON = GetDlgItem(hWnd, IDC_REND_ENV_USE_MAP);
							EMCheckBox * emcb = GetEMCheckBoxInstance(hMapON);
							Raytracer::getInstance()->curScenePtr->env.enabled = emcb->selected;
						}
						break;
					case IDC_REND_ENV_MAP_AZIMUTH:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_ENV_MAP_AZIMUTH));
							Raytracer * rtr = Raytracer::getInstance();
							rtr->curScenePtr->env.azimuth_shift = emes->floatValue;
							rtr->curScenePtr->env.updateAzimuthDirections();
						}
						break;
				}
			}	// WM_COMMAND ends here
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

void RendererMainWindow::changeSunPosAndDateFromControls()
{
	HWND hWnd = hEnvironment;
	HWND hMonth  = GetDlgItem(hWnd, IDC_REND_ENV_MONTH);
	HWND hDay    = GetDlgItem(hWnd, IDC_REND_ENV_DAY);
	HWND hHour   = GetDlgItem(hWnd, IDC_REND_ENV_HOUR);
	HWND hMinute = GetDlgItem(hWnd, IDC_REND_ENV_MINUTE);
	HWND hgmt    = GetDlgItem(hWnd, IDC_REND_ENV_GMT);
	HWND hLong   = GetDlgItem(hWnd, IDC_REND_ENV_LONGITUDE);
	HWND hLat    = GetDlgItem(hWnd, IDC_REND_ENV_LATITUDE);
	HWND hEnv    = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
	HWND hCol	 = GetDlgItem(hWnd, IDC_REND_ENV_SUNCOLOR);
	EMEditSpin * emesmonth  = GetEMEditSpinInstance(hMonth);
	EMEditSpin * emesday    = GetEMEditSpinInstance(hDay);
	EMEditSpin * emeshour   = GetEMEditSpinInstance(hHour);
	EMEditSpin * emesminute = GetEMEditSpinInstance(hMinute);
	EMEditSpin * emesgmt    = GetEMEditSpinInstance(hgmt);
	EMEditSpin * emeslong   = GetEMEditSpinInstance(hLong);
	EMEditSpin * emeslat    = GetEMEditSpinInstance(hLat);
	EMEnvironment * emen    = GetEMEnvironmentInstance(hEnv);
	EMColorShow * emcs		= GetEMColorShowInstance(hCol);


	emen->sunsky.setStandardMeridianForTimeZone(-emesgmt->intValue*PI/12);
	emen->sunsky.setDate(emesmonth->intValue, emesday->intValue, 
				emeshour->intValue, (float)emesminute->intValue);
	emen->sunsky.setPosition((float)-emeslong->intValue*(PI)/180.0f, (float)(-emeslat->intValue)*(PI)/180.0f);
	emen->evalSunPosition();
	emen->repaint();


	Color4 sc = emen->sunsky.getSunColor();
	sc.clamp();
	emcs->color = sc;
	InvalidateRect(hCol, NULL, false);
}

void RendererMainWindow::fillSunSkyControls()
{
	HWND hWnd = hEnvironment;
	HWND hMonth		= GetDlgItem(hWnd, IDC_REND_ENV_MONTH);
	HWND hDay		= GetDlgItem(hWnd, IDC_REND_ENV_DAY);
	HWND hHour		= GetDlgItem(hWnd, IDC_REND_ENV_HOUR);
	HWND hMinute	= GetDlgItem(hWnd, IDC_REND_ENV_MINUTE);
	HWND hgmt		= GetDlgItem(hWnd, IDC_REND_ENV_GMT);
	HWND hLong		= GetDlgItem(hWnd, IDC_REND_ENV_LONGITUDE);
	HWND hLat		= GetDlgItem(hWnd, IDC_REND_ENV_LATITUDE);
	HWND hEnv		= GetDlgItem(hWnd, IDC_REND_ENV_MAP);
	HWND hTurb		= GetDlgItem(hWnd, IDC_REND_ENV_TURBIDITY);
	HWND hAlbedo	= GetDlgItem(hWnd, IDC_REND_ENV_ALBEDO);
	HWND hUse		= GetDlgItem(hWnd, IDC_REND_ENV_USE_SUNSKY);
	HWND hSunON		= GetDlgItem(hWnd, IDC_REND_ENV_SUN_ON);
	HWND hSunOther	= GetDlgItem(hWnd, IDC_REND_ENV_SUN_OTHER_LAYER);
	HWND hSunSize	= GetDlgItem(hWnd, IDC_REND_ENV_SUN_SIZE);
	HWND hModel		= GetDlgItem(hWnd, IDC_REND_ENV_MODEL);
	EMEditSpin * emesmonth			= GetEMEditSpinInstance(hMonth);
	EMEditSpin * emesday			= GetEMEditSpinInstance(hDay);
	EMEditSpin * emeshour			= GetEMEditSpinInstance(hHour);
	EMEditSpin * emesminute			= GetEMEditSpinInstance(hMinute);
	EMEditSpin * emesgmt			= GetEMEditSpinInstance(hgmt);
	EMEditSpin * emeslong			= GetEMEditSpinInstance(hLong);
	EMEditSpin * emeslat			= GetEMEditSpinInstance(hLat);
	EMEnvironment * emen			= GetEMEnvironmentInstance(hEnv);
	EMEditTrackBar * emetturb		= GetEMEditTrackBarInstance(hTurb);
	EMEditTrackBar * emetalbedo		= GetEMEditTrackBarInstance(hAlbedo);
	EMEditTrackBar * emetsunsize	= GetEMEditTrackBarInstance(hSunSize);
	EMCheckBox * emuse				= GetEMCheckBoxInstance(hUse);
	EMCheckBox * emsunon			= GetEMCheckBoxInstance(hSunON);
	EMCheckBox * emsunother 		= GetEMCheckBoxInstance(hSunOther);
	EMComboBox * emmodel			= GetEMComboBoxInstance(hModel);

	if (!emesmonth   ||   !emesday   ||   !emeshour   ||   !emesminute   ||   !emesgmt   ||   !emeslong   ||  !emmodel
					||   !emeslat   ||   !emen   ||   !emetturb   ||   !emetalbedo   ||   !emuse   ||   !emsunon   ||   !emetsunsize   ||   !emsunother)
		return;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (!sc)
		return;
	SunSky * sunsky = &emen->sunsky;
	emen->evalSunPosition();

	emeslong->setValuesToInt	(ROUND(-sunsky->getLongitude()*180/PI) , NOX_ENV_LONG_MIN, NOX_ENV_LONG_MAX, 1, 0.2f);
	emeslat->setValuesToInt		(ROUND(-sunsky->getLatitude()*180/PI)  , NOX_ENV_LAT_MIN,  NOX_ENV_LAT_MAX,  1, 0.2f);
	emesmonth->setValuesToInt	(sunsky->getMonth()     , 1, 12, 1, 0.1f);
	emesday->setValuesToInt		(sunsky->getDay()       , 1, 31, 1, 0.1f);
	emeshour->setValuesToInt	(sunsky->getHour()      , 0, 23, 1, 0.1f);
	emesminute->setValuesToInt	((int)sunsky->getMinute()    , 0, 59, 1, 0.1f);
	emesgmt->setValuesToInt		((int)(-sunsky->getMeridianForTimeZone()*12/PI) , -12, 12, 1, 0.1f);
	emetturb->setValuesToInt	((int)sunsky->turbidity*10  , NOX_ENV_TURB_MIN,  NOX_ENV_TURB_MAX,  1);
	emetsunsize->setValuesToFloat(sunsky->sunSize			, NOX_ENV_SUN_SIZE_MIN,  NOX_ENV_SUN_SIZE_MAX,  0.01f);
	emetalbedo->setValuesToFloat(sunsky->albedo				, NOX_ENV_ALBEDO_MIN,  NOX_ENV_ALBEDO_MAX,  0.1f);
	emuse->selected = sc->sscene.useSunSky;
	emsunon->selected = sc->sscene.sunsky->sunOn;
	emsunother->selected = sc->sscene.sunsky->sunOtherBlendLayer;
	emmodel->selected = sc->sscene.sunsky->use_hosek ? 1 : 0;
	InvalidateRect(hModel, NULL, false);
	InvalidateRect(hUse, NULL, false);
	InvalidateRect(hSunON, NULL, false);
	InvalidateRect(hSunOther, NULL, false);

	emen->sunsky.setStandardMeridianForTimeZone(-emesgmt->intValue*PI/12);
	emen->sunsky.setPosition((float)-emeslong->intValue*(PI)/180.0f, (float)(-emeslat->intValue)*(PI)/180.0f);
	emen->sunsky.setDate(emesmonth->intValue, emesday->intValue, emeshour->intValue, (float)emesminute->intValue);

	updateAzimuthAltitude();
}

void RendererMainWindow::updateAzimuthAltitude()
{
	HWND hWnd = RendererMainWindow::hEnvironment;
	HWND hAlt	 = GetDlgItem(hWnd, IDC_REND_ENV_ALTITUDE);
	HWND hAzm	 = GetDlgItem(hWnd, IDC_REND_ENV_AZIMUTH);
	HWND hEnv    = GetDlgItem(hWnd, IDC_REND_ENV_MAP);
	EMEnvironment * emen      = GetEMEnvironmentInstance(hEnv);
	int azimuth  = 180+(int)(emen->sunsky.phiS*180.0f/PI);
	int altitude = 90-(int)(emen->sunsky.thetaS*180.0f/PI);
	char buf[32];
	sprintf_s(buf, 32, "%d�", azimuth);
	SetWindowText(hAzm, buf);
	sprintf_s(buf, 32, "%d�", altitude);
	SetWindowText(hAlt, buf);
}

void RendererMainWindow::fillEnvironmentMapData()
{
	HWND hWnd = hEnvironment;
	HWND hEnabled = GetDlgItem(hWnd, IDC_REND_ENV_USE_MAP);
	HWND hTexButton = GetDlgItem(hWnd, IDC_REND_ENV_TEXTURE);
	HWND hPower = GetDlgItem(hWnd, IDC_REND_ENV_MAP_POWER);
	HWND hBLayer = GetDlgItem(hWnd, IDC_REND_ENV_MAP_LAYER);
	HWND hAzimuth = GetDlgItem(hWnd, IDC_REND_ENV_MAP_AZIMUTH);
	

	EMCheckBox * emcbEnabled = GetEMCheckBoxInstance(hEnabled);
	EMButton * embTex = GetEMButtonInstance(hTexButton);
	EMEditSpin * emesPower = GetEMEditSpinInstance(hPower);
	EMEditSpin * emesBLayer = GetEMEditSpinInstance(hBLayer);
	EMEditSpin * emesAzimuth = GetEMEditSpinInstance(hAzimuth);

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	emcbEnabled->selected = sc->env.enabled;
	InvalidateRect(hEnabled, NULL, false);
	emesPower->setValuesToFloat(sc->env.powerEV, NOX_ENV_MAP_EV_MIN, NOX_ENV_MAP_EV_MAX, 0.5f, 0.05f);
	emesBLayer->setValuesToInt(sc->env.blendlayer, NOX_ENV_MAP_BLEND_MIN, NOX_ENV_MAP_BLEND_MAX, 1, 0.1f);
	emesAzimuth->setValuesToFloat(sc->env.azimuth_shift, NOX_ENV_MAP_AZIMUTH_MIN, NOX_ENV_MAP_AZIMUTH_MAX, 5, 0.2f);
	embTex->selected = sc->env.tex.isValid();
	InvalidateRect(hTexButton, NULL, false);

	rtr->curScenePtr->env.updateAzimuthDirections();
}

