#ifndef __MY_LIST_H
#define __MY_LIST_H

#include <stdlib.h>

template <class T>
class myNode
{
public:
	myNode<T> * next;
	T obj;
};


template <class T>
class myList
{
public:
	myNode<T> * first;
	myNode<T> * last;
	bool arrayOK;
	T * objArray;
	int objCount;

	myList();
	~myList();

	void * add(T t);
	bool createArray();
	void destroyArray();
	void freeList();
	bool setElement(int n, const T & t);
	bool removeElement(int n);

	// be careful!
	T& operator[](int i);
};

template <class T>
myList<T>::myList()
{
	first = NULL;
	last = NULL;
	objArray = NULL;
	arrayOK = true;
	objCount = 0;
}

template <class T>
myList<T>::~myList()
{
	freeList();
}

template <class T>
void * myList<T>::add(T t)
{
	myNode<T> * newNode = new myNode<T>();
	if (!newNode)
		return 0;//false;

	newNode->next = NULL;
	newNode->obj = t;
	if (objCount)
	{
		last->next = newNode;
		last = newNode;
	}
	else
	{
		first = last = newNode;
	}
	arrayOK = false;
	objCount++;
	return (void*)newNode;
}

template <class T>
bool myList<T>::createArray()
{
	T* newArray;
	newArray = (T*)malloc(sizeof(T)*objCount);
	if (!newArray)
		return false;
	destroyArray();
	objArray = newArray;
	int i = 0;
	myNode<T> * temp = first;
	while (temp && i<objCount)
	{
		newArray[i] = temp->obj;
		temp = temp->next;
		i++;
	}
	arrayOK = true;
	return true;
}

template <class T>
void myList<T>::destroyArray()
{
	if (objArray)
		free(objArray);
	objArray = NULL;
	arrayOK = false;
}

template <class T>
void myList<T>::freeList()
{
	myNode<T> * temp = first;
	myNode<T> * next;
	while (temp)
	{
		next = temp->next;
		delete temp;
		temp = next;
	}
	first = last = NULL;
	objCount = 0;
	destroyArray();
	arrayOK = true;
}

template <class T>
bool myList<T>::setElement(int n, const T & t)
{
	if (n < 0   ||   n >= objCount)
		return false;

	myNode<T> * node = first;
	for (int i=0; i<n; i++)
	{
		node = node->next;
		if (!node)
			return false;
	}
	node->obj = t;
	return true;
}

template <class T>
bool myList<T>::removeElement(int n)
{
	if (n < 0   ||   n >= objCount)
		return false;

	myNode<T> * node = first;
	myNode<T> * prev = NULL;
	if (!node)
		return false;
	for (int i=0; i<n; i++)
	{
		prev = node;
		node = node->next;
		if (!node)
			return false;
	}
	if (prev)
		prev->next = node->next;
	else
		first = node->next;
	if (last == node)
		last = prev;

	objCount--;
	delete node;
	return true;
}


template <class T>
T& myList<T>::operator[](int i)
{
	return objArray[i];
}

////-----------------------------------------------------------


template <class T, int MAXSIZE>
class myFifo 
{
protected:
	T * head;
	T * tail;
	int n;
	T list[MAXSIZE];

public:
	myFifo() 
	{
		head = tail = list;
		n = 0;
	}

	bool putObject(T const & x) 
	{
		if (n >= MAXSIZE) 
		{
			return false;
		}
		*head = x;
		n++;
		if (++head >= list + MAXSIZE) 
		{
			head = list;
		}
		return true;
	}

	T getObject() 
	{
		if (n <= 0) 
		{
			throw 1;
			// Error: list empty.
		}
		n--;
		T * p = tail;
		if (++tail >= list + MAXSIZE) 
		{
			tail = list;
		}
		return *p;
	}

	bool isEmpty()
	{
		return (n==0);
	}

	bool isFull()
	{
		return (n==MAXSIZE);
	}

	int getNumObjects() 
	{
		return n;
	}
};



//===============================================

template <class T>
class bhNode
{
public:
	T * buffer;
	bhNode * next;
};

//-----------------------------------------------

template <class T, int ONESIZE>
class bList
{
public:
	bhNode<T> bheaders;			// first node with buffer
	T ** headers_array;			// array of buffers
	bhNode<T> * curNode;		// current node when adding
	int available;				// number of available places in current buffer
	int numHeaders;				// number of buffers allocated
	int objCount;				// total number of objects added
	int alignment;				// buffer alignment
	bool array_OK;				// buffers array OK ?


	bList(int align);			// 0 - no align
	~bList();

	bool add(const T &t, void (*initCallback)(void * addr)=NULL);
	bool createArray();
	void destroyArray();
	void freeList();
	bool copyTo(int i, T* t);
	bool trimLastObjects(int num);
	bool swapElements(int i1, int i2);

	T& operator[](int i) const;
};

//------------------

template <class T, int ONESIZE>
bList<T, ONESIZE>::bList(int align)
{
	alignment = align;
	if (alignment > 0)
		bheaders.buffer = (T*)_aligned_malloc(sizeof(T)*ONESIZE, alignment);
	else
		bheaders.buffer = (T*)malloc(sizeof(T)*ONESIZE);
	bheaders.next   = 0;
	curNode = &bheaders;
	available = ONESIZE;
	numHeaders = 1;
	objCount = 0;
	headers_array = 0;
	array_OK = false;
}

//------------------

template <class T, int ONESIZE>
bList<T, ONESIZE>::~bList()
{
	freeList();
	if (bheaders.buffer)
	{
		if (alignment > 0)
			_aligned_free(bheaders.buffer);
		else
			free(bheaders.buffer);
	}
}

//------------------

template <class T, int ONESIZE>
bool bList<T, ONESIZE>::add(const T &t, void (*initCallback)(void * addr))
{
	if (available < 1)
	{
		bhNode<T> * nNode = new bhNode<T>();
		nNode->next = 0;

		if (alignment > 0)
			nNode->buffer = (T*)_aligned_malloc(sizeof(T)*ONESIZE, alignment);
		else
			nNode->buffer = (T*)malloc(sizeof(T)*ONESIZE);
		if (!nNode->buffer)
			return false;
		if (curNode)
		{
			curNode->next = nNode;
			curNode = nNode;
		}
		else
		{
			curNode = nNode;
		}
		numHeaders++;
		available = ONESIZE;
	}

	if (initCallback)
		initCallback((void *)&(curNode->buffer[ONESIZE - available]));
	curNode->buffer[ONESIZE - available] = t;
	available--;
	objCount++;
	array_OK = false;

	return true;
}

//------------------

template <class T, int ONESIZE>
bool bList<T, ONESIZE>::trimLastObjects(int num)
{
	bool res = true;
	if (num<1)
		return false;
	if (num>objCount)
		res = false;

	int ndel = min(num, objCount);
	int objLeft = objCount - ndel;
	int headersLeft = (objLeft-1+ONESIZE)/ONESIZE;	// ceil
	headersLeft = max(headersLeft, 1);	// at least one
	int availLeft = headersLeft*ONESIZE - objLeft;

	int i=1;
	bhNode<T> * curHeader = &(bheaders);
	while (curHeader)
	{
		bhNode<T> * nextHeader = curHeader->next;
		if (i==headersLeft)
		{
			curHeader->next = NULL;
			curNode = curHeader;
		}

		if (i>headersLeft)
		{
			if (alignment > 0)
				_aligned_free(curHeader->buffer);
			else
				free(curHeader->buffer);
			curHeader->buffer = NULL;
			delete curHeader;
		}

		i++;
		curHeader = nextHeader;
	}

	available = availLeft;
	objCount = objLeft;

	return res;
}

//------------------

template <class T, int ONESIZE>
bool bList<T, ONESIZE>::createArray()
{
	if (headers_array)
		delete []headers_array;
	headers_array = new T*[numHeaders];
	if (!headers_array)
		return false;

	bhNode<T> * cur = &bheaders;
	int i = 0;
	while (cur)
	{
		headers_array[i] = cur->buffer;
		i++;
		cur = cur->next;
	}

	array_OK = true;
	return true;
}

//------------------

template <class T, int ONESIZE>
void bList<T, ONESIZE>::destroyArray()
{
	delete []headers_array;
	headers_array = 0;
	array_OK = false;
}

//------------------

template <class T, int ONESIZE>
void bList<T, ONESIZE>::freeList()
{
	bhNode<T> * cur = bheaders.next;
	bhNode<T> * temp;
	while (cur)
	{
		temp = cur;
		cur = cur->next;
		if (alignment > 0)
			_aligned_free(temp->buffer);
		else
			free(temp->buffer);
		delete temp;
	}

	destroyArray();

	available = ONESIZE;
	bheaders.next = 0;
	numHeaders = 1;
	objCount = 0;
	curNode = &bheaders;
}

//------------------

template <class T, int ONESIZE>
T& bList<T, ONESIZE>::operator[](int i) const
{
	if (i >= objCount)
		i = objCount-1;

	int hh = i/ONESIZE;
	int ii = i%ONESIZE;

	return headers_array[hh][ii];
}

//------------------

template <class T, int ONESIZE>
bool bList<T, ONESIZE>::copyTo(int i, T *t)
{
	if (!t)
		return false;

	if (i >= objCount)
		i = objCount-1;

	int hh = i/ONESIZE;
	int ii = i%ONESIZE;

	*t = headers_array[hh][ii];

	return true;
}
//------------------

template <class T, int ONESIZE>
bool bList<T, ONESIZE>::swapElements(int i1, int i2)
{
	if (i1<0 || i2<0)
		return false;
	if (i1>=objCount || i2>=objCount)
		return false;
	if (i1==i2)
		return true;

	int hh1 = i1/ONESIZE;
	int ii1 = i1%ONESIZE;
	int hh2 = i2/ONESIZE;
	int ii2 = i2%ONESIZE;

	int ss = (int)sizeof(T);
	T* tobj = (T*)malloc(ss);
	if(!tobj)
		return false;

	memcpy_s(tobj, ss, &(headers_array[hh1][ii1]), ss);
	memcpy_s(&(headers_array[hh1][ii1]), ss, &(headers_array[hh2][ii2]), ss);
	memcpy_s(&(headers_array[hh2][ii2]), ss, tobj, ss);

	free(tobj);

	return true;
}


//===============================================


template <class T, int SSIZE>
class sfStack
{
	T buffer[SSIZE];

public:
	int used;
	bool push(const T &t);
	bool pop(T &t);
	void freeStack();
	sfStack() {used = 0;}
};

//------------------

template <class T, int SSIZE>
bool sfStack<T, SSIZE>::push(const T &t)
{
	if (used >= SSIZE)
		return false;
	buffer[used] = t;
	used++;
	return true;
}

//------------------

template <class T, int SSIZE>
bool sfStack<T, SSIZE>::pop(T &t)
{
	if (used <= 0)
		return false;
	used--;
	t = buffer[used];
	return true;
}

//------------------

template <class T, int SSIZE>
void sfStack<T, SSIZE>::freeStack()
{
	used = 0;
}

//===============================================

#endif

