#ifndef __nox_update__
#define __nox_update__

#define NUM_PLUGINS_ALL 15

#define PROGRAM_STATUS_BEGIN						1
#define PROGRAM_STATUS_LOCAL_VERSION_OK				2
#define PROGRAM_STATUS_LOCAL_VERSION_ERROR			3
#define PROGRAM_STATUS_CHECKING_SERVER_VERSION		4
#define PROGRAM_STATUS_SERVER_VERSION_OK_HIGHER		5
#define PROGRAM_STATUS_SERVER_VERSION_OK_LOWER		6
#define PROGRAM_STATUS_SERVER_VERSION_ERROR			7
#define PROGRAM_STATUS_DOWNLOADING_UPDATE			8
#define PROGRAM_STATUS_DOWN_UPDATE_OK				9
#define PROGRAM_STATUS_DOWN_UPDATE_ERROR			10
#define PROGRAM_STATUS_INSTALLING					11
#define PROGRAM_STATUS_WAIT_FOR_DIR					12
#define PROGRAM_STATUS_UNPACKING					13
#define PROGRAM_STATUS_COPYING						14
#define PROGRAM_STATUS_DELETING_TEMP				15
#define PROGRAM_STATUS_RUN_PLUGINS					16
#define PROGRAM_STATUS_INSTALL_ERROR				17
#define PROGRAM_STATUS_INSTALL_ABORTED				18
#define PROGRAM_STATUS_INSTALL_COMPLETE				19
//#define PROGRAM_STATUS_

bool doesDirExist(char * dirName);
bool doesFileExist(char * filename);		// will return false on dir
char * getDirectoryFromFilePath(char * filename);
char * getFilenameFromFilePath(char * filename);
char * getRendererDirectory();
char * randomTempFilename(char * prefix);
bool getNoxVersion(int &vMajor, int &vMinor, int &vBuild);
bool checkNOXserverVersion(int &vMajor, int &vMinor, int &vBuild, bool alpha);
bool downloadNOXserverVersion(bool alpha);
bool isVersionHigher(int vLocalMajor, int vLocalMinor, int vLocalBuild, int vServerMajor, int vServerMinor, int vServerBuild);
bool downloadUpdate(int vMajor, int vMinor, int vBuild, bool alpha);
bool checkRendererDirs(char * dir);
void setDir();
bool getAutoUpdateRegistryStatus(bool &autoUpdateOn, bool &autoInstall, bool &betaTester, bool &diffrDirs);
bool setRegistryAutoUpdate(bool autoUpdateOn);
bool setRegistryAutoInstall(bool autoInstallOn);
bool setRegistryDifferentDirs(bool diffDirsOn);
char * check3dsMaxDir(char * regPath, bool wow64);
void recognize3dsmaxVersions(bool is64system);
void findAndFill3dsmaxDirs(bool is64system);
char * openDirectoryDialog(HWND hwnd, char* title, char * startDir);
void installPlugins();
bool installUpdate(char * filename7z, char ** tempDir, unsigned int &numberFiles);
bool unpackUpdate(char * filename, char ** dirname);
bool getFileSize(char * fullfilename, unsigned int & result);
bool evalFilesRecursively(char * dirName	, unsigned int &numFiles, unsigned int &sumSize);
bool copyFilesRecursively(char * dstDir, char * srcDir, unsigned int &curBytes, unsigned int &sumBytes, unsigned int &curFiles, unsigned int &sumFiles);
bool deleteDirRecursively(char * dirName, unsigned int &curFiles, unsigned int &sumFiles);
bool runPluginInstaller();
bool runInstallThread(char * filename7z);
bool setNOXdirInRegistry(char * newdir);


void lockControlsBeforeDownloadVersion();
void unlockControlsAfterDownloadVersion(int status);
void lockControlsBeforeDownloadUpdate();
void unlockControlsAfterDownloadUpdate(int status);
void lockControlsBeforeInstallUpdate();
void unlockControlsAfterInstallUpdate(int status);
void unlockControlsAfterRunPluginsInstaller(int status);



#endif
