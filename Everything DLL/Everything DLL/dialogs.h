#ifndef __DIALOGS_H
#define __DIALOGS_H

#include <windows.h>
#include "textures.h"
#include "DLL.h"
#include "raytracer.h"

// ------------------------------------------------------------------------------------------------------------------------------------------------------

struct EnvSunSky
{
	SunSky * sunsky;
	EnvSphere * env;
	EnvSunSky() { sunsky=NULL; env=NULL; }
};

// ------------------------------------------------------------------------------------------------------------------------------------------------------

struct RunNextSettings
{
	bool active;
	bool nextCam;
	bool nCamDesc;
	bool copyPost;
	bool delBuffs;
	bool saveImg;
	bool changeSunsky;
	int sunskyMins;
	int fileFormat;
	char * saveFolder;

	RunNextSettings * getCopy();
	bool copyFrom(RunNextSettings * other);
	RunNextSettings() { active=false; nextCam=true; nCamDesc=false; copyPost=false; delBuffs=false; saveImg=false; changeSunsky=false; sunskyMins=0; fileFormat=0; saveFolder=NULL; }
};

// ------------------------------------------------------------------------------------------------------------------------------------------------------

struct PickedTexture
{
	char * filename;
	char * fullfilename;
	ImageByteBuffer preview;
	int width;
	int height;

	PickedTexture() { filename=NULL; fullfilename=NULL; width=0; height=0; }
};

// ------------------------------------------------------------------------------------------------------------------------------------------------------

struct PresetChoose
{
	bool layers;
	bool post;
	bool correction;
	bool fakedof;
	bool effects;
	PresetChoose() { layers=true; post=true; correction=true; fakedof=true; effects=true; }
};

// ------------------------------------------------------------------------------------------------------------------------------------------------------

typedef	void (notifyCameraPluginCallback)(float focal, float focus);
typedef	void (notifySunSkyPluginCallback)(int month, int day, int hour, int minute, int gmt, float longitude, float latitude, float env_azimuth);

// ------------------------------------------------------------------------------------------------------------------------------------------------------

// normals dialog
INT_PTR CALLBACK NormalDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// textures dialog
INT_PTR CALLBACK TexDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
bool makeTexPreview(HWND hwnd);
char * openTexDialog(HWND hwnd);
void updateDialogFromTex(Texture * tex, HWND hwnd);

// textures new dialog
INT_PTR CALLBACK Tex2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// normal new dialog
INT_PTR CALLBACK Normalmap2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// texture picker dialog
INT_PTR CALLBACK TexPickerDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// texture picker new dialog
INT_PTR CALLBACK TexPicker2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// about dialog
INT_PTR CALLBACK AboutDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// new about dialog
INT_PTR CALLBACK About2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// license dialog
INT_PTR CALLBACK LicenseDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// license dialog
INT_PTR CALLBACK CreditsDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// fresnel dialog
INT_PTR CALLBACK FresnelDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// environment dialog
INT_PTR CALLBACK EnvDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
DECLDIR EnvSunSky * showPluginSunSkyDialog(HWND hParent, SunSky * sun, EnvSphere * env);
DECLDIR void registerSunSkyUpdateNotification(notifySunSkyPluginCallback * callback);

// camera view dialog
INT_PTR CALLBACK CameraViewDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void updateCameraSettings(HWND hwnd);

// color dialog
INT_PTR CALLBACK ColorDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// temperature dialog
INT_PTR CALLBACK TemperatureDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// temperature new dialog
INT_PTR CALLBACK TemperatureDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


// zoom render
INT_PTR CALLBACK PreviewZoomDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// camera as dialog for plugins
INT_PTR CALLBACK PluginCameraDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
DECLDIR Camera * showPluginCameraDialog(HWND hParent, Camera *cam);
DECLDIR void registerCameraUpdateNotification(notifyCameraPluginCallback * callback);

// upload mat
INT_PTR CALLBACK UploadMatDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// download mat
INT_PTR CALLBACK DownloadMatDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK UserPassDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);



INT_PTR CALLBACK BlendNamesDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
DECLDIR void runBlendNamesDialog(HWND hWnd, BlendSettings * blend);

// camera response function graph
INT_PTR CALLBACK ResponseGraphDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK ResponsePreviewDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ResponsePreviewDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ResponsePreviewDlg3Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK SceneInfoDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK SceneInfo2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK ChromaticAberrationGraphDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK MemUsageDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK IESDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK IES2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK DlgProcTest(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
DECLDIR void runTestDialog(HWND hWnd, LPARAM lParam);

INT_PTR CALLBACK AfterStopSettingsDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK AfterStop2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK ColorDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK PresetsLoadDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK TexReloadAskDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
int runAskTexReloadDialog(HWND hWnd, char * txtparam);

INT_PTR CALLBACK PresetNewDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
char * runPresetAddDialog(HWND hWnd, char * name);

INT_PTR CALLBACK PresetManageDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void runPresetManageDialog(HWND hWnd);

bool openAnimCircleDialog(HWND hParent, int cposx, int cposy);
extern bool circleActive;

void InitLicense2WindowClass();
bool openLicense2Window(HWND hParent);


#endif

