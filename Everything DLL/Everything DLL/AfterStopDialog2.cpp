#include <Windows.h>
#include "dialogs.h"
#include "EM2Controls.h"
#include "resource.h"
#include "noxfonts.h"
#include <tchar.h>

// new gui version

extern HMODULE hDllModule;

bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
void setPositionsOnStartAfterRender2(HWND hWnd);
void afterRenderDlgUpdateLocks(HWND hWnd);
void afterRenderDlgGetDataFromGUI(HWND hWnd, RunNextSettings * sett);
bool afterRenderDlgSetButtonPath(HWND hButton, char * text);

char * getShortPathEllipsis(HDC hdc, char * txt, int width, int minfirst);


//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK AfterStop2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;
	static RunNextSettings settings;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = 384 + wrect.right-wrect.left-crect.right;
				tmprect.bottom = 258 + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);

				GetClientRect(hWnd, &crect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(crect.right, crect.bottom, 0,0, NGCOL_BG_LIGHT, NGCOL_BG_DARK);
				NOXFontManager * fonts = NOXFontManager::getInstance();
				RunNextSettings defSettings;
				RunNextSettings * sett = lParam ? (RunNextSettings*)lParam : &defSettings;
				
				settings = *sett;
				settings.saveFolder = copyString(sett->saveFolder);

				setPositionsOnStartAfterRender2(hWnd);

				EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_AFTER2_TITLE));
				emtTitle->bgImage = hBgBMP;
				emtTitle->setFont(fonts->em2paneltitle, false);
				emtTitle->align = EM2Text::ALIGN_CENTER;
				emtTitle->colText = RGB(0xcf, 0xcf, 0xcf); 

				EM2Text * emtMinutes = GetEM2TextInstance(GetDlgItem(hWnd, IDC_AFTER2_TEXT_MINUTES));
				emtMinutes->bgImage = hBgBMP;
				emtMinutes->setFont(fonts->em2text, false);

				EM2GroupBar * emgr_perform = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_AFTER2_GR_PERFORM_ACTIONS));
				emgr_perform->bgImage = hBgBMP;
				emgr_perform->setFont(fonts->em2groupbar, false);

				EM2CheckBox * emcNextCam = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_RENDER_CAMERA));
				emcNextCam->bgImage = hBgBMP;
				emcNextCam->setFont(fonts->em2checkbox, false);
				emcNextCam->selected = sett->nextCam;

				EM2CheckBox * emcCopyPost = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_COPY_POST));
				emcCopyPost->bgImage = hBgBMP;
				emcCopyPost->setFont(fonts->em2checkbox, false);
				emcCopyPost->selected = sett->copyPost;

				EM2CheckBox * emcChangeSunsky = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY));
				emcChangeSunsky->bgImage = hBgBMP;
				emcChangeSunsky->setFont(fonts->em2checkbox, false);
				emcChangeSunsky->selected = sett->changeSunsky;

				EM2CheckBox * emcSaveImage = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE));
				emcSaveImage->bgImage = hBgBMP;
				emcSaveImage->setFont(fonts->em2checkbox, false);
				emcSaveImage->selected = sett->saveImg;

				EM2CheckBox * emcDelBuf = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_DELETE_BUFFER));
				emcDelBuf->bgImage = hBgBMP;
				emcDelBuf->setFont(fonts->em2checkbox, false);
				emcDelBuf->selected = sett->delBuffs;

				EM2EditSpin * emesMinutes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY_MINUTES));
				emesMinutes->bgImage = hBgBMP;
				emesMinutes->setFont(fonts->em2editspin, false);
				emesMinutes->setValuesToInt(sett->sunskyMins, -1440, 1440, 1, 1, 0.1f);

				EM2Button * embFolder = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FOLDER));
				embFolder->bgImage = hBgBMP;
				embFolder->setFont(fonts->em2button, false);

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_AFTER2_OK));
				embOK->bgImage = hBgBMP;
				embOK->setFont(fonts->em2button, false);

				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_AFTER2_CANCEL));
				embCancel->bgImage = hBgBMP;
				embCancel->setFont(fonts->em2button, false);


				EM2ComboBox * emcbCamOrder = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_CAMERA_ORDER));
				emcbCamOrder->setFontMain(fonts->em2combomain, false);
				emcbCamOrder->setFontUnwrapped(fonts->em2combounwrap, false);
				emcbCamOrder->bgImage = hBgBMP;
				emcbCamOrder->maxShownRows = 2;
				emcbCamOrder->rowWidth = 70;
				emcbCamOrder->moveArrow = true;
				emcbCamOrder->align = EM2ComboBox::ALIGN_LEFT;
				emcbCamOrder->setNumItems(2);
				emcbCamOrder->setItem(0, "NEXT", true);
				emcbCamOrder->setItem(1, "PREVIOUS", true);
				emcbCamOrder->selected = sett->nCamDesc ? 1 : 0;

				EM2ComboBox * emcbFormat = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FORMAT));
				emcbFormat->setFontMain(fonts->em2combomain, false);
				emcbFormat->setFontUnwrapped(fonts->em2combounwrap, false);
				emcbFormat->bgImage = hBgBMP;
				emcbFormat->maxShownRows = 3;
				emcbFormat->rowWidth = 50;
				emcbFormat->moveArrow = true;
				emcbFormat->align = EM2ComboBox::ALIGN_LEFT;
				emcbFormat->setNumItems(3);
				emcbFormat->setItem(0, "PNG", true);
				emcbFormat->setItem(1, "JPG", true);
				emcbFormat->setItem(2, "EXR", true);
				emcbFormat->selected = sett->fileFormat;

				updateControlBgShift(emtTitle->hwnd, 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);
				updateControlBgShift(emgr_perform->hwnd, 0, 0, emgr_perform->bgShiftX, emgr_perform->bgShiftY);
				updateControlBgShift(emcNextCam->hwnd, 0, 0, emcNextCam->bgShiftX, emcNextCam->bgShiftY);
				updateControlBgShift(emcCopyPost->hwnd, 0, 0, emcCopyPost->bgShiftX, emcCopyPost->bgShiftY);
				updateControlBgShift(emcChangeSunsky->hwnd, 0, 0, emcChangeSunsky->bgShiftX, emcChangeSunsky->bgShiftY);
				updateControlBgShift(emcSaveImage->hwnd, 0, 0, emcSaveImage->bgShiftX, emcSaveImage->bgShiftY);
				updateControlBgShift(emcDelBuf->hwnd, 0, 0, emcDelBuf->bgShiftX, emcDelBuf->bgShiftY);
				updateControlBgShift(emesMinutes->hwnd, 0, 0, emesMinutes->bgShiftX, emesMinutes->bgShiftY);
				updateControlBgShift(embFolder->hwnd, 0, 0, embFolder->bgShiftX, embFolder->bgShiftY);
				updateControlBgShift(embOK->hwnd, 0, 0, embOK->bgShiftX, embOK->bgShiftY);
				updateControlBgShift(embCancel->hwnd, 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);
				updateControlBgShift(emtMinutes->hwnd, 0, 0, emtMinutes->bgShiftX, emtMinutes->bgShiftY);
				updateControlBgShift(emcbCamOrder->hwnd, 0, 0, emcbCamOrder->bgShiftX, emcbCamOrder->bgShiftY);
				updateControlBgShift(emcbFormat->hwnd, 0, 0, emcbFormat->bgShiftX, emcbFormat->bgShiftY);

				afterRenderDlgSetButtonPath(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FOLDER), settings.saveFolder);
				afterRenderDlgUpdateLocks(hWnd);
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_DESTROY:
			{
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;

				GetClientRect(hWnd, &rect);
				hdc = BeginPaint(hWnd, &ps);

				// back buffer
				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				// copy background image
				HDC thdc = CreateCompatibleDC(hdc);			// bg image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, 0, 0, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				// done
				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_AFTER2_OK:
						{
							CHECK(wmEvent==BN_CLICKED);
							EndDialog(hWnd, (INT_PTR)&settings);
						}
						break;
					case IDC_AFTER2_CANCEL:
						{
							CHECK(wmEvent==BN_CLICKED);
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;
					case IDC_AFTER2_RENDER_CAMERA:
						{
							CHECK(wmEvent==BN_CLICKED);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
					case IDC_AFTER2_CAMERA_ORDER:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
					case IDC_AFTER2_COPY_POST:
						{
							CHECK(wmEvent==BN_CLICKED);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
					case IDC_AFTER2_CHANGE_SUNSKY:
						{
							CHECK(wmEvent==BN_CLICKED);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
					case IDC_AFTER2_CHANGE_SUNSKY_MINUTES:
						{
							CHECK(wmEvent==WM_VSCROLL);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
					case IDC_AFTER2_SAVE_IMAGE:
						{
							CHECK(wmEvent==BN_CLICKED);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
					case IDC_AFTER2_SAVE_IMAGE_FOLDER:
						{
							CHECK(wmEvent==BN_CLICKED);
							char * newdir = saveDirectoryDialog(hWnd, "Choose folder for images", settings.saveFolder);
							if (!newdir)
								break;
							if (settings.saveFolder)
								free(settings.saveFolder);
							settings.saveFolder = newdir;
							afterRenderDlgSetButtonPath(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FOLDER), settings.saveFolder);
						}
						break;
					case IDC_AFTER2_SAVE_IMAGE_FORMAT:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
					case IDC_AFTER2_DELETE_BUFFER:
						{
							CHECK(wmEvent==BN_CLICKED);
							afterRenderDlgGetDataFromGUI(hWnd, &settings);
						}
						break;
				}
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartAfterRender2(HWND hWnd)
{
	// srodek panela x=188
	int x, y;
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_TITLE), HWND_TOP, 100,10, 184, 19, 0);

	
	x=12; y=40;
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_GR_PERFORM_ACTIONS),		HWND_TOP,	x,		y,		360,	16,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_RENDER_CAMERA),			HWND_TOP,	x,		y+33,	115,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_COPY_POST),				HWND_TOP,	x,		y+63,	280,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY),			HWND_TOP,	x,		y+93,	125,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE),				HWND_TOP,	x,		y+123,	80,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_DELETE_BUFFER),			HWND_TOP,	x,		y+153,	240,	10,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_CAMERA_ORDER),				HWND_TOP,	x+125,	y+27,	80,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY_MINUTES),	HWND_TOP,	x+125,	y+88,	60,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_TEXT_MINUTES),				HWND_TOP,	x+192,	y+90,	50,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FOLDER),		HWND_TOP,	x+125,	y+117,	150,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FORMAT),		HWND_TOP,	x+285,	y+117,	50,		22,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_OK),						HWND_TOP,	x+115,	y+180,	72,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_AFTER2_CANCEL),					HWND_TOP,	x+197,	y+180,	72,		22,			0);

}

//------------------------------------------------------------------------------------------------

void afterRenderDlgUpdateLocks(HWND hWnd)
{
	EM2CheckBox * emcNextCam = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_RENDER_CAMERA));
	EM2CheckBox * emcCopyPost = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_COPY_POST));
	EM2CheckBox * emcChangeSunsky = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY));
	EM2CheckBox * emcSaveImage = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE));
	EM2CheckBox * emcDelBuf = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_DELETE_BUFFER));
	EM2EditSpin * emesMinutes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY_MINUTES));
	EM2Button   * embFolder = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FOLDER));
	EM2ComboBox * emcbCamOrder = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_CAMERA_ORDER));
	EM2ComboBox * emcbFormat = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FORMAT));

	if (emcNextCam->selected)
	{
		EnableWindow(emcCopyPost->hwnd, TRUE);
		EnableWindow(emcbCamOrder->hwnd, TRUE);
	}
	else
	{
		emcCopyPost->selected = false;
		EnableWindow(emcCopyPost->hwnd, FALSE);
		EnableWindow(emcbCamOrder->hwnd, FALSE);
	}


	if (emcChangeSunsky->selected)
		EnableWindow(emesMinutes->hwnd, TRUE);
	else
		EnableWindow(emesMinutes->hwnd, FALSE);

	if (emcSaveImage->selected)
	{
		EnableWindow(emcDelBuf->hwnd, TRUE);
		EnableWindow(embFolder->hwnd, TRUE);
		EnableWindow(emcbFormat->hwnd, TRUE);
	}
	else
	{
		emcDelBuf->selected = false;
		EnableWindow(emcDelBuf->hwnd, FALSE);
		EnableWindow(embFolder->hwnd, FALSE);
		EnableWindow(emcbFormat->hwnd, FALSE);
	}

	InvalidateRect(emcNextCam->hwnd, NULL, false);
	InvalidateRect(emcCopyPost->hwnd, NULL, false);
	InvalidateRect(emcChangeSunsky->hwnd, NULL, false);
	InvalidateRect(emcSaveImage->hwnd, NULL, false);
	InvalidateRect(emcDelBuf->hwnd, NULL, false);
	InvalidateRect(emesMinutes->hwnd, NULL, false);
	InvalidateRect(embFolder->hwnd, NULL, false);
	InvalidateRect(emcbCamOrder->hwnd, NULL, false);
	InvalidateRect(emcbFormat->hwnd, NULL, false);

}

//------------------------------------------------------------------------------------------------

void afterRenderDlgGetDataFromGUI(HWND hWnd, RunNextSettings * sett)
{
	if (!sett  ||  !hWnd)
		return;

	afterRenderDlgUpdateLocks(hWnd);

	EM2CheckBox * emcNextCam = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_RENDER_CAMERA));
	EM2CheckBox * emcCopyPost = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_COPY_POST));
	EM2CheckBox * emcChangeSunsky = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY));
	EM2CheckBox * emcSaveImage = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE));
	EM2CheckBox * emcDelBuf = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_DELETE_BUFFER));
	EM2EditSpin * emesMinutes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_AFTER2_CHANGE_SUNSKY_MINUTES));
	EM2Button   * embFolder = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FOLDER));
	EM2ComboBox * emcbCamOrder = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_CAMERA_ORDER));
	EM2ComboBox * emcbFormat = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_AFTER2_SAVE_IMAGE_FORMAT));

	sett->nextCam = emcNextCam->selected;
	sett->copyPost = emcCopyPost->selected;
	sett->changeSunsky = emcChangeSunsky->selected;
	sett->saveImg = emcSaveImage->selected;
	sett->delBuffs = emcDelBuf->selected;
	sett->sunskyMins = emesMinutes->intValue;
	sett->nCamDesc = (emcbCamOrder->selected==1);
	sett->fileFormat = emcbFormat->selected;
}

//------------------------------------------------------------------------------------------------

bool afterRenderDlgSetButtonPath(HWND hButton, char * text)
{
	CHECK(hButton);
	EM2Button * emb = GetEM2ButtonInstance(hButton);
	CHECK(emb);
	if (!text  ||  strlen(text)<1) 
	{
		emb->changeCaption("CHOOSE FOLDER");
		return true;
	}

	HDC hdc = GetDC(hButton);
	RECT rect;
	GetClientRect(hButton, &rect);
	HFONT hOldFont = (HFONT)SelectObject(hdc, emb->getFont());
	char * shortpath = getShortPathEllipsis(hdc, text, rect.right-8, 6);
	SelectObject(hdc, hOldFont);
	ReleaseDC(hButton, hdc);
	
	emb->changeCaption(shortpath ? shortpath : text);

	if (shortpath)
		free(shortpath);
	return true;
}

//------------------------------------------------------------------------------------------------
