#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"

extern HMODULE hDllModule;

void InitEMTabs()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMTabs";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMTabsProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMTabs *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMTabs * GetEMTabsInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMTabs * emt = (EMTabs *)GetWindowLongPtr(hwnd, 0);
	#else
		EMTabs * emt = (EMTabs *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emt;
}

void SetEMTabsInstance(HWND hwnd, EMTabs *emt)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emt);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emt);
	#endif
}

EMTabs::EMTabs(HWND hWnd):
caps(0),
lengths(0),
ends(0)
{
	hwnd = hWnd;

    colText					= RGB(224,224,224);
    colBackGnd				= RGB(80,80,80);
	colSelText				= RGB(255,255,255);
	colSelBackGnd			= RGB(96,96,96);
	colDisText				= RGB(192,192,192);
	colDisBackGnd			= RGB(64,64,64);
	colDisBorderLight		= RGB(128,128,128);
	colDisBorderDark		= RGB(32,32,32);
	colDisBorderBottom		= RGB(32,32,32);
	colBorderLight			= RGB(128,128,128);
	colBorderDark			= RGB(48,48,48);
	colBorderBottom			= RGB(48,48,48);
	colSelBorderBottom		= RGB(96,96,96);
	colDisSelBorderBottom	= RGB(64,64,64);

	heightTab = 20;
	heightTabSel = 22;
	marginSize = 8;
	selected = -1;
	lastSelected = -1;
	drawBottomBorder = true;

	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	LOGFONT lf;
	GetObject(hFont, sizeof(LOGFONT), &lf);
	lf.lfWeight = FW_SEMIBOLD;
	hBoldFont = CreateFontIndirect(&lf);

	hRegion = 0;
	rP = 0;
}

EMTabs::~EMTabs()
{
	DeleteObject(hBoldFont);
	if (hRegion)
		DeleteObject(hRegion);
	if (rP)
		free(rP);
	caps.freeList();
	ends.freeList();
	lengths.freeList();

}

LRESULT CALLBACK EMTabsProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMTabs* emt;
	emt = GetEMTabsInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HFONT hOldFont;
	HPEN hOldPen;

	switch (msg)
	{
		case WM_CREATE:
			emt = new EMTabs(hwnd);
			SetEMTabsInstance(hwnd, emt);
			break;
		case WM_DESTROY:
			{
				delete emt;
				SetEMTabsInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			DWORD style;
			style = GetWindowLong(hwnd, GWL_STYLE);
			bool disabled = ((style & WS_DISABLED) > 0);

			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);
			int selStart, selEnd;

			// colors, pens, fonts stuff
			HPEN hPenLight, hPenDark, hPenBottom, hPenBottomSel;
			if (disabled)
			{
				hPenLight  = CreatePen(PS_SOLID, 1, emt->colDisBorderLight);
				hPenDark   = CreatePen(PS_SOLID, 1, emt->colDisBorderDark);
				hPenBottom = CreatePen(PS_SOLID, 1, emt->colDisBorderBottom);
				hPenBottomSel = CreatePen(PS_SOLID, 1, emt->colDisSelBorderBottom);
			}
			else
			{
				hPenLight  = CreatePen(PS_SOLID, 1, emt->colBorderLight);
				hPenDark   = CreatePen(PS_SOLID, 1, emt->colBorderDark);
				hPenBottom = CreatePen(PS_SOLID, 1, emt->colBorderBottom);
				hPenBottomSel = CreatePen(PS_SOLID, 1, emt->colSelBorderBottom);
			}
			hOldPen = (HPEN)SelectObject(hdc, hPenLight);
			hOldFont = (HFONT)SelectObject(hdc, emt->hFont);
			SetTextColor(hdc, emt->colText);
			SetBkColor  (hdc, emt->colBackGnd);

			emt->createRegion(hdc);

			SIZE sz;
			int curL = 0;
			int y;

			for (int i=0; i<emt->caps.objCount; i++)
			{
				bool selected = (i == emt->selected);
				if (selected)
				{
					SelectObject(hdc, emt->hBoldFont);
					SetTextColor(hdc, emt->colSelText);
					SetBkColor  (hdc, emt->colSelBackGnd);
				}
				else
				{
					SelectObject(hdc, emt->hFont);
					SetTextColor(hdc, emt->colText);
					SetBkColor  (hdc, emt->colBackGnd);
				}
				if (disabled)
				{
					SetTextColor(hdc, emt->colDisText);
					SetBkColor  (hdc, emt->colDisBackGnd);
				}

				char * cap = emt->caps[i];
				int ssize = emt->lengths[i];
				GetTextExtentPoint32(hdc, cap, ssize, &sz);
				RECT crect = {curL,   rect.bottom-emt->heightTabSel,   curL+2*emt->marginSize+sz.cx, rect.bottom};

				if (selected)
					y = rect.bottom-emt->heightTabSel + (emt->heightTabSel - sz.cy)/2;
				else
					y = rect.bottom-emt->heightTab + (emt->heightTab - sz.cy)/2;

				ExtTextOut(hdc, curL+emt->marginSize, y, ETO_OPAQUE, &crect, cap, ssize, 0);

				if (!selected)
				{
					POINT b1[] = {	{curL,								rect.bottom-1}, 
									{curL,								rect.bottom-emt->heightTab+2}, 
									{curL+2,							rect.bottom-emt->heightTab}, 
									{curL+2*emt->marginSize+sz.cx-2 ,	rect.bottom-emt->heightTab}   };
					POINT b2[] = {	{curL+2*emt->marginSize+sz.cx-2 ,	rect.bottom-emt->heightTab+1}, 
									{curL+2*emt->marginSize+sz.cx-1 ,	rect.bottom-emt->heightTab+2}, 
									{curL+2*emt->marginSize+sz.cx-1 ,	rect.bottom}, 
									};

					SelectObject(hdc, hPenLight);
					Polyline(hdc, b1, 4);
					SelectObject(hdc, hPenDark);
					Polyline(hdc, b2, 3);
				}
				else
				{
					POINT b1[] = {	{curL,								rect.bottom-1}, 
									{curL,								rect.bottom-emt->heightTabSel+2}, 
									{curL+2,							rect.bottom-emt->heightTabSel}, 
									{curL+2*emt->marginSize+sz.cx-2 ,	rect.bottom-emt->heightTabSel} };
					POINT b2[] = {	{curL+2*emt->marginSize+sz.cx-2 ,	rect.bottom-emt->heightTabSel+1}, 
									{curL+2*emt->marginSize+sz.cx-1 ,	rect.bottom-emt->heightTabSel+2}, 
									{curL+2*emt->marginSize+sz.cx-1 ,	rect.bottom}   };

					SelectObject(hdc, hPenLight);
					Polyline(hdc, b1, 4);
					SelectObject(hdc, hPenDark);
					Polyline(hdc, b2, 3);
					selStart = curL;
					selEnd = curL+2*emt->marginSize+sz.cx-1;
				}
				curL += 2*emt->marginSize + sz.cx;
			}

			if (emt->drawBottomBorder)
			{
				POINT b3[] = {	{0 ,rect.bottom-1} , {curL ,rect.bottom-1} };
				SelectObject(hdc, hPenBottom);
				Polyline(hdc, b3, 2);
				if (emt->selected > -1)
				{
					POINT b4[] = {	{selStart ,rect.bottom-1} , {selEnd, rect.bottom-1} };
					SelectObject(hdc, hPenBottomSel);
					Polyline(hdc, b4, 2);
				}
			}

			int posx, posy; 
			posy = (rect.bottom-rect.top-sz.cy)/2;
			posx = 5;

			SelectObject(hdc, hOldFont);
			SelectObject(hdc, hOldPen);
			DeleteObject(hPenLight);
			DeleteObject(hPenDark);
			DeleteObject(hPenBottom);

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
		case WM_LBUTTONDOWN:
		{
			int xPos = (short)LOWORD(lParam);
			int yPos = (short)HIWORD(lParam);
			int cl = emt->getClickedTabNumber(xPos, yPos);
			
			emt->lastSelected = emt->selected;
			
			if (emt->selected == cl)
				emt->selected = -1;
			else
				emt->selected = cl;

			InvalidateRect(hwnd, NULL, false);

			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), (LPARAM)hwnd);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}


void EMTabs::updateLenghts()
{
	caps.createArray();
	lengths.freeList();
	
	for (int i=0; i<caps.objCount; i++)
	{
		if (!caps[i])
		{
			lengths.add(0);
			continue;
		}
		lengths.add((int)strlen(caps[i]));
	}

	lengths.createArray();
}

void EMTabs::createRegion(HDC &hdc)
{
	HRGN nReg;
	RECT rect;
	SIZE sz;
	int curL = 0;
	int w;
	int curP;
	ends.freeList();
	GetClientRect(hwnd, &rect);
	int nP = (caps.objCount+1)*3;
	if (selected > -1   &&   selected < caps.objCount)
		nP += 2;
	POINT * rp = (POINT*)malloc(sizeof(POINT)*nP);
	rp[0].x = 0;
	rp[0].y = rect.bottom;
	rp[1].x = 0;
	rp[1].y = rect.bottom-heightTab+2;
	curP = 2;
	for (int i=0; i<caps.objCount; i++)
	{
		if (caps[i])
		{
			if (i == selected)
				SelectObject(hdc, hBoldFont);
			else
				SelectObject(hdc, hFont);

			GetTextExtentPoint32(hdc, caps[i], lengths[i], &sz);
			w = sz.cx + 2*marginSize;
		}
		else
		{
			w = 2*marginSize;
		}

		if (i == selected)
		{
			rp[curP].x = curL;
			rp[curP].y = rect.bottom-heightTabSel+2;
			curP++;
			rp[curP].x = curL + 2;
			rp[curP].y = rect.bottom-heightTabSel;
			curP++;
			rp[curP].x = curL + w - 2;
			rp[curP].y = rect.bottom-heightTabSel;
			curP++;
			rp[curP].x = curL + w;
			rp[curP].y = rect.bottom-heightTabSel+2;
			curP++;
			rp[curP].x = curL + w;
			rp[curP].y = rect.bottom-heightTab+2;
			curP++;
		}
		else
		{
			rp[curP].x = curL + 2;
			rp[curP].y = rect.bottom-heightTab;
			curP++;
			rp[curP].x = curL + w - 2;
			rp[curP].y = rect.bottom-heightTab;
			curP++;
			rp[curP].x = curL + w;
			rp[curP].y = rect.bottom-heightTab+2;
			curP++;
		}
		
		curL += w;
		ends.add(curL);
	}

	ends.createArray();
	rp[curP].x = curL;
	rp[curP].y = rect.bottom;
	curP++;

	nReg = CreatePolygonRgn(rp, nP, WINDING);
	SetWindowRgn(hwnd, nReg, TRUE);

	DeleteObject(hRegion);
	if (rP)
		free(rP);
	rP = rp;

	hRegion = nReg;
}

int EMTabs::getClickedTabNumber(int x, int y)
{
	if (x<0)
		return -1;
	int ll = ends.objCount;
	for (int i=0; i<ll; i++)
	{
		if (x < ends[i])
			return i;
	}
	return -1;
}
