#define _CRT_RAND_S
#include "MatEditor.h"
#include "resource.h"
#include "valuesMinMax.h"
#include "XML_IO.h"
#include "log.h"
#include "Dialogs.h"
#include <mbstring.h>

extern HMODULE hDllModule;
extern ThemeManager gTheme;
extern HBITMAP gIcons;

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::applyControlStyles()
{
	// 774 x 655 - client with lib
	RECT clientRect;
	RECT windowRect;
	GetWindowRect(hMain, &windowRect);
	GetClientRect(hMain, &clientRect);
	clientToWindowX = (windowRect.right - windowRect.left) - (clientRect.right - clientRect.left);
	clientToWindowY = (windowRect.bottom - windowRect.top) - (clientRect.bottom - clientRect.top);

	SetWindowPos(hMain, HWND_TOP, 0,0, 774, 662, SWP_NOOWNERZORDER | SWP_NOMOVE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW), HWND_TOP, 0,0, 192, 192, SWP_NOOWNERZORDER | SWP_NOMOVE);
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();
	if (empv->byteBuff->width!=192  ||  empv->byteBuff->height!=192)
	{
		empv->byteBuff->allocBuffer(192,192);
		empv->byteBuff->clearBuffer();
	}

	int x,y;

	// MATERIAL PREVIEW GROUP
	x = 14;   y = 14;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR1),			HWND_TOP,  x,      y,       192,  18,   SWP_NOOWNERZORDER);	// Material preview
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW),		HWND_TOP,  x,      y+25,    192, 192,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_START),			HWND_TOP,  x+25,   y+220,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_STOP),			HWND_TOP,  x+61,   y+220,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ZOOM),			HWND_TOP,  x+97,   y+220,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFRESH),		HWND_TOP,  x+133,  y+220,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_CHOOSE_SCENE),	HWND_TOP,  x,      y+256,   192, 130,   SWP_NOOWNERZORDER);

	// MATERIAL GROUP
	x = 14;   y = 297;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR2),			HWND_TOP,   x,     y,      192,  18,   SWP_NOOWNERZORDER);	// Material
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT12),			HWND_TOP,   x-1,   y+26,    32,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_NAME),			HWND_TOP,   x+36,  y+25,   156,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LOADMAT),		HWND_TOP,   x-4,   y+43,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_SAVEMAT),		HWND_TOP,   x+32,  y+43,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ARCHIVE_MAT),	HWND_TOP,   x+68,  y+43,    32,  32,   SWP_NOOWNERZORDER);

	// LIBRARY GROUP
	x = 14;  y = 376;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR11),			HWND_TOP,   x,      y,      192,  18,   SWP_NOOWNERZORDER);	// Library
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_SHOWHIDE_LIB),	HWND_TOP,   x-4,    y+21,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH),	HWND_TOP,   x+29,   y+21,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LOAD_LOCALLIB),	HWND_TOP,   x+62,   y+21,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DOWNLOADMAT),	HWND_TOP,   x+62,   y+21,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_UPLOADMAT),		HWND_TOP,   x+95,   y+21,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ADD_LOCALLIB),	HWND_TOP,   x+128,  y+21,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DEL_LOCALLIB),	HWND_TOP,   x+161,  y+21,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_USER_ONLINE),	HWND_TOP,   x+161,  y+21,    32,  32,   SWP_NOOWNERZORDER);

	// LAYERS GROUP
	x = 213;  y = 14;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR3),			HWND_TOP,  x,      y,      270,  18,   SWP_NOOWNERZORDER);	// Layers
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LAYERS_LIST),	HWND_TOP,  x,      y+25,   227, 128,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LAYER_ADD),		HWND_TOP,  x+240,  y+25,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LAYER_DEL),		HWND_TOP,  x+240,  y+58,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LAYER_CLONE),	HWND_TOP,  x+240,  y+90,    32,  32,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LAYER_RESET),	HWND_TOP,  x+240,  y+123,   32,  32,   SWP_NOOWNERZORDER);

	// WEIGHT GROUP
	x = 213;  y = 174;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR4),			HWND_TOP,  x,      y,      270,  18,   SWP_NOOWNERZORDER);	// Weight
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT1),			HWND_TOP,  x+5,    y+26,    40,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT),			HWND_TOP,  x+94,   y+25,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT_TEX),		HWND_TOP,  x+223,  y+25,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT_TEX_ON),	HWND_TOP,  x+255,  y+27,    15,  15,   SWP_NOOWNERZORDER);

	// GEOMETRY GROUP
	x = 213;  y = 238;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR9),				HWND_TOP,  x,      y,     270,  18,   SWP_NOOWNERZORDER);	// Geometry
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT15),				HWND_TOP,  x+6,    y+27,   70,  13,   SWP_NOOWNERZORDER);	// Displacement:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_ON),			HWND_TOP,  x+95,   y+26,   15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_TEX),			HWND_TOP,  x+112,  y+25,   30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT31),				HWND_TOP,  x+185,  y+27,   50,  13,   SWP_NOOWNERZORDER);	// Continuity:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_CONTINUITY),	HWND_TOP,  x+255,  y+26,   15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT17),				HWND_TOP,  x+6,    y+48,   32,  13,   SWP_NOOWNERZORDER);	// Depth:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_DEPTH),		HWND_TOP,  x+95,   y+47,   54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT32),				HWND_TOP,  x+185,  y+48,   30,  13,   SWP_NOOWNERZORDER);	// Shift:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_SHIFT),		HWND_TOP,  x+216,  y+47,   54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT30),				HWND_TOP,  x+6,    y+71,   70,  13,   SWP_NOOWNERZORDER);	// Subdivisions:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_SUBDIVS),		HWND_TOP,  x+95,   y+68,   50, 130,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT33),				HWND_TOP,  x+150,  y+71,   46,  13,   SWP_NOOWNERZORDER);	// Normals:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_NORMALS),		HWND_TOP,  x+197,  y+68,   73, 130,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT34),				HWND_TOP,  x+6,    y+92,   80,  13,   SWP_NOOWNERZORDER);	// Ignore scale:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_IGNORE_SCALE),	HWND_TOP,  x+95,   y+91,   15,  15,   SWP_NOOWNERZORDER);

	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT16),				HWND_TOP,  x+6,    y+113,   80,  13,   SWP_NOOWNERZORDER);	// Normal mapping:
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_NORMAL_ON),			HWND_TOP,  x+95,   y+112,   15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_NORMAL_TEX),			HWND_TOP,  x+112,  y+111,   48,  18,   SWP_NOOWNERZORDER);

	// EMISSION GROUP
	x = 213;  y = 330;
	y = 376;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR5),						HWND_TOP,  x,      y,      270,  18,   SWP_NOOWNERZORDER);	// Emission
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT2),						HWND_TOP,  x+6,    y+26,    42,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_ON),				HWND_TOP,  x+95,   y+25,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT25),						HWND_TOP,  x+197,  y+26,    52,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_SKYPORTAL),			HWND_TOP,  x+255,  y+25,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT3),						HWND_TOP,  x+6,    y+47,    30,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR),				HWND_TOP,  x+95,   y+43,    21,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB),			HWND_TOP,  x+119,  y+41,    25,  25,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE), HWND_TOP,  x+144,  y+41,    25,  25,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX),			HWND_TOP,  x+223,  y+44,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON),		HWND_TOP,  x+255,  y+44,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT4),						HWND_TOP,  x+6,    y+68,    32,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_POWER),				HWND_TOP,  x+95,   y+67,    70,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_UNIT),				HWND_TOP,  x+217,  y+66,    53,  130,  SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT22),						HWND_TOP,  x+6,    y+93,    55,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES),		HWND_TOP,  x+95,   y+90,   175,  130,  SWP_NOOWNERZORDER );
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT26),						HWND_TOP,  x+6,    y+118,   55,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_IES_LOAD),					HWND_TOP,  x+95,   y+115,   55,  20,   SWP_NOOWNERZORDER );
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_IES_UNLOAD),					HWND_TOP,  x+155,  y+115,   55,  20,   SWP_NOOWNERZORDER );
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_IES_PREVIEW),				HWND_TOP,  x+215,  y+115,   55,  20,   SWP_NOOWNERZORDER );
	
	// REFLECTANCE GROUP
	x = 490;  y = 14;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR6),					HWND_TOP,  x,      y,      270,  18,   SWP_NOOWNERZORDER);	// Reflectance
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT5),					HWND_TOP,  x+5,    y+27,    60,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS),				HWND_TOP,  x+80,   y+25,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS_TEX),			HWND_TOP,  x+215,  y+25,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON),		HWND_TOP,  x+246,  y+26,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT6),					HWND_TOP,  x+5,    y+48,    60,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR),			HWND_TOP,  x+81,   y+46,    21,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_RGB),		HWND_TOP,  x+105,  y+44,    25,  25,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_TEX),		HWND_TOP,  x+215,  y+47,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON),		HWND_TOP,  x+246,  y+49,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT7),					HWND_TOP,  x+5,    y+73,    67,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR),			HWND_TOP,  x+81,   y+69,    21,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_RGB),		HWND_TOP,  x+105,  y+67,    25,  25,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_TEX),		HWND_TOP,  x+215,  y+70,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_TEX_ON),	HWND_TOP,  x+246,  y+72,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_REFL90_LINKED),			HWND_TOP,  x+151,  y+60,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT29),					HWND_TOP,  x+169,  y+61,    23,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT18),					HWND_TOP,  x+5,    y+94,    54,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY),				HWND_TOP,  x+80,   y+93,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY_TEX),			HWND_TOP,  x+215,  y+91,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON),		HWND_TOP,  x+246,  y+93,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT19),					HWND_TOP,  x+5,    y+115,   32,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE),			HWND_TOP,  x+80,   y+112,   54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX),		HWND_TOP,  x+215,  y+112,   30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON),		HWND_TOP,  x+246,  y+114,   15,  15,   SWP_NOOWNERZORDER);

	// FRESNEL GROUP
	x = 490;  y = 174;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR7),					HWND_TOP,  x,      y,       270, 18,   SWP_NOOWNERZORDER);	// Fresnel
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT8),					HWND_TOP,  x+5,    y+28,    24,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_IOR),					HWND_TOP,  x+80,   y+25,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_FRESNEL_ADVANCED),		HWND_TOP,  x+140,  y+24,    62,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_FRESNEL),				HWND_TOP,  x+227,  y+20,    42,  42,   SWP_NOOWNERZORDER);

	// TRANSMISSION GROUP
	x = 490;  y = 238;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR8),					HWND_TOP,  x,      y,      270, 18,   SWP_NOOWNERZORDER);	// Transmission
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT9),					HWND_TOP,  x+5,    y+26,    64,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_ON),		HWND_TOP,  x+80,   y+25,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR),		HWND_TOP,  x+101,  y+21,    21,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_RGB),	HWND_TOP,  x+126,  y+20,    23,  23,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX),	HWND_TOP,  x+224,  y+24,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON),	HWND_TOP,  x+255,  y+25,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT11),					HWND_TOP,  x+5,    y+47,    54,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_ON),			HWND_TOP,  x+80,   y+46,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR),		HWND_TOP,  x+101,  y+44,    21,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB),	HWND_TOP,  x+126,  y+43,    23,  23,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT13),					HWND_TOP,  x+159,  y+47,    54,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE),	HWND_TOP,  x+216,  y+46,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT14),					HWND_TOP,  x+5,    y+68,    54,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_ON),			HWND_TOP,  x+80,   y+67,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT24),					HWND_TOP,  x+159,  y+68,    54,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_SIZE),		HWND_TOP,  x+216,  y+67,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT10),					HWND_TOP,  x+5,    y+89,    54,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_FAKE_GLASS_ON),			HWND_TOP,  x+80,   y+88,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT20),					HWND_TOP,  x+5,    y+110,    40,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_OPACITY),				HWND_TOP,  x+79,   y+109,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_OPACITY_TEX),			HWND_TOP,  x+224,  y+109,    30,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_OPACITY_TEX_ON),			HWND_TOP,  x+255,  y+110,    15,  15,   SWP_NOOWNERZORDER);

	// SSS GROUP
	x = 490;  y = 376;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_GR10),				HWND_TOP,  x,     y,      270, 18,   SWP_NOOWNERZORDER);	// SSS
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT21),				HWND_TOP,  x+5,   y+24,    24,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_SSS_ON),				HWND_TOP,  x+80,  y+23,    15,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT27),				HWND_TOP,  x+5,   y+45,    60,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_SSS_DENSITY),		HWND_TOP,  x+80,  y+44,    54,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT28),				HWND_TOP,  x+5,   y+64,    60,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_SSS_COLLISION_DIR),	HWND_TOP,  x+80,  y+65,    54,  18,   SWP_NOOWNERZORDER);
	

	// ---- MATERIAL LIBRARY ----
	x = 14;  y = 519;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_LINE),			HWND_TOP,  x,      y,      746,   2,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_TEXT23),				HWND_TOP,  x+540,  y+14,    40,  13,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY),	HWND_TOP,  x+582,  y+13,   134,  15,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_SEARCH_OK),		HWND_TOP,  x+722,  y+12,    24,  18,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_PAGES),			HWND_TOP,  x+586,  y+163,  139,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_LEFT),			HWND_TOP,  x-2,    y+98,    23,  23,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_RIGHT),			HWND_TOP,  x+726,  y+98,    23,  23,   SWP_NOOWNERZORDER);

	// CATEGORIES
	x = 13;  y = 529;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_BRICKS),		HWND_TOP,   x,      y,       50,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_CARPAINT),	HWND_TOP,   x+50,   y,       63,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_CERAMIC),	HWND_TOP,   x+113,  y,       60,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_CONCRETE),	HWND_TOP,   x+173,  y,       69,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_EMITTER),	HWND_TOP,   x+242,  y,       58,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_FABRIC),		HWND_TOP,   x+300,  y,       50,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_FOOD),		HWND_TOP,   x+350,  y,       42,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_GLASS),		HWND_TOP,   x+392,  y,       47,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_GROUND),		HWND_TOP,   x+439,  y,       58,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_LEATHER),	HWND_TOP,   x,      y+25,    59,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_LIQUIDS),	HWND_TOP,   x+59,   y+25,    54,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_METAL),		HWND_TOP,   x+113,  y+25,    46,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_MINERALS),	HWND_TOP,   x+159,  y+25,    97,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_ORGANIC),	HWND_TOP,   x+256,  y+25,    60,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_PLASTIC),	HWND_TOP,   x+316,  y+25,    55,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_PORCELAIN),	HWND_TOP,   x+371,  y+25,    71,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_SSS),		HWND_TOP,   x+442,  y+25,    34,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_STONE),		HWND_TOP,   x+476,  y+25,    47,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_TILES),		HWND_TOP,   x+523,  y+25,    41,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_WOOD),		HWND_TOP,   x+564,  y+25,    46,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_WALLS),		HWND_TOP,   x+610,  y+25,    47,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_OTHER),		HWND_TOP,   x+657,  y+25,    49,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_CAT_USER),		HWND_TOP,   x+706,  y+25,    42,  22,   SWP_NOOWNERZORDER);

	// PREVIEWS
	x = 41;  y = 581;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB1),	HWND_TOP,  x,       y,  96,  96,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB2),	HWND_TOP,  x+99,    y,  96,  96,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB3),	HWND_TOP,  x+198,   y,  96,  96,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB4),	HWND_TOP,  x+297,   y,  96,  96,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB5),	HWND_TOP,  x+396,   y,  96,  96,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB6),	HWND_TOP,  x+495,   y,  96,  96,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB7),	HWND_TOP,  x+594,   y,  96,  96,   SWP_NOOWNERZORDER);

	// OK CANCEL
	x = 349;  y = 684;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_OK),		HWND_TOP,  x-45, y,    72,  20,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_CANCEL),	HWND_TOP,  x+45, y,    72,  20,   SWP_NOOWNERZORDER);

	// GROUPS
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR1)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR2)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR3)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR4)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR5)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR6)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR7)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR8)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR9)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR10)));
	GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT_GR11)));

	// TEXTS:
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT1 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT2 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT3 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT4 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT5 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT6 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT7 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT8 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT9 )), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT10)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT11)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT12)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT13)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT14)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT15)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT16)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT17)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT18)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT19)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT20)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT21)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT22)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT23)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT24)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT25)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT26)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT27)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT28)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT29)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT30)), false, true);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT31)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT32)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT33)), false, false);
	GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hMain, IDC_MATEDIT_TEXT34)), false, true);

	// UNDER PREVIEW BUTTONS
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_START)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_STOP)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ZOOM)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFRESH)));
	GlobalWindowSettings::colorSchemes.apply(GetEMComboBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_CHOOSE_SCENE)));
	EMComboBox * emcbscene = GetEMComboBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_CHOOSE_SCENE));
	emcbscene->CreateRegions(emcbscene->hRgnClosed, emcbscene->hRgnOpened);


	// LOAD/SAVE SHIT
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSimpleInstance(GetDlgItem(hMain,IDC_MATEDIT_NAME)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LOADMAT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_SAVEMAT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_DOWNLOADMAT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_UPLOADMAT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ARCHIVE_MAT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LOAD_LOCALLIB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_DEL_LOCALLIB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ADD_LOCALLIB)));
	EMEditSimple * emesname = GetEMEditSimpleInstance(GetDlgItem(hMain,IDC_MATEDIT_NAME));
	SetWindowPos(emesname->hEdit, HWND_TOP,   0, 0,   154,  13,   SWP_NOOWNERZORDER | SWP_NOMOVE);

	// LAYERS
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_ADD)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_DEL)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_CLONE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_RESET)));
	GlobalWindowSettings::colorSchemes.apply(GetEMListViewInstance(GetDlgItem( hMain, IDC_MATEDIT_LAYERS_LIST)));

	// WEIGHT
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_WEIGHT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_WEIGHT_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_WEIGHT_TEX_ON)));

	// EMISSION
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_SKYPORTAL)));
	GlobalWindowSettings::colorSchemes.apply(GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_POWER)));
	GlobalWindowSettings::colorSchemes.apply(GetEMComboBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_UNIT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMComboBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_EMISSION_COLOR_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON)));
	EMComboBox * emcbunit = GetEMComboBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_UNIT));
	emcbunit->CreateRegions(emcbunit->hRgnClosed, emcbunit->hRgnOpened);
	EMComboBox * emcblayers = GetEMComboBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES));
	emcblayers->CreateRegions(emcbunit->hRgnClosed, emcbunit->hRgnOpened);
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_IES_LOAD)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_IES_UNLOAD)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_IES_PREVIEW)));

	// REFLECTANCE
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_ROUGHNESS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_ROUGHNESS_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_RGB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_REFL0_COLOR_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_RGB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_REFL90_COLOR_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_REFL90_COLOR_TEX_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_REFL90_LINKED)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_ANISOTROPY)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_ANISOTROPY_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_ANISO_ANGLE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_ANISO_ANGLE_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON)));

	// FRESNEL
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_IOR)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_FRESNEL_ADVANCED)));
	GlobalWindowSettings::colorSchemes.apply(GetEMFresnelInstance(GetDlgItem(  hMain, IDC_MATEDIT_FRESNEL)));

	// TRANSMISSION
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_TRANSMISSION_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_RGB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_FAKE_GLASS_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_DISPERSION_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_DISPERSION_SIZE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_ABSORPTION_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_ABSORPTION_DISTANCE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem( hMain, IDC_MATEDIT_OPACITY)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_OPACITY_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem( hMain, IDC_MATEDIT_OPACITY_TEX_ON)));
	//GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(   hMain, IDC_MATEDIT_OPACITY_SETALL)));

	// GEOMETRY
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem(hMain,  IDC_MATEDIT_DISPL_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_DISPL_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem(hMain,  IDC_MATEDIT_DISPL_DEPTH)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem(hMain,  IDC_MATEDIT_NORMAL_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_NORMAL_TEX)));
	GlobalWindowSettings::colorSchemes.apply(GetEMComboBoxInstance(GetDlgItem(hMain,  IDC_MATEDIT_DISPL_SUBDIVS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMComboBoxInstance(GetDlgItem(hMain,  IDC_MATEDIT_DISPL_NORMALS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem(hMain,  IDC_MATEDIT_DISPL_CONTINUITY)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem(hMain,  IDC_MATEDIT_DISPL_SHIFT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem(hMain,  IDC_MATEDIT_DISPL_IGNORE_SCALE)));


	// SSS
	GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem(hMain,  IDC_MATEDIT_SSS_ON)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem(hMain,  IDC_MATEDIT_SSS_DENSITY)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSpinInstance(GetDlgItem(hMain,  IDC_MATEDIT_SSS_COLLISION_DIR)));
	

	// LIBRARY
	GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB1)));
	GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB2)));
	GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB3)));
	GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB4)));
	GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB5)));
	GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB6)));
	GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB7)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain,	IDC_MATEDIT_LIB_LEFT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain,	IDC_MATEDIT_LIB_RIGHT)));

	GlobalWindowSettings::colorSchemes.apply(GetEMLineInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB_LINE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMEditSimpleInstance(GetDlgItem(hMain,	IDC_MATEDIT_LIB_SEARCH_QUERY)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB_SEARCH_OK)));
	GlobalWindowSettings::colorSchemes.apply(GetEMPagesInstance(GetDlgItem(hMain,		IDC_MATEDIT_LIB_PAGES)));
	GetEMPagesInstance(GetDlgItem(hMain, IDC_MATEDIT_LIB_PAGES))->setCurrentPage(1,1);
	EMEditSimple * emesquery = GetEMEditSimpleInstance(GetDlgItem(hMain,IDC_MATEDIT_LIB_SEARCH_QUERY));
	SetWindowPos(emesquery->hEdit, HWND_TOP,   0, 0,   132,  13,   SWP_NOOWNERZORDER | SWP_NOMOVE);


	// OK/CANCEL
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_OK)));
	GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_CANCEL)));


	// LIB CONTROL
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,		IDC_MATEDIT_SHOWHIDE_LIB)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgButtonInstance(GetDlgItem(hMain,			IDC_MATEDIT_USER_ONLINE)));

	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_STONE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_TILES)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WALLS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WOOD)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_EMITTER)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FABRIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FOOD)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GLASS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GROUND)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LEATHER)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LIQUIDS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_SSS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_MINERALS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PLASTIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_ORGANIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PORCELAIN)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_BRICKS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CONCRETE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CERAMIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CARPAINT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_METAL)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_OTHER)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_USER)));

	for (int i=0; i<7; i++)
	{
		EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, libIDs[i]));
		if (!empv)
			continue;
		if (!empv->byteBuff)
			empv->byteBuff = new ImageByteBuffer();
		empv->byteBuff->allocBuffer(96,96);
		empv->byteBuff->clearBuffer();
	}

	// allow drag/drop colors
	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR))->setAllowedDragDrop();
	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR))->setAllowedDragDrop();
	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR))->setAllowedDragDrop();
	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR))->setAllowedDragDrop();
	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR))->setAllowedDragDrop();

	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR))->redraw(Color4(1,0,0));
	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR))->redraw(Color4(1,1,0));
	GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR))->redraw(Color4(0,0,1));

	//////// ----- IMAGE BUTTONS

	// start button
	EMImgButton * emstart = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_START));
	emstart->bmAll = &(gIcons);
	emstart->pNx = emstart->pMx = emstart->pCx = emstart->pDx = 0;
	emstart->pNy = 0;
	emstart->pMy = 32;
	emstart->pCy = 64;
	emstart->pDy = 96;

	// stop button
	EMImgButton * emstop = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_STOP));
	emstop->bmAll = &(gIcons);
	emstop->pNx = emstop->pMx = emstop->pCx = emstop->pDx = 64;
	emstop->pNy = 0;
	emstop->pMy = 32;
	emstop->pCy = 64;
	emstop->pDy = 96;

	// refresh button
	EMImgButton * emrefresh = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFRESH));
	emrefresh->bmAll = &(gIcons);
	emrefresh->pNx = emrefresh->pMx = emrefresh->pCx = emrefresh->pDx = 448;
	emrefresh->pNy = 0;
	emrefresh->pMy = 32;
	emrefresh->pCy = 64;
	emrefresh->pDy = 96;

	// zoom button
	EMImgButton * emzoom = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ZOOM));
	emzoom->bmAll = &(gIcons);
	emzoom->pNx = emzoom->pMx = emzoom->pCx = emzoom->pDx = 224;
	emzoom->pNy = 0;
	emzoom->pMy = 32;
	emzoom->pCy = 64;
	emzoom->pDy = 96;

	// load mat button
	EMImgButton * emload = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LOADMAT));
	emload->bmAll = &(gIcons);
	emload->pNx = emload->pMx = emload->pCx = emload->pDx = 736;
	emload->pNy = 0;
	emload->pMy = 32;
	emload->pCy = 64;
	emload->pDy = 96;

	// save mat button
	EMImgButton * emsave = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_SAVEMAT));
	emsave->bmAll = &(gIcons);
	emsave->pNx = emsave->pMx = emsave->pCx = emsave->pDx = 704;
	emsave->pNy = 0;
	emsave->pMy = 32;
	emsave->pCy = 64;
	emsave->pDy = 96;

	// download mat button
	EMImgButton * emdownload = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_DOWNLOADMAT));
	emdownload->bmAll = &(gIcons);
	emdownload->pNx = emdownload->pMx = emdownload->pCx = emdownload->pDx = 1056;
	emdownload->pNy = 0;
	emdownload->pMy = 32;
	emdownload->pCy = 64;
	emdownload->pDy = 96;

	// upload mat button
	EMImgButton * emupload = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_UPLOADMAT));
	emupload->bmAll = &(gIcons);
	emupload->pNx = emupload->pMx = emupload->pCx = emupload->pDx = 1088;
	emupload->pNy = 0;
	emupload->pMy = 32;
	emupload->pCy = 64;
	emupload->pDy = 96;

	// emission rgb color button
	EMImgButton * ememrgb = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB));
	ememrgb->bmAll = &(gIcons);
	ememrgb->pNx = ememrgb->pMx = ememrgb->pCx = ememrgb->pDx = 868;
	ememrgb->pNy = 4;
	ememrgb->pMy = 36;
	ememrgb->pCy = 68;
	ememrgb->pDy = 100;

	// emission temperature color button
	EMImgButton * ememtemp = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE));
	ememtemp->bmAll = &(gIcons);
	ememtemp->pNx = ememtemp->pMx = ememtemp->pCx = ememtemp->pDx = 836;
	ememtemp->pNy = 4;
	ememtemp->pMy = 36;
	ememtemp->pCy = 68;
	ememtemp->pDy = 100;

	// reflection 0 rgb color button
	EMImgButton * emc0rgb = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_RGB));
	emc0rgb->bmAll = &(gIcons);
	emc0rgb->pNx = emc0rgb->pMx = emc0rgb->pCx = emc0rgb->pDx = 868;
	emc0rgb->pNy = 4;
	emc0rgb->pMy = 36;
	emc0rgb->pCy = 68;
	emc0rgb->pDy = 100;

	// reflection 90 rgb color button
	EMImgButton * emc90rgb = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_RGB));
	emc90rgb->bmAll = &(gIcons);
	emc90rgb->pNx = emc90rgb->pMx = emc90rgb->pCx = emc90rgb->pDx = 868;
	emc90rgb->pNy = 4;
	emc90rgb->pMy = 36;
	emc90rgb->pCy = 68;
	emc90rgb->pDy = 100;

	// absorption rgb color button
	EMImgButton * emabsrgb = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB));
	emabsrgb->bmAll = &(gIcons);
	emabsrgb->pNx = emabsrgb->pMx = emabsrgb->pCx = emabsrgb->pDx = 869;
	emabsrgb->pNy = 5;
	emabsrgb->pMy = 37;
	emabsrgb->pCy = 69;
	emabsrgb->pDy = 101;

	// transmission rgb color button
	EMImgButton * emtrrgb = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_RGB));
	emtrrgb->bmAll = &(gIcons);
	emtrrgb->pNx = emtrrgb->pMx = emtrrgb->pCx = emtrrgb->pDx = 869;
	emtrrgb->pNy = 5;
	emtrrgb->pMy = 37;
	emtrrgb->pCy = 69;
	emtrrgb->pDy = 101;

	// add layer button
	EMImgButton * emlayadd = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_ADD));
	emlayadd->bmAll = &(gIcons);
	emlayadd->pNx = emlayadd->pMx = emlayadd->pCx = emlayadd->pDx = 896;
	emlayadd->pNy = 0;
	emlayadd->pMy = 32;
	emlayadd->pCy = 64;
	emlayadd->pDy = 96;

	// del layer button
	EMImgButton * emlaydel = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_DEL));
	emlaydel->bmAll = &(gIcons);
	emlaydel->pNx = emlaydel->pMx = emlaydel->pCx = emlaydel->pDx = 928;
	emlaydel->pNy = 0;
	emlaydel->pMy = 32;
	emlaydel->pCy = 64;
	emlaydel->pDy = 96;

	// clone layer button
	EMImgButton * emlayclone = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_CLONE));
	emlayclone->bmAll = &(gIcons);
	emlayclone->pNx = emlayclone->pMx = emlayclone->pCx = emlayclone->pDx = 960;
	emlayclone->pNy = 0;
	emlayclone->pMy = 32;
	emlayclone->pCy = 64;
	emlayclone->pDy = 96;

	// reset layer button
	EMImgButton * emlayreset = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_RESET));
	emlayreset->bmAll = &(gIcons);
	emlayreset->pNx = emlayreset->pMx = emlayreset->pCx = emlayreset->pDx = 992;
	emlayreset->pNy = 0;
	emlayreset->pMy = 32;
	emlayreset->pCy = 64;
	emlayreset->pDy = 96;

	// archive material button
	EMImgButton * emarchive = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ARCHIVE_MAT));
	emarchive->bmAll = &(gIcons);
	emarchive->pNx = emarchive->pMx = emarchive->pCx = emarchive->pDx = 1152;
	emarchive->pNy = 0;
	emarchive->pMy = 32;
	emarchive->pCy = 64;
	emarchive->pDy = 96;

	// show/hide lib button
	EMImgStateButton * emshlib = GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_SHOWHIDE_LIB));
	emshlib->allowUnclick = true;
	emshlib->colBorderSelected = emshlib->colBorder;
	emshlib->bmAll = &(gIcons);
	emshlib->pNx = emshlib->pMx = emshlib->pCx = emshlib->pDx = 1024;
	emshlib->pNy = 0;
	emshlib->pMy = 32;
	emshlib->pCy = 64;
	emshlib->pDy = 96;

	// online/offline lib button
	EMImgStateButton * emonline = GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH));
	emonline->allowUnclick = true;
	emonline->colBorderSelected = emonline->colBorder;
	emonline->bmAll = &(gIcons);
	emonline->pNx = emonline->pMx = emonline->pCx = emonline->pDx = 1184;
	emonline->pNy = 0;
	emonline->pMy = 32;
	emonline->pCy = 64;
	emonline->pDy = 96;

	// user/pass lib button
	EMImgButton * emuserpass = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_USER_ONLINE));
	emuserpass->bmAll = &(gIcons);
	emuserpass->pNx = emuserpass->pMx = emuserpass->pCx = emuserpass->pDx = 1120;
	emuserpass->pNy = 0;
	emuserpass->pMy = 32;
	emuserpass->pCy = 64;
	emuserpass->pDy = 96;

	// add local lib button
	EMImgButton * emaddlocal = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ADD_LOCALLIB));
	emaddlocal->bmAll = &(gIcons);
	emaddlocal->pNx = emaddlocal->pMx = emaddlocal->pCx = emaddlocal->pDx = 1216;
	emaddlocal->pNy = 0;
	emaddlocal->pMy = 32;
	emaddlocal->pCy = 64;
	emaddlocal->pDy = 96;

	// del local lib button
	EMImgButton * emdellocal = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_DEL_LOCALLIB));
	emdellocal->bmAll = &(gIcons);
	emdellocal->pNx = emdellocal->pMx = emdellocal->pCx = emdellocal->pDx = 1248;
	emdellocal->pNy = 0;
	emdellocal->pMy = 32;
	emdellocal->pCy = 64;
	emdellocal->pDy = 96;

	// load local lib button
	EMImgButton * emloadlocal = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LOAD_LOCALLIB));
	emloadlocal->bmAll = &(gIcons);
	emloadlocal->pNx = emloadlocal->pMx = emloadlocal->pCx = emloadlocal->pDx = 1280;
	emloadlocal->pNy = 0;
	emloadlocal->pMy = 32;
	emloadlocal->pCy = 64;
	emloadlocal->pDy = 96;


	// lib left button
	EMImgButton * emlibleft = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LIB_LEFT));
	emlibleft->bmAll = &(gIcons);
	emlibleft->showBorder = false;
	emlibleft->pNx = emlibleft->pMx = emlibleft->pCx = emlibleft->pDx = 1314;
	emlibleft->pNy = 2;
	emlibleft->pMy = 34;
	emlibleft->pCy = 66;
	emlibleft->pDy = 98;

	// lib right button
	EMImgButton * emlibright = GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LIB_RIGHT));
	emlibright->bmAll = &(gIcons);
	emlibright->showBorder = false;
	emlibright->pNx = emlibright->pMx = emlibright->pCx = emlibright->pDx = 1346;
	emlibright->pNy = 2;
	emlibright->pMy = 34;
	emlibright->pCy = 66;
	emlibright->pDy = 98;


	// subdivisions
	EMComboBox * emsdiv = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_SUBDIVS));
	emsdiv->deleteItems();
	emsdiv->addItem("4");
	emsdiv->addItem("16");
	emsdiv->addItem("64");
	emsdiv->addItem("256");
	emsdiv->addItem("1K");
	emsdiv->addItem("4K");
	emsdiv->addItem("16K");
	emsdiv->addItem("64K");
	emsdiv->selected = 5;

	// displacement normals
	EMComboBox * emnrm = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_NORMALS));
	emnrm->deleteItems();
	emnrm->addItem("Smooth");
	emnrm->addItem("Face");
	emnrm->addItem("As source");
	emnrm->selected = 0;


	// emission units
	EMComboBox * emunit = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_UNIT));
	emunit->deleteItems();
	emunit->addItem("W");
	emunit->addItem("W/m2");
	emunit->addItem("lm");
	emunit->addItem("lx");
	emunit->selected = 0;


	// blend names
	EMComboBox * emblayers = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES));
	emblayers->deleteItems();
	for (int i=0; i<16; i++)
	{
		char * aaa = blendNames.names[i];
		if (!aaa)
			aaa = "Unnamed";
		char buf[2048];
		sprintf_s(buf, 2048, "%d: %s", (i+1), aaa);
		emblayers->addItem(buf);
	}
	emblayers->selected = 0;

	iconsGoToOnline(false);

	EMImgStateButton * emCatStone		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_STONE));
	EMImgStateButton * emCatTiles		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_TILES));
	EMImgStateButton * emCatWalls		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WALLS));
	EMImgStateButton * emCatWood		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WOOD));
	EMImgStateButton * emCatEmitter		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_EMITTER));
	EMImgStateButton * emCatFabric		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FABRIC));
	EMImgStateButton * emCatFood		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FOOD));
	EMImgStateButton * emCatGlass		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GLASS));
	EMImgStateButton * emCatGround		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GROUND));
	EMImgStateButton * emCatLeather		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LEATHER));
	EMImgStateButton * emCatLiquids		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LIQUIDS));
	EMImgStateButton * emCatSSS			= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_SSS));
	EMImgStateButton * emCatMinerals	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_MINERALS));
	EMImgStateButton * emCatPlastic		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PLASTIC));
	EMImgStateButton * emCatOrganic		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_ORGANIC));
	EMImgStateButton * emCatPorcelain	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PORCELAIN));
	EMImgStateButton * emCatBricks		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_BRICKS));
	EMImgStateButton * emCatConcrete	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CONCRETE));
	EMImgStateButton * emCatCeramic		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CERAMIC));
	EMImgStateButton * emCatCarpaint	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CARPAINT));
	EMImgStateButton * emCatMetal		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_METAL));
	EMImgStateButton * emCatOther		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_OTHER));
	EMImgStateButton * emCatUser		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_USER));

	emCatStone->bmAll = &(gIcons);
	emCatStone->allowUnclick = false;
	emCatStone->colBorderSelected = emCatStone->colBorder;
	emCatStone->pNx = emCatStone->pMx = emCatStone->pCx = emCatStone->pDx = 320;
	emCatStone->pNy = 128;
	emCatStone->pMy = 156;
	emCatStone->pCy = 184;
	emCatStone->pDy = 212;

	emCatTiles->bmAll = &(gIcons);
	emCatTiles->allowUnclick = false;
	emCatTiles->colBorderSelected = emCatTiles->colBorder;
	emCatTiles->pNx = emCatTiles->pMx = emCatTiles->pCx = emCatTiles->pDx = 273;
	emCatTiles->pNy = 128;
	emCatTiles->pMy = 156;
	emCatTiles->pCy = 184;
	emCatTiles->pDy = 212;

	emCatWalls->bmAll = &(gIcons);
	emCatWalls->allowUnclick = false;
	emCatWalls->colBorderSelected = emCatWalls->colBorder;
	emCatWalls->pNx = emCatWalls->pMx = emCatWalls->pCx = emCatWalls->pDx = 220;
	emCatWalls->pNy = 128;
	emCatWalls->pMy = 156;
	emCatWalls->pCy = 184;
	emCatWalls->pDy = 212;

	emCatWood->bmAll = &(gIcons);
	emCatWood->allowUnclick = false;
	emCatWood->colBorderSelected = emCatWood->colBorder;
	emCatWood->pNx = emCatWood->pMx = emCatWood->pCx = emCatWood->pDx = 168;
	emCatWood->pNy = 128;
	emCatWood->pMy = 156;
	emCatWood->pCy = 184;
	emCatWood->pDy = 212;

	emCatEmitter->bmAll = &(gIcons);
	emCatEmitter->allowUnclick = false;
	emCatEmitter->colBorderSelected = emCatEmitter->colBorder;
	emCatEmitter->pNx = emCatEmitter->pMx = emCatEmitter->pCx = emCatEmitter->pDx = 104;
	emCatEmitter->pNy = 128;
	emCatEmitter->pMy = 156;
	emCatEmitter->pCy = 184;
	emCatEmitter->pDy = 212;

	emCatFabric->bmAll = &(gIcons);
	emCatFabric->allowUnclick = false;
	emCatFabric->colBorderSelected = emCatFabric->colBorder;
	emCatFabric->pNx = emCatFabric->pMx = emCatFabric->pCx = emCatFabric->pDx = 1010;
	emCatFabric->pNy = 128;
	emCatFabric->pMy = 156;
	emCatFabric->pCy = 184;
	emCatFabric->pDy = 212;

	emCatFood->bmAll = &(gIcons);
	emCatFood->allowUnclick = false;
	emCatFood->colBorderSelected = emCatFood->colBorder;
	emCatFood->pNx = emCatFood->pMx = emCatFood->pCx = emCatFood->pDx = 962;
	emCatFood->pNy = 128;
	emCatFood->pMy = 156;
	emCatFood->pCy = 184;
	emCatFood->pDy = 212;

	emCatGlass->bmAll = &(gIcons);
	emCatGlass->allowUnclick = false;
	emCatGlass->colBorderSelected = emCatGlass->colBorder;
	emCatGlass->pNx = emCatGlass->pMx = emCatGlass->pCx = emCatGlass->pDx = 909;
	emCatGlass->pNy = 128;
	emCatGlass->pMy = 156;
	emCatGlass->pCy = 184;
	emCatGlass->pDy = 212;

	emCatGround->bmAll = &(gIcons);
	emCatGround->allowUnclick = false;
	emCatGround->colBorderSelected = emCatGround->colBorder;
	emCatGround->pNx = emCatGround->pMx = emCatGround->pCx = emCatGround->pDx = 845;
	emCatGround->pNy = 128;
	emCatGround->pMy = 156;
	emCatGround->pCy = 184;
	emCatGround->pDy = 212;

	emCatLeather->bmAll = &(gIcons);
	emCatLeather->allowUnclick = false;
	emCatLeather->colBorderSelected = emCatLeather->colBorder;
	emCatLeather->pNx = emCatLeather->pMx = emCatLeather->pCx = emCatLeather->pDx = 780;
	emCatLeather->pNy = 128;
	emCatLeather->pMy = 156;
	emCatLeather->pCy = 184;
	emCatLeather->pDy = 212;

	emCatLiquids->bmAll = &(gIcons);
	emCatLiquids->allowUnclick = false;
	emCatLiquids->colBorderSelected = emCatLiquids->colBorder;
	emCatLiquids->pNx = emCatLiquids->pMx = emCatLiquids->pCx = emCatLiquids->pDx = 720;
	emCatLiquids->pNy = 128;
	emCatLiquids->pMy = 156;
	emCatLiquids->pCy = 184;
	emCatLiquids->pDy = 212;

	emCatSSS->bmAll = &(gIcons);
	emCatSSS->allowUnclick = false;
	emCatSSS->colBorderSelected = emCatSSS->colBorder;
	emCatSSS->pNx = emCatSSS->pMx = emCatSSS->pCx = emCatSSS->pDx = 373;
	emCatSSS->pNy = 128;
	emCatSSS->pMy = 156;
	emCatSSS->pCy = 184;
	emCatSSS->pDy = 212;

	emCatMinerals->bmAll = &(gIcons);
	emCatMinerals->allowUnclick = false;
	emCatMinerals->colBorderSelected = emCatMinerals->colBorder;
	emCatMinerals->pNx = emCatMinerals->pMx = emCatMinerals->pCx = emCatMinerals->pDx = 617;
	emCatMinerals->pNy = 128;
	emCatMinerals->pMy = 156;
	emCatMinerals->pCy = 184;
	emCatMinerals->pDy = 212;

	emCatPlastic->bmAll = &(gIcons);
	emCatPlastic->allowUnclick = false;
	emCatPlastic->colBorderSelected = emCatPlastic->colBorder;
	emCatPlastic->pNx = emCatPlastic->pMx = emCatPlastic->pCx = emCatPlastic->pDx = 556;
	emCatPlastic->pNy = 128;
	emCatPlastic->pMy = 156;
	emCatPlastic->pCy = 184;
	emCatPlastic->pDy = 212;

	emCatOrganic->bmAll = &(gIcons);
	emCatOrganic->allowUnclick = false;
	emCatOrganic->colBorderSelected = emCatOrganic->colBorder;
	emCatOrganic->pNx = emCatOrganic->pMx = emCatOrganic->pCx = emCatOrganic->pDx = 490;
	emCatOrganic->pNy = 128;
	emCatOrganic->pMy = 156;
	emCatOrganic->pCy = 184;
	emCatOrganic->pDy = 212;

	emCatPorcelain->bmAll = &(gIcons);
	emCatPorcelain->allowUnclick = false;
	emCatPorcelain->colBorderSelected = emCatPorcelain->colBorder;
	emCatPorcelain->pNx = emCatPorcelain->pMx = emCatPorcelain->pCx = emCatPorcelain->pDx = 413;
	emCatPorcelain->pNy = 128;
	emCatPorcelain->pMy = 156;
	emCatPorcelain->pCy = 184;
	emCatPorcelain->pDy = 212;

	emCatBricks->bmAll = &(gIcons);
	emCatBricks->allowUnclick = false;
	emCatBricks->colBorderSelected = emCatBricks->colBorder;
	emCatBricks->pNx = emCatBricks->pMx = emCatBricks->pCx = emCatBricks->pDx = 1334;
	emCatBricks->pNy = 128;
	emCatBricks->pMy = 156;
	emCatBricks->pCy = 184;
	emCatBricks->pDy = 212;

	emCatConcrete->bmAll = &(gIcons);
	emCatConcrete->allowUnclick = false;
	emCatConcrete->colBorderSelected = emCatConcrete->colBorder;
	emCatConcrete->pNx = emCatConcrete->pMx = emCatConcrete->pCx = emCatConcrete->pDx = 1072;
	emCatConcrete->pNy = 128;
	emCatConcrete->pMy = 156;
	emCatConcrete->pCy = 184;
	emCatConcrete->pDy = 212;
	
	emCatCeramic->bmAll = &(gIcons);
	emCatCeramic->allowUnclick = false;
	emCatCeramic->colBorderSelected = emCatCeramic->colBorder;
	emCatCeramic->pNx = emCatCeramic->pMx = emCatCeramic->pCx = emCatCeramic->pDx = 1147;
	emCatCeramic->pNy = 128;
	emCatCeramic->pMy = 156;
	emCatCeramic->pCy = 184;
	emCatCeramic->pDy = 212;
	
	emCatCarpaint->bmAll = &(gIcons);
	emCatCarpaint->allowUnclick = false;
	emCatCarpaint->colBorderSelected = emCatCarpaint->colBorder;
	emCatCarpaint->pNx = emCatCarpaint->pMx = emCatCarpaint->pCx = emCatCarpaint->pDx = 1213;
	emCatCarpaint->pNy = 128;
	emCatCarpaint->pMy = 156;
	emCatCarpaint->pCy = 184;
	emCatCarpaint->pDy = 212;
	
	emCatMetal->bmAll = &(gIcons);
	emCatMetal->allowUnclick = false;
	emCatMetal->colBorderSelected = emCatMetal->colBorder;
	emCatMetal->pNx = emCatMetal->pMx = emCatMetal->pCx = emCatMetal->pDx = 1282;
	emCatMetal->pNy = 128;
	emCatMetal->pMy = 156;
	emCatMetal->pCy = 184;
	emCatMetal->pDy = 212;

	emCatOther->bmAll = &(gIcons);
	emCatOther->allowUnclick = false;
	emCatOther->colBorderSelected = emCatOther->colBorder;
	emCatOther->pNx = emCatOther->pMx = emCatOther->pCx = emCatOther->pDx = 49;
	emCatOther->pNy = 128;
	emCatOther->pMy = 156;
	emCatOther->pCy = 184;
	emCatOther->pDy = 212;

	emCatUser->bmAll = &(gIcons);
	emCatUser->allowUnclick = false;
	emCatUser->colBorderSelected = emCatUser->colBorder;
	emCatUser->pNx = emCatUser->pMx = emCatUser->pCx = emCatUser->pDx = 1;
	emCatUser->pNy = 128;
	emCatUser->pMy = 156;
	emCatUser->pCy = 184;
	emCatUser->pDy = 212;

	// layers...
	EMListView * emlay = GetEMListViewInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYERS_LIST));
	emlay->setColNumAndClearData(6, 45);
	emlay->colWidths.freeList();
	emlay->colWidths.add(45);
	emlay->colWidths.add(35);
	emlay->colWidths.add(31);
	emlay->colWidths.add(31);
	emlay->colWidths.add(35);
	emlay->colWidths.add(22);
	emlay->colWidths.createArray();
	emlay->headers.freeList();
	emlay->headers.add((char*)malloc(64));
	emlay->headers.add((char*)malloc(64));
	emlay->headers.add((char*)malloc(64));
	emlay->headers.add((char*)malloc(64));
	emlay->headers.add((char*)malloc(64));
	emlay->headers.add((char*)malloc(64));
	emlay->headers.createArray();
	sprintf_s(emlay->headers[0], 64, "Layer");
	sprintf_s(emlay->headers[1], 64, "weight");
	sprintf_s(emlay->headers[2], 64, "rough");
	sprintf_s(emlay->headers[3], 64, "emiss");
	sprintf_s(emlay->headers[4], 64, "transm");
	sprintf_s(emlay->headers[5], 64, "sss");

	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_START))->setToolTip("Start rendering preview");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_STOP))->setToolTip("Stop rendering preview");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ZOOM))->setToolTip("Render in bigger window");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFRESH))->setToolTip("Refresh preview");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_SAVEMAT))->setToolTip("Save material");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LOADMAT))->setToolTip("Load material");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_UPLOADMAT))->setToolTip("Upload current material to online library");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_DOWNLOADMAT))->setToolTip("Download selected material from online library");
	GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_SHOWHIDE_LIB))->setToolTip("Show/hide material library");
	GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH))->setToolTip("Online/offline library");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_ADD))->setToolTip("Add layer");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_DEL))->setToolTip("Del layer");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_CLONE))->setToolTip("Clone layer");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYER_RESET))->setToolTip("Reset layer");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB))->setToolTip("Set color for emission");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE))->setToolTip("Set temperature color for emission");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_RGB))->setToolTip("Set color for reflection 0");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_RGB))->setToolTip("Set color for reflection 90");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB))->setToolTip("Set color for absorption");

	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ARCHIVE_MAT))->setToolTip("Archive material");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_LOAD_LOCALLIB))->setToolTip("Load selected material from local library");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ADD_LOCALLIB))->setToolTip("Add current material to local library");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_DEL_LOCALLIB))->setToolTip("Delete selected material from local library");
	GetEMImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_USER_ONLINE))->setToolTip("Online user settings");

	for (int i=0; i<7; i++)
	{
		EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, libIDs[i]));
		if (!empv)
			continue;
		empv->setToolTip("");
		empv->dontLetUserChangeZoom = true;
	}

	unclickAllCategories(MatCategory::CAT_BRICKS);
	onlinematlib.currentCategory = MatCategory::CAT_BRICKS;
	onlinematlib.currentPage = 0;
	onlinematlib.queryMode = OnlineMatLib::MODE_CATEGORY;
	localmatlib.currentCategory = MatCategory::CAT_BRICKS;
	localmatlib.currentPage = 0;
	localmatlib.queryMode = LocalMatLib::MODE_CATEGORY;

	// TAB ORDER
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT),					GetDlgItem(hMain, IDC_MATEDIT_NAME),					0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPL_DEPTH),			GetDlgItem(hMain, IDC_MATEDIT_WEIGHT),					0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_POWER),			GetDlgItem(hMain, IDC_MATEDIT_DISPL_DEPTH),				0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS),				GetDlgItem(hMain, IDC_MATEDIT_EMISSION_POWER),			0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY),				GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS),				0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE),			GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY),				0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_IOR),					GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE),				0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE),	GetDlgItem(hMain, IDC_MATEDIT_IOR),						0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_SIZE),		GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE),		0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_OPACITY),				GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_SIZE),			0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_SSS_DENSITY),			GetDlgItem(hMain, IDC_MATEDIT_OPACITY),					0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_SSS_COLLISION_DIR),		GetDlgItem(hMain, IDC_MATEDIT_SSS_DENSITY),				0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY),		GetDlgItem(hMain, IDC_MATEDIT_SSS_COLLISION_DIR),		0,0,0,0,   SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE);

	SetFocus(hMain);
}

//------------------------------------------------------------------------------------------------------


