#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

#define DEBUG_STATS
//#define DEBUG_LOG2

//---------------------------------------------------------------------------------------------------------------------------------------------

#define PMAX 12
float Pd1[] = { 0.25f,  0.5f,  0.125f,  0.0625f,  0.03125f,  0.015625f,  0.0078125f,  0.00390625f,  0.001953125f,  0.0009765625f,  0.00048828125f,  0.000244140625f };
float Pa1[] = { 0.5f,   0.15f, 0.05f,   0.025f,   0.0125f,   0.00625f,   0.003125f,   0.0015625f,   0.00078125f,   0.000390625f,   0.0001953125f,   0.00009765625f  };

//--------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::mutateBidir()
{
	#ifdef DEBUG_LOG2
		Logger::add("bidir mut start");
	#endif

	// don't mutate if specular on connection
	float ctu=0,ctv=0;
	if (eCandidate[numCEP].tri)
		eCandidate[numCEP].tri->evalTexUV(eCandidate[numCEP].u, eCandidate[numCEP].v, ctu, ctv);
	if (eCandidate[numCEP].mlay  &&  eCandidate[numCEP].mlay->getRough(ctu, ctv) == 0)
		return false;
	if (lCandidate[numCLP].tri)
		lCandidate[numCLP].tri->evalTexUV(lCandidate[numCLP].u, lCandidate[numCLP].v, ctu, ctv);
	if (lCandidate[numCLP].mlay  &&  lCandidate[numCLP].mlay->getRough(ctu, ctv) == 0)
		return false;

	// random length of subpath to be deleted
	unsigned int r1;
	float r = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	kd = 0;
	for (int i=PMAX-1; i>=0; i--)
	{
		kd = i;
		if (r <= Pd1[i])
			break;
		r -= Pd1[i];
	}
	kd++; 
	if (kd > numCEP+numCLP+3)	// del more than possible? don't allow
		kd = numCEP+numCLP+3;

	//----------------
	kde = 0;
	kdl = 0;
	float pd;
	int poss = min(min(min(numCEP+2, numCLP+2), kd), numCEP+numCLP-kd+4);
	if (kd > 1)		// chosen to delete at least two edges - so random which vertices must be deleted
	{
		unsigned int rnd = Raytracer::getRandomGeneratorForThread()->getRandomInt(100000)%poss;
		kde = max(0, kd-numCLP-2) + rnd;
		kdl = kd - kde - 1;
	}
	pd = Pd1[kd-1]/(poss);	



	//----------------
	// random new subpath length
	int minE = 0;
	int minL = 0;
	if (numCLP-kdl<0)
		minL = kdl-numCLP;		// at least one vertex must exist after mutation on light side
	if (numCEP-kde<1)
		minE = 1+kde-numCEP;	// at least two vertices must exist after mutation on camera side
	int maxE = MLT_MAX_VERTICES-numCEP+kde-1;
	int maxL = MLT_MAX_VERTICES-numCLP+kdl-1;
	int minKA = minE+minL;
	int maxKA = maxE+maxL;

	r = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	r += Pa1[0];

	// random ka = number of edges to add
	ka = 0;
	bool inRangeUp, inRangeDown;
	for (int i=0; i<kd+1; i++)
	{
		ka = kd-1 - i;
		if (ka>=0)
		{
			inRangeDown = (ka>=minKA);
			inRangeUp = (ka<=maxKA);
			if (r < Pa1[i])
			{
				if (!inRangeDown)
					ka = minKA;
				if (!inRangeUp)
					ka = maxKA;
				break;
			}
			r -= Pa1[i];
		}

		ka = kd-1 + i;
		inRangeDown = (ka>=minKA);
		inRangeUp = (ka<=maxKA);
		if (r < Pa1[i])
		{
			if (!inRangeDown)
				ka = minKA;
			if (!inRangeUp)
				ka = maxKA;
			break;
		}
		r -= Pa1[i];
		ka = kd-1;
	}

	#ifdef DEBUG_LOG2
		char aaa2[64];
		sprintf_s(aaa2, 64, "rand ka=%d (incr)", ka+1);
		Logger::add(aaa2);
	#endif


	//----------------
	// if positive, how many on each side
	int maxkal = 0;
	int maxkae = 0;
	int maxposs = 0;
	int firstpos = 0;
	kae = 0;
	kal = 0;

	if (ka > 0)
	{
		int maxp1, maxp2, maxp3, maxp4;
		maxp1 = ka + 1 - minL - minE;
		maxp2 = maxL + 1 - minL;
		maxp3 = maxE + 1 - minE;
		maxp4 = maxL + maxE - ka + 1;
		maxposs = min(min(maxp1, maxp2), min(maxp3, maxp4));

		if (maxposs <= 0)
		{
			Logger::add("mutate bidir: error 1");
			return false;
		}

		firstpos = max(minE, ka-maxL);
		r1 = Raytracer::getRandomGeneratorForThread()->getRandomInt(100000);
		kae = min(((int)r1%maxposs) + firstpos, maxE);		// random kae = number of vertices to add to eye path
		kal = ka - kae;							// kal = number of vertices to add to light path
		if (kae + kal != ka)
			Logger::add("mutate bidir: error 2");
	}
	else
	{	// better check
		if (numCLP-kdl<0   ||   numCEP-kde<1)
		{
			Logger::add("mutate bidir: error 3");
			return false;
		}
	}


	#ifdef DEBUG_LOG2
		char aaa1[64];
		sprintf_s(aaa1, 64, "rand kal=%d  kae=%d  ka=%d", kal, kae, ka+1);
		Logger::add(aaa1);
	#endif

	ka++;

	if (kdl>=0)
		KDDL[kdl]++;
	if (kde>=0)
		KDDE[kde]++;
	if (kal>=0)
		KAAL[kal]++;
	if (kae>=0)
		KAAE[kae]++;

	numCLP -= kdl;
	numCEP -= kde;

	// remember values
	int rkdl = kdl;
	int rkde = kde;
	int rkal = kal;
	int rkae = kae;

	if (numCEP == -1)			// change pixel
	{	// not available yet
		kae--;
		kde--;
		numCEP = 0;
	}

	if (numCEP == 0)			// change point on lens
	{	// not available yet
		kae--;
		kde--;
		numCEP = 1;
	}

	if (numCLP <= -1)			// random new light source	
	{	
		kal--;
		kdl--;
		numCLP = 0;

		float nlpdf, uu,vv;
		int instID=-1;
		Matrix4d instMatr;
		bool chng = randomNewLightPoint(&lCandidate[0].tri,  &lCandidate[0].mat,  &lCandidate[0].mlay, instID, instMatr, uu, vv, nlpdf);
		if (chng)
		{
			lCandidate[0].instanceID = instID;
			lCandidate[0].inst_mat = instMatr;
			lCandidate[0].dirac = false;
			lCandidate[0].pos = instMatr * lCandidate[0].tri->getPointFromUV(uu, vv);
			lCandidate[0].normal = (instMatr.getMatrixForNormals() * lCandidate[0].tri->evalNormal(uu,vv)).getNormalized();
			lCandidate[0].inDir  = lCandidate[0].normal;
			lCandidate[0].outDir = lCandidate[0].normal;
			lCandidate[0].inCosine  = 1;
			lCandidate[0].outCosine = 1;
			lCandidate[0].u = uu;
			lCandidate[0].v = vv;
			lCandidate[0].inRadiance = lCandidate[0].mlay->gColor * lCandidate[0].mlay->power;
			lCandidate[0].brdf = lCandidate[0].mlay->gColor * lCandidate[0].mlay->power;
			#ifdef DEBUG_LOG2
				Logger::add("new light source");
			#endif
		}
		else
			return false;
	}

	if (numCLP == 0  &&  numCLP+kal>0)			// random direction from light
	{
		#ifdef DEBUG_LOG2
			Logger::add("random dir from light");
		#endif
		HitData hd;
		hd.in = lCandidate[0].normal;
		hd.normal_shade = lCandidate[0].normal;
		hd.lastDist = 1;
		lCandidate[0].tri->evalTexUV(lCandidate[0].u, lCandidate[0].v, hd.tU, hd.tV);
		hd.normal_geom = (lCandidate[0].inst_mat.getMatrixForNormals() * lCandidate[0].tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		lCandidate[0].tri->getTangentSpaceSmooth(lCandidate[0].inst_mat, lCandidate[0].u, lCandidate[0].v, hd.dir_U, hd.dir_V);
		
		float ndpdf;
		Vector3d dir = lCandidate[0].mlay->randomNewDirectionLambert2(hd, ndpdf);
		if (ndpdf < 0.001f)
			return false;
		lCandidate[0].outDir = dir;
		lCandidate[0].outCosine = lCandidate[0].outDir * lCandidate[0].normal;
	}

	kd = kde + kdl + 1;
	ka = kae + kal + 1;


	// now add vertices
	bool eOK = true;
	bool lOK = true;

	if (ka>=0)
		nadd[ka]++;
	ndel[kd]++;

	if (kae>0)
		eOK = createNextCandidateVertices(NULL, false, kae, false);
	if (kal>0)
		lOK = createNextCandidateVertices(NULL, true, kal, false);

	#ifdef DEBUG_LOG2
		Logger::add("created");
	#endif

	if (!eOK   ||   !lOK)
		return false;

	// pop back values	
	kdl = rkdl;
	kde = rkde;
	kal = rkal;
	kae = rkae;
	kd = kde + kdl + 1;
	ka = kae + kal + 1;

	lCnt[numCLP]++;
	eCnt[numCEP]++;
	nEdg[numCEP+numCLP+1]++;

	bool res = visibilityTest();

	if (!res)
		rejNonVis++;

	return res;
}

//--------------------------------------------------------------------------------------------------------------------------------------

float Metropolis::tentativeTransitionForBidir(bool forward)
{
	//return 1;
	int KD, KDE, KDL, KA, KAE, KAL, nL, nE, nBL, nBE;
	MLTVertex * ePtr;
	MLTVertex * lPtr;
	if (forward)
	{
		KD = kd;  KDE = kde;  KDL = kdl;
		KA = ka;  KAE = kae;  KAL = kal;
		ePtr = eCandidate;
		lPtr = lCandidate;
		nL = numCLP;
		nE = numCEP;
		nBL = numLP;
		nBE = numEP;
	}
	else
	{
		KD = ka;  KDE = kae;  KDL = kal;
		KA = kd;  KAE = kde;  KAL = kdl;
		ePtr = ePath;
		lPtr = lPath;
		nL = numLP;
		nE = numEP;
		nBL = numCLP;
		nBE = numCEP;
	}

	if (KA > 2*MLT_MAX_VERTICES   ||   KA < 0)
	{
		Logger::add("exiting... bad value to alloc");
		exit(0);
	}

	int t = 0;			// join paths via pointers
	for (int i=0; i<=KAE; i++)
	{
		int ii = nE-KAE+i;
		if (ii < 0)
		{
			mutPath[t] = NULL;
		}
		else
		{
			mutPath[t] = &(ePtr[ii]);
		}
		t++;
	}
	for (int i=0; i<=KAL; i++)
	{
		if (nL-i<0)
			mutPath[t] = NULL;
		else
			mutPath[t] = &(lPtr[nL-i]);
		t++;
	}

	for (int i=t; i<2*MLT_MAX_VERTICES; i++)
		mutPath[i] = NULL;

	Vector3d dir = mutPath[KAE+1]->pos - mutPath[KAE]->pos;
	dir.normalize();
	mutPath[KAE]->outDir = dir;
	dir.invert();
	mutPath[KAE+1]->outDir = dir;
	mutPath[KAE  ]->outCosine = mutPath[KAE  ]->normal * mutPath[KAE  ]->outDir;
	mutPath[KAE+1]->outCosine = mutPath[KAE+1]->normal * mutPath[KAE+1]->outDir;

	bool wrrr = false;

	HitData hd;
	for (int i=0; i<=KA; i++)		// eval brdf, G, and probabilities for whole subpath
	{
		if (mutPath[i] == NULL)		// no vertex.. todo.. 
		{
			brdf[i] = Color4(1,1,1);
			pEL[i] = 1;
			pLE[i] = 1;
			tG[i] = 1;
			continue;
		}

		if (i>0     &&     mutPath[i-1] != NULL)
			hd.in = mutPath[i-1]->pos - mutPath[i]->pos;
		else
			hd.in = mutPath[i]->inDir;
		hd.in.normalize();

		if (i<KA   &&   mutPath[i+1] != NULL)
			hd.out = mutPath[i+1]->pos - mutPath[i]->pos;
		else
			hd.out = mutPath[i]->inDir;	// other direction so in
		hd.out.normalize();

		hd.normal_shade = mutPath[i]->normal;
		if (mutPath[i]->tri)
		{
			mutPath[i]->tri->evalTexUV(mutPath[i]->u, mutPath[i]->v, hd.tU, hd.tV);
			hd.normal_geom = (mutPath[i]->inst_mat.getMatrixForNormals() * mutPath[i]->tri->normal_geom).getNormalized();
			hd.adjustNormal();
			mutPath[i]->tri->getTangentSpaceSmooth(mutPath[i]->inst_mat, mutPath[i]->u, mutPath[i]->v, hd.dir_U, hd.dir_V);
		}
		else
			hd.tU = hd.tV = 0;
		float cos_in  = hd.in  * hd.normal_shade;
		float cos_out = hd.out * hd.normal_shade;
		float abs_cos_in  = fabs(cos_in);
		float abs_cos_out = fabs(cos_out);

		if (i<KA)
		{	// eval G only for edges
			if (mutPath[i+1]==NULL   ||   mutPath[i]==NULL)
				tG[i]=1;
			else
			{
				Vector3d tdir = mutPath[i+1]->pos - mutPath[i]->pos;
				float d2 = tdir.normalize_return_old_length();
				d2 = d2*d2;
				if (d2 < 0.001f)
					return 0;
				tG[i] = fabs((mutPath[i+1]->normal * tdir) * (mutPath[i]->normal * tdir)) / d2;	// G
			}
		}
	
		if (mutPath[i]->mlay)		
		{
			float mtu=0, mtv=0;
			mutPath[i]->tri->evalTexUV(mutPath[i]->u, mutPath[i]->v, mtu, mtv);
			float rough = mutPath[i]->mlay->getRough(mtu, mtv);
			if (rough == 0)
			{
				wrrr = true;	
			}

			if (mutPath[i]->mlay->type == MatLayer::TYPE_EMITTER)
			{
				brdf[i] = mutPath[i]->mlay->gColor * mutPath[i]->mlay->power;
				pEL[i] = 1.0f;
				Vector3d tV = hd.in;
				hd.in = hd.out;
				hd.out = tV;
				if (abs_cos_in < 0.001f)
					pLE[i] = 0.0f;
				else
					pLE[i] = mutPath[i]->mlay->getProbabilityLambert(hd) / abs_cos_in;	// p_
			}
			else
			{
				if (abs_cos_out < 0.001f)
					pEL[i] = 0.0f;
				else
					pEL[i] = mutPath[i]->mlay->getProbability(hd) / abs_cos_out;
				Vector3d tV = hd.in;
				hd.in = hd.out;
				hd.out = tV;
				brdf[i] = mutPath[i]->mlay->getBRDF(hd);
				if (abs_cos_in < 0.001f)
					pLE[i] = 0;
				else
					pLE[i] = mutPath[i]->mlay->getProbability(hd) / abs_cos_in;				// p_
			}
		}
		else
		{
			brdf[i] = Color4(1,1,1);
			pEL[i] = 1;
			pLE[i] = 1;
		}
	}	// end i = 0..KA

	int maxkal   = MLT_MAX_VERTICES + KDL - nL - 1;
	int maxkae   = MLT_MAX_VERTICES + KDE - nE - 1;
	int maxposs  = min(min(maxkal, maxkae) , KA) + 1;

	int poss = min(min(min(nBE+2, nBL+2), KD), nBE+nBL-KD+4);
	float pd = Pd1[KD-1]/(float)(poss);
	int df = KA - KD;
	df = df<0 ? -df : df;
	float pa = Pa1[df] / (float)maxposs;

	float T = 0;
	Color4 fsp = Color4(1,1,1);

	for (int i=0; i<KA; i++)	// for every path generation possibility
	{	
		fsp *= brdf[i] * tG[i];
		
		if (_isnan(fsp.r)   ||   _isnan(fsp.g)   ||   _isnan(fsp.b))
		{
			char dddd[128];
			sprintf_s(dddd, 128, "fsp: %f  %f  %f      brdf[%d] = %f  %f  %f       tG[%d] = %f", fsp.r, fsp.g, fsp.b, i,  brdf[i].r, brdf[i].g, brdf[i].b, i, tG[i]);
			Logger::add(dddd);
		}

		float pg = 1;

		if (mutPath[i]   &&   mutPath[i]->mlay)
		{
			float mtu=0, mtv=0;
			mutPath[i]->tri->evalTexUV(mutPath[i]->u, mutPath[i]->v, mtu, mtv);
			float rough = mutPath[i]->mlay->getRough(mtu, mtv);
			if (rough == 0)
				pg = 0;
		}

		if (mutPath[i+1]   &&   mutPath[i+1]->mlay)
		{
			float mtu=0, mtv=0;
			mutPath[i+1]->tri->evalTexUV(mutPath[i+1]->u, mutPath[i+1]->v, mtu, mtv);
			float rough = mutPath[i+1]->mlay->getRough(mtu, mtv);
			if (rough == 0)
				pg = 0;
		}

		for (int j=0; j<i; j++)	// path from eye
		{
			pg *= pEL[j];
			pg *= tG[j];
		}

		for (int j=KA; j>i+1; j--)	// path from light
		{
			pg *= pLE[j];
			pg *= tG[j-1];
		}

		T = T + pg;
	}

	fsp *= brdf[KA];

	T = T * pa * pd;
	
	if (T==0)
		return 0;

	float R = ( 0.299f * fsp.r  +  0.587f * fsp.g  +  0.114f * fsp.b ) / T;

	return R;
}

//--------------------------------------------------------------------------------------------------------------------------------------

