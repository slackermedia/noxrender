#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include "EMControls.h"

extern HMODULE hDllModule;

EMText::EMText(HWND hWnd) 
{
	colText = RGB(0,0,0); 
	colLink = RGB(0,160,210);
	colLinkMouseOver = RGB(255,160,110);
	colBackGnd = RGB(96,160,255);
	colDisabledText = RGB(64,64,64);
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hwnd = hWnd;

	drawTransparent = false;
	
	DWORD style;
	style = GetWindowLong(hwnd, GWL_STYLE);
	if (style & ES_RIGHT)
		align = ALIGN_RIGHT;
	else
		align = ALIGN_LEFT;


	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
	{
		captionSize = 0;
		caption[captionSize] = 0;
	}	

	linkAddress = NULL;
	isItHyperLink = false;
	isMouseOverNow = false;

	GetClientRect(hwnd, &textRect);
}

EMText::~EMText() 
{
	if (caption)
		free(caption);
	if  (hFont)
		DeleteObject(hFont);
}

void InitEMText()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMText";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMTextProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMText *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMText * GetEMTextInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMText * emb = (EMText *)GetWindowLongPtr(hwnd, 0);
	#else
		EMText * emb = (EMText *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emb;
}

void SetEMTextInstance(HWND hwnd, EMText *emb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emb);
	#endif
}

LRESULT CALLBACK EMTextProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMText * emt;
	emt = GetEMTextInstance(hwnd);

    switch(msg)
    {
	case WM_CREATE:
		{
			EMText * emt1 = new EMText(hwnd);
			SetEMTextInstance(hwnd, (EMText *)emt1);			
		}
	break;
	case WM_NCDESTROY:
		{
			EMText * emt1;
			emt1 = GetEMTextInstance(hwnd);
			delete emt1;
			SetEMTextInstance(hwnd, 0);
		}
	break;
	case WM_PAINT:
	{
		if (!emt)
			break;

		RECT rect;
		HDC hdc;
		PAINTSTRUCT ps;
		SIZE sz;
		HANDLE hOldFont;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);
		GetClientRect(hwnd, &rect);

		hdc = BeginPaint(hwnd, &ps);

		{
			hOldFont = SelectObject(hdc, emt->hFont);
			if (disabled)
				SetTextColor(hdc, emt->colDisabledText);
			else
			{
				if (emt->isItHyperLink)
				{
					if (emt->isMouseOverNow)
						SetTextColor(hdc, emt->colLinkMouseOver);
					else
						SetTextColor(hdc, emt->colLink);
				}
				else
					SetTextColor(hdc, emt->colText);
			}
			SetBkColor(hdc, emt->colBackGnd);
			GetClientRect(hwnd, &rect);
			if (emt->drawTransparent)
				SetBkMode(hdc, TRANSPARENT);
			else
			{
				SetBkMode(hdc, OPAQUE);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emt->colBackGnd));
				HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emt->colBackGnd));
				Rectangle(hdc, 0, 0, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
			}

			emt->evalTextRect(hdc, &(emt->textRect));	// for link mouse over etc
		
			GetTextExtentPoint32(hdc, emt->caption, emt->captionSize, &sz);
			int posx, posy;
			switch (emt->align)
			{
				case EMText::ALIGN_RIGHT:
					posx = (rect.right-rect.left-sz.cx);
					posy = (rect.bottom-rect.top-sz.cy)/2;
					break;
				case EMText::ALIGN_CENTER:
					posx = (rect.right-rect.left-sz.cx)/2;
					posy = (rect.bottom-rect.top-sz.cy)/2;
					break;
				default:
					posx = 0;
					posy = (rect.bottom-rect.top-sz.cy)/2;
					break;
			}

			//ExtTextOut(hdc, posx, posy, ETO_OPAQUE, &rect, emt->caption, emt->captionSize, 0);
			TextOut(hdc, posx, posy, emt->caption, emt->captionSize);
			
			SelectObject(hdc, hOldFont);
		}

		EndPaint(hwnd, &ps);
	}
	break;
	case WM_SETTEXT:
	{
		char * b = (char *)lParam;
		emt->changeCaption(b);
	}
	break;
	case WM_MOUSEMOVE:
	{
		if (emt->isItHyperLink)
		{
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (GetCapture() != hwnd)
			{
				SetCapture(hwnd);
			}
			else
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				if (!PtInRect(&rect, pt))
					ReleaseCapture();
			}

			if (!PtInRect(&emt->textRect, pt))
			{
				if (emt->isMouseOverNow)
				{
					if (emt->drawTransparent)
						invalidateWithParentRedraw(hwnd);
					InvalidateRect(hwnd, NULL, FALSE);
				}
				SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW)) );
				emt->isMouseOverNow = false;
			}
			else
			{
				if (!emt->isMouseOverNow)
				{
					if (emt->drawTransparent)
						invalidateWithParentRedraw(hwnd);
					InvalidateRect(hwnd, NULL, FALSE);
				}
				if (emt->linkAddress)
					SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_HAND)) );
				emt->isMouseOverNow = true;
			}

			
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		POINT pt = { LOWORD(lParam), HIWORD(lParam) };
		if (PtInRect(&emt->textRect, pt))
		{
			if (emt->isItHyperLink)
			{
				if (emt->linkAddress)
					ShellExecute(hwnd, "open", emt->linkAddress, NULL, NULL, SW_SHOWNORMAL);
				else
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLongPtr(hwnd, GWL_ID), BN_CLICKED), (LPARAM)hwnd);
			}
		}
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

bool EMText::changeCaption(char *newCaption)
{
	int ss;
	if (newCaption)
		ss = (int)strlen(newCaption);
	else
		ss = 0;
	char * newBuff = (char *)malloc(ss+2);
	if (!newBuff)
		return false;
	if (newCaption)
		sprintf_s(newBuff, ss+2, "%s", newCaption);
	else
		sprintf_s(newBuff, ss+2, "");
	if (caption)
		free(caption);
	caption = newBuff;
	captionSize = ss;
	InvalidateRect(hwnd, NULL, false);
	return true;
}

bool EMText::setHyperLink(char * newAddress)
{
	char * oldAddress = linkAddress;
	if (newAddress)
	{
		if (strlen(newAddress)>0)
		{
			char * cpAddress = copyString(newAddress);
			if (!cpAddress)
				return false;
			linkAddress = cpAddress;
			isItHyperLink = true;
		}
		else
		{
			linkAddress = NULL;
			isItHyperLink = false;
		}
	}
	else
	{
		linkAddress = NULL;
		isItHyperLink = false;
	}

	if (oldAddress)
		free(oldAddress);

	InvalidateRect(hwnd, NULL, false);
	return true;
}

bool EMText::evalTextRect(HDC &hdc, RECT * rect)
{
	CHECK(rect);

	RECT crect;
	GetClientRect(hwnd, &crect);
	SIZE sz;
	GetTextExtentPoint32(hdc, caption, captionSize, &sz);
	int posx, posy;
	switch (align)
	{
		case ALIGN_RIGHT:
			posx = (crect.right-crect.left-sz.cx);
			posy = (crect.bottom-crect.top-sz.cy)/2;
			break;
		case ALIGN_CENTER:
			posx = (crect.right-crect.left-sz.cx)/2;
			posy = (crect.bottom-crect.top-sz.cy)/2;
			break;
		default:
			posx = 0;
			posy = (crect.bottom-crect.top-sz.cy)/2;
			break;
	}

	rect->left = posx;
	rect->top = posy;
	rect->right = posx+sz.cx;
	rect->bottom = posy+sz.cy;
	return true;
}
