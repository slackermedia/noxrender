#define _CRT_RAND_S
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <float.h>
#include "Colors.h"


// -------------------------------- RGB --------------------------------

bool Color4::isNaN()
{
	return (_isnan(r)  ||  _isnan(g)  ||  _isnan(b));
}

bool Color4::isInf()		// also nan as docs say
{
	return (!_finite(r)  ||  !_finite(g)  ||  !_finite(b));
}

bool Color4::isBlack()
{
	if (r != 0.0f)
		return false;
	if (g != 0.0f)
		return false;
	if (b != 0.0f)
		return false;

	return true;
}

void Color4::clamp()
{
	if (r<0.0f) r=0.0f;
	if (g<0.0f) g=0.0f;
	if (b<0.0f) b=0.0f;
	if (a<0.0f) a=0.0f;
	if (r>1.0f) r=1.0f;
	if (g>1.0f) g=1.0f;
	if (b>1.0f) b=1.0f;
	if (a>1.0f) a=1.0f;
}

void Color4::toCIE_XYZ(float &X, float &Y, float &Z)
{
	float XX,YY,ZZ,SS;
	XX = 0.4124f * r    +    0.3576f * g    +    0.1805f * b;
	YY = 0.2126f * r    +    0.7152f * g    +    0.0722f * b;
	ZZ = 0.0193f * r    +    0.1192f * g    +    0.9505f * b;
	SS = XX + YY + ZZ;
	SS = 1.0f / SS;
	X = XX*SS;
	Y = YY*SS;
	Z = ZZ*SS;
}


void Color4::toCIE_xyY(float &x, float &y, float &Y)
{
}

COLORREF Color4::toColorRef()
{
	unsigned char cr = (unsigned char)max(0.0f, min(255.0f, r*255.0f));
	unsigned char cg = (unsigned char)max(0.0f, min(255.0f, g*255.0f));
	unsigned char cb = (unsigned char)max(0.0f, min(255.0f, b*255.0f));
	return RGB(cr,cg,cb);
}

ColorHSV Color4::toColorHSV()
{
	ColorHSV res;
	res.v = max(r,max(g,b));
	float temp = min(r,min(g,b));
	float m = res.v-temp;
	if (m==0)
	{
		res.h = 0;
		res.s = 0;
		return res;
	}

	float l, st;
	if (res.v==r)
	{
		l = g - b;
		if (g >= b)
			st = 0.0f;
		else
			st = 1.0f;
	}
	else
	{
		if (res.v==g)
		{
			l = b - r;
			st = 1.0f/3.0f;

		}
		else
		{
			if (res.v==b)
			{
				l = r - g;
				st = 2.0f/3.0f;
			}
		}
	}
	res.h = (l/m)/6.0f+st;

	if (res.v==0)
		res.s = 0;
	else
		res.s = 1 - temp/res.v;

	return res;
}

ColorHSL Color4::toColorHSL()
{
	ColorHSL res;

	float maxcol = max(max(r,g),b);
	float mincol = min(min(r,g),b);
	res.l = (maxcol+mincol)*0.5f;

	if (maxcol==mincol)
	{
		res.h = res.s = 0.0f;
		return res;
	}

	if (res.l<0.5f)
		res.s = (maxcol-mincol)/(maxcol+mincol);
	else
		res.s = (maxcol-mincol)/(2.0f-maxcol-mincol);

	if (maxcol==r)
		 res.h = (g-b)/(maxcol-mincol);
	if (maxcol==g)
		 res.h = 2.0f + (b-r)/(maxcol-mincol);
	if (maxcol==b)
		 res.h = 4.0f + (r-g)/(maxcol-mincol);

	res.h *= 1.0f / 6.0f;
	if (res.h < 0.0f)
		res.h += 1.0f;
	if (res.h > 1.0f)
		res.h -= 1.0f;

	return res;
}

ColorHSI Color4::toColorHSI()
{
	ColorHSI res;

	float sum = r+g+b;
	float persum = 1.0f/sum;
	if (sum == 0.0f)
	{
		res.h = 0.0f;
		res.i = 0.0f;
		res.s = 0.0f;
		return res;
	}

	float rr = r*persum;
	float gg = g*persum;
	float bb = b*persum;

	res.s = 1 - 3*min(min(rr,gg),bb);

	res.h = 0;
	if (res.s!=0)
	{
		float psq = (rr-gg)*(rr-gg) + (rr-bb)*(gg-bb);
		if (psq>0)
		{
			float pac = 0.5f*(rr+rr-gg-bb) / sqrt(psq);
			if (pac>=-1  &&  pac<=1)
			{
				res.h = acos(pac);
				res.h *= 0.15915494309189533576888376337251f;	// per 2pi
				if (b>g)
					res.h = 1-res.h;
			}
		}
	}

	res.s = 1 - 3*min(min(rr,gg),bb);
	res.i = (r+g+b)/3.0f;

	return res;
}

// -------------------------------- HSV --------------------------------

Color4 ColorHSV::toColor4()
{
	Color4 res;
	res.a = 1.0f;
	float f,p,q,t;
	int Hi;
	Hi = (int)(h*6);
	f = h*6 - Hi;
	p = v * (1-s);
	q = v * (1-f*s);
	t = v * (1-(1-f)*s);
	switch (Hi)
	{
		case 6:
		case 0:
			res.r = v;
			res.g = t;
			res.b = p;
			break;
		case 1:
			res.r = q;
			res.g = v;
			res.b = p;
			break;
		case 2:
			res.r = p;
			res.g = v;
			res.b = t;
			break;
		case 3:
			res.r = p;
			res.g = q;
			res.b = v;
			break;
		case 4:
			res.r = t;
			res.g = p;
			res.b = v;
			break;
		case 5:
			res.r = v;
			res.g = p;
			res.b = q;
			break;
		default:
			res.r = 0;
			res.g = 0;
			res.b = 0;
			break;
	}
	return res;
}

// -------------------------------- HSL --------------------------------

Color4 ColorHSL::toColor4()
{
	Color4 res;

	if (s==0.0f)
	{
		res.r = res.g = res.b = l;
		return res;
	}

	float temp2 = (l<0.5f) ? l*(1.0f+s) : l+s-l*s;
	float temp1 = 2.0f*l - temp2;
	float temp3r = h + 1.0f/3.0f;
	float temp3g = h;
	float temp3b = h - 1.0f/3.0f;
	if (temp3r<0)
		temp3r+=1;
	if (temp3r>1)
		temp3r-=1;
	if (temp3g<0)
		temp3g+=1;
	if (temp3g>1)
		temp3g-=1;
	if (temp3b<0)
		temp3b+=1;
	if (temp3b>1)
		temp3b-=1;

	if (6*temp3r<1)
		 res.r = temp1+(temp2-temp1)*6.0f*temp3r;
	else
		if (2*temp3r<1)
			res.r = temp2;
		else
			if (3*temp3r<2)
				res.r = temp1+(temp2-temp1)*((2.0f/3.0f)-temp3r)*6.0f;
			else
				res.r = temp1;

	if (6*temp3g<1)
		 res.g = temp1+(temp2-temp1)*6.0f*temp3g;
	else
		if (2*temp3g<1)
			res.g = temp2;
		else
			if (3*temp3g<2)
				res.g = temp1+(temp2-temp1)*((2.0f/3.0f)-temp3g)*6.0f;
			else
				res.g = temp1;

	if (6*temp3b<1)
		 res.b = temp1+(temp2-temp1)*6.0f*temp3b;
	else
		if (2*temp3b<1)
			res.b = temp2;
		else
			if (3*temp3b<2)
				res.b = temp1+(temp2-temp1)*((2.0f/3.0f)-temp3b)*6.0f;
			else
				res.b = temp1;

	return res;
}

// -------------------------------- HSI --------------------------------

Color4 ColorHSI::toColor4()
{
	Color4 res;
	float hh = h*6.283185307179586476925286766559f;	//2pi

	if (hh<=2.0943951023931954923084289221863f)	// 2/3pi
	{
		float hhh = hh;
		res.b = i * (1-s);
		res.r = i * (1+s*cos(hhh)/cos(1.0471975511965977461542144610932f-hhh));
		res.g = 3*i - (res.b+res.r);
	}
	else
	{
		if (hh<=4.1887902047863909846168578443727f)	// 4/3pi
		{
			float hhh = hh - 2.0943951023931954923084289221863f;
			res.r = i * (1-s);
			res.g = i * (1+s*cos(hhh)/cos(1.0471975511965977461542144610932f-hhh));
			res.b = 3*i - (res.r+res.g);
		}
		else
		{
			float hhh = hh - 4.1887902047863909846168578443727f;
			res.g = i * (1-s);
			res.b = i * (1+s*cos(hhh)/cos(1.0471975511965977461542144610932f-hhh));
			res.r = 3*i - (res.g+res.b);
		}
	}

	return res;
}
