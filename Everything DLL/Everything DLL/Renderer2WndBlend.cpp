#include "RendererMain2.h"
#include "EM2Controls.h"
#include "resource.h"
#include "dialogs.h"
#include "log.h"

extern HMODULE hDllModule;

HWND getBlend2ControlID(HWND hBlend, int id);
HWND getBlend2ControlNameEdit(HWND hBlend, int id);
HWND getBlend2ControlEnabled(HWND hBlend, int id);
HWND getBlend2ControlColor(HWND hBlend, int id);
HWND getBlend2ControlMultiplier(HWND hBlend, int id);
int getBlendIdFromColorControlID(WORD wmID);

void setPositionsOnStartBlendTab(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcBlend(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnStartBlendTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2Button * embRefresh = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_REFRESH_NOW));
				embRefresh->setFont(rMain->fonts->em2button, false);
				embRefresh->bgImage = rMain->hBmpBg;
				EM2CheckBox * emcAuto = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_AUTOREFRESH));
				emcAuto->bgImage = rMain->hBmpBg;
				emcAuto->selected = rMain->autoRedrawPost;
				emcAuto->setFont(rMain->fonts->em2text, false);

				EM2GroupBar * emgLayers = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_GR_LAYERS));
				emgLayers->setFont(rMain->fonts->em2groupbar, false);
				emgLayers->bgImage = rMain->hBmpBg;

				EM2Text * emttID = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_ID));
				emttID->setFont(rMain->fonts->em2text, false);
				emttID->bgImage = rMain->hBmpBg;
				emttID->align = EM2Text::ALIGN_CENTER;

				EM2Text * emttName = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_NAME));
				emttName->setFont(rMain->fonts->em2text, false);
				emttName->bgImage = rMain->hBmpBg;

				EM2Text * emttON = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_ON));
				emttON->setFont(rMain->fonts->em2text, false);
				emttON->bgImage = rMain->hBmpBg;
				emttON->align = EM2Text::ALIGN_CENTER;

				EM2Text * emttColor = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_COLOR));
				emttColor->setFont(rMain->fonts->em2text, false);
				emttColor->bgImage = rMain->hBmpBg;
				emttColor->align = EM2Text::ALIGN_CENTER;

				EM2Text * emttMpl = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_MULTIPLIER));
				emttMpl->setFont(rMain->fonts->em2text, false);
				emttMpl->bgImage = rMain->hBmpBg;
				emttMpl->align = EM2Text::ALIGN_RIGHT;

				for (int i=0; i<16; i++)
				{
					EM2EditSpin * emesMult = GetEM2EditSpinInstance(getBlend2ControlMultiplier(hWnd, i));
					if (emesMult)
					{
						emesMult->setFont(rMain->fonts->em2editspin, false);
						emesMult->bgImage = rMain->hBmpBg;
						emesMult->setValuesToFloat(1.0f, 0.0f, 10.0f, 1.0f, 0.5f, 0.005f);
					}

					EM2Text * emtID = GetEM2TextInstance(getBlend2ControlID(hWnd, i));
					if (emtID)
					{
						emtID->setFont(rMain->fonts->em2text, false);
						emtID->bgImage = rMain->hBmpBg;
						emtID->align = EM2Text::ALIGN_CENTER;
					}

					EM2EditSimple * emtName = GetEM2EditSimpleInstance(getBlend2ControlNameEdit(hWnd, i));
					if (emtName)
					{
						emtName->setFontMain(rMain->fonts->em2editsimpletext, false);
						emtName->setFontEdit(rMain->fonts->em2editsimpleedit, false);
						emtName->bgImage = rMain->hBmpBg;
					}

					EM2CheckBox * emcON = GetEM2CheckBoxInstance(getBlend2ControlEnabled(hWnd, i));
					if (emcON)
					{
						emcON->setFont(rMain->fonts->em2checkbox, false);
						emcON->bgImage = rMain->hBmpBg;
						emcON->selected = true;
					}

					EM2ColorShow * emColor = GetEM2ColorShowInstance(getBlend2ControlColor(hWnd,i));
					if (emColor)
					{
						emColor->bgImage = rMain->hBmpBg;
						float cc = min(1.0f, i/15.0f);
						emColor->redraw(Color4(cc,cc,cc));
						emColor->setAllowedDragDrop();
					}
				}

				EM2Button * embReset = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_RESET_ALL));
				embReset->setFont(rMain->fonts->em2button, false);
				embReset->bgImage = rMain->hBmpBg;

				EM2CheckBox * emcShowDirect = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_SHOW_DIRECT));
				emcShowDirect->setFont(rMain->fonts->em2checkbox, false);
				emcShowDirect->bgImage = rMain->hBmpBg;
				emcShowDirect->selected = true;

				EM2CheckBox * emcShowGI = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_SHOW_GI));
				emcShowGI->setFont(rMain->fonts->em2checkbox, false);
				emcShowGI->bgImage = rMain->hBmpBg;
				emcShowGI->selected = true;
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_TIMER:
			{
				if (wParam == TIMER_POSTPROCESS_ID)
				{
					KillTimer(hWnd, TIMER_POSTPROCESS_ID);
					rMain->refreshRenderOnThread();
				}
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_BLEND_REFRESH_NOW:
						{
							CHECK(wmEvent==BN_CLICKED);
							rMain->refreshRenderOnThread();
						}
						break;
					case IDC_REND2_BLEND_AUTOREFRESH:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcAutoLays = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabBlend,			IDC_REND2_BLEND_AUTOREFRESH));
							EM2CheckBox * emcAutoPost = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabPost,			IDC_REND2_POST_AUTOREFRESH));
							EM2CheckBox * emcAutoCorr = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabCorrection,	IDC_REND2_CORR_AUTOREFRESH));
							EM2CheckBox * ecur = emcAutoLays;
							CHECK(ecur);
							rMain->autoRedrawPost = ecur->selected;
							if (emcAutoLays)
								emcAutoLays->selected = ecur->selected;
							if (emcAutoPost)
								emcAutoPost->selected = ecur->selected;
							if (emcAutoCorr)
								emcAutoCorr->selected = ecur->selected;
						}
						break;
					case IDC_REND2_BLEND_RESET_ALL:
						{
							CHECK(wmEvent==BN_CLICKED);
							Scene *sc = Raytracer::getInstance()->curScenePtr;
							int dRes = MessageBox(hWnd, "All blend layers settings will be lost.\nAre you sure?", "Warning.", MB_YESNO);
							if (dRes==IDYES)
							{
								for (int i=0; i<16; i++)
								{
									sc->blendSettings.blendOn[i] = true;
									sc->blendSettings.weights[i] = 1.0f;
									sc->blendSettings.red[i]     = 1.0f;
									sc->blendSettings.green[i]   = 1.0f;
									sc->blendSettings.blue[i]    = 1.0f;
								}
								rMain->fillLayersTabAll();
								if (rMain->autoRedrawPost)
									rMain->refreshRenderOnThread();
							}
						}
						break;
					case IDC_REND2_BLEND_SHOW_DIRECT:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_SHOW_DIRECT));
							CHECK(emc);
							Raytracer::getInstance()->curScenePtr->sscene.drawBufDirect = emc->selected;
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_BLEND_SHOW_GI:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BLEND_SHOW_GI));
							CHECK(emc);
							Raytracer::getInstance()->curScenePtr->sscene.drawBufGI = emc->selected;
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_BLEND_COLOR_1:
					case IDC_REND2_BLEND_COLOR_2:
					case IDC_REND2_BLEND_COLOR_3:
					case IDC_REND2_BLEND_COLOR_4:
					case IDC_REND2_BLEND_COLOR_5:
					case IDC_REND2_BLEND_COLOR_6:
					case IDC_REND2_BLEND_COLOR_7:
					case IDC_REND2_BLEND_COLOR_8:
					case IDC_REND2_BLEND_COLOR_9:
					case IDC_REND2_BLEND_COLOR_10:
					case IDC_REND2_BLEND_COLOR_11:
					case IDC_REND2_BLEND_COLOR_12:
					case IDC_REND2_BLEND_COLOR_13:
					case IDC_REND2_BLEND_COLOR_14:
					case IDC_REND2_BLEND_COLOR_15:
					case IDC_REND2_BLEND_COLOR_16:
						{
							if (wmEvent==BN_CLICKED)
							{
								int bID = getBlendIdFromColorControlID(wmId);
								if (bID>-1  &&  bID<16)
								{
									EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, wmId));
									if (emc)
									{
										Color4 col = emc->color;

										Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
													MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));

										emc->isMouseOver = false;
										emc->isClicked = false;
										InvalidateRect(emc->hwnd, NULL, false);
										if (res)
										{
											Scene *sc = Raytracer::getInstance()->curScenePtr;
											sc->blendSettings.red[bID] = res->r;
											sc->blendSettings.green[bID] = res->g;
											sc->blendSettings.blue[bID] = res->b;
											emc->redraw(*res);
											if (rMain->autoRedrawPost)
												rMain->refreshRenderOnThread();
										}
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								int bID = getBlendIdFromColorControlID(wmId);
								if (bID>-1  &&  bID<16)
								{
									EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, wmId));
									if (emc)
									{
										Color4 col = emc->color;
										Scene *sc = Raytracer::getInstance()->curScenePtr;
										sc->blendSettings.red[bID] = col.r;
										sc->blendSettings.green[bID] = col.g;
										sc->blendSettings.blue[bID] = col.b;
										if (rMain->autoRedrawPost)
											rMain->refreshRenderOnThread();
									}
								}
							}
						}
						break;
					case IDC_REND2_BLEND_MULTIPLIER_1:
					case IDC_REND2_BLEND_MULTIPLIER_2:
					case IDC_REND2_BLEND_MULTIPLIER_3:
					case IDC_REND2_BLEND_MULTIPLIER_4:
					case IDC_REND2_BLEND_MULTIPLIER_5:
					case IDC_REND2_BLEND_MULTIPLIER_6:
					case IDC_REND2_BLEND_MULTIPLIER_7:
					case IDC_REND2_BLEND_MULTIPLIER_8:
					case IDC_REND2_BLEND_MULTIPLIER_9:
					case IDC_REND2_BLEND_MULTIPLIER_10:
					case IDC_REND2_BLEND_MULTIPLIER_11:
					case IDC_REND2_BLEND_MULTIPLIER_12:
					case IDC_REND2_BLEND_MULTIPLIER_13:
					case IDC_REND2_BLEND_MULTIPLIER_14:
					case IDC_REND2_BLEND_MULTIPLIER_15:
					case IDC_REND2_BLEND_MULTIPLIER_16:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, wmId));
							int bID = getBlendIdFromColorControlID(wmId);
							if (bID>-1  &&  bID<16)
							{
								Scene *sc = Raytracer::getInstance()->curScenePtr;
								sc->blendSettings.weights[bID] = emes->floatValue;
								if (rMain->autoRedrawPost)
									SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
							}
						}
						break;
					case IDC_REND2_BLEND_ENABLED_1:
					case IDC_REND2_BLEND_ENABLED_2:
					case IDC_REND2_BLEND_ENABLED_3:
					case IDC_REND2_BLEND_ENABLED_4:
					case IDC_REND2_BLEND_ENABLED_5:
					case IDC_REND2_BLEND_ENABLED_6:
					case IDC_REND2_BLEND_ENABLED_7:
					case IDC_REND2_BLEND_ENABLED_8:
					case IDC_REND2_BLEND_ENABLED_9:
					case IDC_REND2_BLEND_ENABLED_10:
					case IDC_REND2_BLEND_ENABLED_11:
					case IDC_REND2_BLEND_ENABLED_12:
					case IDC_REND2_BLEND_ENABLED_13:
					case IDC_REND2_BLEND_ENABLED_14:
					case IDC_REND2_BLEND_ENABLED_15:
					case IDC_REND2_BLEND_ENABLED_16:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, wmId));
							int bID = getBlendIdFromColorControlID(wmId);
							if (bID>-1  &&  bID<16)
							{
								Scene * sc = Raytracer::getInstance()->curScenePtr;
								sc->blendSettings.blendOn[bID] = emc->selected;
								if (rMain->autoRedrawPost)
									SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
							}
						}
						break;
					case IDC_REND2_BLEND_TEXT_NAME_1:
					case IDC_REND2_BLEND_TEXT_NAME_2:
					case IDC_REND2_BLEND_TEXT_NAME_3:
					case IDC_REND2_BLEND_TEXT_NAME_4:
					case IDC_REND2_BLEND_TEXT_NAME_5:
					case IDC_REND2_BLEND_TEXT_NAME_6:
					case IDC_REND2_BLEND_TEXT_NAME_7:
					case IDC_REND2_BLEND_TEXT_NAME_8:
					case IDC_REND2_BLEND_TEXT_NAME_9:
					case IDC_REND2_BLEND_TEXT_NAME_10:
					case IDC_REND2_BLEND_TEXT_NAME_11:
					case IDC_REND2_BLEND_TEXT_NAME_12:
					case IDC_REND2_BLEND_TEXT_NAME_13:
					case IDC_REND2_BLEND_TEXT_NAME_14:
					case IDC_REND2_BLEND_TEXT_NAME_15:
					case IDC_REND2_BLEND_TEXT_NAME_16:
						{
							CHECK(wmEvent==ES_RETURN  ||  wmEvent==ES_LOST_FOCUS);
							EM2EditSimple * emes = GetEM2EditSimpleInstance(GetDlgItem(hWnd, wmId));
							int bID = getBlendIdFromColorControlID(wmId);
							if (bID>-1  &&  bID<16)
							{
								Scene *sc = Raytracer::getInstance()->curScenePtr;
								sc->blendSettings.setName(bID, emes->getText());
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::layers2lockUnlockOnlyUsedLayers(Camera * cam)
{
	CHECK(cam);
	HWND hWnd = hTabBlend;
	for (int i=0; i<16; i++)
	{
		BOOL used = (cam->blendBits&(1<<i)) ? TRUE : FALSE;
		HWND hCol   = getBlend2ControlColor(hWnd, i);
		HWND hCheck = getBlend2ControlEnabled(hWnd, i);
		HWND hMpl   = getBlend2ControlMultiplier(hWnd, i);
		HWND hName  = getBlend2ControlNameEdit(hWnd, i);
		EnableWindow(hCol, used);
		EnableWindow(hCheck, used);
		EnableWindow(hMpl, used);
		EnableWindow(hName, used);
	}
	return true;
}

//------------------------------------------------------------------------------------------------

HWND getBlend2ControlID(HWND hBlend, int id)
{
	HWND hwnd = hBlend;
	switch (id)
	{
		case 0:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_1);
		case 1:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_2);
		case 2:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_3);
		case 3:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_4);
		case 4:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_5);
		case 5:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_6);
		case 6:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_7);
		case 7:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_8);
		case 8:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_9);
		case 9:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_10);
		case 10:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_11);
		case 11:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_12);
		case 12:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_13);
		case 13:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_14);
		case 14:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_15);
		case 15:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_ID_16);
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

HWND getBlend2ControlNameEdit(HWND hBlend, int id)
{
	HWND hwnd = hBlend;
	switch (id)
	{
		case 0:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_1);
		case 1:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_2);
		case 2:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_3);
		case 3:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_4);
		case 4:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_5);
		case 5:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_6);
		case 6:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_7);
		case 7:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_8);
		case 8:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_9);
		case 9:		return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_10);
		case 10:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_11);
		case 11:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_12);
		case 12:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_13);
		case 13:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_14);
		case 14:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_15);
		case 15:	return GetDlgItem(hwnd, IDC_REND2_BLEND_TEXT_NAME_16);
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

HWND getBlend2ControlEnabled(HWND hBlend, int id)
{
	HWND hwnd = hBlend;
	switch (id)
	{
		case 0:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_1);
		case 1:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_2);
		case 2:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_3);
		case 3:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_4);
		case 4:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_5);
		case 5:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_6);
		case 6:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_7);
		case 7:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_8);
		case 8:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_9);
		case 9:		return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_10);
		case 10:	return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_11);
		case 11:	return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_12);
		case 12:	return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_13);
		case 13:	return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_14);
		case 14:	return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_15);
		case 15:	return GetDlgItem(hwnd, IDC_REND2_BLEND_ENABLED_16);
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

HWND getBlend2ControlColor(HWND hBlend, int id)
{
	HWND hwnd = hBlend;
	switch (id)
	{
		case 0:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_1);
		case 1:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_2);
		case 2:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_3);
		case 3:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_4);
		case 4:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_5);
		case 5:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_6);
		case 6:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_7);
		case 7:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_8);
		case 8:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_9);
		case 9:		return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_10);
		case 10:	return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_11);
		case 11:	return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_12);
		case 12:	return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_13);
		case 13:	return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_14);
		case 14:	return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_15);
		case 15:	return GetDlgItem(hwnd, IDC_REND2_BLEND_COLOR_16);
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

HWND getBlend2ControlMultiplier(HWND hBlend, int id)
{
	HWND hwnd = hBlend;
	switch (id)
	{
		case 0:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_1);
		case 1:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_2);
		case 2:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_3);
		case 3:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_4);
		case 4:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_5);
		case 5:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_6);
		case 6:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_7);
		case 7:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_8);
		case 8:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_9);
		case 9:		return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_10);
		case 10:	return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_11);
		case 11:	return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_12);
		case 12:	return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_13);
		case 13:	return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_14);
		case 14:	return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_15);
		case 15:	return GetDlgItem(hwnd, IDC_REND2_BLEND_MULTIPLIER_16);
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

int getBlendIdFromColorControlID(WORD wmID)
{
	switch (wmID)
	{
		case IDC_REND2_BLEND_COLOR_1:	
		case IDC_REND2_BLEND_MULTIPLIER_1:
		case IDC_REND2_BLEND_ENABLED_1:
		case IDC_REND2_BLEND_TEXT_NAME_1:
			return 0;
		case IDC_REND2_BLEND_COLOR_2:	
		case IDC_REND2_BLEND_MULTIPLIER_2:
		case IDC_REND2_BLEND_ENABLED_2:
		case IDC_REND2_BLEND_TEXT_NAME_2:
			return 1;
		case IDC_REND2_BLEND_COLOR_3:	
		case IDC_REND2_BLEND_MULTIPLIER_3:
		case IDC_REND2_BLEND_ENABLED_3:
		case IDC_REND2_BLEND_TEXT_NAME_3:
			return 2;
		case IDC_REND2_BLEND_COLOR_4:	
		case IDC_REND2_BLEND_MULTIPLIER_4:
		case IDC_REND2_BLEND_ENABLED_4:
		case IDC_REND2_BLEND_TEXT_NAME_4:
			return 3;
		case IDC_REND2_BLEND_COLOR_5:	
		case IDC_REND2_BLEND_MULTIPLIER_5:
		case IDC_REND2_BLEND_ENABLED_5:
		case IDC_REND2_BLEND_TEXT_NAME_5:
			return 4;
		case IDC_REND2_BLEND_COLOR_6:	
		case IDC_REND2_BLEND_MULTIPLIER_6:
		case IDC_REND2_BLEND_ENABLED_6:
		case IDC_REND2_BLEND_TEXT_NAME_6:
			return 5;
		case IDC_REND2_BLEND_COLOR_7:	
		case IDC_REND2_BLEND_MULTIPLIER_7:
		case IDC_REND2_BLEND_ENABLED_7:
		case IDC_REND2_BLEND_TEXT_NAME_7:
			return 6;
		case IDC_REND2_BLEND_COLOR_8:	
		case IDC_REND2_BLEND_MULTIPLIER_8:
		case IDC_REND2_BLEND_ENABLED_8:
		case IDC_REND2_BLEND_TEXT_NAME_8:
			return 7;
		case IDC_REND2_BLEND_COLOR_9:	
		case IDC_REND2_BLEND_MULTIPLIER_9:
		case IDC_REND2_BLEND_ENABLED_9:
		case IDC_REND2_BLEND_TEXT_NAME_9:
			return 8;
		case IDC_REND2_BLEND_COLOR_10:	
		case IDC_REND2_BLEND_MULTIPLIER_10:
		case IDC_REND2_BLEND_ENABLED_10:
		case IDC_REND2_BLEND_TEXT_NAME_10:
			return 9;
		case IDC_REND2_BLEND_COLOR_11:	
		case IDC_REND2_BLEND_MULTIPLIER_11:
		case IDC_REND2_BLEND_ENABLED_11:
		case IDC_REND2_BLEND_TEXT_NAME_11:
			return 10;
		case IDC_REND2_BLEND_COLOR_12:	
		case IDC_REND2_BLEND_MULTIPLIER_12:
		case IDC_REND2_BLEND_ENABLED_12:
		case IDC_REND2_BLEND_TEXT_NAME_12:
			return 11;
		case IDC_REND2_BLEND_COLOR_13:	
		case IDC_REND2_BLEND_MULTIPLIER_13:
		case IDC_REND2_BLEND_ENABLED_13:
		case IDC_REND2_BLEND_TEXT_NAME_13:
			return 12;
		case IDC_REND2_BLEND_COLOR_14:	
		case IDC_REND2_BLEND_MULTIPLIER_14:
		case IDC_REND2_BLEND_ENABLED_14:
		case IDC_REND2_BLEND_TEXT_NAME_14:
			return 13;
		case IDC_REND2_BLEND_COLOR_15:	
		case IDC_REND2_BLEND_MULTIPLIER_15:
		case IDC_REND2_BLEND_ENABLED_15:
		case IDC_REND2_BLEND_TEXT_NAME_15:
			return 14;
		case IDC_REND2_BLEND_COLOR_16:	
		case IDC_REND2_BLEND_MULTIPLIER_16:
		case IDC_REND2_BLEND_ENABLED_16:
		case IDC_REND2_BLEND_TEXT_NAME_16:
			return 15;
	}
	return -1;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelBlend()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TITLE));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgLayers = GetEM2GroupBarInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_GR_LAYERS));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_GR_LAYERS), bgShiftXRightPanel, 0, emgLayers->bgShiftX, emgLayers->bgShiftY);

	EM2Text * emttID = GetEM2TextInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_ID));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_ID), bgShiftXRightPanel, 0, emttID->bgShiftX, emttID->bgShiftY);
	EM2Text * emttName = GetEM2TextInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_NAME));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_NAME), bgShiftXRightPanel, 0, emttName->bgShiftX, emttName->bgShiftY);
	EM2Text * emttON = GetEM2TextInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_ON));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_ON), bgShiftXRightPanel, 0, emttON->bgShiftX, emttON->bgShiftY);
	EM2Text * emttColor = GetEM2TextInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_COLOR));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_COLOR), bgShiftXRightPanel, 0, emttColor->bgShiftX, emttColor->bgShiftY);
	EM2Text * emttMpl = GetEM2TextInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_MULTIPLIER));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_TEXT_MULTIPLIER), bgShiftXRightPanel, 0, emttMpl->bgShiftX, emttMpl->bgShiftY);

	for (int i=0; i<16; i++)
	{
		EM2EditSpin * emesMult = GetEM2EditSpinInstance(getBlend2ControlMultiplier(hTabBlend, i));
		if (emesMult)
			updateControlBgShift(getBlend2ControlMultiplier(hTabBlend, i), bgShiftXRightPanel, 0, emesMult->bgShiftX, emesMult->bgShiftY);
		EM2Text * emtID = GetEM2TextInstance(getBlend2ControlID(hTabBlend, i));
		if (emtID)
			updateControlBgShift(getBlend2ControlID(hTabBlend, i), bgShiftXRightPanel, 0, emtID->bgShiftX, emtID->bgShiftY);
		EM2EditSimple * emtNameEdit = GetEM2EditSimpleInstance(getBlend2ControlNameEdit(hTabBlend, i));
		if (emtNameEdit)
			updateControlBgShift(getBlend2ControlNameEdit(hTabBlend, i), bgShiftXRightPanel, 0, emtNameEdit->bgShiftX, emtNameEdit->bgShiftY);
		EM2CheckBox * emcON = GetEM2CheckBoxInstance(getBlend2ControlEnabled(hTabBlend, i));
		if (emcON)
			updateControlBgShift(getBlend2ControlEnabled(hTabBlend, i), bgShiftXRightPanel, 0, emcON->bgShiftX, emcON->bgShiftY);
		EM2ColorShow * emcsCol = GetEM2ColorShowInstance(getBlend2ControlColor(hTabBlend, i));
		if (emcsCol)
			updateControlBgShift(getBlend2ControlColor(hTabBlend, i), bgShiftXRightPanel, 0, emcsCol->bgShiftX, emcsCol->bgShiftY);
	}

	EM2Button * embResetSet = GetEM2ButtonInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_RESET_ALL));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_RESET_ALL), bgShiftXRightPanel, 0, embResetSet->bgShiftX, embResetSet->bgShiftY);
	EM2CheckBox * emcDrawDirect = GetEM2CheckBoxInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_SHOW_DIRECT));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_SHOW_DIRECT), bgShiftXRightPanel, 0, emcDrawDirect->bgShiftX, emcDrawDirect->bgShiftY);
	EM2CheckBox * emcDrawGI = GetEM2CheckBoxInstance(GetDlgItem(hTabBlend, IDC_REND2_BLEND_SHOW_GI));
	updateControlBgShift(GetDlgItem(hTabBlend, IDC_REND2_BLEND_SHOW_GI), bgShiftXRightPanel, 0, emcDrawGI->bgShiftX, emcDrawGI->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartBlendTab(HWND hWnd)
{
	int x,y;
	x=8; y=0;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_TITLE), HWND_TOP, 138,11, 100, 16, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_REFRESH_NOW), HWND_TOP, 136,39, 103, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_AUTOREFRESH), HWND_TOP, x+360-46, 45,  46, 10, 0);

	x=8; y=75;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_GR_LAYERS),		HWND_TOP,	x,		y,				360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_ID),			HWND_TOP,	x+7,	y+23,			15,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_NAME),		HWND_TOP,	x+62,	y+23,			50,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_ON),			HWND_TOP,	x+173,	y+23,			20,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_COLOR),		HWND_TOP,	x+235,	y+23,			40,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_TEXT_MULTIPLIER),	HWND_TOP,	x+300,	y+23,			59,		14,			0);

	for (int i=0; i<16; i++)
	{
		SetWindowPos(getBlend2ControlID(hWnd, i),			HWND_TOP,	x+7,		y+48+i*25,			15,		14,			0);
		SetWindowPos(getBlend2ControlNameEdit(hWnd, i),		HWND_TOP,	x+58,		y+46+i*25,			110,	18,			0);
		SetWindowPos(getBlend2ControlEnabled(hWnd, i),		HWND_TOP,	x+179,		y+50+i*25,			10,		10,			0);
		SetWindowPos(getBlend2ControlMultiplier(hWnd, i),	HWND_TOP,	x+303,		y+45+i*25,			54,		20,			0);
		SetWindowPos(getBlend2ControlColor(hWnd, i),		HWND_TOP,	x+249,		y+49+i*25,			14,		14,			0);
	}

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_RESET_ALL),		HWND_TOP,	x,		y+457,			86,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_SHOW_DIRECT),		HWND_TOP,	x-1,	y+496,			160,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BLEND_SHOW_GI),			HWND_TOP,	x-1,	y+526,			160,	10,			0);

	// tab order
	HWND last = HWND_TOP;
	for (int i=0; i<16; i++)
	{
		HWND current = getBlend2ControlNameEdit(hWnd, i);
		SetWindowPos(current,	last,	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
		last = current;
	}
	for (int i=0; i<16; i++)
	{
		HWND current = getBlend2ControlMultiplier(hWnd, i);
		SetWindowPos(current,	last,	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
		last = current;
	}
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillLayersTabAll()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	for (int i=0; i<16; i++)
	{
		EM2EditSimple * emtNameEdit = GetEM2EditSimpleInstance(getBlend2ControlNameEdit(hTabBlend, i));
		EM2CheckBox * emcON = GetEM2CheckBoxInstance(getBlend2ControlEnabled(hTabBlend, i));
		EM2ColorShow * emcsCol = GetEM2ColorShowInstance(getBlend2ControlColor(hTabBlend, i));
		EM2EditSpin * emesMult = GetEM2EditSpinInstance(getBlend2ControlMultiplier(hTabBlend, i));

		if (emtNameEdit)
			emtNameEdit->setText(sc->blendSettings.names[i]);
		if (emcON)
			emcON->selected = sc->blendSettings.blendOn[i];
		InvalidateRect(emcON->hwnd, NULL, false);
		if (emcsCol)
			emcsCol->color = Color4(sc->blendSettings.red[i], sc->blendSettings.green[i], sc->blendSettings.blue[i]);
		InvalidateRect(emcsCol->hwnd, NULL, false);
		if (emesMult)
			emesMult->setFloatValue(sc->blendSettings.weights[i]);
	}
	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabLayers(bool nowrendering)
{
}

//------------------------------------------------------------------------------------------------
