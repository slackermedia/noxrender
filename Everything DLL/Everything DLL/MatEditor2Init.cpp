#include "MatEditor2.h"
#include "raytracer.h"
#include "EM2Controls.h"
#include "resource.h"
#include "newgui_coords.h"
#include "valuesMinMax.h"

bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);

//----------------------------------------------------------------------

bool MatEditor2::initControls()
{
	CHECK(hMain);
	CHECK(fonts);
	Raytracer * rtr = Raytracer::getInstance();

	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_OK));
	embOK->bgImage = hBmpBg;
	embOK->setFont(fonts->em2button, false);
	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_CANCEL));
	embCancel->bgImage = hBmpBg;
	embCancel->setFont(fonts->em2button, false);

	EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TITLE));
	emtitle->bgImage = hBmpBg;
	emtitle->setFont(fonts->em2paneltitle, false);
	emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
	emtitle->align = EM2Text::ALIGN_CENTER;
	
	// GROUP PREVIEW
	EM2GroupBar * emgrPrev = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_PREVIEW));
	emgrPrev->bgImage = hBmpBg;
	emgrPrev->setFont(fonts->em2groupbar, false);
	
	EM2Text * emtMatName= GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NAME));
	emtMatName->bgImage = hBmpBg;
	emtMatName->setFont(fonts->em2text, false);
	EM2EditSimple * emesMatName = GetEM2EditSimpleInstance(GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME));
	emesMatName->bgImage = hBmpBg;
	emesMatName->setFontMain(fonts->em2editsimpletext, false);
	emesMatName->setFontEdit(fonts->em2editsimpleedit, false);
	EM2CheckBox * emcPortal = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SKYPORTAL));
	emcPortal->bgImage = hBmpBg;
	emcPortal->setFont(fonts->em2checkbox, false);
	EM2CheckBox * emcMatteShadow = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_MATTE_SHADOW));
	emcMatteShadow->bgImage = hBmpBg;
	emcMatteShadow->setFont(fonts->em2checkbox, false);
	EM2ComboBox * emcScene = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREV_SCENE));
	emcScene->bgImage = hBmpBg;
	emcScene->setFontMain(fonts->em2combomain, false);
	emcScene->setFontUnwrapped(fonts->em2combounwrap, false);
	emcScene->rowWidth = 160;
	
	// fill scenes combobox
	int scNum = rtr->materialEditorSceneFilenames.objCount;
	emcScene->setNumItems(scNum);
	for (int i=0; i<scNum; i++)
	{
		char * scname = NULL;
		if (i>=rtr->materialEditorSceneNames.objCount)
			scname = rtr->materialEditorSceneFilenames[i];
		else
			scname = rtr->materialEditorSceneNames[i];
		if (!scname)
			scname = "unnamed";
		char * scnamecopy = copyString(scname);
		if (scnamecopy)
		{
			_strupr_s(scnamecopy, strlen(scnamecopy)+1);
			emcScene->setItem(i, scnamecopy, true);
			free(scnamecopy);
		}
		else
			emcScene->setItem(i, "UNNAMED", true);

	}
	emcScene->selected = 0;
	InvalidateRect(emcScene->hwnd, NULL, false);

	EM2Button * embRender = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREV_RENDER));
	embRender->bgImage = hBmpBg;
	embRender->setFont(fonts->em2button, false);
	EM2Button * embLibOpen = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LIBRARY_OPEN));
	embLibOpen->bgImage = hBmpBg;
	embLibOpen->setFont(fonts->em2button, false);
	ShowWindow(embLibOpen->hwnd, SW_HIDE);

	EM2Button * embLoad= GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LOAD));
	embLoad->bgImage = hBmpBg;
	embLoad->setFont(fonts->em2button, false);
	EM2Button * embSave = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_SAVE));
	embSave->bgImage = hBmpBg;
	embSave->setFont(fonts->em2button, false);


	// GROUP GEOMETRY
	EM2GroupBar * emgrGeom = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_GEOMETRY));
	emgrGeom->bgImage = hBmpBg;
	emgrGeom->setFont(fonts->em2groupbar, false);

	EM2Text * emtDispl = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DISPLACEMENT));
	emtDispl->bgImage = hBmpBg;
	emtDispl->setFont(fonts->em2text, false);
	EM2Text * emtDepth = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH));
	emtDepth->bgImage = hBmpBg;
	emtDepth->setFont(fonts->em2text, false);
	EM2Text * emtShift = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SHIFT));
	emtShift->bgImage = hBmpBg;
	emtShift->setFont(fonts->em2text, false);
	EM2Text * emtSubD = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SUBDIVISIONS));
	emtSubD->bgImage = hBmpBg;
	emtSubD->setFont(fonts->em2text, false);
	EM2Text * emtNorms = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMALS));
	emtNorms->bgImage = hBmpBg;
	emtNorms->setFont(fonts->em2text, false);
	EM2Text * emtNBMap = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMAL_BUMP_MAP));
	emtNBMap->bgImage = hBmpBg;
	emtNBMap->setFont(fonts->em2text, false);

	EM2CheckBox * emcTxOnDispl = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_DISPLACEMENT));
	emcTxOnDispl->bgImage = hBmpBg;
	emcTxOnDispl->setFont(fonts->em2checkbox, false);
	EM2Button * embTxDispl = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_DISPLACEMENT));
	embTxDispl->bgImage = hBmpBg;
	embTxDispl->setFont(fonts->em2button, false);
	embTxDispl->ellipsis = true;

	EM2EditSpin * emesDDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH));
	emesDDepth->setFont(fonts->em2editspin, false);
	emesDDepth->bgImage = hBmpBg;
	emesDDepth->floatAfterDot = 2;
	emesDDepth->setValuesToFloat(NOX_MAT_DISP_DEPTH_DEF, NOX_MAT_DISP_DEPTH_MIN, NOX_MAT_DISP_DEPTH_MAX, NOX_MAT_DISP_DEPTH_DEF, 0.1f, 0.005f);
	EM2EditSpin * emesDShift = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT));
	emesDShift->setFont(fonts->em2editspin, false);
	emesDShift->bgImage = hBmpBg;
	emesDShift->setValuesToFloat(NOX_MAT_DISP_SHIFT_DEF, NOX_MAT_DISP_SHIFT_MIN, NOX_MAT_DISP_SHIFT_MAX, NOX_MAT_DISP_SHIFT_DEF, 0.1f, 0.005f);
	emesDShift->floatAfterDot = 2;
	EM2CheckBox * emcDCont = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_CONTINUITY));
	emcDCont->bgImage = hBmpBg;
	emcDCont->setFont(fonts->em2checkbox, false);
	EM2ComboBox * emcDSubdivs = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SUBDIVS));
	emcDSubdivs->bgImage = hBmpBg;
	emcDSubdivs->align = EM2ComboBox::ALIGN_LEFT;
	emcDSubdivs->moveArrow = true;
	emcDSubdivs->setFontMain(fonts->em2combomain, false);
	emcDSubdivs->setFontUnwrapped(fonts->em2combounwrap, false);
	emcDSubdivs->rowWidth = 50;
	emcDSubdivs->setNumItems(8);
	emcDSubdivs->maxShownRows = 8;
	emcDSubdivs->setItem(0, "4", true);
	emcDSubdivs->setItem(1, "16", true);
	emcDSubdivs->setItem(2, "64", true);
	emcDSubdivs->setItem(3, "256", true);
	emcDSubdivs->setItem(4, "1K", true);
	emcDSubdivs->setItem(5, "4K", true);
	emcDSubdivs->setItem(6, "16K", true);
	emcDSubdivs->setItem(7, "64K", true);

	EM2ComboBox * emcDNormals = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_NORMALS));
	emcDNormals->bgImage = hBmpBg;
	emcDNormals->setFontMain(fonts->em2combomain, false);
	emcDNormals->setFontUnwrapped(fonts->em2combounwrap, false);
	emcDNormals->setNumItems(3);
	emcDNormals->setItem(0, "SMOOTH", true);
	emcDNormals->setItem(1, "FACE", true);
	emcDNormals->setItem(2, "SOURCE", true);
	emcDNormals->rowWidth = 80;

	EM2CheckBox * emcDIgnScale = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_IGNORE_SCALE));
	emcDIgnScale->bgImage = hBmpBg;
	emcDIgnScale->setFont(fonts->em2checkbox, false);
	EM2CheckBox * emcTxOnNormBump = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_NORMALBUMP));
	emcTxOnNormBump->bgImage = hBmpBg;
	emcTxOnNormBump->setFont(fonts->em2checkbox, false);
	EM2Button * embTxNormBump = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_NORMALBUMP));
	embTxNormBump->bgImage = hBmpBg;
	embTxNormBump->setFont(fonts->em2button, false);
	embTxNormBump->ellipsis = true;

	// GROUP EMISSION
	EM2GroupBar * emgrEmiss = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_EMISSION));
	emgrEmiss->bgImage = hBmpBg;
	emgrEmiss->setFont(fonts->em2groupbar, false);
	EM2Text * emtPower = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_POWER));
	emtPower->bgImage = hBmpBg;
	emtPower->setFont(fonts->em2text, false);
	EM2Text * emtEmLay = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_EMITTER_LAYER));
	emtEmLay->bgImage = hBmpBg;
	emtEmLay->setFont(fonts->em2text, false);
	EM2Text * emtIES = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IES));
	emtIES->bgImage = hBmpBg;
	emtIES->setFont(fonts->em2text, false);
	emtIES->align = EM2Text::ALIGN_RIGHT;
	EM2CheckBox * emcEmission = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISSION));
	emcEmission->bgImage = hBmpBg;
	emcEmission->setFont(fonts->em2checkbox, false);
	EM2ColorShow * emcsEmiss = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_COLOR));
	emcsEmiss->bgImage = hBmpBg;
	emcsEmiss->setAllowedDragDrop();
	EM2CheckBox * emcEmissInvisible = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_INVISIBLE));
	emcEmissInvisible->bgImage = hBmpBg;
	emcEmissInvisible->setFont(fonts->em2checkbox, false);

	EM2ImgButton * emTemper = GetEM2ImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_TEMPERATURE));
	emTemper->bgImage = hBmpBg;
	emTemper->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
	emTemper->pNx = BTN_TEMP_N_X;
	emTemper->pNy = BTN_TEMP_N_Y;
	emTemper->pMx = BTN_TEMP_M_X;
	emTemper->pMy = BTN_TEMP_M_Y;
	emTemper->pCx = BTN_TEMP_C_X;
	emTemper->pCy = BTN_TEMP_C_Y;
	emTemper->pDx = BTN_TEMP_D_X;
	emTemper->pDy = BTN_TEMP_D_Y;

	EM2EditSpin * emesPower = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER));
	emesPower->setFont(fonts->em2editspin, false);
	emesPower->bgImage = hBmpBg;
	emesPower->floatAfterDot = 1;
	emesPower->setValuesToFloat(NOX_MAT_EMPOWER_DEF, NOX_MAT_EMPOWER_MIN, NOX_MAT_EMPOWER_MAX, NOX_MAT_EMPOWER_DEF, 1.0f, 0.5f);
	EM2ComboBox * emcEmissUnit = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_UNITS));
	emcEmissUnit->bgImage = hBmpBg;
	emcEmissUnit->rowWidth = 50;
	emcEmissUnit->setFontMain(fonts->em2combomain, false);
	emcEmissUnit->setFontUnwrapped(fonts->em2combounwrap, false);
	emcEmissUnit->setNumItems(4);
	emcEmissUnit->setItem(0, "W", true);
	//emcEmissUnit->setItem(1, "W/m2", true);
	emcEmissUnit->setItem(1, "W/m�", true);
	emcEmissUnit->setItem(2, "lm", true);
	emcEmissUnit->setItem(3, "lx", true);
	emcEmissUnit->align = EM2ComboBox::ALIGN_LEFT;
	emcEmissUnit->moveArrow = true;


	EM2ComboBox * emcEmissLayCombo = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_LAYER_COMBO));
	emcEmissLayCombo->bgImage = hBmpBg;
	emcEmissLayCombo->rowWidth = 120;
	emcEmissLayCombo->setFontMain(fonts->em2combomain, false);
	emcEmissLayCombo->setFontUnwrapped(fonts->em2combounwrap, false);
	emcEmissLayCombo->setNumItems(16);
	for (int i=0; i<16; i++)
	{
		emcEmissLayCombo->setItem(i, rtr->curScenePtr->blendSettings.names[i], true);
	}

	emcEmissLayCombo->align = EM2ComboBox::ALIGN_LEFT;
	emcEmissLayCombo->moveArrow = true;
	emcEmissLayCombo->selected = 0;


	EM2CheckBox * emcTxOnEmission = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_EMISS));
	emcTxOnEmission->bgImage = hBmpBg;
	emcTxOnEmission->setFont(fonts->em2checkbox, false);
	EM2Button * embTxEmission = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_EMISS));
	embTxEmission->bgImage = hBmpBg;
	embTxEmission->setFont(fonts->em2button, false);
	embTxEmission->ellipsis = true;
	EM2Button * embIES = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_IES));
	embIES->bgImage = hBmpBg;
	embIES->setFont(fonts->em2button, false);


	// GROUP SSS
	EM2GroupBar * emgrSSS = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_SSS));
	emgrSSS->bgImage = hBmpBg;
	emgrSSS->setFont(fonts->em2groupbar, false);
	EM2Text * emtSssDens = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DENSITY));
	emtSssDens->bgImage = hBmpBg;
	emtSssDens->setFont(fonts->em2text, false);
	EM2Text * emtSssDir = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DIRECTION));
	emtSssDir->bgImage = hBmpBg;
	emtSssDir->setFont(fonts->em2text, false);
	EM2CheckBox * emcSSS = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS));
	emcSSS->bgImage = hBmpBg;
	emcSSS->setFont(fonts->em2checkbox, false);
	EM2EditSpin * emesSssDens = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY));
	emesSssDens->setFont(fonts->em2editspin, false);
	emesSssDens->bgImage = hBmpBg;
	emesSssDens->setValuesToFloat(NOX_MAT_SSS_DENSITY_DEF, NOX_MAT_SSS_DENSITY_MIN, NOX_MAT_SSS_DENSITY_MAX, NOX_MAT_SSS_DENSITY_DEF, 1.0f, 0.5f);
	EM2EditSpin * emesSssDir = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION));
	emesSssDir->setFont(fonts->em2editspin, false);
	emesSssDir->bgImage = hBmpBg;
	emesSssDir->setValuesToFloat(NOX_MAT_SSS_COLL_DEF, NOX_MAT_SSS_COLL_MIN, NOX_MAT_SSS_COLL_MAX, NOX_MAT_SSS_COLL_DEF, 0.1f, 0.005f);


	// GROUP MAT LAYERS
	EM2GroupBar * emgrMLay = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_MAT_LAYERS));
	emgrMLay->bgImage = hBmpBg;
	emgrMLay->setFont(fonts->em2groupbar, false);

	EM2ListView * emList = GetEM2ListViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST));
	emList->setFontHeader(fonts->em2listheader, false);
	emList->setFontData(fonts->em2listentry, false);
	emList->bgImage = hBmpBg;
	emList->setColNumAndClearData(7, 50);
	emList->prepareWindow();
	emList->setHeaderName(0, "ID");
	emList->setHeaderName(1, "WEIGHT");
	emList->setHeaderName(2, "ROUGH");
	emList->setHeaderName(3, "NORMAL");
	emList->setHeaderName(4, "EMISS.");
	emList->setHeaderName(5, "TRANSM.");
	emList->setHeaderName(6, "SSS");
	emList->setHeightData(28);
	emList->setHeightHeader(31);
	emList->colWidths[0] = 61;
	emList->colWidths[1] = 51;
	emList->colWidths[2] = 47;
	emList->colWidths[3] = 55;
	emList->colWidths[4] = 43;
	emList->colWidths[5] = 57;
	emList->colWidths[6] = 32;
	for (int i=0; i<7; i++)
		emList->alignHeader[i] = emList->align[i] = EM2ListView::ALIGN_CENTER;


	EM2Button * embLAdd = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_ADD));
	embLAdd->bgImage = hBmpBg;
	embLAdd->setFont(fonts->em2button, false);
	EM2Button * embLRemove = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_REMOVE));
	embLRemove->bgImage = hBmpBg;
	embLRemove->setFont(fonts->em2button, false);
	EM2Button * embLClone = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_CLONE));
	embLClone->bgImage = hBmpBg;
	embLClone->setFont(fonts->em2button, false);
	EM2Button * embLReset = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_RESET));
	embLReset->bgImage = hBmpBg;
	embLReset->setFont(fonts->em2button, false);
	EM2Text * emtWeight = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_WEIGHT));
	emtWeight->bgImage = hBmpBg;
	emtWeight->setFont(fonts->em2text, false);
	EM2EditSpin * emesWeight = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT));
	emesWeight->setFont(fonts->em2editspin, false);
	emesWeight->bgImage = hBmpBg;
	emesWeight->setValuesToInt(NOX_MAT_CONTR_DEF, NOX_MAT_CONTR_MIN, NOX_MAT_CONTR_MAX, NOX_MAT_CONTR_DEF, 1, 0.2f);
	EM2CheckBox * emcTxOnWeight = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_WEIGHT));
	emcTxOnWeight->bgImage = hBmpBg;
	emcTxOnWeight->setFont(fonts->em2checkbox, false);
	EM2Button * embTxWeight= GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_WEIGHT));
	embTxWeight->bgImage = hBmpBg;
	embTxWeight->setFont(fonts->em2button, false);
	embTxWeight->ellipsis = true;


	// GROUP REFLECTANCE
	EM2GroupBar * emgrRefl = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_REFLECTANCE));
	emgrRefl->bgImage = hBmpBg;
	emgrRefl->setFont(fonts->em2groupbar, false);
	EM2Text * emtRough = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ROUGH));
	emtRough->bgImage = hBmpBg;
	emtRough->setFont(fonts->em2text, false);
	EM2Text * emtRefl0 = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL0));
	emtRefl0->bgImage = hBmpBg;
	emtRefl0->setFont(fonts->em2text, false);
	EM2Text * emtRefl90 = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL90));
	emtRefl90->bgImage = hBmpBg;
	emtRefl90->setFont(fonts->em2text, false);
	EM2Text * emtAniso = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOTROPY));
	emtAniso->bgImage = hBmpBg;
	emtAniso->setFont(fonts->em2text, false);
	EM2Text * emtAnisoAngle  = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOANGLE));
	emtAnisoAngle->bgImage = hBmpBg;
	emtAnisoAngle->setFont(fonts->em2text, false);
	EM2Text * emtRLinked = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_LINKED));
	emtRLinked->bgImage = hBmpBg;
	emtRLinked->setFont(fonts->em2text, false);
	emtRLinked->align = EM2Text::ALIGN_CENTER;
	EM2EditSpin * emesRough = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ROUGH));
	emesRough->setFont(fonts->em2editspin, false);
	emesRough->bgImage = hBmpBg;
	emesRough->floatAfterDot = 1;
	emesRough->setValuesToFloat(NOX_MAT_ROUGH_DEF, NOX_MAT_ROUGH_MIN, NOX_MAT_ROUGH_MAX, NOX_MAT_ROUGH_DEF, 1.0f, 0.05f);
	EM2EditSpin * emesAniso = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY));
	emesAniso->setFont(fonts->em2editspin, false);
	emesAniso->bgImage = hBmpBg;
	emesAniso->floatAfterDot = 1;
	emesAniso->setValuesToFloat(NOX_MAT_ANISO_DEF, NOX_MAT_ANISO_MIN, NOX_MAT_ANISO_MAX, NOX_MAT_ANISO_DEF, 1.0f, 0.5f);
	EM2EditSpin * emesAnisoAngle = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE));
	emesAnisoAngle->setFont(fonts->em2editspin, false);
	emesAnisoAngle->bgImage = hBmpBg;
	emesAnisoAngle->floatAfterDot = 1;
	emesAnisoAngle->setValuesToFloat(NOX_MAT_ANISO_ANGLE_DEF, NOX_MAT_ANISO_ANGLE_MIN, NOX_MAT_ANISO_ANGLE_MAX, NOX_MAT_ANISO_ANGLE_DEF, 1.0f, 0.5f);
	EM2ColorShow * emcsRefl0 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL0));
	emcsRefl0->bgImage = hBmpBg;
	emcsRefl0->setAllowedDragDrop();
	EM2ColorShow * emcsRefl90 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL90));
	emcsRefl90->bgImage = hBmpBg;
	emcsRefl90->setAllowedDragDrop();
	EM2CheckBox * emcLink = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL_LINK));
	emcLink->bgImage = hBmpBg;
	emcLink->setFont(fonts->em2checkbox, false);
	EM2CheckBox * emcTxOnRough = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ROUGH));
	emcTxOnRough->bgImage = hBmpBg;
	emcTxOnRough->setFont(fonts->em2checkbox, false);
	EM2Button * embTxRough = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ROUGH));
	embTxRough->bgImage = hBmpBg;
	embTxRough->setFont(fonts->em2button, false);
	embTxRough->ellipsis = true;
	EM2CheckBox * emcTxOnRefl0 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL0));
	emcTxOnRefl0->bgImage = hBmpBg;
	emcTxOnRefl0->setFont(fonts->em2checkbox, false);
	EM2Button * embTxRefl0 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL0));
	embTxRefl0->bgImage = hBmpBg;
	embTxRefl0->setFont(fonts->em2button, false);
	embTxRefl0->ellipsis = true;
	EM2CheckBox * emcTxOnRefl90 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL90));
	emcTxOnRefl90->bgImage = hBmpBg;
	emcTxOnRefl90->setFont(fonts->em2checkbox, false);
	EM2Button * embTxRefl90 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL90));
	embTxRefl90->bgImage = hBmpBg;
	embTxRefl90->setFont(fonts->em2button, false);
	embTxRefl90->ellipsis = true;
	EM2CheckBox * emcTxOnAniso = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO));
	emcTxOnAniso->bgImage = hBmpBg;
	emcTxOnAniso->setFont(fonts->em2checkbox, false);
	EM2Button * embTxAniso = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO));
	embTxAniso->bgImage = hBmpBg;
	embTxAniso->setFont(fonts->em2button, false);
	embTxAniso->ellipsis = true;
	EM2CheckBox * emcTxOnAnisoAngle = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO_ANGLE));
	emcTxOnAnisoAngle->bgImage = hBmpBg;
	emcTxOnAnisoAngle->setFont(fonts->em2checkbox, false);
	EM2Button * embTxAnisoAngle = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO_ANGLE));
	embTxAnisoAngle->bgImage = hBmpBg;
	embTxAnisoAngle->setFont(fonts->em2button, false);
	embTxAnisoAngle->ellipsis = true;


	// GROUP TRANSMISSION
	EM2GroupBar * emgrTransm = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_TRANSMISSION));
	emgrTransm->bgImage = hBmpBg;
	emgrTransm->setFont(fonts->em2groupbar, false);
	EM2CheckBox * emcTransm = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION));
	emcTransm->bgImage = hBmpBg;
	emcTransm->setFont(fonts->em2checkbox, false);
	EM2CheckBox * emcAbs = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION));
	emcAbs->bgImage = hBmpBg;
	emcAbs->setFont(fonts->em2checkbox, false);
	EM2CheckBox * emcDisp = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION));
	emcDisp->bgImage = hBmpBg;
	emcDisp->setFont(fonts->em2checkbox, false);
	EM2CheckBox * emcFakeGlass = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_FAKE_GLASS));
	emcFakeGlass->bgImage = hBmpBg;
	emcFakeGlass->setFont(fonts->em2checkbox, false);
	EM2ColorShow * emcsTransm = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION_COLOR));
	emcsTransm->bgImage = hBmpBg;
	emcsTransm->setAllowedDragDrop();
	EM2ColorShow * emcsAbs = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_COLOR));
	emcsAbs->bgImage = hBmpBg;
	emcsAbs->setAllowedDragDrop();
	EM2Text * emtOpacity = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_OPACITY));
	emtOpacity->bgImage = hBmpBg;
	emtOpacity->setFont(fonts->em2text, false);
	EM2Text * emtAbsDepth = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH2));
	emtAbsDepth->bgImage = hBmpBg;
	emtAbsDepth->setFont(fonts->em2text, false);
	emtAbsDepth->align = EM2Text::ALIGN_RIGHT;
	EM2Text * emtDispSize = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SIZE));
	emtDispSize->bgImage = hBmpBg;
	emtDispSize->setFont(fonts->em2text, false);
	emtDispSize->align = EM2Text::ALIGN_RIGHT;
	EM2EditSpin * emesOpacity = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_OPACITY));
	emesOpacity->setFont(fonts->em2editspin, false);
	emesOpacity->bgImage = hBmpBg;
	emesOpacity->floatAfterDot = 1;
	//emesOpacity->setValuesToFloat(NOX_MAT_OPACITY_DEF, NOX_MAT_OPACITY_MIN, NOX_MAT_OPACITY_MAX, NOX_MAT_OPACITY_DEF, 1.0f, 0.5f);
	emesOpacity->setValuesToInt(NOX_MAT_OPACITY_INT_DEF, NOX_MAT_OPACITY_INT_MIN, NOX_MAT_OPACITY_INT_MAX, NOX_MAT_OPACITY_INT_DEF, 1, 0.5f);
	EM2EditSpin * emesAbsDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH));
	emesAbsDepth->setFont(fonts->em2editspin, false);
	emesAbsDepth->bgImage = hBmpBg;
	emesAbsDepth->floatAfterDot = 1;
	emesAbsDepth->setValuesToFloat(NOX_MAT_ABSDIST_DEF, NOX_MAT_ABSDIST_MIN, NOX_MAT_ABSDIST_MAX, NOX_MAT_ABSDIST_DEF, 1.0f, 0.5f);
	EM2EditSpin * emesDispSize = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE));
	emesDispSize->setFont(fonts->em2editspin, false);
	emesDispSize->bgImage = hBmpBg;
	emesDispSize->setValuesToFloat(NOX_MAT_DISPERSION_DEF, NOX_MAT_DISPERSION_MIN, NOX_MAT_DISPERSION_MAX, NOX_MAT_DISPERSION_DEF, 0.5f, 0.005f);
	EM2CheckBox * emcTxOnTransm = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_TRANSM));
	emcTxOnTransm->bgImage = hBmpBg;
	emcTxOnTransm->setFont(fonts->em2checkbox, false);
	EM2Button * embTxTransm = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_TRANSM));
	embTxTransm->bgImage = hBmpBg;
	embTxTransm->setFont(fonts->em2button, false);
	embTxTransm->ellipsis = true;
	EM2CheckBox * emcTxOnOpacity = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_OPACITY));
	emcTxOnOpacity->bgImage = hBmpBg;
	emcTxOnOpacity->setFont(fonts->em2checkbox, false);
	EM2Button * embTxOpacity = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_OPACITY));
	embTxOpacity->bgImage = hBmpBg;
	embTxOpacity->setFont(fonts->em2button, false);
	embTxOpacity->ellipsis = true;


	// GROUP FRESNEL
	EM2GroupBar * emgrFresnel = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_FRESNEL));
	emgrFresnel->bgImage = hBmpBg;
	emgrFresnel->setFont(fonts->em2groupbar, false);

	EM2Text * emtIOR = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IOR));
	emtIOR->bgImage = hBmpBg;
	emtIOR->setFont(fonts->em2text, false);
	EM2EditSpin * emesIOR = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_IOR));
	emesIOR->setFont(fonts->em2editspin, false);
	emesIOR->bgImage = hBmpBg;
	emesIOR->setValuesToFloat(NOX_MAT_IOR_DEF, NOX_MAT_IOR_MIN, NOX_MAT_IOR_MAX, NOX_MAT_IOR_DEF, 0.1f, 0.05f);


	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::updateBgShifts()
{
	int px = 0;
	int py = 0;

	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TITLE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TITLE), px, py, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_OK));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_OK), px, py, embOK->bgShiftX, embOK->bgShiftY);
	EM2Button * embCancel= GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_CANCEL));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_CANCEL), px, py, embCancel->bgShiftX, embCancel->bgShiftY);


	// PREVIEW
	EM2GroupBar * emgPrev = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_PREVIEW));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_PREVIEW), px, py, emgPrev->bgShiftX, emgPrev->bgShiftY);
	EM2Text * emtName1 = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NAME));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NAME), px, py, emtName1->bgShiftX, emtName1->bgShiftY);
	EM2EditSimple * emesName = GetEM2EditSimpleInstance(GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME), px, py, emesName->bgShiftX, emesName->bgShiftY);
	EM2CheckBox * emcPortal = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SKYPORTAL));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_SKYPORTAL), px, py, emcPortal->bgShiftX, emcPortal->bgShiftY);
	EM2ComboBox * emcScene = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREV_SCENE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_PREV_SCENE), px, py, emcScene->bgShiftX, emcScene->bgShiftY);
	EM2Button * emcRender = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREV_RENDER));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_PREV_RENDER), px, py, emcRender->bgShiftX, emcRender->bgShiftY);
	EM2Button * embLibOpen = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LIBRARY_OPEN));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_LIBRARY_OPEN), px, py, embLibOpen->bgShiftX, embLibOpen->bgShiftY);
	EM2Button * embLoad = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LOAD));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_LOAD), px, py, embLoad->bgShiftX, embLoad->bgShiftY);
	EM2Button * embSave = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_SAVE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_SAVE), px, py, embSave->bgShiftX, embSave->bgShiftY);

	// GEOMETRY
	EM2GroupBar * emgGeom = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_GEOMETRY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_GEOMETRY), px, py, emgGeom->bgShiftX, emgGeom->bgShiftY);
	EM2Text * emtDispl  = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DISPLACEMENT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DISPLACEMENT), px, py, emtDispl->bgShiftX, emtDispl->bgShiftY);
	EM2Text * emtDepth1 = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH), px, py, emtDepth1->bgShiftX, emtDepth1->bgShiftY);
	EM2Text * emtShift  = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SHIFT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SHIFT), px, py, emtShift->bgShiftX, emtShift->bgShiftY);
	EM2Text * emtSubdivs = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SUBDIVISIONS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SUBDIVISIONS), px, py, emtSubdivs->bgShiftX, emtSubdivs->bgShiftY);
	EM2Text * emtNormals = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMALS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMALS), px, py, emtNormals->bgShiftX, emtNormals->bgShiftY);
	EM2Text * emtNormalBump = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMAL_BUMP_MAP));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMAL_BUMP_MAP), px, py, emtNormalBump->bgShiftX, emtNormalBump->bgShiftY);
	EM2CheckBox * emcTxOnDispl = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_DISPLACEMENT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_DISPLACEMENT), px, py, emcTxOnDispl->bgShiftX, emcTxOnDispl->bgShiftY);
	EM2Button * emcTxDispl = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_DISPLACEMENT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_DISPLACEMENT), px, py, emcTxDispl->bgShiftX, emcTxDispl->bgShiftY);
	EM2CheckBox * emcDisplIgnScale = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_IGNORE_SCALE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_IGNORE_SCALE), px, py, emcDisplIgnScale->bgShiftX, emcDisplIgnScale->bgShiftY);
	EM2EditSpin * emesDisplDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH), px, py, emesDisplDepth->bgShiftX, emesDisplDepth->bgShiftY);
	EM2EditSpin * emesDisplShift = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT), px, py, emesDisplShift->bgShiftX, emesDisplShift->bgShiftY);
	EM2ComboBox * emcDisplSubdivs = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SUBDIVS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SUBDIVS), px, py, emcDisplSubdivs->bgShiftX, emcDisplSubdivs->bgShiftY);
	EM2ComboBox * emcDisplNormals = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_NORMALS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_NORMALS), px, py, emcDisplNormals->bgShiftX, emcDisplNormals->bgShiftY);
	EM2CheckBox * emcDisplCont = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_CONTINUITY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_CONTINUITY), px, py, emcDisplCont->bgShiftX, emcDisplCont->bgShiftY);
	EM2CheckBox * emcTxOnNormBump = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_NORMALBUMP));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_NORMALBUMP), px, py, emcTxOnNormBump->bgShiftX, emcTxOnNormBump->bgShiftY);
	EM2Button * emcTxNormBump = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_NORMALBUMP));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_NORMALBUMP), px, py, emcTxNormBump->bgShiftX, emcTxNormBump->bgShiftY);

	// EMISSION
	EM2GroupBar * emgEmission = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_EMISSION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_EMISSION), px, py, emgEmission->bgShiftX, emgEmission->bgShiftY);
	EM2Text * emtPower = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_POWER));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_POWER), px, py, emtPower->bgShiftX, emtPower->bgShiftY);
	EM2Text * emtEmLayer = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_EMITTER_LAYER));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_EMITTER_LAYER), px, py, emtEmLayer->bgShiftX, emtEmLayer->bgShiftY);
	EM2Text * emtIes = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IES));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IES), px, py, emtIes->bgShiftX, emtIes->bgShiftY);
	EM2CheckBox * emcEmission = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISSION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_EMISSION), px, py, emcEmission->bgShiftX, emcEmission->bgShiftY);
	EM2CheckBox * emcEmissInvisible = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_INVISIBLE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_INVISIBLE), px, py, emcEmissInvisible->bgShiftX, emcEmissInvisible->bgShiftY);
	EM2ColorShow * emcEmissColor = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_COLOR));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_COLOR), px, py, emcEmissColor->bgShiftX, emcEmissColor->bgShiftY);
	EM2ImgButton * emcEmissTemp = GetEM2ImgButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_TEMPERATURE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_TEMPERATURE), px, py, emcEmissTemp->bgShiftX, emcEmissTemp->bgShiftY);
	EM2EditSpin * emesEmissPower = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER), px, py, emesEmissPower->bgShiftX, emesEmissPower->bgShiftY);
	EM2ComboBox * emcEmissUnit = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_UNITS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_UNITS), px, py, emcEmissUnit->bgShiftX, emcEmissUnit->bgShiftY);
	EM2ComboBox * emcEmissLayerCombo = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_LAYER_COMBO));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_LAYER_COMBO), px, py, emcEmissLayerCombo->bgShiftX, emcEmissLayerCombo->bgShiftY);
	EM2CheckBox * emcTxOnEmission = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_EMISS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_EMISS), px, py, emcTxOnEmission->bgShiftX, emcTxOnEmission->bgShiftY);
	EM2Button * emcTxEmission = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_EMISS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_EMISS), px, py, emcTxEmission->bgShiftX, emcTxEmission->bgShiftY);
	EM2Button * emcTxIes = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_IES));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_IES), px, py, emcTxIes->bgShiftX, emcTxIes->bgShiftY);

	// SSS
	EM2GroupBar * emgSSS = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_SSS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_SSS), px, py, emgSSS->bgShiftX, emgSSS->bgShiftY);
	EM2Text * emtDensity = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DENSITY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DENSITY), px, py, emtDensity->bgShiftX, emtDensity->bgShiftY);
	EM2Text * emtDirection  = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DIRECTION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DIRECTION), px, py, emtDirection->bgShiftX, emtDirection->bgShiftY);
	EM2EditSpin * emesSSSDensity = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY), px, py, emesSSSDensity->bgShiftX, emesSSSDensity->bgShiftY);
	EM2EditSpin * emesSSSDir = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION), px, py, emesSSSDir->bgShiftX, emesSSSDir->bgShiftY);
	EM2CheckBox * emcSSS = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_SSS), px, py, emcSSS->bgShiftX, emcSSS->bgShiftY);

	// MATLAYERS
	EM2GroupBar * emgMatLayers = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_MAT_LAYERS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_MAT_LAYERS), px, py, emgMatLayers->bgShiftX, emgMatLayers->bgShiftY);
	EM2ListView * emlListLayers = GetEM2ListViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST), px, py, emlListLayers->bgShiftX, emlListLayers->bgShiftY);
	EM2Button * emcLayersAdd = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_ADD));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_ADD), px, py, emcLayersAdd->bgShiftX, emcLayersAdd->bgShiftY);
	EM2Button * emcLayersRemove = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_REMOVE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_REMOVE), px, py, emcLayersRemove->bgShiftX, emcLayersRemove->bgShiftY);
	EM2Button * emcLayersClone = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_CLONE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_CLONE), px, py, emcLayersClone->bgShiftX, emcLayersClone->bgShiftY);
	EM2Button * emcLayersReset = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_RESET));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_RESET), px, py, emcLayersReset->bgShiftX, emcLayersReset->bgShiftY);
	EM2Text * emtWeight = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_WEIGHT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_WEIGHT), px, py, emtWeight->bgShiftX, emtWeight->bgShiftY);
	EM2EditSpin * emesWeight = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT), px, py, emesWeight->bgShiftX, emesWeight->bgShiftY);
	EM2CheckBox * emcTxOnWeight = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_WEIGHT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_WEIGHT), px, py, emcTxOnWeight->bgShiftX, emcTxOnWeight->bgShiftY);
	EM2Button * emcTxWeight = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_WEIGHT));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_WEIGHT), px, py, emcTxWeight->bgShiftX, emcTxWeight->bgShiftY);

	// REFLECTANCE
	EM2GroupBar * emgRefl = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_REFLECTANCE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_REFLECTANCE), px, py, emgRefl->bgShiftX, emgRefl->bgShiftY);
	EM2Text * emtRough = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ROUGH));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ROUGH), px, py, emtRough->bgShiftX, emtRough->bgShiftY);
	EM2Text * emtRefl0 = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL0));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL0), px, py, emtRefl0->bgShiftX, emtRefl0->bgShiftY);
	EM2Text * emtRefl90 = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL90));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL90), px, py, emtRefl90->bgShiftX, emtRefl90->bgShiftY);
	EM2Text * emtAniso = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOTROPY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOTROPY), px, py, emtAniso->bgShiftX, emtAniso->bgShiftY);
	EM2Text * emtAAngle = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOANGLE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOANGLE), px, py, emtAAngle->bgShiftX, emtAAngle->bgShiftY);
	EM2Text * emtLinked = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_LINKED));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_LINKED), px, py, emtLinked->bgShiftX, emtLinked->bgShiftY);
	EM2EditSpin * emesRough = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ROUGH));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_ROUGH), px, py, emesRough->bgShiftX, emesRough->bgShiftY);
	EM2ColorShow * emcRefl0 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL0));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_REFL0), px, py, emcRefl0->bgShiftX, emcRefl0->bgShiftY);
	EM2ColorShow * emcRefl90 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL90));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_REFL90), px, py, emcRefl90->bgShiftX, emcRefl90->bgShiftY);
	EM2CheckBox * emcReflLink = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL_LINK));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_REFL_LINK), px, py, emcReflLink->bgShiftX, emcReflLink->bgShiftY);
	EM2EditSpin * emesAniso = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY), px, py, emesAniso->bgShiftX, emesAniso->bgShiftY);
	EM2EditSpin * emesAAngle = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE), px, py, emesAAngle->bgShiftX, emesAAngle->bgShiftY);
	EM2CheckBox * emcTxOnRough = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ROUGH));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ROUGH), px, py, emcTxOnRough->bgShiftX, emcTxOnRough->bgShiftY);
	EM2Button * emcTxRough = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ROUGH));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ROUGH), px, py, emcTxRough->bgShiftX, emcTxRough->bgShiftY);
	EM2CheckBox * emcTxOnRefl0 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL0));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL0), px, py, emcTxOnRefl0->bgShiftX, emcTxOnRefl0->bgShiftY);
	EM2Button * emcTxRefl0 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL0));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL0), px, py, emcTxRefl0->bgShiftX, emcTxRefl0->bgShiftY);
	EM2CheckBox * emcTxOnRefl90 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL90));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL90), px, py, emcTxOnRefl90->bgShiftX, emcTxOnRefl90->bgShiftY);
	EM2Button * emcTxRefl90 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL90));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL90), px, py, emcTxRefl90->bgShiftX, emcTxRefl90->bgShiftY);
	EM2CheckBox * emcTxOnAniso = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO), px, py, emcTxOnAniso->bgShiftX, emcTxOnAniso->bgShiftY);
	EM2Button * emcTxAniso = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO), px, py, emcTxAniso->bgShiftX, emcTxAniso->bgShiftY);
	EM2CheckBox * emcTxOnAAngle = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO_ANGLE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO_ANGLE), px, py, emcTxOnAAngle->bgShiftX, emcTxOnAAngle->bgShiftY);
	EM2Button * emcTxAAngle = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO_ANGLE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO_ANGLE), px, py, emcTxAAngle->bgShiftX, emcTxAAngle->bgShiftY);

	// TRANSMISSION
	EM2GroupBar * emgTransm = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_TRANSMISSION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_TRANSMISSION), px, py, emgTransm->bgShiftX, emgTransm->bgShiftY);
	EM2Text * emtDepth = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH2));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH2), px, py, emtDepth->bgShiftX, emtDepth->bgShiftY);
	EM2Text * emtSize = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SIZE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SIZE), px, py, emtSize->bgShiftX, emtSize->bgShiftY);
	EM2Text * emtOpacity = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_OPACITY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_OPACITY), px, py, emtOpacity->bgShiftX, emtOpacity->bgShiftY);
	EM2CheckBox * emcTransm = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION), px, py, emcTransm->bgShiftX, emcTransm->bgShiftY);
	EM2CheckBox * emcAbsorpt = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION), px, py, emcAbsorpt->bgShiftX, emcAbsorpt->bgShiftY);
	EM2CheckBox * emcDispers = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION), px, py, emcDispers->bgShiftX, emcDispers->bgShiftY);
	EM2CheckBox * emcFakeGlass = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_FAKE_GLASS));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_FAKE_GLASS), px, py, emcFakeGlass->bgShiftX, emcFakeGlass->bgShiftY);
	EM2ColorShow * emcolTransm= GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION_COLOR));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION_COLOR), px, py, emcolTransm->bgShiftX, emcolTransm->bgShiftY);
	EM2ColorShow * emcolAbs = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_COLOR));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_COLOR), px, py, emcolAbs->bgShiftX, emcolAbs->bgShiftY);
	EM2EditSpin * emesOpacity = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_OPACITY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_OPACITY), px, py, emesOpacity->bgShiftX, emesOpacity->bgShiftY);
	EM2EditSpin * emesAbsDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH), px, py, emesAbsDepth->bgShiftX, emesAbsDepth->bgShiftY);
	EM2EditSpin * emesDispSize = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE), px, py, emesDispSize->bgShiftX, emesDispSize->bgShiftY);
	EM2CheckBox * emcTxOnTransm = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_TRANSM));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_TRANSM), px, py, emcTxOnTransm->bgShiftX, emcTxOnTransm->bgShiftY);
	EM2Button * emcTxTransm = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_TRANSM));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_TRANSM), px, py, emcTxTransm->bgShiftX, emcTxTransm->bgShiftY);
	EM2CheckBox * emcTxOnOpacity = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_OPACITY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TXON_OPACITY), px, py, emcTxOnOpacity->bgShiftX, emcTxOnOpacity->bgShiftY);
	EM2Button * emcTxOpacity = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_OPACITY));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TX_OPACITY), px, py, emcTxOpacity->bgShiftX, emcTxOpacity->bgShiftY);

	// FRESNEL
	EM2GroupBar * emgFresnel = GetEM2GroupBarInstance(GetDlgItem(hMain, IDC_MATEDIT2_GR_FRESNEL));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_GR_FRESNEL), px, py, emgFresnel->bgShiftX, emgFresnel->bgShiftY);
	EM2Text * emtIor = GetEM2TextInstance(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IOR));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IOR), px, py, emtIor->bgShiftX, emtIor->bgShiftY);
	EM2EditSpin * emesIor = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_IOR));
	updateControlBgShift(GetDlgItem(hMain, IDC_MATEDIT2_IOR), px, py, emesIor->bgShiftX, emesIor->bgShiftY);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::showMatlib(bool on)
{
	int w = on ? 1138 : 759;
	int h = 704;
	isMatLibOpened = on;

	RECT tmprect, wrect,crect;
	GetWindowRect(hMain, &wrect);
	GetClientRect(hMain, &crect);
	tmprect.left = 0;		tmprect.top = 0;
	tmprect.right = w + wrect.right-wrect.left-crect.right;
	tmprect.bottom = h + wrect.bottom-wrect.top-crect.bottom;
	SetWindowPos(hMain, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);


	setPositions(on);
	updateBgShifts();

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::setPositions(bool matLibOn)
{
	int xl = matLibOn ? 379 : 0;
	int x,y;

	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TITLE), HWND_TOP, xl+280,10, 200, 19, 0);

	// PREVIEW
	x=xl+10;	y=39;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_PREVIEW),			HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_PREVIEW),				HWND_TOP,	x+0,	y+27,		192,	192,	SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NAME),				HWND_TOP,	x+200,	y+26,		35,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME),			HWND_TOP,	x+236,	y+26,		120,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_SKYPORTAL),				HWND_TOP,	x+200,	y+59,		85,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_MATTE_SHADOW),			HWND_TOP,	x+200,	y+89,		100,	10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_PREV_SCENE),			HWND_TOP,	x+200,	y+137,		160,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_PREV_RENDER),			HWND_TOP,	x+200,	y+167,		160,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_LIBRARY_OPEN),			HWND_TOP,	x+200,	y+197-90,	160,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_LOAD),					HWND_TOP,	x+200,	y+197,		76,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_SAVE),					HWND_TOP,	x+284,	y+197,		76,		22,		SWP_NOZORDER);


	// GEOMETRY
	x=xl+10;	y=269;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_GEOMETRY),			HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DISPLACEMENT),		HWND_TOP,	x+0,	y+30,		85,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_IGNORE_SCALE),	HWND_TOP,	x+0,	y+63,		95,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SUBDIVISIONS),		HWND_TOP,	x+0,	y+90,		75,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SUBDIVS),			HWND_TOP,	x+79,	y+87,		40,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_CONTINUITY),		HWND_TOP,	x+0,	y+123,		70,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMAL_BUMP_MAP),	HWND_TOP,	x+0,	y+150,		120,	16,		SWP_NOZORDER);
	
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_DISPLACEMENT),		HWND_TOP,	x+181,	y+33,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_DISPLACEMENT),		HWND_TOP,	x+202,	y+27,		158,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH),			HWND_TOP,	x+159,	y+60,		40,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH),			HWND_TOP,	x+202,	y+58,		54,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SHIFT),			HWND_TOP,	x+269,	y+60,		37,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT),			HWND_TOP,	x+306,	y+58,		54,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_NORMALS),			HWND_TOP,	x+235,	y+90,		60,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_NORMALS),			HWND_TOP,	x+295,	y+87,		65,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_NORMALBUMP),		HWND_TOP,	x+181,	y+153,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_NORMALBUMP),			HWND_TOP,	x+202,	y+147,		158,	22,		SWP_NOZORDER);


	// EMISSION
	x=xl+10;	y=449;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_EMISSION),			HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISSION),				HWND_TOP,	x+0,	y+33,		70,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_COLOR),			HWND_TOP,	x+76,	y+32,		14,		14,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_TEMPERATURE),		HWND_TOP,	x+99,	y+32,		14,		14,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_POWER),			HWND_TOP,	x+0,	y+60,		45,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER),			HWND_TOP,	x+47,	y+58,		82,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_UNITS),			HWND_TOP,	x+135,	y+57,		50,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_EMITTER_LAYER),	HWND_TOP,	x+0,	y+90,		40,		16,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_LAYER_COMBO),		HWND_TOP,	x+42,	y+87,		125,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_INVISIBLE),		HWND_TOP,	x+0,	y+123,		70,		10,		SWP_NOZORDER);

	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_EMISS),			HWND_TOP,	x+181,	y+33,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_EMISS),				HWND_TOP,	x+202,	y+27,		158,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IES),				HWND_TOP,	x+161,	y+90,		30,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_IES),					HWND_TOP,	x+202,	y+87,		158,	22,		SWP_NOZORDER);

	// SSS
	//x=xl+10;	y=629;
	x=xl+10;	y=599;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_SSS),				HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_SSS),					HWND_TOP,	x+0,	y+33,		40,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DENSITY),			HWND_TOP,	x+79,	y+30,		50,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY),			HWND_TOP,	x+137,	y+28,		67,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DIRECTION),		HWND_TOP,	x+240,	y+30,		62,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION),			HWND_TOP,	x+305,	y+28,		55,		20,		SWP_NOZORDER);	//

	// MATERIAL LAYERS
	x=xl+390;	y=39;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_MAT_LAYERS),			HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST),			HWND_TOP,	x+0,	y+19,		360,	146,	SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_ADD),				HWND_TOP,	x+0,	y+167,		75,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_REMOVE),			HWND_TOP,	x+82,	y+167,		95,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_CLONE),			HWND_TOP,	x+184,	y+167,		87,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_LAYER_RESET),			HWND_TOP,	x+278,	y+167,		82,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_WEIGHT),			HWND_TOP,	x+0,	y+200,		85,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT),				HWND_TOP,	x+88,	y+198,		48,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_WEIGHT),			HWND_TOP,	x+181,	y+203,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_WEIGHT),				HWND_TOP,	x+202,	y+197,		158,	22,		SWP_NOZORDER);

	// REFLECTANCE
	x=xl+390;	y=269;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_REFLECTANCE),		HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ROUGH),			HWND_TOP,	x+0,	y+30,		70,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ROUGH),					HWND_TOP,	x+88,	y+28,		60,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL0),			HWND_TOP,	x+0,	y+60,		85,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_REFL90),			HWND_TOP,	x+0,	y+90,		85,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_REFL0),					HWND_TOP,	x+88,	y+62,		14,		14,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_REFL90),				HWND_TOP,	x+88,	y+92,		14,		14,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_REFL_LINK),				HWND_TOP,	x+113,	y+78,		40,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOTROPY),		HWND_TOP,	x+0,	y+120,		70,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY),			HWND_TOP,	x+88,	y+118,		60,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_ANISOANGLE),		HWND_TOP,	x+0,	y+150,		70,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE),			HWND_TOP,	x+88,	y+148,		60,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_LINKED),			HWND_TOP,	x+258,	y+90,		48,		16,		SWP_NOZORDER);

	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ROUGH),			HWND_TOP,	x+181,	y+33,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_ROUGH),				HWND_TOP,	x+202,	y+27,		158,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL0),			HWND_TOP,	x+181,	y+63,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL0),				HWND_TOP,	x+202,	y+57,		158,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL90),			HWND_TOP,	x+181,	y+93,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL90),				HWND_TOP,	x+202,	y+87,		158,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO),			HWND_TOP,	x+181,	y+123,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO),				HWND_TOP,	x+202,	y+117,		158,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO_ANGLE),		HWND_TOP,	x+181,	y+153,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO_ANGLE),		HWND_TOP,	x+202,	y+147,		158,	22,		SWP_NOZORDER);

	// TRANSMISSION
	x=xl+390;	y=449;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_TRANSMISSION),		HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION),			HWND_TOP,	x+0,	y+33,		102,	10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION),			HWND_TOP,	x+0,	y+63,		102,	10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_FAKE_GLASS),			HWND_TOP,	x+0,	y+93,		85,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION_COLOR),	HWND_TOP,	x+102,	y+32,		14,		14,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_COLOR),		HWND_TOP,	x+102,	y+62,		14,		14,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_OPACITY),			HWND_TOP,	x+0,	y+120,		55,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_OPACITY),				HWND_TOP,	x+55,	y+118,		48,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_DEPTH2),			HWND_TOP,	x+231,	y+60,		70,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SIZE),				HWND_TOP,	x+271,	y+90,		30,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH),		HWND_TOP,	x+308,	y+58,		52,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION),			HWND_TOP,	x+220,	y+93,		85,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE),		HWND_TOP,	x+308,	y+88,		52,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_TRANSM),			HWND_TOP,	x+181,	y+33,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_TRANSM),				HWND_TOP,	x+202,	y+27,		158,	22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TXON_OPACITY),			HWND_TOP,	x+181,	y+123,		10,		10,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TX_OPACITY),			HWND_TOP,	x+202,	y+117,		158,	22,		SWP_NOZORDER);
	ShowWindow(  GetDlgItem(hMain, IDC_MATEDIT2_TEXT_SIZE), SW_HIDE);

	// FRESNEL
	x=xl+390;	y=599;
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_GR_FRESNEL),			HWND_TOP,	x+0,	y+0,		360,	16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_IOR),				HWND_TOP,	x+0,	y+30,		26,		16,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_IOR),					HWND_TOP,	x+26,	y+28,		60,		20,		SWP_NOZORDER);	//
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_OK),					HWND_TOP,	xl+294,	667,		75,		22,		SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_CANCEL),				HWND_TOP,	xl+390,	667,		75,		22,		SWP_NOZORDER);

	// tab order
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME),		HWND_TOP,											0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH),		GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT),		GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER),		GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY),		GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION),		GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT),			GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ROUGH),				GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT),				0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY),		GetDlgItem(hMain, IDC_MATEDIT2_ROUGH),				0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE),		GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH),	GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE),	GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_OPACITY),			GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hMain, IDC_MATEDIT2_IOR),				GetDlgItem(hMain, IDC_MATEDIT2_OPACITY),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);

	return true;
}

//----------------------------------------------------------------------

