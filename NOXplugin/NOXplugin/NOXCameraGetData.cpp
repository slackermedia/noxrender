#include "max2013predef.h"
#include "NOXCamera.h"
#include <decomp.h>



//-----------------------------------------------------------------------------------------------------------------------

BOOL NOXCamera::IsOrtho()
{
	return trCam.getIsOrtho();
}

//-----------------------------------------------------------------------------------------------------------------------

float NOXCamera::GetFOV(TimeValue t, Interval& valid)
{
	return trCam.getFOV();
}

//-----------------------------------------------------------------------------------------------------------------------

float NOXCamera::GetTDist(TimeValue t, Interval& valid)
{
	return tDist;
}

//-----------------------------------------------------------------------------------------------------------------------

int NOXCamera::GetManualClip()
{
	return trCam.getManualClip();
}

//-----------------------------------------------------------------------------------------------------------------------

float NOXCamera::GetClipDist(TimeValue t, int which, Interval &valid)
{
	if (which == CAM_HITHER_CLIP)
		return trCam.getClipDistHither();
	if (which == CAM_YON_CLIP)
		return trCam.getClipDistYon();
	return 0.0f;
}

//-----------------------------------------------------------------------------------------------------------------------

float NOXCamera::GetEnvRange(TimeValue t, int which, Interval& valid)
{
	if (which == ENV_NEAR_RANGE)
		return trCam.getEnvRangeNear();
	if (which == ENV_FAR_RANGE)
		return trCam.getEnvRangeFar();
	return 0.0f;
}

//-----------------------------------------------------------------------------------------------------------------------

BOOL NOXCamera::GetEnvDisplay(void)
{
	return envDisplay;
}

//-----------------------------------------------------------------------------------------------------------------------

int NOXCamera::GetConeState()
{
	return coneState;
}

//-----------------------------------------------------------------------------------------------------------------------

int NOXCamera::GetHorzLineState()
{
	return horzLineState;
}

//-----------------------------------------------------------------------------------------------------------------------

int NOXCamera::GetFOVType()
{
	return trCam.getFOVType();
}

//-----------------------------------------------------------------------------------------------------------------------

Control * NOXCamera::GetFOVControl()
{
	return cntrl;
}

//-----------------------------------------------------------------------------------------------------------------------

int NOXCamera::Type()
{
	return trCam.getType();
}

//-----------------------------------------------------------------------------------------------------------------------

