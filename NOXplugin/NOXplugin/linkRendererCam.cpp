#include "linkRenderer.h"

#ifdef PORTABLE_COMPILATION
	#include "noxinclude/resource.h"
	#include "noxinclude/Dialogs.h"
#else
	#include "../../Everything DLL/Everything DLL/resource.h"
	#include "../../Everything DLL/Everything DLL/Dialogs.h"
#endif

#include "noxdebug.h"

static void changeFOVCallback(float focal, float focus)
{
	notifyNOXCameraCallback(focal, focus);
}

bool TrCamera::createSimpleCamera()
{
	ADD_LOG_PARTS_MAIN("TrCamera::createSimpleCamera");
	Camera * sCam = new Camera();
	address = (void*)sCam;
	return true;
}

bool TrCamera::updateNOXfromMAX()
{
	if (!address)
		createSimpleCamera();

	Camera * nCam = (Camera*)address;
	nCam->setLengthfromFovHori36(getFOV()*180/PI);

	return true;
}

void TrCamera::runEditor(HINSTANCE hInst, HWND hParent)
{
	ADD_LOG_PARTS_MAIN("TrCamera::runEditor");
	updateNOXfromMAX();
	Camera * cam = (Camera *)address;
	registerCameraUpdateNotification(changeFOVCallback);
	showPluginCameraDialog(hParent, cam);
}

char* TrCamera::getName()
{
	return NULL;
}

TrCamera::TrCamera()
{
	ADD_LOG_CONSTR("TrCamera Constructor");
	active = true;
	fov = 0.94998271186051358871956488528835f;
	clipDistHither = 1;
	clipDistYon = 1000;
	envRangeNear = 0;
	envRangeFar = 1000;
	manualClip = 0;
	horLine = 0;
	fovType = 0;
	camType = 0;
	createSimpleCamera();
}

TrCamera::~TrCamera()
{
	ADD_LOG_CONSTR("TrCamera Destructor");
	if (address)
	{
		Camera * tCam = (Camera*)address;
		delete tCam;
		address = NULL;
	}
}

