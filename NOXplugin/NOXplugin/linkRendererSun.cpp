#include "linkRenderer.h"
#include <tchar.h>

#ifdef PORTABLE_COMPILATION
	#include "noxinclude/resource.h"
	#include "noxinclude/Dialogs.h"
#else
	#include "../../Everything DLL/Everything DLL/resource.h"
	#include "../../Everything DLL/Everything DLL/Dialogs.h"
#endif

#include <math.h>
#include <float.h>

#include "noxdebug.h"

//---------------------------------------  CONSTRUCTOR / DESTRUCTOR

TrSunSky::TrSunSky()
{
	ADD_LOG_CONSTR("TrSunsky contructor");
	sunskyOn = true;
	addressSunSky = NULL;
	addressEnv = NULL;
	targetHidden = false;
	createSunSky();	
	createEnv();
}

TrSunSky::TrSunSky(TrSunSky &src)
{
	if (this == &src)
		return;
	sunskyOn = src.sunskyOn;
	addressSunSky = NULL;
	addressEnv = NULL;
	targetHidden = src.targetHidden;
	createSunSky();
	this->setAerosol(  src.getAerosol());
	this->setDay(      src.getDay());
	this->setGMT(      src.getGMT());
	this->setHour(     src.getHour());
	this->setLatitude( src.getLatitude());
	this->setLongitude(src.getLongitude());
	this->setMinute(   src.getMinute());
	this->setMonth(    src.getMonth());
	this->setTurbidity(src.getTurbidity());
}

TrSunSky::~TrSunSky()
{
	if (addressSunSky)
	{
		SunSky * tSun = (SunSky*)addressSunSky;
		delete tSun;
		addressSunSky = NULL;
	}
	if (addressEnv)
	{
		EnvSphere * tEnv = (EnvSphere *)addressEnv;
		tEnv->tex.setEmptyTex();
		tEnv->enabled = false;
		tEnv->powerEV = 0.0f;
		tEnv->blendlayer = 3;
		tEnv->azimuth_shift = 0.0f;
		Raytracer * rtr = Raytracer::getInstance();
		rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);
		addressEnv = NULL;
	}
}

//---------------------------------------	CALLBACK

static void changeSunPosCallback(int month, int day, int hour, int minute, int gmt, float longitude, float latitude, float env_azimuth)
{
	notifyNOXSunSkyCallback(month, day, hour, minute, gmt, longitude, latitude, env_azimuth);
}

//---------------------------------------	PREPARE FOR DIALOG

bool TrSunSky::updateNOXfromMAX()
{
	if (!addressSunSky)
		createSunSky();

	SunSky * sunsky = (SunSky*)addressSunSky;
	return true;
}

//---------------------------------------	SUNSKY CREATION

bool TrSunSky::createSunSky()
{
	ADD_LOG_PARTS_MAIN("TrSunsky::createSunSky");
	SunSky * sunsky = new SunSky();
	if (addressSunSky)
		delete (SunSky *)addressSunSky;
	addressSunSky = (void*)sunsky;
	sunsky->use_hosek = true;
	sunsky->setDate(sunsky->getMonth(), sunsky->getDay(), sunsky->getHour(), sunsky->getMinute());
	sunsky->setPosition(sunsky->getLongitude(), sunsky->getLatitude());
	return true;
}

//---------------------------------------	ENV CREATE

bool TrSunSky::createEnv()
{
	char buf[256];
	Raytracer * rtr = Raytracer::getInstance();
	for (int i=0; i<rtr->scenes.objCount; i++)
	{
		sprintf_s(buf, 256, "scene %d - %lld", (i+1), (INT_PTR)rtr->scenes[i]);
		ADD_LOG_PARTS_MAIN(buf);
	}

	EnvSphere * env = &rtr->curScenePtr->env;
	addressEnv = (void*)env;
	return true;
}

//---------------------------------------	RUN DIALOG

void TrSunSky::runDialog(HINSTANCE hInst, HWND hParent)
{
	ADD_LOG_PARTS_MAIN("TrSunsky::runDialog");
	updateNOXfromMAX();
	SunSky * sunsky = (SunSky*)addressSunSky;
	EnvSphere * env = (EnvSphere*)addressEnv;

	SunSky * backupSS = new SunSky();
	backupSS->copyFrom(sunsky);
	EnvSphere * backupEnv = new EnvSphere();
	*backupEnv = *env;

	registerSunSkyUpdateNotification(&changeSunPosCallback);
	EnvSunSky * res = showPluginSunSkyDialog(hParent, sunsky, env);

	if (!res  ||  !res->sunsky)
	{
		sunsky->copyFrom(backupSS);
		changeSunPosCallback(backupSS->getMonth(), backupSS->getDay(), backupSS->getHour(), (int)backupSS->getMinute(), 
				(int)(backupSS->getMeridianForTimeZone()*12/PI), backupSS->getLongitude(), backupSS->getLatitude(), backupEnv->azimuth_shift);
	}
	else
		sunsky->copyFrom(res->sunsky);

	if (!res  ||  !res->env)
		*env = *backupEnv;
	else
		*env = *res->env;

	delete backupSS;
}

//---------------------------------------	EVAL LATITUDE

float evalLatitude(float alt, float azi, float decl)
{
	float cosalt = cos(alt);
	float sinalt = sin(alt);
	float cosazi = cos(azi);
	float sinazi = sin(azi);
	float sindecl = sin(decl);
	float a = sinalt;
	float b = cosalt * cosazi;
	float c = -sindecl;
	if (a < 0)
	{
		a *= -1;
		b *= -1;
		c *= -1;
	}
	float R = sqrt(a*a+b*b);
	float alfa = atan(b/a);

	float latitude = (asin(-c/R)) - (alfa);

	if (latitude > PI/2)
		latitude = PI - latitude;

	if (latitude < -PI/2)
		latitude = -PI - latitude;

	return latitude;
}

//---------------------------------------	SET POSITION FROM ALT/AZI

void TrSunSky::setPosFromDir(float alt, float azi)
{
	if (!addressSunSky)
		return;
	SunSky * sunsky = (SunSky*)addressSunSky;
	int J = sunsky->getDayOfYear();
	float solarGlobalTimeInHours = (float)(sunsky->earthTimeInHours + 0.170*sin(4*PI*(J-80)/373.0f)
		- 0.129*sin(2*PI*(J-8)/355)
		+ 12*(sunsky->standardMeridianForTimeZone)/PI);
	float solarDeclination = (float)(0.4092797096*sin(2*PI*(J-81)/368.0f));		// angle in radians
	float sindec = sin(solarDeclination);
	float cosdec = cos(solarDeclination);

	float lat = evalLatitude(alt, azi, solarDeclination);
	float sinlat = sin(lat);
	float coslat = cos(lat);

	float sinsol = -sin(azi)*cos(alt)/cos(solarDeclination);
	float solarTimeInHours = asin(sinsol) * 12/PI;

	if (alt < 0)
	{
		if (solarTimeInHours > 0)
			solarTimeInHours = 12 - solarTimeInHours;
		else
			solarTimeInHours = -12 - solarTimeInHours;
	}

	float earthTimeInHours = sunsky->earthTimeInHours;
	earthTimeInHours += 12; //*= -1;
	int rr = (int)(earthTimeInHours/24);
	earthTimeInHours -= 24*rr;
	float lng =	-(solarTimeInHours - earthTimeInHours 
				- 0.170f*sin(4*PI*(J-80)/373.0f)
				+ 0.129f*sin(2*PI*(J-8)/355))*PI/12
				+ sunsky->standardMeridianForTimeZone;

	if (lng > PI)
		lng -= 2*PI;
	if (lng < -PI)
		lng += 2*PI;
		
	sunsky->setPosition(lng, lat);
}

//---------------------------------------	CORRECT AZIMUTH FOR LATITUDE EVALUATION

bool correctLatitude(float alt, float &azi, float decl)
{
	float cosalt = cos(alt);
	float cosazi = cos(azi);
	float sinalt = sin(alt);
	float sindecl = sin(decl);
	float a = sinalt;
	float b = cosalt * cosazi;
	float c = -sindecl;
	float R = sqrt(a*a+b*b);
	float alfa = atan(b/a);

	float dd = -c/R;
	if (fabs(dd) > 1)
	{
		if (dd > 1)
			dd = 0.999f;
		else 
			dd = -0.999f;
		R = -c/dd;
		if (b>0)
			b = sqrt(R*R - a*a);
		else
			b = -sqrt(R*R - a*a);
		float newcosazi = b / cosalt;
		if (cosazi > 0)
			newcosazi = fabs(newcosazi);
		else
			newcosazi = -fabs(newcosazi);
		float newazi = acos(newcosazi);
		if (azi<PI)
			newazi = fabs(newazi);
		else
			newazi = -fabs(newazi)+2*PI;
		
		azi = newazi;
		return true;
	}
	return false;
}

//---------------------------------------	CORRECT DIRECTION

bool TrSunSky::correctDirection(float &alt, float &azi)
{
	if (!addressSunSky)
	{
		if (!createSunSky())
		{
			MessageBox(0, _T("Error while correcting azimuth. NULL!"), _T(""), 0);
			return false;
		}
	}
	SunSky * sunsky = (SunSky*)addressSunSky;
	int J = sunsky->getDayOfYear();
	float solarGlobalTimeInHours = (float)(sunsky->earthTimeInHours + 0.170*sin(4*PI*(J-80)/373.0f)
		- 0.129*sin(2*PI*(J-8)/355)
		+ 12*(sunsky->standardMeridianForTimeZone)/PI);
	float solarDeclination = (float)(0.4092797096*sin(2*PI*(J-81)/368.0f));		// angle in radians
	float sindec = sin(solarDeclination);
	float cosdec = cos(solarDeclination);

	float oldazi = azi;
	bool corrLat = correctLatitude(alt, azi, solarDeclination);
	if (_isnan(azi))
		MessageBox(0, _T("Error while correcting azimuth. Latitude correction fail."), _T(""), 0);

	float lat = evalLatitude(alt, azi, solarDeclination);

	float sinlat = sin(lat);
	float coslat = cos(lat);

	float sinsol = -sin(azi)*cos(alt)/cos(solarDeclination);
	if (fabs(sinsol) > 1 )
		return corrLat;
	if (corrLat)
		return true;
	return false;
}

//---------------------------------------	PREPARE TO EXPORT

bool TrSunSky::prepareForExport()
{
	return true;
}

//---------------------------------------	GET AZI / ALT FROM SUNSKY

void TrSunSky::getAziAltFromNOX(float &azi,  float &alt)
{
	if (!addressSunSky)
		createSunSky();
	SunSky * ssky = (SunSky*)addressSunSky;
	azi = ssky->phiS;
	alt = ssky->thetaS;
}

//---------------------------------------

bool TrSunSky::getEnvTex(BITMAPINFO ** bmi)
{
	if (!bmi)
		return false;

	if (!addressEnv)
	{
		*bmi = NULL;
		return false;
	}

	EnvSphere * env = (EnvSphere*)addressEnv;
	if (!env->tex.isValid())
	{
		*bmi = NULL;
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Texture * tex = NULL;
	if (env->tex.managerID>=0)
		tex = sc->texManager.textures[env->tex.managerID];
	if (!tex)
	{
		*bmi = NULL;
		return false;
	}

	int bmw = tex->bmp.getWidth();
	int bmh = tex->bmp.getHeight();

	BITMAPINFO * bitmapInfoEnv = (BITMAPINFO *)malloc(sizeof(BITMAPINFOHEADER) + bmw * bmh * sizeof(RGBQUAD));
	BITMAPINFOHEADER *bmih = &bitmapInfoEnv->bmiHeader;
	bmih->biSize = sizeof (BITMAPINFOHEADER);
	bmih->biWidth = bmw;
   	bmih->biHeight = bmh;
	bmih->biPlanes = 1;
	bmih->biBitCount = 32;
	bmih->biCompression = BI_RGB;
	bmih->biSizeImage = bmih->biWidth * bmih->biHeight * sizeof(RGBQUAD);
	bmih->biXPelsPerMeter = 0;
   	bmih->biYPelsPerMeter = 0;
	bmih->biClrUsed = 0;
	bmih->biClrImportant = 0;

	DWORD *row = (DWORD *)((BYTE *)bitmapInfoEnv + sizeof(BITMAPINFOHEADER));

	TexModifer tmod = env->tex.texMod;
	tmod.gamma = 1.0f;

	for(int y = 0; y < bmh; y++)
	{
		DWORD dw;
		int r,g,b;
		for(int x = 0; x < bmw; ++x)//, ++p64) 
		{
			Color4 c = tex->bmp.getColor(x, bmh-y-1);
			c = tex->processColor(c, &tmod);

			r = min(255, max(0, (int)(c.r*255)));
			g = min(255, max(0, (int)(c.g*255)));
			b = min(255, max(0, (int)(c.b*255)));
			dw = (r << 16) | (g << 8) | b ; 
			row[x] = dw;
		}
		row += bmih->biWidth;
	}

	*bmi = bitmapInfoEnv;

	return true;
}
