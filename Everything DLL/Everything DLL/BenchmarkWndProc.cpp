#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include "BenchmarkWindow.h"
#include <gdiplus.h>
#include "log.h"

extern char * bench_license_text;
int MessageBoxNox(HWND hParent, char *messageText, char *titleText, HFONT hFontText, HFONT hFontButton);

//----------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK BenchmarkWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		{
			BenchmarkWindow::backgroundBrush = CreateSolidBrush(RGB(40,40,40));
		}	// end WM_CREATE
		break;
	case WM_GETMINMAXINFO:
		{
			MINMAXINFO * mmi = (MINMAXINFO *)lParam;
			if (!mmi)
				break;

			RECT crect;
			crect.left = 0;
			crect.top = 0;
			crect.right = 1269;
			crect.bottom = 771;
			AdjustWindowRect(&crect, GetWindowLong(hMain, GWL_STYLE), FALSE);

			mmi->ptMinTrackSize.x = mmi->ptMaxTrackSize.x = crect.right-crect.left;
			mmi->ptMinTrackSize.y = mmi->ptMaxTrackSize.y = crect.bottom-crect.top;
		}
		break;
	case WM_SIZE:
		{
			RECT crect, irect;
			GetClientRect(hWnd, &crect);
			GetClientRect(hImage, &irect);
		}
		break;
	case WM_CLOSE:
		{
			DestroyWindow(hMain);
			return 0;
		}
		break;
	case WM_DESTROY:
		{
			Logger::add("Closing benchmark main window. Bye :)");
			Logger::closeFile();
			unloadFonts();
			PostQuitMessage(0);
		}
		break;
	case WM_PAINT:
		{
			BOOL ur;
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect, urect;

			ur = GetUpdateRect(hWnd, &urect, TRUE); 
			GetClientRect(hWnd, &rect);
			if (ur)
				rect = urect;

			hdc = BeginPaint(hWnd, &ps);

			HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
			HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
			HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

			HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
			HBITMAP * curBitmap = NULL;
			if (cmode==NBMODE_LICENSE)
				curBitmap = &hBmpBgLic;
			else
				if (cmode==NBMODE_LOADING  ||  cmode==NBMODE_ABOUT)
					curBitmap = &hBmpBgEmpty;
				else
					curBitmap = &hBmpBg;
			HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, *curBitmap);
			BITMAP tbitmap;
			GetObject(hBmpBg, sizeof(tbitmap), &tbitmap);
			BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
			SelectObject(thdc, oldBitmap);
			DeleteDC(thdc);

			if (cmode==NBMODE_NORMAL  ||  cmode==NBMODE_RENDERING  ||  cmode==NBMODE_LICENSE  ||  cmode==NBMODE_LOADING  ||  bResult>0.0f)
			{
				RECT frect, brect, l1rect, l2rect, urect;
				GetWindowRect(hImage, &frect);
				POINT fleftup = { frect.left, frect.top };
				POINT frightdown = { frect.right, frect.bottom };
				ScreenToClient(hMain, &fleftup);
				ScreenToClient(hMain, &frightdown);

				GetWindowRect(hStartStop, &brect);
				POINT bleftup = { brect.left, brect.top };
				POINT brightdown = { brect.right, brect.bottom };
				ScreenToClient(hMain, &bleftup);
				ScreenToClient(hMain, &brightdown);
				
				GetWindowRect(hSendResult, &urect);
				POINT uleftup = { urect.left, urect.top };
				POINT urightdown = { urect.right, urect.bottom };
				ScreenToClient(hMain, &uleftup);
				ScreenToClient(hMain, &urightdown);

				GetWindowRect(hLicAccept, &l1rect);
				POINT l1leftup = { l1rect.left, l1rect.top };
				POINT l1rightdown = { l1rect.right, l1rect.bottom };
				ScreenToClient(hMain, &l1leftup);
				ScreenToClient(hMain, &l1rightdown);

				GetWindowRect(hProgress, &l2rect);
				POINT l2leftup = { l2rect.left, l2rect.top };
				POINT l2rightdown = { l2rect.right, l2rect.bottom };
				ScreenToClient(hMain, &l2leftup);
				ScreenToClient(hMain, &l2rightdown);

				ULONG_PTR gdiplusToken;		// activate gdi+
				Gdiplus::GdiplusStartupInput gdiplusStartupInput;
				Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
				{
					Gdiplus::Graphics graphics(bhdc);

					if (cmode==NBMODE_NORMAL  ||  cmode==NBMODE_RENDERING)
					{
						Gdiplus::Pen s1pen(Gdiplus::Color(96, 0,0,0));
						Gdiplus::Pen s2pen(Gdiplus::Color(32, 0,0,0));

						// image shadow
						graphics.DrawRectangle(&s1pen, fleftup.x-1, fleftup.y-1, frightdown.x-fleftup.x+1, frightdown.y-fleftup.y+1);
						graphics.DrawRectangle(&s2pen, fleftup.x-2, fleftup.y-2, frightdown.x-fleftup.x+3, frightdown.y-fleftup.y+3);

						// start/stop shadow
						graphics.DrawRectangle(&s1pen, bleftup.x-1, bleftup.y-1, brightdown.x-bleftup.x+1, brightdown.y-bleftup.y+1);
						graphics.DrawRectangle(&s2pen, bleftup.x-2, bleftup.y-2, brightdown.x-bleftup.x+3, brightdown.y-bleftup.y+3);
					}

					if (bResult>0.0f)
					{
						Gdiplus::Pen s1pen(Gdiplus::Color(96, 0,0,0));
						Gdiplus::Pen s2pen(Gdiplus::Color(32, 0,0,0));

						// start/stop shadow
						graphics.DrawRectangle(&s1pen, uleftup.x-1, uleftup.y-1, urightdown.x-uleftup.x+1, urightdown.y-uleftup.y+1);
						graphics.DrawRectangle(&s2pen, uleftup.x-2, uleftup.y-2, urightdown.x-uleftup.x+3, urightdown.y-uleftup.y+3);
					}

					if (cmode==NBMODE_LICENSE)
					{
						Gdiplus::Pen s1pen(Gdiplus::Color(128, 0,0,0));
						Gdiplus::Pen s2pen(Gdiplus::Color(48, 0,0,0));

						// accept license shadow
						graphics.DrawRectangle(&s1pen, l1leftup.x-1, l1leftup.y-1, l1rightdown.x-l1leftup.x+1, l1rightdown.y-l1leftup.y+1);
						graphics.DrawRectangle(&s2pen, l1leftup.x-2, l1leftup.y-2, l1rightdown.x-l1leftup.x+3, l1rightdown.y-l1leftup.y+3);
					}

					if (cmode==NBMODE_LOADING)
					{
						Gdiplus::Pen s1pen(Gdiplus::Color(64, 0,0,0));
						Gdiplus::Pen s2pen(Gdiplus::Color(20, 0,0,0));

						// progress bar shadow
						graphics.DrawRectangle(&s1pen, l2leftup.x-1, l2leftup.y-1, l2rightdown.x-l2leftup.x+1, l2rightdown.y-l2leftup.y+1);
						graphics.DrawRectangle(&s2pen, l2leftup.x-2, l2leftup.y-2, l2rightdown.x-l2leftup.x+3, l2rightdown.y-l2leftup.y+3);
					}

				}	// gdi+ variables destructors here
				Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
			}

			BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

			SelectObject(bhdc, oldBMP);
			DeleteObject(backBMP);
			DeleteDC(bhdc); 

			EndPaint(hWnd, &ps);
		}
		break;
	case WM_MOUSEMOVE:
		{
			SetCursor(LoadCursor(NULL, IDC_ARROW));
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			RECT evermotionRect = {1063,11,1259,53};
			RECT noxbenchRect = {44, 18, 269, 96};
			if (PtInRect(&evermotionRect, pt))
			{
				SetCursor(LoadCursor(NULL, IDC_HAND));
			}
			if (PtInRect(&noxbenchRect, pt))
			{
				SetCursor(LoadCursor(NULL, IDC_HAND));
			}

		}
		break;
	case WM_LBUTTONDOWN:
		{
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			RECT evermotionRect = {1063,11,1259,53};
			RECT noxbenchRect = {44, 18, 269, 96};
			if (PtInRect(&evermotionRect, pt))
			{
				ShellExecute(hWnd, "open", "http://www.evermotion.org/", NULL, NULL, SW_SHOWNORMAL);
			}
			if (PtInRect(&noxbenchRect, pt))
			{
				ShellExecute(hWnd, "open", "http://www.evermotion.org/nox", NULL, NULL, SW_SHOWNORMAL);
			}
			
			// EASTER EGG
			RECT irect;
			GetWindowRect(hImage, &irect);
			POINT ptee = {irect.right, irect.bottom};
			ScreenToClient(hMain, &ptee);
			ptee.x+=10;
			if (pt.x==ptee.x  &&  pt.y==ptee.y)
			{
				// easter egg
				char * eetext = copyString("Tp!zpv!mjlf!fbtufs!fhht///!PL");
				char * eeaddress = copyString("iuuq;00qpoztqjo/dpn0");
				int leetext = (int)strlen(eetext);
				int leeaddress = (int)strlen(eeaddress);
				for (int i=0; i<leetext; i++)
					eetext[i]--;
				for (int i=0; i<leeaddress; i++)
					eeaddress[i]--;
				MessageBoxNox(0, eetext, "Nice", hFontMessageText, hFontMessageButton);
				ShellExecute(hWnd, "open", eeaddress, NULL, NULL, SW_SHOWNORMAL);
				free(eetext);
				free(eeaddress);
			}
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case 1302:	// START/ABORT
					{
						if (Raytracer::getInstance()->curScenePtr->nowRendering)
						{
							stopRender(true);
						}
						else
						{
							startRender();
						}
					}
					break;

				case 1303:	// SEND TO SERVER
					{
						if (dll_ok  &&  scene_hash_ok)
							sendResultToServer();
					}
					break;
				case 1353:	// ABOUT
					{
						CHECK(wmEvent==BN_CLICKED);
						if (cmode==NBMODE_ABOUT)
						{
							if (Raytracer::getInstance()->curScenePtr->nowRendering)
								setMode(NBMODE_RENDERING);
							else
								setMode(NBMODE_NORMAL);
						}
						else
							setMode(NBMODE_ABOUT);
					}
					break;
				case 1305:	// ACCEPT LICENSE
					{
						setMode(NBMODE_NORMAL);
						addLicenseAccepted();
					}
					break;
				case 1306:	// LICENSE SCROLLBAR
					{
						EMScrollBar * emsb = GetEMScrollBarInstance(hLicScroll);
						if (!emsb)
							break;
						if (wmEvent != SB_POS_CHANGED)
							break;

						license_pos = emsb->workarea_pos;
						InvalidateRect(hLicText, NULL, false);
						break;
						int first = (int)SendMessage(hLicText, EM_GETFIRSTVISIBLELINE, 0, 0);
						SendMessage(hLicText, EM_LINESCROLL, 0, emsb->workarea_pos-first);
						invalidateWithParentRedraw(hLicText);
					}
					break;
				case 1308:	// LICENSE TEXT
					{
						if (wmEvent==50012)
						{
							EMScrollBar * emsb = GetEMScrollBarInstance(hLicScroll);
							emsb->workarea_pos = license_pos;
							emsb->updateSize();
							emsb->updateRegions();
							InvalidateRect(emsb->hwnd, NULL, false);
						}
					}
					break;
			}
		}
		break;
	case WM_TIMER:
		{
			static int fcounter = 1;
			if (wParam == TIMER_STOP_ID)
			{
				KillTimer(hWnd, TIMER_STOP_ID);
				if (stoppingPassOn)
				{
					stoppingPassOn = false;
					stopRender(false);
					refreshThrRunningB = false;
					refreshRenderedImage();
				}
			}
		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
			if ((HWND)lParam==hResultValue  &&  bResult>0.001f)
				if (scene_hash_ok  &&  dll_ok)
					SetTextColor(hdc1, RGB(213,162,8));
				else
					SetTextColor(hdc1, RGB(255,62,8));
			if ((HWND)lParam==hResultValue  &&  bResult<=0.001f)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCpuText)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCpuValue)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hCoresThreadsText)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCoresThreadsValue)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hAbout)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hVersion)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCrEngine)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCrBuild)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCrCode)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCrGui)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCrScene)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCrScene2)
				SetTextColor(hdc1, RGB(127,123,121));
			if ((HWND)lParam==hCrCredits)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hCrCodeName)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hCrGuiName)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hCrSceneName)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hCrScene2Name)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hCrContactUs)
				SetTextColor(hdc1, RGB(207,207,207));
			if ((HWND)lParam==hCrEmail)
				SetTextColor(hdc1, RGB(207,207,207));

			if ((HWND)lParam==hLicText)
			{
				SetTextColor(hdc1, RGB(207,207,207));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)(HBRUSH)GetStockObject(NULL_BRUSH);
			}

			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)(HBRUSH)GetStockObject(NULL_BRUSH);
		}
		break;
	case WM_CTLCOLORDLG:
		{
			return (INT_PTR)BenchmarkWindow::backgroundBrush;
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	return 0;
}

//----------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK BenchmarkWindow::LicParentWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_CREATE:
			{
			}
			break;
		case WM_DESTROY:
			{
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				POINT p = {0,0};
				MapWindowPoints(hWnd, GetParent(hWnd), &p, 1);

				ur = GetUpdateRect(hWnd, &urect, TRUE); 
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBmpBgLic);
				BITMAP tbitmap;
				GetObject(hBmpBg, sizeof(tbitmap), &tbitmap);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+p.x, rect.top+p.y, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);


				HFONT oldFont = (HFONT)SelectObject(bhdc, hFontLicense);
				SetBkColor(bhdc, 0);
				SetTextColor(bhdc, RGB(207, 207, 207));
				SetBkMode(bhdc, TRANSPARENT);
			
				RECT trect;
				GetClientRect(hWnd, &trect);
				trect.left = 0;
				trect.top = -license_pos;

				DrawText(bhdc, bench_license_text, (int)strlen(bench_license_text), &trect, DT_NOCLIP);

				SelectObject(bhdc, oldFont);


				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_SIZE:
			{
			}
			break;
		case WM_MOUSEMOVE:
			{
				SetFocus(hWnd);
			}
			break;
		case WM_MOUSEWHEEL:
			{
				RECT crect;
				GetClientRect(hWnd, &crect);
				int zDelta = (int)wParam;
				if (zDelta>0)
				{
					license_pos = max(license_pos-20, 0);
				}
				else
				{
					license_pos = min(license_pos+20, license_height-crect.bottom);
				}
				InvalidateRect(hWnd, NULL, false);
				SendMessage(GetParent(hWnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hWnd, GWL_ID), 50012), (LPARAM)hWnd);
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)BenchmarkWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				if ((HWND)lParam==hLicText)
				{
					SetTextColor(hdc1, RGB(207,207,207));
					SetBkMode(hdc1, TRANSPARENT);
					return (INT_PTR)(HBRUSH)GetStockObject(NULL_BRUSH);
				}

				SetTextColor(hdc1, RGB(207,207,207));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)(HBRUSH)GetStockObject(NULL_BRUSH);
			}
			break;
	}
    return DefWindowProc(hWnd, msg, wParam, lParam);
}

