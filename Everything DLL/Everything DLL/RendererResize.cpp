#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"



//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizePictureView()
{
	RECT rect,crect;
	GetClientRect(hMain, &rect);
	EMTabs * emt = GetEMTabsInstance(hTabs);
	switch (emt->selected)
	{
		case POSRENDER:
		{
			GetClientRect(hOutputConf, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSREGIONS:
		{
			GetClientRect(hRegions, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSCAMERA:
		{
			GetClientRect(hCamera, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSOBJECTS:
		{
			GetClientRect(hObjects, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSMATERIALS:
		{
			GetClientRect(hMaterials, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSENVIRONMENT:
		{
			GetClientRect(hEnvironment, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSBLEND:
		{
			GetClientRect(hBlend, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSPOST:
		{
			GetClientRect(hPost, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		case POSFINAL:
		{
			GetClientRect(hFinal, &crect);
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2+crect.bottom-crect.top;
		}
		break;
		default:
		{
			iposx = 2;
			iposy = tabsHeight+mainBarHeight+2;
		}
		break;
	}
	SetWindowPos(hImage, HWND_TOP, iposx, iposy,  rect.right-rect.left-iposx-2,  rect.bottom-rect.top-iposy-statusHeight-2, 0);//SWP_NOREPOSITION);
	InvalidateRect(hImage, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeMainBar()
{
	RECT rect;
	GetClientRect(hMain, &rect);
	SetWindowPos(hMainBar, HWND_TOP, 0, 0,  rect.right-rect.left,  mainBarHeight, 0);
	InvalidateRect(hMainBar, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeTabs()
{
	RECT rect;
	GetClientRect(hMain, &rect);
	SetWindowPos(hTabs, HWND_TOP, 2, mainBarHeight,  rect.right-rect.left, tabsHeight, 0);
	InvalidateRect(hTabs, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeOutputConf()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hOutputConf, &crect);
	SetWindowPos(hOutputConf, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hOutputConf, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeMaterials()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hMaterials, &crect);
	SetWindowPos(hMaterials, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hMaterials, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeLogs()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hLogs, &crect);
	SetWindowPos(hLogs, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hLogs, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeFinal()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hFinal, &crect);
	SetWindowPos(hFinal, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hFinal, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeRegions()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hRegions, &crect);
	SetWindowPos(hRegions, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hRegions, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeCamera()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hCamera, &crect);
	SetWindowPos(hCamera, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hCamera, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeObjects()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hObjects, &crect);
	SetWindowPos(hObjects, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hObjects, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeBlend()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hBlend, &crect);
	SetWindowPos(hBlend, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hBlend, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizePost()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hPost, &crect);
	SetWindowPos(hPost, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hPost, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeEnvironment()
{
	RECT rect, crect;
	GetClientRect(hMain, &rect);
	GetClientRect(hEnvironment, &crect);
	SetWindowPos(hEnvironment, HWND_TOP, 0, mainBarHeight+tabsHeight,  rect.right-rect.left, crect.bottom-crect.top, 0);
	InvalidateRect(hEnvironment, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::resizeStatusBar()
{
	RECT rect;
	GetClientRect(hMain, &rect);
	int p2 = 220;
	int p3 = 180;
	int p4 = 200;
	int p = (int) ( (rect.right-rect.left - p2 - p3 - p4) * statusPart );
	int r = rect.right-rect.left -p3 -p2 - p - p4 - 11;
	SetWindowPos(hStatus1,   HWND_TOP, 2, rect.bottom-rect.top-statusHeight,  p, statusHeight, 0);
	SetWindowPos(hStatus2,   HWND_TOP, p+3, rect.bottom-rect.top-statusHeight,  p2, statusHeight, 0);
	SetWindowPos(hStatus4,   HWND_TOP, p+p2+5, rect.bottom-rect.top-statusHeight,  p4, statusHeight, 0);
	SetWindowPos(hStatus3,   HWND_TOP, p+p2+p4+7, rect.bottom-rect.top-statusHeight,  p3, statusHeight, 0);
	SetWindowPos(hProgress1, HWND_TOP, p+p2+p3+p4+9, rect.bottom-rect.top-statusHeight,  r, statusHeight, 0);
	InvalidateRect(hStatus1, NULL, false);
	InvalidateRect(hProgress1, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------
