#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "log.h"

HWND hAbbGraph = 0;
float abbShiftXY = 0.0f;
float abbShiftZ = 0.0f;
float abbAchromatic = 0.0f;
float distPattern = 1.0f;

float getFreqToXYZ1964(int i);
float getFreqToXYZ1978(int i);

extern HMODULE hDllModule;
#define MSG_REPAINT 29324

bool createPatternRGB(ImageByteBuffer * img, float dist);
void convertFakeSpectrumToRGB(float wavelength, float &r, float &g, float &b);	// wavelength clamped: 0 - 1

void MyLine(HDC hdc, int X1, int Y1, int X2, int Y2, float w, COLORREF c, COLORREF b);
void MyEllipse(HDC hdc, int X1, int Y1, int X2, int Y2, COLORREF c, COLORREF b);
void MyEllipse2(HDC hdc, int X1, int Y1, int X2, int Y2, float a, COLORREF c, COLORREF b);

//---------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK ChrAbbGrControlProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
		case WM_CREATE:
			{
			}
			break;
		case WM_SIZE:
			{
			}
			break;
		case WM_NCDESTROY:
			{
			}
			break;
		case WM_LBUTTONDOWN:
		case WM_MOUSEMOVE:
			{
				if (msg==WM_MOUSEMOVE)
				{
					if (!(wParam&MK_LBUTTON))
						break;
				}
				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT pt;
				pt.x = (short)LOWORD(lParam);
				pt.y = (short)HIWORD(lParam);
				float ptx = min(rect.right*0.9f, pt.x);
				float d1 = rect.right*0.7f;
				float d0 = rect.right*0.2f;
				float d = (ptx - d0)/(d1-d0);
				d = max(d, 0.1f);
				distPattern = d;
				InvalidateRect(hwnd, NULL, false);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, MSG_REPAINT), (LPARAM)hwnd);

			}
			break;
		case WM_PAINT:
		{
			RECT rect;
			HDC hdc, thdc;
			PAINTSTRUCT ps;

			hdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);
			thdc = CreateCompatibleDC(hdc); 
			HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
			HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

			int ellX = (int)(rect.right*0.2f);
			int ellY = (int)(rect.bottom*0.5f);
			int ellH = (int)(rect.bottom*0.4f);
			int ellW = (int)(ellX*0.1f);

			COLORREF ellCol = RGB(240,240,240);
			COLORREF centerCol = RGB(192,192,192);



			HBRUSH ellBrush = CreateSolidBrush(ellCol);
			HPEN ellPen = CreatePen(PS_SOLID, 1, ellCol);
			HBRUSH hOldBrush = (HBRUSH)SelectObject(thdc, ellBrush);
			HPEN hOldPen = (HPEN)SelectObject(thdc, ellPen);
			MyEllipse(thdc, ellX-ellW, ellY-ellH, ellX+ellW, ellY+ellH, ellCol, RGB(0,0,0));

			// main line
			SetBkMode(thdc, TRANSPARENT);
			HPEN hCenterPen = CreatePen(PS_DASH, 1, centerCol);
			SelectObject(thdc, hCenterPen);
			MoveToEx(thdc, rect.right/10, rect.bottom/2, NULL);
			LineTo(thdc, rect.right*9/10, rect.bottom/2);


			int linewidth = 1;
			HPEN pRed   = CreatePen(PS_SOLID, linewidth, RGB(255,0,0));
			HPEN pGreen = CreatePen(PS_SOLID, linewidth, RGB(0,255,0));
			HPEN pBlue  = CreatePen(PS_SOLID, linewidth, RGB(0,0,255));
			HPEN lPens[] = { pRed, pGreen, pBlue };
			float lShiftsXY[] = {-1, 0, 1};
			float lShiftsZ[]  = {-1, 0, -abbAchromatic*2+1};
			COLORREF tcols[3];
			tcols[0] = RGB(255,0,0);
			tcols[1] = RGB(0,255,0);
			tcols[2] = RGB(0,0,255);

			SetROP2(thdc, R2_MERGEPEN);

			float d1 = rect.right*0.7f;
			float d0 = (float)ellX;
			int ppos = (int)((d1-d0)*distPattern+d0);

			SelectObject(thdc, ellPen);
			MoveToEx(thdc, ppos, rect.bottom*3/20, NULL);
			LineTo(  thdc, ppos, rect.bottom*17/20);

			for (int i=0; i<3; i++)
			{
				int lStartX = ellX;
				int lStartY = (int)( ellY - rect.bottom*0.3f - rect.bottom*0.08*lShiftsXY[i]*abbShiftXY);
				int lHalfX = (int)(rect.right*0.7f + rect.right*0.08f*lShiftsZ[i]*abbShiftZ);
				int lHalfY = ellY;
				int lStopX = (int)(rect.right*0.9f);
				float lRatio = (float)(lHalfY-lStartY)/(float)(lHalfX-lStartX);
				int lStopY = (int)(lStartY+(lStopX-lStartX)*lRatio);

				SelectObject(thdc, lPens[i]);
				
				MyLine(thdc, lStartX, lStartY,				lStopX, lStopY,				1.25f, tcols[i], RGB(0,0,0));
				MyLine(thdc, lStartX, ellY-(lStartY-ellY),	lStopX, ellY-(lStopY-ellY),	1.25f, tcols[i], RGB(0,0,0));

			}

			SetROP2(thdc, R2_COPYPEN);

			// clean stuff
			SelectObject(thdc, hOldBrush);
			SelectObject(thdc, hOldPen);
			DeleteObject(ellBrush);
			DeleteObject(ellPen);
			DeleteObject(pRed);
			DeleteObject(pGreen);
			DeleteObject(pBlue);
			DeleteObject(hCenterPen);

			// copy from back buffer and free resources
			BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
			SelectObject(thdc, oldBMP);
			DeleteObject(hbmp);
			DeleteDC(thdc);
			EndPaint(hwnd, &ps);

		}
			break;
		default:
			break;
	}

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//---------------------------------------------------------------------------------------------------------------

void initChrAbCntrlWndClass()
{
	static bool registered = false;
	if (registered)
		return;
	registered = true;

    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "chrabcntrlwnd";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = ChrAbbGrControlProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = 0;
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//---------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK ChromaticAberrationGraphDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH hBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				hBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

				EMButton   * emOK		= GetEMButtonInstance(GetDlgItem(hWnd, IDC_CHR_OK));
				EMButton   * emCancel	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_CHR_CANCEL));
				EMButton   * emReset1	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_CHR_RESET_VALUE1));
				EMButton   * emReset2	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_CHR_RESET_VALUE2));
				EMButton   * emReset3	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_CHR_RESET_VALUE3));
				EMText     * emTxt1		= GetEMTextInstance(GetDlgItem(hWnd, IDC_CHR_TEXT1));
				EMText     * emTxt2		= GetEMTextInstance(GetDlgItem(hWnd, IDC_CHR_TEXT2));
				EMText     * emTxt3		= GetEMTextInstance(GetDlgItem(hWnd, IDC_CHR_TEXT3));
				EMEditTrackBar * emVal1 = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE1));
				EMEditTrackBar * emVal2 = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE2));
				EMEditTrackBar * emVal3 = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE3));


				HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
				SetWindowPos(hPattern, HWND_TOP, 532,10, 256,256, SWP_NOZORDER);
				EMPView	   * emPattern = GetEMPViewInstance(hPattern);
				if (!emPattern->byteBuff)
					emPattern->byteBuff = new ImageByteBuffer();
				emPattern->byteBuff->allocBuffer(256, 256);
				emPattern->byteBuff->clearBuffer();
				InvalidateRect(hPattern, NULL, false);
				
				emVal1->setEditBoxWidth(32);
				emVal1->setValuesToFloat(abbShiftXY, -1.0f, 1.0f, 0.1f);
				emVal2->setEditBoxWidth(32);
				emVal2->setValuesToFloat(abbShiftZ , -1.0f, 1.0f, 0.1f);
				emVal3->setEditBoxWidth(32);
				emVal3->setValuesToFloat(abbAchromatic, 0.0f, 1.0f, 0.1f);
				GlobalWindowSettings::colorSchemes.apply(emOK);
				GlobalWindowSettings::colorSchemes.apply(emCancel);
				GlobalWindowSettings::colorSchemes.apply(emTxt1, true);
				GlobalWindowSettings::colorSchemes.apply(emTxt2, true);
				GlobalWindowSettings::colorSchemes.apply(emTxt3, true);
				GlobalWindowSettings::colorSchemes.apply(emVal1);
				GlobalWindowSettings::colorSchemes.apply(emVal2);
				GlobalWindowSettings::colorSchemes.apply(emVal3);
				GlobalWindowSettings::colorSchemes.apply(emReset1);
				GlobalWindowSettings::colorSchemes.apply(emReset2);
				GlobalWindowSettings::colorSchemes.apply(emReset3);

				initChrAbCntrlWndClass();
				hAbbGraph = CreateWindow("chrabcntrlwnd", "", WS_VISIBLE | WS_CHILD,
					10, 10, 512, 256, hWnd, (HMENU)12312, hDllModule, 0);
				ShowWindow(hAbbGraph, SW_SHOW);
			}
			break;
		case WM_DESTROY:
			{
				DeleteObject(hBrush);
				hBrush = 0;
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_CHR_OK:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)1);
							}
						}
						break;
					case IDC_CHR_CANCEL:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)NULL);
							}
						}
						break;
					case IDC_CHR_VALUE1:
						{
							if (wmEvent == WM_HSCROLL)
							{
								EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE1));
								if (emtr)
									abbShiftXY = emtr->floatValue;
								InvalidateRect(hAbbGraph, NULL, false);

								HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
								EMPView	   * emPattern = GetEMPViewInstance(hPattern);
								createPatternRGB(emPattern->byteBuff, distPattern);
								InvalidateRect(hPattern, NULL, false);

							}
						}
						break;
					case IDC_CHR_VALUE2:
						{
							if (wmEvent == WM_HSCROLL)
							{
								EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE2));
								if (emtr)
									abbShiftZ = emtr->floatValue;
								InvalidateRect(hAbbGraph, NULL, false);

								HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
								EMPView	   * emPattern = GetEMPViewInstance(hPattern);
								createPatternRGB(emPattern->byteBuff, distPattern);
								InvalidateRect(hPattern, NULL, false);

							}
						}
						break;
					case IDC_CHR_VALUE3:
						{
							if (wmEvent == WM_HSCROLL)
							{
								EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE3));
								if (emtr)
									abbAchromatic = emtr->floatValue;
								InvalidateRect(hAbbGraph, NULL, false);

								HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
								EMPView	   * emPattern = GetEMPViewInstance(hPattern);
								createPatternRGB(emPattern->byteBuff, distPattern);
								InvalidateRect(hPattern, NULL, false);

							}
						}
						break;
					case IDC_CHR_RESET_VALUE1:
						{
							if (wmEvent == BN_CLICKED)
							{
								EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE1));
								abbShiftXY = 0.0f;
								if (emtr)
									emtr->setValuesToFloat(abbShiftXY, -1.0f, 1.0f, 0.1f);
								InvalidateRect(hAbbGraph, NULL, false);

								HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
								EMPView	   * emPattern = GetEMPViewInstance(hPattern);
								createPatternRGB(emPattern->byteBuff, distPattern);
								InvalidateRect(hPattern, NULL, false);

							}
						}
						break;
					case IDC_CHR_RESET_VALUE2:
						{
							if (wmEvent == BN_CLICKED)
							{
								EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE2));
								abbShiftZ = 0.0f;
								if (emtr)
									emtr->setValuesToFloat(abbShiftZ, -1.0f, 1.0f, 0.1f);
								InvalidateRect(hAbbGraph, NULL, false);

								HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
								EMPView	   * emPattern = GetEMPViewInstance(hPattern);
								createPatternRGB(emPattern->byteBuff, distPattern);
								InvalidateRect(hPattern, NULL, false);

							}
						}
						break;
					case IDC_CHR_RESET_VALUE3:
						{
							if (wmEvent == BN_CLICKED)
							{
								EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_CHR_VALUE3));
								abbAchromatic = 0.0f;
								if (emtr)
									emtr->setValuesToFloat(abbAchromatic, 0.0f, 1.0f, 0.1f);
								InvalidateRect(hAbbGraph, NULL, false);

								HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
								EMPView	   * emPattern = GetEMPViewInstance(hPattern);
								createPatternRGB(emPattern->byteBuff, distPattern);
								InvalidateRect(hPattern, NULL, false);

							}
						}
						break;
					case 12312:		// graph
						{
							if (wmEvent == MSG_REPAINT)
							{
								HWND hPattern = GetDlgItem(hWnd, IDC_CHR_PATTERN);
								EMPView	   * emPattern = GetEMPViewInstance(hPattern);
								createPatternRGB(emPattern->byteBuff, distPattern);
								InvalidateRect(hPattern, NULL, false);
							}
						}
						break;

						
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)hBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)hBrush;
			}
			break;
	}
	return false;
}

//---------------------------------------------------------------------------------------------------------------

bool createPatternRGB(ImageByteBuffer * img, float dist)
{
	CHECK(dist>0);
	CHECK(img);
	CHECK(img->width>0);
	CHECK(img->height>0);

	int w = img->width;
	int h = img->height;
	int halfX = w/2;
	int halfY = h/2;
	int hhalf = min(halfX, halfY);

	float startRX = 0.0f;
	float startRY = (0.3f-0.08f*abbShiftXY)/0.4f;
	float startGX = 0.0f;
	float startGY = 0.3f/0.4f;
	float startBX = 0.0f;
	float startBY = (0.3f+0.08f*abbShiftXY)/0.4f;
	float halfRX = (0.7f-0.08f*abbShiftZ-0.2f)/0.5f;
	float halfRY = 0.0f;
	float halfGX = (0.7f-0.2f)/0.5f;
	float halfGY = 0.0f;
	float halfBX = (0.7f+0.08f*abbShiftZ*(-abbAchromatic*2+1)-0.2f)/0.5f;
	float halfBY = 0.0f;


	float ratioR = (halfRY-startRY)/(halfRX-startRX);
	float ratioG = (halfGY-startGY)/(halfGX-startGX);
	float ratioB = (halfBY-startBY)/(halfBX-startBX);

	float stopRY = ratioR*distPattern+startRY;
	float stopGY = ratioG*distPattern+startGY;
	float stopBY = ratioB*distPattern+startBY;

	int dR2 = (int)(stopRY*stopRY*hhalf*hhalf);
	int dG2 = (int)(stopGY*stopGY*hhalf*hhalf);
	int dB2 = (int)(stopBY*stopBY*hhalf*hhalf);
	dR2 = max(dR2,4);
	dG2 = max(dG2,4);
	dB2 = max(dB2,4);

	float rR = fabs(stopRY*hhalf);
	float rG = fabs(stopGY*hhalf);
	float rB = fabs(stopBY*hhalf);
	float rmin = max(4.0f, min(rR, min(rG, rB)));
	float rmax = max(rR, max(rG, rB));
	float mfR = max(2, rR);
	float mfG = max(2, rG);
	float mfB = max(2, rB);
	float mfMax = max(mfR, max(mfB, mfG));

	if (mfR==mfMax)
		mfR = mfR*mfR*PI;
	else
		mfR = PI*(   (mfMax*mfMax*mfMax-mfR*mfR*mfR)/(3*mfMax-3*mfR) );//- 2*mfR*mfR  );
	if (mfG==mfMax)
		mfG = mfG*mfG*PI;
	else
		mfG = PI*(   (mfMax*mfMax*mfMax-mfG*mfG*mfG)/(3*mfMax-3*mfG) );//- 2*mfG*mfG  );
	if (mfB==mfMax)
		mfB = mfB*mfB*PI;
	else
		mfB = PI*(   (mfMax*mfMax*mfMax-mfB*mfB*mfB)/(3*mfMax-3*mfB) );//- 2*mfB*mfB  );
	mfMax = max(mfR, max(mfG, mfB));
	float mfMin = min(mfR, min(mfB, mfG));

	mfR = mfMin/mfR;
	mfG = mfMin/mfG;
	mfB = mfMin/mfB;

	unsigned char mR = (unsigned char)(int)min(255.0f, mfR*255.0f);
	unsigned char mG = (unsigned char)(int)min(255.0f, mfG*255.0f);
	unsigned char mB = (unsigned char)(int)min(255.0f, mfB*255.0f);

	float perRR = 1.0f/(max(0.01f, rmax-rR));
	float perGR = 1.0f/(max(0.01f, rmax-rG));
	float perBR = 1.0f/(max(0.01f, rmax-rB));

	float counter_red = 0;
	float counter_green = 0;
	float counter_blue = 0;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			unsigned char r = 0;
			unsigned char g = 0;
			unsigned char b = 0;
			int dd = (halfX-x)*(halfX-x) + (halfY-y)*(halfY-y);

			float d = sqrt((float)dd);
			float ftr = mR*min(1.0f, max(0.0f, rmax-d)*perRR);
			float ftg = mG*min(1.0f, max(0.0f, rmax-d)*perGR);
			float ftb = mB*min(1.0f, max(0.0f, rmax-d)*perBR);
			r = (unsigned char)(int)(ftr);
			g = (unsigned char)(int)(ftg);
			b = (unsigned char)(int)(ftb);
			img->imgBuf[y][x] = RGB(r,g,b);

			counter_red   += ftr;
			counter_green += ftg;
			counter_blue  += ftb;

		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------------------

void convertFakeSpectrumToRGB(float wavelength, float &r, float &g, float &b)	// wavelength clamped: 0 - 1
{
	float w = min(max(wavelength*0.4f+0.38f, 0.38f), 0.78f);

    if (w < 0.440f)
	{
        r = -(w - 0.440f) / (0.440f - 0.350f);
        g = 0.0f;
        b = 1.0f;
	}
	else
	{
		if (w < 0.490f)
		{
			r = 0.0f;
			g = (w - 0.440f) / (0.490f - 0.440f);
			b = 1.0f;
		}
		else
		{
			if (w < 0.510f)
			{
				r = 0.0f;
				g = 1.0f;
				b = -(w - 0.510f) / (0.510f - 0.490f);
			}
			else
			{
				if (w < 0.580f)
				{
					r = (w - 0.510f) / (0.580f - 0.510f);
					g = 1.0f;
					b = 0.0f;
				}
				else
				{
					if (w < 0.645f)
					{
						r = 1.0f;
						g = -(w - 0.645f) / (0.645f - 0.580f);
						b = 0.0f;
					}
					else
					{
						if (w <= 0.780f)
						{
							r = 1.0f;
							g = 0.0f;
							b = 0.0f;
						}
						else
						{
							r = 0.0f;
							g = 0.0f;
							b = 0.0f;
						}
					}
				}
			}
		}
	}


}

//---------------------------------------------------------------------------------------------------------------------------




