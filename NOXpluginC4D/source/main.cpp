 #include "c4d.h"
 #include "noxexporter.h"

// forward declarations
 C4D_CrashHandler old_handler;

// crash handler init
 void SDKCrashHandler(CHAR* crashinfo)
 {
     // don't forget to call original handler
     if(old_handler) (*old_handler)(crashinfo);
 }

// start the plugin
 Bool PluginStart(void)
 {
     // install crashhandler
     old_handler = C4DOS.CrashHandler;                // save original handler (must be called!)
     C4DOS.CrashHandler = SDKCrashHandler;            // insert new handler

    // register plugin
     GePrint(" ");

     if(!RegisterNOXexporter())
     {
         GePrint("Failed to register NOX exporter.");
         GePrint(" ");
     } else {
         GePrint("NOX exporter successfully loaded.");
         GePrint(" ");
     }

	 return TRUE;
 }

void PluginEnd(void)
 {
     return;
 }

Bool PluginMessage(LONG id, void *data)
 {
     switch(id)
     {
     case C4DPL_INIT_SYS:
     if(!resource.Init())
     {
         GePrint("NOX exporter: failed to load resource file.");
         return FALSE;                                                                     // don't start plugin without its resource
     }
     else
         return TRUE;
     break;

     case C4DMSG_PRIORITY:
         return TRUE;
         break;

    default:
         return FALSE;
     }
 }