#include <windows.h>
#include <stdio.h>
#include "EM2Controls.h"
#include <commctrl.h>

extern HMODULE hDllModule;

EM2ImgButton::EM2ImgButton(HWND hWnd)
{
	colBackground = NGCOL_BG_MEDIUM;

	clicked = false;
	mouseIn = false;
	selected = false;
	two_state = false;
	ts_allow_unclick = false;
	useCursors = true;

	hwnd = hWnd;
	hToolTip = 0;

	pNx = pNy = pMx = pMy = pCx = pCy = pDx = pDy = 0;
	bmImage = NULL;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;
}

EM2ImgButton::~EM2ImgButton()
{
	if (hToolTip)
		DestroyWindow(hToolTip);
	hToolTip = 0;
}

void InitEM2ImgButton()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2ImgButton";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2ImgButtonProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2ImgButton *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}


EM2ImgButton * GetEM2ImgButtonInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2ImgButton * emib = (EM2ImgButton *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2ImgButton * emib = (EM2ImgButton *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emib;
}

void SetEM2ImgButtonInstance(HWND hwnd, EM2ImgButton *emib)
{
	#ifdef _WIN64
	    SetWindowLongPtr(hwnd, 0, (LONG_PTR)emib);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emib);
	#endif
}

LRESULT CALLBACK EM2ImgButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2ImgButton * emib = GetEM2ImgButtonInstance(hwnd);

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2ImgButton * emib1 = new EM2ImgButton(hwnd);
				SetEM2ImgButtonInstance(hwnd, (EM2ImgButton *)emib1);			
			}
		break;
		case WM_NCDESTROY:
			{
				EM2ImgButton * emib1;
				emib1 = GetEM2ImgButtonInstance(hwnd);
				delete emib1;
				SetEM2ImgButtonInstance(hwnd, 0);
			}
		break;
		case WM_PAINT:
			{
				if (!emib) 
					break;

				RECT rect;
				PAINTSTRUCT ps;
				HDC ohdc = BeginPaint(hwnd, &ps);
				GetClientRect(hwnd, &rect);

				HDC hdc = CreateCompatibleDC(ohdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(hdc, backBMP);

				if (emib->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emib->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emib->bgShiftX, emib->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, TRANSPARENT);
				}


				if (emib->bmImage)
				{
					int rx, ry;
					rx = rect.right;
					ry = rect.bottom;
					int px, py;

					bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED)!=0);
					if (disabled)
					{
						px = emib->pDx;
						py = emib->pDy;
					}
					else
					{
						if (emib->clicked || emib->selected)
						{
							px = emib->pCx;
							py = emib->pCy;
						}
						else
						{
							if (emib->mouseIn)
							{
								px = emib->pMx;
								py = emib->pMy;
							}
							else
							{
								px = emib->pNx;
								py = emib->pNy;
							}
						}
					}

					HDC bmpdc = CreateCompatibleDC(hdc); 
					SelectObject(bmpdc, *(emib->bmImage)); 
					BLENDFUNCTION blendFunction;
					blendFunction.BlendOp = AC_SRC_OVER;
					blendFunction.BlendFlags = 0;
					blendFunction.SourceConstantAlpha = 255;
					blendFunction.AlphaFormat = AC_SRC_ALPHA;
					AlphaBlend(hdc, 0, 0,  rx, ry, bmpdc, px, py, rx,ry, blendFunction);
					DeleteDC(bmpdc); 
				}

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY);

				SelectObject(hdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(hdc); 

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_MOUSEMOVE:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				bool nowMouseOver = (PtInRect(&rect, pt)==TRUE);

				if (!GetCapture())
					SetCapture(hwnd);

				if (nowMouseOver!=emib->mouseIn)
				{
					if (emib->useCursors)
						SetCursor(LoadCursor (NULL, nowMouseOver ? IDC_HAND : IDC_ARROW));
					InvalidateRect(hwnd, NULL, false);
				}
				emib->mouseIn = nowMouseOver;

				if (GetCapture()==hwnd  &&  !nowMouseOver  &&  !emib->clicked)
				{
					emib->mouseIn = false;
					InvalidateRect(hwnd, NULL, false);
					ReleaseCapture();
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emib->mouseIn = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				emib->clicked = true;
				emib->mouseIn = true;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONUP:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				bool nowMouseOver = (PtInRect(&rect, pt)==TRUE);

				InvalidateRect(hwnd, NULL, false);

				bool wasClicked = false;
				if (GetCapture() != hwnd)
					emib->clicked = false;

				if (emib->clicked)
				{
					emib->clicked = false;
					emib->mouseIn = true;
					if (nowMouseOver)
						wasClicked = true;
				}
				ReleaseCapture();
				emib->clicked = false;
				emib->mouseIn = false;
				if (wasClicked)
				{
					if (emib->two_state)
					{
						if (emib->selected)
						{
							if (emib->ts_allow_unclick)
								emib->selected = false;
						}
						else
							emib->selected = true;
					}
					LONG wID;
					wID = GetWindowLong(hwnd, GWL_ID);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), 
							(LPARAM)hwnd);
				}
			}
			break;
		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EM2ImgButton::setToolTip(char * text)
{
	if (!hToolTip)
	{
		hToolTip = CreateWindowEx (WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,     
				    CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwnd, NULL, hDllModule, NULL);
	}

	SetWindowPos (hToolTip, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	TOOLINFO ti;
	ti.cbSize = sizeof (TOOLINFO);
	ti.uFlags = TTF_SUBCLASS | TTF_IDISHWND;
	ti.hwnd = hToolTip;
	ti.hinst = NULL;
	ti.uId = (UINT_PTR) hwnd;
	ti.lpszText = (LPSTR) text;

	RECT rect;
	GetClientRect (hwnd, &rect);

	ti.rect.left = rect.left;    
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
	ti.rect.bottom = rect.bottom;

	SendMessage (hToolTip, TTM_ADDTOOL, 0, (LPARAM)&ti);
}
