#define _CRT_RAND_S
#include "QuasiRandom.h"
#include <math.h>
#include "log.h"
#include "raytracer.h"

#define PI 3.1415926535897932384626433832795f

#define GRID(x)     ((int)((x)*GRID_SIZE))%GRID_SIZE
#define GRIDNEXT(x) (((int)((x)*GRID_SIZE)+1)%GRID_SIZE)
#define GRIDPREV(x) (((int)((x+1)*GRID_SIZE)-1)%GRID_SIZE)

bool evalChromaticBokehSizes(float shiftLens, float shiftDepth, float achromatic, float depth, float &red, float &green, float &blue);

//----------------------------------------------------------------------------------------------------------------------------------

DiaphragmSampler::DiaphragmSampler()
{
	s = NULL;
	numSamples = 0;
	cur = 0;
	chrS = NULL;

	intBokehRingSize = 20;
	intBokehRingBalance = 0;
	intBokehFlatten = 0;
	intBokehVignette = 0;
	bokehRingSize = 0.2f;
	bokehRingBalance = 1.0f;
	bokehFlatten = 0.0f;
	bokehVignette = 0.0f;
	chromaticShiftLens = 0.0f;
	chromaticShiftDepth = 0.0f;
	chromaticAchromatic = 0.0f;
}

DiaphragmSampler::~DiaphragmSampler()
{
}

//----------------------------------------------------------------------------------------------------------------------------------

void getABC(float mx, float my, float nx, float ny, float &A, float &B, float &C)
{
	float o1 = mx-nx;
	float o2 = ny-my;
	if (fabs(o1) > fabs(o2))
	{
		B = 1;
		A = o2/o1;
		C = -(A*mx+B*my);
	}
	else
	{
		A = 1;
		B = o1/o2;
		C = -(A*mx+B*my);
	}
}

bool DiaphragmSampler::initializeDiaphragm(int NumBlades, float Aperture, float Angle, bool RoundBlades, float Radius)
{
	float R = 1.0f;
	float r = R/Aperture;

	numBlades    = min(15, max(5, NumBlades));
	aperture     = min(36.0f, max(1.0f, Aperture));
	angle        = min(360.0f, max(0.0f, Angle));
	radius	     = min(10.0f, max(1.0f, Radius));
	roundBlades  = RoundBlades;

	for (int i=0; i<numBlades; i++)
	{
		float k = PI*2*i/numBlades+ PI*angle/180.0f;
		vert[i*2+0] = cos(k)*r ;//+ sx; 
		vert[i*2+1] = sin(k)*r ;//+ sy; 
	}
	vert[numBlades*2  ] = vert[0];
	vert[numBlades*2+1] = vert[1];
	for (int i=0; i<numBlades; i++)
	{
		vec[i*2+0] = vert[i*2+2+0] - vert[i*2+0];
		vec[i*2+1] = vert[i*2+2+1] - vert[i*2+1];
	}

	{
		float c1 = cos(PI/numBlades) * r;
		float c2 = sin(PI/numBlades) * r;
		float beta = asin(c2/R/radius*aperture);
		float c3 = cos(beta)*R*radius/aperture;
		float c4 = c3-c1;

		for (int i=0; i<numBlades; i++)
		{
			float k = PI*2*i/numBlades+ PI*angle/180.0f;
			float h = PI*2*(i+0.5f)/numBlades + PI*angle/180.0f;
			angles[i] = k;
			cx[i] = -cos(h)*c4;
			cy[i] = -sin(h)*c4;
		}
		vert[numBlades*2  ] = vert[0];
		vert[numBlades*2+1] = vert[1];
		angles[numBlades] = angles[0]+2*PI;
	}

	for (int i=0; i<numBlades; i++)
	{
		float a,b,c;
		getABC(vert[i*2+0], vert[i*2+1], vert[i*2+2], vert[i*2+3], a,b,c);
		float mm = 1.0f/sqrt(a*a+b*b);
		ABCM[i*4+0] = a;
		ABCM[i*4+1] = b;
		ABCM[i*4+2] = c;
		ABCM[i*4+3] = mm;
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------


float getDistAbs(float mx, float my, float nx, float ny,  float px, float py)
{
	float A,B,C;
	getABC(mx,my,nx,ny, A,B,C);
	float mm = sqrt(A*A+B*B);
	return fabs(A*px+B*py+C)/mm;
}

float DiaphragmSampler::getDistAbsPreevaled(int n, float px, float py)
{
	return fabs(ABCM[n*4+0]*px+ABCM[n*4+1]*py+ABCM[n*4+2])*ABCM[n*4+3];
}

bool DiaphragmSampler::isPointInDiaphragm(float u, float v, float &w, float &d, float other_aperture)
{
	if (other_aperture<=0)
		other_aperture = aperture;
	float R = 1.0f/other_aperture;

	if (numBlades>15)
		return true; 

	int b = 0;
	double tx = u;
	double ty = v;
	double k = atan2(ty/R,tx/R);
	if (k < 0) 
		k += 2*PI;
	if (k < angles[0])
		k += 2*PI;
	
	for (int i=0; i<numBlades; i++)
	{
		if (k>angles[i] && k<angles[i+1])
			b = i;
	}

	float bokehRingSizeAperture = bokehRingSize/other_aperture;

	if (roundBlades)
	{
		float rr = (cx[b]-u)*(cx[b]-u)  +  (cy[b]-v)*(cy[b]-v);
		d = 1.0f;
		if (rr < radius*radius/other_aperture/other_aperture)
		{
			d = w = radius/other_aperture - sqrt(rr);

			if (w<bokehRingSizeAperture)
			{
				if (bokehRingBalance >= 1)
					w = (bokehRingSizeAperture*0.8f-fabs(bokehRingSizeAperture*0.2f-w))*(1.0f/bokehRingSizeAperture*(bokehRingBalance-1))+1;
				else
					w = w/bokehRingSizeAperture;
			}
			else
				w = 1;

			return true;
		}
		else
		{
			d = radius/other_aperture - sqrt(rr);
			return false;
		}
	}
	else
	{
		bool inside = true;
		int i=b;
		float vx = u - vert[i*2+0];
		float vy = v - vert[i*2+1];
		float z = vx*vec[i*2+1] - vy*vec[i*2+0];
		if (z>0)
			inside = false;
		d = 1.0f;

		if (inside)
		{
			d = w = getDistAbsPreevaled(b, u,v);

			if (w<bokehRingSizeAperture)
			{
				if (bokehRingBalance >= 1)
					w = (bokehRingSizeAperture*0.8f-fabs(bokehRingSizeAperture*0.2f-w))*(1.0f/bokehRingSizeAperture*(bokehRingBalance-1))+1;
				else
					w = w/bokehRingSizeAperture;
			}
			else
				w = 1;

		}
		return inside;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------

void DiaphragmSampler::getNext(float &x, float &y)
{
	x = s[2*cur];
	y = s[2*cur+1];
	cur++;
	cur = cur%numSamples;
}

//----------------------------------------------------------------------------------------------------------------------------------

void DiaphragmSampler::getThat(int n, float &x, float &y)
{
	cur = n%numSamples;
	x = s[2*cur];
	y = s[2*cur+1];
	cur++;
	cur = cur%numSamples;
}

//----------------------------------------------------------------------------------------------------------------------------------

void DiaphragmSampler::create(int num, int triesPerPoint, bool updateProgress)
{
	for (int i=0; i<GRID_SIZE; i++)
		for (int j=0; j<GRID_SIZE; j++)
			grid[i][j].clear();

	if (s)
		_aligned_free(s);
	s = (float *)_aligned_malloc(sizeof(float)*num*2, 8);
	if (!s)
		return;
	numSamples = num;
	cur = 0;

	int nn = triesPerPoint;
	float d1,d2,d3,d4,d5, dx,dy;
	int u,v;

	#ifdef RAND_PER_THREAD
		s[0] = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
		s[1] = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		unsigned int ui1;
		while(rand_s(&ui1));
		s[0]  = (float)((double)ui1/(double)(UINT_MAX));
		while(rand_s(&ui1));
		s[1]  = (float)((double)ui1/(double)(UINT_MAX));
	#endif

	u = GRID(s[0]);
	v = GRID(s[1]);
	grid[u][v].push_back(0);

	vector<int>::iterator it;
	vector<int> *cgrid;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;


	int i,j,k;
	for (i=1; i<num; i++)
	{
		if (updateProgress)
		{
			if (!(i%100))
				sc->notifyProgress("Evaluating lens points...", 100.0f*i/num);
		}

		float cX=10, cY=10, cD2=0;
		for (k=0; k<nn; k++)
		{
			#ifdef RAND_PER_THREAD
				float rX = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
				float rY = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
			#else
				while(rand_s(&ui1));
				float rX  = (float)((double)ui1/(double)(UINT_MAX));
				while(rand_s(&ui1));
				float rY  = (float)((double)ui1/(double)(UINT_MAX));
			#endif
			float rr = (rX-0.5f)*(rX-0.5f) + (rY-0.5f)*(rY-0.5f);
			if (rr > 0.25)
				continue;

			float cl = 10;

			u = GRID(rX);
			v = GRID(rY);

			float u1 = 2*rX-1;
			float v1 = 2*rY-1;
			float w1;
			float tempshit;
			if (!isPointInDiaphragm(u1,v1, w1, tempshit))
				continue;

			int found = 0;
			cgrid = &(grid[u][v]);

			for (j=0; j<9; j++)
			{
				switch (j)
				{
					case 0: cgrid = &(grid[GRID    (rX)][GRID    (rY)]); break;
					case 1: cgrid = &(grid[GRIDNEXT(rX)][GRID    (rY)]); break;
					case 2: cgrid = &(grid[GRIDNEXT(rX)][GRIDNEXT(rY)]); break;
					case 3: cgrid = &(grid[GRIDNEXT(rX)][GRIDPREV(rY)]); break;
					case 4: cgrid = &(grid[GRIDPREV(rX)][GRID    (rY)]); break;
					case 5: cgrid = &(grid[GRIDPREV(rX)][GRIDNEXT(rY)]); break;
					case 6: cgrid = &(grid[GRIDPREV(rX)][GRIDPREV(rY)]); break;
					case 7: cgrid = &(grid[GRID    (rX)][GRIDNEXT(rY)]); break;
					case 8: cgrid = &(grid[GRID    (rX)][GRIDPREV(rY)]); break;
				}
				if (!(cgrid->empty()))
				{
					for (it = cgrid->begin();     it < cgrid->end();    it++)
					{
						int n = *it;

						found ++;
						dx = s[2*n  ]-rX;	
						dy = s[2*n+1]-rY;
						d1 = dx*dx + dy*dy;

						d1 *= w1;

						if (d1 < cl)
						{
							cl = d1;
						}
					}
				}
			}

			if (found < 1)
			{
				for (j=0; j<i; j++)
				{
					dx = s[2*j  ]-rX;
					dy = s[2*j+1]-rY;
					d1 = dx*dx + dy*dy;
					//cl = d1;
					dx = s[2*j  ]-rX-1;
					dy = s[2*j+1]-rY;
					d2 = dx*dx + dy*dy;
					dx = s[2*j  ]-rX+1;
					dy = s[2*j+1]-rY;
					d3 = dx*dx + dy*dy;
					dx = s[2*j  ]-rX;
					dy = s[2*j+1]-rY-1;
					d4 = dx*dx + dy*dy;
					dx = s[2*j  ]-rX;
					dy = s[2*j+1]-rY+1;
					d5 = dx*dx + dy*dy;
					d1 = min(min(min(min(d1, d2), d3), d4), d5);
					if (d1 < cl)
					{
						cl = d1;
					}
				}
			}

			if (cl > cD2)
			{
				cD2 = cl;
				cX = rX;
				cY = rY;
			}
		}

		s[2*i  ] = cX;
		s[2*i+1] = cY;

		u = GRID(cX);
		v = GRID(cY);
		grid[u][v].push_back(i);

	}

	// [0..1] -> [-1..1]
	for (i=0; i<num; i++)
	{
		s[2*i  ] = s[2*i  ] * 2.0f - 1.0f;
		s[2*i+1] = s[2*i+1] * 2.0f - 1.0f;
	}

	for (int i=0; i<GRID_SIZE; i++)
		for (int j=0; j<GRID_SIZE; j++)
			grid[i][j].clear();
}

//----------------------------------------------------------------------------------------------------------------------------------

void DiaphragmSampler::copyPointsFromOther(DiaphragmSampler * other)
{
	if (!other)
		return;
	if (!other->s)
		return;

	if (s)
		_aligned_free(s);
	s = (float *)_aligned_malloc(sizeof(float)*other->numSamples*2, 8);
	if (!s)
		return;

	numSamples = other->numSamples;
	cur = 0;

	for (int i=0; i<numSamples; i++)
	{
		s[2*i+0] = other->s[2*i+0];
		s[2*i+1] = other->s[2*i+1];
	}
}

//----------------------------------------------------------------------------------------------------------------------------------

void DiaphragmSampler::freeSamples()
{
	if (s)
		_aligned_free(s);
	s = NULL;
	numSamples = 0;
	cur = 0;
}

//----------------------------------------------------------------------------------------------------------------------------------

void DiaphragmSampler::zeroEverything()	// be very careful, memory may leak
{
	s = NULL;
	numSamples = 0;
	cur = 0;
}

//----------------------------------------------------------------------------------------------------------------------------------

bool DiaphragmSampler::createApertureShapePreview(ImageByteBuffer * img)
{
	CHECK(img);
	CHECK(img->imgBuf);
	CHECK(img->width>0);
	CHECK(img->height>0);
	int width = img->width;
	int height = img->height;
	float w2 = width/2.0f;
	float h2 = height/2.0f;
	float pw = 2.0f/width;
	float ph = 2.0f/height;
	float mbl = 32.0f;
	float pb = 1.0f/bokehRingBalance*(255-mbl)/255;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float u1 = (x-w2)*pw;
			float v1 = (y-h2)*ph;
			float w1 = 1.0f;
			float d = 1.0f;

			unsigned char c = 0;
			if (isPointInDiaphragm(u1,v1, w1, d))
				c = (unsigned char)(int)min(255.0f, (255*w1* min(1.0f, d*w2)*pb + mbl));
			img->imgBuf[y][x] = RGB(c,c,c);

		}
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------

HBITMAP DiaphragmSampler::createApertureShapePreviewAlpha(HDC hdc, int size)
{
	int w = min(512, max(64, size));
	int h = w;
	int w2 = w/2;
	int h2 = h/2;
	float pw = 2.0f/(w-3);
	float ph = 2.0f/(h-3);

	float mbl = 32.0f;
	float pb = 1.0f/bokehRingBalance*(255-mbl)/255;

	// BGRA
	unsigned char * buffer = (unsigned char *)malloc(4*w*h);
	if (!buffer)
		return 0;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			float u1 = (x-w2)*pw;
			float v1 = (y-h2)*ph;
			float w1 = 1.0f;
			float d = 1.0f;

			unsigned char a = 255;
			unsigned char c = 255;
			if (isPointInDiaphragm(u1,v1, w1, d))
				c = (unsigned char)(int)min(255.0f, 255*( w1* min(1.0f, d*w2)*pb) + mbl );
			else
				c = a = 0;

			buffer[(x+w*y)*4+0] = c;
			buffer[(x+w*y)*4+1] = c;
			buffer[(x+w*y)*4+2] = c;
			buffer[(x+w*y)*4+3] = a;
		}
	}

	int ccx = w;
	int lineSize = ccx*4;
	BITMAPINFOHEADER bmih;
	ZeroMemory(&bmih, sizeof(bmih));
	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biBitCount = 32;
	bmih.biClrImportant = 0;
	bmih.biClrUsed = 0;
	bmih.biCompression =  BI_RGB;
	bmih.biHeight = h;
	bmih.biWidth = ccx;
	bmih.biPlanes = 1;
	bmih.biSizeImage = lineSize*h;
	bmih.biXPelsPerMeter = 0;
	bmih.biYPelsPerMeter = 0;

	BITMAPINFO bmi;
	bmi.bmiHeader = bmih;

	HBITMAP result = CreateCompatibleBitmap(hdc, w, h);
	int ccc = SetDIBits(hdc, result, 0, h, buffer, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);

	free(buffer);

	return result;
}

//----------------------------------------------------------------------------------------------------------------------------------

HBITMAP DiaphragmSampler::createApertureShapePreviewChromaticBokehAlpha(HDC hdc, int size, float depth)
{
	int width = min(512, max(64, size));
	int height = width;
	float w2 = width/2.0f;
	float h2 = height/2.0f;
	float pw = 2.0f/width;
	float ph = 2.0f/height;
	float mbl = 32.0f/255.0f;
	float pb = 1.0f/bokehRingBalance*(1-mbl);

	float rRed, rGreen, rBlue;
	float nrRed, nrGreen, nrBlue;
	evalChromaticBokehSizes(chromaticShiftLens, chromaticShiftDepth, chromaticAchromatic, fabs(depth), rRed, rGreen, rBlue);
	float rMax = max(max(rRed, rGreen), rBlue);
	nrRed = rRed / rMax;
	nrGreen= rGreen / rMax;
	nrBlue = rBlue / rMax;
	float nrMax = max(nrRed, max(nrGreen, nrBlue));
	float nrMin = min(nrRed, min(nrGreen, nrBlue));

	float perRR = 1.0f/(max(0.001f, nrMax-nrRed));
	float perGR = 1.0f/(max(0.001f, nrMax-nrGreen));
	float perBR = 1.0f/(max(0.001f, nrMax-nrBlue));

	float mR = 1.0f;
	float mG = 1.0f;
	float mB = 1.0f;

	float sumR = 0.0f;
	float sumG = 0.0f;
	float sumB = 0.0f;
	int apPixels = 0;
	float maxRval = 0.0f;
	float maxGval = 0.0f;
	float maxBval = 0.0f;
	float perAperture = 1.0f/aperture;

	unsigned char * buffer = (unsigned char *)malloc(4*height*width);
	if (!buffer)
		return 0;
	
	float * tempbuf = (float *)malloc(sizeof(float)*height*width*3);
	if (!tempbuf)
		return 0;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float u1 = (x-w2)*pw;
			float v1 = (y-h2)*ph;
			float w1 = 1.0f;
			float d = 1.0f;

			float cdd = (w2-x)*(w2-x) + (h2-y)*(h2-y);
			float cd = sqrt(cdd)/w2;


			float fr=0.0f, fg=0.0f, fb=0.0f;
			float mcol = 0.0f;

			if (isPointInDiaphragm(u1,v1, w1, d, aperture))
			{
				mcol = min(1.0f, (w1*pb+mbl));

				float st = max(0.0f, d*aperture);
				float fchrR = mR*min(1.0f, st*perRR);
				float fchrG = mG*min(1.0f, st*perGR);
				float fchrB = mB*min(1.0f, st*perBR);

				fr = fg = fb = mcol;
				fr *= fchrR;
				fg *= fchrG;
				fb *= fchrB;

				maxRval = max(maxRval, fr);
				maxGval = max(maxGval, fg);
				maxBval = max(maxBval, fb);
				sumR += fr;
				sumG += fg;
				sumB += fb;
				apPixels++;
			}
			tempbuf[(y*width+x)*3+0] = fr;
			tempbuf[(y*width+x)*3+1] = fg;
			tempbuf[(y*width+x)*3+2] = fb;
		}
	}

	float maxMplR = 1.0f/maxRval;
	float maxMplG = 1.0f/maxGval;
	float maxMplB = 1.0f/maxBval;
	float maxSum = max(max(sumR, sumG), sumB);
	float minSum = max(max(sumR, sumG), sumB);
	float mplR = minSum/sumR;
	float mplG = minSum/sumG;
	float mplB = minSum/sumB;
	float maxMpl = max(max(mplR, mplG), mplB);
	float minMpl = min(min(mplR, mplG), mplB);
	mplR *= minMpl/maxMpl;
	mplG *= minMpl/maxMpl;
	mplB *= minMpl/maxMpl;

	int counterR = 0;
	int counterG = 0;
	int counterB = 0;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float fr = tempbuf[(y*width+x)*3+0] * mplR;
			float fg = tempbuf[(y*width+x)*3+1] * mplG;
			float fb = tempbuf[(y*width+x)*3+2] * mplB;

			buffer[(y*width+x)*4+0] = (unsigned char)(int)min(255.0f, max(0.0f, 255.0f*fb));
			buffer[(y*width+x)*4+1] = (unsigned char)(int)min(255.0f, max(0.0f, 255.0f*fg));
			buffer[(y*width+x)*4+2] = (unsigned char)(int)min(255.0f, max(0.0f, 255.0f*fr));
			buffer[(y*width+x)*4+3] = (unsigned char)(int)min(255.0f, max(0.0f, (fr+fg+fb)*255));
		}
	}


	int ccx = width;
	int lineSize = ccx*4;
	BITMAPINFOHEADER bmih;
	ZeroMemory(&bmih, sizeof(bmih));
	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biBitCount = 32;
	bmih.biClrImportant = 0;
	bmih.biClrUsed = 0;
	bmih.biCompression =  BI_RGB;
	bmih.biHeight = height;
	bmih.biWidth = ccx;
	bmih.biPlanes = 1;
	bmih.biSizeImage = lineSize*height;
	bmih.biXPelsPerMeter = 0;
	bmih.biYPelsPerMeter = 0;

	BITMAPINFO bmi;
	bmi.bmiHeader = bmih;

	HBITMAP result = CreateCompatibleBitmap(hdc, width, height);
	int ccc = SetDIBits(hdc, result, 0, height, buffer, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);

	free(buffer);
	free(tempbuf);

	return result;
}

//----------------------------------------------------------------------------------------------------------------------------------

bool DiaphragmSampler::createApertureShapePreviewChromaticBokeh(ImageByteBuffer * img, float depth)	// img must be allocated
{
	CHECK(img);
	CHECK(img->imgBuf);
	CHECK(img->width>0);
	CHECK(img->height>0);
	int width = img->width;
	int height = img->height;
	float w2 = width/2.0f;
	float h2 = height/2.0f;
	float pw = 2.0f/width;
	float ph = 2.0f/height;
	float mbl = 32.0f/255.0f;
	float pb = 1.0f/bokehRingBalance*(1-mbl);

	float rRed, rGreen, rBlue;
	float nrRed, nrGreen, nrBlue;
	evalChromaticBokehSizes(chromaticShiftLens, chromaticShiftDepth, chromaticAchromatic, fabs(depth), rRed, rGreen, rBlue);
	float rMax = max(max(rRed, rGreen), rBlue);
	nrRed = rRed / rMax;
	nrGreen= rGreen / rMax;
	nrBlue = rBlue / rMax;
	float nrMax = max(nrRed, max(nrGreen, nrBlue));
	float nrMin = min(nrRed, min(nrGreen, nrBlue));

	float perRR = 1.0f/(max(0.001f, nrMax-nrRed));
	float perGR = 1.0f/(max(0.001f, nrMax-nrGreen));
	float perBR = 1.0f/(max(0.001f, nrMax-nrBlue));

	float mR = 1.0f;
	float mG = 1.0f;
	float mB = 1.0f;

	float sumR = 0.0f;
	float sumG = 0.0f;
	float sumB = 0.0f;
	int apPixels = 0;
	float maxRval = 0.0f;
	float maxGval = 0.0f;
	float maxBval = 0.0f;
	float perAperture = 1.0f/aperture;
	
	float * tempbuf = (float *)malloc(sizeof(float)*height*width*3);
	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float u1 = (x-w2)*pw;
			float v1 = (y-h2)*ph;
			float w1 = 1.0f;
			float d = 1.0f;

			float cdd = (w2-x)*(w2-x) + (h2-y)*(h2-y);
			float cd = sqrt(cdd)/w2;

			float fr=0.0f, fg=0.0f, fb=0.0f;
			float mcol = 0.0f;

			if (isPointInDiaphragm(u1,v1, w1, d, aperture))
			{
				mcol = min(1.0f, (w1*pb+mbl));
				float st = max(0.0f, d*aperture);
				float fchrR = mR*min(1.0f, st*perRR);
				float fchrG = mG*min(1.0f, st*perGR);
				float fchrB = mB*min(1.0f, st*perBR);

				fr = fg = fb = mcol;
				fr *= fchrR;
				fg *= fchrG;
				fb *= fchrB;

				maxRval = max(maxRval, fr);
				maxGval = max(maxGval, fg);
				maxBval = max(maxBval, fb);
				sumR += fr;
				sumG += fg;
				sumB += fb;
				apPixels++;
			}
			tempbuf[(y*width+x)*3+0] = fr;
			tempbuf[(y*width+x)*3+1] = fg;
			tempbuf[(y*width+x)*3+2] = fb;

		}
	}

	float maxMplR = 1.0f/maxRval;
	float maxMplG = 1.0f/maxGval;
	float maxMplB = 1.0f/maxBval;
	float maxSum = max(max(sumR, sumG), sumB);
	float minSum = max(max(sumR, sumG), sumB);
	float mplR = minSum/sumR;
	float mplG = minSum/sumG;
	float mplB = minSum/sumB;
	float maxMpl = max(max(mplR, mplG), mplB);
	float minMpl = min(min(mplR, mplG), mplB);
	mplR *= minMpl/maxMpl;
	mplG *= minMpl/maxMpl;
	mplB *= minMpl/maxMpl;
	int counterR = 0;
	int counterG = 0;
	int counterB = 0;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float fr = tempbuf[(y*width+x)*3+0] * mplR;
			float fg = tempbuf[(y*width+x)*3+1] * mplG;
			float fb = tempbuf[(y*width+x)*3+2] * mplB;
			unsigned char cr = (unsigned char)(int)min(255.0f, max(0.0f, 255.0f*fr));
			unsigned char cg = (unsigned char)(int)min(255.0f, max(0.0f, 255.0f*fg));
			unsigned char cb = (unsigned char)(int)min(255.0f, max(0.0f, 255.0f*fb));
			img->imgBuf[y][x] = RGB(cr,cg,cb);
		}
	}

	free(tempbuf);

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------

bool evalChromaticBokehSizes(float shiftLens, float shiftDepth, float achromatic, float depth, float &red, float &green, float &blue)
{
	float startRX = 0.0f;
	float startRY = (0.3f-0.08f*shiftLens)/0.4f;
	float startGX = 0.0f;
	float startGY = 0.3f/0.4f;
	float startBX = 0.0f;
	float startBY = (0.3f+0.08f*shiftLens)/0.4f;
	float halfRX = (0.7f-0.08f*shiftDepth-0.2f)/0.5f;
	float halfRY = 0.0f;
	float halfGX = (0.7f-0.2f)/0.5f;
	float halfGY = 0.0f;
	float halfBX = (0.7f+0.08f*shiftDepth*(-achromatic*2+1)-0.2f)/0.5f;
	float halfBY = 0.0f;


	float ratioR = (halfRY-startRY)/(halfRX-startRX);
	float ratioG = (halfGY-startGY)/(halfGX-startGX);
	float ratioB = (halfBY-startBY)/(halfBX-startBX);

	red   = fabs(ratioR*depth+startRY);
	green = fabs(ratioG*depth+startGY);
	blue  = fabs(ratioB*depth+startBY);

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------

bool DiaphragmSampler::allocSpaceChromaticSampleColors()
{
	freeSpaceChromaticSampleColors();
	chrS = (float *)malloc(sizeof(float)*3*numSamples);
	if (!chrS)
		return false;
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------

bool DiaphragmSampler::freeSpaceChromaticSampleColors()
{
	if (chrS)
		free(chrS);
	chrS = NULL;
	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------

bool DiaphragmSampler::evalChromaticSampleColors(float depth, int numSampl)
{
	if (!chrS)
		return false;

	if (chromaticShiftLens==0.0f   &&   chromaticShiftDepth==0.0f)
	{
		for (int i=0; i<numSamples*3; i++)
			chrS[i] = 1.0f;
		return true;
	}

	float rRed, rGreen, rBlue;
	evalChromaticBokehSizes(chromaticShiftLens, chromaticShiftDepth, chromaticAchromatic, depth, rRed, rGreen, rBlue);
	float rMax = max(max(rRed, rGreen), rBlue);
	rRed   /= rMax;
	rGreen /= rMax;
	rBlue  /= rMax;
	float perRR = 1.0f/(max(0.001f, 1.0f-rRed));
	float perGR = 1.0f/(max(0.001f, 1.0f-rGreen));
	float perBR = 1.0f/(max(0.001f, 1.0f-rBlue));

	float sumR = 0.0f;
	float sumG = 0.0f;
	float sumB = 0.0f;

	numSampl = min(numSampl, numSamples);

	for (int i=0; i<numSampl; i++)
	{
		float u,v;
		getThat(i,u,v);
		float w, d;
		isPointInDiaphragm(u,v, w, d, aperture);

		float st = max(0.0f, d*aperture);
		float fchrR = min(1.0f, st*perRR);
		float fchrG = min(1.0f, st*perGR);
		float fchrB = min(1.0f, st*perBR);
		chrS[i*3+0] = fchrR;
		chrS[i*3+1] = fchrG;
		chrS[i*3+2] = fchrB;
		sumR += fchrR;
		sumG += fchrG;
		sumB += fchrB;
	}

	float mnR = numSampl/sumR;
	float mnG = numSampl/sumG;
	float mnB = numSampl/sumB;

	for (int i=0; i<numSampl; i++)
	{
		chrS[i*3+0] *= mnR;
		chrS[i*3+1] *= mnG;
		chrS[i*3+2] *= mnB;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------

bool DiaphragmSampler::getChrColorSample(unsigned int id, float &r, float &g, float &b)
{
	r = g = b = 1.0f;
	if (!chrS)
		return false;

	id = id % numSamples;
	r = chrS[id*3+0];
	g = chrS[id*3+1];
	b = chrS[id*3+2];

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------
