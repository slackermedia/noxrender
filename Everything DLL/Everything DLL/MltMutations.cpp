#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

#define DEBUG_STATS
//#define DEBUG_LOG2

extern clock_t cfindinters;
extern clock_t cbrdfsample;
extern clock_t clightsample;
extern clock_t csunsample;
extern clock_t cskysample;
extern clock_t cupdateimage;

Color4 Metropolis::runMutations()
{
	for (int i=0; i<=numEP; i++)
		eCandidate[i] = ePath[i];
	for (int i=0; i<=numLP; i++)
		lCandidate[i] = lPath[i];
	numCLP = numLP;
	numCEP = numEP;

	long long mutsAcc[10];
	long long mutsRej[10];
	long long tentAcc[10];
	long long tentRej[10];
	for (int i=0; i<10; i++)
		mutsAcc[i] = 0;
	for (int i=0; i<10; i++)
		mutsRej[i] = 0;
	for (int i=0; i<10; i++)
		tentAcc[i] = 0;
	for (int i=0; i<10; i++)
		tentRej[i] = 0;
	int aaaB = 0;
	int rrrB = 0;
	int aaaL = 0;
	int rrrL = 0;
	int unRej = 0;
	sysRej = 0;
	int lastAcceptedMutation = -2;

	int cx=0, cy=0;

	cam->getCoordsFromDirReturnInFrustum(ePath[0].outDir, ePath[0].pos, pX, pY);

	// START HERE -> MAIN LOOP ----------------
	
	clock_t cmetropolisstart = 0;
	clock_t cmetropolisstop = 0;
	clock_t cdirectstart = 0;
	clock_t cdirectstop = 0;
	clock_t cdirectticks = 0;
	cmetropolisstart = clock();


	
	while (*running)
	{
		sumProbes++;
		// random mutation
		unsigned int r1;
		bool mOK = true;
		int mthd = randomMutationType();
		tempwasspecular = false;

		// mutate
		switch(mthd)
		{
			case MLT_MUT_LENS:
				mOK = mutateLens();
				break;
			case MLT_MUT_PERT_LENS:
				mOK = perturbateLens2();
				break;
			case MLT_MUT_BIDIR:
				mOK = mutateDiffuse();
				break;
			case MLT_MUT_PERT_CAUSTIC:
				mOK = perturbateCaustic();
				break;
			case MLT_MUT_PERT_CHAIN:
				mOK = perturbateChain();
				break;
		}

		#ifdef DEBUG_LOG2
			if (!mOK)
				Logger::add("incomplete mutation - rejecting");		
		#endif

		float R1=-2,R2=-2;

		bool probOK = mOK;
		if (mOK)
		{
			while(rand_s(&r1));
			float r = (float)((double)r1/(double)UINT_MAX);


			switch (mthd)
			{
				case MLT_MUT_LENS:
					R1 = tentativeTransitionForLens(true);
					R2 = tentativeTransitionForLens(false);
					break;
				case MLT_MUT_PERT_LENS:
					R1 = tentativeTransitionForPertLens2(true);
					R2 = tentativeTransitionForPertLens2(false);
					break;
				case MLT_MUT_PERT_CAUSTIC:
					R1 = tentativeTransitionForPertCaustic(true);
					R2 = tentativeTransitionForPertCaustic(false);
					break;
				case MLT_MUT_PERT_CHAIN:
					R1 = tentativeTransitionForPertChain(true);
					R2 = tentativeTransitionForPertChain(false);
					break;
				case MLT_MUT_BIDIR:
					R1 = tentativeTransitionForDiffuse(true);
					R2 = tentativeTransitionForDiffuse(false);
					break;
				default:
					R1 = -3;
					R2 = -3;
					break;
			}

			if (_isnan(R1))
			{
				R1 = 0;
				#ifdef DEBUG_LOG2
					Logger::add("R1 nan");
				#endif
			}
			if (_isnan(R2))
			{
				R2 = 0;
				#ifdef DEBUG_LOG2
					Logger::add("R2 nan");
				#endif
			}

			float A = min(1, R1 /R2);

			if (r < A)
			{
				tentAcc[mthd]++;
				probOK = true;
			}
			else
			{
				tentRej[mthd]++;
				if (sysRej > MLT_MAX_REJ)
				{
					unRej++;
					Logger::add("Path stuck - forcing acceptance");
					probOK = true;
					char dddd[64];
					sprintf_s(dddd, 64, "pixel: %f x %f", (pX/cam->aa), (pY/cam->aa));
					Logger::add(dddd);
				}
				else
					probOK = false;
			}

			if (mthd == MLT_MUT_LENS)
			{
				if (probOK)
					aaaL++;
				else
					rrrL++;
			}

			#ifdef DEBUG_LOG2
				if (mthd != 0)
				{
					char buf[128];
					sprintf_s(buf, 128, "R1=%f  R2=%f  A=%f  r=%f  OK=%s", R1, R2, A, r, probOK?"true":"false");
					Logger::add(buf);
				}
			#endif
		}

		bool ok = true;
		cam->getCoordsFromDirReturnInFrustum(eCandidate[0].outDir, eCandidate[0].pos, pCX, pCY);

		#ifdef DEBUG_LOG2
			if (!ok)
				Logger::add("not in frustum");
		#endif

		// accept or reject
		if (ok  &&  probOK)
		{	// accept
			for (int i=0; i<=numCEP; i++)
				ePath[i] = eCandidate[i];
			for (int i=0; i<=numCLP; i++)
				lPath[i] = lCandidate[i];
			numLP = numCLP;
			numEP = numCEP;
			lTriID = clTriID;
			pX = pCX;
			pY = pCY;
			accCnt++;
			sysRej = 0;
			sourceSky = cSourceSky;
			sourceSun = cSourceSun;
			lastAcceptedMutation = mthd;
			connectionAttenuation = connectionAttenuationCandidate;
			mutsAcc[mthd]++;
			#ifdef DEBUG_LOG2
				Logger::add("accepted");
			#endif

		}
		else
		{	// reject
			for (int i=0; i<=numEP; i++)
				eCandidate[i] = ePath[i];
			for (int i=0; i<=numLP; i++)
				lCandidate[i] = lPath[i];
			numCLP = numLP;	
			numCEP = numEP;
			clTriID = lTriID;
			pCX = pX;
			pCY = pY;
			rejCnt++;
			sysRej++;
			cSourceSky = sourceSky;
			cSourceSun = sourceSun;
			connectionAttenuationCandidate = connectionAttenuation;
			mutsRej[mthd]++;
			#ifdef DEBUG_LOG2
				Logger::add("rejected");
			#endif
		}

		#ifdef DEBUG_LOG2
			char bbuf2[64];
			sprintf_s(bbuf2, 64, "nE=%d  nL=%d   nCE=%d  nCL=%d", numEP, numLP, numCEP, numCLP);
			Logger::add(bbuf2);
		#endif

		// calc contribution and add to histogram
		Color4 c = calculateContribution();

		float cl = c.r * 0.299f + c.g * 0.587f + c.b * 0.114f;
		if (cl>0.00000000001f)
			c *= 1.0f/cl;
		else
			c = Color4(0,0,0);

		c *= weightBias;

		// --- direct here ---
		Point3d directOrigin = cam->position;
		Vector3d directDirection;
		float dX, dY;
		cam->getHaltonCoords(dX, dY, true);
		directDirection = cam->getDirection(dX,dY, directOrigin, 0);
							cdirectstart = clock();
		evalLocalIllumStandard(directOrigin, directDirection, dX, dY);
							cdirectstop = clock();
							cdirectticks += cdirectstop - cdirectstart;

		ss->perfCounter++;

		c *= cam->width * cam->height;

		if (!lPath[0].mlay)
		{
			int bbb = 0;
			if (sourceSun  &&  ss->sunsky->sunOtherBlendLayer)
				bbb = 1;
			if (cam->blendBits & (1<<bbb))
				cam->blendBuffGI[bbb].addPixelColor((int)pX, (int)pY, c, false);
			cam->iReg->incrementHits();
		}
		else
			if (lPath[0].mlay->type == MatLayer::TYPE_EMITTER)
			{
				int bbb = lPath[0].mat->blendIndex;
				if (cam->blendBits & (1<<bbb))
					cam->blendBuffGI[bbb].addPixelColor((int)pX, (int)pY, c, false);
				cam->iReg->incrementHits();
			}
	}

	cmetropolisstop = clock();

	// DONE -------------------

	Logger::add("Rendering loop finished");

	float dpercent = (float)((double)cdirectticks / (double)(cmetropolisstop - cmetropolisstart)) * 100;
	char pppbuf[96];
	sprintf_s(pppbuf, 96, "Direct took %f percent of time,", dpercent);
	Logger::add(pppbuf);

	return Color4(0,0,0);
}
