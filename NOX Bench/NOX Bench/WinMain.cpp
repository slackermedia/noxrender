#include <windows.h>
#include "resource_benchmark.h"
#include "../../../../Visual Studio 2012/Projects/Everything DLL/Everything DLL/BenchmarkWindow.h"
#include "../../../../Visual Studio 2012/Projects/Everything DLL/Everything DLL/nox_version.h"
#include "../../../../Visual Studio 2012/Projects/Everything DLL/Everything DLL/log.h"

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// lpCmdLine non-unicode, use GetCommandLine for unicode
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG msg;
	HACCEL hAccelTable;
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDR_ACCELERATOR1));

	for (int i=1; i<__argc; i++)
	{
		if (!__argv[i])
			continue;
		if (strlen(__argv[i])<2)
			continue;

		if (__argv[i][0] == '-')
		{
		}
		else
		{
		}
	}

	Logger::setLogsOn(false);
	HWND hWindow;
	BenchmarkWindow bWindow(hInstance);
	bool ok = bWindow.createWindow(0, NOX_VER_MAJOR, NOX_VER_MINOR, NOX_VER_BUILD, NOX_VER_COMP_NO);
	if (!ok)
		return 0;

	hWindow = bWindow.getHWND();
	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(hWindow, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

