#ifndef __xml_loader_h
#define __xml_loader_h

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "raytracer.h"
#include "MatEditor.h"

#define XML_TRI_SIZE 208
#define XML_TRI_ROW_SIZE 219

#define LAYER_TEX_SLOT_COLOR0 0
#define LAYER_TEX_SLOT_COLOR90 1
#define LAYER_TEX_SLOT_ROUGHNESS 2
#define LAYER_TEX_SLOT_WEIGHT 3
#define LAYER_TEX_SLOT_LIGHT 4
#define LAYER_TEX_SLOT_NORMAL 5
#define LAYER_TEX_SLOT_TRANSM 6
#define LAYER_TEX_SLOT_ANISO 7
#define LAYER_TEX_SLOT_ANISO_ANGLE 8
#define MAT_TEX_SLOT_OPACITY 100
#define MAT_TEX_SLOT_DISPLACEMENT 101

// conversions between decimal and hexadecimal values.
class DecHex
{
public:
	static bool singleHexToDec(unsigned char hex, unsigned char &dec);
	static unsigned char singleDecToHex(unsigned char dec);
	static void floatToHex(float f, unsigned char *h);
	static bool hex8ToFloat(unsigned char * h, float &f);
	static void uintToHex(unsigned int ui, unsigned char *h);
	static bool hex8ToUint(unsigned char * h, unsigned int &ui);
	static bool hex2ToUchar(unsigned char *h, unsigned char &c);
	static bool hex2ToUcharBigEndian(unsigned char *h, unsigned char &c);
	static bool ucharToHex2(const unsigned char c, unsigned char * h);
	static bool ucharToHex2BigEndian(const unsigned char c, unsigned char * h);
};

//-----------------------------------------------------------------------------

class XMLScene
{
private:
	xmlDocPtr xmlDoc;

public:
	int errLine;
	int triCounter;
	int curMeshNum;	
	int fileSize;
	int eNumTris;
	int cTri;
	bool addCamBuffs;
	char * directory;

	Point3d cbMin, cbMax;

	char * decToHexString(char * src, unsigned int lsrc);
	char * getValidTexPath(char * fname);
	bool parseMaterialEmissionLayer(xmlNodePtr curNode, MaterialNox * mat);
	bool parseMaterialShadeLayer(xmlNodePtr curNode, MaterialNox * mat);
	bool parseFresnel(xmlNodePtr curNode, Fresnel & fresnel);
	bool parseMaterials(xmlNodePtr curNode);
	bool parseMaterial(xmlNodePtr curNode);
	bool parseTexture(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay);
	bool parseTextureTypeFile(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay, int slot);
	bool parseTexturePost(xmlNodePtr curNode, TextureInstance * texptr);
	bool parseTextureNormalPost(xmlNodePtr curNode, TextureInstance * texptr);
	bool parseCamera(xmlNodePtr curNode, char * curPath);

	bool parseCameraFinalValues(xmlNodePtr curNode, FinalModifier * fMod, char ** texObstacle, char ** texDiaphragm);
	bool parseCameraFinalValues(xmlNodePtr curNode, Camera * cam);
	bool parseCameraPostValues(xmlNodePtr curNode, Camera * cam);
	bool parseCameraPostCurve(xmlNodePtr curNode, Camera * cam);
	bool parseCameraPostValues(xmlNodePtr curNode, ImageModifier * iMod);
	bool parseCameraPostCurve(xmlNodePtr curNode, ImageModifier * iMod);
	bool parseUserPresets(xmlNodePtr curNode);
	bool parseUserPreset(xmlNodePtr curNode, PresetPost * pr);


	bool parseImageData(xmlNodePtr curNode, char * curPath, Camera * cam);
	bool parseTriangle(xmlNodePtr curNode);
	bool parseMesh(xmlNodePtr curNode);
	bool parseMeshSrc(xmlNodePtr curNode);
	bool parseInstance(xmlNodePtr curNode);
	bool parseObjects(xmlNodePtr curNode);
	bool parseSkyDate(xmlNodePtr curNode, int &Month, int &Day, int &Hour, int &Minute, int &GMT);
	bool parseSky(xmlNodePtr curNode);
	bool parseUserColors(xmlNodePtr curNode);
	bool parseBlend(xmlNodePtr curNode, BlendSettings &blend);
	bool parseRendererSettings(xmlNodePtr curNode);
	bool parseSceneInfo(xmlNodePtr curNode);

	bool LoadScene(char * filename);


	MaterialNox * loadMaterial(char * filename, int& errorline);
	bool parsePreview(xmlNodePtr curNode, ImageByteBuffer * image);


	bool saveScene(char * filename, Scene * scene, bool addCamBuffers);
	bool insertTriangle(xmlNodePtr curNode, Triangle * tri);
	bool insertCamera(xmlNodePtr curNode, Camera * cam, bool active, char * dirRelative, char * dirFull, int camNum, bool nowRendering );
	bool insertInCameraPost(xmlNodePtr curNode, ImageModifier * iMod);
	bool insertInCameraFinal(xmlNodePtr curNode, FinalModifier * fMod, char * texDiaph, char * texObst);
	bool insertSky(xmlNodePtr curNode, SunSky * sky, bool turnedOn);
	bool insertRenderedData(xmlNodePtr curNode, xmlNodePtr &newNode, int w, int h, unsigned int cseed, int rtime);

	bool saveMaterial(char * filename, MaterialNox * mat);
	bool insertMaterialData(xmlNodePtr curNode, MaterialNox * mat);
	bool insertMatLayer(xmlNodePtr curNode, MatLayer * mlay);
	bool insertEmissionLayer(xmlNodePtr curNode, MatLayer * mlay);
	bool insertShadeLayer(xmlNodePtr curNode, MatLayer * mlay);
	bool insertTexture(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay, int slot);
	bool insertTextureTypeFile(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay, int slot);
	bool insertFresnel(xmlNodePtr curNode, Fresnel * fres);
	bool insertPreview(xmlNodePtr curNode, ImageByteBuffer * preview);
	bool insertUserColors(xmlNodePtr curNode);
	bool insertBlendSettings(xmlNodePtr curNode, BlendSettings &blendSettings, bool onlyNames=false);
	bool insertTimers(xmlNodePtr curNode);
	bool insertSceneInfo(xmlNodePtr curNode);
	bool insertUserPresets(xmlNodePtr curNode);
	bool insertUserPreset(xmlNodePtr curNode, PresetPost * pr);

	bool savePost(char * filename, Camera * cam);
	bool loadPost(char * filename, Camera * cam);
	bool saveBlendSettings(char * filename, BlendSettings &blend, bool onlyNames=false);
	bool loadBlendSettings(char * filename, BlendSettings &blend, bool onlyNames=false);
	bool saveFinal(char * filename, Camera * cam);
	bool loadFinal(char * filename, Camera * cam);
	bool savePreset(char * filename, Camera * cam, BlendSettings &blend);
	bool loadPreset(char * filename, ImageModifier * iMod, FinalModifier * fMod, BlendSettings * blend, Texture * texObstacle, Texture * texDiaphragm);

	XMLScene();
	~XMLScene();
};



class XMLonlineMatInfo
{
private:
	xmlDocPtr xmlDoc;

public:
	int errLine;
	int fSize;
	int numMats;
	int totalResults;

	OnlineMatLib::OnlineMatInfo infos[7];

	bool loadFile(char * filename);
	bool parseMaterialInfo(xmlNodePtr curNode, int num);

	XMLonlineMatInfo();
	~XMLonlineMatInfo();
};


class XMLlocalMatInfo
{
private:
	xmlDocPtr xmlDoc;

public:
	int errLine;

	LocalMatLib::MatInfo info;

	bool loadFile(char * filename);
	bool parseLayer(xmlNodePtr curNode);
	bool parsePreview(xmlNodePtr curNode);

	XMLlocalMatInfo();
	~XMLlocalMatInfo();
};







#endif

