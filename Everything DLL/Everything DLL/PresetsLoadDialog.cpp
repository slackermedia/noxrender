#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EM2Controls.h"
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "noxfonts.h"
#include "log.h"

bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
void presetsDlgSetPositionsOnStart(HWND hWnd);
void presetsDlgUpdateBgShifts(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK PresetsLoadDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBmp = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				PresetChoose pc;
				if (lParam)
					pc = *((PresetChoose *)lParam);

				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBmp = generateBackgroundPattern(1920, 1200, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_OK));
				embOK->bgImage = hBgBmp;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_CANCEL));
				embCancel->bgImage = hBgBmp;
				embCancel->setFont(fonts->em2button, false);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_TITLE));
				emtitle->bgImage = hBgBmp;
				emtitle->setFont(fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2CheckBox * emcLayers = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_LAYERS));
				emcLayers->setFont(fonts->em2checkbox, false);
				emcLayers->bgImage = hBgBmp;
				emcLayers->selected = pc.layers;
				EM2CheckBox * emcPost = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_POST));
				emcPost->setFont(fonts->em2checkbox, false);
				emcPost->bgImage = hBgBmp;
				emcPost->selected = pc.post;
				EM2CheckBox * emcCorrection = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_CORRECTION));
				emcCorrection->setFont(fonts->em2checkbox, false);
				emcCorrection->bgImage = hBgBmp;
				emcCorrection->selected = pc.correction;
				EM2CheckBox * emcFakeDof  = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_FAKEDOF));
				emcFakeDof->setFont(fonts->em2checkbox, false);
				emcFakeDof->bgImage = hBgBmp;
				emcFakeDof->selected = pc.fakedof;
				EM2CheckBox * emcEffects = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_EFFECTS));
				emcEffects->setFont(fonts->em2checkbox, false);
				emcEffects->bgImage = hBgBmp;
				emcEffects->selected = pc.effects;

				presetsDlgSetPositionsOnStart(hWnd);
				presetsDlgUpdateBgShifts(hWnd);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBmp)
					DeleteObject(hBgBmp);
				hBgBmp = 0;
			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBmp)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBmp);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}
				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_PRESETS_OK:
						{
							CHECK(wmEvent==BN_CLICKED);
							static PresetChoose result;
							EM2CheckBox * emcLayers = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_LAYERS));
							EM2CheckBox * emcPost = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_POST));
							EM2CheckBox * emcCorrection = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_CORRECTION));
							EM2CheckBox * emcFakeDof  = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_FAKEDOF));
							EM2CheckBox * emcEffects = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_EFFECTS));
							result.layers = emcLayers ? emcLayers->selected : false;
							result.post = emcPost ? emcPost->selected : false;
							result.correction = emcCorrection ? emcCorrection->selected : false;
							result.fakedof = emcFakeDof ? emcFakeDof->selected : false;
							result.effects = emcEffects ? emcEffects->selected : false;
							EndDialog(hWnd, (INT_PTR)(&result));
						}
						break;
					case IDC_REND2_PRESETS_CANCEL:
						{
							CHECK(wmEvent==BN_CLICKED);
							EndDialog(hWnd, (INT_PTR)0);
						}
						break;
				}
			}	// end WM_COMMAND
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

void presetsDlgUpdateBgShifts(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2CheckBox * emcLayers = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_LAYERS));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_LAYERS), 0, 0, emcLayers->bgShiftX, emcLayers->bgShiftY);
	EM2CheckBox * emcPost = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_POST));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_POST), 0, 0, emcPost->bgShiftX, emcPost->bgShiftY);
	EM2CheckBox * emcCorrection = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_CORRECTION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_CORRECTION), 0, 0, emcCorrection->bgShiftX, emcCorrection->bgShiftY);
	EM2CheckBox * emcFakeDof = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_FAKEDOF));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_FAKEDOF), 0, 0, emcFakeDof->bgShiftX, emcFakeDof->bgShiftY);
	EM2CheckBox * emcEffects = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_EFFECTS));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_EFFECTS), 0, 0, emcEffects->bgShiftX, emcEffects->bgShiftY);

	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);
	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_PRESETS_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_REND2_PRESETS_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void presetsDlgSetPositionsOnStart(HWND hWnd)
{
	RECT rect, wrect;
	GetClientRect(hWnd, &rect);
	GetWindowRect(hWnd, &wrect);
	int ww = 160 + wrect.right-wrect.left-rect.right;
	int wh = 215 + wrect.bottom-wrect.top-rect.bottom;
	SetWindowPos(hWnd, HWND_TOP, 0,0, ww, wh, SWP_NOMOVE);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_TITLE),			HWND_TOP,			0,		6,			160,	20,			0);

	int x,y;
	x=10; y= 35;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_LAYERS),		HWND_TOP,			x,		y+3,			80,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_POST),			HWND_TOP,			x,		y+33,			80,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_CORRECTION),	HWND_TOP,			x,		y+63,			80,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_FAKEDOF),		HWND_TOP,			x,		y+93,			80,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_EFFECTS),		HWND_TOP,			x,		y+123,			80,	10,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_OK),			HWND_TOP,			x,		y+147,			65,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_PRESETS_CANCEL),		HWND_TOP,			x+75,	y+147,			65,	22,			0);
}

//------------------------------------------------------------------------------------------------
