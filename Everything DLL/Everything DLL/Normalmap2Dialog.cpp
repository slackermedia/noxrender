#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EM2Controls.h"
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "noxfonts.h"
#include "log.h"
#include "valuesMinMax.h"

#define TIMER_REDRAW_TEX2_ID 151

//------------------------------------------------------------------------------------------------

extern HMODULE hDllModule;
void normDlg2UpdateBgShifts(HWND hWnd);
void normDlg2SetPositionsOnStart(HWND hWnd);
char * openTexDialog(HWND hwnd);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
bool makeNormalShadePreview(HWND hWnd, Texture * ntex, int px, int py, int d);
bool makeNormal2BytePreview(HWND hWnd, Texture * ntex);
bool guiToNormal2(HWND hWnd, Texture * ntex);
bool normal2InfoToGui(HWND hWnd, Texture * ntex);


//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK Normalmap2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBmp = 0;
	static Texture * tex1 = NULL;
	static Texture * tex2 = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBmp = generateBackgroundPattern(1920, 1200, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();
				normDlg2SetPositionsOnStart(hWnd);
				normDlg2UpdateBgShifts(hWnd);

				tex1 = (Texture *)lParam;
				tex2 = new Texture();
				if (tex1)
				{
					tex2->copyFromOther(tex1);
				}

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_OK));
				embOK->bgImage = hBgBmp;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_CANCEL));
				embCancel->bgImage = hBgBmp;
				embCancel->setFont(fonts->em2button, false);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_TITLE));
				emtitle->bgImage = hBgBmp;
				emtitle->setFont(fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2GroupBar * emgrNorm = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_NORMAL2_GR_NORMAL_MAP));
				emgrNorm->bgImage = hBgBmp;
				emgrNorm->setFont(fonts->em2groupbar, false);
				EM2GroupBar * emgrShade = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_NORMAL2_GR_SHADE_PREVIEW));
				emgrShade->bgImage = hBgBmp;
				emgrShade->setFont(fonts->em2groupbar, false);

				EM2Button * embLoad = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_LOAD));
				embLoad->bgImage = hBgBmp;
				embLoad->setFont(fonts->em2button, false);
				EM2Button * embUnload = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_UNLOAD));
				embUnload->bgImage = hBgBmp;
				embUnload->setFont(fonts->em2button, false);

				EM2Text * emtFilename = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_FILENAME));
				emtFilename->bgImage = hBgBmp;
				emtFilename->setFont(fonts->em2text, false);
				EM2Text * emtRes = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_RESOLUTION));
				emtRes->bgImage = hBgBmp;
				emtRes->setFont(fonts->em2text, false);
				emtRes->align = EM2Text::ALIGN_RIGHT;
				EM2Text * emtPower = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_TEXT_POWER));
				emtPower->bgImage = hBgBmp;
				emtPower->setFont(fonts->em2text, false);

				EM2EditTrackBar * emetPower = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_NORMAL2_POWER));
				emetPower->setFont(fonts->em2edittrack, false);
				emetPower->bgImage = hBgBmp;
				emetPower->setEditBoxWidth(38, 5);
				emetPower->setValuesToFloat(tex2->normMod.powerEV, NOX_NORMAL_POWER_MIN, NOX_NORMAL_POWER_MAX, NOX_NORMAL_POWER_DEF, 0.05f);

				EM2CheckBox * emcInvX = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_X));
				emcInvX->bgImage = hBgBmp;
				emcInvX->setFont(fonts->em2button, false);
				emcInvX->selected = tex2->normMod.invertX;
				EM2CheckBox * emcInvY = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_Y));
				emcInvY->bgImage = hBgBmp;
				emcInvY->setFont(fonts->em2button, false);
				emcInvY->selected = tex2->normMod.invertY;

				EMPView * empvnormal = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_TEX));
				EMPView * empvshade  = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_SHADE));

				empvshade->dontLetUserChangeZoom = true;
				empvshade->byteBuff = new ImageByteBuffer();
				RECT sRect;
				GetClientRect(empvshade->hwnd, &sRect);
				empvshade->byteBuff->allocBuffer(sRect.right,sRect.bottom);
				empvshade->byteBuff->clearBuffer();
				GetClientRect(empvnormal->hwnd, &sRect);
				empvnormal->byteBuff->allocBuffer(sRect.right,sRect.bottom);
				empvnormal->byteBuff->clearBuffer();

				makeNormal2BytePreview(hWnd, tex2);
				normal2InfoToGui(hWnd, tex2);

			}
			break;
		case WM_CLOSE:
			{
				SendMessage(hWnd, WM_COMMAND, MAKELONG(IDC_NORMAL2_CANCEL, BN_CLICKED), (LPARAM)GetDlgItem(hWnd, IDC_NORMAL2_CANCEL));
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBmp)
					DeleteObject(hBgBmp);
				hBgBmp = 0;

				EMPView * empvsrc = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_TEX));
				EMPView * empvdst = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_SHADE));
				if (empvsrc  &&  empvsrc->byteBuff)
				{
					empvsrc->byteBuff->freeBuffer();
					delete empvsrc->byteBuff;
					empvsrc->byteBuff = NULL;
				}
				if (empvdst  &&  empvdst->byteBuff)
				{
					empvdst->byteBuff->freeBuffer();
					delete empvdst->byteBuff;
					empvdst->byteBuff = NULL;
				}
			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBmp)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBmp);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}
				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_MOUSEMOVE:
			{
				POINT pos;
				pos.x = (short)LOWORD(lParam); 
				pos.y = (short)HIWORD(lParam); 
				ClientToScreen(hWnd, &pos);
				ScreenToClient(GetDlgItem(hWnd, IDC_NORMAL2_PREV_SHADE), &pos);

				makeNormalShadePreview(hWnd, tex2, pos.x, pos.y, 128);
				InvalidateRect(GetDlgItem(hWnd, IDC_NORMAL2_PREV_SHADE), NULL, false);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_NORMAL2_OK:
						{
							CHECK(wmEvent==BN_CLICKED);
							EndDialog(hWnd, (INT_PTR)(0));
							if (tex1)
							{
								tex1->freeAllBuffers();
								delete tex1;
								tex1 = NULL;
							}
							EndDialog(hWnd, (INT_PTR)(tex2));
						}
						break;
					case IDC_NORMAL2_CANCEL:
						{
							CHECK(wmEvent==BN_CLICKED);
							if (tex1)
							{
								tex1->freeAllBuffers();
								delete tex1;
								tex1 = NULL;
							}
							if (tex2)
							{
								tex2->freeAllBuffers();
								delete tex2;
								tex2 = NULL;
							}
							EndDialog(hWnd, (INT_PTR)(0));
						}
						break;
					case IDC_NORMAL2_LOAD:
						{
							CHECK(wmEvent==BN_CLICKED);
							Logger::add("Texture load clicked.");
							char * fname = openTexDialog(hWnd);
							if (!fname)
							{
								Logger::add("No file selected.");
								break;
							}
							Logger::add("Loading file:");
							Logger::add(fname);

							Texture * tex = new Texture();
							if (tex->loadFromFile(fname)  &&   tex->isValid())
							{
								HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL2_PREV_TEX);
								EMPView * empv = GetEMPViewInstance(hPrev);
								Texture * ttex = tex2;
								if (ttex)
								{
									ttex->freeAllBuffers();
									delete ttex;
								}
								tex2 = tex;

								if (!tex->isItNormalMap())
								{
									tex->convertMeToNormalMap();
									tex->is_it_converted_normal = true;
								}

								empv->mx = empv->my = 0;

								makeNormal2BytePreview(hWnd, tex2);

								Logger::add("Texture loaded.");

								Sleep(50);
								empv->mx = empv->my = 0;
								empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);
								InvalidateRect(empv->hwnd, NULL, false);
							}

							normal2InfoToGui(hWnd, tex2);
							if (fname)
								free(fname);
						}
						break;
					case IDC_NORMAL2_UNLOAD:
						{
							CHECK(wmEvent==BN_CLICKED);
							Texture * newtex = new Texture();
							if (tex2)
							{
								tex2->freeAllBuffers();
								delete tex2;
							}
							tex2 = newtex;
							makeNormal2BytePreview(hWnd, tex2);
							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_TEX));
							CHECK(empv);
							CHECK(empv->byteBuff);
							RECT sRect;
							GetClientRect(empv->hwnd, &sRect);
							empv->byteBuff->allocBuffer(sRect.right,sRect.bottom);
							empv->byteBuff->clearBuffer();
							empv->zoom100();
							InvalidateRect(empv->hwnd, NULL, false);
							EMPView * empv2 = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_SHADE));
							CHECK(empv2);
							CHECK(empv2->byteBuff);
							empv2->byteBuff->clearBuffer();
							InvalidateRect(empv2->hwnd, NULL, false);
							normal2InfoToGui(hWnd, tex2);
						}
						break;
					case IDC_NORMAL2_INVERT_X:
					case IDC_NORMAL2_INVERT_Y:
						{
							CHECK(wmEvent==BN_CLICKED);
							guiToNormal2(hWnd, tex2);
						}
						break;
					case IDC_NORMAL2_POWER:
						{
							CHECK(wmEvent==WM_HSCROLL);
							guiToNormal2(hWnd, tex2);
						}
						break;
				}
			}	// end WM_COMMAND
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

void normDlg2UpdateBgShifts(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgrTexture = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_NORMAL2_GR_NORMAL_MAP));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_GR_NORMAL_MAP), 0, 0, emgrTexture->bgShiftX, emgrTexture->bgShiftY);

	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);
	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);

	EM2Button * embLoad = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_LOAD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_LOAD), 0, 0, embLoad->bgShiftX, embLoad->bgShiftY);
	EM2Button * embUnload = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NORMAL2_UNLOAD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_UNLOAD), 0, 0, embUnload->bgShiftX, embUnload->bgShiftY);

	EM2Text * emtFilename = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_FILENAME));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_FILENAME), 0, 0, emtFilename->bgShiftX, emtFilename->bgShiftY);
	EM2Text * emtRes = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_RESOLUTION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_RESOLUTION), 0, 0, emtRes->bgShiftX, emtRes->bgShiftY);
	EM2Text * emtPower = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_TEXT_POWER));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_TEXT_POWER), 0, 0, emtPower->bgShiftX, emtPower->bgShiftY);

	EM2EditTrackBar * emetPower = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_NORMAL2_POWER));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_POWER), 0, 0, emetPower->bgShiftX, emetPower->bgShiftY);

	EM2CheckBox * emcInvX = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_X));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_X), 0, 0, emcInvX->bgShiftX, emcInvX->bgShiftY);
	EM2CheckBox * emcInvY = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_Y));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_Y), 0, 0, emcInvY->bgShiftX, emcInvY->bgShiftY);
}

//------------------------------------------------------------------------------------------------

void normDlg2SetPositionsOnStart(HWND hWnd)
{
	RECT tmprect, wrect,crect;
	GetWindowRect(hWnd, &wrect);
	GetClientRect(hWnd, &crect);
	tmprect.left = 0;		tmprect.top = 0;
	tmprect.right = 779 + wrect.right-wrect.left-crect.right;
	tmprect.bottom = 542 + wrect.bottom-wrect.top-crect.bottom;
	SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);

	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_TITLE), HWND_TOP, 250,11, 280, 17, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_OK),					HWND_TOP,			303,	505,		72,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_CANCEL),				HWND_TOP,			398,	505,		72,		22,			0);


	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_GR_NORMAL_MAP),		HWND_TOP,			18,		40,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_GR_SHADE_PREVIEW),	HWND_TOP,			398,	40,			360,	16,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_PREV_TEX),			HWND_TOP,			18,		74,			360,	360,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_PREV_SHADE),			HWND_TOP,			398,	74,			360,	360,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_FILENAME),			HWND_TOP,			18,		448,		270,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_RESOLUTION),			HWND_TOP,			288,	448,		90,		16,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_LOAD),				HWND_TOP,			18,		474,		72,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_UNLOAD),				HWND_TOP,			104,	474,		72,		22,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_TEXT_POWER),			HWND_TOP,			398,	448,		48,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_POWER),				HWND_TOP,			446,	445,		360-48,	22,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_Y),			HWND_TOP,			694,	480,		64,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_X),			HWND_TOP,			602,	480,		64,		10,			0);
}

//------------------------------------------------------------------------------------------------

bool makeNormalShadePreview(HWND hWnd, Texture * ntex, int px, int py, int d)
{
	CHECK(hWnd);
	CHECK(ntex);
	CHECK(ntex->isValid());

	EMPView * empvsrc = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_TEX));
	EMPView * empvdst = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_SHADE));
	CHECK(empvsrc);
	CHECK(empvdst);
	CHECK(empvsrc->byteBuff->imgBuf);
	CHECK(empvdst->byteBuff->imgBuf);

	float invx = ntex->normMod.invertX ? -1.0f : 1.0f;
	float invy = ntex->normMod.invertY ? -1.0f : 1.0f;
	int sx = empvsrc->byteBuff->width;
	int sy = empvsrc->byteBuff->height;
	float mx = empvsrc->mx;
	float my = empvsrc->my;
	int cx = empvdst->byteBuff->width;
	int cy = empvdst->byteBuff->height;
	float mycy = my*cy;
	float mxcx = mx*cx;
	float zoom = empvsrc->zoom;
	float perpower = 1.0f/ntex->normMod.power;


	for (int y=0; y<cx; y++)
	{
		int ty = (int)((y)/zoom + mycy);
		for (int x=0; x<cy; x++)
		{
			int tx = (int)(x/zoom + mxcx);
			if (ty<0 || ty>=sy  ||  tx<0 || tx>=sx)
				empvdst->byteBuff->imgBuf[y][x] = RGB(0,0,0);
			else
			{
				int dx = px-x;
				int dy = y-py;

				COLORREF csrc = empvsrc->byteBuff->imgBuf[ty][tx];
				float vx = (GetRValue(csrc)-128)/128.0f;
				float vy = (GetGValue(csrc)-128)/128.0f;
				float vz = (GetBValue(csrc)-128)/128.0f;
				Vector3d vecnormal = Vector3d(vx*invx, vy*invy, vz*perpower);
				vecnormal.normalize();

				Vector3d vecdir = Vector3d((float)dx, (float)dy, (float)d);
				vecdir.normalize();

				float shval = vecdir * vecnormal;

				unsigned char cc = (unsigned char)min(255, max(0, (int)(255*shval)));
				empvdst->byteBuff->imgBuf[y][x] = RGB(cc,cc,cc);
			}

		}
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool makeNormal2BytePreview(HWND hWnd, Texture * ntex)
{
	CHECK(hWnd);
	CHECK(ntex);
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL2_PREV_TEX));
	CHECK(empv);
	CHECK(ntex->isValid());
	int w = ntex->bmp.getWidth();
	int h = ntex->bmp.getHeight();
	CHECK(w>0  &&  h>0);

	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();
	if (empv->byteBuff->width!=w   ||   empv->byteBuff->height!=h)
		empv->byteBuff->allocBuffer(w,h);

	if (!ntex->bmp.isItFloat)
	{
		for (int j=0; j<h; j++)
		{
			for (int i=0; i<w; i++)
			{
				Color4 c;
				unsigned char cr = ntex->bmp.idata[(j*w+i)*4+0];
				unsigned char cg = ntex->bmp.idata[(j*w+i)*4+1];
				unsigned char cb = ntex->bmp.idata[(j*w+i)*4+2];
				empv->byteBuff->imgBuf[j][i] = RGB(cr, cg, cb);
			}
		}
	}
	else
		empv->byteBuff->clearBuffer();



	return true;
}

//------------------------------------------------------------------------------------------------

bool guiToNormal2(HWND hWnd, Texture * ntex)
{
	CHECK(ntex);
	CHECK(hWnd);

	EM2EditTrackBar * emetPower = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_NORMAL2_POWER));
	EM2CheckBox * emcInvX = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_X));
	EM2CheckBox * emcInvY = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL2_INVERT_Y));
	ntex->normMod.powerEV = emetPower->floatValue;
	ntex->normMod.power = pow(2.0f, ntex->normMod.powerEV);

	ntex->normMod.invertX = emcInvX->selected;
	ntex->normMod.invertY = emcInvY->selected;

	return true;
}

//------------------------------------------------------------------------------------------------

bool normal2InfoToGui(HWND hWnd, Texture * ntex)
{
	EM2Text * emtFilename = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_FILENAME));
	EM2Text * emtRes = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NORMAL2_RESOLUTION));
	if (ntex && ntex->filename)
		emtFilename->changeCaption(ntex->filename);
	else
		emtFilename->changeCaption("NO TEXTURE LOADED");
	char buf[256];
	sprintf_s(buf, 256, "%d x %d PX", ntex?ntex->bmp.getWidth():0, ntex?ntex->bmp.getHeight():0);
	emtRes->changeCaption(buf);
	if (ntex && ntex->filename)
		ShowWindow(emtRes->hwnd, SW_SHOW);
	else
		ShowWindow(emtRes->hwnd, SW_HIDE);
	return true;
}

//------------------------------------------------------------------------------------------------
