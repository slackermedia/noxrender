#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

int MltSeed::errCntNonVisib = 0;
int MltSeed::errCntPxBlack = 0;
int MltSeed::errCntSpecAndNoInt = 0;
int MltSeed::errCntNoIntNoMLay = 0;
int MltSeed::errCntCamSrcInd = 0;
int MltSeed::errCntPdf0 = 0;
int MltSeed::errCntPathBlack = 0;
int MltSeed::errCntPathInfNaN = 0;
int MltSeed::errCntRndLight = 0;

//--------------------------------------------------------------------------------------------------------

MltSeed::MltSeed(SceneStatic * scst, Camera * tcam)
{
	ss = scst;
	validPath = false;
	numTries = 0;
	numL = -1;
	numE = -1;
	rrprob = 0.5f;
	cam = tcam;
	useSun = false;
	useSky = false;
	weight = 0;
	connAtt = Color4(1,1,1);
	sample_time = 0.0f;
	for (int i=0; i<MLT_MAX_VERTICES; i++)
	{
		ePath[i].atten = Color4(1,1,1);
		lPath[i].atten = Color4(1,1,1);
	}

}

MltSeed::MltSeed()
{
	ss = NULL;
	cam = NULL;
	validPath = false;
	numTries = 0;
	numL = -1;
	numE = -1;
	rrprob = 0.5f;
	connAtt = Color4(1,1,1);
	for (int i=0; i<MLT_MAX_VERTICES; i++)
	{
		ePath[i].atten = Color4(1,1,1);
		lPath[i].atten = Color4(1,1,1);
	}
}
//--------------------------------------------------------------------------------------------------------

MltSeed::~MltSeed()
{
}

//--------------------------------------------------------------------------------------------------------

bool MltSeed::createSeed(float camx, float camy, int numOfTries)
{
	for (int i=0; i<numOfTries; i++)
	{
		numTries++;
		if (tryToCreate(camx, camy))
		{
			validPath = true;
			return true;
		}
	}

	return false;
}

//--------------------------------------------------------------------------------------------------------

bool MltSeed::tryToCreate(float cx, float cy)
{
	bool res = true;
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	CHECK(scene);
	if (!cam)
		cam = scene->getActiveCamera();
	CHECK(cam);
	

	Point3d eyeOrigin;
	Vector3d eyeDirection = cam->getDirection(cx,cy, eyeOrigin, 0);

	numL = 0;
	numE = 0;

	// start from camera
	ePath[0].inDir = ePath[0].outDir = ePath[0].normal = eyeDirection;
	ePath[0].pos = eyeOrigin;
	ePath[0].brdf = Color4(1,1,1);
	ePath[0].dirac = false;
	ePath[0].inCosine = ePath[0].outCosine = 1.0f;
	ePath[0].inRadiance = Color4(1,1,1);
	ePath[0].mat = NULL;
	ePath[0].mlay = NULL;
	ePath[0].pOut = 1.0f;
	ePath[0].tri = NULL;
	ePath[0].u = 0.0f;
	ePath[0].v = 0.0f;
	ePath[0].instanceID = -1;
	ePath[0].inst_mat.setIdentity();
	ePath[0].atten = Color4(1,1,1);

	// send recursives
	res = sendNextVertices(false);
	bool eyeOK = res;

	if (!ePath[numE].fetchRoughness())
		ePath[numE].rough = 1.0f;

	if (!res)
	{
		if (!ePath[numE].tri   ||   !ePath[numE].mat)
			return false;

		if (ePath[numE].mlay  &&  ePath[numE].rough==0)
		{
		}
		else
			return false;
	}

	float suncontr = 0;
	float skycontr = 0;
	float lightcontr = 0;
	Vector3d toSun;
	Vector3d toSky;
	bool useLight = false;
	float cs_pdf = 1.0f;

	// random emission type
	if (ss->useSunSky  &&  numE>0)
	{
		float psun = 0.0f;
		float psky = 1.0f;
		float peye = eyeOK ? 1.0f : 0.0f;
		peye = 1;
		if (ss->sunsky->sunOn)
			psun = 1;

		float a = rtr->getRandomFloatThread() * (psun+psky+peye);
		if (a<psun)
		{
			cs_pdf = psun / (psky+psun+peye);
			useSun = true;
		}
		else
			if (a<psun+psky)
			{
				cs_pdf = psky / (psky+psun+peye);
				useSky = true;
			}
			else
			{
				cs_pdf = peye / (psky+psun+peye);
				useLight = true;
			}
	}
	else
	{
		useLight = true;
		if (!eyeOK)
			return false;
	}

	if (useLight && !eyeOK)
		return false;

	// use SKY
	if (useSky)
	{
		float rdpdf;
		HitData hd;
		hd.in = ePath[numE].inDir;
		hd.normal_shade = ePath[numE].normal;
		ePath[numE].tri->evalTexUV(ePath[numE].u, ePath[numE].v, hd.tU, hd.tV);
		hd.normal_geom = (ePath[numE].inst_mat * ePath[numE].tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		ePath[numE].tri->getTangentSpaceSmooth(ePath[numE].inst_mat, ePath[numE].u, ePath[numE].v, hd.dir_U, hd.dir_V);
		hd.clearFlagInvalidRandom();
		Vector3d randDir = ePath[numE].mlay->randomNewDirection(hd, rdpdf);
		toSky = randDir;
		if (rdpdf > 0.001f   &&   !hd.isFlagInvalidRandom())
		{
			//// old way shadow
			Point3d cPoint = ePath[numE].pos + ePath[numE].normal * (ePath[numE].normal*randDir > 0 ? 0.0001f : -0.0001f);
			float u=0, v=0;
			int chosen = -1, instanceID=-1;
			float bigf = BIGFLOAT;

			Color4 att = Color4(1,1,1);
			Matrix4d instMatr;
			float d = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time ,cPoint, randDir, bigf, instMatr, instanceID, chosen, u, v, att, ss, true);
			if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d <= 0)
			{
				hd.out = randDir;
				Color4 skyrbrdf = ePath[numE].mlay->getBRDF(hd);
				Color4 skycolor = ss->sunsky->getColor(randDir);
				skyrbrdf *= skycolor;
				if (ePath[numE].mlay  &&  ePath[numE].rough>0)
					skyrbrdf *= fabs(ePath[numE].normal*randDir);
				skycontr = ( 0.299f * skyrbrdf.r  +  0.587f * skyrbrdf.g  +  0.114f * skyrbrdf.b ) / rdpdf;

				if (skycontr <= 0)
					return false;

				lPath[0].brdf = Color4(1,1,1);
				lPath[0].dirac = false;
				lPath[0].inCosine = 1;
				lPath[0].outCosine = 1;
				lPath[0].inDir = lPath[0].outDir = lPath[0].normal = toSky * -1;
				lPath[0].mat = NULL;
				lPath[0].mlay = NULL;
				lPath[0].pos = ePath[numE].pos + toSky * 1000000;
				lPath[0].tri = NULL;
				lPath[0].u = lPath[0].v = 0;
				lPath[0].rough = 1.0f;
				lPath[0].instanceID = -1;
				lPath[0].inst_mat.setIdentity();

				ePath[numE].outDir = toSky;
				ePath[numE].outCosine = fabs(toSky * ePath[numE].normal);

				lightTriID = -1;
				lightmeshID = -1;

				weight = evalWeight();
				weight *= 1.0f/cs_pdf;
				return (weight > 0.0f);
				return true;
			}
		}
		return false;
	}

	// use SUN
	if (useSun)
	{
		HitData hd;
		hd.in = ePath[numE].inDir;
		hd.normal_shade = ePath[numE].normal;
		ePath[numE].tri->evalTexUV(ePath[numE].u,ePath[numE].v, hd.tU, hd.tV);
		hd.normal_geom = (ePath[numE].inst_mat.getMatrixForNormals() * ePath[numE].tri->normal_geom).getNormalized();
		ePath[numE].tri->getTangentSpaceSmooth(ePath[numE].inst_mat, ePath[numE].u, ePath[numE].v, hd.dir_U, hd.dir_V);
		hd.adjustNormal();
		float rdpdf;
		if (ePath[numE].rough == 0)
		{
			hd.clearFlagInvalidRandom();
			toSun = ePath[numE].mlay->randomNewDirection(hd, rdpdf);
			if (toSun * ss->sunsky->getSunDirection() < ss->sunsky->maxCosSunSize    ||   hd.isFlagInvalidRandom())
				return false;
		}
		else
			toSun = ss->sunsky->getRandomPerturbedSunDirection();
		hd.out = toSun;
		float rspdf = ePath[numE].mlay->getProbability(hd);
		Color4 sunrbrdf = ePath[numE].mlay->getBRDF(hd);

		if (rspdf > 0.001f  &&  !sunrbrdf.isBlack())
		{
			// old simple shadow test
			Point3d cPoint = ePath[numE].pos + ePath[numE].normal * (ePath[numE].normal*toSun > 0 ? 0.0001f : -0.0001f);
			float u=0, v=0;
			int chosen = -1, inst_id;
			float bigf = BIGFLOAT;
			Matrix4d inst_matr;
			Color4 att;
			float d = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time, cPoint, toSun, bigf, inst_matr, inst_id, chosen, u, v, att, ss, true);
			if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d <= 0)
			{
				Color4 suncolor = ss->sunsky->getSunColor();
				sunrbrdf *= suncolor;
				suncontr = ( 0.299f * sunrbrdf.r  +  0.587f * sunrbrdf.g  +  0.114f * sunrbrdf.b ) ;
				if (ePath[numE].rough == 0)
					suncontr *= (1.0f/rspdf);
				else
					suncontr *= ss->sunsky->getSunSizeSteradians() * fabs(ePath[numE].normal*toSun);

				if (suncontr<=0)
					return false;

				lPath[0].brdf = Color4(1,1,1);
				lPath[0].dirac = false;
				lPath[0].inCosine = 1;
				lPath[0].outCosine = 1;
				lPath[0].inDir = lPath[0].outDir = lPath[0].normal = toSun * -1;
				lPath[0].mat = NULL;
				lPath[0].mlay = NULL;
				lPath[0].pos = ePath[numE].pos + toSun * 1000000;
				lPath[0].tri = NULL;
				lPath[0].u = lPath[0].v = 0;
				lPath[0].atten = Color4(1,1,1);
				lPath[0].rough = 1.0f;


				ePath[numE].outDir = toSun;
				ePath[numE].outCosine = fabs(toSun * ePath[numE].normal);

				lightTriID = -1;
				lightmeshID = -1;

				weight = evalWeight();
				weight *= 1.0f/cs_pdf;
				return (weight > 0.0f);
				return true;
			}
		}
		return false;
	}

	// mesh EMITTER
	if (numE > 0   &&   ePath[numE].mlay   &&   ePath[numE].mlay->type==MatLayer::TYPE_EMITTER)
	{
		lPath[0].normal		= ePath[numE].normal;
		lPath[0].inDir		= ePath[numE].normal;
		lPath[0].outDir		= ePath[numE].inDir;
		lPath[0].mat		= ePath[numE].mat;
		lPath[0].pos		= ePath[numE].pos;
		lPath[0].tri		= ePath[numE].tri;
		lPath[0].u			= ePath[numE].u;
		lPath[0].v			= ePath[numE].v;
		lPath[0].rough		= 1.0f;
		lPath[0].instanceID	= ePath[numE].instanceID;
		lPath[0].inst_mat	= ePath[numE].inst_mat;
		
		float u = lPath[0].u;
		float v = lPath[0].v;
		float tu = lPath[0].tri->UV1.x * (1-u-v)  +  lPath[0].tri->UV2.x * u  +  lPath[0].tri->UV3.x * v;
		float tv = lPath[0].tri->UV1.y * (1-u-v)  +  lPath[0].tri->UV2.y * u  +  lPath[0].tri->UV3.y * v;
		HitData hd;
		hd.in = lPath[0].inDir;
		hd.out = lPath[0].outDir;
		hd.normal_shade = lPath[0].normal;
		hd.tU = tu;
		hd.tV = tv;
		float pemms;
		lPath[0].dirac		= false;
		lPath[0].inCosine	= 1.0f;
		lPath[0].outCosine	= fabs(lPath[0].normal * lPath[0].outDir);
		lPath[0].mlay		= lPath[0].mat->getRandomEmissionLayer(tu, tv, pemms);
		CHECK(lPath[0].mlay);
		lPath[0].brdf		= lPath[0].mlay->gColor * lPath[0].mlay->power;
		lPath[0].pOut		= lPath[0].mlay->getProbabilityLambert(hd);

		numE--;
		bool joinOK = joinPaths(false);
		if (!joinOK)
			return false;
		weight = evalWeight();
		weight *= 1.0f/cs_pdf;
		if (weight==0)
			return false;
		return true;
	}
	
	float nlpdf, ldpdf, u,v;
	Matrix4d instMat;
	int instID = -1;
	bool chng = randomNewLightPoint(&lPath[0].tri,  &lPath[0].mat, &lPath[0].mlay, instMat, instID, u, v, nlpdf, lightTriID);
	if (!chng)
	{
		errCntRndLight++;
		return false;
	}

	lPath[0].dirac = false;
	lPath[0].instanceID = instID;
	lPath[0].inst_mat = instMat;
	lPath[0].pos = instMat * lPath[0].tri->getPointFromUV(u, v);
	lPath[0].normal = (lPath[0].inst_mat.getMatrixForNormals() * lPath[0].tri->evalNormal(lPath[0].u, lPath[0].v)).getNormalized();
	lPath[0].inDir  = lPath[0].normal;
	lPath[0].outDir = lPath[0].normal;
	lPath[0].inCosine  = 1;
	lPath[0].outCosine = 1;
	lPath[0].u = u;
	lPath[0].v = v;
	lPath[0].inRadiance = lPath[0].mlay->gColor * lPath[0].mlay->power;
	lPath[0].brdf = lPath[0].mlay->gColor * lPath[0].mlay->power;
	lPath[0].rough = 1.0f;

	HitData hd;
	hd.in = lPath[0].normal;
	hd.normal_geom = hd.normal_shade = lPath[0].normal;
	lPath[0].tri->evalTexUV(u,v, hd.tU, hd.tV);

	hd.out = lPath[0].mlay->randomNewDirectionLambert2(hd, ldpdf);
	lPath[0].outDir = hd.out;
	lPath[0].outCosine = fabs(lPath[0].normal * hd.out);
	lPath[0].pOut = lPath[0].mlay->getProbabilityLambert(hd);

	// roussian roulette
	unsigned int r1;
	while(rand_s(&r1));
	float rf1 = (float)((double)r1/(double)UINT_MAX);
	if (rf1 <= rrprob)
	{
		// send next 
		res = sendNextVertices(true);
		if (!res)
			return false;
	}

	res = joinPaths();	
	if (!res)
		return false;

	if (numE==0)
	{
		if (numL==0)
			return false;
		Vector3d camDir = lPath[numL].pos - ePath[0].pos;
		camDir.normalize();

		float cpx, cpy;
		if (!cam->getCoordsFromDir(camDir, ePath[0].pos, cpx, cpy))
			return false;
		if (cpx < 0 ||  cpx >= cam->width  ||  cpy<0  ||  cpy>=cam->height)
			return false;
	}

	// check for light in mirror/glass etc
	bool gotNonSpec = false;
	for (int i=1; i<=numE; i++)
	{
		if (!ePath[i].fetchRoughness())
			ePath[i].rough = 1.0f;
		if (ePath[i].rough>0)
			gotNonSpec = true;
	}
	for (int i=1; i<=numL; i++)
	{
		if (!lPath[i].fetchRoughness())
			lPath[i].rough = 1.0f;
		if (lPath[i].rough>0)
			gotNonSpec = true;
	}
	if (!gotNonSpec)
		return false;

	weight = evalWeight();
	weight *= 1.0f/cs_pdf;

	if (weight <= 0.0f)
		res = false;
	return res;
}

//--------------------------------------------------------------------------------------------------------

bool MltSeed::sendNextVertices(bool isItLight)
{
	// everything must be filled in current vertex before calling method
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	CHECK(scene);

	while (1)
	{
		MLTVertex * curPath;
		MLTVertex * lastPath;
		if (isItLight)
		{
			lastPath = &(lPath[numL]);
			curPath  = &(lPath[numL+1]);
		}
		else
		{
			lastPath = &(ePath[numE]);
			curPath  = &(ePath[numE+1]);
		}

		Point3d cPoint;
		if (lastPath->outDir * lastPath->normal > 0)
			cPoint = lastPath->pos + lastPath->normal * 0.0001f;
		else
			cPoint = lastPath->pos + lastPath->normal * -0.0001f;
		
		// send a ray
		float u=0, v=0;
		int chosen = -1;
		float bigf = BIGFLOAT;
		Color4 att;
		int instanceID = -1;
		Matrix4d instMatr;
		float d = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time ,cPoint, lastPath->outDir, bigf, instMatr, instanceID, chosen, u, v, att, ss, true);
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.0001f)
		{	// nothing intersected!!!
			if (!isItLight  &&  numE==0)
			{
				errCntPxBlack++;
				return false;
			}
			if (lastPath->mlay)
			{
				return false;
			}
			else
			{
				errCntNoIntNoMLay++;
				return false;				// no mlay? eye point (should be caught earlier) or error
			}
		}


		// fill vertex
		Triangle * tri = &(scene->triangles[chosen]);
		CHECK(tri);
		float tu, tv;
		tri->evalTexUV(u,v,tu,tv);
		curPath->tri			= tri;
		curPath->inst_mat		= instMatr;
		curPath->instanceID		= instanceID;
		curPath->pos			= curPath->inst_mat * tri->getPointFromUV(u,v);
		curPath->inDir			= lastPath->outDir * -1;
		curPath->normal			= (curPath->inst_mat.getMatrixForNormals() * tri->evalNormal(u,v)).getNormalized();
		curPath->inCosine		= fabs(curPath->inDir * curPath->normal);
		curPath->dirac			= false;
		curPath->u				= u;
		curPath->v				= v;
		curPath->outCosine		= 1;				
		curPath->outDir			= curPath->normal;	
		curPath->mat			= scene->mats[scene->instances[instanceID].materials[tri->matInInst]];
		curPath->mlay			= NULL;
		curPath->atten			= att;
		curPath->rough			= 1.0f;

		// random layer
		float prlayer;
		if (isItLight)
		{
			if (curPath->mat->hasShade)
				curPath->mlay = curPath->mat->getRandomShadeLayer(tu, tv, prlayer);
		}
		else
		{
			if (curPath->mat->hasEmitters)
				curPath->mlay = curPath->mat->getRandomEmissionLayer(tu, tv, prlayer);
			else
				if (curPath->mat->hasShade)
					curPath->mlay = curPath->mat->getRandomShadeLayer(tu, tv, prlayer);
		}       
		CHECK(curPath->mlay);
		curPath->fetchRoughness();

		// if emitting layer find lightmesh
		if (curPath->mlay->type==MatLayer::TYPE_EMITTER)
		{	
			if (isItLight)
				MessageBox(0, "impossible is possible", "", 0);
			if (numE < 1)
			{
				errCntCamSrcInd++;
				return false;
			}
			numE++;
			lightTriID = chosen;

			int ilm = -1;
			for (int i=0; i<ss->lightMeshes->objCount; i++)
			{
				LightMesh * tlm = (*(ss->lightMeshes))[i];
				for (int j=0; j<tlm->tris.objCount; j++)
					if (chosen == tlm->tris[j])
						ilm = i;
			}
			lightmeshID = ilm;

			return true;
		}

		// russian roulette
		float rrmult = 1.0f;
		float pcont = 1.0f;
		if (curPath->mlay   &&   curPath->rough>0)
		{
			pcont = rrprob;
			unsigned int r1;
			while(rand_s(&r1));
			float rf1 = (float)((double)r1/(double)UINT_MAX);
			if (rf1 > pcont)
			{
				if (isItLight)
					numL++;
				else
					numE++;
				return true;
			}
			rrmult = 1.0f / pcont;
		}

		// fill hd
		HitData hd;
		hd.in = curPath->inDir;
		hd.normal_shade = curPath->normal;
		tri->evalTexUV(u,v, hd.tU, hd.tV);
		hd.normal_geom = (curPath->inst_mat.getMatrixForNormals() * tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		tri->getTangentSpaceSmooth(curPath->inst_mat, curPath->u, curPath->v, hd.dir_U, hd.dir_V);

		{
			// random new direction
			float pdir;
			hd.clearFlagInvalidRandom();
			curPath->outDir = curPath->mlay->randomNewDirection(hd, pdir);
			int watchdog = 250;
			if (curPath->mat->opacity<1  ||  (curPath->mlay->transmOn  &&  curPath->mlay->fakeGlass))
			{
				float cosIN = curPath->inDir*curPath->normal;
				while (cosIN * (curPath->outDir*curPath->normal) < 0   ||   hd.isFlagInvalidRandom())
				{
					if ((watchdog--)<0)
						return false;
					hd.clearFlagInvalidRandom();
					curPath->outDir = curPath->mlay->randomNewDirection(hd, pdir);
				}
			}
			curPath->outCosine = curPath->outDir * curPath->normal;
			hd.out = curPath->outDir;
			curPath->pOut = curPath->mlay->getProbability(hd) * pcont;	// include rr 
		}

		if (isItLight)
		{
			if (++numL >= MLT_MAX_VERTICES-1)
				return true;
		}
		else
		{
			if (++numE >= MLT_MAX_VERTICES-1)
				return true;
		}

	}

	return true;
}

//--------------------------------------------------------------------------------------------------------

bool MltSeed::joinPaths(bool withVisibTest)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);

	Vector3d dirLE = ePath[numE].pos - lPath[numL].pos;
	float dist = dirLE.normalize_return_old_length();
	dist -= 0.0002f;
	Vector3d dirEL = dirLE * -1;
	ePath[numE].outDir = dirEL;
	lPath[numL].outDir = dirLE;

	if (withVisibTest)
	{

		Scene * scPtr = Raytracer::getInstance()->curScenePtr;
		int maxtri = scPtr->triangles.objCount - 1;

		Vector3d dirEL = lPath[numL].pos - ePath[numE].pos;
		float ldist = dirEL.normalize_return_old_length();
		ldist -= 0.0002f;
		Point3d cPoint = ePath[numE].pos + ePath[numE].normal * ((ePath[numE].normal*dirEL>0) ? 0.0001f : -0.0001f);

		Color4 attToLight = Color4(1,1,1);
		bool shadow = true;
		do 
		{
			int chosen = -1, instID=-1;
			Matrix4d inst_mat;
			float su,sv;
			float sd = ss->bvh.intersectForTrisNonSSE(sample_time, cPoint, dirEL, inst_mat, ldist, instID, chosen, su,sv);
			if (chosen < 0   ||   chosen > maxtri    ||   sd < 0)
				break;
			else
			{	// hit sth - check material
				Triangle * ftri = &(scPtr->triangles[chosen]);
				MaterialNox * fmat = sc->mats[sc->instances[instID].materials[ftri->matInInst]];
				HitData fhd;
				ftri->evalTexUV(su,sv, fhd.tU, fhd.tV);
				float opac = fmat->getOpacity(fhd.tU, fhd.tV);
				if (!fmat->hasFakeGlass   &&   opac>=1.0f)
					return false;	// shadow

				fhd.clearFlagsAll();
				fhd.in = dirEL * -1;
				fhd.out = dirEL;
				fhd.normal_shade = (inst_mat.getMatrixForNormals() * ftri->evalNormal(su, sv)).getNormalized();
				fhd.normal_geom = (inst_mat.getMatrixForNormals() * ftri->normal_geom).getNormalized();
				fhd.adjustNormal();	// done
				ftri->getTangentSpaceSmooth(inst_mat, su, sv, fhd.dir_U, fhd.dir_V);

				float opac1;
				Color4 att = fmat->evalAllLayersFakeGlassAndOpacityAttenuation(fhd, opac1);
				if (att.isBlack())
					return false;
				attToLight *= att;

				cPoint = inst_mat * ftri->getPointFromUV(su,sv);
				dirEL = lPath[numL].pos - cPoint;
				cPoint = cPoint + fhd.normal_shade * ((dirEL*fhd.normal_shade>0) ? 0.0001f : -0.0001f);
				dirEL = lPath[numL].pos - cPoint;
				ldist = dirEL.normalize_return_old_length();
				ldist -= 0.0001f;
			}
		} while (shadow);

		connAtt = attToLight;
	}


	HitData hdL, hdE;
	Triangle * tri;
	float u,v, pdf;
	Color4 brdf;

	tri = lPath[numL].tri;
	u = lPath[numL].u;
	v = lPath[numL].v;
	CHECK(tri);
	hdL.normal_shade = lPath[numL].normal;
	hdL.in = lPath[numL].inDir;
	hdL.out = dirLE;
	if (tri)
	{
		hdL.normal_geom = (lPath[numL].inst_mat.getMatrixForNormals() * tri->normal_geom).getNormalized();
		hdL.adjustNormal();	
		tri->evalTexUV(u,v, hdL.tU, hdL.tV);
		tri->getTangentSpaceSmooth(lPath[numL].inst_mat, u, v, hdL.dir_U, hdL.dir_V);
	}
	else
		hdL.tU = hdL.tV = 0;
	hdL.clearFlagsAll();

	pdf = lPath[numL].mlay->getProbability(hdL);
	if (pdf < 0.0001f)
	{
		errCntPdf0++;
		return false;
	}
	brdf = lPath[numL].mlay->getBRDF(hdL);
	if (brdf.isBlack())
	{
		errCntPathBlack++;
		return false;
	}
	if (brdf.isInf()   ||   brdf.isNaN())
	{
		errCntPathInfNaN++;
		return false;
	}

	// opacity and fake glass only reflects at connection
	if ((lPath[numL].mlay->transmOn  &&   lPath[numL].mlay->fakeGlass)  ||  lPath[numL].mat->opacity<1.0f)
		return false;
	if ((ePath[numE].mlay->transmOn  &&   ePath[numE].mlay->fakeGlass)  ||  ePath[numE].mat->opacity<1.0f)
		return false;

	if (numE>0)
	{
		tri = ePath[numE].tri;
		u = ePath[numE].u;
		v = ePath[numE].v;
		CHECK(tri);
		hdE.normal_shade = ePath[numE].normal;
		hdE.in = ePath[numE].inDir;
		hdE.out = dirEL;
		if (tri)
		{
			hdE.normal_geom = (ePath[numE].inst_mat.getMatrixForNormals() * tri->normal_geom).getNormalized();
			hdE.adjustNormal();	// done
			tri->evalTexUV(u,v, hdE.tU, hdE.tV);
			tri->getTangentSpaceSmooth(ePath[numE].inst_mat, u, v, hdE.dir_U, hdE.dir_V);
		}
		else
			hdE.tU = hdE.tV = 0;
		hdE.clearFlagsAll();
		//hdE.flags = 0;

		pdf = ePath[numE].mlay->getProbability(hdE);
		if (pdf < 0.0001f)
		{
			errCntPdf0++;
			return false;
		}
		brdf = ePath[numE].mlay->getBRDF(hdE);
		if (brdf.isBlack())
		{
			errCntPathBlack++;
			return false;
		}
		if (brdf.isInf()   ||   brdf.isNaN())
		{
			errCntPathInfNaN++;
			return false;
		}
	}

	return true;
}

//--------------------------------------------------------------------------------------------------------

bool MltSeed::randomNewLightPoint(Triangle ** tri, MaterialNox ** mat, MatLayer ** mlay, Matrix4d &instMat, int &instID, float &u, float &v, float &pdf, int &triID)
{
	if (!tri  ||  !mat  ||  !mlay)
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;


	int l,ll;
	float pdf_choose_lightmesh, pdf_choose_light_triangle, pdf_choose_layer;
	ll = ss->randomLightMesh(pdf_choose_lightmesh);	
	LightMesh *lm = (*ss->lightMeshes)[ll];											
	if (ll >= 0 && lm)
		l = lm->randomTriangle(pdf_choose_light_triangle);
	else
		return false;

	MaterialNox * cmat = NULL;	
	MatLayer * cmlay = NULL;
	Triangle * ltri = &(sc->triangles[l]);
	if (!ltri)
		return false;
	MeshInstance * meshInst = &(sc->instances[lm->instNum]);
	Matrix4d inst_mat = meshInst->matrix_transform;

	float cu,cv;	
	ltri->randomPoint(cu, cv, lPath[0].pos);
	float tu,tv;
	ltri->evalTexUV(cu, cv, tu, tv);

	cmat = sc->mats[sc->instances[lm->instNum].materials[ltri->matInInst]];
	if (!cmat)
		return false;
	cmlay = cmat->getRandomEmissionLayer(tu, tv, pdf_choose_layer);	
	if (!cmlay)
		return false;

	*tri = ltri;
	*mat = cmat;
	*mlay = cmlay;
	u = cu;
	v = cv;
	triID = l;
	lightmeshID = ll;
	instMat = inst_mat;
	instID = lm->instNum;

	return true;
}

//--------------------------------------------------------------------------------------------------------

float MltSeed::evalSunSkyWeight()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	// check for light in mirror/glass etc
	if (Metropolis::discardDirect)
	{
		bool gotSpec = false;
		bool gotNonSpec = false;
		bool isDirect = true;
		for (int i=1; i<=numL; i++)
		{
			float r = lPath[i].rough;
			if (r>0  &&  (gotNonSpec  ||  gotSpec))
				isDirect = false;
			if (r>0)
				gotNonSpec = true;
			else
				gotSpec = true;
		}
		for (int i=numE; i>0; i--)
		{
			float r = ePath[i].rough;
			if (r>0  &&  (gotNonSpec  ||  gotSpec))
				isDirect = false;
			if (r>0)
				gotNonSpec = true;
			else
				gotSpec = true;
		}
		if (!gotNonSpec)
			return 0.0f;
		if (isDirect)
			return 0.0f;
	}
	else
	{
		bool gotNonSpec = false;
		for (int i=1; i<=numE; i++)
		{
			float r = ePath[i].rough;
			if (r>0)
				gotNonSpec = true;
		}
		for (int i=1; i<=numL; i++)
		{
			float r = lPath[i].rough;
			if (r>0)
				gotNonSpec = true;
		}
		if (!gotNonSpec)
			return 0.0f;
	}

	if (!ss->useSunSky)
		return 0.0f;

	Color4 gbrdf;
	if (useSun)
	{
		HitData hd;
		hd.in = ePath[numE].inDir;
		hd.out = ePath[numE].outDir;
		hd.normal_shade = ePath[numE].normal;
		ePath[numE].tri->evalTexUV(ePath[numE].u, ePath[numE].v, hd.tU, hd.tV);
		hd.normal_geom = (ePath[numE].inst_mat.getMatrixForNormals() * ePath[numE].tri->normal_geom).getNormalized();
		hd.adjustNormal();
		ePath[numE].tri->getTangentSpaceSmooth(ePath[numE].inst_mat, ePath[numE].u, ePath[numE].v, hd.dir_U, hd.dir_V);

		if (ePath[numE].rough > 0)
			gbrdf = ss->sunsky->getSunColor() * ss->sunsky->getSunSizeSteradians();
		else
		{
			float pdf = ePath[numE].mlay->getProbability(hd);
			if (pdf < 0.001f)
				return 0;
			gbrdf = ss->sunsky->getSunColor() *(1.0f/pdf);
		}
	}
	else
		gbrdf = ss->sunsky->getColor(ePath[numE].outDir);

	bool dispersionUsed = false;

	for (int i=1; i<=numE; i++)
	{
		HitData hd;
		hd.in = ePath[i].inDir;
		hd.out = ePath[i].outDir;
		hd.normal_shade = ePath[i].normal;
		hd.lastDist = (ePath[i].pos-ePath[i-1].pos).length();
		ePath[i].tri->evalTexUV(ePath[i].u, ePath[i].v,   hd.tU, hd.tV);
		hd.normal_geom = (ePath[i].inst_mat.getMatrixForNormals() * ePath[i].tri->normal_geom).getNormalized();
		hd.adjustNormal();
		ePath[i].tri->getTangentSpaceSmooth(ePath[i].inst_mat, ePath[i].u, ePath[i].v, hd.dir_U, hd.dir_V);

		Color4 rbrdf = ePath[i].mlay->getBRDF(hd);
		float rpdf = ePath[i].mlay->getProbability(hd);

		if (ePath[i].mlay->transmOn   &&   ePath[i].mlay->dispersionOn  &&   ePath[i].mlay->dispersionValue!=0.0f   &&   !ePath[i].mlay->fakeGlass   &&   ePath[i].rough<0.0001f)
		{
			if ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) < 0.0f)
				dispersionUsed = true;
		}

		if (rpdf < 0.0001f)
			return 0;

		if (useSun  &&  i==numE)
			rpdf = 1;

		float rough = ePath[i].rough;
		if (rough > 0)
			rpdf *= 0.5f;	// russian roulette

		rbrdf *= ePath[i].atten;
		gbrdf *= rbrdf * (1.0f/rpdf);

		if (rough > 0)
			gbrdf *= fabs(ePath[i].normal*ePath[i].outDir);	
	}

	if (dispersionUsed)
		gbrdf *= 4.0f;

	gbrdf *= connAtt;
	if (gbrdf.isInf()  ||  gbrdf.isNaN())
		return 0;
	if (gbrdf.isBlack())
		return 0;

	float R = ( 0.299f * gbrdf.r  +  0.587f * gbrdf.g  +  0.114f * gbrdf.b );
	return R;
}

//--------------------------------------------------------------------------------------------------------

float MltSeed::evalWeight()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	if (lPath[0].tri == NULL)
		return evalSunSkyWeight();

	int nL, nE, lTri;
	MLTVertex * ePtr;
	MLTVertex * lPtr;

	lTri = lightTriID;
	ePtr = ePath;
	lPtr = lPath;
	nL = numL;
	nE = numE;

	int nA = nL+nE+2;

	PMLTVertex mPtr[MLT_MAX_VERTICES*2];
	float G[MLT_MAX_VERTICES*2];
	float pLE[MLT_MAX_VERTICES*2];
	float pEL[MLT_MAX_VERTICES*2];
	Color4 brdf[MLT_MAX_VERTICES*2];
	double pST[MLT_MAX_VERTICES*2];
	float weight[MLT_MAX_VERTICES*2];

	// check for light in mirror/glass etc
	if (Metropolis::discardDirect)
	{
		bool gotSpec = false;
		bool gotNonSpec = false;
		bool isDirect = true;
		for (int i=1; i<=numL; i++)
		{
			float r = lPath[i].rough;
			if (r>0  &&  (gotNonSpec  ||  gotSpec))
				isDirect = false;
			if (r>0)
				gotNonSpec = true;
			else
				gotSpec = true;
		}
		for (int i=numE; i>0; i--)
		{
			float r = ePath[i].rough;
			if (r>0  &&  (gotNonSpec  ||  gotSpec))
				isDirect = false;
			if (r>0)
				gotNonSpec = true;
			else
				gotSpec = true;
		}
		if (!gotNonSpec)
			return 0.0f;
		if (isDirect)
			return 0.0f;
	}
	else
	{
		bool gotNonSpec = false;
		for (int i=1; i<=numE; i++)
		{
			if (ePath[i].rough>0)
				gotNonSpec = true;
		}
		for (int i=1; i<=numL; i++)
		{
			if (lPath[i].rough>0)
				gotNonSpec = true;
		}
		if (!gotNonSpec)
			return 0.0f;
	}

	int t=0;
	for (int i=0; i<=nL; i++)
		mPtr[t++] = &(lPtr[i]);
	for (int i=nE; i>=0; i--)
		mPtr[t++] = &(ePtr[i]);

	// join directions 
	Point3d p1, p2;
	p1 = ePtr[nE].pos;
	p2 = lPtr[nL].pos;
	Vector3d dirEL = p2-p1;
	dirEL.normalize();
	Vector3d dirLE = dirEL * -1;
	ePtr[nE].outDir = dirEL;
	lPtr[nL].outDir = dirLE;
	ePtr[nE].outCosine = ePtr[nE].outDir * ePtr[nE].normal;
	lPtr[nL].outCosine = lPtr[nL].outDir * lPtr[nL].normal;

	float pLight = ss->evalPDF(lTri);

	// eval G
	for (int i=0; i<nA-1; i++)
	{
		Vector3d v1 = mPtr[i+1]->pos - mPtr[i]->pos;
		float d2 = v1.lengthSqr();
		if (d2 < 0.00000001f)		
			return 0;
		float k1,k2;
		if (i<=nL)
			k1 = mPtr[i]->outDir  * mPtr[i]->normal;
		else
			k1 = mPtr[i]->inDir  * mPtr[i]->normal;
		if (i<nL)
			k2 = mPtr[i+1]->inDir * mPtr[i+1]->normal;
		else
			k2 = mPtr[i+1]->outDir * mPtr[i+1]->normal;
		if (d2 > 0.00000001f)
			G[i] = fabs(k1*k2)/d2;
		else
			return 0;
	}

	bool dispersionUsed = false;

	// eval pLE & pEL & brdf
	for (int i=0; i<nA-1; i++)
	{
		HitData hd;
		float uu = mPtr[i]->u;
		float vv = mPtr[i]->v;
		if (i<=nL)
		{
			hd.in  = mPtr[i]->inDir;
			hd.out = mPtr[i]->outDir;
		}
		else
		{
			hd.in  = mPtr[i]->outDir;
			hd.out = mPtr[i]->inDir;
		}

		hd.normal_shade = mPtr[i]->normal;
		mPtr[i]->tri->evalTexUV(uu, vv, hd.tU, hd.tV);
		hd.clearFlagsAll();
		hd.lastDist = 1;
		hd.normal_geom = (mPtr[i]->inst_mat.getMatrixForNormals() * mPtr[i]->tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		mPtr[i]->tri->getTangentSpaceSmooth(mPtr[i]->inst_mat, mPtr[i]->u, mPtr[i]->v, hd.dir_U, hd.dir_V);
		pLE[i] = 0.0f;
		pEL[i] = 0.0f;
		brdf[i] = Color4(1,1,1);

		if (mPtr[i]->mlay)
		{
			float tcos = fabs(hd.out*hd.normal_shade);
			if (tcos > 0)
			{
				pLE[i] = mPtr[i]->mlay->getProbability(hd);
				if (mPtr[i]->rough > 0)
					pLE[i] *= 0.5f;		// russian roulette
			}
			else
				pLE[i] = 0;
			brdf[i] = mPtr[i]->mlay->getBRDF(hd);		// L->x->E

			Vector3d tvec = hd.in;	// swap dirs
			hd.in = hd.out;
			hd.out = tvec;

			tcos = fabs(hd.out*hd.normal_shade);
			if (tcos > 0)
			{
				pEL[i] = mPtr[i]->mlay->getProbability(hd);
				if (mPtr[i]->rough > 0)
					pEL[i] *= 0.5f;		// russian roulette
			}
			else
				pEL[i] = 0;
			
			if (mPtr[i]->mlay->dispersionOn   &&   mPtr[i]->mlay->dispersionValue!=0   &&   mPtr[i]->mlay->transmOn   &&   mPtr[i]->rough<0.0001f   &&   !mPtr[i]->mlay->fakeGlass)
			{
				if ((mPtr[i]->inDir*mPtr[i]->normal) * (mPtr[i]->outDir*mPtr[i]->normal)<0)
					dispersionUsed = true;
			}

		}
	}

	float tlu, tlv;
	mPtr[0]->tri->evalTexUV(mPtr[0]->u, mPtr[0]->v,  tlu, tlv);
	brdf[0] = mPtr[0]->mlay->getLightColor(tlu, tlv) * mPtr[0]->mlay->getIlluminance(ss, lTri);
	pEL[0] = 1;
	pEL[nA-1] = 1;
	pLE[nA-1] = 1;

	// eval probabilities for whole paths
	for (int i=0; i<nA-1; i++)
	{
		pST[i] = pLight; 
		pST[i] *= 0.5f;	
		if (mPtr[i]->mlay  &&  mPtr[i]->rough==0)
			pST[i] = 0.0f;
		if (mPtr[i+1]->mlay  &&  mPtr[i+1]->rough==0)
			pST[i] = 0.0f;

		for (int j=0; j<i; j++)
			pST[i] *= pLE[j] / fabs(j<=nL?mPtr[j]->outCosine:mPtr[j]->inCosine) * G[j];
		for (int j=nA-2; j>i+1; j--)
			pST[i] *= pEL[j] / fabs(j<=nL?mPtr[j]->inCosine:mPtr[j]->outCosine) * G[j-1];
	}
	// and implicit E->...->L
	double implicitPdf = 1.0f;
	for (int j=nA-2; j>0; j--)
		implicitPdf *= pEL[j] / fabs(j<=nL?mPtr[j]->inCosine:mPtr[j]->outCosine) * G[j-1];

	double sumPdf = 0;
	for (int i=0; i<nA-2; i++)
		sumPdf += pST[i]*pST[i];
	sumPdf += implicitPdf*implicitPdf;

	if (sumPdf <= 0.0f)
		return 0.0f;
	
	// now eval weights - power heuristic
	for (int i=0; i<nA-2; i++)
		weight[i] = (float)(pST[i]*pST[i] / sumPdf);

	Color4 cBrdf = Color4(0,0,0);
	for (int i=0; i<nA-2; i++)
	{
		Color4 pBrdf = Color4(1,1,1) * (1.0f/pLight);
		for (int j=0; j<i; j++)
		{
			if (pLE[j] > 0.00001f)
			{
				if (mPtr[j]->mlay  &&  (mPtr[j]->rough > 0  ||  mPtr[j]->mlay->type==MatLayer::TYPE_EMITTER))
					pBrdf *= brdf[j] * fabs(j<=nL?mPtr[j]->outCosine:mPtr[j]->inCosine) * (1.0f/pLE[j]);
				else
					pBrdf *= brdf[j] * (1.0f/pLE[j]);
			}
			else
				pBrdf = Color4(0,0,0);
		}
		
		for (int j=nA-2; j>i+1; j--)
		{
			if (pEL[j] > 0.00001f)
			{
				if (mPtr[j]->mlay  &&  (mPtr[j]->rough > 0  ||  mPtr[j]->mlay->type==MatLayer::TYPE_EMITTER))
					pBrdf *= brdf[j] * fabs(j<=nL?mPtr[j]->inCosine:mPtr[j]->outCosine) * (1.0f/pEL[j]);
				else
					pBrdf *= brdf[j] * (1.0f/pEL[j]);
			}
			else
				pBrdf = Color4(0,0,0);
		}
		
		pBrdf *= brdf[i] * brdf[i+1] * G[i];
		pBrdf *= 2;		// russian roulette
		cBrdf += pBrdf * weight[i];
	}

	// add implicit path contribution
	Color4 implBrdf = brdf[0];
	for (int i=nA-2; i>0; i--)
	{
		if (mPtr[i]->mlay  &&  (mPtr[i]->rough > 0  ||  mPtr[i]->mlay->type==MatLayer::TYPE_EMITTER))
			implBrdf *= brdf[i] * fabs(i<=nL?mPtr[i]->inCosine:mPtr[i]->outCosine) * (1.0f/pEL[i]);
		else
			implBrdf *= brdf[i] * (1.0f/pEL[i]);
	}
	cBrdf += implBrdf * (float)(implicitPdf*implicitPdf/sumPdf);

	// check for errors
	if (cBrdf.isInf()  ||  cBrdf.isNaN())
		return 0;
	if (cBrdf.isBlack())
		return 0;

	float R = ( 0.299f * cBrdf.r  +  0.587f * cBrdf.g  +  0.114f * cBrdf.b );

	if (dispersionUsed)
		R *= 4.0f;

	return R;

}

//--------------------------------------------------------------------------------------------------------

float MltSeed::evalWeightNoWeightContribution()
{
	Color4 energy = Color4(1,1,1);
	float llpdf = ss->lightMeshPowers[lightmeshID]/ss->allLightsPower * 1/(*ss->lightMeshes)[lightmeshID]->totalArea;
	float tlu, tlv;
	lPath[0].tri->evalTexUV(lPath[0].u, lPath[0].v, tlu, tlv);
	Color4 emission = lPath[0].mlay->getLightColor(tlu, tlv) * lPath[0].mlay->getIlluminance(ss, lightTriID);
	energy *= emission * max(0, lPath[0].outCosine) * (1.0f/llpdf);

	for (int i=1; i<=numL; i++)
	{
		HitData hd;
		hd.in  = lPath[i].inDir;
		hd.out = lPath[i].outDir;
		hd.normal_shade = lPath[i].normal;
		lPath[i].tri->evalTexUV(lPath[i].u, lPath[i].v, hd.tU, hd.tV);
		hd.normal_geom = (lPath[i].inst_mat.getMatrixForNormals() * lPath[i].tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		lPath[i].tri->getTangentSpaceSmooth(lPath[i].inst_mat, lPath[i].u, lPath[i].v, hd.dir_U, hd.dir_V);
		float pdf = lPath[i].mlay->getProbability(hd);
		Color4 brdf = lPath[i].mlay->getBRDF(hd) * fabs(lPath[i].outCosine);
		if (pdf == 0  ||  brdf.isBlack()  ||  brdf.isInf()  ||  brdf.isNaN())
			return 0.0f;
		energy *= brdf * (1.0f/pdf);
	}

	for (int i=1; i<=numE; i++)
	{
		HitData hd;
		hd.in  = ePath[i].inDir;
		hd.out = ePath[i].outDir;
		hd.normal_shade = ePath[i].normal;
		ePath[i].tri->evalTexUV(ePath[i].u, ePath[i].v, hd.tU, hd.tV);
		hd.normal_geom = (ePath[i].inst_mat.getMatrixForNormals() * ePath[i].tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		ePath[i].tri->getTangentSpaceSmooth(ePath[i].inst_mat, ePath[i].u, ePath[i].v, hd.dir_U, hd.dir_V);
		float pdf = ePath[i].mlay->getProbability(hd);
		Color4 brdf = ePath[i].mlay->getBRDF(hd) * fabs(ePath[i].outCosine);
		if (pdf == 0  ||  brdf.isBlack()  ||  brdf.isInf()  ||  brdf.isNaN())
			return 0.0f;
		energy *= brdf * (1.0f/pdf);
	}

	float d2 = (lPath[numL].pos - ePath[numE].pos).lengthSqr();
	if (d2 < 0.00000001f)
		return 0;

	float res = 0.299f * energy.r  +  0.587f * energy.g  +  0.114f * energy.b;
	res *= 1/d2;
	
	return res;
}

//--------------------------------------------------------------------------------------------------------
