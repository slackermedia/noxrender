//#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"
#include <tchar.h>

void print_progress_callback(TCHAR * message, float progress)
{
	MessageBox(0, message, _T(""), 0);
}


bool TrRaytracer::loadMatEditorScenes()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->loadMatEditorScenes(NULL);
	return true;
}

bool TrRaytracer::createMainScene()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->runDialogsUnicode = true;
	int id = rtr->addNewScene();
	if (id<0)
		return false;
	rtr->curSceneInd = id;
	rtr->pluginMaxSceneId = id;
	rtr->curScenePtr = rtr->scenes[id];
	return true;
}

bool TrRaytracer::changeSceneToMain()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr = rtr->scenes[0];
	return true;
}

int TrRaytracer::getVersionMajor()
{
	return NOX_VER_MAJOR;
}

int TrRaytracer::getVersionMinor()
{
	return NOX_VER_MINOR;
}

int TrRaytracer::getVersionBuild()
{
	return NOX_VER_BUILD;
}

void TrRaytracer::checkNOXVersions()
{
	Raytracer * rtr = Raytracer::getInstance();
	int v1,v2,v3,n1,n2,n3;
	v1 = getVersionMajor();
	v2 = getVersionMinor();
	v3 = getVersionBuild();
	n1 = rtr->getVersionMajor();
	n2 = rtr->getVersionMinor();
	n3 = rtr->getVersionBuild();

	if (v1!=n1  ||  v2!=n2  ||  v3!=n3)
	{
		TCHAR buf[128];
		_stprintf_s(buf, 128, _T("NOX plugin compiled as version %d.%d.%d\nwhile NOX dll found as version %d.%d.%d\n\nBetter update NOX."),
			v1,v2,v3,n1,n2,n3);
		MessageBox(0, buf, _T("Error"), 0);
	}
}

void TrRaytracer::addLog(char * txt)
{
	Raytracer * rtr = Raytracer::getInstance();
	Logger::add(txt);
}


bool TrRaytracer::getUserColors(const int num, float &r, float &g, float &b)
{
	Raytracer * rtr = Raytracer::getInstance();
	if (num<0 || num>17)
		return false;
	Color4 c;
	rtr->getUserColor(num, c);
	r = c.r;
	g = c.g;
	b = c.b;
	return true;
}

bool TrRaytracer::setUserColors(const int num, const float &r, const float &g, const float &b)
{
	Raytracer * rtr = Raytracer::getInstance();
	if (num<0 || num>17)
		return false;
	Color4 c(r,g,b);
	rtr->setUserColor(num, c);
	return true;
}
