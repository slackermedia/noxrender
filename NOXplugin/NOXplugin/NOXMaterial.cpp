#include "max2013predef.h"
#include "NOXMaterial.h"
#if MAX_RELEASE<13000
	#include <maxscrpt\maxscrpt.h>		// 2011 and lower
	#include <maxscrpt\definsfn.h>
#else
	#include <maxscript\maxscript.h>	// 2012 and higher
	#include <maxscript\macros\define_instantiation_functions.h>
#endif
#include <paramtype.h>

static NOXMaterialClassDesc noxMaterialDesc;
ClassDesc2* GetNOXMaterialDesc() { return &noxMaterialDesc; }

#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"
#include "iviewportmanager.h"
#include "stdmat.h"
#include "noxdebug.h"

enum { mpbid_col_refl0, mpbid_col_refl90, mpbid_col_transm,  mpbid_is_emitter, mpbid_is_transmission, mpbid_is_dispersion, mpbid_num_layers, mpbid_weight, mpbid_dispersion_power, 
		mpbid_connect90, mpbid_rough, mpbid_ior, mpbid_abs_dist, mpbid_is_absorption, mpbid_is_fakeglass, mpbid_col_emission, mpbid_normal_power, mpbid_col_absorption,
		mpbid_emission_power, mpbid_emission_unit,
		mpbid_tex_refl0, mpbid_tex_refl90, mpbid_tex_transm, mpbid_tex_weight, mpbid_tex_rough, mpbid_tex_light, mpbid_tex_normalmap, mpbid_tex_aniso, mpbid_tex_aniso_angle, mpbid_ies,
		mpbid_is_portal
		};

#if MAX_RELEASE<15000
	#define p_end end
#endif

static ParamBlockDesc2 nox_test_param_blk (
	//Required arguments ----------------------------
	0, _T("params_test"), 0, &noxMaterialDesc, P_AUTO_CONSTRUCT, MAT_NOX_REF_PARAMBLOCK,
 
	//Parameters Specifications ----------------------

	mpbid_col_refl0,		_T("lay_col_refl0"),			TYPE_RGBA_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_col_refl90,		_T("lay_col_refl90"),			TYPE_RGBA_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_col_transm,		_T("lay_col_transm"),			TYPE_RGBA_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_col_emission,		_T("lay_col_emitter"),			TYPE_RGBA_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_col_absorption,	_T("lay_col_abs"),				TYPE_RGBA_TAB, 16,		P_ANIMATABLE,    0, p_end,

	mpbid_is_emitter,		_T("lay_is_emitter"),			TYPE_BOOL_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_is_transmission,	_T("lay_is_transmission"),		TYPE_BOOL_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_is_absorption,	_T("lay_is_absorption"),		TYPE_BOOL_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_is_fakeglass,		_T("lay_is_fakeglass"),			TYPE_BOOL_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_connect90,		_T("lay_link_0_90"),			TYPE_BOOL_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_is_dispersion,	_T("lay_is_dispersion"),		TYPE_BOOL_TAB, 16,		P_ANIMATABLE,    0, p_end,

	mpbid_weight,			_T("lay_weight"),				TYPE_FLOAT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_rough,			_T("lay_rough"),				TYPE_FLOAT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_ior,				_T("lay_ior"),					TYPE_FLOAT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_abs_dist,			_T("lay_abs_dist"),				TYPE_FLOAT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_normal_power,		_T("lay_normal_power"),			TYPE_FLOAT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_dispersion_power,	_T("lay_dispersion_power"),		TYPE_FLOAT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	mpbid_emission_power,	_T("lay_emission_power"),		TYPE_FLOAT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	
	mpbid_emission_unit,	_T("lay_emission_unit"),		TYPE_INT_TAB, 16,		P_ANIMATABLE,    0, p_end,
	
	mpbid_tex_refl0,		_T("lay_tex_refl0"),			TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_refl90,		_T("lay_tex_refl90"),			TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_transm,		_T("lay_tex_transmission"),		TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_weight,		_T("lay_tex_weight"),			TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_rough,		_T("lay_tex_rough"),			TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_light,		_T("lay_tex_emitter"),			TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_normalmap,	_T("lay_tex_normal"),			TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_aniso,		_T("lay_tex_aniso"),			TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_tex_aniso_angle,	_T("lay_tex_aniso_angle"),		TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,
	mpbid_ies,				_T("lay_ies"),					TYPE_STRING_TAB, 16,	P_ANIMATABLE,    0, p_end,

	mpbid_num_layers,		_T("layers_count"),				TYPE_INT,				P_ANIMATABLE,    0, p_end,
	mpbid_is_portal,		_T("is_portal"),				TYPE_BOOL,				P_ANIMATABLE,    0, p_end,

	p_end
);


NOXMaterial::NOXMaterial(BOOL loading) 
	: mat()
{
	ADD_LOG_CONSTR("NOXMaterial Constructor");
	currentTexture = NULL;
	for (int i=0; i<16; i++)
	{
		texRough[i] = NULL;
		texRefl0[i]  = NULL;
		texRefl90[i]  = NULL;
		texWeight[i]  = NULL;
		texLight[i]  = NULL;
		texNormal[i]  = NULL;
		texTransm[i]  = NULL;
		texAniso[i]  = NULL;
		texAnisoAngle[i]  = NULL;
	}
	texOpacity = NULL;
	texDisplacement = NULL;

	diffuse = Color(1.0f, 1.0f, 1.0f);
	ambient = Color(1.0f, 1.0f, 1.0f);
	specular = Color(1.0f, 1.0f, 1.0f);
	shininess = 1;
	shinStr = 1;
	xParency = 0;
	wireSize = 1;

	hTUInfoWindow = 0;
	lastTextureNumber = 0;
	lastLayerNumber = 0;
	curTextureNumber = 0;
	curLayerNumber = 0;


	pb_test = NULL;

	if (!loading) 
	{
		noxMaterialDesc.MakeAutoParamBlocks(this); 
		Reset();
		updateToParamBlock();
	}


	ADD_LOG_CONSTR("NOXMaterial Constructor done");
}

NOXMaterial::~NOXMaterial() 
{
	ADD_LOG_CONSTR("NOXMaterial Destructor - empty");
}

void NOXMaterial::Reset() 
{
											ADD_LOG_PARTS_MAIN("NOXMaterial::Reset()");
	mat.deleteNOXmaterial();
											ADD_LOG_PARTS_ENUM("NOXMaterial::Reset() 1");
	mat.createSimpleMaterial();
											ADD_LOG_PARTS_ENUM("NOXMaterial::Reset() 2");
	ivalid.SetEmpty();
											ADD_LOG_PARTS_MAIN("NOXMaterial::Reset() done");
}



ParamDlg* NOXMaterial::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) 
{
	return new NOXMatParamDialog(this, imp, hInstance);
}

BOOL NOXMaterial::SetDlgThing(ParamDlg* dlg)
{
	return FALSE;
}

int	NOXMaterial::NumParamBlocks() 
{
	return 1;
}

IParamBlock2* NOXMaterial::GetParamBlock(int i)
{
	if (pb_test) { ADD_LOG_CONSTR("getting param block by num"); }
		else { ADD_LOG_CONSTR("getting param block by num ... null"); }
	if (i==0)
		return pb_test;
	return NULL;
}

IParamBlock2* NOXMaterial::GetParamBlockByID(BlockID id)
{
	if (pb_test) { ADD_LOG_CONSTR("getting param block by id"); }
		else { ADD_LOG_CONSTR("getting param block by id ... null"); }
	if (id==0)
		return pb_test;
	return NULL;
}

Interval NOXMaterial::Validity(TimeValue t)
{
	Interval valid = FOREVER;		
	return valid;
}

RefTargetHandle NOXMaterial::GetReference(int i) 
{
	if (i<160)
	{
		int d,r;
		d = i/10;
		r = i%10;
		switch (r)
		{
			case NOX_TEX_ROUGHNESS-1:		return texRough[d];
			case NOX_TEX_REFL0-1:			return texRefl0[d];
			case NOX_TEX_REFL90-1:			return texRefl90[d];
			case NOX_TEX_WEIGHT-1:			return texWeight[d];
			case NOX_TEX_LIGHT-1:			return texLight[d];
			case NOX_TEX_NORMAL-1:			return texNormal[d];
			case NOX_TEX_TRANSM-1:			return texTransm[d];
			case NOX_TEX_ANISO-1:			return texAniso[d];
			case NOX_TEX_ANISO_ANGLE-1:		return texAnisoAngle[d];
			default:						return NULL;
		}
	}
	if (i==160)
		return texOpacity;
	if (i==161)
		return texDisplacement;
	if (i==MAT_NOX_REF_PARAMBLOCK)
	{
		//ADD_LOG_CONSTR("getting param block reference");
		return pb_test;
	}
	return NULL;
}

void NOXMaterial::SetReference(int i, RefTargetHandle rtarg) 
{
	if (i<160)
	{
		int d,r;
		d = i/10;
		r = i%10;
		switch (r)
		{
			case NOX_TEX_ROUGHNESS-1:		texRough[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_REFL0-1:			texRefl0[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_REFL90-1:			texRefl90[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_WEIGHT-1:			texWeight[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_LIGHT-1:			texLight[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_NORMAL-1:			texNormal[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_TRANSM-1:			texTransm[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_ANISO-1:			texAniso[d] = (BitmapTex*)rtarg;	break;
			case NOX_TEX_ANISO_ANGLE-1:		texAnisoAngle[d] = (BitmapTex*)rtarg;	break;
		}
	}
	if (i==160)
		texOpacity  = (BitmapTex*)rtarg;
	if (i==161)
		texDisplacement = (BitmapTex*)rtarg;
	if (i==MAT_NOX_REF_PARAMBLOCK)
	{
		pb_test = (IParamBlock2 *)rtarg;
		//if (pb_test)  {ADD_LOG_CONSTR("param block reference set to sth"); }
		//	else {	ADD_LOG_CONSTR("param block reference set to null"); }

	}
}

RefResult NOXMaterial::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
   PartID& partID, RefMessage message ) 
{
	switch (message) {
		case REFMSG_CHANGE:
			//ADD_LOG_PARTS_MAIN("   REFMSG_CHANGE");
			ivalid.SetEmpty();
		}
	return REF_SUCCEED;
}

void NOXMaterial::updateFromParamBlock()
{
	ADD_LOG_PARTS_MAIN("udpating from param block stuff");

	TimeValue ctime = GetCOREInterface()->GetTime();
	int nlayers = pb_test->GetInt(mpbid_num_layers, ctime, 0);
	nlayers = min(16, max(1, nlayers));
	int cnlayers = mat.getLayersNumber();
	if (cnlayers>nlayers)
		mat.createSimpleMaterial();

	while(mat.getLayersNumber() < nlayers) 
		mat.addLayer();

	Color c;
	BOOL b;
	char buf[256];
	float f;
	int ii;
	for (int i=0; i<nlayers; i++)
	{
		mat.setCurLayer(i);

		c = pb_test->GetColor(mpbid_col_refl0, ctime, i);
		sprintf_s(buf, 256, "lay %d col0 %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerColor0(c.r, c.g, c.b, 1.0f);

		c = pb_test->GetColor(mpbid_col_refl90, ctime, i);
		sprintf_s(buf, 256, "lay %d col90 %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerColor90(c.r, c.g, c.b, 1.0f);

		c = pb_test->GetColor(mpbid_col_transm, ctime, i);
		sprintf_s(buf, 256, "lay %d col transm %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerTransmColor(c.r, c.g, c.b, 1.0f);

		c = pb_test->GetColor(mpbid_col_emission, ctime, i);
		sprintf_s(buf, 256, "lay %d col emission %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerEmissionColor(c.r, c.g, c.b, 1.0f);

		c = pb_test->GetColor(mpbid_col_absorption, ctime, i);
		sprintf_s(buf, 256, "lay %d col absorption %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerAbsColor(c.r, c.g, c.b, 1.0f);

		b = pb_test->GetInt(mpbid_is_emitter, ctime, i);
		sprintf_s(buf, 256, "lay %d emission %s", i, b?"on":"off");
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerType(b?1:2);

		b = pb_test->GetInt(mpbid_is_transmission, ctime, i);
		sprintf_s(buf, 256, "lay %d transmission %s", i, b?"on":"off");
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerTransmOn(b);

		b = pb_test->GetInt(mpbid_is_absorption, ctime, i);
		sprintf_s(buf, 256, "lay %d absorption %s", i, b?"on":"off");
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerAbsOn(b);

		b = pb_test->GetInt(mpbid_is_fakeglass, ctime, i);
		sprintf_s(buf, 256, "lay %d fakeglass %s", i, b?"on":"off");
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerFakeGlass(b);

		b = pb_test->GetInt(mpbid_connect90, ctime, i);
		sprintf_s(buf, 256, "lay %d link col0-90 %s", i, b?"on":"off");
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerConnect90(b);

		b = pb_test->GetInt(mpbid_is_dispersion, ctime, i);
		sprintf_s(buf, 256, "lay %d dispersion %s", i, b?"on":"off");
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerDispersionOn(b);

		f = pb_test->GetFloat(mpbid_weight, ctime, i);
		sprintf_s(buf, 256, "lay %d weight %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerContribution((int)(f*100));

		f = pb_test->GetFloat(mpbid_rough, ctime, i);
		sprintf_s(buf, 256, "lay %d rough %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerRoughness(f);

		f = pb_test->GetFloat(mpbid_ior, ctime, i);
		sprintf_s(buf, 256, "lay %d ior %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerFresnelIOR(f);

		f = pb_test->GetFloat(mpbid_abs_dist, ctime, i);
		sprintf_s(buf, 256, "lay %d abs dist %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerAbsDist(f);

		f = pb_test->GetFloat(mpbid_dispersion_power, ctime, i);
		sprintf_s(buf, 256, "lay %d dispersion power %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerDispersionValue(f);

		f = pb_test->GetFloat(mpbid_emission_power, ctime, i);
		sprintf_s(buf, 256, "lay %d emission power %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerEmissionPower(f);

		ii = pb_test->GetInt(mpbid_emission_unit, ctime, i);
		ii = min(4, max(1, ii));
		sprintf_s(buf, 256, "lay %d emission unit %d", i, ii);
		ADD_LOG_PARTS_MAIN(buf);
		mat.setLayerEmissionUnit(ii);

 
		// --- textures
		const TCHAR * tx1t = pb_test->GetStr(mpbid_tex_refl0, ctime, i);
		char * tx1a = getAnsiCopy1(tx1t);
		bool tx1b = (tx1a!=NULL  &&  strlen(tx1a)>0);
		mat.setLayerTexRefl0(tx1b ? tx1a : NULL);
		mat.setLayerUseTexRefl0(tx1b);
		if (tx1a)
			free(tx1a);

		const TCHAR * tx2t = pb_test->GetStr(mpbid_tex_refl90, ctime, i);
		char * tx2a = getAnsiCopy1(tx2t);
		bool tx2b = (tx2a!=NULL  &&  strlen(tx2a)>0);
		mat.setLayerTexRefl90(tx2b ? tx2a : NULL);
		mat.setLayerUseTexRefl90(tx2b);
		if (tx2a)
			free(tx2a);

		const TCHAR * tx3t = pb_test->GetStr(mpbid_tex_transm, ctime, i);
		char * tx3a = getAnsiCopy1(tx3t);
		bool tx3b = (tx3a!=NULL  &&  strlen(tx3a)>0);
		mat.setLayerTexTransm(tx3b ? tx3a : NULL);
		mat.setLayerUseTexTransm(tx3b);
		if (tx3a)
			free(tx3a);

		const TCHAR * tx4t = pb_test->GetStr(mpbid_tex_weight, ctime, i);
		char * tx4a = getAnsiCopy1(tx4t);
		bool tx4b = (tx4a!=NULL  &&  strlen(tx4a)>0);
		mat.setLayerTexWeight(tx4b ? tx4a : NULL);
		mat.setLayerUseTexWeight(tx4b);
		if (tx4a)
			free(tx4a);

		const TCHAR * tx5t = pb_test->GetStr(mpbid_tex_rough, ctime, i);
		char * tx5a = getAnsiCopy1(tx5t);
		bool tx5b = (tx5a!=NULL  &&  strlen(tx5a)>0);
		mat.setLayerTexRough(tx5b ? tx5a : NULL);
		mat.setLayerUseTexRough(tx5b);
		if (tx5a)
			free(tx5a);

		const TCHAR * tx6t = pb_test->GetStr(mpbid_tex_light, ctime, i);
		char * tx6a = getAnsiCopy1(tx6t);
		bool tx6b = (tx6a!=NULL  &&  strlen(tx6a)>0);
		mat.setLayerTexLight(tx6b ? tx6a : NULL);
		mat.setLayerUseTexLight(tx6b);
		if (tx6a)
			free(tx6a);

		const TCHAR * tx7t = pb_test->GetStr(mpbid_tex_normalmap, ctime, i);
		char * tx7a = getAnsiCopy1(tx7t);
		bool tx7b = (tx7a!=NULL  &&  strlen(tx7a)>0);
		mat.setLayerTexNormal(tx7b ? tx7a : NULL);
		mat.setLayerUseTexNormal(tx7b);
		if (tx7a)
			free(tx7a);

		const TCHAR * tx8t = pb_test->GetStr(mpbid_tex_aniso, ctime, i);
		char * tx8a = getAnsiCopy1(tx8t);
		bool tx8b = (tx8a!=NULL  &&  strlen(tx8a)>0);
		mat.setLayerTexAniso(tx8b ? tx8a : NULL);
		mat.setLayerUseTexAniso(tx8b);
		if (tx8a)
			free(tx8a);

		const TCHAR * tx9t = pb_test->GetStr(mpbid_tex_aniso_angle, ctime, i);
		char * tx9a = getAnsiCopy1(tx9t);
		bool tx9b = (tx9a!=NULL  &&  strlen(tx9a)>0);
		mat.setLayerTexAnisoAngle(tx9b ? tx9a : NULL);
		mat.setLayerUseTexAnisoAngle(tx9b);
		if (tx9a)
			free(tx9a);

		const TCHAR * tx10t = pb_test->GetStr(mpbid_ies, ctime, i);
		char * tx10a = getAnsiCopy1(tx10t);
		bool tx10b = (tx10a!=NULL  &&  strlen(tx10a)>0);
		mat.setLayerEmissionIes(tx10b ? tx10a : NULL);
		//mat.setLayeries(tx10b);
		if (tx10a)
			free(tx10a);
	}
	
	b = pb_test->GetInt(mpbid_is_portal, ctime, 0);
	sprintf_s(buf, 256, "is portal: %s", b?"on":"off");
	ADD_LOG_PARTS_MAIN(buf);
	mat.setSkyportal(b!=0);

	getTexturesFromNOX();

	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE); 

	GetCOREInterface()->RedrawViews(GetCOREInterface()->GetTime());
}

void NOXMaterial::updateToParamBlock()
{
	Color c;
	BOOL b;
	float f;
	int ii;
	float a;

	char buf[256];
	ADD_LOG_PARTS_MAIN("udpating to param block stuff");

	TimeValue ctime = GetCOREInterface()->GetTime();
	int nlayers = mat.getLayersNumber();
	pb_test->SetValue(mpbid_num_layers, ctime, nlayers, 0);
	sprintf_s(buf, 256, "setting paramblock num layers to %d", nlayers);
	ADD_LOG_PARTS_MAIN(buf);

	b = mat.getSkyportal() ? TRUE : FALSE;
	pb_test->SetValue(mpbid_is_portal, ctime, b, 0);
	sprintf_s(buf, 256, "setting paramblock is portal enabled: %s", b?"yes":"no");
	ADD_LOG_PARTS_MAIN(buf);

	for (int i=0; i<16; i++)
	{
		if (i>=nlayers) c.r=c.g=c.b=0.0f; else mat.getLayerColor0(i, c.r, c.g, c.b, a);
		pb_test->SetValue(mpbid_col_refl0, ctime, c, i);
		sprintf_s(buf, 256, "setting paramblock lay %d col0 %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) c.r=c.g=c.b=0.0f; else mat.getLayerColor90(i, c.r, c.g, c.b, a);
		pb_test->SetValue(mpbid_col_refl90, ctime, c, i);
		sprintf_s(buf, 256, "setting paramblock lay %d col90 %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) c.r=c.g=c.b=0.0f; else mat.getLayerTransmColor(i, c.r, c.g, c.b, a);
		pb_test->SetValue(mpbid_col_transm, ctime, c, i);
		sprintf_s(buf, 256, "setting paramblock lay %d transm col %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) c.r=c.g=c.b=0.0f; else mat.getLayerEmissionColor(i, c.r, c.g, c.b, a);
		pb_test->SetValue(mpbid_col_emission, ctime, c, i);
		sprintf_s(buf, 256, "setting paramblock lay %d emiss col %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) c.r=c.g=c.b=0.0f; else mat.getLayerAbsColor(i, c.r, c.g, c.b, a);
		pb_test->SetValue(mpbid_col_absorption, ctime, c, i);
		sprintf_s(buf, 256, "setting paramblock lay %d abs col %f %f %f", i, c.r, c.g, c.b);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) b=FALSE; else b=(mat.getLayerType(i)==1) ? TRUE : FALSE;
		pb_test->SetValue(mpbid_is_emitter, ctime, b, i);
		sprintf_s(buf, 256, "setting paramblock lay %d emission enabled: %s", i, b?"yes":"no");
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) b=FALSE; else b=mat.getLayerConnect90(i) ? TRUE : FALSE;
		pb_test->SetValue(mpbid_connect90, ctime, b, i);
		sprintf_s(buf, 256, "setting paramblock lay %d connect 90 enabled: %s", i, b?"yes":"no");
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) b=FALSE; else b=mat.getLayerTransmOn(i) ? TRUE : FALSE;
		pb_test->SetValue(mpbid_is_transmission, ctime, b, i);
		sprintf_s(buf, 256, "setting paramblock lay %d emission enabled: %s", i, b?"yes":"no");
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) b=FALSE; else b=mat.getLayerFakeGlass(i) ? TRUE : FALSE;
		pb_test->SetValue(mpbid_is_fakeglass, ctime, b, i);
		sprintf_s(buf, 256, "setting paramblock lay %d fake glass enabled: %s", i, b?"yes":"no");
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) b=FALSE; else b=mat.getLayerAbsOn(i) ? TRUE : FALSE;
		pb_test->SetValue(mpbid_is_absorption, ctime, b, i);
		sprintf_s(buf, 256, "setting paramblock lay %d absorption enabled: %s", i, b?"yes":"no");
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) b=FALSE; else b=mat.getLayerDispersionOn(i) ? TRUE : FALSE;
		pb_test->SetValue(mpbid_is_dispersion, ctime, b, i);
		sprintf_s(buf, 256, "setting paramblock lay %d dispersion enabled: %s", i, b?"yes":"no");
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) f=0.0f; else f=mat.getLayerContribution(i)*0.01f;
		pb_test->SetValue(mpbid_weight, ctime, f, i);
		sprintf_s(buf, 256, "setting paramblock lay %d weight: %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) f=0.0f; else f=mat.getLayerRoughness(i);
		pb_test->SetValue(mpbid_rough, ctime, f, i);
		sprintf_s(buf, 256, "setting paramblock lay %d rough: %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) f=0.0f; else f=mat.getLayerFresnelIOR(i);
		pb_test->SetValue(mpbid_ior, ctime, f, i);
		sprintf_s(buf, 256, "setting paramblock lay %d ior: %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) f=0.0f; else f=mat.getLayerAbsDist(i);
		pb_test->SetValue(mpbid_abs_dist, ctime, f, i);
		sprintf_s(buf, 256, "setting paramblock lay %d abs dist: %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) f=0.0f; else f=mat.getLayerTexPostNormalPower(i);
		pb_test->SetValue(mpbid_normal_power, ctime, f, i);
		sprintf_s(buf, 256, "setting paramblock lay %d normal power: %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) f=0.0f; else f=mat.getLayerDispersionValue(i);
		pb_test->SetValue(mpbid_dispersion_power, ctime, f, i);
		sprintf_s(buf, 256, "setting paramblock lay %d dispersion power: %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) f=0.0f; else f=mat.getLayerEmissionPower(i);
		pb_test->SetValue(mpbid_emission_power, ctime, f, i);
		sprintf_s(buf, 256, "setting paramblock lay %d emission power: %f", i, f);
		ADD_LOG_PARTS_MAIN(buf);

		if (i>=nlayers) ii=0; else ii=mat.getLayerEmissionUnit(i);
		pb_test->SetValue(mpbid_emission_unit, ctime, ii, i);
		sprintf_s(buf, 256, "setting paramblock lay %d emission unit: %d", i, ii);
		ADD_LOG_PARTS_MAIN(buf);


		// --- textures
		char * tx1str = mat.getLayerTexRefl0(i);
		bool tx1on = mat.getLayerUseTexRefl0(i);
		if (tx1on && tx1str && strlen(tx1str)>0)
		{
			TCHAR * tx1strT = copyAnsiToTchar1(tx1str);
			pb_test->SetValue(mpbid_tex_refl0, ctime, tx1strT, i);
			if (tx1strT)
				free(tx1strT);
		}
		else
			pb_test->SetValue(mpbid_tex_refl0, ctime, NULL, i);

		char * tx2str = mat.getLayerTexRefl90(i);
		bool tx2on = mat.getLayerUseTexRefl90(i);
		if (tx2on && tx2str && strlen(tx2str)>0)
		{
			TCHAR * tx2strT = copyAnsiToTchar1(tx2str);
			pb_test->SetValue(mpbid_tex_refl90, ctime, tx2strT, i);
			if (tx2strT)
				free(tx2strT);
		}
		else
			pb_test->SetValue(mpbid_tex_refl90, ctime, NULL, i);

		char * tx3str = mat.getLayerTexTransm(i);
		bool tx3on = mat.getLayerUseTexTransm(i);
		if (tx3on && tx3str && strlen(tx3str)>0)
		{
			TCHAR * tx3strT = copyAnsiToTchar1(tx3str);
			pb_test->SetValue(mpbid_tex_transm, ctime, tx3strT, i);
			if (tx3strT)
				free(tx3strT);
		}
		else
			pb_test->SetValue(mpbid_tex_transm, ctime, NULL, i);

		char * tx4str = mat.getLayerTexWeight(i);
		bool tx4on = mat.getLayerUseTexWeight(i);
		if (tx4on && tx4str && strlen(tx4str)>0)
		{
			TCHAR * tx4strT = copyAnsiToTchar1(tx4str);
			pb_test->SetValue(mpbid_tex_weight, ctime, tx4strT, i);
			if (tx4strT)
				free(tx4strT);
		}
		else
			pb_test->SetValue(mpbid_tex_weight, ctime, NULL, i);

		char * tx5str = mat.getLayerTexRough(i);
		bool tx5on = mat.getLayerUseTexRough(i);
		if (tx5on && tx5str && strlen(tx5str)>0)
		{
			TCHAR * tx5strT = copyAnsiToTchar1(tx5str);
			pb_test->SetValue(mpbid_tex_rough, ctime, tx5strT, i);
			if (tx5strT)
				free(tx5strT);
		}
		else
			pb_test->SetValue(mpbid_tex_rough, ctime, NULL, i);

		char * tx6str = mat.getLayerTexLight(i);
		bool tx6on = mat.getLayerUseTexLight(i);
		if (tx6on && tx6str && strlen(tx6str)>0)
		{
			TCHAR * tx6strT = copyAnsiToTchar1(tx6str);
			pb_test->SetValue(mpbid_tex_light, ctime, tx6strT, i);
			if (tx6strT)
				free(tx6strT);
		}
		else
			pb_test->SetValue(mpbid_tex_light, ctime, NULL, i);

		char * tx7str = mat.getLayerTexNormal(i);
		bool tx7on = mat.getLayerUseTexNormal(i);
		if (tx7on && tx7str && strlen(tx7str)>0)
		{
			TCHAR * tx7strT = copyAnsiToTchar1(tx7str);
			pb_test->SetValue(mpbid_tex_normalmap, ctime, tx7strT, i);
			if (tx7strT)
				free(tx7strT);
		}
		else
			pb_test->SetValue(mpbid_tex_normalmap, ctime, NULL, i);

		char * tx8str = mat.getLayerTexAniso(i);
		bool tx8on = mat.getLayerUseTexAniso(i);
		if (tx8on && tx8str && strlen(tx8str)>0)
		{
			TCHAR * tx8strT = copyAnsiToTchar1(tx8str);
			pb_test->SetValue(mpbid_tex_aniso, ctime, tx8strT, i);
			if (tx8strT)
				free(tx8strT);
		}
		else
			pb_test->SetValue(mpbid_tex_aniso, ctime, NULL, i);

		char * tx9str = mat.getLayerTexAnisoAngle(i);
		bool tx9on = mat.getLayerUseTexAnisoAngle(i);
		if (tx9on && tx9str && strlen(tx9str)>0)
		{
			TCHAR * tx9strT = copyAnsiToTchar1(tx9str);
			pb_test->SetValue(mpbid_tex_aniso_angle, ctime, tx9strT, i);
			if (tx9strT)
				free(tx9strT);
		}
		else
			pb_test->SetValue(mpbid_tex_aniso_angle, ctime, NULL, i);

		char * tx10str = mat.getLayerEmissionIes(i);
		if (tx10str && strlen(tx10str)>0)
		{
			TCHAR * tx10strT = copyAnsiToTchar1(tx10str);
			pb_test->SetValue(mpbid_ies, ctime, tx10strT, i);
			if (tx10strT)
				free(tx10strT);
		}
		else
			pb_test->SetValue(mpbid_ies, ctime, NULL, i);

	}
}


def_visible_primitive(nox_update_mat_from_paramblock, "nox_update_mat_from_paramblock");
Value * nox_update_mat_from_paramblock_cf(Value** arg_list, int count)
{
	ADD_LOG_PARTS_MAIN("nox_update_mat_from_paramblock called from script");
	if (count!=1)
	{
		ADD_LOG_PARTS_MAIN("... with no argument, abort");
		return &false_value;
	}
	
	Mtl * mtl = arg_list[0]->to_mtl();
	if (!mtl)
	{
		ADD_LOG_PARTS_MAIN("  ... with non-material argument, abort");
		return &false_value;
	}

	if (NOXMaterial_CLASS_ID != mtl->ClassID())
	{
		ADD_LOG_PARTS_MAIN("  ... with non-nox-material argument, abort");
		return &false_value;
	}

	NOXMaterial * nmat = (NOXMaterial*)mtl;
	nmat->updateFromParamBlock();

	//ADD_LOG_PARTS_MAIN("nox_update_mat_from_paramblock done");
	return &true_value;
}

def_visible_primitive(nox_update_paramblock_from_mat, "nox_update_paramblock_from_mat");
Value * nox_update_paramblock_from_mat_cf(Value** arg_list, int count)
{
	ADD_LOG_PARTS_MAIN("nox_update_paramblock_from_mat called from script");
	if (count!=1)
	{
		ADD_LOG_PARTS_MAIN("... with no argument, abort");
		return &false_value;
	}
	
	Mtl * mtl = arg_list[0]->to_mtl();
	if (!mtl)
	{
		ADD_LOG_PARTS_MAIN("  ... with non-material argument, abort");
		return &false_value;
	}

	if (NOXMaterial_CLASS_ID != mtl->ClassID())
	{
		ADD_LOG_PARTS_MAIN("  ... with non-nox-material argument, abort");
		return &false_value;
	}

	NOXMaterial * nmat = (NOXMaterial*)mtl;
	nmat->updateToParamBlock();

	//ADD_LOG_PARTS_MAIN("nox_update_paramblock_from_mat done");
	return &true_value;
}


RefTargetHandle NOXMaterial::Clone(RemapDir &remap) 
{
	ADD_LOG_PARTS_MAIN("NOXMaterial::Clone");
	NOXMaterial *mnew = new NOXMaterial(FALSE);
	*((MtlBase*)mnew) = *((MtlBase*)this); 
	mnew->ivalid.SetEmpty();	
	BaseClone(this, mnew, remap);
	mnew->mat.address = mat.cloneAddress();
	mnew->mat.curLay = NULL;
	mnew->mat.llay = 0;
	
	mnew->ReplaceReference(MAT_NOX_REF_PARAMBLOCK, remap.CloneRef(pb_test));
	
	return (RefTargetHandle)mnew;
}

void NOXMaterial::Update(TimeValue t, Interval& valid) 
{
	if (!ivalid.InInterval(t)) 
	{
		ivalid.SetInfinite();
		if (currentTexture)
			currentTexture->Update(t, valid);
	}
	valid &= ivalid;
}

void NOXMaterial::SetAmbient(Color c, TimeValue t) 
{
	ambient = c;
	mat.SetAmbient(c.r, c.g, c.b);
}		

void NOXMaterial::SetDiffuse(Color c, TimeValue t) 
{
	diffuse = c;
	mat.SetDiffuse(c.r, c.g, c.b);
}

void NOXMaterial::SetSpecular(Color c, TimeValue t) 
{
	specular = c;
	mat.SetSpecular(c.r, c.g, c.b);
}

void NOXMaterial::SetShininess(float v, TimeValue t) 
{
	shininess = v;
	mat.SetShininess(v);
}
				
BOOL NOXMaterial::GetSelfIllumColorOn (int mtlNum, BOOL backFace)
{
	if (mat.getLayerType(0) == 1)	// emitter
		return TRUE;
	else
		return FALSE;
}

float NOXMaterial::GetSelfIllum (int mtlNum, BOOL backFace)
{
	if (mat.getLayerType(0) == 1)	// emitter
		return 1;
	else
		return 0;
}

Color NOXMaterial::GetSelfIllumColor (int mtlNum, BOOL backFace)
{
	if (mat.getLayerType(0) == 1)	// emitter
	{
		float r,g,b,a;
		mat.getLayerEmissionColor(0,r,g,b,a);
		return Color(r,g,b);
	}
	else
		return Color(0,0,0);
}


Color NOXMaterial::GetAmbient(int mtlNum, BOOL backFace)
{
	float r,g,b;
	mat.GetAmbient(r,g,b);
	return Color(r,g,b);
	return ambient;
}

Color NOXMaterial::GetDiffuse(int mtlNum, BOOL backFace)
{
	if (mat.getLayerType(0) == 1)	// emitter
		return Color(0,0,0);
	float r,g,b;
	mat.GetDiffuse(r,g,b);
	return Color(r,g,b);
	return diffuse;
}

Color NOXMaterial::GetSpecular(int mtlNum, BOOL backFace)
{
	if (mat.getLayerType(0) == 1)	// emitter
		return Color(0,0,0);
	float r,g,b;
	mat.GetSpecular(r,g,b);
	return Color(r,g,b);
	return specular;
}

float NOXMaterial::GetXParency(int mtlNum, BOOL backFace)
{
	return mat.GetXParency();
	return xParency;
}

float NOXMaterial::GetShininess(int mtlNum, BOOL backFace)
{
	return mat.GetShininess();
	return shininess;
}

float NOXMaterial::GetShinStr(int mtlNum, BOOL backFace)
{
	return mat.GetShinStr();
	return shinStr;
}

float NOXMaterial::WireSize(int mtlNum, BOOL backFace)
{
	return 1;
	return wireSize;
}

void NOXMaterial::Shade(ShadeContext& sc) 
{
	if (gbufID) sc.SetGBufferID(gbufID);
	Color c = sc.DiffuseIllum();
	float r,g,b;
	mat.GetDiffuse(r,g,b);
	c.r *= r;
	c.g *= g;
	c.b *= b;
	sc.out.c = c;
}

void NOXMaterial::changeTexture(int newID, int nLayer)
{
	BitmapTex * ctex = NULL;
	int nL = mat.getLayersNumber();
	nLayer = min(nL, max(0, nLayer));
	switch (newID)
	{
		case NOX_TEX_ROUGHNESS:		ctex = texRough[nLayer];		break;
		case NOX_TEX_REFL0:			ctex = texRefl0[nLayer];		break;
		case NOX_TEX_REFL90:		ctex = texRefl90[nLayer];		break;
		case NOX_TEX_WEIGHT:		ctex = texWeight[nLayer];		break;
		case NOX_TEX_LIGHT:			ctex = texLight[nLayer];		break;
		case NOX_TEX_NORMAL:		ctex = texNormal[nLayer];		break;
		case NOX_TEX_TRANSM:		ctex = texTransm[nLayer];		break;
		case NOX_TEX_ANISO:			ctex = texAniso[nLayer];		break;
		case NOX_TEX_ANISO_ANGLE:	ctex = texAnisoAngle[nLayer];	break;
		case NOX_TEX_OPACITY:		ctex = texOpacity;				break;
		case NOX_TEX_DISPLACEMENT:	ctex = texDisplacement;			break;
		default:					ctex = NULL;					break;
	}

	if (ctex)
	{
		showTex = true;
		ctex->ActivateTexDisplay(TRUE);
		SetActiveTexmap(ctex);
		GetCOREInterface()->ActivateTexture(ctex, (Mtl*)this);
		SetMtlFlag(MTL_DISPLAY_ENABLE_FLAGS, TRUE);
		ClearMtlFlag(MTL_TEX_DISPLAY_ENABLED);
	}
	else
	{
		showTex = false;
		SetActiveTexmap(NULL);
		GetCOREInterface()->ActivateTexture(NULL, (Mtl*)this);
	}

	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE); 
	GetCOREInterface()->RedrawViews(GetCOREInterface()->GetTime());
	currentTexture = ctex;
}

void NOXMaterial::getTexturesFromNOX()
{
	hTUInfoWindow = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_NOX_MAT_TEX_UPDATE), 
							GetCOREInterface()->GetMAXHWnd(), NOXMatTexUpdateParamDlgProc);

	int nT = 0;
	char * tname = NULL;
	for (int i=0; i<16; i++)
	{
		if (texRough[i])
			DeleteReference(i*10+NOX_TEX_ROUGHNESS-1);
		texRough[i] = NULL;

		if (texRefl0[i])
			DeleteReference(i*10+NOX_TEX_REFL0-1);
		texRefl0[i] = NULL;

		if (texRefl90[i])
			DeleteReference(i*10+NOX_TEX_REFL90-1);
		texRefl90[i] = NULL;

		if (texWeight[i])
			DeleteReference(i*10+NOX_TEX_WEIGHT-1);
		texWeight[i] = NULL;

		if (texLight[i])
			DeleteReference(i*10+NOX_TEX_LIGHT-1);
		texLight[i] = NULL;

		if (texNormal[i])
			DeleteReference(i*10+NOX_TEX_NORMAL-1);
		texNormal[i] = NULL;

		if (texTransm[i])
			DeleteReference(i*10+NOX_TEX_TRANSM-1);
		texTransm[i] = NULL;

		if (texAniso[i])
			DeleteReference(i*10+NOX_TEX_ANISO-1);
		texAniso[i] = NULL;

		if (texAnisoAngle[i])
			DeleteReference(i*10+NOX_TEX_ANISO_ANGLE-1);
		texAnisoAngle[i] = NULL;

		tname = mat.getLayerTexRough(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexRefl0(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexRefl90(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexWeight(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexLight(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexNormal(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexTransm(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexAniso(i);
		if (tname  &&  strlen(tname)>0)
			nT++;
		tname = mat.getLayerTexAnisoAngle(i);
		if (tname  &&  strlen(tname)>0)
			nT++;

	}

	if (texOpacity)
		DeleteReference(160);
	texOpacity = NULL;

	if (texDisplacement)
		DeleteReference(161);
	texDisplacement = NULL;

	// opacity
	tname = mat.getTexOpacity();
	if (!tname  ||  strlen(tname)<1)
		texOpacity = NULL;
	else
	{
		TCHAR * ttname = copyAnsiToTchar1(tname);
		BitmapTex * bmpt = NewDefaultBitmapTex();
		bmpt->SetMapName(ttname);
		bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
		if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
			MessageBox(0, _T("No texture loaded."), _T("Error"),0);
		bmpt->GetUVGen()->SetMapChannel(1);
		bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
		ReplaceReference(160, bmpt);
		if (ttname)
			free(ttname);
	}

	// displacement
	tname = mat.getTexDisplacement();
	if (!tname  ||  strlen(tname)<1)
		texDisplacement = NULL;
	else
	{
		TCHAR * ttname = copyAnsiToTchar1(tname);
		BitmapTex * bmpt = NewDefaultBitmapTex();
		bmpt->SetMapName(ttname);
		bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
		if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
			MessageBox(0, _T("No texture loaded."), _T("Error"),0);
		bmpt->GetUVGen()->SetMapChannel(1);
		bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
		ReplaceReference(161, bmpt);
		if (ttname)
			free(ttname);
	}

	int cT = 0;
	setTexUpdateProgress(cT, nT);
	int nL = mat.getLayersNumber();
	for (int i=0; i<nL; i++)
	{
		tname = mat.getLayerTexRough(i);
		if (!tname  ||  strlen(tname)<1)
			texRough[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_ROUGHNESS-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexRefl0(i);
		if (!tname  ||  strlen(tname)<1)
			texRefl0[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_REFL0-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexRefl90(i);
		if (!tname  ||  strlen(tname)<1)
			texRefl90[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_REFL90-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexWeight(i);
		if (!tname  ||  strlen(tname)<1)
			texWeight[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_WEIGHT-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexLight(i);
		if (!tname  ||  strlen(tname)<1)
			texLight[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_LIGHT-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexNormal(i);
		if (!tname  ||  strlen(tname)<1)
			texNormal[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_NORMAL-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexTransm(i);
		if (!tname  ||  strlen(tname)<1)
			texTransm[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_TRANSM-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexAniso(i);
		if (!tname  ||  strlen(tname)<1)
			texAniso[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_ANISO-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
		//------
		tname = mat.getLayerTexAnisoAngle(i);
		if (!tname  ||  strlen(tname)<1)
			texAnisoAngle[i] = NULL;
		else
		{
			TCHAR * ttname = copyAnsiToTchar1(tname);
			BitmapTex * bmpt = NewDefaultBitmapTex();
			bmpt->SetMapName(ttname);
			bmpt->LoadMapFiles(GetCOREInterface()->GetTime());
			if (!bmpt->GetBitmap(GetCOREInterface()->GetTime()))
				MessageBox(0,_T("No texture loaded."),_T("Error"),0);
			bmpt->GetUVGen()->SetMapChannel(1);
			bmpt->GetUVGen()->Update(GetCOREInterface()->GetTime(), FOREVER);
			ReplaceReference(i*10+NOX_TEX_ANISO_ANGLE-1, bmpt);
			setTexUpdateProgress(++cT, nT);
			if (ttname)
				free(ttname);
		}
	}


	SetMtlFlag(MTL_DISPLAY_ENABLE_FLAGS, TRUE);
	ClearMtlFlag(MTL_TEX_DISPLAY_ENABLED);

	DestroyWindow(hTUInfoWindow);
	hTUInfoWindow = 0;
}


bool NOXMaterial::isNOXEmitter()
{
	if (mat.getSkyportal())
		return true;
	int ln = mat.getLayersNumber();
	for (int i=0; i<ln; i++)
	{
		if (mat.getLayerType(i)==1)
			return true;
	}
	return false;
}
