#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>
#define clamp(x,a,b)   max((a),min((x),(b)))


//----------		LAMBERT BRDF

Color4 MatLayer::getBRDFLambert(HitData & hd)
{
	if ((hd.in*hd.normal_geom)*(hd.out*hd.normal_geom) < 0)	// on the other side... not transparent
	{
		if (sssON)
		{
			hd.setFlagSSS();
			return Color4(PER_PI, PER_PI, PER_PI);
		}
		else
			return Color4(0,0,0);
	}
	else
		if (sssON)
			return Color4(0,0,0);

	return getColor0(hd.tU, hd.tV) * PER_PI;
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityLambert(HitData & hd)
{
	float sssMinus = sssON ? -1.0f : 1.0f;
	float cosTheta = hd.out*hd.normal_shade;
	float ctin = hd.in * hd.normal_shade;
	if ((ctin*cosTheta) * sssMinus < 0)
		return 0;

	if (sssON)
		hd.setFlagSSS();
	return fabs(cosTheta*PER_PI);
}

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionLambert1(HitData & hd, float &probability)
{

	Vector3d res;

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = ((float)rand()/(float)RAND_MAX);
	#endif

	Vector3d u,v,n;
	u = Vector3d::random();

	if (hd.in*hd.normal_shade >= 0)
		n = hd.normal_shade;
	else
		n = -hd.normal_shade;

	while (fabs(u*n) > 0.95)
		u = Vector3d::random();

	v = n^u;
	v.normalize();

	res = v*sqrt(a) + n*sqrt(1-a); 
	res.normalize();

	float k = n * res;
	probability = max(0.001f, k*PER_PI);
	hd.out = res;
	return res;
}

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionLambert2(HitData &hd, float &probability)
{
	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
		float b = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = getMarsagliaFloat();
		float b = getMarsagliaFloat();
	#endif

	float sx = a*2-1;
	float sy = b*2-1;
	float r;

	if (sx==0.0f && sy==0.0f)
	{
		probability = PER_PI;
		Vector3d res = hd.normal_shade;
		if (hd.in*hd.normal_shade < 0)
			res.invert();
		if (sssON)
			res.invert();
		return res;
	}

	if (sx >= -sy) 
	{
		if (sx > sy) 
			r = sx;
		else 
			r = sy;
	}
	else 
	{
		if (sx <= sy) 
			r = -sx;
		else 
			r = -sy;
	}

	Vector3d u,v,res;
	u = Vector3d::random();
	while (fabs(u*hd.normal_shade) > 0.95)
		u = Vector3d::random();
	v = hd.normal_shade^u;
	v.normalize();

	float costheta = sqrt(1.0f-r*r);
	probability = max(0.001f, costheta*PER_PI);

	res = hd.normal_shade*costheta + v*r;

	if (hd.in*hd.normal_shade < 0)
		res.invert();

	if (sssON)
	{
		hd.setFlagSSS();
		res.invert();
	}

	return res;
}

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionLambert(HitData &hd, float &probability)	
{
	// randomNewDirectionLambert2 should be used!!!

	Vector3d res;
	float a, angle;

	#ifdef RAND_PER_THREAD
		a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		a = ((float)rand()/(float)RAND_MAX);
	#endif

	angle = (float)(FastMath::asin(2*a-1)*0.5f + PI*0.25f);

	float sinTheta, cosTheta;
	sinTheta = FastMath::sin(angle);	
	cosTheta = FastMath::cos(angle);

	Vector3d u,v;
	u = Vector3d::random();
	while (fabs(u*hd.normal_shade) > 0.95)
		u = Vector3d::random();
	v = hd.normal_shade^u;
	v.normalize();

	float p = cosTheta;
	p*=PER_PI;

	if (hd.in*hd.normal_shade < 0)
		hd.normal_shade.invert();
	res = hd.normal_shade*cosTheta + v*sinTheta;
	hd.out = res;

	return res;
}

//-------------------------------------------------------------------------------------------

bool MatLayer::getBRDFandPDFsLambert(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b)
{
	float sssMplSign = sssON ? -1.0f : 1.0f;
	float cosThetaOutShade = hd.out*hd.normal_shade;
	float cosThetaInShade = hd.in*hd.normal_shade;
	float cosThetaOutGeom = hd.out*hd.normal_geom;
	float cosThetaInGeom = hd.in*hd.normal_geom;
	if ((cosThetaInShade*cosThetaOutShade)*sssMplSign < 0)
	{
		pdf_f = 0;
		pdf_b = 0;
	}
	else
	{
		pdf_f = max(0.001f, fabs(cosThetaOutShade*PER_PI));
		pdf_b = max(0.001f, fabs(cosThetaInShade*PER_PI));
	}
	if (cosThetaInGeom*cosThetaOutGeom < 0)
	{
		if (sssON)
		{
			hd.setFlagSSS();
			brdf = Color4(PER_PI, PER_PI, PER_PI);
		}
		else
			brdf = Color4(0,0,0);
	}
	else
	{
		if (sssON)
		{
			brdf = Color4(0,0,0);
		}
		else
			brdf = getColor0(hd.tU, hd.tV) * PER_PI;
	}

	#ifdef VERIFY_MATLAYER_BRDF_PDFS
		Color4 vbrdf = getBRDFLambert(hd);
		float vpdf_f = getProbabilityLambert(hd);
		HitData hdback = hd;
		hdback.swapDirections();
		float vpdf_b = getProbabilityLambert(hdback);
		if (vbrdf.r!=brdf.r  ||  vbrdf.g!=brdf.g  ||  vbrdf.b!=brdf.b)
			Logger::add("brdf verify failed on lambert");
		if (vpdf_f!=pdf_f)
			Logger::add("pdf forward verify failed on lambert");
		if (vpdf_b!=pdf_b)
			Logger::add("pdf backward verify failed on lambert");
	#endif

	return true;
}

//-------------------------------------------------------------------------------------------
