#include "max2013predef.h"
#include "NOXSunLight.h"
#include <decomp.h> 
#include "noxdebug.h"

#define SCHECK(r) if (r!=IO_OK) return r;

#define NOX_SUN_CHUNK_SUN 0x4200
#define NOX_SUN_CHUNK_LONGITUDE 0x4201
#define NOX_SUN_CHUNK_LATITUDE 0x4202
#define NOX_SUN_CHUNK_TURBIDITY 0x4203
#define NOX_SUN_CHUNK_AEROSOL 0x4204
#define NOX_SUN_CHUNK_MONTH 0x4205
#define NOX_SUN_CHUNK_DAY 0x4206
#define NOX_SUN_CHUNK_HOUR 0x4207
#define NOX_SUN_CHUNK_MINUTE 0x4208
#define NOX_SUN_CHUNK_GMT 0x4209
#define NOX_SUN_CHUNK_ON 0x420A
#define NOX_SUN_CHUNK_TARGET_HIDDEN 0x420B
#define NOX_SUN_CHUNK_SUN_ON 0x420C
#define NOX_SUN_CHUNK_SUN_OTHER_LAYER 0x420D
#define NOX_SUN_CHUNK_SUN_SIZE 0x420E
#define NOX_SUN_CHUNK_MODEL 0x4210
#define NOX_SUN_CHUNK_ALBEDO 0x4211

#define NOX_SUN_CHUNK_ENV_ENABLED 0x4271
#define NOX_SUN_CHUNK_ENV_HIDDEN 0x4272
#define NOX_SUN_CHUNK_ENV_POWER 0x4273
#define NOX_SUN_CHUNK_ENV_BLENDLAYER 0x4274
#define NOX_SUN_CHUNK_ENV_TEX 0x4275
#define NOX_SUN_CHUNK_ENV_AZIMUTH 0x4276
#define NOX_SUN_CHUNK_ENV_TEX_POST 0x427F
#define NOX_SUN_CHUNK_ENVTEX_BRIGHTNESS 0x4280
#define NOX_SUN_CHUNK_ENVTEX_CONTRAST 0x4281
#define NOX_SUN_CHUNK_ENVTEX_SATURATION 0x4282
#define NOX_SUN_CHUNK_ENVTEX_HUE 0x4283
#define NOX_SUN_CHUNK_ENVTEX_RED 0x4284
#define NOX_SUN_CHUNK_ENVTEX_GREEN 0x4285
#define NOX_SUN_CHUNK_ENVTEX_BLUE 0x4286
#define NOX_SUN_CHUNK_ENVTEX_GAMMA 0x4287
#define NOX_SUN_CHUNK_ENVTEX_INVERT 0x4288
#define NOX_SUN_CHUNK_ENVTEX_INTERPOLATE 0x4289

//------------------------------------------------------------------------------------------------------------------------

IOResult NOXSunTarget::Load(ILoad *iload)  
{ 
	return IO_OK; 
}

//------------------------------------------------------------------------------------------------------------------------

IOResult NOXSunTarget::Save(ISave *isave)  
{
	return IO_OK;
}

//------------------------------------------------------------------------------------------------------------------------

IOResult NOXSunLight::Load(ILoad *iload)  
{ 
	ADD_LOG_PARTS_MAIN("NOXSunLight::Load");

	IOResult res;
	int id;
	unsigned long rd;
	float tfloat;
	int tint;

	iload->RegisterPostLoadCallback(new NOXSunPostLoad(this));

	while (IO_OK == (res=iload->OpenChunk())) 
	{
		switch(id = iload->CurChunkID())  
		{
			case NOX_SUN_CHUNK_SUN:
				while (IO_OK == (res=iload->OpenChunk()))
				{
					switch(id = iload->CurChunkID())  
					{
						// LONGITUDE
						case NOX_SUN_CHUNK_LONGITUDE:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setLongitude(tfloat);
						}
						break;

						// LATITUDE
						case NOX_SUN_CHUNK_LATITUDE:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setLatitude(tfloat);
						}
						break;

						// TURBIDITY
						case NOX_SUN_CHUNK_TURBIDITY:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setTurbidity(tfloat);
						}
						break;

						// AEROSOL
						case NOX_SUN_CHUNK_AEROSOL:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setAerosol(tfloat);
						}
						break;

						// ALBEDO
						case NOX_SUN_CHUNK_ALBEDO:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setAlbedo(tfloat);
						}
						break;

						// MONTH
						case NOX_SUN_CHUNK_MONTH:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setMonth(tint);
						}
						break;

						// MONTH
						case NOX_SUN_CHUNK_DAY:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setDay(tint);
						}
						break;

						// HOUR
						case NOX_SUN_CHUNK_HOUR:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setHour(tint);
						}
						break;

						// MINUTE
						case NOX_SUN_CHUNK_MINUTE:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setMinute(tint);
						}
						break;

						// GMT
						case NOX_SUN_CHUNK_GMT:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setGMT(tint);
						}
						break;

						// TURNED ON
						case NOX_SUN_CHUNK_ON:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setSunSkyON(tint!=0);
						}
						break;

						// SUN ON
						case NOX_SUN_CHUNK_SUN_ON:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setSunON(tint!=0);
						}
						break;

						// SUN OTHER LAYER
						case NOX_SUN_CHUNK_SUN_OTHER_LAYER :
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setSunOtherLayerON(tint!=0);
						}
						break;

						// SUN SIZE
						case NOX_SUN_CHUNK_SUN_SIZE:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setSunSize(tfloat);
						}
						break;

						// MODEL
						case NOX_SUN_CHUNK_MODEL:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setModel(tint);
						}
						break;


						// TARGET HIDDEN
						case NOX_SUN_CHUNK_TARGET_HIDDEN:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setTargetHidden(tint!=0);
						}
						break;

						// ENV ENABLED
						case NOX_SUN_CHUNK_ENV_ENABLED:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setEnvEnabled(tint!=0);
						}
						break;

						// ENV MAP HIDDEN
						case NOX_SUN_CHUNK_ENV_HIDDEN:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							setEnvHidden(tint!=0);
						}
						break;

						// ENV POWER
						case NOX_SUN_CHUNK_ENV_POWER:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setEnvPower(tfloat);
						}
						break;

						// ENV BLEND LAYER
						case NOX_SUN_CHUNK_ENV_BLENDLAYER:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setEnvBlendLayer(tint);
						}
						break;

						// ENV TEX 
						case NOX_SUN_CHUNK_ENV_TEX:
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp)
								trsunsky.setEnvTexFilename(ctemp);
						break;

						// ENV AZIMUTH
						case NOX_SUN_CHUNK_ENV_AZIMUTH:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trsunsky.setEnvAzimuth(tfloat);
						}
						break;

						// ENV TEX POST 
						case NOX_SUN_CHUNK_ENV_TEX_POST:
							while (IO_OK == (res=iload->OpenChunk()))
							{
								switch(id = iload->CurChunkID())  
								{
									// TEX POST BRIGHTNESS
									case NOX_SUN_CHUNK_ENVTEX_BRIGHTNESS:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostBrightness(tfloat);
									break;

									// TEX POST CONTRAST
									case NOX_SUN_CHUNK_ENVTEX_CONTRAST:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostContrast(tfloat);
									break;

									// TEX POST SATURATION
									case NOX_SUN_CHUNK_ENVTEX_SATURATION:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostSaturation(tfloat);
									break;

									// TEX POST HUE
									case NOX_SUN_CHUNK_ENVTEX_HUE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostHue(tfloat);
									break;

									// TEX POST RED
									case NOX_SUN_CHUNK_ENVTEX_RED:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostRed(tfloat);
									break;

									// TEX POST GREEN
									case NOX_SUN_CHUNK_ENVTEX_GREEN:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostGreen(tfloat);
									break;

									// TEX POST BLUE
									case NOX_SUN_CHUNK_ENVTEX_BLUE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostBlue(tfloat);
									break;

									// TEX POST GAMMA
									case NOX_SUN_CHUNK_ENVTEX_GAMMA:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostGamma(tfloat);
									break;

									// TEX POST INVERT
									case NOX_SUN_CHUNK_ENVTEX_INVERT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostInvert(tint!=0);
									break;

									// TEX POST INTERPOLATE
									case NOX_SUN_CHUNK_ENVTEX_INTERPOLATE:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										trsunsky.setEnvTexPostInterpolateProbe(tint!=0);
									break;
								}
								iload->CloseChunk();
							}	// end post while
						break;

					}
					iload->CloseChunk();
				}	// end while in chunk sunsky
			break;
		}
		iload->CloseChunk();
	}

	return IO_OK;
}

//------------------------------------------------------------------------------------------------------------------------

IOResult NOXSunLight::Save(ISave *isave)  
{
	IOResult res;
	unsigned long wr;
	float tfloat;
	int tint;

	ADD_LOG_PARTS_MAIN("NOXSunLight::Save");

	// SUN
	isave->BeginChunk(NOX_SUN_CHUNK_SUN);

		// LONGITUDE
		isave->BeginChunk(NOX_SUN_CHUNK_LONGITUDE);
			tfloat = trsunsky.getLongitude();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// LATITUDE
		isave->BeginChunk(NOX_SUN_CHUNK_LATITUDE);
			tfloat = trsunsky.getLatitude();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// TURBIDITY
		isave->BeginChunk(NOX_SUN_CHUNK_TURBIDITY);
			tfloat = trsunsky.getTurbidity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// AEROSOL
		isave->BeginChunk(NOX_SUN_CHUNK_AEROSOL);
			tfloat = trsunsky.getAerosol();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ALBEDO
		isave->BeginChunk(NOX_SUN_CHUNK_ALBEDO);
			tfloat = trsunsky.getAlbedo();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// MONTH
		isave->BeginChunk(NOX_SUN_CHUNK_MONTH);
			tint = trsunsky.getMonth();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// DAY
		isave->BeginChunk(NOX_SUN_CHUNK_DAY);
			tint = trsunsky.getDay();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// HOUR
		isave->BeginChunk(NOX_SUN_CHUNK_HOUR);
			tint = trsunsky.getHour();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// MINUTE
		isave->BeginChunk(NOX_SUN_CHUNK_MINUTE);
			tint = trsunsky.getMinute();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// GMT
		isave->BeginChunk(NOX_SUN_CHUNK_GMT);
			tint = trsunsky.getGMT();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// TURNED ON
		isave->BeginChunk(NOX_SUN_CHUNK_ON);
			tint = trsunsky.getSunSkyON() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();
		
		// TARGET HIDDEN
		isave->BeginChunk(NOX_SUN_CHUNK_TARGET_HIDDEN);
			tint = trsunsky.getTargetHidden() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// SUN ON
		isave->BeginChunk(NOX_SUN_CHUNK_SUN_ON);
			tint = trsunsky.getSunON() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// SUN OTHER LAYER
		isave->BeginChunk(NOX_SUN_CHUNK_SUN_OTHER_LAYER);
			tint = trsunsky.getSunOtherLayerON() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// SUN SIZE
		isave->BeginChunk(NOX_SUN_CHUNK_SUN_SIZE);
			tfloat = trsunsky.getSunSize();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// MODEL
		isave->BeginChunk(NOX_SUN_CHUNK_MODEL);
			tint = trsunsky.getModel();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ENV ENABLED
		isave->BeginChunk(NOX_SUN_CHUNK_ENV_ENABLED);
			tint = trsunsky.getEnvEnabled() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ENV HIDDEN
		isave->BeginChunk(NOX_SUN_CHUNK_ENV_HIDDEN);
			tint = trsunsky.getEnvHidden() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ENV POWER
		isave->BeginChunk(NOX_SUN_CHUNK_ENV_POWER);
			tfloat = trsunsky.getEnvPower();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ENV BLEND LAYER
		isave->BeginChunk(NOX_SUN_CHUNK_ENV_BLENDLAYER);
			tint = trsunsky.getEnvBlendLayer();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ENV TEX
		char * cTex = trsunsky.getEnvTexFilename();
		if (cTex)
		{
			isave->BeginChunk(NOX_SUN_CHUNK_ENV_TEX);
				res = isave->WriteCString(cTex);
				SCHECK(res);
			isave->EndChunk();
		}

		// ENV AZIMUTH
		isave->BeginChunk(NOX_SUN_CHUNK_ENV_AZIMUTH);
			tfloat = trsunsky.getEnvAzimuth();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ENV TEX POST
		isave->BeginChunk(NOX_SUN_CHUNK_ENV_TEX_POST);

			// ENV TEX POST BRIGHTNESS
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_BRIGHTNESS);
				tfloat = trsunsky.getEnvTexPostBrightness();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST CONTRAST
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_CONTRAST);
				tfloat = trsunsky.getEnvTexPostContrast();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST SATURATION
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_SATURATION);
				tfloat = trsunsky.getEnvTexPostSaturation();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST HUE
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_HUE);
				tfloat = trsunsky.getEnvTexPostHue();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST RED
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_RED);
				tfloat = trsunsky.getEnvTexPostRed();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST GREEN
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_GREEN);
				tfloat = trsunsky.getEnvTexPostGreen();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST BLUE
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_BLUE);
				tfloat = trsunsky.getEnvTexPostBlue();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST GAMMA
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_GAMMA);
				tfloat = trsunsky.getEnvTexPostGamma();
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST INVERT
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_INVERT);
				tint = trsunsky.getEnvTexPostInvert() ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// ENV TEX POST INTERPOLATE PROBE
			isave->BeginChunk(NOX_SUN_CHUNK_ENVTEX_INTERPOLATE);
				tint = trsunsky.getEnvTexPostInterpolateProbe() ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

		isave->EndChunk();
		// END ENV TEX POST


	isave->EndChunk();	// end sun

	return IO_OK; 
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunPostLoad::proc(ILoad * iload)
{
	if (!sun)
		return;

	sun->setTargetHidden(sun->trsunsky.targetHidden);
	sun->setEnvHidden(sun->trsunsky.envMapHidden);
	
	NOXSunTarget * target = sun->getTargetObject();
	if (target)
	{
		target->updateEnvMap();
	}
}

//------------------------------------------------------------------------------------------------------------------------
