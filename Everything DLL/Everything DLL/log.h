#ifndef ___LOG_H
#define ___LOG_H

#include <windows.h>
#include "DLL.h"

class Logger
{
private:
	static char * dir;
	static bool opened;
	static bool initialize();
	static HANDLE hFile;
	static bool do_not_log;

public:
	DECLDIR static bool add(char * what);
	static bool closeFile();
	static bool setLogDir(char * newDir);
	DECLDIR static void setLogsOn(bool enabled);
};

#endif
