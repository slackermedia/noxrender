#define _CRT_RAND_S
#include <windows.h>
#include <math.h>
#include <gdiplus.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>
#include "log_settings.h"

#define clamp(x,a,b)   max((a),min((x),(b)))
inline void mult_sign(float &dst, float &src);
#define GLOSS_USE_BECKMANN

//-------------------------------------------------------------------------------------------
// MATERIAL

clock_t localticks;

MaterialNox::MaterialNox():
layers(16)
{
	id = 0;
	opacity = 1.0f;
	use_tex_opacity = false;
	hasEmitters = false;
	hasShade = false;
	hasFakeGlass = false;
	hasInvisibleEmitter = false;
	isSkyPortal = false;
	name = NULL;
	preview = NULL;
	prSceneIndex = 0;
	blendIndex = 0;
	displ_depth = 1.0f;
	displ_subdivs_lvl = 6;
	use_tex_displacement = false;
	displ_continuity = false;
	displ_ignore_scale = false;
	displ_shift = 0.0f;
	displ_normal_mode = NM_SMOOTH;
	isMatteShadow = false;
}

MaterialNox::~MaterialNox()
{
	if (name)
		free(name);
	name = NULL;
	tex_opacity.setEmptyTex();
	tex_displacement.setEmptyTex();
}

bool MaterialNox::setName(char * newname)
{
	char * cpName = copyString(newname);
	CHECK(cpName);
	if (name)
		free(name);
	name = cpName;
	return true;
}

bool MaterialNox::createEmptyPreview()
{
	ImageByteBuffer * newpreview = new ImageByteBuffer();
	CHECK(newpreview);
	if (preview)
		delete preview;
	preview = newpreview;
	return true;
}

bool MaterialNox::addMatLayer(MatLayer &mlay)
{
	layers.add(mlay, &(MatLayer::runConstructor));
	layers.createArray();
	return true;
}

bool MaterialNox::deleteBuffers(bool delName, bool delPreview, bool delTextures, bool delIES)
{
	if (delName)
	{
		if (name)
			free(name);
		name = NULL;
	}

	if (delPreview)
	{
		if (preview)
		{
			preview->freeBuffer();
			preview->freeTimeStamp();
		}
		delete preview;
		preview = NULL;
	}

	if (delTextures)
	{
		for (int i=0; i<layers.objCount; i++)
		{
			layers[i].deleteAllTextures();
		}
		tex_opacity.setEmptyTex();
		tex_displacement.setEmptyTex();
	}

	if (delIES)
	{
		for (int i=0; i<layers.objCount; i++)
		{
			if (layers[i].ies)
			{
				layers[i].ies->releaseStuff();
				delete layers[i].ies;
				layers[i].ies = NULL;
			}
		}
	}


	return true;
}

void MaterialNox::updateFlags()
{
	hasEmitters = false;
	hasShade = false;
	hasInvisibleEmitter = false;
	hasFakeGlass = false;
	hasWeightTexShade = false;
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * l = &(layers[i]);
		if (l->type == MatLayer::TYPE_EMITTER)
			hasEmitters = true;
		if (l->type == MatLayer::TYPE_EMITTER  &&  l->emissInvisible)
			hasInvisibleEmitter = true;
		if (l->type == MatLayer::TYPE_SHADE)
			hasShade = true;
		if (l->type == MatLayer::TYPE_SHADE   &&   l->fakeGlass)
			hasFakeGlass = true;
		if (l->use_tex_weight  &&  l->tex_weight.isValid())
			hasWeightTexShade = true;
	}
}

void MaterialNox::checkProperties(bool &use_normals, bool &use_transmission, bool &use_sss, bool &use_emission)
{
	use_normals = false;
	use_transmission = false;
	use_sss = false;
	use_emission = false;

	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * layer = &(layers[i]);
		if (layer->type == MatLayer::TYPE_EMITTER)
		{
			use_emission = true;
			continue;
		}
		if (layer->tex_normal.isValid()  &&  layer->use_tex_normal)
			use_normals = true;
		if (layer->transmOn)
			use_transmission = true;
		if (layer->sssON  &&  layer->roughness==1.0f)
			use_sss = true;
	}
}

void MaterialNox::updateWeights()
{
	weightsSumEmit = 0;
	weightsSumShade = 0;
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * l = &(layers[i]);
		if (l->type == MatLayer::TYPE_EMITTER)
			weightsSumEmit += l->contribution;
		if (l->type == MatLayer::TYPE_SHADE)
			weightsSumShade += l->contribution;
	}
}

float MaterialNox::evalSumWeights(float tu, float tv)
{
	float sum = 0.0f;
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * l = &(layers[i]);
		sum += l->getWeight(tu,tv);
	}
	return sum;
}

float MaterialNox::evalSumWeightsShade(float tu, float tv)
{
	float sum = 0.0f;
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * l = &(layers[i]);
		if (l->type == MatLayer::TYPE_SHADE)
			sum += l->getWeight(tu,tv);
	}
	return sum;
}

float MaterialNox::evalSumWeightsEmitter(float tu, float tv)
{
	float sum = 0.0f;
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * l = &(layers[i]);
		if (l->type == MatLayer::TYPE_EMITTER)
			sum += l->getWeight(tu,tv);
	}
	return sum;
}

float MaterialNox::getOpacity(float tu, float tv)
{
	if (use_tex_opacity &&  tex_opacity.isValid())
		return min(1.0f, max(0.0f, tex_opacity.getColor(tu,tv).r));
	return opacity;
}

float MaterialNox::getDisplacement(float tu, float tv)
{
	if (use_tex_displacement  &&  tex_displacement.isValid())
		return min(1.0f, max(0.0f, tex_displacement.getColor(tu,tv).r));
	return 0.0f;
}

void MaterialNox::prepareToRender()
{
	hasEmitters = false;
	hasShade = false;
	hasWeightTexShade = false;
	weightsSumEmit = 0;
	weightsSumShade = 0;
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * l = &(layers[i]);
		if (l->type == MatLayer::TYPE_EMITTER)
		{
			weightsSumEmit += l->contribution;
			hasEmitters = true;
		}
		if (l->type == MatLayer::TYPE_SHADE)
		{
			weightsSumShade += l->contribution;
			hasShade = true;
		}
		if (l->use_tex_weight  &&  l->tex_weight.isValid())
			hasWeightTexShade = true;
	}

	// and gamma on colors
	float gamma = 2.2f;
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * l = &(layers[i]);
		l->gAbsColor.r = pow(l->absColor.r, gamma);
		l->gAbsColor.g = pow(l->absColor.g, gamma);
		l->gAbsColor.b = pow(l->absColor.b, gamma);
		l->gColor.r    = pow(l->color.r, gamma);
		l->gColor.g    = pow(l->color.g, gamma);
		l->gColor.b    = pow(l->color.b, gamma);
		l->gRefl0.r    = pow(l->refl0.r, gamma);
		l->gRefl0.g    = pow(l->refl0.g, gamma);
		l->gRefl0.b    = pow(l->refl0.b, gamma);
		l->gRefl90.r   = pow(l->refl90.r, gamma);
		l->gRefl90.g   = pow(l->refl90.g, gamma);
		l->gRefl90.b   = pow(l->refl90.b, gamma);
		l->gTransmColor.r   = pow(l->transmColor.r, gamma);
		l->gTransmColor.g   = pow(l->transmColor.g, gamma);
		l->gTransmColor.b   = pow(l->transmColor.b, gamma);
	}
}

Color4 MaterialNox::evalAllLayersFakeGlassAndOpacityAttenuation(HitData &hd, float &opac)
{
	Color4 result = Color4(0,0,0);
	int nLayers = layers.objCount;
	opac = getOpacity(hd.tU, hd.tV);
	if (hasFakeGlass)
	{
		for (int i=0; i<nLayers; i++)
		{
			MatLayer * mlay = &(layers[i]);
			if (mlay->type!=MatLayer::TYPE_SHADE)
				continue;
			if (mlay->getRough(hd.tU, hd.tV)>0)
				continue;
			if (!mlay->transmOn)
				continue;
			if (!mlay->fakeGlass)
				continue;
			Color4 brdf = mlay->getBRDF(hd);
			float w = mlay->getWeight(hd.tU, hd.tV);
			result += brdf*w;
		}
	}
	result = result*opac + Color4(1,1,1)*(1.0f-opac);
	return result;
}

MatLayer * MaterialNox::getRandomFakeGlassLayer(float tu, float tv, float &probability)
{
	if (!hasFakeGlass)
	{
		probability = 1;
		return NULL;
	}

	int k;
	float temp = 0;
	float mm = weightsSumShade;
	#ifdef RAND_PER_THREAD
		float r = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float r = ((float)rand()/(float)RAND_MAX);
	#endif
	bool OK = false;
	MatLayer * mLay;

	if (hasWeightTexShade)
	{
		mm = 0;
		for (k=0; k<layers.objCount; k++)
		{
			mLay = &(layers[k]);
			if (mLay->transmOn  &&  mLay->fakeGlass)
				mm += mLay->getWeight(tu,tv)*100;
		}
		if (mm <= 0)
		{
			probability = 1;
			return NULL;
		}
	}

	r = r * mm;
	for (k=0; k<=layers.objCount; k++)
	{
		mLay = &(layers[k]);
		if (!(mLay->transmOn  &&  mLay->fakeGlass))
			continue;
		temp += mLay->getWeight(tu,tv)*100;
		if (r < temp)
		{
			OK = true;
			break;
		}
	}

	if (OK)
	{
		probability = 100/mm;
		return mLay;
	}

	probability = 1;
	return NULL;
}

MatLayer * MaterialNox::getRandomGlassLayer(float tu, float tv, float &probability, float maxRough)
{
	int k;
	float temp = 0;
	float mm = weightsSumShade;
	#ifdef RAND_PER_THREAD
		float r = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float r = ((float)rand()/(float)RAND_MAX);
	#endif
	bool OK = false;
	MatLayer * mLay;

	if (hasWeightTexShade)
	{
		mm = 0;
		for (k=0; k<layers.objCount; k++)
		{
			mLay = &(layers[k]);
			if (mLay->transmOn  &&  mLay->getRough(tu, tv)<=maxRough)
				mm += mLay->getWeight(tu,tv)*100;
		}
		if (mm <= 0)
		{
			probability = 1;
			return NULL;
		}
	}

	r = r * mm;
	for (k=0; k<=layers.objCount; k++)
	{
		mLay = &(layers[k]);
		if (!(mLay->transmOn  &&  mLay->getRough(tu, tv)<=maxRough))
			continue;
		temp += mLay->getWeight(tu,tv)*100;
		if (r < temp)
		{
			OK = true;
			break;
		}
	}

	if (OK)
	{
		probability = 100/mm;
		return mLay;
	}

	probability = 1;
	return NULL;
}

MatLayer * MaterialNox::getRandomShadeLayer(float tu, float tv, float &probability)
{
	if (!hasShade)
	{
		probability = 1;
		return NULL;
	}

	float sumWeights = 0;
	int index[16];
	float weights[16];
	MatLayer * mLay;
	int k, i=0;
	for (k=0; k<layers.objCount; k++)
	{
		mLay = &(layers[k]);
		if (mLay->type != MatLayer::TYPE_SHADE)
			continue;
		index[i] = k;
		weights[i] = mLay->getWeight(tu,tv)*100;
		sumWeights += weights[i];
		i++;
	}

	if (sumWeights <= 0)
	{
		probability = 1;
		return NULL;
	}

	float temp = 0;
	#ifdef RAND_PER_THREAD
		float r = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float r = ((float)rand()/(float)RAND_MAX);
	#endif
	bool OK = false;


	r = r * sumWeights;
	int l=0;
	for (k=0; k<i; k++)
	{
		temp += weights[k];
		if (r <= temp)
		{
			OK = true;
			l = k;
			break;
		}
	}

	if (OK)
	{
		probability = 100/sumWeights;
		return &(layers[index[l]]);
	}

	probability = 1;
	return NULL;
}

MatLayer * MaterialNox::getRandomAnyLayer(float tu, float tv, float &probability)
{
	if (layers.objCount<1)
	{
		probability = 1;
		return NULL;
	}

	float sumWeights = 0;
	int index[16];
	float weights[16];
	MatLayer * mLay;
	int k, i=0;
	for (k=0; k<layers.objCount; k++)
	{
		mLay = &(layers[k]);
		index[i] = k;
		weights[i] = mLay->getWeight(tu,tv)*100;
		sumWeights += weights[i];
		i++;
	}

	if (sumWeights <= 0)
	{
		probability = 1;
		return NULL;
	}

	float temp = 0;
	#ifdef RAND_PER_THREAD
		float r = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float r = ((float)rand()/(float)RAND_MAX);
	#endif
	bool OK = false;


	r = r * sumWeights;
	int l=0;
	for (k=0; k<i; k++)
	{
		temp += weights[k];
		if (r <= temp)
		{
			OK = true;
			l = k;
			break;
		}
	}

	if (OK)
	{
		probability = 100/sumWeights;
		return &(layers[index[l]]);
	}

	probability = 1;
	return NULL;
}

MatLayer * MaterialNox::getRandomEmissionLayer(float tu, float tv, float &probability)
{
	int k;
	float temp = 0;
	float mm = 0;
	#ifdef RAND_PER_THREAD
		float r = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float r = ((float)rand()/(float)RAND_MAX);
	#endif
	bool OK = false;
	MatLayer * mLay;

	for (k=0; k<layers.objCount; k++)
	{
		mLay = &(layers[k]);
		if (mLay->type == MatLayer::TYPE_EMITTER)
			mm += mLay->getWeight(tu,tv)* mLay->power;
	}

	if (mm <= 0)
	{
		probability = 1;
		return NULL;
	}

	r = r * (float)mm * 0.9999999999f;
	for (k=0; k<layers.objCount; k++)
	{
		mLay = &(layers[k]);
		if (mLay->type != MatLayer::TYPE_EMITTER)
			continue;
		temp += mLay->getWeight(tu,tv)* mLay->power;
		if (r <= (float)temp)
		{
			OK = true;
			break;
		}
	}

	if (OK)
	{
		probability = mLay->getWeight(tu,tv)* mLay->power/(float)mm;
		return mLay;
	}
	probability = 1;
	return NULL;
}

Color4 MaterialNox::getRandomBRDF(const Vector3d &in, const Vector3d &out, const Vector3d &normal, float &probability, const Point2d &tUV)
{
	int k;
	int temp = 0;
	#ifdef RAND_PER_THREAD
		float r = Raytracer::getRandomGeneratorForThread()->getRandomFloat()*100.0f;
	#else
		float r = ((float)rand()*100.0f/(float)RAND_MAX);
	#endif
	bool OK = false;
	MatLayer * mLay;
	for (k=0; k<layers.objCount; k++)
	{
		mLay = &(layers[k]);
		if (r <= mLay->contribution + temp) 
		{
			if (mLay->type == MatLayer::TYPE_SHADE)
				OK = true;
			break;
		}
		temp += mLay->contribution;
	}

	if (OK)
	{
		probability = mLay->contribution/100.0f;
		HitData hd;
		hd.in = in;
		hd.out = out;
		hd.normal_shade = normal;
		hd.tU = tUV.x;
		hd.tV = tUV.y;
		return mLay->getBRDF(hd)*probability;
	}

	probability = 1;
	return Color4(0,0,0);
}

Color4 MaterialNox::getAllLayersBRDF(HitData &hd)
{
	Color4 res = Color4(0,0,0);
	unsigned int nLayers = (unsigned int)layers.objCount;
	if (nLayers == 1)
	{
		MatLayer * mlay = &(layers[0]);
		float lw = mlay->getWeight(hd.tU, hd.tV);
		Color4 tCol = Color4(0,0,0);
		if (lw > 0.0f)
			tCol = mlay->getBRDF(hd);
		res = tCol * lw;
		return res;
	}

	float sumpdf2 = 0;
	float sumWeights = 0;
	for (unsigned int i=0; i<nLayers; i++)
	{
		MatLayer * mlay = &(layers[i]);
		float lw = mlay->getWeight(hd.tU, hd.tV);
		sumWeights += lw;
		float pdf = mlay->getProbability(hd);
		sumpdf2 += pdf*pdf;
		Color4 tCol = Color4(0,0,0);
		if (lw > 0.0f)
			tCol = mlay->getBRDF(hd);
		res += tCol * (pdf*pdf);
	}

	if (sumpdf2 <= 0.0000001f)
		return Color4(0,0,0);

	res *= (1.0f/sumpdf2) * sumWeights;

	return res;
}

//----------		PRE RENDER PROCESS CONST VALUES

void MatLayer::preRenderProcess()
{
}

bool MatLayer::isDeltaDistribution() 
{ 
	// shouldn't be used - rough may be from tex
	return (roughness < 0.0001f); 
}

//==========================================================================================
//------------------------------------------------------------------------------------------
//----------		GENERAL BRDF AND PDF

Vector3d MatLayer::randomNewDirection(HitData & hd, float &probability)
{
	float rghn = getRough(hd.tU, hd.tV);
	if (rghn < 0.0001f)
		return randomNewDirectionIdealReflectionTransmission(hd, probability);
	if (rghn > 0.999f)
		if (transmOn)
			return randomNewDirectionLambertTransmission(hd, probability);
		else
			return randomNewDirectionLambert2(hd, probability);
	if (!transmOn && getAnisotropy(hd.tU, hd.tV) != 0.0f)
		return randomNewDirectionMicrofacetAniso(hd, probability);

	#ifdef GLOSS_USE_BECKMANN
		return randomNewDirectionWalterMicrofacetBeckmann(hd, probability);
	#endif
	return randomNewDirectionWalterMicrofacetGGX(hd, probability);
}

Color4 MatLayer::getBRDF(HitData & hd)
{
	float rghn = getRough(hd.tU, hd.tV);
	if (rghn< 0.0001f)
		return getBRDFIdealReflectionTransmission(hd);
	if (rghn> 0.999f)
	{
		if (transmOn)
			return getBRDFLambertTransmission(hd);
		else
			return getBRDFLambert(hd);
	}

	if (!transmOn && getAnisotropy(hd.tU, hd.tV) != 0.0f)
		return getBRDFMicrofacetAniso(hd);

	#ifdef GLOSS_USE_BECKMANN
		return getBRDFWalterMicrofacetBeckmann(hd);
	#endif
	return getBRDFWalterMicrofacetGGX(hd);
}

float MatLayer::getProbability(HitData & hd)
{
	if (type == TYPE_EMITTER)
		return getProbabilityLambert(hd);
	float rghn = getRough(hd.tU, hd.tV);
	if (rghn < 0.0001f)
		return getProbabilityIdealReflectionTransmission(hd);
	if (rghn > 0.999f)
	{
		if (transmOn)
			return getProbabilityLambertTransmission(hd);
		else
			return getProbabilityLambert(hd);
	}

	if (!transmOn && getAnisotropy(hd.tU, hd.tV) != 0.0f)
		return getProbabilityMicrofacetAniso(hd);

	#ifdef GLOSS_USE_BECKMANN
		return getProbabilityWalterMicrofacetBeckmann(hd);
	#endif
	return getProbabilityWalterMicrofacetGGX(hd);
}

//#define BRDF_PDF_AND_BACK_OLD_WAY

bool MatLayer::getBRDFandPDFs(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b)
{
	#ifdef BRDF_PDF_AND_BACK_OLD_WAY
		brdf = getBRDF(hd);
		pdf_f = getProbability(hd);
		HitData hd2 = hd;
		hd2.swapDirections();
		pdf_b = getProbability(hd2);
		return true;
	#else
		if (type == TYPE_EMITTER)
			return getBRDFandPDFsLambert(hd, brdf, pdf_f, pdf_b);
		float rghn = getRough(hd.tU, hd.tV);
		if (rghn< 0.0001f)
			return getBRDFandPDFsIdealReflectionTransmission(hd, brdf, pdf_f, pdf_b);
		if (rghn> 0.999f)
			if (transmOn)
				return getBRDFandPDFsLambertTransmission(hd, brdf, pdf_f, pdf_b);
			else
				return getBRDFandPDFsLambert(hd, brdf, pdf_f, pdf_b);

		if (!transmOn && getAnisotropy(hd.tU, hd.tV) != 0.0f)
			return getBRDFandPDFsMicrofacetAniso(hd, brdf, pdf_f, pdf_b);

		#ifdef GLOSS_USE_BECKMANN
			return getBRDFandPDFsBeckmann(hd, brdf, pdf_f, pdf_b);
		#endif
		return getBRDFandPDFsGGX(hd, brdf, pdf_f, pdf_b);
	#endif
}

//----------		HOMOGENOUS PDF

Vector3d MatLayer::randomNewDirectionHomogenous(HitData & hd, float &probability)
{
	probability = 1;	// wrong for sure
	Vector3d res = Vector3d::random();
	if ((res * hd.normal_shade)   *   (hd.in * hd.normal_shade)  <  0)
		res.invert();
	hd.out = res;
	return res;
}

float MatLayer::getProbabilityHomogenous(HitData & hd)
{
	return 1;	// wrong for sure
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
// MATERIAL LAYER

MatLayer::MatLayer()
{
	#ifdef LOG_CONSTR_MATLAYER
		char buf[64];
		sprintf_s(buf, 64, "MatLayer constructor at %lld", (INT_PTR)this);
		Logger::add(buf);
	#endif
	use_tex_col0   = false;
	use_tex_col90  = false;
	use_tex_rough  = false;
	use_tex_weight = false;
	use_tex_light  = false;
	use_tex_normal = false;
	use_tex_transm = false;
	use_tex_aniso = false;
	use_tex_aniso_angle = false;
	refl90 = Color4(1,1,1);
	refl0 = Color4(0.75f,0.75f,0.75f);
	fresnel = Fresnel();
	type = TYPE_SHADE;
	contribution = 100;
	color = Color4(0,0,0);
	emitterTemperature = 6500;
	power = 1.0f;
	unit = EMISSION_WATT;
	roughness = 1.0f;
	transmColor = Color4(1,1,1);
	absColor = Color4(1,1,1);
	absDist = 1.0f;
	dispersionValue = 1.0f;
	absOn = false;
	transmOn = false;
	fakeGlass = false;
	dispersionOn = false;
	brdf_type = BRDF_TYPE_GGX;
	gRefl0 = Color4(0,0,0);
	gRefl90 = Color4(0,0,0);
	gAbsColor = Color4(0,0,0);
	gColor = Color4(0,0,0);
	gTransmColor = Color4(0,0,0);
	transluencyWeight = 0.5f;
	sssON = false;
	sssDens = 0.0f;
	sssCollDir = 0.0f;
	connect90 = true;
	anisotropy = 0.0f;
	aniso_angle = 0.0f;
	emissInvisible = false;
	
	tex_col0.texMod.gamma = 2.2f;
	tex_col90.texMod.gamma = 2.2f;
	tex_rough.texMod.gamma = 1.0f;
	tex_light.texMod.gamma = 2.2f;
	tex_weight.texMod.gamma = 1.0f;
	tex_normal.texMod.gamma = 1.0f;
	tex_transm.texMod.gamma = 2.2f;
	tex_aniso.texMod.gamma = 1.0f;
	tex_aniso_angle.texMod.gamma = 1.0f;

	tex_col0.texMod.updateGamma(2.2f);
	tex_col90.texMod.updateGamma(2.2f);
	tex_rough.texMod.updateGamma(1.0f);
	tex_light.texMod.updateGamma(2.2f);
	tex_weight.texMod.updateGamma(1.0f);
	tex_normal.texMod.updateGamma(1.0f);
	tex_transm.texMod.updateGamma(2.2f);
	tex_aniso.texMod.updateGamma(1.0f);
	tex_aniso_angle.texMod.updateGamma(1.0f);

	ies = NULL;
}


MatLayer::~MatLayer()
{
	#ifdef LOG_CONSTR_MATLAYER
		char buf[64];
		sprintf_s(buf, 64, "MatLayer destructor at %lld", (INT_PTR)this);
		Logger::add(buf);
	#endif
	if (ies)
	{
		ies->releaseStuff();
		delete ies;
		ies = NULL;
	}
}

MatLayer::MatLayer(const MatLayer & mlay)
{
	#ifdef LOG_CONSTR_MATLAYER
		char buf[64];
		sprintf_s(buf, 64, "MatLayer copy constructor at %lld from %lld ", (INT_PTR)this, (INT_PTR)&mlay);
		Logger::add(buf);
	#endif
	MatLayer();
	ies = NULL;
	*this = mlay;
}

void MatLayer::runConstructor(void * addr)
{
	if (!addr)
		return;
	MatLayer * llayer = new (addr) MatLayer();
}

MatLayer & MatLayer::operator= (const MatLayer & mlay)
{
	if (this == &mlay) 
		return *this;

	#ifdef LOG_CONSTR_MATLAYER
		char buf[64];
		sprintf_s(buf, 64, "MatLayer operator= at %lld from %lld", (INT_PTR)this, (INT_PTR)&mlay);
		Logger::add(buf);
	#endif

	refl0 = mlay.refl0;
	refl90 = mlay.refl90;
	absColor = mlay.absColor;
	color = mlay.color;
	transmColor = mlay.transmColor;
	gRefl0 = mlay.gRefl0;
	gRefl90 = mlay.gRefl90;
	gAbsColor = mlay.gAbsColor;
	gColor = mlay.gColor;
	gTransmColor = mlay.gTransmColor;

	type = mlay.type;
	contribution = mlay.contribution;
	emissInvisible = mlay.emissInvisible;

	use_tex_weight = mlay.use_tex_weight;
	use_tex_col0 = mlay.use_tex_col0;
	use_tex_col90 = mlay.use_tex_col90;
	use_tex_rough = mlay.use_tex_rough;
	use_tex_light = mlay.use_tex_light;
	use_tex_normal = mlay.use_tex_normal;
	use_tex_transm = mlay.use_tex_transm;
	use_tex_aniso = mlay.use_tex_aniso;
	use_tex_aniso_angle = mlay.use_tex_aniso_angle;

	tex_weight = mlay.tex_weight;
	tex_col0 = mlay.tex_col0;
	tex_col90 = mlay.tex_col90;
	tex_rough = mlay.tex_rough;
	tex_light = mlay.tex_light;
	tex_normal = mlay.tex_normal;
	tex_transm = mlay.tex_transm;
	tex_aniso = mlay.tex_aniso;
	tex_aniso_angle = mlay.tex_aniso_angle;

	connect90 = mlay.connect90;
	roughness = mlay.roughness;
	fresnel = mlay.fresnel;
	brdf_type = mlay.brdf_type;
	anisotropy = mlay.anisotropy;
	aniso_angle = mlay.aniso_angle;

	absDist = mlay.absDist;
	dispersionValue = mlay.dispersionValue;
	absOn = mlay.absOn;
	transmOn = mlay.transmOn;
	fakeGlass = mlay.fakeGlass;
	dispersionOn = mlay.dispersionOn;
	transluencyWeight = mlay.transluencyWeight;

	power = mlay.power;
	emitterTemperature = mlay.emitterTemperature;
	unit = mlay.unit;
	sssON = mlay.sssON;
	sssDens = mlay.sssDens;
	sssCollDir = mlay.sssCollDir;

	if (ies)
	{
		ies->releaseStuff();
		delete ies;
	}
	if (mlay.ies)
	{
		ies = new IesNOX();
		*ies = *mlay.ies;
	}
	else
		ies = NULL;

	return *this;
}

bool MatLayer::addIes()
{
	if (ies)
	{
		ies->releaseStuff();
		delete ies;
		ies = NULL;
	}
	ies = new IesNOX();
	if (ies)
		return true;
	return false;
}

void MatLayer::removeIes()
{
	if (ies)
	{
		ies->releaseStuff();
		delete ies;
		ies = NULL;
	}
}

bool MatLayer::resetValues(bool delTex)
{
	use_tex_col0   = false;
	use_tex_col90  = false;
	use_tex_rough  = false;
	use_tex_weight = false;
	use_tex_light  = false;
	use_tex_normal = false;
	use_tex_transm = false;
	use_tex_aniso = false;
	use_tex_aniso_angle = false;
	refl90 = Color4(1,1,1);
	refl0 = Color4(0.75f,0.75f,0.75f);
	type = TYPE_SHADE;
	contribution = 100;
	color = Color4(0,0,0);
	emitterTemperature = 6500;
	power = 1.0f;
	connect90 = true;
	unit = EMISSION_WATT;
	roughness = 1.0f;
	transmColor = Color4(1.0f, 1.0f, 1.0f);
	absColor = Color4(1,1,1);
	absDist = 1.0f;
	absOn = false;
	transmOn = false;
	fakeGlass = false;
	dispersionOn = false;
	fresnel.IOR = 1.6f;
	sssON = false;
	sssDens = 0.0f;
	sssCollDir = 0.0f;
	emissInvisible = false;

	if (delTex)
	{
		TexModifer tmod;

		tex_col0.setEmptyTex();
		tex_col90.setEmptyTex();
		tex_rough.setEmptyTex();
		tex_light.setEmptyTex();
		tex_weight.setEmptyTex();
		tex_normal.setEmptyTex();
		tex_transm.setEmptyTex();
		tex_aniso.setEmptyTex();
		tex_aniso_angle.setEmptyTex();

		tex_col0.texMod = tmod;
		tex_col90.texMod = tmod;
		tex_rough.texMod = tmod;
		tex_light.texMod = tmod;
		tex_weight.texMod = tmod;
		tex_normal.texMod = tmod;
		tex_transm.texMod = tmod;
		tex_aniso.texMod = tmod;
		tex_aniso_angle.texMod = tmod;
	}

	tex_col0.texMod.gamma = 2.2f;
	tex_col90.texMod.gamma = 2.2f;
	tex_rough.texMod.gamma = 1.0f;
	tex_light.texMod.gamma = 2.2f;
	tex_weight.texMod.gamma = 1.0f;
	tex_normal.texMod.gamma = 1.0f;
	tex_transm.texMod.gamma = 2.2f;
	tex_aniso.texMod.gamma = 1.0f;
	tex_aniso_angle.texMod.gamma = 1.0f;

	if (ies)
	{
		ies->releaseStuff();
		delete ies;
	}
	ies = NULL;

	return false;
}

//-------------------------------------------------------------------------------------------

MaterialNox * MaterialNox::copy()
{
	MaterialNox * mat = new MaterialNox();
	CHECK(mat);

	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer mlay;
		mat->layers.add(mlay, &(MatLayer::runConstructor));
		mat->layers.createArray();
		mat->layers[mat->layers.objCount-1].copyFromOtherWithoutTextures(&(layers[i]));
	}

	for (int i=0; i<layers.objCount; i++)
	{
		mat->layers[i].tex_col0 = layers[i].tex_col0;
		mat->layers[i].tex_col90 = layers[i].tex_col90;
		mat->layers[i].tex_rough = layers[i].tex_rough;
		mat->layers[i].tex_weight = layers[i].tex_weight;
		mat->layers[i].tex_light = layers[i].tex_light;
		mat->layers[i].tex_normal = layers[i].tex_normal;
		mat->layers[i].tex_transm = layers[i].tex_transm;
		mat->layers[i].tex_aniso = layers[i].tex_aniso;
		mat->layers[i].tex_aniso_angle = layers[i].tex_aniso_angle;
	}

	mat->tex_opacity = tex_opacity;
	mat->tex_displacement = tex_displacement;

	mat->use_tex_opacity = use_tex_opacity;
	mat->opacity = opacity;
	mat->id = id;
	mat->hasEmitters = hasEmitters;
	mat->hasShade = hasShade;
	mat->hasFakeGlass = hasFakeGlass;
	mat->hasInvisibleEmitter = hasInvisibleEmitter;
	mat->hasWeightTexShade = hasWeightTexShade;
	mat->weightsSumEmit = weightsSumEmit;
	mat->weightsSumShade = weightsSumShade;
	mat->prSceneIndex = prSceneIndex;
	mat->blendIndex = blendIndex;
	mat->isSkyPortal = isSkyPortal;
	mat->use_tex_displacement = use_tex_displacement;
	mat->displ_depth = displ_depth;
	mat->displ_subdivs_lvl = displ_subdivs_lvl;
	mat->displ_continuity = displ_continuity;
	mat->displ_ignore_scale = displ_ignore_scale;
	mat->displ_shift = displ_shift;
	mat->displ_normal_mode = displ_normal_mode;
	mat->isMatteShadow = isMatteShadow;

	mat->name = copyString(name);

	if (preview)
		mat->preview = preview->copy();

	return mat;
}


bool MaterialNox::updateSceneTexturePointers(Scene * sc)
{
	if (!sc)
		return false;
	tex_opacity.updateSceneTexturePointer(sc, false);
	tex_displacement.updateSceneTexturePointer(sc, false);
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * mlay = &(layers[i]);
		mlay->tex_weight.updateSceneTexturePointer(sc, false);
		mlay->tex_col0.updateSceneTexturePointer(sc, false);
		mlay->tex_col90.updateSceneTexturePointer(sc, false);
		mlay->tex_light.updateSceneTexturePointer(sc, false);
		mlay->tex_normal.updateSceneTexturePointer(sc, true);
		mlay->tex_rough.updateSceneTexturePointer(sc, false);
		mlay->tex_transm.updateSceneTexturePointer(sc, false);
		mlay->tex_aniso.updateSceneTexturePointer(sc, false);
		mlay->tex_aniso_angle.updateSceneTexturePointer(sc, false);
	}
	return true;
}


void MaterialNox::deleteLayers()
{
	layers.freeList();
}

Color4 MatLayer::getColor0(float tu, float tv)
{
	if (use_tex_col0  &&  tex_col0.isValid())
		return tex_col0.getColor(tu,tv);
	else
		return gRefl0;
}

Color4 MatLayer::getColor90(float tu, float tv)
{
	if (connect90)
		return getColor0(tu, tv);
	if (use_tex_col90  &&  tex_col90.isValid())
		return tex_col90.getColor(tu,tv);
	else
		return gRefl90;
}

float  MatLayer::getRough(float tu, float tv)
{
	if (use_tex_rough  &&  tex_rough.isValid())
		return tex_rough.getColor(tu,tv).r;
	else
		return roughness;
}

float  MatLayer::getWeight(float tu, float tv)
{
	if (use_tex_weight &&  tex_weight.isValid())
		return tex_weight.getColor(tu,tv).r * (contribution*0.01f);
	else
		return contribution/100.0f;
}

float  MatLayer::getAnisotropy(float tu, float tv)
{
	if (use_tex_aniso  &&  tex_aniso.isValid())
		return tex_aniso.getColor(tu,tv).r;
	else
		return anisotropy;
}

float  MatLayer::getAnisoAngle(float tu, float tv)
{
	if (use_tex_aniso_angle  &&  tex_aniso_angle.isValid())
		return aniso_angle + tex_aniso_angle.getColor(tu,tv).r*360.0f;
	else
		return aniso_angle;
}

Color4 MatLayer::getLightColor(float tu, float tv)
{
	if (use_tex_light  &&  tex_light.isValid())
		return tex_light.getColor(tu,tv);
	else
		return gColor;
}

Color4 MatLayer::getTransmissionColor(float tu, float tv)
{
	if (use_tex_transm  &&  tex_transm.isValid())
		return tex_transm.getColor(tu,tv);
	else
		return gTransmColor;
}

Vector3d MatLayer::getNormalMap(float tu, float tv)
{
	if (use_tex_normal  &&  tex_normal.isValid())
	{
		return tex_normal.getNormal(tu, tv);
	}
	else
		return Vector3d(0.0f, 0.0f, 1.0f);
}

float MatLayer::getIlluminance(SceneStatic * ss, int ntri)	// deprecated, no instancing
{
	float p = power;
	
	if (unit == EMISSION_LUMEN  ||  unit == EMISSION_WATT)
	{
		int l = ss->findLightMesh(ntri);
		if (l > -1)
			p /= (*ss->lightMeshes)[l]->totalArea;
		else
			p = 0;
	}

	if (unit == EMISSION_WATT  ||  unit == EMISSION_WATT_PER_SQR_METER)
		p *= 17.5f;

	return p;

}

float MatLayer::getIlluminance(SceneStatic * ss, int instanceID, int ntri)
{
	float p = power;
	
	if (unit == EMISSION_LUMEN  ||  unit == EMISSION_WATT)
	{
		int l = ss->findLightMeshByInstanceAndTri(instanceID, ntri);
		if (l > -1)
			p /= (*ss->lightMeshes)[l]->totalArea;
		else
			p = 0;
	}

	if (unit == EMISSION_WATT  ||  unit == EMISSION_WATT_PER_SQR_METER)
		p *= 17.5f;

	return p;

}


void MatLayer::deleteAllTextures()
{
	tex_weight.setEmptyTex();
	tex_col0.setEmptyTex();
	tex_col90.setEmptyTex();
	tex_rough.setEmptyTex();
	tex_light.setEmptyTex();
	tex_normal.setEmptyTex();
	tex_transm.setEmptyTex();
	tex_aniso.setEmptyTex();
	tex_aniso_angle.setEmptyTex();
}

bool MatLayer::copyFromOtherWithoutTextures(MatLayer * src)
{
	// will copy with textures now
	if (this==src)
		return true;

	CHECK(src);
	*this = *src;

	if (src->ies)
	{
		this->ies = new IesNOX();
		*(this->ies) = *(src->ies);
	}

	return true;
}

bool MaterialNox::savePreview(char * filename, int format)
{
	CHECK(filename);
	CHECK(preview);

	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	size_t l = strlen(filename)+2;
	WCHAR * wch = (WCHAR *)malloc(sizeof(WCHAR)*(l));
	MultiByteToWideChar(CP_ACP, 0, filename, -1, wch, (int)l);


	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(preview->width, preview->height, PixelFormat32bppARGB);
	if (!bm)
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	Gdiplus::Color c;
	int x, y;
	for (y=0; y<preview->height; y++)
	{
		for (x=0; x<preview->width; x++)
		{
			c.SetFromCOLORREF(preview->imgBuf[y][x]);
			bm->SetPixel(x,y, c);
		}
	}

	CLSID pngClsid;
	WCHAR * ff;
	switch (format)
	{
		case 1: ff = L"image/bmp"; break;
		case 2: ff = L"image/png"; break;
		case 3: ff = L"image/tiff"; break;
		case 4: ff = L"image/jpeg"; break;
		default: ff = L"image/bmp"; break;
	}

	if (GetEncoderClsid(ff, &pngClsid) < 0)
	{
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	if (bm->Save(wch, &pngClsid, NULL) != 0)
	{
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	free(wch);
	return true;


	return true;
}

void HitData::adjustNormal()
{
	if ( (normal_shade*in) * (normal_geom*in) <= 0 )
	{
		Vector3d tvec = normal_shade ^ in;
		normal_shade = in ^ tvec;
		normal_shade += in*0.001f;
		normal_shade.normalize();
	}
	return;

	// old
	if ( (normal_shade*in) * (normal_geom*in) <= 0 )
	{
		float ncos = normal_shade*normal_geom;
		float sgn = 1;
		mult_sign(sgn, ncos);
		normal_shade = normal_geom * sgn;
	}
}

bool HitData::randomSpectrumPosFromColor(const Color4 &c)
{
	float sum = c.r + c.g + c.b;
	#ifdef RAND_PER_THREAD
		float rr = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float rr = ((float)rand()/(float)RAND_MAX);
	#endif
	if (sum==0)
		spectr_pos = rr;
	else
	{
		rr *= sum;
		if (c.r>rr)
			spectr_pos =  rr*0.5f;
		else
		{
			if (c.g>rr)
				spectr_pos =  rr*0.5f + 0.25f;
			else
				spectr_pos =  rr*0.5f + 0.5f;
		}
	}
	return true;
}

bool HitData::randomSpectrumPosSimple()
{
	#ifdef RAND_PER_THREAD
		float rr = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float rr = ((float)rand()/(float)RAND_MAX);
	#endif
	spectr_pos = rr;
	return true;
}

inline void mult_sign(float &dst, float &src) 
{ 
	int sign_mask = ((int&)dst ^ (int&)src) & 0x80000000; // XOR and mask 
	(int&)dst &= 0x7FFFFFFF; // clear sign bit 
	(int&)dst |= sign_mask; // set sign bit if necessary 
}

void MaterialNox::logMaterial()
{
	char buf[256];
	Logger::add("------ MATERIAL LOG ------");
	Logger::add("--------------------------");
	sprintf_s(buf, 256, "Mat name: %s", name);
	Logger::add(buf);
	sprintf_s(buf, 256, "Mat: emitters: %s, incl invisible: %s, shade: %s, fakeglass: %s, skyportal: %s",
		hasEmitters?"yes":"no", hasInvisibleEmitter?"yes":"no", hasShade?"yes":"no", hasFakeGlass?"yes":"no", isSkyPortal?"yes":"no");
	Logger::add(buf);
	sprintf_s(buf, 256, "Mat blend index: %d", blendIndex);
	Logger::add(buf);
	sprintf_s(buf, 256, "Mat layers count: %d", layers.objCount);
	Logger::add(buf);
	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * mlay = &(layers[i]);
		sprintf_s(buf, 256, "Matlayer %d rough: %.3f", (i+1), mlay->roughness);
		Logger::add(buf);
		sprintf_s(buf, 256, "Matlayer %d col0: %.3f %.3f %.3f", (i+1), mlay->refl0.r, mlay->refl0.g, mlay->refl0.b);
		Logger::add(buf);
		sprintf_s(buf, 256, "Matlayer %d col90: %.3f %.3f %.3f", (i+1), mlay->refl90.r, mlay->refl90.g, mlay->refl90.b);
		Logger::add(buf);
		sprintf_s(buf, 256, "Matlayer %d transmission color: %.3f %.3f %.3f", (i+1), mlay->transmColor.r, mlay->transmColor.g, mlay->transmColor.b);
		Logger::add(buf);
		sprintf_s(buf, 256, "Matlayer %d  ", (i+1));
		Logger::add(buf);
	}
	sprintf_s(buf, 256, "Mat ");
	Logger::add(buf);
	sprintf_s(buf, 256, "Mat ");
	Logger::add(buf);


	for (int i=0; i<layers.objCount; i++)
	{
		MatLayer * mlay = &(layers[i]);

		sprintf_s(buf, 256, "Matlayer %d tex ROUGH", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_rough.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_rough.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_rough.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex COLOR 0", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_col0.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_col0.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_col0.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex COLOR 90", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_col90.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_col90.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_col90.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex EMISSION", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_light.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_light.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_light.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex WEIGHT", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_weight.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_weight.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_weight.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex NORMAL", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_normal.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_normal.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_normal.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex TRANSM", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_transm.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_transm.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_transm.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex ANISO", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_aniso.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_aniso.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_aniso.managerID);
		Logger::add(buf);

		sprintf_s(buf, 256, "Matlayer %d tex ANISO ANGLE", (i+1));
		Logger::add(buf);
		sprintf_s(buf, 256, "  filename: %s", mlay->tex_aniso_angle.filename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  fullfilename: %s", mlay->tex_aniso_angle.fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 256, "  id: %d", mlay->tex_aniso_angle.managerID);
		Logger::add(buf);

	}	
	Logger::add("--------------------------");
}

