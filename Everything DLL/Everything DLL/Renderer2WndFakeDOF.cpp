#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "valuesMinMax.h"
#include "raytracer.h"
#include "ThreadJobWindow.h"
#include "log.h"

extern HMODULE hDllModule;

void setPositionsOnStartFakeDofTab(HWND hWnd);
bool updateDiaphragmImageFake(HWND hWnd, Camera * cam, float depth);
bool copyFakeDofFromCamera(HWND hWnd, Camera * cam);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcFakeDof(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;
	static float prevDist;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));
				setPositionsOnStartFakeDofTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->opensans11, false);
				emtitle->colText = NGCOL_LIGHT_GRAY; 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2Button * embRedraw = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_REDRAW));
				embRedraw->bgImage = rMain->hBmpBg;
				embRedraw->setFont(rMain->fonts->em2button, false);

				EM2GroupBar * emgr_plabb = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_GR_P_C_ABB));
				emgr_plabb->bgImage = rMain->hBmpBg;
				emgr_plabb->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_fakedof = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_GR_FAKEDOF));
				emgr_fakedof->bgImage = rMain->hBmpBg;
				emgr_fakedof->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_aperture = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_GR_APERTURE));
				emgr_aperture->bgImage = rMain->hBmpBg;
				emgr_aperture->setFont(rMain->fonts->em2groupbar, false);

				EM2Text * emtLens = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_LENS));
				emtLens->bgImage = rMain->hBmpBg;
				emtLens->setFont(rMain->fonts->em2text, false);
				EM2Text * emtDepth = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_DEPTH));
				emtDepth->bgImage = rMain->hBmpBg;
				emtDepth->setFont(rMain->fonts->em2text, false);
				EM2Text * emtAchromatic = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_ACHROMATIC));
				emtAchromatic->bgImage = rMain->hBmpBg;
				emtAchromatic->setFont(rMain->fonts->em2text, false);

				EM2EditSpin * emesLens = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_LENS));
				emesLens->setFont(rMain->fonts->em2editspin, false);
				emesLens->bgImage = rMain->hBmpBg;
				emesLens->setValuesToFloat(NOX_FINAL_CHR_SHIFT_LENS_DEF, NOX_FINAL_CHR_SHIFT_LENS_MIN, NOX_FINAL_CHR_SHIFT_LENS_MAX, NOX_FINAL_CHR_SHIFT_LENS_DEF, 0.1f, 0.002f);
				EM2EditSpin * emesDepth = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_DEPTH));
				emesDepth->setFont(rMain->fonts->em2editspin, false);
				emesDepth->bgImage = rMain->hBmpBg;
				emesDepth->setValuesToFloat(NOX_FINAL_CHR_SHIFT_DEPTH_DEF, NOX_FINAL_CHR_SHIFT_DEPTH_MIN, NOX_FINAL_CHR_SHIFT_DEPTH_MAX, NOX_FINAL_CHR_SHIFT_DEPTH_DEF, 0.1f, 0.002f);
				EM2EditSpin * emesAchromatic = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_ACHROMATIC));
				emesAchromatic->setFont(rMain->fonts->em2editspin, false);
				emesAchromatic->bgImage = rMain->hBmpBg;
				emesAchromatic->setValuesToFloat(NOX_FINAL_CHR_ACHROMATIC_DEF, NOX_FINAL_CHR_ACHROMATIC_MIN, NOX_FINAL_CHR_ACHROMATIC_MAX, NOX_FINAL_CHR_ACHROMATIC_DEF, 0.1f, 0.002f);


				EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RED));
				emtRed->bgImage = rMain->hBmpBg;
				emtRed->setFont(rMain->fonts->em2text, false);
				EM2Text * emtGreen = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_GREEN));
				emtGreen->bgImage = rMain->hBmpBg;
				emtGreen->setFont(rMain->fonts->em2text, false);
				EM2Text * emtBlue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_BLUE));
				emtBlue->bgImage = rMain->hBmpBg;
				emtBlue->setFont(rMain->fonts->em2text, false);

				EM2EditSpin * emesRed = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_RED));
				emesRed->setFont(rMain->fonts->em2editspin, false);
				emesRed->bgImage = rMain->hBmpBg;
				emesRed->setValuesToFloat(NOX_FINAL_CHR_PL_RED_DEF, NOX_FINAL_CHR_PL_RED_MIN, NOX_FINAL_CHR_PL_RED_MAX, NOX_FINAL_CHR_PL_RED_DEF, 0.1f, 0.002f);
				EM2EditSpin * emesGreen = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GREEN));
				emesGreen->setFont(rMain->fonts->em2editspin, false);
				emesGreen->bgImage = rMain->hBmpBg;
				emesGreen->setValuesToFloat(NOX_FINAL_CHR_PL_GREEN_DEF, NOX_FINAL_CHR_PL_GREEN_MIN, NOX_FINAL_CHR_PL_GREEN_MAX, NOX_FINAL_CHR_PL_GREEN_DEF, 0.1f, 0.002f);
				EM2EditSpin * emesBlue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_BLUE));
				emesBlue->setFont(rMain->fonts->em2editspin, false);
				emesBlue->bgImage = rMain->hBmpBg;
				emesBlue->setValuesToFloat(NOX_FINAL_CHR_PL_BLUE_DEF, NOX_FINAL_CHR_PL_BLUE_MIN, NOX_FINAL_CHR_PL_BLUE_MAX, NOX_FINAL_CHR_PL_BLUE_DEF, 0.1f, 0.002f);

				EM2CheckBox * emcDOFon= GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON));
				emcDOFon->bgImage = rMain->hBmpBg;
				emcDOFon->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcAlg1 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM1));
				emcAlg1->bgImage = rMain->hBmpBg;
				emcAlg1->selected = false;
				emcAlg1->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcAlg2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM2));
				emcAlg2->bgImage = rMain->hBmpBg;
				emcAlg2->selected = true;
				emcAlg2->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcAlg3 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM3));
				emcAlg3->bgImage = rMain->hBmpBg;
				emcAlg3->selected = false;
				emcAlg3->setFont(rMain->fonts->em2checkbox, false);


				EM2Text * emtAlgorithm = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_ALGORITHM));
				emtAlgorithm->bgImage = rMain->hBmpBg;
				emtAlgorithm->setFont(rMain->fonts->em2text, false);
				EM2Text * emtFocusDist = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_FOCUS_DIST));
				emtFocusDist->bgImage = rMain->hBmpBg;
				emtFocusDist->setFont(rMain->fonts->em2text, false);
				EM2Text * emtThreads = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_THREADS));
				emtThreads->bgImage = rMain->hBmpBg;
				emtThreads->setFont(rMain->fonts->em2text, false);
				emtThreads->align = EM2Text::ALIGN_RIGHT;
				EM2Text * emtQuality = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_QUALITY));
				emtQuality->bgImage = rMain->hBmpBg;
				emtQuality->setFont(rMain->fonts->em2text, false);
				emtQuality->align = EM2Text::ALIGN_RIGHT;

				EM2EditSpin * emesFocusDist = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FOCUS_DIST));
				emesFocusDist->setFont(rMain->fonts->em2editspin, false);
				emesFocusDist->bgImage = rMain->hBmpBg;
				emesFocusDist->setValuesToFloat(NOX_FINAL_FOCUS_DEF, NOX_FINAL_FOCUS_MIN, NOX_FINAL_FOCUS_MAX, NOX_FINAL_FOCUS_DEF, 0.1f, 0.05f);
				EM2EditSpin * emesThreads = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_THREADS));
				emesThreads->setFont(rMain->fonts->em2editspin, false);
				emesThreads->bgImage = rMain->hBmpBg;
				emesThreads->setValuesToInt(NOX_FINAL_THREADS_DEF, NOX_FINAL_THREADS_MIN, NOX_FINAL_THREADS_MAX, NOX_FINAL_THREADS_DEF, 1, 0.2f);
				EM2EditSpin * emesQuality = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_QUALITY));
				emesQuality->setFont(rMain->fonts->em2editspin, false);
				emesQuality->bgImage = rMain->hBmpBg;
				emesQuality->setValuesToInt(NOX_FINAL_QUALITY_DEF, NOX_FINAL_QUALITY_MIN, NOX_FINAL_QUALITY_MAX, NOX_FINAL_QUALITY_DEF, 1, 0.2f);

				EM2Button * embPickFocus = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_PICK_FOCUS));
				embPickFocus->bgImage = rMain->hBmpBg;
				embPickFocus->setFont(rMain->fonts->em2button, false);
				embPickFocus->twoState = true;
				embPickFocus->ts_allow_unclick = true;
				EM2Button * embShowDofRange = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE));
				embShowDofRange->bgImage = rMain->hBmpBg;
				embShowDofRange->setFont(rMain->fonts->em2button, false);
				embShowDofRange->twoState = true;
				embShowDofRange->ts_allow_unclick = true;
				EM2Button * embShowZDepth = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_ZDEPTH));
				embShowZDepth->bgImage = rMain->hBmpBg;
				embShowZDepth->setFont(rMain->fonts->em2button, false);
				embShowZDepth->twoState = true;
				embShowZDepth->ts_allow_unclick = true;


				EM2CheckBox * emcDOFon2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON2));
				emcDOFon2->bgImage = rMain->hBmpBg;
				emcDOFon2->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcRoundBlades = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ROUND_BLADES));
				emcRoundBlades->bgImage = rMain->hBmpBg;
				emcRoundBlades->setFont(rMain->fonts->em2checkbox, false);

				EM2Text * emtAperture  = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_APERTURE));
				emtAperture->bgImage = rMain->hBmpBg;
				emtAperture->setFont(rMain->fonts->em2text, false);
				EM2Text * emtBlades = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_BLADES));
				emtBlades->bgImage = rMain->hBmpBg;
				emtBlades->setFont(rMain->fonts->em2text, false);
				EM2Text * emtRadius = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RADIUS));
				emtRadius->bgImage = rMain->hBmpBg;
				emtRadius->setFont(rMain->fonts->em2text, false);
				EM2Text * emtAngle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_ANGLE));
				emtAngle->bgImage = rMain->hBmpBg;
				emtAngle->setFont(rMain->fonts->em2text, false);
				EM2Text * emtRingBalance = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RING_BALANCE));
				emtRingBalance->bgImage = rMain->hBmpBg;
				emtRingBalance->setFont(rMain->fonts->em2text, false);
				EM2Text * emtRingSize = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RING_SIZE));
				emtRingSize->bgImage = rMain->hBmpBg;
				emtRingSize->setFont(rMain->fonts->em2text, false);
				EM2Text * emtFlatten = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_FLATTEN));
				emtFlatten->bgImage = rMain->hBmpBg;
				emtFlatten->setFont(rMain->fonts->em2text, false);
				EM2Text * emtVignette = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_VIGNETTE));
				emtVignette->bgImage = rMain->hBmpBg;
				emtVignette->setFont(rMain->fonts->em2text, false);

				int editSize = 42;
				EM2EditSpin * emesAperture = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_APERTURE));
				emesAperture->setFont(rMain->fonts->em2editspin, false);
				emesAperture->bgImage = rMain->hBmpBg;
				emesAperture->floatAfterDot = 1;
				emesAperture->setValuesToFloat(NOX_FINAL_APERTURE_DEF, NOX_FINAL_APERTURE_MIN, NOX_FINAL_APERTURE_MAX, NOX_FINAL_APERTURE_DEF, 0.4f, 0.1f);
				EM2EditSpin * emesBlades = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_BLADES));
				emesBlades->setFont(rMain->fonts->em2editspin, false);
				emesBlades->bgImage = rMain->hBmpBg;
				emesBlades->setValuesToInt(NOX_FINAL_BLADES_NUM_DEF, NOX_FINAL_BLADES_NUM_MIN, NOX_FINAL_BLADES_NUM_MAX, NOX_FINAL_BLADES_NUM_DEF, 1, 0.1f);
				EM2EditSpin * emesRadius = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RADIUS));
				emesRadius->setFont(rMain->fonts->em2editspin, false);
				emesRadius->bgImage = rMain->hBmpBg;
				emesRadius->floatAfterDot = 1;
				emesRadius->setValuesToFloat(NOX_FINAL_BLADES_RADIUS_DEF, NOX_FINAL_BLADES_RADIUS_MIN, NOX_FINAL_BLADES_RADIUS_MAX, NOX_FINAL_BLADES_RADIUS_DEF, 0.5f, 0.01f);
				EM2EditSpin * emesAngle = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ANGLE));
				emesAngle->setFont(rMain->fonts->em2editspin, false);
				emesAngle->bgImage = rMain->hBmpBg;
				emesAngle->floatAfterDot = 1;
				emesAngle->setValuesToFloat(NOX_FINAL_BLADES_ANGLE_DEF, NOX_FINAL_BLADES_ANGLE_MIN, NOX_FINAL_BLADES_ANGLE_MAX, NOX_FINAL_BLADES_ANGLE_DEF, 5.0f, 0.5f);
				EM2EditSpin * emesRingBalance = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_BALANCE));
				emesRingBalance->setFont(rMain->fonts->em2editspin, false);
				emesRingBalance->bgImage = rMain->hBmpBg;
				emesRingBalance->setValuesToInt(NOX_FINAL_BOKEH_BALANCE_DEF, NOX_FINAL_BOKEH_BALANCE_MIN, NOX_FINAL_BOKEH_BALANCE_MAX, NOX_FINAL_BOKEH_BALANCE_DEF, 1, 0.1f);
				EM2EditSpin * emesRingSize = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_SIZE));
				emesRingSize->setFont(rMain->fonts->em2editspin, false);
				emesRingSize->bgImage = rMain->hBmpBg;
				emesRingSize->setValuesToInt(NOX_FINAL_BOKEH_RING_SIZE_DEF, NOX_FINAL_BOKEH_RING_SIZE_MIN, NOX_FINAL_BOKEH_RING_SIZE_MAX, NOX_FINAL_BOKEH_RING_SIZE_DEF, 1, 0.2f);
				EM2EditSpin * emesFlatten = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_FLATTEN));
				emesFlatten->setFont(rMain->fonts->em2editspin, false);
				emesFlatten->bgImage = rMain->hBmpBg;
				emesFlatten->setValuesToInt(NOX_FINAL_BOKEH_FLATTEN_DEF, NOX_FINAL_BOKEH_FLATTEN_MIN, NOX_FINAL_BOKEH_FLATTEN_MAX, NOX_FINAL_BOKEH_FLATTEN_DEF, 1, 0.2f);
				EM2EditSpin * emesVignette = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_VIGNETTE));
				emesVignette->setFont(rMain->fonts->em2editspin, false);
				emesVignette->bgImage = rMain->hBmpBg;
				emesVignette->setValuesToInt(NOX_FINAL_BOKEH_VIGNETTE_DEF, NOX_FINAL_BOKEH_VIGNETTE_MIN, NOX_FINAL_BOKEH_VIGNETTE_MAX, NOX_FINAL_BOKEH_VIGNETTE_DEF, 1, 0.2f);

				EM2Button * embLoadFromCam = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_LOAD_FROM_CAMERA));
				embLoadFromCam->bgImage = rMain->hBmpBg;
				embLoadFromCam->setFont(rMain->fonts->em2button, false);

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case PV_PIXEL_PICKED:
			{
				Raytracer * rtr = Raytracer::getInstance();
				if (rtr->curScenePtr->nowRendering)
					break;
				EM2Button * embPick = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_PICK_FOCUS));
				EM2Button * embDofR = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE));
				Camera * cam = rtr->curScenePtr->getActiveCamera();
				CHECK(embPick);
				CHECK(embDofR);
				CHECK(cam);
				float x = (float&)wParam;
				float y = (float&)lParam;
				if (embPick->selected)
				{
					EMPView * empvmain = GetEMPViewInstance(rMain->hViewport);
					int ix = (int)(x*cam->aa);
					int iy = (int)(y*cam->aa);
					if (ix>=cam->imgBuff->width || ix<0)
						break;
					if (iy>=cam->imgBuff->height|| iy<0)
						break;

					int hits = cam->depthBuf->hbuf[iy][ix];
					if (hits <= 0)
						break;
					float focusD = cam->depthBuf->fbuf[iy][ix] / hits;

					EM2EditSpin * emFDist = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FOCUS_DIST));
					emFDist->setFloatValue(focusD);

					cam->fMod.focusDistance = focusD;

					if (embDofR->selected)
					{
						float lng = 0.5f * 0.036f / tan(cam->angle * PI / 360.0f);
						cam->depthBuf->copyAsDOFTemperatureToByteBuffer(empvmain->byteBuff, focusD, cam->aa, lng, cam->fMod.aperture);
					}

				}

			}
			break;
		case EMT_TAB_OPENED:
			{
				Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
				CHECK(cam);
				updateDiaphragmImageFake(hWnd, cam, prevDist);
				InvalidateRect(rMain->getHWND(), NULL, false);
			}
			break;
		case EMT_TAB_CLOSED:
			{
				EM2Button * embPickFocus = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_PICK_FOCUS));
				EM2Button * embShowDofRange = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE));
				EM2Button * embShowZDepth = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_ZDEPTH));
				embPickFocus->selected = false;
				embShowDofRange->selected = false;
				embShowZDepth->selected = false;
				InvalidateRect(embPickFocus->hwnd, NULL, FALSE);
				InvalidateRect(embShowDofRange->hwnd, NULL, FALSE);
				InvalidateRect(embShowZDepth->hwnd, NULL, FALSE);

				EMPView * empv = GetEMPViewInstance(rMain->hViewport);
				if (empv->mode == EMPView::MODE_PICK)
					empv->mode = EMPView::MODE_SHOW;
				rMain->showPickModeInStatus(false);
				rMain->udpateRegionPanelModeFromViewport();
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_FDOF_REDRAW:
						{
							rMain->turnOffDofDephtModes();

							rMain->runRedrawFinalThread();
							rMain->udpateRegionPanelModeFromViewport();
						}
						break;
					case IDC_REND2_FDOF_ABB_GRAPH:
						{
							if (wmEvent != EMA_DIST_CHANGED)
								break;
							EMAberration * emab = GetEMAberrationInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GRAPH));
							prevDist = emab->distPattern;

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_ABB_LENS:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EMAberration * emab = GetEMAberrationInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GRAPH));
							EM2EditSpin * emtr = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_LENS));
							emab->abbShiftXY = emtr->floatValue;
							InvalidateRect(emab->hwnd, NULL, false);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.chromaticShiftLens = emtr->floatValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_ABB_DEPTH:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EMAberration * emab = GetEMAberrationInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GRAPH));
							EM2EditSpin * emtr = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_DEPTH));
							emab->abbShiftZ = emtr->floatValue;
							InvalidateRect(emab->hwnd, NULL, false);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.chromaticShiftDepth = emtr->floatValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_ABB_ACHROMATIC:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EMAberration * emab = GetEMAberrationInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GRAPH));
							EM2EditSpin * emtr = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_ACHROMATIC));
							emab->abbAchromatic = emtr->floatValue;
							InvalidateRect(emab->hwnd, NULL, false);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.chromaticAchromatic = emtr->floatValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_ABB_RED:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emtr = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_RED));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.chromaticPlanarRed = emtr->floatValue;
						}
						break;
					case IDC_REND2_FDOF_ABB_GREEN:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emtr = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GREEN));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.chromaticPlanarGreen = emtr->floatValue;
						}
						break;
					case IDC_REND2_FDOF_ABB_BLUE:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emtr = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_BLUE));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.chromaticPlanarBlue= emtr->floatValue;
						}
						break;
					case IDC_REND2_FDOF_FAKEDOF_ON:
						{
							if (wmEvent != BN_CLICKED)
								break;
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON));
							EM2CheckBox * emc2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON2));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.dofOn = emc->selected;
							emc2->selected = emc->selected;
							InvalidateRect(emc2->hwnd, NULL, false);
							rMain->updateLocksTabFakeDof(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_FDOF_FAKEDOF_ON2:
						{
							if (wmEvent != BN_CLICKED)
								break;
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON));
							EM2CheckBox * emc2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON2));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.dofOn = emc2->selected;
							emc->selected = emc2->selected;
							InvalidateRect(emc->hwnd, NULL, false);
							rMain->updateLocksTabFakeDof(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_FDOF_ALGORITHM1:
						{
							if (wmEvent != BN_CLICKED)
								break;
							EM2CheckBox * emc1 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM1));
							EM2CheckBox * emc2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM2));
							EM2CheckBox * emc3 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM3));
							emc1->selected = true;
							emc2->selected = false;
							emc3->selected = false;
							InvalidateRect(emc1->hwnd, NULL, false);
							InvalidateRect(emc2->hwnd, NULL, false);
							InvalidateRect(emc3->hwnd, NULL, false);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.algorithm = 0;
						}
						break;
					case IDC_REND2_FDOF_ALGORITHM2:
						{
							if (wmEvent != BN_CLICKED)
								break;
							EM2CheckBox * emc1 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM1));
							EM2CheckBox * emc2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM2));
							EM2CheckBox * emc3 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM3));
							emc1->selected = false;
							emc2->selected = true;
							emc3->selected = false;
							InvalidateRect(emc1->hwnd, NULL, false);
							InvalidateRect(emc2->hwnd, NULL, false);
							InvalidateRect(emc3->hwnd, NULL, false);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.algorithm = 1;
						}
						break;
					case IDC_REND2_FDOF_ALGORITHM3:
						{
							if (wmEvent != BN_CLICKED)
								break;
							EM2CheckBox * emc1 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM1));
							EM2CheckBox * emc2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM2));
							EM2CheckBox * emc3 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM3));
							emc1->selected = false;
							emc2->selected = false;
							emc3->selected = true;
							InvalidateRect(emc1->hwnd, NULL, false);
							InvalidateRect(emc2->hwnd, NULL, false);
							InvalidateRect(emc3->hwnd, NULL, false);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.algorithm = 2;
						}
						break;
					case IDC_REND2_FDOF_FOCUS_DIST:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_FOCUS_DIST));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							CHECK(cam);
							cam->fMod.focusDistance = emes->floatValue;
	
							if (GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE))->selected)
								rMain->drawDofRangeOnThread();
						}
						break;
					case IDC_REND2_FDOF_THREADS:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_THREADS));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.numThreads = emes->intValue;
						}
						break;
					case IDC_REND2_FDOF_QUALITY:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_QUALITY));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.quality = emes->intValue;
						}
						break;
					case IDC_REND2_FDOF_AP_ROUND_BLADES:
						{
							if (wmEvent != BN_CLICKED)
								break;
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ROUND_BLADES));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.roundBlades = emc->selected;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
							rMain->updateLocksTabFakeDof(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_FDOF_AP_APERTURE:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_APERTURE));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.aperture = emes->floatValue;

							if (GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE))->selected)
								rMain->drawDofRangeOnThread();
						}
						break;
					case IDC_REND2_FDOF_AP_BLADES:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_BLADES));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.bladesNum = emes->intValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_AP_ANGLE:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ANGLE));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.bladesAngle = emes->floatValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_AP_RADIUS:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RADIUS));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.bladesRadius = emes->floatValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_AP_RING_BALANCE:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_BALANCE));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.bokehRingBalance = emes->intValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_AP_RING_SIZE:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_SIZE));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.bokehRingSize = emes->intValue;
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_FDOF_AP_FLATTEN:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_FLATTEN));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.bokehFlatten = emes->intValue;
						}
						break;
					case IDC_REND2_FDOF_AP_VIGNETTE:
						{
							if (wmEvent != WM_VSCROLL)
								break;
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_VIGNETTE));
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->fMod.bokehVignette = emes->intValue;
						}
						break;
					case IDC_REND2_FDOF_AP_LOAD_FROM_CAMERA:
						{
							CHECK(wmEvent==BN_CLICKED);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							copyFakeDofFromCamera(hWnd, cam);
							updateDiaphragmImageFake(hWnd, cam, prevDist);
							InvalidateRect(rMain->getHWND(), NULL, false);
							rMain->updateLocksTabFakeDof(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_FDOF_SHOW_ZDEPTH:
						{
							CHECK(wmEvent==BN_CLICKED);

							EM2Button * embPickFocus = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_PICK_FOCUS));
							EM2Button * embShowDofRange = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE));
							EM2Button * embShowZDepth = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_ZDEPTH));

							embShowDofRange->selected = false;
							InvalidateRect(embShowDofRange->hwnd, NULL, false);

							if (embShowZDepth->selected)
							{
								RECT crect;
								GetClientRect(rMain->hViewport, &crect);
								openAnimCircleDialog(rMain->getHWND(), (crect.right)/2, (crect.bottom)/2);
								rMain->runShowZDepthThread();
							}
							else
								rMain->refreshRenderOnThread();
						}
						break;
					case IDC_REND2_FDOF_PICK_FOCUS:
						{
							CHECK(wmEvent==BN_CLICKED);
							Raytracer * rtr = Raytracer::getInstance();
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2Button * embPickFocus = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_PICK_FOCUS));
							EM2Button * embShowDofRange = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE));
							EM2Button * embShowZDepth = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_ZDEPTH));
							CHECK(empv);
							CHECK(cam);
							CHECK(embPickFocus);

							if (embPickFocus->selected)
							{
								empv->mode = EMPView::MODE_PICK;
								rMain->showPickModeInStatus(true);
								rMain->udpateRegionPanelModeFromViewport();
							}
							else
							{
								empv->mode = EMPView::MODE_SHOW;
								rMain->showPickModeInStatus(false);
								rMain->udpateRegionPanelModeFromViewport();
							}

							if (!cam->depthBuf   ||   !cam->depthOK)
								rMain->evalDepthMapOnThread();

							CHECK(cam->depthBuf);
							CHECK(cam->depthBuf->fbuf);
						}
						break;
					case IDC_REND2_FDOF_SHOW_DOF_RANGE:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2Button * embPickFocus = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_PICK_FOCUS));
							EM2Button * embShowDofRange = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE));
							EM2Button * embShowZDepth = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_ZDEPTH));

							embShowZDepth->selected = false;
							InvalidateRect(embShowZDepth->hwnd, NULL, false);

							if (embShowDofRange->selected)
							{
								rMain->drawDofRangeOnThread();
							}
							else
								rMain->refreshRenderOnThread();
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelFakeDof()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TITLE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);
	EM2Button * embRedraw = GetEM2ButtonInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_REDRAW));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_REDRAW), bgShiftXRightPanel, 0, embRedraw->bgShiftX, embRedraw->bgShiftY);

	EM2Text * emtLens = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_LENS));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_LENS), bgShiftXRightPanel, 0, emtLens->bgShiftX, emtLens->bgShiftY);
	EM2Text * emtDepth = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_DEPTH));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_DEPTH), bgShiftXRightPanel, 0, emtDepth->bgShiftX, emtDepth->bgShiftY);
	EM2Text * emtAchromatic = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_ACHROMATIC));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_ACHROMATIC), bgShiftXRightPanel, 0, emtAchromatic->bgShiftX, emtAchromatic->bgShiftY);

	EM2EditSpin * emesLens = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_LENS));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_LENS), bgShiftXRightPanel, 0, emesLens->bgShiftX, emesLens->bgShiftY);
	EM2EditSpin * emesDepth = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_DEPTH));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_DEPTH), bgShiftXRightPanel, 0, emesDepth->bgShiftX, emesDepth->bgShiftY);
	EM2EditSpin * emesAcgromatic = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_ACHROMATIC));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_ACHROMATIC), bgShiftXRightPanel, 0, emesAcgromatic->bgShiftX, emesAcgromatic->bgShiftY);

	EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RED));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RED), bgShiftXRightPanel, 0, emtRed->bgShiftX, emtRed->bgShiftY);
	EM2Text * emtGreen = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_GREEN));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_GREEN), bgShiftXRightPanel, 0, emtGreen->bgShiftX, emtGreen->bgShiftY);
	EM2Text * emtBlue = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_BLUE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_BLUE), bgShiftXRightPanel, 0, emtBlue->bgShiftX, emtBlue->bgShiftY);

	EM2EditSpin * emesRed= GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_RED));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_RED), bgShiftXRightPanel, 0, emesRed->bgShiftX, emesRed->bgShiftY);
	EM2EditSpin * emesGreen= GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_GREEN));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_GREEN), bgShiftXRightPanel, 0, emesGreen->bgShiftX, emesGreen->bgShiftY);
	EM2EditSpin * emesBlue = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_BLUE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_BLUE), bgShiftXRightPanel, 0, emesBlue->bgShiftX, emesBlue->bgShiftY);

	EM2GroupBar * emgrPCAbb = GetEM2GroupBarInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_GR_P_C_ABB));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_GR_P_C_ABB), bgShiftXRightPanel, 0, emgrPCAbb->bgShiftX, emgrPCAbb->bgShiftY);
	EM2GroupBar * emgrFakeDof = GetEM2GroupBarInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_GR_FAKEDOF));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_GR_FAKEDOF), bgShiftXRightPanel, 0, emgrFakeDof->bgShiftX, emgrFakeDof->bgShiftY);
	EM2GroupBar * emgrAperture = GetEM2GroupBarInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_GR_APERTURE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_GR_APERTURE), bgShiftXRightPanel, 0, emgrAperture->bgShiftX, emgrAperture->bgShiftY);

	EM2CheckBox * emcDOFon = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON), bgShiftXRightPanel, 0, emcDOFon->bgShiftX, emcDOFon->bgShiftY);
	EM2CheckBox * emcAlgorithm1 = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM1));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM1), bgShiftXRightPanel, 0, emcAlgorithm1->bgShiftX, emcAlgorithm1->bgShiftY);
	EM2CheckBox * emcAlgorithm2 = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM2));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM2), bgShiftXRightPanel, 0, emcAlgorithm2->bgShiftX, emcAlgorithm2->bgShiftY);
	EM2CheckBox * emcAlgorithm3 = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM3));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM3), bgShiftXRightPanel, 0, emcAlgorithm3->bgShiftX, emcAlgorithm3->bgShiftY);

	EM2Text * emtAlgorithm = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_ALGORITHM));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_ALGORITHM), bgShiftXRightPanel, 0, emtAlgorithm->bgShiftX, emtAlgorithm->bgShiftY);
	EM2Text * emtFocusDist = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_FOCUS_DIST));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_FOCUS_DIST), bgShiftXRightPanel, 0, emtFocusDist->bgShiftX, emtFocusDist->bgShiftY);
	EM2Text * emtThreads = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_THREADS));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_THREADS), bgShiftXRightPanel, 0, emtThreads->bgShiftX, emtThreads->bgShiftY);
	EM2Text * emtQuality = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_QUALITY));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_QUALITY), bgShiftXRightPanel, 0, emtQuality->bgShiftX, emtQuality->bgShiftY);

	EM2EditSpin * emesFocusDist = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FOCUS_DIST));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FOCUS_DIST), bgShiftXRightPanel, 0, emesFocusDist->bgShiftX, emesFocusDist->bgShiftY);
	EM2EditSpin * emesThreads = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_THREADS));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_THREADS), bgShiftXRightPanel, 0, emesThreads->bgShiftX, emesThreads->bgShiftY);
	EM2EditSpin * emesQuality = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_QUALITY));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_QUALITY), bgShiftXRightPanel, 0, emesQuality->bgShiftX, emesQuality->bgShiftY);

	EM2Button * embPickFocus = GetEM2ButtonInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_PICK_FOCUS));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_PICK_FOCUS), bgShiftXRightPanel, 0, embPickFocus->bgShiftX, embPickFocus->bgShiftY);
	EM2Button * embShowDofRange = GetEM2ButtonInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_SHOW_DOF_RANGE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_SHOW_DOF_RANGE), bgShiftXRightPanel, 0, embShowDofRange->bgShiftX, embShowDofRange->bgShiftY);
	EM2Button * embShowZdepth = GetEM2ButtonInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_SHOW_ZDEPTH));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_SHOW_ZDEPTH), bgShiftXRightPanel, 0, embShowZdepth->bgShiftX, embShowZdepth->bgShiftY);



	EM2Text * emtAperture = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_APERTURE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_APERTURE), bgShiftXRightPanel, 0, emtAperture->bgShiftX, emtAperture->bgShiftY);
	EM2Text * emtBlades = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_BLADES));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_BLADES), bgShiftXRightPanel, 0, emtBlades->bgShiftX, emtBlades->bgShiftY);
	EM2Text * emtAngle = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_ANGLE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_ANGLE), bgShiftXRightPanel, 0, emtAngle->bgShiftX, emtAngle->bgShiftY);
	EM2Text * emtRadius = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RADIUS));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RADIUS), bgShiftXRightPanel, 0, emtRadius->bgShiftX, emtRadius->bgShiftY);
	EM2Text * emtRingBalance = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RING_BALANCE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RING_BALANCE), bgShiftXRightPanel, 0, emtRingBalance->bgShiftX, emtRingBalance->bgShiftY);
	EM2Text * emtRingSize = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RING_SIZE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_RING_SIZE), bgShiftXRightPanel, 0, emtRingSize->bgShiftX, emtRingSize->bgShiftY);
	EM2Text * emtFlatten = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_FLATTEN));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_FLATTEN), bgShiftXRightPanel, 0, emtFlatten->bgShiftX, emtFlatten->bgShiftY);
	EM2Text * emtVignette = GetEM2TextInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_VIGNETTE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_TEXT_VIGNETTE), bgShiftXRightPanel, 0, emtVignette->bgShiftX, emtVignette->bgShiftY);

	EM2CheckBox * emcDOFon2 = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON2));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON2), bgShiftXRightPanel, 0, emcDOFon2->bgShiftX, emcDOFon2->bgShiftY);
	EM2CheckBox * emcRoundBlades = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_ROUND_BLADES));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_ROUND_BLADES), bgShiftXRightPanel, 0, emcRoundBlades->bgShiftX, emcRoundBlades->bgShiftY);

	EM2EditSpin * emetAperture = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_APERTURE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_APERTURE), bgShiftXRightPanel, 0, emetAperture->bgShiftX, emetAperture->bgShiftY);
	EM2EditSpin * emetBlades = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_BLADES));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_BLADES), bgShiftXRightPanel, 0, emetBlades->bgShiftX, emetBlades->bgShiftY);
	EM2EditSpin * emetAngle = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_ANGLE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_ANGLE), bgShiftXRightPanel, 0, emetAngle->bgShiftX, emetAngle->bgShiftY);
	EM2EditSpin * emetRadius = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RADIUS));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RADIUS), bgShiftXRightPanel, 0, emetRadius->bgShiftX, emetRadius->bgShiftY);
	EM2EditSpin * emetRingBalance = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RING_BALANCE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RING_BALANCE), bgShiftXRightPanel, 0, emetRingBalance->bgShiftX, emetRingBalance->bgShiftY);
	EM2EditSpin * emetRingSize = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RING_SIZE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RING_SIZE), bgShiftXRightPanel, 0, emetRingSize->bgShiftX, emetRingSize->bgShiftY);
	EM2EditSpin * emetFlatten = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_FLATTEN));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_FLATTEN), bgShiftXRightPanel, 0, emetFlatten->bgShiftX, emetFlatten->bgShiftY);
	EM2EditSpin * emetVignette = GetEM2EditSpinInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_VIGNETTE));
	updateControlBgShift(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_VIGNETTE), bgShiftXRightPanel, 0, emetVignette->bgShiftX, emetVignette->bgShiftY);
}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartFakeDofTab(HWND hWnd)
{
	int x, y;
	x = 8;	y = 1;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TITLE),				HWND_TOP,	130+x,	11, 100, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_REDRAW),				HWND_TOP,	128+x,	39, 103, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GRAPH),			HWND_TOP,	x,		74, 360, 130, 0);


	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_LENS),			HWND_TOP,	x,		y+213,			31,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_DEPTH),			HWND_TOP,	x+109,	y+213,			42,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_ACHROMATIC),		HWND_TOP,	x+225,	y+213,			75,		16,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_LENS),				HWND_TOP,	x+35,	y+211,			59,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_DEPTH),			HWND_TOP,	x+152,	y+211,			59,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_ACHROMATIC),		HWND_TOP,	x+302,	y+211,			59,		20,		0);

	x = 8;	y = 244;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_GR_P_C_ABB),			HWND_TOP,	x,		y,				360,	16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RED),				HWND_TOP,	x,		y+34,			30,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_GREEN),			HWND_TOP,	x+109,	y+34,			40,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_BLUE),			HWND_TOP,	x+267,	y+34,			32,		16,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_RED),				HWND_TOP,	x+35,	y+32,			59,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GREEN),			HWND_TOP,	x+152,	y+32,			59,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_BLUE),				HWND_TOP,	x+302,	y+32,			59,		20,		0);

	x = 8;	y = 310;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_GR_FAKEDOF),			HWND_TOP,	x,		y,				360,	16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON),			HWND_TOP,	x,		y+35,			70,		10,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM3),			HWND_TOP,	x+360-21*1-2,	y+35,	23,		10,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM2),			HWND_TOP,	x+360-21*3-2,	y+35,	23,		10,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ALGORITHM1),			HWND_TOP,	x+360-21*5-2,	y+35,	23,		10,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_ALGORITHM),		HWND_TOP,	x+360-187,		y+32,	70,		16,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_FOCUS_DIST),		HWND_TOP,	x,		y+62,			94,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_FOCUS_DIST),			HWND_TOP,	x+97,	y+60,			68,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_THREADS),			HWND_TOP,	x+246,	y+62,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_QUALITY),			HWND_TOP,	x+246,	y+92,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_THREADS),				HWND_TOP,	x+313,	y+60,			48,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_QUALITY),				HWND_TOP,	x+313,	y+90,			48,		20,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_PICK_FOCUS),			HWND_TOP,	x+0,	y+122,			115,	22,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_DOF_RANGE),		HWND_TOP,	x+123,	y+122,			116,	22,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_SHOW_ZDEPTH),			HWND_TOP,	x+244,	y+122,			116,	22,		0);

	x = 8;	y = 468;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_GR_APERTURE),			HWND_TOP,	x,		y,				360,	16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_BLADES),			HWND_TOP,	x,		y+90,			80,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RING_BALANCE),	HWND_TOP,	x,		y+120,			80,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_FLATTEN),			HWND_TOP,	x,		y+150,			80,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_APERTURE),		HWND_TOP,	x+240,	y+30,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RADIUS),			HWND_TOP,	x+240,	y+60,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_ANGLE),			HWND_TOP,	x+240,	y+90,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_RING_SIZE),		HWND_TOP,	x+240,	y+120,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_TEXT_VIGNETTE),		HWND_TOP,	x+240,	y+150,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_BLADES),			HWND_TOP,	x+85,	y+ 88,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_BALANCE),		HWND_TOP,	x+85,	y+118,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_FLATTEN),			HWND_TOP,	x+85,	y+148,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_APERTURE),			HWND_TOP,	x+305,	y+ 28,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RADIUS),			HWND_TOP,	x+305,	y+ 58,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ANGLE),				HWND_TOP,	x+305,	y+ 88,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_SIZE),			HWND_TOP,	x+305,	y+118,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_VIGNETTE),			HWND_TOP,	x+305,	y+148,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_FAKEDOF_ON2),			HWND_TOP,	x+0,	y+33,			90,		10,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ROUND_BLADES),		HWND_TOP,	x+0,	y+63,			100,	10,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_LOAD_FROM_CAMERA),	HWND_TOP,	x,		y+180,			127,	22,		0);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_LENS),			HWND_TOP,											0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_DEPTH),		GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_LENS),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_ACHROMATIC),	GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_DEPTH),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_RED),			GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_ACHROMATIC),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GREEN),		GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_RED),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_BLUE),			GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_GREEN),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_FOCUS_DIST),		GetDlgItem(hWnd, IDC_REND2_FDOF_ABB_BLUE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_THREADS),			GetDlgItem(hWnd, IDC_REND2_FDOF_FOCUS_DIST),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_QUALITY),			GetDlgItem(hWnd, IDC_REND2_FDOF_THREADS),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_APERTURE),		GetDlgItem(hWnd, IDC_REND2_FDOF_QUALITY),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RADIUS),		GetDlgItem(hWnd, IDC_REND2_FDOF_AP_APERTURE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_BLADES),		GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RADIUS),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ANGLE),			GetDlgItem(hWnd, IDC_REND2_FDOF_AP_BLADES),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_BALANCE),	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ANGLE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_SIZE),		GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_BALANCE),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_FLATTEN),		GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_SIZE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_FDOF_AP_VIGNETTE),		GetDlgItem(hWnd, IDC_REND2_FDOF_AP_FLATTEN),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);



}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillFakeDOFTabAll(FinalModifier * fMod)
{
	CHECK(fMod);
	CHECK(hTabFakeDof);

	EMAberration *		emab			= GetEMAberrationInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_GRAPH));
	EM2EditSpin *		emesLens		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_LENS));
	EM2EditSpin *		emesDepth		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_DEPTH));
	EM2EditSpin *		emesAchromatic	= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_ACHROMATIC));
	EM2EditSpin *		emesRed			= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_RED));
	EM2EditSpin *		emesGreen		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_GREEN));
	EM2EditSpin *		emesBlue		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ABB_BLUE));
	EM2CheckBox *		emcDOFon		= GetEM2CheckBoxInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON));
	EM2CheckBox *		emcDOFon2		= GetEM2CheckBoxInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON2));
	EM2CheckBox *		emcAlgorithm1	= GetEM2CheckBoxInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM1));
	EM2CheckBox *		emcAlgorithm2	= GetEM2CheckBoxInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM2));
	EM2CheckBox *		emcAlgorithm3	= GetEM2CheckBoxInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_ALGORITHM3));
	EM2EditSpin *		emesFocusDist	= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FOCUS_DIST));
	EM2EditSpin *		emesThreads		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_THREADS));
	EM2EditSpin *		emesQuality		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_QUALITY));
	EM2CheckBox *		emcRoundBlades	= GetEM2CheckBoxInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_ROUND_BLADES));
	EM2EditSpin *		emesAperture	= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_APERTURE));
	EM2EditSpin *		emesBlades		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_BLADES));
	EM2EditSpin *		emesAngle		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_ANGLE));
	EM2EditSpin *		emesRadius		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RADIUS));
	EM2EditSpin *		emesRingBalance	= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RING_BALANCE));
	EM2EditSpin *		emesRingSize	= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_RING_SIZE));
	EM2EditSpin *		emesFlatten		= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_FLATTEN));
	EM2EditSpin *		emesVignette	= GetEM2EditSpinInstance(	GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_VIGNETTE));

	emab->abbShiftXY = fMod->chromaticShiftLens;
	emab->abbShiftZ = fMod->chromaticShiftDepth;
	emab->abbAchromatic= fMod->chromaticAchromatic;
	InvalidateRect(emab->hwnd, NULL, false);

	emesLens->setFloatValue(fMod->chromaticShiftLens);
	emesDepth->setFloatValue(fMod->chromaticShiftDepth);
	emesAchromatic->setFloatValue(fMod->chromaticAchromatic);
	emesRed->setFloatValue(fMod->chromaticPlanarRed);
	emesGreen->setFloatValue(fMod->chromaticPlanarGreen);
	emesBlue->setFloatValue(fMod->chromaticPlanarBlue);

	emcDOFon->selected = fMod->dofOn;
	emcDOFon2->selected = fMod->dofOn;
	emcAlgorithm1->selected = (fMod->algorithm==0);
	emcAlgorithm2->selected = (fMod->algorithm==1);
	emcAlgorithm3->selected = (fMod->algorithm==2);
	InvalidateRect(emcDOFon->hwnd, NULL, false);
	InvalidateRect(emcDOFon2->hwnd, NULL, false);
	InvalidateRect(emcAlgorithm1->hwnd, NULL, false);
	InvalidateRect(emcAlgorithm2->hwnd, NULL, false);
	InvalidateRect(emcAlgorithm3->hwnd, NULL, false);
	emesFocusDist->setFloatValue(fMod->focusDistance);
	emesThreads->setIntValue(fMod->numThreads);
	emesQuality->setIntValue(fMod->quality);

	emcRoundBlades->selected = fMod->roundBlades;
	InvalidateRect(emcRoundBlades->hwnd, NULL, false);
	emesAperture->setFloatValue(fMod->aperture);
	emesBlades->setIntValue(fMod->bladesNum);
	emesAngle->setFloatValue(fMod->bladesAngle);
	emesRadius->setFloatValue(fMod->bladesRadius);
	emesRingBalance->setIntValue(fMod->bokehRingBalance);
	emesRingSize->setIntValue(fMod->bokehRingSize);
	emesFlatten->setIntValue(fMod->bokehFlatten);
	emesVignette->setIntValue(fMod->bokehVignette);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateDiaphragmImageFake(HWND hWnd, Camera * cam, float depth)
{
	CHECK(hWnd);
	CHECK(cam);

	DiaphragmSampler diaph;
	diaph.initializeDiaphragm(cam->fMod.bladesNum, 1.0f, cam->fMod.bladesAngle, cam->fMod.roundBlades, cam->fMod.bladesRadius);
	diaph.intBokehRingBalance = cam->fMod.bokehRingBalance;
	diaph.intBokehRingSize = cam->fMod.bokehRingSize;
	diaph.intBokehFlatten = cam->fMod.bokehFlatten;
	diaph.intBokehVignette = cam->fMod.bokehVignette;
	diaph.chromaticShiftLens = cam->fMod.chromaticShiftLens;
	diaph.chromaticShiftDepth = cam->fMod.chromaticShiftDepth;
	diaph.chromaticAchromatic = cam->fMod.chromaticAchromatic;
	if (diaph.intBokehRingBalance>=0)
		diaph.bokehRingBalance = diaph.intBokehRingBalance+1.0f;
	else
		diaph.bokehRingBalance = 1.0f + diaph.intBokehRingBalance/21.0f;
	diaph.bokehRingSize = diaph.intBokehRingSize*0.01f;
	diaph.bokehFlatten = diaph.intBokehFlatten*0.01f;
	diaph.bokehVignette = diaph.intBokehVignette*0.01f;

	HDC thdc = GetDC(hWnd);
	HBITMAP hbmp = diaph.createApertureShapePreviewChromaticBokehAlpha(thdc , 64, depth);
	ReleaseDC(hWnd, thdc);
	if (!hbmp)
		return false;
	RendererMain2 * rMain = RendererMain2::getInstance();
	if (rMain->hDiaph)
		DeleteObject(rMain->hDiaph);
	rMain->hDiaph = hbmp;

	return true;
}

//------------------------------------------------------------------------------------------------

bool copyFakeDofFromCamera(HWND hWnd, Camera * cam)
{
	CHECK(cam);

	cam->fMod.aperture = cam->aperture;
	cam->fMod.roundBlades = cam->apBladesRound;
	cam->fMod.bladesNum = cam->apBladesNum;
	cam->fMod.bladesAngle = cam->apBladesAngle;
	cam->fMod.bladesRadius = cam->apBladesRadius;
	cam->fMod.bokehRingBalance = cam->diaphSampler.intBokehRingBalance;
	cam->fMod.bokehRingSize = cam->diaphSampler.intBokehRingSize;
	cam->fMod.bokehFlatten = cam->diaphSampler.intBokehFlatten;
	cam->fMod.bokehVignette = cam->diaphSampler.intBokehVignette;

	CHECK(hWnd);

	EM2CheckBox *	emcRoundBlades	= GetEM2CheckBoxInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ROUND_BLADES));
	EM2EditSpin *	emesAperture	= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_APERTURE));
	EM2EditSpin *	emesBlades		= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_BLADES));
	EM2EditSpin *	emesAngle		= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_ANGLE));
	EM2EditSpin *	emesRadius		= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RADIUS));
	EM2EditSpin *	emesRingBalance	= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_BALANCE));
	EM2EditSpin *	emesRingSize	= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_RING_SIZE));
	EM2EditSpin *	emesFlatten		= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_FLATTEN));
	EM2EditSpin *	emesVignette	= GetEM2EditSpinInstance(	GetDlgItem(hWnd, IDC_REND2_FDOF_AP_VIGNETTE));

	emcRoundBlades->selected = cam->fMod.roundBlades;
	InvalidateRect(emcRoundBlades->hwnd, NULL, false);
	emesAperture->setFloatValue(cam->fMod.aperture);
	emesBlades->setIntValue(cam->fMod.bladesNum);
	emesAngle->setFloatValue(cam->fMod.bladesAngle);
	emesRadius->setFloatValue(cam->fMod.bladesRadius);
	emesRingBalance->setIntValue(cam->fMod.bokehRingBalance);
	emesRingSize->setIntValue(cam->fMod.bokehRingSize);
	emesFlatten->setIntValue(cam->fMod.bokehFlatten);
	emesVignette->setIntValue(cam->fMod.bokehVignette);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

DWORD WINAPI RendererMain2::RedrawFinalThreadProc(LPVOID lpParameter)
{
	ThreadWindow2 * curProgWindow = ThreadWindow2::getCurrentInstance();
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	sc->abortThreadPtr = &curProgWindow->aborted;
	Camera * cam = sc->getActiveCamera();

	if (!cam)
	{
		if (curProgWindow)
		{
			curProgWindow->closeWindow();
			delete curProgWindow;
		}
		return 0;
	}

	sc->registerProgressCallback(ThreadWindow2::notifyProgressBarCallback, 100);

	if (!cam->depthBuf   ||   	!cam->depthOK)
	{
		if (!cam->depthBuf)
			cam->depthBuf = new FloatBuffer();
		if (!cam->depthBuf->fbuf   ||   cam->depthBuf->width!=cam->width*cam->aa   ||   cam->depthBuf->height!=cam->height*cam->aa)
			cam->depthBuf->allocBuffer(cam->width*cam->aa, cam->height*cam->aa, true, true);

		rtr->curScenePtr->evalDepthMap(cam);
	}


	HWND hImg = (HWND)lpParameter;
	EMPView * empvmain = GetEMPViewInstance(hImg);
	updateRenderedImage(empvmain, true, true, NULL, false);

	InvalidateRect(hImg, NULL, false);

	if (curProgWindow)
	{
		curProgWindow->closeWindow();
		sc->abortThreadPtr = NULL;
		delete curProgWindow;
	}

	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::runRedrawFinalThread()
{
	ThreadWindow2 * tWindow = new ThreadWindow2();
	tWindow->createWindow(RendererMain2::getHWND(), hDllModule);
	tWindow->setWindowTitle("Finalizing rendered image");
	tWindow->setText("FINALIZING RENDERED IMAGE");

	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(RedrawFinalThreadProc),
            (LPVOID)hViewport,
            0,
            &threadID);
	return true;

}

//------------------------------------------------------------------------------------------------

bool RendererMain2::showZDepth()
{
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (!cam)
	{
		circleActive = false;
		return false;
	}

	if (!cam->depthBuf   ||   	!cam->depthOK)
		rtr->curScenePtr->evalDepthMap(cam);

	circleActive = false;

	CHECK(cam->depthBuf);
	CHECK(cam->depthBuf->fbuf);

	EMPView * empv = GetEMPViewInstance(hViewport);
	cam->depthBuf->copyAsDepthToByteBuffer(empv->byteBuff, cam->aa);
	Sleep(10);
	InvalidateRect(hViewport, NULL, FALSE);
	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI showZDepthThreadProc(LPVOID lpParameter)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	rMain->showZDepth();
	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::runShowZDepthThread()
{
	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(showZDepthThreadProc),
            (LPVOID)0,
            0,
            &threadID);
	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI evalZDepthThreadProc(LPVOID lpParameter)
{
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	rtr->curScenePtr->evalDepthMap(cam);
	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::evalDepthMapOnThread()
{
	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(evalZDepthThreadProc),
            (LPVOID)0,
            0,
            &threadID);
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::drawDofRange()
{
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(!rtr->curScenePtr->nowRendering);
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	EMPView * empvmain = GetEMPViewInstance(hViewport);
	CHECK(cam);
	CHECK(empvmain);
	float lng = 0.5f * 0.036f / tan(cam->angle * PI / 360.0f);
	cam->depthBuf->copyAsDOFTemperatureToByteBuffer(empvmain->byteBuff, cam->fMod.focusDistance, cam->aa, lng, cam->fMod.aperture);
	InvalidateRect(hViewport, NULL, false);
	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI drawDofRangeThreadProc(LPVOID lpParameter)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (!cam->depthBuf   ||   	!cam->depthOK)
		rtr->curScenePtr->evalDepthMap(cam);
	circleActive = false;
	rMain->drawDofRange();
	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::drawDofRangeOnThread()
{
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(!rtr->curScenePtr->nowRendering);
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	EMPView * empvmain = GetEMPViewInstance(hViewport);
	CHECK(cam);
	CHECK(empvmain);
	if (!cam->depthBuf   ||   	!cam->depthOK)
	{
		RECT crect;
		GetClientRect(hViewport, &crect);
		openAnimCircleDialog(getHWND(), (crect.right)/2, (crect.bottom)/2);
	}
	else
	{
		drawDofRange();
		return true;
	}

	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(drawDofRangeThreadProc),
            (LPVOID)0,
            0,
            &threadID);
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::turnOffDofDephtModes()
{
	BUTTON_SELECT(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_PICK_FOCUS), false);
	BUTTON_SELECT(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_SHOW_DOF_RANGE), false);
	BUTTON_SELECT(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_SHOW_ZDEPTH), false);
	EMPView * empv = GetEMPViewInstance(hViewport);
	if (empv->mode == EMPView::MODE_PICK)
	{
		empv->mode = EMPView::MODE_SHOW;
		showPickModeInStatus(false);
	}
	udpateRegionPanelModeFromViewport();
	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabFakeDof(bool nowrendering)
{
	setControlEnabled(!nowrendering, hTabFakeDof, IDC_REND2_FDOF_REDRAW);
	setControlEnabled(!nowrendering, hTabFakeDof, IDC_REND2_FDOF_PICK_FOCUS);
	setControlEnabled(!nowrendering, hTabFakeDof, IDC_REND2_FDOF_SHOW_DOF_RANGE);
	setControlEnabled(!nowrendering, hTabFakeDof, IDC_REND2_FDOF_SHOW_ZDEPTH);

	EM2CheckBox * emcDOFon1 = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON));
	EM2CheckBox * emcDOFon2 = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_FAKEDOF_ON2));
	EM2CheckBox * emcRoundBlades = GetEM2CheckBoxInstance(GetDlgItem(hTabFakeDof, IDC_REND2_FDOF_AP_ROUND_BLADES));
	bool dofon = emcDOFon1->selected  &&  emcDOFon2->selected;
	bool rounddof = emcRoundBlades->selected && dofon;

	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_ALGORITHM1);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_ALGORITHM2);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_ALGORITHM3);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_THREADS);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_QUALITY);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_ROUND_BLADES);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_BLADES);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_RING_BALANCE);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_FLATTEN);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_APERTURE);
	setControlEnabled(rounddof, hTabFakeDof, IDC_REND2_FDOF_AP_RADIUS);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_ANGLE);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_RING_SIZE);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_AP_VIGNETTE);

	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_ALGORITHM);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_THREADS);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_QUALITY);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_BLADES);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_RING_BALANCE);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_FLATTEN);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_APERTURE);
	setControlEnabled(rounddof, hTabFakeDof, IDC_REND2_FDOF_TEXT_RADIUS);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_ANGLE);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_RING_SIZE);
	setControlEnabled(dofon, hTabFakeDof, IDC_REND2_FDOF_TEXT_VIGNETTE);
}

//------------------------------------------------------------------------------------------------
