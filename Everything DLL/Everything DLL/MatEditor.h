#ifndef __MAT_EDITOR_MAIN_H
#define __MAT_EDITOR_MAIN_H

#include "DLL.h"
#include <windows.h>
#include "ColorSchemes.h"
#include "MatDownload.h"

// old version of material editor (before nox 0.40)

#define RES_PR_X 192
#define RES_PR_Y 192

class MatEditorWindow;
class OnlineMatLib;
class LocalMatLib;
class MatCategory;


//------------------------------------------------------------------------------------------------------

class MatEditorWindow
{
public:
	static const int MODE_ALONE = 0;
	static const int MODE_RENDERER = 1;
	static const int MODE_3DSMAX = 2;
	static int libIDs[];


	static OnlineMatLib onlinematlib;
	static LocalMatLib localmatlib;
	static int autoCategory;
	static int autoID;
	static int libMode;
	static int libHiddenOKTop;
	static int libShownOKTop;
	static int clientToWindowX;
	static int clientToWindowY;
	static int libHiddenHeight;
	static int libShownHeight;
	static int width;
	static int leftCancel;
	static int leftOK;
	static bool saveOnOK;
	static HINSTANCE hInstance;
	static HWND hMain;
	static HANDLE hRefreshThread;
	static bool refreshThreadRunning;
	static bool needToUpdateDisplacement;

	static HBRUSH backgroundBrush;
	static void applyControlStyles();
	static BlendSettings blendNames;


	static bool fillLayersList();
	static bool materialToGUI();
	static bool layerToGUI(int num);
	static bool GUItoLayer(int num);
	static bool GUInameToMaterial();
	static bool preWindowOpen();
	static bool postWindowOpen();
	static bool updateMaterialToEditableMaterial();
	static bool changeScene(int id);
	static MatLayer * getEditedMatLayer();
	static MaterialNox * getEditedMaterial();
	static int getEditedLayerNumber();
	static bool resetEditedMaterialOnAllScenes();
	static bool getColorUnderPointer(COLORREF & col);
	static void releaseMatLibBuffers();

	static bool copyBufferWindowToMaterial();
	static bool copyBufferMaterialToWindow();


	static bool layerAdd();
	static bool layerDel();
	static bool layerClone();
	static bool resetMaterialDelTextures();

	static bool updateDisplacement();
	static void lockBeforeStart();
	static void unlockAfterStop();
	static void lockBeforeTextures();
	static void unlockAfterTextures();
	static bool startRendering();
	static bool stopRendering();
	static bool refresh();
	static void runRefreshThread(float s);
	static void stopRefreshThread();
	static void unclickAllCategories(int except);
	static void clickedLibCategory(int cat);
	static void clickedLibTrayOpen();
	static void clickedLibOnline();
	static void clickedLibPrev();
	static void clickedLibNext();
	static void clickedLibPage();
	static void setPageControl(int current, int all);	// 0-based
	static void clickedLibPreview(int which);
	static void clickedTwiceLibPreview(int which);
	static void iconsGoToOnline(bool online);
	static int  findSelectedMatInLib();
	static int  findMaterialsInLocalLib(char * key, int page);
	static bool invokeUserPassDialog();
	static char * getSearchText();


	DECLDIR bool createWindow(HWND parent, int mode, char * fileToLoad);
	static bool acceptMaterial();
	DECLDIR HWND getHWND();

	static bool loadMaterial();
	static bool loadMaterial(char * filename);
	static char * autoLoadFile;
	static bool saveMaterial();
	static bool uploadMaterial();
	static bool updateLightMeshes();

	static bList<Texture, 8> texToDel;

	static void showLibrary(bool show);
	
	static DWORD WINAPI RefreshThreadProc(LPVOID lpParameter);
	static INT_PTR CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	DECLDIR static void copyBlendNamesFrom(BlendSettings * blend);
	DECLDIR void setSaveMode(bool saveOnOKButton);

	DECLDIR MatEditorWindow(HINSTANCE hInst);
	DECLDIR ~MatEditorWindow();
};

//------------------------------------------------------------------------------------------------------

class OnlineMatLib
{
public:

	class OnlineMatInfo
	{
	public:
		int filesize;
		int category;
		int layers;
		int textures;
		int id;
		char * name;
		char * filename;
		char * imagename;
		char * author;
		char * description;

		OnlineMatInfo();
		~OnlineMatInfo();
		void clearAll();
		bool copyFromOther(OnlineMatLib::OnlineMatInfo * other);		
	};

	int queryMode;
	int currentCategory;
	int currentPage;
	int totalResults;
	static const int MODE_SEARCH = 0;
	static const int MODE_MY_MATS = 1;
	static const int MODE_CATEGORY = 2;

	char * username;
	char * passwordhash;
	bool remember;
	bool invalidUserPass;

	MatDownloader downloaders[14];
	char tempDir[2048];
	char files[7][2048];
	char xmlFilename[2048];
	bList<char*, 16> toDel;
	OnlineMatInfo mat7Info[7];
	
	bool generateTempFilename(int num);
	bool createTempDir();
	void stopAllDownloads();
	void clearPreviewControls(bool refresh=true);
	void deleteAllTempFiles(bool delDir=false);
	bool downloadXMLinfoByCategory(int category, int page);
	bool downloadXMLinfoBySearch(char * text, int page);
	bool downloadXMLinfoByUsername(int page);

	OnlineMatLib();
};

//------------------------------------------------------------------------------------------------------

class LocalMatLib
{
public:

	class MatInfo
	{
	public:
		int unique;
		int layers;
		int textures;
		char * name;
		char * filename;
		ImageByteBuffer preview;

		bool processFromDir(char * dir);
		void releaseEverything();
		MatInfo();
		~MatInfo();
	};


	int queryMode;
	int currentCategory;
	int currentPage;
	LocalMatLib::MatInfo * searchResults[7];
	char searchResCats[7];
	int numSearchResults;

	static const int MODE_SEARCH = 0;
	static const int MODE_MY_MATS = 1;
	static const int MODE_CATEGORY = 2;

	char * getLocalMaterialsDirectoryName();
	char * getLocalCategoryDirectoryName(int cat);
	bool createMaterialsList();
	bool initialize();


	bool saveToLibrary();
	bool removeFromLibrary(int cat, int num);

	bool showByCategory(int cat, int page);
	bool refreshCategory(int cat);
	bool loadMaterial(int cat, int num);

	bList<LocalMatLib::MatInfo, 8> * localList;

	LocalMatLib();
	~LocalMatLib();
};

//------------------------------------------------------------------------------------------------------

class MatCategory
{
public:
	static const int CAT_EMITTER = 0;
	static const int CAT_BRICKS = 1;
	static const int CAT_CARPAINT = 2;
	static const int CAT_CERAMIC = 3;
	static const int CAT_CONCRETE = 4;
	static const int CAT_FABRIC = 5;
	static const int CAT_FOOD = 6;
	static const int CAT_GLASS = 7;
	static const int CAT_GROUND = 8;
	static const int CAT_LEATHER = 9;
	static const int CAT_LIQUIDS = 10;
	static const int CAT_METAL = 11;
	static const int CAT_MINERALS_GEMS = 12;
	static const int CAT_ORGANIC = 13;
	static const int CAT_PLASTIC = 14;
	static const int CAT_PORCELAIN = 15;
	static const int CAT_SSS = 16;
	static const int CAT_STONE = 17;
	static const int CAT_TILES = 18;
	static const int CAT_WALLS = 19;
	static const int CAT_WOOD = 20;
	static const int CAT_OTHER = 21;
	static const int CAT_USER = 22;

	static char * getCategoryName(int cat)
		{
			switch (cat)
			{
				case CAT_EMITTER: return "Emitter";
				case CAT_BRICKS: return "Bricks";
				case CAT_CARPAINT: return "Carpaint";
				case CAT_CERAMIC: return "Ceramic";
				case CAT_CONCRETE: return "Concrete";
				case CAT_FABRIC: return "Fabric";
				case CAT_FOOD: return "Food";
				case CAT_GLASS: return "Glass";
				case CAT_GROUND: return "Ground";
				case CAT_LEATHER: return "Leather";
				case CAT_LIQUIDS: return "Liquids";
				case CAT_METAL: return "Metals";
				case CAT_MINERALS_GEMS: return "Minerals or gems";
				case CAT_ORGANIC: return "Organic";
				case CAT_PLASTIC: return "Plastic";
				case CAT_PORCELAIN: return "Porcelain";
				case CAT_SSS: return "SSS";
				case CAT_STONE: return "Stone";
				case CAT_TILES: return "Tiles";
				case CAT_WALLS: return "Walls";
				case CAT_WOOD: return "Wood";
				case CAT_OTHER: return "Other";
				case CAT_USER: return "User";
				default: return "Error";
			}
		}
};

//------------------------------------------------------------------------------------------------------

#endif
