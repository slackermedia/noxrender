#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>
#include "metropolis.h"
#include "PathTracing.h"

bool formatStringFromUnsignedInteger(char * buf, int bufSize, int numDigits, unsigned int number);

extern bool bdpt_connect_to_cam;
extern bool bdpt_can_hit_emitter;
extern int bdpt_max_refl;

#define ROUND(a) (int)floor(a+0.5)
bool showMLT = false;

void normalizePDFs(int constant);

void rendererOutputArrangeControls(HWND hWnd);
void rendererOutputShowControls(HWND hWnd, int engine, int sampling);

extern HMODULE hDllModule;

INT_PTR CALLBACK RendererMainWindow::OutputWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				HWND hGroup			= GetDlgItem(hWnd, IDC_REND_OUTPUT_GROUP);
				HWND hGroup1		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_SETT);
				HWND hGroup2		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_TIMERS);
				HWND hGroup3		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_METROPOLIS);
				HWND hGroup4		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_SETT2);
				HWND hWidth			= GetDlgItem(hWnd, IDC_REND_OUTPUT_WIDTH);
				HWND hHeight		= GetDlgItem(hWnd, IDC_REND_OUTPUT_HEIGHT);
				HWND hAA			= GetDlgItem(hWnd, IDC_REND_OUTPUT_AA);
				HWND hAspect		= GetDlgItem(hWnd, IDC_REND_OUTPUT_ASPECT);
				HWND hStopOn		= GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_ON);
				HWND hStopHH		= GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_HH);
				HWND hStopMM		= GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_MM);
				HWND hStopSS		= GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_SS);
				HWND hRefrOn		= GetDlgItem(hWnd, IDC_REND_OUTPUT_REFRESH_ON);
				HWND hRefrSS		= GetDlgItem(hWnd, IDC_REND_OUTPUT_REFRESH_SECONDS);
				HWND hASaveOn		= GetDlgItem(hWnd, IDC_REND_OUTPUT_AUTOSAVE_ON);
				HWND hASaveMM		= GetDlgItem(hWnd, IDC_REND_OUTPUT_AUTOSAVE_MINUTES);
				HWND hEngine		= GetDlgItem(hWnd, IDC_REND_OUTPUT_ENGINE);
				HWND hMutLens		= GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_LENS);
				HWND hMutBidir		= GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_BIDIR);
				HWND hPertLens		= GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_LENS);
				HWND hPertCaust		= GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_CAUSTIC);
				HWND hPertChain		= GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_CHAIN);
				HWND hLPMin			= GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MIN);
				HWND hLPMax			= GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MAX);
				HWND hCPMin			= GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MIN);
				HWND hCPMax			= GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MAX);
				HWND hChP1Min		= GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MIN);
				HWND hChP1Max		= GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MAX);
				HWND hChP2Min		= GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MIN);
				HWND hChP2Max		= GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MAX);
				HWND hGIsamples		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GI_SAMPLES);
				HWND hRandom		= GetDlgItem(hWnd, IDC_REND_OUTPUT_RANDOM_TYPE);
				HWND hSecondary		= GetDlgItem(hWnd, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC);
				HWND hFakeCaustic	= GetDlgItem(hWnd, IDC_REND_OUTPUT_FAKE_CAUSTIC);
				HWND hCausticRough	= GetDlgItem(hWnd, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH);
				HWND hRunNext		= GetDlgItem(hWnd, IDC_REND_OUTPUT_RUN_NEXT_ON);
				HWND hRunNextSett	= GetDlgItem(hWnd, IDC_REND_OUTPUT_RUN_NEXT_SETTINGS);
				HWND hEmitterHit	= GetDlgItem(hWnd, IDC_REND_OUTPUT_EMITTER_HIT);
				HWND hCameraConnect = GetDlgItem(hWnd, IDC_REND_OUTPUT_CAMERA_CONNECT);
				HWND hBDPTMaxRefl	= GetDlgItem(hWnd, IDC_REND_OUTPUT_BDPT_MAX_REFL);

				HWND hGroup5		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_INTERSECTIONS);
				HWND hGroup6		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_BDPT);
				HWND hGroup7		= GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_BUCKETS);
				HWND hShade			= GetDlgItem(hWnd, IDC_REND_OUTPUT_SHADE);

				HWND hBSizeX		= GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_X);
				HWND hBSizeY		= GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_Y);
				HWND hSamples		= GetDlgItem(hWnd, IDC_REND_OUTPUT_SAMPLES);
				HWND hStopAfterPass = GetDlgItem(hWnd, IDC_REND_OUTPUT_STOP_AFTER_PASS);
				HWND hRISsamples	= GetDlgItem(hWnd, IDC_REND_OUTPUT_RIS_SAMPLES);

				EMGroupBar * emgb1  = GetEMGroupBarInstance(hGroup);
				EMGroupBar * emgb2  = GetEMGroupBarInstance(hGroup1);
				EMGroupBar * emgb3  = GetEMGroupBarInstance(hGroup2);
				EMGroupBar * emgb4  = GetEMGroupBarInstance(hGroup3);
				EMGroupBar * emgb5  = GetEMGroupBarInstance(hGroup4);
				EMGroupBar * emgb6  = GetEMGroupBarInstance(hGroup5);
				EMGroupBar * emgb7  = GetEMGroupBarInstance(hGroup6);
				EMGroupBar * emgb8  = GetEMGroupBarInstance(hGroup7);
				EMEditSpin * emes1 = GetEMEditSpinInstance(hWidth);
				EMEditSpin * emes2 = GetEMEditSpinInstance(hHeight);
				EMEditSpin * emes4 = GetEMEditSpinInstance(hStopHH);
				EMEditSpin * emes5 = GetEMEditSpinInstance(hStopMM);
				EMEditSpin * emes6 = GetEMEditSpinInstance(hStopSS);
				EMEditSpin * emes7 = GetEMEditSpinInstance(hRefrSS);
				EMEditSpin * emes8 = GetEMEditSpinInstance(hASaveMM);
				EMEditSpin * emes9 = GetEMEditSpinInstance(hAA);
				EMEditSpin * emes10 = GetEMEditSpinInstance(hBDPTMaxRefl);
				EMEditSpin * emes11 = GetEMEditSpinInstance(hBSizeX);
				EMEditSpin * emes12 = GetEMEditSpinInstance(hBSizeY);
				EMEditSpin * emes13 = GetEMEditSpinInstance(hSamples);
				EMEditSpin * emes14 = GetEMEditSpinInstance(hGIsamples);
				EMEditSpin * emes15 = GetEMEditSpinInstance(hRISsamples);
				EMEditTrackBar * emetb1 = GetEMEditTrackBarInstance(hMutLens);
				EMEditTrackBar * emetb2 = GetEMEditTrackBarInstance(hMutBidir);
				EMEditTrackBar * emetb3 = GetEMEditTrackBarInstance(hPertLens);
				EMEditTrackBar * emetb4 = GetEMEditTrackBarInstance(hPertCaust);
				EMEditTrackBar * emetb5 = GetEMEditTrackBarInstance(hPertChain);

				EMEditTrackBar * emetbLPmin = GetEMEditTrackBarInstance(hLPMin);
				EMEditTrackBar * emetbLPmax = GetEMEditTrackBarInstance(hLPMax);
				EMEditTrackBar * emetbCPmin = GetEMEditTrackBarInstance(hCPMin);
				EMEditTrackBar * emetbCPmax = GetEMEditTrackBarInstance(hCPMax);
				EMEditTrackBar * emetbChP1min = GetEMEditTrackBarInstance(hChP1Min);
				EMEditTrackBar * emetbChP1max = GetEMEditTrackBarInstance(hChP1Max);
				EMEditTrackBar * emetbChP2min = GetEMEditTrackBarInstance(hChP2Min);
				EMEditTrackBar * emetbChP2max = GetEMEditTrackBarInstance(hChP2Max);

				EMEditTrackBar * emetbCaustRough = GetEMEditTrackBarInstance(hCausticRough);

				EMCheckBox * emcb   = GetEMCheckBoxInstance(hAspect);
				EMCheckBox * emcb2  = GetEMCheckBoxInstance(hStopOn);
				EMCheckBox * emcb3  = GetEMCheckBoxInstance(hRefrOn);
				EMCheckBox * emcb4  = GetEMCheckBoxInstance(hASaveOn);
				EMComboBox * eEng   = GetEMComboBoxInstance(hEngine);
				EMComboBox * eRand  = GetEMComboBoxInstance(hRandom);
				EMComboBox * eShade = GetEMComboBoxInstance(hShade);
				EMCheckBox * emcb6  = GetEMCheckBoxInstance(hSecondary);
				EMCheckBox * emcb7  = GetEMCheckBoxInstance(hFakeCaustic);
				EMCheckBox * emcb8  = GetEMCheckBoxInstance(hRunNext);
				EMCheckBox * emcb9  = GetEMCheckBoxInstance(hEmitterHit);
				EMCheckBox * emcb10 = GetEMCheckBoxInstance(hCameraConnect);
				EMCheckBox * emcb11 = GetEMCheckBoxInstance(hStopAfterPass);
				emcb9->selected = bdpt_can_hit_emitter;
				emcb10->selected = bdpt_connect_to_cam;
				emcb6->selected = Raytracer::getInstance()->curScenePtr->sscene.disableSecondaryCaustic;
				emcb7->selected = Raytracer::getInstance()->curScenePtr->sscene.causticFake;
				emcb11->selected = 	Raytracer::getInstance()->curScenePtr->sscene.stop_after_pass;
			
				EMButton * emb1 = GetEMButtonInstance(hRunNextSett);

				Metropolis::discardDirect = true;

				eEng->addItem("Path Tracing");
				eEng->addItem("BDPT");
				eEng->addItem("Metropolis LT");
				eEng->addItem("Intersections");
				switch (Raytracer::getInstance()->curScenePtr->sscene.giMethod)
				{
					case Raytracer::GI_METHOD_PATH_TRACING: 				eEng->selected = 0;		break;
					case Raytracer::GI_METHOD_BIDIRECTIONAL_PATH_TRACING:	eEng->selected = 1;		break;
					case Raytracer::GI_METHOD_METROPOLIS:					eEng->selected = 2;		break;
					case Raytracer::GI_METHOD_INTERSECTIONS: 				eEng->selected = 3;		break;
					default:					 				eEng->selected = 0;		break;
				}
				theme.apply(eEng);


				eRand->addItem("Scan Line");
				eRand->addItem("Halton");
				eRand->addItem("Marsaglia");
				eRand->addItem("Rand");
				eRand->addItem("Rand_s");
				eRand->addItem("Lattice");
				eRand->addItem("Buckets");
				eRand->selected = Raytracer::getInstance()->curScenePtr->sscene.random_type;
				theme.apply(eRand);

				eShade->addItem("Shade smooth");
				eShade->addItem("Shade geometry");
				eShade->addItem("Draw normals");
				eShade->addItem("Draw normals geom");
				eShade->selected = ShadeTracer::shade_type;
				theme.apply(eShade);

				emcb->selected = true;
				emcb3->selected = true;
				theme.apply(emgb1);
				theme.apply(emgb2);
				theme.apply(emgb3);
				theme.apply(emgb4);
				theme.apply(emgb5);
				theme.apply(emgb6);
				theme.apply(emgb7);
				theme.apply(emgb8);
				theme.apply(emes1);
				theme.apply(emes2);
				theme.apply(emes4);
				theme.apply(emes5);
				theme.apply(emes6);
				theme.apply(emes7);
				theme.apply(emes8);
				theme.apply(emes9);
				theme.apply(emes10);
				theme.apply(emes11);
				theme.apply(emes12);
				theme.apply(emes13);
				theme.apply(emes14);
				theme.apply(emes15);
				theme.apply(emcb);
				theme.apply(emcb2);
				theme.apply(emcb3);
				theme.apply(emcb4);
				theme.apply(emcb6);
				theme.apply(emcb7);
				theme.apply(emetb1);
				theme.apply(emetb2);
				theme.apply(emetb3);
				theme.apply(emetb4);
				theme.apply(emetb5);
				theme.apply(emetbLPmin);
				theme.apply(emetbLPmax);
				theme.apply(emetbCPmin);
				theme.apply(emetbCPmax);
				theme.apply(emetbChP1min);
				theme.apply(emetbChP1max);
				theme.apply(emetbChP2min);
				theme.apply(emetbChP2max);
				theme.apply(emetbCaustRough);
				theme.apply(emb1);
				theme.apply(emcb8);
				theme.apply(emcb9);
				theme.apply(emcb10);
				theme.apply(emcb11);
				emes1->setValuesToInt(640, NOX_IMAGE_WIDTH_MIN, NOX_IMAGE_WIDTH_MAX, 1, 1);
				emes2->setValuesToInt(480, NOX_IMAGE_HEIGHT_MIN, NOX_IMAGE_HEIGHT_MAX, 1, 1);
				emes4->setValuesToInt(0, 0, 23, 1, 0.1f);		// hours
				emes5->setValuesToInt(0, 0, 59, 1, 0.1f);		// minutes
				emes6->setValuesToInt(0, 0, 59, 1, 0.1f);		// seconds
				emes7->setValuesToInt(10, NOX_REND_AUTOREFRESH_TIME_MIN, NOX_REND_AUTOREFRESH_TIME_MAX, 1, 0.1f);
				emes8->setValuesToInt(10, NOX_REND_AUTOSAVE_TIME_MIN,    NOX_REND_AUTOSAVE_TIME_MAX   , 1, 0.1f);
				emes9->setValuesToInt(2, NOX_IMAGE_AA_MIN, NOX_IMAGE_AA_MAX, 1, 1);
				emetbCaustRough->setValuesToFloat(Raytracer::getInstance()->curScenePtr->sscene.causticMaxRough,
									NOX_RAYTR_CAUSTIC_ROUGH_MIN, NOX_RAYTR_CAUSTIC_ROUGH_MAX, 1.0f);
				emes10->setValuesToInt(bdpt_max_refl, NOX_BDPT_MAX_REFL_MIN, NOX_BDPT_MAX_REFL_MAX, 1, 0.1f);
				emes11->setValuesToInt(Raytracer::getInstance()->curScenePtr->sscene.bucket_size_x, NOX_REND_BUCKET_SIZE_MIN, NOX_REND_BUCKET_SIZE_MAX, 1, 0.1f);
				emes12->setValuesToInt(Raytracer::getInstance()->curScenePtr->sscene.bucket_size_y, NOX_REND_BUCKET_SIZE_MIN, NOX_REND_BUCKET_SIZE_MAX, 1, 0.1f);
				emes13->setValuesToInt(Raytracer::getInstance()->curScenePtr->sscene.samples_per_pixel_in_pass, NOX_REND_SAMPLES_MIN, NOX_REND_SAMPLES_MAX, 1, 0.1f);

				emes14->setValuesToInt(Raytracer::getInstance()->curScenePtr->sscene.pt_gi_samples, NOX_REND_GI_SAMPLES_MIN, NOX_REND_GI_SAMPLES_MAX, 1, 0.1f);
				
				emes15->setValuesToInt(Raytracer::getInstance()->curScenePtr->sscene.pt_ris_samples, NOX_REND_RIS_SAMPLES_MIN, NOX_REND_RIS_SAMPLES_MAX, 1, 0.1f);

				emetb1->setEditBoxWidth(32);
				emetb2->setEditBoxWidth(32);
				emetb3->setEditBoxWidth(32);
				emetb4->setEditBoxWidth(32);
				emetb5->setEditBoxWidth(32);
				emetbLPmin->setEditBoxWidth(32);
				emetbLPmax->setEditBoxWidth(32);
				emetbCPmin->setEditBoxWidth(32);
				emetbCPmax->setEditBoxWidth(32);
				emetbChP1min->setEditBoxWidth(32);
				emetbChP1max->setEditBoxWidth(32);
				emetbChP2min->setEditBoxWidth(32);
				emetbChP2max->setEditBoxWidth(32);

				emetbCaustRough->setEditBoxWidth(40);

				emetb1->setValuesToInt(Metropolis::prMutLens		, 0, 1000, 50);
				emetb2->setValuesToInt(Metropolis::prMutBidir		, 0, 1000, 50);
				emetb3->setValuesToInt(Metropolis::prMutPertLens	, 0, 1000, 50);
				emetb4->setValuesToInt(Metropolis::prMutPertCaust	, 0, 1000, 50);
				emetb5->setValuesToInt(Metropolis::prMutPertChain	, 0, 1000, 50);

				emetbLPmin->setValuesToFloat(Metropolis::plR1, 0.1f, 100, 1);
				emetbLPmax->setValuesToFloat(Metropolis::plR2, 0.1f, 100, 1);
				emetbCPmin->setValuesToFloat(Metropolis::pcR1*180/PI, 0.1f, 20, 1);
				emetbCPmax->setValuesToFloat(Metropolis::pcR2*180/PI, 0.1f, 20, 1);
				emetbChP1min->setValuesToFloat(Metropolis::pch1R1, 0.1f, 30, 1);
				emetbChP1max->setValuesToFloat(Metropolis::pch1R2, 0.1f, 30, 1);
				emetbChP2min->setValuesToFloat(Metropolis::pch2R1*180/PI, 0.1f, 20, 1);
				emetbChP2max->setValuesToFloat(Metropolis::pch2R2*180/PI, 0.1f, 20, 1);

				rendererOutputArrangeControls(hWnd);
				rendererOutputShowControls(hWnd, 1, 6);

				SetTimer(hWnd, TIMER_AUTOSAVE_ID, 600000, NULL);
			}
			break;
		case WM_DESTROY:
			{
				KillTimer(hWnd, TIMER_AUTOSAVE_ID);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_OUTPUT_ASPECT:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_ASPECT));
						if (emcb->selected)
						{
							keepAspect = true;
							EMEditSpin * emes1 = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_WIDTH));
							EMEditSpin * emes2 = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_HEIGHT));
							aspectRatio = (float)emes1->intValue / (float)emes2->intValue;
						}
						else
						{
							keepAspect = false;
						}
					}
					break;
					case IDC_REND_OUTPUT_WIDTH:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						if (keepAspect)
						{
							EMEditSpin * emes1 = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_WIDTH));
							EMEditSpin * emes2 = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_HEIGHT));
							emes2->setValuesToInt((int)(emes1->intValue/aspectRatio), NOX_IMAGE_HEIGHT_MIN, NOX_IMAGE_HEIGHT_MAX, 1, 1);
						}

						Raytracer * rtr = Raytracer::getInstance();
						Camera * cam = rtr->curScenePtr->getActiveCamera();
						if (cam)
							setCameraFocalLength(18.0f/tan(cam->angle*PI/360.0f), false, false);

					}
					break;
					case IDC_REND_OUTPUT_HEIGHT:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						if (keepAspect)
						{
							EMEditSpin * emes1 = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_WIDTH));
							EMEditSpin * emes2 = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_HEIGHT));
							emes1->setValuesToInt((int)(emes2->intValue*aspectRatio), NOX_IMAGE_WIDTH_MIN, NOX_IMAGE_WIDTH_MAX, 1, 1);
						}

						Raytracer * rtr = Raytracer::getInstance();
						Camera * cam = rtr->curScenePtr->getActiveCamera();
						if (cam)
							setCameraFocalLength(18.0f/tan(cam->angle*PI/360.0f), false, false);

					}
					break;
					case IDC_REND_OUTPUT_AA:
					{
					}
					break;

					case IDC_REND_OUTPUT_STOPAFTER_HH:
					case IDC_REND_OUTPUT_STOPAFTER_MM:
					case IDC_REND_OUTPUT_STOPAFTER_SS:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_ON));
						emcb->selected = true;
						InvalidateRect(emcb->hwnd, NULL, false);
					}
					break;
					case IDC_REND_OUTPUT_STOPAFTER_ON:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_ON));
						if (emcb->selected)
							stoppingTimerOn = true;
						else
							stoppingTimerOn = false;
					}
					break;
					case IDC_REND_OUTPUT_RUN_NEXT_ON:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_RUN_NEXT_ON));
						if (emcb->selected)
							runNext.active = true;
						else
							runNext.active = false;
					}
					break;
					case IDC_REND_OUTPUT_RUN_NEXT_SETTINGS:
					{
						if (wmEvent != BN_CLICKED)
							break;

						RunNextSettings * rnc = runNext.getCopy();
						RunNextSettings * res = (RunNextSettings *)DialogBoxParam(hDllModule,
							MAKEINTRESOURCE(IDD_ANIM_SETTINGS), hWnd, AfterStopSettingsDlgProc,(LPARAM)(rnc));
						if (!res)
							break;

						runNext.copyFrom(res);
					}
					break;
					case IDC_REND_OUTPUT_REFRESH_ON:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_REFRESH_ON));
						refreshTimeOn = emcb->selected;
					}
					break;
					case IDC_REND_OUTPUT_REFRESH_SECONDS:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_REFRESH_SECONDS));
						refreshTime = emes->intValue;
					}
					break;
					case IDC_REND_OUTPUT_AUTOSAVE_MINUTES:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_AUTOSAVE_MINUTES));
						int autosave = emes->intValue;
						KillTimer(hWnd, TIMER_AUTOSAVE_ID);
						SetTimer(hWnd, TIMER_AUTOSAVE_ID, 60000*autosave, NULL);
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_AUTOSAVE_ON));
						autosaveTimerOn = emcb->selected = true;
						InvalidateRect(emcb->hwnd, NULL, false);
					}
					break;
					case IDC_REND_OUTPUT_AUTOSAVE_ON:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_AUTOSAVE_ON));
						autosaveTimerOn = emcb->selected;
					}
					break;
					case IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH:
					{
						if (wmEvent != WM_HSCROLL)
							break;
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH));
						Raytracer::getInstance()->curScenePtr->sscene.causticMaxRough = emetb->floatValue;
					}
					break;
					case IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC));
						Raytracer::getInstance()->curScenePtr->sscene.disableSecondaryCaustic = emcb->selected;
					}
					break;
					case IDC_REND_OUTPUT_FAKE_CAUSTIC:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_FAKE_CAUSTIC));
						Raytracer::getInstance()->curScenePtr->sscene.causticFake = emcb->selected;
					}
					break;
					case IDC_REND_OUTPUT_GI_SAMPLES:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_GI_SAMPLES));
						Raytracer::getInstance()->curScenePtr->sscene.pt_gi_samples = emes->intValue;
					}
					break;
					case IDC_REND_OUTPUT_RIS_SAMPLES:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_RIS_SAMPLES));
						Raytracer::getInstance()->curScenePtr->sscene.pt_ris_samples = emes->intValue;
					}
					break;
					case IDC_REND_OUTPUT_ENGINE:
					{
						if (wmEvent != CBN_SELCHANGE)
							break;
						EMComboBox * emcb  = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_ENGINE));
						switch (emcb->selected)
						{
							case 0:
								Raytracer::getInstance()->curScenePtr->sscene.giMethod = Raytracer::GI_METHOD_PATH_TRACING;
								break;
							case 1:
								Raytracer::getInstance()->curScenePtr->sscene.giMethod = Raytracer::GI_METHOD_BIDIRECTIONAL_PATH_TRACING;
								break;
							case 2:
								Raytracer::getInstance()->curScenePtr->sscene.giMethod = Raytracer::GI_METHOD_METROPOLIS;
								break;
							case 3:
								Raytracer::getInstance()->curScenePtr->sscene.giMethod = Raytracer::GI_METHOD_INTERSECTIONS;
								break;
						}
						EMComboBox * emcb2  = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_RANDOM_TYPE));

						rendererOutputShowControls(hWnd, emcb->selected, emcb2->selected);
					}
					break;
					case IDC_REND_OUTPUT_CAMERA_CONNECT:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CAMERA_CONNECT));
						bdpt_connect_to_cam = emcb->selected;
					}
					break;
					case IDC_REND_OUTPUT_EMITTER_HIT:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_EMITTER_HIT));
						bdpt_can_hit_emitter = emcb->selected;
					}
					break;
					case IDC_REND_OUTPUT_BDPT_MAX_REFL:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_BDPT_MAX_REFL));
						bdpt_max_refl = emes->intValue;
					}
					break;
					case IDC_REND_OUTPUT_RANDOM_TYPE:
					{
						if (wmEvent != CBN_SELCHANGE)
							break;
						EMComboBox * emcb  = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_RANDOM_TYPE));
						EMComboBox * emcb2  = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_ENGINE));
						Raytracer::getInstance()->curScenePtr->sscene.random_type = emcb->selected;
						rendererOutputShowControls(hWnd, emcb2->selected, emcb->selected);
					}
					break;
					case IDC_REND_OUTPUT_SHADE:
					{
						if (wmEvent != CBN_SELCHANGE)
							break;
						EMComboBox * emcb  = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_SHADE));
						ShadeTracer::shade_type = emcb->selected;
					}
					break;
					case IDC_REND_OUTPUT_MUT_LENS:
					case IDC_REND_OUTPUT_MUT_BIDIR:
					case IDC_REND_OUTPUT_MUT_PERT_LENS:
					case IDC_REND_OUTPUT_MUT_PERT_CAUSTIC:
					case IDC_REND_OUTPUT_MUT_PERT_CHAIN:
					{
						if (wmId == IDC_REND_OUTPUT_MUT_LENS)
							normalizePDFs(0);
						if (wmId == IDC_REND_OUTPUT_MUT_BIDIR)
							normalizePDFs(1);
						if (wmId == IDC_REND_OUTPUT_MUT_PERT_LENS)
							normalizePDFs(2);
						if (wmId == IDC_REND_OUTPUT_MUT_PERT_CAUSTIC)
							normalizePDFs(3);
						if (wmId == IDC_REND_OUTPUT_MUT_PERT_CHAIN)
							normalizePDFs(4);

					}
					break;
					case IDC_REND_OUTPUT_LP_MIN:
					case IDC_REND_OUTPUT_LP_MAX:
					{
						EMEditTrackBar * emetbmin = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MIN));
						EMEditTrackBar * emetbmax = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MAX));
						Metropolis::plR1 = min(emetbmin->floatValue, emetbmax->floatValue);
						Metropolis::plR2 = max(emetbmin->floatValue, emetbmax->floatValue);
					}
					break;
					case IDC_REND_OUTPUT_CP_MIN:
					case IDC_REND_OUTPUT_CP_MAX:
					{
						EMEditTrackBar * emetbmin = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MIN));
						EMEditTrackBar * emetbmax = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MAX));
						Metropolis::pcR1 = min(emetbmin->floatValue, emetbmax->floatValue)*PI/180;
						Metropolis::pcR2 = max(emetbmin->floatValue, emetbmax->floatValue)*PI/180;
					}
					break;
					case IDC_REND_OUTPUT_CHP1_MIN:
					case IDC_REND_OUTPUT_CHP1_MAX:
					{
						EMEditTrackBar * emetbmin = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MIN));
						EMEditTrackBar * emetbmax = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MAX));
						Metropolis::pch1R1 = min(emetbmin->floatValue, emetbmax->floatValue);
						Metropolis::pch1R2 = max(emetbmin->floatValue, emetbmax->floatValue);
					}
					break;
					case IDC_REND_OUTPUT_CHP2_MIN:
					case IDC_REND_OUTPUT_CHP2_MAX:
					{
						EMEditTrackBar * emetbmin = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MIN));
						EMEditTrackBar * emetbmax = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MAX));
						Metropolis::pch2R1 = min(emetbmin->floatValue, emetbmax->floatValue)*PI/180;
						Metropolis::pch2R2 = max(emetbmin->floatValue, emetbmax->floatValue)*PI/180;
					}
					break;

					case IDC_REND_OUTPUT_BUCKET_SIZE_X:
					case IDC_REND_OUTPUT_BUCKET_SIZE_Y:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMEditSpin * emesx = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_X));
						EMEditSpin * emesy = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_Y));
						Raytracer::getInstance()->curScenePtr->sscene.bucket_size_x = emesx->intValue;
						Raytracer::getInstance()->curScenePtr->sscene.bucket_size_y = emesy->intValue;
					}
					break;
					case IDC_REND_OUTPUT_SAMPLES:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_SAMPLES));
						Raytracer::getInstance()->curScenePtr->sscene.samples_per_pixel_in_pass = emes->intValue;
					}
					break;
					case IDC_REND_OUTPUT_STOP_AFTER_PASS:
					{
						if (wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOP_AFTER_PASS));
						Raytracer::getInstance()->curScenePtr->sscene.stop_after_pass = emcb->selected;
					}
					break;

				}
			}
			break;
		case WM_TIMER:
			{
				static int fcounter = 1;
				if (wParam == TIMER_STOP_ID)
				{
					KillTimer(hWnd, TIMER_STOP_ID);
					if (stoppingTimerOn  ||  stoppingPassOn)
					{
						stoppingPassOn = false;
						stopRendering();
						refreshThrRunning = false;
						refreshRenderedImage();
						if (runNext.active)
						{
							Logger::add("Rendering stopped automatically");
							if (!runNext.saveFolder)
							{
								Logger::add("Images folder not set - aborting sequence");
								MessageBox(0, "Images folder not set - sequence aborted.", "Error", 0);
								break;
							}
							if (!dirExists(runNext.saveFolder))
							{
								char buf[2048];
								sprintf_s(buf, 2048, "Images folder \"%s\" does not exist - sequence aborted.", runNext.saveFolder);
								Logger::add(buf);
								MessageBox(0, buf, "Error", 0);
								break;
							}

							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							
							int cc = sc->sscene.activeCamera+1;
							char nbuf[64], imgname[512], fcbuf[64];
							char * ebuf = NULL;
							if (runNext.fileFormat==1)
								ebuf = "jpg";
							else
								if (runNext.fileFormat==2)
									ebuf = "exr";
								else
									ebuf = "png";
							if (runNext.nextCam)
							{
								bool fok = formatStringFromUnsignedInteger(nbuf, 64, 4, cc);
								if (fok)
									sprintf_s(imgname, 512, "img_camera_%s.%s", nbuf, ebuf);
								else
									sprintf_s(imgname, 512, "img_camera_%d.%s", cc, ebuf);
							}
							else
							{
								bool fok = formatStringFromUnsignedInteger(fcbuf, 64, 5, fcounter);
								if (fok)
									sprintf_s(imgname, 512, "img_%s.%s", fcbuf, ebuf);
								else
									sprintf_s(imgname, 512, "img_%d.%s", fcounter, ebuf);
								fcounter++;
							}
							char fullfilename[2048];
							sprintf_s(fullfilename, "%s\\%s", runNext.saveFolder, imgname);
							Logger::add("Saving image to:");
							Logger::add(fullfilename);

							bool ok = saveImage(fullfilename);
							if (ok)
								Logger::add("Saved successfully");
							else
								Logger::add("Saving failed");


							if (runNext.nextCam)
							{
								if (runNext.nCamDesc)
								{
									if (sc->sscene.activeCamera<1)
										break;
								}
								else
								{
									if (sc->sscene.activeCamera>=sc->cameras.objCount-1)
										break;
								}

								Logger::add("Releasing camera buffers...");
								Camera * cam_old = sc->getActiveCamera();
								if (!cam_old)
									break;
								cam_old->releaseAllBuffers();
								EMPView * empv = GetEMPViewInstance(hImage);
								if (empv)
									empv->otherSource = NULL;

								Logger::add("Changing camera...");
								if (runNext.nCamDesc)
									sc->sscene.activeCamera--;
								else
									sc->sscene.activeCamera++;
								if (sc->sscene.activeCamera < 0   ||   sc->sscene.activeCamera >= sc->cameras.objCount)
									break;
								Camera * cam_new = sc->getActiveCamera();
								if (!cam_new)
									break;

								if (runNext.copyPost)
								{
									Logger::add("Copying settings from old camera...");
									cam_new->fMod = cam_old->fMod;
									cam_new->iMod.copyDataFromAnother(&(cam_old->iMod));

									cam_new->width = cam_old->width;
									cam_new->height = cam_old->height;
									cam_new->aa = cam_old->aa;
								}

								Logger::add("Updating gui stuff...");
								changeCamera(cam_new);

							}

							if (runNext.changeSunsky  &&   runNext.sunskyMins!=0)
							{
								Logger::add("Changing sunsky...");
								HWND hEnv = GetDlgItem(hEnvironment, IDC_REND_ENV_MAP);
								EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
								int month = emen->sunsky.getMonth();
								int day = emen->sunsky.getDay();
								int hour = emen->sunsky.getHour();
								float minute = emen->sunsky.getMinute();

								minute += runNext.sunskyMins;
								while (minute>59.0f)
								{
									minute-=60.0f;
									hour++;
								}
								while (minute<0.0f)
								{
									minute+=60.0f;
									hour--;
								}
								while (hour>23)
								{
									hour -= 24;
									day++;
								}
								while (hour<0)
								{
									hour += 24;
									day--;
								}
								int maxday = 31;
								if (month==4 || month==6 || month==9 || month==11)
									maxday=30;
								if (month==2)
									maxday=28;
								if (day>maxday)
								{
									day=1;
									month++;
									if (month>12)
										month = 1;
								}
								if (day<1)
								{
									month--;
									if (month<1)
										month = 12;
									switch (month)
									{
										case 1:
										case 3:
										case 5:
										case 7:
										case 8:
										case 10:
										case 12:
											day = 31;
											break;
										case 4:
										case 6:
										case 9:
										case 11:
											day = 30;
											break;
										case 2:
											day = 28;
											break;
									}
								}

								emen->sunsky.setDate(month, day, hour, minute);
								emen->evalSunPosition();

								fillSunSkyControls();
							}



							Logger::add("Start rendering");

							updateEngineSettingsBeforeRender();
							startStoppingTimer();
							startRendering();
							runRefreshThread();
							
						}
					}
				}
				if (wParam == TIMER_AUTOSAVE_ID)
				{
					if (autosaveTimerOn)
					{
						Raytracer * rtr = Raytracer::getInstance();
						if (!rtr  ||  !rtr->curScenePtr)
							break;
						char * fname = rtr->curScenePtr->sceneFilename;
						if (fname  &&  rtr->curScenePtr->nowRendering)
						{
							rtr->curScenePtr->notifyProgress("Autosaving", 0);
							saveScene(fname, false, true);
							rtr->curScenePtr->notifyProgress("Rendering", 0);
						}
					}
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

void normalizePDFs(int constant)
{
	EMEditTrackBar * emetb1 = GetEMEditTrackBarInstance(GetDlgItem(RendererMainWindow::hOutputConf, IDC_REND_OUTPUT_MUT_LENS));
	EMEditTrackBar * emetb2 = GetEMEditTrackBarInstance(GetDlgItem(RendererMainWindow::hOutputConf, IDC_REND_OUTPUT_MUT_BIDIR));
	EMEditTrackBar * emetb3 = GetEMEditTrackBarInstance(GetDlgItem(RendererMainWindow::hOutputConf, IDC_REND_OUTPUT_MUT_PERT_LENS));
	EMEditTrackBar * emetb4 = GetEMEditTrackBarInstance(GetDlgItem(RendererMainWindow::hOutputConf, IDC_REND_OUTPUT_MUT_PERT_CAUSTIC));
	EMEditTrackBar * emetb5 = GetEMEditTrackBarInstance(GetDlgItem(RendererMainWindow::hOutputConf, IDC_REND_OUTPUT_MUT_PERT_CHAIN));

	static float pdfMutLens			= (float)Metropolis::prMutLens;
	static float pdfMutBidir		= (float)Metropolis::prMutBidir;
	static float pdfMutPertLens		= (float)Metropolis::prMutPertLens;
	static float pdfMutPertCaustic	= (float)Metropolis::prMutPertCaust;
	static float pdfMutPertChain	= (float)Metropolis::prMutPertChain;

	float r,s;
	switch (constant)
	{
		case 0:
			pdfMutLens = (float)emetb1->intValue;
			r = 1000 - pdfMutLens;
			s = pdfMutBidir + pdfMutPertLens + pdfMutPertCaustic + pdfMutPertChain;
			if (s == 0)
				pdfMutBidir = pdfMutPertLens = pdfMutPertCaustic = pdfMutPertChain = r/4;
			else
			{
				pdfMutBidir       *= r/s;
				pdfMutPertLens    *= r/s;
				pdfMutPertCaustic *= r/s;
				pdfMutPertChain   *= r/s;
			}
			emetb2->setValuesToInt(ROUND(pdfMutBidir)		, 0, 1000, 50);
			emetb3->setValuesToInt(ROUND(pdfMutPertLens)	, 0, 1000, 50);
			emetb4->setValuesToInt(ROUND(pdfMutPertCaustic)	, 0, 1000, 50);
			emetb5->setValuesToInt(1000-emetb1->intValue-emetb2->intValue-emetb3->intValue-emetb4->intValue, 0, 1000, 50);
			break;
		case 1:
			pdfMutBidir = (float)emetb2->intValue;
			r = 1000 - pdfMutBidir;
			s = pdfMutLens + pdfMutPertLens + pdfMutPertCaustic + pdfMutPertChain;
			if (s == 0)
				pdfMutLens = pdfMutPertLens = pdfMutPertCaustic = pdfMutPertChain = r/4;
			else
			{
				pdfMutLens        *= r/s;
				pdfMutPertLens    *= r/s;
				pdfMutPertCaustic *= r/s;
				pdfMutPertChain   *= r/s;
			}
			emetb1->setValuesToInt(ROUND(pdfMutLens)		, 0, 1000, 50);
			emetb3->setValuesToInt(ROUND(pdfMutPertLens)	, 0, 1000, 50);
			emetb4->setValuesToInt(ROUND(pdfMutPertCaustic)	, 0, 1000, 50);
			emetb5->setValuesToInt(1000-emetb1->intValue-emetb2->intValue-emetb3->intValue-emetb4->intValue, 0, 1000, 50);
			break;
		case 2:
			pdfMutPertLens = (float)emetb3->intValue;
			r = 1000 - pdfMutPertLens;
			s = pdfMutLens + pdfMutBidir + pdfMutPertCaustic + pdfMutPertChain;
			if (s == 0)
				pdfMutLens = pdfMutBidir = pdfMutPertCaustic = pdfMutPertChain = r/4;
			else
			{
				pdfMutLens        *= r/s;
				pdfMutBidir       *= r/s;
				pdfMutPertCaustic *= r/s;
				pdfMutPertChain   *= r/s;
			}
			emetb1->setValuesToInt(ROUND(pdfMutLens)		, 0, 1000, 50);
			emetb2->setValuesToInt(ROUND(pdfMutBidir)		, 0, 1000, 50);
			emetb4->setValuesToInt(ROUND(pdfMutPertCaustic)	, 0, 1000, 50);
			emetb5->setValuesToInt(1000-emetb1->intValue-emetb2->intValue-emetb3->intValue-emetb4->intValue, 0, 1000, 50);
			break;
		case 3:
			pdfMutPertCaustic = (float)emetb4->intValue;
			r = 1000 - pdfMutPertCaustic;
			s = pdfMutLens + pdfMutBidir + pdfMutPertLens + pdfMutPertChain;
			if (s == 0)
				pdfMutLens = pdfMutBidir = pdfMutPertLens = pdfMutPertChain = r/4;
			else
			{
				pdfMutLens     *= r/s;
				pdfMutBidir    *= r/s;
				pdfMutPertLens *= r/s;
				pdfMutPertChain   *= r/s;
			}
			emetb1->setValuesToInt(ROUND(pdfMutLens)		, 0, 1000, 50);
			emetb2->setValuesToInt(ROUND(pdfMutBidir)		, 0, 1000, 50);
			emetb3->setValuesToInt(ROUND(pdfMutPertLens)	, 0, 1000, 50);
			emetb5->setValuesToInt(1000-emetb1->intValue-emetb2->intValue-emetb3->intValue-emetb4->intValue, 0, 1000, 50);
			break;
		case 4:
			pdfMutPertChain = (float)emetb5->intValue;
			r = 1000 - pdfMutPertChain;
			s = pdfMutLens + pdfMutBidir + pdfMutPertLens + pdfMutPertCaustic;
			if (s == 0)
				pdfMutLens = pdfMutBidir = pdfMutPertLens = pdfMutPertCaustic = r/4;
			else
			{
				pdfMutLens			*= r/s;
				pdfMutBidir			*= r/s;
				pdfMutPertLens		*= r/s;
				pdfMutPertCaustic	*= r/s;
			}
			emetb1->setValuesToInt(ROUND(pdfMutLens)		, 0, 1000, 50);
			emetb2->setValuesToInt(ROUND(pdfMutBidir)		, 0, 1000, 50);
			emetb3->setValuesToInt(ROUND(pdfMutPertLens)	, 0, 1000, 50);
			emetb4->setValuesToInt(1000-emetb1->intValue-emetb2->intValue-emetb3->intValue-emetb5->intValue, 0, 1000, 50);
			break;
	}
	int i1 = emetb1->intValue;
	int i2 = emetb2->intValue;
	int i3 = emetb3->intValue;
	int i4 = emetb4->intValue;
	int i5 = emetb5->intValue;
	Metropolis::prMutLens		= i1;
	Metropolis::prMutBidir		= i2;
	Metropolis::prMutPertLens	= i3;
	Metropolis::prMutPertCaust	= i4;
	Metropolis::prMutPertChain	= i5;
}

void rendererOutputArrangeControls(HWND hWnd)
{

	if (!hWnd)
		return;

	RECT crect;
	GetClientRect(hWnd, &crect);
	int hh = crect.bottom;

	// ---------- GRUPA RESOLUTION
	int gr1x = 7;
	int gr1y = 7;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GROUP),			HWND_TOP,    gr1x+0,   gr1y+0,     175, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_WIDTH),			HWND_TOP,    gr1x+47,  gr1y+19,     54, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_HEIGHT),			HWND_TOP,    gr1x+47,  gr1y+38,     54, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_AA),				HWND_TOP,    gr1x+47,  gr1y+57,     54, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_ASPECT),			HWND_TOP,    gr1x+105, gr1y+30,     15, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_WIDTH),		HWND_TOP,    gr1x+5,   gr1y+20,     40, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_HEIGHT),		HWND_TOP,    gr1x+5,   gr1y+39,     40, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_AA),			HWND_TOP,    gr1x+5,   gr1y+58,     40, 18,   SWP_NOZORDER);

	// ---------- GRUPA TIMERS
	int gr2x = 7;
	int gr2y = 90;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_TIMERS),			HWND_TOP,    gr2x+0,    gr2y+0,     175, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_ON),		HWND_TOP,    gr2x+60,   gr2y+21,     15, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_REFRESH_ON),			HWND_TOP,    gr2x+60,   gr2y+61,     15, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_AUTOSAVE_ON),			HWND_TOP,    gr2x+60,   gr2y+81,     15, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_HH),		HWND_TOP,    gr2x+78,   gr2y+20,     30, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_MM),		HWND_TOP,    gr2x+108,  gr2y+20,     30, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOPAFTER_SS),		HWND_TOP,    gr2x+138,  gr2y+20,     30, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_REFRESH_SECONDS),		HWND_TOP,    gr2x+78,   gr2y+60,     60, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_AUTOSAVE_MINUTES),	HWND_TOP,    gr2x+78,   gr2y+80,     60, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_RUN_NEXT_ON),			HWND_TOP,    gr2x+60,   gr2y+41,     15, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_RUN_NEXT_SETTINGS),	HWND_TOP,    gr2x+78,   gr2y+41,     90, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_STOP_AFTER),		HWND_TOP,    gr2x+5,    gr2y+21,     52, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_RUN_NEXT),		HWND_TOP,    gr2x+5,    gr2y+41,     52, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_REFRESH),		HWND_TOP,    gr2x+5,    gr2y+61,     52, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_AUTOSAVE),		HWND_TOP,    gr2x+5,    gr2y+81,     52, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_SECONDS),		HWND_TOP,    gr2x+142,  gr2y+61,    124, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_MINUTES),		HWND_TOP,    gr2x+142,  gr2y+81,    124, 18,   SWP_NOZORDER);


	// ---------- GRUPA SETTINGS
	int gr3x = 187;
	int gr3y = 7;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_SETT),			HWND_TOP,    gr3x+0,    gr3y+0,     179, 15,			SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_ENGINE),			HWND_TOP,    gr3x+59,   gr3y+19,    115, hh-(gr3y+19),  SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_RANDOM_TYPE),		HWND_TOP,    gr3x+59,   gr3y+44,    115, hh-(gr3y+44),  SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_ENGINE),		HWND_TOP,    gr3x+5,    gr3y+22,     50, 18,			SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_FILL),		HWND_TOP,    gr3x+5,    gr3y+47,     50, 18,			SWP_NOZORDER);


	// ---------- GRUPA BUCKETS
	int gr8x = 187;
	int gr8y = 90;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_BUCKETS),		HWND_TOP,    gr8x+0,    gr8y+0,      179, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_X),	HWND_TOP,    gr8x+90,   gr8y+20,      40,  18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_Y),	HWND_TOP,    gr8x+135,  gr8y+20,      40,  18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_SAMPLES),			HWND_TOP,    gr8x+90,   gr8y+40,      40,  18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOP_AFTER_PASS),	HWND_TOP,    gr8x+90,   gr8y+60,      15,  15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_BUCKET_SIZE),HWND_TOP,    gr8x+5,    gr8y+21,      80,  18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_SAMPLES),	HWND_TOP,    gr8x+5,    gr8y+41,      80,  18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_STOP_AFTER_PASS),HWND_TOP,gr8x+5,    gr8y+61,      80,  18,   SWP_NOZORDER);


	// ---------- GRUPA PATH TRACING
	int gr4x = 371;
	int gr4y = 7;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_SETT2),					HWND_TOP,    gr4x+0,    gr4y+0,     311, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GI_SAMPLES),					HWND_TOP,    gr4x+113,  gr4y+19,    40, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_RIS_SAMPLES),					HWND_TOP,    gr4x+113,  gr4y+39,    40, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH),			HWND_TOP,    gr4x+113,  gr4y+59,    193, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_FAKE_CAUSTIC),				HWND_TOP,    gr4x+113,  gr4y+79,    15,  15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC),	HWND_TOP,    gr4x+113,  gr4y+99,    15,  15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_GI_SAMPLES),				HWND_TOP,    gr4x+5,    gr4y+20,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_RIS_SAMPLES),			HWND_TOP,    gr4x+5,    gr4y+40,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_MAX_CAUSTIC_ROUGH),		HWND_TOP,    gr4x+5,    gr4y+60,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_FAKE_CAUSTIC),			HWND_TOP,    gr4x+5,    gr4y+80,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_DISABLE_2ND_CAUSTIC),	HWND_TOP,    gr4x+5,    gr4y+100,   105, 18,   SWP_NOZORDER);

  
	// ---------- GRUPA BIDIRECTIONAL PATH TRACING
	int gr7x = 371;
	int gr7y = 7;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_BDPT),						HWND_TOP,    gr7x+0,    gr7y+0,     160, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_EMITTER_HIT),			HWND_TOP,    gr7x+5,    gr7y+20,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_CAMERA_CONNECT),			HWND_TOP,    gr7x+5,    gr7y+40,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_MAX_REFLECTIONS),		HWND_TOP,    gr7x+5,    gr7y+60,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_EMITTER_HIT),					HWND_TOP,    gr7x+113,  gr7y+19,    15, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CAMERA_CONNECT),				HWND_TOP,    gr7x+113,  gr7y+39,    15, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_BDPT_MAX_REFL),				HWND_TOP,    gr7x+113,  gr7y+59,    40, 18,   SWP_NOZORDER);


	// ---------- GRUPA METROPOLIS
	int gr5x = 371;
	int gr5y = 7;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_METROPOLIS),	HWND_TOP,    gr5x+0,    gr5y+0,      625, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT01),				HWND_TOP,    gr5x+5,    gr5y+20,     105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT02),				HWND_TOP,    gr5x+5,    gr5y+40,     105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT03),				HWND_TOP,    gr5x+5,    gr5y+60,     105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT04),				HWND_TOP,    gr5x+5,    gr5y+80,     105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT05),				HWND_TOP,    gr5x+5,    gr5y+100,    105, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT06),				HWND_TOP,    gr5x+311,  gr5y+20,     115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT07),				HWND_TOP,    gr5x+311,  gr5y+40,     115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT08),				HWND_TOP,    gr5x+311,  gr5y+60,     115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT09),				HWND_TOP,    gr5x+311,  gr5y+80,     115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT10),				HWND_TOP,    gr5x+311,  gr5y+100,    115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT11),				HWND_TOP,    gr5x+311,  gr5y+120,    115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT12),				HWND_TOP,    gr5x+311,  gr5y+140,    115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT13),				HWND_TOP,    gr5x+311,  gr5y+160,    115, 18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_LENS),		HWND_TOP,    gr5x+114,  gr5y+19,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_BIDIR),		HWND_TOP,    gr5x+114,  gr5y+39,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_LENS),	HWND_TOP,    gr5x+114,  gr5y+59,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_CAUSTIC),HWND_TOP,    gr5x+114,  gr5y+79,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_CHAIN),	HWND_TOP,    gr5x+114,  gr5y+99,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MIN),			HWND_TOP,    gr5x+430,  gr5y+19,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MAX),			HWND_TOP,    gr5x+430,  gr5y+39,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MIN),			HWND_TOP,    gr5x+430,  gr5y+59,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MAX),			HWND_TOP,    gr5x+430,  gr5y+79,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MIN),		HWND_TOP,    gr5x+430,  gr5y+99,     190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MAX),		HWND_TOP,    gr5x+430,  gr5y+119,    190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MIN),		HWND_TOP,    gr5x+430,  gr5y+139,    190, 20,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MAX),		HWND_TOP,    gr5x+430,  gr5y+159,    190, 20,   SWP_NOZORDER);

	// ---------- GRUPA INTERSECTIONS
	int gr6x = 371;
	int gr6y = 7;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_INTERSECTIONS),HWND_TOP,    gr6x+0,    gr6y+0,      200, 15,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_SHADE),		HWND_TOP,    gr6x+5,    gr6y+20,     70,  18,   SWP_NOZORDER);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND_OUTPUT_SHADE),			HWND_TOP,    gr6x+75,   gr6y+20,     120, 110,   SWP_NOZORDER);

}

void rendererOutputShowControls(HWND hWnd, int engine, int sampling)
{
	if (!hWnd)
		return;

	int show_PT =   (engine==0) ? SW_SHOW : SW_HIDE;
	int show_BDPT = (engine==1) ? SW_SHOW : SW_HIDE;
	int show_MLT =  (engine==2) ? SW_SHOW : SW_HIDE;
	int show_INT =  (engine==3) ? SW_SHOW : SW_HIDE;

	int show_S_buckets =  (sampling==6) ? SW_SHOW : SW_HIDE;

	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_SETT2),						show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_GI_SAMPLES),					show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_RIS_SAMPLES),					show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH),				show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_FAKE_CAUSTIC),					show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC),		show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_MAX_CAUSTIC_ROUGH),		show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_FAKE_CAUSTIC),				show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_DISABLE_2ND_CAUSTIC),		show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_GI_SAMPLES),				show_PT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_RIS_SAMPLES),				show_PT);


	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_BDPT),						show_BDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_CAMERA_CONNECT),			show_BDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_EMITTER_HIT),				show_BDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_MAX_REFLECTIONS),			show_BDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CAMERA_CONNECT),				show_BDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_EMITTER_HIT),					show_BDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_BDPT_MAX_REFL),					show_BDPT);
	

	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT01), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT02), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT03), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT04), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT05), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT06), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT07), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT08), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT09), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT10), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT11), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT12), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT13), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_METROPOLIS), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_LENS), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_BIDIR), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_LENS), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_CAUSTIC), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_MUT_PERT_CHAIN), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MIN), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_LP_MAX), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MIN), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CP_MAX), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MIN), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP1_MAX), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MIN), show_MLT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_CHP2_MAX), show_MLT);

	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_INTERSECTIONS), show_INT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_SHADE), show_INT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_SHADE), show_INT);

	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_GR_BUCKETS),			show_S_buckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_X),			show_S_buckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_BUCKET_SIZE_Y),			show_S_buckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_SAMPLES),				show_S_buckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_STOP_AFTER_PASS),		show_S_buckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_BUCKET_SIZE),		show_S_buckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_SAMPLES),			show_S_buckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND_OUTPUT_TEXT_STOP_AFTER_PASS),	show_S_buckets);
}

