#include "max2013predef.h"
#include "Max.h"
#include "bmmlib.h"
#include "meshadj.h"
#include "modstack.h"
#include "stdmat.h"
#include "templt.h"
#include "render.h"


#include "maxNormals.h"


int MNormal::global_number = 0;
int nSmGr[32];

MNormal::MNormal() 
{
	next = 0; 
	empty = true; 
	smgrp = 0; 
	number = 0; 
	normal = Point3(0,0,0);
}

MNormal::~MNormal() 
{
	if(next) 
	{
		delete next;
	}
}

void MNormal::AddNormal(Point3 n, DWORD s)
{

	if (smgrp & s  || empty)
	{
		normal += n;
		smgrp |= s;
		empty = false;
	}
	else
	{
		if (!next)
			next = new MNormal();
		next->AddNormal(n, s);
	}
}

void MNormal::DisposeAll()
{
	if (next)
		delete next;
	next = 0;
}

Point3 MNormal::GetNormal(DWORD s)
{
	if (smgrp & s) 
		return normal;
	if (!next)
		return normal;
	return next->GetNormal(s);
}

int MNormal::GetNormalNumber(DWORD s)
{
	if (smgrp&s || !next)
		return number;
	else
		return next->GetNormalNumber(s);
	return 0;
}
	
void MNormal::AssignNumber()
{
	number = global_number;
	global_number++;
	if (next)
		next->AssignNumber();
}

int MNormal::CountNormals()
{
	if (!next)
		return 1;
	return next->CountNormals()+1;
}

void MNormal::JoinNormals()
{
	MNormal * ptr = next;
	MNormal * prev = this;
	while (ptr)
	{
		if (smgrp & ptr->smgrp)
		{
			normal += ptr->normal;
			prev->next = ptr->next;
			ptr->next = 0;
			delete ptr;
			ptr = prev->next;
		}
		else
		{
			prev = ptr;
			ptr = ptr->next;
		}
	}
	if(next)
		next->JoinNormals();
}

void MNormal::Normalize()
{
	normal = normal.Normalize();
	if (next) 
		next->Normalize();
}

MNormal * ComputeMyNormals(Mesh *mesh, Point3 **list, Matrix3 m1)
{	
	for (int i=0; i<32; i++)
		nSmGr[i] = 0;

	int i,j;
	Point3 normalna;
	Point3 v0, v1, v2;
	Face * face;
	MNormal::global_number = 0;
	MNormal * normalne;
	normalne = (MNormal *)malloc(sizeof(MNormal)*mesh->getNumVerts());
	if (!normalne)
		return 0;
	for (i=0; i<mesh->getNumVerts(); i++)
	{
		normalne[i] = MNormal();
	}
	for (i=0; i<mesh->getNumFaces(); i++)
	{
		face = &(mesh->faces[i]);
		if (face->Direction(face->getVert(0), face->getVert(1)) < 0)
		{
			v2 = mesh->verts[face->getVert(0)]*m1;
			v1 = mesh->verts[face->getVert(1)]*m1;
			v0 = mesh->verts[face->getVert(2)]*m1;
		}
		else
		{
			v0 = mesh->verts[face->getVert(0)]*m1;
			v1 = mesh->verts[face->getVert(1)]*m1;
			v2 = mesh->verts[face->getVert(2)]*m1;
		}
		
		normalna = (v1-v0)^(v2-v1);
		if (normalna.Length() == 0)
		{
			continue;
		}

		normalna = normalna.Normalize();
		for (j=0; j<3; j++)
		{	
			normalne[face->getVert(j)].AddNormal(normalna, face->smGroup);
		}
		if (face->smGroup>=0  &&  face->smGroup<32)
		{
			nSmGr[face->smGroup]++;
		}
	}

	for (i=0; i<mesh->getNumVerts(); i++)
	{
		normalne[i].Normalize();
		normalne[i].JoinNormals();
		normalne[i].AssignNumber();
		normalne[i].Normalize();
	}
	if (list)
	{
		MNormal *pnorm;
		*list = new Point3[MNormal::global_number];
		if (!(*list))
		{
			for (i=0; i<mesh->getNumVerts(); i++)
				normalne[i].DisposeAll();
			free(normalne);
			return 0;
		}
		for (i=0; i<mesh->getNumVerts(); i++)
		{
			pnorm = &normalne[i];
			while (pnorm)
			{
				(*list)[pnorm->number].x = pnorm->normal.x;
				(*list)[pnorm->number].y = pnorm->normal.y;
				(*list)[pnorm->number].z = pnorm->normal.z;
				pnorm = pnorm->next;
			}
		}
	}
	return normalne;
}
