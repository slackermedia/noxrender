#include <windows.h>
#include "raytracer.h"
#include "log.h"

//------------------------------------------------------------------------------------------------------------------------------------

TextureInstance::TextureInstance() 
{ 
	filename = NULL; 
	fullfilename = NULL; 
	managerID = -1;
	texScPtr = NULL;
	exportedArrayID = -1;
	exportedFilename = NULL;
}

//------------------------------------------------------------------------------------------------------------------------------------

TextureInstance::~TextureInstance()
{
	if (filename)
		free(filename);
	if (fullfilename)
		free(fullfilename);
	filename = NULL;
	fullfilename = NULL;
	texScPtr = NULL;
	exportedArrayID = -1;
	if (exportedFilename)
		free(exportedFilename);
	exportedFilename = NULL;
}

//------------------------------------------------------------------------------------------------------------------------------------

TextureInstance::TextureInstance(const TextureInstance & texinst)
{
	filename = copyString(texinst.filename);
	fullfilename = copyString(texinst.fullfilename);
	managerID = texinst.managerID;
	texMod = texinst.texMod;
	normMod = texinst.normMod;
	texScPtr = texinst.texScPtr;
	exportedArrayID = texinst.exportedArrayID;
	exportedFilename = copyString(texinst.exportedFilename);
}

//------------------------------------------------------------------------------------------------------------------------------------

TextureInstance & TextureInstance::operator= (const TextureInstance & texInst)
{
	 if (this == &texInst) 
		 return *this;

	filename = copyString(texInst.filename);
	fullfilename = copyString(texInst.fullfilename);
	managerID = texInst.managerID;
	texMod = texInst.texMod;
	normMod = texInst.normMod;
	texScPtr = texInst.texScPtr;
	exportedArrayID = texInst.exportedArrayID;
	exportedFilename = copyString(texInst.exportedFilename);
	
	return *this;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool TextureInstance::isValid()
{
	if (managerID<0)
		return false;
	if (Raytracer::getInstance()->curScenePtr->texManager.textures.objCount<=managerID)
		return false;
	Texture * tex = Raytracer::getInstance()->curScenePtr->texManager.textures[managerID];
	if (!tex)
		return false;
	return tex->isValid();
}

//------------------------------------------------------------------------------------------------------------------------------------

Vector3d TextureInstance::getNormal(float x, float y)
{
	if (managerID<0)
		return Vector3d(0,0,1);
	Texture * tex = Raytracer::getInstance()->curScenePtr->texManager.textures[managerID];
	if (!tex)
		return Vector3d(0,0,1);

	Vector3d res(0,0,1);
	tex->getNormalVector(x, y, res.V, &normMod);
	return res;
}

//------------------------------------------------------------------------------------------------------------------------------------

Color4 TextureInstance::getColor(float x, float y)
{
	if (managerID<0)
		return Color4(0,0,0);
	Texture * tex = Raytracer::getInstance()->curScenePtr->texManager.textures[managerID];
	if (!tex)
		return Color4(0,0,0);
	return tex->getColor(x, y, &texMod);
}

//------------------------------------------------------------------------------------------------------------------------------------

void TextureInstance::setEmptyTex()
{
	if (fullfilename)
		free(fullfilename);
	if (filename)
		free(filename);
	fullfilename = NULL;
	filename = NULL;
	managerID = -1;
	texScPtr = NULL;

	exportedArrayID = -1;
	if (exportedFilename)
		free(exportedFilename);
	exportedFilename = NULL;

}

//------------------------------------------------------------------------------------------------------------------------------------

bool TextureInstance::updateSceneTexturePointer(Scene * sc, bool normal_slot)
{
	if (!sc)
		return false;

	int id = sc->texManager.findID(fullfilename, normal_slot);
	if (id<0)
	{
		texScPtr = NULL;
		return true;
	}

	texScPtr = sc->texManager.textures[id];
	return true;
}

//====================================================================================================================================

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

//------------------------------------------------------------------------------------------------------------------------------------

int TextureManager::addTexture(char * fullfilename, bool tryOtherFolders, Texture * src, bool asNormal)
{
	if (!fullfilename)
		return -1;

	int curID = findID(fullfilename, asNormal);
	if (curID>=0)
		return curID;

	Texture *ntex = new Texture;

	bool cpok = false;
	if (src  &&  src->isValid())
		cpok = ntex->copyFromOther(src);

	if (!cpok)
	{
		bool ok;
		if (tryOtherFolders)
			ok = ntex->loadFromFileTryOtherFolders(fullfilename);
		else
			ok = ntex->loadFromFile(fullfilename);
		if (!ok)
		{
			ntex->freeAllBuffers();
			delete ntex;
			return -1;
		}
		if (asNormal)
		{
			if (!ntex->isItNormalMap())
			{
				ntex->convertMeToNormalMap();
				ntex->is_it_converted_normal = true;
			}
		}
	}

	textures.add(ntex);
	textures.createArray();
	
	return textures.objCount-1;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool TextureManager::updateTextures(Scene * sc)
{
	if (!sc)
		return false;
	char buf[2048];

	// delete not used textures
	for (int i=0; i<textures.objCount; i++)
	{
		if (!textures[i])
		{
			sprintf_s(buf, 2048, "Removing NULL texture (id=%d) from tex manager.", i);
			Logger::add(buf);
			textures.removeElement(i);
			textures.createArray();
			i--;
			continue;
		}

		char * ffname = textures[i]->fullfilename;
		if (!ffname)
		{
			sprintf_s(buf, 2048, "Removing non-named texture (id=%d) from tex manager.", i);
			Logger::add(buf);
			textures[i]->freeAllBuffers();
			delete textures[i];

			textures.removeElement(i);
			textures.createArray();
			i--;
			continue;
		}

		bool is_used = checkForUsed(sc,  ffname);
		if (!is_used)
		{
			sprintf_s(buf, 2048, "Removing not used texture (id=%d) from tex manager: %s", i, ffname);
			Logger::add(buf);
			textures[i]->freeAllBuffers();
			textures.removeElement(i);
			textures.createArray();
			i--;
			continue;
		}
	}

	// assign ids to tex instances
	for (int i=0; i<sc->mats.objCount; i++)
	{
		MaterialNox * mat = sc->mats[i];
		if (!mat)
			continue;

		for (int j=0; j<mat->layers.objCount; j++)
		{
			MatLayer * mlay = &(mat->layers[j]);

			updateInstance(&(mlay->tex_weight), false);
			updateInstance(&(mlay->tex_col0),   false);
			updateInstance(&(mlay->tex_col90),  false);
			updateInstance(&(mlay->tex_light),  false);
			updateInstance(&(mlay->tex_normal), true);
			updateInstance(&(mlay->tex_transm), false);
			updateInstance(&(mlay->tex_rough),  false);
			updateInstance(&(mlay->tex_aniso),  false);
			updateInstance(&(mlay->tex_aniso_angle),  false);
		}
		updateInstance(&(mat->tex_opacity), false);
		updateInstance(&(mat->tex_displacement), false);
	}

	// for env
	updateInstance(&(sc->env.tex), false);

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool TextureManager::checkForUsed(Scene * sc, char * fullfilename)
{
	for (int i=0; i<sc->mats.objCount; i++)
	{
		MaterialNox * mat = sc->mats[i];
		if (!mat)
			continue;

		for (int j=0; j<mat->layers.objCount; j++)
		{
			MatLayer * mlay = &(mat->layers[j]);
		
			if (mlay->tex_col0.fullfilename)
				if (strcmp(mlay->tex_col0.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_col90.fullfilename)
				if (strcmp(mlay->tex_col90.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_normal.fullfilename)
				if (strcmp(mlay->tex_normal.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_transm.fullfilename)
				if (strcmp(mlay->tex_transm.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_weight.fullfilename)
				if (strcmp(mlay->tex_weight.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_light.fullfilename)
				if (strcmp(mlay->tex_light.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_rough.fullfilename)
				if (strcmp(mlay->tex_rough.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_aniso.fullfilename)
				if (strcmp(mlay->tex_aniso.fullfilename, fullfilename)==0)
					return true;

			if (mlay->tex_aniso_angle.fullfilename)
				if (strcmp(mlay->tex_aniso_angle.fullfilename, fullfilename)==0)
					return true;
		}

		if (mat->tex_opacity.fullfilename)
			if (strcmp(mat->tex_opacity.fullfilename, fullfilename)==0)
				return true;
		if (mat->tex_displacement.fullfilename)
			if (strcmp(mat->tex_displacement.fullfilename, fullfilename)==0)
				return true;
	}

	if (sc->env.tex.fullfilename)
		if (strcmp(sc->env.tex.fullfilename, fullfilename)==0)
			return true;

	return false;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool TextureManager::updateInstance(TextureInstance * texInst, bool normal_slot)
{
	if (!texInst)
		return false;
	if (!texInst->fullfilename)
	{
		texInst->managerID = -1;
		return true;
	}

	texInst->managerID = findID(texInst->fullfilename, normal_slot);
	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

int TextureManager::findID(char * fullfilename, bool normal_slot)
{
	if (!fullfilename)
		return -1;
	for (int i=0; i<textures.objCount; i++)
	{
		if (!textures[i])
			continue;
		if (!textures[i]->fullfilename)
			continue;
		if (strcmp(fullfilename, textures[i]->fullfilename))
			continue;

		if (normal_slot)
		{
			if (!textures[i]->is_it_converted_normal)
				if (!textures[i]->isItNormalMap())
					continue;
		}
		else
		{
			if (textures[i]->is_it_converted_normal)
				continue;
		}

		return i;
	}
	return -1;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool TextureManager::logTextures()
{
	char buf[2048];
	Logger::add("Texture manager log:");
	sprintf_s(buf, 2048, "Number of textures: %d", textures.objCount);
	Logger::add(buf);
	for (int i=0; i<textures.objCount; i++)
	{
		sprintf_s(buf, 2048, "Texture %d", (i+1));
		Logger::add(buf);
		if (textures[i]==NULL)
		{
			Logger::add("NULL Texture - bug!");
			continue;
		}
		sprintf_s(buf, 2048, "Texture name: %s", textures[i]->filename);
		Logger::add(buf);
		sprintf_s(buf, 2048, "Texture full name: %s", textures[i]->fullfilename);
		Logger::add(buf);
		sprintf_s(buf, 2048, "Res: %d x %d", textures[i]->bmp.getWidth(), textures[i]->bmp.getHeight());
		Logger::add(buf);
	}
	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool TextureManager::checkReloadModifiedTextures(HWND hWnd, bool forceReload)
{
	Raytracer * rtr = Raytracer::getInstance();
	bool all = forceReload;
	bool reload = true;

	int ntex = textures.objCount;
	for (int i=0; i<ntex; i++)
	{
		textures[i]->updateModifiedTexFile(hWnd, reload, all);
	}

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------
