#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "raytracer.h"

void setPositionsOnRegions(HWND hWnd);
bool updateRegionButtonsState(HWND hWnd, EMPView * empv);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcRegions(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnRegions(hWnd);

				EM2ImgButton * emHand	= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_HAND));
				EM2ImgButton * emRect	= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_RECT));
				EM2ImgButton * emBrush	= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_BRUSH));
				EM2ImgButton * emSelAll	= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_SEL_ALL));
				EM2ImgButton * emSelNone= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_SEL_NONE));
				EM2ImgButton * emSelInv	= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_SEL_INVERT));
				EM2ImgButton * emZoomIn	= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_IN));
				EM2ImgButton * emZoomOut= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_OUT));
				EM2ImgButton * emZoom100= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_100));
				EM2ImgButton * emZoomFit= GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_FIT));

				emHand->setToolTip("Hand tool");
				emRect->setToolTip("Select rectangle region");
				emBrush->setToolTip("Draw region brush");
				emSelAll->setToolTip("Select whole image region");
				emSelNone->setToolTip("Clear region selection");
				emSelInv->setToolTip("Invert region selection");
				emZoomIn->setToolTip("Zoom in");
				emZoomOut->setToolTip("Zoom out");
				emZoom100->setToolTip("Show 100% zoom");
				emZoomFit->setToolTip("Fit zoom to window");

				emHand->two_state = true;
				emHand->ts_allow_unclick = false;
				emRect->two_state = true;
				emRect->ts_allow_unclick = false;
				emBrush->two_state = true;
				emBrush->ts_allow_unclick = false;

				emHand->bgImage = rMain->hBmpBg;
				emRect->bgImage = rMain->hBmpBg;
				emBrush->bgImage = rMain->hBmpBg;
				emSelAll->bgImage = rMain->hBmpBg;
				emSelNone->bgImage = rMain->hBmpBg;
				emSelInv->bgImage = rMain->hBmpBg;
				emZoomIn->bgImage = rMain->hBmpBg;
				emZoomOut->bgImage = rMain->hBmpBg;
				emZoom100->bgImage = rMain->hBmpBg;
				emZoomFit->bgImage = rMain->hBmpBg;

				emHand->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emRect->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emBrush->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emSelAll->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emSelNone->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emSelInv->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emZoomIn->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emZoomOut->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emZoom100->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
				emZoomFit->bmImage = EM2Elements::getInstance()->getHBitmapPtr();

				emHand->pNx = 170;
				emHand->pNy = 96;
				emHand->pMx = 200;
				emHand->pMy = 96;
				emHand->pCx = 230;
				emHand->pCy = 96;
				emHand->pDx = 260;
				emHand->pDy = 96;

				emRect->pNx = 170;
				emRect->pNy = 120;
				emRect->pMx = 200;
				emRect->pMy = 120;
				emRect->pCx = 230;
				emRect->pCy = 120;
				emRect->pDx = 260;
				emRect->pDy = 120;

				emBrush->pNx = 170;
				emBrush->pNy = 144;
				emBrush->pMx = 200;
				emBrush->pMy = 144;
				emBrush->pCx = 230;
				emBrush->pCy = 144;
				emBrush->pDx = 260;
				emBrush->pDy = 144;

				emSelAll->pNx = 170;
				emSelAll->pNy = 168;
				emSelAll->pMx = 200;
				emSelAll->pMy = 168;
				emSelAll->pCx = 230;
				emSelAll->pCy = 168;
				emSelAll->pDx = 260;
				emSelAll->pDy = 168;

				emSelNone->pNx = 170;
				emSelNone->pNy = 192;
				emSelNone->pMx = 200;
				emSelNone->pMy = 192;
				emSelNone->pCx = 230;
				emSelNone->pCy = 192;
				emSelNone->pDx = 260;
				emSelNone->pDy = 192;

				emSelInv->pNx = 170;
				emSelInv->pNy = 216;
				emSelInv->pMx = 200;
				emSelInv->pMy = 216;
				emSelInv->pCx = 230;
				emSelInv->pCy = 216;
				emSelInv->pDx = 260;
				emSelInv->pDy = 216;

				emZoomIn->pNx = 170;
				emZoomIn->pNy = 24;
				emZoomIn->pMx = 200;
				emZoomIn->pMy = 24;
				emZoomIn->pCx = 230;
				emZoomIn->pCy = 24;
				emZoomIn->pDx = 260;
				emZoomIn->pDy = 24;

				emZoomOut->pNx = 170;
				emZoomOut->pNy = 0;
				emZoomOut->pMx = 200;
				emZoomOut->pMy = 0;
				emZoomOut->pCx = 230;
				emZoomOut->pCy = 0;
				emZoomOut->pDx = 260;
				emZoomOut->pDy = 0;

				emZoom100->pNx = 170;
				emZoom100->pNy = 72;
				emZoom100->pMx = 200;
				emZoom100->pMy = 72;
				emZoom100->pCx = 230;
				emZoom100->pCy = 72;
				emZoom100->pDx = 260;
				emZoom100->pDy = 72;

				emZoomFit->pNx = 170;
				emZoomFit->pNy = 48;
				emZoomFit->pMx = 200;
				emZoomFit->pMy = 48;
				emZoomFit->pCx = 230;
				emZoomFit->pCy = 48;
				emZoomFit->pDx = 260;
				emZoomFit->pDy = 48;

				updateRegionButtonsState(hWnd, GetEMPViewInstance(rMain->hViewport));

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRegionsPanel, rect.top+rMain->bgShiftYRegionsPanel, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_REGIONS_HAND:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							empv->mode = EMPView::MODE_SHOW;
							updateRegionButtonsState(hWnd, empv);
							rMain->showRegionModeInStatus(false);
						}
						break;
					case IDC_REND2_REGIONS_RECT:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							empv->mode = EMPView::MODE_REGION_RECT;
							updateRegionButtonsState(hWnd, empv);
							rMain->showRegionModeInStatus(true);
						}
						break;
					case IDC_REND2_REGIONS_BRUSH:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							empv->mode = EMPView::MODE_REGION_AIRBRUSH;
							updateRegionButtonsState(hWnd, empv);
							rMain->showRegionModeInStatus(true);
						}
						break;
					case IDC_REND2_REGIONS_SEL_ALL:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							if (empv->rgnPtr)
								empv->rgnPtr->fillRegions();
							InvalidateRect(rMain->hViewport, NULL, false);
						}
						break;
					case IDC_REND2_REGIONS_SEL_NONE:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							if (empv->rgnPtr)
								empv->rgnPtr->clearRegions();
							InvalidateRect(rMain->hViewport, NULL, false);
						}
						break;
					case IDC_REND2_REGIONS_SEL_INVERT:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							if (empv->rgnPtr)
								empv->rgnPtr->inverseRegions();
							InvalidateRect(rMain->hViewport, NULL, false);
						}
						break;
					case IDC_REND2_REGIONS_ZOOM_IN:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							empv->zoomIn();
						}
						break;
					case IDC_REND2_REGIONS_ZOOM_OUT:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							empv->zoomOut();
						}
						break;
					case IDC_REND2_REGIONS_ZOOM_100:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							empv->zoom100();
						}
						break;
					case IDC_REND2_REGIONS_ZOOM_FIT:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							empv->zoomFit();
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsRegions()
{
	EM2ImgButton * embHand = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_HAND));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_HAND), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embHand->bgShiftX, embHand->bgShiftY);
	EM2ImgButton * embRect = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_RECT));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_RECT), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embRect->bgShiftX, embRect->bgShiftY);
	EM2ImgButton * embBrush = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_BRUSH));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_BRUSH), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embBrush->bgShiftX, embBrush->bgShiftY);
	EM2ImgButton * embSelAll = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_SEL_ALL));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_SEL_ALL), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embSelAll->bgShiftX, embSelAll->bgShiftY);
	EM2ImgButton * embSelNone = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_SEL_NONE));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_SEL_NONE), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embSelNone->bgShiftX, embSelNone->bgShiftY);
	EM2ImgButton * embSelInv = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_SEL_INVERT));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_SEL_INVERT), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embSelInv->bgShiftX, embSelInv->bgShiftY);
	EM2ImgButton * embZoomIn = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_IN));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_IN), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embZoomIn->bgShiftX, embZoomIn->bgShiftY);
	EM2ImgButton * embZoomOut = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_OUT));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_OUT), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embZoomOut->bgShiftX, embZoomOut->bgShiftY);
	EM2ImgButton * embZoom100 = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_100));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_100), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embZoom100->bgShiftX, embZoom100->bgShiftY);
	EM2ImgButton * embZoomFit = GetEM2ImgButtonInstance(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_FIT));
	updateControlBgShift(GetDlgItem(hRegionsPanel, IDC_REND2_REGIONS_ZOOM_FIT), bgShiftXRegionsPanel, bgShiftYRegionsPanel, embZoomFit->bgShiftX, embZoomFit->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnRegions(HWND hWnd)
{
	int x = 0, y = 0;
	
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_HAND),			HWND_TOP,	x+0,	y+0,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_RECT),			HWND_TOP,	x+0,	y+24*1,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_BRUSH),			HWND_TOP,	x+0,	y+24*2,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_SEL_ALL),		HWND_TOP,	x+0,	y+24*3,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_SEL_NONE),		HWND_TOP,	x+0,	y+24*4,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_SEL_INVERT),	HWND_TOP,	x+0,	y+24*5,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_IN),		HWND_TOP,	x+0,	y+24*6,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_OUT),		HWND_TOP,	x+0,	y+24*7,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_100),		HWND_TOP,	x+0,	y+24*8,			32,		24,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REGIONS_ZOOM_FIT),		HWND_TOP,	x+0,	y+24*9,			32,		24,			0);
}

//------------------------------------------------------------------------------------------------

bool updateRegionButtonsState(HWND hWnd, EMPView * empv)
{
	CHECK(hWnd);
	CHECK(empv);
	EM2ImgButton * embHand = GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_HAND));
	EM2ImgButton * embRect = GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_RECT));
	EM2ImgButton * embBrush = GetEM2ImgButtonInstance(GetDlgItem(hWnd, IDC_REND2_REGIONS_BRUSH));
	embHand->selected = (empv->mode==EMPView::MODE_SHOW);
	embRect->selected = (empv->mode==EMPView::MODE_REGION_RECT);
	embBrush->selected = (empv->mode==EMPView::MODE_REGION_AIRBRUSH);
	InvalidateRect(embHand->hwnd, NULL, false);
	InvalidateRect(embRect->hwnd, NULL, false);
	InvalidateRect(embBrush->hwnd, NULL, false);
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::udpateRegionPanelModeFromViewport()
{
	updateRegionButtonsState(hRegionsPanel, GetEMPViewInstance(hViewport));
	return true;
}

//------------------------------------------------------------------------------------------------
