#include <Windows.h>
#include "ColorSchemes.h"

extern HMODULE hDllModule;

char * testText = "Undefined message";

#define NMSG_WIDTH_MAX 1000
#define NMSG_WIDTH_MIN 160
#define NMSG_HEIGHT_MAX 500
#define NMSG_HEIGHT_MIN 40

struct MsgBoxNoxSett
{
	char * msg;
	char * title;
	HFONT hFontMsg;
	HFONT hFontBtn;
	MsgBoxNoxSett() { msg=NULL; title=NULL; hFontBtn=0; hFontMsg=0;	}
};

//----------------------------------------------------------------------------------------------

INT_PTR CALLBACK MessageBoxDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HFONT hFont = 0;
	static char * txt = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				MsgBoxNoxSett * sett = (MsgBoxNoxSett *)lParam;

				if (!bgBrush)
					bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
				if (sett  &&  sett->hFontMsg)
					hFont = sett->hFontMsg;

				txt = testText;
				if (sett  &&  sett->msg)
					txt = sett->msg;
				RECT recttxt;
				if (txt)
				{
					HDC tthdc = GetWindowDC(hWnd);
					HFONT oldFont = (HFONT)SelectObject(tthdc, hFont);
					BOOL s_ok = DrawText(tthdc, txt, (int)strlen(txt), &recttxt, DT_CALCRECT);
					SelectObject(tthdc, oldFont);
					ReleaseDC(hWnd, tthdc);

					recttxt.right  = min(NMSG_WIDTH_MAX, max(NMSG_WIDTH_MIN, recttxt.right-recttxt.left));
					recttxt.bottom = min(NMSG_HEIGHT_MAX, max(NMSG_HEIGHT_MIN, recttxt.bottom-recttxt.top));
					recttxt.left = recttxt.top = 0;
				}

				RECT rect2 = recttxt;

				if (AdjustWindowRectEx(&rect2, GetWindowLong(hWnd, GWL_STYLE), FALSE, GetWindowLong(hWnd, GWL_EXSTYLE)))
					SetWindowPos(hWnd, HWND_TOP, 0,0, rect2.right-rect2.left+10, rect2.bottom-rect2.top+45, SWP_NOMOVE | SWP_NOZORDER);

				HWND hTest = CreateWindow("EMButton", "OK", WS_VISIBLE | WS_CHILD, (recttxt.right-50)/2+5, recttxt.bottom+15, 50,21, hWnd, (HMENU)11012, hDllModule, 0);
				EMButton * emb = GetEMButtonInstance(hTest);
				GlobalWindowSettings::colorSchemes.apply(emb);
				if  (sett  &&  sett->hFontBtn)
					emb->setFont(sett->hFontBtn);

				if (sett  &&  sett->title)
					SetWindowText(hWnd, sett->title);
				else
					SetWindowText(hWnd, "Message");

			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, IDCANCEL);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				hFont = 0;
				txt = NULL;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);

				HFONT oldFont = (HFONT)SelectObject(hdc, hFont);
				RECT recttext = { rect.left+5, rect.top+5,rect.right-5, rect.bottom-40 };

				SetBkColor(hdc, GlobalWindowSettings::colorSchemes.DialogBackground);
				SetTextColor(hdc,  GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc, TRANSPARENT);
				
				//recttext = rect;
				if (txt)
					DrawText(hdc, txt, (int)strlen(txt), &recttext, DT_NOCLIP);
				SelectObject(hdc, oldFont);

				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case 11012:
						{
							EndDialog(hWnd, IDOK);
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}



//----------------------------------------------------------------------------------------------

int MessageBoxNox(HWND hParent, char *messageText, char *titleText, HFONT hFontText, HFONT hFontButton)
{
	HGLOBAL hdtmpl = GlobalAlloc (GMEM_ZEROINIT, sizeof (DLGTEMPLATE)+ 3 * sizeof (WORD));
	DLGTEMPLATE * ldtmpl = (DLGTEMPLATE *)GlobalLock (hdtmpl);
	ldtmpl->style = DS_CENTER | DS_SETFOREGROUND | WS_CAPTION | WS_SYSMENU;
	ldtmpl->dwExtendedStyle = WS_EX_TOOLWINDOW | WS_EX_TOPMOST;
	ldtmpl->cdit = 0;
	ldtmpl->x = 100; 
	ldtmpl->y = 100; 
	ldtmpl->cx = 300; 
	ldtmpl->cy = 200; 
	GlobalUnlock (hdtmpl);

	MsgBoxNoxSett msgsett;
	msgsett.msg = messageText;
	msgsett.title = titleText;
	msgsett.hFontBtn = hFontButton;
	msgsett.hFontMsg = hFontText;

	int r = (int)DialogBoxIndirectParam(hDllModule, ldtmpl, hParent, MessageBoxDlgProc, (LPARAM)&msgsett);
	if (r<1)
	{

		int err = GetLastError();
		char * errorText = NULL;
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS,  
			NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&errorText, 0, NULL);
		if ( NULL != errorText )
		{
			
			MessageBox(0, messageText, titleText, 0);
			LocalFree(errorText);
			errorText = NULL;
		}
	}

	GlobalFree (hdtmpl);

	return r;
}

//----------------------------------------------------------------------------------------------
