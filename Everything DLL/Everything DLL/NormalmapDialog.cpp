#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "ColorSchemes.h"
#include "log.h"
#include "DLL.h"
#include "valuesMinMax.h"

#define RECALC_PREV_AS_THREAD

#define TIMER_REDRAW_NORMAL_ID 152

extern HMODULE hDllModule;

void lockNormalDlgBeforeRefresh(HWND hwnd);
void unlockNormalDlgAfterRefresh(HWND hwnd);
void lockNormalDlgBeforeLoading(HWND hwnd);
void unlockNormalDlgAfterLoading(HWND hwnd);
void refreshNormalPost(HWND hwnd);
DWORD WINAPI refreshNormalPostThreadProc(LPVOID lpParameter);
bool makeNormalPreview(HWND hwnd);
bool updateGUItoNormalModifier(HWND hwnd, Texture * tex);
bool updateNormalModifierToGui(HWND hwnd, Texture * tex);
void updateDialogFromNormal(Texture * tex, HWND hwnd);
bool makeShadePreview(HWND hWnd, int px, int py, int d);
Texture * getNormalTexFromEMPV(HWND hwnd);

//----------------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK NormalDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_NORMAL_TEXT_INVERT_X)),		true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_NORMAL_TEXT_INVERT_Y)),		true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_NORMAL_TEXT_RES)),			true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_NORMAL_TEXT_RES_VAL)),		true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_NORMAL_TEXT_POWER)),		true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_NORMAL_FILENAME)),			true);
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_NORMAL_OK)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_NORMAL_CANCEL)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_NORMAL_LOAD)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_NORMAL_UNLOAD)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_NORMAL_BUTTON_RESET_POWER)));
			GlobalWindowSettings::colorSchemes.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_NORMAL_SLIDER_POWER)));
			GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL_INVERT_X)));
			GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_NORMAL_INVERT_Y)));
			GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX)));
			GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_SHADE)));
			GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_NORMAL_GR_FILE)));
			GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_NORMAL_GR_MODIFIERS)));
			GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_NORMAL_GR_TEX_PREV)));
			GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_NORMAL_GR_SHADE_PREV)));

			HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX);
			HWND hShade = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_SHADE);
			SetWindowPos(hPrev,  HWND_TOP, 0,0, 256,256, SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hShade, HWND_TOP, 0,0, 256,256, SWP_NOZORDER | SWP_NOMOVE);
			EMPView * empv = GetEMPViewInstance(hPrev);
			EMPView * empvshade = GetEMPViewInstance(hShade);

			EMEditTrackBar * empower = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_NORMAL_SLIDER_POWER));
			empower->setEditBoxWidth(32);
			empower->setValuesToFloat(1.0f, NOX_NORMAL_POWER_MIN, NOX_NORMAL_POWER_MAX, 0.1f);

			empv->allowDragFiles();

			empvshade->dontLetUserChangeZoom = true;
			empvshade->byteBuff = new ImageByteBuffer();
			empvshade->byteBuff->allocBuffer(256,256);

			Texture * tex = (Texture *)lParam;
			Texture * tex2 = new Texture();
			empv->reserved2 = tex2;

			if (tex)
			{
				empv->reserved1 = (void *)tex;
				tex2->copyFromOther(tex);
				updateDialogFromNormal(tex2, hWnd);
			}

			updateNormalModifierToGui(hWnd, getNormalTexFromEMPV(hWnd));

			refreshNormalPost(hWnd);
			Sleep(50);
			empv->mx = empv->my = 0;
			empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);


		}
		break;
	case WM_DESTROY:
		{
			HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
			EMPView * empv = GetEMPViewInstance(hPrev);
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_NORMAL_OK:
					{
						if (wmEvent == BN_CLICKED)
						{
							KillTimer(hWnd, TIMER_REDRAW_NORMAL_ID);

							HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX);
							EMPView * empv = GetEMPViewInstance(hPrev);
							Texture * tex2 = (Texture *)empv->reserved2;		// new tex
							Texture * tex1 = (Texture *)empv->reserved1;		// old tex
						
							if (tex1)
							{
								tex1->freeAllBuffers();
								updateGUItoNormalModifier(hWnd, tex1);
								delete tex1;
								empv->reserved1 = NULL;
							}

							if (tex2)
								updateGUItoNormalModifier(hWnd, tex2);

							ImageByteBuffer * ib = empv->byteBuff;
							empv->byteBuff = NULL;
							if (ib)
							{
								ib->freeBuffer();
								delete ib;
							}

							EMPView * empvshade = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_SHADE));
							ib = empvshade->byteBuff;
							empvshade->byteBuff = NULL;
							if (ib)
							{
								ib->freeBuffer();
								delete ib;
							}
							
							EndDialog(hWnd, (INT_PTR)(tex2));
						}
					}
					break;
				case IDC_NORMAL_CANCEL:
					{
						if (wmEvent == BN_CLICKED)
						{
							KillTimer(hWnd, TIMER_REDRAW_NORMAL_ID);

							HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX);
							EMPView * empv = GetEMPViewInstance(hPrev);
							Texture * tex1 = (Texture *)empv->reserved1;
							Texture * tex2 = (Texture *)empv->reserved2;
							if (tex2)
							{
								tex2->freeAllBuffers();
								delete tex2;
							}
							empv->reserved2 = NULL;

							if (tex1)
							{
								tex1->freeAllBuffers();
								delete tex1;
							}
							empv->reserved2 = NULL;

							// free preview
							ImageByteBuffer * ib = empv->byteBuff;
							empv->byteBuff = NULL;
							if (ib)
							{
								ib->freeBuffer();
								delete ib;
							}

							EMPView * empvshade = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_SHADE));
							ib = empvshade->byteBuff;
							empvshade->byteBuff = NULL;
							if (ib)
							{
								ib->freeBuffer();
								delete ib;
							}

							EndDialog(hWnd, (INT_PTR)NULL);
						}
					}
					break;
				case IDC_NORMAL_PREVIEW_TEX:
					{
						if (wmEvent==PV_DROPPED_FILE)
						{
							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX));
							CHECK(empv);
							if (!empv->lastDroppedFilename)
								break;

							Logger::add("Loading dropped file:");
							Logger::add(empv->lastDroppedFilename);

							lockNormalDlgBeforeLoading(hWnd);

							Texture * tex = new Texture();
							if (tex->loadFromFile(empv->lastDroppedFilename)  &&   tex->isValid())
							{
								HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX);
								EMPView * empv = GetEMPViewInstance(hPrev);
								Texture * ttex = (Texture *)empv->reserved2;
								if (ttex)
								{
									ttex->freeAllBuffers();
									delete ttex;
								}
								empv->reserved2 = (void *)tex;

								if (!tex->isItNormalMap())
								{
									tex->convertMeToNormalMap();
									tex->is_it_converted_normal = true;
								}

								empv->mx = empv->my = 0;
								refreshNormalPost(hWnd);

								updateDialogFromNormal(tex, hWnd);

								Logger::add("Texture loaded.");

								Sleep(50);
								empv->mx = empv->my = 0;
								empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);
							}
							else
							{
								unlockNormalDlgAfterLoading(hWnd);
								MessageBox(0, "Failed opening. It's probably not an image.", "Error", 0);
							}

							if (empv->lastDroppedFilename)
								free(empv->lastDroppedFilename);
							empv->lastDroppedFilename = NULL;
						}
					}
					break;
				case IDC_NORMAL_LOAD:
					{
						if (wmEvent != BN_CLICKED)
							break;

						KillTimer(hWnd, TIMER_REDRAW_NORMAL_ID);
						Logger::add("Texture load clicked.");

						char * fname = openTexDialog(hWnd);
						if (!fname)
						{
							Logger::add("No file selected.");
							break;
						}

						Logger::add("Loading file:");
						Logger::add(fname);

						lockNormalDlgBeforeLoading(hWnd);

						Texture * tex = new Texture();
						if (tex->loadFromFile(fname)  &&   tex->isValid())
						{
							HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX);
							EMPView * empv = GetEMPViewInstance(hPrev);
							Texture * ttex = (Texture *)empv->reserved2;
							if (ttex)
							{
								ttex->freeAllBuffers();
								delete ttex;
							}
							empv->reserved2 = (void *)tex;

							if (!tex->isItNormalMap())
							{
								tex->convertMeToNormalMap();
								tex->is_it_converted_normal = true;
							}


							empv->mx = empv->my = 0;
							refreshNormalPost(hWnd);

							updateDialogFromNormal(tex, hWnd);

							Logger::add("Texture loaded.");

							Sleep(50);
							empv->mx = empv->my = 0;
							empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);
						}
						else
						{
							unlockNormalDlgAfterLoading(hWnd);
						}

						if (fname)
							free(fname);
					}
					break;
				case IDC_NORMAL_UNLOAD:
					{
						CHECK(wmEvent==BN_CLICKED);

						KillTimer(hWnd, TIMER_REDRAW_NORMAL_ID);
						Texture * tex = new Texture();
						HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX);
						EMPView * empv = GetEMPViewInstance(hPrev);

						Texture * old = (Texture *)empv->reserved2;
						empv->reserved2 = (void*) tex;

						if (old)
						{
							old->freeAllBuffers();
							delete old;
						}

						updateDialogFromNormal(tex, hWnd);

						refreshNormalPost(hWnd);
					}
					break;
				case IDC_NORMAL_INVERT_X:
				case IDC_NORMAL_INVERT_Y:
					{
						updateGUItoNormalModifier(hWnd, getNormalTexFromEMPV(hWnd));
					}
					break;
				case IDC_NORMAL_BUTTON_RESET_POWER:
					{
						CHECK(wmEvent==BN_CLICKED);
						Texture * tex = getNormalTexFromEMPV(hWnd);
						CHECK(tex);
						tex->normMod.power = 1.0f;
						tex->normMod.powerEV = 0.0f;
						EMEditTrackBar * emPower = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_NORMAL_SLIDER_POWER));
						emPower->setValuesToFloat(0.0f, NOX_NORMAL_POWER_MIN, NOX_NORMAL_POWER_MAX, 0.1f);
					}
					break;
				case IDC_NORMAL_SLIDER_POWER:
					{
						CHECK(wmEvent==WM_HSCROLL);
						updateGUItoNormalModifier(hWnd, getNormalTexFromEMPV(hWnd));
					}
					break;
			}
		}
		break;
	case WM_MOUSEMOVE:
		{
			POINT pos;
			pos.x = (short)LOWORD(lParam); 
			pos.y = (short)HIWORD(lParam); 
			ClientToScreen(hWnd, &pos);
			ScreenToClient(GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_SHADE), &pos);

			makeShadePreview(hWnd, pos.x, pos.y, 128);
			InvalidateRect(GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_SHADE), NULL, false);
		}
		break;
	case WM_TIMER:
		{
			if (wParam == TIMER_REDRAW_NORMAL_ID)
			{
				KillTimer(hWnd, TIMER_REDRAW_NORMAL_ID);
				refreshNormalPost(hWnd);
			}
		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)bgBrush;
		}
	case WM_CTLCOLORDLG:
		{
			return (INT_PTR)bgBrush;
		}
		break;
	}
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------

Texture * getNormalTexFromEMPV(HWND hwnd)
{
	HWND hPrev = GetDlgItem(hwnd, IDC_NORMAL_PREVIEW_TEX);
	EMPView * empv = GetEMPViewInstance(hPrev);
	if (empv->reserved2)
		return (Texture *)empv->reserved2;
	else
		return (Texture *)empv->reserved1;
}

//----------------------------------------------------------------------------------------------------------------------------

void refreshNormalPost(HWND hwnd)
{
	#ifdef RECALC_PREV_AS_THREAD
		lockNormalDlgBeforeRefresh(hwnd);
		DWORD threadId = 0;
		HANDLE hThread = CreateThread( NULL, 0, 
			(LPTHREAD_START_ROUTINE)(refreshNormalPostThreadProc), (LPVOID)hwnd, 0, &threadId);
		if (!hThread)
		{
			unlockNormalDlgAfterRefresh(hwnd);
		}
	#else
		updateGUItoNormalModifier(hwnd, getTextureFromEMPV(hwnd));
		makeNormalPreview(hwnd);
		InvalidateRect(GetDlgItem(hwnd, IDC_NORMAL_PREVIEW), NULL, false);
	#endif
}

//----------------------------------------------------------------------------------------------------------------------------

DWORD WINAPI refreshNormalPostThreadProc(LPVOID lpParameter)
{
	HWND hwnd = (HWND)lpParameter;
	DWORD curThreadID = GetCurrentThreadId();
	static bool thMutex = false;
	static DWORD lastThreadID = 0;
	lastThreadID = curThreadID;

	while (thMutex)
	{
		Sleep(10);
	}

	if (lastThreadID != curThreadID)
		return 0;

	thMutex = true;

	updateGUItoNormalModifier(hwnd, getNormalTexFromEMPV(hwnd));

	makeNormalPreview(hwnd);

	unlockNormalDlgAfterRefresh(hwnd);

	thMutex = false;

	return 0;
}

//----------------------------------------------------------------------------------------------------------------------------

bool updateGUItoNormalModifier(HWND hwnd, Texture * tex)
{
	CHECK(hwnd);
	CHECK(tex);
	EMCheckBox * emc_inv_x = GetEMCheckBoxInstance(GetDlgItem(hwnd, IDC_NORMAL_INVERT_X));
	EMCheckBox * emc_inv_y = GetEMCheckBoxInstance(GetDlgItem(hwnd, IDC_NORMAL_INVERT_Y));
	EMEditTrackBar * emPower = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_NORMAL_SLIDER_POWER));
	tex->normMod.invertX     = emc_inv_x->selected;
	tex->normMod.invertY     = emc_inv_y->selected;
	tex->normMod.powerEV	 = emPower->floatValue;
	tex->normMod.power = pow(2.0f, tex->normMod.powerEV);

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------

bool updateNormalModifierToGui(HWND hwnd, Texture * tex)
{
	CHECK(hwnd);
	CHECK(tex);

	EMCheckBox * emc_inv_x = GetEMCheckBoxInstance(GetDlgItem(hwnd, IDC_NORMAL_INVERT_X));
	EMCheckBox * emc_inv_y = GetEMCheckBoxInstance(GetDlgItem(hwnd, IDC_NORMAL_INVERT_Y));
	EMEditTrackBar * emPower = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_NORMAL_SLIDER_POWER));

	emc_inv_x->selected = tex->normMod.invertX;
	emc_inv_y->selected = tex->normMod.invertY;
	InvalidateRect(emc_inv_x->hwnd, NULL, false);
	InvalidateRect(emc_inv_y->hwnd, NULL, false);
	emPower->setValuesToFloat(tex->normMod.powerEV, NOX_NORMAL_POWER_MIN, NOX_NORMAL_POWER_MAX, 0.1f);

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------

bool makeNormalPreview(HWND hwnd)
{
	int i,j,x,y;
	HWND hPrev = GetDlgItem(hwnd, IDC_NORMAL_PREVIEW_TEX);
	EMPView * empv = GetEMPViewInstance(hPrev);
	Texture * tex;
	if (empv->reserved2)
		tex = (Texture *)empv->reserved2;
	else
		tex = (Texture *)empv->reserved1;
	

	ImageByteBuffer * iBuf = NULL;
	if (tex->isValid()  &&  !tex->bmp.isItFloat)
	{
		if (empv->byteBuff  &&  empv->byteBuff->width==tex->bmp.getWidth()  &&  empv->byteBuff->height==tex->bmp.getHeight())
			iBuf = empv->byteBuff;
		else
		{
			iBuf = new ImageByteBuffer();
			iBuf->allocBuffer(tex->bmp.getWidth(), tex->bmp.getHeight());
		}
	}
	else
	{
		RECT crect;
		GetClientRect(hPrev, &crect);
		if (empv->byteBuff  &&  empv->byteBuff->width==crect.right  &&  empv->byteBuff->height==crect.bottom)
			iBuf = empv->byteBuff;
		else
		{
			iBuf = new ImageByteBuffer();
			iBuf->allocBuffer(crect.right, crect.bottom);
		}
		iBuf->clearBuffer();

		if (empv->byteBuff != iBuf)
		{
			ImageByteBuffer * oldBuf = empv->byteBuff;
			empv->byteBuff = iBuf;
			if (oldBuf)
			{
				oldBuf->freeBuffer();
				delete oldBuf;
			}
			return true;
		}
	}

	x = tex->bmp.getWidth();
	y = tex->bmp.getHeight();
	if (x<=0  ||  y<=0)
		return false;

	if (!tex->bmp.isItFloat)
	{
		for (j=0; j<y; j++)
		{
			for (i=0; i<x; i++)
			{
				Color4 c;
				unsigned char cr = tex->bmp.idata[(j*x+i)*4+0];
				unsigned char cg = tex->bmp.idata[(j*x+i)*4+1];
				unsigned char cb = tex->bmp.idata[(j*x+i)*4+2];
				iBuf->imgBuf[j][i] = RGB(cr, cg, cb);
			}
		}
	}

	if (empv->byteBuff != iBuf)
	{
		ImageByteBuffer * oldBuf = empv->byteBuff;
		empv->byteBuff = iBuf;
		if (oldBuf)
		{
			oldBuf->freeBuffer();
			delete oldBuf;
		}
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------

void lockNormalDlgBeforeRefresh(HWND hwnd)
{
	disableControl(hwnd, IDC_NORMAL_CANCEL);
	disableControl(hwnd, IDC_NORMAL_OK);
	disableControl(hwnd, IDC_NORMAL_INVERT_X);
	disableControl(hwnd, IDC_NORMAL_INVERT_Y);
	disableControl(hwnd, IDC_NORMAL_LOAD);
	disableControl(hwnd, IDC_NORMAL_UNLOAD);
	disableControl(hwnd, IDC_NORMAL_PREVIEW_TEX);
}

//----------------------------------------------------------------------------------------------------------------------------

void unlockNormalDlgAfterRefresh(HWND hwnd)
{
	enableControl(hwnd, IDC_NORMAL_CANCEL);
	enableControl(hwnd, IDC_NORMAL_OK);
	enableControl(hwnd, IDC_NORMAL_INVERT_X);
	enableControl(hwnd, IDC_NORMAL_INVERT_Y);
	enableControl(hwnd, IDC_NORMAL_LOAD);
	enableControl(hwnd, IDC_NORMAL_UNLOAD);
	enableControl(hwnd, IDC_NORMAL_PREVIEW_TEX);
}

//----------------------------------------------------------------------------------------------------------------------------

void lockNormalDlgBeforeLoading(HWND hwnd)
{
	disableControl(hwnd, IDC_NORMAL_CANCEL);
	disableControl(hwnd, IDC_NORMAL_OK);
	disableControl(hwnd, IDC_NORMAL_INVERT_X);
	disableControl(hwnd, IDC_NORMAL_INVERT_Y);
	disableControl(hwnd, IDC_NORMAL_LOAD);
	disableControl(hwnd, IDC_NORMAL_UNLOAD);
	disableControl(hwnd, IDC_NORMAL_PREVIEW_TEX);
}

//----------------------------------------------------------------------------------------------------------------------------

void unlockNormalDlgAfterLoading(HWND hwnd)
{
	enableControl(hwnd, IDC_NORMAL_CANCEL);
	enableControl(hwnd, IDC_NORMAL_OK);
	enableControl(hwnd, IDC_NORMAL_INVERT_X);
	enableControl(hwnd, IDC_NORMAL_INVERT_Y);
	enableControl(hwnd, IDC_NORMAL_LOAD);
	enableControl(hwnd, IDC_NORMAL_UNLOAD);
	enableControl(hwnd, IDC_NORMAL_PREVIEW_TEX);
}

//----------------------------------------------------------------------------------------------------------------------------

void updateDialogFromNormal(Texture * tex, HWND hwnd)
{
	if (!hwnd)
		return;

	HWND hRes = GetDlgItem(hwnd, IDC_NORMAL_TEXT_RES_VAL);
	char buf[64];
	if (tex)
		sprintf_s(buf, 64, "%dx%d", tex->bmp.getWidth(), tex->bmp.getHeight());
	else
		sprintf_s(buf, 64, "0x0");
	EMText * emtres = GetEMTextInstance(hRes);
	emtres->changeCaption(buf);

	HWND hFName = GetDlgItem(hwnd, IDC_NORMAL_FILENAME);
	EMText * emtfilename = GetEMTextInstance(hFName);
	if (tex  &&  tex->filename)
		emtfilename->changeCaption(tex->filename);
	else
		emtfilename->changeCaption("No texture loaded.");
}

//----------------------------------------------------------------------------------------------------------------------------

bool makeShadePreview(HWND hWnd, int px, int py, int d)
{
	HWND hPrev = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_TEX);
	HWND hShade = GetDlgItem(hWnd, IDC_NORMAL_PREVIEW_SHADE);
	EMPView * empvsrc = GetEMPViewInstance(hPrev);
	EMPView * empvdst = GetEMPViewInstance(hShade);
	if (!empvsrc->byteBuff->imgBuf)
		return false;
	if (!empvdst->byteBuff->imgBuf)
		return false;

	Texture * tex = NULL;
	if (empvsrc->reserved2)
		tex = (Texture *)empvsrc->reserved2;
	else
		tex = (Texture *)empvsrc->reserved1;
	if (!tex  ||  !tex->filename)
	{
		empvdst->byteBuff->clearBuffer();
		return false;
	}

	float invx = tex->normMod.invertX ? -1.0f : 1.0f;
	float invy = tex->normMod.invertY ? -1.0f : 1.0f;
	int sx = empvsrc->byteBuff->width;
	int sy = empvsrc->byteBuff->height;
	float mx = empvsrc->mx;
	float my = empvsrc->my;
	int cx = 256;
	int cy = 256;
	float mycy = my*cy;
	float mxcx = mx*cx;
	float zoom = empvsrc->zoom;
	float perpower = 1.0f/tex->normMod.power;

	for (int y=0; y<256; y++)
	{
		int ty = (int)((y)/zoom + mycy);
		for (int x=0; x<256; x++)
		{
			int tx = (int)(x/zoom + mxcx);
			if (ty<0 || ty>=sy  ||  tx<0 || tx>=sx)
				empvdst->byteBuff->imgBuf[y][x] = RGB(0,0,0);
			else
			{
				int dx = px-x;
				int dy = y-py;

				COLORREF csrc = empvsrc->byteBuff->imgBuf[ty][tx];
				float vx = (GetRValue(csrc)-128)/128.0f;
				float vy = (GetGValue(csrc)-128)/128.0f;
				float vz = (GetBValue(csrc)-128)/128.0f;
				Vector3d vecnormal = Vector3d(vx*invx, vy*invy, vz*perpower);
				vecnormal.normalize();

				Vector3d vecdir = Vector3d((float)dx, (float)dy, (float)d);
				vecdir.normalize();

				float shval = vecdir * vecnormal;

				unsigned char cc = (unsigned char)min(255, max(0, (int)(255*shval)));

				empvdst->byteBuff->imgBuf[y][x] = RGB(cc,cc,cc);
			}

		}
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------------------------
