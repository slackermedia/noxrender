#include <windows.h>
#include <stdio.h>
#include "EMControls.h"
#include <commctrl.h>

extern HMODULE hDllModule;

EMEditTrackBar::EMEditTrackBar(HWND hWnd) 
{
	colBackground = RGB(96, 192, 255);
	colPathBorderUpLeft = RGB(64,128,160);
	colPathBorderDownRight = RGB(128, 224, 255);
	colPathBackgroundBefore = RGB(255,128,0);
	colPathBackgroundAfter = RGB(0,0,0);
	colSliderBorderUpLeft = RGB(128, 192, 255);
	colSliderBorderDownRight = RGB(32,96,208);
	colSliderBackground = RGB(64,128,255);
	colSliderBackgroundClicked = RGB(96,160,255);

	colEditBorderUpLeft = RGB(64,160,224);
	colEditBorderDownRight = RGB(128,224,255);
	colEditBackground = RGB(160,240,255);
	colEditText = RGB(0,0,0);

	colDisabledBorderLight = RGB(224,224,224);
	colDisabledBorderDark = RGB(160,160,160);
	colDisabledBackground = RGB(192,192,192);
	colDisabledEditBackground = RGB(192,192,192);
	colDisabledEditText = RGB(96,96,96);


	hEditBrush = CreateSolidBrush(colEditBackground);
	hEditDisabledBrush = CreateSolidBrush(colDisabledEditBackground);
	hwnd = hWnd;
	hEdit = 0;
	allowFloat = true;
	nowScrolling = false;
	nowChanged = false;

	minIntValue = 0;
	maxIntValue = 100;
	minFloatValue = 0;
	maxFloatValue = 100;
	intValue = minIntValue;
	floatValue = minFloatValue;
	intFastValue = 5;
	floatFastValue = 5;
	floatAfterDot = 2;


	height = 18;
	editRectWidth = 64;
	pathHeight = 3;
	pathXStart = 5;
	sliderWidth = 8;
	sliderHeight = 16;
	sliderPos = 0;
	resize();
}

void EMEditTrackBar::resize()
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	pathLength = rect.right - pathXStart - editRectWidth - 5;
	pathXEnd = pathLength + pathXStart;

	if (allowFloat)
		sliderPos = (int)((floatValue - minFloatValue)/(float)(maxFloatValue - minFloatValue)*pathLength);
	else
		sliderPos = (int)((intValue - minIntValue)/(float)(maxIntValue - minIntValue)*pathLength);

	if (sliderPos > pathLength)
		sliderPos = pathLength;
	if (sliderPos < 0)
		sliderPos = 0;
}

EMEditTrackBar::~EMEditTrackBar() 
{
	if (hEditBrush)
		DeleteObject(hEditBrush);
}

void InitEMEditTrackBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMEditTrackBar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMEditTrackBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMEditTrackBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMEditTrackBar * GetEMEditTrackBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMEditTrackBar * emetb = (EMEditTrackBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EMEditTrackBar * emetb = (EMEditTrackBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emetb;
}

void SetEMEditTrackBarInstance(HWND hwnd, EMEditTrackBar *emetb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emetb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emetb);
	#endif
}

typedef LRESULT (WINAPI * EDITPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EDITPROC OldTrackBarProc;

void verifyEditTrackbarControlData(HWND hwnd, int &lastIntValue, float &lastFloatValue)
{
	char * addr;
	char * addr1;
	char * addr2;
	char buff[512];
	memset(buff, 0, 512);
	((WORD*)buff)[0] = 512;
	int cpied = (int)SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
	if (cpied>=0)
		buff[cpied]=0;
	HWND hFazia = GetParent(hwnd);
	EMEditTrackBar * emetb;
	emetb = GetEMEditTrackBarInstance(hFazia);
	bool notify  = false;
	if (emetb->allowFloat)
	{
		float a = (float)_strtod_l(buff, &addr1, noxLocale);
		float b = (float)_strtod_l(buff, &addr2, noxLocaleComa);
		if (addr1 == buff   &&   addr2 == buff)
		{	// conversion error (to float)
			a = lastFloatValue;
			emetb->floatToEditbox(a);
		}
		else
		{
			if (addr2>addr1)
				a = b;
			if (a > emetb->maxFloatValue)
				a = emetb->maxFloatValue;
			if (a < emetb->minFloatValue)
				a = emetb->minFloatValue;
			if (lastFloatValue != a)
				if (fabs(lastFloatValue-a) > pow(0.1f, emetb->floatAfterDot))
					notify = true;
			emetb->floatToEditbox(a);
			lastFloatValue = a;
			emetb->floatValue = a;
		}
		emetb->sliderPos = (int)((a-emetb->minFloatValue)/(float)(emetb->maxFloatValue-emetb->minFloatValue)*emetb->pathLength);
		if (emetb->sliderPos > emetb->pathLength)
			emetb->sliderPos = emetb->pathLength;
		if (emetb->sliderPos < 0)
			emetb->sliderPos = 0;
	}
	else
	{
		int a = (int)_strtoi64(buff, &addr, 10);
		if (addr == buff)
		{	// conversion error (to int)
			a = lastIntValue;
			emetb->intToEditbox(a);
		}
		else
		{
			if (a > emetb->maxIntValue)
				a = emetb->maxIntValue;
			if (a < emetb->minIntValue)
				a = emetb->minIntValue;
			emetb->intToEditbox(a);
			if (lastIntValue != a)
				notify = true;
			lastIntValue = a;
			emetb->intValue = a;
		}
		emetb->sliderPos = (int)((a-emetb->minIntValue)/(float)(emetb->maxIntValue-emetb->minIntValue)*emetb->pathLength);
		if (emetb->sliderPos > emetb->pathLength)
			emetb->sliderPos = emetb->pathLength;
		if (emetb->sliderPos < 0)
			emetb->sliderPos = 0;
	}
	RECT rect;
	GetClientRect(hFazia, &rect);
	InvalidateRect(hFazia, &rect, FALSE);
	if (notify)
		emetb->notifyParent();
}

LRESULT CALLBACK EMEditTrackBarEditProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static int lastIntValue = 0;
	static float lastFloatValue = 0;

	switch (msg)
	{
	case 65001:
		{
			lastIntValue = (int&)wParam;
			lastFloatValue = (float&)lParam;
		}
		break;
	case WM_CHAR:
	//case WM_KEYDOWN:
		{
			if (wParam == VK_RETURN)
			{
				verifyEditTrackbarControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_RETURN), (LPARAM)hwnd);
				SendMessage(hwnd, EM_SETSEL, 0, -1);
				return TRUE;
			}
			if (wParam == VK_ESCAPE)
			{
				HWND hFazia = GetParent(hwnd);
				EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hFazia);
				if (emetb->allowFloat)
				{
					emetb->floatValue = lastFloatValue;
					emetb->floatToEditbox(lastFloatValue);
				}
				else
				{
					emetb->intValue = lastIntValue;
					emetb->intToEditbox(lastIntValue);
				}
				return TRUE;
			}
			if (wParam == VK_TAB)
			{
				verifyEditTrackbarControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				if (GetKeyState(VK_SHIFT)&0x8000)
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB_SHIFT), (LPARAM)hwnd);
				else
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB), (LPARAM)hwnd);

				return TRUE;
			}
		}
		break;
	case WM_KILLFOCUS:
		{
			verifyEditTrackbarControlData(hwnd, lastIntValue, lastFloatValue);
		}
		break;
	case WM_SETFOCUS:
		{
			char * addr;
			char buff[512];
			((WORD*)buff)[0] = 512;
			SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
			HWND hFazia = GetParent(hwnd);
			EMEditTrackBar * emetb;
			emetb = GetEMEditTrackBarInstance(hFazia);
			if (emetb->allowFloat)
			{
				float a = (float)strtod(buff, &addr);
				if (addr == buff)
				{	// conversion error (to float)
				}
				else
				{
					if (a > emetb->maxFloatValue)
						a = emetb->maxFloatValue;
					if (a < emetb->minFloatValue)
						a = emetb->minFloatValue;
					lastFloatValue = a;
				}
			}
			else
			{
				int a = (int)_strtoi64(buff, &addr, 10);
				if (addr == buff)
				{	// conversion error (to int)
				}
				else
				{
					if (a > emetb->maxIntValue)
						a = emetb->maxIntValue;
					if (a < emetb->minIntValue)
						a = emetb->minIntValue;
					lastIntValue = a;
				}
			}
			SendMessage(hwnd, EM_SETSEL, 0, -1);
		}
		break;
	default:
		return CallWindowProc(OldTrackBarProc, hwnd, msg, wParam, lParam);
	}

	return CallWindowProc(OldTrackBarProc, hwnd, msg, wParam, lParam);
}


LRESULT CALLBACK EMEditTrackBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMEditTrackBar * emetb;
	emetb = GetEMEditTrackBarInstance(hwnd);
	static int lastPosX;
	static int lastBarPos;

    switch(msg)
    {
	case WM_CREATE:
		{
			EMEditTrackBar * emetb1 = new EMEditTrackBar(hwnd);
			SetEMEditTrackBarInstance(hwnd, (EMEditTrackBar *)emetb1);
			emetb1->hEdit = CreateWindow("EDIT", "", 
				WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VISIBLE | ES_RIGHT 
				| ES_MULTILINE | ES_WANTRETURN,
				1, 1, emetb1->editRectWidth-2, emetb1->height-2,
				hwnd, (HMENU)0, NULL, 0);
			SendMessage(emetb1->hEdit, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

			#ifdef _WIN64
				OldTrackBarProc = (EDITPROC) SetWindowLongPtr(emetb1->hEdit, GWLP_WNDPROC, (LONG_PTR)EMEditTrackBarEditProc) ;
			#else
				OldTrackBarProc = (EDITPROC)(HANDLE)(LONG_PTR) SetWindowLong(emetb1->hEdit, GWL_WNDPROC, (LONG)(LONG_PTR)EMEditTrackBarEditProc) ;
			#endif

			if (emetb1->allowFloat)
				emetb1->floatToEditbox(emetb1->minFloatValue);
			else
				emetb1->intToEditbox(emetb1->minIntValue);

		}
	break;
	case WM_SIZE:
		{
			emetb->resize();
		}
		break;
	case WM_CLOSE:
		{
			return TRUE;
		}
		break;
	case WM_NCDESTROY:
		{
			EMEditTrackBar * emetb1;
			emetb1 = GetEMEditTrackBarInstance(hwnd);
			delete emetb1;
			SetEMEditTrackBarInstance(hwnd, 0);
		}
	break;
	case WM_CTLCOLOREDIT:
	{
		HDC hdc1 = (HDC)wParam;
		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);
		if (!disabled)
		{
			SetTextColor(hdc1, emetb->colEditText);
			SetBkColor(hdc1, emetb->colEditBackground);
		}
		else
		{
			SetTextColor(hdc1, emetb->colDisabledEditText);
			SetBkColor(hdc1, emetb->colDisabledEditBackground);
		}
		if (disabled)
			return (INT_PTR)(emetb->hEditDisabledBrush);
		else
			return (INT_PTR)(emetb->hEditBrush);
	}
	break;
	case WM_PAINT:
	{
		if (!emetb)
			break;

		RECT rect, sRect;
		HDC hdc1, thdc;
		PAINTSTRUCT ps;
		HPEN hOldPen;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);

		// get DC and create backbuffer DC
		hdc1 = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rect);
		thdc = CreateCompatibleDC(hdc1); 
		HBITMAP hbmp = CreateCompatibleBitmap(hdc1, rect.right, rect.bottom);
		HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

		// get slider part rect
		sRect = rect;
		sRect.left = emetb->editRectWidth;

		// paint whole control
		POINT cBorder[] = {	{0, 0}, {rect.right,0}, 
							{rect.right,rect.bottom}, 
							{0,rect.bottom} };	// 4

		HRGN cfhrg = CreatePolygonRgn(cBorder, 4, WINDING);

		HBRUSH cfbr = CreateSolidBrush(emetb->colBackground);
		FillRgn(thdc, cfhrg, cfbr);
		DeleteObject(cfbr);
		DeleteObject(cfhrg);
		
		// paint path bar
		POINT pBorder1[] =	{
			{sRect.left+emetb->pathXStart, (emetb->height + emetb->pathHeight)/2},
			{sRect.left+emetb->pathXStart, (emetb->height - emetb->pathHeight)/2},
			{sRect.left+emetb->pathXEnd+1, (emetb->height - emetb->pathHeight)/2},
		};	// 3
		POINT pBorder2[] =	{
			{sRect.left+emetb->pathXEnd  , (emetb->height - emetb->pathHeight)/2+1},
			{sRect.left+emetb->pathXEnd, (emetb->height + emetb->pathHeight)/2},
			{sRect.left+emetb->pathXStart, (emetb->height + emetb->pathHeight)/2},
		};	// 3
		POINT pTPos[] = {
			{sRect.left+emetb->sliderPos+emetb->pathXStart, (emetb->height - emetb->pathHeight)/2},
			{sRect.left+emetb->sliderPos+emetb->pathXStart, (emetb->height + emetb->pathHeight)/2}
		};

		// first fill region
		POINT freg1[] = {
			{sRect.left+emetb->pathXStart, (emetb->height + emetb->pathHeight)/2},
			{sRect.left+emetb->pathXStart, (emetb->height - emetb->pathHeight)/2},
			{sRect.left+emetb->sliderPos+emetb->pathXStart, (emetb->height - emetb->pathHeight)/2},
			{sRect.left+emetb->sliderPos+emetb->pathXStart, (emetb->height + emetb->pathHeight)/2},
		};
		HRGN fhrg1 = CreatePolygonRgn(freg1, 4, WINDING);

		POINT freg2[] = {
			{sRect.left+emetb->sliderPos+emetb->pathXStart, (emetb->height + emetb->pathHeight)/2},
			{sRect.left+emetb->sliderPos+emetb->pathXStart, (emetb->height - emetb->pathHeight)/2},
			{sRect.left+emetb->pathXEnd, (emetb->height - emetb->pathHeight)/2},
			{sRect.left+emetb->pathXEnd, (emetb->height + emetb->pathHeight)/2},
		};
		HRGN fhrg2 = CreatePolygonRgn(freg2, 4, WINDING);

		if (emetb->sliderHeight>2)
		{
			if (emetb->sliderPos > 1)
			{
				HBRUSH fbr;
				if (disabled)
					fbr = CreateSolidBrush(emetb->colDisabledBackground);
				else
					fbr = CreateSolidBrush(emetb->colPathBackgroundBefore);
				FillRgn(thdc, fhrg1, fbr);
				DeleteObject(fbr);
			}

			if (emetb->sliderPos < emetb->pathLength-1)
			{
				HBRUSH fbr;
				if (disabled)
					fbr = CreateSolidBrush(emetb->colDisabledBackground);
				else
					fbr = CreateSolidBrush(emetb->colPathBackgroundAfter);
				FillRgn(thdc, fhrg2, fbr);
				DeleteObject(fbr);
			}
		}
		DeleteObject(fhrg1);
		DeleteObject(fhrg2);

		// and then draw border
		if (disabled)
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colDisabledBorderDark));
		else
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colPathBorderUpLeft));
		Polyline(thdc, pBorder1, 3);
		if (disabled)
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colDisabledBorderLight)));
		else
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colPathBorderDownRight)));
		Polyline(thdc, pBorder2, 3);
		DeleteObject(SelectObject(thdc, hOldPen));

		// paint slider
		POINT sBorder1[] = {
			{sRect.left + emetb->sliderPos - emetb->sliderWidth/2+emetb->pathXStart,   (emetb->height + emetb->sliderHeight)/2-1 },
			{sRect.left + emetb->sliderPos - emetb->sliderWidth/2+emetb->pathXStart,   (emetb->height - emetb->sliderHeight)/2 },
			{sRect.left + emetb->sliderPos + emetb->sliderWidth/2+emetb->pathXStart,   (emetb->height - emetb->sliderHeight)/2 },
		};

		POINT sBorder2[] = {
			{sRect.left + emetb->sliderPos + emetb->sliderWidth/2+emetb->pathXStart-1,   (emetb->height - emetb->sliderHeight)/2+1},
			{sRect.left + emetb->sliderPos + emetb->sliderWidth/2+emetb->pathXStart-1,   (emetb->height + emetb->sliderHeight)/2-1 },
			{sRect.left + emetb->sliderPos - emetb->sliderWidth/2+emetb->pathXStart  ,   (emetb->height + emetb->sliderHeight)/2-1 },
		};


		// first fill region
		POINT freg[] = {
			{sRect.left + emetb->sliderPos - emetb->sliderWidth/2+emetb->pathXStart,   (emetb->height + emetb->sliderHeight)/2 },
			{sRect.left + emetb->sliderPos - emetb->sliderWidth/2+emetb->pathXStart,   (emetb->height - emetb->sliderHeight)/2 },
			{sRect.left + emetb->sliderPos + emetb->sliderWidth/2+emetb->pathXStart,   (emetb->height - emetb->sliderHeight)/2 },
			{sRect.left + emetb->sliderPos + emetb->sliderWidth/2+emetb->pathXStart,   (emetb->height + emetb->sliderHeight)/2 }
		};
		HRGN fhrg = CreatePolygonRgn(freg, 4, WINDING);

		if (emetb->sliderHeight>2 && emetb->sliderWidth>2)
		{
			HBRUSH fbr;
			if (disabled)
				fbr = CreateSolidBrush(emetb->colDisabledBackground);
			else
				if (emetb->nowScrolling)
					fbr = CreateSolidBrush(emetb->colSliderBackgroundClicked);
				else
					fbr = CreateSolidBrush(emetb->colSliderBackground);

			FillRgn(thdc, fhrg, fbr);
			DeleteObject(fbr);
		}
		DeleteObject(fhrg);

		// and then draw border
		if (disabled)
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colDisabledBorderLight));
		else
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colSliderBorderUpLeft));
		Polyline(thdc, sBorder1, 3);
		if (disabled)
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colDisabledBorderDark)));
		else
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colSliderBorderDownRight)));
		Polyline(thdc, sBorder2, 3);
		DeleteObject(SelectObject(thdc, hOldPen));

		// draw editbox border
		POINT eBorder1[] = {
			{0,   emetb->height-1 },
			{0,   0},
			{emetb->editRectWidth,   0}
		};

		POINT eBorder2[] = {
			{emetb->editRectWidth-1,   1},
			{emetb->editRectWidth-1,   emetb->height-1},
			{0, emetb->height-1}
		};

		hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colEditBorderUpLeft));
		Polyline(thdc, eBorder1, 3);
		DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emetb->colEditBorderDownRight)));
		Polyline(thdc, eBorder2, 3);
		DeleteObject(SelectObject(thdc, hOldPen));

		// copy from back buffer and free resources
		BitBlt(hdc1, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
		SelectObject(thdc, oldBMP);
		DeleteObject(hbmp);
		DeleteDC(thdc);
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_MOUSEMOVE:
	{
		if (!GetCapture())
			SetCapture(hwnd);
		if (GetCapture() == hwnd)
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			rect.left = emetb->editRectWidth;
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (PtInRect(&rect, pt)  || emetb->nowScrolling)
			{
				if (emetb->nowScrolling)
				{
					emetb->sliderPos = lastBarPos + (short)LOWORD(lParam) - lastPosX;
					if (emetb->sliderPos > emetb->pathLength)
						emetb->sliderPos = emetb->pathLength;
					if (emetb->sliderPos < 0)
						emetb->sliderPos = 0;
					float newVal;
					if (emetb->allowFloat)
					{
						newVal = emetb->sliderPos / (float) emetb->pathLength * (emetb->maxFloatValue-emetb->minFloatValue) + emetb->minFloatValue;
						emetb->floatToEditbox(newVal);
						emetb->floatValue = newVal;
					}
					else
					{
						newVal = emetb->sliderPos / (float) emetb->pathLength * (emetb->maxIntValue-emetb->minIntValue) + emetb->minIntValue;
						int iVal = (int)(newVal+0.4f);
						emetb->sliderPos = (int)((iVal-emetb->minIntValue)/(float)(emetb->maxIntValue-emetb->minIntValue)*emetb->pathLength);
						emetb->intToEditbox(iVal);
						emetb->intValue = iVal;
					}
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, FALSE);
					emetb->notifyParent();
				}
			}
			else
			{
				ReleaseCapture();
			}
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		RECT rect;
		GetClientRect(hwnd, &rect);
		rect.left = emetb->editRectWidth;
		POINT pt = { LOWORD(lParam), HIWORD(lParam) };
		if (PtInRect(&rect, pt))
		{
			SetFocus(hwnd);
			RECT rPath, rBar;
			rPath.left   = emetb->editRectWidth + emetb->pathXStart;
			rPath.right  = emetb->editRectWidth + emetb->pathXEnd+1;
			rPath.top    = (emetb->height - emetb->pathHeight)/2;
			rPath.bottom = (emetb->height + emetb->pathHeight)/2+1;

			rBar.left   = emetb->editRectWidth + emetb->sliderPos - emetb->sliderWidth/2+emetb->pathXStart;
			rBar.right  = emetb->editRectWidth + emetb->sliderPos + emetb->sliderWidth/2+emetb->pathXStart;
			rBar.top    = (emetb->height - emetb->sliderHeight)/2;
			rBar.bottom = (emetb->height + emetb->sliderHeight)/2;

			emetb->nowChanged = false;
			emetb->nowScrolling = false;

			if (PtInRect(&rPath, pt))
				emetb->nowChanged = true;

			if (PtInRect(&rBar, pt))
			{
				emetb->nowChanged = false;
				emetb->nowScrolling = true;
			}

			if (emetb->nowChanged)
			{
				if (pt.x > emetb->editRectWidth + emetb->sliderPos + emetb->pathXStart)
				{	// scroll right
					if (emetb->allowFloat)
						emetb->floatValue += emetb->floatFastValue;
					else
						emetb->intValue += emetb->intFastValue;
				}
				else
				{	// scroll left
					if (emetb->allowFloat)
						emetb->floatValue -= emetb->floatFastValue;
					else
						emetb->intValue -= emetb->intFastValue;
				}

				if (emetb->allowFloat)
				{
					if (emetb->floatValue > emetb->maxFloatValue)
						emetb->floatValue = emetb->maxFloatValue;
					if (emetb->floatValue < emetb->minFloatValue)
						emetb->floatValue = emetb->minFloatValue;
					emetb->floatToEditbox(emetb->floatValue);
					emetb->sliderPos = (int)((emetb->floatValue-emetb->minFloatValue)/(float)(emetb->maxFloatValue-emetb->minFloatValue)*emetb->pathLength);
				}
				else
				{
					if (emetb->intValue > emetb->maxIntValue)
						emetb->intValue = emetb->maxIntValue;
					if (emetb->intValue < emetb->minIntValue)
						emetb->intValue = emetb->minIntValue;
					emetb->intToEditbox(emetb->intValue);
					emetb->sliderPos = (int)((emetb->intValue-emetb->minIntValue)/(float)(emetb->maxIntValue-emetb->minIntValue)*emetb->pathLength);
				}

				if (emetb->sliderPos > emetb->pathLength)
					emetb->sliderPos = emetb->pathLength;
				if (emetb->sliderPos < 0)
					emetb->sliderPos = 0;

				SendMessage(emetb->hEdit, 65001, (WPARAM&)emetb->intValue, (LPARAM&)emetb->floatValue);
				emetb->notifyParent();
			}

			if (emetb->nowScrolling)
			{
				lastBarPos = emetb->sliderPos;
				lastPosX = pt.x;
				emetb->notifyParent();
			}
			RECT rect;
			GetClientRect(hwnd, &rect);
			InvalidateRect(hwnd, &rect, FALSE);

			
		}

	}
	break;
	case WM_LBUTTONUP:
	{
		emetb->nowChanged = false;
		emetb->nowScrolling = false;
		if (GetCapture() == hwnd)
		{
			ReleaseCapture();
		}
		RECT rect;
		GetClientRect(hwnd, &rect);
		InvalidateRect(hwnd, &rect, FALSE);
	}
	break;
	case WM_SETFOCUS:
		{
			SetFocus(emetb->hEdit);
		}
		break;
	case WM_COMMAND:
	{
		int wmId, wmEvent;
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		
		LONG editID = GetWindowLong(emetb->hEdit, GWL_ID);
		if (wmId == editID  &&   wmEvent == ES_RETURN)
		{
		}
		if (wmId == editID  &&   wmEvent == ES_TAB)
		{
			HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, false);
			if (hNext != hwnd)
				SetFocus(hNext);
		}
		if (wmId == editID  &&   wmEvent == ES_TAB_SHIFT)
		{
			HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, true);
			if (hNext != hwnd)
				SetFocus(hNext);
		}
	}
	break;

    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMEditTrackBar::floatToEditbox(float value)
{
	char eValue[64];
	char temp[64];
	sprintf_s(temp, 64, "%%.%df", floatAfterDot);
	sprintf_s(eValue, 64, temp, value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}

void EMEditTrackBar::intToEditbox(int value)
{
	char eValue[64];
	sprintf_s(eValue, 64, "%d", value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}

void EMEditTrackBar::setEditBoxWidth(int w)
{
	editRectWidth = w;
	SetWindowPos(hEdit, HWND_TOP, 1, 1, editRectWidth-2, height-2, 0);
	resize();
}

void EMEditTrackBar::setValuesToInt(int value, int minVal, int maxVal, int fastValue)
{
	minIntValue = minVal;
	maxIntValue = maxVal;
	if (value > maxIntValue)
		value = maxIntValue;
	if (value < minIntValue)
		value = minIntValue;
	intValue = value;

	allowFloat = false;
	intToEditbox(value);

	sliderPos = (int)((value - minIntValue)/(float)(maxIntValue - minIntValue)*pathLength);
	if (sliderPos > pathLength)
		sliderPos = pathLength;
	if (sliderPos < 0)
		sliderPos = 0;
	intFastValue = fastValue;
	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

void EMEditTrackBar::setValuesToFloat(float value, float minVal, float maxVal, float fastValue)
{
	minFloatValue = minVal;
	maxFloatValue = maxVal;
	if (value > maxFloatValue)
		value = maxFloatValue;
	if (value < minFloatValue)
		value = minFloatValue;
	floatValue = value;

	allowFloat = true;
	floatToEditbox(value);

	sliderPos = (int)((value - minFloatValue)/(float)(maxFloatValue - minFloatValue)*pathLength);
	if (sliderPos > pathLength)
		sliderPos = pathLength;
	if (sliderPos < 0)
		sliderPos = 0;
	floatFastValue = fastValue;
	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

void EMEditTrackBar::notifyParent()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_HSCROLL), (LPARAM)hwnd);
}
