#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>

extern HMODULE hDllModule;
extern ThemeManager gTheme;
extern HBITMAP gIcons;

bool drawPreviewBokeh(ImageByteBuffer * img, FinalModifier * fMod, float depth);
bool evalChromaticBokehSizes(float shiftLens, float shiftDepth, float achromatic, float depth, float &red, float &green, float &blue);
bool getAllFinalGUIValues(HWND hWnd);
bool redrawFakeDofOnThread(HWND hImg);
bool reevaluateDepthMapOnThread();

bool scaleAperture = false;
bool scaleDepth = false;
float prevDist = 1.0f;


INT_PTR CALLBACK RendererMainWindow::FinalWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_FINAL_PREVIEW));

	switch (message)
	{
		case WM_INITDIALOG:
			{
				hFinal = hWnd;

				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP1)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP2)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP3)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP4)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP5)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP6)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP7)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP8)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP9)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP10)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP11)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP12)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_FINAL_GROUP13)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ON)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOCUS_DIST)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_QUALITY)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GRAIN)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_LOAD)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_SAVE)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_RESET_ALL_FINAL)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_REDRAW)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_DRAW_ZMAP)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_REEVAL_DEPTH)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_RESET_ALL)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_NUM_THREADS)));
				theme.apply(GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ALGORITHM)));

				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_MODE_FOCUS)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SHOW_DOF_RANGE)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SCALE_DISTANCE)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SCALE_APERTURE)));


				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_APERTURE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_NUMBER)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ANGLE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_RADIUS)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_BALANCE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_SIZE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_FLATTEN)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_VIGNETTE)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ROUND)));

				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_LENS)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_DEPTH)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_ACHROMATIC)));

				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_RED)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_GREEN)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_BLUE)));

				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_POWER)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_AREA)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_THRESHOLD)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_POWER)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_DISPERSION)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_THRESHOLD)));
				theme.apply(GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_AREA)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ON)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_ON)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_ON)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_SHAPE_ON)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_OBSTACLE_ON)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_CHANGE_SHAPE)));
				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_CHANGE_OBSTACLE)));
				theme.apply(GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_COLOR)));
				theme.apply(GetEMImgButtonInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_CHANGE_COLOR)));

				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_RADIUS)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_THRESHOLD)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_DENSITY)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_ENABLED)));

				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd,  IDC_FINAL_FOG_DISTANCE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd,  IDC_FINAL_FOG_SPEED)));
				theme.apply(GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_COLOR)));
				theme.apply(GetEMImgButtonInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_COLOR_CHANGE)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd,  IDC_FINAL_FOG_EXCLUDE_SKY)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd,  IDC_FINAL_FOG_ENABLED)));

				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd,  IDC_FINAL_AERIAL_ENABLED)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd,  IDC_FINAL_AERIAL_SCALE)));

				EMEditSpin * emFocusDist = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOCUS_DIST));
				EMEditSpin * emAperture = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_APERTURE));
				EMEditSpin * emBladesNum = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_NUMBER));
				EMEditSpin * emBladesAngle = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ANGLE));
				EMEditSpin * emBladesRadius	= GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_RADIUS));
				EMEditSpin * emBokehRingBalance = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_BALANCE));
				EMEditSpin * emBokehRingSize = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_SIZE));
				EMEditSpin * emBokehFlatten	= GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_FLATTEN));
				EMEditSpin * emBokehVignette = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_VIGNETTE));
				EMCheckBox * emDOFon = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ON));
				EMCheckBox * emBladesRound = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ROUND));
				EMEditTrackBar * emchrShiftLens = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_LENS));
				EMEditTrackBar * emchrShiftDepth = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_DEPTH));
				EMEditTrackBar * emchrAchromatic = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_ACHROMATIC));
				EMEditTrackBar * emchrPlRed = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_RED));
				EMEditTrackBar * emchrPlGreen = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_GREEN));
				EMEditTrackBar * emchrPlBlue = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_BLUE));

				emFocusDist->setValuesToFloat(		3.0f,	NOX_FINAL_FOCUS_MIN,			NOX_FINAL_FOCUS_MAX,			0.1f,	0.01f);
				emAperture->setValuesToFloat(		1.0f,	NOX_FINAL_APERTURE_MIN,			NOX_FINAL_APERTURE_MAX,			0.1f,	0.01f);
				emBladesNum->setValuesToInt(		5,		NOX_FINAL_BLADES_NUM_MIN,		NOX_FINAL_BLADES_NUM_MAX,		1,		0.01f);
				emBladesAngle->setValuesToFloat(	0.0f,	NOX_FINAL_BLADES_ANGLE_MIN,		NOX_FINAL_BLADES_ANGLE_MAX,		5.0f,	0.5f);
				emBladesRadius->setValuesToFloat(	1.0f,	NOX_FINAL_BLADES_RADIUS_MIN,	NOX_FINAL_BLADES_RADIUS_MAX,	0.1f,	0.01f);
				emBokehRingBalance->setValuesToInt(	0,		NOX_FINAL_BOKEH_BALANCE_MIN,	NOX_FINAL_BOKEH_BALANCE_MAX,	1,		0.2f);
				emBokehRingSize->setValuesToInt(	20,		NOX_FINAL_BOKEH_RING_SIZE_MIN,	NOX_FINAL_BOKEH_RING_SIZE_MAX,	1,		0.2f);
				emBokehFlatten->setValuesToInt(		0,		NOX_FINAL_BOKEH_FLATTEN_MIN,	NOX_FINAL_BOKEH_FLATTEN_MAX,	1,		0.02f);
				emBokehVignette->setValuesToInt(	0,		NOX_FINAL_BOKEH_VIGNETTE_MIN,	NOX_FINAL_BOKEH_VIGNETTE_MAX,	1,		0.02f);


				EMEditSpin * emQuality = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_QUALITY));
				EMEditTrackBar * emGrain = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GRAIN));
				emQuality->setValuesToInt(6, NOX_FINAL_QUALITY_MIN, NOX_FINAL_QUALITY_MAX, 1, 0.05f);
				emGrain->setEditBoxWidth(32);
				emGrain->setValuesToFloat(0.0f, NOX_FINAL_GRAIN_MIN, NOX_FINAL_GRAIN_MAX, 0.1f);
				emchrShiftLens->setEditBoxWidth(32);
				emchrShiftDepth->setEditBoxWidth(32);
				emchrAchromatic->setEditBoxWidth(32);
				emchrPlRed->setEditBoxWidth(32);
				emchrPlGreen->setEditBoxWidth(32);
				emchrPlBlue->setEditBoxWidth(32);
				emchrShiftLens->setValuesToFloat(	0.0f, NOX_FINAL_CHR_SHIFT_LENS_MIN,		NOX_FINAL_CHR_SHIFT_LENS_MAX,	0.1f);
				emchrShiftDepth->setValuesToFloat(	0.0f, NOX_FINAL_CHR_SHIFT_DEPTH_MIN,	NOX_FINAL_CHR_SHIFT_DEPTH_MAX,	0.1f);
				emchrAchromatic->setValuesToFloat(	0.0f, NOX_FINAL_CHR_ACHROMATIC_MIN,		NOX_FINAL_CHR_ACHROMATIC_MAX,	0.1f);
				emchrPlRed->setValuesToFloat(		0.0f, NOX_FINAL_CHR_PL_RED_MIN,			NOX_FINAL_CHR_PL_RED_MAX,		0.1f);
				emchrPlGreen->setValuesToFloat(		0.0f, NOX_FINAL_CHR_PL_GREEN_MIN,		NOX_FINAL_CHR_PL_GREEN_MAX,		0.1f);
				emchrPlBlue->setValuesToFloat(		0.0f, NOX_FINAL_CHR_PL_BLUE_MIN,		NOX_FINAL_CHR_PL_BLUE_MAX,		0.1f);

				EMEditSpin * emNumThreads = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_NUM_THREADS));
				EMComboBox * emAlgorithm = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ALGORITHM));
				int numThr = ThreadManager::getProcessorsNumber();
				emNumThreads->setValuesToInt(numThr, 1, 16, 1, 0.1f);
				emAlgorithm->deleteItems();
				emAlgorithm->addItem("Algorithm 1");
				emAlgorithm->addItem("Algorithm 2");
				emAlgorithm->addItem("Algorithm 3");
				emAlgorithm->selected = 0;
				InvalidateRect(emAlgorithm->hwnd, NULL, false);

				EMEditTrackBar * emBlPower = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_POWER));
				EMEditTrackBar * emBlArea  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_AREA));
				EMEditTrackBar * emBlThr   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_THRESHOLD));
				EMEditTrackBar * emGlPower = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_POWER));
				EMEditTrackBar * emGlDisp  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_DISPERSION));
				EMEditTrackBar * emGlThr   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_THRESHOLD));
				EMEditTrackBar * emGlArea  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_AREA));
				emBlPower->setEditBoxWidth(32);
				emBlArea->setEditBoxWidth(32);
				emBlThr->setEditBoxWidth(32);
				emGlPower->setEditBoxWidth(32);
				emGlDisp->setEditBoxWidth(32);
				emGlThr->setEditBoxWidth(32);
				emGlArea->setEditBoxWidth(32);
				emBlPower->setValuesToFloat(0.0f,	NOX_FINAL_BLOOM_POWER_MIN,	NOX_FINAL_BLOOM_POWER_MAX,	0.5f);
				emBlArea->setValuesToInt(	192,	NOX_FINAL_BLOOM_AREA_MIN,	NOX_FINAL_BLOOM_AREA_MAX,	10);
				emBlThr->setValuesToFloat(	2.0f,	NOX_FINAL_BLOOM_THRES_MIN,	NOX_FINAL_BLOOM_THRES_MAX,	0.5f);
				emGlPower->setValuesToFloat(0.0f,	NOX_FINAL_GLARE_POWER_MIN,	NOX_FINAL_GLARE_POWER_MAX,	0.5f);
				emGlDisp->setValuesToFloat(	0.0f,	NOX_FINAL_GLARE_DISP_MIN,	NOX_FINAL_GLARE_DISP_MAX,	0.5f);
				emGlThr->setValuesToFloat(	2.0f,	NOX_FINAL_GLARE_THRES_MIN,	NOX_FINAL_GLARE_THRES_MAX,	0.5f);
				emGlArea->setValuesToInt(	128,	NOX_FINAL_GLARE_AREA_MIN,	NOX_FINAL_GLARE_AREA_MAX,	32);

				EMColorShow * emBlColor = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_COLOR));
				EMImgButton * emBlChngCol = GetEMImgButtonInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_CHANGE_COLOR));
				emBlColor->color = Color4(1,1,1);
				emBlChngCol->bmAll = &(gIcons);
				emBlChngCol->pNx = emBlChngCol->pMx = emBlChngCol->pCx = emBlChngCol->pDx = 868;
				emBlChngCol->pNy = 4;
				emBlChngCol->pMy = 36;
				emBlChngCol->pCy = 68;
				emBlChngCol->pDy = 100;
				InvalidateRect(emBlColor->hwnd, NULL, false);
				InvalidateRect(emBlChngCol->hwnd, NULL, false);


				EMColorShow * emFogColor = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_COLOR));
				EMImgButton * emFogChngCol = GetEMImgButtonInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_COLOR_CHANGE));
				emFogColor->color = Color4(0.7f, 0.7f, 0.7f);
				emFogChngCol->bmAll = &(gIcons);
				emFogChngCol->pNx = emFogChngCol->pMx = emFogChngCol->pCx = emFogChngCol->pDx = 868;
				emFogChngCol->pNy = 4;
				emFogChngCol->pMy = 36;
				emFogChngCol->pCy = 68;
				emFogChngCol->pDy = 100;
				InvalidateRect(emFogColor->hwnd, NULL, false);
				InvalidateRect(emFogChngCol->hwnd, NULL, false);


				EMEditSpin * emDotsRadius =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_RADIUS));
				EMEditSpin * emDotsThreshold =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_THRESHOLD));
				EMEditSpin * emDotsDensity =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_DENSITY));
				EMCheckBox * emDotsEnabled =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_ENABLED));
				emDotsEnabled->selected = false;
				InvalidateRect(emDotsEnabled->hwnd, NULL, false);
				emDotsRadius->setValuesToInt(2, NOX_FINAL_DOTS_RADIUS_MIN, NOX_FINAL_DOTS_RADIUS_MAX, 1, 0.1f);
				emDotsThreshold->setValuesToFloat(10, NOX_FINAL_DOTS_THRESHOLD_MIN, NOX_FINAL_DOTS_THRESHOLD_MAX, 1, 0.1f);
				emDotsDensity->setValuesToInt(1, NOX_FINAL_DOTS_DENSITY_MIN, 4, 1, 0.1f);

				HWND hPrev = GetDlgItem(hWnd, IDC_FINAL_PREVIEW);
				SetWindowPos(hPrev, HWND_TOP, 0,0, 192,192, SWP_NOMOVE | SWP_NOZORDER);
				EMPView * empv = GetEMPViewInstance(hPrev);
				if (!empv->byteBuff)
					empv->byteBuff = new ImageByteBuffer();
				empv->byteBuff->allocBuffer(192,192);
				empv->byteBuff->clearBuffer();
				InvalidateRect(hPrev, NULL, false);

			}
			break;
		case EMT_TAB_CLOSED:
			{
				EMPView * empvmain = GetEMPViewInstance(hImage);
				if (empvmain->mode == EMPView::MODE_PICK)
				{
					empvmain->mode = EMPView::MODE_SHOW;
					EMCheckBox * emMode = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_MODE_FOCUS));
					emMode->selected = false;
				}
			}
			break;
		case PV_PIXEL_PICKED:
			{
				if (rtr->curScenePtr->nowRendering)
					break;
				EMCheckBox * emMode = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_MODE_FOCUS));
				EMCheckBox * emRange = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SHOW_DOF_RANGE));
				float x = (float&)wParam;
				float y = (float&)lParam;
				CHECK(cam);
				if (emMode->selected)
				{
					EMPView * empvmain = GetEMPViewInstance(hImage);
					if (x>=cam->imgBuff->width || x<0)
						break;
					if (y>=cam->imgBuff->height|| y<0)
						break;

					int ix = (int)x*cam->aa;
					int iy = (int)y*cam->aa;
					int hits = cam->depthBuf->hbuf[iy][ix];
					if (hits <= 0)
						break;

					float focusD = cam->depthBuf->fbuf[iy][ix] / hits;

					EMEditSpin * emFocusDist = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOCUS_DIST));
					emFocusDist->setValuesToFloat(focusD, NOX_FINAL_FOCUS_MIN, NOX_FINAL_FOCUS_MAX, 0.1f, 0.01f);

					if (emRange->selected)
					{
						float lng = 0.5f * 0.036f / tan(cam->angle * PI / 360.0f);
						cam->depthBuf->copyAsDOFTemperatureToByteBuffer(empvmain->byteBuff, focusD, cam->aa, lng, cam->fMod.aperture);
					}
				}

			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_FINAL_REDRAW:
						{
							if (rtr->curScenePtr->nowRendering)
							{
								MessageBox(0, "To finalize image, stop renderer first.", "", 0);
								break;
							}
							
							if (!cam)
								break;

							getAllFinalGUIValues(hWnd);

							redrawFakeDofOnThread(hImage);
						}
						break;
					case IDC_FINAL_LOAD:
					{
						if (!cam)
							break;
						loadFinalPost();
					}
					break;
					case IDC_FINAL_SAVE:
					{
						if (!cam)
							break;
						saveFinalPost();
					}
					break;
					case IDC_FINAL_RESET_ALL_FINAL:
					{
						if (!cam)
							break;
						int dRes = MessageBox(hWnd, "All final post settings will be lost.\nAre you sure?", "Warning.", MB_YESNO);
						if (dRes==IDYES)
						{
							FinalModifier fmod;
							cam->fMod = fmod;
							cam->fMod.focusDistance = cam->focusDist;
							cam->fMod.numThreads = ThreadManager::getProcessorsNumber();
							fillFinalSettings(cam);
						}
					}
					break;
					case IDC_FINAL_DRAW_ZMAP:
					{
						if (!cam)
							break;
						if (!cam->depthBuf->fbuf)
							break;

						EMPView * empv = GetEMPViewInstance(hImage);

						cam->depthBuf->copyAsDepthToByteBuffer(empv->byteBuff, cam->aa);
						InvalidateRect(hImage, NULL, false);
					}
					break;
					case IDC_FINAL_REEVAL_DEPTH:
					{
						reevaluateDepthMapOnThread();
					}
					break;
					case IDC_FINAL_BLADES_ROUND:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_APERTURE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);

							EMCheckBox * emcd = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SCALE_APERTURE));
							if (emcd->selected)
							{
								drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
								InvalidateRect(empv->hwnd, NULL, false);
							}

							if (rtr->curScenePtr->nowRendering)
								break;
							EMCheckBox * emRange = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SHOW_DOF_RANGE));
							if (!emRange->selected)
								break;
							if (!cam->depthBuf  ||  !cam->depthBuf->fbuf)
								break;

							float dist = cam->fMod.focusDistance;
							cam->fMod.focal = 0.5f * 0.036f / tan(cam->angle * PI / 360.0f);

							EMPView * empvmain = GetEMPViewInstance(hImage);
							cam->depthBuf->copyAsDOFTemperatureToByteBuffer(empvmain->byteBuff, dist, cam->aa, cam->fMod.focal, cam->fMod.aperture);
							InvalidateRect(hImage, NULL, false);

						}
						break;
					case IDC_FINAL_BLADES_NUMBER:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_BLADES_ANGLE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_BLADES_RADIUS:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_BOKEH_RING_BALANCE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_BOKEH_RING_SIZE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_BOKEH_FLATTEN:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_BOKEH_VIGNETTE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_RESET_ALL:
						{
							if (wmEvent!=BN_CLICKED)
								break;
							if (!cam)
								break;

							cam->resetFinalValues();
							cam->glareOK = false;
							fillFinalSettings(cam);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);

						}
						break;
					case IDC_FINAL_FOCUS_DIST:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							if (rtr->curScenePtr->nowRendering)
								break;

							EMCheckBox * emRange = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SHOW_DOF_RANGE));
							if (!emRange->selected)
								break;

							if (!cam)
								break;
							if (!cam->depthBuf  ||  !cam->depthBuf->fbuf)
								break;

							EMEditSpin * emFocusDist = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOCUS_DIST));
							float dist = emFocusDist->floatValue;

							EMPView * empvmain = GetEMPViewInstance(hImage);
							float lng = 0.5f * 0.036f / tan(cam->angle * PI / 360.0f);
							cam->depthBuf->copyAsDOFTemperatureToByteBuffer(empvmain->byteBuff, dist, cam->aa, lng, cam->fMod.aperture);
							InvalidateRect(hImage, NULL, false);

						}
						break;
					case IDC_FINAL_QUALITY:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							if (rtr->curScenePtr->nowRendering)
								break;

							if (!cam)
								break;

							EMEditSpin * emQ = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_QUALITY));
							cam->fMod.quality = emQ->intValue;

						}
						break;
					case IDC_FINAL_NUM_THREADS:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							if (!cam)
								break;
							EMEditSpin * emThr = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_NUM_THREADS));
							cam->fMod.numThreads = emThr->intValue;
						}
						break;
					case IDC_FINAL_DOF_ALGORITHM:
						{
							if (wmEvent!=CBN_SELCHANGE)
								break;
							if (!cam)
								break;
							EMComboBox * emAlg = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ALGORITHM));
							cam->fMod.algorithm = emAlg->selected;
						}
						break;
					case IDC_FINAL_SHOW_DOF_RANGE:
						{
							if (wmEvent != BN_CLICKED)
								break;
						}
						break;
					case IDC_FINAL_MODE_FOCUS:
						{
							if (wmEvent != BN_CLICKED)
								break;

							EMPView * empvmain = GetEMPViewInstance(hImage);
							EMCheckBox * emMode = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_MODE_FOCUS));

							if (emMode->selected)
								empvmain->mode = EMPView::MODE_PICK;
							else
								empvmain->mode = EMPView::MODE_SHOW;

						}
						break;

					case IDC_FINAL_CHRABB_SHIFT_LENS:
						{
							if (wmEvent != WM_HSCROLL)
								break;
							EMAberration * emab = GetEMAberrationInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SCHEME));
							EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_LENS));
							emab->abbShiftXY = emtr->floatValue;
							prevDist = emab->distPattern;
							InvalidateRect(emab->hwnd, NULL, false);

							getAllFinalGUIValues(hWnd);
							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_FINAL_PREVIEW));
							CHECK(cam);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_CHRABB_SHIFT_DEPTH:
						{
							if (wmEvent != WM_HSCROLL)
								break;
							EMAberration * emab = GetEMAberrationInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SCHEME));
							EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_DEPTH));
							emab->abbShiftZ = emtr->floatValue;
							prevDist = emab->distPattern;
							InvalidateRect(emab->hwnd, NULL, false);

							getAllFinalGUIValues(hWnd);
							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_FINAL_PREVIEW));
							CHECK(cam);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_CHRABB_ACHROMATIC:
						{
							if (wmEvent != WM_HSCROLL)
								break;
							EMAberration * emab = GetEMAberrationInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SCHEME));
							EMEditTrackBar * emtr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_ACHROMATIC));
							emab->abbAchromatic = emtr->floatValue;
							prevDist = emab->distPattern;
							InvalidateRect(emab->hwnd, NULL, false);

							getAllFinalGUIValues(hWnd);
							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_FINAL_PREVIEW));
							CHECK(cam);
							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);

						}
						break;
					case IDC_FINAL_CHRABBPL_RED:
						{
							if (wmEvent!=WM_HSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_CHRABBPL_GREEN:
						{
							if (wmEvent!=WM_HSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_CHRABBPL_BLUE:
						{
							if (wmEvent!=WM_HSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_CHRABB_SCHEME:
						{
							if (wmEvent != EMA_DIST_CHANGED)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);

							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_SCALE_APERTURE:
						{
							if (wmEvent != BN_CLICKED)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);

							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_SCALE_DISTANCE:
						{
							if (wmEvent != BN_CLICKED)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);

							drawPreviewBokeh(empv->byteBuff, &(cam->fMod), prevDist);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_FINAL_DOTS_ENABLED:
						{
							if (wmEvent != BN_CLICKED)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_DOTS_RADIUS:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							EMEditSpin * emDotsRadius =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_RADIUS));
							EMEditSpin * emDotsDensity =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_DENSITY));
							int a = emDotsRadius->intValue;
							emDotsDensity->setValuesToInt(emDotsDensity->intValue, NOX_FINAL_DOTS_DENSITY_MIN, a*a, 1, 0.1f);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_DOTS_THRESHOLD:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_DOTS_DENSITY:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GRAIN:
						{
							if (wmEvent!=WM_HSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_BLOOM_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_BLOOM_POWER:
						{
							CHECK(wmEvent==WM_HSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_BLOOM_AREA:
						{
							CHECK(wmEvent==WM_HSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_BLOOM_THRESHOLD:
						{
							CHECK(wmEvent==WM_HSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_BLOOM_ATTENUATION_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_POWER:
						{
							CHECK(wmEvent==WM_HSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_DISPERSION:
						{
							CHECK(wmEvent==WM_HSCROLL);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_THRESHOLD:
						{
							CHECK(wmEvent==WM_HSCROLL);
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_AREA:
						{
							CHECK(wmEvent==WM_HSCROLL);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_SHAPE_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_OBSTACLE_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							cam->glareOK = false;
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_GLARE_CHANGE_SHAPE:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);

							Texture *t = new Texture();
							if (cam->texDiaphragm.isValid())
								t->copyFromOther(&cam->texDiaphragm);
							Texture *tex = (Texture *)DialogBoxParam(hModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc, (LPARAM)(t));
							if (tex)
							{
								bool texOK = tex->isValid();
								cam->texDiaphragm.copyFromOther(tex);
								tex->freeAllBuffers();
								delete tex;
								cam->glareOK = false;
								EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_SHAPE_ON));
								emc->selected = texOK;
								InvalidateRect(emc->hwnd, NULL, false);
								cam->fMod.glareTextureApertureOn = texOK;
								EMButton * emb = GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_CHANGE_SHAPE));
								emb->selected = texOK;
								InvalidateRect(emb->hwnd, NULL, false);
							}
						}
						break;
					case IDC_FINAL_GLARE_CHANGE_OBSTACLE:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);

							Texture *t = new Texture();
							if (cam->texObstacle.isValid())
								t->copyFromOther(&cam->texObstacle);
							Texture *tex = (Texture *)DialogBoxParam(hModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc, (LPARAM)(t));
							if (tex)
							{
								bool texOK = tex->isValid();
								cam->texObstacle.copyFromOther(tex);
								tex->freeAllBuffers();
								delete tex;
								cam->glareOK = false;
								EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_OBSTACLE_ON));
								emc->selected = texOK;
								InvalidateRect(emc->hwnd, NULL, false);
								cam->fMod.glareTextureObstacleOn = texOK;
								EMButton * emb = GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_CHANGE_OBSTACLE));
								emb->selected = texOK;
								InvalidateRect(emb->hwnd, NULL, false);
							}
						}
						break;
					case IDC_FINAL_BLOOM_ATTENUATION_CHANGE_COLOR:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							EMColorShow * emcolor = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_COLOR));
							Color4 col = cam->fMod.bloomAttenuationColor;
							Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
													MAKEINTRESOURCE(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &col ));
							if (res)
							{
								cam->fMod.bloomAttenuationColor = (*res);
								emcolor->redraw(*res);
							}
						}
						break;
					case IDC_FINAL_FOG_COLOR_CHANGE:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							EMColorShow * emcolor = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_COLOR));
							Color4 col = cam->fMod.fogColor;
							Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
													MAKEINTRESOURCE(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &col ));
							if (res)
							{
								cam->fMod.fogColor = (*res);
								emcolor->redraw(*res);
							}
						}
						break;
					case IDC_FINAL_FOG_DISTANCE:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_FOG_SPEED:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_FOG_EXCLUDE_SKY:
						{
							if (wmEvent!=BN_CLICKED)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_FOG_ENABLED:
						{
							if (wmEvent!=BN_CLICKED)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_AERIAL_ENABLED:
						{
							if (wmEvent!=BN_CLICKED)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;
					case IDC_FINAL_AERIAL_SCALE:
						{
							if (wmEvent!=WM_VSCROLL)
								break;
							CHECK(cam);
							getAllFinalGUIValues(hWnd);
						}
						break;


				}	// end WM_COMMAND
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}


void RendererMainWindow::fillFinalSettings(Camera * cam)
{
	if (!cam)
		return;

	HWND hWnd = hFinal;
	EMEditSpin * emFocusDist =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOCUS_DIST));
	EMEditSpin * emAperture =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_APERTURE));
	EMEditSpin * emBladesNum =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_NUMBER));
	EMEditSpin * emBladesAngle =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ANGLE));
	EMEditSpin * emBladesRadius	=		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_RADIUS));
	EMEditSpin * emBokehRingBalance =	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_BALANCE));
	EMEditSpin * emBokehRingSize =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_SIZE));
	EMEditSpin * emBokehFlatten	=		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_FLATTEN));
	EMEditSpin * emBokehVignette =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_VIGNETTE));
	EMEditSpin * emQuality =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_QUALITY));
	EMCheckBox * emDOFon =				GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ON));
	EMCheckBox * emBladesRound =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ROUND));
	EMEditTrackBar * emShiftLens =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_LENS));
	EMEditTrackBar * emShiftDepth =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_DEPTH));
	EMEditTrackBar * emAchromatic =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_ACHROMATIC));
	EMEditTrackBar * emChrPlRed =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_RED));
	EMEditTrackBar * emChrPlGreen =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_GREEN));
	EMEditTrackBar * emChrPlBlue =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_BLUE));
	EMEditTrackBar * emGrain =			GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GRAIN));
	EMEditSpin * emThreads =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_NUM_THREADS));
	EMAberration * emab =				GetEMAberrationInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SCHEME));
	EMPView * empv =					GetEMPViewInstance(GetDlgItem(hWnd, IDC_FINAL_PREVIEW));
	EMComboBox * emAlgorithm =			GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ALGORITHM));
	EMEditSpin * emDotsRadius =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_RADIUS));
	EMEditSpin * emDotsThreshold =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_THRESHOLD));
	EMEditSpin * emDotsDensity =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_DENSITY));
	EMCheckBox * emDotsEnabled =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_ENABLED));
	EMEditTrackBar * emBloomPower =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_POWER));
	EMEditTrackBar * emBloomArea =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_AREA));
	EMEditTrackBar * emBloomThres =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_THRESHOLD));
	EMEditTrackBar * emGlarePower =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_POWER));
	EMEditTrackBar * emGlareDisp =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_DISPERSION));
	EMEditTrackBar * emGlareThres =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_THRESHOLD));
	EMEditTrackBar * emGlareArea =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_AREA));
	EMCheckBox * emBloomOn =			GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ON));
	EMCheckBox * emBloomAttenOn =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_ON));
	EMCheckBox * emGlareOn =			GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_ON));
	EMCheckBox * emGlareApertTexOn =	GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_SHAPE_ON));
	EMCheckBox * emGlareObstTexOn =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_OBSTACLE_ON));
	EMColorShow * emBloomAttCol =		GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_COLOR));
	EMColorShow * emFogCol =			GetEMColorShowInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_COLOR));
	EMCheckBox * emFogExcludeSky =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_EXCLUDE_SKY));
	EMCheckBox * emFogEnabled =			GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_ENABLED));
	EMEditSpin * emFogDist =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_DISTANCE));
	EMEditSpin * emFogSpeed =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_SPEED));
	EMEditSpin * emAerialScale =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_AERIAL_SCALE));
	EMCheckBox * emAerialEnabled =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_AERIAL_ENABLED));


	emFocusDist->setValuesToFloat(		cam->fMod.focusDistance,	NOX_FINAL_FOCUS_MIN,			NOX_FINAL_FOCUS_MAX,			0.1f,	0.01f);
	emAperture->setValuesToFloat(		cam->fMod.aperture,			NOX_FINAL_APERTURE_MIN,			NOX_FINAL_APERTURE_MAX,			0.1f,	0.01f);
	emBladesNum->setValuesToInt(		cam->fMod.bladesNum,		NOX_FINAL_BLADES_NUM_MIN,		NOX_FINAL_BLADES_NUM_MAX,		1,		0.01f);
	emBladesAngle->setValuesToFloat(	cam->fMod.bladesAngle,		NOX_FINAL_BLADES_ANGLE_MIN,		NOX_FINAL_BLADES_ANGLE_MAX,		5.0f,	0.5f);
	emBladesRadius->setValuesToFloat(	cam->fMod.bladesRadius,		NOX_FINAL_BLADES_RADIUS_MIN,	NOX_FINAL_BLADES_RADIUS_MAX,	0.1f,	0.01f);
	emBokehRingBalance->setValuesToInt(	cam->fMod.bokehRingBalance,	NOX_FINAL_BOKEH_BALANCE_MIN,	NOX_FINAL_BOKEH_BALANCE_MAX,	1,		0.2f);
	emBokehRingSize->setValuesToInt(	cam->fMod.bokehRingSize,	NOX_FINAL_BOKEH_RING_SIZE_MIN,	NOX_FINAL_BOKEH_RING_SIZE_MAX,	1,		0.2f);
	emBokehFlatten->setValuesToInt(		cam->fMod.bokehFlatten,		NOX_FINAL_BOKEH_FLATTEN_MIN,	NOX_FINAL_BOKEH_FLATTEN_MAX,	1,		0.02f);
	emBokehVignette->setValuesToInt(	cam->fMod.bokehVignette,	NOX_FINAL_BOKEH_VIGNETTE_MIN,	NOX_FINAL_BOKEH_VIGNETTE_MAX,	1,		0.02f);
	emDOFon->selected =	cam->fMod.dofOn;
	emBladesRound->selected = cam->fMod.roundBlades;
	InvalidateRect(emDOFon->hwnd, NULL, false);
	InvalidateRect(emBladesRound->hwnd, NULL, false);
	emShiftLens->setValuesToFloat( cam->fMod.chromaticShiftLens,	NOX_FINAL_CHR_SHIFT_LENS_MIN,	NOX_FINAL_CHR_SHIFT_LENS_MAX,	0.1f);
	emShiftDepth->setValuesToFloat(cam->fMod.chromaticShiftDepth,	NOX_FINAL_CHR_SHIFT_DEPTH_MIN,	NOX_FINAL_CHR_SHIFT_DEPTH_MAX,	0.1f);
	emAchromatic->setValuesToFloat(cam->fMod.chromaticAchromatic,	NOX_FINAL_CHR_ACHROMATIC_MIN,	NOX_FINAL_CHR_ACHROMATIC_MAX,	0.1f);
	emChrPlRed->setValuesToFloat(	cam->fMod.chromaticPlanarRed,	NOX_FINAL_CHR_PL_RED_MIN,		NOX_FINAL_CHR_PL_RED_MAX,		0.1f);
	emChrPlGreen->setValuesToFloat(	cam->fMod.chromaticPlanarGreen, NOX_FINAL_CHR_PL_GREEN_MIN,		NOX_FINAL_CHR_PL_GREEN_MAX,		0.1f);
	emChrPlBlue->setValuesToFloat(	cam->fMod.chromaticPlanarBlue,	NOX_FINAL_CHR_PL_BLUE_MIN,		NOX_FINAL_CHR_PL_BLUE_MAX,		0.1f);
	emQuality->setValuesToInt(cam->fMod.quality, NOX_FINAL_QUALITY_MIN, NOX_FINAL_QUALITY_MAX, 1, 0.05f);
	emThreads->setValuesToInt(cam->fMod.numThreads, 1, 16, 1, 0.1f);
	emab->abbShiftXY = cam->fMod.chromaticShiftLens;
	emab->abbShiftZ = cam->fMod.chromaticShiftDepth;
	emab->abbAchromatic= cam->fMod.chromaticAchromatic;
	InvalidateRect(emab->hwnd, NULL, false);
	emGrain->setValuesToFloat(cam->fMod.grain, 0.0f, 1.0f, 0.1f);
	emDotsEnabled->selected = cam->fMod.dotsEnabled;
	InvalidateRect(emDotsEnabled->hwnd, NULL, false);
	emDotsRadius->setValuesToInt(cam->fMod.dotsRadius, NOX_FINAL_DOTS_RADIUS_MIN, NOX_FINAL_DOTS_RADIUS_MAX, 1, 0.1f);
	emDotsThreshold->setValuesToFloat(cam->fMod.dotsThreshold, NOX_FINAL_DOTS_THRESHOLD_MIN, NOX_FINAL_DOTS_THRESHOLD_MAX, 1, 0.1f);
	emDotsDensity->setValuesToInt(cam->fMod.dotsDensity, NOX_FINAL_DOTS_DENSITY_MIN, cam->fMod.dotsRadius*cam->fMod.dotsRadius, 1, 0.1f);

	emBloomPower->setValuesToFloat(	cam->fMod.bloomPower,		NOX_FINAL_BLOOM_POWER_MIN,	NOX_FINAL_BLOOM_POWER_MAX,	0.5f);
	emBloomArea->setValuesToInt(	cam->fMod.bloomArea,		NOX_FINAL_BLOOM_AREA_MIN,	NOX_FINAL_BLOOM_AREA_MAX,	10);
	emBloomThres->setValuesToFloat(	cam->fMod.bloomThreshold,	NOX_FINAL_BLOOM_THRES_MIN,	NOX_FINAL_BLOOM_THRES_MAX,	0.5f);
	emGlarePower->setValuesToFloat(	cam->fMod.glarePower,		NOX_FINAL_GLARE_POWER_MIN,	NOX_FINAL_GLARE_POWER_MAX,	0.5f);
	emGlareDisp->setValuesToFloat(	cam->fMod.glareDispersion,	NOX_FINAL_GLARE_DISP_MIN,	NOX_FINAL_GLARE_DISP_MAX,	0.5f);
	emGlareThres->setValuesToFloat(	cam->fMod.glareThreshold,	NOX_FINAL_GLARE_THRES_MIN,	NOX_FINAL_GLARE_THRES_MAX,	0.5f);
	emGlareArea->setValuesToInt(	cam->fMod.glareArea,		NOX_FINAL_GLARE_AREA_MIN,	NOX_FINAL_GLARE_AREA_MAX,	32);
	emBloomOn->selected = cam->fMod.bloomEnabled;
	emBloomAttenOn->selected = cam->fMod.bloomAttenuationOn;
	emGlareOn->selected = cam->fMod.glareEnabled;
	emGlareApertTexOn->selected = cam->fMod.glareTextureApertureOn;
	emGlareObstTexOn->selected = cam->fMod.glareTextureObstacleOn;
	InvalidateRect(emBloomOn->hwnd, NULL, false);
	InvalidateRect(emBloomAttenOn->hwnd, NULL, false);
	InvalidateRect(emGlareOn->hwnd, NULL, false);
	InvalidateRect(emGlareApertTexOn->hwnd, NULL, false);
	InvalidateRect(emGlareObstTexOn->hwnd, NULL, false);
	emBloomAttCol->redraw(cam->fMod.bloomAttenuationColor);
	

	emFogEnabled->selected = cam->fMod.fogEnabled;
	emFogCol->color = cam->fMod.fogColor;
	emFogExcludeSky->selected = cam->fMod.fogExclSky;
	InvalidateRect(emFogCol->hwnd, NULL, false);
	InvalidateRect(emFogExcludeSky->hwnd, NULL, false);
	InvalidateRect(emFogEnabled->hwnd, NULL, false);
	emFogDist->setValuesToFloat(cam->fMod.fogDist, NOX_FINAL_FOG_DIST_MIN, NOX_FINAL_FOG_DIST_MAX, 1, 0.1f);
	emFogSpeed->setValuesToFloat(cam->fMod.fogSpeed, NOX_FINAL_FOG_SPEED_MIN, NOX_FINAL_FOG_SPEED_MAX, 0.1f, 0.01f);

	emAerialEnabled->selected = cam->fMod.aerialEnabled;
	InvalidateRect(emAerialEnabled->hwnd, NULL, false);
	emAerialScale->setValuesToFloat(cam->fMod.aerialScale, NOX_FINAL_AERIAL_SCALE_MIN, NOX_FINAL_AERIAL_SCALE_MAX, 1, 0.1f);

	
	EMButton * emb1 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_CHANGE_SHAPE));
	emb1->selected = cam->texDiaphragm.isValid();
	InvalidateRect(emb1->hwnd, NULL, false);
	EMButton * emb2 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_CHANGE_OBSTACLE));
	emb2->selected = cam->texObstacle.isValid();
	InvalidateRect(emb2->hwnd, NULL, false);

	drawPreviewBokeh(empv->byteBuff, &(cam->fMod), emab->distPattern);
	InvalidateRect(empv->hwnd, NULL, false);

	emAlgorithm->selected = cam->fMod.algorithm;
	InvalidateRect(emAlgorithm->hwnd, NULL, false);

}

bool getAllFinalGUIValues(HWND hWnd)
{
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (!cam)
		return false;

	EMEditSpin * emFocusDist =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOCUS_DIST));
	EMEditSpin * emAperture =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_APERTURE));
	EMEditSpin * emBladesNum =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_NUMBER));
	EMEditSpin * emBladesAngle =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ANGLE));
	EMEditSpin * emBladesRadius	=		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_RADIUS));
	EMEditSpin * emBokehRingBalance =	GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_BALANCE));
	EMEditSpin * emBokehRingSize =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_RING_SIZE));
	EMEditSpin * emBokehFlatten	=		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_FLATTEN));
	EMEditSpin * emBokehVignette =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_BOKEH_VIGNETTE));
	EMCheckBox * emDOFon =				GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ON));
	EMCheckBox * emBladesRound =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLADES_ROUND));
	EMEditSpin * emQuality =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_QUALITY));
	EMEditTrackBar * emGrain =			GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GRAIN));
	EMEditTrackBar * emShiftLens =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_LENS));
	EMEditTrackBar * emShiftDepth =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SHIFT_DEPTH));
	EMEditTrackBar * emAchromatic =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_ACHROMATIC));
	EMEditTrackBar * emChrPlRed =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_RED));
	EMEditTrackBar * emChrPlGreen =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_GREEN));
	EMEditTrackBar * emChrPlBlue =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABBPL_BLUE));
	EMCheckBox * emScaleAperture =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SCALE_APERTURE));
	EMCheckBox * emScaleDepth =			GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_SCALE_DISTANCE));
	EMAberration * emab =				GetEMAberrationInstance(GetDlgItem(hWnd, IDC_FINAL_CHRABB_SCHEME));
	EMComboBox * emAlgorithm =			GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOF_ALGORITHM));
	EMEditSpin * emDotsRadius =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_RADIUS));
	EMEditSpin * emDotsThreshold =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_THRESHOLD));
	EMEditSpin * emDotsDensity =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_DENSITY));
	EMCheckBox * emDotsEnabled =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_DOTS_ENABLED));
	EMEditTrackBar * emBloomPower =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_POWER));
	EMEditTrackBar * emBloomArea =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_AREA));
	EMEditTrackBar * emBloomThres =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_THRESHOLD));
	EMEditTrackBar * emGlarePower =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_POWER));
	EMEditTrackBar * emGlareDisp =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_DISPERSION));
	EMEditTrackBar * emGlareThres =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_THRESHOLD));
	EMEditTrackBar * emGlareArea =		GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_AREA));
	EMCheckBox * emBloomOn =			GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ON));
	EMCheckBox * emBloomAttenOn =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_BLOOM_ATTENUATION_ON));
	EMCheckBox * emGlareOn =			GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_ON));
	EMCheckBox * emGlareApertTexOn =	GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_SHAPE_ON));
	EMCheckBox * emGlareObstTexOn =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_GLARE_OBSTACLE_ON));
	EMCheckBox * emFogExcludeSky =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_EXCLUDE_SKY));
	EMCheckBox * emFogEnabled =			GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_ENABLED));
	EMEditSpin * emFogDist =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_DISTANCE));
	EMEditSpin * emFogSpeed =			GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_FOG_SPEED));
	EMEditSpin * emAerialScale =		GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FINAL_AERIAL_SCALE));
	EMCheckBox * emAerialEnabled =		GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FINAL_AERIAL_ENABLED));

	cam->fMod.quality = emQuality->intValue;
	cam->fMod.focusDistance = emFocusDist->floatValue;
	cam->fMod.aperture = emAperture->floatValue;
	cam->fMod.bladesNum = emBladesNum->intValue;
	cam->fMod.bladesAngle = emBladesAngle->floatValue;
	cam->fMod.bladesRadius = emBladesRadius->floatValue;
	cam->fMod.bokehRingBalance = emBokehRingBalance->intValue;
	cam->fMod.bokehRingSize = emBokehRingSize->intValue;
	cam->fMod.bokehFlatten = emBokehFlatten->intValue;
	cam->fMod.bokehVignette = emBokehVignette->intValue;
	cam->fMod.dofOn = emDOFon->selected;
	cam->fMod.roundBlades = emBladesRound->selected;
	cam->fMod.chromaticShiftLens = emShiftLens->floatValue;
	cam->fMod.chromaticShiftDepth = emShiftDepth->floatValue;
	cam->fMod.chromaticAchromatic = emAchromatic->floatValue;
	cam->fMod.chromaticPlanarRed = emChrPlRed->floatValue;
	cam->fMod.chromaticPlanarGreen = emChrPlGreen->floatValue;
	cam->fMod.chromaticPlanarBlue = emChrPlBlue->floatValue;
	cam->fMod.algorithm = emAlgorithm->selected;
	cam->fMod.grain = emGrain->floatValue;
	cam->fMod.dotsEnabled = emDotsEnabled->selected;
	cam->fMod.dotsRadius = emDotsRadius->intValue;
	cam->fMod.dotsThreshold = emDotsThreshold->floatValue;
	cam->fMod.dotsDensity = emDotsDensity->intValue;

	scaleAperture = emScaleAperture->selected;
	scaleDepth = emScaleDepth->selected;
	prevDist = emab->distPattern;


	cam->fMod.bloomPower = emBloomPower->floatValue;
	cam->fMod.bloomArea = emBloomArea->intValue;
	cam->fMod.bloomThreshold = emBloomThres->floatValue;
	cam->fMod.glarePower = emGlarePower->floatValue;
	cam->fMod.glareDispersion = emGlareDisp->floatValue;
	cam->fMod.glareThreshold = emGlareThres->floatValue;
	cam->fMod.glareArea = emGlareArea->intValue;
	cam->fMod.bloomEnabled = emBloomOn->selected;
	cam->fMod.bloomAttenuationOn = emBloomAttenOn->selected;
	cam->fMod.glareEnabled = emGlareOn->selected;
	cam->fMod.glareTextureApertureOn = emGlareApertTexOn->selected;
	cam->fMod.glareTextureObstacleOn = emGlareObstTexOn->selected;

	cam->fMod.fogDist = emFogDist->floatValue;
	cam->fMod.fogSpeed = emFogSpeed->floatValue;
	cam->fMod.fogExclSky = emFogExcludeSky->selected;
	cam->fMod.fogEnabled = emFogEnabled->selected;
	cam->fMod.aerialEnabled = emAerialEnabled->selected;
	cam->fMod.aerialScale = emAerialScale->floatValue;

	return true;
}

bool drawPreviewBokeh(ImageByteBuffer * img, FinalModifier * fMod, float depth)
{
	CHECK(fMod);
	CHECK(img);
	CHECK(img->imgBuf);

	float aperture = scaleAperture ? fMod->aperture : 1.0f;

	float rRed, rGreen, rBlue;
	evalChromaticBokehSizes(fMod->chromaticShiftLens, fMod->chromaticShiftDepth, fMod->chromaticAchromatic, fabs(depth), rRed, rGreen, rBlue);
	float maxR = max(rRed, max(rGreen, rBlue));

	if (scaleDepth)
		aperture *= 1.0f/max(0.04f, maxR);

	aperture = max(1.0f, aperture);

	DiaphragmSampler diaph;
	diaph.initializeDiaphragm(fMod->bladesNum, aperture, fMod->bladesAngle, fMod->roundBlades, fMod->bladesRadius);
	diaph.intBokehRingBalance = fMod->bokehRingBalance;
	diaph.intBokehRingSize = fMod->bokehRingSize;
	diaph.intBokehFlatten = fMod->bokehFlatten;
	diaph.intBokehVignette = fMod->bokehVignette;
	diaph.chromaticShiftLens = fMod->chromaticShiftLens;
	diaph.chromaticShiftDepth = fMod->chromaticShiftDepth;
	diaph.chromaticAchromatic = fMod->chromaticAchromatic;
	if (diaph.intBokehRingBalance>=0)
		diaph.bokehRingBalance = diaph.intBokehRingBalance+1.0f;
	else
		diaph.bokehRingBalance = 1.0f + diaph.intBokehRingBalance/21.0f;
	diaph.bokehRingSize = diaph.intBokehRingSize*0.01f;
	diaph.bokehFlatten = diaph.intBokehFlatten*0.01f;
	diaph.bokehVignette = diaph.intBokehVignette*0.01f;

	return diaph.createApertureShapePreviewChromaticBokeh(img, depth);
}

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

DWORD WINAPI FakeDofMainThreadProc(LPVOID lpParameter)
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->registerProgressCallback(ThreadWindow::notifyProgressBarCallback, 100);
	Sleep(10);
	KillTimer(RendererMainWindow::hPost, TIMER_POSTPROCESS_ID);
	KillTimer(RendererMainWindow::hBlend, TIMER_POSTPROCESS_ID);

	HWND hImg = (HWND)lpParameter;
	EMPView * empvmain = GetEMPViewInstance(hImg);
	updateRenderedImage(empvmain, true, true, NULL, false);

	if (RendererMainWindow::showTimeStamp)
		empvmain->byteBuff->applyTimeStamp();
	if (RendererMainWindow::showLogoStamp)
		empvmain->byteBuff->applyLogoStamp(&rtr->logoStamp);

	InvalidateRect(hImg, NULL, false);

	ThreadWindow * curProgWindow = ThreadWindow::getCurrentInstance();
	if (curProgWindow)
	{
		curProgWindow->closeWindow();
		delete curProgWindow;
	}

	rtr->curScenePtr->registerProgressCallback(RendererMainWindow::notifyProgressBarCallback, 100);	// get back notification to main window

	return 0;
}

bool redrawFakeDofOnThread(HWND hImg)
{
	ThreadWindow * tWindow = new ThreadWindow();
	tWindow->createWindow(RendererMainWindow::hMain, RendererMainWindow::hModule);
	tWindow->setWindowTitle("Finalizing rendered image");
	tWindow->setText("Finalizing rendered image");

	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(FakeDofMainThreadProc),
            (LPVOID)hImg,
            0,
            &threadID);
	return true;
}


DWORD WINAPI DepthMapMainThreadProc(LPVOID lpParameter)
{
	Logger::add("Depth Map main thread started");
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->registerProgressCallback(ThreadWindow::notifyProgressBarCallback, 100);

	Camera * cam = rtr->curScenePtr->getActiveCamera();


	rtr->curScenePtr->evalDepthMap(cam);


	ThreadWindow * curProgWindow = ThreadWindow::getCurrentInstance();
	if (curProgWindow)
	{
		curProgWindow->closeWindow();
		delete curProgWindow;
	}

	rtr->curScenePtr->registerProgressCallback(RendererMainWindow::notifyProgressBarCallback, 100);	// get back notification to main window

	Logger::add("Depth Map main thread finished");

	return 0;
}

bool reevaluateDepthMapOnThread()
{
	ThreadWindow * tWindow = new ThreadWindow();
	tWindow->createWindow(RendererMainWindow::hMain, RendererMainWindow::hModule);
	tWindow->setWindowTitle("Depth map");
	tWindow->setText("Evaluating depth map");

	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(DepthMapMainThreadProc),
            (LPVOID)0,
            0,
            &threadID);

	return true;
}
