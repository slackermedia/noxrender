#include <windows.h>
#include <delayimp.h>
#include "stdafx.h"
#include "DLL.h"
#pragma comment(lib, "DelayImp.lib")

HMODULE hDllModule;

bool checkDLLs()
{
	return true;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	hDllModule = hModule;
	char * cml = GetCommandLine();

	bool isUpdate = false;
	if (strstr(cml, "NOXUpdate.exe"))
		isUpdate = true;

	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
		InitializeApplication(!isUpdate);
	
    return TRUE;
}
