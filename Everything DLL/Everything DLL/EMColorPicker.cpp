#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"

extern HMODULE hDllModule;

void InitEMColorPicker()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMColorPicker";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMColorPickerProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMColorPicker *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMColorPicker * GetEMColorPickerInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMColorPicker * emcp = (EMColorPicker *)GetWindowLongPtr(hwnd, 0);
	#else
		EMColorPicker * emcp = (EMColorPicker *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcp;
}

void SetEMColorPickerInstance(HWND hwnd, EMColorPicker *emcp)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcp);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcp);
	#endif
}

EMColorPicker::EMColorPicker(HWND hWnd)
{
	colBorderLeftUp = RGB(112,176,240);
	colBorderRightDown = RGB(144,208,255);
	colArrow = RGB(0,0,0);
	colArrowClicked = RGB(255,128,0);
	colBackground = RGB(128,192,255);
	showBorder = false;
	hwnd = hWnd;

	sliderWidth = 16;
	hueSlPos = 0;
	satPos = 0;
	valPos = 255;
	rgb = Color4(1,0,0);
	hsv = rgb.toColorHSV();
	fullSatHSV = ColorHSV(0,1,1);
	fullSatRGB = fullSatHSV.toColor4();
	satPos = (int)(hsv.s * 255);
	valPos = (int)(hsv.v * 255);
	buffH = (unsigned char *)malloc(4*256*slW());
	buffSV = (unsigned char *)malloc(4*256*256);
	generateSlider();
	clickedSlider = false;
	clickedQuad = false;

	POINT bounding[] = { {0,0}, {300, 0}, {300, 266}, {0, 266} };
	cRgn = CreatePolygonRgn(bounding, 4, WINDING);
	SetWindowRgn(hwnd, cRgn, FALSE);
}

EMColorPicker::~EMColorPicker()
{
	if (buffH)
		free(buffH);
	if (buffSV)
		free(buffSV);
	if (cRgn)
		DeleteObject(cRgn);
}

LRESULT CALLBACK EMColorPickerProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMColorPicker* emcp;
	emcp = GetEMColorPickerInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			emcp = new EMColorPicker(hwnd);
			SetEMColorPickerInstance(hwnd, emcp);
			break;
		case WM_DESTROY:
			{
				delete emcp;
				SetEMColorShowInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			HDC thdc;
			HBITMAP hbmp;
			HBITMAP oldBmp;
			HRGN rg;
			HBRUSH fbr;

			hdc = BeginPaint(hwnd, &ps);
			// create temp back buffer
			HDC backDC = CreateCompatibleDC(hdc);
			HBITMAP backBMP = CreateCompatibleBitmap(hdc, 300, 266);
			SelectObject(backDC, backBMP);

			// fill back buffer
			POINT bounding[] = { {0,0}, {300, 0}, {300, 266}, {0, 266} };
			rg = CreatePolygonRgn(bounding, 4, WINDING);
			fbr = CreateSolidBrush(emcp->colBackground);
			FillRgn(backDC, rg, fbr);
			DeleteObject(fbr);
			DeleteObject(rg);
			
			// generate new SV picture
			emcp->generateQuad();

			// show Sat/Val picture
			thdc = CreateCompatibleDC(backDC);
			hbmp = CreateCompatibleBitmap(hdc, 256,256);
			oldBmp = (HBITMAP)SelectObject(thdc, hbmp); 

			BITMAPINFOHEADER bmih;
			ZeroMemory(&bmih,sizeof(bmih));
			bmih.biSize = sizeof(BITMAPINFOHEADER);
			bmih.biBitCount = 32;
			bmih.biClrImportant = 0;
			bmih.biClrUsed = 0;
			bmih.biCompression = BI_RGB;
			bmih.biHeight = 256;
			bmih.biWidth = 256;
			bmih.biPlanes = 1;
			bmih.biSizeImage = 256*256;
			bmih.biXPelsPerMeter = 0;
			bmih.biYPelsPerMeter = 0;
			BITMAPINFO bmi;
			bmi.bmiHeader = bmih;
			int ccc = SetDIBits(thdc, hbmp, 0, 256, emcp->buffSV, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);

			BitBlt(backDC, 5, 5, 256, 256,	thdc, 0, 0, SRCCOPY);
			SelectObject(thdc, oldBmp);
			DeleteObject(hbmp);

			// show Hue trackbar
			thdc = CreateCompatibleDC(hdc);

			hbmp = CreateCompatibleBitmap(hdc, emcp->slW(),256);
			oldBmp = (HBITMAP)SelectObject(thdc, hbmp); 

			ZeroMemory(&bmih,sizeof(bmih));
			bmih.biSize = sizeof(BITMAPINFOHEADER);
			bmih.biBitCount = 32;
			bmih.biClrImportant = 0;
			bmih.biClrUsed = 0;
			bmih.biCompression = BI_RGB;
			bmih.biHeight = 256;
			bmih.biWidth = emcp->slW();
			bmih.biPlanes = 1;
			bmih.biSizeImage = emcp->slW()*256;
			bmih.biXPelsPerMeter = 0;
			bmih.biYPelsPerMeter = 0;
			bmi.bmiHeader = bmih;
			ccc = SetDIBits(thdc, hbmp, 0, 256, emcp->buffH, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);

			oldBmp = (HBITMAP)SelectObject(thdc, hbmp); 
			BitBlt(backDC, 270, 5, emcp->slW(), 256, thdc, 0, 0, SRCCOPY);
			SelectObject(thdc, oldBmp);
			DeleteObject(hbmp);
			DeleteDC(thdc); 

			// draw left arrow
			POINT arrowLeft[] = {
				{265, emcp->hueSlPos+1},
				{265, emcp->hueSlPos+9},
				{269, emcp->hueSlPos+5},
			};
			rg = CreatePolygonRgn(arrowLeft, 3, WINDING);
			if (emcp->clickedSlider)
				fbr = CreateSolidBrush(emcp->colArrowClicked);
			else
				fbr = CreateSolidBrush(emcp->colArrow);
			FillRgn(backDC, rg, fbr);
			DeleteObject(fbr);
			DeleteObject(rg);

			// draw right arrow
			POINT arrowRight[] = {
				{270+emcp->slW()+5, emcp->hueSlPos+1},
				{270+emcp->slW()+5, emcp->hueSlPos+9},
				{270+emcp->slW()+1, emcp->hueSlPos+5},
			};
			rg = CreatePolygonRgn(arrowRight, 3, WINDING);
			if (emcp->clickedSlider)
				fbr = CreateSolidBrush(emcp->colArrowClicked);
			else
				fbr = CreateSolidBrush(emcp->colArrow);
			FillRgn(backDC, rg, fbr);
			DeleteObject(fbr);
			DeleteObject(rg);

			// draw SV pos circle
			HPEN oldPen = (HPEN)(SelectObject(backDC, CreatePen(PS_SOLID, 1, 
				RGB(  (int)(255-emcp->fullSatRGB.r*255),  (int)(255-emcp->fullSatRGB.g*255),  (int)(255-emcp->fullSatRGB.b*255)  ))));
			HBRUSH oldBrush = (HBRUSH)SelectObject(backDC, (HBRUSH)GetStockObject(HOLLOW_BRUSH));
			Ellipse(backDC, emcp->satPos+1, 256-emcp->valPos+2, emcp->satPos+8, 265-emcp->valPos);
			DeleteObject(SelectObject(backDC, oldBrush));
			DeleteObject(SelectObject(backDC, oldPen));


			if (emcp->showBorder)
			{
				GetClientRect(hwnd, &rect);
				POINT border1[] = {	{0, 265},  {0,0}, {300, 0} };
				POINT border2[] = {	{299,1}, {299,265}, 
									{0, 265}};
				oldPen = (HPEN)SelectObject(backDC, CreatePen(PS_SOLID, 1, emcp->colBorderLeftUp));
				Polyline(backDC, border1, 3);
				DeleteObject(SelectObject(backDC, CreatePen(PS_SOLID, 1, emcp->colBorderRightDown)));
				Polyline(backDC, border2, 3);
				DeleteObject(SelectObject(backDC, oldPen));
			}

			// copy from back buffer and release
			BitBlt(hdc, 0, 0, 300, 266, backDC, 0, 0, SRCCOPY );
			DeleteDC(backDC); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
		case WM_LBUTTONDOWN:
			{
				POINT pt = {(short)LOWORD(lParam), (short)HIWORD(lParam)};
				RECT rect;

				// check if on quad
				rect.left = 5;
				rect.top = 5;
				rect.right = 261;
				rect.bottom = 261;
				if (PtInRect(&rect, pt))
				{
					int s,v;
					s = pt.x - 5;
					v = 261 - pt.y;
					if (s < 0)
						s = 0;
					if (v < 0)
						v = 0;
					if (s > 255)
						s = 255;
					if (v > 255)
						v = 255;
					emcp->satPos = s;
					emcp->valPos = v;
					emcp->hsv.s = s/255.0f;
					emcp->hsv.v = v/255.0f;
					emcp->hsv.h = emcp->hueSlPos/255.0f;
					emcp->rgb = emcp->hsv.toColor4();
					emcp->clickedQuad = true;
					emcp->notifyParent();
				}

				// check if on slider
				rect.top = 5;
				rect.left = 270;
				rect.bottom = 261;
				rect.right = 270 + emcp->slW();
				if (PtInRect(&rect, pt))
				{
					emcp->clickedSlider = true;
					int y = (short)HIWORD(lParam)-5;
					if (y < 0)
						y = 0;
					if (y > 255)
						y = 255;
					emcp->hueSlPos = y;
					emcp->fullSatHSV.h = y/255.0f;
					emcp->fullSatHSV.s = 1;
					emcp->fullSatHSV.v = 1;
					emcp->fullSatRGB = emcp->fullSatHSV.toColor4();
					emcp->hsv.h = y/255.0f;
					emcp->rgb = emcp->hsv.toColor4();
					emcp->notifyParent();
				}
				GetClientRect(hwnd, &rect);
				InvalidateRect(hwnd, &rect, false);
			}
			break;
		case WM_LBUTTONUP:
			{
					emcp->clickedSlider = false;
					emcp->clickedQuad = false;
					RECT rect;
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
			}
			break;
		case WM_MOUSEMOVE:
			{
				if (!GetCapture())
					SetCapture(hwnd);

				POINT pt = {(short)LOWORD(lParam), (short)HIWORD(lParam)};
				RECT rect;

				if (emcp->clickedQuad)
				{
					int s,v;
					s = pt.x - 5;
					v = 261 - pt.y;
					if (s < 0)
						s = 0;
					if (v < 0)
						v = 0;
					if (s > 255)
						s = 255;
					if (v > 255)
						v = 255;
					emcp->satPos = s;
					emcp->valPos = v;
					emcp->hsv.s = s/255.0f;
					emcp->hsv.v = v/255.0f;
					emcp->hsv.h = emcp->hueSlPos/255.0f;
					emcp->rgb = emcp->hsv.toColor4();
					emcp->notifyParent();
				}

				if (emcp->clickedSlider)
				{
					int y = (short)HIWORD(lParam)-5;
					if (y < 0)
						y = 0;
					if (y > 255)
						y = 255;
					emcp->hueSlPos = y;
					emcp->fullSatHSV.h = y/255.0f;
					emcp->fullSatHSV.s = 1;
					emcp->fullSatHSV.v = 1;
					emcp->fullSatRGB = emcp->fullSatHSV.toColor4();
					emcp->hsv.h = y/255.0f;
					emcp->rgb = emcp->hsv.toColor4();
					emcp->notifyParent();
				}

				if ((!emcp->clickedQuad) && (!emcp->clickedSlider))
				{
					rect.top = 0;
					rect.left = 0;
					rect.bottom = 266;
					rect.right = 300;
					if (!PtInRect(&rect, pt))
					{
						ReleaseCapture();
						break;
					}
				}

				GetClientRect(hwnd, &rect);
				InvalidateRect(hwnd, &rect, false);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}


void EMColorPicker::generateSlider()
{
	ColorHSV tHSV;
	Color4 tRGB;
	int i,j;
	tHSV.s = 1;
	tHSV.v = 1;
	for (i=0; i<256; i++)
	{
		tHSV.h = (255-i)/255.01f;
		tRGB = tHSV.toColor4();
		for (j=0; j<slW(); j++)
		{
			buffH[i*slW()*4+j*4+0] = (unsigned char)(tRGB.b*255);
			buffH[i*slW()*4+j*4+1] = (unsigned char)(tRGB.g*255);
			buffH[i*slW()*4+j*4+2] = (unsigned char)(tRGB.r*255);
			buffH[i*slW()*4+j*4+3] = 255;
		}
	}
}

void EMColorPicker::generateQuad()
{
	int i,j;
	for (j=0; j<256; j++)
	{
		for (i=0; i<256; i++)
		{
			buffSV[j*1024+i*4+0] = (unsigned char)((fullSatRGB.b*i+255-i)*(j)/255.0f);
			buffSV[j*1024+i*4+1] = (unsigned char)((fullSatRGB.g*i+255-i)*(j)/255.0f);
			buffSV[j*1024+i*4+2] = (unsigned char)((fullSatRGB.r*i+255-i)*(j)/255.0f);
			buffSV[j*1024+i*4+3] = 255;
		}
	}
}

int EMColorPicker::slW()
{
	return sliderWidth;
}

void EMColorPicker::notifyParent()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_VSCROLL), (LPARAM)hwnd);
}

void EMColorPicker::setRGBandRedraw(const Color4 &c)
{
	rgb = c;
	hsv = rgb.toColorHSV();
	fullSatHSV.h = hsv.h;
	fullSatHSV.s = 1;
	fullSatHSV.v = 1;
	fullSatRGB = fullSatHSV.toColor4();
	hueSlPos = (int)(hsv.h*255);
	satPos = (int)(hsv.s*255);
	valPos = (int)(hsv.v*255);
	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

void EMColorPicker::setHSVandRedraw(ColorHSV c)
{
	hsv = c; 
	rgb = hsv.toColor4();
	ColorHSV fullSatHSV = hsv;
	fullSatHSV.s = 1;
	fullSatHSV.v = 1;
	fullSatRGB = fullSatHSV.toColor4();
	hueSlPos = (int)(hsv.h*255);
	satPos = (int)(hsv.s*255);
	valPos = (int)(hsv.v*255);
	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

