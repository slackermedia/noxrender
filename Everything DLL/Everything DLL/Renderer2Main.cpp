#include "RendererMain2.h"
#include <Windows.h>
#include "resource.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "log.h"
#include "MatEditor2.h"
#include "newgui_coords.h"
#include "ThreadJobWindow.h"
#include "XML_IO.h"
#include "dialogs.h"

extern HMODULE hDllModule;
extern char  * defaultDirectory;
RendererMain2 * RendererMain2::rmwInstance = NULL;
bool RendererMain2::isWindowClassRegistered = false;
bool updateRegionButtonsState(HWND hWnd, EMPView * empv);

#define NOX_MAIN_WIDTH 1280
#define NOX_MAIN_HEIGHT 690

//#define DEBUG_ONE_THREAD

//------------------------------------------------------------------------------------------------

RendererMain2::RendererMain2()
{
	hMain = 0;

	int cx = GetSystemMetrics(SM_CXSCREEN);
	int cy = GetSystemMetrics(SM_CYSCREEN);

	hTabRender = 0;
	hTabCamera = 0;
	hTabMats = 0;
	hTabEnv = 0;
	hTabBlend = 0;
	hTabPost = 0;
	hTabCorrection = 0;
	hTabFakeDof = 0;
	hTabEffects = 0;
	hTabs = 0;
	hViewport = 0;
	hTopPanel = 0;
	hBottomPanel = 0;
	hButtonMain = 0;
	hButtonRegions = 0;

	hStopThreadId = 0;

	start_texts.ht1_welcome = 0;
	start_texts.ht2_noxname = 0;
	start_texts.ht3_loadLink = 0;
	start_texts.ht4_noxScene = 0;
	start_texts.ht5_openLink = 0;
	start_texts.ht6_SampleScenes = 0;
	start_texts.ht7_or = 0;

	sceneID = 0;
	cantloadscene = false;
	showstartgui = true;

	fonts = NOXFontManager::getInstance();

	rightPanelOpened = true;
	rightButtonShowing = true;
	regionsPanelOpened = false;

	timers.autosaveOn = false;
	timers.refreshOn = true;
	timers.stopOn = false;
	timers.autosaveMin = 60;
	timers.refreshSec = 10;
	timers.stopSec = 0;

	matcontrol = NULL;
	numMatControls = 0;
	totalPixelSamples = 0;

	hBmpBg = generateBackgroundPattern(cx, cy, 0,0, NGCOL_BG_LIGHT, NGCOL_BG_DARK);

	autoRedrawPost = true;
	autoRedrawTimer = 500;
	bgShiftXRightPanel = 0;
	bgShiftXRegionsPanel = 0;
	bgShiftYRegionsPanel = 0;
	hDiaph = 0;
	hLogoGradient = 0;
}

//------------------------------------------------------------------------------------------------

RendererMain2::~RendererMain2()
{
	DeleteObject(hBmpBg);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::create(bool asPlugin, HWND hParent, HINSTANCE hInst)
{
	DWORD VIS_ON_START =0;// WS_VISIBLE;

	// eval window size from client size
	RECT tmprect;
	tmprect.left = 0;
	tmprect.top = 0;
	tmprect.right = NOX_MAIN_WIDTH;
	tmprect.bottom = NOX_MAIN_HEIGHT;
	AdjustWindowRect(&tmprect, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE, FALSE);
	int wx = tmprect.right - tmprect.left;
	int wy = tmprect.bottom - tmprect.top;
	int cx = GetSystemMetrics(SM_CXSCREEN);
	int cy = GetSystemMetrics(SM_CYSCREEN);

	// register main window
	if (!isWindowClassRegistered)
	{
		registerMyClass(hInst);
		isWindowClassRegistered = true;
	}

	// make main window object
	RendererMain2 * main = new RendererMain2();
	main->rmwInstance = main;
	main->hInstance = hInst;

	// create main scene
	main->createFirstScene();

	main->hLogoGradient = loadHBitmapFromBinaryResource(hDllModule, IDB_NOX_LOGO_START);

	// create window
	char noxnametitle[256];
	sprintf_s(noxnametitle, 256, "NOX %d.%.2d", NOX_VER_MAJOR, NOX_VER_MINOR);
	main->hMain = CreateWindow("NOXMainWindow2", noxnametitle, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE,
					(cx-wx)/2, (cy-wy)/2,   wx,wy,    hParent, NULL, hInst, NULL);

	RECT dRect;
	BOOL gok1 = SystemParametersInfo(SPI_GETWORKAREA, 0, &dRect, 0);
	if (gok1)
	{
		int dH = dRect.bottom - dRect.top;
		if (dH < wy)
			ShowWindow(main->hMain, SW_MAXIMIZE);	//62px pasek ma
	}

	// make viewport
	main->hViewport = CreateWindow("EMPView", "", VIS_ON_START | WS_CHILD | WS_CLIPSIBLINGS,
					10, 10,   800, 600,    main->hMain, (HMENU)IDC_REND2_VIEWPORT, hDllModule, NULL);
	EMPView * empv = GetEMPViewInstance(main->hViewport);
	empv->colBorder = RGB(64,64,64);
	empv->colBackground = RGB(0,0,0);
	empv->showBorder = false;
	empv->allowDragFiles();

	// make top menu scene
	HWND hMenuScene = CreateWindow("EM2ComboBox", "SCENE", WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS,
					10, 1,   53, 28,    main->hMain, (HMENU)IDC_REND2_MENU_SCENE, hDllModule, NULL);
	EM2ComboBox * emcScene = GetEM2ComboBoxInstance(hMenuScene);
	emcScene->bgImage = main->hBmpBg;
	emcScene->setFontMain(main->fonts->em2combomain, false);
	emcScene->setFontUnwrapped(main->fonts->em2combounwrap, false);
	emcScene->colUnwrapBg = RGB(57,57,57);
	emcScene->colUnwrapTextShadow = RGB(40,40,40);
	emcScene->rowWidth = 145;
	emcScene->setNumItems(5);
	emcScene->setItem(ID_MENU_SCENE_LOAD				, "LOAD", true);
	emcScene->setItem(ID_MENU_SCENE_MERGE				, "MERGE", false);
	emcScene->setItem(ID_MENU_SCENE_SAVE				, "SAVE", false);
	emcScene->setItem(ID_MENU_SCENE_SAVE_WITH_TEXTURES	, "SAVE WITH TEXTURES", false);
	emcScene->setItem(ID_MENU_SCENE_SAVE_IMAGE			, "SAVE IMAGE", false);
	emcScene->selected = -1;
	emcScene->maxShownRows = 5;
	emcScene->setHeader("SCENE");
	emcScene->align = EM2ComboBox::ALIGN_LEFT;
	emcScene->alignUnwrap = EM2ComboBox::ALIGN_CENTER;
	emcScene->shiftUnwrapRight = -11;

	HWND hMenuHelp = CreateWindow("EM2ComboBox", "HELP", WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS,
					152, 1,   45, 28,    main->hMain, (HMENU)IDC_REND2_MENU_HELP, hDllModule, NULL);

	EM2ComboBox * emcHelp = GetEM2ComboBoxInstance(hMenuHelp);
	emcHelp->bgImage = main->hBmpBg;
	emcHelp->setFontMain(main->fonts->em2combomain, false);
	emcHelp->setFontUnwrapped(main->fonts->em2combounwrap, false);
	emcHelp->colUnwrapBg = RGB(57,57,57);
	emcHelp->colUnwrapTextShadow = RGB(40,40,40);
	emcHelp->rowWidth = 105;
	emcHelp->setNumItems(4);
	emcHelp->maxShownRows = 4;
	emcHelp->setItem(ID_MENU_HELP_ABOUT, "ABOUT", true);
	emcHelp->setItem(ID_MENU_HELP_LICENSE, "LICENSE", true);
	emcHelp->setItem(ID_MENU_HELP_NOX_FORUM, "NOX FORUM", true);
	emcHelp->setItem(ID_MENU_HELP_NOX_PROFILE, "LOCAL PROFILE", true);
	emcHelp->selected = -1;
	emcHelp->setHeader("HELP");
	emcHelp->align = EM2ComboBox::ALIGN_LEFT;
	emcHelp->alignUnwrap = EM2ComboBox::ALIGN_CENTER;


	HWND hMenuPresets = CreateWindow("EM2ComboBox", "HELP", WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS,
					75, 1,   65, 28,    main->hMain, (HMENU)IDC_REND2_MENU_PRESETS, hDllModule, NULL);
	EM2ComboBox * emcPresets = GetEM2ComboBoxInstance(hMenuPresets);
	emcPresets->bgImage = main->hBmpBg;
	emcPresets->setFontMain(main->fonts->em2combomain, false);
	emcPresets->setFontUnwrapped(main->fonts->em2combounwrap, false);
	emcPresets->colUnwrapBg = RGB(57,57,57);
	emcPresets->colUnwrapTextShadow = RGB(40,40,40);
	emcPresets->rowWidth = 150;
	emcPresets->setNumItems(4);
	emcPresets->maxShownRows = 20;
	emcPresets->setItem(0, "ADD PRESET", true);
	emcPresets->setItem(1, "MANAGE PRESETS", true);
	emcPresets->setItem(2, "LOAD FROM FILE", true);
	emcPresets->setItem(3, "SAVE TO FILE", true);
	//emcPresets->setItem(1, "-", false);
	emcPresets->selected = -1;
	emcPresets->setHeader("PRESETS");
	emcPresets->align = EM2ComboBox::ALIGN_LEFT;
	emcPresets->alignUnwrap = EM2ComboBox::ALIGN_CENTER;


	// make tabs
	main->hTabs = CreateWindow("EM2Tabs", "", VIS_ON_START | WS_CHILD | WS_CLIPSIBLINGS,
					1000, 0,   88, 270,    main->hMain, (HMENU)IDC_REND2_TABS, hDllModule, NULL);
	EM2Tabs * em2tabs = GetEM2TabsInstance(main->hTabs);
	em2tabs->setTabsCount(10);
	em2tabs->setTabText(0, "RENDER");
	em2tabs->setTabText(1, "CAMERA");
	em2tabs->setTabText(2, "MATERIALS");
	em2tabs->setTabText(3, "ENVIRONMENT");
	em2tabs->setTabText(4, "LAYERS");
	em2tabs->setTabText(5, "POST");
	em2tabs->setTabText(6, "CORRECTION");
	em2tabs->setTabText(7, "FAKE DOF");
	em2tabs->setTabText(8, "EFFECTS");
	em2tabs->setTabText(9, "OPTIONS");
	em2tabs->setFont(main->fonts->em2tabs, false);
	em2tabs->selected = 0;
	if (!VIS_ON_START)
		em2tabs->selected = -1;
	em2tabs->bgImage = main->hBmpBg;

	main->hTabRender		= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_RENDER),		main->getHWND(), RendererMain2::WndProcRender);
	main->hTabCamera		= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_CAMERA),		main->getHWND(), RendererMain2::WndProcCamera);
	main->hTabMats			= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_MATERIALS),	main->getHWND(), RendererMain2::WndProcMaterials);
	main->hTabEnv			= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_ENVIRONMENT),	main->getHWND(), RendererMain2::WndProcEnvironment);
	main->hTabBlend			= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_BLEND),		main->getHWND(), RendererMain2::WndProcBlend);
	main->hTabPost			= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_POST),			main->getHWND(), RendererMain2::WndProcPost);
	main->hTabCorrection	= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_CORRECTION),	main->getHWND(), RendererMain2::WndProcCorrection);
	main->hTabFakeDof		= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_FAKEDOF),		main->getHWND(), RendererMain2::WndProcFakeDof);
	main->hTabEffects		= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_EFFECTS),		main->getHWND(), RendererMain2::WndProcEffects);
	main->hTabOptions		= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_OPTIONS),		main->getHWND(), RendererMain2::WndProcOptions);
	main->hTopPanel			= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_TOP_PANEL),	main->getHWND(), RendererMain2::WndProcTopPanel);
	main->hBottomPanel		= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_BOTTOM_PANEL),	main->getHWND(), RendererMain2::WndProcBottomPanel);
	main->hRegionsPanel		= CreateDialog(hDllModule, MAKEINTRESOURCE(IDD_RENDERER2_REGIONS),		main->getHWND(), RendererMain2::WndProcRegions);
	ShowWindow(main->hRegionsPanel, SW_HIDE);

	if (!VIS_ON_START)
	{
		ShowWindow(main->hBottomPanel, SW_HIDE);
		ShowWindow(main->hTopPanel, SW_HIDE);
		main->rightPanelOpened = false;
	}

	ShowWindow(main->hTabRender		, SW_HIDE);
	ShowWindow(main->hTabCamera		, SW_HIDE);
	ShowWindow(main->hTabMats		, SW_HIDE);
	ShowWindow(main->hTabEnv		, SW_HIDE);
	ShowWindow(main->hTabBlend		, SW_HIDE);
	ShowWindow(main->hTabPost		, SW_HIDE);
	ShowWindow(main->hTabCorrection	, SW_HIDE);
	ShowWindow(main->hTabFakeDof	, SW_HIDE);
	ShowWindow(main->hTabEffects	, SW_HIDE);
	ShowWindow(main->hTabOptions, SW_HIDE);


	// make show/hide panels button
	main->hButtonMain = CreateWindow("EM2ImgButton", "", VIS_ON_START | WS_CHILD | WS_CLIPSIBLINGS,
					400, NOX_MAIN_HEIGHT/2,   MBTN_W, MBTN_H,    main->hMain, (HMENU)IDC_REND2_BUTTON_SHOWHIDE_PANEL, hDllModule, NULL);
	main->hButtonRegions = CreateWindow("EM2ImgButton", "", VIS_ON_START | WS_CHILD | WS_CLIPSIBLINGS,
					400, NOX_MAIN_HEIGHT/2,   RBTN_W, RBTN_H,    main->hMain, (HMENU)IDC_REND2_BUTTON_SHOWHIDE_REGIONS, hDllModule, NULL);
	EM2ImgButton * btnMain = GetEM2ImgButtonInstance(main->hButtonMain);
	btnMain->bgImage = main->hBmpBg;
	btnMain->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
	btnMain->pNx = MBTN_LN_X;
	btnMain->pNy = MBTN_LN_Y;
	btnMain->pMx = MBTN_LM_X;
	btnMain->pMy = MBTN_LM_Y;
	btnMain->pCx = MBTN_LC_X;
	btnMain->pCy = MBTN_LC_Y;
	btnMain->pDx = MBTN_LD_X;
	btnMain->pDy = MBTN_LD_Y;
	btnMain->setToolTip("Show/hide tab panel");
	EM2ImgButton * btnReg = GetEM2ImgButtonInstance(main->hButtonRegions);
	btnReg->bgImage = main->hBmpBg;
	btnReg->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
	btnReg->pNx = RBTN_RN_X;
	btnReg->pNy = RBTN_RN_Y;
	btnReg->pMx = RBTN_RM_X;
	btnReg->pMy = RBTN_RM_Y;
	btnReg->pCx = RBTN_RC_X;
	btnReg->pCy = RBTN_RC_Y;
	btnReg->pDx = RBTN_RD_X;
	btnReg->pDy = RBTN_RD_Y;
	btnReg->setToolTip("Show/hide navigation tools");

	// start texts
	char noxnamever[256];
	sprintf_s(noxnamever, 256, "NOX RENDERER %d.%.2d", NOX_VER_MAJOR, NOX_VER_MINOR);
	main->start_texts.ht1_welcome = CreateWindow("EM2Text", "WELCOME TO", WS_CHILD | WS_VISIBLE, 
					0,100, 200, 16, main->getHWND(), (HMENU)IDC_REND2_STEXT_1_WELCOME, hDllModule, NULL);
	main->start_texts.ht2_noxname = CreateWindow("EM2Text", noxnamever, WS_CHILD | WS_VISIBLE, 
					203,100, 200, 16, main->getHWND(), (HMENU)IDC_REND2_STEXT_2_NOXNAME, hDllModule, NULL);

	main->start_texts.ht3_loadLink = CreateWindow("EM2Text", "LOAD", WS_CHILD | WS_VISIBLE, 
					0,130, 200, 16, main->getHWND(), (HMENU)IDC_REND2_STEXT_3_LOAD_LINK, hDllModule, NULL);
	main->start_texts.ht4_noxScene = CreateWindow("EM2Text", "NOX SCENE", WS_CHILD | WS_VISIBLE, 
					203,130, 200, 16, main->getHWND(), (HMENU)IDC_REND2_STEXT_4_NOX_SCENE, hDllModule, NULL);

	main->start_texts.ht7_or = CreateWindow("EM2Text", "OR", WS_CHILD | WS_VISIBLE, 
					203,160, 200, 16, main->getHWND(), (HMENU)IDC_REND2_STEXT_7_OR, hDllModule, NULL);

	main->start_texts.ht5_openLink = CreateWindow("EM2Text", "OPEN", WS_CHILD | WS_VISIBLE, 
					0,190, 200, 16, main->getHWND(), (HMENU)IDC_REND2_STEXT_5_OPEN_LINK, hDllModule, NULL);
	main->start_texts.ht6_SampleScenes = CreateWindow("EM2Text", "SAMPLE SCENES FOLDER", WS_CHILD | WS_VISIBLE, 
					203,190, 200, 16, main->getHWND(), (HMENU)IDC_REND2_STEXT_6_SAMPLESCENES, hDllModule, NULL);

	EM2Text * emtst1 = GetEM2TextInstance(main->start_texts.ht1_welcome);
	emtst1->setFont(main->fonts->em2text, false);
	emtst1->bgImage = main->hBmpBg;
	emtst1->align = EM2Text::ALIGN_RIGHT;
	//emtst1->isItHyperLink = true;
	EM2Text * emtst2 = GetEM2TextInstance(main->start_texts.ht2_noxname);
	emtst2->setFont(main->fonts->em2text, false);
	emtst2->bgImage = main->hBmpBg;
	emtst2->colText = NGCOL_LIGHT_GRAY;
	EM2Text * emtst3 = GetEM2TextInstance(main->start_texts.ht3_loadLink);
	emtst3->setFont(main->fonts->em2text, false);
	emtst3->bgImage = main->hBmpBg;
	emtst3->align = EM2Text::ALIGN_RIGHT;
	emtst3->isItHyperLink = true;
	emtst3->colLink = NGCOL_YELLOW;
	EM2Text * emtst4 = GetEM2TextInstance(main->start_texts.ht4_noxScene);
	emtst4->setFont(main->fonts->em2text, false);
	emtst4->bgImage = main->hBmpBg;
	EM2Text * emtst5 = GetEM2TextInstance(main->start_texts.ht5_openLink);
	emtst5->setFont(main->fonts->em2text, false);
	emtst5->bgImage = main->hBmpBg;
	emtst5->align = EM2Text::ALIGN_RIGHT;
	emtst5->isItHyperLink = true;
	emtst5->colLink = NGCOL_YELLOW;
	EM2Text * emtst6 = GetEM2TextInstance(main->start_texts.ht6_SampleScenes);
	emtst6->setFont(main->fonts->em2text, false);
	emtst6->bgImage = main->hBmpBg;
	EM2Text * emtst7 = GetEM2TextInstance(main->start_texts.ht7_or);
	emtst7->setFont(main->fonts->em2text, false);
	emtst7->bgImage = main->hBmpBg;
	emtst7->align = EM2Text::ALIGN_CENTER;

	// update positions/size
	main->tabsWereChanged();
	main->processResizeMessage(0, MAKELONG(NOX_MAIN_WIDTH,NOX_MAIN_HEIGHT));
	UpdateWindow(main->hMain);
	InvalidateRect(main->hTabRender, NULL, false);

	main->fillEnvTabAll();

	// load mat editor scenes
	Sleep(10);
	bool loadOK = Raytracer::getInstance()->loadMatEditorScenes(NULL);
	Raytracer::getInstance()->curSceneInd = 0;
	Raytracer::getInstance()->curScenePtr = Raytracer::getInstance()->scenes[0];

	Raytracer::getInstance()->logoStamp.loadFromResource(IDB_NOX_LOGO_STAMP);

	InvalidateRect(main->getHWND(), NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

RendererMain2 * RendererMain2::getInstance()
{
	return rmwInstance;
}

//------------------------------------------------------------------------------------------------

HWND RendererMain2::getHWND()
{
	return hMain;
}

//------------------------------------------------------------------------------------------------

ATOM RendererMain2::registerMyClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	HICON sIcon = LoadIcon(hDllModule, MAKEINTRESOURCE(IDI_ICON1));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= RendererMain2::WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= sIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= 0;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "NOXMainWindow2";
	wcex.hIconSm		= sIcon;
	return RegisterClassEx(&wcex);
}

//------------------------------------------------------------------------------------------------

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark)
{
	int ss = w*h*4;
	unsigned char * buffer = (unsigned char *)malloc(ss);
	if (!buffer)
		return 0;

	unsigned char lr = GetRValue(colLight);
	unsigned char lg = GetGValue(colLight);
	unsigned char lb = GetBValue(colLight);
	unsigned char dr = GetRValue(colDark);
	unsigned char dg = GetGValue(colDark);
	unsigned char db = GetBValue(colDark);

	for (int i=0; i<w*h; i++)
	{
		int y = i/w;
		int a = (i%w-y+h)%5;
		if (a==3||a==4)
		{
			buffer[i*4+0] = dr;
			buffer[i*4+1] = dg;
			buffer[i*4+2] = db;
			buffer[i*4+3] = 255;
		}
		else
		{
			buffer[i*4+0] = lr;
			buffer[i*4+1] = lg;
			buffer[i*4+2] = lb;
			buffer[i*4+3] = 255;
		}
	}

	HBITMAP hbitmap = CreateBitmap(w,h, 1, 32, buffer);

	free(buffer);

	return hbitmap;
}

//------------------------------------------------------------------------------------------------

HBITMAP loadImageFromResMakeTiled(HMODULE hMod, WORD idRes, int w, int h)
{
	ImageByteBuffer * img = new ImageByteBuffer();
	CHECK(img);
	bool loaded = img->loadFromResource(hMod, idRes);
	CHECK(loaded);
	CHECK(img->width>0);
	CHECK(img->height>0);
	CHECK(img->imgBuf);

	int sw = img->width;
	int sh = img->height;
	int ss = w*h*4;
	unsigned char * buffer = (unsigned char *)malloc(ss);
	if (!buffer)
	{
		img->freeBuffer();
		delete img;
		return 0;
	}

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			COLORREF c4 = img->imgBuf[y%sh][x%sw];
			unsigned char cr = GetRValue(c4);
			unsigned char cg = GetGValue(c4);
			unsigned char cb = GetBValue(c4);
			int i = x+y*w;
			buffer[i*4+0] = cb;
			buffer[i*4+1] = cg;
			buffer[i*4+2] = cr;
			buffer[i*4+3] = 255;
		}
	}

	img->freeBuffer();
	delete img;

	HBITMAP hbitmap = CreateBitmap(w,h, 1, 32, buffer);

	free(buffer);

	return hbitmap;
}

//------------------------------------------------------------------------------------------------

HBITMAP loadImageMakeTiled(char * filename, int w, int h)
{
	CHECK(filename);

	ImageByteBuffer * img = new ImageByteBuffer();
	CHECK(img);
	bool loaded = img->loadFromWindowsFormat(filename);
	CHECK(loaded);
	CHECK(img->width>0);
	CHECK(img->height>0);
	CHECK(img->imgBuf);


	int sw = img->width;
	int sh = img->height;
	int ss = w*h*4;
	unsigned char * buffer = (unsigned char *)malloc(ss);
	if (!buffer)
	{
		img->freeBuffer();
		delete img;
		return 0;
	}


	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			COLORREF c4 = img->imgBuf[y%sh][x%sw];
			unsigned char cr = GetRValue(c4);
			unsigned char cg = GetGValue(c4);
			unsigned char cb = GetBValue(c4);
			int i = x+y*w;
			buffer[i*4+0] = cb;
			buffer[i*4+1] = cg;
			buffer[i*4+2] = cr;
			buffer[i*4+3] = 255;
		}
	}

	img->freeBuffer();
	delete img;

	HBITMAP hbitmap = CreateBitmap(w,h, 1, 32, buffer);

	free(buffer);

	return hbitmap;

}

//------------------------------------------------------------------------------------------------

void RendererMain2::createFirstScene()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->scenes.add(new Scene);
	rtr->scenes.createArray();
	rtr->curSceneInd = rtr->scenes.objCount-1;
	rtr->curScenePtr = rtr->scenes.objArray[rtr->curSceneInd];
	sceneID = rtr->curSceneInd;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::saveImage(char * filename)
{
	if (!filename)
	{
		Logger::add("No file selected.");
		return false;
	}

	int l = (int)strlen(filename);
	if (l < 5)
	{
		char buf[64];
		sprintf_s(buf, 64, "Wrong filename: \"%s\"", filename);
		Logger::add(buf);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (!cam)
		return false;
	int aa = cam->aa;
	ImageModifier * iMod = &cam->iMod;
	ImageBuffer * iBuff;
	EMPView * empv = GetEMPViewInstance(hViewport);
	if (!empv->otherSource)
		iBuff = empv->imgBuff;
	else
		iBuff = empv->otherSource;

	ImageByteBuffer * ibbuf = empv->byteBuff;
	if (!ibbuf)
		return false;

	char l0,l1,l2,l3,l4;
	l0 = filename[l-4];
	l1 = filename[l-3];
	l2 = filename[l-2];
	l3 = filename[l-1];
	l4 = filename[l-5];

	int filetype = 0;	// unspecified
	if ( (l0 == '.') && (l1 == 'e' || l1 == 'E')  &&  (l2 == 'x' || l2 == 'X')  &&  (l3 == 'r' || l3 == 'R'))
		filetype = 1;	// exr
	if ( (l0 == '.') && (l1 == 'b' || l1 == 'B')  &&  (l2 == 'm' || l2 == 'M')  &&  (l3 == 'p' || l3 == 'P'))
		filetype = 2;	// bmp
	if ( (l0 == '.') && (l1 == 'p' || l1 == 'P')  &&  (l2 == 'n' || l2 == 'N')  &&  (l3 == 'g' || l3 == 'G'))
		filetype = 3;	// png
	if ( (l0 == '.') && (l1 == 'j' || l1 == 'J')  &&  (l2 == 'p' || l2 == 'P')  &&  (l3 == 'g' || l3 == 'G'))
		filetype = 4;	// jpg
	if ( (l4 == '.') && (l0 == 't' || l0 == 'T')  &&  (l1 == 'i' || l1 == 'I')  &&  (l2 == 'f' || l2 == 'F')  &&  (l3 == 'f' || l3 == 'F'))
		filetype = 5;	// tiff

	bool saveOK = false;

	Logger::add("Saving:");
	Logger::add(filename);
	rtr->curScenePtr->notifyProgress("Saving image", 0);

	NoxEXIFdata exif;
	exif.iso = (short)(int)(cam->ISO*1.00001f);
	exif.focalNom = (unsigned int)(0.5f * 36 / tan(cam->angle * PI / 360.0f));
	exif.focalDenom = 1;
	exif.shutterNom = 10000;
	exif.shutterDenom = (unsigned int)(cam->shutter*10000);
	exif.fNumberDenom = 100;
	exif.fNumberNom = (unsigned int)(cam->aperture*100);


	switch (filetype)
	{
		case 1:
			saveOK = iBuff->saveAsEXR(filename, *iMod, aa);
			break;
		case 2:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 1, &exif);
			break;
		case 3:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 2, &exif);
			break;
		case 4:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 4, &exif);
			break;
		case 5:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 3, &exif);
			break;
		default:
			saveOK = false;
			break;
	}

	if (!saveOK)
	{
		Logger::add("Not saved!");
		MessageBox(NULL, "Image not saved!", "Error", 0);
		return false;
	}
	else
	{
		rtr->curScenePtr->notifyProgress("Image saved", 0);
		Logger::add("Saved");
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::loadSceneInNewWindow(char * filename)
{
	CHECK(defaultDirectory);
	CHECK(dirExists(defaultDirectory));
	char runNoxCommand[2048];
	#ifdef _WIN64
		sprintf_s(runNoxCommand, 2048, "%s\\bin\\64\\Renderer64.exe", defaultDirectory);
	#else
		sprintf_s(runNoxCommand, 2048, "%s\\bin\\32\\Renderer32.exe", defaultDirectory);
	#endif
	CHECK(fileExists(runNoxCommand));
	char wholeCommand[4096];
	sprintf_s(wholeCommand, 4096, "\"%s\" \"%s\"", runNoxCommand, filename);


	PROCESS_INFORMATION processInformation;
	STARTUPINFO startupInfo;
	memset(&processInformation, 0, sizeof(processInformation));
	memset(&startupInfo, 0, sizeof(startupInfo));
	startupInfo.cb = sizeof(startupInfo);

	BOOL ok = CreateProcess(runNoxCommand, wholeCommand, 
			NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, NULL, 
			&startupInfo, &processInformation);


	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::callLoadSceneDialog(HWND hWnd, char * folder)
{
	char * filename = openFileDialog(hWnd ? hWnd : getHWND(), 
		"Scene file (*.nxs *.nox)\0*.nxs;*.nox\0Scene binary file (*.nox)\0*.nox\0Scene XML file (*.nxs)\0*.nxs\0All files\0*.*\0", "Load scene", "nox", 1, folder);
	if (filename)
	{
		if (cantloadscene)
		{
			if (!fileExists(filename))
				return false;
			bool ok = loadSceneInNewWindow(filename);
		}
		else
		{
			loadSceneAsync(filename, false);
		}
	}
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::tryLoadSceneDroppedAsync(char * filename, bool andStartRendering)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	return rMain->loadSceneAsync(filename, andStartRendering);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::loadSceneAsync(char * filename, bool andStartRendering)
{
	if (!filename)
		return false;
	Raytracer * rtr = Raytracer::getInstance();
	ThreadWindow2 * tw = new ThreadWindow2(); 
	tw->createWindow(hMain, hDllModule);
	tw->setWindowTitle("Loading scene");
	rtr->curScenePtr->startAfterLoad = andStartRendering;
	rtr->curScenePtr->abortThreadPtr = &(tw->aborted);
	rtr->curScenePtr->registerProgressCallback(ThreadWindow2::notifyProgressBarCallback, 50);
						
	DWORD threadId;
	HANDLE hThr = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMain2::LoadSceneThreadProc), (LPVOID)filename, 0, &threadId);

	if (!hThr)
	{
		unlockAfterSceneLoadFailed();
	}

	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMain2::LoadSceneThreadProc(LPVOID lpParameter)
{
	if (!lpParameter)
		return 0;	// scene name null

	RendererMain2 * rMain = RendererMain2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();
	char * filename = (char *)lpParameter;

	bool ok = rMain->loadScene(filename);

	ThreadWindow2 * tw = ThreadWindow2::getCurrentInstance();
	if (tw)
	{
		rtr->curScenePtr->abortThreadPtr = NULL;
		tw->closeWindow();
		delete tw;
	}

	Raytracer::getInstance()->curScenePtr->registerProgressCallback(NULL, 100);

	if (ok)
	{
		rMain->unlockAfterSceneLoaded();
		if (rtr->curScenePtr->startAfterLoad)
		rMain->startRender(false);
	}
	else
		rMain->unlockAfterSceneLoadFailed();

	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::loadScene(char * filename)
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->sscene.useSunSky = false;

	UpdateWindow(hMain);
	if (!filename)
	{
		Logger::add("File was not selected.");
		return false;
	}
	unsigned int fnlen = (unsigned int)strlen(filename);
	if (fnlen < 5)
	{
		Logger::add("Something wrong with file name.");
		return false;
	}

	bool isBinaryExt = false;
	if (	(filename[fnlen-3]=='n'  ||  filename[fnlen-3]=='N')  &&
			(filename[fnlen-2]=='o'  ||  filename[fnlen-2]=='O')  &&
			(filename[fnlen-1]=='x'  ||  filename[fnlen-1]=='X'))
		isBinaryExt = true;

	// can't load any other scene
	cantloadscene = true;
	EM2ComboBox * emcmenu = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_REND2_MENU_SCENE));
	emcmenu->enabledItems[ID_MENU_SCENE_LOAD] = true;
	emcmenu->setItem(ID_MENU_SCENE_LOAD, "LOAD NEW", true);
	EMPView * empv = GetEMPViewInstance(hViewport);

	int err = 0;
	if (isBinaryExt)
	{
		err = rtr->curScenePtr->loadSceneFromBinary(filename);
		if (err)
			MessageBox(0, "Binary NOX scene load failed", "", 0);
	}
	else
		err = rtr->curScenePtr->loadSceneFromXML(filename);

	if (err == 0)
	{
		Logger::add("Scene loaded.");
		Logger::add("Initializing");
		rtr->curScenePtr->postLoadInitialize(false);

		if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
		{
			Logger::add("Aborted by user!!!");
			return false;
		}
		rtr->curScenePtr->decodeSceneStrings();

		// fill gui
		updateTimersFromRtrToGUI();
		fillRenderTab(rtr->curScenePtr->getActiveCamera());
		fillCameraTabAll();
		updateCamerasStateOnList();

		SendMessage(hTabMats, 14001, 0, 0);

		fillLayersTabAll();
		fillEnvTabAll();
		fillPostTabAll(&rtr->curScenePtr->getActiveCamera()->iMod);
		fillCorrectionTabAll(&rtr->curScenePtr->getActiveCamera()->iMod);
		fillFakeDOFTabAll(&rtr->curScenePtr->getActiveCamera()->fMod);
		fillEffectsTabAll(rtr->curScenePtr->getActiveCamera(), &rtr->curScenePtr->getActiveCamera()->fMod);
		fillStatusBar();
		fillPresetsCombo(GetDlgItem(getHWND(), IDC_REND2_MENU_PRESETS));

		// unlock gui
		emcmenu->enabledItems[ID_MENU_SCENE_MERGE] = true;
		emcmenu->enabledItems[ID_MENU_SCENE_SAVE] = true;
		emcmenu->enabledItems[ID_MENU_SCENE_SAVE_WITH_TEXTURES] = true;
		emcmenu->enabledItems[ID_MENU_SCENE_SAVE_IMAGE] = true;
		emcmenu->enabledItems[ID_MENU_SCENE_SEPARATOR_1] = false;
		emcmenu->enabledItems[ID_MENU_SCENE_LOAD_PRESET] = true;
		emcmenu->enabledItems[ID_MENU_SCENE_SAVE_PRESET] = true;

		// camera buffers
		Camera * cam = rtr->curScenePtr->getActiveCamera();
		EMPView * empv = GetEMPViewInstance(hViewport);
		if (cam)
		{
			if (!cam->imgBuff)
				cam->imgBuff = new ImageBuffer();
			cam->imgBuff->allocBuffer(cam->width*cam->aa , cam->height*cam->aa);

			if (empv)
			{
				if (!empv->byteBuff)
					empv->byteBuff = new ImageByteBuffer();
				empv->byteBuff->allocBuffer(cam->width, cam->height);
				empv->byteBuff->clearBuffer();
				empv->otherSource = cam->imgBuff;
				empv->imgMod->copyDataFromAnother(&cam->iMod);
			}
		}

		showAllPanelsAfterSceneLoaded();
		char titlename[2048];
		char * ofname = getOnlyFile(filename);
		sprintf_s(titlename, 2048, "NOX %d.%.2d - %s", NOX_VER_MAJOR, NOX_VER_MINOR, ofname);
		SetWindowText(getHWND(), titlename);


		refreshRender(NULL);	// progress window should be still opened, so no circle/thread

		runTextureWatcherThread();
	}
	else	// failed loading
	{
		if (!isBinaryExt)
		{
			char xbuf[256];
			if (err > 0)
			{
				sprintf_s(xbuf, 256, "Scene not loaded! Watch line %d.", err);
				Logger::add(xbuf);
			}
			else
				Logger::add("XML structure inconsistent.");
			rtr->curScenePtr->notifyProgress("Scene not loaded", 0);
		}
	}

	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMain2::MergeSceneThreadProc(LPVOID lpParameter)
{
	if (!lpParameter)
		return 0;	// scene name null

	RendererMain2 * rMain = RendererMain2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();
	char * filename = (char *)lpParameter;

	bool ok = rMain->mergeScene(filename);

	ThreadWindow2 * tw = ThreadWindow2::getCurrentInstance();
	if (tw)
	{
		rtr->curScenePtr->abortThreadPtr = NULL;
		tw->closeWindow();
		delete tw;
	}

	Raytracer::getInstance()->curScenePtr->registerProgressCallback(NULL, 100);

	rMain->unlockAfterSceneLoaded();

	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::mergeSceneAsync(char * filename)
{
	if (!filename)
		return false;
	if (!fileExists(filename))
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	ThreadWindow2 * tw = new ThreadWindow2(); 
	tw->createWindow(hMain, hDllModule);
	tw->setWindowTitle("Loading scene for merge");
	rtr->curScenePtr->abortThreadPtr = &(tw->aborted);
	rtr->curScenePtr->registerProgressCallback(ThreadWindow2::notifyProgressBarCallback, 50);
						
	DWORD threadId;
	HANDLE hThr = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMain2::MergeSceneThreadProc), (LPVOID)filename, 0, &threadId);
	if (!hThr)
	{
		unlockAfterSceneLoaded();
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::mergeScene(char * filename)
{
	if (!filename)
		return false;
	if (!fileExists(filename))
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	if (!rtr->curScenePtr->mergeSceneFromBinary(filename))
		return false;

	refreshRenderOnThread();

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::saveScene(char * filename, bool withTextures, bool silentMode)
{
	Logger::add("Saving scene.");
	Raytracer * rtr = Raytracer::getInstance();

	if (!filename)
	{
		Logger::add("Error: Filename empty.");
		return false;
	}
	if (strlen(filename) < 5)
	{
		Logger::add("Error: Something wrong with file name, too short.");
		return false;
	}

	unsigned int fnlen = (unsigned int)strlen(filename);
	bool isBinaryExt = false;
	if (	(filename[fnlen-3]=='n'  ||  filename[fnlen-3]=='N')  &&    
			(filename[fnlen-2]=='o'  ||  filename[fnlen-2]=='O')  &&    
			(filename[fnlen-1]=='x'  ||  filename[fnlen-1]=='X'))
		isBinaryExt = true;

	bool addCamBuffs = true;
	if (withTextures)
	{
		char * fdir = getDirectory(filename);
		char * tsdir = getOnlyFile(filename);
		char * sdir = NULL;
		if (tsdir  &&  strlen(tsdir)>0)
		{
			int sts = (int)strlen(tsdir);
			sdir = (char*)malloc(sts+32);
			if (sdir)
				sprintf_s(sdir, sts+32, "%s textures", tsdir);
		}

		bool changed = rtr->curScenePtr->changeTexturePaths(fdir, sdir);

		if (fdir)
			free(fdir);
		if (tsdir)
			free(tsdir);
		if (sdir)
			free(sdir);

		int addbb = MessageBox(hMain, "Add rendered buffers to saved file?\nThis may greatly increase file size.", "Exporting", MB_YESNO|MB_ICONQUESTION);
		if (IDYES==addbb)
			addCamBuffs = true;
		else
			addCamBuffs = false;
	}

	updateTimersFromGUItoRtr();

	int err;
	if (isBinaryExt)
		err = rtr->curScenePtr->saveSceneToBinary(filename, addCamBuffs, silentMode);
	else
		err = rtr->curScenePtr->saveSceneToXML(filename, addCamBuffs);

	if (withTextures)
	{
		// release relative paths in texture file names
		rtr->curScenePtr->releaseRelativePaths();
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::loadPreset(HWND hWnd, char * filename, bool forceUseAll)
{
	CHECK(filename);
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	CHECK(cam);
	EMPView * empv = GetEMPViewInstance(hViewport);
	CHECK(empv);
	CHECK(empv->imgMod);

	XMLScene xmlsc;
	ImageModifier iMod;
	FinalModifier fMod;
	BlendSettings blend;
	Texture texObstacle, texDiaphragm;
	bool ok = xmlsc.loadPreset(filename, &iMod, &fMod, &blend, &texObstacle, &texDiaphragm);
	if (!ok)
		return false;

	PresetChoose pc;
	PresetChoose * pc2 = NULL;

	if (!forceUseAll)
		pc2 = (PresetChoose *)DialogBoxParam(hDllModule,
				MAKEINTRESOURCE(IDD_RENDERER2_LOAD_PRESETS), hWnd, PresetsLoadDlgProc,(LPARAM)( &pc ));

	if (forceUseAll  ||  (pc2 && pc2->post))
	{
		empv->imgMod->copyDataFromAnother(&iMod, true, false);
		cam->iMod.copyDataFromAnother(&iMod, true, false);
	}
	if (forceUseAll  ||  (pc2 && pc2->correction))
	{
		empv->imgMod->copyDataFromAnother(&iMod, false, true);
		cam->iMod.copyDataFromAnother(&iMod, false, true);
	}
	if (forceUseAll  ||  (pc2 && pc2->fakedof))
	{
		cam->fMod.copyDataFromOther(&fMod, true, false);
	}
	if (forceUseAll  ||  (pc2 && pc2->effects))
	{
		cam->fMod.copyDataFromOther(&fMod, false, true);
	}
	if (forceUseAll  ||  (pc2 && pc2->layers))
	{
		rtr->curScenePtr->blendSettings.copyFrom(&blend);
	}

	fillPostTabAll(&cam->iMod);
	fillCorrectionTabAll(&cam->iMod);
	fillFakeDOFTabAll(&cam->fMod);
	fillEffectsTabAll(cam, &cam->fMod);
	fillLayersTabAll();

	refreshRenderOnThread();

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::savePreset(char * filename)
{
	XMLScene xmlsc;
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	CHECK(cam);
	bool ok = xmlsc.savePreset(filename, cam, rtr->curScenePtr->blendSettings);
	return ok;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::startRender(bool resume)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Camera * cam = sc->getActiveCamera();
	CHECK(cam);

	int prevsec = cam->getRendTime(GetTickCount());
	if (prevsec>60  &&  !resume)
	{
		int r = MessageBox(getHWND(), "Restart rendering? If you press \"OK\" you will lose current progress. Continue?", "Warning", MB_OKCANCEL);
		if (r!=IDOK)
		{
			unlockOnStopRender();
			return false;
		}
	}

	cam->blendBits = sc->evalBlendBits();
	cam->bucket_hwnd_image = hViewport;

	EM2EditSpin * emesWidth = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_WIDTH));
	EM2EditSpin * emesHeight = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_HEIGHT));
	EM2EditSpin * emesAA = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AA));
	cam->width = emesWidth->intValue;
	cam->height = emesHeight->intValue;
	cam->aa = emesAA->intValue;

	EMPView * empv = GetEMPViewInstance(hViewport);
	empv->rgnPtr = &(cam->staticRegion);
	bool initOK = cam->initializeCamera(!resume);
	if (!initOK)
	{
		rtr->curScenePtr->notifyProgress("Stop forced.", 0);
		unlockOnStopRender();
		return false;
	}

	lockOnStartRender();
	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();
	empv->byteBuff->allocBuffer(cam->width, cam->height);
	empv->byteBuff->clearBuffer();
	empv->otherSource = cam->imgBuff;
	if (!resume)
		cam->staticRegion.overallHits = 0;

	sc->thrM->stopThreads();
	sc->thrM->disposeThreads();
	int prn = sc->thrM->getProcessorsNumber();
	if (prn < 1)
		prn = 1;

	#ifdef DEBUG_ONE_THREAD
		prn = 1;
	#endif

	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		MaterialNox * m = rtr->curScenePtr->mats[i];
		if (m)
			m->prepareToRender();
	}

	if (!resume)
		cam->rendTime = 0;

	cam->startTick = GetTickCount();
	rtr->curScenePtr->sscene.sunsky->copyFrom(
		&(GetEMEnvironmentInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_EARTH))->sunsky));
	
	cam->nowRendering = true;

	if (cam->buckets)
	{
		empv->showBuckets = true;
		cam->buckets->nThr = prn;
		cam->buckets->coordsThreads = (int *)malloc(cam->buckets->nThr*sizeof(int)*4);
		for (int i=0; i<cam->buckets->nThr*4; i++)
			cam->buckets->coordsThreads[i] = -1;
		empv->bucketsCoords = cam->buckets->coordsThreads;
		empv->nBuckets = cam->buckets->nThr;
	}
	stoppingPassOn = false;

	sc->thrM->registerStoppedCallback(stoppedRenderingCallback);
	sc->thrM->initThreads(prn, false);
	sc->thrM->runThreads();
	sc->nowRendering = true;

	totalPixelSamples = 0;
	runAutoRefreshThread();
	runAutoStopThread();

	updateCamerasStateOnList();

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::stopRender()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	sc->thrM->showThreadsDlls();

	sc->thrM->stopThreads();
	sc->thrM->resumeThreads();
	sc->thrM->disposeThreads();

	KillTimer(hTabRender, TIMER_STOP_ID);

	unlockOnStopRender();

	sc->nowRendering = false;
	Camera * cam = sc->getActiveCamera();
	CHECK(cam);
	cam->nowRendering = false;
	int ss = (GetTickCount() - cam->startTick)/1000;
	cam->rendTime += ss;
	cam->lastRendTime = ss;
	if (cam->buckets)
	{
		cam->buckets->nThr = 0;
		if (cam->buckets->coordsThreads)
			free(cam->buckets->coordsThreads);
		cam->buckets->coordsThreads = NULL;	
	}

	EMPView * empv = GetEMPViewInstance(hViewport);
	empv->bucketsCoords = NULL;
	empv->nBuckets = 0;
	empv->showBuckets = false;

	refreshCounterInStatus(true);

	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::stoppedRenderingCallback(void)
{
	getInstance()->stoppingPassOn = true;
}

//------------------------------------------------------------------------------------------------

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

//------------------------------------------------------------------------------------------------

bool RendererMain2::refreshRender(RedrawRegion * rgn)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Camera * cam = sc->getActiveCamera();
	CHECK(cam);
	EMPView * empv = GetEMPViewInstance(hViewport);
	static bool nowRefreshing = false;
	if (nowRefreshing)
		return false;
	nowRefreshing = true;
	bool ok = updateRenderedImage(empv, true, false, rgn, false);
	nowRefreshing = false;

	turnOffDofDephtModes();

	if (!rgn)	// STAMPS
	{
		char * tst = createTimeStamp();
		if (tst)
		{
			if (empv->byteBuff)  
			{
				if (empv->byteBuff->timestamp)
					free(empv->byteBuff->timestamp);
				empv->byteBuff->timestamp = tst;
			}
		}

		if (sc->sscene.drawStampTime  &&  empv->byteBuff)
			empv->byteBuff->applyTimeStamp();
		if (sc->sscene.drawStampLogo  &&  empv->byteBuff)
			empv->byteBuff->applyLogoStamp(&rtr->logoStamp);
	}



	InvalidateRect(hViewport, NULL, false);

	refreshTimeInStatus();

	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI refreshRenderThreadProc(LPVOID lpParameter)
{
	Sleep(10);
	RendererMain2 * rMain = RendererMain2::getInstance();
	rMain->showRefreshingInStatus(true);
	rMain->refreshRender(NULL);
	rMain->showRefreshingInStatus(false);
	circleActive = false;
	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::refreshRenderOnThread()
{
	RECT crect;
	GetClientRect(hViewport, &crect);
	openAnimCircleDialog(getHWND(), (crect.right)/2, (crect.bottom)/2);

	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(refreshRenderThreadProc),
            (LPVOID)0,
            0,
            &threadID);
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::runMaterialEditor(int matnum)
{
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(matnum>=0);
	CHECK(matnum<rtr->curScenePtr->mats.objCount);

	rtr->curScenePtr->copyToEditedMaterial(matnum);

	MatEditor2 * matedit = MatEditor2::getInstance();
	bool accept = matedit->createWindow(hDllModule, hMain, MatEditor2::MODE_RENDERER, NULL);

	rtr->curSceneInd = sceneID;
	rtr->curScenePtr = rtr->scenes[sceneID];

	if (accept)
	{
		bool ok = rtr->curScenePtr->copyFromEditedMaterial(matnum);
		rtr->curScenePtr->deleteEditedMaterial();
		matControlsUpdateOneMat(matnum);
		matControlsUpdateMats();
	}

	rtr->curScenePtr->texManager.checkReloadModifiedTextures(getHWND(), true);

	rtr->curScenePtr->updateSkyportals();
	rtr->curScenePtr->updateLightMeshes();

	rtr->curScenePtr->mats[matnum]->logMaterial();

	return accept;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMain2::RefreshThreadProc(LPVOID lpParameter)
{
	// AUTOREFRESH
	RendererMain2 * rMain = RendererMain2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();

	clock_t lTime = clock();
	clock_t cTime;
	double diff;

	while (rtr->curScenePtr->nowRendering)
	{
		Sleep(200);
		cTime = clock();
		diff  = (cTime - lTime)/(double)CLOCKS_PER_SEC;
		if (diff > rMain->timers.refreshSec)
		{
			if (rMain->timers.refreshOn)
			{
				SendMessage(rMain->getHWND(), 32900, 0,0);
			}
			lTime = clock();
		}
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::refreshCounterInStatus(bool average)
{
	Raytracer * rtr = Raytracer::getInstance();
	
	unsigned long long cnt = 0;
	cnt = rtr->curScenePtr->thrM->collectPerfCounters();
	totalPixelSamples += cnt;
	if (average)
	{
		Camera * cam = rtr->curScenePtr->getActiveCamera();
		int sec = cam ? max(1, cam->lastRendTime) : 1;
		cnt = totalPixelSamples/sec;
	}

	char txt[256];
	if (cnt>10000)
		if (cnt>10000000)
			sprintf_s(txt, 256, "%lldM", cnt/1000000);
		else
			sprintf_s(txt, 256, "%lldK", cnt/1000);
	else
		sprintf_s(txt, 256, "%lld", cnt);

	SetWindowText(GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_SAMPLES), txt);
	SetWindowText(GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_TEXT_SAMPLES_SEC), average ? "AVG SAMPLES/SEC:" : "SAMPLES/SEC:");
}

//------------------------------------------------------------------------------------------------

void RendererMain2::refreshTimeInStatus()
{
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	int sec = cam ? cam->getRendTime(GetTickCount()) : 1;
	int hours = sec/60/60;
	int minutes = (sec/60)%60;
	sec = sec%60;

	char buf[128];
	sprintf_s(buf, 128, "RENDER TIME: %02d:%02d:%02d", hours, minutes, sec);

	EM2Text * emt = GetEM2TextInstance(GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_RENDER_TIME));
	emt->changeCaption(buf);
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMain2::CounterRefreshThreadProc(LPVOID lpParameter)
{
	Raytracer * rtr = Raytracer::getInstance();
	RendererMain2 * rMain = RendererMain2::getInstance();
	while (rtr->curScenePtr->nowRendering)
	{
		rMain->refreshCounterInStatus(false);
		Sleep(1000);
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::runAutoRefreshThread()
{
	DWORD threadID, threadIDc;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMain2::RefreshThreadProc), (LPVOID)0, 0, &threadID);
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMain2::CounterRefreshThreadProc), (LPVOID)0, 0, &threadIDc);
	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMain2::AutoStopThreadProc(LPVOID lpParameter)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	Sleep(rMain->timers.stopSec*1000+500);
	if (rMain->timers.stopOn)
	{
		rMain->stopRender();
		rMain->refreshRender(NULL);	// no circle creation on thread
		if (rMain->runNext.active)
			rMain->doAfterRenderingStuff();
	}
	rMain->hStopThreadId = 0;
	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::runAutoStopThread()
{
	DWORD threadID;
	hStopThreadId = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMain2::AutoStopThreadProc), (LPVOID)0, 0, &threadID);
	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMain2::TextureWatcherThreadProc(LPVOID lpParameter)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();
	while (1)
	{
		Sleep(1000);
		bool inMatEdit = (rtr->curSceneInd != rMain->sceneID);
		bool nowRendering = rtr->curScenePtr->nowRendering;

		if (nowRendering)
			continue;
		if (inMatEdit)
			continue;

		if (rtr->options.texAutoReloadMode>0)
			rtr->curScenePtr->texManager.checkReloadModifiedTextures(rMain->getHWND(), rtr->options.texAutoReloadMode==2);
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::runTextureWatcherThread()
{
	DWORD threadID;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMain2::TextureWatcherThreadProc), (LPVOID)0, 0, &threadID);
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::showAllPanelsAfterSceneLoaded()
{
	ShowWindow(hTopPanel, SW_SHOW);
	ShowWindow(hBottomPanel, SW_SHOW);
	ShowWindow(hViewport, SW_SHOW);
	InvalidateRect(getHWND(), NULL, false);
	ShowWindow(hTabs, SW_SHOW);
	ShowWindow(hButtonRegions, SW_SHOW);
	ShowWindow(hButtonMain, SW_SHOW);

	openCloseRegionsPanel(true);
	EMPView * empv = GetEMPViewInstance(hViewport);
	updateRegionButtonsState(hRegionsPanel, empv);

	InvalidateRect(getHWND(), NULL, false);
	showstartgui = false;
	ShowWindow(start_texts.ht1_welcome, SW_HIDE);
	ShowWindow(start_texts.ht2_noxname, SW_HIDE);
	ShowWindow(start_texts.ht3_loadLink, SW_HIDE);
	ShowWindow(start_texts.ht4_noxScene, SW_HIDE);
	ShowWindow(start_texts.ht5_openLink, SW_HIDE);
	ShowWindow(start_texts.ht6_SampleScenes, SW_HIDE);
	ShowWindow(start_texts.ht7_or, SW_HIDE);

	EM2Tabs * emt = GetEM2TabsInstance(hTabs);
	emt->selected = 0;
	InvalidateRect(hTabs, NULL, false);
	openCloseRightPanel(true);


	return true;
}

//------------------------------------------------------------------------------------------------

void Raytracer::updateOpenCLStateOnGui()
{
	EM2CheckBox * emocl = GetEM2CheckBoxInstance(GetDlgItem(RendererMain2::getInstance()->hTabOptions, IDC_REND2_OPT_OPENCL_POST));
	if (emocl)
	{
		emocl->selected = options.useOpenCLonPost;
		EnableWindow(emocl->hwnd, options.openCLsupported ? TRUE : FALSE);
		InvalidateRect(emocl->hwnd, NULL, FALSE);
	}
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillPresetsCombo(HWND hCombo)
{
	CHECK(hCombo);
	EM2ComboBox * emc = GetEM2ComboBoxInstance(hCombo);
	CHECK(emc);

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	int n = sc->presets.objCount;
	emc->setNumItems(n>0 ? n+5 : 4);
	emc->setItem(0, "ADD PRESET", true);
	emc->setItem(1, "MANAGE PRESETS", true);
	emc->setItem(2, "LOAD FROM FILE", true);
	emc->setItem(3, "SAVE TO FILE", true);
	if (n>0)
		emc->setItem(4, "-", false);

	for (int i=0; i<n; i++)
	{
		PresetPost * pr = sc->presets[i];
		if (pr && pr->name && strlen(pr->name)>0)
			emc->setItem(i+5, pr->name, true);
		else
			emc->setItem(i+5, "Unnamed", true);
	}
	emc->selected = -1;

	return true;
}

//------------------------------------------------------------------------------------------------

DECLDIR void RendererMain2::setLegacyMode(bool legacy)
{
	Logger::add("Setting engine to legacy mode.");
	Raytracer::getInstance()->use_embree = !legacy;
}

//------------------------------------------------------------------------------------------------
