#define _CRT_RAND_S
#include "MatEditor.h"
#include "resource.h"
#include "valuesMinMax.h"
#include "XML_IO.h"
#include "log.h"
#include "Dialogs.h"
#include <mbstring.h>

extern HMODULE hDllModule;

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::showLibrary(bool show)
{
	int comm = show ? SW_SHOW : SW_HIDE;

	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB1), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB2), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB3), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB4), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB5), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB6), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB7), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB_LEFT), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB_RIGHT), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB_LINE), comm);


	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_STONE), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_TILES), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WALLS), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WOOD), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_EMITTER), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FABRIC), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FOOD), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GLASS), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GROUND), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LEATHER), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LIQUIDS), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_SSS), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_MINERALS), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PLASTIC), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_ORGANIC), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PORCELAIN), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_BRICKS), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CONCRETE), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CERAMIC), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CARPAINT), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_METAL), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_OTHER), comm);
	ShowWindow(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_USER), comm);


	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_TEXT23), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB_SEARCH_OK), comm);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LIB_PAGES), comm);

	SetWindowPos(hMain, HWND_TOP, 0, 0,  width+clientToWindowX, clientToWindowY+(show?libShownHeight:libHiddenHeight), SWP_NOMOVE);

	HWND hOK = GetDlgItem(hMain, IDC_MATEDIT_OK);
	HWND hCancel = GetDlgItem(hMain, IDC_MATEDIT_CANCEL);
	SetWindowPos(hOK,     HWND_TOP,  leftOK,      show?libShownOKTop:libHiddenOKTop,  0,0, SWP_NOSIZE);
	SetWindowPos(hCancel, HWND_TOP,  leftCancel,  show?libShownOKTop:libHiddenOKTop,  0,0, SWP_NOSIZE);

}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::uploadMaterial()
{
	MaterialNox * mat = getEditedMaterial();

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->runDialogsUnicode)
	{
		DialogBoxParamW(hDllModule, MAKEINTRESOURCEW(IDD_UPLOAD_MAT), hMain, UploadMatDlgProc,(LPARAM)(mat));
	}
	else
	{
		DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_UPLOAD_MAT), hMain, UploadMatDlgProc,(LPARAM)(mat));
	}



	return true;
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::releaseMatLibBuffers()
{
	for (int i=0; i<7; i++)
	{
		int l;
		switch (i)
		{
			case 0: l = IDC_MATEDIT_LIB1; break;
			case 1: l = IDC_MATEDIT_LIB2; break;
			case 2: l = IDC_MATEDIT_LIB3; break;
			case 3: l = IDC_MATEDIT_LIB4; break;
			case 4: l = IDC_MATEDIT_LIB5; break;
			case 5: l = IDC_MATEDIT_LIB6; break;
			case 6: l = IDC_MATEDIT_LIB7; break;
		}

		HWND hP = GetDlgItem(MatEditorWindow::hMain, l);
		EMPView * empv = GetEMPViewInstance(hP);
		if (!empv)
			continue;
		empv->byteBuff->freeBuffer();
	}
}

//------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedLibCategory(int cat)
{
	unclickAllCategories(cat);
	if (libMode == 1)
		localmatlib.showByCategory(cat, 0);
	else
	{
		if (cat == MatCategory::CAT_USER)
		{
			onlinematlib.downloadXMLinfoByUsername(0);
			return;	// todo
		}
		onlinematlib.downloadXMLinfoByCategory(cat, 0);
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedLibTrayOpen()
{
	EMImgStateButton * emisb = GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH));
	libMode = emisb->stateOn ? 2 : 1;
	iconsGoToOnline(emisb->stateOn);
	if (libMode == 1)	// offline
	{
		onlinematlib.stopAllDownloads();
		switch (localmatlib.queryMode)
		{
			case LocalMatLib::MODE_CATEGORY:
				{
					localmatlib.showByCategory(localmatlib.currentCategory, 0);
					MatEditorWindow::setPageControl(0, max(0, localmatlib.localList[onlinematlib.currentCategory].objCount-1)/7+1);
				}
				break;
			case LocalMatLib::MODE_SEARCH:
				{
					char * sq = getSearchText();
					findMaterialsInLocalLib(sq, 0);
					if (sq)
						free(sq);
					MatEditorWindow::setPageControl(0, max(0, localmatlib.numSearchResults-1)/7+1);
				}
				break;
		}
	}
	if (libMode == 2)	// online
	{
		switch (onlinematlib.queryMode)
		{
			case OnlineMatLib::MODE_CATEGORY:
				{
					onlinematlib.downloadXMLinfoByCategory(onlinematlib.currentCategory, 0);
				}
				break;
			case OnlineMatLib::MODE_MY_MATS:
				{
					onlinematlib.downloadXMLinfoByUsername(0);
				}
				break;
			case OnlineMatLib::MODE_SEARCH:
				{
					char * sq = getSearchText();
					onlinematlib.downloadXMLinfoBySearch(sq, 0);
					if (sq)
						free(sq);
				}
				break;
		}
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedLibOnline()
{
	EMImgStateButton * emisb = GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH));
	libMode = emisb->stateOn ? 2 : 1;
	iconsGoToOnline(emisb->stateOn);
	if (libMode == 1)	// offline
	{
		onlinematlib.stopAllDownloads();
		switch (onlinematlib.queryMode)
		{
			case OnlineMatLib::MODE_CATEGORY:
				{
					localmatlib.showByCategory(onlinematlib.currentCategory, 0);
					MatEditorWindow::setPageControl(0, max(0, localmatlib.localList[onlinematlib.currentCategory].objCount-1)/7+1);
				}
				break;
			case OnlineMatLib::MODE_MY_MATS:
				{
					localmatlib.showByCategory(MatCategory::CAT_USER, 0);
				}
				break;
			case OnlineMatLib::MODE_SEARCH:
				{
					char * sq = getSearchText();
					findMaterialsInLocalLib(sq, 0);
					if (sq)
						free(sq);
					MatEditorWindow::setPageControl(0, max(0, localmatlib.numSearchResults-1)/7+1);
				}
				break;
		}
	}
	if (libMode == 2)	// online
	{
		switch (localmatlib.queryMode)
		{
			case LocalMatLib::MODE_CATEGORY:
				{
					if (localmatlib.currentCategory == MatCategory::CAT_USER)
					{
						onlinematlib.downloadXMLinfoByUsername(0);
						return;
					}
					onlinematlib.downloadXMLinfoByCategory(localmatlib.currentCategory, 0);
				}
				break;
			case LocalMatLib::MODE_MY_MATS:
				{
				}
				break;
			case LocalMatLib::MODE_SEARCH:
				{
					char * sq = getSearchText();
					onlinematlib.downloadXMLinfoBySearch(sq, 0);
					if (sq)
						free(sq);
				}
				break;
		}
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedLibPrev()
{
	EMImgStateButton * emisb = GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH));
	libMode = emisb->stateOn ? 2 : 1;
	if (libMode == 1)	// offline
	{
		switch (localmatlib.queryMode)
		{
			case LocalMatLib::MODE_CATEGORY:
				{
					if (localmatlib.currentCategory > 22   ||   localmatlib.currentCategory < 0)
						return;
					int p = max(localmatlib.currentPage-1, 0);
					localmatlib.showByCategory(localmatlib.currentCategory, p);
					MatEditorWindow::setPageControl(p, max(0, localmatlib.localList[localmatlib.currentCategory].objCount-1)/7+1);
				}
				break;
			case LocalMatLib::MODE_MY_MATS:
				{
				}
				break;
			case LocalMatLib::MODE_SEARCH:
				{
					if (localmatlib.currentPage>0)
					{
						char * sq = getSearchText();
						findMaterialsInLocalLib(sq, localmatlib.currentPage-1);
						if (sq)
							free(sq);
						MatEditorWindow::setPageControl(localmatlib.currentPage+1, max(0, localmatlib.numSearchResults-1)/7+1);
					}
				}
				break;
		}
	}
	if (libMode == 2)	// online
	{
		switch (onlinematlib.queryMode)
		{
			case OnlineMatLib::MODE_CATEGORY:
				{
					int p = max(onlinematlib.currentPage-1, 0);
					onlinematlib.downloadXMLinfoByCategory(onlinematlib.currentCategory, p);
				}
				break;
			case OnlineMatLib::MODE_MY_MATS:
				{
					int p = max(onlinematlib.currentPage-1, 0);
					onlinematlib.downloadXMLinfoByUsername(p);
				}
				break;
			case OnlineMatLib::MODE_SEARCH:
				{
					if (onlinematlib.currentPage>0)
					{
						char * sq = getSearchText();
						onlinematlib.downloadXMLinfoBySearch(sq, onlinematlib.currentPage-1);
						if (sq)
							free(sq);
					}
				}
				break;
		}
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedLibNext()
{
	EMImgStateButton * emisb = GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH));
	libMode = emisb->stateOn ? 2 : 1;
	if (libMode == 1)	// offline
	{
		switch (localmatlib.queryMode)
		{
			case LocalMatLib::MODE_CATEGORY:
				{
					if (localmatlib.currentCategory > 22   ||   localmatlib.currentCategory < 0)
						return;
					int all = localmatlib.localList[localmatlib.currentCategory].objCount/7;
					int p = min(localmatlib.currentPage+1, all); 
					localmatlib.showByCategory(localmatlib.currentCategory, p);
					MatEditorWindow::setPageControl(p, max(0, localmatlib.localList[localmatlib.currentCategory].objCount-1)/7+1);
				}
				break;
			case LocalMatLib::MODE_MY_MATS:
				{
				}
				break;
			case LocalMatLib::MODE_SEARCH:
				{
					if (localmatlib.numSearchResults > (localmatlib.currentPage+1)*7)
					{
						char * sq = getSearchText();
						findMaterialsInLocalLib(sq, localmatlib.currentPage+1);
						if (sq)
							free(sq);
						MatEditorWindow::setPageControl(localmatlib.currentPage+1, max(0, localmatlib.numSearchResults-1)/7+1);
					}
				}
				break;
		}
	}
	if (libMode == 2)	// online
	{
		switch (onlinematlib.queryMode)
		{
			case OnlineMatLib::MODE_CATEGORY:
				{
					int p = min(onlinematlib.currentPage+1, onlinematlib.totalResults/7);
					if (onlinematlib.totalResults < 0)
						p = onlinematlib.currentPage+1;
					onlinematlib.downloadXMLinfoByCategory(onlinematlib.currentCategory, p);
				}
				break;
			case OnlineMatLib::MODE_MY_MATS:
				{
					int p = min(onlinematlib.currentPage+1, onlinematlib.totalResults/7);
					if (onlinematlib.totalResults < 0)
						p = onlinematlib.currentPage+1;
					onlinematlib.downloadXMLinfoByUsername(p);
				}
				break;
			case OnlineMatLib::MODE_SEARCH:
				{
					int p = min(onlinematlib.currentPage+1, onlinematlib.totalResults/7);
					if (onlinematlib.totalResults < 0)
						p = onlinematlib.currentPage+1;
					char * sq = getSearchText();
					onlinematlib.downloadXMLinfoBySearch(sq, p);
					if (sq)
						free(sq);
				}
				break;
		}
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedLibPage()
{
	EMPages * emp = GetEMPagesInstance(GetDlgItem(hMain, IDC_MATEDIT_LIB_PAGES));
	EMImgStateButton * emisb = GetEMImgStateButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ONLINE_SWITCH));
	libMode = emisb->stateOn ? 2 : 1;
	if (!emp  ||  !emisb)
		return;
	int page = emp->curPage-1;
	if (libMode == 1)	// offline
	{
		switch (localmatlib.queryMode)
		{
			case LocalMatLib::MODE_CATEGORY:
				{
					if (localmatlib.currentCategory > 22   ||   localmatlib.currentCategory < 0)
						return;
					int p = min(page, localmatlib.localList[localmatlib.currentCategory].objCount/7);
					localmatlib.showByCategory(localmatlib.currentCategory, p);
				}
				break;
			case LocalMatLib::MODE_MY_MATS:
				{
				}
				break;
			case LocalMatLib::MODE_SEARCH:
				{
					if (localmatlib.numSearchResults > (page)*7)
					{
						char * sq = getSearchText();
						findMaterialsInLocalLib(sq, localmatlib.currentPage+1);
						if (sq)
							free(sq);
					}
				}
				break;
		}
	}
	if (libMode == 2)	// online
	{
		switch (onlinematlib.queryMode)
		{
			case OnlineMatLib::MODE_CATEGORY:
				{
					int p = min(page, onlinematlib.totalResults/7);
					if (onlinematlib.totalResults < 0)
						p = page;
					onlinematlib.downloadXMLinfoByCategory(onlinematlib.currentCategory, p);
				}
				break;
			case OnlineMatLib::MODE_MY_MATS:
				{
					int p = min(page, onlinematlib.totalResults/7);
					if (onlinematlib.totalResults < 0)
						p = page;
					onlinematlib.downloadXMLinfoByUsername(p);
				}
				break;
			case OnlineMatLib::MODE_SEARCH:
				{
					int p = min(page, onlinematlib.totalResults/7);
					if (onlinematlib.totalResults < 0)
						p = page;
					char * sq = getSearchText();
					onlinematlib.downloadXMLinfoBySearch(sq, p);
					if (sq)
						free(sq);
				}
				break;
		}
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::setPageControl(int current, int all)
{
	EMPages * emp = GetEMPagesInstance(GetDlgItem(hMain, IDC_MATEDIT_LIB_PAGES));
	if (!emp)
		return;
	emp->setCurrentPage(current+1, all);
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedLibPreview(int which)
{
	if (which<0 || which>=7)
		return;

	for (int i=0; i<7; i++)
	{
		EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, libIDs[i]));
		if (!empv)
			continue;
		empv->colBorder = GlobalWindowSettings::colorSchemes.EMPViewBorder;
		if (i==which)
		{

			if (libMode == 1)
			{
				if (localmatlib.queryMode == LocalMatLib::MODE_CATEGORY)
					if (localmatlib.currentPage*7+which >= localmatlib.localList[localmatlib.currentCategory].objCount)
						return;
				if (localmatlib.queryMode == LocalMatLib::MODE_SEARCH)
					if (localmatlib.currentPage*7+which >= localmatlib.numSearchResults)
						return;
			}
			if (libMode == 2)
			{
				if (onlinematlib.queryMode == OnlineMatLib::MODE_CATEGORY)
					if (onlinematlib.currentPage*7+which >= onlinematlib.totalResults)
						continue;
				if (onlinematlib.queryMode == OnlineMatLib::MODE_MY_MATS)
					if (onlinematlib.currentPage*7+which >= onlinematlib.totalResults)
						continue;
				if (onlinematlib.queryMode == OnlineMatLib::MODE_SEARCH)
					if (onlinematlib.currentPage*7+which >= onlinematlib.totalResults)
						continue;
			}
			empv->colBorder = RGB(255,0,0);
		}
		InvalidateRect(empv->hwnd, NULL, false);
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::clickedTwiceLibPreview(int which)
{
	if (which<0 || which>=7)
		return;

	if (libMode == 1)
	{
		if (localmatlib.queryMode == LocalMatLib::MODE_CATEGORY)
		{
			if (localmatlib.currentPage*7+which >= localmatlib.localList[localmatlib.currentCategory].objCount)
				return;
			localmatlib.loadMaterial(localmatlib.currentCategory, localmatlib.currentPage*7+which);
			autoCategory = localmatlib.currentCategory;
			autoID = localmatlib.localList[localmatlib.currentCategory][localmatlib.currentPage*7+which].unique;
		}
		if (localmatlib.queryMode == LocalMatLib::MODE_SEARCH)
		{
			if (localmatlib.currentPage*7+which >= localmatlib.numSearchResults)
				return;
			loadMaterial(localmatlib.searchResults[which]->filename);
			autoCategory = MatCategory::CAT_USER;
			autoID = -1;
		}
	}

	if (libMode == 2)
	{
		if (onlinematlib.queryMode == OnlineMatLib::MODE_CATEGORY)
		{
			if (onlinematlib.currentPage*7+which >= onlinematlib.totalResults)
				return;
			OnlineMatLib::OnlineMatInfo * matinfo = &(onlinematlib.mat7Info[which]);
			char * fileName = (char*)DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_DOWNLOAD_MAT), hMain, DownloadMatDlgProc,(LPARAM)(matinfo));
			if (fileName)
			{
				loadMaterial(fileName);
			}
			autoCategory = matinfo->category;
			autoID = matinfo->id;
		}
		if (onlinematlib.queryMode == OnlineMatLib::MODE_MY_MATS)
		{
			if (onlinematlib.currentPage*7+which >= onlinematlib.totalResults)
				return;
			OnlineMatLib::OnlineMatInfo * matinfo = &(onlinematlib.mat7Info[which]);
			char * fileName = (char*)DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_DOWNLOAD_MAT), hMain, DownloadMatDlgProc,(LPARAM)(matinfo));
			if (fileName)
			{
				loadMaterial(fileName);
			}
			autoCategory = matinfo->category;
			autoID = matinfo->id;
		}
		if (onlinematlib.queryMode == OnlineMatLib::MODE_SEARCH)
		{
			if (onlinematlib.currentPage*7+which >= onlinematlib.totalResults)
				return;
			OnlineMatLib::OnlineMatInfo * matinfo = &(onlinematlib.mat7Info[which]);
			char * fileName = (char*)DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_DOWNLOAD_MAT), hMain, DownloadMatDlgProc,(LPARAM)(matinfo));
			if (fileName)
			{
				loadMaterial(fileName);
			}
			autoCategory = matinfo->category;
			autoID = matinfo->id;
		}
	}
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::iconsGoToOnline(bool online)
{
	if (online)
	{
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LOAD_LOCALLIB),	SW_HIDE);
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_DEL_LOCALLIB),		SW_HIDE);
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_DOWNLOADMAT),		SW_SHOW);
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_USER_ONLINE),		SW_SHOW);
	}
	else
	{
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_LOAD_LOCALLIB),	SW_SHOW);
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_DEL_LOCALLIB),		SW_SHOW);
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_DOWNLOADMAT),		SW_HIDE);
		ShowWindow(GetDlgItem(hMain, IDC_MATEDIT_USER_ONLINE),		SW_HIDE);
	}
}

//------------------------------------------------------------------------------------------------------

int MatEditorWindow::findSelectedMatInLib()
{
	int which = -1;
	for (int i=0; i<7; i++)
	{
		EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, libIDs[i]));
		if (!empv)
			continue;
		if (empv->colBorder == RGB(255,0,0))
			which = i;
	}
	if (which>=0  &&  which<7)
		return which;
	return -1;
}

//------------------------------------------------------------------------------------------------------

char * MatEditorWindow::getSearchText()
{
	EMEditSimple * emes = GetEMEditSimpleInstance(GetDlgItem(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY));
	char * sq = emes->getText();
	return sq;
}

//------------------------------------------------------------------------------------------------------
int MatEditorWindow::findMaterialsInLocalLib(char * key, int page)
{
	onlinematlib.clearPreviewControls();
	unclickAllCategories(-1);
	localmatlib.numSearchResults = 0;

	if (!key)
		return -1;
	if (page < 0)
		return 0;

	bList<char *, 8> tokens(0);
	char * tkey = copyString(key);
	char delim[] = " ";

	char * ntok = NULL;
	char * ctok = strtok_s(tkey, delim, &ntok);
	while (ctok)
	{
		if (strlen(ctok) > 1)
			tokens.add(ctok);
		ctok = strtok_s(NULL, delim, &ntok);
	}
	tokens.createArray();

	Logger::add("Tokens in search:");
	for (int i=0; i<tokens.objCount; i++)
		Logger::add(tokens[i]);

	if (tokens.objCount < 1)
		return -1;

	bool areIn[16];
	int tcount = min(16, tokens.objCount);
	int rNum = 0;


	Logger::add("search results:");

	for (int c=0; c<23; c++)		// in all categories
	{

		for (int i=0; i<localmatlib.localList[c].objCount; i++)		// all mats in category
		{
			for (int j=0; j<16; j++)
				areIn[j] = false;
			for (int j=0; j<tcount; j++)
			{
				if (strstr(localmatlib.localList[c][i].name, tokens[j]))
					areIn[j] = true;
			}

			bool add = true;
			for (int j=0; j<tcount; j++)
				if (!areIn[j])
					add = false;
			if (add)
			{
				if (rNum >= page*7   &&   rNum < page*7+7)
				{
					localmatlib.searchResults[rNum-page*7] = &(localmatlib.localList[c][i]);
					localmatlib.searchResCats[rNum-page*7] = c;
					Logger::add(localmatlib.localList[c][i].name);
				}
				localmatlib.numSearchResults++;
				rNum++;
			}

		}
	}


	if (localmatlib.numSearchResults > (page+1)*7)		// check NEXT button
		enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_RIGHT);
	else
		disableControl(MatEditorWindow::hMain, IDC_MATEDIT_LIB_RIGHT);
	if (page > 0)										// check PREV button
		enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_LEFT);
	else
		disableControl(MatEditorWindow::hMain, IDC_MATEDIT_LIB_LEFT);


	localmatlib.currentPage = page;
	localmatlib.queryMode = LocalMatLib::MODE_SEARCH;


	for (int i=0; i<7; i++)
	{
		EMPView * empv = GetEMPViewInstance(GetDlgItem(MatEditorWindow::hMain, MatEditorWindow::libIDs[i]));
		if (!empv)
			continue;
		if (page*7+i < localmatlib.numSearchResults)
		{
			for (int y=0; y<96; y++)
				for (int x=0; x<96; x++)
					empv->byteBuff->imgBuf[y][x] = localmatlib.searchResults[i]->preview.imgBuf[y][x];
		}
		else
			empv->byteBuff->clearBuffer();

		InvalidateRect(empv->hwnd, NULL, false);


		char tempstring[2048];
		if (page*7+i < localmatlib.numSearchResults)
			sprintf_s(tempstring, 2048, "Name: %s\nCategory: %s\nLayers: %d\nTextures: %d", 
					localmatlib.searchResults[i]->name,
					MatCategory::getCategoryName(localmatlib.searchResCats[i]),
					localmatlib.searchResults[i]->layers,
					localmatlib.searchResults[i]->textures);
		else
			tempstring[0] = 0;
		empv->setToolTip(tempstring);
	}

	return localmatlib.numSearchResults;
}
//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::invokeUserPassDialog()
{
	int res = 0;
	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->runDialogsUnicode)
	{
		res = (int)DialogBoxParamW(hDllModule, MAKEINTRESOURCEW(IDD_USER_PASS), hMain, UserPassDlgProc,(LPARAM)(NULL));
	}
	else
	{
		res = (int)DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_USER_PASS), hMain, UserPassDlgProc,(LPARAM)(NULL));
	}

	return (res!=0);
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::unclickAllCategories(int select)
{
	EMImgStateButton * emCatStone		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_STONE));
	EMImgStateButton * emCatTiles		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_TILES));
	EMImgStateButton * emCatWalls		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WALLS));
	EMImgStateButton * emCatWood		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_WOOD));
	EMImgStateButton * emCatEmitter		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_EMITTER));
	EMImgStateButton * emCatFabric		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FABRIC));
	EMImgStateButton * emCatFood		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_FOOD));
	EMImgStateButton * emCatGlass		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GLASS));
	EMImgStateButton * emCatGround		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_GROUND));
	EMImgStateButton * emCatLeather		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LEATHER));
	EMImgStateButton * emCatLiquids		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_LIQUIDS));
	EMImgStateButton * emCatSSS			= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_SSS));
	EMImgStateButton * emCatMinerals	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_MINERALS));
	EMImgStateButton * emCatPlastic		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PLASTIC));
	EMImgStateButton * emCatOrganic		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_ORGANIC));
	EMImgStateButton * emCatPorcelain	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_PORCELAIN));
	EMImgStateButton * emCatBricks		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_BRICKS));
	EMImgStateButton * emCatConcrete	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CONCRETE));
	EMImgStateButton * emCatCeramic		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CERAMIC));
	EMImgStateButton * emCatCarpaint	= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_CARPAINT));
	EMImgStateButton * emCatMetal		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_METAL));
	EMImgStateButton * emCatOther		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_OTHER));
	EMImgStateButton * emCatUser		= GetEMImgStateButtonInstance(GetDlgItem(hMain,    IDC_MATEDIT_LIB_CAT_USER));


	emCatStone->stateOn = false;
	emCatTiles->stateOn = false;
	emCatWalls->stateOn = false;
	emCatWood->stateOn = false;
	emCatEmitter->stateOn = false;
	emCatFabric->stateOn = false;
	emCatFood->stateOn = false;
	emCatGlass->stateOn = false;
	emCatGround->stateOn = false;
	emCatLeather->stateOn = false;
	emCatLiquids->stateOn = false;
	emCatSSS->stateOn = false;
	emCatMinerals->stateOn = false;
	emCatPlastic->stateOn = false;
	emCatOrganic->stateOn = false;
	emCatPorcelain->stateOn = false;
	emCatBricks->stateOn = false;
	emCatConcrete->stateOn = false;
	emCatCeramic->stateOn = false;
	emCatCarpaint->stateOn = false;
	emCatMetal->stateOn = false;
	emCatOther->stateOn = false;
	emCatUser->stateOn = false;

	switch (select)
	{
		case MatCategory::CAT_EMITTER:
			emCatEmitter->stateOn = true;
			break;
		case MatCategory::CAT_BRICKS:
			emCatBricks->stateOn = true;
			break;
		case MatCategory::CAT_CARPAINT:
			emCatCarpaint->stateOn = true;
			break;
		case MatCategory::CAT_CERAMIC:
			emCatCeramic->stateOn = true;
			break;
		case MatCategory::CAT_CONCRETE:
			emCatConcrete->stateOn = true;
			break;
		case MatCategory::CAT_FABRIC:
			emCatFabric->stateOn = true;
			break;
		case MatCategory::CAT_FOOD:
			emCatFood->stateOn = true;
			break;
		case MatCategory::CAT_GLASS:
			emCatGlass->stateOn = true;
			break;
		case MatCategory::CAT_GROUND:
			emCatGround->stateOn = true;
			break;
		case MatCategory::CAT_LEATHER:
			emCatLeather->stateOn = true;
			break;
		case MatCategory::CAT_LIQUIDS:
			emCatLiquids->stateOn = true;
			break;
		case MatCategory::CAT_METAL:
			emCatMetal->stateOn = true;
			break;
		case MatCategory::CAT_MINERALS_GEMS:
			emCatMinerals->stateOn = true;
			break;
		case MatCategory::CAT_ORGANIC:
			emCatOrganic->stateOn = true;
			break;
		case MatCategory::CAT_PLASTIC:
			emCatPlastic->stateOn = true;
			break;
		case MatCategory::CAT_PORCELAIN:
			emCatPorcelain->stateOn = true;
			break;
		case MatCategory::CAT_SSS:
			emCatSSS->stateOn = true;
			break;
		case MatCategory::CAT_STONE:
			emCatStone->stateOn = true;
			break;
		case MatCategory::CAT_TILES:
			emCatTiles->stateOn = true;
			break;
		case MatCategory::CAT_WALLS:
			emCatWalls->stateOn = true;
			break;
		case MatCategory::CAT_WOOD:
			emCatWood->stateOn = true;
			break;
		case MatCategory::CAT_OTHER:
			emCatOther->stateOn = true;
			break;
		case MatCategory::CAT_USER:
			emCatUser->stateOn = true;
			break;
	}


	InvalidateRect(emCatStone->hwnd, NULL, false);
	InvalidateRect(emCatTiles->hwnd, NULL, false);
	InvalidateRect(emCatWalls->hwnd, NULL, false);
	InvalidateRect(emCatWood->hwnd, NULL, false);
	InvalidateRect(emCatEmitter->hwnd, NULL, false);
	InvalidateRect(emCatFabric->hwnd, NULL, false);
	InvalidateRect(emCatFood->hwnd, NULL, false);
	InvalidateRect(emCatGlass->hwnd, NULL, false);
	InvalidateRect(emCatGround->hwnd, NULL, false);
	InvalidateRect(emCatLeather->hwnd, NULL, false);
	InvalidateRect(emCatLiquids->hwnd, NULL, false);
	InvalidateRect(emCatSSS->hwnd, NULL, false);
	InvalidateRect(emCatMinerals->hwnd, NULL, false);
	InvalidateRect(emCatPlastic->hwnd, NULL, false);
	InvalidateRect(emCatOrganic->hwnd, NULL, false);
	InvalidateRect(emCatPorcelain->hwnd, NULL, false);
	InvalidateRect(emCatBricks->hwnd, NULL, false);
	InvalidateRect(emCatConcrete->hwnd, NULL, false);
	InvalidateRect(emCatCeramic->hwnd, NULL, false);
	InvalidateRect(emCatCarpaint->hwnd, NULL, false);
	InvalidateRect(emCatMetal->hwnd, NULL, false);
	InvalidateRect(emCatOther->hwnd, NULL, false);
	InvalidateRect(emCatUser->hwnd, NULL, false);


}

//------------------------------------------------------------------------------------------------------

