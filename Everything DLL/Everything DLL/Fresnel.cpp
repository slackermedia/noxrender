#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>

#define clamp(x,a,b)   max((a),min((x),(b)))

//-------------------------------------------------------------------------------------------

float getFresnelAnother(float IOR, float kos)
{
	float g = IOR*IOR - 1.0f + kos*kos;
	if (g<0)
		return 1.0f;

	g = sqrt(g);
	float c = kos;
	float gmc = g-c;
	float gpc = g+c;
	float l = c*gpc-1.0f;
	float m = c*gmc+1.0f;

	float f = 0.5f * gmc*gmc/(gpc*gpc) * (1 + l*l/(m*m));
	return min(f, 1.0f);
}

//-------------------------------------------------------------------------------------------
float getFresnelTemp(float IOR, float kos, bool goingIn);

float getFresnelTemp(float IOR, float kos)
{
	if (IOR > 1)
		return getFresnelTemp(IOR, kos, true);
	else
		return getFresnelTemp(1/IOR, kos, false);

	return getFresnelAnother(IOR, kos);
	float sqrt1 = IOR*IOR + kos*kos - 1.0f;
	if (sqrt1>0)
		sqrt1 = sqrt(sqrt1);
	else
		return 1.0f;
	float f1 = (IOR*kos-sqrt1/IOR)/(IOR*kos+sqrt1/IOR);
	float f2 = (kos-sqrt1)/(kos+sqrt1);
	float fres = (f1*f1 + f2*f2)*0.5f;
	return fres;
}

//-------------------------------------------------------------------------------------------

float getFresnelTemp(float IOR, float kos, bool goingIn)
{
	float n1,n2;
	if (goingIn)
	{
		n1 = 1;
		n2 = IOR;
	}
	else
	{
		n1 = IOR;
		n2 = 1;
	}

	float n1pern2 = n1/n2;
	float sqrt1 = 1 - ( n1pern2*n1pern2*(1-kos*kos) );
	if (sqrt1>0)
		sqrt1 = sqrt(sqrt1);
	else
		return 1.0f;

	float f1 = (n2*kos-n1*sqrt1)/(n2*kos+n1*sqrt1);
	float f2 = (n1*kos-n2*sqrt1)/(n1*kos+n2*sqrt1);
	float fres = (f1*f1 + f2*f2)*0.5f;
	return fres;
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

Fresnel::Fresnel()
{
	mode = MODE_FRESNEL; 
	IOR = 1.6f;

	X0 = 0.0f;
	X1 = 0.25f;
	X2 = 0.75f;
	X3 = 1.0f;

	Y0 = 0.0f;
	Y1 = 0.25f;
	Y2 = 0.75f;
	Y3 = 1.0f;

	customPolynomial = false;
}

Fresnel::~Fresnel()
{
}

//-------------------------------------------------------------------------------------------

float Fresnel::getValue(float angle)
{
	switch (mode)
	{
		default:
		case MODE_FRESNEL:
			return getValueFresnel(angle);
			break;
		case MODE_CUSTOM:
			return getValueCustom(angle);
			break;
	}
}

//-------------------------------------------------------------------------------------------

float Fresnel::getValueFromCos(float kos)
{
	switch (mode)
	{
		case MODE_FRESNEL:
		{
			return getValueFresnelFromCos(kos);
		}
			break;
		case MODE_CUSTOM:
		{
			float angle = FastMath::acos(kos)/1.5707963267948966192313216916398f;
			return getValueCustom(angle);
		}
		break;
	}
	return 0;
}

//-------------------------------------------------------------------------------------------

float Fresnel::getValueCustom(float angle)
{
	float res;
	float x = angle;

	float X[4];
	float Y[4];
	X[0] = X0;
	X[1] = X1;
	X[2] = X2;
	X[3] = X3;
	Y[0] = Y0;
	Y[1] = Y1;
	Y[2] = Y2;
	Y[3] = Y3;

	int i,j;
	res = 0;
	float l;
	if (abs(X[1] - X[2]) < 0.005f)
	{
		for (i=0; i<4; i++)
		{
			if (i==1)
				i++;
			l = 1;
			for (j=0; j<4; j++)
			{
				if (j==1)
					j++;
				if (j == i)
					continue;
				l *= (x-X[j])/(X[i]-X[j]);
			}
			res += l*Y[i];
		}
	}
	else
	{
		for (i=0; i<4; i++)
		{
			l = 1;
			for (j=0; j<4; j++)
			{
				if (j == i)
					continue;
				l *= (x-X[j])/(X[i]-X[j]);
			}
			res += l*Y[i];
		}
	}
	if (res > 1)
		res = 1;
	if (res < 0)
		res = 0;

	return res;
}

//-------------------------------------------------------------------------------------------

float Fresnel::getValueFresnel(float angle)
{
	float x = angle*1.5707963267948966192313216916398f;	// PI/2
	float f1,f2;
	float sqrt1;
	float sin1, cos1;
	sin1 = FastMath::sin(x);
	cos1 = FastMath::cos(x);

	sqrt1 = IOR*IOR - sin1*sin1;
	if (sqrt1>0)
		sqrt1 = sqrt(sqrt1);
	else 
		sqrt1 = 0;
	f1 = (IOR*IOR*cos1 - sqrt1) / (IOR*IOR*cos1 + sqrt1);
	f2 = (cos1 - sqrt1) / (cos1 + sqrt1);
	return (f2*f2 + f1*f1)*0.5f;
}

//-------------------------------------------------------------------------------------------

float Fresnel::getValueFresnelFromCos(float kos)
{
	float sqrt1 = IOR*IOR + kos*kos - 1.0f;
	if (sqrt1>0)
		sqrt1 = sqrt(sqrt1);
	else
		sqrt1 = 0;
	float f1 = (IOR*IOR*kos-sqrt1)/(IOR*IOR*kos+sqrt1);
	float f2 = (kos-sqrt1)/(kos+sqrt1);
	float fres = (f1*f1 + f2*f2)*0.5f;

	return fres;
}

//-------------------------------------------------------------------------------------------

void Fresnel::preEval()
{
	int i;
	float a;
	for (i=0; i<101; i++)
	{
		a = (float)i/100.0f;
		preEvaledValues[i] = getValueFromCos(a);
	}
	preEvaledValues[101] = preEvaledValues[100];
}

//-------------------------------------------------------------------------------------------

float Fresnel::getPreEvaled(float kos)
{
	int i = (int)(fabs(kos)*100);
	float r = kos*100.0f-i;
	return  ( preEvaledValues[i]*(1-r)  +   preEvaledValues[i+1]*r );
}

//-------------------------------------------------------------------------------------------
