#include <windows.h>
#include <math.h>
#include <gdiplus.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "QuasiRandom.h"
#include "resource.h"
#include "RendererMainWindow.h"
#include "log.h"
#include <float.h>

#define USE_CHROMATIC_BOKEH

#define NOX_NUM_ALGORITHMS 3

//----------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::evalSimpleDOFImage(FloatBuffer * depth, FinalModifier * fMod)
{
	CHECK(fMod);
	CHECK(imgBuf);
	CHECK(depth);
	CHECK(depth->fbuf);
	CHECK(depth->width==width);
	CHECK(depth->height==height);
	int wh = width * height;

	time_t time_start, time_threads_start, time_threads_stop, time_stop;
	time_start = clock();

	ImageBuffer * res = new ImageBuffer();
	CHECK(res);

	bool allOK = res->allocBuffer(width, height);
	if (!allOK)
	{
		delete res;
		return NULL;
	}
	res->clearBuffer();

	Raytracer * rtr = Raytracer::getInstance();

	DiaphragmSampler diaphSampler;
	diaphSampler.initializeDiaphragm(fMod->bladesNum, 1.0f, fMod->bladesAngle, fMod->roundBlades, fMod->bladesRadius);
	diaphSampler.intBokehRingBalance = fMod->bokehRingBalance;
	diaphSampler.intBokehRingSize = fMod->bokehRingSize;
	diaphSampler.intBokehFlatten = fMod->bokehFlatten;
	diaphSampler.intBokehVignette = fMod->bokehVignette;
	if (diaphSampler.intBokehRingBalance>=0)
		diaphSampler.bokehRingBalance = diaphSampler.intBokehRingBalance+1.0f;
	else
		diaphSampler.bokehRingBalance = 1.0f + diaphSampler.intBokehRingBalance/21.0f;
	diaphSampler.bokehRingSize = diaphSampler.intBokehRingSize*0.01f;
	diaphSampler.bokehFlatten  = diaphSampler.intBokehFlatten*0.01f;
	diaphSampler.bokehVignette = diaphSampler.intBokehVignette*0.01f;
	diaphSampler.create(fMod->quality*30, 10*fMod->quality, true);

	#ifdef USE_CHROMATIC_BOKEH
		diaphSampler.chromaticShiftLens = fMod->chromaticShiftLens;
		diaphSampler.chromaticShiftDepth = fMod->chromaticShiftDepth;
		diaphSampler.chromaticAchromatic = fMod->chromaticAchromatic;
		diaphSampler.allocSpaceChromaticSampleColors();
	#endif

	time_threads_start = clock();

	int numThreads = min(16, max(1, fMod->numThreads));
	int algorithm = min(NOX_NUM_ALGORITHMS-1, max(0, fMod->algorithm));

	bool a1Phase1DoneAll = false;
	bool a1Phase2DoneAll = false;
	float * radiusSBuffer = NULL;
	float * radiusDBuffer = NULL;
	if (algorithm == 0)
	{
		radiusSBuffer = (float*)malloc(sizeof(float)*wh);
		radiusDBuffer = (float*)malloc(sizeof(float)*wh);
		for (int i=0; i<wh; i++)
			radiusSBuffer[i] = 0.0f;
		for (int i=0; i<wh; i++)
			radiusDBuffer[i] = 0.0f;
	}

	FakeDOFThread * threads = new FakeDOFThread[numThreads];

	for (int t=0; t<numThreads; t++)
	{
		threads[t].numAllThreads = numThreads;
		threads[t].curThreadNum = t;
		threads[t].algorithmChosen = algorithm;

		threads[t].imgSrc = this;
		threads[t].imgDst = res;
		threads[t].depth = depth;
		threads[t].fMod = fMod;
		threads[t].diaph = &diaphSampler;
		threads[t].a1sRadius = radiusSBuffer;
		threads[t].a1dRadius = radiusDBuffer;

		threads[t].runThread();
	}

	bool stillWorking = true;
	while (stillWorking)
	{
		stillWorking = false;
		a1Phase1DoneAll = true;
		a1Phase2DoneAll = true;
		float sumProgress = 0;
		for (int t=0; t<numThreads; t++)
		{
			sumProgress += threads[t].progress;
			if (!threads[t].jobDone)
				stillWorking = true;
			if (!threads[t].a1Phase1Done)
				a1Phase1DoneAll = false;
			if (!threads[t].a1Phase2Done)
				a1Phase2DoneAll = false;
		}
		if (a1Phase1DoneAll)
			for (int t=0; t<numThreads; t++)
				threads[t].a1Phase1DoneInAll = true;
		if (a1Phase2DoneAll)
			for (int t=0; t<numThreads; t++)
				threads[t].a1Phase2DoneInAll = true;
		sumProgress *= 1.0f/numThreads;
		if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
		{
			for (int t=0; t<numThreads; t++)
				threads[t].abort = true;
		}
		else
			rtr->curScenePtr->notifyProgress("Evaluating fake DOF...", sumProgress);
		Sleep(100);
	}

	delete [] threads;

	time_threads_stop = clock();

	rtr->curScenePtr->notifyProgress("Fake DOF done", 100.0f);

	if (radiusSBuffer)
		free(radiusSBuffer);
	if (radiusDBuffer)
		free(radiusDBuffer);
	radiusSBuffer = NULL;
	radiusDBuffer = NULL;


	for (int y=0; y<height; y++)
		for (int x=0; x<width; x++)
		{
			int h = max(1, res->hitsBuf[y][x]);
			res->imgBuf[y][x] *= 1.0f/h;
		}
	res->setHitsValues(1);


	diaphSampler.freeSamples();
	diaphSampler.freeSpaceChromaticSampleColors();

	time_stop = clock();

	char buf[256];
	sprintf_s(buf, 256, "Redrawed fake dof on %d threads on algorithm %d, quality %d", numThreads, (algorithm+1), fMod->quality);
	Logger::add(buf);
	float wholetime = (float)((time_stop - time_start)/ (double)CLOCKS_PER_SEC);
	float threadstime = (float)((time_threads_stop - time_threads_start)/ (double)CLOCKS_PER_SEC);
	sprintf_s(buf, 256, "Whole time: %.3f sec     Threads time: %.3f sec", wholetime, threadstime);
	Logger::add(buf);

	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		res->freeBuffer();
		delete res;
		return NULL;
	}

	return res;
}
//----------------------------------------------------------------------------------------------------------------

FakeDOFThread::FakeDOFThread()
{
	hThread = 0;
	threadId = 0;
	numAllThreads = 1;
	curThreadNum = 0;
	algorithmChosen = 1;
	jobDone = false;
	abort = false;
	a1Phase1Done = false;
	a1Phase1DoneInAll = false;
	a1Phase2Done = false;
	a1Phase2DoneInAll = false;

	imgSrc = NULL;
	imgDst = NULL;
	depth = NULL;
	fMod = NULL;
	diaph = NULL;
	a1sRadius = NULL;
	a1dRadius = NULL;
}

FakeDOFThread::~FakeDOFThread()
{
	jobDone = true;
	imgSrc = NULL;
	imgDst = NULL;
	depth = NULL;
	fMod = NULL;
	diaph = NULL;
}

//----------------------------------------------------------------------------------------------------------------

bool FakeDOFThread::runThread()
{
	hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(FakeDofThreadProc),
            (LPVOID)this,
            0,
            &threadId);

	SetThreadPriority(hThread, THREAD_PRIORITY_BELOW_NORMAL);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

DWORD WINAPI FakeDOFThread::FakeDofThreadProc(LPVOID lpParameter)
{
	FakeDOFThread * thr = (FakeDOFThread *)lpParameter;
	if (!thr)
	{
		thr->jobDone = true;
		return 1;
	}
	thr->abort = false;

	switch (thr->algorithmChosen)
	{
		case 0:
			thr->simpleDOFAlgorithm1();
			break;
		case 1:
			thr->simpleDOFAlgorithm2();
			break;
		case 2:
			thr->simpleDOFAlgorithm3();
			break;
	}

	thr->jobDone = true;

	return 0;
}

//----------------------------------------------------------------------------------------------------------------

int compare_depth( const void *arg1, const void *arg2 )
{
	float f1 = *(float*)arg1;
	float f2 = *(float*)arg2;
	if (f1==f2)
	{
		return 0;
	}
	else
	{
		if (f1<f2)
			return 1;
		else 
			return -1;
	}
}

//----------------------------------------------------------------------------------------------------------------

bool FakeDOFThread::simpleDOFAlgorithm1()
{
	ImageBuffer * res = imgDst;
	CHECK(res);
	CHECK(imgSrc);
	CHECK(depth);
	CHECK(fMod);

	progress = 0;

	int height = res->height;
	int width = res->width;
	int startHeight = min(height, height * curThreadNum / numAllThreads);
	int stopHeight = min(height, height * (curThreadNum+1) / numAllThreads);

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	int quality = fMod->quality;

	for (int y=0; y<height; y++)
		for (int x=0; x<width; x++)
			res->hitsBuf[y][x] = 0;

	int wh = width*height;
	float * dRadius = a1dRadius;
	float * sRadius = a1sRadius;
	for (int i=0; i<wh; i++)
	{
		int y = i/width;
		int x = i%width;
		int nd = depth->hbuf[y][x];
		float d = nd ? depth->fbuf[y][x]/nd : fMod->focusDistance;
		float r = fabs(fMod->focusDistance-d)*fMod->focal*fMod->focal   /   (d*fMod->focusDistance*fMod->aperture*0.018f*2);
		sRadius[i] = max(1.0f, fabs(r*width/2));
	}

	a1Phase1Done = true;

	DiaphragmSampler diaphSampl;
	diaphSampl.initializeDiaphragm(fMod->bladesNum, 1.0f, fMod->bladesAngle, fMod->roundBlades, fMod->bladesRadius);
	diaphSampl.intBokehRingBalance = fMod->bokehRingBalance;
	diaphSampl.intBokehRingSize = fMod->bokehRingSize;
	diaphSampl.intBokehFlatten = fMod->bokehFlatten;
	diaphSampl.intBokehVignette = fMod->bokehVignette;
	if (diaphSampl.intBokehRingBalance>=0)
		diaphSampl.bokehRingBalance = diaphSampl.intBokehRingBalance+1.0f;
	else
		diaphSampl.bokehRingBalance = 1.0f + diaphSampl.intBokehRingBalance/21.0f;
	diaphSampl.bokehRingSize = diaphSampl.intBokehRingSize*0.01f;
	diaphSampl.bokehFlatten = diaphSampl.intBokehFlatten*0.01f;
	diaphSampl.bokehVignette = diaphSampl.intBokehVignette*0.01f;
	diaphSampl.copyPointsFromOther(diaph);

	#ifdef USE_CHROMATIC_BOKEH
		diaphSampl.chromaticShiftLens = fMod->chromaticShiftLens;
		diaphSampl.chromaticShiftDepth = fMod->chromaticShiftDepth;
		diaphSampl.chromaticAchromatic = fMod->chromaticAchromatic;
		diaphSampl.allocSpaceChromaticSampleColors();
	#endif

	while (!a1Phase1DoneInAll)
	{
		Sleep(10);
	}

	for (int y=startHeight; y<stopHeight; y++)
	{
		progress = ((y-startHeight)*100.0f/(float)(stopHeight-startHeight)) * 0.5f;
		if (abort)
			break;
		for (int x=0; x<width; x++)
		{
			int is = y*width+x;
			float r = sRadius[is];
			float r2 = r*r;
			int ir = (int)r+1;
			int x1 = max(0, x-ir);
			int y1 = max(0, y-ir);
			int x2 = min(width, x+ir+1);
			int y2 = min(height, y+ir+1);
			for (int yy=y1; yy<y2; yy++)
			{
				for (int xx=x1; xx<x2; xx++)
				{
					 float rD = r;
					int ii = xx+yy*width;
					int rrr2 = (xx-x)*(xx-x) + (yy-y)*(yy-y);

					if (rrr2<=r2)
						dRadius[ii] = max(dRadius[ii], rD);
				}
			}
		}
	}

	a1Phase2Done = true;
	while (!a1Phase2DoneInAll)
	{
		Sleep(10);
	}

	for (int y=startHeight; y<stopHeight; y++)
	{
		progress = ((y-startHeight)*100.0f/(float)(stopHeight-startHeight)) * 0.5f + 50.0f;
		if (abort)
			break;
		for (int x=0; x<width; x++)
		{
			int ii = y*width+x;
			float r = dRadius[ii];
			//r = 5;

			int nd = depth->hbuf[y][x];
			float srcdist = nd ? depth->fbuf[y][x]/nd : fMod->focusDistance;

			int numPoints = quality * 30;// r*r;

			#ifdef USE_CHROMATIC_BOKEH
				diaphSampl.evalChromaticSampleColors(srcdist/fMod->focusDistance, numPoints);
			#endif

			float power = 1.0f/numPoints;
			power = 1.0f;
			Color4 origsrccol = imgSrc->imgBuf[y][x] * power;
			for (int i=0; i<numPoints; i++)
			{
				float sx, sy;
				diaphSampl.getThat(i, sx, sy);
				int xxx = min(width-1, max(0, (int)(x+sx*r)));
				int yyy = min(height-1, max(0, (int)(y+sy*r)));

				Color4 srccol = origsrccol;

				float chrRed = 1.0f;
				float chrGreen = 1.0f;
				float chrBlue = 1.0f;

				#ifdef USE_CHROMATIC_BOKEH
					diaphSampl.getChrColorSample(i, chrRed, chrGreen, chrBlue);
					srccol *= Color4(chrRed, chrGreen, chrBlue);
				#endif

				float R1 = sRadius[y*width+x];
				float R1a = sRadius[max(0, min(wh, y*width+x+1))];
				float R1b = sRadius[max(0, min(wh, y*width+x-1))];
				float R1c = sRadius[max(0, min(wh, y*width+x+width))];
				float R1d = sRadius[max(0, min(wh, y*width+x-width))];
				float R2 = sRadius[yyy*width+xxx];
				float R2a = sRadius[max(0, min(wh, yyy*width+xxx+1))];
				float R2b = sRadius[max(0, min(wh, yyy*width+xxx-1))];
				float R2c = sRadius[max(0, min(wh, yyy*width+xxx+width))];
				float R2d = sRadius[max(0, min(wh, yyy*width+xxx-width))];
				float R1max = max(R1, max(R1a, max(R1b, max(R1c, R1d))));
				float R2max = max(R2, max(R2a, max(R2b, max(R2c, R2d))));
				float R1min = min(R1, min(R1a, min(R1b, min(R1c, R1d))));
				float R2min = min(R2, min(R2a, min(R2b, min(R2c, R2d))));
				R1 = max(R1, max(R1a, max(R1b, max(R1c, R1d))));
				R2 = max(R2, max(R2a, max(R2b, max(R2c, R2d))));
				float R32 = (float)((yyy-y)*(yyy-y) + (xxx-x)*(xxx-x));	// real dist
				float R12 = R1*R1;	// src dist
				float R22 = R2*R2;	// dst dist 

				if (R32<=R12  ||  R32<R22)
				{
					int nd = depth->hbuf[yyy][xxx];
					float dstdist = nd ? depth->fbuf[yyy][xxx]/nd : fMod->focusDistance;
					
					if (dstdist<srcdist)
					{
						if (R32>R22) 
						{
							float p = min(1.0f, R32/R22);
							p=1;
							float np = 1-p;
							res->imgBuf[yyy][xxx] += imgSrc->imgBuf[yyy][xxx]*p + srccol*np;
							res->hitsBuf[yyy][xxx] += 1;
						}
						else
						{
							res->imgBuf [yyy][xxx] += srccol;
							res->hitsBuf[yyy][xxx] += 1;
						}
					}
					else
					{
						if (R32>R12)
						{
							float p = 1.0f/R12;
							float np = 1-p;
						}
						else
						{
							res->imgBuf[yyy][xxx] += srccol;
							res->hitsBuf[yyy][xxx] += 1;
						}
					}
				}
				else
				{
					float R = min(R1, R2);
					int xxx2 = min(width-1, max(0, x+(int)(sx*R)));
					int yyy2 = min(height-1, max(0, y+(int)(sy*R)));
					res->imgBuf[yyy2][xxx2] += srccol;
					res->hitsBuf[yyy2][xxx2] += 1;	
				}
			}
		}
	}

	#ifdef USE_CHROMATIC_BOKEH
		diaphSampl.freeSpaceChromaticSampleColors();
	#endif
	diaphSampl.freeSamples();
	return true;


	// MANY PIXELS TO ONE PIXEL VERSION
	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			int nd = depth->hbuf[y][x];
			float d = nd ? depth->fbuf[y][x]/nd : fMod->focusDistance;
			float r = fabs(fMod->focusDistance-d)*fMod->focal*fMod->focal   /   (d*fMod->focusDistance*fMod->aperture*0.018f*2);
			r *= width/2;
			r = fabs(r);
			r = max(1.0f, r);

			int numPoints = (int)(quality * r*r);
			float power = 1.0f/numPoints;
			power = 1.0f;
			Color4 col = Color4(0,0,0);
			int cc = 0;
			for (int i=0; i<numPoints; i++)
			{
				float sx, sy;
				diaphSampl.getThat(i, sx, sy);
				int xxx = min(width-1, max(0, x+(int)(sx*r)));
				int yyy = min(height-1, max(0, y+(int)(sy*r)));

				float td = depth->fbuf[yyy][xxx]/depth->hbuf[yyy][xxx];
				float tr = fabs(fMod->focusDistance-td)*fMod->focal*fMod->focal   /   (td*fMod->focusDistance*fMod->aperture*0.018f*2);
				tr = max(1.0f, tr*width/2);
				
				col += imgSrc->imgBuf[yyy][xxx]*power;
				cc++;
			}
			res->imgBuf[y][x] = col;
			res->hitsBuf[y][x] += cc;
		}
	}

	// END MANY PIXELS TO ONE PIXEL VERSION

	for (int y=0; y<height; y++)
	{
		rtr->curScenePtr->notifyProgress("Applying DOF", 100.0f*y/height);

		for (int x=0; x<width; x++)
		{
			int nd = depth->hbuf[y][x];
			float d = nd ? depth->fbuf[y][x]/nd : fMod->focusDistance;
			float r = fabs(fMod->focusDistance-d)*fMod->focal*fMod->focal   /   (d*fMod->focusDistance*fMod->aperture*0.018f*2);
			r *= width/2;

			r = max(1.25f, r);
			#ifdef RAND_PER_THREAD
				r = r * (0.96f + (Raytracer::getRandomGeneratorForThread()->getRandomFloat())*0.08f);
			#else
				r = r * (0.96f + (rand()/(float)RAND_MAX)*0.08f);
			#endif

			float r2 = r*r;
			int ir = (int)r+1;

			if (1)
			{
				#ifdef RAND_PER_THREAD
					int irand = Raytracer::getRandomGeneratorForThread()->getRandomInt(10000)%(5*quality);
				#else
					int irand = rand()%(5*quality);
				#endif
				int numPoints = (int)((r*r*(quality/4))+5*quality+irand);
				float power = 1.0f/numPoints;

				numPoints = quality*10;
				power = 1.0f;

				Color4 srccol = imgSrc->imgBuf[y][x] * power;
				for (int i=0; i<numPoints; i++)
				{
					float sx, sy;
					diaphSampl.getThat(i, sx, sy);

					int ix = (int)(sx*r);
					int iy = (int)(sy*r);

					int xxx = min(width-1, max(0, x+ix));
					int yyy = min(height-1, max(0, y+iy));

					float td = depth->fbuf[yyy][xxx]/depth->hbuf[yyy][xxx];
					float tr = fabs(fMod->focusDistance-td)*fMod->focal*fMod->focal   /   (td*fMod->focusDistance*fMod->aperture*0.018f*2);
					tr = max(1.0f, tr*width/2);

					res->imgBuf[yyy][xxx] += srccol;
					res->hitsBuf[yyy][xxx]++;
				}
			}
		}
	}

	for (int y=0; y<height; y++)
		for (int x=0; x<width; x++)
		{
			int h = max(1, res->hitsBuf[y][x]);
			res->imgBuf[y][x] *= 1.0f/h;
			res->hitsBuf[y][x] = 1;
		}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool FakeDOFThread::simpleDOFAlgorithm2()
{
	ImageBuffer * res = imgDst;
	CHECK(res);
	int width = res->width;
	int height = res->height;
	int startHeight = min(height, height * curThreadNum / numAllThreads);
	int stopHeight = min(height, height * (curThreadNum+1) / numAllThreads);
	progress = 0;

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	int quality = fMod->quality;
	float focusDist = fMod->focusDistance;
	float aperture = fMod->aperture;
	float focal = fMod->focal;
	float focal2 = focal*focal;
	float w2 = width*0.5f;
	float h2 = height*0.5f;
	float pcotg = 1.0f/cam->cotg;
	int wwym = max(width, height);
	float pwwym = 1.0f/wwym;


	DiaphragmSampler diaphSampler;
	diaphSampler.initializeDiaphragm(fMod->bladesNum, 1.0f, fMod->bladesAngle, fMod->roundBlades, fMod->bladesRadius);
	diaphSampler.intBokehRingBalance = fMod->bokehRingBalance;
	diaphSampler.intBokehRingSize = fMod->bokehRingSize;
	diaphSampler.intBokehFlatten = fMod->bokehFlatten;
	diaphSampler.intBokehVignette = fMod->bokehVignette;
	if (diaphSampler.intBokehRingBalance>=0)
		diaphSampler.bokehRingBalance = diaphSampler.intBokehRingBalance+1.0f;
	else
		diaphSampler.bokehRingBalance = 1.0f + diaphSampler.intBokehRingBalance/21.0f;
	diaphSampler.bokehRingSize = diaphSampler.intBokehRingSize*0.01f;
	diaphSampler.bokehFlatten  = diaphSampler.intBokehFlatten*0.01f;
	diaphSampler.bokehVignette = diaphSampler.intBokehVignette*0.01f;
	diaphSampler.copyPointsFromOther(diaph);
	float flatten = diaphSampler.bokehFlatten;
	float vign = diaphSampler.bokehVignette;

	// sort by distance
	int wh = width*height;
	float * dtab = (float*)malloc(wh*(sizeof(float)+sizeof(int)));
	for (int i=0; i<wh; i++)
	{
		int y = i/width;
		int x = i%width;
		int nd = depth->hbuf[y][x];
		float d = nd ? depth->fbuf[y][x]/nd : focusDist;
		float diff = d - focusDist;
		dtab[i*2]		  = d;
		(int&)dtab[i*2+1] = i;
	}
	qsort(dtab, wh, sizeof(float)+sizeof(int), compare_depth);

	// start here
	for (int y=startHeight; y<stopHeight; y++)
	{
		if (abort)
			break;
		progress = (y-startHeight)*100.0f/(float)(stopHeight-startHeight);
		for (int x=0; x<width; x++)
		{
			int nd = depth->hbuf[y][x];
			float d = nd ? depth->fbuf[y][x]/nd : focusDist;

			float pmn = 1.0f;
			float wx = (x - w2) * pwwym;
			float wy = (h2 - y) * pwwym * -1;
			float nwx = 1.0f;
			float nwy = 1.0f;
			if (flatten != 0.0f)
			{
				float sss = sqrt(wx*wx+wy*wy);
				if (_isnan(sss)  ||  sss==0)
					sss = 1;
				nwx = wx / sss;
				nwy = wy / sss;
				float mn = (nwx*nwx+nwy*nwy);
				pmn = 1.0f/mn;
			}

			float r = fabs(focusDist-d)*focal2   /   (d*focusDist*aperture*0.018f*2);
			float srcRadius = max(1.0f, fabs(r*width/2));

			int numPoints = (int)(quality * sqrt(srcRadius) * srcRadius / 2);
			float power = 1.0f/numPoints;
			power = 1.0f;
			Color4 origsrccol = imgSrc->imgBuf[y][x] * power;
			for (int i=0; i<numPoints; i++)
			{
				Color4 srccol = origsrccol;
				float sx, sy;
				diaphSampler.getThat(i, sx, sy);

				if (vign!=0)
				{
					float px = sx-wx*vign*4;
					float py = sy-wy*vign*4;
					if (px*px+py*py>1)
						continue;
				}

				if (flatten != 0.0f)
				{
					float rF = -(nwx*sx+nwy*sy) * pmn;
					if (_isnan(rF))
						rF = 0;
					rF *= flatten;
					sx = sx + wx*rF;
					sy = sy + wy*rF;
				}


				int xxx = min(width-1, max(0, (int)(x+sx*srcRadius)));
				int yyy = min(height-1, max(0, (int)(y+sy*srcRadius)));

				int dstnd = depth->hbuf[yyy][xxx];
				float dstd = dstnd ? depth->fbuf[yyy][xxx]/dstnd : focusDist;

				if (dstd>=d*0.99f)
				{
					res->imgBuf[yyy][xxx] += srccol;
					res->hitsBuf[yyy][xxx]++;
				}
				else
				{
					float dstr = fabs(focusDist-dstd)*focal2   /   (dstd*focusDist*aperture*0.018f*2);
					float dstRadius = max(1.1f, fabs(dstr*width/2));
					if (dstRadius*dstRadius>=srcRadius*srcRadius*(sx*sx+sy*sy))
					{
						res->imgBuf[yyy][xxx] += srccol;
						res->hitsBuf[yyy][xxx]++;
					}
				}
			}


		}
	}

	diaphSampler.freeSamples();
	free(dtab);
	dtab = NULL;

	if (abort)
		return false;

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool FakeDOFThread::simpleDOFAlgorithm3()
{
	ImageBuffer * res = imgDst;
	CHECK(res);
	int width = res->width;
	int height = res->height;
	int startHeight = min(height, height * curThreadNum / numAllThreads);
	int stopHeight = min(height, height * (curThreadNum+1) / numAllThreads);
	progress = 0;

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	int quality = fMod->quality;
	float focusDist = fMod->focusDistance;
	float aperture = fMod->aperture;
	float focal = fMod->focal;
	float focal2 = focal*focal;
	float w2 = width*0.5f;
	float h2 = height*0.5f;
	float pcotg = 1.0f/cam->cotg;
	int wwym = max(width, height);
	float pwwym = 1.0f/wwym;


	DiaphragmSampler diaphSampler;
	diaphSampler.initializeDiaphragm(fMod->bladesNum, 1.0f, fMod->bladesAngle, fMod->roundBlades, fMod->bladesRadius);
	diaphSampler.intBokehRingBalance = fMod->bokehRingBalance;
	diaphSampler.intBokehRingSize = fMod->bokehRingSize;
	diaphSampler.intBokehFlatten = fMod->bokehFlatten;
	diaphSampler.intBokehVignette = fMod->bokehVignette;
	if (diaphSampler.intBokehRingBalance>=0)
		diaphSampler.bokehRingBalance = diaphSampler.intBokehRingBalance+1.0f;
	else
		diaphSampler.bokehRingBalance = 1.0f + diaphSampler.intBokehRingBalance/21.0f;
	diaphSampler.bokehRingSize = diaphSampler.intBokehRingSize*0.01f;
	diaphSampler.bokehFlatten  = diaphSampler.intBokehFlatten*0.01f;
	diaphSampler.bokehVignette = diaphSampler.intBokehVignette*0.01f;
	diaphSampler.copyPointsFromOther(diaph);
	float flatten = diaphSampler.bokehFlatten;
	float vign = diaphSampler.bokehVignette;

	#ifdef USE_CHROMATIC_BOKEH
		diaphSampler.chromaticShiftLens = fMod->chromaticShiftLens;
		diaphSampler.chromaticShiftDepth = fMod->chromaticShiftDepth;
		diaphSampler.chromaticAchromatic = fMod->chromaticAchromatic;
		diaphSampler.allocSpaceChromaticSampleColors();
	#endif

	// sort by distance
	int wh = width*height;
	float * dtab = (float*)malloc(wh*(sizeof(float)+sizeof(int)));
	for (int i=0; i<wh; i++)
	{
		int y = i/width;
		int x = i%width;
		int nd = depth->hbuf[y][x];
		float d = nd ? depth->fbuf[y][x]/nd : focusDist;
		float diff = d - focusDist;
		dtab[i*2]		  = d;
		(int&)dtab[i*2+1] = i;
	}
	qsort(dtab, wh, sizeof(float)+sizeof(int), compare_depth);

	// start here
	for (int y=startHeight; y<stopHeight; y++)
	{
		if (abort)
			break;
		progress = (y-startHeight)*100.0f/(float)(stopHeight-startHeight);
		for (int x=0; x<width; x++)
		{
			int nd = depth->hbuf[y][x];
			float d = nd ? depth->fbuf[y][x]/nd : focusDist;

			float pmn = 1.0f;
			float wx = (x - w2) * pwwym;
			float wy = (h2 - y) * pwwym * -1;
			float nwx = 1.0f;
			float nwy = 1.0f;
			if (flatten != 0.0f)
			{
				float sss = sqrt(wx*wx+wy*wy);
				if (_isnan(sss)  ||  sss==0)
					sss = 1;
				nwx = wx / sss;
				nwy = wy / sss;
				float mn = (nwx*nwx+nwy*nwy);
				pmn = 1.0f/mn;
			}

			float r = fabs(focusDist-d)*focal2   /   (d*focusDist*aperture*0.018f*2);
			float srcRadius = max(1.0f, fabs(r*width/2));

			int numPoints = (int)(min(quality*30, quality * sqrt(srcRadius) * srcRadius / 2));

			#ifdef USE_CHROMATIC_BOKEH
				diaphSampler.evalChromaticSampleColors(d/focusDist, numPoints);
			#endif

			float power = 1.0f/numPoints;
			power = 1.0f;
			Color4 origsrccol = imgSrc->imgBuf[y][x] * power;
			for (int i=0; i<numPoints; i++)
			{
				Color4 srccol = origsrccol;
				float sx, sy;
				diaphSampler.getThat(i, sx, sy);

				#ifdef USE_CHROMATIC_BOKEH
					float chrRed = 1.0f;
					float chrGreen = 1.0f;
					float chrBlue = 1.0f;
					diaphSampler.getChrColorSample(i, chrRed, chrGreen, chrBlue);
					srccol *= Color4(chrRed, chrGreen, chrBlue);
				#endif

				if (vign!=0)
				{
					float px = sx-wx*vign*4;
					float py = sy-wy*vign*4;
					if (px*px+py*py>1)
						continue;
				}

				if (flatten != 0.0f)
				{
					float rF = -(nwx*sx+nwy*sy) * pmn;
					if (_isnan(rF))
						rF = 0;
					rF *= flatten;
					sx = sx + wx*rF;
					sy = sy + wy*rF;
				}


				int xxx = min(width-1, max(0, (int)(x+sx*srcRadius)));
				int yyy = min(height-1, max(0, (int)(y+sy*srcRadius)));

				int dstnd = depth->hbuf[yyy][xxx];
				float dstd = dstnd ? depth->fbuf[yyy][xxx]/dstnd : focusDist;

				if (dstd>=d*0.999f)
				{
					res->imgBuf[yyy][xxx] += srccol;
					res->hitsBuf[yyy][xxx]++;
				}
				else
				{
					float dstr = fabs(focusDist-dstd)*focal2   /   (dstd*focusDist*aperture*0.018f*2);
					float dstRadius = max(1.0f, fabs(dstr*width/2));
					if ((dstRadius*dstRadius>=srcRadius*srcRadius*(sx*sx+sy*sy)*0.75f  &&  dstRadius>1.0f)  ||  dstRadius/srcRadius>0.9f)
					{
						res->imgBuf[yyy][xxx] += srccol;
						res->hitsBuf[yyy][xxx]++;
					}
				}
			}
		}
	}

	#ifdef USE_CHROMATIC_BOKEH
		diaphSampler.freeSpaceChromaticSampleColors();
	#endif

	diaphSampler.freeSamples();
	free(dtab);
	dtab = NULL;

	if (abort)
		return false;

	return true;
}

//----------------------------------------------------------------------------------------------------------------
