#include "MatDownload.h"
#include <stdio.h>
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include <iostream>
#include "log.h"

#ifndef CHECK
	#define CHECK(a) if(!(a)) return false
#endif

DWORD WINAPI DownloadFileThreadProc(LPVOID lpParameter);
size_t write_data (void *ptr, size_t size, size_t nmemb, void *stream);	

MatDownloader::MatDownloader()
{
	noxProgressCallback = NULL;
	tDoneCallback = NULL;
	prObj = NULL;
	tdObj = NULL;
	threadURL = NULL;
	threadFilename = NULL;
	breakDownloading = false;
	threadHandle = 0;
}

int progressCallbackDown1(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
	MatDownloader * md = (MatDownloader*)clientp;
	if (md  &&  md->breakDownloading)
		return 2;
	if (md  &&  md->noxProgressCallback)
	{
		float f = (float)(dlnow/dltotal);
		bool res = (*md->noxProgressCallback)(md->prObj, f);
		if (!res)
			return 1;
	}
	return 0;
}


bool MatDownloader::downloadFile(char * address, char * filename)
{
	CHECK(address);
	CHECK(filename);

	CURL *curl;
	CURLcode res;

	FILE * file = NULL;
	if (fopen_s(&file, filename, "wb"))
	{
		Logger::add("Downloader: can't create file:");
		Logger::add(filename);
		return false;
	}
	CHECK(file);

	curl = curl_easy_init(); 

	curl_easy_setopt(curl, CURLOPT_POST, true); 

	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
	curl_easy_setopt(curl, CURLOPT_URL, address); 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); 

	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0); 
	curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, this); 
	curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, progressCallbackDown1); 

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

	res = curl_easy_perform(curl); 
	
	curl_easy_cleanup(curl); 
	fclose(file);

	CHECK(res==0);

	return true;
}

bool MatDownloader::downloadFileThread(char * address, char * filename)
{
	CHECK(address);
	CHECK(filename);

	threadURL = address;
	threadFilename = filename;

	DWORD threadId;
	threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DownloadFileThreadProc), (LPVOID)this, 0, &threadId);

	if (!threadHandle)
		return false;

	return true;
}


DWORD WINAPI DownloadFileThreadProc(LPVOID lpParameter)
{
	MatDownloader * md = (MatDownloader *)lpParameter;
	CHECK(md);

	bool res = md->downloadFile(md->threadURL, md->threadFilename);

	if (!res)
	{
		if (!(md->threadURL))
			Logger::add("url null");
		else
			Logger::add(md->threadURL);
		Logger::add(md->threadFilename);
	}

	if (md->tDoneCallback)
		(*md->tDoneCallback)(md->tdObj, res);

	return 0;
}