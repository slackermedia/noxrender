#include <windows.h>

#ifndef __DLL_IMPORT_OR_EXPORT
#define __DLL_IMPORT_OR_EXPORT

#if defined EVERYTHINGDLL_EXPORTS
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif

class ThemeManager;

void InitializeApplication(bool showLog);
bool doRegistryStuffOnStartup(bool showLog);
bool dirExists(char * dirName);
bool fileExists(char * filename);
int fileSize(char * filename);
bool correctFilename(char * filename);		// NO PATH!!!!
char * getDirectory(char * filename);
char * getOnlyFile(char * filename);
char * addOneToFilename(char * filename);
char * copyDirPathWithoutLastBackslash(char * path, bool correctRoot);
bool isFileExtSceneNox(char * filename);
bool isFileExtSceneNxs(char * filename);
bool isFileExtPreset(char * filename);
bool isFileExtMaterial(char * filename);


char * openDirectoryDialog(HWND hwnd, char* title, char * startDir);
char * saveDirectoryDialog(HWND hwnd, char* title, char * startDir);
char * openFileDialog(HWND hwnd, char * loadFilter, char * dialogTitle, char * defaultExtension, int extensionDefaultIndex=1, char * initDir=0);
char * saveFileDialog(HWND hwnd, char * saveFilter, char * dialogTitle, char * defaultExtension, int extensionDefaultIndex=1);
char * saveFileDialogWithOptions(HWND hwnd, char * saveFilter, char * dialogTitle, char * defaultExtension, int extensionDefaultIndex=1);
void disableControl(HWND hDlg, int itemID);
void  enableControl(HWND hDlg, int itemID);
void setControlEnabled(bool enabled, HWND hDlg, int itemID);
char * getProcessorInfo(bool addnumcores);
bool getProcessorInfoAdvanced(int &numCPUs, int &numCores, int &numThreads);
DWORDLONG getMemorySize();
char * getFormatedMemorySize();
DECLDIR char * copyString(char * src, unsigned int addSpace=0);
char * createMD5(char * txt);
bool saveUserPassToRegistry(char * user, char * password, char salt);
bool loadUserPassFromRegistry(char ** user, char ** password, char salt);
bool delUserPassFromRegistry();
bool checkForNOXUpdateAsThread();
bool checkLRegStatus();
bool runUpdater(bool silentMode);
bool checkBetaTesterStatusFromRegistry();
bool checkAutoupdateStatusFromRegistry();
bool setAutoupdateStatusInRegistry(bool on);
bool checkLegacyModeFromRegistry();
void invalidateWithParentRedraw(HWND hWnd);

class GlobalWindowSettings
{
public:
	static ThemeManager colorSchemes;
};

#endif

