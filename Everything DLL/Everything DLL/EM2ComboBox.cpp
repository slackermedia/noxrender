#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <stdio.h>
#include "EM2Controls.h"
#include "log.h"
#include "RendererMain2.h"

extern HMODULE hDllModule;

#define BN_X 10
#define BN_Y 50
#define BN_W 11
#define BN_H 6

#define BM_X 30
#define BM_Y 50
#define BM_W 11
#define BM_H 6

#define BC_X 50
#define BC_Y 50
#define BC_W 11
#define BC_H 6

#define BD_X 70
#define BD_Y 50
#define BD_W 11
#define BD_H 6


//----------------------------------------------------------------------

EM2ComboBox::EM2ComboBox(HWND hWnd)
{
	hwnd = hWnd;
	hRgnSlider = 0;

	#ifdef GUI2_DRAW_TEXT_SHADOWS
		drawTextShadow = true;
	#else
		drawTextShadow = false;
	#endif

	numItems = 0;
	items = NULL;
	enabledItems = NULL;
	overwrittenHeader = NULL;

	colText = NGCOL_LIGHT_GRAY;
	colTextMouseOver = NGCOL_YELLOW;
	colTextClicked = NGCOL_YELLOW;
	colTextDisabled = NGCOL_DARK_GRAY;
	colTextDisabled = NGCOL_TEXT_DISABLED;
	colTextShadow = RGB(15,15,15);
	colUnwrapBg = RGB(7,7,7);
	colUnwrapBgMouseOver = RGB(34,34,34);
	colUnwrapBgSelected = RGB(7,7,7);
	colUnwrapText = NGCOL_LIGHT_GRAY;
	colUnwrapTextMouseOver = NGCOL_YELLOW;
	colUnwrapTextSelected = NGCOL_YELLOW;
	colUnwrapTextShadow = RGB(15,15,15);
	colUnwrapBorder = RGB(72,72,72);
	colUnwrapSlider = RGB(87,87,87);
	colUnwrapSliderMouseOver = RGB(97,97,97);
	colUnwrapSliderClicked = RGB(97,97,97);
	colUnwrapTextDisabled = NGCOL_DARK_GRAY;
	shiftUnwrapRight = 0;

	hFontMain = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hFontUnwrapped = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfontmain = true;
	autodelfontunwrapped = true;

	bgImage = NULL;
	bgShiftX = 0;
	bgShiftY = 0;

	ellipsis = false; 
	moveArrow = true;
	useCursors = true;

	scrollPos = 0;
	scrollHeight = 0;
	maxScrollPos = 0;

	align = ALIGN_RIGHT;
	alignUnwrap = ALIGN_LEFT;

	GetClientRect(hwnd, &mouseRect);
	mouseOverItem = 3;
	selected = 1;
	nowScrolling = false;

	isUnwrapped = false;
	mouseOverMain = false;

	maxShownRows = 5;
	rowHeight = 28;
	rowWidth = 200;
	rowStart = 0;
}

//----------------------------------------------------------------------

EM2ComboBox::~EM2ComboBox()
{
	deleteItems();

	if  (hFontMain  &&  autodelfontmain)
		DeleteObject(hFontMain);
	hFontMain = 0;
	if  (hFontUnwrapped  &&  autodelfontunwrapped)
		DeleteObject(hFontUnwrapped);
	hFontUnwrapped = 0;

}

//----------------------------------------------------------------------

void InitEM2ComboBox()
{
    WNDCLASSEX wc;
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2ComboBox";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2ComboBoxProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2ComboBox *);
    wc.hIconSm        = 0;
    ATOM a = RegisterClassEx(&wc);

    WNDCLASSEX wc2;
    wc2.cbSize         = sizeof(wc2);
    wc2.lpszClassName  = "EM2ComboBoxUnwrap";
    wc2.hInstance      = GetModuleHandle(NULL);
	wc2.lpfnWndProc    = EM2ComboBoxUnwrappedProc;
    wc2.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc2.hIcon          = 0;
    wc2.lpszMenuName   = 0;
    wc2.hbrBackground  = (HBRUSH)0;
    wc2.style          = 0;
    wc2.cbClsExtra     = 0;
    wc2.cbWndExtra     = sizeof(EM2ComboBox *);
    wc2.hIconSm        = 0;
    ATOM b = RegisterClassEx(&wc2);
}

//----------------------------------------------------------------------

EM2ComboBox * GetEM2ComboBoxInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2ComboBox * emcb = (EM2ComboBox *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2ComboBox * emcb = (EM2ComboBox *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcb;
}

//----------------------------------------------------------------------

void SetEM2ComboBoxInstance(HWND hwnd, EM2ComboBox *emcb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcb);
	#endif
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2ComboBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2ComboBox * emcb = GetEM2ComboBoxInstance(hwnd);

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2ComboBox * emcb1 = new EM2ComboBox(hwnd);
				SetEM2ComboBoxInstance(hwnd, (EM2ComboBox *)emcb1);			
			}
			break;
		case WM_DESTROY:
			{
				EM2ComboBox * emcb1;
				emcb1 = GetEM2ComboBoxInstance(hwnd);
				delete emcb1;
				SetEM2ComboBoxInstance(hwnd, 0);
			}
			break;
		case CB_GETCURSEL:
			{
				return (LRESULT)emcb->selected;
			}
			break;
		case 13001:
			{
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), CBN_SELCHANGE), (LPARAM)hwnd);
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_SIZE:
			{
				//GetClientRect(hwnd, &emcb->mouseRect);
				emcb->mouseRect.bottom = HIWORD(lParam);
			}
			break;
		case WM_PAINT:
			{
				if (!emcb) 
					break;

				RECT rect;
				HDC hdc;
				PAINTSTRUCT ps;

				GetClientRect(hwnd, &rect);
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);

				if (emcb->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emcb->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emcb->bgShiftX, emcb->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, OPAQUE);
				}

				int arrowPosX = 4;
				char * txt = NULL;
				if (emcb->overwrittenHeader)
					txt = copyString(emcb->overwrittenHeader, 10);
				else
					if (emcb->selected>=0  &&  emcb->selected<emcb->numItems)
						txt = copyString(emcb->items[emcb->selected], 10);
				if (txt)
				{
					HFONT hOldFont = (HFONT)SelectObject(hdc, emcb->getFontMain());

					int ll = (int)strlen(txt);
					SIZE sz;
					int vc = rect.bottom/2;
					int margin_right = 6;
					int margin_left = 0;
					int maxsize = rect.right-BN_W-margin_right-1;

					GetTextExtentPoint32(hdc, txt, ll, &sz);
					if (emcb->ellipsis  &&  sz.cx>maxsize)
					{
						int l = (int)strlen(txt);
						txt[l] = txt[l+1] = txt[l+2] = '.';
						txt[l+3] = 0;
						while (sz.cx>maxsize  &&  l>1)
						{
							l--;
							txt[l] = '.';
							txt[l+3] = 0;
							GetTextExtentPoint32(hdc, txt, l+3, &sz);
						}
						ll = (int)strlen(txt);
					}

					RECT trect = {max(0, rect.right-BN_W-margin_right-sz.cx-1), vc-(sz.cy+1)/2,    rect.right-BN_W-margin_right+1, vc+sz.cy-(sz.cy+1)/2};

					trect.top = vc-(sz.cy+1)/2;
					trect.bottom = vc+sz.cy-(sz.cy+1)/2;
					trect.left = 0;
					trect.right = rect.right-BN_W-margin_right+1;
					int px = max(1, rect.right-BN_W-margin_right-sz.cx);

					arrowPosX = rect.right - BN_W;
					switch (emcb->align)
					{
						case EM2ComboBox::ALIGN_CENTER:
							if (emcb->moveArrow)
							{
								px = max(1, (rect.right-BN_W-margin_right)/2-sz.cx/2);
								arrowPosX = min(sz.cx+px+margin_right, rect.right - BN_W);
							}
							else
							{
								px = max(1, (rect.right-BN_W-margin_right)/2-sz.cx/2);
							}
							break;
						case EM2ComboBox::ALIGN_RIGHT:
							px = max(1, rect.right-BN_W-margin_right-sz.cx);
							arrowPosX = rect.right - BN_W;
							break;
						default:
						case EM2ComboBox::ALIGN_LEFT:
							px = 1;
							if (emcb->moveArrow)
								arrowPosX = min(sz.cx+px+margin_right, rect.right - BN_W);
							break;
					}
					emcb->mouseRect = rect;
					emcb->mouseRect.left = px;
					emcb->mouseRect.right = arrowPosX + BN_W;

					SetBkColor(hdc, NGCOL_TEXT_BACKGROUND);
					if (emcb->drawTextShadow)
					{
						SetTextColor(hdc, emcb->colTextShadow);
						ExtTextOut(hdc,   px-1,  vc-(sz.cy+1)/2,    ETO_CLIPPED, &trect, txt, ll, 0);
						ExtTextOut(hdc,   px+1,  vc-(sz.cy+1)/2,    ETO_CLIPPED, &trect, txt, ll, 0);
						ExtTextOut(hdc,   px,    vc-(sz.cy+1)/2-1,  ETO_CLIPPED, &trect, txt, ll, 0);
						ExtTextOut(hdc,   px,    vc-(sz.cy+1)/2+1,  ETO_CLIPPED, &trect, txt, ll, 0);
					}

					if (disabled)
						SetTextColor(hdc, emcb->colTextDisabled);
					else
						if (emcb->isUnwrapped)
							SetTextColor(hdc, emcb->colTextClicked);
						else
							if (emcb->mouseOverMain)
								SetTextColor(hdc, emcb->colTextMouseOver);
							else
								SetTextColor(hdc, emcb->colText);

					ExtTextOut(hdc,   px,    vc-(sz.cy+1)/2,    ETO_CLIPPED, &trect, txt, ll, 0);

					SelectObject(hdc, hOldFont);

					free(txt);
				}

				// draw arrow
				int bpos_u = rect.bottom/2 - BN_H/2;
				int bpos_l = arrowPosX;
				HBITMAP hBArrrow = EM2Elements::getInstance()->getHBitmap();
				HDC thdc2 = CreateCompatibleDC(hdc);
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc2, hBArrrow);
				BLENDFUNCTION blendFunction;
				blendFunction.BlendOp = AC_SRC_OVER;
				blendFunction.BlendFlags = 0;
				blendFunction.SourceConstantAlpha = 255;
				blendFunction.AlphaFormat = AC_SRC_ALPHA;
				if (disabled)
					AlphaBlend(hdc, bpos_l, bpos_u,  BD_W,BD_H ,thdc2, BD_X,BD_Y,BD_W,BD_H, blendFunction);
				else
					if (emcb->isUnwrapped)
						AlphaBlend(hdc, bpos_l, bpos_u,  BC_W,BC_H ,thdc2, BC_X,BC_Y,BC_W,BC_H, blendFunction);
					else
						if (emcb->mouseOverMain)
							AlphaBlend(hdc, bpos_l, bpos_u,  BM_W,BM_H ,thdc2, BM_X,BM_Y,BM_W,BM_H, blendFunction);
						else
							AlphaBlend(hdc, bpos_l, bpos_u,  BN_W,BN_H ,thdc2, BN_X,BN_Y,BN_W,BN_H, blendFunction);
				SelectObject(thdc2, oldBitmap);
				DeleteDC(thdc2);


				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_MOUSEMOVE:
			{
				POINT p = { (short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				RECT rect;
				GetClientRect(hwnd, &rect);
				if (!GetCapture())
					SetCapture(hwnd);

				if (PtInRect(&emcb->mouseRect, p))
				{
					if (!emcb->mouseOverMain)
					{
						if (emcb->useCursors)
							SetCursor(LoadCursor (NULL, IDC_HAND));
						InvalidateRect(hwnd, NULL, false);
					}
					emcb->mouseOverMain = true;
				}
				else
				{
					if (emcb->mouseOverMain)
					{
						if (emcb->useCursors)
							SetCursor(LoadCursor (NULL, IDC_ARROW));
						InvalidateRect(hwnd, NULL, false);
					}
					emcb->mouseOverMain = false;
				}

				if (!PtInRect(&rect, p))
				{
					InvalidateRect(hwnd, NULL, false);
					emcb->mouseOverMain = false;
					ReleaseCapture();
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emcb->mouseOverMain = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				ReleaseCapture();
				InvalidateRect(hwnd, NULL, false);

				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT p = { (short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				if (PtInRect(&emcb->mouseRect, p))
				{
					if (emcb->useCursors)
						SetCursor(LoadCursor (NULL, IDC_ARROW));
					emcb->mouseOverMain = false;
					emcb->isUnwrapped = true;
					RECT orect;
					GetWindowRect(hwnd, &orect);

					int w = emcb->rowWidth + 2;
					int h = emcb->rowHeight * min(emcb->maxShownRows, emcb->numItems) + 2;

					char buf[256];
					sprintf_s(buf, 256, "opening combo - hwnd: %d  emcb: %lld", hwnd, emcb);
					Logger::add(buf);

					HWND hUnwrap = CreateWindow("EM2ComboBoxUnwrap", "", WS_VISIBLE | WS_POPUP,
						orect.left+emcb->shiftUnwrapRight, orect.bottom, w, h, hwnd, (HMENU)0, 
						GetModuleHandle(NULL), emcb);

					emcb->updateScrollDimensions(hUnwrap);
					emcb->rowStart = max(0, emcb->selected-min(emcb->maxShownRows, emcb->numItems)/2);
					emcb->updateScrollFromPos();
					emcb->updateRegionsScroll(hUnwrap);
				}
			}
			break;
		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2ComboBoxUnwrappedProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2ComboBox * emcb = GetEM2ComboBoxInstance(hwnd);
	static bool initialized = false; 
	static int ly=0;
	static int lsbst=0;

    switch(msg)
    {
		case WM_CREATE:
			{
				if (!lParam)
				{
					DestroyWindow(hwnd);
					break;
				}
				emcb = (EM2ComboBox *)(((CREATESTRUCT*)lParam)->lpCreateParams);
				if (!emcb)
				{
					DestroyWindow(hwnd);
					break;
				}
				SetEM2ComboBoxInstance(hwnd, emcb);

				SetFocus(hwnd);
				ReleaseCapture();
				SetCapture(hwnd);
				initialized = true;
				emcb->updateScrollFromPos();
				emcb->updateRegionsScroll(hwnd);
			}
			break;
		case WM_DESTROY:
			{
				initialized = false;
				emcb->isUnwrapped = false;
				InvalidateRect(emcb->hwnd, NULL, false);
				emcb = NULL;
			}
			break;
		case WM_SIZE:
			{
			}
			break;
		case WM_PAINT:
			{
				if (!emcb) 
					break;

				RECT rect;
				HDC hdc;
				PAINTSTRUCT ps;

				GetClientRect(hwnd, &rect);
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);

				HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emcb->colUnwrapBorder));
				HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emcb->colUnwrapBg));
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, hOldBrush));
				DeleteObject(SelectObject(hdc, hOldPen2));
				SetBkMode(hdc, TRANSPARENT);

				int shownRows = min(emcb->maxShownRows, emcb->numItems);
				int firstShown = max(0, min(emcb->rowStart, emcb->numItems-shownRows));
				int lastShown = min(firstShown+shownRows-1, emcb->numItems-1);
				int rightRow = rect.right-1;
				if (emcb->numItems>emcb->maxShownRows)
					rightRow = rect.right - 14;

				if (emcb->selected >= firstShown  &&  emcb->selected <= lastShown)
				{
					HBRUSH brBgSelected = CreateSolidBrush(emcb->colUnwrapBgSelected);
					RECT drect = { 1, (emcb->selected-firstShown)*emcb->rowHeight+1, rightRow, (emcb->selected-firstShown+1)*emcb->rowHeight+1};
					FillRect(hdc, &drect, brBgSelected);
					DeleteObject(brBgSelected);
				}
				if (emcb->mouseOverItem >= firstShown  &&  emcb->mouseOverItem <= lastShown)
				{
					HBRUSH brBgMouseOver = CreateSolidBrush(emcb->colUnwrapBgMouseOver);
					RECT drect = { 1, (emcb->mouseOverItem-firstShown)*emcb->rowHeight+1, rightRow, (emcb->mouseOverItem-firstShown+1)*emcb->rowHeight+1};
					FillRect(hdc, &drect, brBgMouseOver);
					DeleteObject(brBgMouseOver);
				}

				HFONT hOldFont = (HFONT)SelectObject(hdc, emcb->getFontUnwrapped());

				for (int i=0; i<shownRows; i++)
				{
					int curid = firstShown+i;
					if (curid>=emcb->numItems)
						continue;
					char * txt = emcb->items[curid];
					if (!txt)
						continue;

					if (!strcmp(txt, "-"))
					{
						HPEN hcPen = CreatePen(PS_SOLID, 1, emcb->colUnwrapTextDisabled);
						HPEN hbPen = CreatePen(PS_SOLID, 1, emcb->colUnwrapTextShadow);
						HPEN hOldPen = (HPEN)SelectObject(hdc, hcPen);
						int py =  i*emcb->rowHeight + emcb->rowHeight/2;
						int mm = 10;
						MoveToEx(hdc, mm+1, py, NULL);
						LineTo(hdc, rightRow-mm, py);
						SelectObject(hdc, hbPen);
						MoveToEx(hdc, mm, py, NULL);
						LineTo(hdc, mm+1, py-1);
						LineTo(hdc, rightRow-mm-1, py-1);
						LineTo(hdc, rightRow-mm, py);
						LineTo(hdc, rightRow-mm-1, py+1);
						LineTo(hdc, mm+1, py+1);
						LineTo(hdc, mm, py);
						DeleteObject(hcPen);
						DeleteObject(hbPen);
						continue;
					}

					int ll = (int)strlen(txt);
					COLORREF txtCol;
					if (!emcb->enabledItems[curid])
						txtCol = emcb->colUnwrapTextDisabled;
					else
						if (emcb->mouseOverItem==curid)
							txtCol = emcb->colUnwrapTextMouseOver;
						else
							if (emcb->selected==curid)
								txtCol = emcb->colUnwrapTextSelected;
							else
								txtCol = emcb->colUnwrapText;

					SIZE sz;
					GetTextExtentPoint32(hdc, txt, ll, &sz);
					RECT trect = {    1,    i*emcb->rowHeight+1,    rightRow,    (i+1)*emcb->rowHeight+1};

					int py = i*emcb->rowHeight+1+emcb->rowHeight/2 - (sz.cy+1)/2;
					int px = 1;
					switch (emcb->alignUnwrap)
					{
						case EM2ComboBox::ALIGN_CENTER:		px = (rightRow-sz.cx)/2;	break;
						case EM2ComboBox::ALIGN_RIGHT:		px = rightRow - sz.cx - 10;	break;
						case EM2ComboBox::ALIGN_LEFT:
						default:
							px = 10;	break;
					}

					if (emcb->drawTextShadow)
					{
						SetTextColor(hdc, emcb->colUnwrapTextShadow);
						ExtTextOut(hdc,   px-1,   py,    ETO_CLIPPED, &trect, txt, ll, 0);
						ExtTextOut(hdc,   px+1,   py,    ETO_CLIPPED, &trect, txt, ll, 0);
						ExtTextOut(hdc,   px,   py-1,    ETO_CLIPPED, &trect, txt, ll, 0);
						ExtTextOut(hdc,   px,   py+1,    ETO_CLIPPED, &trect, txt, ll, 0);
					}

					SetTextColor(hdc, txtCol);
					ExtTextOut(hdc,   px,   py,    ETO_CLIPPED, &trect, txt, ll, 0);
				}
				SelectObject(hdc, hOldFont);

				if (emcb->numItems>emcb->maxShownRows)
				{
					emcb->updateRegionsScroll(hwnd);
					HBRUSH hBrSl;
					if (emcb->nowScrolling)
						hBrSl = CreateSolidBrush(emcb->colUnwrapSliderClicked);
					else
						if (emcb->mouseOverSlider)
							hBrSl = CreateSolidBrush(emcb->colUnwrapSliderMouseOver);
						else
							hBrSl = CreateSolidBrush(emcb->colUnwrapSlider);
					FillRgn(hdc, emcb->hRgnSlider, hBrSl);
					DeleteObject(hBrSl);
				}

				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_KILLFOCUS:
			{
				if (initialized)
				{
					ReleaseCapture();
					DestroyWindow(hwnd);
				}
			}
			break;
		case WM_MOUSEWHEEL:
			{
				if (emcb->nowScrolling)
					break;
				int zDelta = (int)wParam;
				if (zDelta<0)
					emcb->rowStart = min(emcb->numItems-emcb->maxShownRows, emcb->rowStart + 1);
				else
					emcb->rowStart = max(0, emcb->rowStart - 1);
				emcb->updateScrollFromPos();
				emcb->updateRegionsScroll(hwnd);

				POINT p = { (short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				ScreenToClient(hwnd, &p);
				LPARAM lParam2 = MAKELONG(p.x, p.y);
				SendMessage(hwnd, WM_MOUSEMOVE, wParam&0xffff, lParam2);
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_MOUSEMOVE:
			{
				POINT p = { (short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				bool drawScroll = (emcb->numItems>emcb->maxShownRows);
				RECT rect, cellsrect;
				GetClientRect(hwnd, &rect);
				rect.left++;
				rect.top++;
				rect.right--;
				rect.bottom--;
				cellsrect = rect;
				if (drawScroll)
					cellsrect.right -= 13;

				if (emcb->nowScrolling)
				{
					emcb->scrollPos = min(emcb->maxScrollPos, max(0, lsbst + p.y-ly));
					emcb->updatePosFromScroll();
					emcb->updateRegionsScroll(hwnd);
					InvalidateRect(hwnd, NULL, false);
				}
				else   // no scrolling
				{
					if (PtInRegion(emcb->hRgnSlider, p.x, p.y))
					{
						if (emcb->mouseOverItem>-1  ||  !emcb->mouseOverSlider)
							InvalidateRect(hwnd, NULL, false);
						emcb->mouseOverItem = -1;
						emcb->mouseOverSlider = true;
					}
					else
					{
						int mo = -1;
						if (PtInRect(&cellsrect, p))
						{
							int shownRows = min(emcb->maxShownRows, emcb->numItems);
							int firstShown = max(0, min(emcb->rowStart, emcb->numItems-shownRows));
							mo = (p.y-1)/emcb->rowHeight + firstShown;
							if (mo>=emcb->numItems)
								mo = -1;
							if (mo>=0 && mo<emcb->numItems)
								if (!emcb->enabledItems[mo])
									mo = -1;
						}
						if (mo!=emcb->mouseOverItem  ||  emcb->mouseOverSlider)
							InvalidateRect(hwnd, NULL, false);
						emcb->mouseOverItem = mo;
						emcb->mouseOverSlider = false;
					}
				}
							InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT p = { (short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				rect.right--;
				rect.left++;
				rect.top++;
				rect.bottom--;
				if (PtInRect(&rect, p))
				{
					if (emcb->numItems>emcb->maxShownRows  &&  p.x >rect.right-13)	// on trackbar side
					{
						if (PtInRegion(emcb->hRgnSlider, p.x, p.y))
						{
							emcb->nowScrolling = true;
							ly = p.y;
							lsbst = emcb->scrollPos;
							InvalidateRect(hwnd, NULL, false);
						}
						else
						{
							if (p.y<3+emcb->scrollPos)
							{
									emcb->rowStart = max(0, emcb->rowStart-emcb->maxShownRows);
									emcb->updateScrollFromPos();
									InvalidateRect(hwnd, NULL, false);
							}
							else
								if (p.y>3+emcb->scrollPos+emcb->scrollHeight)
								{
									emcb->rowStart = min(emcb->numItems-emcb->maxShownRows, emcb->rowStart+emcb->maxShownRows);
									emcb->updateScrollFromPos();
									InvalidateRect(hwnd, NULL, false);
								}

						}


					}
					else
					{
						int shownRows = min(emcb->maxShownRows, emcb->numItems);
						int firstShown = max(0, min(emcb->rowStart, emcb->numItems-shownRows));
						int sel = (p.y-1)/emcb->rowHeight + firstShown;
						if (sel>=emcb->numItems)
							sel = -1;
						if (sel>=0  &&  sel<emcb->numItems)
							if (!emcb->enabledItems[sel])
								sel = -1;
						ReleaseCapture();
						if (emcb->selected != sel)
						{
							emcb->selected = sel;
							SendMessage(emcb->hwnd, 13001, 0,0);
						}
						DestroyWindow(hwnd);
					}
				}
				else
				{
					ReleaseCapture();
					DestroyWindow(hwnd);
				}
			}
			break;
		case WM_LBUTTONUP:
			{
				if (emcb->nowScrolling)
				{
					emcb->nowScrolling = false;
					SendMessage(hwnd, WM_MOUSEMOVE, 0, lParam);
					InvalidateRect(hwnd, NULL, false);
				}
			}
			break;
		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

bool EM2ComboBox::setNumItems(int newnum)
{
	if (newnum<0)
		return false;
	newnum = min(newnum, 1000);

	bool * newenabled = (bool *)malloc(sizeof(bool)*newnum);
	if (!newenabled)
		return false;
	char ** newitems = (char **)malloc(sizeof(char *)*newnum);
	if (!newitems)
		return false;
	for (int i=0; i<newnum; i++)
		newitems[i] = NULL;
	for (int i=0; i<newnum; i++)
		newenabled[i] = true;

	deleteItems();

	items = newitems;
	numItems = newnum;
	enabledItems = newenabled;

	return true;
}

//----------------------------------------------------------------------

bool EM2ComboBox::setItem(int id, char * caption, bool enabled)
{
	if (id<0 || id>=numItems)
		return false;
	if (!items)
		return false;

	char * ccaption = copyString(caption);
	if (caption && !ccaption)
		return false;

	if (items[id])
		free(items[id]);
	items[id] = ccaption;
	enabledItems[id] = enabled;

	return true;
}

//----------------------------------------------------------------------

void EM2ComboBox::deleteItems()
{
	if (items)
	{
		for (int i=0; i<numItems; i++)
		{
			if (items[i])
				free(items[i]);
			items[i] = NULL;
		}
		free(enabledItems);
		free(items);
	}
	enabledItems = NULL;
	items = NULL;
	numItems = 0;
}

//----------------------------------------------------------------------

bool EM2ComboBox::setHeader(char * newname)
{
	char * ccaption = copyString(newname);
	if (newname && !ccaption)
		return false;
	if (overwrittenHeader)
		free(overwrittenHeader);
	overwrittenHeader = ccaption;
	return true;
}

//----------------------------------------------------------------------

bool EM2ComboBox::setFontMain(HFONT hNewFont, bool autodel)
{
	if (autodelfontmain)
		DeleteObject(hFontMain);
	hFontMain = hNewFont;
	autodelfontmain = autodel;
	return true;
}

//----------------------------------------------------------------------

bool EM2ComboBox::setFontUnwrapped(HFONT hNewFont, bool autodel)
{
	if (autodelfontunwrapped)
		DeleteObject(hFontUnwrapped);
	hFontUnwrapped = hNewFont;
	autodelfontunwrapped = autodel;
	return true;
}

//----------------------------------------------------------------------

HFONT EM2ComboBox::getFontMain()
{
	return hFontMain;
}

//----------------------------------------------------------------------

HFONT EM2ComboBox::getFontUnwrapped()
{
	return hFontUnwrapped;
}

//----------------------------------------------------------------------

void EM2ComboBox::updateScrollDimensions(HWND hUnwrapped)
{
	RECT rect;
	GetClientRect(hUnwrapped, &rect);
	int totalH = rect.bottom - 6;
	if (numItems<=maxShownRows)
	{
		scrollPos = 0;
		scrollHeight = totalH;
		maxScrollPos = 0;
		return;
	}

	scrollHeight = totalH * maxShownRows / numItems;
	scrollHeight = max(10, scrollHeight);
	maxScrollPos = totalH - scrollHeight;
}

//----------------------------------------------------------------------

void EM2ComboBox::updateScrollFromPos()
{
	scrollPos = maxScrollPos * rowStart / max(1, numItems-maxShownRows);
}

//----------------------------------------------------------------------

void EM2ComboBox::updatePosFromScroll()
{
	rowStart = (numItems-maxShownRows) * scrollPos / maxScrollPos;
}

//----------------------------------------------------------------------

void EM2ComboBox::updateRegionsScroll(HWND hUnwraped)
{
	RECT rect;
	GetClientRect(hUnwraped, &rect);

	int sbWidth = 9;
	int sbStart = scrollPos;
	int sbEnd = sbStart + scrollHeight;
	int l = rect.right - sbWidth - 3;
	int y1 = 3;

	POINT slp[] = {
		{l+sbWidth/2,	y1+sbStart-1}, 
		{l+sbWidth,		y1+sbStart+sbWidth/2}, 
		{l+sbWidth,		y1+sbEnd-sbWidth/2-1}, 
		{l+sbWidth/2,	y1+sbEnd}, 
		{l,				y1+sbEnd-sbWidth/2-1}, 
		{l,				y1+sbStart+sbWidth/2}, 
	};
	if (hRgnSlider)
		DeleteObject(hRgnSlider);
	hRgnSlider = CreatePolygonRgn(slp, 6, WINDING);
}

//----------------------------------------------------------------------
