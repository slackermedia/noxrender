#ifndef __REGIONS_H
#define __REGIONS_H

#define RSIZE 8

class ImageRegion
{
public:
	bool * regions;
	double * hits;
	long long overallHits;
	bool regionsExists;
	int airbrush_size;
	int rnX, rnY;
	int ibX, ibY;
	int * regionsSelected;
	int numRegionsSelected;


	bool isRegionSelected(int px, int py);		// pixel coords
	bool randomPointFromRegions(float &x, float &y);
	bool makeRegionsList();
	bool createRegions(int bwidth, int bheight);
	void clearRegions();
	void fillRegions();
	void inverseRegions();
	void checkRegions();
	void incrementHits(int num=1);
	static void copyFast(ImageRegion &src, ImageRegion &dst);

	ImageRegion();
	~ImageRegion();
};

#endif
