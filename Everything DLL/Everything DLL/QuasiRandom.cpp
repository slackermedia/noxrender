#define _CRT_RAND_S
#include "QuasiRandom.h"

double getHalton(unsigned int number, int base)
{
	double res = 0.0f;
	int iloraz;
	double dz = 1.0/base;
	iloraz = number;
	while (iloraz >= base)
	{
		res += ((iloraz%base)*dz);
		iloraz = iloraz / base;
		dz = dz/base;
	}
	res += ((iloraz%base)*dz);
	return res;
}

void getDoubleHalton(int number, float &x, float &y)
{
	float x1,y1;
	x1 = y1 = 0.5f;
	int base = 4;
	float dz = 0.5f;
	int iloraz, reszta;
	iloraz = number;
	while (iloraz > 0)
	{
		dz = dz*0.5f;
		reszta = iloraz % base;
		iloraz = iloraz / base;

		switch (reszta)
		{
		case 3:
			x1 -= dz;
			y1 -= dz;
			break;
		case 2:
			x1 += dz;
			y1 -= dz;
			break;
		case 1:
			x1 -= dz;
			y1 += dz;
			break;
		case 0:
			x1 += dz;
			y1 += dz;
			break;
		}
	}
	x = x1;
	y = y1;
}


unsigned int getMarsaglia(unsigned int seed1, unsigned int seed2)
{
	#define aMarsaglia 18000
	#define bMarsaglia 36969
	static unsigned int X = 6745745;
	static unsigned int Y = 43634;
	if (seed1 && seed2)
	{
		X = seed1;
		Y = seed2;
	}

	X = aMarsaglia*(X&0xFFFF)+(X>>16);
	Y = bMarsaglia*(Y&0xFFFF)+(Y>>16);

	return (X<<16)+(Y&0xFFFF);
}

//==============================================================================================


#define GRID(x)     ((int)((x)*GRID_SIZE))%GRID_SIZE
#define GRIDNEXT(x) (((int)((x)*GRID_SIZE)+1)%GRID_SIZE)
#define GRIDPREV(x) (((int)((x+1)*GRID_SIZE)-1)%GRID_SIZE)


Random2DCircleSampler::Random2DCircleSampler()
{
	s = 0;
	numSamples = 0;
	cur = 0;
}

Random2DCircleSampler::~Random2DCircleSampler()
{
	if (s)
		free(s);
}


void Random2DCircleSampler::getNext(float &x, float &y)
{
	x = s[2*cur];
	y = s[2*cur+1];
	cur++;
	cur = cur%numSamples;
}

void Random2DCircleSampler::getThat(int n, float &x, float &y)
{
	cur = n%numSamples;
	x = s[2*cur];
	y = s[2*cur+1];
	cur++;
	cur = cur%numSamples;
}



void Random2DCircleSampler::create(int num, int triesPerPoint)
{
	for (int i=0; i<GRID_SIZE; i++)
		for (int j=0; j<GRID_SIZE; j++)
			grid[i][j].clear();

	if (s)
		_aligned_free(s);
	s = (float *)_aligned_malloc(sizeof(float)*num*2, 8);
	if (!s)
		return;
	numSamples = num;
	cur = 0;

	int nn = triesPerPoint;	// number of probes for one point
	float d1,d2,d3,d4,d5, dx,dy;
	int u,v;

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = ((float)rand()/(float)RAND_MAX);
	#endif

	#ifdef RAND_PER_THREAD
		s[0] = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
		s[1] = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		unsigned int ui1;
		while(rand_s(&ui1));
		s[0]  = (float)((double)ui1/(double)(UINT_MAX));
		while(rand_s(&ui1));
		s[1]  = (float)((double)ui1/(double)(UINT_MAX));
	#endif
	u = GRID(s[0]);
	v = GRID(s[1]);
	grid[u][v].push_back(0);

	vector<int>::iterator it;
	vector<int> *cgrid;

	int i,j,k;
	for (i=1; i<num; i++)
	{
		float cX=10, cY=10, cD2=0;
		for (k=0; k<nn; k++)
		{
			#ifdef RAND_PER_THREAD
				float rX = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
				float rY = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
			#else
				while(rand_s(&ui1));
				float rX  = (float)((double)ui1/(double)(UINT_MAX));
				while(rand_s(&ui1));
				float rY  = (float)((double)ui1/(double)(UINT_MAX));
			#endif
			float rr = (rX-0.5f)*(rX-0.5f) + (rY-0.5f)*(rY-0.5f);
			if (rr > 0.25)
				continue;

			float cl = 10;

			u = GRID(rX);
			v = GRID(rY);
			int found = 0;
			cgrid = &(grid[u][v]);

			for (j=0; j<9; j++)
			{
				switch (j)
				{
					case 0: cgrid = &(grid[GRID    (rX)][GRID    (rY)]); break;
					case 1: cgrid = &(grid[GRIDNEXT(rX)][GRID    (rY)]); break;
					case 2: cgrid = &(grid[GRIDNEXT(rX)][GRIDNEXT(rY)]); break;
					case 3: cgrid = &(grid[GRIDNEXT(rX)][GRIDPREV(rY)]); break;
					case 4: cgrid = &(grid[GRIDPREV(rX)][GRID    (rY)]); break;
					case 5: cgrid = &(grid[GRIDPREV(rX)][GRIDNEXT(rY)]); break;
					case 6: cgrid = &(grid[GRIDPREV(rX)][GRIDPREV(rY)]); break;
					case 7: cgrid = &(grid[GRID    (rX)][GRIDNEXT(rY)]); break;
					case 8: cgrid = &(grid[GRID    (rX)][GRIDPREV(rY)]); break;
				}
				if (!(cgrid->empty()))
				{
					for (it = cgrid->begin();     it < cgrid->end();    it++)
					{
						int n = *it;

						found ++;
						dx = s[2*n  ]-rX;	
						dy = s[2*n+1]-rY;
						d1 = dx*dx + dy*dy;
						if (d1 < cl)
						{
							cl = d1;
						}
					}
				}
			}

			if (found < 1)
			{
				for (j=0; j<i; j++)
				{
					dx = s[2*j  ]-rX;
					dy = s[2*j+1]-rY;
					d1 = dx*dx + dy*dy;
					//cl = d1;
					dx = s[2*j  ]-rX-1;
					dy = s[2*j+1]-rY;
					d2 = dx*dx + dy*dy;
					dx = s[2*j  ]-rX+1;
					dy = s[2*j+1]-rY;
					d3 = dx*dx + dy*dy;
					dx = s[2*j  ]-rX;
					dy = s[2*j+1]-rY-1;
					d4 = dx*dx + dy*dy;
					dx = s[2*j  ]-rX;
					dy = s[2*j+1]-rY+1;
					d5 = dx*dx + dy*dy;
					d1 = min(min(min(min(d1, d2), d3), d4), d5);
					if (d1 < cl)
					{
						cl = d1;
					}
				}
			}

			if (cl > cD2)
			{
				cD2 = cl;
				cX = rX;
				cY = rY;
			}
		}

		s[2*i  ] = cX;
		s[2*i+1] = cY;

		u = GRID(cX);
		v = GRID(cY);
		grid[u][v].push_back(i);

	}

	for (i=0; i<num; i++)
	{
		s[2*i  ] = s[2*i  ] * 2.0f - 1.0f;
		s[2*i+1] = s[2*i+1] * 2.0f - 1.0f;
	}

	for (int i=0; i<GRID_SIZE; i++)
		for (int j=0; j<GRID_SIZE; j++)
			grid[i][j].clear();
}

void Random2DCircleSampler::freeSamples()
{
	if (s)
		_aligned_free(s);
	s = NULL;
	numSamples = 0;
	cur = 0;
}

void Random2DCircleSampler::zeroEverything()	// be very careful, memory may leak
{
	s = NULL;
	numSamples = 0;
	cur = 0;
}

//==============================================================================================
