#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "ColorSchemes.h"
#include "log.h"
#include "DLL.h"

#define RECALC_PREV_AS_THREAD

#define TIMER_REDRAW_TEX_ID 151

extern HMODULE hDllModule;

void lockBeforeLoading(HWND hwnd);
void unlockAfterLoading(HWND hwnd);
bool updateGUItoTexModifier(HWND hwnd, Texture * tex);
bool updateTexModifierToGui(HWND hwnd, Texture * tex);
void refreshTexPost(HWND hwnd);
DWORD WINAPI refreshPostThreadProc(LPVOID lpParameter);
Texture * getTextureFromEMPV(HWND hwnd);
void lockOnRefresh(HWND hwnd);
void unlockAfterRefresh(HWND hwnd);


INT_PTR CALLBACK TexDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

			HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
			SetWindowPos(hPrev, HWND_TOP, 0,0, 256,256, SWP_NOZORDER | SWP_NOMOVE);
			EMPView * empv = GetEMPViewInstance(hPrev);
			GlobalWindowSettings::colorSchemes.apply(empv);
			empv->allowDragFiles();
			HWND hProgress = GetDlgItem(hWnd, IDC_TEX_PROGRESSBAR);
			SetWindowPos(hProgress, HWND_TOP, 0,0, 256,12, SWP_NOZORDER | SWP_NOMOVE);
			EMProgressBar * empb = GetEMProgressBarInstance(hProgress);
			GlobalWindowSettings::colorSchemes.apply(empb);
			empb->setPos(0);

			Texture * tex = (Texture *)lParam;
			Texture * tex2 = new Texture();
			empv->reserved2 = tex2;

			if (tex)
			{
				empv->reserved1 = (void *)tex;
				tex2->copyFromOther(tex);

				HWND hFName = GetDlgItem(hWnd, IDC_TEX_FILENAME);
				EMText * emtfilename = GetEMTextInstance(hFName);
				if (emtfilename)
					emtfilename->changeCaption(tex->filename);
	
				updateDialogFromTex(tex, hWnd);		// print depth and resolution
			}


			EMGroupBar * emg1 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_TEX_GROUP_PREVIEW));
			EMGroupBar * emg2 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_TEX_GROUP_FILE));
			EMGroupBar * emg3 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_TEX_GROUP_IMAGE));
			EMGroupBar * emg4 = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_TEX_GROUP_MODIFIERS));
			EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_TEX_INTERPOLATE));
			EMButton * emb1 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_CANCEL));
			EMButton * emb2 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_OK));
			EMButton * emb3 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_LOAD));
			EMButton * emb4 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RELEASE));
			EMButton * emb5 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_PICK));
			EMEditTrackBar * emtb_bri = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_BRIGHTNESS));
			EMEditTrackBar * emtb_con = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_CONTRAST));
			EMEditTrackBar * emtb_sat = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_SATURATION));
			EMEditTrackBar * emtb_hue = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_HUE));
			EMEditTrackBar * emtb_red = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_RED));
			EMEditTrackBar * emtb_gre = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_GREEN));
			EMEditTrackBar * emtb_blu = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_BLUE));
			EMEditTrackBar * emtb_gam = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_GAMMA));
			EMCheckBox * emc_inv = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_TEX_INVERT));

			emtb_bri->setEditBoxWidth(40);
			emtb_con->setEditBoxWidth(40);
			emtb_sat->setEditBoxWidth(40);
			emtb_hue->setEditBoxWidth(40);
			emtb_red->setEditBoxWidth(40);
			emtb_gre->setEditBoxWidth(40);
			emtb_blu->setEditBoxWidth(40);
			emtb_gam->setEditBoxWidth(40);

			GlobalWindowSettings::colorSchemes.apply(emg1);
			GlobalWindowSettings::colorSchemes.apply(emg2);
			GlobalWindowSettings::colorSchemes.apply(emg3);
			GlobalWindowSettings::colorSchemes.apply(emg4);
			GlobalWindowSettings::colorSchemes.apply(emb1);
			GlobalWindowSettings::colorSchemes.apply(emb2);
			GlobalWindowSettings::colorSchemes.apply(emb3);
			GlobalWindowSettings::colorSchemes.apply(emb4);
			GlobalWindowSettings::colorSchemes.apply(emb5);
			GlobalWindowSettings::colorSchemes.apply(emcb);
			//GlobalWindowSettings::colorSchemes.apply(emcb2);
			GlobalWindowSettings::colorSchemes.apply(emtb_bri);
			GlobalWindowSettings::colorSchemes.apply(emtb_con);
			GlobalWindowSettings::colorSchemes.apply(emtb_sat);
			GlobalWindowSettings::colorSchemes.apply(emtb_hue);
			GlobalWindowSettings::colorSchemes.apply(emtb_red);
			GlobalWindowSettings::colorSchemes.apply(emtb_gre);
			GlobalWindowSettings::colorSchemes.apply(emtb_blu);
			GlobalWindowSettings::colorSchemes.apply(emtb_gam);
			GlobalWindowSettings::colorSchemes.apply(emc_inv);
			
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT1)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT2)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT3)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT4)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT5)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT6)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT7)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT8)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT9)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT10)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT11)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT12)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT13)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT14)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TXT15)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_FILENAME)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_RESOLUTION)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_TYPE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_COORD)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_COLOR)), true);


			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_BRIGHTNESS)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_CONTRAST)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_SATURATION)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_HUE)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_RED)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_GREEN)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_BLUE)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEX_RESET_GAMMA)));
			
			emtb_bri->setValuesToFloat(TEXMOD_DEF_BRIGHTNESS,	TEXMOD_MIN_BRIGHTNESS,	TEXMOD_MAX_BRIGHTNESS,	0.1f);
			emtb_con->setValuesToFloat(TEXMOD_DEF_CONTRAST,		TEXMOD_MIN_CONTRAST,	TEXMOD_MAX_CONTRAST,	0.1f);
			emtb_sat->setValuesToFloat(TEXMOD_DEF_SATURATION,	TEXMOD_MIN_SATURATION,	TEXMOD_MAX_SATURATION,	0.1f);
			emtb_hue->setValuesToFloat(TEXMOD_DEF_HUE,			TEXMOD_MIN_HUE,			TEXMOD_MAX_HUE,			0.1f);
			emtb_red->setValuesToFloat(TEXMOD_DEF_RED,			TEXMOD_MIN_RED,			TEXMOD_MAX_RED,			0.1f);
			emtb_gre->setValuesToFloat(TEXMOD_DEF_GREEN,		TEXMOD_MIN_GREEN,		TEXMOD_MAX_GREEN,		0.1f);
			emtb_blu->setValuesToFloat(TEXMOD_DEF_BLUE,			TEXMOD_MIN_BLUE,		TEXMOD_MAX_BLUE,		0.1f);
			emtb_gam->setValuesToFloat(TEXMOD_DEF_GAMMA,		TEXMOD_MIN_GAMMA,		TEXMOD_MAX_GAMMA,		0.1f);
			emc_inv->selected = tex->texMod.invert;
			InvalidateRect(emc_inv->hwnd, NULL, false);

			if (tex2->isValid())
				updateTexModifierToGui(hWnd, tex2);
			else
				updateTexModifierToGui(hWnd, tex);
			
			refreshTexPost(hWnd);
			Sleep(50);
			empv->mx = empv->my = 0;
			empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);
		}
		break;
	case WM_DESTROY:
		{
			HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
			EMPView * empv = GetEMPViewInstance(hPrev);
				
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_MOUSEMOVE:
		{
			EMText * emCoords = GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_COORD));
			EMText * emColor = GetEMTextInstance(GetDlgItem(hWnd, IDC_TEX_COLOR));
			HWND hImage = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
			POINT pos;
			pos.x = (short)LOWORD(lParam); 
			pos.y = (short)HIWORD(lParam); 
			ClientToScreen(hWnd, &pos);
			ScreenToClient(hImage, &pos);
			RECT crect;
			GetClientRect(hImage, &crect);
			if (pos.x < 0 || pos.y < 0 || pos.x>crect.right || pos.y>crect.bottom)
			{
				emCoords->changeCaption("");
				emColor->changeCaption("");
				break;
			}
			
			EMPView * empv = GetEMPViewInstance(hImage);
			if (!empv->byteBuff)
			{
				emCoords->changeCaption("");
				emColor->changeCaption("");
				break;
			}

			int cx = crect.right-crect.left;
			int cy = crect.bottom-crect.top;
			int tx =     (int)(pos.x/empv->zoom + empv->mx*cx);
			int ty =     (int)(pos.y/empv->zoom + empv->my*cy);

			if (tx>=empv->byteBuff->width || tx<0 || ty>=empv->byteBuff->height || ty<0)
			{
				emCoords->changeCaption("");
				emColor->changeCaption("");
				break;
			}

			COLORREF color = empv->byteBuff->imgBuf[ty][tx];
			unsigned char colr = GetRValue(color);
			unsigned char colg = GetGValue(color);
			unsigned char colb = GetBValue(color);
			char buff[128];
			sprintf_s(buff, 128, "%d , %d", tx, ty);
			emCoords->changeCaption(buff);
			sprintf_s(buff, 128, "%d , %d , %d", colr, colg, colb);
			emColor->changeCaption(buff);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
			case IDC_TEX_OK:
				{
					if (wmEvent == BN_CLICKED)
					{
						KillTimer(hWnd, TIMER_REDRAW_TEX_ID);

						HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
						EMPView * empv = GetEMPViewInstance(hPrev);
						Texture * tex2 = (Texture *)empv->reserved2;		// new tex
						Texture * tex1 = (Texture *)empv->reserved1;		// old tex
						bool interpolate = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_TEX_INTERPOLATE))->selected;
						
						if (tex1)
						{
							tex1->freeAllBuffers();
							updateGUItoTexModifier(hWnd, tex1);
							delete tex1;
							empv->reserved1 = NULL;
						}

						if (tex2)
							updateGUItoTexModifier(hWnd, tex2);

						ImageByteBuffer * ib = empv->byteBuff;
						empv->byteBuff = NULL;
						if (ib)
						{
							ib->freeBuffer();
							delete ib;
						}

						EndDialog(hWnd, (INT_PTR)(tex2));
					}
				}
				break;
			case IDC_TEX_CANCEL:
				{
					if (wmEvent == BN_CLICKED)
					{
						KillTimer(hWnd, TIMER_REDRAW_TEX_ID);

						HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
						EMPView * empv = GetEMPViewInstance(hPrev);
						Texture * tex1 = (Texture *)empv->reserved1;
						Texture * tex2 = (Texture *)empv->reserved2;
						if (tex2)
						{
							tex2->freeAllBuffers();
							delete tex2;
						}
						empv->reserved2 = NULL;

						if (tex1)
						{
							tex1->freeAllBuffers();
							delete tex1;
						}
						empv->reserved2 = NULL;

						// free preview
						ImageByteBuffer * ib = empv->byteBuff;
						empv->byteBuff = NULL;
						if (ib)
						{
							ib->freeBuffer();
							delete ib;
						}

						EndDialog(hWnd, (INT_PTR)NULL);
					}
				}
				break;
			case IDC_TEX_PREVIEW:
				{
					if (wmEvent==PV_DROPPED_FILE)
					{
						EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_TEX_PREVIEW));
						CHECK(empv);
						if (!empv->lastDroppedFilename)
							break;

						Logger::add("Loading dropped file:");
						Logger::add(empv->lastDroppedFilename);

						lockBeforeLoading(hWnd);

						Texture * tex = new Texture();
						if (tex->loadFromFile(empv->lastDroppedFilename)  &&   tex->isValid())
						{
							HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
							EMPView * empv = GetEMPViewInstance(hPrev);
							Texture * ttex = (Texture *)empv->reserved2;
							if (ttex)
							{
								ttex->freeAllBuffers();
								delete ttex;
							}
							empv->reserved2 = (void *)tex;

							updateDialogFromTex(tex, hWnd);

							empv->mx = empv->my = 0;
							refreshTexPost(hWnd);

							HWND hFName = GetDlgItem(hWnd, IDC_TEX_FILENAME);
							EMText * emtfilename = GetEMTextInstance(hFName);
							emtfilename->changeCaption(tex->filename);

							Logger::add("Texture loaded.");

							Sleep(50);
							empv->mx = empv->my = 0;
							empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);
						}
						else
						{
							MessageBox(0, "Failed opening. It's probably not an image.", "Error", 0);
						}

						if (empv->lastDroppedFilename)
							free(empv->lastDroppedFilename);
						empv->lastDroppedFilename = NULL;
					}
				}
				break;
			case IDC_TEX_RELEASE:
				{
					CHECK(wmEvent==BN_CLICKED);

					KillTimer(hWnd, TIMER_REDRAW_TEX_ID);
					Texture * tex = new Texture();
					HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
					EMPView * empv = GetEMPViewInstance(hPrev);


					Texture * old = (Texture *)empv->reserved2;
					empv->reserved2 = (void*) tex;

					if (old)
					{
						old->freeAllBuffers();
						delete old;
					}

					HWND hFName = GetDlgItem(hWnd, IDC_TEX_FILENAME);
					EMText * emtfilename = GetEMTextInstance(hFName);
					emtfilename->changeCaption("No texture loaded.");

					updateDialogFromTex(tex, hWnd);

					refreshTexPost(hWnd);
				}
				break;
			case IDC_TEX_LOAD:
				{
					if (wmEvent != BN_CLICKED)
						break;

					KillTimer(hWnd, TIMER_REDRAW_TEX_ID);

					Logger::add("Texture load clicked.");

					char * fname = openTexDialog(hWnd);
					if (!fname)
					{
						Logger::add("No file selected.");
						break;
					}

					Logger::add("Loading file:");
					Logger::add(fname);

					lockBeforeLoading(hWnd);

					Texture * tex = new Texture();
					if (tex->loadFromFile(fname)  &&   tex->isValid())
					{
						HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
						EMPView * empv = GetEMPViewInstance(hPrev);
						Texture * ttex = (Texture *)empv->reserved2;
						if (ttex)
						{
							ttex->freeAllBuffers();
							delete ttex;
						}
						empv->reserved2 = (void *)tex;

						empv->mx = empv->my = 0;
						refreshTexPost(hWnd);

						HWND hFName = GetDlgItem(hWnd, IDC_TEX_FILENAME);
						EMText * emtfilename = GetEMTextInstance(hFName);
						emtfilename->changeCaption(tex->filename);

						updateDialogFromTex(tex, hWnd);

						Logger::add("Texture loaded.");

						Sleep(50);

						empv->mx = empv->my = 0;
						empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);
					}
					else
					{
						unlockAfterLoading(hWnd);
					}

					if (fname)
						free(fname);

				}
				break;
			case IDC_TEX_PICK:
				{
					Texture * ttex = getTextureFromEMPV(hWnd);
					char * srcfname = NULL;
					if (ttex)
						srcfname = ttex->fullfilename;
					char * dstfname = (char *)DialogBoxParamW(hDllModule, 
									MAKEINTRESOURCEW(IDD_TEXTURE_PICKER), hWnd, TexPickerDlgProc,(LPARAM)(srcfname));

					if (dstfname)
					{
						if (srcfname &&  !strcmp(srcfname, dstfname))
							break;

						Logger::add("Loading file:");
						Logger::add(dstfname);

						lockBeforeLoading(hWnd);

						Texture * tex = new Texture();
						if (tex->loadFromFile(dstfname)  &&   tex->isValid())
						{
							HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
							EMPView * empv = GetEMPViewInstance(hPrev);
							Texture * ttex = (Texture *)empv->reserved2;
							if (ttex)
							{
								ttex->freeAllBuffers();
								delete ttex;
							}
							empv->reserved2 = (void *)tex;

							empv->mx = empv->my = 0;
							refreshTexPost(hWnd);

							HWND hFName = GetDlgItem(hWnd, IDC_TEX_FILENAME);
							EMText * emtfilename = GetEMTextInstance(hFName);
							emtfilename->changeCaption(tex->filename);

							updateDialogFromTex(tex, hWnd);

							Logger::add("Texture loaded.");

							Sleep(50);
							empv->mx = empv->my = 0;
							empv->zoomResFit(tex->bmp.getWidth(), tex->bmp.getHeight(), false);
						}
						else
						{
							unlockAfterLoading(hWnd);
						}
					}
				}
				break;
			case IDC_TEX_INTERPOLATE:
				{
					HWND hInterp = GetDlgItem(hWnd, IDC_TEX_INTERPOLATE);
					EMCheckBox * emcb = GetEMCheckBoxInstance(hInterp);

					HWND hPrev = GetDlgItem(hWnd, IDC_TEX_PREVIEW);
					EMPView * empv = GetEMPViewInstance(hPrev);

					Texture * tex = (Texture *)empv->reserved2;
					if (tex)
					{
						tex->texMod.interpolateProbe = emcb->selected;
					}
				}
				break;

			case IDC_TEX_RESET_BRIGHTNESS:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_BRIGHTNESS))->setValuesToFloat(TEXMOD_DEF_BRIGHTNESS,	TEXMOD_MIN_BRIGHTNESS,	TEXMOD_MAX_BRIGHTNESS,	0.1f);
				refreshTexPost(hWnd);
				break;
			case IDC_TEX_RESET_CONTRAST:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_CONTRAST))->setValuesToFloat(TEXMOD_DEF_CONTRAST,		TEXMOD_MIN_CONTRAST,	TEXMOD_MAX_CONTRAST,	0.1f);
				refreshTexPost(hWnd);
				break;
			case IDC_TEX_RESET_SATURATION:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_SATURATION))->setValuesToFloat(TEXMOD_DEF_SATURATION,	TEXMOD_MIN_SATURATION,	TEXMOD_MAX_SATURATION,	0.1f);
				refreshTexPost(hWnd);
				break;
			case IDC_TEX_RESET_HUE:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_HUE))->setValuesToFloat(TEXMOD_DEF_HUE,					TEXMOD_MIN_HUE,			TEXMOD_MAX_HUE,			0.1f);
				refreshTexPost(hWnd);
				break;
			case IDC_TEX_RESET_RED:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_RED))->setValuesToFloat(TEXMOD_DEF_RED,					TEXMOD_MIN_RED,			TEXMOD_MAX_RED,			0.1f);
				refreshTexPost(hWnd);
				break;
			case IDC_TEX_RESET_GREEN:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_GREEN))->setValuesToFloat(TEXMOD_DEF_GREEN,				TEXMOD_MIN_GREEN,		TEXMOD_MAX_GREEN,		0.1f);
				refreshTexPost(hWnd);
				break;
			case IDC_TEX_RESET_BLUE:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_BLUE))->setValuesToFloat(TEXMOD_DEF_BLUE,				TEXMOD_MIN_BLUE,		TEXMOD_MAX_BLUE,		0.1f);
				refreshTexPost(hWnd);
				break;
			case IDC_TEX_RESET_GAMMA:
				CHECK(wmEvent==BN_CLICKED);
				GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX_GAMMA))->setValuesToFloat(TEXMOD_DEF_GAMMA,				TEXMOD_MIN_GAMMA,		TEXMOD_MAX_GAMMA,		0.1f);
				refreshTexPost(hWnd);
				break;

			case IDC_TEX_BRIGHTNESS:
			case IDC_TEX_CONTRAST:
			case IDC_TEX_SATURATION:
			case IDC_TEX_HUE:
			case IDC_TEX_RED:
			case IDC_TEX_GREEN:
			case IDC_TEX_BLUE:
			case IDC_TEX_INVERT:
				{
					CHECK(wmEvent==WM_HSCROLL || (wmEvent==BN_CLICKED&&wmId==IDC_TEX_INVERT));
					updateGUItoTexModifier(hWnd, getTextureFromEMPV(hWnd));
					SetTimer(hWnd, TIMER_REDRAW_TEX_ID, 500, NULL);
				}
				break;
			}
		}	// end WM_COMMAND
		break;
	case WM_TIMER:
		{
			if (wParam == TIMER_REDRAW_TEX_ID)
			{
				KillTimer(hWnd, TIMER_REDRAW_TEX_ID);
				refreshTexPost(hWnd);
			}
		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)bgBrush;
		}
	case WM_CTLCOLORDLG:
		{
			return (INT_PTR)bgBrush;
		}
		break;
	}
	return false;
}

bool makeTexPreview(HWND hwnd)
{
	int i,j,x,y;
	HWND hPrev = GetDlgItem(hwnd, IDC_TEX_PREVIEW);
	EMPView * empv = GetEMPViewInstance(hPrev);
	Texture * tex;
	if (empv->reserved2)
		tex = (Texture *)empv->reserved2;
	else
		tex = (Texture *)empv->reserved1;
	

	ImageByteBuffer * iBuf = NULL;
	if (tex->isValid())
	{
		if (empv->byteBuff  &&  empv->byteBuff->width==tex->bmp.getWidth()  &&  empv->byteBuff->height==tex->bmp.getHeight())
			iBuf = empv->byteBuff;
		else
		{
			iBuf = new ImageByteBuffer();
			iBuf->allocBuffer(tex->bmp.getWidth(), tex->bmp.getHeight());
		}
	}
	else
	{
		RECT crect;
		GetClientRect(hPrev, &crect);
		if (empv->byteBuff  &&  empv->byteBuff->width==crect.right  &&  empv->byteBuff->height==crect.bottom)
			iBuf = empv->byteBuff;
		else
		{
			iBuf = new ImageByteBuffer();
			iBuf->allocBuffer(crect.right, crect.bottom);
		}
		iBuf->clearBuffer();

		if (empv->byteBuff != iBuf)
		{
			ImageByteBuffer * oldBuf = empv->byteBuff;
			empv->byteBuff = iBuf;
			if (oldBuf)
			{
				oldBuf->freeBuffer();
				delete oldBuf;
			}
			return true;
		}
	}

	x = tex->bmp.getWidth();
	y = tex->bmp.getHeight();
	if (x<=0  ||  y<=0)
		return false;

	TexModifer tm = tex->texMod;
	float invert = tm.invert ? 1.0f : 0.0f;

	HWND hProgress = GetDlgItem(hwnd, IDC_TEX_PROGRESSBAR);
	EMProgressBar * empb = GetEMProgressBarInstance(hProgress);


	time_t time_start = clock();

	if (tex->bmp.isItFloat)
	{
		for (j=0; j<y; j++)
		{
			if (j%10==0)
				empb->setPos((j+1)*100.0f/y);

			for (i=0; i<x; i++)
			{
				Color4 c = tex->bmp.fdata[j*x+i];

				c.r = min(1.0f, max(0.0f, tm.brightness+c.r));
				c.g = min(1.0f, max(0.0f, tm.brightness+c.g));
				c.b = min(1.0f, max(0.0f, tm.brightness+c.b));
				
				float h2 = 0.5f;
				c.r = min(1.0f, max(0.0f, (c.r-h2) * tm.contrast + h2 ));
				c.g = min(1.0f, max(0.0f, (c.g-h2) * tm.contrast + h2 ));
				c.b = min(1.0f, max(0.0f, (c.b-h2) * tm.contrast + h2 ));

				ColorHSL hsl = c.toColorHSL();
				hsl.s = min(1, max(0, hsl.s+tm.saturation*sqrt(hsl.s)));
				hsl.h += tm.hue;
				hsl.h = hsl.h - (int)hsl.h;
				c = hsl.toColor4();

				c.r = min(1.0f, max(0.0f, tm.red+c.r));
				c.g = min(1.0f, max(0.0f, tm.green+c.g));
				c.b = min(1.0f, max(0.0f, tm.blue+c.b));

				c.r = invert * (1-c.r) + (1-invert) * c.r;
				c.g = invert * (1-c.g) + (1-invert) * c.g;
				c.b = invert * (1-c.b) + (1-invert) * c.b;
				
				iBuf->imgBuf[j][i] = RGB(min(1, c.r)*255,  min(1, c.g)*255,  min(1, c.b)*255);
			}
		}
	}
	else
	{
		for (j=0; j<y; j++)
		{
			if (j%10==0)
				empb->setPos((j+1)*100.0f/y);

			for (i=0; i<x; i++)
			{
				Color4 c;
				c.r = tex->bmp.idata[(j*x+i)*4+0] / 255.0f;
				c.g = tex->bmp.idata[(j*x+i)*4+1] / 255.0f;
				c.b = tex->bmp.idata[(j*x+i)*4+2] / 255.0f;

				c.r = min(1.0f, max(0.0f, tm.brightness+c.r));
				c.g = min(1.0f, max(0.0f, tm.brightness+c.g));
				c.b = min(1.0f, max(0.0f, tm.brightness+c.b));
				
				float h2 = 0.5f;
				c.r = min(1.0f, max(0.0f, (c.r-h2) * tm.contrast + h2 ));
				c.g = min(1.0f, max(0.0f, (c.g-h2) * tm.contrast + h2 ));
				c.b = min(1.0f, max(0.0f, (c.b-h2) * tm.contrast + h2 ));

				ColorHSL hsl = c.toColorHSL();
				hsl.s = min(1, max(0, hsl.s+tm.saturation*sqrt(hsl.s)));
				hsl.h += tm.hue;
				hsl.h = hsl.h - (int)hsl.h;
				c = hsl.toColor4();

				c.r = min(1.0f, max(0.0f, tm.red+c.r));
				c.g = min(1.0f, max(0.0f, tm.green+c.g));
				c.b = min(1.0f, max(0.0f, tm.blue+c.b));

				c.r = invert * (1-c.r) + (1-invert) * c.r;
				c.g = invert * (1-c.g) + (1-invert) * c.g;
				c.b = invert * (1-c.b) + (1-invert) * c.b;
				
				iBuf->imgBuf[j][i] = RGB(min(1, c.r)*255,  min(1, c.g)*255,  min(1, c.b)*255);
			}
		}
	}

	empb->setPos(0.0f);

	if (empv->byteBuff != iBuf)
	{
		ImageByteBuffer * oldBuf = empv->byteBuff;
		empv->byteBuff = iBuf;
		if (oldBuf)
		{
			oldBuf->freeBuffer();
			delete oldBuf;
		}
	}

	time_t time_stop = clock();
	time_t time_diff = time_stop - time_start;
	char aaa[64];
	sprintf_s(aaa, 64, "tex update %d x %d took time: %lld", x,y, time_diff);
	Logger::add(aaa);

	return true;
}

void updateDialogFromTex(Texture * tex, HWND hwnd)
{
	if (!tex)
		return;

	HWND hInterp = GetDlgItem(hwnd, IDC_TEX_INTERPOLATE);
	EMCheckBox * emcb = GetEMCheckBoxInstance(hInterp);
	emcb->selected = tex->texMod.interpolateProbe;
	InvalidateRect(hInterp, NULL, false);

	HWND hRes = GetDlgItem(hwnd, IDC_TEX_RESOLUTION);
	char buf[64];
	sprintf_s(buf, 64, "%dx%d", tex->bmp.getWidth(), tex->bmp.getHeight());
	EMText * emtres = GetEMTextInstance(hRes);
	emtres->changeCaption(buf);
	
	HWND hType = GetDlgItem(hwnd, IDC_TEX_TYPE);
	EMText * emttype = GetEMTextInstance(hType);
	if (!tex->bmp.isValid())
		SetWindowText(hType, "no texture");
	else
		if (tex->bmp.isItFloat)
			emttype->changeCaption("128bit HDR");
		else
			emttype->changeCaption("32bit bitmap");

}

char * openTexDialog(HWND hwnd)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hDllModule;
	ofn.lpstrFilter = "Image file\0*.bmp;*.jpg;*.jpeg;*.png;*.gif;*.exr;*.hdr\0All files\0*.*\0";
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = (char *)malloc(1024);
	ofn.lpstrFile[0] = 0;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = "";
	ofn.lpstrTitle = "Load texture";
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = NULL;
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST;

	bool res = (GetOpenFileName(&ofn) == TRUE);
	if (res)
	{
		return ofn.lpstrFile;
	}
	else
	{
		free(ofn.lpstrFile);
		return NULL;
	}
	
	return NULL;
}


bool updateGUItoTexModifier(HWND hwnd, Texture * tex)
{
	CHECK(hwnd);
	CHECK(tex);

	EMEditTrackBar * emtb_bri = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_BRIGHTNESS));
	EMEditTrackBar * emtb_con = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_CONTRAST));
	EMEditTrackBar * emtb_sat = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_SATURATION));
	EMEditTrackBar * emtb_hue = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_HUE));
	EMEditTrackBar * emtb_red = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_RED));
	EMEditTrackBar * emtb_gre = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_GREEN));
	EMEditTrackBar * emtb_blu = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_BLUE));
	EMEditTrackBar * emtb_gam = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_GAMMA));
	EMCheckBox * emc_inv = GetEMCheckBoxInstance(GetDlgItem(hwnd, IDC_TEX_INVERT));

	tex->texMod.brightness = emtb_bri->floatValue;
	tex->texMod.contrast   = emtb_con->floatValue;
	tex->texMod.saturation = emtb_sat->floatValue;
	tex->texMod.hue        = emtb_hue->floatValue;
	tex->texMod.red        = emtb_red->floatValue;
	tex->texMod.green      = emtb_gre->floatValue;
	tex->texMod.blue       = emtb_blu->floatValue;
	tex->texMod.gamma      = emtb_gam->floatValue;
	tex->texMod.updateGamma(tex->texMod.gamma);
	tex->texMod.invert     = emc_inv->selected;

	return true;
}

bool updateTexModifierToGui(HWND hwnd, Texture * tex)
{
	CHECK(hwnd);
	CHECK(tex);

	EMEditTrackBar * emtb_bri = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_BRIGHTNESS));
	EMEditTrackBar * emtb_con = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_CONTRAST));
	EMEditTrackBar * emtb_sat = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_SATURATION));
	EMEditTrackBar * emtb_hue = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_HUE));
	EMEditTrackBar * emtb_red = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_RED));
	EMEditTrackBar * emtb_gre = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_GREEN));
	EMEditTrackBar * emtb_blu = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_BLUE));
	EMEditTrackBar * emtb_gam = GetEMEditTrackBarInstance(GetDlgItem(hwnd, IDC_TEX_GAMMA));
	EMCheckBox * emc_inv = GetEMCheckBoxInstance(GetDlgItem(hwnd, IDC_TEX_INVERT));

	emtb_bri->setValuesToFloat(tex->texMod.brightness,	TEXMOD_MIN_BRIGHTNESS,	TEXMOD_MAX_BRIGHTNESS,	0.1f);
	emtb_con->setValuesToFloat(tex->texMod.contrast,	TEXMOD_MIN_CONTRAST,	TEXMOD_MAX_CONTRAST,	0.1f);
	emtb_sat->setValuesToFloat(tex->texMod.saturation,	TEXMOD_MIN_SATURATION,	TEXMOD_MAX_SATURATION,	0.1f);
	emtb_hue->setValuesToFloat(tex->texMod.hue,			TEXMOD_MIN_HUE,			TEXMOD_MAX_HUE,			0.1f);
	emtb_red->setValuesToFloat(tex->texMod.red,			TEXMOD_MIN_RED,			TEXMOD_MAX_RED,			0.1f);
	emtb_gre->setValuesToFloat(tex->texMod.green,		TEXMOD_MIN_GREEN,		TEXMOD_MAX_GREEN,		0.1f);
	emtb_blu->setValuesToFloat(tex->texMod.blue,		TEXMOD_MIN_BLUE,		TEXMOD_MAX_BLUE,		0.1f);
	emtb_gam->setValuesToFloat(tex->texMod.gamma,		TEXMOD_MIN_GAMMA,		TEXMOD_MAX_GAMMA,		0.1f);
	emc_inv->selected = tex->texMod.invert;
	InvalidateRect(emc_inv->hwnd, NULL, false);

	return true;
}

Texture * getTextureFromEMPV(HWND hwnd)
{
	HWND hPrev = GetDlgItem(hwnd, IDC_TEX_PREVIEW);
	EMPView * empv = GetEMPViewInstance(hPrev);
	if (empv->reserved2)
		return (Texture *)empv->reserved2;
	else
		return (Texture *)empv->reserved1;
}

void refreshTexPost(HWND hwnd)
{
	#ifdef RECALC_PREV_AS_THREAD
		lockOnRefresh(hwnd);
		DWORD threadId = 0;
		HANDLE hThread = CreateThread( NULL, 0, 
			(LPTHREAD_START_ROUTINE)(refreshPostThreadProc), (LPVOID)hwnd, 0, &threadId);
		if (!hThread)
		{
			unlockAfterRefresh(hwnd);
		}
	#else
		updateGUItoTexModifier(hwnd, getTextureFromEMPV(hwnd));
		makeTexPreview(hwnd);
		InvalidateRect(GetDlgItem(hwnd, IDC_TEX_PREVIEW), NULL, false);
	#endif
}

DWORD WINAPI refreshPostThreadProc(LPVOID lpParameter)
{
	HWND hwnd = (HWND)lpParameter;
	DWORD curThreadID = GetCurrentThreadId();
	static bool thMutex = false;
	static DWORD lastThreadID = 0;
	lastThreadID = curThreadID;

	char buf[64];
	sprintf_s(buf, 64, "refresh thread started %d", curThreadID); 
	Logger::add(buf);

	while (thMutex)
	{
		Sleep(10);
	}

	sprintf_s(buf, 64, "refresh thread continue %d", curThreadID); 
	Logger::add(buf);

	if (lastThreadID != curThreadID)
		return 0;

	thMutex = true;

	updateGUItoTexModifier(hwnd, getTextureFromEMPV(hwnd));

	makeTexPreview(hwnd);

	unlockAfterRefresh(hwnd);

	thMutex = false;

	sprintf_s(buf, 64, "refresh thread done %d", curThreadID); 
	Logger::add(buf);

	return 0;
}

void lockOnRefresh(HWND hwnd)
{
	disableControl(hwnd, IDC_TEX_LOAD);
	disableControl(hwnd, IDC_TEX_RELEASE);
	disableControl(hwnd, IDC_TEX_INTERPOLATE);
	disableControl(hwnd, IDC_TEX_BRIGHTNESS);
	disableControl(hwnd, IDC_TEX_CONTRAST);
	disableControl(hwnd, IDC_TEX_SATURATION);
	disableControl(hwnd, IDC_TEX_HUE);
	disableControl(hwnd, IDC_TEX_RED);
	disableControl(hwnd, IDC_TEX_GREEN);
	disableControl(hwnd, IDC_TEX_BLUE);
	disableControl(hwnd, IDC_TEX_GAMMA);
	disableControl(hwnd, IDC_TEX_INVERT);
	disableControl(hwnd, IDC_TEX_RESET_BRIGHTNESS);
	disableControl(hwnd, IDC_TEX_RESET_CONTRAST);
	disableControl(hwnd, IDC_TEX_RESET_SATURATION);
	disableControl(hwnd, IDC_TEX_RESET_HUE);
	disableControl(hwnd, IDC_TEX_RESET_RED);
	disableControl(hwnd, IDC_TEX_RESET_GREEN);
	disableControl(hwnd, IDC_TEX_RESET_BLUE);
	disableControl(hwnd, IDC_TEX_RESET_GAMMA);
	disableControl(hwnd, IDC_TEX_OK);
	disableControl(hwnd, IDC_TEX_CANCEL);
	disableControl(hwnd, IDC_TEX_PREVIEW);
}

void unlockAfterRefresh(HWND hwnd)
{
	enableControl(hwnd, IDC_TEX_LOAD);
	enableControl(hwnd, IDC_TEX_RELEASE);
	enableControl(hwnd, IDC_TEX_INTERPOLATE);
	enableControl(hwnd, IDC_TEX_BRIGHTNESS);
	enableControl(hwnd, IDC_TEX_CONTRAST);
	enableControl(hwnd, IDC_TEX_SATURATION);
	enableControl(hwnd, IDC_TEX_HUE);
	enableControl(hwnd, IDC_TEX_RED);
	enableControl(hwnd, IDC_TEX_GREEN);
	enableControl(hwnd, IDC_TEX_BLUE);
	enableControl(hwnd, IDC_TEX_GAMMA);
	enableControl(hwnd, IDC_TEX_INVERT);
	enableControl(hwnd, IDC_TEX_RESET_BRIGHTNESS);
	enableControl(hwnd, IDC_TEX_RESET_CONTRAST);
	enableControl(hwnd, IDC_TEX_RESET_SATURATION);
	enableControl(hwnd, IDC_TEX_RESET_HUE);
	enableControl(hwnd, IDC_TEX_RESET_RED);
	enableControl(hwnd, IDC_TEX_RESET_GREEN);
	enableControl(hwnd, IDC_TEX_RESET_BLUE);
	enableControl(hwnd, IDC_TEX_RESET_GAMMA);
	enableControl(hwnd, IDC_TEX_OK);
	enableControl(hwnd, IDC_TEX_CANCEL);
	enableControl(hwnd, IDC_TEX_PREVIEW);
}

void lockBeforeLoading(HWND hwnd)
{
	disableControl(hwnd, IDC_TEX_LOAD);
	disableControl(hwnd, IDC_TEX_RELEASE);
	disableControl(hwnd, IDC_TEX_INTERPOLATE);
	disableControl(hwnd, IDC_TEX_BRIGHTNESS);
	disableControl(hwnd, IDC_TEX_CONTRAST);
	disableControl(hwnd, IDC_TEX_SATURATION);
	disableControl(hwnd, IDC_TEX_HUE);
	disableControl(hwnd, IDC_TEX_RED);
	disableControl(hwnd, IDC_TEX_GREEN);
	disableControl(hwnd, IDC_TEX_BLUE);
	disableControl(hwnd, IDC_TEX_GAMMA);
	disableControl(hwnd, IDC_TEX_INVERT);
	disableControl(hwnd, IDC_TEX_RESET_BRIGHTNESS);
	disableControl(hwnd, IDC_TEX_RESET_CONTRAST);
	disableControl(hwnd, IDC_TEX_RESET_SATURATION);
	disableControl(hwnd, IDC_TEX_RESET_HUE);
	disableControl(hwnd, IDC_TEX_RESET_RED);
	disableControl(hwnd, IDC_TEX_RESET_GREEN);
	disableControl(hwnd, IDC_TEX_RESET_BLUE);
	disableControl(hwnd, IDC_TEX_RESET_GAMMA);
	disableControl(hwnd, IDC_TEX_OK);
	disableControl(hwnd, IDC_TEX_CANCEL);
	disableControl(hwnd, IDC_TEX_PREVIEW);
}

void unlockAfterLoading(HWND hwnd)
{
	enableControl(hwnd, IDC_TEX_LOAD);
	enableControl(hwnd, IDC_TEX_RELEASE);
	enableControl(hwnd, IDC_TEX_INTERPOLATE);
	enableControl(hwnd, IDC_TEX_BRIGHTNESS);
	enableControl(hwnd, IDC_TEX_CONTRAST);
	enableControl(hwnd, IDC_TEX_SATURATION);
	enableControl(hwnd, IDC_TEX_HUE);
	enableControl(hwnd, IDC_TEX_RED);
	enableControl(hwnd, IDC_TEX_GREEN);
	enableControl(hwnd, IDC_TEX_BLUE);
	enableControl(hwnd, IDC_TEX_GAMMA);
	enableControl(hwnd, IDC_TEX_INVERT);
	enableControl(hwnd, IDC_TEX_RESET_BRIGHTNESS);
	enableControl(hwnd, IDC_TEX_RESET_CONTRAST);
	enableControl(hwnd, IDC_TEX_RESET_SATURATION);
	enableControl(hwnd, IDC_TEX_RESET_HUE);
	enableControl(hwnd, IDC_TEX_RESET_RED);
	enableControl(hwnd, IDC_TEX_RESET_GREEN);
	enableControl(hwnd, IDC_TEX_RESET_BLUE);
	enableControl(hwnd, IDC_TEX_RESET_GAMMA);
	enableControl(hwnd, IDC_TEX_OK);
	enableControl(hwnd, IDC_TEX_CANCEL);
	enableControl(hwnd, IDC_TEX_PREVIEW);
}
