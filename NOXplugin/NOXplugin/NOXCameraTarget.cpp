#include "max2013predef.h"
#include "NOXCamera.h"
#include <macrorec.h>
#include <decomp.h>

NOXCamCreateMode noxCamCrMode;

int NOXCameraTargetClassDesc::BeginCreate(Interface *i)
{
	SuspendSetKeyMode();
	IObjCreate *iob = i->GetIObjCreate();
	noxCamCrMode.Begin( iob, this );
	iob->PushCommandMode( &noxCamCrMode );
	return TRUE;
}

int NOXCameraTargetClassDesc::EndCreate(Interface *i)
{
	ResumeSetKeyMode();
	noxCamCrMode.End();
	i->RemoveMode(&noxCamCrMode);
	GetCOREInterface()->GetMacroRecorder()->EmitScript();
	return TRUE;
}


void NOXCamCreationManager::Begin( IObjCreate *ioc, ClassDesc *desc )
{
	createInterface = ioc;
	cDesc           = desc;
	attachedToNode  = FALSE;
	createCB        = NULL;
	camNode         = NULL;
	targNode        = NULL;
	camObject       = NULL;
	targObject      = NULL;
	CreateNewObject();
}

void NOXCamCreationManager::End()
{
	if ( camObject ) {
		if ( !attachedToNode ) 
		{
			camObject->EndEditParams( (IObjParam*)createInterface, END_EDIT_REMOVEUI, NULL);
			
			theHold.Suspend(); 
			camObject->DeleteAllRefsFromMe();
			camObject->DeleteAllRefsToMe();
			camObject->DeleteThis();
			camObject = NULL;
			theHold.Resume();
			if (theHold.GetGlobalPutCount()!=lastPutCount) {
				GetSystemSetting(SYSSET_CLEAR_UNDO);
				}
			macroRec->Cancel();
		} 
		else if ( camNode ) 
		{
			theHold.Suspend();
			DeleteReference(0);
			theHold.Resume();
			camObject->EndEditParams( (IObjParam*)createInterface, END_EDIT_REMOVEUI, NULL);
		}
	}	
}

void NOXCamCreationManager::CreateNewObject()
{
	camObject = (NOXCamera*)cDesc->Create();
	camObject->SetType(1);
	lastPutCount = theHold.GetGlobalPutCount();
    macroRec->BeginCreate(cDesc);
	camObject->BeginEditParams( (IObjParam*)createInterface, BEGIN_EDIT_CREATE, NULL );
}

static BOOL needToss;
int NOXCamCreationManager::proc( HWND hwnd, int msg, int point, int flag, IPoint2 m )
{
	int res = TRUE;	
	TSTR targName;
	#ifdef ISMAX2013
		ViewExp * vpx = &(createInterface->GetActiveViewExp());
	#else
		ViewExp * vpx = createInterface->GetActiveViewport();
	#endif

	switch ( msg ) 
	{
		case MOUSE_POINT:
			switch ( point ) 
			{
				case 0: 
				{
					pt0 = m;
					if ( createInterface->SetActiveViewport(hwnd) )
						return FALSE;

					if (createInterface->IsCPEdgeOnInView()) 
					{
						#ifndef ISMAX2013
							createInterface->ReleaseViewport(vpx); 
						#endif
						return FALSE;
					}

					GetCOREInterface()->SetHideByCategoryFlags(GetCOREInterface()->GetHideByCategoryFlags() & ~HIDE_CAMERAS);

					if ( attachedToNode ) 
					{
						#ifndef NO_CREATE_TASK
				   			camObject->EndEditParams( (IObjParam*)createInterface, END_EDIT_REMOVEUI, NULL);							
						#endif
						macroRec->EmitScript();
						if (camNode) 
						{
							theHold.Suspend();
							DeleteReference(0);
							theHold.Resume();
						}
						CreateNewObject();
					}

					needToss = (theHold.GetGlobalPutCount() != lastPutCount);

				   	theHold.Begin();
					mat.IdentityMatrix();

					// link it up
					INode *l_camNode = createInterface->CreateObjectNode( camObject);
					attachedToNode = TRUE;
					createCB = camObject->GetCreateMouseCallBack();
					createInterface->SelectNode( l_camNode );
					
						// Create target object and node
						targObject = new NOXTargetObject;
						targNode = createInterface->CreateObjectNode( targObject);
						targName = l_camNode->GetName();
						targName += GetString(IDS_NOX_CAM_DOT_TARGET);
						macroRec->Disable();
						targNode->SetName(targName);
						macroRec->Enable();

						// hook up camera to target using lookat controller.
						createInterface->BindToTarget(l_camNode,targNode);			
						camObject->etarg = targObject;

					// Reference the new node so we'll get notifications.
					theHold.Suspend();
					ReplaceReference( 0, l_camNode);
					theHold.Resume();

					// Position camera and target at first point then drag.
					mat.IdentityMatrix();
					//mat[3] = vpx->GetPointOnCP(m);
					mat.SetTrans( vpx->SnapPoint(m,m,NULL,SNAP_IN_3D) );
					createInterface->SetNodeTMRelConstPlane(camNode, mat);
						createInterface->SetNodeTMRelConstPlane(targNode, mat);
						camObject->Enable(1);

				   		ignoreSelectionChange = TRUE;
				   		createInterface->SelectNode( targNode,0);
				   		ignoreSelectionChange = FALSE;
						res = TRUE;

					if ( camNode ) 
					{
						Point3 color = GetUIColor( COLOR_CAMERA_OBJ );
						camNode->SetWireColor( RGB( color.x*255.0f, color.y*255.0f, color.z*255.0f ) );
					}
				}
				break;
				case 1:
				{
					if (Length(m-pt0)<2)
					{
						camObject->EndEditParams( (IObjParam*)createInterface,0,NULL);
						macroRec->Cancel();
						theHold.Cancel();
						if (needToss) 
							GetSystemSetting(SYSSET_CLEAR_UNDO);
						camNode = NULL;			
						targNode = NULL;	 	
						createInterface->RedrawViews(createInterface->GetTime()); 
						CreateNewObject();	
						attachedToNode = FALSE;
						#ifndef ISMAX2013
							createInterface->ReleaseViewport(vpx); 
						#endif
						return FALSE;						
					}

					mat.SetTrans( vpx->SnapPoint(m,m,NULL,SNAP_IN_3D) );
					macroRec->Disable();
					createInterface->SetNodeTMRelConstPlane(targNode, mat);
					macroRec->Enable();
				   	ignoreSelectionChange = TRUE;
				   	createInterface->SelectNode( camNode);
				   	ignoreSelectionChange = FALSE;
					createInterface->RedrawViews(createInterface->GetTime());  
				    theHold.Accept(GetString(IDS_NOX_CAM_DS_CREATE));
					res = FALSE;
				}
				break;
		}
		break;
		case MOUSE_MOVE:
		{
			bool temp = camObject->focusConnectedToTarget;
			camObject->focusConnectedToTarget = true;
			camObject->UpdateTargDistance(createInterface->GetTime(), camNode);
			camObject->focusConnectedToTarget = temp;
			mat.SetTrans( vpx->SnapPoint(m,m,NULL,SNAP_IN_3D) );
			macroRec->Disable();
			createInterface->SetNodeTMRelConstPlane(targNode, mat);
			macroRec->Enable();
			createInterface->RedrawViews(createInterface->GetTime());	   
			macroRec->SetProperty(camObject, _T("target"), mr_create, Class_ID(TARGET_CLASS_ID, 0), GEOMOBJECT_CLASS_ID, 1, _T("transform"), mr_matrix3, &mat);
			res = TRUE;
		}
		break;
		case MOUSE_FREEMOVE:
		{
			vpx->SnapPreview(m,m,NULL, SNAP_IN_3D);
		}
		break;
		case MOUSE_PROPCLICK:
		{
			createInterface->RemoveMode(NULL);
		}
		break;
		case MOUSE_ABORT:
		{
			camObject->EndEditParams( (IObjParam*)createInterface,0,NULL);
			macroRec->Cancel();
			theHold.Cancel();
			if (needToss) 
				GetSystemSetting(SYSSET_CLEAR_UNDO);
			camNode = NULL;			
			targNode = NULL;	 	
			createInterface->RedrawViews(createInterface->GetTime()); 
			CreateNewObject();	
			attachedToNode = FALSE;
			res = FALSE;						
		}
		break;
	}

	#ifndef ISMAX2013
		createInterface->ReleaseViewport(vpx); 
	#endif
	return res;
}

RefResult NOXCamCreationManager::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	return REF_SUCCEED;
}
