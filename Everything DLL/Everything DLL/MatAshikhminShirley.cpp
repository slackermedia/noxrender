#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>

#define clamp(x,a,b)   max((a),min((x),(b)))


//----------		ASHIKHMIN-SHIRLEY RANDOM

Vector3d MatLayer::randomNewDirectionAshikhminShirley(HitData & hd, float &probability)
{
	float a,b;
	char l;
	float cosPhi, cosTheta, sinPhi, sinTheta;
	float p_h,pr;
	RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();

	#ifdef RAND_PER_THREAD
		a = rg->getRandomFloat();
		b = rg->getRandomFloat();
		l = rg->getRandomInt(10000)%4;
	#else
		unsigned int r1;
		while(rand_s(&r1));
		a = (float)(r1/(double)(UINT_MAX));
		while(rand_s(&r1));
		b = (float)(r1/(double)(UINT_MAX));
		l = rand()%4;
	#endif

	cosPhi = (float)FastMath::cos(PI*a/2.0f);
	sinPhi = (float)FastMath::sin(PI*a/2.0f);
	if (l>1)
		sinPhi = -sinPhi;
	if (l==1 || l==2)
		cosPhi = -cosPhi;

	//nx = 15;
	float nx = pow(10000.0f, (1.0f-roughness)*(1.0f-roughness))-1.0f;

	cosTheta = pow((1-b), 1.0f/(nx+1));
	sinTheta = sqrt(1-cosTheta*cosTheta);
	
	Vector3d H,H_, u,v;
	u = Vector3d::random();
	while (fabs(u*hd.normal_shade) > 0.95f)
		u = Vector3d::random();
	v = hd.normal_shade^u;
	u = v^hd.normal_shade;
	u.normalize();

	H_ = u*cosPhi + v*sinPhi;
	if (hd.normal_shade*hd.in> 0)
	{
		H = hd.normal_shade*cosTheta + H_*sinTheta;
	}
	else
	{
		H = hd.normal_shade*(-cosTheta) + H_*sinTheta;
	}

	Vector3d res;
	res = hd.in.reflect(H);

	if ((res*hd.normal_shade)*(hd.in*hd.normal_shade) < 0)
		res.invert();

	p_h = (float)fabs( (nx+1)/2.5f * pow(fabs(hd.normal_shade*H), nx) );		///// negative? how?
	if (p_h < 0.09f)
		p_h = 0.09f;

	pr = fabs(4.0f * (hd.in*H) / p_h);
	if (pr > 5)
		pr = 5;

	probability = pr;

	hd.out = res;
	return res;
}

//----------		ASHIKHMIN-SHIRLEY BRDF

Color4 MatLayer::getBRDFAshikhminShirley(HitData & hd)
{
	float rs, rd;
	Vector3d H,k;
	float cosBeta, cosDelta;
	float cosTheta_i, cosTheta_o, maxCosTheta;

	H = hd.in+hd.out;
	H.normalize();
	cosDelta = hd.normal_shade*H;
	cosBeta = hd.in*H;
	cosTheta_i = hd.in *hd.normal_shade;
	cosTheta_o = hd.out*hd.normal_shade;
	maxCosTheta = max(cosTheta_i, cosTheta_o);
	if (maxCosTheta < 0.001f)
		maxCosTheta = 0.001f;
	if (cosBeta < 0.001f)
		cosBeta = 0.001f;
	if (cosDelta < 0.001f)
		cosDelta = 0.001f;

	rd = getRough(hd.tU, hd.tV);

	float nx = pow(1000.0f, (1.0f-roughness)*(1.0f-roughness))-1.0f;
	float rs_ = (float)((nx+1.0f)/8.0f/PI);
	rs = rs_ * pow(cosDelta, nx)/maxCosTheta * PI;

	if (rs > 15)
		rs = 15;

	return (getColor0(hd.tU, hd.tV)*(rs+rd));
}

//----------		ASHIKHMIN-SHIRLEY PDF

float MatLayer::getProbabilityAshikhminShirley(HitData & hd)
{
	// of course it is wrong
	return 1;
}

