#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include "nox_update.h"

extern HWND hMainUpdate;
bool wereIgnore = false;
bool stopInstalling = false; 
extern HWND hTextInfo2;
extern HWND hProgress;
extern bool diffrDirs;
extern int programStatus;

DWORD WINAPI installUpdateThreadProc(LPVOID lpParameter)
{
	char * filename = (char *)lpParameter;
	if (!filename)
	{
		SetWindowText(hTextInfo2, "Internal error. Sorry");
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		unlockControlsAfterInstallUpdate(0);
		return false;
	}

	char * tempdir = NULL;
	unsigned int numFiles = 0;
	bool iOK = installUpdate(filename, &tempdir, numFiles);

	SetWindowText(hTextInfo2, "Cleaning up...");
	if (filename && doesFileExist(filename))
	{
		DeleteFile(filename);
	}

	if (tempdir)
	{
		char temp[2048];
		sprintf_s(temp, 2048, "%s\\Renderer", tempdir);
		if (doesDirExist(temp))
		{
			unsigned int curFiles = 0;
			deleteDirRecursively(tempdir, curFiles, numFiles);
		}

		if (doesDirExist(tempdir))
		{
			RemoveDirectory(tempdir);
		}

		SetWindowText(hTextInfo2, "OK");
	}

	if (!iOK)
	{
		if (stopInstalling)
			SetWindowText(hTextInfo2, "Installation aborted.");
		else
			SetWindowText(hTextInfo2, "Installation failed.");
		unlockControlsAfterInstallUpdate(0);
		return false;
	}
	else
	{
		if (wereIgnore)
			SetWindowText(hTextInfo2, "Installation complete, however some files were ignored.");
		else
			SetWindowText(hTextInfo2, "Installation complete.");
		unlockControlsAfterInstallUpdate(1);
	}
	
	SetWindowText(hTextInfo2, "Done. Running plugins installer...");
	programStatus = PROGRAM_STATUS_RUN_PLUGINS;
	runPluginInstaller();
	unlockControlsAfterRunPluginsInstaller(1);

	return 1;
}

bool runInstallThread(char * filename7z)
{
	stopInstalling = false;
	lockControlsBeforeInstallUpdate();
	DWORD threadId;
	HANDLE threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(installUpdateThreadProc), (LPVOID)filename7z, 0, &threadId);
	if (!threadHandle)
		return false;
	return true;
}

bool installUpdate(char * filename7z, char ** tempDir, unsigned int &numberFiles)
{
	programStatus = PROGRAM_STATUS_INSTALLING;
	numberFiles = 0;

	if (!filename7z)
	{
		SetWindowText(hTextInfo2, "Internal error. Sorry");
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		return false;
	}
	if (!doesFileExist(filename7z))
	{
		SetWindowText(hTextInfo2, "Internal error. Sorry");
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		return false;
	}

	char * noxDir = getRendererDirectory();
	if (!noxDir)
	{
		SetWindowText(hTextInfo2, "Internal error. Sorry");
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		return false;
	}
	if (!doesDirExist(noxDir))
	{
		SetWindowText(hTextInfo2, "Internal error. Sorry");
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		return false;
	}

	char * installDir = NULL;
	if (diffrDirs)
	{
		programStatus = PROGRAM_STATUS_WAIT_FOR_DIR;
		char * aaa = openDirectoryDialog(hMainUpdate, "Choose directory for new version:", noxDir);
		if (aaa)
		{
			installDir = aaa;
		}
		else
		{
			int mRes = MessageBox(hMainUpdate, "You have not chosen new directory.\nDo you want to update to old directory?", "What now?", MB_YESNO);
			if (mRes == IDNO)
			{
				SetWindowText(hTextInfo2, "Update aborted by user.");
				programStatus = PROGRAM_STATUS_INSTALL_ABORTED;
				return false;
			}
			installDir = noxDir;
		}
	}
	else
	{
		installDir = noxDir;
	}

	if (!installDir)
	{	
		SetWindowText(hTextInfo2, "Internal error. Sorry.");
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		return false;
	}

	char * tempDirName;
	programStatus = PROGRAM_STATUS_UNPACKING;
	bool uOK = unpackUpdate(filename7z, &tempDirName);
	if (!uOK)
	{
		SetWindowText(hTextInfo2, "Something wrong with unpacking update file.");
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		return false;
	}

	if (stopInstalling)
	{
		programStatus = PROGRAM_STATUS_INSTALL_ABORTED;
		SetWindowText(hTextInfo2, "Installation aborted.");
		return false;
	}

	*tempDir = tempDirName;

	char srcdir[2048];
	sprintf_s(srcdir, 2048, "%s\\Renderer", tempDirName);
	if (!doesDirExist(srcdir))
	{
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		SetWindowText(hTextInfo2, "Error. Can't find unpacked folder.");
		return false;
	}

	programStatus = PROGRAM_STATUS_COPYING;
	unsigned int numFiles=0, sumSize=0;
	bool ok = evalFilesRecursively(tempDirName, numFiles, sumSize);
	if (!ok)
	{
		programStatus = PROGRAM_STATUS_INSTALL_ERROR;
		SetWindowText(hTextInfo2, "Error checking unpacked files.");
		return false;
	}
	else
	{
		numberFiles = numFiles;
		unsigned int curbytes=0, curfiles=0;
		wereIgnore = false;
		ok = copyFilesRecursively(installDir, srcdir, curbytes, sumSize, curfiles, numFiles);
		if (ok)
			if (wereIgnore)
			{
				SetWindowText(hTextInfo2, "Files copied, with some exceptions.");
			}
			else
			{
				SetWindowText(hTextInfo2, "File successfully copied.");
				programStatus = PROGRAM_STATUS_INSTALL_ERROR;
			}
		else
		{
			SetWindowText(hTextInfo2, "File copy failed.");
			programStatus = PROGRAM_STATUS_INSTALL_ERROR;
			if (stopInstalling)
				programStatus = PROGRAM_STATUS_INSTALL_ABORTED;
			return false;
		}
	}

	setNOXdirInRegistry(installDir);

	programStatus = PROGRAM_STATUS_INSTALL_COMPLETE;
	return true;
}

bool unpackUpdate(char * filename, char ** dirname)
{
	if (!filename  ||  !dirname)
	{
		return false;
	}

	if (!doesFileExist(filename))
	{
		return false;
	}

	char * noxDir = getRendererDirectory();
	if (!noxDir)
	{
		return false;
	}

	if (!doesDirExist(noxDir))
	{
		return false;
	}

	char sevenzipfilename[1024];
	sprintf_s(sevenzipfilename, 1024, "%s\\bin\\32\\7zG.exe", noxDir);
	if (!doesFileExist(sevenzipfilename))
	{
		return false;
	}

	char * tempDirName = randomTempFilename("nox");
	if (!tempDirName)
	{
		return false;
	}

	if (!CreateDirectory(tempDirName, NULL))
	{
		return false;
	}

	*dirname = tempDirName;

	SetWindowText(hTextInfo2, "Unpacking file... please wait");

	char args[2048];
	sprintf_s(args, 2048, "%s\\bin\\32\\7zG.exe x \"%s\" -o\"%s\" -y", noxDir, filename, tempDirName);

	PROCESS_INFORMATION pi;
	STARTUPINFO si = {	sizeof(STARTUPINFO),	NULL,	"",	NULL, 0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0 };

	bool ok1 = (CreateProcess(NULL, args, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS , NULL, NULL, &si, &pi) == TRUE);
	if (ok1)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		DWORD exitCode;
		GetExitCodeProcess(pi.hProcess, &exitCode);
		SwitchToThisWindow(hMainUpdate, true);
		if (exitCode)	// some error while decompressing
		{	
			ok1 = false;
			SetWindowText(hTextInfo2, "Something wrong with unpacking update file.");
		}

		CloseHandle(pi.hThread );
		CloseHandle(pi.hProcess );
	}
	if (!ok1)
		return false;

	return true;
}



bool evalFilesRecursively(char * dirName, unsigned int &numFiles, unsigned int &sumSize)	// 2,3 arg must be 0
{
	if (!dirName)
		return false;
	if (!doesDirExist(dirName))
		return false;

	char wcDirName[2048];
	sprintf_s(wcDirName, 2048, "%s\\*.*", dirName);

	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	HANDLE hFind = FindFirstFile(wcDirName, &fd);

	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return false;
	} 
	else 
	{
		do 
		{
			if (strcmp(fd.cFileName, ".")==0)
				continue;
			if (strcmp(fd.cFileName, "..")==0)
				continue;

			char fullfilename[2048];
			sprintf_s(fullfilename, 2048, "%s\\%s", dirName, fd.cFileName);

			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)	// dir
			{
				unsigned int nFiles=0, sSize=0;
				if (!evalFilesRecursively(fullfilename, nFiles, sSize))
					return false;
				numFiles += nFiles;
				sumSize += sSize;
			}
			else	// file
			{
				unsigned int fs;
				if (getFileSize(fullfilename, fs))
				{
					numFiles++;
					sumSize += fs;
				}
				else
					return false;
			}
		}
		while (FindNextFile(hFind, &fd) != 0) ;

		DWORD dwError = GetLastError();
		FindClose(hFind);
		if (dwError != ERROR_NO_MORE_FILES) 
		{
			return false;
		}
	}

	return true;
}

bool copyFilesRecursively(char * dstDir, char * srcDir, unsigned int &curBytes, unsigned int &sumBytes, unsigned int &curFiles, unsigned int &sumFiles)
{
	if (!dstDir   ||   !srcDir)
		return false;
	if (!doesDirExist(dstDir)   ||   !doesDirExist(srcDir))
		return false;

	if (stopInstalling)
	{
		programStatus = PROGRAM_STATUS_INSTALL_ABORTED;
		SetWindowText(hTextInfo2, "Installation aborted.");
		return false;
	}

	char wcDirName[2048];
	sprintf_s(wcDirName, 2048, "%s\\*.*", srcDir);

	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	HANDLE hFind = FindFirstFile(wcDirName, &fd);

	char msgBuf[512];

	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return false;
	} 
	else 
	{
		do 
		{
			if (strcmp(fd.cFileName, ".")==0)
				continue;
			if (strcmp(fd.cFileName, "..")==0)
				continue;

			char srcfullfilename[2048];
			char dstfullfilename[2048];
			sprintf_s(srcfullfilename, 2048, "%s\\%s", srcDir, fd.cFileName);
			sprintf_s(dstfullfilename, 2048, "%s\\%s", dstDir, fd.cFileName);

			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)	// dir
			{
				bool dstDirExist = doesDirExist(dstfullfilename);
				while (!dstDirExist)
				{
					bool ignoreCur = false;
					BOOL dOK = CreateDirectory(dstfullfilename, NULL);
					if (!dOK)
					{
						char errbuf[2048];
						sprintf_s(errbuf, 2048, "Can't create directory:\n%s\nProbably you don't have write permissions there.", dstfullfilename);
						int mbRes = MessageBox(hMainUpdate, errbuf, "Error", MB_ABORTRETRYIGNORE);
						switch (mbRes)
						{
							case IDRETRY:
								break;
							case IDIGNORE:
								ignoreCur = true;
								wereIgnore = true;
								break;
							case IDABORT:
								return false;
								break;
						}
					}
					if (ignoreCur)
						break;
					dstDirExist = doesDirExist(dstfullfilename);
				}

				if (dstDirExist)
				{
					bool rOK = copyFilesRecursively(dstfullfilename, srcfullfilename, curBytes, sumBytes, curFiles, sumFiles);
					if (!rOK)
					{
						return false;
					}
				}
			}
			else	// file
			{
				bool copiedOK = false;
				while (!copiedOK)
				{
					bool ignoreCur = false;
					BOOL cOK = CopyFile(srcfullfilename, dstfullfilename, FALSE);
					if (!cOK)
					{
						char errbuf[2048];
						sprintf_s(errbuf, 2048, "Can't copy file\n%s\nto\n%s\nProbably old one is being used (NOX/Max) or you don't have write permissions there.",
									srcfullfilename, dstfullfilename);
						int mbRes = MessageBox(hMainUpdate, errbuf, "Error", MB_ABORTRETRYIGNORE);
						switch (mbRes)
						{
							case IDRETRY:
								break;
							case IDIGNORE:
								ignoreCur = true;
								wereIgnore = true;
								break;
							case IDABORT:
								return false;
								break;
						}
					}
					else
					{
						copiedOK = true;

						unsigned int fs;
						if (getFileSize(srcfullfilename, fs))
							curBytes += fs;
						curFiles++;
						sprintf_s(msgBuf, 512, "Copied %d of %d files.", curFiles, sumFiles);
						unsigned int ppos = 100*curBytes/sumBytes;
						SetWindowText(hTextInfo2, msgBuf);
						SendMessage(hProgress, PBM_SETPOS, ppos, 0); 
					}
					
					if (ignoreCur)
						break;
				}
			}
		}
		while (FindNextFile(hFind, &fd) != 0) ;

		DWORD dwError = GetLastError();
		FindClose(hFind);
		if (dwError != ERROR_NO_MORE_FILES) 
		{
			return false;
		}
	}

	return true;
}

bool deleteDirRecursively(char * dirName, unsigned int &curFiles, unsigned int &sumFiles)
{
	if (!dirName)
		return false;
	if (!doesDirExist(dirName))
		return false;

	char wcDirName[2048];
	sprintf_s(wcDirName, 2048, "%s\\*.*", dirName);

	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	HANDLE hFind = FindFirstFile(wcDirName, &fd);

	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return false;
	} 
	else 
	{
		do 
		{
			if (strcmp(fd.cFileName, ".")==0)
				continue;
			if (strcmp(fd.cFileName, "..")==0)
				continue;

			char fullfilename[2048];
			sprintf_s(fullfilename, 2048, "%s\\%s", dirName, fd.cFileName);

			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)	// dir
			{
				if (!deleteDirRecursively(fullfilename, curFiles, sumFiles))
					return false;
			}
			else	// file
			{
				DeleteFile(fullfilename);
				curFiles++;
				unsigned int ppos = 100*curFiles/sumFiles;
				SendMessage(hProgress, PBM_SETPOS, ppos, 0); 
			}
		}
		while (FindNextFile(hFind, &fd) != 0) ;

		DWORD dwError = GetLastError();
		FindClose(hFind);
		if (dwError != ERROR_NO_MORE_FILES) 
		{
			return false;
		}
		RemoveDirectory(dirName);

	}

	return true;
}

bool getFileSize(char * fullfilename, unsigned int & result)
{
	result = 0;
	HANDLE hFile = CreateFile(fullfilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile)
	{
		unsigned int fs = GetFileSize(hFile, NULL);
		CloseHandle(hFile);
		if (fs!=INVALID_FILE_SIZE)
		{
			result = fs;
			return true;
		}
		else
			return false;
	}
	else
		return false;
	return true;
}

bool runPluginInstaller()
{
	char * noxDir = getRendererDirectory();
	if (!noxDir)
		return false;

	char args[2048];
	sprintf_s(args, 2048, "%s\\bin\\32\\NOX_InstallPlugins.exe", noxDir);

	if (!doesFileExist(args))
		return false;


	PROCESS_INFORMATION pi;
	STARTUPINFO si = {	sizeof(STARTUPINFO),	NULL,	"",	NULL, 0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0 };

	bool ok1 = (CreateProcess(NULL, args, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS , NULL, NULL, &si, &pi) == TRUE);
	if (!ok1)
		return false;
	return true;
}

