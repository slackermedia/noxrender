#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"
#include "blackbody.h"

extern HMODULE hDllModule;
#define HGHT 35

void InitEMTemperaturePicker()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMTemperaturePicker";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMTemperaturePickerProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMTemperaturePicker *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMTemperaturePicker * GetEMTemperaturePickerInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMTemperaturePicker * emtp = (EMTemperaturePicker *)GetWindowLongPtr(hwnd, 0);
	#else
		EMTemperaturePicker * emtp = (EMTemperaturePicker *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emtp;
}

void SetEMTemperaturePickerInstance(HWND hwnd, EMTemperaturePicker *emtp)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emtp);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emtp);
	#endif
}

EMTemperaturePicker::EMTemperaturePicker(HWND hWnd)
{
	SetWindowPos(hWnd, HWND_TOP, 0,0, 364, HGHT+2, SWP_NOOWNERZORDER | SWP_NOMOVE);
	sliderPos = 50;
	colBorderLeftUp = RGB(32,32,32);
	colBorderRightDown = RGB(128,128,128);
	colSlider = RGB(48,48,48);
	colText = RGB(0,0,0);
	changeTextPosValue = 4500;
	hwnd = hWnd;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
}

EMTemperaturePicker::~EMTemperaturePicker()
{
	if (hFont)
		DeleteObject(hFont);
}

LRESULT CALLBACK EMTemperaturePickerProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMTemperaturePicker* emtp;
	emtp = GetEMTemperaturePickerInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc, thdc;
	RECT rect;
	HPEN hOldPen;

	switch (msg)
	{
		case WM_CREATE:
			emtp = new EMTemperaturePicker(hwnd);
			SetEMTemperaturePickerInstance(hwnd, emtp);
			break;
		case WM_DESTROY:
			{
				delete emtp;
				SetEMTemperaturePickerInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			hdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// CREATE TEMP HDC
			thdc = CreateCompatibleDC(hdc); 
			HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
			HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);


			// DRAW BACKGROUND
			for (int i=0; i<=180; i++)
			{
				float r,g,b;
				if (i%2)
				{
					r = 0.5f*(blackbody[(i/2)*3] + blackbody[(i/2)*3+3]);
					g = 0.5f*(blackbody[(i/2)*3+1] + blackbody[(i/2)*3+4]);
					b = 0.5f*(blackbody[(i/2)*3+2] + blackbody[(i/2)*3+5]);
				}
				else
				{
					r = blackbody[(i/2)*3];
					g = blackbody[(i/2)*3+1];
					b = blackbody[(i/2)*3+2];
				}
				r = pow(r,(1/2.2f));
				g = pow(g,(1/2.2f));
				b = pow(b,(1/2.2f));
				int ir = int(255*r);
				int ig = int(255*g);
				int ib = int(255*b);
				RECT rct;
				rct.bottom = rect.bottom-1;
				rct.top = 1;
				rct.left = i*2+1;
				rct.right = i*2+3;
				HBRUSH hbr = CreateSolidBrush(RGB(ir,ig,ib));
				FillRect(thdc, &rct, hbr);
				DeleteObject(hbr);
			}

			// DRAW SLIDER
			POINT eSlider[] = {   
				{emtp->sliderPos*2, 1},
				{emtp->sliderPos*2+3, 1},
				{emtp->sliderPos*2+3, rect.bottom-2},
				{emtp->sliderPos*2, rect.bottom-2},
				{emtp->sliderPos*2, 1} };
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emtp->colSlider));
			Polyline(thdc, eSlider, 5);
			DeleteObject(SelectObject(thdc, hOldPen));

			// DRAW TEXT
			HFONT hOldFont = (HFONT)SelectObject(thdc, emtp->hFont);
			SetTextColor(thdc, emtp->colText);
			SIZE sz;
			char tbuf[32];
			SetBkMode(thdc, TRANSPARENT);
			int temperature = emtp->sliderPos * 50 + 1000;
			sprintf_s(tbuf,32, "%d K", temperature);
			GetTextExtentPoint32(thdc, tbuf, (int)strlen(tbuf), &sz);
			if (temperature < emtp->changeTextPosValue)
				ExtTextOut(thdc, emtp->sliderPos*2+10, (HGHT-sz.cy)/2,   ETO_CLIPPED, &rect, tbuf, (int)strlen(tbuf), 0);
			else
				ExtTextOut(thdc, emtp->sliderPos*2-sz.cx-10, (HGHT-sz.cy)/2,   ETO_CLIPPED, &rect, tbuf, (int)strlen(tbuf), 0);

			SelectObject(thdc, hOldFont);

			// DRAW BORDER
			POINT eBorder1[] = {    {0,   rect.bottom-1 },    {0,   0},    {rect.right,   0}    };
			POINT eBorder2[] = {    {rect.right-1,   1},    {rect.right-1,   rect.bottom-1},    {0, rect.bottom-1}    };
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emtp->colBorderLeftUp));
			Polyline(thdc, eBorder1, 3);
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emtp->colBorderRightDown)));
			Polyline(thdc, eBorder2, 3);
			DeleteObject(SelectObject(thdc, hOldPen));

			// COPY TO ORIGINAL HDC
			GetClientRect(hwnd, &rect);
			BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
			SelectObject(thdc, oldBMP);
			DeleteObject(hbmp);
			DeleteDC(thdc);

			EndPaint(hwnd, &ps);
		}
		break;
		case WM_LBUTTONDOWN:
		{
			if (!GetCapture())
				SetCapture(hwnd);
			POINT pt = { (short)LOWORD(lParam),  (short)HIWORD(lParam) };
			emtp->sliderPos = min(180, max(0, (pt.x-1)/2));
			InvalidateRect(hwnd, NULL, false);
			emtp->notifyParent();
		}
		break;
		case WM_LBUTTONUP:
		{
			POINT pt = { (short)LOWORD(lParam),  (short)HIWORD(lParam) };
			emtp->sliderPos = min(180, max(0,(pt.x-1)/2));
			InvalidateRect(hwnd, NULL, false);
			if (GetCapture() == hwnd)
				ReleaseCapture();
			emtp->notifyParent();
		}
		break;
		case WM_MOUSEMOVE:
		{
			if (GetCapture() == hwnd)
			{
				POINT pt = { (short)LOWORD(lParam),  (short)HIWORD(lParam) };
				emtp->sliderPos = min(180, max(0,(pt.x-1)/2));
				InvalidateRect(hwnd, NULL, false);
				emtp->notifyParent();
			}
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}


int EMTemperaturePicker::getTemperature()
{
	int slpos = min(180, max(0 ,sliderPos));
	int temperature = slpos * 50 + 1000;
	return temperature;
}

void EMTemperaturePicker::setTemperature(int tmp)
{
	int slpos = (tmp-1000)/50;
	slpos = min(180, max(0 ,slpos));
	sliderPos = slpos;
	InvalidateRect(hwnd, NULL, false);
}

Color4 EMTemperaturePicker::getEnergyColor()
{
	int temperature = sliderPos*50+1000;
	return TemperatureColor::getEnergyColor(temperature);
}


Color4 EMTemperaturePicker::getGammaColor()
{
	int temperature = sliderPos*50+1000;
	return TemperatureColor::getGammaColor(temperature);
}

void EMTemperaturePicker::notifyParent()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_HSCROLL), (LPARAM)hwnd);
}

