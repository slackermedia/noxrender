#include "resource.h"
#include "RendererMainWindow.h"
//#include "MaterialEditorMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>
#include "XML_IO.h"

void initializeBlendControls(HWND hWnd);
void setBlendDefaultValues(HWND hWnd);
void blendUItoScene(HWND hWnd);
void blendSceneToUI(HWND hWnd);
void blendNamesUItoScene(HWND hWnd);
void blendNamesSceneToUI(HWND hWnd);
bool exportBlendSettings(HWND hWnd, BlendSettings * blend);
bool importBlendNames(HWND hWnd, BlendSettings * blend);

bool redrawFakeDofOnThread(HWND hImg);	// for final tab


#define BL_DIFF 1.0f

//------------------------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMainWindow::BlendWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				initializeBlendControls(hWnd);
				setBlendDefaultValues(hWnd);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_BLEND_W1:
					case IDC_REND_BLEND_W2:
					case IDC_REND_BLEND_W3:
					case IDC_REND_BLEND_W4:
					case IDC_REND_BLEND_W5:
					case IDC_REND_BLEND_W6:
					case IDC_REND_BLEND_W7:
					case IDC_REND_BLEND_W8:
					case IDC_REND_BLEND_W9:
					case IDC_REND_BLEND_W10:
					case IDC_REND_BLEND_W11:
					case IDC_REND_BLEND_W12:
					case IDC_REND_BLEND_W13:
					case IDC_REND_BLEND_W14:
					case IDC_REND_BLEND_W15:
					case IDC_REND_BLEND_W16:

					case IDC_REND_BLEND_R1:
					case IDC_REND_BLEND_R2:
					case IDC_REND_BLEND_R3:
					case IDC_REND_BLEND_R4:
					case IDC_REND_BLEND_R5:
					case IDC_REND_BLEND_R6:
					case IDC_REND_BLEND_R7:
					case IDC_REND_BLEND_R8:
					case IDC_REND_BLEND_R9:
					case IDC_REND_BLEND_R10:
					case IDC_REND_BLEND_R11:
					case IDC_REND_BLEND_R12:
					case IDC_REND_BLEND_R13:
					case IDC_REND_BLEND_R14:
					case IDC_REND_BLEND_R15:
					case IDC_REND_BLEND_R16:

					case IDC_REND_BLEND_G1:
					case IDC_REND_BLEND_G2:
					case IDC_REND_BLEND_G3:
					case IDC_REND_BLEND_G4:
					case IDC_REND_BLEND_G5:
					case IDC_REND_BLEND_G6:
					case IDC_REND_BLEND_G7:
					case IDC_REND_BLEND_G8:
					case IDC_REND_BLEND_G9:
					case IDC_REND_BLEND_G10:
					case IDC_REND_BLEND_G11:
					case IDC_REND_BLEND_G12:
					case IDC_REND_BLEND_G13:
					case IDC_REND_BLEND_G14:
					case IDC_REND_BLEND_G15:
					case IDC_REND_BLEND_G16:

					case IDC_REND_BLEND_B1:
					case IDC_REND_BLEND_B2:
					case IDC_REND_BLEND_B3:
					case IDC_REND_BLEND_B4:
					case IDC_REND_BLEND_B5:
					case IDC_REND_BLEND_B6:
					case IDC_REND_BLEND_B7:
					case IDC_REND_BLEND_B8:
					case IDC_REND_BLEND_B9:
					case IDC_REND_BLEND_B10:
					case IDC_REND_BLEND_B11:
					case IDC_REND_BLEND_B12:
					case IDC_REND_BLEND_B13:
					case IDC_REND_BLEND_B14:
					case IDC_REND_BLEND_B15:
					case IDC_REND_BLEND_B16:
						{
							if (wmEvent == WM_HSCROLL)
							{
								blendUItoScene(hWnd);
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
							}
						}
						break;

					case IDC_REND_BLEND_ON1:
					case IDC_REND_BLEND_ON2:
					case IDC_REND_BLEND_ON3:
					case IDC_REND_BLEND_ON4:
					case IDC_REND_BLEND_ON5:
					case IDC_REND_BLEND_ON6:
					case IDC_REND_BLEND_ON7:
					case IDC_REND_BLEND_ON8:
					case IDC_REND_BLEND_ON9:
					case IDC_REND_BLEND_ON10:
					case IDC_REND_BLEND_ON11:
					case IDC_REND_BLEND_ON12:
					case IDC_REND_BLEND_ON13:
					case IDC_REND_BLEND_ON14:
					case IDC_REND_BLEND_ON15:
					case IDC_REND_BLEND_ON16:
						{
							if (wmEvent == BN_CLICKED)
							{
								blendUItoScene(hWnd);
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
							}
						}
						break;

					case IDC_REND_BLEND_NAME1:
					case IDC_REND_BLEND_NAME2:
					case IDC_REND_BLEND_NAME3:
					case IDC_REND_BLEND_NAME4:
					case IDC_REND_BLEND_NAME5:
					case IDC_REND_BLEND_NAME6:
					case IDC_REND_BLEND_NAME7:
					case IDC_REND_BLEND_NAME8:
					case IDC_REND_BLEND_NAME9:
					case IDC_REND_BLEND_NAME10:
					case IDC_REND_BLEND_NAME11:
					case IDC_REND_BLEND_NAME12:
					case IDC_REND_BLEND_NAME13:
					case IDC_REND_BLEND_NAME14:
					case IDC_REND_BLEND_NAME15:
					case IDC_REND_BLEND_NAME16:
						{
							if (wmEvent == ES_RETURN  ||  wmEvent == ES_LOST_FOCUS)
							{
								blendNamesUItoScene(hWnd);
							}
						}
						break;

					case IDC_REND_BLEND_RESET1:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W1))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R1))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G1))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B1))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET2:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W2))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R2))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G2))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B2))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET3:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W3))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R3))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G3))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B3))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET4:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W4))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R4))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G4))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B4))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET5:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W5))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R5))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G5))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B5))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET6:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W6))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R6))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G6))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B6))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET7:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W7))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R7))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G7))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B7))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET8:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W8))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R8))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G8))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B8))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET9:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W9))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R9))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G9))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B9))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET10:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W10))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R10))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G10))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B10))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET11:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W11))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R11))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G11))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B11))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET12:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W12))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R12))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G12))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B12))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET13:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W13))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R13))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G13))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B13))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET14:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W14))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R14))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G14))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B14))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET15:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W15))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R15))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G15))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B15))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_RESET16:
						{
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W16))->setValuesToFloat(100,	NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R16))->setValuesToFloat(100,	NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G16))->setValuesToFloat(100,	NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
							GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B16))->setValuesToFloat(100,	NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
							blendUItoScene(hWnd);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
						break;
					case IDC_REND_BLEND_IMPORT:
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							if (!sc)
								break;
							bool ok = importBlendNames(hWnd, &(sc->blendSettings) );
							if (ok)
							{
								blendSceneToUI(hWnd);
								refreshRenderedImage();
							}
						}
						break;
					case IDC_REND_BLEND_EXPORT:
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							if (!sc)
								break;
							exportBlendSettings(hWnd, &(sc->blendSettings) );
						}
						break;
					case IDC_REND_BLEND_RESET_ALL:
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							CHECK(sc);

							int dRes = MessageBox(hWnd, "All blend layers settings will be lost.\nAre you sure?", "Warning.", MB_YESNO);
							if (dRes==IDYES)
							{
								for (int i=0; i<16; i++)
								{
									sc->blendSettings.blendOn[i] = true;
									sc->blendSettings.weights[i] = 1.0f;
									sc->blendSettings.red[i]     = 1.0f;
									sc->blendSettings.green[i]   = 1.0f;
									sc->blendSettings.blue[i]    = 1.0f;
								}
								blendSceneToUI(hWnd);
							}
						}
						break;
					case IDC_REND_BLEND_REDRAW_FINAL:
						{
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->curScenePtr->nowRendering)
							{
								MessageBox(0, "To finalize image, stop renderer first.", "", 0);
								break;
							}
							Camera * cam = rtr->curScenePtr->getActiveCamera();
							if (!cam)
								break;
							redrawFakeDofOnThread(hImage);
						}
						break;
				}
			}
			break;
		case WM_TIMER:
			{
				if (wParam == TIMER_POSTPROCESS_ID)
				{
					KillTimer(hWnd, TIMER_POSTPROCESS_ID);
					refreshRenderedImage();
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------------------------------------------

void initializeBlendControls(HWND hWnd)
{
	COLORREF colBlendBefore = RGB(180,180,180);
	COLORREF colBlendAfter  = RGB(180,180,180);
	COLORREF colRedBefore   = RGB(170, 30, 30);
	COLORREF colRedAfter    = RGB(170, 30, 30);
	COLORREF colGreenBefore = RGB( 30,170, 30);
	COLORREF colGreenAfter  = RGB( 30,170, 30);
	COLORREF colBlueBefore  = RGB( 30, 30,170);
	COLORREF colBlueAfter   = RGB( 30, 30,170);

	RendererMainWindow::theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_IMPORT)));
	RendererMainWindow::theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_EXPORT)));
	RendererMainWindow::theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_REDRAW_FINAL)));
	RendererMainWindow::theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET_ALL)));

	HWND hPrevTab = 0;

	for (int i=0; i<4; i++)
	{
		for (int j=0; j<4; j++)
		{
			EMEditTrackBar * emblend  = NULL;
			EMEditTrackBar * emred  = NULL;
			EMEditTrackBar * emgreen  = NULL;
			EMEditTrackBar * emblue  = NULL;
			EMCheckBox * emc  = NULL;
			EMEditSimple * emname  = NULL;
			EMLine * emline  = NULL;
			EMLine * emline2  = NULL;
			EMButton * emreset = NULL;
			HWND hText = 0;

			int k = i*4+j + 1;
			switch (k)
			{
				case 1:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W1));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R1));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G1));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B1));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON1));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME1));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE1));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET1));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT1);
					break;
				case 2:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W2));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R2));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G2));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B2));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON2));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME2));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE2));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET2));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT2);
					break;
				case 3:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W3));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R3));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G3));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B3));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON3));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME3));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE3));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET3));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT3);
					break;
				case 4:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W4));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R4));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G4));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B4));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON4));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME4));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE4));
					emline2  = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE5));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET4));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT4);
					break;
				case 5:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W5));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R5));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G5));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B5));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON5));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME5));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE6));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET5));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT5);
					break;
				case 6:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W6));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R6));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G6));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B6));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON6));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME6));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE7));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET6));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT6);
					break;
				case 7:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W7));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R7));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G7));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B7));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON7));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME7));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE8));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET7));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT7);
					break;
				case 8:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W8));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R8));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G8));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B8));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON8));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME8));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE9));
					emline2  = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE10));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET8));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT8);
					break;
				case 9:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W9));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R9));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G9));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B9));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON9));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME9));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE11));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET9));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT9);
					break;
				case 10:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W10));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R10));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G10));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B10));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON10));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME10));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE12));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET10));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT10);
					break;
				case 11:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W11));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R11));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G11));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B11));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON11));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME11));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE13));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET11));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT11);
					break;
				case 12:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W12));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R12));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G12));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B12));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON12));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME12));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE14));
					emline2  = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE15));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET12));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT12);
					break;
				case 13:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W13));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R13));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G13));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B13));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON13));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME13));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE16));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET13));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT13);
					break;
				case 14:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W14));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R14));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G14));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B14));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON14));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME14));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE17));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET14));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT14);
					break;
				case 15:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W15));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R15));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G15));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B15));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON15));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME15));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE18));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET15));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT15);
					break;
				case 16:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W16));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R16));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G16));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B16));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON16));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME16));
					emline   = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE19));
					emline2  = GetEMLineInstance(GetDlgItem(hWnd, IDC_REND_BLEND_LINE20));
					emreset  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_BLEND_RESET16));
					hText = GetDlgItem(hWnd, IDC_REND_BLEND_TEXT16);
					break;
				default: 
					continue;
			}

			int x = i*319;
			int y = j*64-3;

			SetWindowPos(emblend->hwnd, emname->hwnd,     23+x,  31+y,    283, 18,   SWP_NOOWNERZORDER);
			SetWindowPos(emred->hwnd,	emblend->hwnd,     4+x,  50+y,    100, 18,   SWP_NOOWNERZORDER);
			SetWindowPos(emgreen->hwnd,	emred->hwnd,     105+x,  50+y,    100, 18,   SWP_NOOWNERZORDER);
			SetWindowPos(emblue->hwnd,  emgreen->hwnd,   206+x,  50+y,    100, 18,   SWP_NOOWNERZORDER);
			SetWindowPos(emc->hwnd,		HWND_TOP,          4+x,  31+y,     15, 15,   SWP_NOOWNERZORDER);
			SetWindowPos(emname->hwnd,  hPrevTab,         56+x,  12+y,    206, 15,   SWP_NOOWNERZORDER);
			SetWindowPos(emreset->hwnd, HWND_TOP,        264+x,  12+y,     38, 17,   SWP_NOOWNERZORDER);
			SetWindowPos(emline->hwnd,  HWND_TOP,          3+x,   7+y,    300,  2,   SWP_NOOWNERZORDER);
			SetWindowPos(hText,			HWND_TOP,	       4+x,  13+y,     50, 15,   SWP_NOOWNERZORDER);
			if (k%4==0)
				SetWindowPos(emline2->hwnd,  HWND_TOP,     3+x,   64+7+y,    300, 2,   SWP_NOOWNERZORDER);
			SetWindowPos(emname->hEdit, HWND_TOP,   0, 0,   204,  13,   SWP_NOOWNERZORDER | SWP_NOMOVE);
			hPrevTab = emblue->hwnd;

			RendererMainWindow::theme.apply(emblend);
			RendererMainWindow::theme.apply(emred);
			RendererMainWindow::theme.apply(emgreen);
			RendererMainWindow::theme.apply(emblue);
			RendererMainWindow::theme.apply(emc);
			RendererMainWindow::theme.apply(emname);
			RendererMainWindow::theme.apply(emreset);
			RendererMainWindow::theme.apply(emline);
			if (k%4==0)
				RendererMainWindow::theme.apply(emline2);

			emblend->colPathBackgroundBefore  = colBlendBefore;
			emblend->colPathBackgroundAfter   = colBlendAfter;
			emred->colPathBackgroundBefore  = colRedBefore;
			emred->colPathBackgroundAfter   = colRedAfter;
			emgreen->colPathBackgroundBefore  = colGreenBefore;
			emgreen->colPathBackgroundAfter   = colGreenAfter;
			emblue->colPathBackgroundBefore  = colBlueBefore;
			emblue->colPathBackgroundAfter   = colBlueAfter;
			emblend->setEditBoxWidth(32);
			emred->setEditBoxWidth(32);
			emgreen->setEditBoxWidth(32);
			emblue->setEditBoxWidth(32);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

void setBlendDefaultValues(HWND hWnd)
{
	for (int i=0; i<4; i++)
	{
		for (int j=0; j<4; j++)
		{
			EMEditTrackBar * emblend  = NULL;
			EMEditTrackBar * emred  = NULL;
			EMEditTrackBar * emgreen  = NULL;
			EMEditTrackBar * emblue  = NULL;
			EMCheckBox * emc  = NULL;
			EMEditSimple * emname  = NULL;

			int k = i*4+j + 1;
			switch (k)
			{
				case 1:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W1));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R1));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G1));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B1));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON1));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME1));
					break;
				case 2:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W2));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R2));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G2));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B2));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON2));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME2));
					break;
				case 3:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W3));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R3));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G3));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B3));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON3));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME3));
					break;
				case 4:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W4));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R4));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G4));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B4));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON4));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME4));
					break;
				case 5:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W5));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R5));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G5));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B5));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON5));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME5));
					break;
				case 6:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W6));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R6));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G6));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B6));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON6));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME6));
					break;
				case 7:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W7));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R7));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G7));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B7));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON7));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME7));
					break;
				case 8:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W8));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R8));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G8));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B8));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON8));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME8));
					break;
				case 9:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W9));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R9));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G9));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B9));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON9));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME9));
					break;
				case 10:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W10));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R10));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G10));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B10));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON10));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME10));
					break;
				case 11:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W11));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R11));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G11));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B11));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON11));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME11));
					break;
				case 12:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W12));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R12));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G12));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B12));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON12));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME12));
					break;
				case 13:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W13));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R13));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G13));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B13));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON13));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME13));
					break;
				case 14:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W14));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R14));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G14));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B14));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON14));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME14));
					break;
				case 15:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W15));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R15));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G15));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B15));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON15));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME15));
					break;
				case 16:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W16));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R16));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G16));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B16));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON16));
					emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME16));
					break;
				default: 
					continue;
			}

			emblend->setValuesToFloat(100, NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX, BL_DIFF);
			emred->setValuesToFloat(  100, NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX, BL_DIFF);
			emgreen->setValuesToFloat(100, NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX, BL_DIFF);
			emblue->setValuesToFloat( 100, NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX, BL_DIFF);
			emc->selected = true;
			InvalidateRect(emc->hwnd, NULL, false);
			char tempchar[64];
			sprintf_s(tempchar, 64, "Layer %d", k);
			emname->setText(tempchar);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

void blendUItoScene(HWND hWnd)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	for (int i=0; i<4; i++)
	{
		for (int j=0; j<4; j++)
		{
			EMEditTrackBar * emblend  = NULL;
			EMEditTrackBar * emred  = NULL;
			EMEditTrackBar * emgreen  = NULL;
			EMEditTrackBar * emblue  = NULL;
			EMCheckBox * emc  = NULL;

			int k = i*4+j + 1;
			switch (k)
			{
				case 1:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W1));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R1));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G1));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B1));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON1));
					break;
				case 2:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W2));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R2));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G2));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B2));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON2));
					break;
				case 3:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W3));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R3));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G3));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B3));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON3));
					break;
				case 4:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W4));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R4));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G4));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B4));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON4));
					break;
				case 5:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W5));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R5));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G5));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B5));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON5));
					break;
				case 6:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W6));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R6));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G6));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B6));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON6));
					break;
				case 7:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W7));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R7));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G7));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B7));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON7));
					break;
				case 8:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W8));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R8));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G8));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B8));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON8));
					break;
				case 9:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W9));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R9));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G9));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B9));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON9));
					break;
				case 10:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W10));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R10));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G10));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B10));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON10));
					break;
				case 11:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W11));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R11));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G11));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B11));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON11));
					break;
				case 12:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W12));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R12));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G12));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B12));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON12));
					break;
				case 13:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W13));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R13));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G13));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B13));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON13));
					break;
				case 14:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W14));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R14));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G14));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B14));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON14));
					break;
				case 15:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W15));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R15));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G15));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B15));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON15));
					break;
				case 16:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W16));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R16));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G16));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B16));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON16));
					break;
				default: 
					continue;
			}

			int num = k-1;
			sc->blendSettings.blendOn[num] = emc->selected;
			sc->blendSettings.weights[num] = emblend->floatValue / 100.0f;
			sc->blendSettings.red[num]     = emred->floatValue / 100.0f;
			sc->blendSettings.green[num]   = emgreen->floatValue / 100.0f;
			sc->blendSettings.blue[num]    = emblue->floatValue / 100.0f;

		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

void blendSceneToUI(HWND hWnd)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	for (int i=0; i<4; i++)
	{
		for (int j=0; j<4; j++)
		{
			EMEditTrackBar * emblend  = NULL;
			EMEditTrackBar * emred  = NULL;
			EMEditTrackBar * emgreen  = NULL;
			EMEditTrackBar * emblue  = NULL;
			EMCheckBox * emc  = NULL;

			int k = i*4+j + 1;
			switch (k)
			{
				case 1:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W1));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R1));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G1));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B1));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON1));
					break;
				case 2:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W2));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R2));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G2));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B2));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON2));
					break;
				case 3:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W3));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R3));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G3));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B3));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON3));
					break;
				case 4:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W4));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R4));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G4));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B4));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON4));
					break;
				case 5:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W5));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R5));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G5));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B5));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON5));
					break;
				case 6:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W6));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R6));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G6));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B6));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON6));
					break;
				case 7:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W7));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R7));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G7));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B7));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON7));
					break;
				case 8:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W8));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R8));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G8));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B8));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON8));
					break;
				case 9:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W9));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R9));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G9));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B9));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON9));
						break;
				case 10:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W10));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R10));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G10));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B10));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON10));
					break;
				case 11:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W11));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R11));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G11));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B11));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON11));
					break;
				case 12:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W12));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R12));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G12));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B12));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON12));
					break;
				case 13:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W13));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R13));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G13));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B13));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON13));
					break;
				case 14:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W14));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R14));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G14));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B14));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON14));
					break;
				case 15:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W15));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R15));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G15));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B15));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON15));
					break;
				case 16:
					emblend  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_W16));
					emred    = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_R16));
					emgreen  = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_G16));
					emblue   = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_BLEND_B16));
					emc      = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_BLEND_ON16));
					break;
				default: 
					continue;
			}

			int num = k-1;
			emblend->setValuesToFloat(100.0f*sc->blendSettings.weights[num]	, NOX_BLEND_WEIGHT_MIN, NOX_BLEND_WEIGHT_MAX,	BL_DIFF);
			emred->setValuesToFloat(  100.0f*sc->blendSettings.red[num]		, NOX_BLEND_RED_MIN,    NOX_BLEND_RED_MAX,		BL_DIFF);
			emgreen->setValuesToFloat(100.0f*sc->blendSettings.green[num]	, NOX_BLEND_GREEN_MIN,  NOX_BLEND_GREEN_MAX,	BL_DIFF);
			emblue->setValuesToFloat( 100.0f*sc->blendSettings.blue[num]	, NOX_BLEND_BLUE_MIN,   NOX_BLEND_BLUE_MAX,		BL_DIFF);
			emc->selected = sc->blendSettings.blendOn[num];
			InvalidateRect(emc->hwnd, NULL, false);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

void blendNamesUItoScene(HWND hWnd)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	for (int i=0; i<4; i++)
	{
		for (int j=0; j<4; j++)
		{
			EMEditSimple * emname  = NULL;

			int k = i*4+j + 1;
			switch (k)
			{
				case 1: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME1));			break;
				case 2: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME2));			break;
				case 3: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME3));			break;
				case 4: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME4));			break;
				case 5: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME5));			break;
				case 6: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME6));			break;
				case 7: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME7));			break;
				case 8: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME8));			break;
				case 9: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME9));			break;
				case 10: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME10));		break;
				case 11: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME11));		break;
				case 12: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME12));		break;
				case 13: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME13));		break;
				case 14: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME14));		break;
				case 15: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME15));		break;
				case 16: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME16));		break;
				default: continue;
			}

			int num = k-1;
			sc->blendSettings.setName(num, emname->getText());

		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

void blendNamesSceneToUI(HWND hWnd)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	for (int i=0; i<4; i++)
	{
		for (int j=0; j<4; j++)
		{
			EMEditSimple * emname  = NULL;

			int k = i*4+j + 1;
			switch (k)
			{
				case 1: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME1));			break;
				case 2: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME2));			break;
				case 3: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME3));			break;
				case 4: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME4));			break;
				case 5: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME5));			break;
				case 6: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME6));			break;
				case 7: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME7));			break;
				case 8: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME8));			break;
				case 9: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME9));			break;
				case 10: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME10));		break;
				case 11: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME11));		break;
				case 12: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME12));		break;
				case 13: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME13));		break;
				case 14: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME14));		break;
				case 15: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME15));		break;
				case 16: emname   = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_REND_BLEND_NAME16));		break;
				default: continue;
			}

			int num = k-1;
			emname->setText(sc->blendSettings.names[num]);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillBlendLayersData()
{
	blendSceneToUI(hBlend);
	blendNamesSceneToUI(hBlend);
}

//------------------------------------------------------------------------------------------------------------------------------------

bool exportBlendSettings(HWND hWnd, BlendSettings * blend)
{
	if (!blend)
	{
		MessageBox(0, "Internal error, blend settings is NULL.\nNot saved.", "Error", 0);
		return false;
	}

	char * filename;
	filename = saveFileDialog(hWnd, "NOX XML blend layer names (*.nxb)\0*.nxb\0", "Save blend layer names", "nxb", 1);
	if (!filename)
		return false;

	XMLScene xscene;
	bool saved = xscene.saveBlendSettings(filename, *blend, false);
	
	if (saved)
		MessageBox(hWnd, "Blend XML saved", "", 0);
	else
		MessageBox(hWnd, "Blend sXML not saved", "", 0);

	return saved;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool importBlendNames(HWND hWnd, BlendSettings * blend)
{
	if (!blend)
	{
		MessageBox(0, "Internal error, blend settings is NULL.\nNot saved.", "Error", 0);
		return false;
	}

	char * filename;
	filename = openFileDialog(hWnd, "XML blend layer names (*.nxb)\0*.nxb\0", "Load blend layer names", "nxb", 1);
	if (!filename)
		return false;

	XMLScene xscene;
	int ok = xscene.loadBlendSettings(filename, *blend, false);
	if (!ok)
	{
		int err = xscene.errLine;
		char buf[256];
		if (err>0)
			sprintf_s(buf, 256, "Something is wrong with loading blend layer names.\nWatch line %d", err); 
		else
			sprintf_s(buf, 256, "Something is wrong with XML while loading blend layer names.\nError code: %d", err); 
		MessageBox(0, buf, "", 0);
		return false;
	}

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

