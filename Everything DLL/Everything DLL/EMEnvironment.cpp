#include <windows.h>
#include <stdio.h>
#include "EMControls.h"
#include <GdiPlus.h>

extern HMODULE hDllModule;
#define EN_bpp 32
#define EN_Bpp 4

EMEnvironment::EMEnvironment(HWND hWnd) 
{
	hwnd = hWnd;
	bmEarth = 0;
	drawBorder = true;

	earthBits = (unsigned char *)malloc(EN_Bpp*360*180);
	shBits = (unsigned char *)malloc(EN_Bpp*360*180);
	shBMPinfo.bmWidthBytes = 360 * EN_Bpp;
	if (shBits)
	{
		shBMPinfo.bmBitsPixel = EN_bpp;
		shBMPinfo.bmHeight = 180;
		shBMPinfo.bmWidth = 360;
		shBMPinfo.bmPlanes = 1;
		shBMPinfo.bmType = 0;
		shBMPinfo.bmWidthBytes = 360 * EN_Bpp;
		shBMPinfo.bmBits = shBits;
	}
	else 
		shadowedOK = false;

	evalShadow();
}

EMEnvironment::~EMEnvironment() 
{
	if (shadowedOK)
		DeleteObject(bmShadowedEarth);
	if (shBits)
		free(shBits);
	if (earthBits)
		free(earthBits);
		
}

void InitEMEnvironment()
{
    WNDCLASSEX wc;

	wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMEnvironment";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMEnvironmentProc;
	wc.hCursor        = LoadCursor (NULL, IDC_CROSS);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMEnvironment *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMEnvironment * GetEMEnvironmentInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMEnvironment * emen = (EMEnvironment *)GetWindowLongPtr(hwnd, 0);
	#else
		EMEnvironment * emen = (EMEnvironment *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emen;
}

void SetEMEnvironmentInstance(HWND hwnd, EMEnvironment *emen)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emen);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emen);
	#endif
}

LRESULT CALLBACK EMEnvironmentProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMEnvironment * emen;
	emen = GetEMEnvironmentInstance(hwnd);

    switch(msg)
    {
	case WM_CREATE:
		{
			EMEnvironment * emen1 = new EMEnvironment(hwnd);
			SetEMEnvironmentInstance(hwnd, (EMEnvironment *)emen1);			
		}
	break;
	case WM_NCDESTROY:
		{
			EMEnvironment * emen1;
			emen1 = GetEMEnvironmentInstance(hwnd);
			delete emen1;
			SetEMEnvironmentInstance(hwnd, 0);
		}
	break;
	case WM_PAINT:
	{
		if (!emen) 
			break;

		RECT rect;
		HDC hdc, thdc;
		PAINTSTRUCT ps;

		// get DC and create backbuffer DC
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rect);
		thdc = CreateCompatibleDC(hdc); 
		HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
		HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

		bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) != 0);
		int pp = emen->drawBorder ? 1 : 0;

		if (emen->shadowedOK)
			emen->drawShadowTexture(thdc);
		else
			// copy original earth texture
			if (emen->bmEarth)
			{
				BITMAP bm;
				GetObject(emen->bmEarth, sizeof(BITMAP), &bm);
				int ry = bm.bmHeight;
				int rx = bm.bmWidth;
				HDC bmpdc = CreateCompatibleDC(hdc);
				HBITMAP oldhbmp = (HBITMAP)SelectObject(bmpdc, emen->bmEarth); 
				BitBlt(thdc, pp, pp, rx, ry, bmpdc, 0, 0, SRCCOPY);
				SelectObject(bmpdc, oldhbmp); 
				DeleteDC(bmpdc); 
			}

		{	// position circle
			int x,y;
			x = ((int)(-emen->sunsky.getLongitude()*180/PI + 180)) % 360 + pp;
			y = ((int)(-emen->sunsky.getLatitude()*180/PI + 90)) % 180 + pp;
			HPEN oldPen = (HPEN)(SelectObject(thdc, CreatePen(PS_SOLID, 2, RGB(255,0,255) ) ) );
			HBRUSH oldBrush = (HBRUSH)SelectObject(thdc, (HBRUSH)GetStockObject(HOLLOW_BRUSH));
			Ellipse(thdc, x-5, y-5, x+5, y+5);
			DeleteObject(SelectObject(thdc, oldBrush));
			DeleteObject(SelectObject(thdc, oldPen));
		}

		{	// sun circle
			int x,y;
			x = ((int)(emen->sunPhi + 180)) % 360 + pp;
			y = ((int)(-emen->sunTheta + 90)) % 180 + pp;
			HPEN oldPen = (HPEN)(SelectObject(thdc, CreatePen(PS_SOLID, 1, RGB(255,255,255) ) ) );
			HBRUSH oldBrush = (HBRUSH)SelectObject(thdc, CreateSolidBrush(RGB(255,255,0)));
			Ellipse(thdc, x-4, y-4, x+5, y+5);
			DeleteObject(SelectObject(thdc, oldBrush));
			DeleteObject(SelectObject(thdc, oldPen));
		}

		if (disabled)
			emen->drawDisabledFog(thdc, hdc);

		// copy and free resources
		GetClientRect(hwnd, &rect);
		BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
		SelectObject(thdc, oldBMP);
		DeleteObject(hbmp);
		DeleteDC(thdc);

		EndPaint(hwnd, &ps);

	}
	break;
	case WM_MOUSEMOVE:
		{
			if (wParam & MK_LBUTTON)
			{
				int pp = emen->drawBorder ? 1 : 0;
				int xPos = (short)LOWORD(lParam)-180 - pp; 
				xPos = min(xPos, 179);
				xPos = max(xPos, -180);
				int yPos = (short)HIWORD(lParam)-90 - pp;
				yPos = min(yPos, 89);
				yPos = max(yPos, -89);

				emen->sunsky.setPosition(-xPos*PI/180, -yPos*PI/180);
				emen->evalSunPosition();
				emen->repaint();
				emen->notifyParent();
			}
		}
		break;
	case WM_LBUTTONDOWN:
		{
			if (!GetCapture())
				SetCapture(hwnd);

			int pp = emen->drawBorder ? 1 : 0;
			int xPos = (short)LOWORD(lParam)-180 - pp; 
			xPos = min(xPos, 179);
			xPos = max(xPos, -180);
			int yPos = (short)HIWORD(lParam)-90 - pp;
			yPos = min(yPos, 89);
			yPos = max(yPos, -89);

			emen->sunsky.setPosition(-xPos*PI/180, -yPos*PI/180);
			emen->repaint();
			emen->notifyParent();
		}
		break;
	case WM_LBUTTONUP:
		{
			if (GetCapture() == hwnd)
				ReleaseCapture();
		}
		break;


    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMEnvironment::notifyParent()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, EN_POSITION_CHANGED), (LPARAM)hwnd);
}

void EMEnvironment::repaint()
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

void EMEnvironment::evalSunPosition()
{
	sunPhi = 180 - (sunsky.getSolarGlobalTimeInHours() - (int)(sunsky.getSolarGlobalTimeInHours()/24)*24) * 15.0f;
	sunTheta = sunsky.getSolarDeclination() * 180.0f/PI;
}

void EMEnvironment::evalShadow()
{
	if (!shBits)
		return;

	if (!earthBits)
		return;

	BITMAPINFO bmpi;
	bmpi.bmiColors[0].rgbRed = 255;
	bmpi.bmiColors[0].rgbGreen= 255;
	bmpi.bmiColors[0].rgbBlue = 255;
	bmpi.bmiColors[0].rgbReserved = 0;
	bmpi.bmiHeader.biBitCount = 32;
	bmpi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmpi.bmiHeader.biWidth = 360;
	bmpi.bmiHeader.biHeight = 180;
	bmpi.bmiHeader.biPlanes = 1;
	bmpi.bmiHeader.biCompression = BI_RGB;
	bmpi.bmiHeader.biSizeImage = 0;
	bmpi.bmiHeader.biXPelsPerMeter = 1;
	bmpi.bmiHeader.biYPelsPerMeter = 1;
	bmpi.bmiHeader.biClrUsed = 0;
	bmpi.bmiHeader.biClrImportant = 0;





	HDC tempDC = CreateCompatibleDC(NULL);
	HGDIOBJ hOldbmp = SelectObject(tempDC, bmEarth);
	GetDIBits(tempDC, bmEarth, 0, 180, earthBits, &bmpi, DIB_RGB_COLORS );

	SelectObject(tempDC, hOldbmp);
	DeleteDC(tempDC);


	unsigned char * line;
	unsigned char * linesrc;
	int i,j;
	for (j=0; j<180; j++)
	{
		line = shBits + j*shBMPinfo.bmWidthBytes;
		linesrc = earthBits + j*shBMPinfo.bmWidthBytes;
		for (i=0; i<360; i++)
		{
			unsigned char sAlpha = sunsky.getEnlightedAlpha((179-j)*PI/180.0f, i*PI/180.0f+PI);
			line[EN_Bpp*i+0] = linesrc[EN_Bpp*i+0]*sAlpha/255;
			line[EN_Bpp*i+1] = linesrc[EN_Bpp*i+1]*sAlpha/255;
			line[EN_Bpp*i+2] = linesrc[EN_Bpp*i+2]*sAlpha/255;
			line[EN_Bpp*i+3] = 255;
		}
	}

	bmShadowedEarth = CreateBitmapIndirect(&shBMPinfo);
	if (bmShadowedEarth)
		shadowedOK = true;
	else 
		shadowedOK = false;

}

bool EMEnvironment::drawShadowTexture(HDC hdc)
{
	bool result;
	HDC hMemSrc = CreateCompatibleDC(hdc);
	if (hMemSrc)
	{
		HBITMAP tbitmap = CreateCompatibleBitmap(hdc, shBMPinfo.bmWidth, shBMPinfo.bmHeight);
		HBITMAP oldbitmap = (HBITMAP)SelectObject(hMemSrc, tbitmap);

		BITMAPINFOHEADER bmih;
		ZeroMemory(&bmih,sizeof(bmih));
		bmih.biSize = sizeof(BITMAPINFOHEADER);
		bmih.biBitCount = 32;
		bmih.biClrImportant = 0;
		bmih.biClrUsed = 0;
		bmih.biCompression = BI_RGB;
		bmih.biHeight = shBMPinfo.bmHeight;
		bmih.biWidth = shBMPinfo.bmWidth;
		bmih.biPlanes = 1;
		bmih.biSizeImage = shBMPinfo.bmWidth*shBMPinfo.bmHeight;
		bmih.biXPelsPerMeter = 0;
		bmih.biYPelsPerMeter = 0;

		BITMAPINFO bmi;
		bmi.bmiHeader = bmih;

		int pp = drawBorder ? 1 : 0;
		int ccc = SetDIBits(hMemSrc, tbitmap, 0, shBMPinfo.bmHeight, shBits, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);

		if (BitBlt(hdc, pp, pp, 360, 180, hMemSrc, 0, 0, SRCCOPY))
			result = true;
		else
			result = false;

		SelectObject(hMemSrc, oldbitmap);
		DeleteObject(tbitmap);

		DeleteDC(hMemSrc);
	}
	else
		result = false;

	return result;
}

bool EMEnvironment::drawDisabledFog(HDC hdc, HDC orighdc)
{
	CHECK(hdc);
	RECT rect;
	GetClientRect(hwnd, &rect);

	ULONG_PTR gdiplusToken;		// activate gdi+
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	{
		Gdiplus::Graphics graphics(hdc);
		Gdiplus::SolidBrush brush(Gdiplus::Color(128, 0,0,0));
		graphics.FillRectangle(&brush,  0,0, rect.right, rect.bottom);
	}	// gdi+ variables destructors here
	Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+

	return true;
}

