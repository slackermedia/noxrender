#include "max2013predef.h"
#include "NOXCamera.h"
#include "noxdebug.h"

static NOXTargetObjectClassDesc everTargetObjDesc;
ClassDesc2* GetTargetObjDesc() { return &everTargetObjDesc; }

//------------------------------------------------------------------------------------------------------------------------

void NOXTargetObject::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)	{}
		
void NOXTargetObject::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next )	{}

int NOXTargetObjectCreateCallBack::proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat )
{
	Point3 c;
	if (msg==MOUSE_POINT||msg==MOUSE_MOVE) {
		switch(point) {
			case 0:
				c = vpt->GetPointOnCP(m);
				mat.SetTrans(c);
				return CREATE_STOP;
				break;
			}
		}
	else
	if (msg == MOUSE_ABORT)
		return CREATE_ABORT;
	return TRUE;
}

static NOXTargetObjectCreateCallBack boxEverCreateCB;

CreateMouseCallBack* NOXTargetObject::GetCreateMouseCallBack() 
{
	boxEverCreateCB.SetObj(this);
	return(&boxEverCreateCB);
}

//------------------------------------------------------------------------------------------------------------------------

void MakeQuad(Face *f, int a, int b , int c , int d, int sg) 
{
	f[0].setVerts( a, b, c);
	f[0].setSmGroup(sg);
	f[0].setEdgeVisFlags(1,1,0);
	f[1].setVerts( c, d, a);
	f[1].setSmGroup(sg);
	f[1].setEdgeVisFlags(1,1,0);
}

//------------------------------------------------------------------------------------------------------------------------

#define sz float(4.0)
void NOXTargetObject::BuildMesh()
{
	int nverts = 8;
	int nfaces = 12;
	Point3 va(-sz,-sz,-sz);
	Point3 vb( sz, sz, sz);
	targMesh.setNumVerts(nverts);
	targMesh.setNumFaces(nfaces);

	targMesh.setVert(0, Point3( va.x, va.y, va.z));
	targMesh.setVert(1, Point3( vb.x, va.y, va.z));
	targMesh.setVert(2, Point3( va.x, vb.y, va.z));
	targMesh.setVert(3, Point3( vb.x, vb.y, va.z));
	targMesh.setVert(4, Point3( va.x, va.y, vb.z));
	targMesh.setVert(5, Point3( vb.x, va.y, vb.z));
	targMesh.setVert(6, Point3( va.x, vb.y, vb.z));
	targMesh.setVert(7, Point3( vb.x, vb.y, vb.z));

	MakeQuad(&(targMesh.faces[ 0]), 0,2,3,1,  1);
	MakeQuad(&(targMesh.faces[ 2]), 2,0,4,6,  2);
	MakeQuad(&(targMesh.faces[ 4]), 3,2,6,7,  4);
	MakeQuad(&(targMesh.faces[ 6]), 1,3,7,5,  8);
	MakeQuad(&(targMesh.faces[ 8]), 0,1,5,4, 16);
	MakeQuad(&(targMesh.faces[10]), 4,5,7,6, 32);
	targMesh.buildNormals();
	targMesh.EnableEdgeList(1);
}

//------------------------------------------------------------------------------------------------------------------------

NOXTargetObject::NOXTargetObject()
{
	BuildMesh();
}

//------------------------------------------------------------------------------------------------------------------------

void NOXTargetObject::GetMatrix(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm) 
{
	tm = inode->GetObjectTM(t);
	tm.NoScale();
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(tm.GetTrans())/(float)360.0;
	if (scaleFactor!=(float)1.0)
		tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
}

//------------------------------------------------------------------------------------------------------------------------

void NOXTargetObject::GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel )
{
	box = targMesh.getBoundingBox(tm);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXTargetObject::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	Matrix3 m = inode->GetObjectTM(t);
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(m.GetTrans())/(float)360.0;
	box = targMesh.getBoundingBox();
	box.Scale(scaleFactor);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXTargetObject::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	int i,nv;
	Matrix3 m;
	GetMatrix(t,inode,vpt,m);
	nv = targMesh.getNumVerts();
	box.Init();
	for (i=0; i<nv; i++) 
		box += m*targMesh.getVert(i);
}

//------------------------------------------------------------------------------------------------------------------------

int NOXTargetObject::HitTest(TimeValue t, INode *inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) 
{
	HitRegion hitRegion;
	DWORD savedLimits;
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();	
	MakeHitRegion(hitRegion,type,crossing,4,p);	
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	GetMatrix(t,inode,vpt,m);
	gw->setTransform(m);
	if(targMesh.select( gw, gw->getMaterial(), &hitRegion, flags & HIT_ABORTONHIT ))
		return TRUE;
	gw->setRndLimits( savedLimits );
	return FALSE;
}

//------------------------------------------------------------------------------------------------------------------------

int NOXTargetObject::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) 
{
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();
	GetMatrix(t,inode,vpt,m);
	gw->setTransform(m);
	DWORD rlim = gw->getRndLimits();
	gw->setRndLimits(GW_WIREFRAME|GW_EDGES_ONLY|GW_BACKCULL);
	if (inode->Selected()) 
		gw->setColor( LINE_COLOR, GetSelColor());
	else 
		if(!inode->IsFrozen() && !inode->Dependent() && inode->GetLookatNode()) 
		{
			const ObjectState& os = inode->GetLookatNode()->EvalWorldState(t);
			Object* ob = os.obj;
			if ( (ob!=NULL) && ( (ob->SuperClassID()==LIGHT_CLASS_ID) ||
								 (ob->SuperClassID()==CAMERA_CLASS_ID) ) )
				gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_OBJ));
			else
				gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_OBJ));
		}
	targMesh.render( gw, gw->getMaterial(), NULL, COMP_ALL);	
    gw->setRndLimits(rlim);
	return(0);
}

//------------------------------------------------------------------------------------------------------------------------

RefResult NOXTargetObject::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message ) 
{
	return(REF_SUCCEED);
}

ObjectState NOXTargetObject::Eval(TimeValue time) 
{
	return ObjectState(this);
}

RefTargetHandle NOXTargetObject::Clone(RemapDir& remap) 
{
	NOXTargetObject* newob = new NOXTargetObject();
	BaseClone(this, newob, remap);
	return(newob);
}

IOResult NOXTargetObject::Save(ISave *isave) 
{
	return IO_OK;
}

IOResult NOXTargetObject::Load(ILoad *iload) 
{
	return IO_OK;
}

//------------------------------------------------------------------------------------------------------------------------

