//#include "raytracer.h"
//#include "log.h"
//#include <time.h>
//#include "myList.h"
//
//// from displacement
//bool samePoint(const Point3d &p1, const Point3d &p2);		
//bool sameNormal(const Vector3d &n1, const Vector3d &n2);
//
////#define NO_UV_DIR_SMOOTH
//#define USE_UV_BLIST
//
//struct UVvertex
//{
//	Point3d pos;
//	Vector3d normal;
//	Vector3d udir;
//	Vector3d vdir;
//	UVvertex() { pos=Point3d(0,0,0); normal=Vector3d(0,1,0); udir=vdir=Vector3d(0,0,0); }
//};

//// -------------------------- BRUTE FORCE VERSION
//
//bool Mesh::evalUVdirs()
//{
//	char buf[256];
//	Raytracer * rtr = Raytracer::getInstance();
//	Scene * sc = rtr->curScenePtr;
//	sprintf_s(buf, 256, "Evaluating UV directions for mesh %s", name);
//	Logger::add(buf);
//
//	DWORD time1 = GetTickCount();
//
//	int ntris = lastTri-firstTri+1;
//	for (int i=0; i<ntris; i++)
//	{
//		int ii = i+firstTri;
//		Triangle * tri = &(sc->triangles[ii]);
//		bool ok = tri->evalUVvectors(tri->dirU1, tri->dirU2, tri->dirU3, tri->dirV1, tri->dirV2, tri->dirV3);
//		if (!ok)
//		{
//			tri->getBadUVdirSmooth(tri->N1, tri->dirU1, tri->dirV1);
//			tri->getBadUVdirSmooth(tri->N2, tri->dirU2, tri->dirV2);
//			tri->getBadUVdirSmooth(tri->N3, tri->dirU3, tri->dirV3);
//		}
//	}
//	
//	DWORD time2 = GetTickCount();
//	sprintf_s(buf, 256, "First stage took %d ms.", (time2-time1));
//	Logger::add(buf);
//
//
//	#ifdef NO_UV_DIR_SMOOTH
//		return true;
//	#endif
//
//	UVvertex * uvdirs = new UVvertex[ntris*3];
//	if (!uvdirs)
//		return false;
//
//	int sofar=0;
//
//	for (int i=0; i<ntris; i++)
//	{
//		int ii = i+firstTri;
//		Triangle * tri = &(sc->triangles[ii]);
//
//		int foundid1 = -1, foundid2 = -1, foundid3 = -1;
//		//for (int j=0; j<sofar; j++)
//		for (int j=sofar-1; j>=0; j--)
//		{
//			if (samePoint(uvdirs[j].pos, tri->V1)   &&   sameNormal(uvdirs[j].normal, tri->N1))
//				foundid1 = j;
//			if (samePoint(uvdirs[j].pos, tri->V2)   &&   sameNormal(uvdirs[j].normal, tri->N2))
//				foundid2 = j;
//			if (samePoint(uvdirs[j].pos, tri->V3)   &&   sameNormal(uvdirs[j].normal, tri->N3))
//				foundid3 = j;
//			if (foundid1>-1  &&  foundid2>-1  &&  foundid3>-1)
//				break;
//		}
//
//		if (foundid1<0)
//		{
//			uvdirs[sofar].normal = tri->N1;
//			uvdirs[sofar].pos = tri->V1;
//			foundid1 = sofar;
//			sofar++;
//		}
//		if (foundid2<0)
//		{
//			uvdirs[sofar].normal = tri->N2;
//			uvdirs[sofar].pos = tri->V2;
//			foundid2 = sofar;
//			sofar++;
//		}
//		if (foundid3<0)
//		{
//			uvdirs[sofar].normal = tri->N3;
//			uvdirs[sofar].pos = tri->V3;
//			foundid3 = sofar;
//			sofar++;
//		}
//		uvdirs[foundid1].udir += tri->dirU1;
//		uvdirs[foundid1].vdir += tri->dirV1;
//		uvdirs[foundid2].udir += tri->dirU2;
//		uvdirs[foundid2].vdir += tri->dirV2;
//		uvdirs[foundid3].udir += tri->dirU3;
//		uvdirs[foundid3].vdir += tri->dirV3;
//	}
//
//	DWORD time3 = GetTickCount();
//	sprintf_s(buf, 256, "Second stage took %d ms.", (time3-time2));
//	Logger::add(buf);
//
//	for (int j=0; j<sofar; j++)
//	{
//		uvdirs[j].udir.normalize();
//		uvdirs[j].vdir.normalize();
//	}
//
//	DWORD time4 = GetTickCount();
//	sprintf_s(buf, 256, "Third stage took %d ms.", (time4-time3));
//	Logger::add(buf);
//
//	for (int i=0; i<ntris; i++)
//	{
//		int ii = i+firstTri;
//		Triangle * tri = &(sc->triangles[ii]);
//
//		int foundid1 = -1, foundid2 = -1, foundid3 = -1;
//		//for (int j=0; j<sofar; j++)
//		for (int j=sofar-1; j>=0; j--)
//		{
//			if (samePoint(uvdirs[j].pos, tri->V1)   &&   sameNormal(uvdirs[j].normal, tri->N1))
//				foundid1 = j;
//			if (samePoint(uvdirs[j].pos, tri->V2)   &&   sameNormal(uvdirs[j].normal, tri->N2))
//				foundid2 = j;
//			if (samePoint(uvdirs[j].pos, tri->V3)   &&   sameNormal(uvdirs[j].normal, tri->N3))
//				foundid3 = j;
//			if (foundid1>-1  &&  foundid2>-1  &&  foundid3>-1)
//				break;
//		}
//		if (foundid1>-1)
//		{
//			tri->dirU1 = uvdirs[foundid1].udir;
//			tri->dirV1 = uvdirs[foundid1].vdir;
//		}
//		if (foundid2>-1)
//		{
//			tri->dirU2 = uvdirs[foundid2].udir;
//			tri->dirV2 = uvdirs[foundid2].vdir;
//		}
//		if (foundid3>-1)
//		{
//			tri->dirU3 = uvdirs[foundid3].udir;
//			tri->dirV3 = uvdirs[foundid3].vdir;
//		}
//	}
//
//	DWORD time5 = GetTickCount();
//	sprintf_s(buf, 256, "Fourth stage took %d ms.", (time5-time4));
//	Logger::add(buf);
//	sprintf_s(buf, 256, "Total time %d ms.", (time5-time1));
//	Logger::add(buf);
//
//	delete [] uvdirs;
//
//	return true;
//}
//
//------------------------------------------------------------------------------------------------------------------