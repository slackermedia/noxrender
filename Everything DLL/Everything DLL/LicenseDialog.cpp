#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

int evalFontHeight(HWND hWnd);
int getControlLines(HWND hWnd);
int getTextLines(HWND hWnd);
bool setLicenseTextInControl(HWND hWnd);

//---------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK LicenseDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBRUSH editBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			if (!editBrush)
				editBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			
			EMButton * embclose = GetEMButtonInstance(GetDlgItem(hWnd, IDC_LICENSE_CLOSE));
			GlobalWindowSettings::colorSchemes.apply(embclose);
			EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_LICENSE_SCROLL));
			GlobalWindowSettings::colorSchemes.apply(emsb);

			HWND hLic = GetDlgItem(hWnd, IDC_LICENSE_TEXT);
			setLicenseTextInControl(hLic);

			int wl = getTextLines(hLic);
			emsb->size_screen = getControlLines(hLic);
			emsb->size_work = wl;
			emsb->workarea_pos = 0;
			emsb->updateSize();
			emsb->updateRegions();
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_LICENSE_CLOSE:
					{
						EndDialog(hWnd, (INT_PTR)NULL);
					}
					break;
				case IDC_LICENSE_SCROLL:
				{
					HWND hLic = GetDlgItem(hWnd, IDC_LICENSE_TEXT);
					EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_LICENSE_SCROLL));
					if (!emsb)
						break;
					if (!hLic)
						break;
					if (wmEvent != SB_POS_CHANGED)
						break;

					int first = (int)SendMessage(hLic, EM_GETFIRSTVISIBLELINE, 0, 0);
					SendMessage(hLic, EM_LINESCROLL, 0, emsb->workarea_pos-first);
					InvalidateRect(hLic, NULL, true);
				}
				break;
				case IDC_LICENSE_TEXT:
				{
					if (wmEvent == EN_VSCROLL)
					{
						HWND hLic = GetDlgItem(hWnd, IDC_LICENSE_TEXT);
						EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_LICENSE_SCROLL));
						int first = (int)SendMessage(hLic, EM_GETFIRSTVISIBLELINE, 0, 0);
						emsb->workarea_pos = first;
						emsb->updateSize();
						emsb->updateRegions();
						InvalidateRect(emsb->hwnd, NULL, false);
					}
				}
				break;
			}
		}
		break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(200,200,200));
				SetBkColor(hdc1, GlobalWindowSettings::colorSchemes.DialogBackground);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//---------------------------------------------------------------------------------------------------------------------

int evalFontHeight(HWND hWnd)
{
	HFONT hFont = (HFONT)SendMessage(hWnd, WM_GETFONT, 0, 0);
	int fontheight = 0;
	if (!hFont)
		hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	LOGFONT lf;
	GetObject(hFont, sizeof(LOGFONT), &lf);
	fontheight = abs(lf.lfHeight);

	int height2 = 0;
	int ohfp = (int)SendMessage(hWnd, EM_CHARFROMPOS, 0, 0);
	int chfp = ohfp;
	int y = 0;
	while (ohfp==chfp  &&  y<500)
	{
		y++;
		chfp = (int)SendMessage(hWnd, EM_CHARFROMPOS, 0, ((y&0xffff)<<16));
	}
	height2 = y;

	fontheight = max(fontheight, height2);
	return fontheight;
}

//---------------------------------------------------------------------------------------------------------------------

int getControlLines(HWND hWnd)
{
	int fontheight = evalFontHeight(hWnd);
	fontheight = max(1, fontheight);
	RECT crect;
	GetClientRect(hWnd, &crect);
	int numLines = crect.bottom/fontheight;
	return numLines;
}

//---------------------------------------------------------------------------------------------------------------------

int getTextLines(HWND hWnd)
{
	return (int)SendMessage(hWnd, EM_GETLINECOUNT, 0, 0);
}

//---------------------------------------------------------------------------------------------------------------------

char * noxlicensetext = "\
\r\n\
Apache License\r\n\
Version 2.0, January 2004\r\n\
http://www.apache.org/licenses\r\n\

TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION\r\n\

1. Definitions.\r\n\

\"License\" shall mean the terms and conditions for use, reproduction, and distribution as defined by Sections 1 through 9 of this document.\r\n\

\"Licensor\" shall mean the copyright owner or entity authorized by the copyright owner that is granting the License.\r\n\

\"Legal Entity\" shall mean the union of the acting entity and all other entities that control, are controlled by, or are under common control with that entity. For the purposes of this definition, "control" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.\r\n\

\"You\" (or \"Your\") shall mean an individual or Legal Entity exercising permissions granted by this License.\r\n\

\"Source\" form shall mean the preferred form for making modifications, including but not limited to software source code, documentation source, and configuration files.\r\n\

\"Object\" form shall mean any form resulting from mechanical transformation or translation of a Source form, including but not limited to compiled object code, generated documentation, and conversions to other media types.\r\n\

\"Work\" shall mean the work of authorship, whether in Source or Object form, made available under the License, as indicated by a copyright notice that is included in or attached to the work (an example is provided in the Appendix below).\r\n\

\"Derivative Works\" shall mean any work, whether in Source or Object form, that is based on (or derived from) the Work and for which the editorial revisions, annotations, elaborations, or other modifications represent, as a whole, an original work of authorship. For the purposes of this License, Derivative Works shall not include works that remain separable from, or merely link (or bind by name) to the interfaces of, the Work and Derivative Works thereof.\r\n\

\"Contribution\" shall mean any work of authorship, including the original version of the Work and any modifications or additions to that Work or Derivative Works thereof, that is intentionally submitted to Licensor for inclusion in the Work by the copyright owner or by an individual or Legal Entity authorized to submit on behalf of the copyright owner. For the purposes of this definition, \"submitted\" means any form of electronic, verbal, or written communication sent to the Licensor or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, the Licensor for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by the copyright owner as \"Not a Contribution.\"\r\n\

\"Contributor\" shall mean Licensor and any individual or Legal Entity on behalf of whom a Contribution has been received by Licensor and subsequently incorporated within the Work.\r\n\

2. Grant of Copyright License. Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare Derivative Works of, publicly display, publicly perform, sublicense, and distribute the Work and such Derivative Works in Source or Object form.\r\n\

3. Grant of Patent License. Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by such Contributor that are necessarily infringed by their Contribution(s) alone or by combination of their Contribution(s) with the Work to which such Contribution(s) was submitted. If You institute patent litigation against any entity (including a cross-claim or counterclaim in a lawsuit) alleging that the Work or a Contribution incorporated within the Work constitutes direct or contributory patent infringement, then any patent licenses granted to You under this License for that Work shall terminate as of the date such litigation is filed.\r\n\

4. Redistribution. You may reproduce and distribute copies of the Work or Derivative Works thereof in any medium, with or without modifications, and in Source or Object form, provided that You meet the following conditions:\r\n\

(a) You must give any other recipients of the Work or Derivative Works a copy of this License; and\r\n\

(b) You must cause any modified files to carry prominent notices stating that You changed the files; and\r\n\

(c) You must retain, in the Source form of any Derivative Works that You distribute, all copyright, patent, trademark, and attribution notices from the Source form of the Work, excluding those notices that do not pertain to any part of the Derivative Works; and\r\n\

(d) If the Work includes a \"NOTICE\" text file as part of its distribution, then any Derivative Works that You distribute must include a readable copy of the attribution notices contained within such NOTICE file, excluding those notices that do not pertain to any part of the Derivative Works, in at least one of the following places: within a NOTICE text file distributed as part of the Derivative Works; within the Source form or documentation, if provided along with the Derivative Works; or, within a display generated by the Derivative Works, if and wherever such third-party notices normally appear. The contents of the NOTICE file are for informational purposes only and do not modify the License. You may add Your own attribution notices within Derivative Works that You distribute, alongside or as an addendum to the NOTICE text from the Work, provided that such additional attribution notices cannot be construed as modifying the License.\r\n\

You may add Your own copyright statement to Your modifications and may provide additional or different license terms and conditions for use, reproduction, or distribution of Your modifications, or for any such Derivative Works as a whole, provided Your use, reproduction, and distribution of the Work otherwise complies with the conditions stated in this License.\r\n\

5. Submission of Contributions. Unless You explicitly state otherwise, any Contribution intentionally submitted for inclusion in the Work by You to the Licensor shall be under the terms and conditions of this License, without any additional terms or conditions.  Notwithstanding the above, nothing herein shall supersede or modify the terms of any separate license agreement you may have executed with Licensor regarding such Contributions.\r\n\

6. Trademarks. This License does not grant permission to use the trade names, trademarks, service marks, or product names of the Licensor, except as required for reasonable and customary use in describing the origin of the Work and reproducing the content of the NOTICE file.\r\n\

7. Disclaimer of Warranty. Unless required by applicable law or agreed to in writing, Licensor provides the Work (and each Contributor provides its Contributions) on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE. You are solely responsible for determining the appropriateness of using or redistributing the Work and assume any risks associated with Your exercise of permissions under this License.\r\n\

8. Limitation of Liability. In no event and under no legal theory, whether in tort (including negligence), contract, or otherwise, unless required by applicable law (such as deliberate and grossly negligent acts) or agreed to in writing, shall any Contributor be liable to You for damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of this License or out of the use or inability to use the Work (including but not limited to damages for loss of goodwill, work stoppage, computer failure or malfunction, or any and all other commercial damages or losses), even if such Contributor has been advised of the possibility of such damages.\r\n\

9. Accepting Warranty or Additional Liability. While redistributing the Work or Derivative Works thereof, You may choose to offer, and charge a fee for, acceptance of support, warranty, indemnity, or other liability obligations and/or rights consistent with this License. However, in accepting such obligations, You may act only on Your own behalf and on Your sole responsibility, not on behalf of any other Contributor, and only if You agree to indemnify, defend, and hold each Contributor harmless for any liability incurred by, or claims asserted against, such Contributor by reason of your accepting any such warranty or additional liability.\r\n\

END OF TERMS AND CONDITIONS\r\n\

APPENDIX: How to apply the Apache License to your work.\r\n\

To apply the Apache License to your work, attach the following boilerplate notice, with the fields enclosed by brackets "[]" replaced with your own identifying information. (Don't include the brackets!)  The text should be enclosed in the appropriate comment syntax for the file format. We also recommend that a file or class name and description of purpose be included on the same "printed page" as the copyright notice for easier identification within third-party archives.\r\n\

Copyright [yyyy] [name of copyright owner]\r\n\

Licensed under the Apache License, Version 2.0 (the \"License\"); you may not use this file except in compliance with the License.  You may obtain a copy of the License at\r\n\

http://www.apache.org/licenses/LICENSE-2.0\r\n\

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.\r\n\
\r\n\
III. Third party licenses\r\n\
\r\n\
	Following third party software was used during creation of NOX Renderer. Some \r\n\
	of their libraries are distributed with NOX Renderer. These are their licenses:\r\n\
\r\n\
	1. LibXML2 is granted on MIT License:\r\n\
\r\n\
		Copyright (C) 1998-2003 Daniel Veillard.\r\n\
\r\n\
		Permission is hereby granted, free of charge, to any person obtaining a copy\r\n\
		of this software and associated documentation files (the \"Software\"), to deal\r\n\
		in the Software without restriction, including without limitation the rights\r\n\
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\r\n\
		copies of the Software, and to permit persons to whom the Software is\r\n\
		furnished to do so, subject to the following conditions:\r\n\
\r\n\
		The above copyright notice and this permission notice shall be included in\r\n\
		all copies or substantial portions of the Software.\r\n\
\r\n\
		THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\r\n\
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\r\n\
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\r\n\
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\r\n\
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\r\n\
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN\r\n\
		THE SOFTWARE.\r\n\
\r\n\
\r\n\
	2. libCurl is granted on MIT Lincense:\r\n\
\r\n\
		Copyright (c) 1996 - 2011, Daniel Stenberg, <daniel@haxx.se>.\r\n\
\r\n\
		All rights reserved.\r\n\
\r\n\
		Permission to use, copy, modify, and distribute this software for any purpose\r\n\
		with or without fee is hereby granted, provided that the above copyright\r\n\
		notice and this permission notice appear in all copies.\r\n\
\r\n\
		THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\r\n\
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\r\n\
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN\r\n\
		NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,\r\n\
		DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR\r\n\
		OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE\r\n\
		OR OTHER DEALINGS IN THE SOFTWARE.\r\n\
\r\n\
		Except as contained in this notice, the name of a copyright holder shall not\r\n\
		be used in advertising or otherwise to promote the sale, use or other dealings\r\n\
		in this Software without prior written authorization of the copyright holder.\r\n\
\r\n\
\r\n\
	3. 7-zip is granted on LGPL license:\r\n\
\r\n\
		You can find it at http://www.7-zip.org/\r\n\
\r\n\
		7-Zip Copyright (C) 1999-2009 Igor Pavlov.\r\n\
\r\n\
		This library is free software; you can redistribute it and/or\r\n\
		modify it under the terms of the GNU Lesser General Public\r\n\
		License as published by the Free Software Foundation; either\r\n\
		version 2.1 of the License, or (at your option) any later version.\r\n\
\r\n\
		This library is distributed in the hope that it will be useful,\r\n\
		but WITHOUT ANY WARRANTY; without even the implied warranty of\r\n\
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\r\n\
		Lesser General Public License for more details.\r\n\
\r\n\
		You should have received a copy of the GNU Lesser General Public\r\n\
		License along with this library; if not, write to the Free Software\r\n\
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA\r\n\
\r\n\
\r\n\
	4. OpenEXR is granted on BSD license:\r\n\
\r\n\
		Redistribution and use in source and binary forms, with or without modification, \r\n\
		are permitted provided that the following conditions are met: \r\n\
		* Redistributions of source code must retain the above copyright notice, this \r\n\
		  list of conditions and the following disclaimer. \r\n\
		* Redistributions in binary form must reproduce the above copyright notice, \r\n\
		  this list of conditions and the following disclaimer in the documentation \r\n\
		  and/or other materials provided with the distribution. \r\n\
		* Neither the name of Industrial Light & Magic nor the names of its contributors \r\n\
		  may be used to endorse or promote products derived from this software without \r\n\
		  specific prior written permission.\r\n\
\r\n\
		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" \r\n\
		AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED \r\n\
		WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. \r\n\
		IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, \r\n\
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, \r\n\
		BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, \r\n\
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY \r\n\
		OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE \r\n\
		OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED \r\n\
		OF THE POSSIBILITY OF SUCH DAMAGE.\r\n\
\r\n\
\r\n\
	5. 3ds Max SDK\r\n\
\r\n\
		This software contains copyrighted code owned by Autodesk but has been modified \r\n\
		and is not endorsed by Autodesk in its modified form.\r\n\
\r\n\
\r\n\
	6. InnoSetup \r\n\
\r\n\
		Except where otherwise noted, all of the documentation and software included\r\n\
		in the Inno Setup package is copyrighted by Jordan Russell.\r\n\
\r\n\
		Copyright (C) 1997-2010 Jordan Russell. All rights reserved.\r\n\
		Portions Copyright (C) 2000-2010 Martijn Laan. All rights reserved.\r\n\
\r\n\
		This software is provided \"as-is,\" without any express or implied warranty.\r\n\
		In no event shall the author be held liable for any damages arising from the\r\n\
		use of this software.\r\n\
\r\n\
		Permission is granted to anyone to use this software for any purpose,\r\n\
		including commercial applications, and to alter and redistribute it,\r\n\
		provided that the following conditions are met:\r\n\
\r\n\
		1. All redistributions of source code files must retain all copyright\r\n\
		   notices that are currently in place, and this list of conditions without\r\n\
		   modification.\r\n\
\r\n\
		2. All redistributions in binary form must retain all occurrences of the\r\n\
		   above copyright notice and web site addresses that are currently in\r\n\
		   place (for example, in the About boxes).\r\n\
\r\n\
		3. The origin of this software must not be misrepresented; you must not\r\n\
		   claim that you wrote the original software. If you use this software to\r\n\
		   distribute a product, an acknowledgment in the product documentation\r\n\
		   would be appreciated but is not required.\r\n\
\r\n\
		4. Modified versions in source or binary form must be plainly marked as\r\n\
		   such, and must not be misrepresented as being the original software.\r\n\
\r\n\
\r\n\
		Jordan Russell\r\n\
		jr-2010 AT jrsoftware.org\r\n\
		http://www.jrsoftware.org/\r\n\
\r\n\
	7. InnoTool is granted on BSD License:\r\n\
\r\n\
		Copyright (c) 2009, Bjornar Henden\r\n\
		All rights reserved.\r\n\
\r\n\
		Redistribution and use in source and binary forms, with or without modification, \r\n\
		are permitted provided that the following conditions are met:\r\n\
		* Redistributions of source code must retain the above copyright notice, this \r\n\
		  list of conditions and the following disclaimer.\r\n\
		* Redistributions in binary form must reproduce the above copyright notice, \r\n\
		  this list of conditions and the following disclaimer in the documentation \r\n\
		  and/or other materials provided with the distribution.\r\n\
		* The name of the copyright holders and contributors may not be used to endorse \r\n\
		  or promote products derived from this software without specific prior \r\n\
		  written permission.\r\n\
\r\n\
		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" \r\n\
		AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED\r\n\
		WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. \r\n\
		IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, \r\n\
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, \r\n\
		BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, \r\n\
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY \r\n\
		OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE \r\n\
		OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED \r\n\
		OF THE POSSIBILITY OF SUCH DAMAGE.\r\n\
\r\n\
	8. Embree is granted on Apache 2.0 License\r\n\
		\r\n\
		Copyright 2009-2013 Intel Corporation\r\n\
		\r\n\
		Licensed under the Apache License, Version 2.0 (the \"License\");\r\n\
		you may not use this file except in compliance with the License.\r\n\
		You may obtain a copy of the License at\r\n\
		\r\n\
		http://www.apache.org/licenses/LICENSE-2.0\r\n\
		\r\n\
		Unless required by applicable law or agreed to in writing, software\r\n\
		distributed under the License is distributed on an \"AS IS\" BASIS,\r\n\
		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\n\
		See the License for the specific language governing permissions and\r\n\
		limitations under the License.\r\n\
		\r\n\
";

//---------------------------------------------------------------------------------------------------------------------

bool setLicenseTextInControl(HWND hWnd)
{
	CHECK(hWnd);

	int ok = SetWindowText(hWnd, noxlicensetext);
	if (!ok)
		return false;


	return true;
}

//---------------------------------------------------------------------------------------------------------------------
