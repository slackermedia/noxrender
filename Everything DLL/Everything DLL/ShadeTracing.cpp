#define _CRT_RAND_S
#include "PathTracing.h"
#include "embree.h"

int ShadeTracer::shade_type = ShadeTracer::ST_SMOOTH;

//-----------------------------------------------------------------------------------------------------

ShadeTracer::ShadeTracer(SceneStatic * scs, Camera * camera, bool * running)
{
	ss = scs;
	cam = camera;
	working = running;
	threadID = 0;
}

//-----------------------------------------------------------------------------------------------------

ShadeTracer::~ShadeTracer()
{
}

//-----------------------------------------------------------------------------------------------------

void ShadeTracer::renderingLoop()
{
	if (!ss  ||  !cam  ||  !working)
		return;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	int randomType = scene->sscene.random_type;

	float cmpl = 1.0f / cam->mplApSh;
	bool finished = false;

	Marsaglia mm;

	while (*working  &&  !finished)
	{
		Point3d origin = cam->position;
		float dX, dY;
		cam->randomNewCoords(dX, dY, randomType, finished, true);
		Vector3d direction = cam->getDirection(dX,dY, origin, 0);

		float bigf = BIGFLOAT;
		int itri = -1;
		float u,v;
		Matrix4d inst_mat;
		int instanceID = 0;

		ss->perfCounter++;
		
		float d;
		if (rtr->use_embree)
			d = intersectEmbree(scene, origin, direction, inst_mat, bigf, instanceID, itri, u,v);
		else
			d = rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(0.0f, origin, direction, inst_mat, bigf, instanceID, itri, u,v);
		if (d<=0.0f)
		{
			cam->blendBuffDirect[0].addPixelColor((int)dX, (int)dY, Color4(0,0,0), true);
			continue;
		}

		direction.normalize();

		Triangle * tri = &(scene->triangles[itri]);
		Vector3d normal;
		if (shade_type==ST_GEOM   ||   shade_type==ST_NORMAL_GEOM)
			normal = inst_mat.getMatrixForNormals() * tri->normal_geom;
		else
			normal = inst_mat.getMatrixForNormals() * tri->evalNormal(u, v);
		normal.normalize();


		if (shade_type==ST_NORMAL   ||   shade_type==ST_NORMAL_GEOM)
		{
			Vector3d nvec;
			nvec.z = (-(cam->direction * normal));
			nvec.x = ( (cam->rightDir * normal));
			nvec.y = ( (cam->upDir * normal));
			nvec.normalize();
			Color4 c = Color4(nvec.x/2+0.5f, nvec.y/2+0.5f, nvec.z/2+0.5f);
			c *= cmpl;
			cam->blendBuffDirect[0].addPixelColor((int)dX, (int)dY, c, true);
			continue;
		}

		Color4 c = Color4(1,1,1);
		c *= max(0, -normal*direction)*cmpl;
		cam->blendBuffDirect[0].addPixelColor((int)dX, (int)dY, c, true);	// WARNING: only first layer
	}
}

//-----------------------------------------------------------------------------------------------------
