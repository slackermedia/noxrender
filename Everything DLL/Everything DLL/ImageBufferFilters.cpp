#include "Colors.h"
#include "EMControls.h"
#include "log.h"
#include <math.h>


//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyFilter(int type, int w, int h)
{
	ImageBuffer * imb = NULL;

	switch (type)
	{
		case 1: imb = applyBoxFilter(w,h); break;
		case 2: imb = applyBox2Filter(w,h); break;
		case 3: imb = applyAAWeightedFilter(w,h); break;
		case 4: imb = applyMitchellNetravaliFilter(w,h); break;
		case 5: imb = applyCatmullRomFilter(w,h); break;
		case 6: imb = applyBSplineFilter(w,h); break;
		case 7: imb = applyGaussFilter(w,h); break;
		case 8: imb = applyGaussExtremeFilter(w,h); break;
		default: imb = applyNoAAFilter(w,h); break;
	}

	return imb;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyNoAAFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			int x2 = (int)((x+0.5f)*zx);
			int y2 = (int)((y+0.5f)*zy);
			int h1 = hitsBuf[y2][x2];
			float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
			res->imgBuf[y][x] = imgBuf[y2][x2] * hf;
			res->hitsBuf[y][x] = 1;
		}
	}
	return res;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyBoxFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;

	float faa = max(zx,zy);
	int iaa = (int)faa;
	if (iaa < faa)
		iaa++;

	float mm = 1.0f/9.0f;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			Color4 r = Color4(0,0,0);
			int x2 = (int)((x+0.5f)*zx);
			int y2 = (int)((y+0.5f)*zy);
			for (int j=-1; j<=1; j++)
			{
				int y1 = max(0, min(y2+j, height-1));
				for (int i=-1; i<=1; i++)
				{
					int x1 = max(0, min(x2+i, width-1));

					int h1 = hitsBuf[y1][x1];
					float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
					r += imgBuf[y1][x1] * hf;
				}
			}

			res->imgBuf[y][x] = r * mm;
			res->hitsBuf[y][x] = 1;
		}
	}
	return res;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyBox2Filter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;

	if (fabs(zx-2.0f)<0.01f   &&   fabs(zy-2.0f)<0.01f)
	{	// AA x2
		float mm = 1.0f / 4.0f;
		for (int y=0; y<h; y++)
		{
			for (int x=0; x<w; x++)
			{
				Color4 r = Color4(0,0,0);
				for (int j=0; j<2; j++)
				{
					int y1 = max(0, min(y*2+j, height-1));
					for (int i=0; i<2; i++)
					{
						int x1 = max(0, min(x*2+i, width-1));

						int h1 = hitsBuf[y1][x1];
						float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
						r += imgBuf[y1][x1] * hf;
					}
				}
				res->imgBuf[y][x] = r * mm;
				res->hitsBuf[y][x] = 1;
			}
		}
	}
	else
	{
		if (fabs(zx-3.0f)<0.01f   &&   fabs(zy-3.0f)<0.01f)
		{	// AA x3
			float mm = 1.0f / 9.0f;
			for (int y=0; y<h; y++)
			{
				for (int x=0; x<w; x++)
				{
					Color4 r = Color4(0,0,0);
					for (int j=0; j<3; j++)
					{
						int y1 = max(0, min(y*3+j, height-1));
						for (int i=0; i<3; i++)
						{
							int x1 = max(0, min(x*3+i, width-1));

							int h1 = hitsBuf[y1][x1];
							float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
							r += imgBuf[y1][x1] * hf;
						}
					}
					res->imgBuf[y][x] = r * mm;
					res->hitsBuf[y][x] = 1;
				}
			}
		}
		else
		{	// not regular 
			for (int y=0; y<h; y++)
			{
				for (int x=0; x<w; x++)
				{
					int y1 = max(0, min((int)(y*zy), height-1));
					int x1 = max(0, min((int)(x*zx), width-1));

					int h1 = hitsBuf[y1][x1];
					float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
					Color4 r = imgBuf[y1][x1] * hf;

					res->imgBuf[y][x] = r;
					res->hitsBuf[y][x] = 1;
				}
			}
		}
	}

	return res;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyAAWeightedFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;

	Raytracer * rtr = Raytracer::getInstance();
	float iso = rtr->curScenePtr->getActiveCamera()->iMod.getMultiplier();

	float faa = max(zx,zy);
	int iaa = (int)faa;
	if (iaa < faa)
		iaa++;

	if (iaa == 2)
	{
		for (int y=0; y<h; y++)
		{
			for (int x=0; x<w; x++)
			{
				Color4 r = Color4(0,0,0);
				int x2 = (int)((x+0.5f)*zx);
				int y2 = (int)((y+0.5f)*zy);

				float weights[4];
				Color4 colors[4];
				for (int j=0; j<=1; j++)
				{
					int y1 = max(0, min(y2+j, height-1));
					for (int i=0; i<=1; i++)
					{
						int x1 = max(0, min(x2+i, width-1));
						int h1 = hitsBuf[y1][x1];
						float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
						colors[(j)*2 + (i)] = imgBuf[y1][x1] * hf;
					}
				}
				float sumweights = 0;
				for (int i=0; i<4; i++)
				{
					weights[i] = 1.0f/(colors[i].r*iso +0.1f) + 1.0f/(colors[i].g*iso +0.1f) + 1.0f/(colors[i].b*iso +0.1f);
					sumweights += weights[i];
					r += colors[i] * weights[i];
				}

				r *= 1.0f/sumweights;
				res->imgBuf[y][x] = r ;
				res->hitsBuf[y][x] = 1;
			}
		}
	}
	else
	{
		for (int y=0; y<h; y++)
		{
			for (int x=0; x<w; x++)
			{
				Color4 r = Color4(0,0,0);
				int x2 = (int)((x+0.5f)*zx);
				int y2 = (int)((y+0.5f)*zy);

				float weights[9];
				Color4 colors[9];
				for (int j=-1; j<=1; j++)
				{
					int y1 = max(0, min(y2+j, height-1));
					for (int i=-1; i<=1; i++)
					{
						int x1 = max(0, min(x2+i, width-1));
						int h1 = hitsBuf[y1][x1];
						float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
						colors[(j+1)*3 + (i+1)] = imgBuf[y1][x1] * hf;
					}
				}

				float sumweights = 0;
				for (int i=0; i<9; i++)
				{
					weights[i] = 1.0f/(colors[i].r*iso +0.1f) + 1.0f/(colors[i].g*iso +0.1f) + 1.0f/(colors[i].b*iso +0.1f);
					sumweights += weights[i];
					r += colors[i] * weights[i];
				}

				r *= 1.0f/sumweights;

				res->imgBuf[y][x] = r ;
				res->hitsBuf[y][x] = 1;
			}
		}
	}
	return res;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyMitchellNetravaliFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;
	float rx = 0.5f*zx;
	float ry = 0.5f*zy;
	rx = rx - (int)rx;
	ry = ry - (int)ry;

	float mx[4];
	float my[4];
	float t;
	t = 2 - fabs(-2+rx);
	mx[0] = (5*t*t*t - 3*t*t)/18.0f;
	t = 1 - fabs(-1+rx);
	mx[1] = (-15*t*t*t + 18*t*t + 9*t + 2)/18.0f;
	t = 1 - fabs(rx);
	mx[2] = (-15*t*t*t + 18*t*t + 9*t + 2)/18.0f;
	t = 2 -  fabs(1+rx);
	mx[3] = (5*t*t*t - 3*t*t)/18.0f;
	t = 2 - fabs(-2+ry);
	my[0] = (5*t*t*t - 3*t*t)/18.0f;
	t = 1 - fabs(-1+ry);
	my[1] = (-15*t*t*t + 18*t*t + 9*t + 2)/18.0f;
	t = 1 - fabs(ry);
	my[2] = (-15*t*t*t + 18*t*t + 9*t + 2)/18.0f;
	t = 2 -  fabs(1+ry);
	my[3] = (5*t*t*t - 3*t*t)/18.0f;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			Color4 r = Color4(0,0,0);
			int x2 = (int)((x+0.5f)*zx);
			int y2 = (int)((y+0.5f)*zy);
			for (int j=-2; j<=1; j++)
			{
				int y1 = max(0, min(y2+j, height-1));
				for (int i=-2; i<=1; i++)
				{
					int x1 = max(0, min(x2+i, width-1));

					int h1 = hitsBuf[y1][x1];
					float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
					r += imgBuf[y1][x1] * hf * mx[i+2] * my[j+2];
				}
			}
			res->imgBuf[y][x] = r;
			res->hitsBuf[y][x] = 1;
		}
	}
	return res;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyCatmullRomFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;
	float rx = 0.5f*zx;
	float ry = 0.5f*zy;
	rx = rx - (int)rx;
	ry = ry - (int)ry;

	float faa = max(zx,zy);
	int iaa = (int)faa;
	if (iaa < faa)
		iaa++;

	float mx[4];
	float my[4];
	float t;
	t = 2 - fabs(-2+rx);
	mx[0] = (t*t*t - t*t)/2.0f;
	t = 1 - fabs(-1+rx);
	mx[1] = (-3*t*t*t + 4*t*t + t)/2.0f;
	t = 1 - fabs(rx);
	mx[2] = (-3*t*t*t + 4*t*t + t)/2.0f;
	t = 2 -  fabs(1+rx);
	mx[3] = (t*t*t - t*t)/2.0f;
	t = 2 - fabs(-2+ry);
	my[0] = (t*t*t - t*t)/2.0f;
	t = 1 - fabs(-1+ry);
	my[1] = (-3*t*t*t + 4*t*t + t)/2.0f;
	t = 1 - fabs(ry);
	my[2] = (-3*t*t*t + 4*t*t + t)/2.0f;
	t = 2 -  fabs(1+ry);
	my[3] = (t*t*t - t*t)/2.0f;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			Color4 r = Color4(0,0,0);
			int x2 = (int)((x+0.5f)*zx);
			int y2 = (int)((y+0.5f)*zy);
			for (int j=-2; j<=1; j++)
			{
				int y1 = max(0, min(y2+j, height-1));
				for (int i=-2; i<=1; i++)
				{
					int x1 = max(0, min(x2+i, width-1));

					int h1 = hitsBuf[y1][x1];
					float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
					r += imgBuf[y1][x1] * (hf * mx[i+2] * my[j+2]);
				}
			}
			res->imgBuf[y][x] = r;
			res->hitsBuf[y][x] = 1;
		}
	}
	return res;
	return NULL;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyBSplineFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;
	float rx = 0.5f*zx;
	float ry = 0.5f*zy;
	rx = rx - (int)rx;
	ry = ry - (int)ry;

	float faa = max(zx,zy);
	int iaa = (int)faa;
	if (iaa < faa)
		iaa++;

	float mx[4];
	float my[4];
	float t;
	t = 2 - fabs(-2+rx);
	mx[0] = t*t*t/6.0f;
	t = 1 - fabs(-1+rx);
	mx[1] = (-3*t*t*t + 3*t*t + 3*t + 1)/6.0f;
	t = 1 - fabs(rx);
	mx[2] = (-3*t*t*t + 3*t*t + 3*t + 1)/6.0f;
	t = 2 -  fabs(1+rx);
	mx[3] = t*t*t/6.0f;
	t = 2 - fabs(-2+ry);
	my[0] = t*t*t/6.0f;
	t = 1 - fabs(-1+ry);
	my[1] = (-3*t*t*t + 3*t*t + 3*t + 1)/6.0f;
	t = 1 - fabs(ry);
	my[2] = (-3*t*t*t + 3*t*t + 3*t + 1)/6.0f;
	t = 2 -  fabs(1+ry);
	my[3] = t*t*t/6.0f;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			Color4 r = Color4(0,0,0);
			int x2 = (int)((x+0.5f)*zx);
			int y2 = (int)((y+0.5f)*zy);
			for (int j=-2; j<=1; j++)
			{
				int y1 = max(0, min(y2+j, height-1));
				for (int i=-2; i<=1; i++)
				{
					int x1 = max(0, min(x2+i, width-1));

					int h1 = hitsBuf[y1][x1];
					float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
					r += imgBuf[y1][x1] * hf * mx[i+2] * my[j+2];
				}
			}
			res->imgBuf[y][x] = r;
			res->hitsBuf[y][x] = 1;
		}
	}
	return res;
	return NULL;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyGaussFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;
	float rx = 0.5f*zx;
	float ry = 0.5f*zy;
	rx = rx - (int)rx;
	ry = ry - (int)ry;

	float faa = max(zx,zy);
	int iaa = (int)faa;
	if (iaa < faa)
		iaa++;

	float mx[4];
	float my[4];
	float t,d;

	float p = 1.0f/sqrt(2.0f*PI);
	t = fabs(-2+rx);
	mx[0] = p * exp(-t*t/2);
	t = fabs(-1+rx);
	mx[1] = p * exp(-t*t/2);
	t = fabs(rx);
	mx[2] = p * exp(-t*t/2);
	t =  fabs(1+rx);
	mx[3] = p * exp(-t*t/2);
	t = fabs(-2+ry);
	my[0] = p * exp(-t*t/2);
	t = fabs(-1+ry);
	my[1] = p * exp(-t*t/2);
	t = fabs(ry);
	my[2] = p * exp(-t*t/2);
	t = fabs(1+ry);
	my[3] = p * exp(-t*t/2);

	d = mx[0] + mx[1] + mx[2] + mx[3];
	d = 1.0f/(d);
	mx[0] *= d;
	mx[1] *= d;
	mx[2] *= d;
	mx[3] *= d;

	d = my[0] + my[1] + my[2] + my[3];
	d = 1.0f/(d);
	my[0] *= d;
	my[1] *= d;
	my[2] *= d;
	my[3] *= d;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			Color4 r = Color4(0,0,0);
			int x2 = (int)((x+0.5f)*zx);
			int y2 = (int)((y+0.5f)*zy);
			for (int j=-2; j<=1; j++)
			{
				int y1 = max(0, min(y2+j, height-1));
				for (int i=-2; i<=1; i++)
				{
					int x1 = max(0, min(x2+i, width-1));

					int h1 = hitsBuf[y1][x1];
					float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
					r += imgBuf[y1][x1] * hf * mx[i+2] * my[j+2];
				}
			}
			res->imgBuf[y][x] = r;
			res->hitsBuf[y][x] = 1;
		}
	}
	return res;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyGaussExtremeFilter(int w, int h)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res->allocBuffer(w, h))
	{
		delete res;
		return NULL;
	}

	float zx = width/(float)w;
	float zy = height/(float)h;
	float rx = 0.5f*zx;
	float ry = 0.5f*zy;
	rx = rx - (int)rx;
	ry = ry - (int)ry;

	float faa = max(zx,zy);
	int iaa = (int)faa;
	if (iaa < faa)
		iaa++;

	float mx[6];
	float my[6];
	float t,d;

	float sigma = 1.5f;
	float sigma2 = 0.5f/(sigma*sigma);
	float p = 1.0f/sqrt(2.0f*PI)/sigma;
	t = fabs(-3+rx);
	mx[0] = p * exp(-t*t*sigma2);
	t = fabs(-2+rx);
	mx[1] = p * exp(-t*t*sigma2);
	t = fabs(-1+rx);
	mx[2] = p * exp(-t*t*sigma2);
	t = fabs(rx);
	mx[3] = p * exp(-t*t*sigma2);
	t = fabs(1+rx);
	mx[4] = p * exp(-t*t*sigma2);
	t = fabs(2+rx);
	mx[5] = p * exp(-t*t*sigma2);
	t = fabs(-3+ry);
	my[0] = p * exp(-t*t*sigma2);
	t = fabs(-2+ry);
	my[1] = p * exp(-t*t*sigma2);
	t = fabs(-1+ry);
	my[2] = p * exp(-t*t*sigma2);
	t = fabs(ry);
	my[3] = p * exp(-t*t*sigma2);
	t = fabs(1+ry);
	my[4] = p * exp(-t*t*sigma2);
	t = fabs(2+ry);
	my[5] = p * exp(-t*t*sigma2);

	d = mx[0] + mx[1] + mx[2] + mx[3] + mx[4] + mx[5];
	d = 1.0f/(d);
	mx[0] *= d;
	mx[1] *= d;
	mx[2] *= d;
	mx[3] *= d;
	mx[4] *= d;
	mx[5] *= d;

	d = my[0] + my[1] + my[2] + my[3] + my[4] + my[5];
	d = 1.0f/(d);
	my[0] *= d;
	my[1] *= d;
	my[2] *= d;
	my[3] *= d;
	my[4] *= d;
	my[5] *= d;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			Color4 r = Color4(0,0,0);
			int x2 = (int)((x+0.5f)*zx);
			int y2 = (int)((y+0.5f)*zy);
			for (int j=-3; j<=2; j++)
			{
				int y1 = max(0, min(y2+j, height-1));
				for (int i=-3; i<=2; i++)
				{
					int x1 = max(0, min(x2+i, width-1));

					int h1 = hitsBuf[y1][x1];
					float hf = (h1>0)  ?  1.0f/h1 : 0.0f;
					r += imgBuf[y1][x1] * hf * mx[i+3] * my[j+3];
				}
			}
			res->imgBuf[y][x] = r;
			res->hitsBuf[y][x] = 1;
		}
	}
	return res;
}

//---------------------------------------------------------------------------------------------------------------

