#ifndef __NOX__IES__
#define __NOX__IES__

#include "DLL.h"
#include <windows.h>
#include <stdio.h>

char * openIesFileDialog(HWND hwnd);

class IesNOX
{
	FILE * file;
public:
	char * filename;
	char * exportedFilename;
	unsigned int exportedArrayID;
	char * errorBuf;
	DECLDIR bool parseFile(char * filename);
	int parseHeader();
	bool parseIES86();
	bool parseIES91();
	bool parseIES95();

	int numLamps;
	float lumensPerLamp;
	float candelaMultiplier;
	int numAnglesV;
	int numAnglesH;
	int photometricType;
	int unitsType;
	float luminaireWidth;
	float luminaireLength;
	float luminaireHeight;
	float ballast;
	float ballastP;
	float inputWatts;
	float shapeVolume;

	float * anglesV;
	float * anglesH;
	float ** values;

	float getEmission(float hAngle, float vAngle);
	float getMaxValue();

	void releaseStuff(bool delErrorLog=true);
	bool isValid();

	bool evalVolume();
	float randomDirectionAngle(float &pdf);
	float getProbability(float angle);

	IesNOX & operator= (const IesNOX & ies);
	IesNOX(const IesNOX & ies);

	IesNOX();
	~IesNOX();
};



#endif

