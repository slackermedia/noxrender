#ifndef __mersenne_twister__h__
#define __mersenne_twister__h__

class MersenneTwister
{
	int index;
	unsigned int MT[624];
	void generate_numbers();

public:
	bool initialize(int seed);


	unsigned int randUInt();
};

#endif
