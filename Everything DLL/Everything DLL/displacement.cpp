#include "raytracer.h"
#include "log.h"

Vector3d findDispNormalOnUV(const Triangle &tri, MaterialNox * mat, const Vector3d &vScale, int numdivs, float triu, float triv);
bool findUVs(const Vector3d &vecU, const Vector3d &vecV, const Vector3d &vecN, const Vector3d &vecK, float &u, float &v);

//---------------------------------------------------------------------

struct CornerTri	
{
	int tID;
	int vID;
	Vector3d normal;
	Vector3d normal_tex;
	float displ;
	MaterialNox * mat;
	CornerTri() { mat=NULL; tID=-1; vID=-1; normal_tex=normal=Vector3d(0,0,0); displ=0.0f; }
};

//---------------------------------------------------------------------

inline void getPointOnStraightLine(const float &t, const Point3d &p1, const Point3d &p2, const Vector3d &n1, const Vector3d &n2, Point3d &pt, Vector3d &nt)
{
	pt.x = p1.x*(1.0f-t)+p2.x*t;
	pt.y = p1.y*(1.0f-t)+p2.y*t;
	pt.z = p1.z*(1.0f-t)+p2.z*t;
	nt.x = n1.x*(1.0f-t)+n2.x*t;
	nt.y = n1.y*(1.0f-t)+n2.y*t;
	nt.z = n1.z*(1.0f-t)+n2.z*t;
	nt.normalize();
}

//---------------------------------------------------------------------

void getPointOnParamLine(const float &t, const Point3d &p1, const Point3d &p2, const Vector3d &n1, const Vector3d &n2, Point3d &pt, Vector3d &nt)
{
	float ax = (n2.x - n1.x) / 6.0f;
	float ay = (n2.y - n1.y) / 6.0f;
	float az = (n2.z - n1.z) / 6.0f;
	float bx = n1.x / 2.0f;
	float by = n1.y / 2.0f;
	float bz = n1.z / 2.0f;
	float cx = p2.x - p1.x - n1.x/3.0f - n2.x/6.0f;
	float cy = p2.y - p1.y - n1.y/3.0f - n2.y/6.0f;
	float cz = p2.z - p1.z - n1.z/3.0f - n2.z/6.0f;
	float dx = p1.x;
	float dy = p1.y;
	float dz = p1.z;

	float t2 = t*t;
	float t3 = t*t*t;
	pt.x = ax*t3 + bx*t2 + cx*t + dx;
	pt.y = ay*t3 + by*t2 + cy*t + dy;
	pt.z = az*t3 + bz*t2 + cz*t + dz;

	nt.x = 6.0f*ax + 2.0f*bx;
	nt.x = 6.0f*ay + 2.0f*by;
	nt.x = 6.0f*az + 2.0f*bz;
	nt.normalize();
}

//---------------------------------------------------------------------

bool Triangle::addDisplacedTris(MaterialNox * mat, MeshInstance * mInst)
{
	if (!mat)
		return false;

	int numDivs = (1<<mat->displ_subdivs_lvl);
	if (numDivs<2 || numDivs>(1<<16))
		return false;

	float dmpl = 1.0f;

	Vector3d scale = Vector3d(1,1,1);
	if (!mat->displ_ignore_scale)
	{
		scale = mInst->matrix_transform.getScaleComponent();
		scale.x = 1.0f/scale.x;   scale.y = 1.0f/scale.y;   scale.z = 1.0f/scale.z;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	Point3d lastPU = V1;
	Point3d lastPV = V1;
	Vector3d lastNU = N1;
	Vector3d lastNV = N1;

	for (int iu=0; iu<numDivs; iu++)
	{
		Point3d PU, PV;
		Vector3d NU, NV;
		float tfuv = (float)(iu+1)/(float)numDivs;
		getPointOnStraightLine(tfuv, V1, V2, N1, N2, PU, NU);
		getPointOnStraightLine(tfuv, V1, V3, N1, N3, PV, NV);

		for (int iw=0; iw<=iu; iw++)
		{
			float tfw1 = (float)iw/(float)max(1, iu);
			float tfw2 = (float)iw/(float)(iu+1);
			float tfw3 = (float)(iw+1)/(float)(iu+1);
			Point3d VV1, VV2, VV3;
			Vector3d NN1, NN2, NN3;
			getPointOnStraightLine(tfw1, lastPU, lastPV, lastNU, lastNV, VV1, NN1);
			getPointOnStraightLine(tfw2, PU, PV, NU, NV, VV2, NN2);
			getPointOnStraightLine(tfw3, PU, PV, NU, NV, VV3, NN3);

			Triangle tri1;
			tri1.V1 = VV1;
			tri1.V2 = VV2;
			tri1.V3 = VV3;
			tri1.N1 = NN1;
			tri1.N2 = NN2;
			tri1.N3 = NN3;
			float tuvuu1 = (iu/(float)numDivs)-(iw/(float)numDivs);
			float tuvuu2 = ((iu+1)/(float)numDivs)-(iw/(float)numDivs);
			float tuvuu3 = (iu/(float)numDivs)-(iw/(float)numDivs);
			float tuvvv1 = (iw/(float)numDivs);
			float tuvvv2 = (iw/(float)numDivs);
			float tuvvv3 = ((iw+1)/(float)numDivs);
			tri1.UV1.x = UV1.x*(1-tuvuu1-tuvvv1) + UV2.x*tuvuu1 + UV3.x*tuvvv1;
			tri1.UV1.y = UV1.y*(1-tuvuu1-tuvvv1) + UV2.y*tuvuu1 + UV3.y*tuvvv1;
			tri1.UV2.x = UV1.x*(1-tuvuu2-tuvvv2) + UV2.x*tuvuu2 + UV3.x*tuvvv2;
			tri1.UV2.y = UV1.y*(1-tuvuu2-tuvvv2) + UV2.y*tuvuu2 + UV3.y*tuvvv2;
			tri1.UV3.x = UV1.x*(1-tuvuu3-tuvvv3) + UV2.x*tuvuu3 + UV3.x*tuvvv3;
			tri1.UV3.y = UV1.y*(1-tuvuu3-tuvvv3) + UV2.y*tuvuu3 + UV3.y*tuvvv3;

			if (tri1.normal_geom * normal_geom < 0) 
				Logger::add("first triangle normal opposite");

			float dsp1 = mat->getDisplacement(tri1.UV1.x, tri1.UV1.y) * mat->displ_depth + mat->displ_shift;
			float dsp2 = mat->getDisplacement(tri1.UV2.x, tri1.UV2.y) * mat->displ_depth + mat->displ_shift;
			float dsp3 = mat->getDisplacement(tri1.UV3.x, tri1.UV3.y) * mat->displ_depth + mat->displ_shift;
			dsp1 *= dmpl;
			dsp2 *= dmpl;
			dsp3 *= dmpl;

			NN1.x=NN1.x*scale.x;	NN1.y=NN1.y*scale.y;	NN1.z=NN1.z*scale.z;	
			NN2.x=NN2.x*scale.x;	NN2.y=NN2.y*scale.y;	NN2.z=NN2.z*scale.z;	
			NN3.x=NN3.x*scale.x;	NN3.y=NN3.y*scale.y;	NN3.z=NN3.z*scale.z;	
			tri1.V1 = tri1.V1 + NN1 * dsp1;
			tri1.V2 = tri1.V2 + NN2 * dsp2;
			tri1.V3 = tri1.V3 + NN3 * dsp3;

			if (mat->displ_normal_mode == MaterialNox::NM_SMOOTH)
			{
				tri1.N1 = findDispNormalOnUV(*this, mat, scale, numDivs, tuvuu1, tuvvv1);
				tri1.N2 = findDispNormalOnUV(*this, mat, scale, numDivs, tuvuu2, tuvvv2);
				tri1.N3 = findDispNormalOnUV(*this, mat, scale, numDivs, tuvuu3, tuvvv3);
			}

			tri1.matInInst = matInInst;
			tri1.area = tri1.evaluateArea();
			tri1.normal_geom = (tri1.V2-tri1.V1) ^ (tri1.V3-tri1.V1);
			tri1.normal_geom.normalize();
			if (mat->displ_normal_mode == MaterialNox::NM_FACE)
				tri1.N1 = tri1.N2 = tri1.N3 = tri1.normal_geom;

			tri1.meshOwn = sc->meshes.objCount-1;

			sc->triangles.add(tri1);

			if (iw<iu)
			{
				float tfw1 = (float)iw/(float)max(1, iu);
				float tfw2 = (float)(iw+1)/(float)(iu+1);
				float tfw3 = (float)(iw+1)/(float)max(1, iu);

				Point3d VV1, VV2, VV3;
				Vector3d NN1, NN2, NN3;
				getPointOnStraightLine(tfw1, lastPU, lastPV, lastNU, lastNV, VV1, NN1);
				getPointOnStraightLine(tfw2, PU, PV, NU, NV, VV2, NN2);
				getPointOnStraightLine(tfw3, lastPU, lastPV, lastNU, lastNV, VV3, NN3);

				Triangle tri2;
				tri2.V1 = VV1;
				tri2.V2 = VV2;
				tri2.V3 = VV3;
				tri2.N1 = NN1;
				tri2.N2 = NN2;
				tri2.N3 = NN3;

				float tuvuu1 = (iu/(float)numDivs)-(iw/(float)numDivs);
				float tuvuu2 = (iu/(float)numDivs)-(iw/(float)numDivs);
				float tuvuu3 = ((iu-1)/(float)numDivs)-(iw/(float)numDivs);
				float tuvvv1 = (iw/(float)numDivs);
				float tuvvv2 = ((iw+1)/(float)numDivs);
				float tuvvv3 = ((iw+1)/(float)numDivs);
				tri2.UV1.x = UV1.x*(1-tuvuu1-tuvvv1) + UV2.x*tuvuu1 + UV3.x*tuvvv1;
				tri2.UV1.y = UV1.y*(1-tuvuu1-tuvvv1) + UV2.y*tuvuu1 + UV3.y*tuvvv1;
				tri2.UV2.x = UV1.x*(1-tuvuu2-tuvvv2) + UV2.x*tuvuu2 + UV3.x*tuvvv2;
				tri2.UV2.y = UV1.y*(1-tuvuu2-tuvvv2) + UV2.y*tuvuu2 + UV3.y*tuvvv2;
				tri2.UV3.x = UV1.x*(1-tuvuu3-tuvvv3) + UV2.x*tuvuu3 + UV3.x*tuvvv3;
				tri2.UV3.y = UV1.y*(1-tuvuu3-tuvvv3) + UV2.y*tuvuu3 + UV3.y*tuvvv3;

				float dsp1 = mat->getDisplacement(tri2.UV1.x, tri2.UV1.y) * mat->displ_depth + mat->displ_shift;
				float dsp2 = mat->getDisplacement(tri2.UV2.x, tri2.UV2.y) * mat->displ_depth + mat->displ_shift;
				float dsp3 = mat->getDisplacement(tri2.UV3.x, tri2.UV3.y) * mat->displ_depth + mat->displ_shift;
				dsp1 *= dmpl;
				dsp2 *= dmpl;
				dsp3 *= dmpl;

				NN1.x=NN1.x*scale.x;	NN1.y=NN1.y*scale.y;	NN1.z=NN1.z*scale.z;	
				NN2.x=NN2.x*scale.x;	NN2.y=NN2.y*scale.y;	NN2.z=NN2.z*scale.z;	
				NN3.x=NN3.x*scale.x;	NN3.y=NN3.y*scale.y;	NN3.z=NN3.z*scale.z;	
				tri2.V1 = tri2.V1 + NN1 * dsp1;
				tri2.V2 = tri2.V2 + NN2 * dsp2;
				tri2.V3 = tri2.V3 + NN3 * dsp3;

				if (mat->displ_normal_mode == MaterialNox::NM_SMOOTH)
				{
					tri2.N1 = findDispNormalOnUV(*this, mat, scale, numDivs, tuvuu1, tuvvv1);
					tri2.N2 = findDispNormalOnUV(*this, mat, scale, numDivs, tuvuu2, tuvvv2);
					tri2.N3 = findDispNormalOnUV(*this, mat, scale, numDivs, tuvuu3, tuvvv3);
				}

				tri2.matInInst = matInInst;
				tri2.area = tri2.evaluateArea();
				tri2.normal_geom = (tri2.V2-tri2.V1) ^ (tri2.V3-tri2.V1);
				tri2.normal_geom.normalize();
				if (tri2.normal_geom * normal_geom < 0) 
					Logger::add("second triangle normal opposite");
				if (mat->displ_normal_mode == MaterialNox::NM_FACE)
					tri2.N1 = tri2.N2 = tri2.N3 = tri2.normal_geom;

				tri2.meshOwn = sc->meshes.objCount-1;

				sc->triangles.add(tri2);
			}
		}
		lastPU = PU;
		lastPV = PV;
		lastNU = NU;
		lastNV = NV;
	}

	return true;
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------

bool samePoint(const Point3d &p1, const Point3d &p2)
{
	if (p1.x != p2.x)
		return false;
	if (p1.y != p2.y)
		return false;
	if (p1.z != p2.z)
		return false;
	return true;
}

//---------------------------------------------------------------------

bool sameNormal(const Vector3d &n1, const Vector3d &n2)
{
	if (n1.x != n2.x)
		return false;
	if (n1.y != n2.y)
		return false;
	if (n1.z != n2.z)
		return false;
	return true;
}

//---------------------------------------------------------------------

bool trisShareEdge(const Triangle & t1, const Triangle & t2, int &nDiffNormal, int &t1v1i, int &t1v2i, int &t2v1i, int &t2v2i)
{
	Point3d t1p[]  = { t1.V1, t1.V2, t1.V3 };
	Point3d t2p[]  = { t2.V1, t2.V2, t2.V3 };
	Vector3d t1n[] = { t1.N1, t1.N2, t1.N3 };
	Vector3d t2n[] = { t2.N1, t2.N2, t2.N3 };
	
	int numSamePoints = 0;
	int numDiffNormals = 0;
	int ids1[3], ids2[3];
	int ii=0;
	for (int i=0; i<3; i++)
	{
		for (int j=0; j<3; j++)
		{
			if (samePoint(t1p[i], t2p[j]))
			{
				numSamePoints++;
				ids1[ii] = i;
				ids2[ii] = j;
				ii++;
				if (!sameNormal(t1n[i], t2n[j]))
					numDiffNormals++;
			}
		}
	}
	if (numSamePoints==2)
	{
		if (ids1[0]==0  &&  ids1[1]==2)
		{
			t1v1i = ids1[0];
			t1v2i = ids1[1];
			t2v1i = ids2[0];
			t2v2i = ids2[1];
		}
		else
		{
			t1v1i = ids1[1];
			t1v2i = ids1[0];
			t2v1i = ids2[1];
			t2v2i = ids2[0];
		}
		nDiffNormal = numDiffNormals;
		return true;	
	}
	return false;
}

//---------------------------------------------------------------------

Vector3d findDispNormalOnUV(const Triangle &tri, MaterialNox * mat, const Vector3d &vScale, int numdivs, float triu, float triv)
{
	Vector3d vecNt = tri.evalNormal(triu, triv);
	vecNt.normalize();

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Texture * tex = sc->texManager.textures[ mat->tex_displacement.managerID ];
	if (!tex)
		return vecNt;
	int ww = tex->bmp.getWidth();
	int hh = tex->bmp.getHeight();

	Vector3d vecU[3], vecV[3];
	Point2d  uv[] = { tri.UV1, tri.UV2, tri.UV3 };
	Vector3d vecUt, vecVt;
	tri.evalUVvectors(vecU[0], vecU[1], vecU[2], vecV[0], vecV[1], vecV[2]);
	vecUt = vecU[0]*(1-triu-triv) + vecU[1]*triu + vecU[2]*triv;
	vecVt = vecV[0]*(1-triu-triv) + vecV[1]*triu + vecV[2]*triv;
	float u,v;
	tri.evalTexUV(triu, triv, u, v);
	float lenVecU = (vecU[0].length() + vecU[1].length() + vecU[2].length())/3;
	float lenVecV = (vecV[0].length() + vecV[1].length() + vecV[2].length())/3;
	float nw1 = sqrt((tri.UV1.x-tri.UV2.x)*(tri.UV1.x-tri.UV2.x) + (tri.UV1.y-tri.UV2.y)*(tri.UV1.y-tri.UV2.y));
	float nw2 = sqrt((tri.UV1.x-tri.UV3.x)*(tri.UV1.x-tri.UV3.x) + (tri.UV1.y-tri.UV3.y)*(tri.UV1.y-tri.UV3.y));
	float nw3 = sqrt((tri.UV3.x-tri.UV2.x)*(tri.UV3.x-tri.UV2.x) + (tri.UV3.y-tri.UV2.y)*(tri.UV3.y-tri.UV2.y));
	float nw = (nw1+nw2+nw3)/3 / numdivs / 10 *20;

	float ddu = sqrt(vecUt.x*vecUt.x+vecUt.y*vecUt.y+vecUt.z*vecUt.z) * nw/ww;
	float ddv = sqrt(vecVt.x*vecVt.x+vecVt.y*vecVt.y+vecVt.z*vecVt.z) * nw/hh;
	float pddu = mat->displ_depth / ddu;
	float pddv = mat->displ_depth / ddv;

	vecUt = (vecNt ^ vecUt) ^ vecNt;
	vecVt = (vecNt ^ vecVt) ^ vecNt;
	vecUt.normalize();
	vecVt.normalize();

	float u1 = u-nw/ww;
	float u2 = u;
	float u3 = u+nw/ww;
	float v1 = v-nw/hh;
	float v2 = v;
	float v3 = v+nw/hh;
	float uu1 = u1-floor(u1);
	float vv1 = v1-floor(v1);
	float uu2 = u2-floor(u2);
	float vv2 = v2-floor(v2);
	float uu3 = u3-floor(u3);
	float vv3 = v3-floor(v3);
	int iu1 = (int)(uu1*ww);
	int iv1 = (int)(vv1*hh);
	int iu2 = (int)(uu2*ww);
	int iv2 = (int)(vv2*hh);
	int iu3 = (int)(uu3*ww);
	int iv3 = (int)(vv3*hh);

	float c11 = mat->getDisplacement(u1, v1);
	float c21 = mat->getDisplacement(u2, v1);
	float c31 = mat->getDisplacement(u3, v1);
	float c12 = mat->getDisplacement(u1, v2);
	float c22 = mat->getDisplacement(u2, v2);
	float c32 = mat->getDisplacement(u3, v2);
	float c13 = mat->getDisplacement(u1, v3);
	float c23 = mat->getDisplacement(u2, v3);
	float c33 = mat->getDisplacement(u3, v3);

	Vector3d nv11, nv12, nv13, nv21, nv23, nv31, nv32, nv33;
	nv11.x = -(c11-c22)*pddu;
	nv11.y = -(c11-c22)*pddv;
	nv11.z = 1.0f;

	nv21.x = 0;
	nv21.y = -(c21-c22)*pddv;
	nv21.z = 1.0f;

	nv31.x = (c31-c22)*pddu;
	nv31.y = -(c31-c22)*pddv;
	nv31.z = 1.0f;

	nv12.x = -(c12-c22)*pddu;
	nv12.y = 0;
	nv12.z = 1.0f;

	nv32.x = (c32-c22)*pddu;
	nv32.y = 0;
	nv32.z = 1.0f;

	nv13.x = -(c13-c22)*pddu;
	nv13.y = (c13-c22)*pddv;
	nv13.z = 1.0f;

	nv23.x = 0;
	nv23.y = (c23-c22)*pddv;
	nv23.z = 1.0f;

	nv33.x = (c33-c22)*pddu;
	nv33.y = (c33-c22)*pddv;
	nv33.z = 1.0f;

	nv11.normalize();
	nv12.normalize();
	nv13.normalize();
	nv21.normalize();
	nv23.normalize();
	nv31.normalize();
	nv32.normalize();
	nv33.normalize();

	Vector3d lnormal = nv11+nv12+nv13+nv21+nv23+nv31+nv32+nv33;
	lnormal.normalize();

	lnormal.x *= -1;
	lnormal.y *= -1;

	Vector3d nnormal = vecNt*lnormal.z + vecUt*lnormal.x + vecVt*lnormal.y; 
	nnormal.normalize();

	return nnormal;
}

//---------------------------------------------------------------------

bool findUVs(const Vector3d &vecU, const Vector3d &vecV, const Vector3d &vecN, const Vector3d &vecK, float &u, float &v)
{
	Matrix3d m(vecU, vecV, vecN, Matrix3d::BY_COLS);
	if (!m.invert())
		return false;
	Vector3d res = m*vecK;
	u = res.x;
	v = res.y;
	return true;
}

//---------------------------------------------------------------------

Vector3d findDispNormalOnEdge(const Triangle &tri, MaterialNox * mat, int numdivs, float t, int tv1i, int tv2i)
{
	Vector3d vecN[] = { tri.N1, tri.N2, tri.N3 };
	Vector3d vecNt = vecN[tv1i]*(1-t) + vecN[tv2i]*t;
	vecNt.normalize();

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (mat->tex_displacement.managerID<0)
		return vecNt;
	Texture * tex = sc->texManager.textures[ mat->tex_displacement.managerID ];
	if (!tex)
		return vecNt;
	int ww = tex->bmp.getWidth();
	int hh = tex->bmp.getHeight();

	Vector3d vecU[3], vecV[3];
	Point2d  uv[] = { tri.UV1, tri.UV2, tri.UV3 };
	Vector3d vecUt, vecVt;
	tri.evalUVvectors(vecU[0], vecU[1], vecU[2], vecV[0], vecV[1], vecV[2]);
	vecUt = vecU[tv1i]*(1-t) + vecU[tv2i]*t;
	vecVt = vecV[tv1i]*(1-t) + vecV[tv2i]*t;
	float u = uv[tv1i].x*(1-t) + uv[tv2i].x*t;
	float v = uv[tv1i].y*(1-t) + uv[tv2i].y*t;
	float lenVecU = (vecU[0].length() + vecU[1].length() + vecU[2].length())/3;
	float lenVecV = (vecV[0].length() + vecV[1].length() + vecV[2].length())/3;
	float nw1 = sqrt((tri.UV1.x-tri.UV2.x)*(tri.UV1.x-tri.UV2.x) + (tri.UV1.y-tri.UV2.y)*(tri.UV1.y-tri.UV2.y));
	float nw2 = sqrt((tri.UV1.x-tri.UV3.x)*(tri.UV1.x-tri.UV3.x) + (tri.UV1.y-tri.UV3.y)*(tri.UV1.y-tri.UV3.y));
	float nw3 = sqrt((tri.UV3.x-tri.UV2.x)*(tri.UV3.x-tri.UV2.x) + (tri.UV3.y-tri.UV2.y)*(tri.UV3.y-tri.UV2.y));
	float nw = (nw1+nw2+nw3)/3 / numdivs / 10 *20;

	float ddu = sqrt(vecUt.x*vecUt.x+vecUt.y*vecUt.y+vecUt.z*vecUt.z) * nw/ww;
	float ddv = sqrt(vecVt.x*vecVt.x+vecVt.y*vecVt.y+vecVt.z*vecVt.z) * nw/hh;
	float pddu = mat->displ_depth / ddu;
	float pddv = mat->displ_depth / ddv;

	vecUt = (vecNt ^ vecUt) ^ vecNt;
	vecVt = (vecNt ^ vecVt) ^ vecNt;
	vecUt.normalize();
	vecVt.normalize();

	float u1 = u-nw/ww;
	float u2 = u;
	float u3 = u+nw/ww;
	float v1 = v-nw/hh;
	float v2 = v;
	float v3 = v+nw/hh;
	float uu1 = u1-floor(u1);
	float vv1 = v1-floor(v1);
	float uu2 = u2-floor(u2);
	float vv2 = v2-floor(v2);
	float uu3 = u3-floor(u3);
	float vv3 = v3-floor(v3);
	int iu1 = (int)(uu1*ww);
	int iv1 = (int)(vv1*hh);
	int iu2 = (int)(uu2*ww);
	int iv2 = (int)(vv2*hh);
	int iu3 = (int)(uu3*ww);
	int iv3 = (int)(vv3*hh);

	float c11 = mat->getDisplacement(u1, v1);
	float c21 = mat->getDisplacement(u2, v1);
	float c31 = mat->getDisplacement(u3, v1);
	float c12 = mat->getDisplacement(u1, v2);
	float c22 = mat->getDisplacement(u2, v2);
	float c32 = mat->getDisplacement(u3, v2);
	float c13 = mat->getDisplacement(u1, v3);
	float c23 = mat->getDisplacement(u2, v3);
	float c33 = mat->getDisplacement(u3, v3);

	Vector3d nv11, nv12, nv13, nv21, nv23, nv31, nv32, nv33;
	nv11.x = -(c11-c22)*pddu;
	nv11.y = -(c11-c22)*pddv;
	nv11.z = 1.0f;

	nv21.x = 0;
	nv21.y = -(c21-c22)*pddv;
	nv21.z = 1.0f;

	nv31.x = (c31-c22)*pddu;
	nv31.y = -(c31-c22)*pddv;
	nv31.z = 1.0f;

	nv12.x = -(c12-c22)*pddu;
	nv12.y = 0;
	nv12.z = 1.0f;

	nv32.x = (c32-c22)*pddu;
	nv32.y = 0;
	nv32.z = 1.0f;

	nv13.x = -(c13-c22)*pddu;
	nv13.y = (c13-c22)*pddv;
	nv13.z = 1.0f;

	nv23.x = 0;
	nv23.y = (c23-c22)*pddv;
	nv23.z = 1.0f;

	nv33.x = (c33-c22)*pddu;
	nv33.y = (c33-c22)*pddv;
	nv33.z = 1.0f;

	nv11.normalize();
	nv12.normalize();
	nv13.normalize();
	nv21.normalize();
	nv23.normalize();
	nv31.normalize();
	nv32.normalize();
	nv33.normalize();

	Vector3d lnormal = nv11+nv12+nv13+nv21+nv23+nv31+nv32+nv33;
	lnormal.normalize();

	lnormal.x *= -1;
	lnormal.y *= -1;

	Vector3d nnormal = vecNt*lnormal.z + vecUt*lnormal.x + vecVt*lnormal.y; 
	nnormal.normalize();

	return nnormal;
}

//---------------------------------------------------------------------

inline bool isUVcontinued(const Point2d &T1UV1, const Point2d &T1UV2, const Point2d &T2UV1, const Point2d &T2UV2)
{
	Point2d tT1UV1, tT1UV2, tT2UV1, tT2UV2, tDiff1, tDiff2;
	tT1UV1.x = T1UV1.x - floor(T1UV1.x);		tT1UV1.y = T1UV1.y - floor(T1UV1.y);
	tT1UV2.x = T1UV2.x - floor(T1UV2.x);		tT1UV2.y = T1UV2.y - floor(T1UV2.y);
	tT2UV1.x = T2UV1.x - floor(T2UV1.x);		tT2UV1.y = T2UV1.y - floor(T2UV1.y);
	tT2UV2.x = T2UV2.x - floor(T2UV2.x);		tT2UV2.y = T2UV2.y - floor(T2UV2.y);
	tDiff1.x = T1UV2.x - T1UV1.x;				tDiff1.y = T1UV2.y - T1UV1.y;
	tDiff2.x = T2UV2.x - T2UV1.x;				tDiff2.y = T2UV2.y - T2UV1.y;

	bool uvcontinue = (tT1UV1.x==tT2UV1.x  &&  tT1UV1.y==tT2UV1.y  &&  tT1UV2.x==tT2UV2.x  &&  tT1UV2.y==tT2UV2.y  &&  tDiff1.x==tDiff2.x  &&  tDiff1.y==tDiff2.y);
	return uvcontinue;
}

//---------------------------------------------------------------------

int fillBetween2TrianglesOrdered(const MeshInstance * mInst, const Triangle &tri1, const Triangle &tri2, MaterialNox * mat1, MaterialNox * mat2, int t1v1i, int t1v2i, int t2v1i, int t2v2i)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	int added_tris = 0;
	Vector3d scale1 = Vector3d(1,1,1), scale2 = Vector3d(1,1,1);
	if (!mat1->displ_ignore_scale)
	{
		scale1 = mInst->matrix_transform.getScaleComponent();
		scale1.x=1.0f/scale1.x;	scale1.y=1.0f/scale1.y;	scale1.z=1.0f/scale1.z;
	}
	if (!mat2->displ_ignore_scale)
	{
		scale2 = mInst->matrix_transform.getScaleComponent();
		scale2.x=1.0f/scale2.x;	scale2.y=1.0f/scale2.y;	scale2.z=1.0f/scale2.z;
	}

	int numdivs1 = (mat1->use_tex_displacement  &&  mat1->tex_displacement.isValid()) ? (1<<mat1->displ_subdivs_lvl) : 1;
	int numdivs2 = (mat2->use_tex_displacement  &&  mat2->tex_displacement.isValid()) ? (1<<mat2->displ_subdivs_lvl) : 1;
	bool same_divs = (numdivs1 == numdivs2);
	if (numdivs1<numdivs2)
		return 0;
	int rpt = numdivs1/numdivs2;
	Point3d ATRI1V[] = { tri1.V1, tri1.V2, tri1.V3 };
	Point3d ATRI2V[] = { tri2.V1, tri2.V2, tri2.V3 };
	Point3d T1V1 = ATRI1V[t1v1i];	Point3d T1V2 = ATRI1V[t1v2i];
	Point3d T2V1 = ATRI2V[t2v1i];	Point3d T2V2 = ATRI2V[t2v2i];
	Vector3d ATRI1N[] = { tri1.N1, tri1.N2, tri1.N3 };
	Vector3d ATRI2N[] = { tri2.N1, tri2.N2, tri2.N3 };
	Vector3d T1N1 = ATRI1N[t1v1i];	Vector3d T1N2 = ATRI1N[t1v2i];
	Vector3d T2N1 = ATRI2N[t2v1i];	Vector3d T2N2 = ATRI2N[t2v2i];
	Point2d ATRI1UV[] = { tri1.UV1, tri1.UV2, tri1.UV3 };
	Point2d ATRI2UV[] = { tri2.UV1, tri2.UV2, tri2.UV3 };
	Point2d T1UV1 = ATRI1UV[t1v1i];	Point2d T1UV2 = ATRI1UV[t1v2i];
	Point2d T2UV1 = ATRI2UV[t2v1i];	Point2d T2UV2 = ATRI2UV[t2v2i];

	bool uvcontinue = isUVcontinued(T1UV1, T1UV2, T2UV1, T2UV2);

	Vector3d vecU1[3], vecV1[3], vecU2[3], vecV2[3];
	tri1.evalUVvectors(vecU1[0], vecU1[1], vecU1[2], vecV1[0], vecV1[1], vecV1[2]);
	tri2.evalUVvectors(vecU2[0], vecU2[1], vecU2[2], vecV2[0], vecV2[1], vecV2[2]);
	Vector3d vecK1 = tri1.normal_geom ^ (T1V2-T1V1);	vecK1.normalize();
	Vector3d vecK2 = tri2.normal_geom ^ (T2V2-T2V1);	vecK2.normalize();
	float unitU1, unitV1, unitU2, unitV2; 
	bool uv1ok = findUVs(vecU1[0], vecV1[0], tri1.normal_geom, vecK1, unitU1, unitV1);
	bool uv2ok = findUVs(vecU2[0], vecV2[0], tri2.normal_geom, vecK2, unitU2, unitV2);

	float dd1 = mat1->displ_depth, dd2 = mat2->displ_depth;
	float dds1 = mat1->displ_shift, dds2 = mat2->displ_shift;

	Point3d   v1_1 = T1V1,    v2_1 = T2V1;
	Vector3d  n1_1 = T1N1,    n2_1 = T2N1;
	Point2d  uv1_1 = T1UV1,  uv2_1 = T2UV1;
	uv1_1.x -= floor(uv1_1.x);		uv1_1.y -= floor(uv1_1.y);
	uv2_1.x -= floor(uv2_1.x);		uv2_1.y -= floor(uv2_1.y);
	n1_1.x*=scale1.x;	n1_1.y*=scale1.y;	n1_1.z*=scale1.z;
	n2_1.x*=scale2.x;	n2_1.y*=scale2.y;	n2_1.z*=scale2.z;

	Vector3d dn1_1 = T1N1;
	Vector3d dn2_1 = T2N1;
	if (mat1->displ_normal_mode == MaterialNox::NM_SMOOTH)
		dn1_1 = findDispNormalOnEdge(tri1, mat1, numdivs1, 0, t1v1i, t1v2i);
	if (mat2->displ_normal_mode == MaterialNox::NM_SMOOTH)
		dn2_1 = findDispNormalOnEdge(tri2, mat2, numdivs1, 0, t2v1i, t2v2i);

	for (int i=0; i<numdivs1; i++)
	{
		Point3d v1_2, v2_2;   Vector3d n1_2, n2_2;   Point2d uv1_2, uv2_2;
		float t2 = (float)(i+1)/(float)numdivs1;

		getPointOnStraightLine(t2, T1V1, T1V2, T1N1, T1N2, v1_2, n1_2);
		getPointOnStraightLine(t2, T2V1, T2V2, T2N1, T2N2, v2_2, n2_2);
		uv1_2.x = T1UV1.x*(1-t2) + T1UV2.x*t2;		uv1_2.y = T1UV1.y*(1-t2) + T1UV2.y*t2;
		uv2_2.x = T2UV1.x*(1-t2) + T2UV2.x*t2;		uv2_2.y = T2UV1.y*(1-t2) + T2UV2.y*t2;

		Vector3d dn1_2 = n1_2;
		Vector3d dn2_2 = n2_2;
		n1_2.x*=scale1.x;	n1_2.y*=scale1.y;	n1_2.z*=scale1.z;
		n2_2.x*=scale2.x;	n2_2.y*=scale2.y;	n2_2.z*=scale2.z;


		if (fabs(uv1_2.x-uv1_1.x)>0.5f)
			uv1_2.x -= floor(uv1_2.x);		
		if (fabs(uv1_2.y-uv1_1.y)>0.5f)
			uv1_2.y -= floor(uv1_2.y);
		if (fabs(uv2_2.x-uv2_1.x)>0.5f)
			uv2_2.x -= floor(uv2_2.x);
		if (fabs(uv2_2.y-uv2_1.y)>0.5f)
			uv2_2.y -= floor(uv2_2.y);

		float dsp1_1 = mat1->getDisplacement(uv1_1.x, uv1_1.y) * dd1 + dds1;
		float dsp1_2 = mat1->getDisplacement(uv1_2.x, uv1_2.y) * dd1 + dds1;
		float dsp2_1 = mat2->getDisplacement(uv2_1.x, uv2_1.y) * dd2 + dds2;
		float dsp2_2 = mat2->getDisplacement(uv2_2.x, uv2_2.y) * dd2 + dds2;

		Point3d dv1_1 = v1_1 + n1_1 * dsp1_1;
		Point3d dv1_2 = v1_2 + n1_2 * dsp1_2;
		Point3d dv2_1 = v2_1 + n2_1 * dsp2_1;
		Point3d dv2_2 = v2_2 + n2_2 * dsp2_2;
		Point3d dv3_1 = dv1_1 + (dv2_1-dv1_1) * 0.5f;
		Point3d dv3_2 = dv1_2 + (dv2_2-dv1_2) * 0.5f;
		
		if (mat1->displ_normal_mode == MaterialNox::NM_SMOOTH)
			dn1_2 = findDispNormalOnEdge(tri1, mat1, numdivs1, t2, t1v1i, t1v2i);
		if (mat2->displ_normal_mode == MaterialNox::NM_SMOOTH)
			dn2_2 = findDispNormalOnEdge(tri2, mat2, numdivs1, t2, t2v1i, t2v2i);
		Vector3d dn3_1 = dn1_1 + dn2_1;
		Vector3d dn3_2 = dn1_2 + dn2_2;
		dn3_1.normalize();
		dn3_2.normalize();


		Point2d uv3_1, uv3_2, uv4_1, uv4_2;
		if (uvcontinue)
		{
			float dist1 = (dv2_1-dv1_1).length()/2;
			float dist2 = (dv2_2-dv1_2).length()/2;
			uv3_1.x = uv1_1.x + unitU1 * -dist1;
			uv3_1.y = uv1_1.y + unitV1 * -dist1;
			uv3_2.x = uv1_2.x + unitU1 * -dist2;
			uv3_2.y = uv1_2.y + unitV1 * -dist2;
			uv4_1.x = uv3_1.x;	uv4_1.y = uv3_1.y;
			uv4_2.x = uv3_2.x;	uv4_2.y = uv3_2.y;
		}
		else
		{
			float dist1 = (dv2_1-dv1_1).length()/2;
			float dist2 = (dv2_2-dv1_2).length()/2;
			uv3_1.x = uv1_1.x + unitU1 * -dist1;
			uv3_1.y = uv1_1.y + unitV1 * -dist1;
			uv3_2.x = uv1_2.x + unitU1 * -dist2;
			uv3_2.y = uv1_2.y + unitV1 * -dist2;
			uv4_1.x = uv2_1.x + unitU2 * -dist1;
			uv4_1.y = uv2_1.y + unitV2 * -dist1;
			uv4_2.x = uv2_2.x + unitU2 * -dist2;
			uv4_2.y = uv2_2.y + unitV2 * -dist2;
		}

		Triangle dtri1, dtri2, dtri3, dtri4;

		dtri1.V1 = dv1_1;	dtri1.UV1 = uv1_1;		dtri1.N1 = dn1_1;
		dtri1.V2 = dv1_2;	dtri1.UV2 = uv1_2;		dtri1.N2 = dn1_2;
		dtri1.V3 = dv3_2;	dtri1.UV3 = uv3_2;		dtri1.N3 = dn3_2;
		dtri1.matInInst = tri1.matInInst;
		dtri1.area = dtri1.evaluateArea();
		dtri1.normal_geom = (dtri1.V2-dtri1.V1) ^ (dtri1.V3-dtri1.V1);
		dtri1.normal_geom.normalize();
		if (mat1->displ_normal_mode == MaterialNox::NM_FACE)
			dtri1.N1 = dtri1.N2 = dtri1.N3 = dtri1.normal_geom; 
		dtri1.meshOwn = sc->meshes.objCount-1;
		sc->triangles.add(dtri1);
		added_tris++;

		dtri2.V1 = dv1_1;	dtri2.UV1 = uv1_1;		dtri2.N1 = dn1_1;
		dtri2.V2 = dv3_2;	dtri2.UV2 = uv3_2;		dtri2.N2 = dn3_2;
		dtri2.V3 = dv3_1;	dtri2.UV3 = uv3_1;		dtri2.N3 = dn3_1;
		dtri2.matInInst = tri1.matInInst;
		dtri2.area = dtri2.evaluateArea();
		dtri2.normal_geom = (dtri2.V2-dtri2.V1) ^ (dtri2.V3-dtri2.V1);
		dtri2.normal_geom.normalize();
		if (mat1->displ_normal_mode == MaterialNox::NM_FACE)
			dtri2.N1 = dtri2.N2 = dtri2.N3 = dtri2.normal_geom; 
		dtri2.meshOwn = sc->meshes.objCount-1;
		sc->triangles.add(dtri2);
		added_tris++;

		if (same_divs)
		{
			dtri3.V1 = dv3_1;	dtri3.UV1 = uv4_1;		dtri3.N1 = dn3_1;
			dtri3.V2 = dv3_2;	dtri3.UV2 = uv4_2;		dtri3.N2 = dn3_2;
			dtri3.V3 = dv2_2;	dtri3.UV3 = uv2_2;		dtri3.N3 = dn2_2;
			dtri3.matInInst = tri2.matInInst;
			dtri3.area = dtri3.evaluateArea();
			dtri3.normal_geom = (dtri3.V2-dtri3.V1) ^ (dtri3.V3-dtri3.V1);
			dtri3.normal_geom.normalize();
			if (mat2->displ_normal_mode == MaterialNox::NM_FACE)
				dtri3.N1 = dtri3.N2 = dtri3.N3 = dtri3.normal_geom; 
			dtri3.meshOwn = sc->meshes.objCount-1;
			sc->triangles.add(dtri3);
			added_tris++;

			dtri4.V1 = dv3_1;	dtri4.UV1 = uv4_1;		dtri4.N1 = dn3_1;
			dtri4.V2 = dv2_2;	dtri4.UV2 = uv2_2;		dtri4.N2 = dn2_2;
			dtri4.V3 = dv2_1;	dtri4.UV3 = uv2_1;		dtri4.N3 = dn2_1;
			dtri4.matInInst = tri2.matInInst;
			dtri4.area = dtri4.evaluateArea();
			dtri4.normal_geom = (dtri4.V2-dtri4.V1) ^ (dtri4.V3-dtri4.V1);
			dtri4.normal_geom.normalize();
			if (mat2->displ_normal_mode == MaterialNox::NM_FACE)
				dtri4.N1 = dtri4.N2 = dtri4.N3 = dtri4.normal_geom; 
			dtri4.meshOwn = sc->meshes.objCount-1;
			sc->triangles.add(dtri4);
			added_tris++;
		}
		else		// different number of divisions
		{
			int j = i/rpt;
			float t3a = (float)(j)/(float)numdivs2;
			float t3b = (float)(j+1)/(float)numdivs2;
			Point3d vv_1, vv_2;
			Vector3d nn_1, nn_2;
			getPointOnStraightLine(t3a, T2V1, T2V2, T2N1, T2N2, vv_1, nn_1);
			getPointOnStraightLine(t3b, T2V1, T2V2, T2N1, T2N2, vv_2, nn_2);
			Point2d uvv_1, uvv_2;
			uvv_1.x = T2UV1.x*(1-t3a) + T2UV2.x*t3a;
			uvv_1.y = T2UV1.y*(1-t3a) + T2UV2.y*t3a;
			uvv_2.x = T2UV1.x*(1-t3b) + T2UV2.x*t3b;
			uvv_2.y = T2UV1.y*(1-t3b) + T2UV2.y*t3b;
			uvv_1.x -= floor(uvv_1.x);		uvv_1.y -= floor(uvv_1.y);
			if (fabs(uvv_2.x-uvv_1.x)>0.5f)
				uvv_2.x -= floor(uvv_2.x);		
			if (fabs(uvv_2.y-uvv_1.y)>0.5f)
				uvv_2.y -= floor(uvv_2.y);

			Vector3d dnn_1 = nn_1;
			Vector3d dnn_2 = nn_2;
			float dsp2_a = mat2->getDisplacement(uvv_1.x, uvv_1.y) * dd2 + dds2;
			float dsp2_b = mat2->getDisplacement(uvv_2.x, uvv_2.y) * dd2 + dds2;
			nn_1.x *= scale2.x;   nn_1.y *= scale2.y;   nn_1.z *= scale2.z;
			nn_2.x *= scale2.x;   nn_2.y *= scale2.y;   nn_2.z *= scale2.z;
			Point3d dv2_a = vv_1 + nn_1 * dsp2_a;
			Point3d dv2_b = vv_2 + nn_2 * dsp2_b;
			if (mat2->displ_normal_mode == MaterialNox::NM_SMOOTH)
			{
				dnn_1 = findDispNormalOnEdge(tri2, mat2, numdivs2, t3a, t2v1i, t2v2i);
				dnn_2 = findDispNormalOnEdge(tri2, mat2, numdivs2, t3b, t2v1i, t2v2i);
			}

			dtri3.V1 = dv3_1;	dtri3.UV1 = uv4_1;		dtri3.N1 = dn3_1;
			dtri3.V2 = dv3_2;	dtri3.UV2 = uv4_2;		dtri3.N2 = dn3_2;
			if ((i%rpt)<rpt/2)
			{
				dtri3.V3 = dv2_a;	dtri3.UV3 = uvv_1;		dtri3.N3 = dnn_1;
			}
			else
			{
				dtri3.V3 = dv2_b;	dtri3.UV3 = uvv_2;		dtri3.N3 = dnn_2;
			}
			dtri3.matInInst = tri2.matInInst;
			dtri3.area = dtri3.evaluateArea();
			dtri3.normal_geom = (dtri3.V2-dtri3.V1) ^ (dtri3.V3-dtri3.V1);
			dtri3.normal_geom.normalize();
			if (mat2->displ_normal_mode == MaterialNox::NM_FACE)
				dtri3.N1 = dtri3.N2 = dtri3.N3 = dtri3.normal_geom;
			dtri3.meshOwn = sc->meshes.objCount-1;
			sc->triangles.add(dtri3);
			added_tris++;


			if ((i%rpt)==(rpt/2))	// big one
			{
				dtri4.V1 = dv3_1;	dtri4.UV1 = uv4_1;		dtri4.N1 = dn3_1;
				dtri4.V2 = dv2_b;	dtri4.UV2 = uvv_2;		dtri4.N2 = dnn_2;
				dtri4.V3 = dv2_a;	dtri4.UV3 = uvv_1;		dtri4.N3 = dnn_1;
				dtri4.matInInst = tri2.matInInst;
				dtri4.area = dtri4.evaluateArea();
				dtri4.normal_geom = (dtri4.V2-dtri4.V1) ^ (dtri4.V3-dtri4.V1);
				dtri4.normal_geom.normalize();
				if (mat2->displ_normal_mode == MaterialNox::NM_FACE)
					dtri4.N1 = dtri4.N2 = dtri4.N3 = dtri4.normal_geom;
				dtri4.meshOwn = sc->meshes.objCount-1;
				sc->triangles.add(dtri4);
				added_tris++;
			}
		}

		v1_1=v1_2;  v2_1=v2_2;  n1_1=n1_2;  n2_1=n2_2;  uv1_1=uv1_2;  uv2_1=uv2_2;  dn1_1=dn1_2;  dn2_1=dn2_2;
	}

	return added_tris;
}

//---------------------------------------------------------------------

int fillBetween2Triangles(const MeshInstance * mInst, const Triangle &tri1, const Triangle &tri2, MaterialNox * mat1, MaterialNox * mat2, int t1v1i, int t1v2i, int t2v1i, int t2v2i)
{
	int numdivs1 = mat1->use_tex_displacement ? (1<<mat1->displ_subdivs_lvl) : 1;
	int numdivs2 = mat2->use_tex_displacement ? (1<<mat2->displ_subdivs_lvl) : 1;
	if (numdivs1>=numdivs2)
		return fillBetween2TrianglesOrdered(mInst, tri1, tri2, mat1, mat2, t1v1i, t1v2i, t2v1i, t2v2i);
	else
		return fillBetween2TrianglesOrdered(mInst, tri2, tri1, mat2, mat1, t2v1i, t2v2i, t1v1i, t1v2i);
	return 0;
}

//---------------------------------------------------------------------

int addCorner(const CornerTri ct1, const CornerTri ct2, const MeshInstance * mInst, Point3d pCenter, Vector3d nCenter, int t1v1i, int t1v2i, int t2v1i, int t2v2i)
{
	int added_tris = 0;
	Scene * sc  = Raytracer::getInstance()->curScenePtr;

	Vector3d scale1 = Vector3d(1,1,1), scale2 = Vector3d(1,1,1);
	if (!ct1.mat->displ_ignore_scale)
	{
		scale1 = mInst->matrix_transform.getScaleComponent();
		scale1.x=1.0f/scale1.x;	scale1.y=1.0f/scale1.y;	scale1.z=1.0f/scale1.z;
	}
	if (!ct2.mat->displ_ignore_scale)
	{
		scale2 = mInst->matrix_transform.getScaleComponent();
		scale2.x=1.0f/scale2.x;	scale2.y=1.0f/scale2.y;	scale2.z=1.0f/scale2.z;
	}

	Triangle tri1 = sc->triangles[ct1.tID];
	Triangle tri2 = sc->triangles[ct2.tID];
	Point3d ATRI1V[] = { tri1.V1, tri1.V2, tri1.V3 };
	Point3d ATRI2V[] = { tri2.V1, tri2.V2, tri2.V3 };
	Point3d T1V1 = ATRI1V[t1v1i];	Point3d T1V2 = ATRI1V[t1v2i];
	Point3d T2V1 = ATRI2V[t2v1i];	Point3d T2V2 = ATRI2V[t2v2i];
	Vector3d ATRI1N[] = { tri1.N1, tri1.N2, tri1.N3 };
	Vector3d ATRI2N[] = { tri2.N1, tri2.N2, tri2.N3 };
	Vector3d T1N1 = ATRI1N[t1v1i];	Vector3d T1N2 = ATRI1N[t1v2i];
	Vector3d T2N1 = ATRI2N[t2v1i];	Vector3d T2N2 = ATRI2N[t2v2i];
	Point2d ATRI1UV[] = { tri1.UV1, tri1.UV2, tri1.UV3 };
	Point2d ATRI2UV[] = { tri2.UV1, tri2.UV2, tri2.UV3 };
	Point2d T1UV1 = ATRI1UV[t1v1i];	Point2d T1UV2 = ATRI1UV[t1v2i];
	Point2d T2UV1 = ATRI2UV[t2v1i];	Point2d T2UV2 = ATRI2UV[t2v2i];

	bool uvcontinue = isUVcontinued(T1UV1, T1UV2, T2UV1, T2UV2);

	Vector3d vecU1[3], vecV1[3], vecU2[3], vecV2[3];
	tri1.evalUVvectors(vecU1[0], vecU1[1], vecU1[2], vecV1[0], vecV1[1], vecV1[2]);
	tri2.evalUVvectors(vecU2[0], vecU2[1], vecU2[2], vecV2[0], vecV2[1], vecV2[2]);
	Vector3d vecK3 = T1V2-T1V1;	vecK3.normalize();
	Vector3d vecK4 = T2V2-T2V1;	vecK4.normalize();
	Vector3d vecK1 = tri1.normal_geom ^ vecK3;	vecK1.normalize();
	Vector3d vecK2 = tri2.normal_geom ^ vecK4;	vecK2.normalize();
	float unitU1, unitV1, unitU2, unitV2, unitU3, unitV3, unitU4, unitV4; 
	bool uv1ok = findUVs(vecU1[t1v1i], vecV1[t1v1i], tri1.normal_geom, vecK1, unitU1, unitV1);
	bool uv2ok = findUVs(vecU2[t2v1i], vecV2[t2v1i], tri2.normal_geom, vecK2, unitU2, unitV2);
	bool uv3ok = findUVs(vecU1[t1v1i], vecV1[t1v1i], tri1.normal_geom, vecK3, unitU3, unitV3);
	bool uv4ok = findUVs(vecU2[t2v1i], vecV2[t2v1i], tri2.normal_geom, vecK4, unitU4, unitV4);

	Vector3d cnormal1 = ct1.normal;
	Vector3d cnormal2 = ct2.normal;
	if (ct1.mat->displ_normal_mode == MaterialNox::NM_SMOOTH)
		cnormal1 = ct1.normal_tex;
	if (ct2.mat->displ_normal_mode == MaterialNox::NM_SMOOTH)
		cnormal2 = ct2.normal_tex;

	Vector3d scnormal1, scnormal2;
	scnormal1 = ct1.normal;		scnormal2 = ct2.normal;
	scnormal1.x *= scale1.x;		scnormal1.y *= scale1.y;		scnormal1.z *= scale1.z;		
	scnormal2.x *= scale2.x;		scnormal2.y *= scale2.y;		scnormal2.z *= scale2.z;		

	Point3d dp1 = T1V1 + scnormal1 * ct1.displ;
	Point3d dp2 = T2V1 + scnormal2 * ct2.displ;
	Point3d dp3 = dp1 + (dp2-dp1)*0.5f;
	Vector3d dnc = cnormal1 + cnormal2;
	dnc.normalize();

	Point2d uvc1, uvc2, uvc3, uvc4;	
	if (uvcontinue)
	{
		float dist1 = (dp2-dp1).length()/2;
		float dist2 = (pCenter-dp3).length();
		uvc1.x = T1UV1.x + unitU1 * dist1;
		uvc1.y = T1UV1.y + unitV1 * dist1;
		uvc2.x = T2UV1.x + unitU1 * dist1;
		uvc2.y = T2UV1.y + unitV1 * dist1;
		uvc3.x = uvc1.x + unitU3 * dist2;
		uvc3.y = uvc1.y + unitV3 * dist2;
		uvc4.x = uvc2.x + unitU3 * dist2;
		uvc4.y = uvc2.y + unitV3 * dist2;
	}
	else
	{
		float dist1 = (dp2-dp1).length()/2;
		float dist2 = (pCenter-dp3).length();
		uvc1.x = T1UV1.x + unitU1 * -dist1;
		uvc1.y = T1UV1.y + unitV1 * -dist1;
		uvc2.x = T2UV1.x + unitU2 * -dist1;
		uvc2.y = T2UV1.y + unitV2 * -dist1;
		uvc3.x = uvc1.x + unitU3 * dist2;
		uvc3.y = uvc1.y + unitV3 * dist2;
		uvc4.x = uvc2.x + unitU4 * dist2;
		uvc4.y = uvc2.y + unitV4 * dist2;
	}


	Triangle dtri1, dtri2;
	dtri1.V1 = dp1;		dtri1.UV1 = T1UV1;			dtri1.N1 = cnormal1;
	dtri1.V2 = dp3;		dtri1.UV2 = uvc1;			dtri1.N2 = dnc;
	dtri1.V3 = pCenter;	dtri1.UV3 = uvc3;			dtri1.N3 = nCenter;
	dtri1.matInInst = tri1.matInInst;
	dtri1.area = dtri1.evaluateArea();
	dtri1.normal_geom = (dtri1.V2-dtri1.V1) ^ (dtri1.V3-dtri1.V1);
	dtri1.normal_geom.normalize();
	dtri1.meshOwn = sc->meshes.objCount-1;


	dtri2.V1 = dp3;		dtri2.UV1 = uvc2;			dtri2.N1 = dnc;
	dtri2.V2 = dp2;		dtri2.UV2 = T2UV1;			dtri2.N2 = cnormal2;
	dtri2.V3 = pCenter;	dtri2.UV3 = uvc4;			dtri2.N3 = nCenter;
	dtri2.matInInst = tri2.matInInst;
	dtri2.area = dtri2.evaluateArea();
	dtri2.normal_geom = (dtri2.V2-dtri2.V1) ^ (dtri2.V3-dtri2.V1);
	dtri2.normal_geom.normalize();
	dtri2.meshOwn = sc->meshes.objCount-1;

	if (t1v1i+1==t1v2i   ||   t1v1i-2==t1v2i)	// invert tri
	{
		Point3d ptemp1 = dtri1.V1;		dtri1.V1=dtri1.V2;		dtri1.V2 = ptemp1;
		Vector3d vtemp1 = dtri1.N1;		dtri1.N1=dtri1.N2;		dtri1.N2 = vtemp1;
		Point2d p2temp1 = dtri1.UV1;	dtri1.UV1=dtri1.UV2;	dtri1.UV2 = p2temp1;
		dtri1.normal_geom.invert();

		Point3d ptemp2 = dtri2.V1;		dtri2.V1=dtri2.V2;		dtri2.V2 = ptemp2;
		Vector3d vtemp2 = dtri2.N1;		dtri2.N1=dtri2.N2;		dtri2.N2 = vtemp2;
		Point2d p2temp2 = dtri2.UV1;	dtri2.UV1=dtri2.UV2;	dtri2.UV2 = p2temp2;
		dtri2.normal_geom.invert();
	}

	if (ct1.mat->displ_normal_mode == MaterialNox::NM_FACE)
		dtri1.N1 = dtri1.N2 = dtri1.N3 = dtri1.normal_geom;
	if (ct2.mat->displ_normal_mode == MaterialNox::NM_FACE)
		dtri2.N1 = dtri2.N2 = dtri2.N3 = dtri2.normal_geom;

	sc->triangles.add(dtri1);
	added_tris++;
	sc->triangles.add(dtri2);
	added_tris++;



	return added_tris;
}

//---------------------------------------------------------------------

int fillCorners(CornerTri * corners, int numverts, const MeshInstance * mInst)
{
	Vector3d centerNormal;
	for (int i=0; i<numverts; i++)
		centerNormal += corners[i].normal;
	centerNormal.normalize();
	int added_total = 0;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc  = rtr->curScenePtr;

	Vector3d scale = mInst->matrix_transform.getScaleComponent();
	scale.x = 1.0f/scale.x;   scale.y = 1.0f/scale.y;   scale.z = 1.0f/scale.z;

	Point3d cpoint, spoint;
	Vector3d cnormal;
	for (int i=0; i<numverts; i++)
	{
		Vector3d scale2 = Vector3d(1,1,1);
		if (!corners[i].mat->displ_ignore_scale)
			scale2 = scale;
		Triangle * tri = &(sc->triangles[corners[i].tID]);
		Vector3d tnormal;
		tnormal.x = corners[i].normal.x*scale2.x;   tnormal.y = corners[i].normal.y*scale2.y;   tnormal.z = corners[i].normal.z*scale2.z;
		Point2d ATRIUV[] = { tri->UV1, tri->UV2, tri->UV3 };
		corners[i].displ = corners[i].mat->getDisplacement(ATRIUV[corners[i].vID].x, ATRIUV[corners[i].vID].y) * corners[i].mat->displ_depth + corners[i].mat->displ_shift;
		int ndiv = 1;
		if (corners[i].mat->displ_depth>0.0f  &&  corners[i].mat->use_tex_displacement)
			ndiv = (1<<(corners[i].mat->displ_subdivs_lvl));
		corners[i].normal_tex = findDispNormalOnEdge(*tri, corners[i].mat, ndiv, 0.0f, corners[i].vID, corners[i].vID);
		Point3d ATRIV[] = { tri->V1, tri->V2, tri->V3 };
		spoint = ATRIV[corners[i].vID];
		Point3d pd = spoint + tnormal * corners[i].displ;
		cpoint = cpoint + pd.toVec();
	}
	cpoint = cpoint * (1.0f/numverts);
	cnormal = cpoint - spoint;
	cnormal.normalize();

	for (int i=0; i<numverts; i++)
	{
		Triangle * tri1 = &(sc->triangles[corners[i].tID]);
		for (int j=i+1; j<numverts; j++)
		{
			Triangle * tri2 = &(sc->triangles[corners[j].tID]);
			if (sameNormal(corners[i].normal, corners[j].normal))
				continue;
			int nd, t1v1i, t1v2i, t2v1i, t2v2i;
			if (!trisShareEdge(*tri1, *tri2, nd, t1v1i, t1v2i, t2v1i, t2v2i))
				continue;
			if (!corners[i].mat->displ_continuity   &&   !corners[j].mat->displ_continuity)
				continue;

			int cadded = 0;
			if (t1v1i==corners[i].vID  &&  t2v1i==corners[j].vID)
				cadded = addCorner(corners[i], corners[j], mInst, cpoint, cnormal, t1v1i, t1v2i, t2v1i, t2v2i);
			else
				cadded = addCorner(corners[i], corners[j], mInst, cpoint, cnormal, t1v2i, t1v1i, t2v2i, t2v1i);
			added_total += cadded;
		}
	}

	return added_total;
}

//---------------------------------------------------------------------

int Scene::fillDisplacementHoles(int instID)
{
	if (instID<0 || instID>=instances.objCount)
		return -1;
	MeshInstance * inst = &(instances[instID]);
	Mesh * meshsrc = &(meshes[inst->displace_orig_ID]);

	int sumtris = 0;

	// ---- EDGES ----
	int itr1 = meshsrc->firstTri;
	int itr2 = meshsrc->lastTri+1;
	for (int i=itr1; i<itr2; i++)
	{
		for (int j=i+1; j<itr2; j++)
		{
			Triangle *tri1 = &(triangles[i]);
			Triangle *tri2 = &(triangles[j]);

			int ndn;
			int t1v1i, t1v2i, t2v1i, t2v2i;
			if (!trisShareEdge(*tri1, *tri2, ndn, t1v1i, t1v2i, t2v1i, t2v2i))
				continue;

			MaterialNox * mat1 = mats[inst->materials[tri1->matInInst]];
			MaterialNox * mat2 = mats[inst->materials[tri2->matInInst]];

			if (!mat1->displ_continuity  &&  !mat2->displ_continuity)
				continue;
			int added = fillBetween2Triangles(inst, *tri1, *tri2, mat1, mat2, t1v1i, t1v2i, t2v1i, t2v2i);
			sumtris += added;
		}
	}

	// ---- CORNERS ----
	CornerTri vnormals[256];
	int nvnorm = 0;
	for (int i=itr1; i<itr2; i++)
	{
		Triangle *tri1 = &(triangles[i]);
		Point3d t1p[]  = { tri1->V1, tri1->V2, tri1->V3 };
		Vector3d t1n[] = { tri1->N1, tri1->N2, tri1->N3 };
		for (int k=0; k<3; k++)
		{
			nvnorm = 0;
			vnormals[0].normal = t1n[k];
			vnormals[0].tID = i;
			vnormals[0].vID = k;
			vnormals[0].displ = 0.0f;
			vnormals[0].mat = mats[inst->materials[tri1->matInInst]];
			nvnorm++;
			for (int j=itr1; j<itr2; j++)
			{
				if (i==j)
					continue;
				Triangle *tri2 = &(triangles[j]);
				Point3d t2p[]  = { tri2->V1, tri2->V2, tri2->V3 };
				Vector3d t2n[] = { tri2->N1, tri2->N2, tri2->N3 };

				bool checkit = false;
				int ll = 0;
				for (int l=0; l<3; l++)
				{
					if (samePoint(t1p[k], t2p[l]))
					{
						checkit = true;
						ll = l;
					}
				}
				if (!checkit)
					continue;
				if (i>j)	// counted before
					break;

				vnormals[nvnorm].normal = t2n[ll];
				vnormals[nvnorm].tID = j;
				vnormals[nvnorm].vID = ll;
				vnormals[nvnorm].displ = 0.0f;
				vnormals[nvnorm].mat = mats[inst->materials[tri2->matInInst]];
				nvnorm++;
			}

			if (nvnorm<3)
				continue;

			int added = fillCorners(vnormals, nvnorm, inst);
			sumtris += added;
		}
	}

	triangles.createArray();

	return sumtris;
}

//---------------------------------------------------------------------
