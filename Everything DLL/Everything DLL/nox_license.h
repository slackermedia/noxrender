// this is license for nox bench

char * nox_license_text = "\
NOX RENDERER LIMITED USE SOFTWARE LICENSE AGREEMENT\r\n\
 \r\n\
I. Software License Agreement\r\n\
\r\n\
        Software License Agreement is an agreement between you and Evermotion s.c. \r\n\
        Installation or any other way (like copying or unpacking) to enable running \r\n\
        NOX Renderer (software) means you have agreed to terms of this license.\r\n\
\r\n\
\r\n\
II. License\r\n\
\r\n\
        You have the right to use this software as long as you agree to following terms \r\n\
        and the licenses of other libraries listed in section III in further part \r\n\
        of this document.\r\n\
\r\n\
        1. You can use images rendered using this software in any way including \r\n\
            commercial purpose.\r\n\
        2. You will not reverse engineer the software.\r\n\
        3. You will not send materials containing textures that you have no copyrights \r\n\
            to the NOX Renderer Material Library.\r\n\
        4. Evermotion s.c. will not take any responsibility for any kind of damage taken \r\n\
            during use of NOX Renderer.\r\n\
        5. This license is regulated and construed in accordance with the Polish law, and \r\n\
            you agree to submit it to the exclusive jurisdiction of the Polish courts.\r\n\
        6. Evermotion s.c. reserves rights to change this license with future versions \r\n\
            of NOX Renderer.\r\n\
\r\n\
\r\n\
III. Third party licenses\r\n\
\r\n\
        Following third party software was used during creation of NOX Renderer. Some \r\n\
        of their libraries are distributed with NOX Renderer. These are their licenses:\r\n\
\r\n\
        1. LibXML2 is granted on MIT License:\r\n\
\r\n\
                Copyright (C) 1998-2003 Daniel Veillard.\r\n\
\r\n\
                Permission is hereby granted, free of charge, to any person obtaining a copy\r\n\
                of this software and associated documentation files (the \"Software\"), to deal\r\n\
                in the Software without restriction, including without limitation the rights\r\n\
                to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\r\n\
                copies of the Software, and to permit persons to whom the Software is\r\n\
                furnished to do so, subject to the following conditions:\r\n\
\r\n\
                The above copyright notice and this permission notice shall be included in\r\n\
                all copies or substantial portions of the Software.\r\n\
\r\n\
                THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY\r\n\
                KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE\r\n\
                WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR\r\n\
                PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS\r\n\
                OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\r\n\
                LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,\r\n\
                ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE\r\n\
                OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\r\n\
\r\n\
\r\n\
        2. libCurl is granted on MIT Lincense:\r\n\
\r\n\
                Copyright (c) 1996 - 2011, Daniel Stenberg, <daniel@haxx.se>.\r\n\
\r\n\
                All rights reserved.\r\n\
\r\n\
                Permission to use, copy, modify, and distribute this software for any purpose\r\n\
                with or without fee is hereby granted, provided that the above copyright\r\n\
                notice and this permission notice appear in all copies.\r\n\
\r\n\
                THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY\r\n\
                KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE\r\n\
                WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR\r\n\
                PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS\r\n\
                OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\r\n\
                LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,\r\n\
                ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE\r\n\
                OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\r\n\
\r\n\
                Except as contained in this notice, the name of a copyright holder shall not\r\n\
                be used in advertising or otherwise to promote the sale, use or other dealings\r\n\
                in this Software without prior written authorization of the copyright holder.\r\n\
\r\n\
\r\n\
        3. OpenEXR is granted on BSD license:\r\n\
\r\n\
                Redistribution and use in source and binary forms, with or without modification, \r\n\
                are permitted provided that the following conditions are met: \r\n\
                * Redistributions of source code must retain the above copyright notice, this \r\n\
                  list of conditions and the following disclaimer. \r\n\
                * Redistributions in binary form must reproduce the above copyright notice, \r\n\
                  this list of conditions and the following disclaimer in the documentation \r\n\
                  and/or other materials provided with the distribution. \r\n\
                * Neither the name of Industrial Light & Magic nor the names of its contributors \r\n\
                  may be used to endorse or promote products derived from this software without \r\n\
                  specific prior written permission.\r\n\
\r\n\
                THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND\r\n\
                CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES,\r\n\
                INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\r\n\
                MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\r\n\
                DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR\r\n\
                CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,\r\n\
                SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, \r\n\
                BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR \r\n\
                SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS \r\n\
                INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,\r\n\
                WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING\r\n\
                NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE \r\n\
                OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH\r\n\
                DAMAGE.\r\n\
";
