#ifndef __EM_CONTROLS_H
#define __EM_CONTROLS_H

// this is header for old gui control, these are still used

#define CN_CAMERA_CHANGED 51999
#define EN_POSITION_CHANGED 51998
#define PV_REGIONS_CHANGED 51997
#define PV_CLICKED 51996
#define PV_DOUBLECLICKED 51995
#define PV_MIDDLECLICKED 51981
#define LV_MOUSEOVER_CHANGED 51994
#define LV_SELECTION_CHANGED 51993
#define LV_DOUBLECLIKED 51992
#define CU_CHANGED 51991
#define PV_CTRL_C 51990
#define CL_RCLICKED 51989
#define ES_RETURN 51988
#define CS_YOUGOT_DRAG 51987
#define CS_IGOT_DRAG 51986
#define PG_CLICKED 51985
#define ES_LOST_FOCUS 51984
#define ES_TAB 51983
#define ES_TAB_SHIFT 51982
#define SB_POS_CHANGED 51980
#define IL_POS_CHANGED 51979
#define IL_SELECTION_CHANGED 51978
#define IL_MOUSEOVER_CHANGED 51977
#define IL_DOUBLECLICK 51976
#define PV_DROPPED_FILE 51975
#define PV_PIXEL_PICKED 51974
#define EMT_TAB_CLOSED 51973
#define EMA_DIST_CHANGED 51972

#include <vector>
#include "Colors.h"
#include "myList.h"
#include "raytracer.h"
#include "regions.h"

using namespace std;

class EMButton;
class EMImgButton;
class EMImgStateButton;
class EMPView;
class EMComboBox;
class EMCheckBox;
class EMEditTrackBar;
class EMEditSpin;
class EMFresnel;
class EMColorPicker;
class EMColorShow;
class EMGroupBar;
class EMCameraView;
class EMEnvironment;
class EMProgressBar;
class EMStatusBar;
class EMTabs;
class EMListView;
class EMText;
class EMCurve;
class EMTemperaturePicker;
class EMLine;
class EMEditSimple;
class EMPages;
class EMScrollBar;
class EMImageList;
class EMAberration;
class EMGraphList;

//----------------------------------------------------------------------
//	EM Button (simple)

void InitEMButton();
LRESULT CALLBACK EMButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMButton * GetEMButtonInstance(HWND hwnd);
void SetEMButtonInstance(HWND hwnd, EMButton *emb);

class EMButton
{
	bool fontWasChanged;
public:
    COLORREF colText;
    COLORREF colBackGnd;
    COLORREF colBackGndSelected;
	COLORREF colBackGndClicked;
	COLORREF colBorderLight;
	COLORREF colBorderDark;
	COLORREF colDisabledBackground;
	COLORREF colDisabledBorderTopLeft;
	COLORREF colDisabledBorderBottomRight;
	COLORREF colDisabledText;
	int captionSize;
	char * caption;
	bool clicked;
	bool selected;
	bool mouseIn;
    HFONT    hFont;
    HWND     hwnd;


	EMButton(HWND hWnd);
	~EMButton();
	bool changeCaption(char * newCaption);
	void setFont(HFONT hNewFont);
};

//----------------------------------------------------------------------
//	EM Image Button

void InitEMImgButton();
LRESULT CALLBACK EMImgButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMImgButton * GetEMImgButtonInstance(HWND hwnd);
void SetEMImgButtonInstance(HWND hwnd, EMImgButton *emb);

class EMImgButton
{
public:
	COLORREF colBorder;
	COLORREF colBorderMouseOver;
	COLORREF colBorderClick;
	COLORREF colBorderDisabled;
	bool showBorder;
	bool clicked;
	bool mouseIn;
	int pNx, pNy, pMx, pMy, pCx, pCy, pDx, pDy;
	HWND hwnd;
	HWND hToolTip;

	HBITMAP bmNormal;
	HBITMAP bmMouseOver;
	HBITMAP bmClicked;
	HBITMAP bmDisabled;
	HBITMAP * bmAll;

	void setToolTip(char * text);
	EMImgButton(HWND hWnd);
	~EMImgButton();
};

//----------------------------------------------------------------------
//	EM Image 2 state Button

void InitEMImgStateButton();
LRESULT CALLBACK EMImgStateButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMImgStateButton * GetEMImgStateButtonInstance(HWND hwnd);
void SetEMImgStateButtonInstance(HWND hwnd, EMImgStateButton *emb);

class EMImgStateButton
{
public:
	COLORREF colBorder;
	COLORREF colBorderSelected;
	bool stateOn;
	bool allowUnclick;
	HWND hwnd;
	HWND hToolTip;
	HBITMAP bmStateOn;
	HBITMAP bmStateOff;
	HBITMAP * bmAll;
	int pNx, pNy, pMx, pMy, pCx, pCy, pDx, pDy;

	void setToolTip(char * text);
	EMImgStateButton(HWND hWnd);
	~EMImgStateButton();
};

//---------------------------------------------------------------------
//	EM Picture View
class EMPView;

void InitEMPView();
LRESULT CALLBACK EMPViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMPView * GetEMPViewInstance(HWND hwnd);
void SetEMPViewInstance(HWND hwnd, EMPView *empv);

class EMPView
{
public:
	COLORREF colBorder;
	COLORREF colBackground;
	float zoom;
	float mx, my; // shift
	bool showBorder;
	ImageBuffer * imgBuff;
	ImageBuffer * otherSource;
	ImageByteBuffer * byteBuff;

	ImageModifier * imgMod;
	void * reserved1;
	void * reserved2;
	float multiplier;
	float gamma;
	float tone_bias;
	bool use_tone_mapping;

	bool useImageOpacity;
	bool opacityToParent;

	HWND hwnd;
	HWND hToolTip;
	char * toolTxt;

	bool drawNice;
	bool nowDrawing;

	bool useGaussianBlur;
	int gaussianBlurR;

	bool dontLetUserChangeZoom;
	bool lmClicked;
	bool rmClicked;
	int mode;
	bool showRegions;
	bool showBuckets;
	static const int MODE_SHOW = 1;
	static const int MODE_REGION_AIRBRUSH = 2;
	static const int MODE_REGION_RECT = 3;
	static const int MODE_PICK = 4;
	static const int MODE_HYPERLINK = 5;
	HBITMAP getRenderHBitmap();


	ImageRegion * rgnPtr;
	int * bucketsCoords;
	int nBuckets;
	char * hyperlink;

	char * lastDroppedFilename;

	int ppx, ppy;	// pixel x / pixel y - set on mouse click in pick mode (control coords)

	unsigned int histogram[768];
	int hs_posx;
	int hs_posy;
	int hs_width;
	int hs_height;
	int hs_margin;
	bool show_histogram;
	


	bool getColor(int x, int y, bool image_modified, Color4 &c, int & imagex, int &imagey, int aa=1);
	void copyBufferToDevice(HWND hwnd, HDC &hdc, float zoom, float &mX, float &mY, COLORREF bgCol);
	void copyByteBufferToDevice(HWND hwnd, HDC &hdc);
	void drawRegionBorders(HDC &hdc);
	void drawAirShadow(HDC &hdc, int x, int y);
	void drawRectShadow(HDC &hdc, int x1, int y1, int x2, int y2);
	void drawHistogram(HDC &hdc);
	void drawBuckets(HDC &hdc);
	/*void drawTimeStamp();*/
	void updateRegionsRect(int x1, int y1, int x2, int y2, bool activate);
	bool createRegions();
	void notifyRegionsChanged();
	void notifyPickPixel(int ctrlX, int ctrlY);
	bool evalControlPosToImagePos(int cx, int cy, float &ix, float &iy);	// return false on out of image coords
	void zoomIn();
	void zoomOut();
	void zoom100();
	void zoomFit();
	void zoomByteFit(bool repaint=true);
	void zoomResFit(int zwidth, int zheight, bool repaint);
	void setToolTip(char * text);
	void delToolTip();
	bool updateHistogram();

	void allowDragFiles();
	void denyDragFiles();

	bool resize(int newWidth, int newHeight, bool increaseForBorder);

	EMPView(HWND hWnd);
	~EMPView();
};

//----------------------------------------------------------------------
//	EM Combo Box

void InitEMComboBox();
LRESULT CALLBACK EMComboBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMComboBox * GetEMComboBoxInstance(HWND hwnd);
void SetEMComboBoxInstance(HWND hwnd, EMComboBox *emcb);

class EMComboBox
{
public:
	HWND hwnd;
	bool opened;
	bool nowOpening;
	bool nowScrolling;
	bool scrollClickedBG;
	bool drawArrow;
	int leftMargin;
	int hght;
	int canShowNumRows;

	int numItems;
	int selected;
	int mouseOverItem;

	myList<char *>items;

	bool showSlider;
	bool canShowSlider;
	int sliderWidth;
	int sliderHeight;
	RECT sliderRect;
	int sliderPos;
	int startDrawing;

    HFONT hFont;
	HRGN hRgnOpened;
	HRGN hRgnClosed;
	COLORREF colBorderTopLeft;
	COLORREF colBorderBottomRight;
	COLORREF colBorderTopLeftClicked;
	COLORREF colBorderBottomRightClicked;
	COLORREF colSelected;
	COLORREF colMouseOver;
	COLORREF colNormal;
	COLORREF colText;
	COLORREF colArrowNormal;
	COLORREF colArrowPressed;
	COLORREF colDisabledArrow;
	COLORREF colDisabledBackground;
	COLORREF colDisabledBorderTopLeft;
	COLORREF colDisabledBorderBottomRight;
	COLORREF colDisabledText;

	void CreateRegions(HRGN &hrgClosed, HRGN &hrgOpened);
	void updateScroll();
	int addItem(char * caption);
	void deleteItems();

	EMComboBox(HWND hWnd);
	~EMComboBox();
};

//----------------------------------------------------------------------
//	EM Check Box

void InitEMCheckBox();
LRESULT CALLBACK EMCheckBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMCheckBox * GetEMCheckBoxInstance(HWND hwnd);
void SetEMCheckBoxInstance(HWND hwnd, EMCheckBox *emb);

class EMCheckBox
{
public:
	COLORREF colBorderLight;
	COLORREF colBorderDark;
	COLORREF colBackground;
	COLORREF colBackgroundDisabled;
	COLORREF colX;
	COLORREF colXDisabled;
	bool clicked;
	bool selected;
	HWND hwnd;
	HRGN hRgn;
	int size;

	void evalRegion();

	EMCheckBox(HWND hWnd);
	~EMCheckBox();
};

//----------------------------------------------------------------------
//	EM Track Bar

void InitEMEditTrackBar();
LRESULT CALLBACK EMEditTrackBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMEditTrackBar * GetEMEditTrackBarInstance(HWND hwnd);
void SetEMEditTrackBarInstance(HWND hwnd, EMEditTrackBar *emetb);

class EMEditTrackBar
{
public:
	COLORREF colBackground;
	COLORREF colPathBorderUpLeft;
	COLORREF colPathBorderDownRight;
	COLORREF colPathBackgroundBefore;
	COLORREF colPathBackgroundAfter;
	COLORREF colSliderBorderUpLeft;
	COLORREF colSliderBorderDownRight;
	COLORREF colSliderBackground;
	COLORREF colSliderBackgroundClicked;

	COLORREF colEditBorderUpLeft;
	COLORREF colEditBorderDownRight;
	COLORREF colEditBackground;
	COLORREF colEditText;

	COLORREF colDisabledBorderLight;
	COLORREF colDisabledBorderDark;
	COLORREF colDisabledBackground;
	COLORREF colDisabledEditBackground;
	COLORREF colDisabledEditText;



	HBRUSH hEditBrush;
	HBRUSH hEditDisabledBrush;
	HWND hwnd;
	HWND hEdit;

	bool allowFloat;
	bool nowScrolling;
	bool nowChanged;

	int height;
	int editRectWidth;
	int pathHeight;
	int pathXStart;
	int pathXEnd;
	int pathLength;
	int sliderWidth;
	int sliderHeight;
	int sliderPos;

	int minIntValue;
	int maxIntValue;
	int intValue;
	int intFastValue;

	float minFloatValue;
	float maxFloatValue;
	float floatValue;
	float floatFastValue;
	int floatAfterDot;


	EMEditTrackBar(HWND hWnd);
	~EMEditTrackBar();

	void setEditBoxWidth(int w);
	void notifyParent();
	void resize();
	void floatToEditbox(float value);
	void intToEditbox(int value);
	void setValuesToInt(int value, int minVal, int maxVal, int fastValue);
	void setValuesToFloat(float value, float minVal, float maxVal, float fastValue);
};

//----------------------------------------------------------------------
//	EM Edit Spin

void InitEMEditSpin();
LRESULT CALLBACK EMEditSpinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMEditSpin * GetEMEditSpinInstance(HWND hwnd);
void SetEMEditSpinInstance(HWND hwnd, EMEditSpin *emes);

class EMEditSpin
{
public:
	COLORREF colBackground;
	COLORREF colButtonBorderUpLeft;
	COLORREF colButtonBorderDownRight;
	COLORREF colButtonBackground;
	COLORREF colButtonBackgroundClick;
	COLORREF colArrow;
	COLORREF colArrowClicked;
	COLORREF colEditBorderUpLeft;
	COLORREF colEditBorderDownRight;
	COLORREF colEditBackground;
	COLORREF colEditText;
	COLORREF colDisabledBorderLight;
	COLORREF colDisabledBorderDark;
	COLORREF colDisabledBackground;
	COLORREF colDisabledEditBackground;
	COLORREF colDisabledEditText;
	COLORREF colDisabledArrow;

	HBRUSH hEditBrush;
	HBRUSH hEditDisabledBrush;
	HWND hwnd;
	HWND hEdit;
	RECT rEdit, rUpper, rLower;

	bool allowFloat;

	bool nowScrolling;
	bool clicked1;
	bool clicked2;
	bool overButton1;
	bool overButton2;

	int height;
	int editRectWidth;
	int buttonHeight;
	int buttonWidth;

	int minIntValue;
	int maxIntValue;
	int intValue;
	int intChangeValue;
	float intMouseChangeValue;

	float minFloatValue;
	float maxFloatValue;
	float floatValue;
	float floatChangeValue;
	float floatMouseChangeValue;
	int floatAfterDot;

	EMEditSpin(HWND hWnd);
	~EMEditSpin();

	//void resize();
	void updateChangedValue();
	void floatToEditbox(float value);
	void intToEditbox(int value);
	void setValuesToInt(int value, int minVal, int maxVal, int changeValue, float mouseChangeValue);
	void setValuesToFloat(float value, float minVal, float maxVal, float changeValue, float mouseChangeValue);
	void notifyScrolling();
};

//---------------------------------------------------------------------
//	EM Fresnel

void InitEMFresnel();
LRESULT CALLBACK EMFresnelProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMFresnel * GetEMFresnelInstance(HWND hwnd);
void SetEMFresnelInstance(HWND hwnd, EMFresnel *emf);

class EMFresnel
{
public:
	static const int MODE_FRESNEL = 1;
	static const int MODE_CUSTOM = 2;
	COLORREF colBorderLeftUp;
	COLORREF colBorderRightDown;
	COLORREF colBackgroundUp;
	COLORREF colBackgroundDown;
	COLORREF colPoint;
	COLORREF colPointOver;
	COLORREF colPointClicked;
	COLORREF colLine;
	bool showBorder;

	int mode;
	bool drawSimpleLines;
	bool clicked;
	bool overControl;

	float IOR;
	short lastYPos;
	float lastIOR;
	float moveChangeIOR;
	float maxIOR;
	float minIOR;

	bool overPoint0;
	bool overPoint1;
	bool overPoint2;
	bool overPoint3;
	float X1, X2, Y1, Y2, Y0, Y3;

	Fresnel * dialogFresnelPtr;
	HWND hwnd;
	HRGN hUp;
	HRGN hDown;
	POINT *pLine;
	int numLinePoints;
	void processMouseMoveFresnel(short x, short y);
	void processMouseDownFresnel(short x, short y);
	void processMouseUpFresnel(short x, short y);
	void processMouseMoveCustom(short x, short y);
	void processMouseDownCustom(short x, short y);
	void processMouseUpCustom(short x, short y);

	EMFresnel(HWND hWnd);
	~EMFresnel();

	void createNewRegions();
	float getFresnelValue(float x);
	float getCustomValue(float x);
	float getCustomValueSimpleLines(float x);
	void notifyScrolling();
	Fresnel getFresnelClassInstance();
};

//---------------------------------------------------------------------

void InitEMColorPicker();
LRESULT CALLBACK EMColorPickerProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMColorPicker * GetEMColorPickerInstance(HWND hwnd);
void SetEMColorPickerInstance(HWND hwnd, EMColorPicker *emcp);

class EMColorPicker
{
private:
	int sliderWidth;
public:
	COLORREF colBorderLeftUp;
	COLORREF colBorderRightDown;
	COLORREF colBackground;
	COLORREF colArrow;
	COLORREF colArrowClicked;
	HWND hwnd;
	HRGN cRgn;
	bool showBorder;
	Color4 rgb;
	Color4 fullSatRGB;
	ColorHSV hsv;
	ColorHSV fullSatHSV;
	int hueSlPos;
	int satPos;
	int valPos;
	unsigned char * buffSV;
	unsigned char * buffH;
	bool clickedSlider;
	bool clickedQuad;
	Color4 * result;

	int slW();
	void generateSlider();
	void generateQuad();
	void notifyParent();
	void setRGBandRedraw(const Color4 &c);
	void setHSVandRedraw(ColorHSV c);

	EMColorPicker(HWND hWnd);
	~EMColorPicker();
};

//---------------------------------------------------------------------

void InitEMColorShow();
LRESULT CALLBACK EMColorShowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMColorShow * GetEMColorShowInstance(HWND hwnd);
void SetEMColorShowInstance(HWND hwnd, EMColorShow *emcs);

class EMColorShow
{
public:
	Color4 color;
	COLORREF colBorderLeftUp;
	COLORREF colBorderRightDown;
	COLORREF colDragBorderLeftUp;
	COLORREF colDragBorderRightDown;
	HBRUSH dialogBrush;
	HWND hwnd;
	bool showBorder;
	bool allowDragSrc;
	bool allowDragDst;
	bool isDragOver;

	HANDLE hCursor;
	HBITMAP hImageCursor;
	HBITMAP hImageCursorMask;
	bool createCursor(COLORREF col);

	void dragOver();
	void dragNoOver();
	void setAllowedDragDrop();
	void setDeniedDragDrop();
	void redraw(const Color4 &c);
	EMColorShow(HWND hWnd);
	~EMColorShow();
};

//---------------------------------------------------------------------


void InitEMGroupBar();
LRESULT CALLBACK EMGroupBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMGroupBar * GetEMGroupBarInstance(HWND hwnd);
void SetEMGroupBarInstance(HWND hwnd, EMGroupBar *emgb);

class EMGroupBar
{
public:
	static const char VALIGN_TOP    = 1;
	static const char VALIGN_CENTER = 2;
	static const char VALIGN_BOTTOM = 3;
	static const char HALIGN_LEFT   = 1;
	static const char HALIGN_CENTER = 2;
	static const char HALIGN_RIGHT  = 3;

	HWND hwnd;
    COLORREF colText;
    COLORREF colBackGnd;
	COLORREF colBorderLight;
	COLORREF colBorderDark;
	int captionSize;
	char * caption;
    HFONT    hFont;	
	char hAlign;
	char vAlign;

	EMGroupBar(HWND hWnd);
	~EMGroupBar();
};

//---------------------------------------------------------------------

void InitEMCameraView();
LRESULT CALLBACK EMCameraViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMCameraView * GetEMCameraViewInstance(HWND hwnd);
void SetEMCameraViewInstance(HWND hwnd, EMCameraView *emcv);

class EMCameraView
{
public:
	bool active;
	HWND hwnd;
    HFONT hFont;
	HGLRC hRC;
	HDC hDC;
	bool lButton;
	bool rButton;
	bool mButton;

	Camera * cam;
	Camera tcam;
	float cphi, ctheta;

	int w, h;
	unsigned int sceneList;
	float lightPos[4];

	bool showCompass;
	float ctg;
	unsigned int compass_texture;
	unsigned int compass_letters_texture;
	bool compass_textures_loaded;
	Vector3d dirN, dirS, dirW, dirE, dirU;

	unsigned int * sceneTextures;
	int numTextures;
	bool showTextures;
	bool wireframe;

	void SetDCPixelFormat(HDC hDC);
	void GLRender();
	void refreshViewport();
	void activateGL();
	void deactivateGL();
	void createSceneList();
	void disposeSceneList();
	void setupGL();
	void initializeCamera();
	void createPerspectiveMatrix();

	void notifyParent();

	bool loadTexture32(unsigned int * texture, HINSTANCE hInst, unsigned int resNumber);
	void deleteTexture(unsigned int * texture);

	EMCameraView(HWND hWnd);
	~EMCameraView();
};

//---------------------------------------------------------------------


void InitEMEnvironment();
LRESULT CALLBACK EMEnvironmentProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMEnvironment * GetEMEnvironmentInstance(HWND hwnd);
void SetEMEnvironmentInstance(HWND hwnd, EMEnvironment *emen);

class EMEnvironment
{
public:
    HWND     hwnd;
	SunSky sunsky;

	float sunPhi, sunTheta;
	bool drawBorder;

	HBITMAP bmEarth;
	HBITMAP bmShadowedEarth;
	BITMAP shBMPinfo;

	unsigned char * shBits;
	unsigned char * earthBits;
	bool shadowedOK;
	void evalShadow();
	bool drawShadowTexture(HDC hdc);
	bool drawDisabledFog(HDC hdc, HDC orighdc);

	void notifyParent();
	void repaint();
	void evalSunPosition();

	EMEnvironment(HWND hWnd);
	~EMEnvironment();
};

//---------------------------------------------------------------------

void InitEMProgressBar();
LRESULT CALLBACK EMProgressBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMProgressBar * GetEMProgressBarInstance(HWND hwnd);
void SetEMProgressBarInstance(HWND hwnd, EMProgressBar *empb);

class EMProgressBar
{
public:
	COLORREF colBackGnd;
	COLORREF colBorderLight;
	COLORREF colBorderDark;
	COLORREF colPBackGnd;
	COLORREF colPBorderLight;
	COLORREF colPBorderDark;
	COLORREF colDisabledBackground;
	COLORREF colDisabledBorderLight;
	COLORREF colDisabledBorderDark;
	COLORREF colDisabledPBackground;
	COLORREF colDisabledPBorderLight;
	COLORREF colDisabledPBorderDark;
    HWND hwnd;
	int	posMin;
	int posMax;
	int posCur;
	float posMain;	// 0 - 100

	bool useNewStyle;
	HFONT hFont;
	int align;
	COLORREF colNBgUp;
	COLORREF colNBgDown;
	COLORREF colNTrackUp;
	COLORREF colNTrackDown;
	COLORREF colNTrackBorder;
	COLORREF colNText;
	bool changeCaption(char * newtext);

	static const char ALIGN_LEFT = 0;
	static const char ALIGN_CENTER_CONTROL = 1;
	static const char ALIGN_RIGHT_CONTROL  = 2;
	static const char ALIGN_CENTER_TRACK = 3;
	static const char ALIGN_RIGHT_TRACK = 4;


	void refreshSize();
	void setPos(float pos);

	EMProgressBar(HWND hWnd);
	~EMProgressBar();
};

//---------------------------------------------------------------------


void InitEMStatusBar();
LRESULT CALLBACK EMStatusBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMStatusBar * GetEMStatusBarInstance(HWND hwnd);
void SetEMStatusBarInstance(HWND hwnd, EMStatusBar *emsb);

class EMStatusBar
{
public:

	HWND hwnd;
    COLORREF colText;
    COLORREF colBackGnd;
	COLORREF colBorderUpLeft;
	COLORREF colBorderDownRight;
	
	int distLeft;
	int distRight;
	int distBottom;
	int width;
	int height;
	bool anchorLeft;
	bool anchorRight;
	bool anchorBottom;

	int captionSize;
	char * caption;
    HFONT    hFont;

	void updateSize();
	bool setCaption(char * newCap);
	EMStatusBar(HWND hWnd);
	~EMStatusBar();
};

//---------------------------------------------------------------------

void InitEMTabs();
LRESULT CALLBACK EMTabsProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMTabs * GetEMTabsInstance(HWND hwnd);
void SetEMTabsInstance(HWND hwnd, EMTabs *emt);

class EMTabs
{
public:
	HWND hwnd;
    COLORREF colText;
    COLORREF colBackGnd;
	COLORREF colBorderLight;
	COLORREF colBorderDark;
	COLORREF colBorderBottom;
	COLORREF colSelText;
	COLORREF colSelBackGnd;
	COLORREF colSelBorderBottom;
	COLORREF colDisText;
	COLORREF colDisBackGnd;
	COLORREF colDisBorderLight;
	COLORREF colDisBorderDark;
	COLORREF colDisBorderBottom;
	COLORREF colDisSelBorderBottom;

	int heightTab;
	int heightTabSel;
	int marginSize;
	bool drawBottomBorder;

	HFONT hFont;
	HFONT hBoldFont;
	HRGN hRegion;
	POINT * rP;

	bList<char *, 8> caps;
	bList<int, 8> lengths;
	bList<int, 8> ends;
	int selected;
	int lastSelected;



	void updateLenghts();
	void createRegion(HDC &hdc);
	int getClickedTabNumber(int x, int y);
	EMTabs(HWND hWnd);
	~EMTabs();
};

//---------------------------------------------------------------------

void InitEMListView();
LRESULT CALLBACK EMListViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMListView * GetEMListViewInstance(HWND hwnd);
void SetEMListViewInstance(HWND hwnd, EMListView *emlv);

class EMListView
{
public:
	HWND hwnd;
	HFONT hFont;
	HRGN hRegion;

	COLORREF colHeaderBackground;
	COLORREF colHeaderBorderUpLeft;
	COLORREF colHeaderBorderDownRight;
	COLORREF colBackgroundOdd;
	COLORREF colBackgroundEven;
	COLORREF colBackgroundSelected;
	COLORREF colText;
	COLORREF colTextSelected;
	COLORREF colMainBorderUpLeft;
	COLORREF colMainBorderDownRight;
	COLORREF colTextMouseOver;
	COLORREF colBackgroundMouseOver;
	COLORREF colSBTrackBackground;
	COLORREF colSBBackground;
	COLORREF colSBBackgroundClicked;
	COLORREF colSBBorderUpLeft;
	COLORREF colSBBorderDownRight;

	int rowHeight;
	int maxShownRows;
	int maxShownRowsAndHeader;
	int headerHeight;
	int selected;
	int mouseOver;
	int firstShown;

	int numCols;
	bList<char *, 16> headers;
	bList<char ** , 16> data;
	bList<int, 16> colWidths;
	bList<char, 16> align;

	bool addEmptyRow();
	bool setData(int col, int row, char * str);
	void setColNumAndClearData(int num, int width);
	void drawScrollBar(HDC hdc);
	void freeEntries();

	// scrollbar data
	int sbWidth;
	int sbStart;
	int sbEnd;
	bool sliding;
	void calcSB();
	void calcFP();
	// end scrollbar data
	bool showHeader;

	void notifySelectionChanged();
	void notifyMouseOverChanged();
	void notifyDoubleClick();
	void prepareWindow();
	void gotoEnd();
	void Refresh();

	static const char ALIGN_LEFT   = 0;
	static const char ALIGN_CENTER = 1;
	static const char ALIGN_RIGHT  = 2;

	EMListView(HWND hWnd);
	~EMListView();
};

//---------------------------------------------------------------------

void InitEMText();
LRESULT CALLBACK EMTextProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMText * GetEMTextInstance(HWND hwnd);
void SetEMTextInstance(HWND hwnd, EMText *emlv);

class EMText
{
public:
	static const int ALIGN_LEFT = 0;
	static const int ALIGN_RIGHT = 1;
	static const int ALIGN_CENTER = 2;

	HWND     hwnd;
    HFONT    hFont;

	COLORREF colText;				// Foreground text colour
    COLORREF colBackGnd;			// Background text colour
    COLORREF colDisabledText;		// Foreground text colour for disabled
	COLORREF colLink;
	COLORREF colLinkMouseOver;
	int captionSize;
	char * caption;
	char * linkAddress;
	int align;
	RECT textRect;
	bool isItHyperLink;
	bool drawTransparent;
	bool isMouseOverNow;

	EMText(HWND hWnd);
	~EMText();

	bool changeCaption(char * newCaption);
	bool setHyperLink(char * newAddress);
	bool evalTextRect(HDC &hdc, RECT * rect);
};

//----------------------------------------------------------------------
//	EM Curve

#define EMCMAXPOINTS 30
void InitEMCurve();
LRESULT CALLBACK EMCurveProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMCurve * GetEMCurveInstance(HWND hwnd);
void SetEMCurveInstance(HWND hwnd, EMCurve *emc);

class EMCurve
{
public:
	struct P2D { int x, y; };
    COLORREF colBackGnd;
	COLORREF colDotLines;
	COLORREF colSelOuter;
	COLORREF colBorder;
	COLORREF colLineRed;
	COLORREF colLineGreen;
	COLORREF colLineBlue;
	COLORREF colLineLight;

    HWND     hwnd;
	int	margin;

	bool isLuminocityMode;
	bool mOverRGB[4];
	int mOverIndexRGB[4];
	int lastIgnoredRGB[4];
	bool nowDragging;

	bool activeRGB[4];
	vector<P2D> pointsR;
	vector<P2D> pointsG;
	vector<P2D> pointsB;
	vector<P2D> pointsL;
	POINT lineR[257];
	POINT lineG[257];
	POINT lineB[257];
	POINT lineL[257];
	float fra[EMCMAXPOINTS+2];
	float frb[EMCMAXPOINTS+2];
	float frc[EMCMAXPOINTS+2];
	float frd[EMCMAXPOINTS+2];
	float fga[EMCMAXPOINTS+2];
	float fgb[EMCMAXPOINTS+2];
	float fgc[EMCMAXPOINTS+2];
	float fgd[EMCMAXPOINTS+2];
	float fba[EMCMAXPOINTS+2];
	float fbb[EMCMAXPOINTS+2];
	float fbc[EMCMAXPOINTS+2];
	float fbd[EMCMAXPOINTS+2];
	float fla[EMCMAXPOINTS+2];
	float flb[EMCMAXPOINTS+2];
	float flc[EMCMAXPOINTS+2];
	float fld[EMCMAXPOINTS+2];
	P2D sortedPtsR[EMCMAXPOINTS+2];
	P2D sortedPtsG[EMCMAXPOINTS+2];
	P2D sortedPtsB[EMCMAXPOINTS+2];
	P2D sortedPtsL[EMCMAXPOINTS+2];

	bool prepareSplineEquation(int whichRGB, const int ignoreIndex);

	void reset();
	void createLines();
	void createLine(int whichRGB, int ignoreIndex=-1);
	void createLine(vector<P2D> points, POINT * line, int ignoreIndex=-1);
	vector<P2D> & getPoints(int numRGB);
	POINT * getLine(int numRGB);
	float * getFRGBABCD(int whichRGB, int whichABCD);
	P2D * getSortedPoints(int whichRGB);
	int xPointExists(int x, vector<P2D> points, int excIndex=-1);
	int findPointAround(vector<P2D> points, int x, int y);
	Color4 processColor(const Color4 &col);
	Color4 processColorRGB(const Color4 &col);
	Color4 processColorLuminosity(const Color4 &col);
	bool hasAnyPoint();
	void notifyChanged();

	EMCurve(HWND hWnd);
	~EMCurve();
};

//---------------------------------------------------------------------

void InitEMTemperaturePicker();
LRESULT CALLBACK EMTemperaturePickerProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMTemperaturePicker * GetEMTemperaturePickerInstance(HWND hwnd);
void SetEMTemperaturePickerInstance(HWND hwnd, EMTemperaturePicker *emtp);

class EMTemperaturePicker
{
public:
	COLORREF colBorderLeftUp;
	COLORREF colBorderRightDown;
	COLORREF colSlider;
	COLORREF colText;

	HWND hwnd;
	HFONT hFont;
	int sliderPos;
	int changeTextPosValue;

	int getTemperature();
	void setTemperature(int tmp);
	Color4 getEnergyColor();
	Color4 getGammaColor();
	
	void notifyParent();
	EMTemperaturePicker(HWND hWnd);
	~EMTemperaturePicker();
};

//---------------------------------------------------------------------

void InitEMLine();
LRESULT CALLBACK EMLineProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMLine * GetEMLineInstance(HWND hwnd);
void SetEMLineInstance(HWND hwnd, EMLine *eml);

class EMLine
{
public:
	COLORREF colBorderLeftUp;
	COLORREF colBorderRightDown;
	HWND hwnd;

	EMLine(HWND hWnd);
	~EMLine();
};

//---------------------------------------------------------------------

void InitEMEditSimple();
LRESULT CALLBACK EMEditSimpleProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMEditSimple * GetEMEditSimpleInstance(HWND hwnd);
void SetEMEditSimpleInstance(HWND hwnd, EMEditSimple *emes);

class EMEditSimple
{
public:
	COLORREF colBorderUpLeft;
	COLORREF colBorderDownRight;
	COLORREF colBackground;
	COLORREF colText;
	COLORREF colDisabledBorderUpLeft;
	COLORREF colDisabledBorderDownRight;
	COLORREF colDisabledBackground;
	COLORREF colDisabledText;

	HBRUSH hBrush;
	HBRUSH hDisabledBrush;
	HWND hwnd;
	HWND hEdit;

	char * getText();
	bool setText(char * text);
	EMEditSimple(HWND hWnd);
	~EMEditSimple();
};

//---------------------------------------------------------------------

void InitEMPages();
LRESULT CALLBACK EMPagesProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMPages * GetEMPagesInstance(HWND hwnd);
void SetEMPagesInstance(HWND hwnd, EMPages *emp);

class EMPages
{
public:
	static const int ALIGN_LEFT = 0;
	static const int ALIGN_RIGHT = 1;
	static const int ALIGN_CENTER = 2;

	COLORREF colLinkNormal;
	COLORREF colLinkCurrent;
	COLORREF colLinkMouseOver;
	COLORREF colNoLink;
	COLORREF colBackground;
	char * noLinkText;
	char * preText;
	int numPages;
	int curPage;
	int align;
	int leftMargin;
	int mouseOver;
	int lastMouseOver;
	HWND hwnd;
    HFONT hFont;
	HFONT hBoldFont;
	int getMouseOver(POINT px);

	RECT nrects[8];
	int numbers[8];

	void setCurrentPage(int cp, int np);

	EMPages(HWND hWnd);
	~EMPages();
};

//---------------------------------------------------------------------

void InitEMScrollBar();
LRESULT CALLBACK EMScrollBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMScrollBar * GetEMScrollBarInstance(HWND hwnd);
void SetEMScrollBarInstance(HWND hwnd, EMScrollBar *emsb);

class EMScrollBar
{
public:
	static const int ARR_VERTICAL = 0;
	static const int ARR_HORIZONTAL = 1;

	HWND hwnd;
	HRGN rgnButtonLeft;
	HRGN rgnButtonRight;
	HRGN rgnButtonTop;
	HRGN rgnButtonBottom;
	HRGN rgnTrack;
	HRGN rgnTrackBefore;
	HRGN rgnTrackAfter;
	HRGN rgnSlider;

	int control_arrangement;
	int control_thickness;
	int control_length;
	int control_button_length;
	int control_track_length;
	int control_slider_length;
	int slider_pos;
	int workarea_pos;	// visible shift in pixels
	int size_work;		// whole size
	int size_screen;	// visible size
	int grid;

	COLORREF colBg;
	COLORREF colTrack;
	COLORREF colTrackLight;
	COLORREF colTrackDark;
	COLORREF colSlider;
	COLORREF colSliderMouseOver;
	COLORREF colSliderClicked;
	COLORREF colSliderLight;
	COLORREF colSliderDark;
	COLORREF colButton;
	COLORREF colButtonMouseOver;
	COLORREF colButtonClicked;
	COLORREF colButtonLight;
	COLORREF colButtonDark;
	COLORREF colButtonArrow;
	COLORREF colButtonArrowMouseOver;
	COLORREF colButtonArrowClicked;
	COLORREF colDisabledBg;
	COLORREF colDisabledTrack;
	COLORREF colDisabledTrackLight;
	COLORREF colDisabledTrackDark;
	COLORREF colDisabledSlider;
	COLORREF colDisabledSliderLight;
	COLORREF colDisabledSliderDark;
	COLORREF colDisabledButton;
	COLORREF colDisabledButtonLight;
	COLORREF colDisabledButtonDark;
	COLORREF colDisabledButtonArrow;

	// new style colors
	COLORREF colNButtonLeft;
	COLORREF colNButtonRight;
	COLORREF colNButtonBevelLeft;
	COLORREF colNButtonBevelTop;
	COLORREF colNButtonBevelRight;
	COLORREF colNButtonBevelBottom;
	COLORREF colNArrowLeft;
	COLORREF colNArrowRight;
	COLORREF colNPathLeft;
	COLORREF colNPathRight;
	COLORREF colNDisabledPathLeft;
	COLORREF colNDisabledPathRight;
	COLORREF colNMOverButtonLeft;
	COLORREF colNMOverButtonRight;
	COLORREF colNMOverButtonBevelLeft;
	COLORREF colNMOverButtonBevelTop;
	COLORREF colNMOverButtonBevelRight;
	COLORREF colNMOverButtonBevelBottom;
	COLORREF colNMOverArrowLeft;
	COLORREF colNMOverArrowRight;
	COLORREF colNMClickButtonLeft;
	COLORREF colNMClickButtonRight;
	COLORREF colNMClickButtonBevelLeft;
	COLORREF colNMClickButtonBevelTop;
	COLORREF colNMClickButtonBevelRight;
	COLORREF colNMClickButtonBevelBottom;
	COLORREF colNMClickArrowLeft;
	COLORREF colNMClickArrowRight;

	bool buttonLeftClicked;
	bool buttonRightClicked;
	bool buttonTopClicked;
	bool buttonBottomClicked;
	bool sliderClicked;
	bool buttonLeftMouseOver;
	bool buttonRightMouseOver;
	bool buttonTopMouseOver;
	bool buttonBottomMouseOver;
	bool sliderMouseOver;
	bool useNewStyle;

	bool updateRegions(bool onlySlider=false);
	bool updateSize();
	bool updateWorkareaPositionFromSlider();

	bool notifyParentChange();

	EMScrollBar(HWND hWnd);
	~EMScrollBar();
};

//---------------------------------------------------------------------

void InitEMImageList();
LRESULT CALLBACK EMImageListProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMImageList * GetEMImageListInstance(HWND hwnd);
void SetEMImageListInstance(HWND hwnd, EMImageList *emil);

class EMImageList
{
public:
	HWND hwnd;
	HWND lastFocusHWND;
    HFONT hFont;

	int img_width;
	int img_height;
	int img_cols;
	int img_rows;
	int img_grid_x;
	int img_grid_y;
	int text_horiz_margin;
	int text_upper_margin;
	int corner_round;
	int scrollGrid;
	int start_x;
	int start_y;
	int imageSelected;
	int imageMouseOver;
	bool drawImageBorders;
	bool drawControlBorder;
	bool alignToGrid;			// not used yet
	bool scrollVertical;
	bool showNames1;
	bool showNames2;

	bList<COLORREF *, 16> imgBuffers;
	bList<char *, 16> imgNames1;
	bList<char *, 16> imgNames2;

	COLORREF colBg;
	COLORREF colBgMouseOver;
	COLORREF colBgSelected;
	COLORREF colControlBorderLeftUp;
	COLORREF colControlBorderRightDown;
	COLORREF colImgBorderLeftUp;
	COLORREF colImgBorderRightDown;
	COLORREF colImgBorderSelectedLeftUp;
	COLORREF colImgBorderSelectedRightDown;
	COLORREF colText1;
	COLORREF colText2;
	COLORREF colText1Selected;
	COLORREF colText2Selected;
	COLORREF colDisabledBg;
	COLORREF colDisabledControlBorderLeftUp;
	COLORREF colDisabledControlBorderRightDown;
	COLORREF colDisabledImgBorderLeftUp;
	COLORREF colDisabledImgBorderRightDown;
	COLORREF colDisabledText1;
	COLORREF colDisabledText2;

	bool notifyParentChangePos();
	bool notifyParentChangeSelected();
	bool notifyParentDblClick();
	bool notifyParentMouseOver();

	bool drawControl(HDC hdc, bool disabled=false);
	int findTileMouseOver(POINT mpt);
	int getWorkspaceLength();
	void verifyPosition();

	EMImageList(HWND hWnd);
	~EMImageList();
};

//---------------------------------------------------------------------

void InitEMAberration();
LRESULT CALLBACK EMAberrationProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMAberration * GetEMAberrationInstance(HWND hwnd);
void SetEMAberrationInstance(HWND hwnd, EMAberration *emb);

class EMAberration
{
public:
    HWND hwnd;

	float abbShiftXY;
	float abbShiftZ;
	float abbAchromatic;
	float distPattern;

    COLORREF colBackground;
    COLORREF colLens;
    COLORREF colAxis;
    COLORREF colDepthLine;
    COLORREF colRay;
    COLORREF colBorderLeftUp;
    COLORREF colBorderDownRight;

	bool drawNice;

	EMAberration(HWND hWnd);
	~EMAberration();
};

//----------------------------------------------------------------------

void InitEMGraphList();
LRESULT CALLBACK EMGraphListProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EMGraphList * GetEMGraphListInstance(HWND hwnd);
void SetEMGraphListInstance(HWND hwnd, EMGraphList *emgl);

class EMGraphList
{
public:
	struct Record
	{
		char * name;
		float value;
		COLORREF colBarUp;
		COLORREF colBarDown;
		COLORREF colBorder;
		COLORREF colText;
		Record() { name=NULL; value=0.0f; colBarUp=RGB(255,255,255); colBarDown=RGB(128, 128, 128); colBorder=RGB(0,0,0); colText=RGB(255,255,255); }
		Record(char * nname, float nvalue, COLORREF ncolBarUp, COLORREF ncolBarDown, COLORREF ncolBorder, COLORREF ncolText) 
				{ name=copyString(nname); value=nvalue; colBarUp=ncolBarUp; colBarDown=ncolBarDown; colBorder=ncolBorder; colText=ncolText; }
	};

	HWND hwnd;
	HFONT hFont;

	COLORREF colBackground;
	COLORREF colText;
	COLORREF colBars;
	COLORREF colBarBgUp;
	COLORREF colBarBgDown;
	COLORREF colDisText;
	COLORREF colDisBarUp;
	COLORREF colDisBarDown;

	bool bgTransparent;
	bool drawShadowGdiplus;
	bool drawNumbers;

	int paddingVert;
	int barHeight;
	int bmx, bmy, bmr;	// margin of bars from left and top and right

	bList<Record, 8> entries;
	bList<int, 8> ids_sorted;

	bool addData(Record r);
	bool clearData();
	bool sortIDs(bool descending);
	bool resetSortedIDs();
	bool removeLastEntry(bool sort, bool descending);

	static const char ALIGN_LEFT   = 0;
	static const char ALIGN_CENTER = 1;
	static const char ALIGN_RIGHT  = 2;

	EMGraphList(HWND hWnd);
	~EMGraphList();
};

//----------------------------------------------------------------------

void drawGradientHorizontal(HDC hdc, COLORREF cLeft, COLORREF cRight, int px, int py, int w, int h);
void drawGradientVertical(HDC hdc, COLORREF cTop, COLORREF cBottom, int px, int py, int w, int h);
void drawBevel(HDC hdc, COLORREF cLeft, COLORREF cTop, COLORREF cRight, COLORREF cBottom, int px1, int py1, int px2, int py2);

//----------------------------------------------------------------------

#endif
