#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"

extern HMODULE hDllModule;

void InitEMGroupBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMGroupBar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMGroupBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMGroupBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMGroupBar * GetEMGroupBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMGroupBar * emgb = (EMGroupBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EMGroupBar * emgb = (EMGroupBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emgb;
}

void SetEMGroupBarInstance(HWND hwnd, EMGroupBar *emgb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emgb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emgb);
	#endif
}

EMGroupBar::EMGroupBar(HWND hWnd)
{
	hwnd = hWnd;
	colText = RGB(0,0,0); 
	colBackGnd = RGB(96,160,224);
	colBorderLight = RGB(160,204,255);
	colBorderDark = RGB(64,128,192);
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hAlign = HALIGN_LEFT;
	vAlign = VALIGN_CENTER;

	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
	{
		captionSize = 0;
	}	
}

EMGroupBar::~EMGroupBar()
{
}

LRESULT CALLBACK EMGroupBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMGroupBar* emgb;
	emgb = GetEMGroupBarInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HFONT hOldFont;
	HPEN hOldPen;

	switch (msg)
	{
		case WM_CREATE:
			emgb = new EMGroupBar(hwnd);
			SetEMGroupBarInstance(hwnd, emgb);
			break;
		case WM_DESTROY:
			{
				delete emgb;
				SetEMGroupBarInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);


			hOldFont =(HFONT) SelectObject(hdc, emgb->hFont);
			SetTextColor(hdc, emgb->colText);
			SetBkColor  (hdc, emgb->colBackGnd);

			SIZE sz;
			GetTextExtentPoint32(hdc, emgb->caption, emgb->captionSize, &sz);
			int posx, posy; 
			switch (emgb->vAlign)
			{
				case EMGroupBar::VALIGN_TOP:
					posy = 2;
					break;
				case EMGroupBar::VALIGN_CENTER:
					posy = (rect.bottom-rect.top-sz.cy)/2;
					break;
				case EMGroupBar::VALIGN_BOTTOM:
					posy = rect.bottom - sz.cy - 2;
					break;
			}
			switch (emgb->hAlign)
			{
				case EMGroupBar::HALIGN_LEFT:
					posx = 5;
					break;
				case EMGroupBar::HALIGN_CENTER:
					posx = (rect.right-rect.left-sz.cx)/2;
					break;
				case EMGroupBar::HALIGN_RIGHT:
					posx = rect.right - sz.cx - 5;
					break;
			}

			ExtTextOut(hdc, posx, posy, ETO_OPAQUE, &rect, emgb->caption, emgb->captionSize, 0);


			POINT b1[] = { {0, rect.bottom-1}, {0,0}, {rect.right, 0} };
			POINT b2[] = { {rect.right-1, 1}, {rect.right-1, rect.bottom-1}, {0, rect.bottom-1} };
			
			hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emgb->colBorderLight));
			Polyline(hdc, b1, 3);
			DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emgb->colBorderDark)));
			Polyline(hdc, b2, 3);
			DeleteObject(SelectObject(hdc, hOldPen));

			DeleteObject(SelectObject(hdc, hOldFont));

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

