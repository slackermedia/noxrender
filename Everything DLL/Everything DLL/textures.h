#ifndef __TEXTURES_H
#define __TEXTURES_H

#include "Colors.h"
#include "DLL.h"


#define TEXMOD_DEF_BRIGHTNESS	0.0f
#define TEXMOD_MIN_BRIGHTNESS	-1.0f
#define TEXMOD_MAX_BRIGHTNESS	1.0f
#define TEXMOD_DEF_CONTRAST		1.0f
#define TEXMOD_MIN_CONTRAST		0.0f
#define TEXMOD_MAX_CONTRAST		4.0f
#define TEXMOD_DEF_SATURATION	0.0f
#define TEXMOD_MIN_SATURATION	-1.0f
#define TEXMOD_MAX_SATURATION	1.0f
#define TEXMOD_DEF_HUE			0.0f
#define TEXMOD_MIN_HUE			0.0f
#define TEXMOD_MAX_HUE			1.0f
#define TEXMOD_DEF_RED			0.0f
#define TEXMOD_MIN_RED			-1.0f
#define TEXMOD_MAX_RED			1.0f
#define TEXMOD_DEF_GREEN		0.0f
#define TEXMOD_MIN_GREEN		-1.0f
#define TEXMOD_MAX_GREEN		1.0f
#define TEXMOD_DEF_BLUE			0.0f
#define TEXMOD_MIN_BLUE			-1.0f
#define TEXMOD_MAX_BLUE			1.0f
#define TEXMOD_DEF_GAMMA		2.2f
#define TEXMOD_MIN_GAMMA		1.0f
#define TEXMOD_MAX_GAMMA		4.0f
#define TEXMOD_DEF_INVERT		false
#define TEXMOD_DEF_INTERPOLATE  true
//----------------------------------------------------------------------------------

#define GAMMAARRAYSIZE 1025

class TexModifer	
{
public:
	float brightness;
	float contrast;
	float saturation;
	float hue;
	float red;
	float green;
	float blue;
	float gamma;
	bool invert;
	bool interpolateProbe;

	float gammaLookup[GAMMAARRAYSIZE];

	void updateGamma(float newgamma);
	void clipValues();
	void reset()	{	brightness = TEXMOD_DEF_BRIGHTNESS; contrast = TEXMOD_DEF_CONTRAST; saturation = TEXMOD_DEF_SATURATION; hue = TEXMOD_DEF_HUE; 
						red = TEXMOD_DEF_RED; green = TEXMOD_DEF_GREEN; blue = TEXMOD_DEF_BLUE; gamma = TEXMOD_DEF_GAMMA; invert = TEXMOD_DEF_INVERT; 
						interpolateProbe = TEXMOD_DEF_INTERPOLATE; updateGamma(gamma); }

	TexModifer()	{	brightness = TEXMOD_DEF_BRIGHTNESS; contrast = TEXMOD_DEF_CONTRAST; saturation = TEXMOD_DEF_SATURATION; hue = TEXMOD_DEF_HUE; 
						red = TEXMOD_DEF_RED; green = TEXMOD_DEF_GREEN; blue = TEXMOD_DEF_BLUE; gamma = TEXMOD_DEF_GAMMA; invert = TEXMOD_DEF_INVERT; 
						interpolateProbe = TEXMOD_DEF_INTERPOLATE; updateGamma(gamma); }
	~TexModifer()	{}
};

//----------------------------------------------------------------------------------

class NormalModifier
{
public:
	float powerEV;
	float power;
	bool invertX;
	bool invertY;

	NormalModifier()	{ powerEV = 0.0f; power = 1.0f; invertX = false; invertY = false; }
	~NormalModifier()	{}
};

//----------------------------------------------------------------------------------

class BitmapImage
{
private:
	int width, height;
	bool imgOK;
public:

	Color4 * fdata;			// float hdr colors
	unsigned char * idata;	// bitmap
	bool isItFloat;

	Color4 badColor;

	bool loadFromFile(char * filename, TexModifer texmod, bool keepAsHDR);
	bool loadFromExrFile(char * filename, TexModifer texmod, bool keepAsHDR=true);
	bool loadFromHDRFile(char * filename, TexModifer texmod, bool keepAsHDR=true);
	bool allocByteBuffer(int w, int h);
	bool allocFloatBuffer(int w, int h);
	void freeByteBuffer();
	void freeFloatBuffer();
	void freeEverything();

	DECLDIR Color4 getColor(int x, int y);
	Color4 getColorUnprocessed(int x, int y);

	bool isValid();

	int getWidth()   { return width; }
	int getHeight()  { return height; }

	bool copyFrom(BitmapImage * other);

	BitmapImage();
	~BitmapImage();
};

//----------------------------------------------------------------------------------

class Texture
{
private:
	bool imgOK;
public:
	BitmapImage bmp;

	char * filename;
	char * fullfilename;
	unsigned int exportedArrayID;		// used to find fast rel path
	char * exportedFilename;			// relative path

	bool allocByteBuffer(int w, int h);
	bool allocFloatBuffer(int w, int h);
	void freeByteBuffer();
	void freeFloatBuffer();
	void freeAllBuffers();
	float gammaDown;
	DECLDIR bool loadFromFile(char * fileName);
	DECLDIR bool loadFromFileTryOtherFolders(char * fileName);
	unsigned int reserved;	// for gl texture number, oooold
	bool is_it_converted_normal;

	DWORD fileModTime[2];
	DWORD fileModTimeDontAsk[2];
	bool updateModifiedTexFile(HWND hWnd, bool &reload, bool &all);

	TexModifer texMod;
	NormalModifier normMod;

	DECLDIR Color4 getColor(float x, float y, TexModifer * tMod=NULL);
	DECLDIR Color4 processColor(const Color4 &srccol, TexModifer * texModifier);
	DECLDIR void getNormalVector(float x, float y, float v[3], NormalModifier *nMod=NULL);

	bool isValid();

	bool copyFromOther(Texture * other);		// dangerous

	bool isItNormalMap();	// false on HDR and non-blue version
	bool convertMeToNormalMap();

	Texture();
	~Texture();
};

//----------------------------------------------------------------------------------

#endif

