#include "XML_IO.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <math.h>
#include "raytracer.h"
#define ROUND(a) (int)floor(a+0.5)

char * xorStringToNew(char * src, char * pswd, unsigned int lsrc, unsigned int lpswd);

//-------------------------------------------------------------------------------------------------------------

char * XMLScene::decToHexString(char * src, unsigned int lsrc)
{
	if (!src)
		return NULL;
	if (!lsrc)
		return NULL;

	char * res = (char*)malloc(2*lsrc+1);
	if (!res)
		return false;
	for (unsigned int i=0; i<lsrc; i++)
	{
		DecHex::ucharToHex2(src[i], (unsigned char*)(&(res[2*i])));
	}
	res[2*lsrc] = 0;

	return res;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::saveMaterial(char * filename, MaterialNox * mat)
{
	if (!filename   ||   !mat)
		return false;

	xmlDocPtr xmlDoc;
	xmlDoc = xmlNewDoc((xmlChar*)"1.0");
	if (!xmlDoc)
	{
		return false;
	}

	xmlNodePtr xRoot = xmlNewDocNode(xmlDoc, NULL, (xmlChar*)"material", NULL);
	if (!xRoot)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	xmlDocSetRootElement(xmlDoc, xRoot);

	if (!insertMaterialData(xRoot, mat))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	int written = xmlSaveFormatFile(filename, xmlDoc, 1);
	xmlFreeDoc(xmlDoc);

	if (written < 1)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertMaterialData(xmlNodePtr curNode, MaterialNox * mat)
{
	if (!curNode   ||   !mat)
		return false;

	// add ID
	xmlAttrPtr xAttr;
	char buf[64];
	unsigned int id = mat->id;
	DecHex::uintToHex(id, (unsigned char*)buf);
	buf[8] = 0;
	xAttr = xmlNewProp(curNode, (xmlChar*)"id", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add name
	if (mat->name)
	{
		xmlNodePtr xName = xmlNewChild(curNode, NULL, (xmlChar*)"name", (xmlChar*)mat->name);
		if (!xName)
			return false;
	}

	// add opacity
	float opacity = mat->opacity;
	_sprintf_s_l(buf, 128, "%.2f", noxLocale, min(100, max(0, opacity*100.0f)));
	xmlNodePtr xOpac = xmlNewChild(curNode, NULL, (xmlChar*)"opacity", (xmlChar *)buf);
	if (!xOpac)
		return false;

	// add displacement_depth
	float displ_depth = mat->displ_depth;
	_sprintf_s_l(buf, 128, "%.3f", noxLocale, displ_depth);
	xmlNodePtr xDisplDepth = xmlNewChild(curNode, NULL, (xmlChar*)"displacement_depth", (xmlChar *)buf);
	if (!xDisplDepth)
		return false;

	// add displacement_shift
	float displ_shift = mat->displ_shift;
	_sprintf_s_l(buf, 128, "%.3f", noxLocale, displ_shift);
	xmlNodePtr xDisplShift= xmlNewChild(curNode, NULL, (xmlChar*)"displacement_shift", (xmlChar *)buf);
	if (!xDisplShift)
		return false;

	// add displacement_subdivs
	int displ_subdivs = mat->displ_subdivs_lvl;
	_sprintf_s_l(buf, 128, "%d", noxLocale, displ_subdivs);
	xmlNodePtr xDisplSubdivs = xmlNewChild(curNode, NULL, (xmlChar*)"displacement_subdivs", (xmlChar *)buf);
	if (!xDisplSubdivs)
		return false;

	// add displacement_normal_mode
	int displ_nm = mat->displ_normal_mode;
	_sprintf_s_l(buf, 128, "%d", noxLocale, displ_nm);
	xmlNodePtr xDisplNormalMode = xmlNewChild(curNode, NULL, (xmlChar*)"displacement_normal_mode", (xmlChar *)buf);
	if (!xDisplNormalMode)
		return false;

	// add displacement_continuity
	xmlNodePtr xDisplContinuity;
	if (mat->displ_continuity)
		xDisplContinuity = xmlNewChild(curNode, NULL, (xmlChar*)"displacement_continuity", (xmlChar *)"yes");
	else
		xDisplContinuity = xmlNewChild(curNode, NULL, (xmlChar*)"displacement_continuity", (xmlChar *)"no");
	if (!xDisplContinuity)
		return false;

	// add displacement_ignore_scale
	xmlNodePtr xDisplIgnoreScale;
	if (mat->displ_ignore_scale)
		xDisplIgnoreScale = xmlNewChild(curNode, NULL, (xmlChar*)"displacement_ignore_scale", (xmlChar *)"yes");
	else
		xDisplIgnoreScale = xmlNewChild(curNode, NULL, (xmlChar*)"displacement_ignore_scale", (xmlChar *)"no");
	if (!xDisplIgnoreScale)
		return false;

	// add tex opacity
	if (mat->tex_opacity.fullfilename)
	{
		if (!insertTexture(curNode, mat, NULL, MAT_TEX_SLOT_OPACITY))
			return false;
	}

	// add tex displacement
	if (mat->tex_displacement.fullfilename)
	{
		if (!insertTexture(curNode, mat, NULL, MAT_TEX_SLOT_DISPLACEMENT))
			return false;
	}

	// add blend layer
	sprintf_s(buf, 64, "%d", (mat->blendIndex+1));
	xmlNodePtr xBlend = xmlNewChild(curNode, NULL, (xmlChar*)"blendlayer", (xmlChar *)buf);
	if (!xBlend)
		return false;

	// add skyportal
	xmlNodePtr xSkyPortal;
	if (mat->isSkyPortal)
		xSkyPortal = xmlNewChild(curNode, NULL, (xmlChar*)"skyportal", (xmlChar *)"yes");
	else
		xSkyPortal = xmlNewChild(curNode, NULL, (xmlChar*)"skyportal", (xmlChar *)"no");
	if (!xSkyPortal)
		return false;

	// add matteshadow
	xmlNodePtr xMatteShadow;
	if (mat->isMatteShadow)
		xMatteShadow = xmlNewChild(curNode, NULL, (xmlChar*)"matteshadow", (xmlChar *)"yes");
	else
		xMatteShadow = xmlNewChild(curNode, NULL, (xmlChar*)"matteshadow", (xmlChar *)"no");
	if (!xMatteShadow)
		return false;

	// add layers
	for (int i=0; i<mat->layers.objCount; i++)
	{
		bool OK = insertMatLayer(curNode, &(mat->layers[i]));
		if (!OK)
			return false;
	}

	// add preview
	if (mat->preview)
	{
		if (!insertPreview(curNode, mat->preview))
			return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertFresnel(xmlNodePtr curNode, Fresnel * fres)
{
	if (!curNode   ||   !fres)
		return false;

	// add <fresnel>
	xmlNodePtr xFresnel = xmlNewChild(curNode, NULL, (xmlChar*)"fresnel", NULL);
	if (!xFresnel)
		return false;

	// add <IOR>
	char buf[64];
	_sprintf_s_l(buf,64, "%.2f", noxLocale, fres->IOR);

	xmlNodePtr xIOR = xmlNewChild(xFresnel, NULL, (xmlChar*)"ior", (xmlChar*)buf);
	if (!xIOR)
		return false;
	
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertTexture(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay, int slot)
{
	if (!curNode)
		return false;

	TextureInstance * texI;
	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			CHECK(mlay);	texI = &(mlay->tex_col0);			break;
		case LAYER_TEX_SLOT_COLOR90:		CHECK(mlay);	texI = &(mlay->tex_col90);			break;
		case LAYER_TEX_SLOT_ROUGHNESS:		CHECK(mlay);	texI = &(mlay->tex_rough);			break;
		case LAYER_TEX_SLOT_WEIGHT:			CHECK(mlay);	texI = &(mlay->tex_weight);			break;
		case LAYER_TEX_SLOT_LIGHT:			CHECK(mlay);	texI = &(mlay->tex_light);			break;
		case LAYER_TEX_SLOT_NORMAL:			CHECK(mlay);	texI = &(mlay->tex_normal);			break;
		case LAYER_TEX_SLOT_TRANSM:			CHECK(mlay);	texI = &(mlay->tex_transm);			break;
		case LAYER_TEX_SLOT_ANISO:			CHECK(mlay);	texI = &(mlay->tex_aniso);			break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:	CHECK(mlay);	texI = &(mlay->tex_aniso_angle);	break;
		case MAT_TEX_SLOT_OPACITY:			CHECK(mat);		texI = &(mat->tex_opacity);			break;
		case MAT_TEX_SLOT_DISPLACEMENT:		CHECK(mat);		texI = &(mat->tex_displacement);	break;
	}

	if (!texI)
		return false;

	// only file type texture
	if (!insertTextureTypeFile(curNode, mat, mlay, slot))
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertTextureTypeFile(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay, int slot)
{
	if (!curNode)
		return false;

	char buf[64];

	TextureInstance * texI;
	char * tagname = NULL;
	bool enabled = false;
	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			CHECK(mlay);	texI = &(mlay->tex_col0);			tagname = "tex_refl0";			enabled = mlay->use_tex_col0;			break;
		case LAYER_TEX_SLOT_COLOR90:		CHECK(mlay);	texI = &(mlay->tex_col90);			tagname = "tex_refl90";			enabled = mlay->use_tex_col90;			break;
		case LAYER_TEX_SLOT_ROUGHNESS:		CHECK(mlay);	texI = &(mlay->tex_rough);			tagname = "tex_rough";			enabled = mlay->use_tex_rough;			break;
		case LAYER_TEX_SLOT_WEIGHT:			CHECK(mlay);	texI = &(mlay->tex_weight);			tagname = "tex_weight";			enabled = mlay->use_tex_weight;			break;
		case LAYER_TEX_SLOT_LIGHT:			CHECK(mlay);	texI = &(mlay->tex_light);			tagname = "tex_light";			enabled = mlay->use_tex_light;			break;
		case LAYER_TEX_SLOT_NORMAL:			CHECK(mlay);	texI = &(mlay->tex_normal);			tagname = "tex_normal";			enabled = mlay->use_tex_normal;			break;
		case LAYER_TEX_SLOT_TRANSM:			CHECK(mlay);	texI = &(mlay->tex_transm);			tagname = "tex_transm";			enabled = mlay->use_tex_transm;			break;
		case LAYER_TEX_SLOT_ANISO:			CHECK(mlay);	texI = &(mlay->tex_aniso);			tagname = "tex_aniso";			enabled = mlay->use_tex_aniso;			break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:	CHECK(mlay);	texI = &(mlay->tex_aniso_angle);	tagname = "tex_aniso_angle";	enabled = mlay->use_tex_aniso_angle;	break;
		case MAT_TEX_SLOT_OPACITY:			CHECK(mat);		texI = &(mat->tex_opacity);			tagname = "tex_opacity";		enabled = mat->use_tex_opacity;			break;
		case MAT_TEX_SLOT_DISPLACEMENT:		CHECK(mat);		texI = &(mat->tex_displacement);	tagname = "tex_displacement";	enabled = mat->use_tex_displacement;	break;
	}
	if (!texI)
		return false;

	// add <texture_slot_dependent_name>
	xmlNodePtr xTex = xmlNewChild(curNode, NULL, (xmlChar*)tagname, NULL);
	if (!xTex)
		return false;

	// add attribute type
	xmlAttrPtr xAttrType;
	xAttrType = xmlNewProp(xTex, (xmlChar*)"type", (xmlChar*)"file");
	if (!xAttrType)
		return false;

	// add attribute enabled
	xmlAttrPtr xAttrEnabled;
	xAttrEnabled = xmlNewProp(xTex, (xmlChar*)"enabled", (xmlChar*)(enabled ? "yes" : "no"));
	if (!xAttrEnabled)
		return false;

	// add <filename>
	if (texI->fullfilename)
	{
		char * fname = (texI->exportedFilename ? texI->exportedFilename : texI->fullfilename);
		xmlNodePtr xFilename = xmlNewChild(xTex, NULL, (xmlChar*)"filename", (xmlChar *)fname);
		if (!xFilename)
			return false;
	}

	// add <post>
	xmlNodePtr xPost = xmlNewChild(xTex, NULL, (xmlChar*)"post", (xmlChar *)NULL);
	if (!xPost)
		return false;

	// add <brightness>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.brightness);
	xmlNodePtr xBrightness = xmlNewChild(xPost, NULL, (xmlChar*)"brightness", (xmlChar *)buf);
	if (!xBrightness)
		return false;

	// add <contrast>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.contrast);
	xmlNodePtr xContrast = xmlNewChild(xPost, NULL, (xmlChar*)"contrast", (xmlChar *)buf);
	if (!xContrast)
		return false;

	// add <saturation>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.saturation);
	xmlNodePtr xSaturation = xmlNewChild(xPost, NULL, (xmlChar*)"saturation", (xmlChar *)buf);
	if (!xSaturation)
		return false;

	// add <hue>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.hue);
	xmlNodePtr xHue = xmlNewChild(xPost, NULL, (xmlChar*)"hue", (xmlChar *)buf);
	if (!xHue)
		return false;

	// add <red>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.red);
	xmlNodePtr xRed = xmlNewChild(xPost, NULL, (xmlChar*)"red", (xmlChar *)buf);
	if (!xRed)
		return false;

	// add <green>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.green);
	xmlNodePtr xGreen = xmlNewChild(xPost, NULL, (xmlChar*)"green", (xmlChar *)buf);
	if (!xGreen)
		return false;

	// add <blue>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.blue);
	xmlNodePtr xBlue = xmlNewChild(xPost, NULL, (xmlChar*)"blue", (xmlChar *)buf);
	if (!xBlue)
		return false;

	// add <gamma>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->texMod.gamma);
	xmlNodePtr xGamma = xmlNewChild(xPost, NULL, (xmlChar*)"gamma", (xmlChar *)buf);
	if (!xGamma)
		return false;

	// add <invert>
	sprintf_s(buf, 64, "%s", (texI->texMod.invert ? "yes" : "no"));
	xmlNodePtr xInvert = xmlNewChild(xPost, NULL, (xmlChar*)"invert", (xmlChar *)buf);
	if (!xInvert)
		return false;

	// add <filter>
	char * ffilter = texI->texMod.interpolateProbe ? "1" : "0";
	xmlNodePtr xFilter = xmlNewChild(xPost, NULL, (xmlChar*)"filter", (xmlChar *)ffilter);
	if (!xFilter)
		return false;

	// add <post_normal>
	xmlNodePtr xPostNorm = xmlNewChild(xTex, NULL, (xmlChar*)"post_normal", (xmlChar *)NULL);
	if (!xPostNorm)
		return false;

	// add <pn_invert_x>
	sprintf_s(buf, 64, "%s", (texI->normMod.invertX ? "yes" : "no"));
	xmlNodePtr xInvertX = xmlNewChild(xPostNorm, NULL, (xmlChar*)"pn_invert_x", (xmlChar *)buf);
	if (!xInvertX)
		return false;

	// add <pn_invert_y>
	sprintf_s(buf, 64, "%s", (texI->normMod.invertY ? "yes" : "no"));
	xmlNodePtr xInvertY = xmlNewChild(xPostNorm, NULL, (xmlChar*)"pn_invert_y", (xmlChar *)buf);
	if (!xInvertY)
		return false;

	// add <pn_power>
	_sprintf_s_l(buf, 64, "%.4f", noxLocale, texI->normMod.powerEV);
	xmlNodePtr xPower = xmlNewChild(xPostNorm, NULL, (xmlChar*)"pn_power", (xmlChar *)buf);
	if (!xPower)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertShadeLayer(xmlNodePtr curNode, MatLayer * mlay)
{
	if (!curNode   ||   !mlay)
		return false;

	xmlNodePtr xLayer = xmlNewChild(curNode, NULL, (xmlChar*)"layer", NULL);
	if (!xLayer)
		return false;

	// add type
	xmlAttrPtr xAttr;
	xAttr = xmlNewProp(xLayer, (xmlChar*)"type", (xmlChar*)"shade");
	if (!xAttr)
		return false;

	// add contribution
	char buf[128];
	sprintf_s(buf, 128, "%d", mlay->contribution);
	xAttr = xmlNewProp(xLayer, (xmlChar*)"contribution", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add <diffuse>
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->refl0.r * 255))), (unsigned char *)buf);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->refl0.g * 255))), (unsigned char *)buf+2);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->refl0.b * 255))), (unsigned char *)buf+4);
	buf[6] = 0;
	xmlNodePtr xDiff = xmlNewChild(xLayer, NULL, (xmlChar*)"diffuse", (xmlChar *)buf);
	if (!xDiff)
		return false;

	// add <diffuse90>
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->refl90.r * 255))), (unsigned char *)buf);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->refl90.g * 255))), (unsigned char *)buf+2);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->refl90.b * 255))), (unsigned char *)buf+4);
	buf[6] = 0;
	xmlNodePtr xDiff90 = xmlNewChild(xLayer, NULL, (xmlChar*)"diffuse90", (xmlChar *)buf);
	if (!xDiff90)
		return false;

	// add connect90
	xmlNodePtr xC90 = xmlNewChild(xLayer, NULL, (xmlChar*)"connect90", (xmlChar *)NULL);
	if (mlay->connect90)
		xAttr = xmlNewProp(xC90, (xmlChar*)"active", (xmlChar*)"yes");
	else
		xAttr = xmlNewProp(xC90, (xmlChar*)"active", (xmlChar*)"no");
	if (!xAttr)
		return false;

	// add roughness
	_sprintf_s_l(buf, 128, "%.2f", noxLocale, min(100, max(0, mlay->roughness*100.0f)));
	xmlNodePtr xRough = xmlNewChild(xLayer, NULL, (xmlChar*)"roughness", (xmlChar *)buf);
	if (!xRough)
		return false;

	// add anisotropy
	_sprintf_s_l(buf, 128, "%.2f", noxLocale, min(100, max(0, mlay->anisotropy*100.0f)));
	xmlNodePtr xAniso = xmlNewChild(xLayer, NULL, (xmlChar*)"anisotropy", (xmlChar *)buf);
	if (!xAniso)
		return false;

	// add anisotropy_angle
	_sprintf_s_l(buf, 128, "%.2f", noxLocale, mlay->aniso_angle);
	xmlNodePtr xAnisoAngle = xmlNewChild(xLayer, NULL, (xmlChar*)"anisotropy_angle", (xmlChar *)buf);
	if (!xAnisoAngle)
		return false;

	if (mlay->tex_col0.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_COLOR0))
			return false;
	}
	if (mlay->tex_col90.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_COLOR90))
			return false;
	}
	if (mlay->tex_rough.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_ROUGHNESS))
			return false;
	}
	if (mlay->tex_weight.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_WEIGHT))
			return false;
	}
	if (mlay->tex_transm.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_TRANSM))
			return false;
	}
	if (mlay->tex_aniso.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_ANISO))
			return false;
	}
	if (mlay->tex_aniso_angle.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_ANISO_ANGLE))
			return false;
	}
	if (mlay->tex_light.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_LIGHT))
			return false;
	}
	if (mlay->tex_normal.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_NORMAL))
			return false;
	}

	// add transmission
	xmlNodePtr xTr = xmlNewChild(xLayer, NULL, (xmlChar*)"transmission", (xmlChar *)NULL);
	if (mlay->transmOn)
		xAttr = xmlNewProp(xTr, (xmlChar*)"active", (xmlChar*)"yes");
	else
		xAttr = xmlNewProp(xTr, (xmlChar*)"active", (xmlChar*)"no");
	if (!xAttr)
		return false;

	// add transmcolor
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->transmColor.r * 255))), (unsigned char *)buf);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->transmColor.g * 255))), (unsigned char *)buf+2);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->transmColor.b * 255))), (unsigned char *)buf+4);
	buf[6] = 0;
	xmlNodePtr xTrc= xmlNewChild(xTr, NULL, (xmlChar*)"transmcolor", (xmlChar *)buf);
	if (!xTrc)
		return false;


	// add fakeglass
	xmlNodePtr xFake = xmlNewChild(xTr, NULL, (xmlChar*)"fakeglass", (xmlChar *)(mlay->fakeGlass ? "yes" : "no"));
	if (!xFake)
		return false;

	// add absorption
	xmlNodePtr xAbs = xmlNewChild(xTr, NULL, (xmlChar*)"absorption", (xmlChar *)NULL);
	if (mlay->absOn)
		xAttr = xmlNewProp(xAbs, (xmlChar*)"active", (xmlChar*)"yes");
	else
		xAttr = xmlNewProp(xAbs, (xmlChar*)"active", (xmlChar*)"no");
	if (!xAttr)
		return false;

	// add abscolor
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->absColor.r * 255))), (unsigned char *)buf);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->absColor.g * 255))), (unsigned char *)buf+2);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->absColor.b * 255))), (unsigned char *)buf+4);
	buf[6] = 0;
	xmlNodePtr xAbsc= xmlNewChild(xAbs, NULL, (xmlChar*)"abscolor", (xmlChar *)buf);
	if (!xAbsc)
		return false;

	// add distance
	_sprintf_s_l(buf, 128, "%1.3f", noxLocale, mlay->absDist);
	xmlNodePtr xAbsd = xmlNewChild(xAbs, NULL, (xmlChar*)"distance", (xmlChar *)buf);
	if (!xAbsd)
		return false;

	// add dispersion_on
	xmlNodePtr xDispOn = NULL;
	if (mlay->dispersionOn)
		xDispOn = xmlNewChild(xTr, NULL, (xmlChar*)"dispersion_on", (xmlChar *)"yes");
	else
		xDispOn = xmlNewChild(xTr, NULL, (xmlChar*)"dispersion_on", (xmlChar *)"no");
	if (!xDispOn)
		return false;

	// add dispersion_val
	float disp = mlay->dispersionValue;
	_sprintf_s_l(buf, 128, "%1.3f", noxLocale, disp);
	xmlNodePtr xDispVal = xmlNewChild(xTr, NULL, (xmlChar*)"dispersion_val", (xmlChar *)buf);
	if (!xDispVal)
		return false;

	// add sss
	xmlNodePtr xSSS = xmlNewChild(xLayer, NULL, (xmlChar*)"sss", (xmlChar *)NULL);
	if (mlay->sssON)
		xAttr = xmlNewProp(xSSS, (xmlChar*)"active", (xmlChar*)"yes");
	else
		xAttr = xmlNewProp(xSSS, (xmlChar*)"active", (xmlChar*)"no");
	if (!xAttr)
		return false;

	// add sss_density
	_sprintf_s_l(buf, 128, "%1.3f", noxLocale, mlay->sssDens);
	xmlNodePtr xSssDens = xmlNewChild(xSSS, NULL, (xmlChar*)"sss_density", (xmlChar *)buf);
	if (!xSssDens)
		return false;

	// add sss_coll_dir
	_sprintf_s_l(buf, 128, "%1.3f", noxLocale, mlay->sssCollDir);
	xmlNodePtr xSssCollDir = xmlNewChild(xSSS, NULL, (xmlChar*)"sss_coll_dir", (xmlChar *)buf);
	if (!xSssCollDir)
		return false;

	// add fresnel
	if (!insertFresnel(xLayer, &mlay->fresnel))
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------


bool XMLScene::insertEmissionLayer(xmlNodePtr curNode, MatLayer * mlay)
{
	if (!curNode   ||   !mlay)
		return false;

	xmlNodePtr xLayer = xmlNewChild(curNode, NULL, (xmlChar*)"layer", NULL);
	if (!xLayer)
		return false;

	// add type
	xmlAttrPtr xAttr;
	xAttr = xmlNewProp(xLayer, (xmlChar*)"type", (xmlChar*)"emission");
	if (!xAttr)
		return false;

	// add contribution
	char buf[128];
	sprintf_s(buf, 128, "%d", mlay->contribution);
	xAttr = xmlNewProp(xLayer, (xmlChar*)"contribution", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add <emission>
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->color.r * 255))), (unsigned char *)buf);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->color.g * 255))), (unsigned char *)buf+2);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, mlay->color.b * 255))), (unsigned char *)buf+4);
	buf[6] = 0;
	xmlNodePtr xEmission = xmlNewChild(xLayer, NULL, (xmlChar*)"emission", (xmlChar *)buf);
	if (!xEmission)
		return false;

	if (mlay->tex_light.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_LIGHT))
			return false;
	}
	if (mlay->tex_weight.fullfilename)
	{
		if (!insertTexture(xLayer, NULL, mlay, LAYER_TEX_SLOT_WEIGHT))
			return false;
	}

	// add power
	DecHex::floatToHex(mlay->power, (unsigned char *)buf);
	buf[8] = 0;
	xmlNodePtr xPower = xmlNewChild(xLayer, NULL, (xmlChar*)"power", (xmlChar *)buf);
	if (!xPower)
		return false;

	// add unit
	char * uu = NULL;
	switch(mlay->unit)
	{
		case MatLayer::EMISSION_WATT:					uu = "w";		break;
		case MatLayer::EMISSION_WATT_PER_SQR_METER:		uu = "w/m2";	break;
		case MatLayer::EMISSION_LUMEN:					uu = "lm";		break;
		case MatLayer::EMISSION_LUX:					uu = "lx";		break;
		default:										uu = "w";		break;
	}
	xmlNodePtr xUnit = xmlNewChild(xLayer, NULL, (xmlChar*)"unit", (xmlChar *)uu);
	if (!xPower)
		return false;

	// add ies
	if (mlay->ies  &&  mlay->ies->isValid())
	{
		char * ffname;
		if (mlay->ies->exportedFilename)
			ffname = mlay->ies->exportedFilename;
		else
			ffname = mlay->ies->filename;

		xmlNodePtr xIES= xmlNewChild(xLayer, NULL, (xmlChar*)"ies", (xmlChar *)ffname);
		if (!xIES)
			return false;
	}

	// add temperature
	sprintf_s(buf, 128, "%d", mlay->emitterTemperature);
	xmlNodePtr xTemperature = xmlNewChild(xLayer, NULL, (xmlChar*)"temperature", (xmlChar *)buf);
	if (!xTemperature)
		return false;

	// add invisible
	xmlNodePtr xInvisible = xmlNewChild(xLayer, NULL, (xmlChar*)"invisible", (xmlChar *)NULL);
	if (mlay->emissInvisible)
		xAttr = xmlNewProp(xInvisible, (xmlChar*)"active", (xmlChar*)"yes");
	else
		xAttr = xmlNewProp(xInvisible, (xmlChar*)"active", (xmlChar*)"no");
	if (!xAttr)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertMatLayer(xmlNodePtr curNode, MatLayer * mlay)
{
	if (!curNode   ||   !mlay)
		return false;

	switch (mlay->type)
	{
		case MatLayer::TYPE_EMITTER:
			return insertEmissionLayer(curNode, mlay);
		case MatLayer::TYPE_SHADE:
			return insertShadeLayer(curNode, mlay);
		default:
			return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertPreview(xmlNodePtr curNode, ImageByteBuffer * preview)
{
	if (!curNode   ||   !preview)
		return false;

	if (preview->height < 1   ||   preview->width < 1   ||   !preview->imgBuf)
		return false;

	// add preview
	xmlNodePtr xPreview = xmlNewChild(curNode, NULL, (xmlChar*)"preview", NULL);
	if (!xPreview)
		return false;

	// add width
	xmlAttrPtr xAttr;
	char buf[32];
	sprintf_s(buf, 32, "%d", preview->width);
	xAttr = xmlNewProp(xPreview, (xmlChar*)"width", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add height
	sprintf_s(buf, 32, "%d", preview->height);
	xAttr = xmlNewProp(xPreview, (xmlChar*)"height", (xmlChar*)buf);
	if (!xAttr)
		return false;

	int bsize = 6*preview->width+1;
	unsigned char * lbuf = (unsigned char *)malloc(bsize);
	if (!lbuf)
		return false;

	// add lines
	for (int i=0; i<preview->height; i++)
	{
		sprintf_s(buf, 32, "l%d", i);
		for (int j=0; j<preview->width; j++)
		{
			DecHex::ucharToHex2BigEndian(GetRValue(preview->imgBuf[i][j]), &(lbuf[j*6+0]));
			DecHex::ucharToHex2BigEndian(GetGValue(preview->imgBuf[i][j]), &(lbuf[j*6+2]));
			DecHex::ucharToHex2BigEndian(GetBValue(preview->imgBuf[i][j]), &(lbuf[j*6+4]));
		}
		lbuf[bsize-1] = 0;

		// add line
		xmlNodePtr xLine = xmlNewChild(xPreview, NULL, (xmlChar*)buf, (xmlChar*)lbuf);
		if (!xLine)
		{
			free(lbuf);
			return false;
		}
	}

	free(lbuf);
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertUserColors(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	// add tri
	xmlNodePtr xUser = xmlNewChild(curNode, NULL, (xmlChar*)"user_colors", NULL);
	if (!xUser)
		return false;

	char name[32];
	unsigned char buf[32];
	for (int i=0; i<18; i++)
	{
		sprintf_s(name, "color%d", (i+1));

		DecHex::floatToHex(UserColors::colors[i].r, buf+0);
		DecHex::floatToHex(UserColors::colors[i].g, buf+8);
		DecHex::floatToHex(UserColors::colors[i].b, buf+16);
		buf[24] = 0;

		xmlNodePtr xColor = xmlNewChild(xUser, NULL, (xmlChar*)name, (xmlChar*)buf);
		if (!xColor)
			return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertBlendSettings(xmlNodePtr curNode, BlendSettings &blendSettings, bool onlyNames)
{
	CHECK(curNode);

	xmlNodePtr xBlend = curNode;

	char bufname[64];
	char bufvalue[64];
	for (int i=0; i<16; i++)
	{
		sprintf_s(bufname,  64, "name%d", (i+1));
		char * tname = blendSettings.names[i];
		if (tname)
		{
			xmlNodePtr xName = xmlNewChild(xBlend, NULL, (xmlChar*)bufname, (xmlChar*)tname);
			if (!xName)
				return false;
		}
	}

	if (onlyNames)
		return true;

	for (int i=0; i<16; i++)
	{
		sprintf_s(bufname,  64, "on%d", (i+1));
		if (blendSettings.blendOn[i])
			sprintf_s(bufvalue, 64, "yes");
		else
			sprintf_s(bufvalue, 64, "no");
		xmlNodePtr xOn = xmlNewChild(xBlend, NULL, (xmlChar*)bufname, (xmlChar*)bufvalue);
		if (!xOn)
			return false;
	}

	for (int i=0; i<16; i++)
	{
		sprintf_s(bufname,  64, "weight%d", (i+1));
		_sprintf_s_l(bufvalue, 64, "%.3f", noxLocale, (blendSettings.weights[i]*100.0f));
		xmlNodePtr xWeight = xmlNewChild(xBlend, NULL, (xmlChar*)bufname, (xmlChar*)bufvalue);
		if (!xWeight)
			return false;
	}

	for (int i=0; i<16; i++)
	{
		sprintf_s(bufname,  64, "red%d", (i+1));
		_sprintf_s_l(bufvalue, 64, "%.3f", noxLocale, (blendSettings.red[i]*100.0f));
		xmlNodePtr xRed = xmlNewChild(xBlend, NULL, (xmlChar*)bufname, (xmlChar*)bufvalue);
		if (!xRed)
			return false;
	}

	for (int i=0; i<16; i++)
	{
		sprintf_s(bufname,  64, "green%d", (i+1));
		_sprintf_s_l(bufvalue, 64, "%.3f", noxLocale, (blendSettings.green[i]*100.0f));
		xmlNodePtr xGreen = xmlNewChild(xBlend, NULL, (xmlChar*)bufname, (xmlChar*)bufvalue);
		if (!xGreen)
			return false;
	}

	for (int i=0; i<16; i++)
	{
		sprintf_s(bufname,  64, "blue%d", (i+1));
		_sprintf_s_l(bufvalue, 64, "%.3f", noxLocale, (blendSettings.blue[i]*100.0f));
		xmlNodePtr xBlue= xmlNewChild(xBlend, NULL, (xmlChar*)bufname, (xmlChar*)bufvalue);
		if (!xBlue)
			return false;
	}

	return true;
}

//=============================================================================================================

bool XMLScene::saveScene(char * filename, Scene * scene, bool addCamBuffers)
{
	if (!filename   ||   !scene)
		return false;

	char buf[160];
	xmlDocPtr xmlDoc;
	xmlDoc = xmlNewDoc((xmlChar*)"1.0");
	if (!xmlDoc)
	{
		return false;
	}

	addCamBuffs = addCamBuffers;

	xmlNodePtr xRoot = xmlNewDocNode(xmlDoc, NULL, (xmlChar*)"scene", NULL);
	if (!xRoot)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	xmlDocSetRootElement(xmlDoc, xRoot);

	// add objects
	xmlNodePtr xObjects = xmlNewChild(xRoot, NULL, (xmlChar*)"objects", NULL);
	if (!xObjects)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	// add totalTris
	xmlAttrPtr xAttr;
	sprintf_s(buf, 160, "%d", scene->triangles.objCount);
	xAttr = xmlNewProp(xObjects, (xmlChar*)"totalTris", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add scale
	xmlAttrPtr xScale;
	_sprintf_s_l(buf, 160, "%.3f", noxLocale, scene->sscene.scaleScene);
	xScale = xmlNewProp(xObjects, (xmlChar*)"scale", (xmlChar*)buf);
	if (!xScale)
		return false;

	// add meshes
	for (int i=0; i<scene->meshes.objCount; i++)
	{
		Mesh * m = &(scene->meshes[i]);
		if (m->displacement_instance_id > -1)
			continue;

		// add mesh_src
		xmlNodePtr xMesh = xmlNewChild(xObjects, NULL, (xmlChar*)"mesh_src", NULL);
		if (!xMesh)
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
		xmlAttrPtr xAttr;
		sprintf_s(buf, 160, "%d", m->instSrcID);
		xAttr = xmlNewProp(xMesh, (xmlChar*)"id", (xmlChar*)buf);
		if (!xAttr)
			return false;

		// add name
		if (m->name)
		{
			xmlNodePtr xName = xmlNewChild(xMesh, NULL, (xmlChar*)"name", (xmlChar*)m->name);
			if (!xName)
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// add triangles
		int nTris = m->lastTri - m->firstTri + 1;
		for (int j=m->firstTri; j<=m->lastTri; j++)
		{
			if (!insertTriangle(xMesh , &(scene->triangles[j])))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}
	}

	// add instances
	for (int i=0; i<scene->instances.objCount; i++)
	{
		MeshInstance * inst = &(scene->instances[i]);

		// add instance
		xmlNodePtr xInst = xmlNewChild(xObjects, NULL, (xmlChar*)"instance", NULL);
		if (!xInst)
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
		xmlAttrPtr xAttr;
		sprintf_s(buf, 160, "%d", inst->instSrcID);
		xAttr = xmlNewProp(xInst, (xmlChar*)"id", (xmlChar*)buf);
		if (!xAttr)
			return false;

		// add name
		if (inst->name)
		{
			xmlNodePtr xName = xmlNewChild(xInst, NULL, (xmlChar*)"name", (xmlChar*)inst->name);
			if (!xName)
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// add matrix
		Matrix4d m = inst->matrix_transform;
		for (int i=0; i<4; i++)
			for (int j=0; j<4; j++)
			{
				DecHex::floatToHex(m.M[i][j], (unsigned char *)buf+(i*4+j)*8);
			}
		buf[128] = 0;
		xmlNodePtr xMatrix = xmlNewChild(xInst, NULL, (xmlChar*)"matrix", (xmlChar*)buf);
		if (!xMatrix)
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}

		// add materials
		int nummats = inst->matsFromFile.objCount;
		int b2size = 8*nummats + 10;
		char * buf2 = (char*)malloc(b2size);
		for (int i=0; i<nummats; i++)
			DecHex::uintToHex(inst->matsFromFile[i], (unsigned char *)buf2+i*8);
		buf2[nummats*8] = 0;
		xmlNodePtr xMats = xmlNewChild(xInst, NULL, (xmlChar*)"materials", (xmlChar*)buf2);
		free(buf2);
		if (!xMats)
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
		sprintf_s(buf, 160, "%d", nummats);
		xAttr = xmlNewProp(xMats, (xmlChar*)"count", (xmlChar*)buf);
		if (!xAttr)
			return false;
	}

	// add materials
	xmlNodePtr xMats = xmlNewChild(xRoot, NULL, (xmlChar*)"materials", NULL);
	if (!xMats)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	for (int j=0; j<scene->mats.objCount; j++)
	{
		xmlNodePtr xMaterial = xmlNewChild(xMats, NULL, (xmlChar*)"material", NULL);
		if (!xMaterial)
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}

		if (!insertMaterialData(xMaterial , scene->mats[j]))//, NULL))
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
	}

	// user colors
	if (!insertUserColors(xRoot))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	// add blend
	xmlNodePtr xBlend = xmlNewChild(xRoot, NULL, (xmlChar*)"blend", NULL);
	if (!xBlend)
		return false;
	if (!insertBlendSettings(xBlend, scene->blendSettings))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	// renderer timers
	if (!insertTimers(xRoot))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	// create directory for rest of data
	char dirRelative[2048];
	char dirFull[2048];
	char * oFile = strrchr(filename, '\\');
	if (oFile)
		sprintf_s(dirRelative, 2048, "%s data", oFile+1);
	else
		sprintf_s(dirRelative, 2048, "%s data", filename);
	sprintf_s(dirFull,     2048, "%s data", filename);
	if (!dirExists(dirFull))
	{
		if (addCamBuffs)
		{
			if (!CreateDirectory(dirFull, NULL))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}
	}

	// add cameras
	for (int j=0; j<scene->cameras.objCount; j++)
	{
		bool active = (scene->sscene.activeCamera == j);
		if (!insertCamera(xRoot , scene->cameras[j], active, dirRelative, dirFull, j, scene->nowRendering))		///////////////////////////////////
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
	}

	// add sun and sky
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (sc)
	{
		SunSky * sunsky = sc->sscene.sunsky;
		if (!insertSky(xRoot, sunsky, sc->sscene.useSunSky))
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
	}

	// add presets
	if (!insertUserPresets(xRoot))
	{
		return false;
	}

	// add scene info
	if (!insertSceneInfo(xRoot))
	{
		return false;
	}

	// save
	int written = xmlSaveFormatFile(filename, xmlDoc, 1);
	xmlFreeDoc(xmlDoc);

	if (written < 1)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertTriangle(xmlNodePtr curNode, Triangle * tri)
{
	if (!curNode   ||   !tri)
		return false;

	unsigned char buf[512];

	DecHex::uintToHex(tri->matInInst, buf);
	DecHex::uintToHex(0, buf+8);
	DecHex::floatToHex(tri->V1.x, buf+16);
	DecHex::floatToHex(tri->V1.y, buf+24);
	DecHex::floatToHex(tri->V1.z, buf+32);
	DecHex::floatToHex(tri->V2.x, buf+40);
	DecHex::floatToHex(tri->V2.y, buf+48);
	DecHex::floatToHex(tri->V2.z, buf+56);
	DecHex::floatToHex(tri->V3.x, buf+64);
	DecHex::floatToHex(tri->V3.y, buf+72);
	DecHex::floatToHex(tri->V3.z, buf+80);
	DecHex::floatToHex(tri->N1.x, buf+88);
	DecHex::floatToHex(tri->N1.y, buf+96);
	DecHex::floatToHex(tri->N1.z, buf+104);
	DecHex::floatToHex(tri->N2.x, buf+112);
	DecHex::floatToHex(tri->N2.y, buf+120);
	DecHex::floatToHex(tri->N2.z, buf+128);
	DecHex::floatToHex(tri->N3.x, buf+136);
	DecHex::floatToHex(tri->N3.y, buf+144);
	DecHex::floatToHex(tri->N3.z, buf+152);
	DecHex::floatToHex(tri->UV1.x, buf+160);
	DecHex::floatToHex(tri->UV1.y, buf+168);
	DecHex::floatToHex(tri->UV2.x, buf+176);
	DecHex::floatToHex(tri->UV2.y, buf+184);
	DecHex::floatToHex(tri->UV3.x, buf+192);
	DecHex::floatToHex(tri->UV3.y, buf+200);

	buf[208] = 0;

	// add tri
	xmlNodePtr xTri = xmlNewChild(curNode, NULL, (xmlChar*)"tri", (xmlChar*)buf);
	if (!xTri)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertCamera(xmlNodePtr curNode, Camera * cam, bool active, char * dirRelative, char * dirFull, int camNum, bool nowRendering)
{
	if (!curNode   ||   !cam   ||   !dirRelative   ||   !dirFull)
		return false;

	// add <camera>
	xmlNodePtr xCamera = xmlNewChild(curNode, NULL, (xmlChar*)"camera", NULL);
	if (!xCamera)
		return false;

	// add width
	char buf[64];
	xmlAttrPtr xAttr;
	sprintf_s(buf, 64, "%d", cam->width);
	xAttr = xmlNewProp(xCamera, (xmlChar*)"width", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add height
	sprintf_s(buf, 64, "%d", cam->height);
	xAttr = xmlNewProp(xCamera, (xmlChar*)"height", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add antialiasing
	sprintf_s(buf, 64, "%d", cam->aa);
	xAttr = xmlNewProp(xCamera, (xmlChar*)"antialiasing", (xmlChar*)buf);
	if (!xAttr)
		return false;

	if (active)
	{
		// add active
		xmlAttrPtr xAttr;
		xAttr = xmlNewProp(xCamera, (xmlChar*)"active", (xmlChar*)"yes");
		if (!xAttr)
			return false;
	}

	// add name
	if (cam->name)
	{
		xmlNodePtr xName = xmlNewChild(xCamera, NULL, (xmlChar*)"name", (xmlChar*)cam->name);
		if (!xName)
			return false;
	}

	// add pos
	DecHex::floatToHex(cam->position.x, (unsigned char*)buf+0);
	DecHex::floatToHex(cam->position.y, (unsigned char*)buf+8);
	DecHex::floatToHex(cam->position.z, (unsigned char*)buf+16);
	buf[24]=0;
	xmlNodePtr xPos = xmlNewChild(xCamera, NULL, (xmlChar*)"pos", (xmlChar*)buf);
	if (!xPos)
		return false;

	// add dir
	DecHex::floatToHex(cam->direction.x, (unsigned char*)buf+0);
	DecHex::floatToHex(cam->direction.y, (unsigned char*)buf+8);
	DecHex::floatToHex(cam->direction.z, (unsigned char*)buf+16);
	buf[24]=0;
	xmlNodePtr xDir = xmlNewChild(xCamera, NULL, (xmlChar*)"dir", (xmlChar*)buf);
	if (!xDir)
		return false;

	// add updir
	DecHex::floatToHex(cam->upDir.x, (unsigned char*)buf+0);
	DecHex::floatToHex(cam->upDir.y, (unsigned char*)buf+8);
	DecHex::floatToHex(cam->upDir.z, (unsigned char*)buf+16);
	buf[24]=0;
	xmlNodePtr xUp = xmlNewChild(xCamera, NULL, (xmlChar*)"updir", (xmlChar*)buf);
	if (!xUp)
		return false;

	// add rightdir
	DecHex::floatToHex(cam->rightDir.x, (unsigned char*)buf+0);
	DecHex::floatToHex(cam->rightDir.y, (unsigned char*)buf+8);
	DecHex::floatToHex(cam->rightDir.z, (unsigned char*)buf+16);
	buf[24]=0;
	xmlNodePtr xRight = xmlNewChild(xCamera, NULL, (xmlChar*)"rightdir", (xmlChar*)buf);
	if (!xRight)
		return false;

	// add angle
	DecHex::floatToHex(cam->angle, (unsigned char*)buf);
	buf[8]=0;
	xmlNodePtr xAngle = xmlNewChild(xCamera, NULL, (xmlChar*)"angle", (xmlChar*)buf);
	if (!xAngle)
		return false;

	// add <iso>
	_sprintf_s_l(buf, 64, "%.2f", noxLocale, cam->ISO);
	xmlNodePtr xISO = xmlNewChild(xCamera, NULL, (xmlChar*)"iso", (xmlChar*)buf);
	if (!xISO)
		return false;

	// add <aperture>
	_sprintf_s_l(buf, 64, "%.2f", noxLocale, cam->aperture);
	xmlNodePtr xAperture = xmlNewChild(xCamera, NULL, (xmlChar*)"aperture", (xmlChar*)buf);
	if (!xAperture)
		return false;

	// add <shutter>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->shutter);
	xmlNodePtr xShutter = xmlNewChild(xCamera, NULL, (xmlChar*)"shutter", (xmlChar*)buf);
	if (!xShutter)
		return false;

	// add <autoexposure>
	char * autoexp = cam->autoexposure ? "yes" : "no";
	xmlNodePtr xAutoExp = xmlNewChild(xCamera, NULL, (xmlChar*)"autoexposure", (xmlChar*)autoexp);
	if (!xAutoExp)
		return false;

	// add <autoexptype>
	sprintf_s(buf, 64, "%d", cam->autoexposureType);
	xmlNodePtr xAutoExpType = xmlNewChild(xCamera, NULL, (xmlChar*)"autoexptype", (xmlChar*)buf);
	if (!xAutoExpType)
		return false;

	// add <bladesnumber>
	sprintf_s(buf, 64, "%d", cam->apBladesNum);
	xmlNodePtr xBlNum = xmlNewChild(xCamera, NULL, (xmlChar*)"bladesnumber", (xmlChar*)buf);
	if (!xBlNum)
		return false;

	// add <bladesangle>
	_sprintf_s_l(buf, 64, "%.2f", noxLocale, cam->apBladesAngle);
	xmlNodePtr xBlAngle = xmlNewChild(xCamera, NULL, (xmlChar*)"bladesangle", (xmlChar*)buf);
	if (!xBlAngle)
		return false;

	// add <bladesround>
	xmlNodePtr xBlRound;
	if (cam->apBladesRound)
		xBlRound = xmlNewChild(xCamera, NULL, (xmlChar*)"bladesround", (xmlChar*)"yes");
	else
		xBlRound = xmlNewChild(xCamera, NULL, (xmlChar*)"bladesround", (xmlChar*)"no");
	if (!xBlRound)
		return false;

	// add <bladesradius>
	_sprintf_s_l(buf, 64, "%.2f", noxLocale, cam->apBladesRadius);
	xmlNodePtr xBlRadius = xmlNewChild(xCamera, NULL, (xmlChar*)"bladesradius", (xmlChar*)buf);
	if (!xBlRadius)
		return false;

	// add <dof_on>
	char * dofON = cam->dofOnTemp ? "yes" : "no";
	xmlNodePtr xDOF = xmlNewChild(xCamera, NULL, (xmlChar*)"dof_on", (xmlChar*)dofON);
	if (!xDOF)
		return false;

	// add <bokeh_ring_balance>
	sprintf_s(buf, 64, "%d", cam->diaphSampler.intBokehRingBalance);
	xmlNodePtr xBokehRingBalance = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_ring_balance", (xmlChar*)buf);
	if (!xBokehRingBalance)
		return false;

	// add <bokeh_ring_size>
	sprintf_s(buf, 64, "%d", cam->diaphSampler.intBokehRingSize);
	xmlNodePtr xBokehRingSize = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_ring_size", (xmlChar*)buf);
	if (!xBokehRingSize)
		return false;
	
	// add <bokeh_vignette>
	sprintf_s(buf, 64, "%d", cam->diaphSampler.intBokehVignette);
	xmlNodePtr xBokehRingVignette = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_vignette", (xmlChar*)buf);
	if (!xBokehRingVignette)
		return false;

	// add <bokeh_flatten>
	sprintf_s(buf, 64, "%d", cam->diaphSampler.intBokehFlatten);
	xmlNodePtr xBokehRingFlatten = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_flatten", (xmlChar*)buf);
	if (!xBokehRingFlatten)
		return false;

	// add <diffraction_obstacle>
	if (char * c = cam->texObstacle.fullfilename)
	{
		xmlNodePtr xObst = xmlNewChild(xCamera , NULL, (xmlChar*)"diffraction_obstacle", (xmlChar *)c);
		if (!xObst)
			return false;
	}

	// add <diffraction_diaphragm>
	if (char * c = cam->texDiaphragm.fullfilename)
	{
		xmlNodePtr xDiaph = xmlNewChild(xCamera, NULL, (xmlChar*)"diffraction_diaphragm", (xmlChar *)c);
		if (!xDiaph)
			return false;
	}

	// add <autofocus>
	char * autofocus = cam->autoFocus ? "yes" : "no";
	xmlNodePtr xAutoFocus = xmlNewChild(xCamera, NULL, (xmlChar*)"autofocus", (xmlChar*)autofocus);
	if (!xAutoFocus)
		return false;

	// add <focusdist>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->focusDist);
	xmlNodePtr xFocusDist = xmlNewChild(xCamera, NULL, (xmlChar*)"focusdist", (xmlChar*)buf);
	if (!xFocusDist)
		return false;

	// add <iso_post>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getMultiplier()*100);
	xmlNodePtr xISOpost = xmlNewChild(xCamera, NULL, (xmlChar*)"iso_post", (xmlChar*)buf);
	if (!xISOpost)
		return false;

	// add <iso_ev_correction>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getISO_EV_compensation());
	xmlNodePtr xISOev = xmlNewChild(xCamera, NULL, (xmlChar*)"iso_ev_correction", (xmlChar*)buf);
	if (!xISOev)
		return false;

	// add <tone>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getToneMappingValue());
	xmlNodePtr xTone = xmlNewChild(xCamera, NULL, (xmlChar*)"tone", (xmlChar*)buf);
	if (!xTone)
		return false;

	// add <gamma>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getGamma());
	xmlNodePtr xGamma = xmlNewChild(xCamera, NULL, (xmlChar*)"gamma", (xmlChar*)buf);
	if (!xGamma)
		return false;

	// add <brightness>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getBrightness());
	xmlNodePtr xBrightness = xmlNewChild(xCamera, NULL, (xmlChar*)"brightness", (xmlChar*)buf);
	if (!xBrightness)
		return false;

	// add <contrast>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getContrast());
	xmlNodePtr xContrast= xmlNewChild(xCamera, NULL, (xmlChar*)"contrast", (xmlChar*)buf);
	if (!xContrast)
		return false;

	// add <saturation>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getSaturation());
	xmlNodePtr xSaturation = xmlNewChild(xCamera, NULL, (xmlChar*)"saturation", (xmlChar*)buf);
	if (!xSaturation)
		return false;

	// add <addred>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getR());
	xmlNodePtr xAddRed = xmlNewChild(xCamera, NULL, (xmlChar*)"addred", (xmlChar*)buf);
	if (!xAddRed)
		return false;

	// add <addgreen>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getG());
	xmlNodePtr xAddGreen = xmlNewChild(xCamera, NULL, (xmlChar*)"addgreen", (xmlChar*)buf);
	if (!xAddGreen)
		return false;

	// add <addblue>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, cam->iMod.getB());
	xmlNodePtr xAddBlue = xmlNewChild(xCamera, NULL, (xmlChar*)"addblue", (xmlChar*)buf);
	if (!xAddBlue)
		return false;

	// add <temperature>
	sprintf_s(buf, 64, "%d", (int)cam->iMod.getTemperature());
	xmlNodePtr xTemperature = xmlNewChild(xCamera, NULL, (xmlChar*)"temperature", (xmlChar*)buf);
	if (!xTemperature)
		return false;
	
	// add <vignette>
	sprintf_s(buf, 64, "%d", cam->iMod.getVignette());
	xmlNodePtr xVignette = xmlNewChild(xCamera, NULL, (xmlChar*)"vignette", (xmlChar*)buf);
	if (!xVignette)
		return false;

	// add <response_function>
	sprintf_s(buf, 64, "%d", cam->iMod.getResponseFunctionNumber());
	xmlNodePtr xResponse = xmlNewChild(xCamera, NULL, (xmlChar*)"response_function", (xmlChar*)buf);
	if (!xResponse)
		return false;

	// add <filter>
	sprintf_s(buf, 64, "%d", (int)cam->iMod.getFilter());
	xmlNodePtr xFilter = xmlNewChild(xCamera, NULL, (xmlChar*)"filter", (xmlChar*)buf);
	if (!xFilter)
		return false;

	// add <curve>
	xmlNodePtr xCurve = xmlNewChild(xCamera, NULL, (xmlChar*)"curve", NULL);
	if (!xCurve)
		return false;

		// add <cr> <cg> <cb> <cl>
		bool cr,cg,cb,cl;
		cam->iMod.getCurveCheckboxes(cr,cg,cb,cl);
		xmlNodePtr xCR, xCG, xCB, xCL;
		if (cr)
			xCR = xmlNewChild(xCurve, NULL, (xmlChar*)"cr", (xmlChar*)"yes");
		else
			xCR = xmlNewChild(xCurve, NULL, (xmlChar*)"cr", (xmlChar*)"no");
		if (cg)
			xCG = xmlNewChild(xCurve, NULL, (xmlChar*)"cg", (xmlChar*)"yes");
		else
			xCG = xmlNewChild(xCurve, NULL, (xmlChar*)"cg", (xmlChar*)"no");
		if (cb)
			xCB = xmlNewChild(xCurve, NULL, (xmlChar*)"cb", (xmlChar*)"yes");
		else
			xCB = xmlNewChild(xCurve, NULL, (xmlChar*)"cb", (xmlChar*)"no");
		if (cl)
			xCL = xmlNewChild(xCurve, NULL, (xmlChar*)"cl", (xmlChar*)"yes");
		else
			xCL = xmlNewChild(xCurve, NULL, (xmlChar*)"cl", (xmlChar*)"no");
		if (!xCR   ||   !xCG   ||   !xCB   ||   !xCL)
			return false;

		xmlNodePtr xCanal, xCanalX, xCanalY;
		int x,y;

		// add <r>
		for (int i=0; i<cam->iMod.getCurveRedPointsCount(); i++)
		{
			cam->iMod.getCurveRedPoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"r", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

		// add <g>
		for (int i=0; i<cam->iMod.getCurveGreenPointsCount(); i++)
		{
			cam->iMod.getCurveGreenPoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"g", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

		// add <b>
		for (int i=0; i<cam->iMod.getCurveBluePointsCount(); i++)
		{
			cam->iMod.getCurveBluePoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"b", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

		// add <l>
		for (int i=0; i<cam->iMod.getCurveLuminancePointsCount(); i++)
		{
			cam->iMod.getCurveLuminancePoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"l", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

	xmlNodePtr xFinal = xmlNewChild(xCamera, NULL, (xmlChar*)"final_post", (xmlChar*)NULL);
	if (!xFinal)
		return false;
	if (!insertInCameraFinal(xFinal, &cam->fMod, cam->texDiaphragm.fullfilename, cam->texObstacle.fullfilename))
		return false;

	// add image buffer data
	if (addCamBuffs)
	{
		int rtime;
		if (nowRendering)
		{
			rtime = cam->getRendTime(GetTickCount());
		}
		else
			rtime = cam->rendTime;

		xmlNodePtr dataNode;
		if (!insertRenderedData(xCamera, dataNode, cam->imgBuff->width, cam->imgBuff->height, cam->maxHalton, rtime))
			return false;

		char fileRelative[1024];
		char fileFull[2048];
		char fileTag[256];

		for (int i=0; i<16; i++)
		{
			if (!(cam->blendBits&(1<<i)))
				continue;


			if (cam->blendBuffDirect[i].imgBuf  &&  cam->blendBuffDirect[i].hitsBuf)
			{
				sprintf_s(fileRelative, 1024, "%s\\camera%d_%dd.dat", dirRelative, camNum+1, (i+1) );
				sprintf_s(fileFull, 2048, "%s\\camera%d_%dd.dat", dirFull, camNum+1, (i+1) );
				sprintf_s(fileTag, 256, "file%dd", (i+1) );
				bool saved = saveImageData(fileFull, &(cam->blendBuffDirect[i]));
				if (!saved)
					MessageBox(0, "Camera buffer not saved, watch out!", "", 0);
				else
				{
					xmlNodePtr xBufFile = xmlNewChild(dataNode, NULL, (xmlChar*)fileTag, (xmlChar*)fileRelative);
					if (!xBufFile)
						return false;
				}
			}

			if (cam->blendBuffGI[i].imgBuf  &&  cam->blendBuffGI[i].hitsBuf)
			{
				sprintf_s(fileRelative, 1024, "%s\\camera%d_%dg.dat", dirRelative, camNum+1, (i+1) );
				sprintf_s(fileFull, 2048, "%s\\camera%d_%dg.dat", dirFull, camNum+1, (i+1) );
				sprintf_s(fileTag, 256, "file%dg", (i+1) );
				bool saved = saveImageData(fileFull, &(cam->blendBuffGI[i]));
				if (!saved)
					MessageBox(0, "Camera buffer not saved, watch out!", "", 0);
				else
				{
					xmlNodePtr xBufFile = xmlNewChild(dataNode, NULL, (xmlChar*)fileTag, (xmlChar*)fileRelative);
					if (!xBufFile)
						return false;
				}
			}
		}

		if (cam->depthBuf  &&  cam->depthBuf->fbuf  &&  cam->depthBuf->hbuf)
		{
			sprintf_s(fileRelative, 1024, "%s\\camera%d_z.dat", dirRelative, camNum+1 );
			sprintf_s(fileFull, 2048, "%s\\camera%d_z.dat", dirFull, camNum+1 );
			sprintf_s(fileTag, 256, "file_z" );
			bool saved = saveFloatData(fileFull, cam->depthBuf);
			if (!saved)
				MessageBox(0, "Camera depth buffer not saved, watch out!", "", 0);
			else
			{
				xmlNodePtr xBufFile = xmlNewChild(dataNode, NULL, (xmlChar*)fileTag, (xmlChar*)fileRelative);
				if (!xBufFile)
					return false;
			}
		}

	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertRenderedData(xmlNodePtr curNode, xmlNodePtr &newNode, int w, int h, unsigned int cseed, int rtime)
{
	if (!curNode   ||   w < 1   ||   h < 1)
		return false;

	// add <imagedata>
	xmlNodePtr xData = xmlNewChild(curNode, NULL, (xmlChar*)"imagedata", (xmlChar*)NULL);
	if (!xData)
		return false;

	xmlAttrPtr xAttr;
	char buf[32];

	// add width
	sprintf_s(buf, 32, "%d", w);
	xAttr = xmlNewProp(xData, (xmlChar*)"width", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add height
	sprintf_s(buf, 32, "%d", h);
	xAttr = xmlNewProp(xData, (xmlChar*)"height", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add rtime
	sprintf_s(buf, 32, "%d", rtime);
	xAttr = xmlNewProp(xData, (xmlChar*)"rtime", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add camera seed
	sprintf_s(buf, 32, "%d", cseed);
	xAttr = xmlNewProp(xData, (xmlChar*)"cseed", (xmlChar*)buf);
	if (!xAttr)
		return false;

	newNode = xData;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertSky(xmlNodePtr curNode, SunSky * sky, bool turnedOn)
{
	if (!curNode   ||   !sky)
		return false;

	char buf[64];

	// add <sky>
	xmlNodePtr xSky = xmlNewChild(curNode, NULL, (xmlChar*)"sky", (xmlChar*)NULL);
	if (!xSky)
		return false;

	// add <use_sunsky>
	xmlNodePtr xTOn;
	if (turnedOn)
		xTOn = xmlNewChild(xSky, NULL, (xmlChar*)"use_sunsky", (xmlChar*)"yes");
	else
		xTOn = xmlNewChild(xSky, NULL, (xmlChar*)"use_sunsky", (xmlChar*)"no");
	if (!xTOn)
		return false;
	
	// add <sun_on>
	xmlNodePtr xSunOn;
	if (sky->sunOn)
		xSunOn = xmlNewChild(xSky, NULL, (xmlChar*)"sun_on", (xmlChar*)"yes");
	else
		xSunOn = xmlNewChild(xSky, NULL, (xmlChar*)"sun_on", (xmlChar*)"no");
	if (!xSunOn)
		return false;

	// add <sun_other_layer>
	xmlNodePtr xSunOther;
	if (sky->sunOtherBlendLayer)
		xSunOther = xmlNewChild(xSky, NULL, (xmlChar*)"sun_other_layer", (xmlChar*)"yes");
	else
		xSunOther = xmlNewChild(xSky, NULL, (xmlChar*)"sun_other_layer", (xmlChar*)"no");
	if (!xSunOther)
		return false;

	// add <sun_size>
	_sprintf_s_l(buf, 64, "%.6f", noxLocale, (sky->sunSize));
	xmlNodePtr xSunSize = xmlNewChild(xSky, NULL, (xmlChar*)"sun_size", (xmlChar*)buf);
	if (!xSunSize)
		return false;


	// add <sky_model>
	sprintf_s(buf, 64, "%d", (sky->use_hosek ? 1 : 0));
	xmlNodePtr xModel = xmlNewChild(xSky, NULL, (xmlChar*)"sky_model", (xmlChar*)buf);
	if (!xModel)
		return false;

	// add <month>
	sprintf_s(buf, 64, "%d", sky->getMonth());
	xmlNodePtr xMonth = xmlNewChild(xSky, NULL, (xmlChar*)"month", (xmlChar*)buf);
	if (!xMonth)
		return false;

	// add <day>
	sprintf_s(buf, 64, "%d", sky->getDay());
	xmlNodePtr xDay = xmlNewChild(xSky, NULL, (xmlChar*)"day", (xmlChar*)buf);
	if (!xDay)
		return false;

	// add <hour>
	sprintf_s(buf, 64, "%d", sky->getHour());
	xmlNodePtr xHour = xmlNewChild(xSky, NULL, (xmlChar*)"hour", (xmlChar*)buf);
	if (!xHour)
		return false;

	// add <minute>
	sprintf_s(buf, 64, "%d", sky->getMinute());
	xmlNodePtr xMinute = xmlNewChild(xSky, NULL, (xmlChar*)"minute", (xmlChar*)buf);
	if (!xMinute)
		return false;

	// add <gmt>
	sprintf_s(buf, 64, "%d", (int)(sky->getMeridianForTimeZone()*12/PI));
	xmlNodePtr xGmt = xmlNewChild(xSky, NULL, (xmlChar*)"gmt", (xmlChar*)buf);
	if (!xGmt)
		return false;

	// add <longitude>
	sprintf_s(buf, 64, "%d", ROUND(-sky->getLongitude()*180/PI));
	xmlNodePtr xLong = xmlNewChild(xSky, NULL, (xmlChar*)"longitude", (xmlChar*)buf);
	if (!xLong)
		return false;

	// add <latitude>
	sprintf_s(buf, 64, "%d", ROUND(-sky->getLatitude()*180/PI));
	xmlNodePtr xLat = xmlNewChild(xSky, NULL, (xmlChar*)"latitude", (xmlChar*)buf);
	if (!xLat)
		return false;

	// add <turbidity>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, (sky->turbidity*10));
	xmlNodePtr xTurb = xmlNewChild(xSky, NULL, (xmlChar*)"turbidity", (xmlChar*)buf);
	if (!xTurb)
		return false;

	// add <aerosol>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, sky->aerosol);
	xmlNodePtr xAero = xmlNewChild(xSky, NULL, (xmlChar*)"aerosol", (xmlChar*)buf);
	if (!xAero)
		return false;

	// add <albedo>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, sky->albedo);
	xmlNodePtr xAlbedo = xmlNewChild(xSky, NULL, (xmlChar*)"albedo", (xmlChar*)buf);
	if (!xAlbedo)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::savePost(char * filename, Camera * cam)
{
	if (!filename   ||   !cam)
		return false;

	xmlDocPtr xmlDoc;
	xmlDoc = xmlNewDoc((xmlChar*)"1.0");
	if (!xmlDoc)
	{
		return false;
	}

	xmlNodePtr xRoot = xmlNewDocNode(xmlDoc, NULL, (xmlChar*)"postprocess", NULL);
	if (!xRoot)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	xmlDocSetRootElement(xmlDoc, xRoot);

	if (!insertInCameraPost(xRoot, &cam->iMod))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	int written = xmlSaveFormatFile(filename, xmlDoc, 1);
	xmlFreeDoc(xmlDoc);

	if (written < 1)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertInCameraPost(xmlNodePtr curNode, ImageModifier * iMod)
{
	if (!curNode)
		return false;

	xmlNodePtr xCamera = curNode;
	char buf[64];

	// add <iso_ev_correction>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getISO_EV_compensation());
	xmlNodePtr xISOpostEV = xmlNewChild(xCamera, NULL, (xmlChar*)"iso_ev_correction", (xmlChar*)buf);
	if (!xISOpostEV)
		return false;

	// add <gi_ev_correction>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getGICompensation());
	xmlNodePtr xGIpostEV = xmlNewChild(xCamera, NULL, (xmlChar*)"gi_ev_correction", (xmlChar*)buf);
	if (!xGIpostEV)
		return false;

	// add <tone>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getToneMappingValue());
	xmlNodePtr xTone = xmlNewChild(xCamera, NULL, (xmlChar*)"tone", (xmlChar*)buf);
	if (!xTone)
		return false;

	// add <hot_pixels>
	xmlNodePtr xHotPixels;
	if (iMod->getDotsEnabled())
		xHotPixels = xmlNewChild(xCamera, NULL, (xmlChar*)"hot_pixels", (xmlChar*)"yes");
	else
		xHotPixels = xmlNewChild(xCamera, NULL, (xmlChar*)"hot_pixels", (xmlChar*)"no");
	if (!xHotPixels)
		return false;

	// add <gamma>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getGamma());
	xmlNodePtr xGamma = xmlNewChild(xCamera, NULL, (xmlChar*)"gamma", (xmlChar*)buf);
	if (!xGamma)
		return false;

	// add <brightness>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getBrightness());
	xmlNodePtr xBrightness = xmlNewChild(xCamera, NULL, (xmlChar*)"brightness", (xmlChar*)buf);
	if (!xBrightness)
		return false;

	// add <contrast>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getContrast());
	xmlNodePtr xContrast= xmlNewChild(xCamera, NULL, (xmlChar*)"contrast", (xmlChar*)buf);
	if (!xContrast)
		return false;

	// add <saturation>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getSaturation());
	xmlNodePtr xSaturation = xmlNewChild(xCamera, NULL, (xmlChar*)"saturation", (xmlChar*)buf);
	if (!xSaturation)
		return false;

	// add <addred>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getR());
	xmlNodePtr xAddRed = xmlNewChild(xCamera, NULL, (xmlChar*)"addred", (xmlChar*)buf);
	if (!xAddRed)
		return false;

	// add <addgreen>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getG());
	xmlNodePtr xAddGreen = xmlNewChild(xCamera, NULL, (xmlChar*)"addgreen", (xmlChar*)buf);
	if (!xAddGreen)
		return false;

	// add <addblue>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, iMod->getB());
	xmlNodePtr xAddBlue = xmlNewChild(xCamera, NULL, (xmlChar*)"addblue", (xmlChar*)buf);
	if (!xAddBlue)
		return false;

	// add <temperature>
	sprintf_s(buf, 64, "%d", (int)iMod->getTemperature());
	xmlNodePtr xTemperature = xmlNewChild(xCamera, NULL, (xmlChar*)"temperature", (xmlChar*)buf);
	if (!xTemperature)
		return false;
	
	// add <vignette>
	sprintf_s(buf, 64, "%d", iMod->getVignette());
	xmlNodePtr xVignette = xmlNewChild(xCamera, NULL, (xmlChar*)"vignette", (xmlChar*)buf);
	if (!xVignette)
		return false;

	// add <response_function>
	sprintf_s(buf, 64, "%d", iMod->getResponseFunctionNumber());
	xmlNodePtr xResponse = xmlNewChild(xCamera, NULL, (xmlChar*)"response_function", (xmlChar*)buf);
	if (!xResponse)
		return false;

	// add <filter>
	sprintf_s(buf, 64, "%d", (int)iMod->getFilter());
	xmlNodePtr xFilter = xmlNewChild(xCamera, NULL, (xmlChar*)"filter", (xmlChar*)buf);
	if (!xFilter)
		return false;

	// add <curve>
	xmlNodePtr xCurve = xmlNewChild(xCamera, NULL, (xmlChar*)"curve", NULL);
	if (!xCurve)
		return false;

		// add <cr> <cg> <cb> <cl>
		bool cr,cg,cb,cl;
		iMod->getCurveCheckboxes(cr,cg,cb,cl);
		xmlNodePtr xCR, xCG, xCB, xCL;
		if (cr)
			xCR = xmlNewChild(xCurve, NULL, (xmlChar*)"cr", (xmlChar*)"yes");
		else
			xCR = xmlNewChild(xCurve, NULL, (xmlChar*)"cr", (xmlChar*)"no");
		if (cg)
			xCG = xmlNewChild(xCurve, NULL, (xmlChar*)"cg", (xmlChar*)"yes");
		else
			xCG = xmlNewChild(xCurve, NULL, (xmlChar*)"cg", (xmlChar*)"no");
		if (cb)
			xCB = xmlNewChild(xCurve, NULL, (xmlChar*)"cb", (xmlChar*)"yes");
		else
			xCB = xmlNewChild(xCurve, NULL, (xmlChar*)"cb", (xmlChar*)"no");
		if (cl)
			xCL = xmlNewChild(xCurve, NULL, (xmlChar*)"cl", (xmlChar*)"yes");
		else
			xCL = xmlNewChild(xCurve, NULL, (xmlChar*)"cl", (xmlChar*)"no");
		if (!xCR   ||   !xCG   ||   !xCB   ||   !xCL)
			return false;

		xmlNodePtr xCanal, xCanalX, xCanalY;
		int x,y;

		// add <r>
		for (int i=0; i<iMod->getCurveRedPointsCount(); i++)
		{
			iMod->getCurveRedPoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"r", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

		// add <g>
		for (int i=0; i<iMod->getCurveGreenPointsCount(); i++)
		{
			iMod->getCurveGreenPoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"g", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

		// add <b>
		for (int i=0; i<iMod->getCurveBluePointsCount(); i++)
		{
			iMod->getCurveBluePoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"b", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

		// add <l>
		for (int i=0; i<iMod->getCurveLuminancePointsCount(); i++)
		{
			iMod->getCurveLuminancePoint(i, x, y);
			xCanal = xmlNewChild(xCurve, NULL, (xmlChar*)"l", NULL);
			if (!xCanal)
				return false;

			sprintf_s(buf, 64, "%d", x);
			xCanalX = xmlNewChild(xCanal, NULL, (xmlChar*)"x", (xmlChar*)buf);
			if (!xCanalX)
				return false;

			sprintf_s(buf, 64, "%d", y);
			xCanalY = xmlNewChild(xCanal, NULL, (xmlChar*)"y", (xmlChar*)buf);
			if (!xCanalY)
				return false;
		}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::saveBlendSettings(char * filename, BlendSettings &blend, bool onlyNames)
{
	CHECK(filename);

	xmlDocPtr xmlDoc;
	xmlDoc = xmlNewDoc((xmlChar*)"1.0");
	if (!xmlDoc)
	{
		return false;
	}

	xmlNodePtr xRoot = xmlNewDocNode(xmlDoc, NULL, (xmlChar*)"blend", NULL);
	if (!xRoot)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	xmlDocSetRootElement(xmlDoc, xRoot);

	if (!insertBlendSettings(xRoot, blend, onlyNames))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	int written = xmlSaveFormatFile(filename, xmlDoc, 1);
	xmlFreeDoc(xmlDoc);

	if (written < 1)
		return false;

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::insertTimers(xmlNodePtr curNode)
{
	if (!curNode)
		return false;
	Raytracer * rtr = Raytracer::getInstance();

	char buf[64];

	bool timerOnRefresh = rtr->gui_timers.refreshTimeOn;
	bool timerOnStop = rtr->gui_timers.stoppingTimerOn;
	bool timerOnAutosave = rtr->gui_timers.autosaveTimerOn;
	int stopHours = rtr->gui_timers.stopTimerHours;
	int stopMinutes = rtr->gui_timers.stopTimerMinutes;
	int stopSeconds = rtr->gui_timers.stopTimerSeconds;
	int refreshSeconds = rtr->gui_timers.refreshTime;
	int autosaveMinutes = rtr->gui_timers.autosaveTimerMinutes;

	int engine = Raytracer::getInstance()->curScenePtr->sscene.giMethod;


	// add <renderer_settings>
	xmlNodePtr xSettings = xmlNewChild(curNode, NULL, (xmlChar*)"renderer_settings", (xmlChar*)NULL);
	if (!xSettings)
		return false;

	// add <timer_stopafter_hours>
	sprintf_s(buf, 64, "%d", stopHours);
	xmlNodePtr xStopH = xmlNewChild(xSettings, NULL, (xmlChar*)"timer_stopafter_hours", (xmlChar*)buf);
	if (!xStopH)
		return false;

	// add <timer_stopafter_minutes>
	sprintf_s(buf, 64, "%d", stopMinutes);
	xmlNodePtr xStopM = xmlNewChild(xSettings, NULL, (xmlChar*)"timer_stopafter_minutes", (xmlChar*)buf);
	if (!xStopM)
		return false;

	// add <timer_stopafter_seconds>
	sprintf_s(buf, 64, "%d", stopSeconds);
	xmlNodePtr xStopS = xmlNewChild(xSettings, NULL, (xmlChar*)"timer_stopafter_seconds", (xmlChar*)buf);
	if (!xStopS)
		return false;

	// add <timer_refresh_seconds>
	sprintf_s(buf, 64, "%d", refreshSeconds);
	xmlNodePtr xRefreshS= xmlNewChild(xSettings, NULL, (xmlChar*)"timer_refresh_seconds", (xmlChar*)buf);
	if (!xRefreshS)
		return false;

	// add <timer_autosave_minutes>
	sprintf_s(buf, 64, "%d", autosaveMinutes);
	xmlNodePtr xAutosaveM = xmlNewChild(xSettings, NULL, (xmlChar*)"timer_autosave_minutes", (xmlChar*)buf);
	if (!xAutosaveM)
		return false;


	// add <timer_on_refresh>
	char * tRefresh = timerOnRefresh ? "yes" : "no";
	xmlNodePtr xTimerRefresh = xmlNewChild(xSettings, NULL, (xmlChar*)"timer_on_refresh", (xmlChar*)tRefresh);
	if (!xTimerRefresh)
		return false;

	// add <timer_on_stop>
	char * tStop = timerOnStop ? "yes" : "no";
	xmlNodePtr xTimerStop = xmlNewChild(xSettings, NULL, (xmlChar*)"timer_on_stop", (xmlChar*)tStop);
	if (!xTimerStop)
		return false;

	// add <timer_on_autosave>
	char * tAutosave = timerOnAutosave ? "yes" : "no";
	xmlNodePtr xTimerAutosave = xmlNewChild(xSettings, NULL, (xmlChar*)"timer_on_autosave", (xmlChar*)tAutosave);
	if (!xTimerAutosave)
		return false;

	// add <engine>
	sprintf_s(buf, 64, "%d", engine);
	xmlNodePtr xEngine = xmlNewChild(xSettings, NULL, (xmlChar*)"engine", (xmlChar*)buf);
	if (!xEngine)
		return false;

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::insertSceneInfo(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	unsigned int numTris = sc->triangles.objCount;
	char pswd[64];
	sprintf_s(pswd, 64, "%d", numTris);
	unsigned int lpswd = (unsigned int)strlen(pswd);

	// add <scene_info>
	xmlNodePtr xSceneInfo = xmlNewChild(curNode, NULL, (xmlChar*)"scene_info", (xmlChar*)NULL);
	if (!xSceneInfo)
		return false;

	unsigned int size_scene_name   = (sc->scene_name)			?	(unsigned int)strlen(sc->scene_name)			: 0;
	unsigned int size_scene_author = (sc->scene_author_name)	?	(unsigned int)strlen(sc->scene_author_name)		: 0;
	unsigned int size_scene_email  = (sc->scene_author_email)	?	(unsigned int)strlen(sc->scene_author_email)	: 0;
	unsigned int size_scene_www    = (sc->scene_author_www)		?	(unsigned int)strlen(sc->scene_author_www)		: 0;

	char * decSceneName		= xorStringToNew(sc->scene_name,		 pswd, size_scene_name,		lpswd);
	char * decSceneAuthor	= xorStringToNew(sc->scene_author_name,	 pswd, size_scene_author,	lpswd);
	char * decSceneEmail	= xorStringToNew(sc->scene_author_email, pswd, size_scene_email,	lpswd);
	char * decSceneWWW		= xorStringToNew(sc->scene_author_www,	 pswd, size_scene_www,		lpswd);
	char * hexSceneName		= decToHexString(decSceneName,		size_scene_name);
	char * hexSceneAuthor	= decToHexString(decSceneAuthor,	size_scene_author);
	char * hexSceneEmail	= decToHexString(decSceneEmail,		size_scene_email);
	char * hexSceneWWW		= decToHexString(decSceneWWW,		size_scene_www);

	if (hexSceneName)
	{
		// add scene_name
		xmlNodePtr xSceneName = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"scene_name", (xmlChar*)hexSceneName);
		if (!xSceneName)
			return false;
	}

	if (hexSceneAuthor)
	{
		// add scene_author
		xmlNodePtr xSceneAuthor = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"scene_author", (xmlChar*)hexSceneAuthor);
		if (!xSceneAuthor)
			return false;
	}

	if (hexSceneEmail)
	{
		// add author_email
		xmlNodePtr xSceneAuthorEmail = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"author_email", (xmlChar*)hexSceneEmail);
		if (!xSceneAuthorEmail)
			return false;
	}

	if (hexSceneWWW)
	{
		// add author_www
		xmlNodePtr xSceneAuthorWWW = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"author_www", (xmlChar*)hexSceneWWW);
		if (!xSceneAuthorWWW)
			return false;
	}

	if (decSceneName)
		free(decSceneName);
	if (decSceneAuthor)
		free(decSceneAuthor);
	if (decSceneEmail)
		free(decSceneEmail);
	if (decSceneWWW)
		free(decSceneWWW);
	if (hexSceneName)
		free(hexSceneName);
	if (hexSceneAuthor)
		free(hexSceneAuthor);
	if (hexSceneEmail)
		free(hexSceneEmail);
	if (hexSceneWWW)
		free(hexSceneWWW);

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertUserPresets(xmlNodePtr curNode)
{
	if (!curNode)
		return false;
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	// add <user_presets>
	xmlNodePtr xUserPresets = xmlNewChild(curNode, NULL, (xmlChar*)"user_presets", (xmlChar*)NULL);
	if (!xUserPresets)
		return false;

	int n = sc->presets.objCount;
	for (int i=0; i<n; i++)
	{
		PresetPost * pr = sc->presets[i];
		if (pr)
		{
			if (!insertUserPreset(xUserPresets, pr))
				return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertUserPreset(xmlNodePtr curNode, PresetPost * pr)
{
	if (!curNode)
		return false;
	if (!pr)
		return false;

	// add <user_preset>
	xmlNodePtr xUserPreset = xmlNewChild(curNode, NULL, (xmlChar*)"user_preset", (xmlChar*)NULL);
	if (!xUserPreset)
		return false;

	if (pr->name)
	{
		// add <name>
		xmlNodePtr xName = xmlNewChild(xUserPreset, NULL, (xmlChar*)"name", (xmlChar*)pr->name);
		if (!xName)
			return false;
	}

	// add <blend>
	xmlNodePtr xBlend = xmlNewChild(xUserPreset, NULL, (xmlChar*)"blend", (xmlChar*)NULL);
	if (!xBlend)
		return false;
	if (!insertBlendSettings(xBlend, pr->blend))
		return false;

	// add <post>
	xmlNodePtr xPost = xmlNewChild(xUserPreset, NULL, (xmlChar*)"post", (xmlChar*)NULL);
	if (!xPost)
		return false;
	if (!insertInCameraPost(xPost, &pr->iMod))
		return false;

	// add <final>
	xmlNodePtr xFinal = xmlNewChild(xUserPreset, NULL, (xmlChar*)"final", (xmlChar*)NULL);
	if (!xFinal)
		return false;
	if (!insertInCameraFinal(xFinal, &pr->fMod, pr->texGlareDiaphragm, pr->texGlareObstacle))
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::saveFinal(char * filename, Camera * cam)
{
	if (!filename   ||   !cam)
		return false;

	xmlDocPtr xmlDoc;
	xmlDoc = xmlNewDoc((xmlChar*)"1.0");
	if (!xmlDoc)
	{
		return false;
	}

	xmlNodePtr xRoot = xmlNewDocNode(xmlDoc, NULL, (xmlChar*)"final_post", NULL);
	if (!xRoot)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	xmlDocSetRootElement(xmlDoc, xRoot);

	if (!insertInCameraFinal(xRoot, &cam->fMod, cam->texDiaphragm.fullfilename, cam->texObstacle.fullfilename))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	int written = xmlSaveFormatFile(filename, xmlDoc, 1);
	xmlFreeDoc(xmlDoc);

	if (written < 1)
		return false;

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::insertInCameraFinal(xmlNodePtr curNode, FinalModifier * fMod, char * texDiaph, char * texObst)
{
	if (!curNode)
		return false;

	xmlNodePtr xCamera = curNode;
	char buf[64];

	// add <focus_dist>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->focusDistance);
	xmlNodePtr xFocusDist = xmlNewChild(xCamera, NULL, (xmlChar*)"focus_dist", (xmlChar*)buf);
	if (!xFocusDist)
		return false;

	// add <dof_quality>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->quality);
	xmlNodePtr xQuality = xmlNewChild(xCamera, NULL, (xmlChar*)"dof_quality", (xmlChar*)buf);
	if (!xQuality)
		return false;

	// add <dof_algorithm>
	_sprintf_s_l(buf, 64, "%d", noxLocale, (fMod->algorithm+1));
	xmlNodePtr xAlgorithm = xmlNewChild(xCamera, NULL, (xmlChar*)"dof_algorithm", (xmlChar*)buf);
	if (!xAlgorithm)
		return false;

	// add <dof_on>
	xmlNodePtr xDofOn;
	if (fMod->dofOn)
		xDofOn = xmlNewChild(xCamera, NULL, (xmlChar*)"dof_on", (xmlChar*)"yes");
	else
		xDofOn = xmlNewChild(xCamera, NULL, (xmlChar*)"dof_on", (xmlChar*)"no");
	if (!xDofOn)
		return false;

	// add <aperture>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->aperture);
	xmlNodePtr xAperture = xmlNewChild(xCamera, NULL, (xmlChar*)"aperture", (xmlChar*)buf);
	if (!xAperture)
		return false;

	// add <aperture_num_blades>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->bladesNum);
	xmlNodePtr xBladesNum = xmlNewChild(xCamera, NULL, (xmlChar*)"aperture_num_blades", (xmlChar*)buf);
	if (!xBladesNum)
		return false;

	// add <aperture_blades_angle>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->bladesAngle);
	xmlNodePtr xBladesAngle = xmlNewChild(xCamera, NULL, (xmlChar*)"aperture_blades_angle", (xmlChar*)buf);
	if (!xBladesAngle)
		return false;

	// add <aperture_blades_radius>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->bladesRadius);
	xmlNodePtr xBladesRadius = xmlNewChild(xCamera, NULL, (xmlChar*)"aperture_blades_radius", (xmlChar*)buf);
	if (!xBladesRadius)
		return false;

	// add <aperture_blades_round>
	xmlNodePtr xBladesRound;
	if (fMod->roundBlades)
		xBladesRound = xmlNewChild(xCamera, NULL, (xmlChar*)"aperture_blades_round", (xmlChar*)"yes");
	else
		xBladesRound = xmlNewChild(xCamera, NULL, (xmlChar*)"aperture_blades_round", (xmlChar*)"no");
	if (!xBladesRound)
		return false;

	// add <bokeh_balance>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->bokehRingBalance);
	xmlNodePtr xBokehBalance = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_balance", (xmlChar*)buf);
	if (!xBokehBalance)
		return false;

	// add <bokeh_ring_size>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->bokehRingSize);
	xmlNodePtr xBokehSize = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_ring_size", (xmlChar*)buf);
	if (!xBokehSize)
		return false;

	// add <bokeh_flatten>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->bokehFlatten);
	xmlNodePtr xBokehFlatten = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_flatten", (xmlChar*)buf);
	if (!xBokehFlatten)
		return false;

	// add <bokeh_vignette>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->bokehVignette);
	xmlNodePtr xBokehVign = xmlNewChild(xCamera, NULL, (xmlChar*)"bokeh_vignette", (xmlChar*)buf);
	if (!xBokehVign)
		return false;

	// add <chr_abb_shift_lens>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->chromaticShiftLens);
	xmlNodePtr xChrAbbShiftLens = xmlNewChild(xCamera, NULL, (xmlChar*)"chr_abb_shift_lens", (xmlChar*)buf);
	if (!xChrAbbShiftLens)
		return false;

	// add <chr_abb_shift_depth>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->chromaticShiftDepth);
	xmlNodePtr xChrAbbShiftDepth = xmlNewChild(xCamera, NULL, (xmlChar*)"chr_abb_shift_depth", (xmlChar*)buf);
	if (!xChrAbbShiftDepth)
		return false;

	// add <chr_abb_achromatic>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->chromaticAchromatic);
	xmlNodePtr xChrAbbAchromatic = xmlNewChild(xCamera, NULL, (xmlChar*)"chr_abb_achromatic", (xmlChar*)buf);
	if (!xChrAbbAchromatic)
		return false;

	// add <chr_abb_red>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->chromaticPlanarRed);
	xmlNodePtr xChrAbbPlRed = xmlNewChild(xCamera, NULL, (xmlChar*)"chr_abb_red", (xmlChar*)buf);
	if (!xChrAbbPlRed)
		return false;

	// add <chr_abb_green>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->chromaticPlanarGreen);
	xmlNodePtr xChrAbbPlGreen = xmlNewChild(xCamera, NULL, (xmlChar*)"chr_abb_green", (xmlChar*)buf);
	if (!xChrAbbPlGreen)
		return false;

	// add <chr_abb_blue>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->chromaticPlanarBlue);
	xmlNodePtr xChrAbbPlBlue = xmlNewChild(xCamera, NULL, (xmlChar*)"chr_abb_blue", (xmlChar*)buf);
	if (!xChrAbbPlBlue)
		return false;

	// add <hot_pixels_enabled>
	xmlNodePtr xHotEnabled;
	if (fMod->dotsEnabled)
		xHotEnabled = xmlNewChild(xCamera, NULL, (xmlChar*)"hot_pixels_enabled", (xmlChar*)"yes");
	else
		xHotEnabled = xmlNewChild(xCamera, NULL, (xmlChar*)"hot_pixels_enabled", (xmlChar*)"no");
	if (!xHotEnabled)
		return false;

	// add <hot_pixels_radius>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->dotsRadius);
	xmlNodePtr xHotRadius = xmlNewChild(xCamera, NULL, (xmlChar*)"hot_pixels_radius", (xmlChar*)buf);
	if (!xHotRadius)
		return false;

	// add <hot_pixels_threshold>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->dotsThreshold);
	xmlNodePtr xHotThres = xmlNewChild(xCamera, NULL, (xmlChar*)"hot_pixels_threshold", (xmlChar*)buf);
	if (!xHotThres)
		return false;

	// add <hot_pixels_density>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->dotsDensity);
	xmlNodePtr xHotDensity = xmlNewChild(xCamera, NULL, (xmlChar*)"hot_pixels_density", (xmlChar*)buf);
	if (!xHotDensity)
		return false;

	// add <grain>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->grain);
	xmlNodePtr xGrain = xmlNewChild(xCamera, NULL, (xmlChar*)"grain", (xmlChar*)buf);
	if (!xGrain)
		return false;

	// add <fog_distance>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->fogDist);
	xmlNodePtr xFogDist = xmlNewChild(xCamera, NULL, (xmlChar*)"fog_distance", (xmlChar*)buf);
	if (!xFogDist)
		return false;

	// add <fog_density>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->fogSpeed);
	xmlNodePtr xFogDens = xmlNewChild(xCamera, NULL, (xmlChar*)"fog_density", (xmlChar*)buf);
	if (!xFogDens)
		return false;

	// add <fog_exclude_background>
	xmlNodePtr xFogExcl;
	if (fMod->fogExclSky)
		xFogExcl = xmlNewChild(xCamera, NULL, (xmlChar*)"fog_exclude_background", (xmlChar*)"yes");
	else
		xFogExcl = xmlNewChild(xCamera, NULL, (xmlChar*)"fog_exclude_background", (xmlChar*)"no");
	if (!xFogExcl)
		return false;

	// add <fog_enabled>
	xmlNodePtr xFogOn;
	if (fMod->fogEnabled)
		xFogOn = xmlNewChild(xCamera, NULL, (xmlChar*)"fog_enabled", (xmlChar*)"yes");
	else
		xFogOn = xmlNewChild(xCamera, NULL, (xmlChar*)"fog_enabled", (xmlChar*)"no");
	if (!xFogOn)
		return false;

	// add <fog_color>
	Color4 fgc = fMod->fogColor;
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, fgc.r * 255))), (unsigned char *)buf);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, fgc.g * 255))), (unsigned char *)buf+2);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, fgc.b * 255))), (unsigned char *)buf+4);
	buf[6] = 0;
	xmlNodePtr xFogColor = xmlNewChild(xCamera, NULL, (xmlChar*)"fog_color", (xmlChar *)buf);
	if (!xFogColor)
		return false;

	// add <bloom_enabled>
	xmlNodePtr xBloomOn;
	if (fMod->bloomEnabled)
		xBloomOn = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_enabled", (xmlChar*)"yes");
	else
		xBloomOn = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_enabled", (xmlChar*)"no");
	if (!xBloomOn)
		return false;

	// add <bloom_power>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->bloomPower);
	xmlNodePtr xBloomPower = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_power", (xmlChar*)buf);
	if (!xBloomPower)
		return false;

	// add <bloom_area>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->bloomArea);
	xmlNodePtr xBloomArea = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_area", (xmlChar*)buf);
	if (!xBloomArea)
		return false;

	// add <bloom_threshold>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->bloomThreshold);
	xmlNodePtr xBloomThreshold = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_threshold", (xmlChar*)buf);
	if (!xBloomThreshold)
		return false;


	// add <bloom_attenuation_enabled>
	xmlNodePtr xBloomAttOn;
	if (fMod->bloomAttenuationOn)
		xBloomAttOn = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_attenuation_enabled", (xmlChar*)"yes");
	else
		xBloomAttOn = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_attenuation_enabled", (xmlChar*)"no");
	if (!xBloomAttOn)
		return false;

	// add <bloom_attenuation_color>
	Color4 bc = fMod->bloomAttenuationColor;
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, bc.r * 255))), (unsigned char *)buf);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, bc.g * 255))), (unsigned char *)buf+2);
	DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, bc.b * 255))), (unsigned char *)buf+4);
	buf[6] = 0;
	xmlNodePtr xBloomAttColor = xmlNewChild(xCamera, NULL, (xmlChar*)"bloom_attenuation_color", (xmlChar *)buf);
	if (!xBloomAttColor)
		return false;

	// add <glare_enabled>
	xmlNodePtr xGlareOn;
	if (fMod->glareEnabled)
		xGlareOn = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_enabled", (xmlChar*)"yes");
	else
		xGlareOn = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_enabled", (xmlChar*)"no");
	if (!xGlareOn)
		return false;

	// add <glare_power>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->glarePower);
	xmlNodePtr xGlarePower = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_power", (xmlChar*)buf);
	if (!xGlarePower)
		return false;
	
	// add <glare_dispersion>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->glareDispersion);
	xmlNodePtr xGlareDisp = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_dispersion", (xmlChar*)buf);
	if (!xGlareDisp)
		return false;

	// add <glare_threshold>
	_sprintf_s_l(buf, 64, "%.3f", noxLocale, fMod->glareThreshold);
	xmlNodePtr xGlareThres = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_threshold", (xmlChar*)buf);
	if (!xGlareThres)
		return false;

	// add <glare_area>
	_sprintf_s_l(buf, 64, "%d", noxLocale, fMod->glareArea);
	xmlNodePtr xGlareArea = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_area", (xmlChar*)buf);
	if (!xGlareArea)
		return false;

	// add <glare_diaphragm_on>
	xmlNodePtr xglareDiffOn;
	if (fMod->glareTextureApertureOn)
		xglareDiffOn = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_diaphragm_on", (xmlChar*)"yes");
	else
		xglareDiffOn = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_diaphragm_on", (xmlChar*)"no");
	if (!xglareDiffOn)
		return false;

	// add <glare_obstacle_on>
	xmlNodePtr xGlareObstOn;
	if (fMod->glareTextureObstacleOn)
		xGlareObstOn = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_obstacle_on", (xmlChar*)"yes");
	else
		xGlareObstOn = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_obstacle_on", (xmlChar*)"no");
	if (!xGlareObstOn)
		return false;

	// add <glare_obstacle>
	if (char * c = texObst)
	{
		xmlNodePtr xGlareObst = xmlNewChild(xCamera , NULL, (xmlChar*)"glare_obstacle", (xmlChar *)c);
		if (!xGlareObst)
			return false;
	}

	// add <glare_diaphragm>
	if (char * c = texDiaph)
	{
		xmlNodePtr xGlareDiaph = xmlNewChild(xCamera, NULL, (xmlChar*)"glare_diaphragm", (xmlChar *)c);
		if (!xGlareDiaph)
			return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::savePreset(char * filename, Camera * cam, BlendSettings &blend)
{
	CHECK(filename);
	CHECK(cam);

	xmlDocPtr xmlDoc;
	xmlDoc = xmlNewDoc((xmlChar*)"1.0");
	if (!xmlDoc)
	{
		return false;
	}

	xmlNodePtr xRoot = xmlNewDocNode(xmlDoc, NULL, (xmlChar*)"nox_presets", NULL);
	if (!xRoot)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	xmlDocSetRootElement(xmlDoc, xRoot);

	// add blend
	xmlNodePtr xBlend = xmlNewChild(xRoot, NULL, (xmlChar*)"blend", NULL);
	if (!xBlend)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	if (!insertBlendSettings(xBlend, blend, false))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	// add post
	xmlNodePtr xPost = xmlNewChild(xRoot, NULL, (xmlChar*)"post", NULL);
	if (!xPost)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	if (!insertInCameraPost(xPost, &cam->iMod))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	// add final
	xmlNodePtr xFinal = xmlNewChild(xRoot, NULL, (xmlChar*)"final", NULL);
	if (!xFinal)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	if (!insertInCameraFinal(xFinal, &cam->fMod, cam->texDiaphragm.fullfilename, cam->texObstacle.fullfilename))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	int written = xmlSaveFormatFile(filename, xmlDoc, 1);
	xmlFreeDoc(xmlDoc);

	if (written < 1)
		return false;

	return true;
}

//----------------------------------------------------------------------------------------------------------------

