#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"

extern HMODULE hDllModule;

void InitEMLine()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMLine";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMLineProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMLine *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMLine * GetEMLineInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMLine * eml = (EMLine *)GetWindowLongPtr(hwnd, 0);
	#else
		EMLine * eml = (EMLine *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return eml;
}

void SetEMLineInstance(HWND hwnd, EMLine *eml)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)eml);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)eml);
	#endif
}

EMLine::EMLine(HWND hWnd)
{
	colBorderLeftUp = RGB(112,176,240);
	colBorderRightDown = RGB(144,208,255);
	hwnd = hWnd;
}

EMLine::~EMLine()
{
}

LRESULT CALLBACK EMLineProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMLine * eml;
	eml = GetEMLineInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			eml = new EMLine(hwnd);
			SetEMLineInstance(hwnd, eml);
			break;
		case WM_DESTROY:
			{
				delete eml;
				SetEMLineInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			hdc = BeginPaint(hwnd, &ps);

			GetClientRect(hwnd, &rect);
			POINT b1[] = { {0, rect.bottom-1}, {0,0}, {rect.right, 0}};
			POINT b2[] = { {rect.right-1, 1}, {rect.right-1, rect.bottom-1}, {0, rect.bottom-1}};
			HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, eml->colBorderLeftUp));
			Polyline(hdc, b1, 3);
			DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, eml->colBorderRightDown)));
			Polyline(hdc, b2, 3);
			DeleteObject(SelectObject(hdc, hOldPen));

			EndPaint(hwnd, &ps);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

