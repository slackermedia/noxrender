#include "Colors.h"
#include "raytracer.h"
#include "log.h"
#include "ThreadJobWindow.h"
#include "embree.h"


//-------------------------------------------------------------------------------------------------

DepthMapThread::DepthMapThread()
{
	hThread = 0;
	threadId = 0;
	numAllThreads = 0;
	curThreadNum = 0;
	progress = 0.0f;	// 0 - 100
	jobDone = false;
	abort = false;

	cam = NULL;
	sc = NULL;
	depth = NULL;
}

//-------------------------------------------------------------------------------------------------

DepthMapThread::~DepthMapThread()
{
}

//-------------------------------------------------------------------------------------------------

DWORD WINAPI DepthMapThreadProc(LPVOID lpParameter);

bool DepthMapThread::runThread()
{
	hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(DepthMapThreadProc),
            (LPVOID)this,
            0,
            &threadId);

	SetThreadPriority(hThread, THREAD_PRIORITY_BELOW_NORMAL);

	return true;
}

//-------------------------------------------------------------------------------------------------

DWORD WINAPI DepthMapThreadProc(LPVOID lpParameter)
{
	if (!lpParameter)
		return 0;
	DepthMapThread * thr = (DepthMapThread *)lpParameter;
	Camera * cam = thr->cam;
	if (!cam)
		cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
	if (!cam)
		return false;

	int daa = 1;
	float fdaa = (float)daa;
	float pfdaa = 1.0f/fdaa;

	int linesPerThread = thr->depth->height / thr->numAllThreads + 1;
	int startline = linesPerThread * thr->curThreadNum;
	int lastline = min(startline + linesPerThread, thr->depth->height); //+1

	char buf[256];
	sprintf_s(buf, 256, "Thread evals lines: %d -> %d", startline, lastline);
	Logger::add(buf);
	
	Raytracer * rtr = Raytracer::getInstance();

	for (int y=startline; y<lastline; y++)
	{
		thr->progress = (y-startline)*100.0f/linesPerThread;
		if (thr->abort)
		{
			thr->jobDone = true;
			return 0;
		}
		for (int x=0; x<thr->depth->width; x++)
		{
			for (int ay=0; ay<daa; ay++)
			{
				for (int ax=0; ax<daa; ax++)
				{
					Point3d startPoint;
					float fx = (float)x-0.5f + 0.5f*pfdaa + ax*pfdaa;
					float fy = (float)y-0.5f + 0.5f*pfdaa + ay	*pfdaa;
					Vector3d direction = cam->getDirection(fx, fy, startPoint, 0);
					direction.normalize();
					float rd = 0.0f;

					startPoint = cam->position;

					bool notdone = true;
					while (notdone)
					{
						float maxdist = BIGFLOAT;
						int itri = -1;
						Matrix4d inst_mat;
						int instanceID;
						float u,v;
						float d;
						if (rtr->use_embree)
							d = intersectEmbree(thr->sc, startPoint, direction, inst_mat, maxdist, instanceID, itri, u,v);
						else
							d = thr->sc->sscene.bvh.intersectForTrisNonSSE(0, startPoint, direction, inst_mat, maxdist, instanceID, itri, u,v);

						if (d<=0.0f  ||  itri<0)
						{
							thr->depth->fbuf[y][x] = BIGFLOAT;
							thr->depth->hbuf[y][x] = 1;
							notdone = false;
						}
						else
						{
							rd += d;
							Triangle * tri = &(thr->sc->triangles[itri]);
							MaterialNox * mat = thr->sc->mats[thr->sc->instances[instanceID].materials[tri->matInInst]];

							float tu, tv;
							tri->evalTexUV(u,v, tu, tv);
							bool tracemore = false;
							if (mat->isSkyPortal)
								tracemore = true;
							else
								if (mat->getOpacity(tu, tv) == 0.0f)
									tracemore = true;
							if (tracemore)
							{
								notdone = true;
								startPoint = tri->getPointFromUV(u, v);
								startPoint = inst_mat * startPoint;
								Vector3d normal_geom = inst_mat * tri->normal_geom;
								normal_geom.normalize();
								startPoint = startPoint + normal_geom * (normal_geom*direction>0 ? 0.0001f : -0.0001f);
							}
							else
							{
								float dMod = cam->evalDistMult(fx, fy);
								thr->depth->fbuf[y][x] = rd * dMod;
								thr->depth->hbuf[y][x] = 1;
								notdone = false;
							}
						}
					}
				}
			}
		}
	}

	thr->jobDone = true;
	return 0;
}

//-------------------------------------------------------------------------------------------------

bool Scene::evalDepthMap(Camera * cam)
{
	if (!cam)
		return false;

	Logger::add("Init camera");
	cam->initializeCamera(false);

	if (!cam->depthBuf)
	{
		Logger::add("No depth buffer... creating");
		cam->depthBuf = new FloatBuffer();
	}

	if (!cam->depthBuf->fbuf  ||  !cam->depthBuf->hbuf)
	{
		Logger::add("Allocating buffer");
		bool allocok = cam->depthBuf->allocBuffer(cam->width*cam->aa, cam->height*cam->aa, true, true);
		if (!allocok)
			return false;
	}
	else
	{
		char buf[256];
		sprintf_s(buf, 256, "Found allocated depth buffer: %d x %d", cam->depthBuf->width, cam->depthBuf->height);
		Logger::add(buf);
	}

	cam->depthBuf->clearBuffer();

	Logger::add("Creating threads");

	int num_threads = numThreads;
	DepthMapThread * depththreads = new DepthMapThread[num_threads];
	for (int i=0; i<num_threads; i++)
	{
		depththreads[i].numAllThreads = num_threads;
		depththreads[i].curThreadNum = i;
		depththreads[i].progress = 0.0f;
		depththreads[i].jobDone = false;
		depththreads[i].abort = false;

		depththreads[i].cam = cam;
		depththreads[i].sc = this;
		depththreads[i].depth = cam->depthBuf;
	}

	Logger::add("Starting threads");

	for (int i=0; i<num_threads; i++)
		depththreads[i].runThread();

	bool allfinished = false;
	while (!allfinished)
	{
		Sleep(100);
		allfinished = true;
		for (int i=0; i<num_threads; i++)
			if (!depththreads[i].jobDone)
				allfinished = false;

		float sumProgress = 0;
		for (int t=0; t<num_threads; t++)
			sumProgress += depththreads[t].progress;
		sumProgress *= 1.0f/num_threads;
		if (abortThreadPtr  &&  *abortThreadPtr)
		{
			for (int t=0; t<num_threads; t++)
				depththreads[t].abort = true;
			cam->depthOK = false;
		}
		else
			notifyProgress("Evaluating depth map...", sumProgress);

	}

	cam->depthOK = true;

	return true;
}

//-------------------------------------------------------------------------------------------------
