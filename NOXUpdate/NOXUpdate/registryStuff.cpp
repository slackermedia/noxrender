#define _WIN32_WINNT 0x0502
#include <windows.h>
#include <stdio.h>
#include <shlobj.h>
#include "nox_update.h"

extern HINSTANCE hInst;
extern bool isSystem64;

extern bool m3ds9_32;
extern bool m3ds9_64;
extern bool m3ds2008_32;
extern bool m3ds2008_64;
extern bool m3ds2009_32;
extern bool m3ds2009_64;
extern bool m3ds2010_32;
extern bool m3ds2010_64;
extern bool m3ds2011_32;
extern bool m3ds2011_64;
extern char * dir_3dsmax_9_32;
extern char * dir_3dsmax_9_64;
extern char * dir_3dsmax_2008_32;
extern char * dir_3dsmax_2008_64;
extern char * dir_3dsmax_2009_32;
extern char * dir_3dsmax_2009_64;
extern char * dir_3dsmax_2010_32;
extern char * dir_3dsmax_2010_64;
extern char * dir_3dsmax_2011_32;
extern char * dir_3dsmax_2011_64;

//-------------------------------------------------------------------------------------------------------------

bool doesDirExist(char * dirName)
{
	bool res;
	HANDLE hDir;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hDir = FindFirstFile(dirName, &fd);
	
	if (hDir == INVALID_HANDLE_VALUE   ||   !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hDir != INVALID_HANDLE_VALUE)
		FindClose(hDir);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

bool doesFileExist(char * filename)		// will return false on dir
{
	if (!filename)
		return false;

	bool res;
	HANDLE hFile;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hFile = FindFirstFile(filename, &fd);
	
	if (hFile == INVALID_HANDLE_VALUE   ||   (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hFile != INVALID_HANDLE_VALUE)
		FindClose(hFile);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

char * getFilenameFromFilePath(char * filename)
{
	if (!filename)
		return NULL;
	int s = (int)strlen(filename) + 1;
	if (s < 1)
		return NULL;

	char * temp = (char*)malloc(s);
	if (!temp)
		return NULL;
	sprintf_s(temp, s, "%s", filename);
	for (int i=s-2; i>2; i--)
	{
		if (temp[i]=='\\'   ||   temp[i]=='/')
			temp[i] = 0;
		else
			break;
	}
	s = (int)strlen(temp)+1;

	char * fname = strrchr(temp, '\\');


	char * res = (char*)malloc(s);
	if (!res)
	{
		free(temp);
		return NULL;
	}

	if (fname)
		sprintf_s(res, s, "%s", fname+1);
	else 
		sprintf_s(res, s, "%s", temp);
	
	free(temp);
	
	return res;
}

//-------------------------------------------------------------------------------------------------------------
char * getDirectoryFromFilePath(char * filename)
{
	if (!filename)
		return NULL;
	int s = (int)strlen(filename) + 1;
	if (s < 1)
		return NULL;
	
	char * fname = strrchr(filename, '\\');
	if (!fname)
		return NULL;

	char * res = (char*)malloc(s);
	if (!res)
		return NULL;

	sprintf_s(res, s, "%s", filename);
	res[fname-filename] = 0;
	
	return res;
}

//-------------------------------------------------------------------------------------------------------------

char * getRendererDirectory()
{
	// get default directory
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres = RegQueryValueEx(key3, "directory", NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
	{	// if OK
	}
	else
	{
		return NULL;
	}

	size_t ss = strlen(buffer)+1;
	char * res = (char *)malloc(ss);
	if (res)
		sprintf_s(res, ss, "%s", buffer);

	for (int i=(int)ss-2; i>2; i--)
	{
		if (res[i]=='\\'  ||  res[i]=='/')
			res[i] = 0;
		else
			break;
	}

	return res;
}

//-------------------------------------------------------------------------------------------------------------

bool checkRendererDirs(char * dir)
{
	char dirName[2560];
	sprintf_s(dirName, 2560, "%s", dir);
	if (!doesDirExist(dirName))
	{
		return false;
	}

	#ifdef _WIN64
		sprintf_s(dirName, 2560, "%s\\dll\\64", dir);
	#else
		sprintf_s(dirName, 2560, "%s\\dll\\32", dir);
	#endif
	if (!doesDirExist(dirName))
	{
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

void setDir()
{
	// get win version and set dll directory search
	OSVERSIONINFO osver;
	osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx (&osver);

	if ((osver.dwMajorVersion > 5)  ||
		(osver.dwMajorVersion == 5   &&   osver.dwMinorVersion >= 1))
	{
		char * dir = getRendererDirectory();
		if (dir)
		{
			if (checkRendererDirs(dir))
			{
				char dirName[2560];
				#ifdef _WIN64
					sprintf_s(dirName, 2560, "%s\\dll\\64", dir);
				#else
					sprintf_s(dirName, 2560, "%s\\dll\\32", dir);
				#endif
				BOOL ss = SetDllDirectory(dirName);
				if (ss == FALSE)
					MessageBox(0, "Directory setting for DLLs failed.\nInstructions later.", "", 0);
			}
			else
			{
				char c[2560];
				#ifdef _WIN64
					sprintf_s(c, 2560, "Directory\n%s\\dll\\64\ndoes not exist.\nApplication may fail to find DLLs and crash.", dir);
				#else
					sprintf_s(c, 2560, "Directory\n%s\\dll\\32\ndoes not exist.\nApplication may fail to find DLLs and crash.", dir);
				#endif
				MessageBox(0, c, "Warning", MB_ICONEXCLAMATION);
			}
			free(dir);
		}
		else
		{
			MessageBox(0, "Directory not set in registry.\nApplication may fail to find DLLs and crash.", "Warning", MB_ICONEXCLAMATION);
		}
	}
}

//-------------------------------------------------------------------------------------------------------------

int __stdcall MyBrowseCallbackProc(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData) 
{ 
   switch (uMsg) 
   { 
      case BFFM_INITIALIZED: 
         SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
   } 
   return (uMsg == BFFM_VALIDATEFAILED); 
}

char * openDirectoryDialog(HWND hwnd, char* title, char * startDir)
{
	char * res = (char *)malloc(MAX_PATH);
	res[0] = 0;
	BROWSEINFO bi;
	bi.hwndOwner = hwnd;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = res;
	bi.lpszTitle = title;
	bi.ulFlags = BIF_RETURNONLYFSDIRS;
	bi.lpfn = NULL;
	bi.lParam = 0;
	bi.iImage = 0;

	if (startDir)
	{
		bi.lpfn = MyBrowseCallbackProc; 
		bi.lParam = (LPARAM)startDir; // unicode! 
	}

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	if (pidl)
	{
		BOOL bRet = SHGetPathFromIDList(pidl,res);
		if (bRet)
			return res;
	}
	else
		return NULL;

	free(res);
	return NULL;
}

bool setRegistryAutoUpdate(bool autoUpdateOn)
{
	LONG regres, regres2;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_WRITE, &key3);

	// set value
	if (regres == ERROR_SUCCESS)
	{
		DWORD val = (autoUpdateOn ? 1 : 0);
		regres2 = RegSetValueEx(key3, "autoupdate", 0, REG_DWORD, (BYTE*)&val, 4);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS   &&   regres2 == ERROR_SUCCESS)
		return true;
	else
		return false;
}

bool setRegistryAutoInstall(bool autoInstallOn)
{
	LONG regres, regres2;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_WRITE, &key3);

	// set value
	if (regres == ERROR_SUCCESS)
	{
		DWORD val = (autoInstallOn ? 1 : 0);
		regres2 = RegSetValueEx(key3, "autoinstall", 0, REG_DWORD, (BYTE*)&val, 4);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS   &&   regres2 == ERROR_SUCCESS)
		return true;
	else
		return false;
}

bool setRegistryDifferentDirs(bool diffDirsOn)
{
	LONG regres, regres2;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_WRITE, &key3);

	// set value
	if (regres == ERROR_SUCCESS)
	{
		DWORD val = (diffDirsOn ? 1 : 0);
		regres2 = RegSetValueEx(key3, "differentdirs", 0, REG_DWORD, (BYTE*)&val, 4);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS   &&   regres2 == ERROR_SUCCESS)
		return true;
	else
		return false;
}


bool getAutoUpdateRegistryStatus(bool &autoUpdateOn, bool &autoInstall, bool &betaTester, bool &diffrDirs)
{
	// get default directory
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres, regres2;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "betatester", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				betaTester = (val==1);
			}
		}

		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "autoupdate", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				autoUpdateOn = (val==1);
			}
		}

		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "autoinstall", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				autoInstall = (val==1);
			}
		}

		buffer[0] = 0;
		rsize = 2048;
		regres2 = RegQueryValueEx(key3, "differentdirs", NULL, &type, (BYTE*)buffer, &rsize);
		if (regres2 == ERROR_SUCCESS)
		{
			if (type==REG_DWORD)
			{
				DWORD val = *((DWORD*)buffer);
				diffrDirs = (val==1);
			}
		}

	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
		return true;
	else
		return false;
}

bool setNOXdirInRegistry(char * newdir)
{
	if (!newdir)
		return false;

	LONG regres, regres2;
	HKEY key1, key2, key3;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_WRITE, &key3);

	// set value
	if (regres == ERROR_SUCCESS)
	{
		DWORD ss = (DWORD)strlen(newdir) + 1;
		regres2 = RegSetValueEx(key3, "directory", 0, REG_SZ, (BYTE*)newdir, ss);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS   &&   regres2 == ERROR_SUCCESS)
		return true;
	else
		return false;

}
