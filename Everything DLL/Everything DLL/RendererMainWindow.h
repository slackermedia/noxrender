#ifndef __RENDERER_MAIN_WINDOW
#define __RENDERER_MAIN_WINDOW

// old version of main renderer window - used before v. 0.40

#define TIMER_STOP_ID 101
#define TIMER_AUTOSAVE_ID 102
#define TIMER_POSTPROCESS_ID 103
#define POSTPROCESS_TIMEOUT 500
#define WN_LOADSCENE 55000
#define WN_LOADSCENE_AND_RUN 55001
#define WN_RUN_RENDERER 55002
#define WN_LOAD_POST 55011
#define WN_LOAD_BLEND 55012
#define WN_LOAD_FINAL 55013
#define MAT_PREV_LIST_IMG_RES 64

#include "DLL.h"
#include <windows.h>
#include "ColorSchemes.h"
#include "ThreadJobWindow.h"
#include "Dialogs.h"

#define POSRENDER 0
#define POSREGIONS 1
#define POSCAMERA 2
#define POSOBJECTS 3
#define POSMATERIALS 4
#define POSENVIRONMENT 5
#define POSBLEND 6
#define POSPOST 7
#define POSLOGS 8
#define POSFINAL 8

class RendererMainWindow
{
public:
	static HINSTANCE hInstance1;
	static HMODULE hModule;
	static HWND hMain;
	static HWND hMainBar;
	static HWND hTabs;
	static HWND hOutputConf;
	static HWND hMaterials;
	static HWND hLogs;
	static HWND hFinal;
	static HWND hRegions;
	static HWND hCamera;
	static HWND hObjects;
	static HWND hBlend;
	static HWND hPost;
	static HWND hEnvironment;
	static HWND hImage;
	static HWND hStatus1;
	static HWND hProgress1;
	static HWND hStatus2;
	static HWND hStatus3;
	static HWND hStatus4;
	static ThreadWindow * thrWindow;
	static HBRUSH backgroundBrush;
	static ThemeManager theme;

	static int mainBarHeight;
	static int tabsHeight;
	static int outputConfHeight;
	static int materialsHeight;
	static int statusHeight;

	static int iposx;
	static int iposy;
	static bool keepAspect;
	static float aspectRatio;
	static float statusPart;


	static bool rendererWindowClassRegistered;
	static bool showTimeStamp;
	static bool showLogoStamp;
	static bool stoppingTimerOn;
	static bool stoppingPassOn;
	static RunNextSettings runNext;
	static bool autosaveTimerOn;
	static bool refreshTimeOn;
	static bool refreshThrRunning;
	static int  refreshTime;
	static int  stopTimerHours;
	static int  stopTimerMinutes;
	static int  stopTimerSeconds;
	static int  autosaveTimerMinutes;

	DECLDIR HWND getHWND();
	DECLDIR bool create(bool asPlugin, HWND hParent);
	ATOM MyRegisterClass(HINSTANCE hInstance);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK OutputWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK MaterialsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK LogsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK FinalWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK RegionsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK CameraWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK ObjectsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK BlendWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK PostWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK EnvironmentWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static INT_PTR CALLBACK MainBarWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static DWORD WINAPI RefreshThreadProc(LPVOID lpParameter);
	static DWORD WINAPI CounterRefreshThreadProc(LPVOID lpParameter);
	
	static void runRefreshThread();
	static void notifyProgressBarCallback(char * message, float progress);


	static void tabsChanged();
	static void resizePictureView();
	static void resizeMainBar();
	static void resizeTabs();
	static void resizeLogs();
	static void resizeFinal();
	static void resizeRegions();
	static void resizeMaterials();
	static void resizeCamera();
	static void resizeObjects();
	static void resizeBlend();
	static void resizePost();
	static void resizeEnvironment();
	static void resizeOutputConf();
	static void resizeStatusBar();

	static void lockUnlockAfterStart();
	static void lockUnlockAfterLoadScene();
	static void lockUnlockAfterRenderStart();
	static void lockUnlockAfterRenderPause();
	static void lockUnlockAfterRenderUnpause();
	static void lockUnlockAfterRenderEnd();
	static void lockUnlockUsedBlendControls(bool unlock);


	static void fillMaterialsList();
	static void fillMaterialsImageList();
	static void fillSingleMaterialData();
	static void fillObjectsList();
	static void fillInstancesList();
	static void fillCameraList();
	static void fillBlendLayersData();
	static void fillTimersData();
	static void fillFinalSettings(Camera * cam);
	static void fillEnvironmentMapData();
	static void updateTimersFromGUI();
	static void updateTimersAndRunNext();
	static void fillRenderSettings();


	static void fillCameraData(Camera * cam = NULL);
	static void fillCameraPostData(Camera * cam = NULL);
	static Camera * getSelectedCamera();
	static void setCameraFocalLength(float focal, bool changeSpinner, bool changeInCamera);
	static void updateMaterialPreview(int matnum);
	static void updateMaterialPreviewFullSize(int matnum);
	static void updateEngineSettingsBeforeRender();
	static void updateDimensionsFromActiveCamera();

	static bool editMaterial(int matnum);
	static void notifyBackFromMaterialEditor(bool AcceptChanges);
	static void afterMatEdit(bool accepted, int matnum);
	static bool updateLightMeshes();	// ??
	static bool refreshRenderedImage(RedrawRegion * rgn=NULL);
	static void changeSunPosAndDateFromControls();
	static void fillSunSkyControls();
	static void updateAzimuthAltitude();
	static void refreshTimeInStatus();
	static void refreshCounterInStatus();
	static void changeCamera(Camera * cam);
	static void stoppedRenderingCallback(void);





	static DWORD WINAPI LoadSceneThreadProc(LPVOID lpParameter);
	static DWORD WINAPI MergeSceneThreadProc(LPVOID lpParameter);
	DECLDIR static bool tryLoadSceneDroppedAsync(char * filename, bool andStartRendering=false);
	static bool loadSceneAsync(char * filename, bool andStartRendering=false);
	static bool loadScene(char * filename);
	static bool mergeSceneAsync(char * filename);
	static bool mergeScene(char * filename);
	static bool saveScene(char * filename, bool withTextures, bool silentMode=false);
	static bool saveImage(char * filename);
	static void startRendering(bool withErase=true);
	static void stopRendering();
	static void startStoppingTimer();
	static bool savePostprocess();
	static bool loadPostprocess();
	static bool saveFinalPost();
	static bool loadFinalPost();
	static int sceneNumber;
	


	DECLDIR RendererMainWindow(HINSTANCE hInst);
	DECLDIR ~RendererMainWindow();
};

#endif
