#define _WIN32_WINNT 0x0502
#define DONT_INCLUDE_EVER_RENDERER

#include "max2013predef.h"
#include <windows.h>
#include "NOXplugin.h"
#include "linkRenderer.h"
#include <delayimp.h>
#include <shlobj.h>


extern ClassDesc2* GetNOXRendererDesc();
extern ClassDesc2* GetNOXMaterialDesc();
extern ClassDesc2* GetNOXCameraDesc();
extern ClassDesc2* GetNOXCameraTargetDesc();
extern ClassDesc2* GetNOXExportDesc();
extern ClassDesc2* GetNOXSunLightDesc();
extern ClassDesc2* GetNOXSunTargetDesc();

HINSTANCE hInstance;
int controlsInit = FALSE;

//-------------------------------------------------------------------------------------------------------------

bool dirExists1(TCHAR * dirName)
{
	bool res;
	HANDLE hDir;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hDir = FindFirstFile(dirName, &fd);
	
	if (hDir == INVALID_HANDLE_VALUE   ||   !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hDir != INVALID_HANDLE_VALUE)
		FindClose(hDir);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

TCHAR * getRendererDirectory1()
{
	// get default directory
	DWORD rsize = 2048;
	TCHAR buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software"), 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, _T("Evermotion"), 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, _T("Renderer"), 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048 * sizeof(TCHAR);
		regres = RegQueryValueEx(key3, _T("directory"), NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
	{	// if OK
	}
	else
	{
		return NULL;
	}

	size_t ss = (_tcsclen(buffer)+1);
	TCHAR * res = (TCHAR *)malloc(ss*sizeof(TCHAR));
	if (res)
		_stprintf_s(res, ss, _T("%s"), buffer);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

TCHAR * getNoxUserDirectoryFromReg()
{
	DWORD rsize = 2048;
	TCHAR buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software"), 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, _T("Evermotion"), 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, _T("Renderer"), 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048 * sizeof(TCHAR);
		regres = RegQueryValueEx(key3, _T("userdirectory"), NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres != ERROR_SUCCESS)
		return NULL;

	size_t ss = (_tcsclen(buffer)+1);
	TCHAR * res = (TCHAR *)malloc(ss*sizeof(TCHAR));
	if (res)
		_stprintf_s(res, ss, _T("%s"), buffer);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

TCHAR * getNoxUserDirectory1()
{
	TCHAR szPath[MAX_PATH];
	SHGetSpecialFolderPath(0, szPath, CSIDL_LOCAL_APPDATA, FALSE);
	bool dirok1 = false;
	if (dirExists1(szPath))
	{
		int l = (int)_tcslen(szPath)+30;
		TCHAR * res = (TCHAR *)malloc(l*sizeof(TCHAR));
		_stprintf_s(res, l ,_T("%s\\Evermotion\\NOX"), szPath);
		if (dirExists1(res))
			return res;
		free(res);
	}
	return getRendererDirectory1();
}

//-------------------------------------------------------------------------------------------------------------

bool checkRendererDirs1(TCHAR * dir)
{
	TCHAR dirName[2560];
	_stprintf_s(dirName, 2560, _T("%s"), dir);
	if (!dirExists1(dirName))
	{
		return false;
	}

	#ifdef _WIN64
		_stprintf_s(dirName, 2560, _T("%s\\dll\\64"), dir);
	#else
		_stprintf_s(dirName, 2560, _T("%s\\dll\\32"), dir);
	#endif
	if (!dirExists1(dirName))
	{
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool setDLLDir1()
{
	OSVERSIONINFO osver;
	osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx (&osver);
	if ((osver.dwMajorVersion > 5)  ||
		(osver.dwMajorVersion == 5   &&   osver.dwMinorVersion >= 1))
	{
		TCHAR * dir = getRendererDirectory1();
		if (dir)
		{
			if (checkRendererDirs1(dir))
			{
				TCHAR dirName[2560];
				#ifdef _WIN64
					_stprintf_s(dirName, 2560, _T("%s\\dll\\64"), dir);
				#else
					_stprintf_s(dirName, 2560, _T("%s\\dll\\32"), dir);
				#endif
				SetDllDirectory(dirName);
			}
			else
			{
				TCHAR c[2560];
				#ifdef _WIN64
					_stprintf_s(c, 2560, _T("Directory\n%s\\dll\\64\ndoes not exist.\nApplication may fail to find DLLs and crash."), dir);
				#else
					_stprintf_s(c, 2560, _T("Directory\n%s\\dll\\32\ndoes not exist.\nApplication may fail to find DLLs and crash."), dir);
				#endif
				MessageBox(0, c, _T("Warning"), MB_ICONEXCLAMATION);
				return false;
			}
			free(dir);

		}
		else
		{
			MessageBox(0, _T("Directory not set in registry.\nApplication may fail to find DLLs and crash."), _T("Warning"), MB_ICONEXCLAMATION);
			return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

char * copyStringMax1(const char * src, unsigned int addSpace)
{
	if (!src)
		return NULL;
	int l = (int)strlen(src);
	char * res = (char *)malloc(l+1+addSpace);
	if (res)
		strcpy_s(res, l+1+addSpace, src);
	return res;
}

//-------------------------------------------------------------------------------------------------------------

char * getAnsiCopy1(const TCHAR * src, int * rlen)
{
	if (rlen)
		*rlen = -1;
	if (!src)
		return NULL;

	#ifdef UNICODE
		int rsize = WideCharToMultiByte(CP_ACP, 0, src, (int)wcslen(src)+1, NULL, 0, NULL, NULL);
		rsize += 2;
		char * res_ansi = (char *)malloc(rsize);
		if (!res_ansi)
			return NULL;
		WideCharToMultiByte(CP_ACP, 0, src, (int)wcslen(src)+1, res_ansi, rsize, NULL, NULL);
		if (rlen)
			*rlen = rsize-2;
	#else
		int csize = (int)strlen(src);
		char * res_ansi = (char *)malloc(csize+1);
		if (!res_ansi)
			return NULL;
		strcpy_s(res_ansi, csize+1, src);
		if (rlen)
			*rlen = csize;
	#endif

	return res_ansi;
}

//-------------------------------------------------------------------------------------------------------------

TCHAR * copyAnsiToTchar1(char * src)
{
	if (!src)
		return NULL;

	int l = (int)strlen(src);
	TCHAR * res = NULL;

	#ifdef UNICODE
		res = (wchar_t *)malloc(2*l+2);
		MultiByteToWideChar(CP_ACP, 0, src, l+1, res, l+1);
	#else
		res = (char *)malloc(l+1);
		strcpy_s(res, l+1, src);
	#endif

	return res;
}

//-------------------------------------------------------------------------------------------------------------

DWORD WINAPI Test1ThreadProc(LPVOID lpParameter)
{
		Sleep(100);

		setDLLDir1();
		TrRaytracer::checkNOXVersions();
		TrRaytracer::createMainScene();
		bool loadedscenes = TrRaytracer::loadMatEditorScenes();
		TrMaterial::createBlendSettings();
		TrRaytracer::changeSceneToMain();

		return 1;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;

	if (!controlsInit) 
	{
		controlsInit = TRUE;

		DWORD threadId;		/// imho bad idea
		HANDLE hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(Test1ThreadProc), (LPVOID)0, 0, &threadId);
	}

	return (TRUE);
}

__declspec( dllexport ) const TCHAR* LibDescription()
{
	int v1,v2,v3;
	v1 = TrRaytracer::getVersionMajor();
	v2 = TrRaytracer::getVersionMinor();
	v3 = TrRaytracer::getVersionBuild();

	static TCHAR libdescbuf[512];
	//_stprintf_s(libdescbuf, 512, _T("NOX plugin - www.evermotion.org"));
	_stprintf_s(libdescbuf, 512, _T("NOX plugin - %d.%d.%d - www.evermotion.org"), v1,v2,v3);
	return libdescbuf;

	return GetString(IDS_LIBDESCRIPTION);
}

__declspec( dllexport ) int LibNumberClasses()
{
	return 7;
}

__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	//return 0;
	switch(i) 
	{
		case 0: return GetNOXRendererDesc();
		case 1: return GetNOXMaterialDesc();
		case 2: return GetNOXCameraTargetDesc();
		case 3: return GetNOXCameraDesc();
		case 4: return GetNOXExportDesc();
		case 5: return GetNOXSunLightDesc();
		case 6: return GetNOXSunTargetDesc();
		default: return 0;
	}
}

__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, 256) ? buf : NULL;
	return NULL;
}

__declspec( dllexport ) int LibInitialize()
{  
	return TRUE;
}

__declspec( dllexport ) int LibShutdown()
{  
	return TRUE;
}

extern "C" int __cdecl _purecall(void) { return 0; }
