#include "BinaryScene.h"
#include "raytracer.h"
#include "log.h"
#include <math.h>

#define FWRITE_ULONG8(a)		fwrite((&a), 8, 1, file)
#define FWRITE_UINT(a)			fwrite((&a), 4, 1, file)
#define FWRITE_FLOAT(a)			fwrite((&a), 4, 1, file)
#define FWRITE_USHORT(a) 		fwrite((&a), 2, 1, file)
#define FWRITE_UCHAR(a)			fwrite((&a), 1, 1, file)
#define FWRITE_STRING(a, size)	fwrite((a), (size), 1, file)

//----------------------------------------------------------------------------------------

char * xorStringToNew(char * src, char * pswd, unsigned int lsrc, unsigned int lpswd)
{
	if (!src)
		return NULL;
	if (!pswd)
		return NULL;
	if (!lsrc)
		return NULL;
	if (!lpswd)
		return NULL;

	char * data = (char *)malloc(lsrc+1);
	data[lsrc] = 0;

	for (unsigned int i=0; i<lsrc; i++)
	{
		data[i] = src[i] ^ pswd[i%lpswd];
	}

	return data;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::SaveScene(char * filename, bool addCamBuffers, bool silentMode)
{
	file = NULL;
	showMessages = !silentMode;

	if (fopen_s(&file, filename, "wb"))
	{
		if (showMessages)
			MessageBox(0, "Error. Can't open file.", "Error", 0);
		return false;
	}

	addCamBuffs = addCamBuffers;
	directory = getDirectory(filename);

	geometrySize = 0;
	materialsSize = 0;
	sceneSize = evalSizeScene();


	unsigned int nox_start_header = NOX_BIN_HEADER;
	if (1!=FWRITE_UINT(nox_start_header))
	{
		if (showMessages)
			MessageBox(0, "Writing header error.", "Error", 0);
		return false;
	}

	fileSize = sceneSize + 16;
	if (1!=FWRITE_ULONG8(fileSize))
	{
		if (showMessages)
			MessageBox(0, "Writing file size error.", "Error", 0);
		return false;
	}

	unsigned int noxversion;
	((char*)(&noxversion))[0] = 0;
	((char*)(&noxversion))[1] = (unsigned char)NOX_VER_MAJOR;
	((char*)(&noxversion))[2] = (unsigned char)NOX_VER_MINOR;
	((char*)(&noxversion))[3] = (unsigned char)NOX_VER_BUILD;
	if (1!=FWRITE_UINT(noxversion))
	{
		if (showMessages)
			MessageBox(0, "Writing NOX version error.", "Error", 0);
		return false;
	}


	if (!insertScenePart())
	{
		fclose(file);
		return false;
	}

	fclose(file);

	if (showMessages)
		MessageBox(0, "File saved.", "Info", 0);

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertScenePart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "File is NULL - inner error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_scene_tag = NOX_BIN_SCENE;
	if (1!=FWRITE_USHORT(nox_scene_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing scene tag error.", "Error", 0);
		return false;
	}

	if (sceneSize == 0)
		sceneSize = evalSizeScene();

	if (1!=FWRITE_ULONG8(sceneSize))
	{
		if (showMessages)
			MessageBox(0, "Writing scene size error.", "Error", 0);
		return false;
	}

	if (!insertGeometryPart())
	{
		return false;
	}

	unsigned int numCams = (unsigned int)rtr->curScenePtr->cameras.objCount;
	for (unsigned int i=0; i<numCams; i++)
	{
		if (!insertCameraPart(i))
		{
			return false;
		}
	}

	if (!insertMaterialsAllPart())
	{
		return false;
	}

	if (!insertBlendLayersPart(&rtr->curScenePtr->blendSettings))
	{
		return false;
	}

	if (!insertUserColorsPart())
	{
		return false;
	}

	if (!insertRendererSettingsPart())
	{
		return false;
	}

	if (Raytracer::getInstance()->curScenePtr->sscene.sunsky)
	{
		if (!insertSunskyPart())
		{
			return false;
		}
	}

	if (!insertEnvironmentMapPart())
	{
		return false;
	}

	if (!insertSceneInfoPart())
	{
		return false;
	}

	if (!insertSceneSettingsPart())
	{
		return false;
	}

	if (!insertUserPresets())
	{
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;

	if (realSize != sceneSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Scene chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sceneSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}
 
	_fseeki64(file, 0, SEEK_END);
	unsigned long long fileEnd = _ftelli64(file);
	if (fileEnd != fileSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. File size is incorrect.\nShould be %llu instead of %llu bytes.", fileSize, fileEnd);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertGeometryPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "File is NULL - inner error.", "Error", 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);

	Raytracer * rtr = Raytracer::getInstance();
	unsigned int numMeshes = rtr->curScenePtr->meshes.objCount;
	unsigned int numInstances = rtr->curScenePtr->instances.objCount;
	unsigned int numberTris = rtr->curScenePtr->triangles.objCount;
	unsigned int triSize = 106;



	unsigned short nox_geom_tag = NOX_BIN_GEOMETRY;
	if (1!=FWRITE_USHORT(nox_geom_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing scene tag error.", "Error", 0);
		return false;
	}

	if (geometrySize == 0)
		geometrySize = evalSizeGeometry();

	if (1!=FWRITE_ULONG8(geometrySize))
	{
		if (showMessages)
			MessageBox(0, "Writing geometry size error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_UINT(numMeshes))
	{
		if (showMessages)
			MessageBox(0, "Writing number of meshes error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_UINT(triSize))
	{
		if (showMessages)
			MessageBox(0, "Writing size of triangle error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_UINT(numberTris))
	{
		if (showMessages)
			MessageBox(0, "Writing total number of triangles error.", "Error", 0);
		return false;
	}

	unsigned short nox_scale_tag = NOX_BIN_SCALE_SCENE;
	if (1!=FWRITE_USHORT(nox_scale_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing geometry scale tag error.", "Error", 0);
		return false;
	}

	float scale = rtr->curScenePtr->sscene.scaleScene;
	if (1!=FWRITE_FLOAT(scale))
	{
		if (showMessages)
			MessageBox(0, "Writing geometry scale error.", "Error", 0);
		return false;
	}

	for (unsigned int i=0; i<numMeshes; i++)
	{
		if (!insertInstanceSourcePart(i))
		{
			return false;
		}
	}

	for (unsigned int i=0; i<numInstances; i++)
	{
		if (!insertInstancePart(i))
		{
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != geometrySize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Geometry chunk size is incorrect.\nShould be %llu instead of %llu bytes.", geometrySize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertInstancePart(unsigned int iId)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner plugin error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	if (iId < 0   ||   iId >= (unsigned int)sc->instances.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Instance ID out of bounds.", "Error", 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);
	unsigned short tag_inst= NOX_BIN_INSTANCE;
	if (1!=FWRITE_USHORT(tag_inst))
	{
		if (showMessages)
			MessageBox(0, "Writing instance tag error.", "Error", 0);
		return false;
	}

	unsigned long long instSize = evalSizeInstancePart(iId);
	if (1!=FWRITE_ULONG8(instSize))
	{
		if (showMessages)
			MessageBox(0, "Writing instance size error.", "Error", 0);
		return false;
	}

	unsigned int meshID = sc->instances[iId].meshID;
	if (sc->instances[iId].displace_orig_ID>-1)
		meshID = sc->instances[iId].displace_orig_ID;
	if (1!=FWRITE_UINT(meshID))
	{
		if (showMessages)
			MessageBox(0, "Writing instance ID error.", "Error", 0);
		return false;
	}

	if (sc->instances[iId].name)
	{
		unsigned int namesize = 6 + (unsigned int)strlen(sc->instances[iId].name);
		unsigned short tag_inst_name = NOX_BIN_INSTANCE_NAME;
		if (1!=FWRITE_USHORT(tag_inst_name))
		{
			if (showMessages)
				MessageBox(0, "Writing instance name tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(namesize))
		{
			if (showMessages)
				MessageBox(0, "Writing instance name size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(sc->instances[iId].name, (namesize-6)))
		{
			if (showMessages)
				MessageBox(0, "Writing instance name error.", "Error", 0);
			return false;
		}
	}

	unsigned short tag_inst_matrix = NOX_BIN_INSTANCE_MATRIX;
	if (1!=FWRITE_USHORT(tag_inst_matrix))
	{
		if (showMessages)
			MessageBox(0, "Writing instance transformation matrix tag error.", "Error", 0);
		return false;
	}
	for (int i=0; i<4; i++)
		for (int j=0; j<4; j++)
		{
			float val = sc->instances[iId].matrix_transform.M[i][j];
			if (1!=FWRITE_FLOAT(val))
			{
				if (showMessages)
					MessageBox(0, "Writing instance transformation matrix error.", "Error", 0);
				return false;
			}
		}

	unsigned short tag_inst_mats = NOX_BIN_INSTANCE_MATERIALS;
	if (1!=FWRITE_USHORT(tag_inst_mats))
	{
		if (showMessages)
			MessageBox(0, "Writing instance materials tag error.", "Error", 0);
		return false;
	}
	unsigned short nummats = sc->instances[iId].matsFromFile.objCount;
	if (1!=FWRITE_USHORT(nummats))
	{
		if (showMessages)
			MessageBox(0, "Writing instance materials count error.", "Error", 0);
		return false;
	}
	for (int i=0; i<nummats; i++)
	{
		unsigned int matID = sc->instances[iId].matsFromFile[i];		
		if (1!=FWRITE_UINT(matID))
		{
			if (showMessages)
				MessageBox(0, "Writing instance material ID error.", "Error", 0);
			return false;
		}
	}

	unsigned short tag_mb_on = NOX_BIN_INSTANCE_MOTION_BLUR;
	if (1!=FWRITE_USHORT(tag_mb_on))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short mb_on = sc->instances[iId].mb_on ? 1 : 0;
	if (1!=FWRITE_USHORT(mb_on))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Enabled error.", "Error", 0);
		return false;
	}

	unsigned short tag_mb_move = NOX_BIN_INSTANCE_MB_POS;
	if (1!=FWRITE_USHORT(tag_mb_move))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Move tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(sc->instances[iId].mb_velocity.x))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Move error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(sc->instances[iId].mb_velocity.y))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Move error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(sc->instances[iId].mb_velocity.z))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Move error.", "Error", 0);
		return false;
	}

	unsigned short tag_mb_rot = NOX_BIN_INSTANCE_MB_ANGLE;
	if (1!=FWRITE_USHORT(tag_mb_rot))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Rotation tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(sc->instances[iId].mb_rot_vel))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Rotation error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(sc->instances[iId].mb_rot_axis.x))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Rotation error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(sc->instances[iId].mb_rot_axis.y))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Rotation error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(sc->instances[iId].mb_rot_axis.z))
	{
		if (showMessages)
			MessageBox(0, "Writing Instance Motion Blur Rotation error.", "Error", 0);
		return false;
	}

	unsigned short tag_inst_offset_matrix = NOX_BIN_INSTANCE_OFFSET_MATRIX;
	if (1!=FWRITE_USHORT(tag_inst_offset_matrix))
	{
		if (showMessages)
			MessageBox(0, "Writing instance offset matrix tag error.", "Error", 0);
		return false;
	}
	for (int i=0; i<4; i++)
		for (int j=0; j<4; j++)
		{
			float val = sc->instances[iId].matrix_offset.M[i][j];
			if (1!=FWRITE_FLOAT(val))
			{
				if (showMessages)
					MessageBox(0, "Writing instance offset matrix error.", "Error", 0);
				return false;
			}
		}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != instSize)
	{
		char errmsg[512];
		sprintf_s(errmsg, 512, "Save error. Instance chunk size is incorrect.\nShould be %llu instead of %llu bytes.", instSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertInstanceSourcePart(unsigned int mId)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner plugin error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	if (mId < 0   ||   mId >= (unsigned int)sc->meshes.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Instance source ID out of bounds.", "Error", 0);
		return false;
	}

	Mesh * mesh = &(sc->meshes[mId]);
	if (mesh->displacement_instance_id > -1)
		return true;

	unsigned long long chunkStart = _ftelli64(file);
	unsigned short tag_inst_src = NOX_BIN_INSTANCE_SOURCE;
	if (1!=FWRITE_USHORT(tag_inst_src))
	{
		if (showMessages)
			MessageBox(0, "Writing instance source tag error.", "Error", 0);
		return false;
	}

	unsigned long long instSize = evalSizeInstanceSourcePart(mId);
	if (1!=FWRITE_ULONG8(instSize))
	{
		if (showMessages)
			MessageBox(0, "Writing instance source size error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_UINT(mId))
	{
		if (showMessages)
			MessageBox(0, "Writing instance source ID error.", "Error", 0);
		return false;
	}

	if (!insertMeshPart(mId))
		return false;

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != instSize)
	{
		char errmsg[512];
		sprintf_s(errmsg, 512, "Save error. Instance source chunk size is incorrect.\nShould be %llu instead of %llu bytes.", instSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertMeshPart(unsigned int mNum)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner plugin error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	if (mNum < 0   ||   mNum >= (unsigned int)rtr->curScenePtr->meshes.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save mesh that doesn't exist.", "Error", 0);
		return false;
	}

	Mesh &mesh = rtr->curScenePtr->meshes[mNum];

	unsigned long long chunkStart = _ftelli64(file);
	unsigned int numFaces = mesh.lastTri - mesh.firstTri + 1;



	unsigned short nox_mesh_tag = NOX_BIN_MESH;
	if (1!=FWRITE_USHORT(nox_mesh_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing mesh tag error.", "Error", 0);
		return false;
	}

	unsigned long long meshSize = evalSizeMesh(mNum);
	if (1!=FWRITE_ULONG8(meshSize))
	{
		if (showMessages)
			MessageBox(0, "Writing mesh size error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_UINT(numFaces))
	{
		if (showMessages)
			MessageBox(0, "Writing number of triangles in mesh error.", "Error", 0);
		return false;
	}

	char * name = mesh.name;
	if (!name)
		name = "Unnamed";
	unsigned int lname = (unsigned int)strlen(name);
	if(lname==0)
	{
		name = "Unnamed";
		lname = 7;
	}
	lname +=6;		

	unsigned short nox_meshname_tag = NOX_BIN_MESHNAME;
	if (1!=FWRITE_USHORT(nox_meshname_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing mesh name tag error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_UINT(lname))
	{
		if (showMessages)
			MessageBox(0, "Writing mesh name size error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_STRING(name, (lname-6)))
	{
		if (showMessages)
			MessageBox(0, "Writing mesh name error.", "Error", 0);
		return false;
	}

	for (unsigned int i=(unsigned int)mesh.firstTri; i<=(unsigned int)mesh.lastTri; i++)
	{
		if (!insertTrianglePart(i))
		{
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != meshSize)
	{
		char errmsg[512];
		sprintf_s(errmsg, 512, "Save error. Mesh chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n\n%s", meshSize, realSize, name);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertTrianglePart(unsigned int tNum)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner plugin error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	if (tNum < 0   ||   tNum >= (unsigned int)rtr->curScenePtr->triangles.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save triangle that doesn't exist.", "Error", 0);
		return false;
	}

	Triangle tri = rtr->curScenePtr->triangles[tNum];

	char triBuf[200];

	*((unsigned short*)&(triBuf[0])) = NOX_BIN_TRIANGLE;

	*((unsigned int *)&(triBuf[2])) = tri.matInInst;
	*((unsigned int *)&(triBuf[6 ])) = 0;
	*((float *)&(triBuf[10])) = tri.V1.x;
	*((float *)&(triBuf[14])) = tri.V1.y;
	*((float *)&(triBuf[18])) = tri.V1.z;
	*((float *)&(triBuf[22])) = tri.V2.x;
	*((float *)&(triBuf[26])) = tri.V2.y;
	*((float *)&(triBuf[30])) = tri.V2.z;
	*((float *)&(triBuf[34])) = tri.V3.x;
	*((float *)&(triBuf[38])) = tri.V3.y;
	*((float *)&(triBuf[42])) = tri.V3.z;
	*((float *)&(triBuf[46])) = tri.N1.x;
	*((float *)&(triBuf[50])) = tri.N1.y;
	*((float *)&(triBuf[54])) = tri.N1.z;
	*((float *)&(triBuf[58])) = tri.N2.x;
	*((float *)&(triBuf[62])) = tri.N2.y;
	*((float *)&(triBuf[66])) = tri.N2.z;
	*((float *)&(triBuf[70])) = tri.N3.x;
	*((float *)&(triBuf[74])) = tri.N3.y;
	*((float *)&(triBuf[78])) = tri.N3.z;
	*((float *)&(triBuf[82])) = tri.UV1.x;
	*((float *)&(triBuf[86])) = tri.UV1.y;
	*((float *)&(triBuf[90])) = tri.UV2.x;
	*((float *)&(triBuf[94])) = tri.UV2.y;
	*((float *)&(triBuf[98])) = tri.UV3.x;
	*((float *)&(triBuf[102]))= tri.UV3.y;


	if (1!= fwrite(triBuf, 106, 1, file))
	{
		if (showMessages)
			MessageBox(0, "Error writing triangle.", "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertMaterialsAllPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long matsSize = evalSizeMaterials();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_materials_tag = NOX_BIN_MATERIALS;
	if (1!=FWRITE_USHORT(nox_materials_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing materials tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(matsSize))
	{
		if (showMessages)
			MessageBox(0, "Writing materials size error.", "Error", 0);
		return false;
	}
	unsigned int numMats = (unsigned int)rtr->curScenePtr->mats.objCount;
	if (1!=FWRITE_UINT(numMats))
	{
		if (showMessages)
			MessageBox(0, "Writing materials number error.", "Error", 0);
		return false;
	}

	for (unsigned int i=0; i<numMats; i++)
	{
		if (!insertMaterialSinglePart(i))
			return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matsSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. Materials chunk size is incorrect.\nShould be %llu instead of %llu bytes.", matsSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertMaterialSinglePart(unsigned int mNum)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (mNum >= (unsigned int)rtr->curScenePtr->mats.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save material that doesn't exist.", "Error", 0);
		return false;
	}

	MaterialNox * mat = rtr->curScenePtr->mats[mNum];
	if (!mat)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save NULL material.", "Error", 0);
		return false;
	}

	char * matname = NULL;
	matname = mat->name;
	int matnamelength;
	if (matname)
	{
		matnamelength = (int)strlen(matname)+6;
		if (matnamelength == 6)
		{
			matname = "Unnamed";
			matnamelength = 7+6;
		}
	}
	else
	{
		matname = "Unnamed";
		matnamelength = 7+6;	// "Unnamed"
	}

	char * texOpac = mat->tex_opacity.fullfilename;
	int lOpac = texOpac ? ((int)strlen(texOpac)+6) : 0;
	float opacity = mat->opacity;
	unsigned short use_tex_opacity = mat->use_tex_opacity ? 1 : 0;

	unsigned int nLayers = mat->layers.objCount;
	unsigned int blend = mat->blendIndex;
	float displacement_depth = mat->displ_depth;
	float displacement_shift = mat->displ_shift;
	unsigned short displacement_subdivs = mat->displ_subdivs_lvl;
	int displacement_normal_mode = mat->displ_normal_mode;

	unsigned long long matSize = evalSizeMaterial(mNum);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_material_tag = NOX_BIN_MATERIAL;
	if (1!=FWRITE_USHORT(nox_material_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(matSize))
	{
		if (showMessages)
			MessageBox(0, "Writing material size error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(mat->id))
	{
		if (showMessages)
			MessageBox(0, "Writing material ID error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(blend))
	{
		if (showMessages)
			MessageBox(0, "Writing material blend layer error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(nLayers))
	{
		if (showMessages)
			MessageBox(0, "Writing material layers number error.", "Error", 0);
		return false;
	}
	
	unsigned short nox_matname_tag = NOX_BIN_MATNAME;
	if (1!=FWRITE_USHORT(nox_matname_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material name tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(matnamelength))
	{
		if (showMessages)
			MessageBox(0, "Writing material name length error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_STRING(matname, (matnamelength-6)))
	{
		if (showMessages)
			MessageBox(0, "Writing material name error.", "Error", 0);
		return false;
	}

	unsigned short nox_mat_skyportal_tag = NOX_BIN_MAT_SKYPORTAL;
	if (1!=FWRITE_USHORT(nox_mat_skyportal_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material skyportal tag error.", "Error", 0);
		return false;
	}
	unsigned short isskyportal = mat->isSkyPortal ? 1 : 0;
	if (1!=FWRITE_USHORT(isskyportal))
	{
		if (showMessages)
			MessageBox(0, "Writing material skyportal error.", "Error", 0);
		return false;
	}

	unsigned short nox_mat_matte_tag = NOX_BIN_MAT_MATTE_SHADOW;
	if (1!=FWRITE_USHORT(nox_mat_matte_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material matte shadow tag error.", "Error", 0);
		return false;
	}
	unsigned short ismatte = mat->isMatteShadow ? 1 : 0;
	if (1!=FWRITE_USHORT(ismatte))
	{
		if (showMessages)
			MessageBox(0, "Writing material matte shadow error.", "Error", 0);
		return false;
	}

	unsigned short nox_matopacity_tag = NOX_BIN_MAT_OPACITY;
	if (1!=FWRITE_USHORT(nox_matopacity_tag))
	{
		MessageBox(0, "Writing material opacity tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(opacity))
	{
		MessageBox(0, "Writing material opacity error.", "Error", 0);
		return false;
	}

	unsigned short nox_matdispldepth_tag = NOX_BIN_MAT_DISPLACEMENT_DEPTH;
	if (1!=FWRITE_USHORT(nox_matdispldepth_tag))
	{
		MessageBox(0, "Writing material displacement depth tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(displacement_depth))
	{
		MessageBox(0, "Writing material displacement depth error.", "Error", 0);
		return false;
	}

	unsigned short nox_matdisplsubdivs_tag = NOX_BIN_MAT_DISPLACEMENT_SUBDIVS;
	if (1!=FWRITE_USHORT(nox_matdisplsubdivs_tag))
	{
		MessageBox(0, "Writing material displacement subdivs tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(displacement_subdivs))
	{
		MessageBox(0, "Writing material displacement subdivs error.", "Error", 0);
		return false;
	}

	unsigned short nox_mat_disp_continuity_tag = NOX_BIN_MAT_DISPLACEMENT_CONTINUITY;
	if (1!=FWRITE_USHORT(nox_mat_disp_continuity_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material displacement continuity tag error.", "Error", 0);
		return false;
	}
	unsigned short dcontinuity = mat->displ_continuity ? 1 : 0;
	if (1!=FWRITE_USHORT(dcontinuity))
	{
		if (showMessages)
			MessageBox(0, "Writing material displacement continuity error.", "Error", 0);
		return false;
	}

	unsigned short nox_mat_disp_ignscale_tag = NOX_BIN_MAT_DISPLACEMENT_IGNORE_SCALE;
	if (1!=FWRITE_USHORT(nox_mat_disp_ignscale_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material displacement ignore scale tag error.", "Error", 0);
		return false;
	}
	unsigned short dignsc = mat->displ_ignore_scale ? 1 : 0;
	if (1!=FWRITE_USHORT(dignsc))
	{
		if (showMessages)
			MessageBox(0, "Writing material displacement ignore scale error.", "Error", 0);
		return false;
	}

	unsigned short nox_matdisplshift_tag = NOX_BIN_MAT_DISPLACEMENT_SHIFT;
	if (1!=FWRITE_USHORT(nox_matdisplshift_tag))
	{
		MessageBox(0, "Writing material displacement shift tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(displacement_shift))
	{
		MessageBox(0, "Writing material displacement shift error.", "Error", 0);
		return false;
	}

	unsigned short nox_matdisplnormalmode_tag = NOX_BIN_MAT_DISPLACEMENT_NORMAL_MODE;
	if (1!=FWRITE_USHORT(nox_matdisplnormalmode_tag))
	{
		MessageBox(0, "Writing material displacement normal mode tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(displacement_normal_mode))
	{
		MessageBox(0, "Writing material displacement normal mode error.", "Error", 0);
		return false;
	}

	unsigned short nox_matopacity_usetex_tag = NOX_BIN_MAT_USE_TEX_OPACITY;
	if (1!=FWRITE_USHORT(nox_matopacity_usetex_tag))
	{
		MessageBox(0, "Writing material opacity use texture tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(use_tex_opacity))
	{
		MessageBox(0, "Writing material opacity use texture error.", "Error", 0);
		return false;
	}

	if (!insertTexturePart(mNum, 0, MAT_TEX_SLOT_OPACITY))
		return false;
	if (!insertTexturePart(mNum, 0, MAT_TEX_SLOT_DISPLACEMENT))
		return false;

	for (unsigned int i=0; i<nLayers; i++)
	{
		if (!insertMatLayerPart(mNum, i))
			return false;
	}

	if (mat->preview   &&   mat->preview->imgBuf)
	{
		if (!insertMatPreviewPart(mNum))
			return false;
	}		

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matSize)
	{
		char errmsg[512];
		sprintf_s(errmsg, 512, "Save error. Material chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n%s", matSize, realSize, matname);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertMatLayerPart(unsigned int mNum, unsigned int lNum)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (mNum >= (unsigned int)rtr->curScenePtr->mats.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save material layer from material that doesn't exist.", "Error", 0);
		return false;
	}

	MaterialNox * mat = rtr->curScenePtr->mats[mNum];
	if (!mat)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save material layer from NULL material.", "Error", 0);
		return false;
	}

	if (lNum >= (unsigned int)mat->layers.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save material layer that doesn't exist.", "Error", 0);
		return false;
	}

	MatLayer * mlay = &(mat->layers[lNum]);
	if (!mlay)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save NULL material layer.", "Error", 0);
		return false;
	}

	unsigned long long layerSize = evalSizeMaterialLayer(mNum, lNum);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_layer_tag = NOX_BIN_MATLAYER;
	if (1!=FWRITE_USHORT(nox_layer_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(layerSize))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer size error.", "Error", 0);
		return false;
	}
	unsigned short layType = mlay->type;
	if (1!=FWRITE_USHORT(layType))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer type error.", "Error", 0);
		return false;
	}
	unsigned int layContr = mlay->contribution;
	if (1!=FWRITE_UINT(layContr))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer contribution error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_refl0_tag = NOX_BIN_MLAY_REFL0;
	if (1!=FWRITE_USHORT(nox_mlay_refl0_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color0 tag error.", "Error", 0);
		return false;
	}
	float r,g,b,a;
	r = mlay->refl0.r;
	g = mlay->refl0.g;
	b = mlay->refl0.b;
	a = mlay->refl0.a;
	if (1!=FWRITE_FLOAT(r))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color0 error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color0 error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color0 error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color0 error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_refl90_tag = NOX_BIN_MLAY_REFL90;
	if (1!=FWRITE_USHORT(nox_mlay_refl90_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color90 tag error.", "Error", 0);
		return false;
	}
	r = mlay->refl90.r;
	g = mlay->refl90.g;
	b = mlay->refl90.b;
	a = mlay->refl90.a;
	if (1!=FWRITE_FLOAT(r))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color90 error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color90 error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color90 error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color90 error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_connect90_tag = NOX_BIN_MLAY_CONNECT90;
	if (1!=FWRITE_USHORT(nox_mlay_connect90_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer refl0/90 connection tag error.", "Error", 0);
		return false;
	}
	unsigned short conn90 = mlay->connect90 ? 1 : 0;
	if (1!=FWRITE_USHORT(conn90))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer refl0/90 connection error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_rough_tag = NOX_BIN_MLAY_ROUGH;
	if (1!=FWRITE_USHORT(nox_mlay_rough_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer roughness tag error.", "Error", 0);
		return false;
	}
	float rough = mlay->roughness;
	if (1!=FWRITE_FLOAT(rough))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer roughness error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_fresnel_tag = NOX_BIN_MLAY_FRESNEL;
	if (1!=FWRITE_USHORT(nox_mlay_fresnel_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer fresnel tag error.", "Error", 0);
		return false;
	}
	unsigned int freslen = 12;
	if (1!=FWRITE_UINT(freslen))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer fresnel size error.", "Error", 0);
		return false;
	}
	unsigned short nox_mlay_fresnel_ior_tag = NOX_BIN_MLAY_FRES_IOR;
	if (1!=FWRITE_USHORT(nox_mlay_fresnel_ior_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer fresnel ior tag error.", "Error", 0);
		return false;
	}
	float ior = mlay->fresnel.IOR;
	if (1!=FWRITE_FLOAT(ior))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer fresnel ior error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_transm_tag = NOX_BIN_MLAY_TRANSM_ON;
	if (1!=FWRITE_USHORT(nox_mlay_transm_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer transmission on tag error.", "Error", 0);
		return false;
	}
	unsigned short transmON = mlay->transmOn ? 1 : 0;
	if (1!=FWRITE_USHORT(transmON))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer transmission on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_tr_color_tag = NOX_BIN_MLAY_TRANSM_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_tr_color_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer transmission color tag error.", "Error", 0);
		return false;
	}
	r = mlay->transmColor.r;
	g = mlay->transmColor.g;
	b = mlay->transmColor.b;
	a = mlay->transmColor.a;
	if (1!=FWRITE_FLOAT(r))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer transmission color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer transmission color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer transmission color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer transmission color error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_fakeglass_tag = NOX_BIN_MLAY_FAKEGLASS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_fakeglass_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer fake glass on tag error.", "Error", 0);
		return false;
	}
	unsigned short fakeglassON = mlay->fakeGlass ? 1 : 0;
	if (1!=FWRITE_USHORT(fakeglassON))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer fake glass on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_abs_on_tag = NOX_BIN_MLAY_ABS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_abs_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption on tag error.", "Error", 0);
		return false;
	}
	unsigned short absON = mlay->absOn ? 1 : 0;
	if (1!=FWRITE_USHORT(absON))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_abs_color_tag = NOX_BIN_MLAY_ABS_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_abs_color_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption color tag error.", "Error", 0);
		return false;
	}
	r = mlay->absColor.r;
	g = mlay->absColor.g;
	b = mlay->absColor.b;
	a = mlay->absColor.a;
	if (1!=FWRITE_FLOAT(r))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption color error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_abs_dist_tag = NOX_BIN_MLAY_ABS_DIST;
	if (1!=FWRITE_USHORT(nox_mlay_abs_dist_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption distance tag error.", "Error", 0);
		return false;
	}
	float absDist = mlay->absDist;
	if (1!=FWRITE_FLOAT(absDist))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer absorption distance error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_disp_on_tag = NOX_BIN_MLAY_DISP_ON;
	if (1!=FWRITE_USHORT(nox_mlay_disp_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer dispersion on tag error.", "Error", 0);
		return false;
	}
	unsigned short dispON =mlay->dispersionOn ? 1 : 0;
	if (1!=FWRITE_USHORT(dispON))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer dispersion on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_abs_disp_val_tag = NOX_BIN_MLAY_DISP_VALUE;
	if (1!=FWRITE_USHORT(nox_mlay_abs_disp_val_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer dispersion value tag error.", "Error", 0);
		return false;
	}
	float dispVal = mlay->dispersionValue;
	if (1!=FWRITE_FLOAT(dispVal))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer dispersion value error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_em_color_tag = NOX_BIN_MLAY_EMISS_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_em_color_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission color tag error.", "Error", 0);
		return false;
	}
	r = mlay->color.r;
	g = mlay->color.g;
	b = mlay->color.b;
	a = mlay->color.a;
	if (1!=FWRITE_FLOAT(r))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission color error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission color error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_em_temp_tag = NOX_BIN_MLAY_EMISS_TEMP;
	if (1!=FWRITE_USHORT(nox_mlay_em_temp_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission temperature tag error.", "Error", 0);
		return false;
	}
	unsigned int emTemp =  mlay->emitterTemperature;
	if (1!=FWRITE_UINT(emTemp))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission temperature error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_em_pow_tag = NOX_BIN_MLAY_EMISS_POWER;
	if (1!=FWRITE_USHORT(nox_mlay_em_pow_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission power tag error.", "Error", 0);
		return false;
	}
	float emPow =  mlay->power;
	if (1!=FWRITE_FLOAT(emPow))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission power error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_em_unit_tag = NOX_BIN_MLAY_EMISS_UNIT;
	if (1!=FWRITE_USHORT(nox_mlay_em_unit_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission unit tag error.", "Error", 0);
		return false;
	}
	unsigned int emUnit =  mlay->unit;
	if (1!=FWRITE_UINT(emUnit))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission unit error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_em_inv_tag = NOX_BIN_MLAY_EMISS_INVISIBLE;
	if (1!=FWRITE_USHORT(nox_mlay_em_inv_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission invisible tag error.", "Error", 0);
		return false;
	}
	unsigned short eminv =  mlay->emissInvisible ? 1 : 0;
	if (1!=FWRITE_USHORT(eminv))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission invisible error.", "Error", 0);
		return false;
	}


	if (mlay->ies  &&  mlay->ies->filename)
	{
		char * ffname = NULL;
		if (mlay->ies->exportedFilename)
			ffname = mlay->ies->exportedFilename;
		else
			ffname = mlay->ies->filename;
		int iesSize = ((int)strlen(ffname)+6);

		unsigned short nox_ies_filename_tag = NOX_BIN_MLAY_EMISS_IES;
		if (1!=FWRITE_USHORT(nox_ies_filename_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing IES filename tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(iesSize))
		{
			if (showMessages)
				MessageBox(0, "Writing IES filename size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(ffname, (iesSize-6)))
		{
			if (showMessages)
				MessageBox(0, "Writing IES filename error.", "Error", 0);
			return false;
		}
	}

	unsigned short nox_mlay_sss_on_tag = NOX_BIN_MLAY_SSS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_sss_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer SSS on tag error.", "Error", 0);
		return false;
	}
	unsigned short sssON =mlay->sssON ? 1 : 0;
	if (1!=FWRITE_USHORT(sssON))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer SSS on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_sss_dens_tag = NOX_BIN_MLAY_SSS_DENSITY;
	if (1!=FWRITE_USHORT(nox_mlay_sss_dens_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer SSS density tag error.", "Error", 0);
		return false;
	}
	float sssDens =  mlay->sssDens;
	if (1!=FWRITE_FLOAT(sssDens))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer SSS density error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_sss_dir_tag = NOX_BIN_MLAY_SSS_DIRECTION;
	if (1!=FWRITE_USHORT(nox_mlay_sss_dir_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer SSS collision direction tag error.", "Error", 0);
		return false;
	}
	float sssDir =  mlay->sssCollDir;
	if (1!=FWRITE_FLOAT(sssDir))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer SSS collisiion direction error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_aniso_tag = NOX_BIN_MLAY_ANISOTROPY;
	if (1!=FWRITE_USHORT(nox_mlay_aniso_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer anisotropy tag error.", "Error", 0);
		return false;
	}
	float aniso =  mlay->anisotropy;
	if (1!=FWRITE_FLOAT(aniso))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer anisotropy error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_anisoangle_tag = NOX_BIN_MLAY_ANISOTROPY_ANGLE;
	if (1!=FWRITE_USHORT(nox_mlay_anisoangle_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer anisotropy angle tag error.", "Error", 0);
		return false;
	}
	float anisoa =  mlay->aniso_angle;
	if (1!=FWRITE_FLOAT(anisoa))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer anisotropy angle error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_refl0_tag = NOX_BIN_MLAY_REFL0_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_refl0_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color0 texture on tag error.", "Error", 0);
		return false;
	}
	unsigned short r0texOn = mlay->use_tex_col0 ? 1 : 0;
	if (1!=FWRITE_USHORT(r0texOn))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color0 texture on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_refl90_tag = NOX_BIN_MLAY_REFL90_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_refl90_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color90 texture on tag error.", "Error", 0);
		return false;
	}
	unsigned short r90texOn = mlay->use_tex_col90 ? 1 : 0;
	if (1!=FWRITE_USHORT(r90texOn))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer color90 texture on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_rough_tag = NOX_BIN_MLAY_ROUGH_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_rough_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer roughness texture on tag error.", "Error", 0);
		return false;
	}
	unsigned short roughTexOn = mlay->use_tex_rough ? 1 : 0;
	if (1!=FWRITE_USHORT(roughTexOn))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer roughness texture on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_weight_tag = NOX_BIN_MLAY_WEIGHT_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_weight_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer contribution texture on tag error.", "Error", 0);
		return false;
	}
	unsigned short weightTexOn = mlay->use_tex_weight ? 1 : 0;
	if (1!=FWRITE_USHORT(weightTexOn))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer contribution texture on error.", "Error", 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_light_tag = NOX_BIN_MLAY_LIGHT_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_light_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission texture on tag error.", "Error", 0);
		return false;
	}
	unsigned short lightTexOn = mlay->use_tex_light ? 1 : 0;
	if (1!=FWRITE_USHORT(lightTexOn))
	{
		if (showMessages)
			MessageBox(0, "Writing material layer emission texture on error.", "Error", 0);
		return false;
	}

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_LIGHT))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_COLOR0))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_COLOR90))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_ROUGHNESS))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_WEIGHT))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_NORMAL))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_TRANSM))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_ANISOTROPY))
		return false;

	if (!insertTexturePart(mNum, lNum, LAYER_TEX_SLOT_ANISO_ANGLE))
		return false;

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != layerSize)
	{
		char * matname = mat->name;
		if (!matname)
			matname = "Unnamed";
		char errmsg[1024];
		sprintf_s(errmsg, 1024, "Export error. Layer (%d) chunk in material (%s) size is incorrect.\nShould be %llu instead of %llu bytes.", (lNum+1), matname, layerSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertTexturePart(unsigned int mNum, unsigned int lNum, unsigned int slot)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	if (mNum >= (unsigned int)rtr->curScenePtr->mats.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save texture from material that doesn't exist.", "Error", 0);
		return false;
	}

	MaterialNox * mat = NULL;
	MatLayer * mlay = NULL;
	
	if (slot != TEX_SLOT_ENV)
	{
		mat = rtr->curScenePtr->mats[mNum];
		if (!mat)
		{
			if (showMessages)
				MessageBox(0, "Error. Tried to save texture from NULL material.", "Error", 0);
			return false;
		}

		if (lNum >= (unsigned int)mat->layers.objCount)
		{
			if (showMessages)
				MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
			return false;
		}

		mlay = &(mat->layers[lNum]);
	}

	bool use_tex = false;
	char * texffname = NULL;
	TextureInstance * texptr = NULL;
	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_col0);
			use_tex = mlay->use_tex_col0;
			break;
		case LAYER_TEX_SLOT_COLOR90:		
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_col90);
			use_tex = mlay->use_tex_col90;
			break;
		case LAYER_TEX_SLOT_ROUGHNESS:		
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_rough);
			use_tex = mlay->use_tex_rough;
			break;
		case LAYER_TEX_SLOT_WEIGHT:			
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_weight);
			use_tex = mlay->use_tex_weight;
			break;
		case LAYER_TEX_SLOT_LIGHT:			
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_light);
			use_tex = mlay->use_tex_light;
			break;
		case LAYER_TEX_SLOT_TRANSM:			
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_transm);
			use_tex = mlay->use_tex_transm;
			break;
		case LAYER_TEX_SLOT_ANISOTROPY:			
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_aniso);
			use_tex = mlay->use_tex_aniso;
			break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_aniso_angle);
			use_tex = mlay->use_tex_aniso_angle;
			break;
		case LAYER_TEX_SLOT_NORMAL:		
			if (!mlay)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material layer that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mlay->tex_normal);
			use_tex = mlay->use_tex_normal;
			break;
		case MAT_TEX_SLOT_OPACITY:			
			if (!mat)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mat->tex_opacity);
			use_tex = mat->use_tex_opacity;
			break;
		case MAT_TEX_SLOT_DISPLACEMENT:			
			if (!mat)
			{
				if (showMessages)
					MessageBox(0, "Error. Tried to save texture from material that doesn't exist.", "Error", 0);
				return false;
			}
			texptr = &(mat->tex_displacement);
			use_tex = mat->use_tex_displacement;
			break;
		case TEX_SLOT_ENV:			
			texptr = &(rtr->curScenePtr->env.tex);
			use_tex = rtr->curScenePtr->env.enabled;
			break;
	}

	if (!texptr)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save texture from material (layer) slot that doesn't exist.", "Error", 0);
		return false;
	}

	texffname = (texptr->exportedFilename ? texptr->exportedFilename : texptr->fullfilename);
	unsigned int ltLen = texffname  ? ((int)strlen(texffname)+6) : 0;

	unsigned long long textureSize = evalSizeTexture(mNum, lNum, slot);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_texture_tag = NOX_BIN_MLAY_TEXTURE;
	if (1!=FWRITE_USHORT(nox_texture_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(textureSize))
	{
		if (showMessages)
			MessageBox(0, "Writing texture size error.", "Error", 0);
		return false;
	}
	unsigned short tex_type = 0;
	if (1!=FWRITE_USHORT(tex_type))
	{
		if (showMessages)
			MessageBox(0, "Writing texture type error.", "Error", 0);
		return false;
	}
	unsigned short tex_slot = slot;
	if (1!=FWRITE_USHORT(tex_slot))
	{
		if (showMessages)
			MessageBox(0, "Writing texture slot error.", "Error", 0);
		return false;
	}

	if (ltLen>5)
	{
		unsigned short nox_tex_filename_tag = NOX_BIN_MLAY_TEX_FILENAME;
		if (1!=FWRITE_USHORT(nox_tex_filename_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing texture filename tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(ltLen))
		{
			if (showMessages)
				MessageBox(0, "Writing texture filename size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(texffname, (ltLen-6)))
		{
			if (showMessages)
				MessageBox(0, "Writing texture filename error.", "Error", 0);
			return false;
		}
	}

	unsigned short nox_tex_enabled_tag = NOX_BIN_MLAY_TEX_ENABLED;
	if (1!=FWRITE_USHORT(nox_tex_enabled_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short texenabled = use_tex ? 1 : 0;
	if (1!=FWRITE_USHORT(texenabled))
	{
		if (showMessages)
			MessageBox(0, "Writing texture enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_tex_post_tag = NOX_BIN_MLAY_TEX_POST;
	if (1!=FWRITE_USHORT(nox_tex_post_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post tag error.", "Error", 0);
		return false;
	}
	unsigned int texpostsize = 62;
	if (1!=FWRITE_UINT(texpostsize))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post size error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_bri_tag = NOX_BIN_MLAY_TEX_POST_BRIGHTNESS;
	if (1!=FWRITE_USHORT(nox_post_bri_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post brightness tag error.", "Error", 0);
		return false;
	}
	float post_bri = texptr->texMod.brightness;
	if (1!=FWRITE_FLOAT(post_bri))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post brightness error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_con_tag = NOX_BIN_MLAY_TEX_POST_CONTRAST;
	if (1!=FWRITE_USHORT(nox_post_con_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post contrast tag error.", "Error", 0);
		return false;
	}
	float post_con = texptr->texMod.contrast;
	if (1!=FWRITE_FLOAT(post_con))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post contrast error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_sat_tag = NOX_BIN_MLAY_TEX_POST_SATURATION;
	if (1!=FWRITE_USHORT(nox_post_sat_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post saturation tag error.", "Error", 0);
		return false;
	}
	float post_sat = texptr->texMod.saturation;
	if (1!=FWRITE_FLOAT(post_sat))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post saturation error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_hue_tag = NOX_BIN_MLAY_TEX_POST_HUE;
	if (1!=FWRITE_USHORT(nox_post_hue_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post hue tag error.", "Error", 0);
		return false;
	}
	float post_hue = texptr->texMod.hue;
	if (1!=FWRITE_FLOAT(post_hue))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post hue error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_red_tag = NOX_BIN_MLAY_TEX_POST_RED;
	if (1!=FWRITE_USHORT(nox_post_red_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post red tag error.", "Error", 0);
		return false;
	}
	float post_red = texptr->texMod.red;
	if (1!=FWRITE_FLOAT(post_red))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post red error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_gre_tag = NOX_BIN_MLAY_TEX_POST_GREEN;
	if (1!=FWRITE_USHORT(nox_post_gre_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post green tag error.", "Error", 0);
		return false;
	}
	float post_gre = texptr->texMod.green;
	if (1!=FWRITE_FLOAT(post_gre))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post green error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_blu_tag = NOX_BIN_MLAY_TEX_POST_BLUE;
	if (1!=FWRITE_USHORT(nox_post_blu_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post blue tag error.", "Error", 0);
		return false;
	}
	float post_blu = texptr->texMod.blue;
	if (1!=FWRITE_FLOAT(post_blu))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post blue error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_gam_tag = NOX_BIN_MLAY_TEX_POST_GAMMA;
	if (1!=FWRITE_USHORT(nox_post_gam_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post gamma tag error.", "Error", 0);
		return false;
	}
	float post_gam = texptr->texMod.gamma;
	if (1!=FWRITE_FLOAT(post_gam))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post gamma error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_inv_tag = NOX_BIN_MLAY_TEX_POST_INVERT;
	if (1!=FWRITE_USHORT(nox_post_inv_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post invert tag error.", "Error", 0);
		return false;
	}
	unsigned short post_inv = texptr->texMod.invert ? 1 : 0;
	if (1!=FWRITE_USHORT(post_inv))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post invert error.", "Error", 0);
		return false;
	}

	unsigned short nox_post_interpolate_tag = NOX_BIN_MLAY_TEX_POST_INTERPOLATE;
	if (1!=FWRITE_USHORT(nox_post_interpolate_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post interpolate tag error.", "Error", 0);
		return false;
	}
	unsigned short post_interp = texptr->texMod.interpolateProbe ? 1 : 0;
	if (1!=FWRITE_USHORT(post_interp))
	{
		if (showMessages)
			MessageBox(0, "Writing texture post interpolate error.", "Error", 0);
		return false;
	}

	unsigned short nox_norm_post_tag = NOX_BIN_MLAY_NORM_POST;
	if (1!=FWRITE_USHORT(nox_norm_post_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post tag error.", "Error", 0);
		return false;
	}
	unsigned int normpostsize = 20;
	if (1!=FWRITE_UINT(normpostsize))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post size error.", "Error", 0);
		return false;
	}

	unsigned short nox_norm_post_inv_x_tag = NOX_BIN_MLAY_NORM_POST_INVERT_X;
	if (1!=FWRITE_USHORT(nox_norm_post_inv_x_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post invert x tag error.", "Error", 0);
		return false;
	}
	unsigned short post_inv_x = texptr->normMod.invertX ? 1 : 0;
	if (1!=FWRITE_USHORT(post_inv_x))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post x invert error.", "Error", 0);
		return false;
	}

	unsigned short nox_norm_post_inv_y_tag = NOX_BIN_MLAY_NORM_POST_INVERT_Y;
	if (1!=FWRITE_USHORT(nox_norm_post_inv_y_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post invert y tag error.", "Error", 0);
		return false;
	}
	unsigned short post_inv_y = texptr->normMod.invertY ? 1 : 0;
	if (1!=FWRITE_USHORT(post_inv_y))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post y invert error.", "Error", 0);
		return false;
	}

	unsigned short nox_norm_post_power_tag = NOX_BIN_MLAY_NORM_POST_POWER_EV;
	if (1!=FWRITE_USHORT(nox_norm_post_power_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post power tag error.", "Error", 0);
		return false;
	}
	float post_power = texptr->normMod.powerEV;
	if (1!=FWRITE_FLOAT(post_power))
	{
		if (showMessages)
			MessageBox(0, "Writing normal post power error.", "Error", 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != textureSize)
	{
		char * matname = mat->name;
		if (!matname)
			matname = "Unnamed";
		char errmsg[1024];
		sprintf_s(errmsg, 1024, "Export error. Texture chunk in slot %d in layer %d chunk in material (%s) size is incorrect.\nShould be %llu instead of %llu bytes.", slot, (lNum+1), matname, textureSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertMatPreviewPart(unsigned int mNum)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	if (mNum >= (unsigned int)rtr->curScenePtr->mats.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save material preview from material that doesn't exist.", "Error", 0);
		return false;
	}

	MaterialNox * mat = rtr->curScenePtr->mats[mNum];
	if (!mat)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save material preview from NULL material.", "Error", 0);
		return false;
	}

	bool prevExists = (mat->preview  &&  mat->preview->imgBuf);
	if (!prevExists)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save material preview but material has no preview.", "Error", 0);
		return false;
	}

	unsigned int bufsize = mat->preview->width * mat->preview->height * sizeof(COLORREF);
	unsigned int pwidth  = mat->preview->width;
	unsigned int pheight = mat->preview->height;

	COLORREF * prevBuf = (COLORREF *)malloc(bufsize);
	if (!prevBuf)
	{
		char errmsg[512];
		sprintf_s(errmsg, 512, "Error. Tried to save material preview but can't allocate %d bytes buffer for preview.", bufsize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	int i = 0;
	for (unsigned int y=0; y<pheight; y++)
	{
		for (unsigned int x=0; x<pwidth; x++)
		{
			prevBuf[i] = mat->preview->imgBuf[y][x];
			i++;
		}
	}

	unsigned long long matPrevSize = bufsize + 18;
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_matprev_tag = NOX_BIN_MAT_PREVIEW;
	if (1!=FWRITE_USHORT(nox_matprev_tag))
	{
		free(prevBuf);
		if (showMessages)
			MessageBox(0, "Writing material preview tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(matPrevSize))
	{
		free(prevBuf);
		if (showMessages)
			MessageBox(0, "Writing material preview size error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(pwidth))
	{
		free(prevBuf);
		if (showMessages)
			MessageBox(0, "Writing material preview width error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_UINT(pheight))
	{
		free(prevBuf);
		if (showMessages)
			MessageBox(0, "Writing material preview height error.", "Error", 0);
		return false;
	}


	if (1!=fwrite(prevBuf, bufsize, 1, file))
	{
		free(prevBuf);
		if (showMessages)
			MessageBox(0, "Writing material preview buffer error.", "Error", 0);
		return false;
	}

	free(prevBuf);

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matPrevSize)
	{
		char errmsg[512];
		sprintf_s(errmsg, 512, "Export error. Material preview chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n", matPrevSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertCameraPart(unsigned int cNum)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	if (cNum >= (unsigned int)rtr->curScenePtr->cameras.objCount)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save camera that doesn't exist.", "Error", 0);
		return false;
	}

	Camera * cam = rtr->curScenePtr->cameras[cNum];
	if (!cam)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save NULL camera.", "Error", 0);
		return false;
	}

	unsigned short aa = cam->aa;
	float posx = cam->position.x;
	float posy = cam->position.y;
	float posz = cam->position.z;
	float dirx = cam->direction.x;
	float diry = cam->direction.y;
	float dirz = cam->direction.z;
	float upx = cam->upDir.x;
	float upy = cam->upDir.y;
	float upz = cam->upDir.z;
	float crx = cam->rightDir.x;
	float cry = cam->rightDir.y;
	float crz = cam->rightDir.z;
	float angle = cam->angle;
	float iso = cam->ISO;
	float aperture = cam->aperture;
	float shutter = cam->shutter;
	bool autoexposure = cam->autoexposure;
	bool autofocus = cam->autoFocus;
	unsigned short autoexp_type = cam->autoexposureType;
	unsigned short bladesnumber = cam->apBladesNum;
	float bladesangle = cam->apBladesAngle;
	bool bladesround = cam->apBladesRound;
	float bladesradius = cam->apBladesRadius;
	float focusdist = cam->focusDist;
	bool dofON = cam->dofOnTemp;
	unsigned short ringSize = cam->diaphSampler.intBokehRingSize;
	short ringBalance = cam->diaphSampler.intBokehRingBalance;
	short bokehVignette = cam->diaphSampler.intBokehVignette;
	unsigned short bokehFlatten = cam->diaphSampler.intBokehFlatten;
	unsigned int width = cam->width;
	unsigned int height = cam->height;
	bool isActive = (rtr->curScenePtr->sscene.activeCamera == cNum);
	char * name = cam->name;

	unsigned short mb_enabled = cam->mb_on ? 1 : 0;
	Vector3d mb_pos = cam->mb_velocity;
	Vector3d mb_rot = cam->mb_rot_vel;
	float mb_rot_angle = cam->mb_rot_angle;

	unsigned int lname = 0;
	if (name)
		lname = (unsigned int)strlen(name)+6;
	else
	{
		name = "Unnamed";
		lname = 7+6;	// Unnamed
	}

	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_camera_tag = NOX_BIN_CAMERA;
	if (1!=FWRITE_USHORT(nox_camera_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera tag error.", "Error", 0);
		return false;
	}

	unsigned long long camerasize = evalSizeCamera(cNum);
	if (1!=FWRITE_ULONG8(camerasize))
	{
		if (showMessages)
			MessageBox(0, "Writing camera size error.", "Error", 0);
		return false;
	}

	unsigned short activeCamera = isActive ? 1 : 0;
	unsigned short nox_cam_active_tag = NOX_BIN_CAM_ACTIVE;
	if (1!=FWRITE_USHORT(nox_cam_active_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera active tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(activeCamera))
	{
		if (showMessages)
			MessageBox(0, "Writing camera active error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_width_tag = NOX_BIN_CAM_WIDTH;
	if (1!=FWRITE_USHORT(nox_cam_width_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera width tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(width))
	{
		if (showMessages)
			MessageBox(0, "Writing camera width error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_height_tag = NOX_BIN_CAM_HEIGHT;
	if (1!=FWRITE_USHORT(nox_cam_height_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera height tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(height))
	{
		if (showMessages)
			MessageBox(0, "Writing camera height error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_name_tag = NOX_BIN_CAM_NAME;
	if (1!=FWRITE_USHORT(nox_cam_name_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera name tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(lname))
	{
		if (showMessages)
			MessageBox(0, "Writing camera name size error.", "Error", 0);
		return false;
	}

	if (1!=FWRITE_STRING(name, lname-6))
	{
		if (showMessages)
			MessageBox(0, "Writing camera name error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_pos_tag = NOX_BIN_CAM_POS;
	if (1!=FWRITE_USHORT(nox_cam_pos_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera position tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(posx))
	{
		if (showMessages)
			MessageBox(0, "Writing camera position error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(posy))
	{
		if (showMessages)
			MessageBox(0, "Writing camera position error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(posz))
	{
		if (showMessages)
			MessageBox(0, "Writing camera position error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_dir_tag = NOX_BIN_CAM_DIR;
	if (1!=FWRITE_USHORT(nox_cam_dir_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera direction tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirx))
	{
		if (showMessages)
			MessageBox(0, "Writing camera direction error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(diry))
	{
		if (showMessages)
			MessageBox(0, "Writing camera direction error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirz))
	{
		if (showMessages)
			MessageBox(0, "Writing camera direction error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_updir_tag = NOX_BIN_CAM_UPDIR;
	if (1!=FWRITE_USHORT(nox_cam_updir_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera up direction tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(upx))
	{
		if (showMessages)
			MessageBox(0, "Writing camera up direction error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(upy))
	{
		if (showMessages)
			MessageBox(0, "Writing camera up direction error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(upz))
	{
		if (showMessages)
			MessageBox(0, "Writing camera up direction error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_rdir_tag = NOX_BIN_CAM_RIGHTDIR;
	if (1!=FWRITE_USHORT(nox_cam_rdir_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera right direction tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(crx))
	{
		if (showMessages)
			MessageBox(0, "Writing camera right direction error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(cry))
	{
		if (showMessages)
			MessageBox(0, "Writing camera right direction error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(crz))
	{
		if (showMessages)
			MessageBox(0, "Writing camera right direction error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_angle_tag = NOX_BIN_CAM_ANGLE;
	if (1!=FWRITE_USHORT(nox_cam_angle_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera angle tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(angle))
	{
		if (showMessages)
			MessageBox(0, "Writing camera angle error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_iso_tag = NOX_BIN_CAM_ISO;
	if (1!=FWRITE_USHORT(nox_cam_iso_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera ISO tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(iso))
	{
		if (showMessages)
			MessageBox(0, "Writing camera ISO error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_aperture_tag = NOX_BIN_CAM_APERTURE;
	if (1!=FWRITE_USHORT(nox_cam_aperture_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(aperture))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_shutter_tag = NOX_BIN_CAM_SHUTTER;
	if (1!=FWRITE_USHORT(nox_cam_shutter_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera shutter tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(shutter))
	{
		if (showMessages)
			MessageBox(0, "Writing camera shutter error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_autoexp_on_tag = NOX_BIN_CAM_AUTOEXPOSURE_ON;
	if (1!=FWRITE_USHORT(nox_cam_autoexp_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera autoexposure on tag error.", "Error", 0);
		return false;
	}
	unsigned short aexpON = autoexposure ? 1 : 0;
	if (1!=FWRITE_USHORT(aexpON))
	{
		if (showMessages)
			MessageBox(0, "Writing camera autoexposure on error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_autoexp_type_tag = NOX_BIN_CAM_AUTOEXPOSURE_TYPE;
	if (1!=FWRITE_USHORT(nox_cam_autoexp_type_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera autoexposure type tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(autoexp_type))
	{
		if (showMessages)
			MessageBox(0, "Writing camera autoexposure type error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_autofocus_on_tag = NOX_BIN_CAM_AUTOFOCUS;
	if (1!=FWRITE_USHORT(nox_cam_autofocus_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera autofocus on tag error.", "Error", 0);
		return false;
	}
	unsigned short afocusON = autofocus ? 1 : 0;
	if (1!=FWRITE_USHORT(afocusON))
	{
		if (showMessages)
			MessageBox(0, "Writing camera autofocus on error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_focusdist_tag = NOX_BIN_CAM_FOCUSDIST;
	if (1!=FWRITE_USHORT(nox_cam_focusdist_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera focus distance tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(focusdist))
	{
		if (showMessages)
			MessageBox(0, "Writing camera focus distance error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_bladesnumber_on_tag = NOX_BIN_CAM_BLADESNUMBER;
	if (1!=FWRITE_USHORT(nox_cam_bladesnumber_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades number tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bladesnumber))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades number error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_bladesangle_tag = NOX_BIN_CAM_BLADESANGLE;
	if (1!=FWRITE_USHORT(nox_cam_bladesangle_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades angle tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(bladesangle))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades angle error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_bladesround_on_tag = NOX_BIN_CAM_BLADESROUND;
	if (1!=FWRITE_USHORT(nox_cam_bladesround_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades round tag error.", "Error", 0);
		return false;
	}
	unsigned short blRound = bladesround ? 1 : 0;
	if (1!=FWRITE_USHORT(blRound))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades round error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_bladesradius_tag = NOX_BIN_CAM_BLADESRADIUS;
	if (1!=FWRITE_USHORT(nox_cam_bladesradius_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades radius tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(bladesradius))
	{
		if (showMessages)
			MessageBox(0, "Writing camera aperture blades radius error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_dof_on_tag = NOX_BIN_CAM_DOF_ON;
	if (1!=FWRITE_USHORT(nox_cam_dof_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera dof on tag error.", "Error", 0);
		return false;
	}
	unsigned short dofIsON = dofON ? 1 : 0;
	if (1!=FWRITE_USHORT(dofIsON))
	{
		if (showMessages)
			MessageBox(0, "Writing camera dof on error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_ring_size_tag = NOX_BIN_CAM_BOKEH_RING_SIZE;
	if (1!=FWRITE_USHORT(nox_cam_ring_size_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh ring size tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(ringSize))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh ring size error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_ring_balance_tag = NOX_BIN_CAM_BOKEH_RING_BALANCE;
	if (1!=FWRITE_USHORT(nox_cam_ring_balance_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh ring balance tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(ringBalance))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh ring balance error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_bokeh_flatten_tag = NOX_BIN_CAM_BOKEH_FLATTEN;
	if (1!=FWRITE_USHORT(nox_cam_bokeh_flatten_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh flatten tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bokehFlatten))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh flatten error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_bokeh_vignette_tag = NOX_BIN_CAM_BOKEH_VIGNETTE;
	if (1!=FWRITE_USHORT(nox_cam_bokeh_vignette_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh vignette tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bokehVignette))
	{
		if (showMessages)
			MessageBox(0, "Writing camera bokeh vignette error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_aa_tag = NOX_BIN_CAM_AA;
	if (1!=FWRITE_USHORT(nox_cam_aa_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera antialiasing tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(aa))
	{
		if (showMessages)
			MessageBox(0, "Writing camera antialiasing error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_mb_on_tag = NOX_BIN_CAM_MB_ON;
	if (1!=FWRITE_USHORT(nox_cam_mb_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur enabled tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(mb_enabled))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_mb_pos_tag = NOX_BIN_CAM_MB_POS;
	if (1!=FWRITE_USHORT(nox_cam_mb_pos_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur pos tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_pos.x))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur pos error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_pos.y))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur pos error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_pos.z))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur pos error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_mb_angle_tag = NOX_BIN_CAM_MB_ANGLE;
	if (1!=FWRITE_USHORT(nox_cam_mb_angle_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur rotation tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_rot_angle))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur rotation angle error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_rot.x))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur rotation axis error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_rot.y))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur rotation axis error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_rot.z))
	{
		if (showMessages)
			MessageBox(0, "Writing camera motion blur rotation axis error.", "Error", 0);
		return false;
	}

	if (!insertCameraPostPart(&cam->iMod))
	{
		return false;
	}

	if (!insertCameraFinalPostPart(&cam->fMod, cam->texDiaphragm.fullfilename, cam->texObstacle.fullfilename))
	{
		return false;
	}

	if (addCamBuffs)
	{
		if (!insertCameraBuffersPart(cNum))
		{
			return false;
		}

		if (!insertCameraDepthBufPart(cNum))
		{
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != camerasize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Camera chunk size is incorrect.\nShould be %llu instead of %llu bytes.", camerasize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertCameraBuffersPart(unsigned int cNum)
{
	Raytracer * rtr = Raytracer::getInstance();

	unsigned int numCams = rtr->curScenePtr->cameras.objCount;
	if (cNum >= numCams)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to write buffer from camera that doesn't exist.", "Error", 0);
		return false;
	}

	Camera * cam = rtr->curScenePtr->cameras[cNum];
	if (!cam)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to write buffer from NULL camera.", "Error", 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_camera_buffers_tag = NOX_BIN_CAM_BUFFERS;
	if (1!=FWRITE_USHORT(nox_camera_buffers_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers tag error.", "Error", 0);
		return false;
	}
	unsigned long long camerabufssize = evalSizeCameraBuffers(cNum);
	if (1!=FWRITE_ULONG8(camerabufssize))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers size error.", "Error", 0);
		return false;
	}

	unsigned int bw = cam->width*cam->aa;
	unsigned int bh = cam->height*cam->aa;
	unsigned long long gi_hits = 0;
	gi_hits = cam->staticRegion.overallHits;

	if (1!=FWRITE_UINT(bw))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers width error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_UINT(bh))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers width error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(gi_hits))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers size error.", "Error", 0);
		return false;
	}
	unsigned int rend_time = (rtr->curScenePtr->nowRendering) ? (cam->getRendTime(GetTickCount())) : (cam->rendTime);
	if (1!=FWRITE_UINT(rend_time))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers rendering time error.", "Error", 0);
		return false;
	}
	unsigned int halton = cam->maxHalton;
	if (1!=FWRITE_UINT(halton))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers Halton seed error.", "Error", 0);
		return false;
	}

	unsigned short nox_cam_gi_type_tag = NOX_BIN_CAM_BUFFERS_GI_TYPE;
	if (1!=FWRITE_USHORT(nox_cam_gi_type_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers gi type error.", "Error", 0);
		return false;
	}
	unsigned int gitype = cam->gi_type;
	if (1!=FWRITE_UINT(gitype))
	{
		if (showMessages)
			MessageBox(0, "Writing camera buffers gi type error.", "Error", 0);
		return false;
	}

	for (unsigned int j=0; j<32; j++)
	{
		int i = j%16;
		if (!cam->blendBits & (1<<i))
			continue;
		if (!cam->blendBuffDirect[i].imgBuf)
			continue;	
		if (!cam->blendBuffDirect[i].hitsBuf)
			continue;
		if (!cam->blendBuffGI[i].imgBuf)
			continue;
		if (!cam->blendBuffGI[i].hitsBuf)
			continue;
		if (cam->blendBuffGI[i].width != cam->width*cam->aa   ||   cam->blendBuffGI[i].height != cam->height*cam->aa)
			continue;
		if (cam->blendBuffDirect[i].width != cam->width*cam->aa   ||   cam->blendBuffDirect[i].height != cam->height*cam->aa)
			continue;

		unsigned short nox_camera_buffer_tag = NOX_BIN_CAM_BUFFER;
		if (1!=FWRITE_USHORT(nox_camera_buffer_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing camera buffer tag error.", "Error", 0);
			return false;
		}
		unsigned long long camerabufsize = cam->width*cam->height*cam->aa*cam->aa * (sizeof(Color4)+sizeof(int));
		if (1!=FWRITE_ULONG8(camerabufsize))
		{
			if (showMessages)
				MessageBox(0, "Writing camera buffer size error.", "Error", 0);
			return false;
		}
		unsigned short nox_camera_buffer_type = j<16 ? 0 : 1;
		if (1!=FWRITE_USHORT(nox_camera_buffer_type))
		{
			if (showMessages)
				MessageBox(0, "Writing camera buffer type error.", "Error", 0);
			return false;
		}
		unsigned short nox_camera_buffer_layer = i;
		if (1!=FWRITE_USHORT(nox_camera_buffer_layer))
		{
			if (showMessages)
				MessageBox(0, "Writing camera buffer layer error.", "Error", 0);
			return false;
		}

		ImageBuffer * imgbuf = (j<16) ? &(cam->blendBuffDirect[i]) : &(cam->blendBuffGI[i]);
		unsigned int bwidth  = cam->width* cam->aa;
		unsigned int bheight = cam->height*cam->aa;

		for (unsigned int y=0; y<bheight; y++)
		{
			if (1!=fwrite(imgbuf->imgBuf[y], bwidth*sizeof(Color4), 1, file))
			{
				if (showMessages)
					MessageBox(0, "Writing camera buffer error.", "Error", 0);
				return false;
			}
		}
		for (unsigned int y=0; y<bheight; y++)
		{
			if (1!=fwrite(imgbuf->hitsBuf[y], bwidth*sizeof(int), 1, file))
			{
				if (showMessages)
					MessageBox(0, "Writing camera hits buffer error.", "Error", 0);
				return false;
			}
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != camerabufssize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Camera buffer chunk size is incorrect.\nShould be %llu instead of %llu bytes.", camerabufssize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertCameraDepthBufPart(unsigned int cNum)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned int numCams = rtr->curScenePtr->cameras.objCount;
	if (cNum >= numCams)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to write depth buffer from camera that doesn't exist.", "Error", 0);
		return false;
	}

	Camera * cam = rtr->curScenePtr->cameras[cNum];
	if (!cam)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to write depth buffer from NULL camera.", "Error", 0);
		return false;
	}

	FloatBuffer * dbuf = cam->depthBuf;
	if (!dbuf  ||  !dbuf->fbuf  ||  !dbuf->hbuf  ||  dbuf->width<1  ||  dbuf->height<1)
		return true;


	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_camera_depthbuf_tag = NOX_BIN_CAM_DEPTH_BUF;
	if (1!=FWRITE_USHORT(nox_camera_depthbuf_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera depth buffer tag error.", "Error", 0);
		return false;
	}
	unsigned long long cameradbufsize = evalSizeCameraDepthBuf(cNum);
	if (1!=FWRITE_ULONG8(cameradbufsize))
	{
		if (showMessages)
			MessageBox(0, "Writing camera depth buffer size error.", "Error", 0);
		return false;
	}

	unsigned int width = dbuf->width;
	if (1!=FWRITE_UINT(width))
	{
		if (showMessages)
			MessageBox(0, "Writing camera depth buffer width error.", "Error", 0);
		return false;
	}
	unsigned int height = dbuf->height;
	if (1!=FWRITE_UINT(height))
	{
		if (showMessages)
			MessageBox(0, "Writing camera depth buffer height error.", "Error", 0);
		return false;
	}

	for (unsigned int y=0; y<height; y++)
	{
		if (1!=fwrite(dbuf->fbuf[y], width*sizeof(float), 1, file))
		{
			if (showMessages)
				MessageBox(0, "Writing camera depth buffer error.", "Error", 0);
			return false;
		}
	}
	for (unsigned int y=0; y<height; y++)
	{
		if (1!=fwrite(dbuf->hbuf[y], width*sizeof(unsigned int), 1, file))
		{
			if (showMessages)
				MessageBox(0, "Writing camera depth buffer error.", "Error", 0);
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != cameradbufsize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Camera depth buffer chunk size is incorrect.\nShould be %llu instead of %llu bytes.", cameradbufsize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertCameraPostPart(ImageModifier * iMod)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}
	if (!iMod)
	{
		if (showMessages)
			MessageBox(0, "Error. Image modifier is NULL.", "Error", 0);
		return false;
	}
	ImageModifier * im = iMod;

	unsigned int rCnt = (unsigned int)im->getCurveRedPointsCount();
	unsigned int gCnt = (unsigned int)im->getCurveGreenPointsCount();
	unsigned int bCnt = (unsigned int)im->getCurveBluePointsCount();
	unsigned int lCnt = (unsigned int)im->getCurveLuminancePointsCount();

	unsigned long long postsize = evalSizePost(iMod);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_camera_post_tag = NOX_BIN_CAM_POST;
	if (1!=FWRITE_USHORT(nox_camera_post_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(postsize))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post size error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_iso_tag = NOX_BIN_CAM_POST_ISO;
	if (1!=FWRITE_USHORT(nox_camera_post_iso_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post iso compensation tag error.", "Error", 0);
		return false;
	}
	float iso = im->getMultiplier();
	if (1!=FWRITE_FLOAT(iso))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post iso compensation error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_ev_tag = NOX_BIN_CAM_POST_EV;
	if (1!=FWRITE_USHORT(nox_camera_post_ev_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post iso ev compensation tag error.", "Error", 0);
		return false;
	}
	float ev = im->getISO_EV_compensation();
	if (1!=FWRITE_FLOAT(ev))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post iso ev compensation error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_gi_ev_tag = NOX_BIN_CAM_POST_GI_EV;
	if (1!=FWRITE_USHORT(nox_camera_post_gi_ev_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post gi ev compensation tag error.", "Error", 0);
		return false;
	}
	float giev = im->getGICompensation();
	if (1!=FWRITE_FLOAT(giev))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post gi ev compensation error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_tone_tag = NOX_BIN_CAM_POST_TONE;
	if (1!=FWRITE_USHORT(nox_camera_post_tone_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post tone tag error.", "Error", 0);
		return false;
	}
	float tone = im->getToneMappingValue();
	if (1!=FWRITE_FLOAT(tone))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post tone error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_gamma_tag = NOX_BIN_CAM_POST_GAMMA;
	if (1!=FWRITE_USHORT(nox_camera_post_gamma_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post gamma tag error.", "Error", 0);
		return false;
	}
	float gamma = im->getGamma();
	if (1!=FWRITE_FLOAT(gamma))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post gamma error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_bri_tag = NOX_BIN_CAM_POST_BRIGHTNESS;
	if (1!=FWRITE_USHORT(nox_camera_post_bri_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post brightness tag error.", "Error", 0);
		return false;
	}
	float brightness = im->getBrightness();
	if (1!=FWRITE_FLOAT(brightness))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post brightness error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_contr_tag = NOX_BIN_CAM_POST_CONTRAST;
	if (1!=FWRITE_USHORT(nox_camera_post_contr_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post contrast tag error.", "Error", 0);
		return false;
	}
	float contrast = im->getContrast();
	if (1!=FWRITE_FLOAT(contrast))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post contrast error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_satur_tag = NOX_BIN_CAM_POST_SATURATION;
	if (1!=FWRITE_USHORT(nox_camera_post_satur_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post saturation tag error.", "Error", 0);
		return false;
	}
	float saturation = im->getSaturation();
	if (1!=FWRITE_FLOAT(saturation))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post saturation error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_addR_tag = NOX_BIN_CAM_POST_ADD_RED;
	if (1!=FWRITE_USHORT(nox_camera_post_addR_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post add red tag error.", "Error", 0);
		return false;
	}
	float addred = im->getR();
	if (1!=FWRITE_FLOAT(addred))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post add red error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_addG_tag = NOX_BIN_CAM_POST_ADD_GREEN;
	if (1!=FWRITE_USHORT(nox_camera_post_addG_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post add green tag error.", "Error", 0);
		return false;
	}
	float addgreen = im->getG();
	if (1!=FWRITE_FLOAT(addgreen))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post add green error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_addB_tag = NOX_BIN_CAM_POST_ADD_BLUE;
	if (1!=FWRITE_USHORT(nox_camera_post_addB_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post add blue tag error.", "Error", 0);
		return false;
	}
	float addblue = im->getB();
	if (1!=FWRITE_FLOAT(addblue))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post add blue error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_temperature_tag = NOX_BIN_CAM_POST_TEMPERATURE;
	if (1!=FWRITE_USHORT(nox_camera_post_temperature_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post temperature tag error.", "Error", 0);
		return false;
	}
	float temperature = im->getTemperature();
	if (1!=FWRITE_FLOAT(temperature))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post temperature error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_vignette_tag = NOX_BIN_CAM_POST_VIGNETTE;
	if (1!=FWRITE_USHORT(nox_camera_post_vignette_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post vignette tag error.", "Error", 0);
		return false;
	}
	unsigned short vignette = im->getVignette();
	if (1!=FWRITE_USHORT(vignette))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post vignette error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_response_function_tag = NOX_BIN_CAM_POST_RESPONSE_FUNCTION;
	if (1!=FWRITE_USHORT(nox_camera_post_response_function_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post response function tag error.", "Error", 0);
		return false;
	}
	unsigned short response = im->getResponseFunctionNumber();
	if (1!=FWRITE_USHORT(response))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post response function error.", "Error", 0);
		return false;
	}
	
	unsigned short nox_camera_post_filter_tag = NOX_BIN_CAM_POST_FILTER;
	if (1!=FWRITE_USHORT(nox_camera_post_filter_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post aa filter tag error.", "Error", 0);
		return false;
	}
	unsigned short filter = im->getFilter();
	if (1!=FWRITE_USHORT(filter))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post aa filter error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_dots_tag = NOX_BIN_CAM_POST_DOTS_ENABLED;
	if (1!=FWRITE_USHORT(nox_camera_post_dots_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover tag error.", "Error", 0);
		return false;
	}
	unsigned short dots = im->getDotsEnabled() ? 1 : 0;
	if (1!=FWRITE_USHORT(dots))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_dots_radius_tag = NOX_BIN_CAM_POST_DOTS_RADIUS;
	if (1!=FWRITE_USHORT(nox_camera_post_dots_radius_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover radius tag error.", "Error", 0);
		return false;
	}
	unsigned int dotsRad = im->getDotsRadius();
	if (1!=FWRITE_UINT(dotsRad))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover radius error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_dots_threshold_tag = NOX_BIN_CAM_POST_DOTS_THRESHOLD;
	if (1!=FWRITE_USHORT(nox_camera_post_dots_threshold_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover threshold tag error.", "Error", 0);
		return false;
	}
	float dotsThr = im->getDotsThreshold();
	if (1!=FWRITE_FLOAT(dotsThr))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover threshold error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_dots_density_tag = NOX_BIN_CAM_POST_DOTS_DENSITY;
	if (1!=FWRITE_USHORT(nox_camera_post_dots_density_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover density tag error.", "Error", 0);
		return false;
	}
	unsigned int dotsDens = im->getDotsDensity();
	if (1!=FWRITE_UINT(dotsDens))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post hot pixel remover density error.", "Error", 0);
		return false;
	}

	unsigned short nox_camera_post_curve_tag = NOX_BIN_CAM_POST_CURVE;
	if (1!=FWRITE_USHORT(nox_camera_post_curve_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post curve tag error.", "Error", 0);
		return false;
	}
	unsigned int numPoints = rCnt+gCnt+bCnt+lCnt;
	if (1!=FWRITE_UINT(numPoints))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post curve number points error.", "Error", 0);
		return false;
	}
	bool bR, bG, bB, bL;
	im->getCurveCheckboxes(bR, bG, bB, bL);
	unsigned short redON = bR ? 1 : 0;
	unsigned short greenON = bG ? 1 : 0;
	unsigned short blueON = bB ? 1 : 0;
	unsigned short lightON = bL ? 1 : 0;
	if (1!=FWRITE_USHORT(redON))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post curve red on chekbox error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(greenON))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post curve green on chekbox error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(blueON))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post curve blue on chekbox error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(lightON))
	{
		if (showMessages)
			MessageBox(0, "Writing camera post curve lightness on chekbox error.", "Error", 0);
		return false;
	}
	for (unsigned int i=0; i<rCnt; i++)
	{
		int x,y;
		im->getCurveRedPoint(i, x, y);
		unsigned short sx = x;
		unsigned short sy = y;
		unsigned short nox_camera_post_curve_red_point_tag = NOX_BIN_CAM_POST_CURVE_PR;
		if (1!=FWRITE_USHORT(nox_camera_post_curve_red_point_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve red point tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sx))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve red point x error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sy))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve red point y error.", "Error", 0);
			return false;
		}
	}
	for (unsigned int i=0; i<gCnt; i++)
	{
		int x,y;
		im->getCurveGreenPoint(i, x, y);
		unsigned short sx = x;
		unsigned short sy = y;
		unsigned short nox_camera_post_curve_green_point_tag = NOX_BIN_CAM_POST_CURVE_PG;
		if (1!=FWRITE_USHORT(nox_camera_post_curve_green_point_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve green point tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sx))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve green point x error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sy))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve green point y error.", "Error", 0);
			return false;
		}
	}
	for (unsigned int i=0; i<bCnt; i++)
	{
		int x,y;
		im->getCurveBluePoint(i, x, y);
		unsigned short sx = x;
		unsigned short sy = y;
		unsigned short nox_camera_post_curve_blue_point_tag = NOX_BIN_CAM_POST_CURVE_PB;
		if (1!=FWRITE_USHORT(nox_camera_post_curve_blue_point_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve blue point tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sx))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve blue point x error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sy))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve blue point y error.", "Error", 0);
			return false;
		}
	}
	for (unsigned int i=0; i<lCnt; i++)
	{
		int x,y;
		im->getCurveLuminancePoint(i, x, y);
		unsigned short sx = x;
		unsigned short sy = y;
		unsigned short nox_camera_post_curve_lightness_point_tag = NOX_BIN_CAM_POST_CURVE_PL;
		if (1!=FWRITE_USHORT(nox_camera_post_curve_lightness_point_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve lightness point tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sx))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve lightness point x error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_USHORT(sy))
		{
			if (showMessages)
				MessageBox(0, "Writing camera post curve lightness point y error.", "Error", 0);
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != postsize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Camera post chunk size is incorrect.\nShould be %llu instead of %llu bytes.", postsize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertCameraFinalPostPart(FinalModifier * fMod, char * texDiaph, char * texObst)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	if (!fMod)
	{
		if (showMessages)
			MessageBox(0, "Inner error. Final Modifier is NULL.", "Error", 0);
		return false;
	}
	FinalModifier * fm = fMod;

	unsigned long long postsize = evalSizeCameraFinalPost(fMod, texDiaph, texObst);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_final_post_tag = NOX_BIN_FINAL_POST;
	if (1!=FWRITE_USHORT(nox_final_post_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final post tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(postsize))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final post size error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_quality_tag = NOX_BIN_FINAL_QUALITY;
	if (1!=FWRITE_USHORT(nox_final_quality_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final quality tag error.", "Error", 0);
		return false;
	}
	unsigned int quality = fm->quality;
	if (1!=FWRITE_UINT(quality))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final quality error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_threads_tag = NOX_BIN_FINAL_THREADS;
	if (1!=FWRITE_USHORT(nox_final_threads_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final threads tag error.", "Error", 0);
		return false;
	}
	unsigned short nThreads = fm->numThreads;
	if (1!=FWRITE_USHORT(nThreads))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final threads error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_algorithm_tag = NOX_BIN_FINAL_ALGORITHM;
	if (1!=FWRITE_USHORT(nox_final_algorithm_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final algorithm tag error.", "Error", 0);
		return false;
	}
	unsigned short cAlgorithm = fm->algorithm;
	if (1!=FWRITE_USHORT(cAlgorithm))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final algorithm error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_fakedof_on_tag = NOX_BIN_FINAL_FAKE_DOF_ON;
	if (1!=FWRITE_USHORT(nox_final_fakedof_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fake dof on tag error.", "Error", 0);
		return false;
	}
	unsigned short fakeDofOn = fm->dofOn ? 1 : 0;
	if (1!=FWRITE_USHORT(fakeDofOn))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fake dof on error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_focus_dist_tag = NOX_BIN_FINAL_FOCUS_DISTANCE;
	if (1!=FWRITE_USHORT(nox_final_focus_dist_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final focus distance tag error.", "Error", 0);
		return false;
	}
	float focusDist = fm->focusDistance;
	if (1!=FWRITE_FLOAT(focusDist))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final focus distance error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_aperture_tag = NOX_BIN_FINAL_APERTURE;
	if (1!=FWRITE_USHORT(nox_final_aperture_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final aperture tag error.", "Error", 0);
		return false;
	}
	float aperture = fm->aperture;
	if (1!=FWRITE_FLOAT(aperture))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final aperture error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_num_blades_tag = NOX_BIN_FINAL_DIAPH_NUM_BLADES;
	if (1!=FWRITE_USHORT(nox_final_num_blades_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm blades number tag error.", "Error", 0);
		return false;
	}
	unsigned short numBlades = fm->bladesNum;
	if (1!=FWRITE_USHORT(numBlades))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm blades number error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_angle_tag = NOX_BIN_FINAL_DIAPH_ANGLE;
	if (1!=FWRITE_USHORT(nox_final_angle_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm angle tag error.", "Error", 0);
		return false;
	}
	float blAngle = fm->bladesAngle;
	if (1!=FWRITE_FLOAT(blAngle))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm angle error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_radius_tag = NOX_BIN_FINAL_DIAPH_RADIUS;
	if (1!=FWRITE_USHORT(nox_final_radius_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm blade radius tag error.", "Error", 0);
		return false;
	}
	float blRadius = fm->bladesRadius;
	if (1!=FWRITE_FLOAT(blRadius))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm blade radius error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_blades_round_tag = NOX_BIN_FINAL_DIAPH_BLADES_ROUND;
	if (1!=FWRITE_USHORT(nox_final_blades_round_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm blades round tag error.", "Error", 0);
		return false;
	}
	unsigned short bladesRound = fm->roundBlades ? 1 : 0;
	if (1!=FWRITE_USHORT(bladesRound))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final diaphragm blades round error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bokeh_balance_tag = NOX_BIN_FINAL_BOKEH_RING_BALANCE;
	if (1!=FWRITE_USHORT(nox_final_bokeh_balance_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh ring balance tag error.", "Error", 0);
		return false;
	}
	unsigned short bokehBalance = fm->bokehRingBalance;
	if (1!=FWRITE_USHORT(bokehBalance))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh ring balance error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bokeh_size_tag = NOX_BIN_FINAL_BOKEH_RING_SIZE;
	if (1!=FWRITE_USHORT(nox_final_bokeh_size_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh ring size tag error.", "Error", 0);
		return false;
	}
	unsigned short bokehRingSize = fm->bokehRingSize;
	if (1!=FWRITE_USHORT(bokehRingSize))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh ring size error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bokeh_flatten_tag = NOX_BIN_FINAL_BOKEH_FLATTEN;
	if (1!=FWRITE_USHORT(nox_final_bokeh_flatten_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh flatten tag error.", "Error", 0);
		return false;
	}
	unsigned short bokehFlatten = fm->bokehFlatten;
	if (1!=FWRITE_USHORT(bokehFlatten))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh flatten error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bokeh_vignette_tag = NOX_BIN_FINAL_BOKEH_VIGNETTE;
	if (1!=FWRITE_USHORT(nox_final_bokeh_vignette_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh vignette tag error.", "Error", 0);
		return false;
	}
	unsigned short bokehVignette= fm->bokehVignette;
	if (1!=FWRITE_USHORT(bokehVignette))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bokeh vignette error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_ab_shift_lens_tag = NOX_BIN_FINAL_ABERRATION_SHIFT_LENS;
	if (1!=FWRITE_USHORT(nox_final_ab_shift_lens_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration shift on lens tag error.", "Error", 0);
		return false;
	}
	float abshlens = fm->chromaticShiftLens;
	if (1!=FWRITE_FLOAT(abshlens))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration shift on lens error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_ab_shift_depth_tag = NOX_BIN_FINAL_ABERRATION_SHIFT_DEPTH;
	if (1!=FWRITE_USHORT(nox_final_ab_shift_depth_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration shift on depth tag error.", "Error", 0);
		return false;
	}
	float abshdepth = fm->chromaticShiftDepth;
	if (1!=FWRITE_FLOAT(abshdepth))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration shift on depth error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_ab_achromatic_tag = NOX_BIN_FINAL_ABERRATION_ACHROMATIC;
	if (1!=FWRITE_USHORT(nox_final_ab_achromatic_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration achromatic tag error.", "Error", 0);
		return false;
	}
	float abachromatic = fm->chromaticAchromatic;
	if (1!=FWRITE_FLOAT(abachromatic))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration achromatic error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_ab_red_tag = NOX_BIN_FINAL_ABERRATION_RED;
	if (1!=FWRITE_USHORT(nox_final_ab_red_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration red tag error.", "Error", 0);
		return false;
	}
	float abplRed= fm->chromaticPlanarRed;
	if (1!=FWRITE_FLOAT(abplRed))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration red error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_ab_green_tag = NOX_BIN_FINAL_ABERRATION_GREEN;
	if (1!=FWRITE_USHORT(nox_final_ab_green_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration green tag error.", "Error", 0);
		return false;
	}
	float abplGreen= fm->chromaticPlanarGreen;
	if (1!=FWRITE_FLOAT(abplGreen))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration green error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_ab_blue_tag = NOX_BIN_FINAL_ABERRATION_BLUE;
	if (1!=FWRITE_USHORT(nox_final_ab_blue_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration blue tag error.", "Error", 0);
		return false;
	}
	float abplBlue = fm->chromaticPlanarBlue;
	if (1!=FWRITE_FLOAT(abplBlue))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final chromatic aberration blue error.", "Error", 0);
		return false;
	}


	unsigned short nox_final_bloom_on_tag = NOX_BIN_FINAL_BLOOM_ON;
	if (1!=FWRITE_USHORT(nox_final_bloom_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom on tag error.", "Error", 0);
		return false;
	}
	unsigned short bloomon = fm->bloomEnabled ? 1 : 0;
	if (1!=FWRITE_USHORT(bloomon))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom on error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bloom_power_tag = NOX_BIN_FINAL_BLOOM_POWER;
	if (1!=FWRITE_USHORT(nox_final_bloom_power_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom power tag error.", "Error", 0);
		return false;
	}
	float bloompower = fm->bloomPower;
	if (1!=FWRITE_FLOAT(bloompower))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom power error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bloom_area_tag = NOX_BIN_FINAL_BLOOM_AREA;
	if (1!=FWRITE_USHORT(nox_final_bloom_area_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom area tag error.", "Error", 0);
		return false;
	}
	unsigned int bloomarea = fm->bloomArea;
	if (1!=FWRITE_UINT(bloomarea))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom area error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bloom_thres_tag = NOX_BIN_FINAL_BLOOM_THRESHOLD;
	if (1!=FWRITE_USHORT(nox_final_bloom_thres_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom threshold tag error.", "Error", 0);
		return false;
	}
	float bloomthres = fm->bloomThreshold;
	if (1!=FWRITE_FLOAT(bloomthres))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom threshold error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_att_on_tag = NOX_BIN_FINAL_BLOOM_ATTENUATION_ON;
	if (1!=FWRITE_USHORT(nox_final_att_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom attenuation enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short blatton = fm->bloomAttenuationOn ? 1 : 0;
	if (1!=FWRITE_USHORT(blatton))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom attenuation enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_bl_att_col_tag = NOX_BIN_FINAL_BLOOM_ATTENUATION_COLOR;
	if (1!=FWRITE_USHORT(nox_final_bl_att_col_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom attenuation color tag error.", "Error", 0);
		return false;
	}
	float blattcolr  = fm->bloomAttenuationColor.r;
	float blattcolg  = fm->bloomAttenuationColor.g;
	float blattcolb  = fm->bloomAttenuationColor.b;
	if (1!=FWRITE_FLOAT(blattcolr))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom attenuation color R error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(blattcolg))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom attenuation color G error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(blattcolb))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final bloom attenuation color B error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_glare_on_tag = NOX_BIN_FINAL_GLARE_ON;
	if (1!=FWRITE_USHORT(nox_final_glare_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short glareon = fm->glareEnabled ? 1 : 0;
	if (1!=FWRITE_USHORT(glareon))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_glare_power_tag = NOX_BIN_FINAL_GLARE_POWER;
	if (1!=FWRITE_USHORT(nox_final_glare_power_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare power tag error.", "Error", 0);
		return false;
	}
	float glarepower = fm->glarePower;
	if (1!=FWRITE_FLOAT(glarepower))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare power error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_glare_disp_tag = NOX_BIN_FINAL_GLARE_DISPERSION;
	if (1!=FWRITE_USHORT(nox_final_glare_disp_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare dispersion tag error.", "Error", 0);
		return false;
	}
	float glaredisp  = fm->glareDispersion;
	if (1!=FWRITE_FLOAT(glaredisp))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare dispersion error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_glare_thres_tag = NOX_BIN_FINAL_GLARE_THRESHOLD;
	if (1!=FWRITE_USHORT(nox_final_glare_thres_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare threshold tag error.", "Error", 0);
		return false;
	}
	float glarethres = fm->glareThreshold;
	if (1!=FWRITE_FLOAT(glarethres))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare threshold error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_glare_area_tag = NOX_BIN_FINAL_GLARE_AREA;
	if (1!=FWRITE_USHORT(nox_final_glare_area_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare area tag error.", "Error", 0);
		return false;
	}
	unsigned int glarearea = fm->glareArea;
	if (1!=FWRITE_UINT(glarearea))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare area error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_glare_shape_on_tag = NOX_BIN_FINAL_GLARE_SHAPE_ON;
	if (1!=FWRITE_USHORT(nox_final_glare_shape_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare shape enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short glareshapeon = fm->glareTextureApertureOn ? 1 : 0;
	if (1!=FWRITE_USHORT(glareshapeon))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare shape enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_glare_obst_on_tag = NOX_BIN_FINAL_GLARE_OBSTACLE_ON;
	if (1!=FWRITE_USHORT(nox_final_glare_obst_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare obstacle enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short glareobston = fm->glareTextureObstacleOn ? 1 : 0;
	if (1!=FWRITE_USHORT(glareobston))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final glare obstacle enabled error.", "Error", 0);
		return false;
	}

	char * t1 = texDiaph;
	char * t2 = texObst;

	if (t1)
	{
		unsigned short nox_final_glare_shape_tex_tag = NOX_BIN_FINAL_GLARE_SHAPE_TEX;
		if (1!=FWRITE_USHORT(nox_final_glare_shape_tex_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing camera final glare shape texture tag error.", "Error", 0);
			return false;
		}
		unsigned int texlen = t1  ? ((int)strlen(t1)+6) : 0;
		if (1!=FWRITE_UINT(texlen))
		{
			if (showMessages)
				MessageBox(0, "Writing camera final glare shape texture size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(t1, texlen-6))
		{
			if (showMessages)
				MessageBox(0, "Writing camera final glare shape texture error.", "Error", 0);
			return false;
		}
	}

	if (t2)
	{
		unsigned short nox_final_glare_obst_tex_tag = NOX_BIN_FINAL_GLARE_OBSTACLE_TEX;
		if (1!=FWRITE_USHORT(nox_final_glare_obst_tex_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing camera final glare obstacle texture tag error.", "Error", 0);
			return false;
		}
		unsigned int texlen = t2 ? ((int)strlen(t2)+6) : 0;
		if (1!=FWRITE_UINT(texlen))
		{
			if (showMessages)
				MessageBox(0, "Writing camera final glare obstacle texture size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(t2, texlen-6))
		{
			if (showMessages)
				MessageBox(0, "Writing camera final glare obstacle texture error.", "Error", 0);
			return false;
		}
	}

	unsigned short nox_final_dots_on_tag = NOX_BIN_FINAL_DOTS_ON;
	if (1!=FWRITE_USHORT(nox_final_dots_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short dotson = fm->dotsEnabled ? 1 : 0;
	if (1!=FWRITE_USHORT(dotson))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_dots_radius_tag = NOX_BIN_FINAL_DOTS_RADIUS;
	if (1!=FWRITE_USHORT(nox_final_dots_radius_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover radius tag error.", "Error", 0);
		return false;
	}
	unsigned int dotsRad = fm->dotsRadius;
	if (1!=FWRITE_UINT(dotsRad))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover radius error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_dots_thres_tag = NOX_BIN_FINAL_DOTS_THRESHOLD;
	if (1!=FWRITE_USHORT(nox_final_dots_thres_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover threshold tag error.", "Error", 0);
		return false;
	}
	float dotsThres = fm->dotsThreshold;
	if (1!=FWRITE_FLOAT(dotsThres))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover threshold error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_dots_density_tag = NOX_BIN_FINAL_DOTS_DENSITY;
	if (1!=FWRITE_USHORT(nox_final_dots_density_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover density tag error.", "Error", 0);
		return false;
	}
	unsigned int dotsDens = fm->dotsDensity;
	if (1!=FWRITE_UINT(dotsDens))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final hot pixels remover density error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_grain_tag = NOX_BIN_FINAL_GRAIN;
	if (1!=FWRITE_USHORT(nox_final_grain_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final grain tag error.", "Error", 0);
		return false;
	}
	float grain = fm->grain;
	if (1!=FWRITE_FLOAT(grain))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final grain error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_fog_on_tag = NOX_BIN_FINAL_FOG_ENABLED;
	if (1!=FWRITE_USHORT(nox_final_fog_on_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short fogon = fm->fogEnabled ? 1 : 0;
	if (1!=FWRITE_USHORT(fogon))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_fog_exclbg_tag = NOX_BIN_FINAL_FOG_EXCLUDE_BG;
	if (1!=FWRITE_USHORT(nox_final_fog_exclbg_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog exclude bg tag error.", "Error", 0);
		return false;
	}
	unsigned short fogexcl = fm->fogExclSky ? 1 : 0;
	if (1!=FWRITE_USHORT(fogexcl))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog exclude bg error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_fog_dist_tag = NOX_BIN_FINAL_FOG_DISTANCE;
	if (1!=FWRITE_USHORT(nox_final_fog_dist_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog distance tag error.", "Error", 0);
		return false;
	}
	float fogdist = fm->fogDist;
	if (1!=FWRITE_FLOAT(fogdist))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog distance error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_fog_dens_tag = NOX_BIN_FINAL_FOG_DENSITY;
	if (1!=FWRITE_USHORT(nox_final_fog_dens_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog density tag error.", "Error", 0);
		return false;
	}
	float fogdens = fm->fogSpeed;
	if (1!=FWRITE_FLOAT(fogdens))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog density error.", "Error", 0);
		return false;
	}

	unsigned short nox_final_fog_col_tag = NOX_BIN_FINAL_FOG_COLOR;
	if (1!=FWRITE_USHORT(nox_final_fog_col_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog color tag error.", "Error", 0);
		return false;
	}
	float fgcolr  = fm->fogColor.r;
	float fgcolg  = fm->fogColor.g;
	float fgcolb  = fm->fogColor.b;
	if (1!=FWRITE_FLOAT(fgcolr))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog color R error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(fgcolg))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog color G error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(fgcolb))
	{
		if (showMessages)
			MessageBox(0, "Writing camera final fog color B error.", "Error", 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != postsize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Camera final post chunk size is incorrect.\nShould be %llu instead of %llu bytes.", postsize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertSunskyPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (!rtr->curScenePtr->sscene.sunsky)
	{
		if (showMessages)
			MessageBox(0, "Error. Tried to save NULL sunsky.", "Error", 0);
		return false;
	}

	unsigned short nss_on = rtr->curScenePtr->sscene.useSunSky ? 1 : 0;
	unsigned short nss_month = rtr->curScenePtr->sscene.sunsky->getMonth();
	unsigned short nss_day = rtr->curScenePtr->sscene.sunsky->getDay();
	unsigned short nss_hour = rtr->curScenePtr->sscene.sunsky->getHour();
	unsigned short nss_minute = (unsigned short)rtr->curScenePtr->sscene.sunsky->getMinute();
	unsigned short nss_gmt = (unsigned short)(rtr->curScenePtr->sscene.sunsky->getMeridianForTimeZone()*12/PI);
	unsigned short nss_sun_on = rtr->curScenePtr->sscene.sunsky->sunOn ? 1 : 0;
	unsigned short nss_sun_other_layer = rtr->curScenePtr->sscene.sunsky->sunOtherBlendLayer ? 1 : 0;
	unsigned short nss_model = rtr->curScenePtr->sscene.sunsky->use_hosek ? 1 : 0;
	float nss_longitude = rtr->curScenePtr->sscene.sunsky->getLongitude();
	float nss_latitude = rtr->curScenePtr->sscene.sunsky->getLatitude();
	float nss_turbidity = rtr->curScenePtr->sscene.sunsky->turbidity;
	float nss_aerosol = rtr->curScenePtr->sscene.sunsky->aerosol;
	float nss_sunsize = rtr->curScenePtr->sscene.sunsky->sunSize;
	float nss_dirNx = rtr->curScenePtr->sscene.sunsky->dirN.x;
	float nss_dirNy = rtr->curScenePtr->sscene.sunsky->dirN.y;
	float nss_dirNz = rtr->curScenePtr->sscene.sunsky->dirN.z;
	float nss_dirEx = rtr->curScenePtr->sscene.sunsky->dirE.x;
	float nss_dirEy = rtr->curScenePtr->sscene.sunsky->dirE.y;
	float nss_dirEz = rtr->curScenePtr->sscene.sunsky->dirE.z;
	float nss_albedo = rtr->curScenePtr->sscene.sunsky->albedo;

	unsigned long long sskysize = evalSizeSunsky();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_sunsky_tag = NOX_BIN_SUNSKY;
	if (1!=FWRITE_USHORT(nox_sunsky_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(sskysize))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky size error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_on = NOX_BIN_SS_ON;
	if (1!=FWRITE_USHORT(nox_ss_on))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky on tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_on))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky on error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_month = NOX_BIN_SS_MONTH;
	if (1!=FWRITE_USHORT(nox_ss_month))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky month tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_month))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky month error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_day = NOX_BIN_SS_DAY;
	if (1!=FWRITE_USHORT(nox_ss_day))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky day tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_day))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky day error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_hour = NOX_BIN_SS_HOUR;
	if (1!=FWRITE_USHORT(nox_ss_hour))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky hour tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_hour))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky hour error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_minute = NOX_BIN_SS_MINUTE;
	if (1!=FWRITE_USHORT(nox_ss_minute))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky minute tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_minute))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky minute error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_gmt = NOX_BIN_SS_GMT;
	if (1!=FWRITE_USHORT(nox_ss_gmt))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky gmt tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_gmt))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky gmt error.", "Error", 0);
		return false;
	}


	unsigned short nox_ss_longitude = NOX_BIN_SS_LONGITUDE;
	if (1!=FWRITE_USHORT(nox_ss_longitude))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky longitude tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_longitude))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky longitude error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_latitude = NOX_BIN_SS_LATITUDE;
	if (1!=FWRITE_USHORT(nox_ss_latitude))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky latitude tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_latitude))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky latitude error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_turbidity = NOX_BIN_SS_TURBIDITY;
	if (1!=FWRITE_USHORT(nox_ss_turbidity))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky turbidity tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_turbidity))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky turbidity error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_aerosol = NOX_BIN_SS_AEROSOL;
	if (1!=FWRITE_USHORT(nox_ss_aerosol))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky aerosol tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_aerosol))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky aerosol error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_sunsize = NOX_BIN_SS_SUN_SIZE;
	if (1!=FWRITE_USHORT(nox_ss_sunsize))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky sun diameter tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_sunsize))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky sun diameter error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_sunON = NOX_BIN_SS_SUN_ON;
	if (1!=FWRITE_USHORT(nox_ss_sunON))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky sun on tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_sun_on))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky sun on error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_sunOL = NOX_BIN_SS_SUN_OTHER_LAYER;
	if (1!=FWRITE_USHORT(nox_ss_sunOL))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky sun other blend layer tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_sun_other_layer))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky sun other blend layer error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_dirs = NOX_BIN_SS_DIRECTIONS;
	if (1!=FWRITE_USHORT(nox_ss_dirs))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky directions tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_dirNx))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_dirNy))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_dirNz))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_dirEx))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_dirEy))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_dirEz))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky directions error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_model = NOX_BIN_SS_MODEL;
	if (1!=FWRITE_USHORT(nox_ss_model))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky model tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(nss_model))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky model error.", "Error", 0);
		return false;
	}

	unsigned short nox_ss_albedo = NOX_BIN_SS_ALBEDO;
	if (1!=FWRITE_USHORT(nox_ss_albedo))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky albedo tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(nss_albedo))
	{
		if (showMessages)
			MessageBox(0, "Writing sunsky albedo error.", "Error", 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sskysize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. SunSky chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sskysize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertBlendLayersPart(BlendSettings * blend)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}
	if (!blend)
	{
		if (showMessages)
			MessageBox(0, "Error. Blend layers settings object is NULL.", "Error", 0);
		return false;
	}

	BlendSettings * bs = blend;

	unsigned long long allblendssize = evalSizeBlendLayers(blend);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_blends_tag = NOX_BIN_BLEND_LAYERS;
	if (1!=FWRITE_USHORT(nox_blends_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Blend Layers tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(allblendssize))
	{
		if (showMessages)
			MessageBox(0, "Writing blend layers size error.", "Error", 0);
		return false;
	}
	for (unsigned int i=0; i<16; i++)
	{
		char * tname = bs->names[i];
		if (!tname)
			tname = "Unnamed";
		
		unsigned int tlen = (unsigned int)strlen(tname);
		if (tlen<1)
		{
			tname = "Unnamed";
			tlen = 7;	// "Unnamed"
		}
		tlen +=6;

		unsigned long long layersize = tlen + 42;
		unsigned short nox_blend_tag = NOX_BIN_BLEND_LAYER;
		if (1!=FWRITE_USHORT(nox_blend_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing Blend Layer tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_ULONG8(layersize))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(i))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer ID error.", "Error", 0);
			return false;
		}

		unsigned short nox_blend_on_tag = NOX_BIN_BLEND_LAYER_ON;
		if (1!=FWRITE_USHORT(nox_blend_on_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing Blend Layer On tag error.", "Error", 0);
			return false;
		}
		unsigned short isOn = (bs->blendOn[i] ? 1 : 0);
		if (1!=FWRITE_USHORT(isOn))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer on error.", "Error", 0);
			return false;
		}

		unsigned short nox_blend_layer_name_tag = NOX_BIN_BLEND_LAYER_NAME;
		if (1!=FWRITE_USHORT(nox_blend_layer_name_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing Blend Layer Name tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(tlen))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer name size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(tname, (tlen-6)))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer name error.", "Error", 0);
			return false;
		}

		unsigned short nox_blend_layer_weight_tag = NOX_BIN_BLEND_LAYER_WEIGHT;
		if (1!=FWRITE_USHORT(nox_blend_layer_weight_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing Blend Layer Weight tag error.", "Error", 0);
			return false;
		}
		float weight = bs->weights[i] * 100;
		if (1!=FWRITE_FLOAT(weight))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer weight error.", "Error", 0);
			return false;
		}

		unsigned short nox_blend_layer_red_tag = NOX_BIN_BLEND_LAYER_RED;
		if (1!=FWRITE_USHORT(nox_blend_layer_red_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing Blend Layer Red tag error.", "Error", 0);
			return false;
		}
		float red = bs->red[i] * 100;
		if (1!=FWRITE_FLOAT(red))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer red error.", "Error", 0);
			return false;
		}

		unsigned short nox_blend_layer_green_tag = NOX_BIN_BLEND_LAYER_GREEN;
		if (1!=FWRITE_USHORT(nox_blend_layer_green_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing Blend Layer Green tag error.", "Error", 0);
			return false;
		}
		float green = bs->green[i] * 100;
		if (1!=FWRITE_FLOAT(green))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer green error.", "Error", 0);
			return false;
		}

		unsigned short nox_blend_layer_blue_tag = NOX_BIN_BLEND_LAYER_BLUE;
		if (1!=FWRITE_USHORT(nox_blend_layer_blue_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing Blend Layer Blue tag error.", "Error", 0);
			return false;
		}
		float blue = bs->blue[i] * 100;
		if (1!=FWRITE_FLOAT(blue))
		{
			if (showMessages)
				MessageBox(0, "Writing blend layer blue error.", "Error", 0);
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != allblendssize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. Blend layers chunk size is incorrect.\nShould be %llu instead of %llu bytes.", allblendssize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertUserColorsPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long usercolorssize = evalSizeUserColors();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_usercolors_tag = NOX_BIN_USERCOLORS;
	if (1!=FWRITE_USHORT(nox_usercolors_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing User Colors tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(usercolorssize))
	{
		if (showMessages)
			MessageBox(0, "Writing User Colors size error.", "Error", 0);
		return false;
	}

	for (int i=0; i<18; i++)
	{
		unsigned short nox_usersinglecolor_tag = NOX_BIN_USER_SINGLE_COLOR;
		if (1!=FWRITE_USHORT(nox_usersinglecolor_tag))
		{
			if (showMessages)
				MessageBox(0, "Writing User Color tag error.", "Error", 0);
			return false;
		}
		unsigned short color_id = i;
		if (1!=FWRITE_USHORT(color_id))
		{
			if (showMessages)
				MessageBox(0, "Writing User Color ID error.", "Error", 0);
			return false;
		}
		float col_r = UserColors::colors[i].r;
		if (1!=FWRITE_FLOAT(col_r))
		{
			if (showMessages)
				MessageBox(0, "Writing User Color R error.", "Error", 0);
			return false;
		}
		float col_g = UserColors::colors[i].g;
		if (1!=FWRITE_FLOAT(col_g))
		{
			if (showMessages)
				MessageBox(0, "Writing User Color G error.", "Error", 0);
			return false;
		}
		float col_b = UserColors::colors[i].b;
		if (1!=FWRITE_FLOAT(col_b))
		{
			if (showMessages)
				MessageBox(0, "Writing User Color B error.", "Error", 0);
			return false;
		}
		float col_a = UserColors::colors[i].a;
		if (1!=FWRITE_FLOAT(col_a))
		{
			if (showMessages)
				MessageBox(0, "Writing User Color A error.", "Error", 0);
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != usercolorssize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. User Colors chunk size is incorrect.\nShould be %llu instead of %llu bytes.", usercolorssize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertRendererSettingsPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long settingssize = evalSizeRendererSettings();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_settings_tag = NOX_BIN_RENDERER_SETTINGS;
	if (1!=FWRITE_USHORT(nox_settings_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Renderer Settings tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(settingssize))
	{
		if (showMessages)
			MessageBox(0, "Writing Renderer Settings size error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_on_stop_tag = NOX_BIN_RSETT_TIMER_ON_STOPAFTER;
	if (1!=FWRITE_USHORT(nox_timer_on_stop_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer On tag error.", "Error", 0);
		return false;
	}
	unsigned short stopOn = rtr->gui_timers.stoppingTimerOn ? 1 : 0;
	if (1!=FWRITE_USHORT(stopOn))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer On error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_on_refresh_tag = NOX_BIN_RSETT_TIMER_ON_REFRESH;
	if (1!=FWRITE_USHORT(nox_timer_on_refresh_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Refresh Timer On tag error.", "Error", 0);
		return false;
	}
	unsigned short refreshOn = rtr->gui_timers.refreshTimeOn ? 1 : 0;
	if (1!=FWRITE_USHORT(refreshOn))
	{
		if (showMessages)
			MessageBox(0, "Writing Refresh Timer On error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_on_autosave_tag = NOX_BIN_RSETT_TIMER_ON_AUTOSAVE;
	if (1!=FWRITE_USHORT(nox_timer_on_autosave_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Autosave Timer On tag error.", "Error", 0);
		return false;
	}
	unsigned short autosaveOn = rtr->gui_timers.autosaveTimerOn ? 1 : 0;
	if (1!=FWRITE_USHORT(autosaveOn))
	{
		if (showMessages)
			MessageBox(0, "Writing Autosave Timer On error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_stop_hours_tag = NOX_BIN_RSETT_TIMER_STOPAFTER_HOURS;
	if (1!=FWRITE_USHORT(nox_timer_stop_hours_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer Hours tag error.", "Error", 0);
		return false;
	}
	unsigned short stopHours = rtr->gui_timers.stopTimerHours;
	if (1!=FWRITE_USHORT(stopHours))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer Hours error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_stop_minutes_tag = NOX_BIN_RSETT_TIMER_STOPAFTER_MINUTES;
	if (1!=FWRITE_USHORT(nox_timer_stop_minutes_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer Minutes tag error.", "Error", 0);
		return false;
	}
	unsigned short stopMinutes = rtr->gui_timers.stopTimerMinutes;
	if (1!=FWRITE_USHORT(stopMinutes))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer Minutes error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_stop_seconds_tag = NOX_BIN_RSETT_TIMER_STOPAFTER_SECONDS;
	if (1!=FWRITE_USHORT(nox_timer_stop_seconds_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer Seconds tag error.", "Error", 0);
		return false;
	}
	unsigned short stopSeconds = rtr->gui_timers.stopTimerSeconds;
	if (1!=FWRITE_USHORT(stopSeconds))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop Timer Seconds error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_refresh_tag = NOX_BIN_RSETT_TIMER_REFRESH_SECONDS;
	if (1!=FWRITE_USHORT(nox_timer_refresh_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Refresh Timer tag error.", "Error", 0);
		return false;
	}
	unsigned short refreshTime = rtr->gui_timers.refreshTime;
	if (1!=FWRITE_USHORT(refreshTime))
	{
		if (showMessages)
			MessageBox(0, "Writing Refresh Timer error.", "Error", 0);
		return false;
	}

	unsigned short nox_timer_autosave_tag = NOX_BIN_RSETT_TIMER_AUTOSAVE_MINUTES;
	if (1!=FWRITE_USHORT(nox_timer_autosave_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Autosave Timer tag error.", "Error", 0);
		return false;
	}
	unsigned short autosaveTime = rtr->gui_timers.autosaveTimerMinutes;
	if (1!=FWRITE_USHORT(autosaveTime))
	{
		if (showMessages)
			MessageBox(0, "Writing Autosave Timer error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_active = NOX_BIN_RSETT_RUNNEXT_ACTIVE;
	if (1!=FWRITE_USHORT(tag_runnext_active))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Active tag error.", "Error", 0);
		return false;
	}
	unsigned short rnActive = rtr->gui_timers.rn_active ? 1 : 0;
	if (1!=FWRITE_USHORT(rnActive))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Active error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_camchange = NOX_BIN_RSETT_RUNNEXT_CAMERA_CHANGE;
	if (1!=FWRITE_USHORT(tag_runnext_camchange))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Camera Change tag error.", "Error", 0);
		return false;
	}
	unsigned short rnCamChange = rtr->gui_timers.rn_nextCam ? 1 : 0;
	if (1!=FWRITE_USHORT(rnCamChange))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Camera Change error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_camdesc = NOX_BIN_RSETT_RUNNEXT_CAMERA_DESCENNDING;
	if (1!=FWRITE_USHORT(tag_runnext_camdesc))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Camera Descending tag error.", "Error", 0);
		return false;
	}
	unsigned short rnCamDesc = rtr->gui_timers.rn_nCamDesc ? 1 : 0;
	if (1!=FWRITE_USHORT(rnCamDesc))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Camera Descending error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_copypost = NOX_BIN_RSETT_RUNNEXT_COPY_POST;
	if (1!=FWRITE_USHORT(tag_runnext_copypost))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Copy Post tag error.", "Error", 0);
		return false;
	}
	unsigned short rnCopyPost = rtr->gui_timers.rn_copyPost ? 1 : 0;
	if (1!=FWRITE_USHORT(rnCopyPost))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Copy Post error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_delbuff = NOX_BIN_RSETT_RUNNEXT_DELETE_BUFFERS;
	if (1!=FWRITE_USHORT(tag_runnext_delbuff))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Delete Buffers tag error.", "Error", 0);
		return false;
	}
	unsigned short rnDelBuff = rtr->gui_timers.rn_delBuffs ? 1 : 0;
	if (1!=FWRITE_USHORT(rnDelBuff))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Delete Buffers error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_saveimg = NOX_BIN_RSETT_RUNNEXT_SAVE_IMAGE;
	if (1!=FWRITE_USHORT(tag_runnext_saveimg))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Save Image tag error.", "Error", 0);
		return false;
	}
	unsigned short rnSaveImg = rtr->gui_timers.rn_saveImg ? 1 : 0;
	if (1!=FWRITE_USHORT(rnSaveImg))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Save Image error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_sunsky_change = NOX_BIN_RSETT_RUNNEXT_SUNSKY_CHANGE;
	if (1!=FWRITE_USHORT(tag_runnext_sunsky_change))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Sunsky Change tag error.", "Error", 0);
		return false;
	}
	unsigned short rnSunskyChange = rtr->gui_timers.rn_changeSunsky ? 1 : 0;
	if (1!=FWRITE_USHORT(rnSunskyChange))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Sunsky Change error.", "Error", 0);
		return false;
	}

	unsigned short tag_runnext_sunsky_minutes = NOX_BIN_RSETT_RUNNEXT_SUNSKY_MINUTES;
	if (1!=FWRITE_USHORT(tag_runnext_sunsky_minutes))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Sunsky Minutes tag error.", "Error", 0);
		return false;
	}
	unsigned int rnSunskyMinutes = rtr->gui_timers.rn_sunskyMins;
	if (1!=FWRITE_UINT(rnSunskyMinutes))
	{
		if (showMessages)
			MessageBox(0, "Writing Run Next Sunsky Minutes error.", "Error", 0);
		return false;
	}

	char * cFolder = rtr->gui_timers.rn_saveFolder;
	if (cFolder)
	{
		unsigned short tag_runnext_imgfolder = NOX_BIN_RSETT_RUNNEXT_SAVE_IMAGE_FOLDER;
		if (1!=FWRITE_USHORT(tag_runnext_imgfolder))
		{
			MessageBox(0, "Writing Run Next Save Image Folder tag error.", "Error", 0);
			return false;
		}
		unsigned int size_folder = (unsigned int)strlen(cFolder)+6;
		if (1!=FWRITE_UINT(size_folder))
		{
			MessageBox(0, "Writing Run Next Save Image Folder size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(cFolder, size_folder-6))
		{
			MessageBox(0, "Writing Run Next Save Image Folder error.", "Error", 0);
			return false;
		}
	}

	unsigned short tag_engine = NOX_BIN_RSETT_ENGINE;
	if (1!=FWRITE_USHORT(tag_engine))
	{
		if (showMessages)
			MessageBox(0, "Writing Engine tag error.", "Error", 0);
		return false;
	}
	Scene * sc = rtr->curScenePtr;
	unsigned short engine = (unsigned short)sc->sscene.giMethod;
	if (1!=FWRITE_USHORT(engine))
	{
		if (showMessages)
			MessageBox(0, "Writing Engine error.", "Error", 0);
		return false;
	}

	unsigned short tag_fill = NOX_BIN_RSETT_FILL_PATTERN;
	if (1!=FWRITE_USHORT(tag_fill))
	{
		if (showMessages)
			MessageBox(0, "Writing Pixel Fill Pattern tag error.", "Error", 0);
		return false;
	}
	unsigned short fillpattern = (unsigned short)sc->sscene.random_type;
	if (1!=FWRITE_USHORT(fillpattern))
	{
		if (showMessages)
			MessageBox(0, "Writing Pixel Fill Pattern error.", "Error", 0);
		return false;
	}

	unsigned short tag_bucketsize = NOX_BIN_RSETT_BUCKET_SIZE;
	if (1!=FWRITE_USHORT(tag_bucketsize))
	{
		if (showMessages)
			MessageBox(0, "Writing Bucket Size tag error.", "Error", 0);
		return false;
	}
	unsigned short bucketsizex = (unsigned short)sc->sscene.bucket_size_x;
	unsigned short bucketsizey = (unsigned short)sc->sscene.bucket_size_y;
	if (1!=FWRITE_USHORT(bucketsizex))
	{
		if (showMessages)
			MessageBox(0, "Writing Bucket Size error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bucketsizey))
	{
		if (showMessages)
			MessageBox(0, "Writing Bucket Size error.", "Error", 0);
		return false;
	}

	unsigned short tag_stopafterpass = NOX_BIN_RSETT_STOP_AFTER_PASS;
	if (1!=FWRITE_USHORT(tag_stopafterpass))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop After Pass tag error.", "Error", 0);
		return false;
	}
	unsigned short stopafterpass = sc->sscene.stop_after_pass ? 1 : 0;
	if (1!=FWRITE_USHORT(stopafterpass))
	{
		if (showMessages)
			MessageBox(0, "Writing Stop After Pass error.", "Error", 0);
		return false;
	}

	unsigned short tag_samplesperpixel = NOX_BIN_RSETT_SAMPLES_PER_PIXEL;
	if (1!=FWRITE_USHORT(tag_samplesperpixel))
	{
		if (showMessages)
			MessageBox(0, "Writing Samples Per Pixel tag error.", "Error", 0);
		return false;
	}
	unsigned int spp = sc->sscene.samples_per_pixel_in_pass;
	if (1!=FWRITE_UINT(spp))
	{
		if (showMessages)
			MessageBox(0, "Writing Samples Per Pixel error.", "Error", 0);
		return false;
	}

	unsigned short tag_ptgisamples = NOX_BIN_RSETT_PT_GI_SAMPLES;
	if (1!=FWRITE_USHORT(tag_ptgisamples))
	{
		if (showMessages)
			MessageBox(0, "Writing PT GI Samples tag error.", "Error", 0);
		return false;
	}
	unsigned int ptgs = sc->sscene.pt_gi_samples;
	if (1!=FWRITE_UINT(ptgs))
	{
		if (showMessages)
			MessageBox(0, "Writing PT GI Samples error.", "Error", 0);
		return false;
	}

	unsigned short tag_ptfakecaustic = NOX_BIN_RSETT_PT_USE_FAKE_CAUSTIC;
	if (1!=FWRITE_USHORT(tag_ptfakecaustic))
	{
		if (showMessages)
			MessageBox(0, "Writing PT Fake Caustic tag error.", "Error", 0);
		return false;
	}
	unsigned short ptcf = sc->sscene.causticFake ? 1 : 0;
	if (1!=FWRITE_USHORT(ptcf))
	{
		if (showMessages)
			MessageBox(0, "Writing PT Fake Caustic error.", "Error", 0);
		return false;
	}

	unsigned short tag_ptd2caustic = NOX_BIN_RSETT_PT_GI_DISABLE_SECOND_CAUSTIC;
	if (1!=FWRITE_USHORT(tag_ptd2caustic))
	{
		if (showMessages)
			MessageBox(0, "Writing PT Disable Second Caustic tag error.", "Error", 0);
		return false;
	}
	unsigned short ptd2c = sc->sscene.disableSecondaryCaustic ? 1 : 0;
	if (1!=FWRITE_USHORT(ptd2c))
	{
		if (showMessages)
			MessageBox(0, "Writing PT Disable Second Caustic error.", "Error", 0);
		return false;
	}

	unsigned short tag_ptmaxcr = NOX_BIN_RSETT_PT_MAX_CAUSTIC_ROUGH;
	if (1!=FWRITE_USHORT(tag_ptmaxcr))
	{
		if (showMessages)
			MessageBox(0, "Writing PT Max Caustic Rough tag error.", "Error", 0);
		return false;
	}
	float ptmcr = sc->sscene.causticMaxRough;
	if (1!=FWRITE_FLOAT(ptmcr))
	{
		if (showMessages)
			MessageBox(0, "Writing PT Max Caustic Rough error.", "Error", 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != settingssize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. Renderer Settings chunk size is incorrect.\nShould be %llu instead of %llu bytes.", settingssize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertSceneInfoPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	unsigned int numTris = sc->triangles.objCount;
	char pswd[64];
	sprintf_s(pswd, 64, "%d", numTris);
	unsigned int lpswd = (unsigned int)strlen(pswd);


	unsigned int size_scene_name   = (sc->scene_name) ?				6+(unsigned int)strlen(sc->scene_name)			: 0;
	unsigned int size_scene_author = (sc->scene_author_name) ?		6+(unsigned int)strlen(sc->scene_author_name)	: 0;
	unsigned int size_scene_email  = (sc->scene_author_email) ?		6+(unsigned int)strlen(sc->scene_author_email)	: 0;
	unsigned int size_scene_www    = (sc->scene_author_www) ?		6+(unsigned int)strlen(sc->scene_author_www)	: 0;
	
	unsigned long long sceneinfoSize = 10 + size_scene_name + size_scene_author + size_scene_email + size_scene_www;
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_sceneinfo_tag = NOX_BIN_SCENE_INFO;
	if (1!=FWRITE_USHORT(nox_sceneinfo_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Scene Info tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(sceneinfoSize))
	{
		if (showMessages)
			MessageBox(0, "Writing Scene Info size error.", "Error", 0);
		return false;
	}

	if (sc->scene_name)
	{
		unsigned short nox_scenename_tag = NOX_BIN_SCENEINFO_SCENENAME;
		if (1!=FWRITE_USHORT(nox_scenename_tag))
		{
			MessageBox(0, "Writing Scene Name tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_name))
		{
			MessageBox(0, "Writing Scene Name size error.", "Error", 0);
			return false;
		}
		char * decString = xorStringToNew(sc->scene_name, pswd, size_scene_name-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_name-6))
		{
			free(decString);
			MessageBox(0, "Writing Scene Name error.", "Error", 0);
			return false;
		}
		free(decString);
	}

	if (sc->scene_author_name)
	{
		unsigned short nox_sceneauthor_tag = NOX_BIN_SCENEINFO_AUTHOR;
		if (1!=FWRITE_USHORT(nox_sceneauthor_tag))
		{
			MessageBox(0, "Writing Scene Author tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_author))
		{
			MessageBox(0, "Writing Scene Author size error.", "Error", 0);
			return false;
		}
		char * decString = xorStringToNew(sc->scene_author_name, pswd, size_scene_author-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_author-6))
		{
			free(decString);
			MessageBox(0, "Writing Scene Author error.", "Error", 0);
			return false;
		}
		free(decString);
	}

	if (sc->scene_author_email)
	{
		unsigned short nox_sceneemail_tag = NOX_BIN_SCENEINFO_EMAIL;
		if (1!=FWRITE_USHORT(nox_sceneemail_tag))
		{
			MessageBox(0, "Writing Scene Author Email tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_email))
		{
			MessageBox(0, "Writing Scene Author Email size error.", "Error", 0);
			return false;
		}
		char * decString = xorStringToNew(sc->scene_author_email, pswd, size_scene_email-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_email-6))
		{
			free(decString);
			MessageBox(0, "Writing Scene Author Email error.", "Error", 0);
			return false;
		}
		free(decString);
	}

	if (sc->scene_author_www)
	{
		unsigned short nox_scenewww_tag = NOX_BIN_SCENEINFO_WWW;
		if (1!=FWRITE_USHORT(nox_scenewww_tag))
		{
			MessageBox(0, "Writing Scene Author Webpage tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_www))
		{
			MessageBox(0, "Writing Scene Author Webpage size error.", "Error", 0);
			return false;
		}
		char * decString = xorStringToNew(sc->scene_author_www, pswd, size_scene_www-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_www-6))
		{
			free(decString);
			MessageBox(0, "Writing Scene Author Webpage error.", "Error", 0);
			return false;
		}
		free(decString);
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sceneinfoSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. Scene info chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sceneinfoSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertSceneSettingsPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	unsigned long long sceneSettingsSize = evalSizeSceneSettings();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_scenesettings_tag = NOX_BIN_SCENE_SETTINGS;
	if (1!=FWRITE_USHORT(nox_scenesettings_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Scene Settings tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(sceneSettingsSize))
	{
		if (showMessages)
			MessageBox(0, "Writing Scene Settings size error.", "Error", 0);
		return false;
	}

	unsigned short nox_mb_enabled_tag = NOX_BIN_SCENESETT_MOTION_BLUR_ON;
	if (1!=FWRITE_USHORT(nox_mb_enabled_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Motion Blur Enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short mb_enabled = sc->sscene.motion_blur_on ? 1 : 0;
	if (1!=FWRITE_USHORT(mb_enabled))
	{
		if (showMessages)
			MessageBox(0, "Writing Motion Blur Enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_mb_time_tag = NOX_BIN_SCENESETT_MOTION_BLUR_TIME;
	if (1!=FWRITE_USHORT(nox_mb_time_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Motion Blur Time tag error.", "Error", 0);
		return false;
	}
	float mb_time = sc->sscene.motion_blur_time;
	if (1!=FWRITE_FLOAT(mb_time))
	{
		if (showMessages)
			MessageBox(0, "Writing Motion Blur Time error.", "Error", 0);
		return false;
	}

	unsigned short nox_mb_max_time_tag = NOX_BIN_SCENESETT_MOTION_BLUR_MAX_TIME;
	if (1!=FWRITE_USHORT(nox_mb_max_time_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Motion Blur Max Time tag error.", "Error", 0);
		return false;
	}
	float mb_max_time = sc->sscene.motion_blur_max_time;
	if (1!=FWRITE_FLOAT(mb_max_time))
	{
		if (showMessages)
			MessageBox(0, "Writing Motion Blur Max Time error.", "Error", 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sceneSettingsSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. Scene settings chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sceneSettingsSize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertEnvironmentMapPart()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long envmapsize = evalSizeEnvironmentMap();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_envmap_tag = NOX_BIN_ENVIRONMENT_MAP;
	if (1!=FWRITE_USHORT(nox_envmap_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(envmapsize))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map size error.", "Error", 0);
		return false;
	}


	if (!insertTexturePart(0, 0, TEX_SLOT_ENV))
		return false;

	unsigned short nox_env_enabled_tag = NOX_BIN_ENV_ENABLED;
	if (1!=FWRITE_USHORT(nox_env_enabled_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Enabled tag error.", "Error", 0);
		return false;
	}
	unsigned short enabled = rtr->curScenePtr->env.enabled ? 1 : 0;
	if (1!=FWRITE_USHORT(enabled))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Enabled error.", "Error", 0);
		return false;
	}

	unsigned short nox_env_blendlayer_tag = NOX_BIN_ENV_BLEND_LAYER;
	if (1!=FWRITE_USHORT(nox_env_blendlayer_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Blend Layer tag error.", "Error", 0);
		return false;
	}
	unsigned short blendlayer = rtr->curScenePtr->env.blendlayer;
	if (1!=FWRITE_USHORT(blendlayer))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Blend Layer error.", "Error", 0);
		return false;
	}

	unsigned short nox_env_powerev_tag = NOX_BIN_ENV_POWER_EV;
	if (1!=FWRITE_USHORT(nox_env_powerev_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Power EV tag error.", "Error", 0);
		return false;
	}
	float powerev = rtr->curScenePtr->env.powerEV;
	if (1!=FWRITE_FLOAT(powerev))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Power EV error.", "Error", 0);
		return false;
	}

	unsigned short nox_env_azimuth_tag = NOX_BIN_ENV_AZIMUTH;
	if (1!=FWRITE_USHORT(nox_env_azimuth_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Azimuth tag error.", "Error", 0);
		return false;
	}
	float azimuth = rtr->curScenePtr->env.azimuth_shift;
	if (1!=FWRITE_FLOAT(azimuth))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Azimuth error.", "Error", 0);
		return false;
	}

	unsigned short nox_env_directions_tag = NOX_BIN_ENV_DIRECTIONS;
	if (1!=FWRITE_USHORT(nox_env_directions_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Directions tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(rtr->curScenePtr->env.dirN.x))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(rtr->curScenePtr->env.dirN.y))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(rtr->curScenePtr->env.dirN.z))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(rtr->curScenePtr->env.dirE.x))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(rtr->curScenePtr->env.dirE.y))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Directions error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(rtr->curScenePtr->env.dirE.z))
	{
		if (showMessages)
			MessageBox(0, "Writing Environment Map Directions error.", "Error", 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != envmapsize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. Environment Map chunk size is incorrect.\nShould be %llu instead of %llu bytes.", envmapsize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertUserPresets()
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	unsigned long long presetssize = evalSizeUserPresets();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_presets_tag = NOX_BIN_USER_PRESETS;
	if (1!=FWRITE_USHORT(nox_presets_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing User Presets tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(presetssize))
	{
		if (showMessages)
			MessageBox(0, "Writing User Presets size error.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	int np = rtr->curScenePtr->presets.objCount;
	for (int i=0; i<np; i++)
	{
		if (!insertUserPreset(rtr->curScenePtr->presets[i]))
			return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != presetssize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. User Presets chunk size is incorrect.\nShould be %llu instead of %llu bytes.", presetssize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::insertUserPreset(PresetPost * preset)
{
	if (!file)
	{
		if (showMessages)
			MessageBox(0, "Inner error. File is NULL.", "Error", 0);
		return false;
	}

	if (!preset)
		return 0;

	unsigned long long presetsize = evalSizeUserPreset(preset);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_preset_tag = NOX_BIN_USER_PRESET;
	if (1!=FWRITE_USHORT(nox_preset_tag))
	{
		if (showMessages)
			MessageBox(0, "Writing User Preset tag error.", "Error", 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(presetsize))
	{
		if (showMessages)
			MessageBox(0, "Writing User Preset size error.", "Error", 0);
		return false;
	}

	if (preset->name)
	{
		unsigned int namesize = 6 + (unsigned int)strlen(preset->name);
		unsigned short tag_inst_name = NOX_BIN_USER_PRESET_NAME;
		if (1!=FWRITE_USHORT(tag_inst_name))
		{
			if (showMessages)
				MessageBox(0, "Writing user preset name tag error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_UINT(namesize))
		{
			if (showMessages)
				MessageBox(0, "Writing user preset name size error.", "Error", 0);
			return false;
		}
		if (1!=FWRITE_STRING(preset->name, (namesize-6)))
		{
			if (showMessages)
				MessageBox(0, "Writing user preset name error.", "Error", 0);
			return false;
		}
	}

	if (!insertBlendLayersPart(&preset->blend))
		return false;
	if (!insertCameraPostPart(&preset->iMod))
		return false;
	if (!insertCameraFinalPostPart(&preset->fMod, preset->texGlareDiaphragm, preset->texGlareObstacle))
		return false;
	
	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != presetsize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Save error. User Preset chunk size is incorrect.\nShould be %llu instead of %llu bytes.", presetsize, realSize);
		if (showMessages)
			MessageBox(0, errmsg, "Error", 0);
		return false;
	}

	return true;
}


//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeScene()
{
	Raytracer * rtr = Raytracer::getInstance();
	geometrySize = evalSizeGeometry();
	unsigned long long camerasSize = 0;
	for (int i=0; i<rtr->curScenePtr->cameras.objCount; i++)
		camerasSize += evalSizeCamera((unsigned int)i);
	materialsSize = evalSizeMaterials();
	unsigned long long sunskySize = evalSizeSunsky();
	unsigned long long blendsSize = evalSizeBlendLayers(&rtr->curScenePtr->blendSettings);
	unsigned long long userColorsSize = evalSizeUserColors();
	unsigned long long rendererSettingsSize = evalSizeRendererSettings();
	unsigned long long sceneInfoSize = evalSizeSceneInfo();
	unsigned long long envMapSize = evalSizeEnvironmentMap();
	unsigned long long sceneSettingsSize = evalSizeSceneSettings();
	unsigned long long userPresetsSize = evalSizeUserPresets();

	sceneSize = 10 + geometrySize + camerasSize + sunskySize + blendsSize + materialsSize + userColorsSize + rendererSettingsSize + sceneInfoSize + envMapSize + sceneSettingsSize + userPresetsSize;
	return sceneSize;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeGeometry()
{
	Raytracer * rtr = Raytracer::getInstance();
	unsigned int numMeshes = (unsigned int)rtr->curScenePtr->meshes.objCount;
	unsigned int numInstances = (unsigned int)rtr->curScenePtr->instances.objCount;
	unsigned long long sumMeshesSize = 0;
	unsigned long long sumInstancesSize = 0;

	for (unsigned int i=0; i<numMeshes; i++)
		sumMeshesSize += evalSizeInstanceSourcePart(i);

	for (unsigned int i=0; i<numInstances; i++)
		sumMeshesSize += evalSizeInstancePart(i);

	return sumMeshesSize + 28;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeInstancePart(unsigned int iId)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	if (iId < 0   ||   iId >= (unsigned int)sc->instances.objCount)
		return 0;
	unsigned long long res = 14 + 66;
	if (sc->instances[iId].name)
		res += 6 + strlen(sc->instances[iId].name);
	res += 4 + 4 * sc->instances[iId].materials.objCount; 
	res += 36;
	res += 66;

	return res;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeInstanceSourcePart(unsigned int mId)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	if (mId < 0   ||   mId >= (unsigned int)sc->meshes.objCount)
		return 0;

	Mesh * m = &(sc->meshes[mId]);
	if (m->displacement_instance_id > -1)
		return 0;

	unsigned long long res = 14;
	unsigned long long mesh_size = evalSizeMesh(mId);
	res += mesh_size;
	return res;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeMesh(unsigned int num)
{
	Raytracer * rtr = Raytracer::getInstance();

	if (num < 0   ||   num >= (unsigned int)rtr->curScenePtr->meshes.objCount)
		return 0;

	Mesh &mesh = rtr->curScenePtr->meshes[num];

	char * name = mesh.name;
	if (!name)
		name = "Unnamed";
	unsigned int lname = (unsigned int)strlen(name);
	if(lname==0)
	{
		name = "Unnamed";
		lname = 7;
	}
	lname +=6;

	unsigned int mnTris = mesh.lastTri - mesh.firstTri + 1;

	unsigned long long triSize = evalSizeTriangle();
	unsigned long long res = triSize * mnTris + lname + 14;

	return res;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeTriangle()
{
	return 106;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeCamera(unsigned int num)
{
	Raytracer * rtr = Raytracer::getInstance();

	unsigned int numCams = rtr->curScenePtr->cameras.objCount;
	if (num < 0   ||   num >= (unsigned int)rtr->curScenePtr->cameras.objCount)
		return 0;

	Camera * cam = rtr->curScenePtr->cameras[num];
	CHECK(cam);

	char * name = cam->name;
	if (!name)
		name = "Unnamed";
	unsigned int lname = (unsigned int)strlen(name);
	if (lname==0)
	{
		name = "Unnamed";
		lname = 7;
	}
	lname +=6;

	unsigned long long bufsSize  = evalSizeCameraBuffers(num);
	unsigned long long dbufSize  = evalSizeCameraDepthBuf(num);
	unsigned long long postSize  = evalSizePost(&cam->iMod);
	unsigned long long finalSize = evalSizeCameraFinalPost(&cam->fMod, cam->texDiaphragm.fullfilename, cam->texObstacle.fullfilename);

	if (!addCamBuffs)
	{
		bufsSize = 0;
		dbufSize = 0;
	}

	return 144 + lname + bufsSize + dbufSize + postSize + finalSize + 10 + 36 + 14;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeCameraBuffers(unsigned int num)
{
	Raytracer * rtr = Raytracer::getInstance();

	unsigned int numCams = rtr->curScenePtr->cameras.objCount;
	if (num < 0   ||   num >= (unsigned int)rtr->curScenePtr->cameras.objCount)
		return 0;

	Camera * cam = rtr->curScenePtr->cameras[num];
	CHECK(cam);

	unsigned long long bufsSize = 0;
	for (unsigned int i=0; i<16; i++)
	{
		if (!cam->blendBits & (1<<i))
			continue;
		if (!cam->blendBuffDirect[i].imgBuf)
			continue;
		if (!cam->blendBuffDirect[i].hitsBuf)
			continue;
		if (!cam->blendBuffGI[i].imgBuf)
			continue;
		if (!cam->blendBuffGI[i].hitsBuf)
			continue;
		if (cam->blendBuffGI[i].width != cam->width*cam->aa   ||   cam->blendBuffGI[i].height != cam->height*cam->aa)
			continue;
		if (cam->blendBuffDirect[i].width != cam->width*cam->aa   ||   cam->blendBuffDirect[i].height != cam->height*cam->aa)
			continue;

		bufsSize += cam->width*cam->height*cam->aa*cam->aa * (sizeof(Color4)+sizeof(int)) * 2;
		bufsSize += 14 * 2;
	}

	bufsSize += 34+6;

	return bufsSize;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeCameraDepthBuf(unsigned int num)
{
	Raytracer * rtr = Raytracer::getInstance();

	unsigned int numCams = rtr->curScenePtr->cameras.objCount;
	if (num < 0   ||   num >= (unsigned int)rtr->curScenePtr->cameras.objCount)
		return 0;

	Camera * cam = rtr->curScenePtr->cameras[num];
	CHECK(cam);
	CHECK(cam->depthBuf);
	CHECK(cam->depthBuf->fbuf);
	CHECK(cam->depthBuf->hbuf);
	CHECK(cam->depthBuf->width>0);
	CHECK(cam->depthBuf->height>0);

	unsigned long long res = 0;
	res = cam->depthBuf->width * cam->depthBuf->height * (sizeof(float) + sizeof(unsigned int)) + 18;

	return res;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeMaterials()
{
	unsigned long long sumsize = 0;
	unsigned int nummats = (unsigned int)Raytracer::getInstance()->curScenePtr->mats.objCount;
	for (unsigned int i=0; i<nummats; i++)
		sumsize += evalSizeMaterial(i);

	return 14 + sumsize;

	return 0;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeMaterial(unsigned int num)
{
	Raytracer * rtr = Raytracer::getInstance();
	unsigned int nMats = (unsigned int)rtr->curScenePtr->mats.objCount;
	if (num>=nMats || num<0)
		return 0;

	MaterialNox * mat = rtr->curScenePtr->mats[num];
	CHECK(mat);

	char * name = mat->name;
	if (!name)
		name = "Unnamed";
	unsigned int lname = (unsigned int)strlen(name);
	if(lname==0)
	{
		name = "Unnamed";
		lname = 7;
	}
	lname +=6;

	char * texOpac = mat->tex_opacity.fullfilename;
	int lOpac = texOpac ? ((int)strlen(texOpac)+6) : 0;
	unsigned long long lsOpac = evalSizeTexture(num, 0, MAT_TEX_SLOT_OPACITY);
	unsigned long long lsDisp = evalSizeTexture(num, 0, MAT_TEX_SLOT_DISPLACEMENT);

	unsigned int nLays = mat->layers.objCount;
	unsigned long long lSize = 0;
	for (unsigned int i=0; i<nLays; i++)
		lSize += evalSizeMaterialLayer(num, i);

	unsigned long long prevSize = evalSizeMaterialPreview(num);

	unsigned long long result = 22 + prevSize + lSize + lname + 4 + 10 + 10+lsOpac+lsDisp+20+4;
	return result;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeMaterialPreview(unsigned int num)
{
	Raytracer * rtr = Raytracer::getInstance();
	unsigned int nMats = (unsigned int)rtr->curScenePtr->mats.objCount;
	if (num>=nMats || num<0)
		return 0;

	MaterialNox * mat = rtr->curScenePtr->mats[num];
	CHECK(mat);

	CHECK(mat->preview);
	CHECK(mat->preview->imgBuf);
	unsigned int pWidth  = mat->preview->width;
	unsigned int pHeight = mat->preview->height;

	return pWidth * pHeight * sizeof(COLORREF) + 18;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeMaterialLayer(unsigned int num, unsigned int numlayer)
{
	Raytracer * rtr = Raytracer::getInstance();
	unsigned int nMats = (unsigned int)rtr->curScenePtr->mats.objCount;
	if (num>=nMats || num<0)
		return 0;

	MaterialNox * mat = rtr->curScenePtr->mats[num];
	CHECK(mat);

	if (numlayer >= (unsigned int)mat->layers.objCount)
		return 0;

	MatLayer * mlay = &(mat->layers[numlayer]);

	int iesLen = 0;
	if (mlay->ies  &&  mlay->ies->filename)
	{
		if (mlay->ies->exportedFilename)
			iesLen = ((int)strlen(mlay->ies->exportedFilename)+6);
		else
			iesLen = ((int)strlen(mlay->ies->filename)+6);
	}
	
	unsigned long long ltLight  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_LIGHT);
	unsigned long long ltRefl0  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_COLOR0);
	unsigned long long ltRefl90 = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_COLOR90);
	unsigned long long ltRough  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_ROUGHNESS);
	unsigned long long ltWeight = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_WEIGHT);
	unsigned long long ltNormal = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_NORMAL);
	unsigned long long ltTransm = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_TRANSM);
	unsigned long long ltAniso  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_ANISOTROPY);
	unsigned long long ltAnisoA = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_ANISO_ANGLE);
	unsigned long long totalTexNew = ltLight + ltRefl0 + ltRefl90 + ltRough + ltWeight + ltNormal + ltTransm + ltAniso + ltAnisoA;

	return totalTexNew + 226 + iesLen;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeTexture(unsigned int num, unsigned int numlayer, unsigned int slot)
{
	Raytracer * rtr = Raytracer::getInstance();
	unsigned int nMats = (unsigned int)rtr->curScenePtr->mats.objCount;
	if (num>=nMats || num<0)
		return 0;

	if (slot==TEX_SLOT_ENV)
	{
		TextureInstance * tex = &(rtr->curScenePtr->env.tex);
		char * texffname = (tex->exportedFilename	? tex->exportedFilename : tex->fullfilename);
		int ltLen = texffname  ? ((int)strlen(texffname)+6) : 0;
		return ltLen + 80 + 20;
	}

	MaterialNox * mat = rtr->curScenePtr->mats[num];
	CHECK(mat);

	if (numlayer >= (unsigned int)mat->layers.objCount)
		return 0;

	MatLayer * mlay = &(mat->layers[numlayer]);

	char * texffname = NULL;
	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			
			texffname = (mlay->tex_col0.exportedFilename	? mlay->tex_col0.exportedFilename	: mlay->tex_col0.fullfilename);		break;
		case LAYER_TEX_SLOT_COLOR90:		
			texffname = (mlay->tex_col90.exportedFilename	? mlay->tex_col90.exportedFilename	: mlay->tex_col90.fullfilename);	break;
		case LAYER_TEX_SLOT_ROUGHNESS:		
			texffname = (mlay->tex_rough.exportedFilename	? mlay->tex_rough.exportedFilename	: mlay->tex_rough.fullfilename);	break;
		case LAYER_TEX_SLOT_WEIGHT:			
			texffname = (mlay->tex_weight.exportedFilename	? mlay->tex_weight.exportedFilename : mlay->tex_weight.fullfilename);	break;
		case LAYER_TEX_SLOT_LIGHT:			
			texffname = (mlay->tex_light.exportedFilename	? mlay->tex_light.exportedFilename	: mlay->tex_light.fullfilename);	break;
		case LAYER_TEX_SLOT_NORMAL:
			texffname = (mlay->tex_normal.exportedFilename	? mlay->tex_normal.exportedFilename	: mlay->tex_normal.fullfilename);	break;
		case LAYER_TEX_SLOT_TRANSM:
			texffname = (mlay->tex_transm.exportedFilename	? mlay->tex_transm.exportedFilename	: mlay->tex_transm.fullfilename);	break;
		case LAYER_TEX_SLOT_ANISOTROPY:
			texffname = (mlay->tex_aniso.exportedFilename	? mlay->tex_aniso.exportedFilename	: mlay->tex_aniso.fullfilename);	break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:
			texffname = (mlay->tex_aniso_angle.exportedFilename	? mlay->tex_aniso_angle.exportedFilename	: mlay->tex_aniso_angle.fullfilename);	break;
		case MAT_TEX_SLOT_OPACITY:			
			texffname = (mat->tex_opacity.exportedFilename	? mat->tex_opacity.exportedFilename : mat->tex_opacity.fullfilename);	break;
		case MAT_TEX_SLOT_DISPLACEMENT:			
			texffname = (mat->tex_displacement.exportedFilename	? mat->tex_displacement.exportedFilename : mat->tex_displacement.fullfilename);	break;
	}
	int ltLen = texffname  ? ((int)strlen(texffname)+6) : 0;

	return ltLen + 80 + 20;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeSunsky()
{
	if (!Raytracer::getInstance()->curScenePtr->sscene.sunsky)
		return 0;
	else
		return 108;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeBlendLayers(BlendSettings * blend)
{
	unsigned int res = 0;
	Raytracer * rtr = Raytracer::getInstance();
	for (int i=0; i<16; i++)
	{
		char * tname = blend->names[i];
		if (!tname)
		{
			res += 7+6;		// "Unnamed"
			continue;
		}
		
		unsigned int tlen = (unsigned int)strlen(tname);
		if (tlen<1)
		{
			res += 7+6;	// "Unnamed"
			continue;
		}

		res += tlen+6;
	}

	res += 16*42;
	res += 10;

	return res;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeUserColors()
{
	return 370;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizePost(ImageModifier * iMod)
{
	CHECK(iMod);
	ImageModifier * im = iMod;
	unsigned int ltexGlDiaph = 0, ltexGlObst = 0;
	int rCnt = im->getCurveRedPointsCount();
	int gCnt = im->getCurveGreenPointsCount();
	int bCnt = im->getCurveBluePointsCount();
	int lCnt = im->getCurveLuminancePointsCount();

	unsigned long long result = 198 + (rCnt+gCnt+bCnt+lCnt)*6 + ltexGlDiaph + ltexGlObst - 68;// post/glare deleted
	return result;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeRendererSettings()
{
	Raytracer * rtr = Raytracer::getInstance();
	char * cFolder = rtr->gui_timers.rn_saveFolder;
	unsigned int size_folder = 0;
	if (cFolder)
		size_folder = (unsigned int)strlen(cFolder)+6;

	return 42 + 34 + 4 + 20 + 20 + size_folder;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeSceneInfo()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	unsigned int size_scene_name   = (sc->scene_name) ?				6+(unsigned int)strlen(sc->scene_name)			: 0;
	unsigned int size_scene_author = (sc->scene_author_name) ?		6+(unsigned int)strlen(sc->scene_author_name)	: 0;
	unsigned int size_scene_email  = (sc->scene_author_email) ?		6+(unsigned int)strlen(sc->scene_author_email)	: 0;
	unsigned int size_scene_www    = (sc->scene_author_www) ?		6+(unsigned int)strlen(sc->scene_author_www)	: 0;
	
	unsigned long long result = 10 + size_scene_name + size_scene_author + size_scene_email + size_scene_www;
	return result;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeSceneSettings()
{
	return 26;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeCameraFinalPost(FinalModifier * fMod, char * texDiaph, char * texObst)
{
	Raytracer * rtr = Raytracer::getInstance();

	if (!fMod)
		return 0;

	char * t1 = texDiaph; 
	char * t2 = texObst; 
	int lt1Len = t1  ? ((int)strlen(t1)+6) : 0;
	int lt2Len = t2  ? ((int)strlen(t2)+6) : 0;

	return 94 + 122 + 34 + lt1Len + lt2Len;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeEnvironmentMap()
{
	Raytracer * rtr = Raytracer::getInstance();
	unsigned long long texSize = 0;
	texSize = evalSizeTexture(0, 0, TEX_SLOT_ENV);
	return texSize + 24 + 32;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeUserPresets()
{
	unsigned long long res = 10;
	Raytracer * rtr = Raytracer::getInstance();
	int np = rtr->curScenePtr->presets.objCount;
	for (int i=0; i<np; i++)
	{
		unsigned long long pss = evalSizeUserPreset(rtr->curScenePtr->presets[i]);
		res += pss;
	}
	return res;
}

//----------------------------------------------------------------------------------------

unsigned long long BinaryScene::evalSizeUserPreset(PresetPost * preset)
{
	if (!preset)
		return 0;
	unsigned long long res = 10;
	res += evalSizeBlendLayers(&preset->blend);
	res += evalSizePost(&preset->iMod);
	res += evalSizeCameraFinalPost(&preset->fMod, preset->texGlareDiaphragm, preset->texGlareObstacle);
	if (preset->name)
		res += 6 + strlen(preset->name);
	return res;
}

//----------------------------------------------------------------------------------------
