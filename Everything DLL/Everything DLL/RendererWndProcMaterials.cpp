#include "resource.h"
#include "RendererMainWindow.h"
#include "MatEditor.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>

extern HMODULE hDllModule;
void tempCheckIntegrity(int a);

struct image_list_style
{
	static int list_width;
	int columns;
	int icon_size;
	int grid_x;
	int grid_y;
	int text_margin;
	int text_upper_margin;
	int round_corner;
	bool show_name;
};

int image_list_style::list_width;
image_list_style cur_style;


INT_PTR CALLBACK RendererMainWindow::MaterialsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				hMaterials = hWnd;	// dirty

				HWND hEdit  = GetDlgItem(hWnd, IDC_REND_MATERIALS_EDIT_BUTTON);
				EMButton * emb = GetEMButtonInstance(hEdit);
				theme.apply(emb);

				HWND hPr = GetDlgItem(hWnd, IDC_REND_MATERIALS_PREVIEW);
				EMPView * empv = GetEMPViewInstance(hPr);
				empv->dontLetUserChangeZoom = true;
				if (empv->byteBuff)
					delete empv->byteBuff;
				empv->byteBuff = NULL;
				theme.apply(empv);

				HWND hList  = GetDlgItem(hWnd, IDC_REND_MATERIALS_LISTVIEW);
				EMListView * emlv  = GetEMListViewInstance(hList);
				theme.apply(emlv);
				emlv->setColNumAndClearData(4, 100);
				emlv->colWidths[0] = 20;
				emlv->colWidths[2] = 40;
				emlv->colWidths[3] = 30;
				emlv->headers.freeList();
				emlv->headers.add((char*)malloc(64));
				emlv->headers.add((char*)malloc(64));
				emlv->headers.add((char*)malloc(64));
				emlv->headers.add((char*)malloc(64));
				emlv->headers.createArray();
				sprintf_s(emlv->headers[0], 64, "ID");
				sprintf_s(emlv->headers[1], 64, "Name");
				sprintf_s(emlv->headers[2], 64, "Layers");
				sprintf_s(emlv->headers[3], 64, "Emiss");

				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_GR1)));
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_TEXT1)), false);
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_TEXT2)), false);
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_TEXT3)), false);
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_TEXT4)), false);
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_NAME)), false);
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_LAYERS_COUNT)), false);
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_EMISSION)), false);
				theme.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_BLEND)), false);

				cur_style.list_width = 721;
				cur_style.columns = 9;
				cur_style.icon_size = 64;
				cur_style.grid_x = 80;
				cur_style.grid_y = 96;
				cur_style.text_margin = 6;
				cur_style.round_corner = 16;
				cur_style.show_name = true;
				cur_style.text_upper_margin = 4;

				EMImageList * emil = GetEMImageListInstance(GetDlgItem(hWnd, IDC_REND_MATERIALS_IMAGE_LIST));
				if (emil)
				{
					emil->img_width = cur_style.icon_size;
					emil->img_height = cur_style.icon_size;
					emil->img_cols = cur_style.columns;
					emil->img_rows = 4;
					emil->img_grid_x = cur_style.grid_x;
					emil->img_grid_y = cur_style.grid_y;
					emil->scrollGrid = 10;
					emil->start_x = 0;
					emil->start_y = 0;
					emil->imageSelected = -1;
					emil->imageMouseOver = -1;
					emil->drawImageBorders = true;
					emil->drawControlBorder = true;
					emil->scrollVertical = true;
					emil->showNames1 = cur_style.show_name;
					emil->showNames2 = false;
					emil->corner_round = cur_style.round_corner;
					emil->text_horiz_margin = cur_style.text_margin;
					emil->text_upper_margin = cur_style.text_upper_margin;
					emil->imgNames1.createArray();
					emil->imgNames2.createArray();
					emil->imgBuffers.createArray();

					emil->verifyPosition();

					RECT wrect;
					GetWindowRect(emil->hwnd, &wrect);
					POINT p1 = {wrect.left, wrect.top};
					POINT p2 = {wrect.right, wrect.bottom};
					ScreenToClient(hWnd, &p1);
					ScreenToClient(hWnd, &p2);
					wrect.left   = p1.x;
					wrect.top    = p1.y;
					wrect.right  = p2.x;
					wrect.bottom = p2.y;
					SetWindowPos(emil->hwnd, HWND_TOP, 0,0,  cur_style.list_width, wrect.bottom-wrect.top, SWP_NOMOVE | SWP_NOZORDER);
					HWND hPrev = GetDlgItem(hWnd, IDC_REND_MATERIALS_PREVIEW);
					HWND hEdit = GetDlgItem(hWnd, IDC_REND_MATERIALS_EDIT_BUTTON);
					HWND hScr  = GetDlgItem(hWnd, IDC_REND_MATERIALS_PREVIEW_IMLIST_SCROLL);
					SetWindowPos(hScr,  HWND_TOP, wrect.left+cur_style.list_width   , wrect.top,     0,0, SWP_NOSIZE | SWP_NOZORDER);
					SetWindowPos(hPrev, HWND_TOP, wrect.left+cur_style.list_width+24, wrect.top,     0,0, SWP_NOSIZE | SWP_NOZORDER);
					SetWindowPos(hEdit, HWND_TOP, wrect.left+cur_style.list_width+24, wrect.top+198, 0,0, SWP_NOSIZE | SWP_NOZORDER);
					
					int wl = emil->getWorkspaceLength();
					EMScrollBar * emsb = GetEMScrollBarInstance(hScr);
					emsb->size_screen = wrect.bottom - wrect.top;
					emsb->size_work = wl;
					emsb->workarea_pos = emil->start_y;
					emsb->updateSize();
					emsb->updateRegions();

					InvalidateRect(emil->hwnd, NULL, false);
					InvalidateRect(emsb->hwnd, NULL, false);

					theme.apply(emil);
					theme.apply(emsb);
				}

				RendererMainWindow::fillMaterialsImageList();

			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_MATERIALS_LISTVIEW:
					{
						if (wmEvent == LV_DOUBLECLIKED)
						{
							HWND hButton = GetDlgItem(hMaterials, IDC_REND_MATERIALS_EDIT_BUTTON);
							DWORD style;
							style = GetWindowLong(hButton, GWL_STYLE);
							bool disabled = ((style & WS_DISABLED) > 0);
							bool accept = false;

							HWND hList = GetDlgItem(hMaterials, IDC_REND_MATERIALS_LISTVIEW);
							EMListView * emlv = GetEMListViewInstance(hList);
							CHECK(emlv);

							Raytracer * rtr = Raytracer::getInstance();
							bool nowRendering = rtr->curScenePtr->nowRendering;

							if (!disabled  &&  !nowRendering)
								accept = editMaterial(emlv->selected);

							afterMatEdit(accept, emlv->selected);

							break;
						}

						if (wmEvent == LV_SELECTION_CHANGED)
						{
							HWND hList = GetDlgItem(hMaterials, IDC_REND_MATERIALS_LISTVIEW);
							EMListView * emlv = GetEMListViewInstance(hList);
							CHECK(emlv);

							updateMaterialPreview(emlv->selected);
							break;
						}

						if (wmEvent == LV_MOUSEOVER_CHANGED)
						{
						}
					}
					break;

					case IDC_REND_MATERIALS_IMAGE_LIST:
					{
						EMImageList * emil = GetEMImageListInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_IMAGE_LIST));
						if (!emil)
							break;

						switch (wmEvent)
						{
							case IL_POS_CHANGED:
								{
									HWND hScr = GetDlgItem(hMaterials, IDC_REND_MATERIALS_PREVIEW_IMLIST_SCROLL);
									EMScrollBar * emsb = GetEMScrollBarInstance(hScr);
									emsb->workarea_pos = emil->start_y;
									emsb->updateSize();
									emsb->updateRegions();
									InvalidateRect(hScr, NULL, false);
								}
								break;
							case IL_SELECTION_CHANGED:
								{
									updateMaterialPreviewFullSize(emil->imageSelected);
									fillSingleMaterialData();
								}
								break;
							case IL_MOUSEOVER_CHANGED:
								{
								}
								break;
							case IL_DOUBLECLICK:
								{
									bool accept = false;

									Raytracer * rtr = Raytracer::getInstance();
									if (rtr->curScenePtr->nowRendering)
										break;

									if (emil->imageSelected>=0   &&   emil->imageSelected<rtr->curScenePtr->mats.objCount)
									{
										accept = editMaterial(emil->imageSelected);

										afterMatEdit(accept, emil->imageSelected);
									}
								}
								break;
						}
					}
					break;
					case IDC_REND_MATERIALS_PREVIEW_IMLIST_SCROLL:
					{
						EMImageList * emil = GetEMImageListInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_IMAGE_LIST));
						EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_PREVIEW_IMLIST_SCROLL));
						if (!emil | !emsb)
							break;
						if (wmEvent != SB_POS_CHANGED)
							break;

						emil->start_y = emsb->workarea_pos;
						InvalidateRect(emil->hwnd, NULL, false);
					}
					break;

					case IDC_REND_MATERIALS_EDIT_BUTTON:
					{
						if (wmEvent != BN_CLICKED)
							break;

						EMImageList * emil = GetEMImageListInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_IMAGE_LIST));
						CHECK(emil);
						int mat = emil->imageSelected;
						if (mat<0)
							break;

						bool accept = editMaterial(mat);

						afterMatEdit(accept, mat);
					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------------------------
