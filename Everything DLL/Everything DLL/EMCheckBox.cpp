#include <windows.h>
#include <stdio.h>
#include "EMControls.h"

extern HMODULE hDllModule;

EMCheckBox::EMCheckBox(HWND hWnd)
{
	colBorderLight = RGB(128,240,255);
	colBorderDark  = RGB(0,128,192);
	colBackground  = RGB(255,255,255);
	colBackgroundDisabled  = RGB(160,160,160);
	colX = RGB(0,0,0);
	colXDisabled = RGB(64,64,64);
	clicked = false;
	selected = false;
	hwnd = hWnd;
	size = 15;
	hRgn = 0;
}

EMCheckBox::~EMCheckBox()
{
}

void EMCheckBox::evalRegion()
{
	HRGN rg;
	POINT p[] = {
		{0, 2},
		{2, 0},
		{size-2, 0},
		{size-0, 2},
		{size-0, size-3},
		{size-3, size-0},
		{2, size-0},
		{0, size-3},
	};
	rg = CreatePolygonRgn(p, 8, WINDING);
	SetWindowRgn(hwnd, rg, TRUE);
	if (hRgn)
		DeleteObject(hRgn);
	hRgn = rg;
}


void InitEMCheckBox()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMCheckBox";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMCheckBoxProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMCheckBox *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}


EMCheckBox * GetEMCheckBoxInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMCheckBox * emcb = (EMCheckBox *)GetWindowLongPtr(hwnd, 0);
	#else
		EMCheckBox * emcb = (EMCheckBox *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcb;
}

void SetEMCheckBoxInstance(HWND hwnd, EMCheckBox *emcb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcb);
	#endif
}

LRESULT CALLBACK EMCheckBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMCheckBox * emcb;
	emcb = GetEMCheckBoxInstance(hwnd);

    switch(msg)
    {
	case WM_CREATE:
		{
			EMCheckBox * emcb1 = new EMCheckBox(hwnd);
			SetEMCheckBoxInstance(hwnd, (EMCheckBox *)emcb1);			
			emcb1->evalRegion();

		}
	break;
	case WM_NCDESTROY:
		{
			EMCheckBox * emcb1;
			emcb1 = GetEMCheckBoxInstance(hwnd);
			delete emcb1;
			SetEMCheckBoxInstance(hwnd, 0);
		}
	break;
	case BM_GETCHECK:
	{
		if (emcb->selected)
			return BST_CHECKED;
		else
			return BST_UNCHECKED;
	}
	break;
	case WM_PAINT:
	{
		if (!emcb) 
			break;

		RECT rect;
		HDC hdc;
		PAINTSTRUCT ps;
		HPEN hOldPen;

		int size = emcb->size;

		HDC ohdc = BeginPaint(hwnd, &ps);

		hdc = CreateCompatibleDC(ohdc);
		GetClientRect(hwnd, &rect);
		HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
		SelectObject(hdc, backBMP);

		GetClientRect(hwnd, &rect);

		HRGN rg;
		POINT p[] = {
			{0, 2},
			{2, 0},
			{size-2, 0},
			{size-0, 2},
			{size-0, size-3},
			{size-3, size-0},
			{2, size-0},
			{0, size-3},
		};
		rg = CreatePolygonRgn(p, 8, WINDING);

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);

		HBRUSH fbr;
		if (disabled)
			fbr = CreateSolidBrush(emcb->colBackgroundDisabled);
		else
			fbr = CreateSolidBrush(emcb->colBackground);
		FillRgn(hdc, rg, fbr);
		DeleteObject(fbr);
		DeleteObject(rg);

		POINT border1[] = {	{0, size-3}, 
							{0, 2}, 
							{2, 0}, 
							{size-2, 0} };	// 4
		POINT border2[] = {	{size-2, 1}, 
							{size-1, 2}, 
							{size-1, size-3}, 
							{size-3, size-1}, 
							{2, size-1},
							{0, size-3} };	// 6
		
		if (emcb->selected)
		{
			POINT px1[] = { {3,3}, {size-3, size-3} };
			POINT px2[] = { {3,4}, {size-4, size-3} };
			POINT px3[] = { {4,3}, {size-3, size-4} };

			POINT px4[] = { {size-4,3}, {2, size-3} };
			POINT px5[] = { {size-5,3}, {2, size-4} };
			POINT px6[] = { {size-4,4}, {3, size-3} };
			if (disabled)
				hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emcb->colXDisabled));
			else
				hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emcb->colX));
			Polyline(hdc, px1, 2);
			Polyline(hdc, px2, 2);
			Polyline(hdc, px3, 2);
			Polyline(hdc, px4, 2);
			Polyline(hdc, px5, 2);
			Polyline(hdc, px6, 2);
			DeleteObject(SelectObject(hdc, hOldPen));
		}
		else
		{
		}

		hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emcb->colBorderDark));
		Polyline(hdc, border1, 4);
		DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emcb->colBorderLight)) );
		Polyline(hdc, border2, 6);
		DeleteObject(SelectObject(hdc, hOldPen));
		
		GetClientRect(hwnd, &rect);
		BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
		DeleteDC(hdc); 
		DeleteObject(backBMP);

		EndPaint(hwnd, &ps);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		if (!GetCapture())
		{
			emcb->selected = !emcb->selected;
			RECT rect;
			GetClientRect(hwnd, &rect);
			InvalidateRect(hwnd, &rect, false);
			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), 
					(LPARAM)hwnd);
		}
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

