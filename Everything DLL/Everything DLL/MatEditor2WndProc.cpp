#include "MatEditor2.h"
#include "EM2Controls.h"
#include "resource.h"
#include <GdiPlus.h>
#include "dialogs.h"
#include "log.h"
#include "XML_IO.h"

extern HMODULE hDllModule;

//----------------------------------------------------------------------

INT_PTR CALLBACK MatEditor2::MatEditor2WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int mat_mode = 0;
	HBRUSH bgBrush = 0;
	MatEditor2 * matedit = MatEditor2::getInstance();

	switch (message)
	{
		case WM_INITDIALOG:
			{
				matedit->isMatEditOpened = true;
				mat_mode = (int)lParam;
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				matedit->hMain = hWnd;	// dirty but necessary
				matedit->initControls();
				matedit->showMatlib(false);
				matedit->postWindowOpen();

				if (matedit->autoLoadFilename)
				{
					matedit->loadMaterial(matedit->autoLoadFilename);
				}

				matedit->copyBufferMaterialToWindow();
				// h = 724
				matedit->needToUpdateDisplacement = true;
				matedit->updateControlLocks(false);
				SetFocus(0);
				matedit->startTextureWatcherThread();
				return FALSE;
			}
			break;
		case WM_DESTROY:
			{
				matedit->isMatEditOpened = false;
				EndDialog(hWnd, 0);
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (mat_mode == MatEditor2::MODE_ALONE)
				{
					Logger::add("Closing Material Editor window.");
					Logger::closeFile();
					PostQuitMessage(0);
				}
			}
			break;
		case WM_CLOSE:
			{
				SendMessage(hWnd, WM_COMMAND, MAKEWPARAM(IDC_MATEDIT2_CANCEL, BN_CLICKED), (LPARAM)GetDlgItem(hWnd, IDC_MATEDIT2_CANCEL));
			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (matedit->hBmpBg)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, matedit->hBmpBg);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				
				int xl = matedit->isMatLibOpened ? 379 : 0;
				int xr=xl+390-xx, yr=269-yy;
				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);
						Gdiplus::Pen s1pen(Gdiplus::Color(255, 104,104,104));

						Gdiplus::Point pts1[] = {
									Gdiplus::Point(xr+109, yr+68), 
									Gdiplus::Point(xr+118, yr+68), 
									Gdiplus::Point(xr+118, yr+72) };
						graphics.DrawLines(&s1pen, pts1, 3);
						Gdiplus::Point pts2[] = {
									Gdiplus::Point(xr+109, yr+97), 
									Gdiplus::Point(xr+118, yr+97), 
									Gdiplus::Point(xr+118, yr+93) };
						graphics.DrawLines(&s1pen, pts2, 3);

						Gdiplus::Point pts3[] = {
									Gdiplus::Point(xr+253, yr+89), 
									Gdiplus::Point(xr+253, yr+98), 
									Gdiplus::Point(xr+257, yr+98) };
						graphics.DrawLines(&s1pen, pts3, 3);
						Gdiplus::Point pts4[] = {
									Gdiplus::Point(xr+310, yr+89), 
									Gdiplus::Point(xr+310, yr+98), 
									Gdiplus::Point(xr+306, yr+98) };
						graphics.DrawLines(&s1pen, pts4, 3);

					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(104,104,104)));
					MoveToEx(hdc,	xr+109, yr+68, NULL);
					LineTo(hdc,		xr+118, yr+68);
					LineTo(hdc,		xr+118, yr+73);
					MoveToEx(hdc,	xr+109, yr+97, NULL);
					LineTo(hdc,		xr+118, yr+97);
					LineTo(hdc,		xr+118, yr+92);
					MoveToEx(hdc,	xr+253, yr+89, NULL);
					LineTo(hdc,		xr+253, yr+98);
					LineTo(hdc,		xr+258, yr+98);
					MoveToEx(hdc,	xr+310, yr+89, NULL);
					LineTo(hdc,		xr+310, yr+98);
					LineTo(hdc,		xr+305, yr+98);
					DeleteObject(SelectObject(hdc, hOldPen));
				#endif


				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;

		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_MATEDIT2_OK:						//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							if (sc->nowRendering)
							{
								HWND hButton = GetDlgItem(hWnd, IDC_MATEDIT2_PREV_RENDER);
								EM2Button * embutton = GetEM2ButtonInstance(hButton);
								embutton->changeCaption("RENDER");
								matedit->stopRendering();
								Sleep(100);
							}   

							matedit->copyBufferWindowToMaterial();
							matedit->updateMaterialToEditableMaterial();

							if (matedit->saveOnOK  &&  matedit->autoLoadFilename  &&  mat_mode==MatEditor2::MODE_ALONE)
							{
								XMLScene xscene;
								Raytracer * rtr = Raytracer::getInstance();
								bool saved = xscene.saveMaterial(matedit->autoLoadFilename, 
											matedit->getEditedMaterial());
							}

							EndDialog(hWnd, (INT_PTR)1);
						}
						break;
					case IDC_MATEDIT2_CANCEL:					//----------------------------------------------------------
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							if (sc->nowRendering)
							{
								HWND hButton = GetDlgItem(hWnd, IDC_MATEDIT2_PREV_RENDER);
								EM2Button * embutton = GetEM2ButtonInstance(hButton);
								embutton->changeCaption("RENDER");
								matedit->stopRendering();
								Sleep(100);
							}

							EndDialog(hWnd, (INT_PTR)0);
						}
						break;


					case IDC_MATEDIT2_PREVIEW:					//----------------------------------------------------------
						{
						}
						break;
					case IDC_MATEDIT2_MATERIAL_NAME:			//----------------------------------------------------------
						{
							CHECK(wmEvent==ES_RETURN  ||  wmEvent==ES_LOST_FOCUS);
							matedit->guiToMat();
						}
						break;
					case IDC_MATEDIT2_SKYPORTAL:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToMat();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);

						}
						break;
					case IDC_MATEDIT2_MATTE_SHADOW:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToMat();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);

						}
						break;
					case IDC_MATEDIT2_PREV_SCENE:				//----------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EM2ComboBox * emcb = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_PREV_SCENE));
							CHECK(emcb);
							int scNum = emcb->selected;

							matedit->updateMaterialToEditableMaterial();
							MaterialNox * matold = matedit->getEditedMaterial();
							matold->deleteBuffers(true, true, true);

							matedit->changeScene(scNum);

							MaterialNox * mat = matedit->getEditedMaterial();
							mat->prSceneIndex = scNum;
							matedit->needToUpdateDisplacement = true;

						}
						break;
					case IDC_MATEDIT2_PREV_RENDER:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							HWND hButton = GetDlgItem(hWnd, IDC_MATEDIT2_PREV_RENDER);
							EM2Button * embutton = GetEM2ButtonInstance(hButton);
							if (sc->nowRendering)
							{
								embutton->changeCaption("RENDER");
								matedit->stopRendering();
							}
							else
							{
								bool canRender = true;
								if (matedit->needToUpdateDisplacement)
									canRender = matedit->updateDisplacement();
								if (!canRender)
									break;
								matedit->needToUpdateDisplacement = false;
								bool started = matedit->startRendering();
								if (started)
									embutton->changeCaption("STOP");
							}

							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
							InvalidateRect(hButton, NULL, false);
						}
						break;
					case IDC_MATEDIT2_LOAD:						//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->loadMaterial();
							matedit->needToUpdateDisplacement = true;
							SetFocus(0);
						}
						break;
					case IDC_MATEDIT2_SAVE:						//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->saveMaterial();
						}
						break;
					case IDC_MATEDIT2_LIBRARY_OPEN:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MessageBox(0, "Soon...\nor not too soon", "", 0);
						}
						break;


					case IDC_MATEDIT2_DISPL_IGNORE_SCALE:		//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToMat();
							matedit->needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT2_DISPL_SUBDIVS:			//----------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							matedit->guiToMat();
							matedit->needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT2_DISPL_CONTINUITY:			//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToMat();
							matedit->needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT2_DISPL_DEPTH:				//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToMat();
							matedit->needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT2_DISPL_SHIFT:				//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToMat();
							matedit->needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT2_DISPL_NORMALS:			//----------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							matedit->guiToMat();
							matedit->needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT2_TXON_DISPLACEMENT:		//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToMat();
							matedit->updateCurrentLayerPropsOnList();
							matedit->needToUpdateDisplacement = true;
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_TX_DISPLACEMENT:			//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MaterialNox * mat = matedit->getEditedMaterial();
							bool ok = matedit->runTextureDialog(&mat->tex_displacement, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_DISPLACEMENT));
								mat->use_tex_displacement = emcon->selected = mat->tex_displacement.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
								matedit->updateCurrentLayerPropsOnList();
								matedit->needToUpdateDisplacement = true;
								matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
							}
						}
						break;
					case IDC_MATEDIT2_TXON_NORMALBUMP:			//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							matedit->updateCurrentLayerPropsOnList();
						}
						break;
					case IDC_MATEDIT2_TX_NORMALBUMP:			//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_normal, true);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_NORMALBUMP));
								mlay->use_tex_normal = emcon->selected = mlay->tex_normal.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
								matedit->updateCurrentLayerPropsOnList();
								matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
							}
						}
						break;


					case IDC_MATEDIT2_EMISSION:					//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							matedit->updateCurrentLayerPropsOnList();
							Raytracer::getInstance()->curScenePtr->createLightMeshes();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_EMISS_COLOR:				//----------------------------------------------------------
						{
							if (wmEvent==BN_CLICKED)
							{
								EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, wmId));
								if (emc)
								{
									Color4 col = emc->color;
									Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));
									emc->isMouseOver = false;
									emc->isClicked = false;
									InvalidateRect(emc->hwnd, NULL, false);
									if (res)
									{
										MatLayer * mlay = matedit->getEditedMatLayer();
										CHECK(mlay);
										mlay->color = *res;
										emc->redraw(*res);
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								matedit->guiToLayerCurrent();
							}
						}
						break;
					case IDC_MATEDIT2_EMISS_TEMPERATURE:		//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_MATEDIT2_EMISS_COLOR));
							CHECK(mlay);
							CHECK(emc);
							unsigned int temperaturesrc = mlay->emitterTemperature;
							unsigned int * temperaturedst = (unsigned int *)DialogBoxParam(hDllModule,
										MAKEINTRESOURCE(IDD_TEMPERATURE_DIALOG2), hWnd, TemperatureDlg2Proc,(LPARAM)( temperaturesrc ));
							if (temperaturedst)
							{
								mlay->emitterTemperature = *temperaturedst;
								Color4 c = TemperatureColor::getGammaColor(*temperaturedst);
								emc->redraw(c);
								mlay->color = c;
							}
						}
						break;
					case IDC_MATEDIT2_EMISS_POWER:				//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
							Raytracer::getInstance()->curScenePtr->createLightMeshes();
						}
						break;
					case IDC_MATEDIT2_EMISS_UNITS:				//----------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							matedit->guiToLayerCurrent();
							Raytracer::getInstance()->curScenePtr->createLightMeshes();
						}
						break;
					case IDC_MATEDIT2_EMISS_INVISIBLE:			//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_EMISS_LAYER_COMBO:		//----------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							matedit->guiToMat();
						}
						break;
					case IDC_MATEDIT2_TXON_EMISS:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TX_EMISS:					//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_light, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_EMISS));
								mlay->use_tex_light = emcon->selected = mlay->tex_light.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
							}
						}
						break;
					case IDC_MATEDIT2_IES:						//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							CHECK(mlay);

							INT_PTR resIes = 0;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								resIes = DialogBoxParamW(hDllModule, 
										MAKEINTRESOURCEW(IDD_IES_PREVIEW2), hWnd, IES2DlgProc,(LPARAM)(mlay->ies));
							}
							else
							{
								resIes = DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_IES_PREVIEW2), hWnd, IES2DlgProc,(LPARAM)(mlay->ies));
							}
							if (resIes==0)	// OK, but no ies
							{
								if (mlay->ies)
								{
									mlay->ies->releaseStuff();
									delete mlay->ies;
									mlay->ies = NULL;
								}
							}
							else
								if (resIes==-1)	// Cancel
								{
								}
								else
								{	// OK, with ies
									if (mlay->ies)
									{
										mlay->ies->releaseStuff();
										delete mlay->ies;
									}
									mlay->ies = (IesNOX*)resIes;
								}
							matedit->layerCurrentToGUI();
							matedit->updateTexButtons();
						}
						break;
					case IDC_MATEDIT2_SSS:						//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							MatLayer * mlay = matedit->getEditedMatLayer();
							if (mlay->sssON)
							{
								mlay->roughness = 1.0f;
								mlay->use_tex_rough = false;
								mlay->transmOn = false;
								mlay->fakeGlass = false;
								mlay->dispersionOn = false;
								mlay->use_tex_rough = false;
								matedit->layerCurrentToGUI();
							}
							matedit->updateCurrentLayerPropsOnList();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_SSS_DENSITY:				//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_SSS_DIRECTION:			//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
						}
						break;


					case IDC_MATEDIT2_LAYERS_LIST:				//----------------------------------------------------------
						{
							CHECK(wmEvent==LV_SELECTION_CHANGED);
							matedit->layerCurrentToGUI();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_LAYER_ADD:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->layerAdd();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_LAYER_CLONE:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->layerClone();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_LAYER_REMOVE:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->layerDel();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_LAYER_RESET:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							mlay->resetValues(true);
							matedit->layerCurrentToGUI();
							matedit->updateCurrentLayerPropsOnList();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
					}
						break;
					case IDC_MATEDIT2_WEIGHT:					//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
							matedit->updateCurrentLayerPropsOnList();
						}
						break;
					case IDC_MATEDIT2_TXON_WEIGHT:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							matedit->updateCurrentLayerPropsOnList();
						}
						break;
					case IDC_MATEDIT2_TX_WEIGHT:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_weight, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_WEIGHT));
								mlay->use_tex_weight = emcon->selected = mlay->tex_weight.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
								matedit->updateCurrentLayerPropsOnList();
							}
						}
						break;
					case IDC_MATEDIT2_ROUGH:					//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
							matedit->updateCurrentLayerPropsOnList();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_REFL0:					//----------------------------------------------------------
						{
							EM2ColorShow * emc0  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_MATEDIT2_REFL0));
							if (wmEvent==BN_CLICKED)
							{
								EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, wmId));
								if (emc)
								{
									Color4 col = emc->color;
									Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));
									emc->isMouseOver = false;
									emc->isClicked = false;
									InvalidateRect(emc->hwnd, NULL, false);
									if (res)
									{
										MatLayer * mlay = matedit->getEditedMatLayer();
										CHECK(mlay);
										mlay->refl0 = *res;
										emc->redraw(*res);
										if (mlay->connect90)
										{
											EM2ColorShow * emc2 = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_MATEDIT2_REFL90));
											emc2->redraw(*res);
										}
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								MatLayer * mlay = matedit->getEditedMatLayer();
								mlay->refl0 = emc0->color;
								matedit->layerCurrentToGUI();
							}
						}
						break;
					case IDC_MATEDIT2_REFL90:					//----------------------------------------------------------
						{
							EM2ColorShow * emc0  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_MATEDIT2_REFL0));
							EM2ColorShow * emc90 = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_MATEDIT2_REFL90));
							if (wmEvent==BN_CLICKED)
							{
								EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, wmId));
								if (emc)
								{
									Color4 col = emc->color;
									Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));
									emc->isMouseOver = false;
									emc->isClicked = false;
									InvalidateRect(emc->hwnd, NULL, false);
									if (res)
									{
										MatLayer * mlay = matedit->getEditedMatLayer();
										CHECK(mlay);
										if (mlay->connect90)
											mlay->refl0 = *res;
										else
											mlay->refl90 = *res;
										emc->redraw(*res);
										if (mlay->connect90)
										{
											EM2ColorShow * emc2 = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_MATEDIT2_REFL0));
											emc2->redraw(*res);
										}
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								MatLayer * mlay = matedit->getEditedMatLayer();
								if (mlay->connect90)
									mlay->refl0 = emc90->color;
								else
									mlay->refl90 = emc90->color;
								matedit->layerCurrentToGUI();
							}
						}
						break;
					case IDC_MATEDIT2_REFL_LINK:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcb = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_REFL_LINK));
							MatLayer * mlay = matedit->getEditedMatLayer();
							mlay->connect90 = emcb->selected;
							matedit->layerCurrentToGUI();
							InvalidateRect(hWnd, NULL, false);
						}
						break;
					case IDC_MATEDIT2_ANISOTROPY:				//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_ANISO_ANGLE:				//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TXON_ROUGH:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							matedit->updateCurrentLayerPropsOnList();
						}
						break;
					case IDC_MATEDIT2_TX_ROUGH:					//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_rough, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_ROUGH));
								mlay->use_tex_rough = emcon->selected = mlay->tex_rough.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
								matedit->updateCurrentLayerPropsOnList();
							}
						}
						break;
					case IDC_MATEDIT2_TXON_REFL0:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TX_REFL0:					//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_col0, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_REFL0));
								mlay->use_tex_col0 = emcon->selected = mlay->tex_col0.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
							}
						}
						break;
					case IDC_MATEDIT2_TXON_REFL90:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TX_REFL90:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_col90, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_REFL90));
								mlay->use_tex_col90 = emcon->selected = mlay->tex_col90.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
							}
						}
						break;
					case IDC_MATEDIT2_TXON_ANISO:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TX_ANISO:					//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_aniso, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_ANISO));
								mlay->use_tex_aniso = emcon->selected = mlay->tex_aniso.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
							}
						}
						break;
					case IDC_MATEDIT2_TXON_ANISO_ANGLE:			//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TX_ANISO_ANGLE:			//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							bool ok = matedit->runTextureDialog(&mlay->tex_aniso_angle, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_ANISO_ANGLE));
								mlay->use_tex_aniso_angle = emcon->selected = mlay->tex_aniso_angle.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
							}
						}
						break;


					case IDC_MATEDIT2_TRANSMISSION:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();

							MatLayer * mlay = matedit->getEditedMatLayer();
							if (!mlay->transmOn)
							{
								mlay->fakeGlass = false;
								mlay->dispersionOn = false;
								matedit->layerCurrentToGUI();
							}

							matedit->updateCurrentLayerPropsOnList();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_ABSORPTION:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_DISPERSION:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_FAKE_GLASS:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
							MatLayer * mlay = matedit->getEditedMatLayer();
							if (mlay->fakeGlass)
							{
								mlay->roughness = 0.0f;
								mlay->use_tex_rough = false;
								mlay->dispersionOn = false;
								mlay->use_tex_rough = false;
								matedit->layerCurrentToGUI();
							}
							matedit->updateControlLocks(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_MATEDIT2_TRANSMISSION_COLOR:		//----------------------------------------------------------
						{
							if (wmEvent==BN_CLICKED)
							{
								EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, wmId));
								if (emc)
								{
									Color4 col = emc->color;
									Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));
									emc->isMouseOver = false;
									emc->isClicked = false;
									InvalidateRect(emc->hwnd, NULL, false);
									if (res)
									{
										MatLayer * mlay = matedit->getEditedMatLayer();
										CHECK(mlay);
										mlay->transmColor = *res;
										emc->redraw(*res);
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								matedit->guiToLayerCurrent();
							}
						}
						break;
					case IDC_MATEDIT2_ABSORPTION_COLOR:			//----------------------------------------------------------
						{
							if (wmEvent==BN_CLICKED)
							{
								EM2ColorShow * emc = GetEM2ColorShowInstance(GetDlgItem(hWnd, wmId));
								if (emc)
								{
									Color4 col = emc->color;
									Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));
									emc->isMouseOver = false;
									emc->isClicked = false;
									InvalidateRect(emc->hwnd, NULL, false);
									if (res)
									{
										MatLayer * mlay = matedit->getEditedMatLayer();
										CHECK(mlay);
										mlay->absColor = *res;
										emc->redraw(*res);
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								matedit->guiToLayerCurrent();
							}
						}
						break;
					case IDC_MATEDIT2_ABSORPTION_DEPTH:			//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_DISPERSION_SIZE:			//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TXON_TRANSM:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToLayerCurrent();
						}
						break;
					case IDC_MATEDIT2_TX_TRANSM:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = matedit->getEditedMatLayer();
							
							bool ok = matedit->runTextureDialog(&mlay->tex_transm, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_TRANSM));
								mlay->use_tex_transm = emcon->selected = mlay->tex_transm.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
							}
						}
						break;
					case IDC_MATEDIT2_OPACITY:					//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToMat();
						}
						break;

					case IDC_MATEDIT2_TXON_OPACITY:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							matedit->guiToMat();
						}
						break;
					case IDC_MATEDIT2_TX_OPACITY:				//----------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MaterialNox * mat = matedit->getEditedMaterial();
							bool ok = matedit->runTextureDialog(&mat->tex_opacity, false);
							if (ok)
							{
								matedit->updateTexButtons();
								EM2CheckBox * emcon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT2_TXON_OPACITY));
								mat->use_tex_opacity = emcon->selected = mat->tex_opacity.isValid();
								InvalidateRect(emcon->hwnd, NULL, false);
							}
						}
						break;


					case IDC_MATEDIT2_IOR:						//----------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							matedit->guiToLayerCurrent();
						}
						break;
				}	// end switch
			}		// end WM_COMMAND
			break;
		case WM_LBUTTONDOWN:
			{
				SetFocus(0);
			}
			break;
		default:
				return 0;
			break;
	}

	return 1;
}

//----------------------------------------------------------------------
