#ifndef __MaterialArchiver
#define __MaterialArchiver

#include "raytracer.h"

class MatArchiver
{
public:
	bool archiveMaterial(MaterialNox * mat, char * filename);

};

class RenamedTextures
{
public:
	int layer;
	int type;
	char * filename;
	char * fullfilename;
	char * newfilename;

	RenamedTextures() {layer=0; type=0; filename=NULL; fullfilename=NULL, newfilename=NULL;}
};

char * checkDuplicates(char * newString, char * existingString, bool freePrevious = true);
bool copyTexFile(char * src, char * dst);



#endif
