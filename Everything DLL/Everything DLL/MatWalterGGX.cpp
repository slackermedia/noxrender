#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>

#define clamp(x,a,b)   max((a),min((x),(b)))

float getFresnelTemp(float IOR, float kos, bool goingIn);
float getFresnelTemp(float IOR, float kos);

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionWalterMicrofacetGGX(HitData & hd, float &probability)
{
	float alphag = getRough(hd.tU, hd.tV);
	bool not_done = true;
	while (not_done)
	{
		#ifdef RAND_PER_THREAD
			float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
		#else
			float a = ((float)rand()/(float)RAND_MAX);
		#endif
		float tanthetaH = alphag*sqrt(a/(1-a));
		float thetaH = atan(tanthetaH);
		float sinThetaH = sin(thetaH);
		float cosThetaH = cos(thetaH);

		Vector3d temp = Vector3d::random();
		Vector3d H_ = temp^hd.normal_shade;
		H_.normalize();
		Vector3d H = H_*sinThetaH + hd.normal_shade*cosThetaH;
		H.normalize();

		if (transmOn)
		{
			#ifdef RAND_PER_THREAD
				float b = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
			#else
				float b = ((float)rand()/(float)RAND_MAX);
			#endif

			float f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H), (hd.in*hd.normal_shade > 0) );

			if (b > f)
			{
				float ior;
				if (hd.in*hd.normal_shade > 0)
					ior = fresnel.IOR;
				else
					ior = 1.0f/fresnel.IOR;
				bool inner;
				Vector3d res2 = hd.in.refractSnell(H, ior, inner);
				if (inner)
					hd.setFlagInternalReflection();

				if (((res2*hd.normal_geom)*(hd.in*hd.normal_geom)>0)  &&  !inner)
					hd.setFlagInvalidRandom();

				hd.out = res2;
				probability = getProbabilityWalterMicrofacetGGX(hd);
				return res2;
			}
		}

		Vector3d res1 = hd.in.reflect(H);
		res1.normalize();
		if ((res1*hd.normal_geom)*(hd.in*hd.normal_geom) < 0)
		{
			not_done = false;
			hd.out = res1;
			hd.setFlagInvalidRandom();
		}
		else
		{
			not_done = false;
			hd.out = res1;
		}

	}

	probability = getProbabilityWalterMicrofacetGGX(hd);

	return hd.out;
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityWalterMicrofacetGGX2(HitData & hd)
{
	float alphag = getRough(hd.tU, hd.tV);
	Vector3d H = hd.in + hd.out;
	H.normalize();
	float cosThetaH = fabs(H*hd.normal_shade);
	float cos2ThetaH = cosThetaH * cosThetaH;
	float sin2ThetaH = (1-cos2ThetaH);
	float tan2ThetaH = sin2ThetaH/cos2ThetaH;

	float m = PI*cos2ThetaH*cos2ThetaH * pow((alphag*alphag + tan2ThetaH) , 2);
	float l = alphag*alphag;
	float D = l/m;
	float jacobian = 0.25f/fabs(hd.out * H);
	return D * cosThetaH * jacobian;
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityWalterMicrofacetGGX(HitData & hd)
{
	bool goingOut = (hd.in*hd.normal_shade < 0   &&   hd.out*hd.normal_shade > 0);
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);

	if (!transmOn)
	{
		if ((hd.in*hd.normal_geom)*(hd.out*hd.normal_geom)<=0)
			return 0.0f;
	}

	float alphag = getRough(hd.tU, hd.tV);
	float alphag2 = alphag * alphag;
	Vector3d H;
	if (sameSide)
	{
		H = hd.in + hd.out;
		H.normalize();	
		if (H*hd.normal_shade < 0)
			H.invert();
	}
	else
	{
		if (goingOut)
			H = hd.in * fresnel.IOR + hd.out;
		else
			H = hd.in + hd.out * fresnel.IOR;
		H.normalize();
		H *= -1;
		if (H*hd.normal_shade < 0)
			H.invert();
	}

	float hMulIn = H*hd.in;
	float hMulOut = H*hd.out;
	float hhio = hMulIn*hMulOut;
	if ( hhio > 0   &&  !sameSide)
		return 0;
	if ( hhio < 0   &&  sameSide)
		return 0;

	float cosThetaH = fabs(H*hd.normal_shade);
	float cos2ThetaH = cosThetaH * cosThetaH;
	float sin2ThetaH = (1-cos2ThetaH);
	float tan2ThetaH = sin2ThetaH/cos2ThetaH;
	float m = PI*cos2ThetaH*cos2ThetaH * pow((alphag2+ tan2ThetaH) , 2);
	float l = alphag2;
	float D = l/m;
	float f = 1.0f;

	if (transmOn)
	{
		f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H), (hd.in*hd.normal_shade > 0) );
		if (!sameSide)
			f = 1.0f - f;
	}

	float jacobian = 1;
	if (transmOn && !sameSide)
		if (goingOut)
			jacobian = fabs(hd.out*H)/pow((hd.in*H * fresnel.IOR + (hd.out*H)),2);
		else
			jacobian = fresnel.IOR*fresnel.IOR*fabs(hd.out*H)/pow((hd.in*H + fresnel.IOR*(hd.out*H)), 2);
	else
		jacobian = 0.25f/fabs(hd.out * H);
	return D*cosThetaH*jacobian*f;		// jacobian: H->dirOut
	return max(0.001f, D*cosThetaH*jacobian);
}

//-------------------------------------------------------------------------------------------

Color4 MatLayer::getBRDFWalterMicrofacetGGX(HitData & hd)
{
	bool goingOut = (hd.in*hd.normal_shade < 0   &&   hd.out*hd.normal_shade > 0);
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);

	if (!transmOn)
	{
		if ((hd.in*hd.normal_geom)*(hd.out*hd.normal_geom)<=0)
			return Color4(0,0,0);
	}

	float alphag = getRough(hd.tU, hd.tV);
	Vector3d H = hd.in + hd.out;
	H.normalize();
	if (!sameSide)
	{
		if (goingOut)
			H = hd.in*fresnel.IOR + hd.out;
		else
			H = hd.in + hd.out * fresnel.IOR;
		H.normalize();
		if (H*hd.normal_shade < 0)
			H.invert();
	}

	if ( (H*hd.in) * (H*hd.out) > 0   &&  !sameSide)
		return Color4(0,0,0);
	if ( (H*hd.in) * (H*hd.out) < 0   &&  sameSide)
		return Color4(0,0,0);

	float cosThetaH = fabs(H*hd.normal_shade);
	float cos2ThetaH = cosThetaH * cosThetaH;
	float sin2ThetaH = (1-cos2ThetaH);
	float tan2ThetaH = sin2ThetaH/cos2ThetaH;
	float pp = alphag*alphag + tan2ThetaH;
	float m = PI*cos2ThetaH*cos2ThetaH	 * pp*pp;
	float l = alphag*alphag;
	float D = l/m;

	float cosTheta_i = fabs(hd.in  * hd.normal_shade);
	float cosTheta_o = fabs(hd.out * hd.normal_shade);
	cosTheta_i = min(1, cosTheta_i);
	cosTheta_o = min(1, cosTheta_o);
	float tan2Theta_i = (1-cosTheta_i*cosTheta_i)/(cosTheta_i*cosTheta_i);
	float tan2Theta_o = (1-cosTheta_o*cosTheta_o)/(cosTheta_o*cosTheta_o);

	//G_i
	float kk,m1,G_i,G_o;
	kk  = (H*hd.in/(hd.in*hd.normal_shade)>0 ? 1.0f : 0.0f);
	kk  = 1;
	m1  = 1+sqrt(1+alphag*alphag*tan2Theta_i);
	G_i = kk * 2.0f/m1;

	//G_o
	kk  = (H*hd.out/(hd.out*hd.normal_shade)>0 ? 1.0f : 0.0f);
	kk  = 1;
	m1  = 1+sqrt(1+alphag*alphag*tan2Theta_o);
	G_o = kk * 2.0f/m1;

	float f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H), (hd.in*hd.normal_shade > 0) );

	float FF = f;
	if (transmOn)
	{
		if (sameSide)
			FF = f;
		else
		{
			FF = 1-f;
			Color4 absorption = Color4(1,1,1);
			if ((hd.in*hd.normal_shade < 0)   &&   (absOn))
			{	// going out of object - absorb part of energy
				absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
				absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
				absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
			}
			float brdf;
			if (goingOut)
				brdf = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
						FF*G_i*G_o*D/(pow((hd.in*H) * fresnel.IOR + (hd.out*H),2));
			else
				brdf = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
						fresnel.IOR*fresnel.IOR*FF*G_i*G_o*D/(pow((hd.in*H) + fresnel.IOR * (hd.out*H),2));
			return getTransmissionColor(hd.tU, hd.tV)*brdf*absorption;
		}
	}

	if (!sameSide)
		return Color4(0,0,0);

	Color4 absorption = Color4(1,1,1);
	if (transmOn)
	{
		if ((hd.in*hd.normal_shade < 0)   &&   (absOn))
		{	// internal reflection - absorb part of energy
			absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
			absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
			absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
		}
	}

	float brdf = G_i*G_o * D * FF * 0.25f / cosTheta_i / cosTheta_o;

	float nthI = fabs(FastMath::acos(cosTheta_i))/PI*2.0f;
	float nthO = fabs(FastMath::acos(cosTheta_o))/PI*2.0f;

	return (getColor0(hd.tU, hd.tV)*(1-nthI) + (getColor90(hd.tU, hd.tV)*(1-alphag)+getColor0(hd.tU, hd.tV)*alphag)*nthI) * absorption * brdf;
}

//-------------------------------------------------------------------------------------------
#define EQUALS_ROUND_SUB(a, b, delta) (fabs((a)-(b))<(delta))
#define EQUALS_ROUND_DIV(a, b, delta) (fabs((a)/(b)-1)<(delta))
#define EQUALS_ROUND(a, b, delta) EQUALS_ROUND_SUB(a, b, delta) && EQUALS_ROUND_DIV(a, b, delta)

bool MatLayer::getBRDFandPDFsGGX(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b)
{
	bool goingOut = (hd.in*hd.normal_shade < 0   &&   hd.out*hd.normal_shade > 0);
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);

	float cosThetaInShade  = hd.normal_shade*hd.in;
	float cosThetaOutShade = hd.normal_shade*hd.out;
	float cosThetaInGeom   = hd.normal_geom *hd.in;
	float cosThetaOutGeom  = hd.normal_geom *hd.out;

	if ( (!transmOn  &&  cosThetaInGeom*cosThetaOutGeom<=0)  ||  cosThetaInShade==0  ||  cosThetaOutShade==0)
	{
		brdf = Color4(0.0f, 0.0f, 0.0f);
		pdf_f = pdf_b = 0.0f;
	}
	else	// most probably ok
	{
		float alphag = getRough(hd.tU, hd.tV);
		float alphag2 = alphag * alphag;
		Vector3d H;
		if (sameSide)
			H = hd.in + hd.out;
		else
			if (goingOut)
				H = hd.in * fresnel.IOR + hd.out;
			else
				H = hd.in + hd.out * fresnel.IOR;
		H.normalize();	
		if (H*hd.normal_shade < 0)
			H.invert();

		float hMulIn = H*hd.in;
		float hMulOut = H*hd.out;
		float hhio = hMulIn*hMulOut;
		if ( (hhio>0  &&  !sameSide)  ||  (hhio<0  &&  sameSide))
		{
			brdf = Color4(0.0f, 0.0f, 0.0f);
			pdf_f = pdf_b = 0.0f;
		}
		else
		{
			float cosThetaH = fabs(H*hd.normal_shade);
			float cos2ThetaH = cosThetaH * cosThetaH;
			float sin2ThetaH = (1-cos2ThetaH);
			float tan2ThetaH = sin2ThetaH/cos2ThetaH;
			float pp = alphag2 + tan2ThetaH;
			float m = PI*cos2ThetaH*cos2ThetaH * pp*pp;
			float l = alphag2;
			float D = l/m;
			float jacobian_f = 1.0f, jacobian_b = 1.0f;
 
			if (transmOn && !sameSide)
			{
				if (goingOut)
				{
					jacobian_f = fabs(hd.out*H)/pow((hd.in*H * fresnel.IOR + (hd.out*H)),2);
					jacobian_b = fresnel.IOR*fresnel.IOR*fabs(hd.in*H)/pow((hd.out*H + fresnel.IOR*(hd.in*H)), 2);
				}
				else
				{
					jacobian_f = fresnel.IOR*fresnel.IOR*fabs(hd.out*H)/pow((hd.in*H + fresnel.IOR*(hd.out*H)), 2);
					jacobian_b = fabs(hd.in*H)/pow((hd.out*H * fresnel.IOR + (hd.in*H)),2);
				}
			}
			else
			{
				jacobian_f = 0.25f/fabs(hd.out * H);
				jacobian_b = 0.25f/fabs(hd.in * H);
 			}

			float f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H), (hd.in*hd.normal_shade > 0) );
			float FT = 1.0f;
			if (transmOn)
				FT = sameSide ? f : 1.0f - f;

			pdf_f = D * cosThetaH * jacobian_f * FT;
			pdf_b = D * cosThetaH * jacobian_b * FT;

			float cosTheta_i = min(1, fabs(hd.in  * hd.normal_shade));
			float cosTheta_o = min(1, fabs(hd.out * hd.normal_shade));
			float tan2Theta_i = (1-cosTheta_i*cosTheta_i)/(cosTheta_i*cosTheta_i);
			float tan2Theta_o = (1-cosTheta_o*cosTheta_o)/(cosTheta_o*cosTheta_o);

			//G_i
			float kk,m1,G_i,G_o;
			kk  = (H*hd.in/(hd.in*hd.normal_shade)>0 ? 1.0f : 0.0f);
			kk  = 1;
			m1  = 1+sqrt(1+alphag*alphag*tan2Theta_i);
			G_i = kk * 2.0f/m1;
			//G_o
			kk  = (H*hd.out/(hd.out*hd.normal_shade)>0 ? 1.0f : 0.0f);
			kk  = 1;
			m1  = 1+sqrt(1+alphag*alphag*tan2Theta_o);
			G_o = kk * 2.0f/m1;

			Color4 absorption = Color4(1,1,1);
			if (transmOn  &&  absOn  &&  (hd.in*hd.normal_shade<0))	// warning: non-symmetric
			{	// internal reflection or transmission going out - absorb part of energy
				absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
				absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
				absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
			}

			if (transmOn && !sameSide)
			{
				float brdfm;
				if (goingOut)
				{
					brdfm = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
							(1-f)*G_i*G_o*D/(pow((hd.in*H) * fresnel.IOR + (hd.out*H),2));
				}
				else
				{
					brdfm = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
							fresnel.IOR*fresnel.IOR*(1-f)*G_i*G_o*D/(pow((hd.in*H) + fresnel.IOR * (hd.out*H),2));
				}
				brdf = getTransmissionColor(hd.tU, hd.tV)*brdfm*absorption;
			}
			else
			{
				float brdfm = G_i*G_o * D * f * 0.25f / cosTheta_i / cosTheta_o;
				float nthI = fabs(FastMath::acos(cosTheta_i))/PI*2.0f;
				float nthO = fabs(FastMath::acos(cosTheta_o))/PI*2.0f;
				brdf = (getColor0(hd.tU, hd.tV)*(1-nthI) + (getColor90(hd.tU, hd.tV)*(1-alphag)+getColor0(hd.tU, hd.tV)*alphag)*nthI) * absorption * brdfm;
			}

			if (brdf.isNaN())
			{
				char buf[256];
				sprintf_s(buf, 256, "brdf nan... G_i=%f  G_o=%f  D=%f  f=%f  cosTheta_i=%f  cosTheta_o=%f", G_i, G_o, D, f, cosTheta_i, cosTheta_o);
				Logger::add(buf);
			}

			brdf *= cosThetaH;

		}
	}


	#ifdef VERIFY_MATLAYER_BRDF_PDFS
		Color4 vbrdf = getBRDFWalterMicrofacetGGX(hd);
		float vpdf_f = getProbabilityWalterMicrofacetGGX(hd);
		HitData hdback = hd;
		hdback.swapDirections();
		float vpdf_b = getProbabilityWalterMicrofacetGGX(hdback);
		if (!EQUALS_ROUND(vbrdf.r, brdf.r, 0.05f)  ||  !EQUALS_ROUND(vbrdf.r, brdf.r, 0.05f)  ||  !EQUALS_ROUND(vbrdf.r, brdf.r, 0.05f))
			Logger::add("brdf verify failed on ggx");
		if (!EQUALS_ROUND(vpdf_f, pdf_f, 0.005f))
		{
			char buf[256];
			sprintf_s(buf, 256, "pdf forward verify failed on ggx.. cos_in=%f  cos_out=%f  fpdf_new=%f  fpdf_old=%f", cosThetaInShade, cosThetaOutShade, pdf_f, vpdf_f);
			Logger::add(buf);
		}
		if (!EQUALS_ROUND(vpdf_b, pdf_b, 0.005f))
		{
			char buf[256];
			sprintf_s(buf, 256, "pdf backward verify failed on ggx.. cos_in=%f  cos_out=%f  bpdf_new=%f  bpdf_old=%f", cosThetaInShade, cosThetaOutShade, pdf_b, vpdf_b);
			Logger::add(buf);
		}

	#endif

	return true;
}

//-------------------------------------------------------------------------------------------
