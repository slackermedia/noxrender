#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

#define DEBUG_STATS
//#define DEBUG_LOG2

//--------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::mutateLens()
{
	#ifdef DEBUG_LOG2
		Logger::add("lens mutation");
	#endif

	float x,y;
	cam->getHaltonCoords(x, y);
	pCX = x;
	pCY = y;

	MltSeed seed(ss, cam);
	bool seedok = seed.createSeed(x,y, 1);
	if (!seedok)
	{
		sumprobes++;
		return false;
	}
	copySeedToCandidate(&seed);

	float w = seed.weight;
	sumweights += w;
	sumprobes++;
	weightBias = (float)(sumweights/(double)sumprobes);

	int a = seed.numE + seed.numL;
	lprobes[a]++;
	lweights[a] += seed.weight;

	if (sumprobes%10000 == 0)
	{
		float weightBias2 = 0;
		for (int i=0; i<MLT_MAX_VERTICES*2; i++)
		{
			if (lprobes[i]>0)
				weightBias2 += (float)(lweights[i]/lprobes[i]);
		}

		float weightBias3 = 0;
		long long maxprobes = 0;
		long long tempsumprobes = 0;
		for (int i=0; i<MLT_MAX_VERTICES*2; i++)
		{
			maxprobes = max(maxprobes, lprobes[i]);
			tempsumprobes += lprobes[i];
		}
		weightBias3 = (float)(sumweights / ((double)maxprobes*sumprobes/tempsumprobes));

		weightBias2 *= (float)((double)sumprobes/(double)tempsumprobes);
	}

	if (numCEP==0)
	{
		Vector3d camDir = lCandidate[numCLP].pos - eCandidate[0].pos;
		camDir.normalize();
		float cpx, cpy;
		if (!cam->getCoordsFromDir(camDir, eCandidate[0].pos, cpx, cpy))
			return false;
		if (cpx < 0 ||  cpx >= cam->width  ||  cpy<0  ||  cpy>=cam->height)
			return false;
		pCX = (float)cpx;
		pCY = (float)cpy;
	}

	isPathValid(true);

	return true;
}

//--------------------------------------------------------------------------------------------------------------------------------------

float Metropolis::tentativeTransitionForLensSunsky(bool forward)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (!ss->useSunSky)
		return 0.0f;

	int nL, nE, lTri;
	MLTVertex * ePtr;
	MLTVertex * lPtr;
	bool useSun;
	bool useSky;
	if (forward)
	{
		lTri = clTriID;
		ePtr = eCandidate;
		lPtr = lCandidate;
		nL = numCLP;
		nE = numCEP;
		useSun = cSourceSun;
		useSky = cSourceSky;
	}
	else
	{
		lTri = lTriID;
		ePtr = ePath;
		lPtr = lPath;
		nL = numLP;
		nE = numEP;
		useSun = sourceSun;
		useSky = sourceSky;
	}
	int nA = nL+nE+2;

	if (!useSky  &&  !useSun)
		return 0;

	Color4 gbrdf;
	if (useSun)
	{
		HitData hd;
		hd.in = ePtr[nE].inDir;
		hd.out = ePtr[nE].outDir;
		hd.normal_shade = ePtr[nE].normal;
		ePtr[nE].tri->evalTexUV(ePtr[nE].u, ePtr[nE].v, hd.tU, hd.tV);
		hd.normal_geom = (ePtr[nE].inst_mat.getMatrixForNormals() * ePtr[nE].tri->normal_geom).getNormalized();
		hd.adjustNormal();
		ePtr[nE].tri->getTangentSpaceSmooth(ePtr[nE].inst_mat, ePtr[nE].u, ePtr[nE].v, hd.dir_U, hd.dir_V);

		float rough = ePtr[nE].rough;
		if (rough > 0)
			gbrdf = ss->sunsky->getSunColor() * ss->sunsky->getSunSizeSteradians();
		else
		{
			float pdf = ePtr[nE].mlay->getProbability(hd);

			if (pdf < 0.001f)
				return 0;
			gbrdf = ss->sunsky->getSunColor() *(1.0f/pdf);
		}
	}
	else
		gbrdf = ss->sunsky->getColor(ePtr[nE].outDir);

	bool dispersionUsed = false;

	for (int i=1; i<=nE; i++)
	{
		HitData hd;
		hd.in = ePtr[i].inDir;
		hd.out = ePtr[i].outDir;
		hd.normal_shade = ePtr[i].normal;
		hd.lastDist = (ePtr[i].pos-ePtr[i-1].pos).length();
		ePtr[i].tri->evalTexUV(ePtr[i].u, ePtr[i].v,   hd.tU, hd.tV);
		hd.normal_geom = (ePtr[i].inst_mat.getMatrixForNormals() * ePtr[i].tri->normal_geom).getNormalized();
		hd.adjustNormal();
		ePtr[i].tri->getTangentSpaceSmooth(ePtr[i].inst_mat, ePtr[i].u, ePtr[i].v, hd.dir_U, hd.dir_V);

		Color4 rbrdf = ePtr[i].mlay->getBRDF(hd)   * ePtr[i].mat->weightsSumShade*0.01f;
		float rpdf = ePtr[i].mlay->getProbability(hd);

		if (ePtr[i].mlay->transmOn   &&   ePtr[i].mlay->dispersionOn  &&   ePtr[i].mlay->dispersionValue!=0.0f   &&   !ePtr[i].mlay->fakeGlass   &&   ePtr[i].rough<0.0001f)
		{
			if ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) < 0.0f)
				dispersionUsed = true;
		}

		if (rpdf < 0.0001f)
			return 0;

		if (useSun  &&  i==nE)
			rpdf = 1;

		float rough = ePtr[i].rough;
		if (rough > 0)
			rpdf *= 0.5f;	// russian roulette

		gbrdf *= rbrdf * (1.0f/rpdf);
		if (rough > 0)
			gbrdf *= fabs(ePtr[i].normal*ePtr[i].outDir);
	}

	if (dispersionUsed)
		gbrdf *= 4.0f;

	float R = ( 0.299f * gbrdf.r  +  0.587f * gbrdf.g  +  0.114f * gbrdf.b );
	return R;

}


float Metropolis::tentativeTransitionForLens(bool forward)
{
	float rrProb = 0.5f;
	#ifdef DEBUG_LOG2
		Logger::add("lens transition start");
	#endif

	int nL, nE, lTri;
	MLTVertex * ePtr;
	MLTVertex * lPtr;
	if (forward)
	{
		lTri = clTriID;
		ePtr = eCandidate;
		lPtr = lCandidate;
		nL = numCLP;
		nE = numCEP;
	}
	else
	{
		lTri = lTriID;
		ePtr = ePath;
		lPtr = lPath;
		nL = numLP;
		nE = numEP;
	}

	int nA = nL+nE+2;

	if (lPtr[0].tri == NULL)
		return tentativeTransitionForLensSunsky(forward);

	PMLTVertex mPtr[MLT_MAX_VERTICES*2];
	float G[MLT_MAX_VERTICES*2];
	float pLE[MLT_MAX_VERTICES*2];
	float pEL[MLT_MAX_VERTICES*2];
	Color4 brdf[MLT_MAX_VERTICES*2];
	double pST[MLT_MAX_VERTICES*2];
	float weight[MLT_MAX_VERTICES*2];

	int t=0;
	for (int i=0; i<=nL; i++)
		mPtr[t++] = &(lPtr[i]);
	for (int i=nE; i>=0; i--)
		mPtr[t++] = &(ePtr[i]);

	// join directions 
	Point3d p1, p2;
	p1 = ePtr[nE].pos;
	p2 = lPtr[nL].pos;
	Vector3d dirEL = p2-p1;
	dirEL.normalize();
	Vector3d dirLE = dirEL * -1;
	ePtr[nE].outDir = dirEL;
	lPtr[nL].outDir = dirLE;
	ePtr[nE].outCosine = ePtr[nE].outDir * ePtr[nE].normal;
	lPtr[nL].outCosine = lPtr[nL].outDir * lPtr[nL].normal;

	float pLight = ss->evalPDF(lTri);

	Color4 fakeAndOpacityAttenuation = Color4(1,1,1);
	if (forward)
	{
		fakeAndOpacityAttenuation = connectionAttenuationCandidate;
		for (int i=1; i<=numCEP; i++)
			fakeAndOpacityAttenuation *= eCandidate[i].atten;
		for (int i=1; i<=numCLP; i++)
			fakeAndOpacityAttenuation *= lCandidate[i].atten;
	}
	else
	{
		fakeAndOpacityAttenuation = connectionAttenuation;
		for (int i=1; i<=numEP; i++)
			fakeAndOpacityAttenuation *= ePath[i].atten;
		for (int i=1; i<=numLP; i++)
			fakeAndOpacityAttenuation *= lPath[i].atten;
	}

	// eval G
	for (int i=0; i<nA-1; i++)
	{
		Vector3d v1 = mPtr[i+1]->pos - mPtr[i]->pos;
		float d2 = v1.lengthSqr();
		if (d2 < 0.00000001f)
			return 0;
		float k1,k2;
		if (i<=nL)
			k1 = mPtr[i]->outDir  * mPtr[i]->normal;
		else
			k1 = mPtr[i]->inDir  * mPtr[i]->normal;
		if (i<nL)
			k2 = mPtr[i+1]->inDir * mPtr[i+1]->normal;
		else
			k2 = mPtr[i+1]->outDir * mPtr[i+1]->normal;
		if (d2 > 0.00000001f)
			G[i] = fabs(k1*k2)/d2;
		else
			return 0;
	}

	bool dispersionUsed = false;

	// eval pLE & pEL & brdf
	for (int i=0; i<nA-1; i++)
	{
		HitData hd;
		float uu = mPtr[i]->u;
		float vv = mPtr[i]->v;
		if (i<=nL)
		{
			hd.in  = mPtr[i]->inDir;
			hd.out = mPtr[i]->outDir;
		}
		else
		{
			hd.in  = mPtr[i]->outDir;
			hd.out = mPtr[i]->inDir;
		}

		hd.normal_shade = mPtr[i]->normal;
		mPtr[i]->tri->evalTexUV(uu, vv, hd.tU, hd.tV);
		hd.clearFlagsAll();
		hd.lastDist = 1;
		hd.normal_geom = (mPtr[i]->inst_mat.getMatrixForNormals() * mPtr[i]->tri->normal_geom).getNormalized();
		hd.adjustNormal();
		mPtr[i]->tri->getTangentSpaceSmooth(mPtr[i]->inst_mat, mPtr[i]->u, mPtr[i]->v, hd.dir_U, hd.dir_V);
		if (i>0)
			hd.lastDist = (mPtr[i]->pos - mPtr[i-1]->pos).length();
		pLE[i] = 0.0f;
		pEL[i] = 0.0f;
		brdf[i] = Color4(1,1,1);

		if (mPtr[i]->mlay)
		{
			float rough = mPtr[i]->rough;
			float tcos = fabs(hd.out*hd.normal_shade);
			if (tcos > 0)
			{
				pLE[i] = mPtr[i]->mlay->getProbability(hd);
				if (rough > 0)
					pLE[i] *= 0.5f;		// russian roulette
			}
			else
				pLE[i] = 0;
			brdf[i] = mPtr[i]->mlay->getBRDF(hd);		// L->x->E

			Vector3d tvec = hd.in;
			hd.in = hd.out;
			hd.out = tvec;

			tcos = fabs(hd.out*hd.normal_shade);
			if (tcos > 0)
			{
				pEL[i] = mPtr[i]->mlay->getProbability(hd);
				if (rough > 0)
					pEL[i] *= 0.5f;		// russian roulette
			}
			else
				pEL[i] = 0;

			if (mPtr[i]->mlay->dispersionOn   &&   mPtr[i]->mlay->dispersionValue!=0   &&   mPtr[i]->mlay->transmOn   &&   mPtr[i]->rough<0.0001f   &&   !mPtr[i]->mlay->fakeGlass)
			{
				if ((mPtr[i]->inDir*mPtr[i]->normal) * (mPtr[i]->outDir*mPtr[i]->normal)<0)
					dispersionUsed = true;
			}
		}
	}

	float tlu, tlv;
	mPtr[0]->tri->evalTexUV(mPtr[0]->u, mPtr[0]->v,  tlu, tlv);
	brdf[0] = mPtr[0]->mlay->getLightColor(tlu, tlv) * mPtr[0]->mlay->getIlluminance(ss, lTri);
	pEL[0] = 1;
	pEL[nA-1] = 1;
	pLE[nA-1] = 1;

	// eval probabilities for whole paths
	for (int i=0; i<nA-1; i++)
	{
		pST[i] = pLight;
		pST[i] *= 0.5f;		// russian roulette
		if (mPtr[i]->mlay)
		{
			float rough = mPtr[i]->rough;
			if (rough==0)
				pST[i] = 0.0f;
		}
		if (mPtr[i+1]->mlay)
		{
			float rough = mPtr[i+1]->rough;
			if (rough==0)
				pST[i] = 0.0f;
		}
		for (int j=0; j<i; j++)
			pST[i] *= pLE[j] / fabs(j<=nL?mPtr[j]->outCosine:mPtr[j]->inCosine) * G[j];		// projected solid angle pdf * G
		for (int j=nA-2; j>i+1; j--)
			pST[i] *= pEL[j] / fabs(j<=nL?mPtr[j]->inCosine:mPtr[j]->outCosine) * G[j-1];	// projected solid angle pdf * G
	}

	// and implicit E->...->L
	double implicitPdf = 1.0f;
	for (int j=nA-2; j>0; j--)
		implicitPdf *= pEL[j] / fabs(j<=nL?mPtr[j]->inCosine:mPtr[j]->outCosine) * G[j-1];	// projected solid angle pdf * G

	double sumPdf = implicitPdf*implicitPdf;
	for (int i=0; i<nA-2; i++)
		sumPdf += pST[i]*pST[i];

	if (sumPdf <= 0.0f)
		return 0.0f;
	
	// now eval weights - power heuristic
	for (int i=0; i<nA-2; i++)
		weight[i] = (float)(pST[i]*pST[i] / sumPdf);

	Color4 cBrdf = Color4(0,0,0);
	for (int i=0; i<nA-2; i++)
	{
		Color4 pBrdf = Color4(1,1,1) * (1.0f/pLight);
		for (int j=0; j<i; j++)
		{
			if (pLE[j] > 0.00001f)
			{
				float rough = mPtr[j]->rough;
				if (mPtr[j]->mlay  &&  (rough > 0  ||  mPtr[j]->mlay->type==MatLayer::TYPE_EMITTER))
					pBrdf *= brdf[j] * fabs(j<=nL?mPtr[j]->outCosine:mPtr[j]->inCosine) * (1.0f/pLE[j]);
				else
					pBrdf *= brdf[j] * (1.0f/pLE[j]);
			}
			else
				pBrdf = Color4(0,0,0);
		}
		
		for (int j=nA-2; j>i+1; j--)
		{
			if (pEL[j] > 0.00001f)
			{
				float rough = mPtr[j]->rough;
				if (mPtr[j]->mlay  &&  (rough > 0  ||  mPtr[j]->mlay->type==MatLayer::TYPE_EMITTER))
					pBrdf *= brdf[j] * fabs(j<=nL?mPtr[j]->inCosine:mPtr[j]->outCosine) * (1.0f/pEL[j]);
				else
					pBrdf *= brdf[j] * (1.0f/pEL[j]);
			}
			else
				pBrdf = Color4(0,0,0);
		}
		
		pBrdf *= brdf[i] * brdf[i+1] * G[i];
		pBrdf *= 2;		// russian roulette
		cBrdf += pBrdf * weight[i];
	}

	// add implicit path contribution
	Color4 implBrdf = brdf[0];
	for (int i=nA-2; i>0; i--)
	{
		float rough = mPtr[i]->rough;
		if (mPtr[i]->mlay  &&  (rough > 0  ||  mPtr[i]->mlay->type==MatLayer::TYPE_EMITTER))
			implBrdf *= brdf[i] * fabs(i<=nL?mPtr[i]->inCosine:mPtr[i]->outCosine) * (1.0f/pEL[i]);
		else
			implBrdf *= brdf[i] * (1.0f/pEL[i]);
	}
	cBrdf += implBrdf * (float)(implicitPdf*implicitPdf/sumPdf);

	// check for errors
	if (cBrdf.isInf()  ||  cBrdf.isInf())
		return 0;
	if (cBrdf.isBlack())
		return 0;

	cBrdf *= fakeAndOpacityAttenuation;

	float R = ( 0.299f * cBrdf.r  +  0.587f * cBrdf.g  +  0.114f * cBrdf.b );

	if (dispersionUsed)
		R *= 4.0f;

	return R;
}
