#include "Colors.h"

// Half floats are not used anymore

HalfFloat::HalfFloat()
{
	float f = 0.0f;
	unsigned int a = *((int*)(&f));
	w = ( (a>>16) & 0x8000 )   |   ((((a & 0x7f800000) - 0x38000000) >> 13 ) & 0x7c00)    |    ( (a>>13) & 0x03ff);
}

HalfFloat::HalfFloat(float &fsrc)
{
	unsigned int a = *((int*)(&fsrc));
	int e = (a>>23) & 0x000000FF;
	e -= 127;
	if (e < -24)
		w = ((a>>16)&0x8000);		// map to zero
	else
	{
		if (e<-14)
		{	// map to denormal
			WORD s1 = (a>>16) & 0x8000;
			WORD m1 = (a>>13) & 0x03ff;
			m1 |= 0x00800000>>13;
			while (e<-14)
			{
				e++;
				m1 >>= 1;
			}
			w = s1 | m1;
		}
		else
		{
			if (e <= 15)
				// normal conversion
				w = ( (a>>16) & 0x8000 )   |   ((((a & 0x7f800000) - 0x38000000) >> 13 ) & 0x7c00)    |    ( (a>>13) & 0x03ff);
			else
			{
				if (e<128)
					w = ((a>>16)&0x8000) | 0x7C00;				// big values map to Inf
				else
					if (a & 0x3FFFFF)
						w = ((a>>16)&0x8000) | 0x7FFF;			// NaN
					else
						w = ((a>>16)&0x8000) | 0x7C00;			// Inf
			}
		}
	}
}

HalfFloat::~HalfFloat()
{
}

float HalfFloat::toFloat()
{
	WORD n = (w & 0x8000);
    WORD e = (w >> 10) & 0x001f;
    WORD m =  w & 0x3ff;

	if (e == 0)
	{
		if (m == 0)
		{
			if (n)
				return -0.0f;
			else
				return 0.0f;
		}
		else
		{	// denormal
			DWORD m1 = m<<13;						// Zero pad mantissa bits
			DWORD e1 = 0;
			while(!(m1&0x00800000))
			{										// While not normalized
				e1 -= 0x00800000;					// Decrement exponent (1<<23)
				m1 <<= 1;							// Shift mantissa
			}
			m1 &= ~0x00800000;						// Clear leading 1 bit
			e1  +=  0x38800000;						// Adjust bias ((127-14)<<23)
			DWORD a = (n<<16) | m1 | e1;			// Combine
			float f = *( (float*)(&a) );			// change to float
			return f;
		}
	}
	else
	{
		if (e == 31)
		{
			if (m == 0)	
			{	// infinity
				DWORD res = (n<<16)|0x7F800000;
				return *(float*)(&res);
			}
			else
			{	// NaN
				DWORD res = (n<<16)|0x7FFFFFFF;
				return *(float*)(&res);
			}
		}
		else
		{
		}
	}

	DWORD a = ((w&0x8000)<<16) | (((w&0x7c00)+0x1C000)<<13) | ((w&0x03FF)<<13);
	float f = *( (float*)(&a) );
	return f;
}

HalfFloat HalfFloat::fromFloat(float &src)
{
	return HalfFloat(src);
}


bool HalfFloat::isInf()
{
    WORD e = (w >> 10) & 0x001f;
    WORD m =  w & 0x3ff;
    return e == 31 && m == 0;
}

bool HalfFloat::isInfPlus()
{
	WORD n = (w & 0x8000);
    WORD e = (w >> 10) & 0x001f;
    WORD m =  w & 0x3ff;
    return e == 31   &&   m == 0   &&   n == 0;
}

bool HalfFloat::isInfMinus()
{
	WORD n = (w & 0x8000);
    WORD e = (w >> 10) & 0x001f;
    WORD m =  w & 0x3ff;
    return e == 31   &&   m == 0   &&   n == 1;
}

bool HalfFloat::isNaN()
{
    WORD e = (w >> 10) & 0x001f;
    WORD m =  w & 0x3ff;
    return e == 31 && m != 0;
}

bool HalfFloat::isZero()
{
	return (w & 0x7fff) == 0;
}

bool HalfFloat::isDenorm()
{
    WORD e = (w >> 10) & 0x001f;
    WORD m =  w & 0x3ff;
    return e == 0 && m != 0;
}


Color4 ColorHalf4::getColor4()
{
	return Color4(r.toFloat(), g.toFloat(), b.toFloat(), a.toFloat());
}

