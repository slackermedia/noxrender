#include "XML_IO.h"
#include "DLL.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

//------------------------------------------------------------------------------------------------------

XMLlocalMatInfo::XMLlocalMatInfo()
{
	xmlDoc = NULL;
	errLine = -1;
}

//------------------------------------------------------------------------------------------------------

XMLlocalMatInfo::~XMLlocalMatInfo()
{
	if (info.name)
		free(info.name);
	info.name = NULL;
	info.preview.freeBuffer();
}

//------------------------------------------------------------------------------------------------------

bool XMLlocalMatInfo::loadFile(char * filename)
{
	CHECK(fileExists(filename));
		
	xmlNodePtr cur;
	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
		return false;

	cur = xmlDocGetRootElement(xmlDoc);
	if (cur == NULL) 
	{	// is empty
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "material")) 
	{	// bad root
		xmlFreeDoc(xmlDoc);
		return false;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) 
	{
		// check <name>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
			{
				info.name = copyString(pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// check <layer>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"layer")))
		{
			if (!parseLayer(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <preview>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"preview")))
		{
			if (!parsePreview(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// go to next
		cur = cur->next;
	}

	xmlFreeDoc(xmlDoc);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool XMLlocalMatInfo::parseLayer(xmlNodePtr curNode)
{
	info.layers++;

	xmlNodePtr cur = curNode->xmlChildrenNode;

	while (cur != NULL) 
	{
		// check <tex_refl0>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_refl0")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
				info.textures++;
			if (pc)
				xmlFree(pc);
		}

		// check <tex_refl90>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_refl90")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
				info.textures++;
			if (pc)
				xmlFree(pc);
		}

		// check <tex_rough>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_rough")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
				info.textures++;
			if (pc)
				xmlFree(pc);
		}

		// check <tex_weight>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_weight")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
				info.textures++;
			if (pc)
				xmlFree(pc);
		}

		// go to next child
		cur = cur->next;
	}

	return true;
}

//------------------------------------------------------------------------------------------------------

bool XMLlocalMatInfo::parsePreview(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	xmlNodePtr cur;
	xmlChar * key;
	int w,h;
	cur = curNode->xmlChildrenNode;

	// get width
	key = xmlGetProp(curNode, (xmlChar*)"width");
	if (!key   ||   strlen((char *)key) == 0)
	{
		if (key)
			xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	char * addr;
	__int64 a = _strtoi64((char *)key, &addr, 10);
	if ( (addr == (char *)key)  || ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(key);
		return false;
	}
	xmlFree(key);
	w = (int)a;

	// get height
	key = xmlGetProp(curNode, (xmlChar*)"height");
	if (!key   ||   strlen((char *)key) == 0)
	{
		if (key)
			xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	a = _strtoi64((char *)key, &addr, 10);
	if ( (addr == (char *)key)  || ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(key);
		return false;
	}
	xmlFree(key);
	h = (int)a;

	if (w < 1   ||   h < 1)
		return false;

	ImageByteBuffer * image = new ImageByteBuffer();

	image->allocBuffer(w,h);
	image->clearBuffer();
	
	while (cur != NULL) 
	{
		if (cur->name[0] == 'l')
		{
			int l = (int)strlen((char*)cur->name);
			int n;
			if (l < 2)
				return false;

			char * addr;
			char * nn = (char*)cur->name+1;
			__int64 a = _strtoi64((char *)nn, &addr, 10);
			if ( (addr == (char *)nn)  || ((unsigned int)(addr-(char*)nn) < strlen((char *)nn)) )
			{	// conversion error (to int)
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			n = (int)a;
			if (n < 0   ||   n >= h)
			{
				errLine = xmlGetLineNo(curNode);
				return false;
			}

			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(curNode);
				return false;
			}

			l = (int)strlen(pc);
			if (l != w*6)
			{
				errLine = xmlGetLineNo(curNode);
				xmlFree(pc);
				return false;
			}

			unsigned char r,g,b;
			for (int i=0; i<w; i++)
			{
				DecHex::hex2ToUcharBigEndian((unsigned char*)&(pc[i*6+0]), r);
				DecHex::hex2ToUcharBigEndian((unsigned char*)&(pc[i*6+2]), g);
				DecHex::hex2ToUcharBigEndian((unsigned char*)&(pc[i*6+4]), b);
				image->imgBuf[n][i] = RGB(r,g,b);
			}

			xmlFree(pc);
		}

		// go to next child
		cur = cur->next;
	}


	int ww = max(image->width, image->height);

	if (!info.preview.imgBuf  ||  info.preview.width!=96  ||  info.preview.height!=96)
		info.preview.allocBuffer(96,96);

	ImageByteBuffer * aBuff = image->copyWithZoom(96.0/ww);
	for (int y=0; y<96; y++)
		for (int x=0; x<96; x++)
			info.preview.imgBuf[y][x] = aBuff->imgBuf[y][x];

	aBuff->freeBuffer();
	delete aBuff;
	aBuff = NULL;

	image->freeBuffer();
	delete image;
	image = NULL;

	return true;
}

//------------------------------------------------------------------------------------------------------
