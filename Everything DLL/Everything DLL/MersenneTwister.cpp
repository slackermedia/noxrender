#include "MersenneTwister.h"

bool MersenneTwister::initialize(int seed)
{
	index = 0;
	MT[0] = seed;
	for (int i=1; i<624; i++)
	{
		//last 32 bits of(1812433253 * (MT[i-1] xor (right shift by 30 bits(MT[i-1]))) + i) // 0x6c078965
		MT[i] = (unsigned int)( (1812433253 * ((unsigned long long)MT[i-1] ^ ((MT[i-1])>>30)) + i) & 0xffffffff);
	}

	return true;
}

unsigned int MersenneTwister::randUInt()
{
	if (index==0)
		generate_numbers();

	unsigned int y = MT[index];
	y = y ^ (y>>11);
	y = y ^ ((y<<7)&0x9d2c5680);
	y = y ^ ((y<<15)&0xefc60000);
	y = y ^ (y>>18);
	index = (index+1) % 624;

	return y;
}

void MersenneTwister::generate_numbers()
{
	for (int i=0; i<624; i++)
	{
		unsigned int y = (MT[i] & 0x80000000) + (MT[(i+1)%624] & 0x7fffffff);
		MT[i] = MT[(i+397)%624] ^ (y>>1);
		if (y%2)
			MT[i] = MT[i] ^ 0x9908b0df;
	}
}

