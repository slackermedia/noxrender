#include "raytracer.h"
#include <math.h>


//---------------------------------------------------
//     VECTOR3D

Vector3d Vector3d::operator+(const Vector3d &another) const
{
	Vector3d res;
	#ifdef FORCE_SSE
		res.sse = _mm_add_ps(sse, another.sse);
	#else
		res.x = x + another.x;
		res.y = y + another.y;
		res.z = z + another.z;
	#endif
	return res;
}

Vector3d Vector3d::operator+=(const Vector3d &another)
{
	#ifdef FORCE_SSE
		sse = _mm_add_ps(sse, another.sse);
	#else
		x += another.x;
		y += another.y;
		z += another.z;
	#endif
	return *this;
}

Vector3d Vector3d::operator-()
{
	Vector3d res;
	#ifdef FORCE_SSE
		res.sse = _mm_sub_ps(_mm_setzero_ps(), sse);
	#else
		res.x = -x;
		res.y = -y;
		res.z = -z;
	#endif
	return res;
}

Vector3d Vector3d::operator*(const float &s) const
{
	Vector3d res;
	#ifdef FORCE_SSE
		__m128 S = _mm_set1_ps(s);
		res.sse = _mm_mul_ps(sse, S);
	#else
		res.x = x * s;
		res.y = y * s;
		res.z = z * s;
	#endif
	return res;
}

Vector3d Vector3d::operator*=(const float &s)
{
	#ifdef FORCE_SSE
		__m128 S = _mm_set1_ps(s);
		sse = _mm_mul_ps(sse, S);
	#else
		x *= s;
		y *= s;
		z *= s;
	#endif
	return *this;
}

Vector3d Vector3d::operator^(const Vector3d &other) const
{
	Vector3d res;
	#ifdef FORCE_SSE
		__m128 d0,d1,d2,d3;
		d0 = _mm_shuffle_ps(sse,sse,     _MM_SHUFFLE(3,0,2,1));
		d1 = _mm_shuffle_ps(other.sse,other.sse, _MM_SHUFFLE(3,1,0,2));
		d2 = _mm_shuffle_ps(sse,sse, _MM_SHUFFLE(3,1,0,2));
		d3 = _mm_shuffle_ps(other.sse,other.sse,     _MM_SHUFFLE(3,0,2,1));
		res.sse = _mm_sub_ps( _mm_mul_ps(d0,d1) , _mm_mul_ps(d2,d3) );
	#else
		res.x = y*other.z - z*other.y;
		res.y = z*other.x - x*other.z;
		res.z = x*other.y - y*other.x;
	#endif
	return res;
}

float Vector3d::operator*(const Vector3d &other) const
{
	float res;
	#ifdef FORCE_SSE
		__m128 d0 = sse;
		d0 = _mm_mul_ps(d0, other.sse);
		#ifdef OWN_SSE3
			d0 = _mm_hadd_ps(d0,d0);
			d0 = _mm_hadd_ps(d0,d0);
		#else
			__m128 d1 = d0;
			d0 = _mm_shuffle_ps(d0,d1, 0x4e);
			d0 = _mm_add_ps(d0,d1);
			d1 = d0;
			d1 = _mm_shuffle_ps(d1,d1, 0x11);
			d0 = _mm_add_ps(d0,d1);
		#endif
		_mm_store_ss(&res, d0);
	#else
		res = x*other.x + y*other.y + z*other.z;
	#endif
	return res;
}

//----------------------------------------------------------------
//    POINT3D

Vector3d Point3d::operator-(const Point3d &another) const
{
	Vector3d res;
	#ifdef FORCE_SSE
		res.sse = _mm_sub_ps(sse, another.sse);
	#else
		res.x = x - another.x;
		res.y = y - another.y;
		res.z = z - another.z;
	#endif
	return res;	
}

Point3d Point3d::operator+(const Vector3d &dir) const
{
	Point3d res;
	#ifdef FORCE_SSE
		res.sse = _mm_add_ps(sse, dir.sse);
	#else
		res.x = x + dir.x;
		res.y = y + dir.y;
		res.z = z + dir.z;
	#endif
	return res;
}

Point3d  Point3d::operator*(const float &f) const
{
	Point3d res;
	res.x = x*f;
	res.y = y*f;
	res.z = z*f;
	return res;
}


//--------------------------------------------------------------------
//    COLOR4

Color4 Color4::operator+(const Color4 &another) const
{
	Color4 res;
	#ifdef COLOR_SSE
	#else
	#endif

	#ifdef COLOR_SSE
		res.sse = _mm_add_ps(sse, another.sse);
	#else
		res.r = r + another.r;
		res.g = g + another.g;
		res.b = b + another.b;
		res.a = a;
	#endif
	return res;
}

Color4 Color4::operator+=(const Color4 &another)
{
	#ifdef COLOR_SSE
		sse = _mm_add_ps(sse, another.sse);
	#else
		r += another.r;
		g += another.g;
		b += another.b;
	#endif
	return *this;
}

Color4 Color4::operator*(const Color4 &another) const
{
	Color4 res;
	#ifdef COLOR_SSE
		res.sse = _mm_mul_ps(sse, another.sse);
	#else
		res.r = r*another.r;
		res.g = g*another.g;
		res.b = b*another.b;
		res.a = a;
	#endif
	return res;
}

Color4 Color4::operator*=(const Color4 &another)
{
	#ifdef COLOR_SSE
		sse = _mm_mul_ps(sse, another.sse);
	#else
		r *= another.r;
		g *= another.g;
		b *= another.b;
	#endif
	return *this;
}

Color4 Color4::operator*(const float s) const
{
	Color4 res;
	#ifdef COLOR_SSE
		res.r = r*s;
		res.g = g*s;
		res.b = b*s;
		res.a = a;
	#else
		res.r = r*s;
		res.g = g*s;
		res.b = b*s;
		res.a = a;
	#endif
	return res;
}

Color4 Color4::operator*=(const float s)
{
	#ifdef COLOR_SSE
		__m128 mpl = _mm_set1_ps(s);
		sse = _mm_mul_ps(sse, mpl);
	#else
		r *= s;
		g *= s;
		b *= s;
		a *= s;
	#endif
	return *this;
}

Vector3d operator*(const Matrix3d &m, const Vector3d &v)
{
	Vector3d res;
	res.x = m.M00*v.x + m.M01*v.y + m.M02*v.z;
	res.y = m.M10*v.x + m.M11*v.y + m.M12*v.z;
	res.z = m.M20*v.x + m.M21*v.y + m.M22*v.z;
	return res;
}


Matrix3d Matrix3d::operator* (const Matrix3d &m) const
{
	Matrix3d r;
	r.M00 = M00*m.M00 + M01*m.M10 + M02*m.M20;
	r.M01 = M00*m.M01 + M01*m.M11 + M02*m.M21;
	r.M02 = M00*m.M02 + M01*m.M12 + M02*m.M22;
	r.M10 = M10*m.M00 + M11*m.M10 + M12*m.M20;
	r.M11 = M10*m.M01 + M11*m.M11 + M12*m.M21;
	r.M12 = M10*m.M02 + M11*m.M12 + M12*m.M22;
	r.M20 = M20*m.M00 + M21*m.M10 + M22*m.M20;
	r.M21 = M20*m.M01 + M21*m.M11 + M22*m.M21;
	r.M22 = M20*m.M02 + M21*m.M12 + M22*m.M22;
	return r;
}
