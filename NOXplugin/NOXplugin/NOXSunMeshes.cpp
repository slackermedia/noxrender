#include "max2013predef.h"
#include "NOXSunLight.h"
#include <decomp.h> 


void NOXSunTarget::BuildMesh()
{
	float lScale = 0.2f;
	Face f;

	//-----------------------------------

	meshN.setNumVerts(10);
	meshN.setNumFaces(8);

	meshN.setVert(0, Point3(-17.2f, 0.0f, 0.0f ));
	meshN.setVert(1, Point3(-17.2f, 50.3813f, 0.0f ));
	meshN.setVert(2, Point3(16.6281f, 0.0f, 0.0f ));
	meshN.setVert(3, Point3(25.3f, 0.0f, 0.0f ));
	meshN.setVert(4, Point3(25.3f, 64.1f, 0.0f ));
	meshN.setVert(5, Point3(17.2f, 64.1f, 0.0f ));
	meshN.setVert(6, Point3(17.2f, 13.7172f, 0.0f ));
	meshN.setVert(7, Point3(-16.6281f, 64.1f, 0.0f ));
	meshN.setVert(8, Point3(-25.3f, 64.1f, 0.0f ));
	meshN.setVert(9, Point3(-25.3f, 0.0f, 0.0f ));
	for (int i=0; i<meshN.getNumVerts(); i++)
		meshN.verts[i] *= lScale;

	f.v[0] = 8;	f.v[1] = 9;	f.v[2] = 0;	f.smGroup = 1;	f.flags = 3;	meshN.faces[0] = f;
	f.v[0] = 8;	f.v[1] = 0;	f.v[2] = 1;	f.smGroup = 1;	f.flags = 2;	meshN.faces[1] = f;
	f.v[0] = 7;	f.v[1] = 8;	f.v[2] = 1;	f.smGroup = 1;	f.flags = 1;	meshN.faces[2] = f;
	f.v[0] = 6;	f.v[1] = 7;	f.v[2] = 1;	f.smGroup = 1;	f.flags = 1;	meshN.faces[3] = f;
	f.v[0] = 6;	f.v[1] = 1;	f.v[2] = 2;	f.smGroup = 1;	f.flags = 2;	meshN.faces[4] = f;
	f.v[0] = 6;	f.v[1] = 2;	f.v[2] = 3;	f.smGroup = 1;	f.flags = 2;	meshN.faces[5] = f;
	f.v[0] = 6;	f.v[1] = 3;	f.v[2] = 4;	f.smGroup = 1;	f.flags = 2;	meshN.faces[6] = f;
	f.v[0] = 5;	f.v[1] = 6;	f.v[2] = 4;	f.smGroup = 1;	f.flags = 5;	meshN.faces[7] = f;

	meshN.buildNormals();

	//-----------------------------------


	meshS.setNumVerts(233);
	meshS.setNumFaces(231);

	meshS.setVert(0, Point3(-25.286f, 18.7897f, 0.0f ));
	meshS.setVert(1, Point3(-25.0432f, 17.0339f, 0.0f ));
	meshS.setVert(2, Point3(-24.6715f, 15.3326f, 0.0f ));
	meshS.setVert(3, Point3(-24.1709f, 13.6857f, 0.0f ));
	meshS.setVert(4, Point3(-23.5415f, 12.0933f, 0.0f ));
	meshS.setVert(5, Point3(-22.7832f, 10.5554f, 0.0f ));
	meshS.setVert(6, Point3(-21.8961f, 9.07188f, 0.0f ));
	meshS.setVert(7, Point3(-20.893f, 7.66786f, 0.0f ));
	meshS.setVert(8, Point3(-19.7869f, 6.3683f, 0.0f ));
	meshS.setVert(9, Point3(-18.5778f, 5.17321f, 0.0f ));
	meshS.setVert(10, Point3(-17.2656f, 4.08259f, 0.0f ));
	meshS.setVert(11, Point3(-15.8504f, 3.09643f, 0.0f ));
	meshS.setVert(12, Point3(-14.3322f, 2.21473f, 0.0f ));
	meshS.setVert(13, Point3(-12.7109f, 1.4375f, 0.0f ));
	meshS.setVert(14, Point3(-10.9809f, 0.764286f, 0.0f ));
	meshS.setVert(15, Point3(-9.13619f, 0.194643f, 0.0f ));
	meshS.setVert(16, Point3(-7.17691f, -0.271429f, 0.0f ));
	meshS.setVert(17, Point3(-5.10303f, -0.633928f, 0.0f ));
	meshS.setVert(18, Point3(-2.91454f, -0.892857f, 0.0f ));
	meshS.setVert(19, Point3(-0.611448f, -1.04821f, 0.0f ));
	meshS.setVert(20, Point3(1.80625f, -1.1f, 0.0f ));
	meshS.setVert(21, Point3(3.73095f, -1.04879f, 0.0f ));
	meshS.setVert(22, Point3(5.59968f, -0.895153f, 0.0f ));
	meshS.setVert(23, Point3(7.41245f, -0.639094f, 0.0f ));
	meshS.setVert(24, Point3(9.16926f, -0.280612f, 0.0f ));
	meshS.setVert(25, Point3(10.8701f, 0.180293f, 0.0f ));
	meshS.setVert(26, Point3(12.515f, 0.743623f, 0.0f ));
	meshS.setVert(27, Point3(14.1039f, 1.40938f, 0.0f ));
	meshS.setVert(28, Point3(15.6129f, 2.16732f, 0.0f ));
	meshS.setVert(29, Point3(17.0179f, 3.00721f, 0.0f ));
	meshS.setVert(30, Point3(18.3189f, 3.92905f, 0.0f ));
	meshS.setVert(31, Point3(19.516f, 4.93284f, 0.0f ));
	meshS.setVert(32, Point3(20.6092f, 6.01859f, 0.0f ));
	meshS.setVert(33, Point3(21.5984f, 7.18629f, 0.0f ));
	meshS.setVert(34, Point3(22.4836f, 8.43594f, 0.0f ));
	meshS.setVert(35, Point3(23.2573f, 9.73862f, 0.0f ));
	meshS.setVert(36, Point3(23.912f, 11.0654f, 0.0f ));
	meshS.setVert(37, Point3(24.4477f, 12.4163f, 0.0f ));
	meshS.setVert(38, Point3(24.8643f, 13.7913f, 0.0f ));
	meshS.setVert(39, Point3(25.1619f, 15.1904f, 0.0f ));
	meshS.setVert(40, Point3(25.3405f, 16.6136f, 0.0f ));
	meshS.setVert(41, Point3(25.4f, 18.0609f, 0.0f ));
	meshS.setVert(42, Point3(25.3459f, 19.5079f, 0.0f ));
	meshS.setVert(43, Point3(25.1834f, 20.9058f, 0.0f ));
	meshS.setVert(44, Point3(24.9127f, 22.2548f, 0.0f ));
	meshS.setVert(45, Point3(24.5337f, 23.5548f, 0.0f ));
	meshS.setVert(46, Point3(24.0464f, 24.8058f, 0.0f ));
	meshS.setVert(47, Point3(23.4508f, 26.0079f, 0.0f ));
	meshS.setVert(48, Point3(22.7469f, 27.1609f, 0.0f ));
	meshS.setVert(49, Point3(21.9294f, 28.2614f, 0.0f ));
	meshS.setVert(50, Point3(20.9929f, 29.3058f, 0.0f ));
	meshS.setVert(51, Point3(19.9375f, 30.2941f, 0.0f ));
	meshS.setVert(52, Point3(18.7632f, 31.2262f, 0.0f ));
	meshS.setVert(53, Point3(17.4699f, 32.1023f, 0.0f ));
	meshS.setVert(54, Point3(16.0577f, 32.9222f, 0.0f ));
	meshS.setVert(55, Point3(14.5266f, 33.6859f, 0.0f ));
	meshS.setVert(56, Point3(13.3013f, 34.1966f, 0.0f ));
	meshS.setVert(57, Point3(11.8122f, 34.7314f, 0.0f ));
	meshS.setVert(58, Point3(10.0592f, 35.2903f, 0.0f ));
	meshS.setVert(59, Point3(8.04238f, 35.8732f, 0.0f ));
	meshS.setVert(60, Point3(5.76166f, 36.4802f, 0.0f ));
	meshS.setVert(61, Point3(3.21706f, 37.1113f, 0.0f ));
	meshS.setVert(62, Point3(0.408594f, 37.7664f, 0.0f ));
	meshS.setVert(63, Point3(-2.37152f, 38.4314f, 0.0f ));
	meshS.setVert(64, Point3(-4.83107f, 39.0918f, 0.0f ));
	meshS.setVert(65, Point3(-6.97006f, 39.7477f, 0.0f ));
	meshS.setVert(66, Point3(-8.78847f, 40.3992f, 0.0f ));
	meshS.setVert(67, Point3(-10.2863f, 41.0461f, 0.0f ));
	meshS.setVert(68, Point3(-11.4636f, 41.6886f, 0.0f ));
	meshS.setVert(69, Point3(-12.3203f, 42.3266f, 0.0f ));
	meshS.setVert(70, Point3(-12.9782f, 42.9872f, 0.0f ));
	meshS.setVert(71, Point3(-13.5349f, 43.6977f, 0.0f ));
	meshS.setVert(72, Point3(-13.9903f, 44.4581f, 0.0f ));
	meshS.setVert(73, Point3(-14.3445f, 45.2684f, 0.0f ));
	meshS.setVert(74, Point3(-14.5976f, 46.1285f, 0.0f ));
	meshS.setVert(75, Point3(-14.7494f, 47.0386f, 0.0f ));
	meshS.setVert(76, Point3(-14.8f, 47.9984f, 0.0f ));
	meshS.setVert(77, Point3(-14.7285f, 49.1096f, 0.0f ));
	meshS.setVert(78, Point3(-14.514f, 50.1736f, 0.0f ));
	meshS.setVert(79, Point3(-14.1566f, 51.1904f, 0.0f ));
	meshS.setVert(80, Point3(-13.6561f, 52.1599f, 0.0f ));
	meshS.setVert(81, Point3(-13.0127f, 53.0822f, 0.0f ));
	meshS.setVert(82, Point3(-12.2263f, 53.9573f, 0.0f ));
	meshS.setVert(83, Point3(-11.2969f, 54.7852f, 0.0f ));
	meshS.setVert(84, Point3(-10.2112f, 55.532f, 0.0f ));
	meshS.setVert(85, Point3(-8.95593f, 56.1638f, 0.0f ));
	meshS.setVert(86, Point3(-7.53109f, 56.6809f, 0.0f ));
	meshS.setVert(87, Point3(-5.93667f, 57.083f, 0.0f ));
	meshS.setVert(88, Point3(-4.17267f, 57.3702f, 0.0f ));
	meshS.setVert(89, Point3(-2.23909f, 57.5426f, 0.0f ));
	meshS.setVert(90, Point3(-0.135937f, 57.6f, 0.0f ));
	meshS.setVert(91, Point3(1.89212f, 57.5376f, 0.0f ));
	meshS.setVert(92, Point3(3.77363f, 57.3504f, 0.0f ));
	meshS.setVert(93, Point3(5.50858f, 57.0385f, 0.0f ));
	meshS.setVert(94, Point3(7.09697f, 56.6018f, 0.0f ));
	meshS.setVert(95, Point3(8.53881f, 56.0403f, 0.0f ));
	meshS.setVert(96, Point3(9.83409f, 55.354f, 0.0f ));
	meshS.setVert(97, Point3(10.9828f, 54.543f, 0.0f ));
	meshS.setVert(98, Point3(11.9948f, 53.6098f, 0.0f ));
	meshS.setVert(99, Point3(12.8798f, 52.557f, 0.0f ));
	meshS.setVert(100, Point3(13.6378f, 51.3847f, 0.0f ));
	meshS.setVert(101, Point3(14.2688f, 50.0928f, 0.0f ));
	meshS.setVert(102, Point3(14.7728f, 48.6814f, 0.0f ));
	meshS.setVert(103, Point3(15.1499f, 47.1505f, 0.0f ));
	meshS.setVert(104, Point3(15.4f, 45.5f, 0.0f ));
	meshS.setVert(105, Point3(23.5f, 46.1f, 0.0f ));
	meshS.setVert(106, Point3(23.3828f, 47.6831f, 0.0f ));
	meshS.setVert(107, Point3(23.1552f, 49.2182f, 0.0f ));
	meshS.setVert(108, Point3(22.8174f, 50.7053f, 0.0f ));
	meshS.setVert(109, Point3(22.3691f, 52.1444f, 0.0f ));
	meshS.setVert(110, Point3(21.8106f, 53.5354f, 0.0f ));
	meshS.setVert(111, Point3(21.1417f, 54.8784f, 0.0f ));
	meshS.setVert(112, Point3(20.3625f, 56.1734f, 0.0f ));
	meshS.setVert(113, Point3(19.477f, 57.4004f, 0.0f ));
	meshS.setVert(114, Point3(18.4892f, 58.5391f, 0.0f ));
	meshS.setVert(115, Point3(17.3992f, 59.5898f, 0.0f ));
	meshS.setVert(116, Point3(16.2069f, 60.5523f, 0.0f ));
	meshS.setVert(117, Point3(14.9123f, 61.4266f, 0.0f ));
	meshS.setVert(118, Point3(13.5155f, 62.2129f, 0.0f ));
	meshS.setVert(119, Point3(12.0164f, 62.9109f, 0.0f ));
	meshS.setVert(120, Point3(10.432f, 63.5182f, 0.0f ));
	meshS.setVert(121, Point3(8.77903f, 64.0321f, 0.0f ));
	meshS.setVert(122, Point3(7.05765f, 64.4526f, 0.0f ));
	meshS.setVert(123, Point3(5.26781f, 64.7796f, 0.0f ));
	meshS.setVert(124, Point3(3.4095f, 65.0131f, 0.0f ));
	meshS.setVert(125, Point3(1.48273f, 65.1533f, 0.0f ));
	meshS.setVert(126, Point3(-0.512499f, 65.2f, 0.0f ));
	meshS.setVert(127, Point3(-2.33134f, 65.1553f, 0.0f ));
	meshS.setVert(128, Point3(-4.10217f, 65.0212f, 0.0f ));
	meshS.setVert(129, Point3(-5.82497f, 64.7976f, 0.0f ));
	meshS.setVert(130, Point3(-7.49974f, 64.4847f, 0.0f ));
	meshS.setVert(131, Point3(-9.1265f, 64.0823f, 0.0f ));
	meshS.setVert(132, Point3(-10.7052f, 63.5906f, 0.0f ));
	meshS.setVert(133, Point3(-12.2359f, 63.0094f, 0.0f ));
	meshS.setVert(134, Point3(-13.691f, 62.3419f, 0.0f ));
	meshS.setVert(135, Point3(-15.043f, 61.5912f, 0.0f ));
	meshS.setVert(136, Point3(-16.2917f, 60.7573f, 0.0f ));
	meshS.setVert(137, Point3(-17.4372f, 59.8402f, 0.0f ));
	meshS.setVert(138, Point3(-18.4796f, 58.84f, 0.0f ));
	meshS.setVert(139, Point3(-19.4187f, 57.7565f, 0.0f ));
	meshS.setVert(140, Point3(-20.2547f, 56.5898f, 0.0f ));
	meshS.setVert(141, Point3(-20.983f, 55.3687f, 0.0f ));
	meshS.setVert(142, Point3(-21.5993f, 54.1216f, 0.0f ));
	meshS.setVert(143, Point3(-22.1036f, 52.8487f, 0.0f ));
	meshS.setVert(144, Point3(-22.4958f, 51.5499f, 0.0f ));
	meshS.setVert(145, Point3(-22.7759f, 50.2253f, 0.0f ));
	meshS.setVert(146, Point3(-22.944f, 48.8748f, 0.0f ));
	meshS.setVert(147, Point3(-23.0f, 47.4984f, 0.0f ));
	meshS.setVert(148, Point3(-22.954f, 46.2512f, 0.0f ));
	meshS.setVert(149, Point3(-22.8161f, 45.0388f, 0.0f ));
	meshS.setVert(150, Point3(-22.5863f, 43.8614f, 0.0f ));
	meshS.setVert(151, Point3(-22.2645f, 42.7188f, 0.0f ));
	meshS.setVert(152, Point3(-21.8508f, 41.6112f, 0.0f ));
	meshS.setVert(153, Point3(-21.3452f, 40.5386f, 0.0f ));
	meshS.setVert(154, Point3(-20.7477f, 39.5008f, 0.0f ));
	meshS.setVert(155, Point3(-20.0564f, 38.5042f, 0.0f ));
	meshS.setVert(156, Point3(-19.2696f, 37.555f, 0.0f ));
	meshS.setVert(157, Point3(-18.3873f, 36.6532f, 0.0f ));
	meshS.setVert(158, Point3(-17.4095f, 35.7989f, 0.0f ));
	meshS.setVert(159, Point3(-16.3362f, 34.9919f, 0.0f ));
	meshS.setVert(160, Point3(-15.1674f, 34.2324f, 0.0f ));
	meshS.setVert(161, Point3(-13.9031f, 33.5203f, 0.0f ));
	meshS.setVert(162, Point3(-12.784f, 32.9772f, 0.0f ));
	meshS.setVert(163, Point3(-11.4506f, 32.4246f, 0.0f ));
	meshS.setVert(164, Point3(-9.90311f, 31.8625f, 0.0f ));
	meshS.setVert(165, Point3(-8.14139f, 31.291f, 0.0f ));
	meshS.setVert(166, Point3(-6.16548f, 30.71f, 0.0f ));
	meshS.setVert(167, Point3(-3.97538f, 30.1195f, 0.0f ));
	meshS.setVert(168, Point3(-1.57109f, 29.5195f, 0.0f ));
	meshS.setVert(169, Point3(0.813297f, 28.9376f, 0.0f ));
	meshS.setVert(170, Point3(2.9437f, 28.401f, 0.0f ));
	meshS.setVert(171, Point3(4.82012f, 27.91f, 0.0f ));
	meshS.setVert(172, Point3(6.44256f, 27.4643f, 0.0f ));
	meshS.setVert(173, Point3(7.811f, 27.0641f, 0.0f ));
	meshS.setVert(174, Point3(8.92546f, 26.7093f, 0.0f ));
	meshS.setVert(175, Point3(9.78594f, 26.4f, 0.0f ));
	meshS.setVert(176, Point3(10.8832f, 25.9488f, 0.0f ));
	meshS.setVert(177, Point3(11.8866f, 25.4686f, 0.0f ));
	meshS.setVert(178, Point3(12.7961f, 24.9592f, 0.0f ));
	meshS.setVert(179, Point3(13.6117f, 24.4207f, 0.0f ));
	meshS.setVert(180, Point3(14.3335f, 23.853f, 0.0f ));
	meshS.setVert(181, Point3(14.9613f, 23.2563f, 0.0f ));
	meshS.setVert(182, Point3(15.4953f, 22.6305f, 0.0f ));
	meshS.setVert(183, Point3(15.9476f, 21.9745f, 0.0f ));
	meshS.setVert(184, Point3(16.3303f, 21.2874f, 0.0f ));
	meshS.setVert(185, Point3(16.6434f, 20.5691f, 0.0f ));
	meshS.setVert(186, Point3(16.8869f, 19.8196f, 0.0f ));
	meshS.setVert(187, Point3(17.0608f, 19.039f, 0.0f ));
	meshS.setVert(188, Point3(17.1652f, 18.2273f, 0.0f ));
	meshS.setVert(189, Point3(17.2f, 17.3844f, 0.0f ));
	meshS.setVert(190, Point3(17.1632f, 16.5479f, 0.0f ));
	meshS.setVert(191, Point3(17.0529f, 15.7294f, 0.0f ));
	meshS.setVert(192, Point3(16.8691f, 14.929f, 0.0f ));
	meshS.setVert(193, Point3(16.6117f, 14.1466f, 0.0f ));
	meshS.setVert(194, Point3(16.2808f, 13.3823f, 0.0f ));
	meshS.setVert(195, Point3(15.8764f, 12.636f, 0.0f ));
	meshS.setVert(196, Point3(15.3984f, 11.9078f, 0.0f ));
	meshS.setVert(197, Point3(14.8452f, 11.2105f, 0.0f ));
	meshS.setVert(198, Point3(14.2148f, 10.5569f, 0.0f ));
	meshS.setVert(199, Point3(13.5074f, 9.9471f, 0.0f ));
	meshS.setVert(200, Point3(12.7229f, 9.38103f, 0.0f ));
	meshS.setVert(201, Point3(11.8614f, 8.85871f, 0.0f ));
	meshS.setVert(202, Point3(10.9227f, 8.38014f, 0.0f ));
	meshS.setVert(203, Point3(9.90703f, 7.94531f, 0.0f ));
	meshS.setVert(204, Point3(8.82975f, 7.56186f, 0.0f ));
	meshS.setVert(205, Point3(7.7064f, 7.2374f, 0.0f ));
	meshS.setVert(206, Point3(6.53696f, 6.97194f, 0.0f ));
	meshS.setVert(207, Point3(5.32145f, 6.76547f, 0.0f ));
	meshS.setVert(208, Point3(4.05985f, 6.61799f, 0.0f ));
	meshS.setVert(209, Point3(2.75219f, 6.5295f, 0.0f ));
	meshS.setVert(210, Point3(1.39844f, 6.5f, 0.0f ));
	meshS.setVert(211, Point3(-0.129861f, 6.53887f, 0.0f ));
	meshS.setVert(212, Point3(-1.61387f, 6.65548f, 0.0f ));
	meshS.setVert(213, Point3(-3.05359f, 6.84984f, 0.0f ));
	meshS.setVert(214, Point3(-4.44901f, 7.12194f, 0.0f ));
	meshS.setVert(215, Point3(-5.80014f, 7.47178f, 0.0f ));
	meshS.setVert(216, Point3(-7.10698f, 7.89936f, 0.0f ));
	meshS.setVert(217, Point3(-8.36953f, 8.40469f, 0.0f ));
	meshS.setVert(218, Point3(-9.56343f, 8.97301f, 0.0f ));
	meshS.setVert(219, Point3(-10.6643f, 9.58957f, 0.0f ));
	meshS.setVert(220, Point3(-11.6722f, 10.2544f, 0.0f ));
	meshS.setVert(221, Point3(-12.587f, 10.9674f, 0.0f ));
	meshS.setVert(222, Point3(-13.4088f, 11.7287f, 0.0f ));
	meshS.setVert(223, Point3(-14.1376f, 12.5383f, 0.0f ));
	meshS.setVert(224, Point3(-14.7734f, 13.3961f, 0.0f ));
	meshS.setVert(225, Point3(-15.3321f, 14.3133f, 0.0f ));
	meshS.setVert(226, Point3(-15.8297f, 15.3012f, 0.0f ));
	meshS.setVert(227, Point3(-16.2661f, 16.3597f, 0.0f ));
	meshS.setVert(228, Point3(-16.6413f, 17.4888f, 0.0f ));
	meshS.setVert(229, Point3(-16.9554f, 18.6886f, 0.0f ));
	meshS.setVert(230, Point3(-17.2083f, 19.959f, 0.0f ));
	meshS.setVert(231, Point3(-17.4f, 21.3f, 0.0f ));
	meshS.setVert(232, Point3(-25.4f, 20.6f, 0.0f ));
	for (int i=0; i<meshS.getNumVerts(); i++)
		meshS.verts[i] *= lScale;

	f.v[0] = 104;	f.v[1] = 105;	f.v[2] = 106;	f.smGroup = 1;	f.flags = 3;	meshS.faces[0] = f;
	f.v[0] = 103;	f.v[1] = 104;	f.v[2] = 106;	f.smGroup = 1;	f.flags = 1;	meshS.faces[1] = f;
	f.v[0] = 103;	f.v[1] = 106;	f.v[2] = 107;	f.smGroup = 1;	f.flags = 2;	meshS.faces[2] = f;
	f.v[0] = 102;	f.v[1] = 103;	f.v[2] = 107;	f.smGroup = 1;	f.flags = 1;	meshS.faces[3] = f;
	f.v[0] = 102;	f.v[1] = 107;	f.v[2] = 108;	f.smGroup = 1;	f.flags = 2;	meshS.faces[4] = f;
	f.v[0] = 102;	f.v[1] = 108;	f.v[2] = 109;	f.smGroup = 1;	f.flags = 2;	meshS.faces[5] = f;
	f.v[0] = 101;	f.v[1] = 102;	f.v[2] = 109;	f.smGroup = 1;	f.flags = 1;	meshS.faces[6] = f;
	f.v[0] = 101;	f.v[1] = 109;	f.v[2] = 110;	f.smGroup = 1;	f.flags = 2;	meshS.faces[7] = f;
	f.v[0] = 101;	f.v[1] = 110;	f.v[2] = 111;	f.smGroup = 1;	f.flags = 2;	meshS.faces[8] = f;
	f.v[0] = 100;	f.v[1] = 101;	f.v[2] = 111;	f.smGroup = 1;	f.flags = 1;	meshS.faces[9] = f;
	f.v[0] = 100;	f.v[1] = 111;	f.v[2] = 112;	f.smGroup = 1;	f.flags = 2;	meshS.faces[10] = f;
	f.v[0] = 99;	f.v[1] = 100;	f.v[2] = 112;	f.smGroup = 1;	f.flags = 1;	meshS.faces[11] = f;
	f.v[0] = 99;	f.v[1] = 112;	f.v[2] = 113;	f.smGroup = 1;	f.flags = 2;	meshS.faces[12] = f;
	f.v[0] = 99;	f.v[1] = 113;	f.v[2] = 114;	f.smGroup = 1;	f.flags = 2;	meshS.faces[13] = f;
	f.v[0] = 98;	f.v[1] = 99;	f.v[2] = 114;	f.smGroup = 1;	f.flags = 1;	meshS.faces[14] = f;
	f.v[0] = 98;	f.v[1] = 114;	f.v[2] = 115;	f.smGroup = 1;	f.flags = 2;	meshS.faces[15] = f;
	f.v[0] = 98;	f.v[1] = 115;	f.v[2] = 116;	f.smGroup = 1;	f.flags = 2;	meshS.faces[16] = f;
	f.v[0] = 97;	f.v[1] = 98;	f.v[2] = 116;	f.smGroup = 1;	f.flags = 1;	meshS.faces[17] = f;
	f.v[0] = 97;	f.v[1] = 116;	f.v[2] = 117;	f.smGroup = 1;	f.flags = 2;	meshS.faces[18] = f;
	f.v[0] = 96;	f.v[1] = 97;	f.v[2] = 117;	f.smGroup = 1;	f.flags = 1;	meshS.faces[19] = f;
	f.v[0] = 96;	f.v[1] = 117;	f.v[2] = 118;	f.smGroup = 1;	f.flags = 2;	meshS.faces[20] = f;
	f.v[0] = 96;	f.v[1] = 118;	f.v[2] = 119;	f.smGroup = 1;	f.flags = 2;	meshS.faces[21] = f;
	f.v[0] = 95;	f.v[1] = 96;	f.v[2] = 119;	f.smGroup = 1;	f.flags = 1;	meshS.faces[22] = f;
	f.v[0] = 95;	f.v[1] = 119;	f.v[2] = 120;	f.smGroup = 1;	f.flags = 2;	meshS.faces[23] = f;
	f.v[0] = 94;	f.v[1] = 95;	f.v[2] = 120;	f.smGroup = 1;	f.flags = 1;	meshS.faces[24] = f;
	f.v[0] = 94;	f.v[1] = 120;	f.v[2] = 121;	f.smGroup = 1;	f.flags = 2;	meshS.faces[25] = f;
	f.v[0] = 93;	f.v[1] = 94;	f.v[2] = 121;	f.smGroup = 1;	f.flags = 1;	meshS.faces[26] = f;
	f.v[0] = 93;	f.v[1] = 121;	f.v[2] = 122;	f.smGroup = 1;	f.flags = 2;	meshS.faces[27] = f;
	f.v[0] = 93;	f.v[1] = 122;	f.v[2] = 123;	f.smGroup = 1;	f.flags = 2;	meshS.faces[28] = f;
	f.v[0] = 92;	f.v[1] = 93;	f.v[2] = 123;	f.smGroup = 1;	f.flags = 1;	meshS.faces[29] = f;
	f.v[0] = 92;	f.v[1] = 123;	f.v[2] = 124;	f.smGroup = 1;	f.flags = 2;	meshS.faces[30] = f;
	f.v[0] = 91;	f.v[1] = 92;	f.v[2] = 124;	f.smGroup = 1;	f.flags = 1;	meshS.faces[31] = f;
	f.v[0] = 91;	f.v[1] = 124;	f.v[2] = 125;	f.smGroup = 1;	f.flags = 2;	meshS.faces[32] = f;
	f.v[0] = 90;	f.v[1] = 91;	f.v[2] = 125;	f.smGroup = 1;	f.flags = 1;	meshS.faces[33] = f;
	f.v[0] = 90;	f.v[1] = 125;	f.v[2] = 126;	f.smGroup = 1;	f.flags = 2;	meshS.faces[34] = f;
	f.v[0] = 89;	f.v[1] = 90;	f.v[2] = 126;	f.smGroup = 1;	f.flags = 1;	meshS.faces[35] = f;
	f.v[0] = 89;	f.v[1] = 126;	f.v[2] = 127;	f.smGroup = 1;	f.flags = 2;	meshS.faces[36] = f;
	f.v[0] = 89;	f.v[1] = 127;	f.v[2] = 128;	f.smGroup = 1;	f.flags = 2;	meshS.faces[37] = f;
	f.v[0] = 88;	f.v[1] = 89;	f.v[2] = 128;	f.smGroup = 1;	f.flags = 1;	meshS.faces[38] = f;
	f.v[0] = 88;	f.v[1] = 128;	f.v[2] = 129;	f.smGroup = 1;	f.flags = 2;	meshS.faces[39] = f;
	f.v[0] = 87;	f.v[1] = 88;	f.v[2] = 129;	f.smGroup = 1;	f.flags = 1;	meshS.faces[40] = f;
	f.v[0] = 87;	f.v[1] = 129;	f.v[2] = 130;	f.smGroup = 1;	f.flags = 2;	meshS.faces[41] = f;
	f.v[0] = 87;	f.v[1] = 130;	f.v[2] = 131;	f.smGroup = 1;	f.flags = 2;	meshS.faces[42] = f;
	f.v[0] = 86;	f.v[1] = 87;	f.v[2] = 131;	f.smGroup = 1;	f.flags = 1;	meshS.faces[43] = f;
	f.v[0] = 86;	f.v[1] = 131;	f.v[2] = 132;	f.smGroup = 1;	f.flags = 2;	meshS.faces[44] = f;
	f.v[0] = 85;	f.v[1] = 86;	f.v[2] = 132;	f.smGroup = 1;	f.flags = 1;	meshS.faces[45] = f;
	f.v[0] = 85;	f.v[1] = 132;	f.v[2] = 133;	f.smGroup = 1;	f.flags = 2;	meshS.faces[46] = f;
	f.v[0] = 85;	f.v[1] = 133;	f.v[2] = 134;	f.smGroup = 1;	f.flags = 2;	meshS.faces[47] = f;
	f.v[0] = 84;	f.v[1] = 85;	f.v[2] = 134;	f.smGroup = 1;	f.flags = 1;	meshS.faces[48] = f;
	f.v[0] = 84;	f.v[1] = 134;	f.v[2] = 135;	f.smGroup = 1;	f.flags = 2;	meshS.faces[49] = f;
	f.v[0] = 83;	f.v[1] = 84;	f.v[2] = 135;	f.smGroup = 1;	f.flags = 1;	meshS.faces[50] = f;
	f.v[0] = 83;	f.v[1] = 135;	f.v[2] = 136;	f.smGroup = 1;	f.flags = 2;	meshS.faces[51] = f;
	f.v[0] = 83;	f.v[1] = 136;	f.v[2] = 137;	f.smGroup = 1;	f.flags = 2;	meshS.faces[52] = f;
	f.v[0] = 82;	f.v[1] = 83;	f.v[2] = 137;	f.smGroup = 1;	f.flags = 1;	meshS.faces[53] = f;
	f.v[0] = 82;	f.v[1] = 137;	f.v[2] = 138;	f.smGroup = 1;	f.flags = 2;	meshS.faces[54] = f;
	f.v[0] = 81;	f.v[1] = 82;	f.v[2] = 138;	f.smGroup = 1;	f.flags = 1;	meshS.faces[55] = f;
	f.v[0] = 81;	f.v[1] = 138;	f.v[2] = 139;	f.smGroup = 1;	f.flags = 2;	meshS.faces[56] = f;
	f.v[0] = 81;	f.v[1] = 139;	f.v[2] = 140;	f.smGroup = 1;	f.flags = 2;	meshS.faces[57] = f;
	f.v[0] = 80;	f.v[1] = 81;	f.v[2] = 140;	f.smGroup = 1;	f.flags = 1;	meshS.faces[58] = f;
	f.v[0] = 80;	f.v[1] = 140;	f.v[2] = 141;	f.smGroup = 1;	f.flags = 2;	meshS.faces[59] = f;
	f.v[0] = 79;	f.v[1] = 80;	f.v[2] = 141;	f.smGroup = 1;	f.flags = 1;	meshS.faces[60] = f;
	f.v[0] = 79;	f.v[1] = 141;	f.v[2] = 142;	f.smGroup = 1;	f.flags = 2;	meshS.faces[61] = f;
	f.v[0] = 79;	f.v[1] = 142;	f.v[2] = 143;	f.smGroup = 1;	f.flags = 2;	meshS.faces[62] = f;
	f.v[0] = 78;	f.v[1] = 79;	f.v[2] = 143;	f.smGroup = 1;	f.flags = 1;	meshS.faces[63] = f;
	f.v[0] = 78;	f.v[1] = 143;	f.v[2] = 144;	f.smGroup = 1;	f.flags = 2;	meshS.faces[64] = f;
	f.v[0] = 77;	f.v[1] = 78;	f.v[2] = 144;	f.smGroup = 1;	f.flags = 1;	meshS.faces[65] = f;
	f.v[0] = 77;	f.v[1] = 144;	f.v[2] = 145;	f.smGroup = 1;	f.flags = 2;	meshS.faces[66] = f;
	f.v[0] = 77;	f.v[1] = 145;	f.v[2] = 146;	f.smGroup = 1;	f.flags = 2;	meshS.faces[67] = f;
	f.v[0] = 76;	f.v[1] = 77;	f.v[2] = 146;	f.smGroup = 1;	f.flags = 1;	meshS.faces[68] = f;
	f.v[0] = 76;	f.v[1] = 146;	f.v[2] = 147;	f.smGroup = 1;	f.flags = 2;	meshS.faces[69] = f;
	f.v[0] = 75;	f.v[1] = 76;	f.v[2] = 147;	f.smGroup = 1;	f.flags = 1;	meshS.faces[70] = f;
	f.v[0] = 75;	f.v[1] = 147;	f.v[2] = 148;	f.smGroup = 1;	f.flags = 2;	meshS.faces[71] = f;
	f.v[0] = 75;	f.v[1] = 148;	f.v[2] = 149;	f.smGroup = 1;	f.flags = 2;	meshS.faces[72] = f;
	f.v[0] = 74;	f.v[1] = 75;	f.v[2] = 149;	f.smGroup = 1;	f.flags = 1;	meshS.faces[73] = f;
	f.v[0] = 74;	f.v[1] = 149;	f.v[2] = 150;	f.smGroup = 1;	f.flags = 2;	meshS.faces[74] = f;
	f.v[0] = 74;	f.v[1] = 150;	f.v[2] = 151;	f.smGroup = 1;	f.flags = 2;	meshS.faces[75] = f;
	f.v[0] = 73;	f.v[1] = 74;	f.v[2] = 151;	f.smGroup = 1;	f.flags = 1;	meshS.faces[76] = f;
	f.v[0] = 73;	f.v[1] = 151;	f.v[2] = 152;	f.smGroup = 1;	f.flags = 2;	meshS.faces[77] = f;
	f.v[0] = 72;	f.v[1] = 73;	f.v[2] = 152;	f.smGroup = 1;	f.flags = 1;	meshS.faces[78] = f;
	f.v[0] = 72;	f.v[1] = 152;	f.v[2] = 153;	f.smGroup = 1;	f.flags = 2;	meshS.faces[79] = f;
	f.v[0] = 72;	f.v[1] = 153;	f.v[2] = 154;	f.smGroup = 1;	f.flags = 2;	meshS.faces[80] = f;
	f.v[0] = 71;	f.v[1] = 72;	f.v[2] = 154;	f.smGroup = 1;	f.flags = 1;	meshS.faces[81] = f;
	f.v[0] = 71;	f.v[1] = 154;	f.v[2] = 155;	f.smGroup = 1;	f.flags = 2;	meshS.faces[82] = f;
	f.v[0] = 71;	f.v[1] = 155;	f.v[2] = 156;	f.smGroup = 1;	f.flags = 2;	meshS.faces[83] = f;
	f.v[0] = 70;	f.v[1] = 71;	f.v[2] = 156;	f.smGroup = 1;	f.flags = 1;	meshS.faces[84] = f;
	f.v[0] = 70;	f.v[1] = 156;	f.v[2] = 157;	f.smGroup = 1;	f.flags = 2;	meshS.faces[85] = f;
	f.v[0] = 69;	f.v[1] = 70;	f.v[2] = 157;	f.smGroup = 1;	f.flags = 1;	meshS.faces[86] = f;
	f.v[0] = 69;	f.v[1] = 157;	f.v[2] = 158;	f.smGroup = 1;	f.flags = 2;	meshS.faces[87] = f;
	f.v[0] = 69;	f.v[1] = 158;	f.v[2] = 159;	f.smGroup = 1;	f.flags = 2;	meshS.faces[88] = f;
	f.v[0] = 68;	f.v[1] = 69;	f.v[2] = 159;	f.smGroup = 1;	f.flags = 1;	meshS.faces[89] = f;
	f.v[0] = 68;	f.v[1] = 159;	f.v[2] = 160;	f.smGroup = 1;	f.flags = 2;	meshS.faces[90] = f;
	f.v[0] = 67;	f.v[1] = 68;	f.v[2] = 160;	f.smGroup = 1;	f.flags = 1;	meshS.faces[91] = f;
	f.v[0] = 67;	f.v[1] = 160;	f.v[2] = 161;	f.smGroup = 1;	f.flags = 2;	meshS.faces[92] = f;
	f.v[0] = 67;	f.v[1] = 161;	f.v[2] = 162;	f.smGroup = 1;	f.flags = 2;	meshS.faces[93] = f;
	f.v[0] = 66;	f.v[1] = 67;	f.v[2] = 162;	f.smGroup = 1;	f.flags = 1;	meshS.faces[94] = f;
	f.v[0] = 66;	f.v[1] = 162;	f.v[2] = 163;	f.smGroup = 1;	f.flags = 2;	meshS.faces[95] = f;
	f.v[0] = 65;	f.v[1] = 66;	f.v[2] = 163;	f.smGroup = 1;	f.flags = 1;	meshS.faces[96] = f;
	f.v[0] = 65;	f.v[1] = 163;	f.v[2] = 164;	f.smGroup = 1;	f.flags = 2;	meshS.faces[97] = f;
	f.v[0] = 65;	f.v[1] = 164;	f.v[2] = 165;	f.smGroup = 1;	f.flags = 2;	meshS.faces[98] = f;
	f.v[0] = 64;	f.v[1] = 65;	f.v[2] = 165;	f.smGroup = 1;	f.flags = 1;	meshS.faces[99] = f;
	f.v[0] = 64;	f.v[1] = 165;	f.v[2] = 166;	f.smGroup = 1;	f.flags = 2;	meshS.faces[100] = f;
	f.v[0] = 63;	f.v[1] = 64;	f.v[2] = 166;	f.smGroup = 1;	f.flags = 1;	meshS.faces[101] = f;
	f.v[0] = 63;	f.v[1] = 166;	f.v[2] = 167;	f.smGroup = 1;	f.flags = 2;	meshS.faces[102] = f;
	f.v[0] = 62;	f.v[1] = 63;	f.v[2] = 167;	f.smGroup = 1;	f.flags = 1;	meshS.faces[103] = f;
	f.v[0] = 62;	f.v[1] = 167;	f.v[2] = 168;	f.smGroup = 1;	f.flags = 2;	meshS.faces[104] = f;
	f.v[0] = 62;	f.v[1] = 168;	f.v[2] = 169;	f.smGroup = 1;	f.flags = 2;	meshS.faces[105] = f;
	f.v[0] = 61;	f.v[1] = 62;	f.v[2] = 169;	f.smGroup = 1;	f.flags = 1;	meshS.faces[106] = f;
	f.v[0] = 61;	f.v[1] = 169;	f.v[2] = 170;	f.smGroup = 1;	f.flags = 2;	meshS.faces[107] = f;
	f.v[0] = 60;	f.v[1] = 61;	f.v[2] = 170;	f.smGroup = 1;	f.flags = 1;	meshS.faces[108] = f;
	f.v[0] = 60;	f.v[1] = 170;	f.v[2] = 171;	f.smGroup = 1;	f.flags = 2;	meshS.faces[109] = f;
	f.v[0] = 59;	f.v[1] = 60;	f.v[2] = 171;	f.smGroup = 1;	f.flags = 1;	meshS.faces[110] = f;
	f.v[0] = 59;	f.v[1] = 171;	f.v[2] = 172;	f.smGroup = 1;	f.flags = 2;	meshS.faces[111] = f;
	f.v[0] = 58;	f.v[1] = 59;	f.v[2] = 172;	f.smGroup = 1;	f.flags = 1;	meshS.faces[112] = f;
	f.v[0] = 58;	f.v[1] = 172;	f.v[2] = 173;	f.smGroup = 1;	f.flags = 2;	meshS.faces[113] = f;
	f.v[0] = 57;	f.v[1] = 58;	f.v[2] = 173;	f.smGroup = 1;	f.flags = 1;	meshS.faces[114] = f;
	f.v[0] = 57;	f.v[1] = 173;	f.v[2] = 174;	f.smGroup = 1;	f.flags = 2;	meshS.faces[115] = f;
	f.v[0] = 57;	f.v[1] = 174;	f.v[2] = 175;	f.smGroup = 1;	f.flags = 2;	meshS.faces[116] = f;
	f.v[0] = 56;	f.v[1] = 57;	f.v[2] = 175;	f.smGroup = 1;	f.flags = 1;	meshS.faces[117] = f;
	f.v[0] = 56;	f.v[1] = 175;	f.v[2] = 176;	f.smGroup = 1;	f.flags = 2;	meshS.faces[118] = f;
	f.v[0] = 55;	f.v[1] = 56;	f.v[2] = 176;	f.smGroup = 1;	f.flags = 1;	meshS.faces[119] = f;
	f.v[0] = 54;	f.v[1] = 55;	f.v[2] = 176;	f.smGroup = 1;	f.flags = 1;	meshS.faces[120] = f;
	f.v[0] = 54;	f.v[1] = 176;	f.v[2] = 177;	f.smGroup = 1;	f.flags = 2;	meshS.faces[121] = f;
	f.v[0] = 53;	f.v[1] = 54;	f.v[2] = 177;	f.smGroup = 1;	f.flags = 1;	meshS.faces[122] = f;
	f.v[0] = 53;	f.v[1] = 177;	f.v[2] = 178;	f.smGroup = 1;	f.flags = 2;	meshS.faces[123] = f;
	f.v[0] = 52;	f.v[1] = 53;	f.v[2] = 178;	f.smGroup = 1;	f.flags = 1;	meshS.faces[124] = f;
	f.v[0] = 52;	f.v[1] = 178;	f.v[2] = 179;	f.smGroup = 1;	f.flags = 2;	meshS.faces[125] = f;
	f.v[0] = 51;	f.v[1] = 52;	f.v[2] = 179;	f.smGroup = 1;	f.flags = 1;	meshS.faces[126] = f;
	f.v[0] = 51;	f.v[1] = 179;	f.v[2] = 180;	f.smGroup = 1;	f.flags = 2;	meshS.faces[127] = f;
	f.v[0] = 50;	f.v[1] = 51;	f.v[2] = 180;	f.smGroup = 1;	f.flags = 1;	meshS.faces[128] = f;
	f.v[0] = 50;	f.v[1] = 180;	f.v[2] = 181;	f.smGroup = 1;	f.flags = 2;	meshS.faces[129] = f;
	f.v[0] = 49;	f.v[1] = 50;	f.v[2] = 181;	f.smGroup = 1;	f.flags = 1;	meshS.faces[130] = f;
	f.v[0] = 49;	f.v[1] = 181;	f.v[2] = 182;	f.smGroup = 1;	f.flags = 2;	meshS.faces[131] = f;
	f.v[0] = 48;	f.v[1] = 49;	f.v[2] = 182;	f.smGroup = 1;	f.flags = 1;	meshS.faces[132] = f;
	f.v[0] = 48;	f.v[1] = 182;	f.v[2] = 183;	f.smGroup = 1;	f.flags = 2;	meshS.faces[133] = f;
	f.v[0] = 47;	f.v[1] = 48;	f.v[2] = 183;	f.smGroup = 1;	f.flags = 1;	meshS.faces[134] = f;
	f.v[0] = 46;	f.v[1] = 47;	f.v[2] = 183;	f.smGroup = 1;	f.flags = 1;	meshS.faces[135] = f;
	f.v[0] = 46;	f.v[1] = 183;	f.v[2] = 184;	f.smGroup = 1;	f.flags = 2;	meshS.faces[136] = f;
	f.v[0] = 45;	f.v[1] = 46;	f.v[2] = 184;	f.smGroup = 1;	f.flags = 1;	meshS.faces[137] = f;
	f.v[0] = 45;	f.v[1] = 184;	f.v[2] = 185;	f.smGroup = 1;	f.flags = 2;	meshS.faces[138] = f;
	f.v[0] = 44;	f.v[1] = 45;	f.v[2] = 185;	f.smGroup = 1;	f.flags = 1;	meshS.faces[139] = f;
	f.v[0] = 44;	f.v[1] = 185;	f.v[2] = 186;	f.smGroup = 1;	f.flags = 2;	meshS.faces[140] = f;
	f.v[0] = 43;	f.v[1] = 44;	f.v[2] = 186;	f.smGroup = 1;	f.flags = 1;	meshS.faces[141] = f;
	f.v[0] = 43;	f.v[1] = 186;	f.v[2] = 187;	f.smGroup = 1;	f.flags = 2;	meshS.faces[142] = f;
	f.v[0] = 42;	f.v[1] = 43;	f.v[2] = 187;	f.smGroup = 1;	f.flags = 1;	meshS.faces[143] = f;
	f.v[0] = 42;	f.v[1] = 187;	f.v[2] = 188;	f.smGroup = 1;	f.flags = 2;	meshS.faces[144] = f;
	f.v[0] = 41;	f.v[1] = 42;	f.v[2] = 188;	f.smGroup = 1;	f.flags = 1;	meshS.faces[145] = f;
	f.v[0] = 41;	f.v[1] = 188;	f.v[2] = 189;	f.smGroup = 1;	f.flags = 2;	meshS.faces[146] = f;
	f.v[0] = 40;	f.v[1] = 41;	f.v[2] = 189;	f.smGroup = 1;	f.flags = 1;	meshS.faces[147] = f;
	f.v[0] = 40;	f.v[1] = 189;	f.v[2] = 190;	f.smGroup = 1;	f.flags = 2;	meshS.faces[148] = f;
	f.v[0] = 39;	f.v[1] = 40;	f.v[2] = 190;	f.smGroup = 1;	f.flags = 1;	meshS.faces[149] = f;
	f.v[0] = 38;	f.v[1] = 39;	f.v[2] = 190;	f.smGroup = 1;	f.flags = 1;	meshS.faces[150] = f;
	f.v[0] = 38;	f.v[1] = 190;	f.v[2] = 191;	f.smGroup = 1;	f.flags = 2;	meshS.faces[151] = f;
	f.v[0] = 37;	f.v[1] = 38;	f.v[2] = 191;	f.smGroup = 1;	f.flags = 1;	meshS.faces[152] = f;
	f.v[0] = 37;	f.v[1] = 191;	f.v[2] = 192;	f.smGroup = 1;	f.flags = 2;	meshS.faces[153] = f;
	f.v[0] = 36;	f.v[1] = 37;	f.v[2] = 192;	f.smGroup = 1;	f.flags = 1;	meshS.faces[154] = f;
	f.v[0] = 36;	f.v[1] = 192;	f.v[2] = 193;	f.smGroup = 1;	f.flags = 2;	meshS.faces[155] = f;
	f.v[0] = 35;	f.v[1] = 36;	f.v[2] = 193;	f.smGroup = 1;	f.flags = 1;	meshS.faces[156] = f;
	f.v[0] = 35;	f.v[1] = 193;	f.v[2] = 194;	f.smGroup = 1;	f.flags = 2;	meshS.faces[157] = f;
	f.v[0] = 34;	f.v[1] = 35;	f.v[2] = 194;	f.smGroup = 1;	f.flags = 1;	meshS.faces[158] = f;
	f.v[0] = 34;	f.v[1] = 194;	f.v[2] = 195;	f.smGroup = 1;	f.flags = 2;	meshS.faces[159] = f;
	f.v[0] = 33;	f.v[1] = 34;	f.v[2] = 195;	f.smGroup = 1;	f.flags = 1;	meshS.faces[160] = f;
	f.v[0] = 33;	f.v[1] = 195;	f.v[2] = 196;	f.smGroup = 1;	f.flags = 2;	meshS.faces[161] = f;
	f.v[0] = 32;	f.v[1] = 33;	f.v[2] = 196;	f.smGroup = 1;	f.flags = 1;	meshS.faces[162] = f;
	f.v[0] = 32;	f.v[1] = 196;	f.v[2] = 197;	f.smGroup = 1;	f.flags = 2;	meshS.faces[163] = f;
	f.v[0] = 31;	f.v[1] = 32;	f.v[2] = 197;	f.smGroup = 1;	f.flags = 1;	meshS.faces[164] = f;
	f.v[0] = 31;	f.v[1] = 197;	f.v[2] = 198;	f.smGroup = 1;	f.flags = 2;	meshS.faces[165] = f;
	f.v[0] = 30;	f.v[1] = 31;	f.v[2] = 198;	f.smGroup = 1;	f.flags = 1;	meshS.faces[166] = f;
	f.v[0] = 30;	f.v[1] = 198;	f.v[2] = 199;	f.smGroup = 1;	f.flags = 2;	meshS.faces[167] = f;
	f.v[0] = 29;	f.v[1] = 30;	f.v[2] = 199;	f.smGroup = 1;	f.flags = 1;	meshS.faces[168] = f;
	f.v[0] = 29;	f.v[1] = 199;	f.v[2] = 200;	f.smGroup = 1;	f.flags = 2;	meshS.faces[169] = f;
	f.v[0] = 28;	f.v[1] = 29;	f.v[2] = 200;	f.smGroup = 1;	f.flags = 1;	meshS.faces[170] = f;
	f.v[0] = 28;	f.v[1] = 200;	f.v[2] = 201;	f.smGroup = 1;	f.flags = 2;	meshS.faces[171] = f;
	f.v[0] = 27;	f.v[1] = 28;	f.v[2] = 201;	f.smGroup = 1;	f.flags = 1;	meshS.faces[172] = f;
	f.v[0] = 27;	f.v[1] = 201;	f.v[2] = 202;	f.smGroup = 1;	f.flags = 2;	meshS.faces[173] = f;
	f.v[0] = 26;	f.v[1] = 27;	f.v[2] = 202;	f.smGroup = 1;	f.flags = 1;	meshS.faces[174] = f;
	f.v[0] = 26;	f.v[1] = 202;	f.v[2] = 203;	f.smGroup = 1;	f.flags = 2;	meshS.faces[175] = f;
	f.v[0] = 25;	f.v[1] = 26;	f.v[2] = 203;	f.smGroup = 1;	f.flags = 1;	meshS.faces[176] = f;
	f.v[0] = 25;	f.v[1] = 203;	f.v[2] = 204;	f.smGroup = 1;	f.flags = 2;	meshS.faces[177] = f;
	f.v[0] = 25;	f.v[1] = 204;	f.v[2] = 205;	f.smGroup = 1;	f.flags = 2;	meshS.faces[178] = f;
	f.v[0] = 24;	f.v[1] = 25;	f.v[2] = 205;	f.smGroup = 1;	f.flags = 1;	meshS.faces[179] = f;
	f.v[0] = 24;	f.v[1] = 205;	f.v[2] = 206;	f.smGroup = 1;	f.flags = 2;	meshS.faces[180] = f;
	f.v[0] = 23;	f.v[1] = 24;	f.v[2] = 206;	f.smGroup = 1;	f.flags = 1;	meshS.faces[181] = f;
	f.v[0] = 23;	f.v[1] = 206;	f.v[2] = 207;	f.smGroup = 1;	f.flags = 2;	meshS.faces[182] = f;
	f.v[0] = 22;	f.v[1] = 23;	f.v[2] = 207;	f.smGroup = 1;	f.flags = 1;	meshS.faces[183] = f;
	f.v[0] = 22;	f.v[1] = 207;	f.v[2] = 208;	f.smGroup = 1;	f.flags = 2;	meshS.faces[184] = f;
	f.v[0] = 21;	f.v[1] = 22;	f.v[2] = 208;	f.smGroup = 1;	f.flags = 1;	meshS.faces[185] = f;
	f.v[0] = 21;	f.v[1] = 208;	f.v[2] = 209;	f.smGroup = 1;	f.flags = 2;	meshS.faces[186] = f;
	f.v[0] = 20;	f.v[1] = 21;	f.v[2] = 209;	f.smGroup = 1;	f.flags = 1;	meshS.faces[187] = f;
	f.v[0] = 20;	f.v[1] = 209;	f.v[2] = 210;	f.smGroup = 1;	f.flags = 2;	meshS.faces[188] = f;
	f.v[0] = 19;	f.v[1] = 20;	f.v[2] = 210;	f.smGroup = 1;	f.flags = 1;	meshS.faces[189] = f;
	f.v[0] = 19;	f.v[1] = 210;	f.v[2] = 211;	f.smGroup = 1;	f.flags = 2;	meshS.faces[190] = f;
	f.v[0] = 18;	f.v[1] = 19;	f.v[2] = 211;	f.smGroup = 1;	f.flags = 1;	meshS.faces[191] = f;
	f.v[0] = 18;	f.v[1] = 211;	f.v[2] = 212;	f.smGroup = 1;	f.flags = 2;	meshS.faces[192] = f;
	f.v[0] = 18;	f.v[1] = 212;	f.v[2] = 213;	f.smGroup = 1;	f.flags = 2;	meshS.faces[193] = f;
	f.v[0] = 17;	f.v[1] = 18;	f.v[2] = 213;	f.smGroup = 1;	f.flags = 1;	meshS.faces[194] = f;
	f.v[0] = 17;	f.v[1] = 213;	f.v[2] = 214;	f.smGroup = 1;	f.flags = 2;	meshS.faces[195] = f;
	f.v[0] = 16;	f.v[1] = 17;	f.v[2] = 214;	f.smGroup = 1;	f.flags = 1;	meshS.faces[196] = f;
	f.v[0] = 16;	f.v[1] = 214;	f.v[2] = 215;	f.smGroup = 1;	f.flags = 2;	meshS.faces[197] = f;
	f.v[0] = 15;	f.v[1] = 16;	f.v[2] = 215;	f.smGroup = 1;	f.flags = 1;	meshS.faces[198] = f;
	f.v[0] = 15;	f.v[1] = 215;	f.v[2] = 216;	f.smGroup = 1;	f.flags = 2;	meshS.faces[199] = f;
	f.v[0] = 14;	f.v[1] = 15;	f.v[2] = 216;	f.smGroup = 1;	f.flags = 1;	meshS.faces[200] = f;
	f.v[0] = 14;	f.v[1] = 216;	f.v[2] = 217;	f.smGroup = 1;	f.flags = 2;	meshS.faces[201] = f;
	f.v[0] = 13;	f.v[1] = 14;	f.v[2] = 217;	f.smGroup = 1;	f.flags = 1;	meshS.faces[202] = f;
	f.v[0] = 13;	f.v[1] = 217;	f.v[2] = 218;	f.smGroup = 1;	f.flags = 2;	meshS.faces[203] = f;
	f.v[0] = 12;	f.v[1] = 13;	f.v[2] = 218;	f.smGroup = 1;	f.flags = 1;	meshS.faces[204] = f;
	f.v[0] = 12;	f.v[1] = 218;	f.v[2] = 219;	f.smGroup = 1;	f.flags = 2;	meshS.faces[205] = f;
	f.v[0] = 11;	f.v[1] = 12;	f.v[2] = 219;	f.smGroup = 1;	f.flags = 1;	meshS.faces[206] = f;
	f.v[0] = 11;	f.v[1] = 219;	f.v[2] = 220;	f.smGroup = 1;	f.flags = 2;	meshS.faces[207] = f;
	f.v[0] = 10;	f.v[1] = 11;	f.v[2] = 220;	f.smGroup = 1;	f.flags = 1;	meshS.faces[208] = f;
	f.v[0] = 10;	f.v[1] = 220;	f.v[2] = 221;	f.smGroup = 1;	f.flags = 2;	meshS.faces[209] = f;
	f.v[0] = 9;	f.v[1] = 10;	f.v[2] = 221;	f.smGroup = 1;	f.flags = 1;	meshS.faces[210] = f;
	f.v[0] = 9;	f.v[1] = 221;	f.v[2] = 222;	f.smGroup = 1;	f.flags = 2;	meshS.faces[211] = f;
	f.v[0] = 8;	f.v[1] = 9;	f.v[2] = 222;	f.smGroup = 1;	f.flags = 1;	meshS.faces[212] = f;
	f.v[0] = 8;	f.v[1] = 222;	f.v[2] = 223;	f.smGroup = 1;	f.flags = 2;	meshS.faces[213] = f;
	f.v[0] = 7;	f.v[1] = 8;	f.v[2] = 223;	f.smGroup = 1;	f.flags = 1;	meshS.faces[214] = f;
	f.v[0] = 7;	f.v[1] = 223;	f.v[2] = 224;	f.smGroup = 1;	f.flags = 2;	meshS.faces[215] = f;
	f.v[0] = 6;	f.v[1] = 7;	f.v[2] = 224;	f.smGroup = 1;	f.flags = 1;	meshS.faces[216] = f;
	f.v[0] = 5;	f.v[1] = 6;	f.v[2] = 224;	f.smGroup = 1;	f.flags = 1;	meshS.faces[217] = f;
	f.v[0] = 5;	f.v[1] = 224;	f.v[2] = 225;	f.smGroup = 1;	f.flags = 2;	meshS.faces[218] = f;
	f.v[0] = 4;	f.v[1] = 5;	f.v[2] = 225;	f.smGroup = 1;	f.flags = 1;	meshS.faces[219] = f;
	f.v[0] = 4;	f.v[1] = 225;	f.v[2] = 226;	f.smGroup = 1;	f.flags = 2;	meshS.faces[220] = f;
	f.v[0] = 3;	f.v[1] = 4;	f.v[2] = 226;	f.smGroup = 1;	f.flags = 1;	meshS.faces[221] = f;
	f.v[0] = 3;	f.v[1] = 226;	f.v[2] = 227;	f.smGroup = 1;	f.flags = 2;	meshS.faces[222] = f;
	f.v[0] = 2;	f.v[1] = 3;	f.v[2] = 227;	f.smGroup = 1;	f.flags = 1;	meshS.faces[223] = f;
	f.v[0] = 2;	f.v[1] = 227;	f.v[2] = 228;	f.smGroup = 1;	f.flags = 2;	meshS.faces[224] = f;
	f.v[0] = 1;	f.v[1] = 2;	f.v[2] = 228;	f.smGroup = 1;	f.flags = 1;	meshS.faces[225] = f;
	f.v[0] = 1;	f.v[1] = 228;	f.v[2] = 229;	f.smGroup = 1;	f.flags = 2;	meshS.faces[226] = f;
	f.v[0] = 0;	f.v[1] = 1;	f.v[2] = 229;	f.smGroup = 1;	f.flags = 1;	meshS.faces[227] = f;
	f.v[0] = 0;	f.v[1] = 229;	f.v[2] = 230;	f.smGroup = 1;	f.flags = 2;	meshS.faces[228] = f;
	f.v[0] = 232;	f.v[1] = 0;	f.v[2] = 230;	f.smGroup = 1;	f.flags = 1;	meshS.faces[229] = f;
	f.v[0] = 231;	f.v[1] = 232;	f.v[2] = 230;	f.smGroup = 1;	f.flags = 5;	meshS.faces[230] = f;

	meshS.buildNormals();

	//-----------------------------------

	meshW.setNumVerts(12);
	meshW.setNumFaces(10);

	meshW.setVert(0, Point3(23.85f, 0.0f, 0.0f ));
	meshW.setVert(1, Point3(23.85f, 7.6f, 0.0f ));
	meshW.setVert(2, Point3(-15.35f, 7.6f, 0.0f ));
	meshW.setVert(3, Point3(-15.35f, 29.4f, 0.0f ));
	meshW.setVert(4, Point3(19.95f, 29.4f, 0.0f ));
	meshW.setVert(5, Point3(19.95f, 37.0f, 0.0f ));
	meshW.setVert(6, Point3(-15.35f, 37.0f, 0.0f ));
	meshW.setVert(7, Point3(-15.35f, 56.5f, 0.0f ));
	meshW.setVert(8, Point3(22.35f, 56.5f, 0.0f ));
	meshW.setVert(9, Point3(22.35f, 64.1f, 0.0f ));
	meshW.setVert(10, Point3(-23.85f, 64.1f, 0.0f ));
	meshW.setVert(11, Point3(-23.85f, 0.0f, 0.0f ));
	for (int i=0; i<meshW.getNumVerts(); i++)
		meshW.verts[i] *= lScale;

	f.v[0] = 8;	f.v[1] = 9;	f.v[2] = 10;	f.smGroup = 1;	f.flags = 3;	meshW.faces[0] = f;
	f.v[0] = 7;	f.v[1] = 8;	f.v[2] = 10;	f.smGroup = 1;	f.flags = 1;	meshW.faces[1] = f;
	f.v[0] = 7;	f.v[1] = 10;	f.v[2] = 11;	f.smGroup = 1;	f.flags = 2;	meshW.faces[2] = f;
	f.v[0] = 6;	f.v[1] = 7;	f.v[2] = 11;	f.smGroup = 1;	f.flags = 1;	meshW.faces[3] = f;
	f.v[0] = 4;	f.v[1] = 5;	f.v[2] = 6;	f.smGroup = 1;	f.flags = 3;	meshW.faces[4] = f;
	f.v[0] = 3;	f.v[1] = 4;	f.v[2] = 6;	f.smGroup = 1;	f.flags = 1;	meshW.faces[5] = f;
	f.v[0] = 3;	f.v[1] = 6;	f.v[2] = 11;	f.smGroup = 1;	f.flags = 0;	meshW.faces[6] = f;
	f.v[0] = 2;	f.v[1] = 3;	f.v[2] = 11;	f.smGroup = 1;	f.flags = 1;	meshW.faces[7] = f;
	f.v[0] = 2;	f.v[1] = 11;	f.v[2] = 0;	f.smGroup = 1;	f.flags = 2;	meshW.faces[8] = f;
	f.v[0] = 1;	f.v[1] = 2;	f.v[2] = 0;	f.smGroup = 1;	f.flags = 5;	meshW.faces[9] = f;

	meshW.buildNormals();

	//-----------------------------------

	meshE.setNumVerts(25);
	meshE.setNumFaces(23);

	meshE.setVert(0, Point3(-15.4937f, 0.0f, 0.0f ));
	meshE.setVert(1, Point3(-2.01094f, 48.8266f, 0.0f ));
	meshE.setVert(2, Point3(-0.0499992f, 56.3609f, 0.0f ));
	meshE.setVert(3, Point3(2.02031f, 48.8406f, 0.0f ));
	meshE.setVert(4, Point3(15.4672f, 0.0f, 0.0f ));
	meshE.setVert(5, Point3(23.7656f, 0.0f, 0.0f ));
	meshE.setVert(6, Point3(41.2f, 64.1f, 0.0f ));
	meshE.setVert(7, Point3(32.6781f, 64.1f, 0.0f ));
	meshE.setVert(8, Point3(22.6859f, 22.8781f, 0.0f ));
	meshE.setVert(9, Point3(19.45f, 8.96406f, 0.0f ));
	meshE.setVert(10, Point3(18.958f, 12.0252f, 0.0f ));
	meshE.setVert(11, Point3(18.3917f, 15.1459f, 0.0f ));
	meshE.setVert(12, Point3(17.7512f, 18.3264f, 0.0f ));
	meshE.setVert(13, Point3(17.0365f, 21.5666f, 0.0f ));
	meshE.setVert(14, Point3(16.2475f, 24.8665f, 0.0f ));
	meshE.setVert(15, Point3(15.3843f, 28.2261f, 0.0f ));
	meshE.setVert(16, Point3(14.4469f, 31.6453f, 0.0f ));
	meshE.setVert(17, Point3(5.28125f, 64.1f, 0.0f ));
	meshE.setVert(18, Point3(-4.94218f, 64.1f, 0.0f ));
	meshE.setVert(19, Point3(-17.1219f, 20.7875f, 0.0f ));
	meshE.setVert(20, Point3(-20.05f, 8.96406f, 0.0f ));
	meshE.setVert(21, Point3(-22.7828f, 22.0656f, 0.0f ));
	meshE.setVert(22, Point3(-32.4109f, 64.1f, 0.0f ));
	meshE.setVert(23, Point3(-41.2f, 64.1f, 0.0f ));
	meshE.setVert(24, Point3(-24.3703f, 0.0f, 0.0f ));
	for (int i=0; i<meshE.getNumVerts(); i++)
		meshE.verts[i] *= lScale;

	f.v[0] = 5;	f.v[1] = 6;	f.v[2] = 7;	f.smGroup = 1;	f.flags = 3;	meshE.faces[0] = f;
	f.v[0] = 5;	f.v[1] = 7;	f.v[2] = 8;	f.smGroup = 1;	f.flags = 2;	meshE.faces[1] = f;
	f.v[0] = 5;	f.v[1] = 8;	f.v[2] = 9;	f.smGroup = 1;	f.flags = 2;	meshE.faces[2] = f;
	f.v[0] = 4;	f.v[1] = 5;	f.v[2] = 9;	f.smGroup = 1;	f.flags = 1;	meshE.faces[3] = f;
	f.v[0] = 3;	f.v[1] = 4;	f.v[2] = 9;	f.smGroup = 1;	f.flags = 1;	meshE.faces[4] = f;
	f.v[0] = 3;	f.v[1] = 9;	f.v[2] = 10;	f.smGroup = 1;	f.flags = 2;	meshE.faces[5] = f;
	f.v[0] = 3;	f.v[1] = 10;	f.v[2] = 11;	f.smGroup = 1;	f.flags = 2;	meshE.faces[6] = f;
	f.v[0] = 3;	f.v[1] = 11;	f.v[2] = 12;	f.smGroup = 1;	f.flags = 2;	meshE.faces[7] = f;
	f.v[0] = 3;	f.v[1] = 12;	f.v[2] = 13;	f.smGroup = 1;	f.flags = 2;	meshE.faces[8] = f;
	f.v[0] = 3;	f.v[1] = 13;	f.v[2] = 14;	f.smGroup = 1;	f.flags = 2;	meshE.faces[9] = f;
	f.v[0] = 3;	f.v[1] = 14;	f.v[2] = 15;	f.smGroup = 1;	f.flags = 2;	meshE.faces[10] = f;
	f.v[0] = 3;	f.v[1] = 15;	f.v[2] = 16;	f.smGroup = 1;	f.flags = 2;	meshE.faces[11] = f;
	f.v[0] = 3;	f.v[1] = 16;	f.v[2] = 17;	f.smGroup = 1;	f.flags = 2;	meshE.faces[12] = f;
	f.v[0] = 2;	f.v[1] = 3;	f.v[2] = 17;	f.smGroup = 1;	f.flags = 1;	meshE.faces[13] = f;
	f.v[0] = 2;	f.v[1] = 17;	f.v[2] = 18;	f.smGroup = 1;	f.flags = 2;	meshE.faces[14] = f;
	f.v[0] = 2;	f.v[1] = 18;	f.v[2] = 19;	f.smGroup = 1;	f.flags = 2;	meshE.faces[15] = f;
	f.v[0] = 1;	f.v[1] = 2;	f.v[2] = 19;	f.smGroup = 1;	f.flags = 1;	meshE.faces[16] = f;
	f.v[0] = 0;	f.v[1] = 1;	f.v[2] = 19;	f.smGroup = 1;	f.flags = 1;	meshE.faces[17] = f;
	f.v[0] = 0;	f.v[1] = 19;	f.v[2] = 20;	f.smGroup = 1;	f.flags = 2;	meshE.faces[18] = f;
	f.v[0] = 24;	f.v[1] = 0;	f.v[2] = 20;	f.smGroup = 1;	f.flags = 1;	meshE.faces[19] = f;
	f.v[0] = 23;	f.v[1] = 24;	f.v[2] = 20;	f.smGroup = 1;	f.flags = 1;	meshE.faces[20] = f;
	f.v[0] = 23;	f.v[1] = 20;	f.v[2] = 21;	f.smGroup = 1;	f.flags = 2;	meshE.faces[21] = f;
	f.v[0] = 23;	f.v[1] = 21;	f.v[2] = 22;	f.smGroup = 1;	f.flags = 6;	meshE.faces[22] = f;
	meshE.buildNormals();

	//-----------------------------------

	targMesh.setNumVerts(9);
	targMesh.setNumFaces(8);
	float cR = 32;
	float cr = 6;
	targMesh.setVert(0, Point3(0.0f, 0.0f, 0.0f));
	targMesh.setVert(1, Point3( -cR, 0.0f, 0.0f));
	targMesh.setVert(2, Point3( -cr,   cr, 0.0f));
	targMesh.setVert(3, Point3(0.0f,   cR, 0.0f));
	targMesh.setVert(4, Point3(  cr,   cr, 0.0f));
	targMesh.setVert(5, Point3(  cR, 0.0f, 0.0f));
	targMesh.setVert(6, Point3(  cr,  -cr, 0.0f));
	targMesh.setVert(7, Point3(0.0f,  -cR, 0.0f));
	targMesh.setVert(8, Point3( -cr,  -cr, 0.0f));
	targMesh.faces[0].setVerts(1,0,2);
	targMesh.faces[1].setVerts(2,0,3);
	targMesh.faces[2].setVerts(3,0,4);
	targMesh.faces[3].setVerts(4,0,5);
	targMesh.faces[4].setVerts(5,0,6);
	targMesh.faces[5].setVerts(6,0,7);
	targMesh.faces[6].setVerts(7,0,8);
	targMesh.faces[7].setVerts(8,0,1);
	targMesh.buildNormals();
	targMesh.EnableEdgeList(1);

	for (int i=0; i<8; i++)
		targMesh.faces[i].setEdgeVisFlags(0,1,1); 

}

void NOXSunTarget::RebuildSphereMesh(float scale)
{
	scale = 1000;
	int xSegs = 64;
	int ySegs = 32;
	float vxa[64];
	float vya[64];
	for (int i=0; i<xSegs; i++)
	{
		vxa[i] = sin(i/(float)xSegs*2*PI) * scale;
		vya[i] = cos(i/(float)xSegs*2*PI) * scale;
	}
	
	sphMesh.setNumVerts(xSegs*(ySegs+1));
	sphMesh.setNumTVerts((xSegs+1)*(ySegs+1));
	sphMesh.setMapSupport(2, TRUE);
	sphMesh.setNumMapVerts(2, (xSegs+1)*(ySegs+1));
	for (int y=0; y<=ySegs; y++)
	{
		float costh = cos(y/(float)ySegs*PI);
		float sinth = sin(y/(float)ySegs*PI);
		for (int x=0; x<=xSegs; x++)
		{
			float fx = vxa[x] * sinth;
			float fy = vya[x] * sinth;
			float fz = costh * scale;

			float tx = x/(float)xSegs;
			float ty = y/(float)ySegs;
			float tz = 0.0f;

			if (x<xSegs)
				sphMesh.setVert(x+y*xSegs, Point3(fx, fy, fz));
			sphMesh.setTVert(x+y*(xSegs+1), Point3(tx, ty, tz));
			//sphMesh.mapVerts(2)[x+y*(xSegs+1)] = UVVert(tx,ty,tz);
			sphMesh.setMapVert(2, x+y*(xSegs+1), UVVert(tx,ty,tz));
		}
	}

	sphMesh.setNumFaces(xSegs * ySegs * 2);
	sphMesh.setNumTVFaces(xSegs * ySegs * 2);

	sphMesh.setNumMapFaces(2, xSegs*ySegs*2);

	for (int y=0; y<ySegs; y++)
	{
		for (int x=0; x<xSegs; x++)
		{
			int f = y*xSegs + x;
			int v1 = y*xSegs + x;
			int v2 = y*xSegs + ((x+1)%xSegs);
			int v3 = (y+1)*xSegs + ((x+1)%xSegs);
			int v4 = (y+1)*xSegs + x;
			int t1 = y*(xSegs+1) + x;
			int t2 = y*(xSegs+1) + (x+1);
			int t3 = (y+1)*(xSegs+1) + (x+1);
			int t4 = (y+1)*(xSegs+1) + x;
			sphMesh.faces[2*f+0].setVerts(v1,v2,v3);
			sphMesh.faces[2*f+1].setVerts(v1,v3,v4);
			sphMesh.faces[2*f+0].setEdgeVisFlags(1,1,0); 
			sphMesh.faces[2*f+1].setEdgeVisFlags(0,0,0); 
			sphMesh.tvFace[2*f+0].setTVerts(t1,t2,t3);
			sphMesh.tvFace[2*f+1].setTVerts(t1,t3,t4);

			sphMesh.mapFaces(2)[2*f+0].setTVerts(t1,t2,t3);
			sphMesh.mapFaces(2)[2*f+1].setTVerts(t1,t3,t4);
		}
	}

	sphMesh.buildNormals();
}

void NOXSunTarget::deleteMeshes()
{
}

