#include <windows.h>
#include <stdio.h>
#include "nox_update.h"

extern HINSTANCE hInst;
extern bool isSystem64;

bool m3ds9_32 = false;
bool m3ds9_64 = false;
bool m3ds2008_32 = false;
bool m3ds2008_64 = false;
bool m3ds2009_32 = false;
bool m3ds2009_64 = false;
bool m3ds2010_32 = false;
bool m3ds2010_64 = false;
bool m3ds2011_32 = false;
bool m3ds2011_64 = false;
bool m3ds2012_32 = false;
bool m3ds2012_64 = false;
bool m3ds2013_32 = false;
bool m3ds2013_64 = false;
bool m3ds2014_64 = false;
char * dir_3dsmax_9_32 = NULL;
char * dir_3dsmax_9_64 = NULL;
char * dir_3dsmax_2008_32 = NULL;
char * dir_3dsmax_2008_64 = NULL;
char * dir_3dsmax_2009_32 = NULL;
char * dir_3dsmax_2009_64 = NULL;
char * dir_3dsmax_2010_32 = NULL;
char * dir_3dsmax_2010_64 = NULL;
char * dir_3dsmax_2011_32 = NULL;
char * dir_3dsmax_2011_64 = NULL;
char * dir_3dsmax_2012_32 = NULL;
char * dir_3dsmax_2012_64 = NULL;
char * dir_3dsmax_2013_32 = NULL;
char * dir_3dsmax_2013_64 = NULL;
char * dir_3dsmax_2014_64 = NULL;



//-------------------------------------------------------------------------------------------------------------

char * check3dsMaxDir(char * regPath, bool wow64)
{
	LONG regres;
	HKEY key1;
	if (wow64)
		regres = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regPath, 0, KEY_READ | KEY_WOW64_64KEY, &key1);
	else
		regres = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regPath, 0, KEY_READ , &key1);
	if (regres == ERROR_SUCCESS)
	{
		char buffer[2048];
		ZeroMemory(buffer, 2048);
		buffer[0] = 0;
		DWORD rsize = 2048;
		DWORD type;
		regres = RegQueryValueEx(key1, "Installdir", NULL, &type, (BYTE*)buffer, &rsize);
		RegCloseKey(key1);
		if (regres!=ERROR_SUCCESS  ||  buffer[0]==0)
			return NULL;
		int ll = (int)strlen(buffer);
		char * result = (char*)malloc(ll+1);
		sprintf_s(result, ll+1, "%s", buffer);
		return result;
	}
	return NULL;
}

//-------------------------------------------------------------------------------------------------------------

void recognize3dsmaxVersions(bool is64system)
{
	if (is64system)
	{
		dir_3dsmax_9_32		= check3dsMaxDir("SOFTWARE\\Wow6432Node\\Autodesk\\3dsmax\\9.0\\MAX-1:409", false);
		dir_3dsmax_9_64		= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\9.0\\MAX-1:409", true);
		dir_3dsmax_2008_32	= check3dsMaxDir("SOFTWARE\\Wow6432Node\\Autodesk\\3dsmax\\10.0\\MAX-1:409", false);
		dir_3dsmax_2008_64	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\10.0\\MAX-1:409", true);
		dir_3dsmax_2009_32	= check3dsMaxDir("SOFTWARE\\Wow6432Node\\Autodesk\\3dsmax\\11.0\\MAX-1:409", false);
		dir_3dsmax_2009_64	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\11.0\\MAX-1:409", true);
		dir_3dsmax_2010_32	= check3dsMaxDir("SOFTWARE\\Wow6432Node\\Autodesk\\3dsmax\\12.0\\MAX-1:409", false);
		dir_3dsmax_2010_64	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\12.0\\MAX-1:409", true);
		dir_3dsmax_2011_32	= check3dsMaxDir("SOFTWARE\\Wow6432Node\\Autodesk\\3dsmax\\13.0\\MAX-1:409", false);
		dir_3dsmax_2011_64	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\13.0\\MAX-1:409", true);
		dir_3dsmax_2012_32	= check3dsMaxDir("SOFTWARE\\Wow6432Node\\Autodesk\\3dsmax\\14.0\\MAX-1:409", false);
		dir_3dsmax_2012_64	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\14.0\\MAX-1:409", true);
		dir_3dsmax_2013_32	= check3dsMaxDir("SOFTWARE\\Wow6432Node\\Autodesk\\3dsmax\\15.0", false);
		dir_3dsmax_2013_64	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\15.0", true);
		dir_3dsmax_2014_64	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\16.0", true);
	}
	else
	{
		dir_3dsmax_9_32		= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\9.0\\MAX-1:409", false);
		dir_3dsmax_9_64		= NULL; 
		dir_3dsmax_2008_32	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\10.0\\MAX-1:409", false);
		dir_3dsmax_2008_64	= NULL;
		dir_3dsmax_2009_32	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\11.0\\MAX-1:409", false);
		dir_3dsmax_2009_64	= NULL;
		dir_3dsmax_2010_32	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\12.0\\MAX-1:409", false);
		dir_3dsmax_2010_64	= NULL;
		dir_3dsmax_2011_32	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\13.0\\MAX-1:409", false);
		dir_3dsmax_2011_64	= NULL;
		dir_3dsmax_2012_32	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\14.0\\MAX-1:409", false);
		dir_3dsmax_2012_64	= NULL;
		dir_3dsmax_2013_32	= check3dsMaxDir("SOFTWARE\\Autodesk\\3dsmax\\15.0", false);
		dir_3dsmax_2013_64	= NULL;
		dir_3dsmax_2014_64	= NULL;
	}

	m3ds9_32 =		(NULL!=dir_3dsmax_9_32);
	m3ds9_64 =		(NULL!=dir_3dsmax_9_64);
	m3ds2008_32 =	(NULL!=dir_3dsmax_2008_32);
	m3ds2008_64 =	(NULL!=dir_3dsmax_2008_64);
	m3ds2009_32 =	(NULL!=dir_3dsmax_2009_32);
	m3ds2009_64 =	(NULL!=dir_3dsmax_2009_64);
	m3ds2010_32 =	(NULL!=dir_3dsmax_2010_32);
	m3ds2010_64 =	(NULL!=dir_3dsmax_2010_64);
	m3ds2011_32 =	(NULL!=dir_3dsmax_2011_32);
	m3ds2011_64 =	(NULL!=dir_3dsmax_2011_64);
	m3ds2012_32 =	(NULL!=dir_3dsmax_2012_32);
	m3ds2012_64 =	(NULL!=dir_3dsmax_2012_64);
	m3ds2013_32 =	(NULL!=dir_3dsmax_2013_32);
	m3ds2013_64 =	(NULL!=dir_3dsmax_2013_64);
	m3ds2014_64 =	(NULL!=dir_3dsmax_2013_64);
}

//-------------------------------------------------------------------------------------------------------------
