#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>

extern ThemeManager gTheme;
extern HBITMAP gIcons;

INT_PTR CALLBACK RendererMainWindow::RegionsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				HWND hAirTrack = GetDlgItem(hWnd, IDC_REND_REGIONS_AIRSIZE);
				EMEditTrackBar * emtb = GetEMEditTrackBarInstance(hAirTrack);
				theme.apply(emtb);
				emtb->setEditBoxWidth(24);
				emtb->setValuesToInt(10, NOX_RGN_AIRSIZE_MIN, NOX_RGN_AIRSIZE_MAX, 5);

				HWND hPriority = GetDlgItem(hWnd, IDC_REND_REGIONS_PRIORITY);
				emtb = GetEMEditTrackBarInstance(hPriority);
				theme.apply(emtb);
				emtb->setEditBoxWidth(24);
				emtb->setValuesToInt(100, 0, 100, 5);

				HWND hAir    = GetDlgItem(hWnd, IDC_REND_REGIONS_AIRBRUSH);
				HWND hRect   = GetDlgItem(hWnd, IDC_REND_REGIONS_RECT);
				HWND hHand   = GetDlgItem(hWnd, IDC_REND_REGIONS_HANDTOOL);
				HWND hFill   = GetDlgItem(hWnd, IDC_REND_REGIONS_FILL);
				HWND hClear  = GetDlgItem(hWnd, IDC_REND_REGIONS_CLEAR);
				HWND hInvert = GetDlgItem(hWnd, IDC_REND_REGIONS_INVERT);

				EMImgStateButton * emHand  = GetEMImgStateButtonInstance(hHand);
				EMImgStateButton * emAir   = GetEMImgStateButtonInstance(hAir);
				EMImgStateButton * emRect  = GetEMImgStateButtonInstance(hRect);
				EMImgButton * emFill   = GetEMImgButtonInstance(hFill);
				EMImgButton * emClear  = GetEMImgButtonInstance(hClear);
				EMImgButton * emInvert = GetEMImgButtonInstance(hInvert);

				theme.apply(emHand);
				theme.apply(emAir);
				theme.apply(emRect);
				theme.apply(emFill);
				theme.apply(emClear);
				theme.apply(emInvert);

				emHand->setToolTip("Hand tool");
				emAir->setToolTip("Brush selection tool");
				emRect->setToolTip("Rectangle selection tool");
				emFill->setToolTip("Select whole image");
				emClear->setToolTip("Clear selection");
				emInvert->setToolTip("Invert selection");


				emHand->stateOn = true;

				emHand->bmAll = &(gIcons);
				emHand->pNx = emHand->pMx = emHand->pCx = emHand->pDx = 416;
				emHand->pNy = 0;
				emHand->pMy = 32;
				emHand->pCy = 64;
				emHand->pDy = 96;
				emHand->colBorderSelected = emHand->colBorder;


				emAir->bmAll = &(gIcons);
				emAir->pNx = emAir->pMx = emAir->pCx = emAir->pDx = 256;
				emAir->pNy = 0;
				emAir->pMy = 32;
				emAir->pCy = 64;
				emAir->pDy = 96;
				emAir->colBorderSelected = emAir->colBorder;

				emRect->bmAll = &(gIcons);
				emRect->pNx = emRect->pMx = emRect->pCx = emRect->pDx = 288;
				emRect->pNy = 0;
				emRect->pMy = 32;
				emRect->pCy = 64;
				emRect->pDy = 96;
				emRect->colBorderSelected = emRect->colBorder;

				emFill->bmAll = &(gIcons);
				emFill->pNx = emFill->pMx = emFill->pCx = emFill->pDx = 320;
				emFill->pNy = 0;
				emFill->pMy = 32;
				emFill->pCy = 64;
				emFill->pDy = 96;
				emFill->colBorderClick = emFill->colBorder;

				emClear->bmAll = &(gIcons);
				emClear->pNx = emClear->pMx = emClear->pCx = emClear->pDx = 384;
				emClear->pNy = 0;
				emClear->pMy = 32;
				emClear->pCy = 64;
				emClear->pDy = 96;
				emClear->colBorderClick = emClear->colBorder;

				emInvert->bmAll = &(gIcons);
				emInvert->pNx = emInvert->pMx = emInvert->pCx = emInvert->pDx = 352;
				emInvert->pNy = 0;
				emInvert->pMy = 32;
				emInvert->pCy = 64;
				emInvert->pDy = 96;
				emInvert->colBorderClick = emInvert->colBorder;
			}
			break;
		case EMT_TAB_CLOSED:
			{
				EMPView * empvmain = GetEMPViewInstance(hImage);
				empvmain->mode = EMPView::MODE_SHOW;

				HWND h0 = GetDlgItem(hWnd, IDC_REND_REGIONS_HANDTOOL);
				HWND h1 = GetDlgItem(hWnd, IDC_REND_REGIONS_RECT);
				HWND h2 = GetDlgItem(hWnd, IDC_REND_REGIONS_AIRBRUSH);
				EMImgStateButton * emisb0 = GetEMImgStateButtonInstance(h0);
				EMImgStateButton * emisb1 = GetEMImgStateButtonInstance(h1);
				EMImgStateButton * emisb2 = GetEMImgStateButtonInstance(h2);
				emisb0->stateOn = true;
				emisb1->stateOn = false;
				emisb2->stateOn = false;
				InvalidateRect(h0, NULL, false);
				InvalidateRect(h1, NULL, false);
				InvalidateRect(h2, NULL, false);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_REGIONS_AIRBRUSH:
					{
						if (wmEvent != BN_CLICKED)
							break;
						HWND h1 = GetDlgItem(hWnd, IDC_REND_REGIONS_RECT);
						HWND h2 = GetDlgItem(hWnd, IDC_REND_REGIONS_HANDTOOL);
						EMImgStateButton * emisb1 = GetEMImgStateButtonInstance(h1);
						EMImgStateButton * emisb2 = GetEMImgStateButtonInstance(h2);
						emisb1->stateOn = false;
						emisb2->stateOn = false;
						InvalidateRect(h1, NULL, false);
						InvalidateRect(h2, NULL, false);

						EMPView * empv = GetEMPViewInstance(hImage);
						empv->mode = EMPView::MODE_REGION_AIRBRUSH;
					}
					break;
					case IDC_REND_REGIONS_RECT:
					{
						if (wmEvent != BN_CLICKED)
							break;
						HWND h1 = GetDlgItem(hWnd, IDC_REND_REGIONS_AIRBRUSH);
						HWND h2 = GetDlgItem(hWnd, IDC_REND_REGIONS_HANDTOOL);
						EMImgStateButton * emisb1 = GetEMImgStateButtonInstance(h1);
						EMImgStateButton * emisb2 = GetEMImgStateButtonInstance(h2);
						emisb1->stateOn = false;
						emisb2->stateOn = false;
						InvalidateRect(h1, NULL, false);
						InvalidateRect(h2, NULL, false);

						EMPView * empv = GetEMPViewInstance(hImage);
						empv->mode = EMPView::MODE_REGION_RECT;
					}
					break;
					case IDC_REND_REGIONS_HANDTOOL:
					{
						if (wmEvent != BN_CLICKED)
							break;
						HWND h1 = GetDlgItem(hWnd, IDC_REND_REGIONS_RECT);
						HWND h2 = GetDlgItem(hWnd, IDC_REND_REGIONS_AIRBRUSH);
						EMImgStateButton * emisb1 = GetEMImgStateButtonInstance(h1);
						EMImgStateButton * emisb2 = GetEMImgStateButtonInstance(h2);
						emisb1->stateOn = false;
						emisb2->stateOn = false;
						InvalidateRect(h1, NULL, false);
						InvalidateRect(h2, NULL, false);

						EMPView * empv = GetEMPViewInstance(hImage);
						empv->mode = EMPView::MODE_SHOW;
					}
					break;
					case IDC_REND_REGIONS_FILL:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						if (empv->rgnPtr)
							empv->rgnPtr->fillRegions();
						InvalidateRect(hImage, NULL, false);
					}
					break;
					case IDC_REND_REGIONS_CLEAR:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						if (empv->rgnPtr)
							empv->rgnPtr->clearRegions();
						InvalidateRect(hImage, NULL, false);
					}
					break;
					case IDC_REND_REGIONS_INVERT:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						if (empv->rgnPtr)
							empv->rgnPtr->inverseRegions();
						InvalidateRect(hImage, NULL, false);
					}
					break;
					case IDC_REND_REGIONS_AIRSIZE:
					{
						HWND hSize = GetDlgItem(hWnd, IDC_REND_REGIONS_AIRSIZE);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hSize);
						EMPView * empv = GetEMPViewInstance(hImage);
						if (empv->rgnPtr)
							empv->rgnPtr->airbrush_size = emetb->intValue + 4;
					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

