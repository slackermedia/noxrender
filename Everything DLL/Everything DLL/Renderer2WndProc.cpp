#include "RendererMain2.h"
#include "raytracer.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include <GdiPlus.h>
#include "log.h"
#include "dialogs.h"
#include "resource.h"
#include <shlwapi.h>
#include <ShlObj.h>

#define WN_LOADSCENE 55000
#define WN_LOADSCENE_AND_RUN 55001
#define WN_RUN_RENDERER 55002
#define WN_LOAD_POST 55011
#define WN_LOAD_BLEND 55012
#define WN_LOAD_FINAL 55013


extern char * userDirectory;
extern char * defaultDirectory;
extern HMODULE hDllModule;
bool updateRegionButtonsState(HWND hWnd, EMPView * empv);

//------------------------------------------------------------------------------------------------

LRESULT CALLBACK RendererMain2::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	HWND hMain = rMain ? rMain->getHWND() : 0;

	switch (message)
	{
		case WM_CREATE:
			{
			}	// end WM_CREATE
			break;
		case WM_SIZE:
			{
				return rMain->processResizeMessage(wParam, lParam);
			}
			break;
		case WM_GETMINMAXINFO:
			{
				CHECK(lParam);
				MINMAXINFO * mm = (MINMAXINFO*)lParam;
				mm->ptMinTrackSize.x = 1050;
				mm->ptMinTrackSize.y = 720;
			}
			break;
		case WM_CLOSE:
			{
				Raytracer * rtr = Raytracer::getInstance();
				if (rtr->curScenePtr->nowRendering)
				{
					int rres = MessageBox(hMain, "NOX is still rendering.\nClose NOX anyway?", "Warning", MB_YESNO|MB_ICONSTOP);
					if (rres==IDYES)
						DestroyWindow(hMain);
				}
				else
				{
					DestroyWindow(hMain);
				}
				return 0;
			}
			break;
		case WM_DESTROY:
			{
				PostQuitMessage(0);
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int trb = rect.bottom;
				int trr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				if (rMain->showstartgui)		// draw logo, size: 102 x 95
				{
					int cx = trr/2 - 102/2 - rect.left;
					int cy = trb/2 - 206/2 - rect.top;
					HDC thdc2 = CreateCompatibleDC(hdc);
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc2, rMain->hLogoGradient);

					BLENDFUNCTION blendFunction;
					blendFunction.BlendOp = AC_SRC_OVER;
					blendFunction.BlendFlags = 0;
					blendFunction.SourceConstantAlpha = 204;
					blendFunction.AlphaFormat = AC_SRC_ALPHA;
					AlphaBlend(bhdc, cx, cy, 102, 95,  thdc2, 0, 0, 102, 95, blendFunction);

					SelectObject(thdc2, oldBitmap);
					DeleteDC(thdc2);
				}

				int line_end = trr - RIGHT_PANEL_WIDTH - 7;
				if (!rMain->rightPanelOpened)
					line_end = rect.right+6;
				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(bhdc);

						Gdiplus::Pen s1pen(Gdiplus::Color(48, 0xc0,0xc0,0xc0));
						Gdiplus::Pen s2pen(Gdiplus::Color(96, 0x15,0x15,0x15));
						Gdiplus::Pen s3pen(Gdiplus::Color(46, 0x15,0x15,0x15));
						Gdiplus::Pen s4pen(Gdiplus::Color(48, 0xc0,0xc0,0xc0));

						graphics.DrawLine(&s1pen, line_end+4-xx, 4-yy, line_end+4-xx, trb-6-yy);
						Gdiplus::Point pts3[] = {
									Gdiplus::Point(line_end-xx+5, 4-yy), 
									Gdiplus::Point(line_end-xx+5, trb-6-yy),
									Gdiplus::Point(line_end-xx+4, trb-5-yy),
									Gdiplus::Point(line_end-xx+3, trb-6-yy),
									Gdiplus::Point(line_end-xx+3, 4-yy),
									Gdiplus::Point(line_end-xx+4, 3-yy),
									Gdiplus::Point(line_end-xx+5, 4-yy) };
						graphics.DrawLines(&s2pen, pts3, 7);
						Gdiplus::Point pts4[] = {
									Gdiplus::Point(line_end-xx+6, 4-yy),
									Gdiplus::Point(line_end-xx+6, trb-6-yy),
									Gdiplus::Point(line_end-xx+4, trb-4-yy),
									Gdiplus::Point(line_end-xx+2, trb-6-yy),
									Gdiplus::Point(line_end-xx+2, 4-yy),
									Gdiplus::Point(line_end-xx+4, 2-yy),
									Gdiplus::Point(line_end-xx+6, 4-yy) };
						graphics.DrawLines(&s3pen, pts4, 7);

						if (rMain->cantloadscene)
							graphics.DrawRectangle(&s4pen, rMain->vpRect.left-xx, rMain->vpRect.top-yy, rMain->vpRect.right-rMain->vpRect.left-1, rMain->vpRect.bottom-rMain->vpRect.top-1);

						if (rMain->regionsPanelOpened)
						{
							int tabsPosX = trr-TABS_WIDTH-5-RIGHT_PANEL_WIDTH;
							if (!rMain->rightPanelOpened)
								tabsPosX = trr-TABS_WIDTH-3;
							int pnx = tabsPosX-7;
							int pny = 269;//255;
							Gdiplus::Point pts5[] = {
										Gdiplus::Point(pnx+0 -xx, pny-1-yy), 
										Gdiplus::Point(pnx+30-xx, pny-1-yy), 
										Gdiplus::Point(pnx+30-xx, pny+240-yy),
										Gdiplus::Point(pnx+0 -xx, pny+240-yy) };
							graphics.DrawLines(&s1pen, pts5, 4);
							Gdiplus::Point pts6[] = {
										Gdiplus::Point(pnx+30-xx+1, pny-1-yy), 
										Gdiplus::Point(pnx+30-xx+1, pny+240-yy+1),
										Gdiplus::Point(pnx+0 -xx,   pny+240-yy+1) };
							Gdiplus::Point pts7[] = {
										Gdiplus::Point(pnx+30-xx+2, pny-1-yy), 
										Gdiplus::Point(pnx+30-xx+2, pny+240-yy+2),
										Gdiplus::Point(pnx+0 -xx,   pny+240-yy+2) };
							graphics.DrawLines(&s2pen, pts6, 3);
							graphics.DrawLines(&s3pen, pts7, 3);
						}

					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					HPEN hOldPen = (HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(0x41,0x41,0x41)));
					MoveToEx(bhdc, line_end+4-xx, 4-yy, NULL);
					LineTo(bhdc, line_end+4-xx, trb-5-yy);

					DeleteObject((HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(0x1e,0x1e,0x1e))));
					MoveToEx(bhdc, line_end-xx+5, 4-yy, NULL);
					LineTo(bhdc,   line_end-xx+5, trb-6-yy);
					LineTo(bhdc,   line_end-xx+4, trb-5-yy);
					LineTo(bhdc,   line_end-xx+3, trb-6-yy);
					LineTo(bhdc,   line_end-xx+3, 4-yy);
					LineTo(bhdc,   line_end-xx+4, 3-yy);
					LineTo(bhdc,   line_end-xx+5, 4-yy);
					DeleteObject((HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(0x21,0x21,0x21))));
					MoveToEx(bhdc, line_end-xx+6, 4-yy, NULL);
					LineTo(bhdc,   line_end-xx+6, trb-6-yy);
					LineTo(bhdc,   line_end-xx+4, trb-4-yy);
					LineTo(bhdc,   line_end-xx+2, trb-6-yy);
					LineTo(bhdc,   line_end-xx+2, 4-yy);
					LineTo(bhdc,   line_end-xx+4, 2-yy);
					LineTo(bhdc,   line_end-xx+6, 4-yy);

					DeleteObject((HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(0x41,0x41,0x41))));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(bhdc, GetStockObject(HOLLOW_BRUSH));
					Rectangle(bhdc, rMain->vpRect.left-xx, rMain->vpRect.top-yy, rMain->vpRect.right-xx, rMain->vpRect.bottom-yy);
					DeleteObject(SelectObject(bhdc, hOldBrush));
					DeleteObject(SelectObject(bhdc, hOldPen));


					if (rMain->regionsPanelOpened)
					{
						int tabsPosX = trr-TABS_WIDTH-5-RIGHT_PANEL_WIDTH;
						if (!rMain->rightPanelOpened)
							tabsPosX = trr-TABS_WIDTH-3;
						int pnx = tabsPosX-7;
						int pny = 270;
						HPEN hOldPen = (HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(0x41,0x41,0x41)));
						MoveToEx(bhdc,	pnx+0 -xx, pny-1-yy		,	NULL);
						LineTo(bhdc,	pnx+30-xx, pny-1-yy		);
						LineTo(bhdc,	pnx+30-xx, pny+240-yy	);
						LineTo(bhdc,	pnx-1 -xx, pny+240-yy	);
						DeleteObject((HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(0x1e,0x1e,0x1e))));
						MoveToEx(bhdc,	pnx+30-xx+1, pny-1-yy		, NULL);
						LineTo(bhdc,	pnx+30-xx+1, pny+240-yy+1	);
						LineTo(bhdc,	pnx-1 -xx,   pny+240-yy+1	);
						DeleteObject((HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(0x21,0x21,0x21))));
						MoveToEx(bhdc,	pnx+30-xx+2, pny-1-yy		, NULL);
						LineTo(bhdc,	pnx+30-xx+2, pny+240-yy+2	);
						LineTo(bhdc,	pnx-1 -xx,   pny+240-yy+2	);
						DeleteObject(SelectObject(bhdc, hOldPen));
					}

				#endif

				// diaphragm 
				bool drawDiaph = true;
				if (!rMain->rightPanelOpened)
					drawDiaph = false;
				EM2Tabs * emt = GetEM2TabsInstance(rMain->hTabs);
				if (!emt)
					drawDiaph = false;
				else
					if (emt->selected!=1  &&  emt->selected!=7)
						drawDiaph = false;
				if (!rMain->hDiaph)
					drawDiaph = false;

				if (drawDiaph)
				{
					HDC dhdc = CreateCompatibleDC(hdc);			// diaphragm image buffer hdc
					HBITMAP oldBitmap2 = (HBITMAP)SelectObject(dhdc, rMain->hDiaph);
					BLENDFUNCTION blendFunction;
					blendFunction.BlendOp = AC_SRC_OVER;
					blendFunction.BlendFlags = 0;
					blendFunction.SourceConstantAlpha = 204;
					blendFunction.AlphaFormat = AC_SRC_ALPHA;
					//int y5 = 524;
					int y5 = 539;
					if (emt->selected==7)	// fakedof
						y5 -= 4;
					AlphaBlend(bhdc, line_end-74, y5,  64,64,  dhdc, 0, 0, 64,64, blendFunction);
					SelectObject(dhdc, oldBitmap2);
					DeleteDC(dhdc);

					int xc, xl, xr, xe, y1, y2, y3, y4, yt;
					xc = line_end - 42;
					xl = xc - 24;
					xr = xc + 24;
					xe = line_end - 5;
					y1 = 510;		y2 = 529;		y3 = 611;		y4 = 630;
					if (emt->selected==7)	// fakedof
					{
						y1-=4;	y2-=4;	y3-=4;	y4-=4;
					}

					yt = 5;
					HPEN oldPen = (HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(62,62,62)));
					MoveToEx(bhdc, xe,y1, NULL);
					LineTo(bhdc, xc,y1);
					LineTo(bhdc, xc,y2);
					MoveToEx(bhdc, xe,y4, NULL);
					LineTo(bhdc, xc,y4);
					LineTo(bhdc, xc,y3);
					MoveToEx(bhdc, xl,y2+yt, NULL);
					LineTo(bhdc, xl,y2);
					LineTo(bhdc, xr,y2);
					LineTo(bhdc, xr,y2+yt+1);

					MoveToEx(bhdc, xl,y3-yt, NULL);
					LineTo(bhdc, xl,y3);
					LineTo(bhdc, xr,y3);
					LineTo(bhdc, xr,y3-yt-1);

					DeleteObject(SelectObject(bhdc, oldPen));
				}


				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case 32900:		// autorefresh called it
					case 32771:		// F5 accelerator
							rMain->refreshRenderOnThread();
						break;
					case IDC_REND2_TABS:
						{
							if (!rMain->rightPanelOpened)
								rMain->openCloseRightPanel(true);
							else
								rMain->tabsWereChanged();
						}
						break;
					case IDC_REND2_BUTTON_SHOWHIDE_PANEL:
						{
							rMain->openCloseRightPanel(!rMain->rightPanelOpened);
						}
						break;
					case IDC_REND2_BUTTON_SHOWHIDE_REGIONS:
						{
							rMain->openCloseRegionsPanel(!rMain->regionsPanelOpened);
							if (!rMain->regionsPanelOpened)
							{
								EMPView * empv = GetEMPViewInstance(rMain->hViewport);
								if (empv->mode==EMPView::MODE_REGION_AIRBRUSH  ||  empv->mode==EMPView::MODE_REGION_RECT)
								{
									rMain->showRegionModeInStatus(false);
									empv->mode = EMPView::MODE_SHOW;
								}
								updateRegionButtonsState(rMain->hRegionsPanel, empv);
							}
						}
						break;
					case IDC_REND2_STEXT_3_LOAD_LINK:
						{
							CHECK(wmEvent==BN_CLICKED);
							Logger::add("Load Scene from main window chosen. Dialog opened.");
							rMain->callLoadSceneDialog(rMain->getHWND(), NULL);
						}
						break;
					case IDC_REND2_STEXT_5_OPEN_LINK:
						{
							CHECK(wmEvent==BN_CLICKED);
							Logger::add("Load Sample Scene from main window chosen. Dialog opened.");
							if (defaultDirectory)
							{
								char sdir[2048];
								sprintf_s(sdir, 2048, "%s\\sample scenes", defaultDirectory);
								rMain->callLoadSceneDialog(rMain->getHWND(), sdir);
							}
							else
								rMain->callLoadSceneDialog(rMain->getHWND(), NULL);
						}
						break;
					case IDC_REND2_MENU_SCENE:
						{
							if (wmEvent==CBN_SELCHANGE)
							{
								EM2ComboBox * emc = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_MENU_SCENE));
								int chosen = emc->selected;
								emc->selected = -1;
								switch (chosen)
								{
									case ID_MENU_SCENE_LOAD:
										{
											Logger::add("Load Scene from combo chosen. Dialog opened.");
											rMain->callLoadSceneDialog(rMain->getHWND(), NULL);
										}
										break;
									case ID_MENU_SCENE_MERGE:
										{
											char * filename = openFileDialog(hWnd, 
												"Scene file (*.nxs *.nox)\0*.nxs;*.nox\0Scene binary file (*.nox)\0*.nox\0Scene XML file (*.nxs)\0*.nxs\0All files\0*.*\0", "Load scene for merge", "nox", 1, NULL);
											if (filename)
											{
												bool ok = rMain->mergeSceneAsync(filename);
											}
										}
										break;
									case ID_MENU_SCENE_SAVE:
										{
											Logger::add("Save Scene from combo chosen. Dialog opened.");
											char * filename = saveFileDialog(hMain, 
												"Scene file (*.nox *.nxs)\0*.nox;*.nxs\0Scene binary file (*.nox)\0*.nox\0Scene XML file (*.nxs)\0*.nxs\0All files\0*.*\0\0", "Save scene", "nox");
											if (filename)
											{
												bool ok = rMain->saveScene(filename, false, false);
											}
										}
										break;
									case ID_MENU_SCENE_SAVE_WITH_TEXTURES:
										{
											Logger::add("Save Scene with Textures from combo chosen. Dialog opened.");
											char * filename = saveFileDialog(hMain, 
												"Scene file (*.nox *.nxs)\0*.nox;*.nxs\0Scene binary file (*.nox)\0*.nox\0Scene XML file (*.nxs)\0*.nxs\0All files\0*.*\0\0", "Save scene", "nox");
											if (filename)
											{
												bool ok = rMain->saveScene(filename, true, false);
											}
										}
										break;
									case ID_MENU_SCENE_SAVE_IMAGE:
										{
											Logger::add("Save image button pressed. Dialog opened.");
											char * filename = saveFileDialog(hMain, 
												"EXR high dynamic range image (*.exr)\0*.exr\0BMP image (*.bmp)\0*.bmp\0PNG image (*.png)\0*.png\0JPEG image (*.jpg)\0*.jpg\0TIFF image (*.tiff)\0*.tiff\0", 
												"Save image", "png", 3);
											rMain->saveImage(filename);
										}
										break;
									case ID_MENU_SCENE_LOAD_PRESET:
										{


										}
										break;
									case ID_MENU_SCENE_SAVE_PRESET:
										{
										}
										break;
									case ID_MENU_SCENE_SCENE_INFO:
										{
											DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_SCENE_INFO2), hWnd, SceneInfo2DlgProc,(LPARAM)( 0 ));

										}
										break;
								}
							}
						}
						break;
					case IDC_REND2_MENU_HELP:
						{
							if (wmEvent==CBN_SELCHANGE)
							{
								EM2ComboBox * emc = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_MENU_HELP));
								int chosen = emc->selected;
								emc->selected = -1;
								switch (chosen)
								{
									case ID_MENU_HELP_ABOUT:
										{
											DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_ABOUT_DIALOG2), hWnd, About2DlgProc,(LPARAM)( 0 ));

										}
										break;
									case ID_MENU_HELP_LICENSE:
										openLicense2Window(hWnd);
										break;
									case ID_MENU_HELP_NOX_FORUM:
										ShellExecute(hWnd, "open", "http://www.evermotion.org/vbulletin/forumdisplay.php?398-NOX-RENDERER", NULL, NULL, SW_SHOWNORMAL);

										break;
									case ID_MENU_HELP_NOX_PROFILE:
										{
											if (!userDirectory)
												return 0;
											char fname[2048], fname2[2048];
											fname2[0] = 0;
											char * fptr = NULL;
											sprintf_s(fname, 2048, "%s\\logs", userDirectory);
											GetFullPathName(fname, 2048, fname2, &fptr);

											HRESULT hres = CoInitialize(NULL);

											ITEMIDLIST __unaligned *pIDL = ILCreateFromPath(fname2) ;
											if(NULL != pIDL)
											{
												SHOpenFolderAndSelectItems(pIDL, 0, 0, 0) ;
												ILFree(pIDL) ;
											}
											else
											{
												Logger::add("ILCreateFromPath failed");
											}

											CoUninitialize();
										}
										break;
								}
							}
						}
						break;
					case IDC_REND2_MENU_PRESETS:
						{
							if (wmEvent==CBN_SELCHANGE)
							{
								EM2ComboBox * emc = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_MENU_PRESETS));
								int chosen = emc->selected;
								emc->selected = -1;
								switch (chosen)
								{
									case 0:	// add preset
										{
											char * pr = runPresetAddDialog(hWnd, NULL);
											if (pr)
											{
												Raytracer * rtr = Raytracer::getInstance();
												Scene * sc = rtr->curScenePtr;
												sc->presetAddCurrent(pr);
												rMain->fillPresetsCombo(GetDlgItem(hWnd, IDC_REND2_MENU_PRESETS));
												
											}
										}
										break;
									case 1:	// manage presets
										{
											runPresetManageDialog(hWnd);
											rMain->fillPresetsCombo(GetDlgItem(hWnd, IDC_REND2_MENU_PRESETS));
										}										
										break;
									case 2:	
										{ // load preset
											Logger::add("Load Scene from combo chosen. Dialog opened.");
											char * filename = openFileDialog(hMain, 
												"Preset file (*.npr)\0*.npr\0All files\0*.*\0", "Load post preset", "npr");
											if (!filename)
												break;

											rMain->loadPreset(hWnd, filename, false);
										}
										break;
									case 3:	
										{ // save preset
											Logger::add("Save preset button pressed. Dialog opened.");
											char * filename = saveFileDialog(hMain, 
												"NOX preset (*.npr)\0*.npr\0", 
												"Save preset", "npr", 3);
											rMain->savePreset(filename);
										}
										break;
									case 4:	// separator
										break;
									default:
										{
											int s = chosen - 5;
											Raytracer * rtr = Raytracer::getInstance();
											Scene * sc = rtr->curScenePtr;
											sc->presetApply(s);
											Camera * cam = sc->getActiveCamera();

											rMain->fillLayersTabAll();
											rMain->fillPostTabAll(&cam->iMod);
											rMain->fillCorrectionTabAll(&cam->iMod);
											rMain->fillFakeDOFTabAll(&cam->fMod);
											rMain->fillEffectsTabAll(cam, &cam->fMod);

											EMPView * empv = GetEMPViewInstance(rMain->hViewport);
											empv->imgMod->copyDataFromAnother(&cam->iMod);

											SetFocus(hWnd);
											rMain->refreshRenderOnThread();
										}
										break;
								}
							}
						}
						break;
					case IDC_REND2_VIEWPORT:
						{
							if (wmEvent == PV_MIDDLECLICKED)
							{
								EMPView * empv = GetEMPViewInstance(rMain->hViewport);
								empv->zoom100();
							}
							if (wmEvent == PV_DROPPED_FILE)
							{
								EMPView * empv = GetEMPViewInstance(rMain->hViewport);
								char * filename = empv->lastDroppedFilename;
								if (filename)
								{
									if (rMain->cantloadscene)
									{
										if (isFileExtPreset(filename))
										{
											rMain->loadPreset(hWnd, filename, true);
										}
									}
									else
									{
										if (isFileExtSceneNox(filename) || isFileExtSceneNxs(filename))
											rMain->loadSceneAsync(empv->lastDroppedFilename, false);
									}
								}
							}
							if (wmEvent == PV_PIXEL_PICKED)
							{
								EM2Tabs * emt = GetEM2TabsInstance(rMain->hTabs);
								{
									EMPView * empv = GetEMPViewInstance(rMain->hViewport);
									float px, py;
									empv->evalControlPosToImagePos(empv->ppx, empv->ppy, px, py);
									SendMessage(rMain->hTabFakeDof, PV_PIXEL_PICKED, (WPARAM&)px, (LPARAM&)py);
								}
							}
							if (wmEvent == PV_CTRL_C)
							{
								EMPView * empv = GetEMPViewInstance(rMain->hViewport);
								HBITMAP hBMP = empv->getRenderHBitmap();
								if (hBMP == 0)
									break;

								OpenClipboard(rMain->hViewport);
								EmptyClipboard();
								SetClipboardData(CF_BITMAP, hBMP);
								CloseClipboard();

								DeleteObject(hBMP);
							}
						}
						break;
				}
			}
			break;
		case 32900:		// autorefresh called it
			{
				rMain->refreshRenderOnThread();
			}
			break;
		case WM_COPYDATA:
			{
				if (!lParam)
					break;
				COPYDATASTRUCT * cds = (COPYDATASTRUCT*)lParam;
				switch (cds->dwData)
				{
					case WN_LOADSCENE:
					case WN_LOADSCENE_AND_RUN:
						{
							char * tfname = NULL;
							char * ptr = (char*)cds->lpData;
							tfname = (char*)malloc(cds->cbData);
							sprintf_s(tfname, cds->cbData, "%s", ptr);
							if (!PathFileExists(tfname))
								break;
							RendererMain2 * rMain = RendererMain2::getInstance();
							rMain->loadSceneAsync(tfname, (cds->dwData==WN_LOADSCENE_AND_RUN) );
						}
						break;
					case WN_LOAD_POST:
						{
							Logger::add("load post message");
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							sc->postLoadPostFile = (char*)malloc(cds->cbData);
							sprintf_s(sc->postLoadPostFile, cds->cbData, "%s", (char*)cds->lpData);
							Logger::add(sc->postLoadPostFile);
						}
						break;
					case WN_LOAD_BLEND:
						{
							Logger::add("load blend message");
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							sc->postLoadBlendFile = (char*)malloc(cds->cbData);
							sprintf_s(sc->postLoadBlendFile, cds->cbData, "%s", (char*)cds->lpData);
							Logger::add(sc->postLoadBlendFile);
						}
						break;
					case WN_LOAD_FINAL:
						{
							Logger::add("load final message");
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							sc->postLoadFinalFile = (char*)malloc(cds->cbData);
							sprintf_s(sc->postLoadFinalFile, cds->cbData, "%s", (char*)cds->lpData);
							Logger::add(sc->postLoadFinalFile);
						}
						break;
				}
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
			break;
	}
	return 0;
}

//------------------------------------------------------------------------------------------------

