#include "max2013predef.h"
#include <windows.h>
#include <math.h>
#include <gdiplus.h>
#include "NOXRenderer.h"
#include "NOXMaterial.h"
#include "NOXExporter.h"
#include <IPathConfigMgr.h> 
#include <Path.h> 
#include "noxdebug.h"

#define WN_LOADSCENE 55000
#define WN_LOADSCENE_AND_RUN 55001
#define WN_RUN_RENDERER 55002
#define WN_LOAD_POST 55011
#define WN_LOAD_BLEND 55012
#define WN_LOAD_FINAL 55013

#define NOX_MAT_PREV_NONOX 1
#define NOX_MAT_PREV_NOPREV 2

//#define RENDER_XML

BOOL foundWindow = false;
HWND hRenderer = 0;

Mtl * matPrevNodesEnum(INode * inode)
{
	for (int c = 0; c < inode->NumberOfChildren(); c++) 
	{
		Mtl * mat = matPrevNodesEnum(inode->GetChildNode(c));
		if (mat)
			return mat;
	}

	ObjectState ostate = inode->EvalWorldState(0);
	if (ostate.obj->SuperClassID() == GEOMOBJECT_CLASS_ID)
		if (ostate.obj->CanConvertToType(triObjectClassID))
		{
			Object * obj = ostate.obj;
			if (!obj)
				return NULL;
			TriObject * tobj = (TriObject *)obj->ConvertToType(0, triObjectClassID);
			if (!tobj)
				return NULL;
			Mesh * cmesh = &(tobj->mesh);
			deleteSecondIfCopied(obj, tobj);
			if (!cmesh)
				return NULL;

			return inode->GetMtl();
		}

	return NULL;
}


class NOXRendererClassDesc : public ClassDesc2 
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new NOXRenderer(); }
	const TCHAR *	ClassName() { return GetString(IDS_NOX_REND_CLASS_NAME); }
	SClass_ID		SuperClassID() { return RENDERER_CLASS_ID; }
	Class_ID		ClassID() { return NOXRenderer_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_NOX_REND_CATEGORY); }

	const TCHAR*	InternalName() { return _T("NOX Renderer"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }					// returns owning module handle
};

static NOXRendererClassDesc NOXRendererDesc;
ClassDesc2* GetNOXRendererDesc() { return &NOXRendererDesc; }


//------------------------------------------------------------------------------------------------------------------------------------------

NOXRenderer::NOXRenderer() 
	: xScene(true), bScene(true)
{
	ADD_LOG_CONSTR("NOXRenderer Constructor");
	inMatEditor = false;
	matPrevCurMat = NULL;
	filename = NULL;
	scene_name = NULL;
	scene_author_name = NULL;
	scene_author_www = NULL;
	scene_author_email = NULL;
	overrideMatEnabled = false;
	overrideMaterial = NULL;
	mb_duration = 0.5f;
	mb_on = false;
	engine = 1;

	filename_post = NULL;
	filename_blend = NULL;
	filename_final = NULL;
	use_file_post = false;
	use_file_blend = false;
	use_file_final = false;
}

NOXRenderer::~NOXRenderer() 
{
	if (scene_name)
		free(scene_name);
	if (scene_author_name)
		free(scene_author_name);
	if (scene_author_www)
		free(scene_author_www);
	if (scene_author_email)
		free(scene_author_email);
	scene_name = NULL;
	scene_author_name = NULL;
	scene_author_www = NULL;
	scene_author_email = NULL;
}

//------------------------------------------------------------------------------------------------------------------------------------------

RefTargetHandle NOXRenderer::GetReference(int i) 
{ 
	if (i==0)
		return overrideMaterial;
	return NULL; 
}

//------------------------------------------------------------------------------------------------------------------------------------------

void NOXRenderer::SetReference(int i, RefTargetHandle rtarg) 
{ 
	if (i==0)
		overrideMaterial = (Mtl*)rtarg;
}

//------------------------------------------------------------------------------------------------------------------------------------------

int NOXRenderer::Open(INode *scene, INode *vnode, ViewParams *viewPar, RendParams &rp, HWND hwnd, 
					  DefaultLight* defaultLights, int numDefLights, RendProgressCallback* prog)
{
	SuspendAll uberSuspend(TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
	GetCOREInterface()->DisableSceneRedraw();
	inMatEditor = (rp.inMtlEdit == TRUE);

	if (inMatEditor)
	{
		Mtl * mat = matPrevNodesEnum(scene);
		matPrevCurMat = mat;
	}

	#ifdef RENDER_XML
		SceneEnum sEnum(NULL, &xScene);
		sEnum.callback(scene);
	#else
		SceneEnum sEnum(NULL, &bScene);
		sEnum.callback(scene);
	#endif

	return 1;
}

//------------------------------------------------------------------------------------------------------------------------------------------

void NOXRenderer::Close(HWND hwnd, RendProgressCallback* prog)
{
	ADD_LOG_PARTS_MAIN("NOXRenderer::Close");

	SuspendAll uberSuspend(TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);

	xScene.resetMe();

	GetCOREInterface()->EnableSceneRedraw();
	matPrevCurMat = NULL;
	if (prog) 
		prog->SetTitle(_T("Done!"));

}

//------------------------------------------------------------------------------------------------------------------------------------------

BOOL CALLBACK EnumNOXAppThreadWndProc(HWND hwnd, LPARAM lParam)
{
	TCHAR tt[1024];
	GetClassName(hwnd, tt, 1024);
	//if (0 == _tcscmp(_T("RendererMainWindow"), tt)) // on old gui
	if (0 == _tcscmp(_T("NOXMainWindow2"), tt))
	{
		foundWindow = true;
		hRenderer = hwnd;
	}
	return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------------------------

int NOXRenderer::Render(TimeValue t, Bitmap *tobm, FrameRendParams &frp, HWND hwnd, RendProgressCallback *prog, ViewParams *viewPar)
{

	if (inMatEditor)
	{	
		// show previews of materials
		renderMaterialPreview(tobm);
		return 1;
	}

	if (prog)
		prog->SetTitle(_T("Exporting..."));

	#ifdef RENDER_XML
		bool asXML = true;
	#else
		bool asXML = false;
	#endif

	if (!filename)
		generateFilename(asXML);

	TCHAR * t_filename = copyAnsiToTchar1(filename);

	bool ok;
	if (asXML)
	{
		xScene.prog = prog;
		ok = xScene.saveScene(t_filename);
	}
	else
	{
		bScene.prog = prog;
		ok = bScene.saveScene(t_filename);
	}

	if (t_filename)
		free(t_filename);

	if (!ok)
		return 0;


	if (prog)
		prog->SetTitle(_T("Waiting for NOX..."));

	ADD_LOG_PARTS_MAIN("NOXRenderer::Render - creating NOX Renderer process");

	bool running = true;
	PROCESS_INFORMATION pi;
	TCHAR * rd = getRendererDirectory1();
	if (rd)
	{
		TCHAR rc[2048];
		#ifdef _WIN64
			_stprintf_s(rc, 2048, _T("%s\\bin\\64\\Renderer64.exe"), rd);
		#else
			_stprintf_s(rc, 2048, _T("%s\\bin\\32\\Renderer32.exe"), rd);
		#endif
		free(rd);

		STARTUPINFO si = {	sizeof(STARTUPINFO),	NULL,	_T(""),	NULL,
					0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0 };

		bool ok1 = (CreateProcess(rc, _T(""), NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS , NULL, NULL, &si, &pi) == TRUE);

		if (!ok1)
			running = false;
	}
	else
		running = false;

	if (!running)
	{
		MessageBox(tobm->GetWindow(), _T("Couldn't run NOX application."), _T("Error"), 0);
		return 0;
	}

	ADD_LOG_PARTS_MAIN("NOXRenderer::Render - waiting for NOX ...");

	while (!foundWindow)
	{
		Sleep(100);
		EnumThreadWindows(pi.dwThreadId, EnumNOXAppThreadWndProc, 0);
	}

	ADD_LOG_PARTS_MAIN("NOXRenderer::Render NOX running");

	HWND hRend = hRenderer;
	foundWindow = false;
	hRenderer = 0;

	// LOAD SCENE MESSAGE
	COPYDATASTRUCT cds;
	cds.cbData = (int)strlen(filename)+1;
	cds.dwData = WN_LOADSCENE_AND_RUN;
	cds.lpData = filename;
	SendMessage(hRend, WM_COPYDATA, (WPARAM)tobm->GetWindow(), (LPARAM)&cds);

	

	// LOAD POST MESSAGE
	if (filename_post  &&  use_file_post)
	{
		COPYDATASTRUCT cds2;
		cds2.cbData = (int)strlen(filename_post)+1;
		cds2.dwData = WN_LOAD_POST;
		cds2.lpData = filename_post;
		SendMessage(hRend, WM_COPYDATA, (WPARAM)tobm->GetWindow(), (LPARAM)&cds2);
	}

	// LOAD BLEND MESSAGE
	if (filename_blend  &&  use_file_blend)
	{
		COPYDATASTRUCT cds3;
		cds3.cbData = (int)strlen(filename_blend)+1;
		cds3.dwData = WN_LOAD_BLEND;
		cds3.lpData = filename_blend;
		SendMessage(hRend, WM_COPYDATA, (WPARAM)tobm->GetWindow(), (LPARAM)&cds3);
	}

	// LOAD FINAL MESSAGE
	if (filename_final  &&  use_file_final)
	{
		COPYDATASTRUCT cds4;
		cds4.cbData = (int)strlen(filename_final)+1;
		cds4.dwData = WN_LOAD_FINAL;
		cds4.lpData = filename_final;
		SendMessage(hRend, WM_COPYDATA, (WPARAM)tobm->GetWindow(), (LPARAM)&cds4);
	}

	if (prog)
		prog->SetTitle(_T("Done"));

	showDeletedCopiesStats();

	return 1;
}

//------------------------------------------------------------------------------------------------------------------------------------------

void NOXRenderer::renderMaterialPreview(Bitmap *tobm)
{
	if (tobm  &&  inMatEditor)
	{

		if (matPrevCurMat->ClassID() != NOXMaterial_CLASS_ID)
		{
			renderMaterialError(tobm, NOX_MAT_PREV_NONOX);
			return;
		}

		NOXMaterial * nmat = (NOXMaterial*)matPrevCurMat;
		float * img = nmat->mat.createPreview(tobm->Width(), tobm->Height());
		if (!img)
		{
			renderMaterialError(tobm, NOX_MAT_PREV_NOPREV);
			return;
		}

		int w = tobm->Width();
		int h = tobm->Height();
		for (int y=0; y<h; y++)
		{
			float * adr = &(img[((y*w)*4)]);
			tobm->PutPixels(0,y, w, (BMM_Color_fl *)adr);
		}
		free(img);
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------

void NOXRenderer::renderMaterialError(Bitmap *tobm, int error)
{
	if (!tobm  ||  !inMatEditor)
		return;

	int w = tobm->Width();
	int h = tobm->Height();
	BMM_Color_fl * line;
	line = (BMM_Color_fl *)malloc(sizeof(BMM_Color_fl)*w);
	if (!line)
		return;

	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(w, h, PixelFormat32bppARGB);
	if (!bm)
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(line);
		return ;
	}

	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	LOGFONT lf;
	GetObject(hFont, sizeof(LOGFONT), &lf);
	//size_t l1 = strlen(lf.lfFaceName)+2;
	size_t l1 = _tcsclen(lf.lfFaceName)+2;
	WCHAR * wface = (WCHAR *)malloc(sizeof(WCHAR)*(l1));
	#ifdef UNICODE
		wcscpy(wface, lf.lfFaceName);
	#else
		MultiByteToWideChar(CP_ACP, 0, lf.lfFaceName, -1, wface, (int)l1);
	#endif
	Gdiplus::FontFamily  * fontFamily = new Gdiplus::FontFamily(wface);
	Gdiplus::Font        * font = NULL;
	Gdiplus::PointF        pointF = Gdiplus::PointF(w*0.5f, h*0.5f);
	Gdiplus::SolidBrush  * solidBrush = new Gdiplus::SolidBrush(Gdiplus::Color(255, 255, 255, 255));
	Gdiplus::StringFormat * stringFormat = new Gdiplus::StringFormat();
	stringFormat->SetAlignment(Gdiplus::StringAlignmentCenter);
	stringFormat->SetLineAlignment(Gdiplus::StringAlignmentCenter);

	Gdiplus::Graphics * g = new Gdiplus::Graphics(bm);
	g->Clear(Gdiplus::Color(128,128,128));


	float fh1 = 100;//-lf.lfHeight;
	WCHAR * wtstamp;
	switch (error)
	{
		case NOX_MAT_PREV_NONOX:
			wtstamp = L"Not\na NOX\nmaterial";
			break;
		case NOX_MAT_PREV_NOPREV:
			wtstamp = L"No\npreview";
			break;
		default:
			wtstamp = L"Error";
			break;
	}
	Gdiplus::RectF rect;
	rect.Width  = w + 1.0f;
	rect.Height = h + 1.0f;
	

	while (rect.Width >= w*0.9f   ||   rect.Height >= h*0.9f)
	{
		if (font)
		{
			delete font;
			fh1--;
		}
		font = new Gdiplus::Font(fontFamily, fh1, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		g->MeasureString(wtstamp, -1, font, pointF, &rect);
	}

	g->DrawString(wtstamp, -1, font, pointF, stringFormat, solidBrush);

	delete stringFormat;
	delete g;
	delete solidBrush;
	delete font;
	delete fontFamily;
	free(wface);

	Gdiplus::Color color;
	for (int y=0; y<tobm->Height(); y++)
	{
		for (int x=0; x<tobm->Width(); x++)
		{
			bm->GetPixel(x,y, &color);
			line[x].r = color.GetR()/255.0f;
			line[x].g = color.GetG()/255.0f;
			line[x].b = color.GetB()/255.0f;
		}
		tobm->PutPixels(0,y, w, line);
	}

	free(line);
	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);

}

//------------------------------------------------------------------------------------------------------------------------------------------

RendParamDlg * NOXRenderer::CreateParamDialog(IRendParams *ir,BOOL prog)
{
	return new NOXRendParamDlg(this, ir, prog, hInstance);
	return NULL;
}

//------------------------------------------------------------------------------------------------------------------------------------------

void NOXRenderer::ResetParams()
{
	if (scene_name)
		free(scene_name);
	if (scene_author_name)
		free(scene_author_name);
	if (scene_author_www)
		free(scene_author_www);
	if (scene_author_email)
		free(scene_author_email);
	if (filename)
		free(filename);
	if (filename_post)
		free(filename_post);
	if (filename_blend)
		free(filename_blend);
	if (filename_final)
		free(filename_final);

	filename = NULL;
	filename_post = NULL;
	filename_blend = NULL;
	filename_final = NULL;
	scene_name = NULL;
	scene_author_name = NULL;
	scene_author_www = NULL;
	scene_author_email = NULL;
	use_file_post = false;
	use_file_blend = false;
	use_file_final = false;

	overrideMatEnabled = false;
	if (overrideMaterial)
		SetReference(0, NULL);

}


//------------------------------------------------------------------------------------------------------------------------------------------

RefTargetHandle NOXRenderer::Clone( RemapDir &remap )
{
	NOXRenderer * newob = new NOXRenderer();
	BaseClone(this, newob, remap);
	return newob;
}

//------------------------------------------------------------------------------------------------------------------------------------------

RefResult NOXRenderer::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	return REF_SUCCEED;
}

//------------------------------------------------------------------------------------------------------------------------------------------

