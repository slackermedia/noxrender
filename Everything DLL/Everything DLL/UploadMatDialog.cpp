#define _CRT_RAND_S
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "MaterialArchiver.h"
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include "log.h"
#include "MatEditor.h"

extern ThemeManager gTheme;
extern HBITMAP gIcons;


HWND upHwnd = 0;
char matArchFilename[2048];
char matPrevFilename[2048];
char resultFilename[2048];
char tempfilename[2048];

bool abortedByUser = false;
bool nowSending = false;
int f7zSize = 0;
int jpgSize = 0;

size_t write_data (void *ptr, size_t size, size_t nmemb, void *stream);	// <- check in NOXupdate.cpp

DWORD WINAPI UploadThreadProc(LPVOID lpParameter);
bool prepareAndSendMaterial(MaterialNox * mat);
void cleanUpAfterSending();
void lockBeforeSend();
void unlockAfterSend();
void setStatusText(char * status);
void initializeUploadCatButtons(HWND hwnd);
void unclickAllUploadCategories(HWND hwnd, int select);
bool checkCategoryClicked(HWND hwnd);
int upCategory = -1;

//-----------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK UploadMatDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static MaterialNox * mat;
	static HBRUSH bgBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			mat = (MaterialNox*)lParam;
			upHwnd = hWnd;

			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_UPLOAD_TEXT_YOUR_DESC)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMEditSimpleInstance(	GetDlgItem(hWnd, IDC_UPLOAD_DESCRIPTION)));
			GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(		GetDlgItem(hWnd, IDC_UPLOAD_AGREEMENT)));
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_UPLOAD_AGREEMENT1)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_UPLOAD_AGREEMENT2)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMProgressBarInstance(	GetDlgItem(hWnd, IDC_UPLOAD_PROGRESS)));
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_UPLOAD_STATUS)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(		GetDlgItem(hWnd, IDC_UPLOAD_SEND)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(		GetDlgItem(hWnd, IDC_UPLOAD_CANCEL)));
			initializeUploadCatButtons(hWnd);
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_UPLOAD_CANCEL:
					{
						if(wmEvent != BN_CLICKED)
							break;
						if (nowSending)
						{
							abortedByUser = true;
						}
						else
						{
							mat = NULL;
							upCategory = -1;
							EndDialog(hWnd, (INT_PTR)NULL);
						}
					}
					break;
				case IDC_UPLOAD_SEND:
					{
						if(wmEvent != BN_CLICKED)
							break;
						EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_UPLOAD_AGREEMENT));
						if (!emcb)
							break;
						if (!checkCategoryClicked(hWnd))
						{
							MessageBox(hWnd, "You must choose a material category.", "Warning", 0);
							break;
						}
						if (!emcb->selected)
						{
							MessageBox(hWnd, "You must accept the agreement.", "Warning", 0);
							break;
						}
						bool res = prepareAndSendMaterial(mat);
						if (!res)
						{
							setStatusText("Creating temporary files error. Not sent.");
							Logger::add("Creating temporary files error. Not sent.");
							unlockAfterSend();
						}
					}
					break;

				case IDC_UPLOAD_CAT_EMITTER:
						upCategory = MatCategory::CAT_EMITTER;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_EMITTER);
					break;
				case IDC_UPLOAD_CAT_BRICKS:
						upCategory = MatCategory::CAT_BRICKS;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_BRICKS);
					break;
				case IDC_UPLOAD_CAT_CARPAINT:
						upCategory = MatCategory::CAT_CARPAINT;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_CARPAINT);
					break;
				case IDC_UPLOAD_CAT_CERAMIC:
						upCategory = MatCategory::CAT_CERAMIC;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_CERAMIC);
					break;
				case IDC_UPLOAD_CAT_CONCRETE:
						upCategory = MatCategory::CAT_CONCRETE;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_CONCRETE);
					break;
				case IDC_UPLOAD_CAT_FABRIC:
						upCategory = MatCategory::CAT_FABRIC;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_FABRIC);
					break;
				case IDC_UPLOAD_CAT_FOOD:
						upCategory = MatCategory::CAT_FOOD;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_FOOD);
					break;
				case IDC_UPLOAD_CAT_GLASS:
						upCategory = MatCategory::CAT_GLASS;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_GLASS);
					break;
				case IDC_UPLOAD_CAT_GROUND:
						upCategory = MatCategory::CAT_GROUND;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_GROUND);
					break;
				case IDC_UPLOAD_CAT_LEATHER:
						upCategory = MatCategory::CAT_LEATHER;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_LEATHER);
					break;
				case IDC_UPLOAD_CAT_LIQUIDS:
						upCategory = MatCategory::CAT_LIQUIDS;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_LIQUIDS);
					break;
				case IDC_UPLOAD_CAT_METAL:
						upCategory = MatCategory::CAT_METAL;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_METAL);
					break;
				case IDC_UPLOAD_CAT_MINERALS:
						upCategory = MatCategory::CAT_MINERALS_GEMS;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_MINERALS_GEMS);
					break;
				case IDC_UPLOAD_CAT_ORGANIC:
						upCategory = MatCategory::CAT_ORGANIC;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_ORGANIC);
					break;
				case IDC_UPLOAD_CAT_PLASTIC:
						upCategory = MatCategory::CAT_PLASTIC;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_PLASTIC);
					break;
				case IDC_UPLOAD_CAT_PORCELAIN:
						upCategory = MatCategory::CAT_PORCELAIN;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_PORCELAIN);
					break;
				case IDC_UPLOAD_CAT_SSS:
						upCategory = MatCategory::CAT_SSS;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_SSS);
					break;
				case IDC_UPLOAD_CAT_STONE:
						upCategory = MatCategory::CAT_STONE;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_STONE);
					break;
				case IDC_UPLOAD_CAT_TILES:
						upCategory = MatCategory::CAT_TILES;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_TILES);
					break;
				case IDC_UPLOAD_CAT_WALLS:
						upCategory = MatCategory::CAT_WALLS;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_WALLS);
					break;
				case IDC_UPLOAD_CAT_WOOD:
						upCategory = MatCategory::CAT_WOOD;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_WOOD);
					break;
				case IDC_UPLOAD_CAT_OTHER:
						upCategory = MatCategory::CAT_OTHER;
						unclickAllUploadCategories(hWnd, MatCategory::CAT_OTHER);
					break;
			}
		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)bgBrush;
		}
	case WM_CTLCOLORDLG:
		{
			return (INT_PTR)bgBrush;
		}
		break;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------------------------

void lockBeforeSend()
{
	disableControl(upHwnd, IDC_UPLOAD_DESCRIPTION);
	disableControl(upHwnd, IDC_UPLOAD_AGREEMENT);
	disableControl(upHwnd, IDC_UPLOAD_SEND);
	disableControl(upHwnd, IDC_UPLOAD_CANCEL);
	EMButton * emb = GetEMButtonInstance(GetDlgItem(upHwnd, IDC_UPLOAD_CANCEL));
	if (emb)
		emb->changeCaption("Abort");
	InvalidateRect(emb->hwnd, NULL, false);
}

//-----------------------------------------------------------------------------------------------------------------------

void unlockAfterSend()
{
	enableControl(upHwnd, IDC_UPLOAD_DESCRIPTION);
	enableControl(upHwnd, IDC_UPLOAD_AGREEMENT);
	enableControl(upHwnd, IDC_UPLOAD_SEND);
	enableControl(upHwnd, IDC_UPLOAD_CANCEL);
	EMButton * emb = GetEMButtonInstance(GetDlgItem(upHwnd, IDC_UPLOAD_CANCEL));
	if (emb)
		emb->changeCaption("Close");
	InvalidateRect(emb->hwnd, NULL, false);
}

//-----------------------------------------------------------------------------------------------------------------------

void setStatusText(char * status)
{
	HWND hs = GetDlgItem(upHwnd, IDC_UPLOAD_STATUS);
	EMText * emt = GetEMTextInstance(hs);
	if (emt)
		emt->changeCaption(status);
}

//-----------------------------------------------------------------------------------------------------------------------

bool prepareAndSendMaterial(MaterialNox * mat)
{
	if (!MatEditorWindow::onlinematlib.username  ||  !MatEditorWindow::onlinematlib.passwordhash   ||   MatEditorWindow::onlinematlib.invalidUserPass)
	{
		bool res = MatEditorWindow::invokeUserPassDialog();
		if (!res)
			return false;
	}

	matArchFilename[0] = 0;
	matPrevFilename[0] = 0;
	tempfilename[0] = 0;
	resultFilename[0] = 0;
	
	CHECK(mat);
	CHECK(upHwnd);

	char lbuf[2048];

	lockBeforeSend();

	Logger::add("I'm about to send your material to NOX online library.");

	setStatusText("Creating temporary directory...");

	char temppath[2048];
	int ll = GetTempPath(2048, temppath);
	if (ll > 2048)
		return false;

	if (ll < 3)
		return false;

	char temppathlong[2048];
	int ll2 = GetLongPathName(temppath, temppathlong, 2048);
	if (ll2 > 2048)
		return false;

	if (temppathlong[ll2-1] == '\\'  ||  temppathlong[ll2-1] == '/')
		temppathlong[ll2-1] = 0;

	if (!dirExists(temppathlong))
		return false;

	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(temppathlong, "mat_", unique, tempfilename);

	if (strlen(tempfilename) < strlen(temppathlong))
		return false;

	Logger::add(" Creating temporary directory");
	CreateDirectory(tempfilename, NULL);

	sprintf_s(matArchFilename, 2048, "%s\\mat.7z", tempfilename);
	sprintf_s(matPrevFilename, 2048, "%s\\mat.jpg", tempfilename);
	sprintf_s(resultFilename, 2048, "%s\\res.txt", tempfilename);

	Logger::add(" Compressing material file");
	setStatusText("Compressing file");
	MatArchiver mArch;
	bool archOK = mArch.archiveMaterial(mat, matArchFilename);
	f7zSize = fileSize(matArchFilename);
	sprintf_s(lbuf, 2048, " Created \"%s\" - %d bytes", matArchFilename, f7zSize);
	Logger::add(lbuf);
	SwitchToThisWindow(upHwnd, true);

	
	Logger::add(" Saving preview...");
	setStatusText("Saving preview...");
	bool prevOK = mat->savePreview(matPrevFilename, 4);		// 4 for jpg
	jpgSize = fileSize(matPrevFilename);
	sprintf_s(lbuf, 2048, " Created \"%s\" - %d bytes", matPrevFilename, jpgSize);
	Logger::add(lbuf);

	setStatusText("Launching connection thread...");
	Logger::add(" Launching connection thread");
	enableControl(upHwnd, IDC_UPLOAD_CANCEL);
	DWORD threadId;
	HANDLE hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(UploadThreadProc), (LPVOID)mat, 0, &threadId);

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

void cleanUpAfterSending()
{
	char delinfobuf[2048];
	if (fileExists(matArchFilename))
	{
		sprintf_s(delinfobuf, 2048, " Deleting %s", matArchFilename);
		Logger::add(delinfobuf);
		DeleteFile(matArchFilename);
	}

	if (fileExists(matPrevFilename))
	{
		sprintf_s(delinfobuf, 2048, " Deleting %s", matPrevFilename);
		Logger::add(delinfobuf);
		DeleteFile(matPrevFilename);
	}

	if (fileExists(resultFilename))
	{
		sprintf_s(delinfobuf, 2048, " Deleting %s", resultFilename);
		Logger::add(delinfobuf);
		DeleteFile(resultFilename);
	}

	if (dirExists(tempfilename))
	{
		sprintf_s(delinfobuf, 2048, " Deleting %s", tempfilename);
		Logger::add(delinfobuf);
		RemoveDirectory(tempfilename);
	}
}

//-----------------------------------------------------------------------------------------------------------------------

int checkSendError()
{
	FILE * file;
	if (fopen_s(&file, resultFilename, "rb"))
		return 1001;

	char buf[64];
	int r = (int)fread(buf, 1, 64, file);
	if (r!=1)
	{
		fclose(file);
		return 1002;
	}

	fclose(file);

	switch (buf[0])
	{
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		default: return 1003;
	}
}

//-----------------------------------------------------------------------------------------------------------------------

int progressCallbackSend(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
	HWND hPr = GetDlgItem(upHwnd, IDC_UPLOAD_PROGRESS);
	EMProgressBar * empb = GetEMProgressBarInstance(hPr);
	empb->setPos((float)(100*ulnow/ultotal));
	InvalidateRect(hPr, NULL, false);

	char buf[64];
	sprintf_s(buf, 64, "Sending (%d bytes)... %d%%", (f7zSize+jpgSize), (int)(100*ulnow/ultotal));
	setStatusText(buf);

	if (abortedByUser)
		return 1;

	return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

DWORD WINAPI UploadThreadProc(LPVOID lpParameter)
{
	abortedByUser = false;
	MaterialNox * mat = (MaterialNox*)lpParameter;

	CURL *curl;
	CURLcode res;

	FILE * file = NULL;
	if (fopen_s(&file, resultFilename, "wb"))
		return false;
	CHECK(file);

	char * strURL = "www.evermotion.org/nox/noxUploadMaterial";

	curl_global_init(CURL_GLOBAL_ALL); 
	curl = curl_easy_init(); 

	curl_easy_setopt(curl, CURLOPT_POST, 1); 
	
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0); 
	curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, progressCallbackSend); 

	char cNumLayers[16];
	sprintf_s(cNumLayers, 16, "%d", mat->layers.objCount);
	int numTex = 0;
	for (int i=0; i<mat->layers.objCount; i++)
	{
		if (mat->layers[i].tex_col0.isValid())
			numTex++;
		if (mat->layers[i].tex_col90.isValid())
			numTex++;
		if (mat->layers[i].tex_rough.isValid())
			numTex++;
		if (mat->layers[i].tex_weight.isValid())
			numTex++;
		if (mat->layers[i].tex_light.isValid())
			numTex++;
		if (mat->layers[i].tex_normal.isValid())
			numTex++;
		if (mat->layers[i].tex_transm.isValid())
			numTex++;
		if (mat->layers[i].tex_aniso.isValid())
			numTex++;
		if (mat->layers[i].tex_aniso_angle.isValid())
			numTex++;
	}
	if (mat->tex_opacity.isValid())
		numTex++;
	if (mat->tex_displacement.isValid())
		numTex++;

	char cNumTex[16];
	sprintf_s(cNumTex, 16, "%d", numTex);
	char cCat[16];
	sprintf_s(cCat, 16, "%d", upCategory);
	char * matName;
	if (!mat->name  ||  (int)strlen(mat->name)<1)
		matName = "noname";
	else
		matName = mat->name;
	
	char * descr = NULL;
	bool fdesc = false;
	EMEditSimple * emes = GetEMEditSimpleInstance(GetDlgItem(upHwnd, IDC_UPLOAD_DESCRIPTION));
	if (emes)
	{
		fdesc = true;
		descr = emes->getText();
	}
	if (!descr)
	{
		descr = "";
		fdesc = false;
	}

	struct curl_httppost *post=NULL;
	struct curl_httppost *last=NULL;
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "fLogin",
			   CURLFORM_COPYCONTENTS, MatEditorWindow::onlinematlib.username, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "fPassword",
			   CURLFORM_COPYCONTENTS, MatEditorWindow::onlinematlib.passwordhash, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "category_id",
               CURLFORM_COPYCONTENTS, cCat, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "layers",
			   CURLFORM_COPYCONTENTS, cNumLayers, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "textures",
			   CURLFORM_COPYCONTENTS, cNumTex, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "description",
               CURLFORM_COPYCONTENTS, descr, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "material_name",
               CURLFORM_COPYCONTENTS, matName, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "file0",
			   CURLFORM_CONTENTTYPE, "application/octet-stream",
			   CURLFORM_FILE, matArchFilename, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "file1",
			   CURLFORM_CONTENTTYPE, "application/octet-stream",
			   CURLFORM_FILE, matPrevFilename, CURLFORM_END);

	curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
	curl_easy_setopt(curl, CURLOPT_URL, strURL); 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); 

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

	nowSending = true;
	setStatusText("Sending...");
	Logger::add(" Sending");

	res = curl_easy_perform(curl); 

	if (res)
	{
		setStatusText("Connection error. Not sent.");
		Logger::add(" Connection error. Not sent.");
	}

	// always cleanup 
	curl_easy_cleanup(curl); 
	curl_global_cleanup();
	fclose(file);
	if (fdesc)
		free(descr);
	descr = NULL;

	int s = checkSendError();

	if (s && abortedByUser)
	{
		setStatusText("Aborted by user.");
	}
	else
		switch (s)
		{
			case 0:
				setStatusText("Successfully sent. Thank you.");
				Logger::add(" Successfully sent. Thank you.");
				break;
			case 1:
				setStatusText("Error: no user/pass. Not sent.");
				Logger::add(" Error: no user/pass. Not sent.");
				break;
			case 2:
				setStatusText("Error: bad user/pass. Not sent.");
				Logger::add(" Error: bad user/pass. Not sent.");
				MatEditorWindow::onlinematlib.invalidUserPass = true;
				break;
			case 3:
				setStatusText("Error: missing file. Not sent.");
				Logger::add(" Error: missing file. Not sent.");
				break;
			case 4:
				setStatusText("Error: transfer error. Not sent.");
				Logger::add(" Error: transfer error. Not sent.");
				break;
			case 5:
				setStatusText("Temporary secure policy: wrong user. Not sent");
				Logger::add(" Temporary secure policy: wrong user. Not sent");
				break;
			case 1001:
				setStatusText("Can't open result file. Consider as not sent.");
				Logger::add(" Can't open result file. Consider as not sent.");
				break;
			case 1002:
				setStatusText("Wrong result file size. Consider as not sent.");
				Logger::add(" Wrong result file size. Consider as not sent.");
				break;
			case 1003:
				setStatusText("Other error: Consider as not sent.");
				Logger::add(" Other error: Consider as not sent.");
				break;
			default:
				setStatusText("Other error: Consider as not sent.");
				Logger::add(" Other error: Consider as not sent.");
				break;
		}

	nowSending = false;

	Logger::add(" Cleaning up");
	cleanUpAfterSending();
	unlockAfterSend();

	if (!s)
		disableControl(upHwnd, IDC_UPLOAD_SEND);
	
	Logger::add(" Done");

	return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

void initializeUploadCatButtons(HWND hwnd)
{
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_BRICKS),		HWND_TOP,   38, 123,    50,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_CARPAINT),		HWND_TOP,   88, 123,    63,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_CERAMIC),		HWND_TOP,  151, 123,    60,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_CONCRETE),		HWND_TOP,  211, 123,    69,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_EMITTER),		HWND_TOP,  280, 123,    58,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_FABRIC),		HWND_TOP,  338, 123,    50,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_FOOD),			HWND_TOP,  388, 123,    42,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_GLASS),		HWND_TOP,  430, 123,    47,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_GROUND),		HWND_TOP,  477, 123,    58,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_LEATHER),		HWND_TOP,   50, 146,    59,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_LIQUIDS),		HWND_TOP,  109, 146,    54,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_METAL),		HWND_TOP,  163, 146,    46,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_MINERALS),		HWND_TOP,  209, 146,    97,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_ORGANIC),		HWND_TOP,  306, 146,    60,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_PLASTIC),		HWND_TOP,  366, 146,    55,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_PORCELAIN),	HWND_TOP,  421, 146,    71,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_SSS),			HWND_TOP,  492, 146,    34,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_STONE),		HWND_TOP,  152, 170,    47,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_TILES),		HWND_TOP,  199, 170,    41,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_WOOD),			HWND_TOP,  240, 170,    46,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_WALLS),		HWND_TOP,  286, 170,    47,  22,   SWP_NOOWNERZORDER);
	SetWindowPos(GetDlgItem(hwnd, IDC_UPLOAD_CAT_OTHER),		HWND_TOP,  333, 170,    49,  22,   SWP_NOOWNERZORDER);

	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_STONE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_TILES)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WALLS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WOOD)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_EMITTER)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FABRIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FOOD)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GLASS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GROUND)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LEATHER)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LIQUIDS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_SSS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_MINERALS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PLASTIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_ORGANIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PORCELAIN)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_BRICKS)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CONCRETE)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CERAMIC)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CARPAINT)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_METAL)));
	GlobalWindowSettings::colorSchemes.apply(GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_OTHER)));

	EMImgStateButton * emCatStone		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_STONE));
	EMImgStateButton * emCatTiles		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_TILES));
	EMImgStateButton * emCatWalls		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WALLS));
	EMImgStateButton * emCatWood		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WOOD));
	EMImgStateButton * emCatEmitter		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_EMITTER));
	EMImgStateButton * emCatFabric		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FABRIC));
	EMImgStateButton * emCatFood		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FOOD));
	EMImgStateButton * emCatGlass		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GLASS));
	EMImgStateButton * emCatGround		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GROUND));
	EMImgStateButton * emCatLeather		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LEATHER));
	EMImgStateButton * emCatLiquids		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LIQUIDS));
	EMImgStateButton * emCatSSS			= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_SSS));
	EMImgStateButton * emCatMinerals	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_MINERALS));
	EMImgStateButton * emCatPlastic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PLASTIC));
	EMImgStateButton * emCatOrganic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_ORGANIC));
	EMImgStateButton * emCatPorcelain	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PORCELAIN));
	EMImgStateButton * emCatBricks		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_BRICKS));
	EMImgStateButton * emCatConcrete	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CONCRETE));
	EMImgStateButton * emCatCeramic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CERAMIC));
	EMImgStateButton * emCatCarpaint	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CARPAINT));
	EMImgStateButton * emCatMetal		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_METAL));
	EMImgStateButton * emCatOther		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_OTHER));

	emCatStone->bmAll = &(gIcons);
	emCatStone->allowUnclick = false;
	emCatStone->colBorderSelected = emCatStone->colBorder;
	emCatStone->pNx = emCatStone->pMx = emCatStone->pCx = emCatStone->pDx = 320;
	emCatStone->pNy = 128;
	emCatStone->pMy = 156;
	emCatStone->pCy = 184;
	emCatStone->pDy = 212;

	emCatTiles->bmAll = &(gIcons);
	emCatTiles->allowUnclick = false;
	emCatTiles->colBorderSelected = emCatTiles->colBorder;
	emCatTiles->pNx = emCatTiles->pMx = emCatTiles->pCx = emCatTiles->pDx = 273;
	emCatTiles->pNy = 128;
	emCatTiles->pMy = 156;
	emCatTiles->pCy = 184;
	emCatTiles->pDy = 212;

	emCatWalls->bmAll = &(gIcons);
	emCatWalls->allowUnclick = false;
	emCatWalls->colBorderSelected = emCatWalls->colBorder;
	emCatWalls->pNx = emCatWalls->pMx = emCatWalls->pCx = emCatWalls->pDx = 220;
	emCatWalls->pNy = 128;
	emCatWalls->pMy = 156;
	emCatWalls->pCy = 184;
	emCatWalls->pDy = 212;

	emCatWood->bmAll = &(gIcons);
	emCatWood->allowUnclick = false;
	emCatWood->colBorderSelected = emCatWood->colBorder;
	emCatWood->pNx = emCatWood->pMx = emCatWood->pCx = emCatWood->pDx = 168;
	emCatWood->pNy = 128;
	emCatWood->pMy = 156;
	emCatWood->pCy = 184;
	emCatWood->pDy = 212;

	emCatEmitter->bmAll = &(gIcons);
	emCatEmitter->allowUnclick = false;
	emCatEmitter->colBorderSelected = emCatEmitter->colBorder;
	emCatEmitter->pNx = emCatEmitter->pMx = emCatEmitter->pCx = emCatEmitter->pDx = 104;
	emCatEmitter->pNy = 128;
	emCatEmitter->pMy = 156;
	emCatEmitter->pCy = 184;
	emCatEmitter->pDy = 212;

	emCatFabric->bmAll = &(gIcons);
	emCatFabric->allowUnclick = false;
	emCatFabric->colBorderSelected = emCatFabric->colBorder;
	emCatFabric->pNx = emCatFabric->pMx = emCatFabric->pCx = emCatFabric->pDx = 1010;
	emCatFabric->pNy = 128;
	emCatFabric->pMy = 156;
	emCatFabric->pCy = 184;
	emCatFabric->pDy = 212;

	emCatFood->bmAll = &(gIcons);
	emCatFood->allowUnclick = false;
	emCatFood->colBorderSelected = emCatFood->colBorder;
	emCatFood->pNx = emCatFood->pMx = emCatFood->pCx = emCatFood->pDx = 962;
	emCatFood->pNy = 128;
	emCatFood->pMy = 156;
	emCatFood->pCy = 184;
	emCatFood->pDy = 212;

	emCatGlass->bmAll = &(gIcons);
	emCatGlass->allowUnclick = false;
	emCatGlass->colBorderSelected = emCatGlass->colBorder;
	emCatGlass->pNx = emCatGlass->pMx = emCatGlass->pCx = emCatGlass->pDx = 909;
	emCatGlass->pNy = 128;
	emCatGlass->pMy = 156;
	emCatGlass->pCy = 184;
	emCatGlass->pDy = 212;

	emCatGround->bmAll = &(gIcons);
	emCatGround->allowUnclick = false;
	emCatGround->colBorderSelected = emCatGround->colBorder;
	emCatGround->pNx = emCatGround->pMx = emCatGround->pCx = emCatGround->pDx = 845;
	emCatGround->pNy = 128;
	emCatGround->pMy = 156;
	emCatGround->pCy = 184;
	emCatGround->pDy = 212;

	emCatLeather->bmAll = &(gIcons);
	emCatLeather->allowUnclick = false;
	emCatLeather->colBorderSelected = emCatLeather->colBorder;
	emCatLeather->pNx = emCatLeather->pMx = emCatLeather->pCx = emCatLeather->pDx = 780;
	emCatLeather->pNy = 128;
	emCatLeather->pMy = 156;
	emCatLeather->pCy = 184;
	emCatLeather->pDy = 212;

	emCatLiquids->bmAll = &(gIcons);
	emCatLiquids->allowUnclick = false;
	emCatLiquids->colBorderSelected = emCatLiquids->colBorder;
	emCatLiquids->pNx = emCatLiquids->pMx = emCatLiquids->pCx = emCatLiquids->pDx = 720;
	emCatLiquids->pNy = 128;
	emCatLiquids->pMy = 156;
	emCatLiquids->pCy = 184;
	emCatLiquids->pDy = 212;

	emCatSSS->bmAll = &(gIcons);
	emCatSSS->allowUnclick = false;
	emCatSSS->colBorderSelected = emCatSSS->colBorder;
	emCatSSS->pNx = emCatSSS->pMx = emCatSSS->pCx = emCatSSS->pDx = 373;
	emCatSSS->pNy = 128;
	emCatSSS->pMy = 156;
	emCatSSS->pCy = 184;
	emCatSSS->pDy = 212;

	emCatMinerals->bmAll = &(gIcons);
	emCatMinerals->allowUnclick = false;
	emCatMinerals->colBorderSelected = emCatMinerals->colBorder;
	emCatMinerals->pNx = emCatMinerals->pMx = emCatMinerals->pCx = emCatMinerals->pDx = 617;
	emCatMinerals->pNy = 128;
	emCatMinerals->pMy = 156;
	emCatMinerals->pCy = 184;
	emCatMinerals->pDy = 212;

	emCatPlastic->bmAll = &(gIcons);
	emCatPlastic->allowUnclick = false;
	emCatPlastic->colBorderSelected = emCatPlastic->colBorder;
	emCatPlastic->pNx = emCatPlastic->pMx = emCatPlastic->pCx = emCatPlastic->pDx = 556;
	emCatPlastic->pNy = 128;
	emCatPlastic->pMy = 156;
	emCatPlastic->pCy = 184;
	emCatPlastic->pDy = 212;

	emCatOrganic->bmAll = &(gIcons);
	emCatOrganic->allowUnclick = false;
	emCatOrganic->colBorderSelected = emCatOrganic->colBorder;
	emCatOrganic->pNx = emCatOrganic->pMx = emCatOrganic->pCx = emCatOrganic->pDx = 490;
	emCatOrganic->pNy = 128;
	emCatOrganic->pMy = 156;
	emCatOrganic->pCy = 184;
	emCatOrganic->pDy = 212;

	emCatPorcelain->bmAll = &(gIcons);
	emCatPorcelain->allowUnclick = false;
	emCatPorcelain->colBorderSelected = emCatPorcelain->colBorder;
	emCatPorcelain->pNx = emCatPorcelain->pMx = emCatPorcelain->pCx = emCatPorcelain->pDx = 413;
	emCatPorcelain->pNy = 128;
	emCatPorcelain->pMy = 156;
	emCatPorcelain->pCy = 184;
	emCatPorcelain->pDy = 212;

	emCatBricks->bmAll = &(gIcons);
	emCatBricks->allowUnclick = false;
	emCatBricks->colBorderSelected = emCatBricks->colBorder;
	emCatBricks->pNx = emCatBricks->pMx = emCatBricks->pCx = emCatBricks->pDx = 1334;
	emCatBricks->pNy = 128;
	emCatBricks->pMy = 156;
	emCatBricks->pCy = 184;
	emCatBricks->pDy = 212;

	emCatConcrete->bmAll = &(gIcons);
	emCatConcrete->allowUnclick = false;
	emCatConcrete->colBorderSelected = emCatConcrete->colBorder;
	emCatConcrete->pNx = emCatConcrete->pMx = emCatConcrete->pCx = emCatConcrete->pDx = 1072;
	emCatConcrete->pNy = 128;
	emCatConcrete->pMy = 156;
	emCatConcrete->pCy = 184;
	emCatConcrete->pDy = 212;
	
	emCatCeramic->bmAll = &(gIcons);
	emCatCeramic->allowUnclick = false;
	emCatCeramic->colBorderSelected = emCatCeramic->colBorder;
	emCatCeramic->pNx = emCatCeramic->pMx = emCatCeramic->pCx = emCatCeramic->pDx = 1147;
	emCatCeramic->pNy = 128;
	emCatCeramic->pMy = 156;
	emCatCeramic->pCy = 184;
	emCatCeramic->pDy = 212;
	
	emCatCarpaint->bmAll = &(gIcons);
	emCatCarpaint->allowUnclick = false;
	emCatCarpaint->colBorderSelected = emCatCarpaint->colBorder;
	emCatCarpaint->pNx = emCatCarpaint->pMx = emCatCarpaint->pCx = emCatCarpaint->pDx = 1213;
	emCatCarpaint->pNy = 128;
	emCatCarpaint->pMy = 156;
	emCatCarpaint->pCy = 184;
	emCatCarpaint->pDy = 212;
	
	emCatMetal->bmAll = &(gIcons);
	emCatMetal->allowUnclick = false;
	emCatMetal->colBorderSelected = emCatMetal->colBorder;
	emCatMetal->pNx = emCatMetal->pMx = emCatMetal->pCx = emCatMetal->pDx = 1282;
	emCatMetal->pNy = 128;
	emCatMetal->pMy = 156;
	emCatMetal->pCy = 184;
	emCatMetal->pDy = 212;

	emCatOther->bmAll = &(gIcons);
	emCatOther->allowUnclick = false;
	emCatOther->colBorderSelected = emCatOther->colBorder;
	emCatOther->pNx = emCatOther->pMx = emCatOther->pCx = emCatOther->pDx = 49;
	emCatOther->pNy = 128;
	emCatOther->pMy = 156;
	emCatOther->pCy = 184;
	emCatOther->pDy = 212;
}

//-----------------------------------------------------------------------------------------------------------------------

void unclickAllUploadCategories(HWND hwnd, int select)
{
	EMImgStateButton * emCatStone		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_STONE));
	EMImgStateButton * emCatTiles		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_TILES));
	EMImgStateButton * emCatWalls		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WALLS));
	EMImgStateButton * emCatWood		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WOOD));
	EMImgStateButton * emCatEmitter		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_EMITTER));
	EMImgStateButton * emCatFabric		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FABRIC));
	EMImgStateButton * emCatFood		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FOOD));
	EMImgStateButton * emCatGlass		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GLASS));
	EMImgStateButton * emCatGround		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GROUND));
	EMImgStateButton * emCatLeather		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LEATHER));
	EMImgStateButton * emCatLiquids		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LIQUIDS));
	EMImgStateButton * emCatSSS			= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_SSS));
	EMImgStateButton * emCatMinerals	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_MINERALS));
	EMImgStateButton * emCatPlastic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PLASTIC));
	EMImgStateButton * emCatOrganic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_ORGANIC));
	EMImgStateButton * emCatPorcelain	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PORCELAIN));
	EMImgStateButton * emCatBricks		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_BRICKS));
	EMImgStateButton * emCatConcrete	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CONCRETE));
	EMImgStateButton * emCatCeramic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CERAMIC));
	EMImgStateButton * emCatCarpaint	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CARPAINT));
	EMImgStateButton * emCatMetal		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_METAL));
	EMImgStateButton * emCatOther		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_OTHER));

	emCatStone->stateOn = false;
	emCatTiles->stateOn = false;
	emCatWalls->stateOn = false;
	emCatWood->stateOn = false;
	emCatEmitter->stateOn = false;
	emCatFabric->stateOn = false;
	emCatFood->stateOn = false;
	emCatGlass->stateOn = false;
	emCatGround->stateOn = false;
	emCatLeather->stateOn = false;
	emCatLiquids->stateOn = false;
	emCatSSS->stateOn = false;
	emCatMinerals->stateOn = false;
	emCatPlastic->stateOn = false;
	emCatOrganic->stateOn = false;
	emCatPorcelain->stateOn = false;
	emCatBricks->stateOn = false;
	emCatConcrete->stateOn = false;
	emCatCeramic->stateOn = false;
	emCatCarpaint->stateOn = false;
	emCatMetal->stateOn = false;
	emCatOther->stateOn = false;

	switch (select)
	{
		case MatCategory::CAT_EMITTER:
			emCatEmitter->stateOn = true;
			break;
		case MatCategory::CAT_BRICKS:
			emCatBricks->stateOn = true;
			break;
		case MatCategory::CAT_CARPAINT:
			emCatCarpaint->stateOn = true;
			break;
		case MatCategory::CAT_CERAMIC:
			emCatCeramic->stateOn = true;
			break;
		case MatCategory::CAT_CONCRETE:
			emCatConcrete->stateOn = true;
			break;
		case MatCategory::CAT_FABRIC:
			emCatFabric->stateOn = true;
			break;
		case MatCategory::CAT_FOOD:
			emCatFood->stateOn = true;
			break;
		case MatCategory::CAT_GLASS:
			emCatGlass->stateOn = true;
			break;
		case MatCategory::CAT_GROUND:
			emCatGround->stateOn = true;
			break;
		case MatCategory::CAT_LEATHER:
			emCatLeather->stateOn = true;
			break;
		case MatCategory::CAT_LIQUIDS:
			emCatLiquids->stateOn = true;
			break;
		case MatCategory::CAT_METAL:
			emCatMetal->stateOn = true;
			break;
		case MatCategory::CAT_MINERALS_GEMS:
			emCatMinerals->stateOn = true;
			break;
		case MatCategory::CAT_ORGANIC:
			emCatOrganic->stateOn = true;
			break;
		case MatCategory::CAT_PLASTIC:
			emCatPlastic->stateOn = true;
			break;
		case MatCategory::CAT_PORCELAIN:
			emCatPorcelain->stateOn = true;
			break;
		case MatCategory::CAT_SSS:
			emCatSSS->stateOn = true;
			break;
		case MatCategory::CAT_STONE:
			emCatStone->stateOn = true;
			break;
		case MatCategory::CAT_TILES:
			emCatTiles->stateOn = true;
			break;
		case MatCategory::CAT_WALLS:
			emCatWalls->stateOn = true;
			break;
		case MatCategory::CAT_WOOD:
			emCatWood->stateOn = true;
			break;
		case MatCategory::CAT_OTHER:
			emCatOther->stateOn = true;
			break;
	}


	InvalidateRect(emCatStone->hwnd, NULL, false);
	InvalidateRect(emCatTiles->hwnd, NULL, false);
	InvalidateRect(emCatWalls->hwnd, NULL, false);
	InvalidateRect(emCatWood->hwnd, NULL, false);
	InvalidateRect(emCatEmitter->hwnd, NULL, false);
	InvalidateRect(emCatFabric->hwnd, NULL, false);
	InvalidateRect(emCatFood->hwnd, NULL, false);
	InvalidateRect(emCatGlass->hwnd, NULL, false);
	InvalidateRect(emCatGround->hwnd, NULL, false);
	InvalidateRect(emCatLeather->hwnd, NULL, false);
	InvalidateRect(emCatLiquids->hwnd, NULL, false);
	InvalidateRect(emCatSSS->hwnd, NULL, false);
	InvalidateRect(emCatMinerals->hwnd, NULL, false);
	InvalidateRect(emCatPlastic->hwnd, NULL, false);
	InvalidateRect(emCatOrganic->hwnd, NULL, false);
	InvalidateRect(emCatPorcelain->hwnd, NULL, false);
	InvalidateRect(emCatBricks->hwnd, NULL, false);
	InvalidateRect(emCatConcrete->hwnd, NULL, false);
	InvalidateRect(emCatCeramic->hwnd, NULL, false);
	InvalidateRect(emCatCarpaint->hwnd, NULL, false);
	InvalidateRect(emCatMetal->hwnd, NULL, false);
	InvalidateRect(emCatOther->hwnd, NULL, false);
}

//-----------------------------------------------------------------------------------------------------------------------

bool checkCategoryClicked(HWND hwnd)
{
	EMImgStateButton * emCatStone		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_STONE));
	EMImgStateButton * emCatTiles		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_TILES));
	EMImgStateButton * emCatWalls		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WALLS));
	EMImgStateButton * emCatWood		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_WOOD));
	EMImgStateButton * emCatEmitter		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_EMITTER));
	EMImgStateButton * emCatFabric		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FABRIC));
	EMImgStateButton * emCatFood		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_FOOD));
	EMImgStateButton * emCatGlass		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GLASS));
	EMImgStateButton * emCatGround		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_GROUND));
	EMImgStateButton * emCatLeather		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LEATHER));
	EMImgStateButton * emCatLiquids		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_LIQUIDS));
	EMImgStateButton * emCatSSS			= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_SSS));
	EMImgStateButton * emCatMinerals	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_MINERALS));
	EMImgStateButton * emCatPlastic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PLASTIC));
	EMImgStateButton * emCatOrganic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_ORGANIC));
	EMImgStateButton * emCatPorcelain	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_PORCELAIN));
	EMImgStateButton * emCatBricks		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_BRICKS));
	EMImgStateButton * emCatConcrete	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CONCRETE));
	EMImgStateButton * emCatCeramic		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CERAMIC));
	EMImgStateButton * emCatCarpaint	= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_CARPAINT));
	EMImgStateButton * emCatMetal		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_METAL));
	EMImgStateButton * emCatOther		= GetEMImgStateButtonInstance(GetDlgItem(hwnd,    IDC_UPLOAD_CAT_OTHER));

	if (emCatStone->stateOn)
		return true;
	if (emCatTiles->stateOn)
		return true;
	if (emCatWalls->stateOn)
		return true;
	if (emCatWood->stateOn)
		return true;
	if (emCatEmitter->stateOn)
		return true;
	if (emCatFabric->stateOn)
		return true;
	if (emCatFood->stateOn)
		return true;
	if (emCatGlass->stateOn)
		return true;
	if (emCatGround->stateOn)
		return true;
	if (emCatLeather->stateOn)
		return true;
	if (emCatLiquids->stateOn)
		return true;
	if (emCatSSS->stateOn)
		return true;
	if (emCatMinerals->stateOn)
		return true;
	if (emCatPlastic->stateOn)
		return true;
	if (emCatOrganic->stateOn)
		return true;
	if (emCatPorcelain->stateOn)
		return true;
	if (emCatBricks->stateOn)
		return true;
	if (emCatConcrete->stateOn)
		return true;
	if (emCatCeramic->stateOn)
		return true;
	if (emCatCarpaint->stateOn)
		return true;
	if (emCatMetal->stateOn)
		return true;
	if (emCatOther->stateOn)
		return true;

	return false;
}

//-----------------------------------------------------------------------------------------------------------------------
