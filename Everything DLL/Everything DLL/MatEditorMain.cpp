#define _CRT_RAND_S
#include "MatEditor.h"
#include "resource.h"
#include "valuesMinMax.h"
#include "XML_IO.h"
#include "log.h"
#include "Dialogs.h"
#include <mbstring.h>

HINSTANCE MatEditorWindow::hInstance = 0;
HBRUSH MatEditorWindow::backgroundBrush = 0;
HWND MatEditorWindow::hMain;
bool MatEditorWindow::saveOnOK = false;
int MatEditorWindow::clientToWindowX = 0;
int MatEditorWindow::clientToWindowY = 0;
int MatEditorWindow::libHiddenHeight = 557;
int MatEditorWindow::libShownHeight = 717;
int MatEditorWindow::width = 774;
int MatEditorWindow::libHiddenOKTop = 522;
int MatEditorWindow::libShownOKTop = 684;
int MatEditorWindow::leftCancel = 394;
int MatEditorWindow::leftOK = 304;
HANDLE MatEditorWindow::hRefreshThread = 0;
bool MatEditorWindow::refreshThreadRunning = false;
OnlineMatLib MatEditorWindow::onlinematlib;
LocalMatLib MatEditorWindow::localmatlib;
int MatEditorWindow::autoCategory = MatCategory::CAT_USER;
int MatEditorWindow::autoID = 0;
int MatEditorWindow::libMode = 1;
int MatEditorWindow::libIDs[] = {IDC_MATEDIT_LIB1 , IDC_MATEDIT_LIB2 , IDC_MATEDIT_LIB3 , IDC_MATEDIT_LIB4 , IDC_MATEDIT_LIB5 , IDC_MATEDIT_LIB6 , IDC_MATEDIT_LIB7};
char * MatEditorWindow::autoLoadFile = NULL;
BlendSettings MatEditorWindow::blendNames;
bool MatEditorWindow::needToUpdateDisplacement = true;

void updateLinked90Controls(HWND hWnd);

extern HMODULE hDllModule;

//------------------------------------------------------------------------------------------------------

MatEditorWindow::MatEditorWindow(HINSTANCE hInst)
{
	hInstance = hInst;
}

MatEditorWindow::~MatEditorWindow()
{
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::createWindow(HWND parent, int mode, char * fileToLoad)
{
	if (fileToLoad)
	{
		autoLoadFile = fileToLoad;
	}

	needToUpdateDisplacement = true;
	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->runDialogsUnicode)
	{
		return ( 0 != DialogBoxParamW(hDllModule, MAKEINTRESOURCEW(IDD_MAT_EDITOR), parent, MatEditorWindow::WndProc,(LPARAM)(mode)) );
	}
	else
	{
		return ( 0 != DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_MAT_EDITOR), parent, MatEditorWindow::WndProc,(LPARAM)(mode)) );
	}

	return true;
}

HWND MatEditorWindow::getHWND()
{
	return hMain;
}

void MatEditorWindow::setSaveMode(bool saveOnOKButton)
{
	saveOnOK = saveOnOKButton;
}

bool MatEditorWindow::materialToGUI()
{
	MaterialNox * mat = getEditedMaterial();
	if (!mat)
		return false;

	EMEditSimple * emes = GetEMEditSimpleInstance(GetDlgItem(hMain, IDC_MATEDIT_NAME));
	if (mat->name)
		emes->setText(mat->name);
	
	fillLayersList();

	EMListView * emlv = GetEMListViewInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYERS_LIST));
	CHECK(emlv);
	layerToGUI(emlv->selected);

	copyBufferMaterialToWindow();

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::GUInameToMaterial()
{
	EMEditSimple * emes = GetEMEditSimpleInstance(GetDlgItem(hMain, IDC_MATEDIT_NAME));
	CHECK(emes);
	char * name = emes->getText();
	CHECK(name);
	int l = (int)strlen(name);
	if (l<1)
		return false;
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	char * oldname = mat->name;
	mat->name = name;
	if (oldname)
		free(oldname);
	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::layerToGUI(int num)
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (num >= mat->layers.objCount  ||  num < 0)
		return false;

	MatLayer * mlay = &(mat->layers[num]);
	CHECK(mlay);

	// WEIGHT ----------
	EMEditSpin * emweight = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT));
	CHECK(emweight);
	emweight->setValuesToInt(mlay->contribution, NOX_MAT_CONTR_MIN, NOX_MAT_CONTR_MAX, 1,1);

	EMCheckBox * emweighttexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT_TEX_ON));
	CHECK(emweighttexon);
	emweighttexon->selected = mlay->use_tex_weight;
	InvalidateRect(emweighttexon->hwnd, NULL, false);


	// DISPLACEMENT
	EMCheckBox * emdisplacementon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_ON));
	CHECK(emdisplacementon);
	emdisplacementon->selected = mat->use_tex_displacement;
	InvalidateRect(emdisplacementon->hwnd, NULL, false);

	EMCheckBox * emdisplacementcontinuity = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_CONTINUITY));
	CHECK(emdisplacementcontinuity);
	emdisplacementcontinuity->selected = mat->displ_continuity;
	InvalidateRect(emdisplacementcontinuity->hwnd, NULL, false);

	EMCheckBox * emdisplacementignorescale = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_IGNORE_SCALE));
	CHECK(emdisplacementignorescale);
	emdisplacementignorescale->selected = mat->displ_ignore_scale;
	InvalidateRect(emdisplacementignorescale->hwnd, NULL, false);

	EMEditSpin * emdisplacementdepth = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_DEPTH));
	CHECK(emdisplacementdepth);
	emdisplacementdepth->setValuesToFloat(mat->displ_depth, NOX_MAT_DISP_DEPTH_MIN, NOX_MAT_DISP_DEPTH_MAX, 0.01f, 0.01f);

	EMEditSpin * emdisplacementshift = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_SHIFT));
	CHECK(emdisplacementshift);
	emdisplacementshift->setValuesToFloat(mat->displ_shift, NOX_MAT_DISP_SHIFT_MIN, NOX_MAT_DISP_SHIFT_MAX, 0.01f, 0.01f);

	EMComboBox * emdisplacementsubdivs = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_SUBDIVS));
	CHECK(emdisplacementsubdivs);
	emdisplacementsubdivs->selected = mat->displ_subdivs_lvl-1;
	InvalidateRect(emdisplacementsubdivs->hwnd, NULL, false);

	EMComboBox * emdisplacementnormalmode = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_NORMALS));
	CHECK(emdisplacementnormalmode);
	switch (mat->displ_normal_mode)
	{
		case MaterialNox::NM_SMOOTH:	emdisplacementnormalmode->selected = 0;		break;
		case MaterialNox::NM_FACE:		emdisplacementnormalmode->selected = 1;		break;
		case MaterialNox::NM_SOURCE:	emdisplacementnormalmode->selected = 2;		break;
		default:						emdisplacementnormalmode->selected = 0;		break;
	}
	InvalidateRect(emdisplacementnormalmode->hwnd, NULL, false);

	// NORMAL
	EMCheckBox * emnormaltexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_NORMAL_ON));
	CHECK(emnormaltexon);
	emnormaltexon->selected = mlay->use_tex_normal;
	InvalidateRect(emnormaltexon->hwnd, NULL, false);


	// EMISSION ----------
	EMCheckBox * ememissionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_ON));
	CHECK(ememissionon);
	ememissionon->selected = (mlay->type == MatLayer::TYPE_EMITTER);
	InvalidateRect(ememissionon->hwnd, NULL, false);

	EMColorShow * ememissioncolor = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR));
	CHECK(ememissioncolor);
	ememissioncolor->redraw(mlay->color);

	EMEditSpin * ememissionpower = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_POWER));
	CHECK(ememissionpower);
	ememissionpower->setValuesToFloat(mlay->power, NOX_MAT_EMPOWER_MIN, NOX_MAT_EMPOWER_MAX, 1, 5);

	EMComboBox * ememissionunit = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_UNIT));
	CHECK(ememissionunit);
	ememissionunit->selected = mlay->unit-1;
	InvalidateRect(ememissionunit->hwnd, NULL, false);

	EMCheckBox * ememissiontexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON));
	CHECK(ememissiontexon);
	ememissiontexon->selected = mlay->use_tex_light;
	InvalidateRect(ememissiontexon->hwnd, NULL, false);

	EMCheckBox * ememissionskyportal = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_SKYPORTAL));
	CHECK(ememissionskyportal);
	ememissionskyportal->selected = mat->isSkyPortal;
	InvalidateRect(ememissionskyportal->hwnd, NULL, false);

	EMButton * emiesbutton = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_IES_LOAD));
	CHECK(emiesbutton);
	IesNOX * ies = mlay->ies;
	if (ies)
		if (ies->numAnglesH<1 || ies->numAnglesV<1 || ies->values==NULL || ies->anglesH==NULL || ies->anglesV==NULL || ies->filename==NULL)
			emiesbutton->selected = false;
		else
			emiesbutton->selected = true;
	else
		emiesbutton->selected = false;
	InvalidateRect(emiesbutton->hwnd, NULL, false);

	EMComboBox * ememissionblend = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES));
	CHECK(ememissionblend);
	ememissionblend->selected = mat->blendIndex;
	InvalidateRect(ememissionblend->hwnd, NULL, false);

	// REFLECTANCE ----------
	EMEditSpin * emrough = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS));
	CHECK(emrough);
	emrough->setValuesToFloat(mlay->roughness*100, NOX_MAT_ROUGH_MIN, NOX_MAT_ROUGH_MAX, 0.5f, 0.1f);

	EMCheckBox * emlinked90 = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_LINKED));
	CHECK(emlinked90);
	emlinked90->selected = mlay->connect90;
	InvalidateRect(emlinked90->hwnd, NULL, false);

	EMColorShow * emrefl0 = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR));
	CHECK(emrefl0);
	emrefl0->redraw(mlay->refl0);

	EMColorShow * emrefl90 = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR));
	CHECK(emrefl90);
	emrefl90->redraw(mlay->refl90);

	EMCheckBox * emroughtexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON));
	CHECK(emroughtexon);
	emroughtexon->selected = mlay->use_tex_rough;
	InvalidateRect(emroughtexon->hwnd, NULL, false);

	EMCheckBox * emrefl0texon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON));
	CHECK(emrefl0texon);
	emrefl0texon->selected = mlay->use_tex_col0;
	InvalidateRect(emrefl0texon->hwnd, NULL, false);

	EMCheckBox * emrefl90texon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_TEX_ON));
	CHECK(emrefl90texon);
	emrefl90texon->selected = mlay->use_tex_col90;
	InvalidateRect(emrefl90texon->hwnd, NULL, false);

	EMEditSpin * emaniso = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY));
	CHECK(emaniso);
	emaniso->setValuesToFloat(mlay->anisotropy*100, NOX_MAT_ANISO_MIN, NOX_MAT_ANISO_MAX, 0.5f, 0.1f);

	EMEditSpin * emanisoangle = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE));
	CHECK(emanisoangle);
	emanisoangle->setValuesToFloat(mlay->aniso_angle, NOX_MAT_ANISO_ANGLE_MIN, NOX_MAT_ANISO_ANGLE_MAX, 0.5f, 0.1f);

	EMCheckBox * emanisotexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON));
	CHECK(emanisotexon);
	emanisotexon->selected = mlay->use_tex_aniso;
	InvalidateRect(emanisotexon->hwnd, NULL, false);

	EMCheckBox * emanisoangletexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON));
	CHECK(emanisoangletexon);
	emanisoangletexon->selected = mlay->use_tex_aniso_angle;
	InvalidateRect(emanisoangletexon->hwnd, NULL, false);

	// FRESNEL ----------
	EMEditSpin * emior = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_IOR));
	CHECK(emior);
	emior->setValuesToFloat(mlay->fresnel.IOR,  NOX_MAT_IOR_MIN, NOX_MAT_IOR_MAX, 0.1f, 0.01f);

	EMFresnel * emfresnel = GetEMFresnelInstance(GetDlgItem(hMain, IDC_MATEDIT_FRESNEL));
	CHECK(emfresnel);
	emfresnel->mode = Fresnel::MODE_FRESNEL;
	emfresnel->IOR = mlay->fresnel.IOR;
	InvalidateRect(emfresnel->hwnd, NULL, false);

	// TRANSMISSION ----------
	EMCheckBox * emtransmissionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_ON));
	CHECK(emtransmissionon);
	emtransmissionon->selected = mlay->transmOn;
	InvalidateRect(emtransmissionon->hwnd, NULL, false);
	
	EMCheckBox * emabsorptionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_ON));
	CHECK(emabsorptionon);
	emabsorptionon->selected = mlay->absOn;
	InvalidateRect(emabsorptionon->hwnd, NULL, false);

	EMCheckBox * emdispersionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_ON));
	CHECK(emdispersionon);
	emdispersionon->selected = mlay->dispersionOn;
	InvalidateRect(emdispersionon->hwnd, NULL, false);

	EMButton * emtransmtex = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX));
	CHECK(emtransmtex);
	emtransmtex->selected = mlay->use_tex_transm;
	InvalidateRect(emtransmtex->hwnd, NULL, false);

	EMCheckBox * emtrtexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON));
	CHECK(emtrtexon);
	emtrtexon->selected = mlay->use_tex_transm;
	InvalidateRect(emtrtexon->hwnd, NULL, false);

	EMCheckBox * emfakeglasson = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_FAKE_GLASS_ON));
	CHECK(emfakeglasson);
	emfakeglasson->selected = mlay->fakeGlass;
	InvalidateRect(emfakeglasson->hwnd, NULL, false);

	EMCheckBox * emopacitytexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_OPACITY_TEX_ON));
	CHECK(emopacitytexon);
	emopacitytexon->selected = mat->use_tex_opacity;
	InvalidateRect(emopacitytexon->hwnd, NULL, false);

	EMEditSpin * emopacity = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_OPACITY));
	CHECK(emopacity);
	emopacity->setValuesToFloat(mat->opacity*100, NOX_MAT_OPACITY_MIN, NOX_MAT_OPACITY_MAX, 0.5f, 0.1f);

	EMColorShow * emtrcol = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR));
	CHECK(emtrcol);
	emtrcol->redraw(mlay->transmColor);

	EMColorShow * emabsorptioncolor = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR));
	CHECK(emabsorptioncolor);
	emabsorptioncolor->redraw(mlay->absColor);

	EMEditSpin * emabsorptiondist = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE));
	CHECK(emabsorptiondist);
	emabsorptiondist->floatAfterDot = 3;
	emabsorptiondist->setValuesToFloat(mlay->absDist*100, NOX_MAT_ABSDIST_MIN, NOX_MAT_ABSDIST_MAX, 1, 0.1f);

	EMEditSpin * emdispersionsize = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_SIZE));
	CHECK(emdispersionsize);
	emdispersionsize->floatAfterDot = 3;
	emdispersionsize->setValuesToFloat(mlay->dispersionValue, NOX_MAT_DISPERSION_MIN, NOX_MAT_DISPERSION_MAX,0.1f, 0.001f);

	EMButton * emtexcol0 = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_TEX));
	CHECK(emtexcol0);
	//emtexcol0->selected = mlay->tex_col0.bmp.isValid();
	emtexcol0->selected = mlay->tex_col0.isValid();
	InvalidateRect(emtexcol0->hwnd, NULL, false);

	EMButton * emtexcol90 = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_TEX));
	CHECK(emtexcol90);
	emtexcol90->selected = mlay->tex_col90.isValid();
	InvalidateRect(emtexcol90->hwnd, NULL, false);

	EMButton * emtexrough = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS_TEX));
	CHECK(emtexrough);
	emtexrough->selected = mlay->tex_rough.isValid();
	InvalidateRect(emtexrough->hwnd, NULL, false);

	EMButton * emtexweight = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT_TEX));
	CHECK(emtexweight);
	//emtexweight->selected = mlay->tex_weight.bmp.isValid();
	emtexweight->selected = mlay->tex_weight.isValid();
	InvalidateRect(emtexweight->hwnd, NULL, false);

	EMButton * emtexlight = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX));
	CHECK(emtexlight);
	emtexlight->selected = mlay->tex_light.isValid();
	InvalidateRect(emtexlight->hwnd, NULL, false);

	EMButton * emtexnormal = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_NORMAL_TEX));
	CHECK(emtexnormal);
	emtexnormal->selected = mlay->tex_normal.isValid();
	InvalidateRect(emtexnormal->hwnd, NULL, false);

	EMButton * emtextransm = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX));
	CHECK(emtextransm);
	emtextransm->selected = mlay->tex_transm.isValid();
	InvalidateRect(emtextransm->hwnd, NULL, false);

	EMButton * emtexaniso = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY_TEX));
	CHECK(emtexaniso);
	emtexaniso->selected = mlay->tex_aniso.isValid();
	InvalidateRect(emtexaniso->hwnd, NULL, false);

	EMButton * emtexanisoangle = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX));
	CHECK(emtexanisoangle);
	emtexanisoangle->selected = mlay->tex_aniso_angle.isValid();
	InvalidateRect(emtexanisoangle->hwnd, NULL, false);

	EMButton * emtexopacity = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_OPACITY_TEX));
	CHECK(emtexopacity);
	emtexopacity->selected = mat->tex_opacity.isValid();
	InvalidateRect(emtexopacity->hwnd, NULL, false);

	EMButton * emtexdisplacement = GetEMButtonInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_TEX));
	CHECK(emtexdisplacement);
	emtexdisplacement->selected = mat->tex_displacement.isValid();
	InvalidateRect(emtexdisplacement->hwnd, NULL, false);

	EMCheckBox * emssson = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_SSS_ON));
	CHECK(emssson);
	emssson->selected = mlay->sssON;
	InvalidateRect(emssson->hwnd, NULL, false);

	EMEditSpin * emssdensity = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_SSS_DENSITY));
	CHECK(emssdensity);
	emssdensity->setValuesToFloat(mlay->sssDens, NOX_MAT_SSS_DENSITY_MIN, NOX_MAT_SSS_DENSITY_MAX, 1.0f, 0.5f);

	EMEditSpin * emssscolldir = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_SSS_COLLISION_DIR));
	CHECK(emssscolldir);
	emssscolldir->setValuesToFloat(mlay->sssCollDir, NOX_MAT_SSS_COLL_MIN, NOX_MAT_SSS_COLL_MAX, 0.1f, 0.02f);;

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::GUItoLayer(int num)
{
	autoCategory = MatCategory::CAT_USER;
	autoID = 0;

	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (num >= mat->layers.objCount  ||  num < 0)
		return false;

	MatLayer * mlay = &(mat->layers[num]);
	CHECK(mlay);

	// WEIGHT ----------
	EMEditSpin * emweight = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT));
	CHECK(emweight);
	mlay->contribution = emweight->intValue;

	EMCheckBox * emweighttexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_WEIGHT_TEX_ON));
	CHECK(emweighttexon);
	mlay->use_tex_weight = emweighttexon->selected;

	// DISPLACEMENT
	EMCheckBox * emdisplacementon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_ON));
	CHECK(emdisplacementon);
	mat->use_tex_displacement = emdisplacementon->selected;

	EMCheckBox * emdisplacementcontinuity = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_CONTINUITY));
	CHECK(emdisplacementcontinuity);
	mat->displ_continuity = emdisplacementcontinuity->selected;

	EMCheckBox * emdisplacementignorescale = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_IGNORE_SCALE));
	CHECK(emdisplacementignorescale);
	mat->displ_ignore_scale = emdisplacementignorescale->selected;

	EMEditSpin * emdisplacementdepth = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_DEPTH));
	CHECK(emdisplacementdepth);
	mat->displ_depth = emdisplacementdepth->floatValue;

	EMEditSpin * emdisplacementshift = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_SHIFT));
	CHECK(emdisplacementshift);
	mat->displ_shift = emdisplacementshift->floatValue;

	EMComboBox * emdisplacementnormalmode = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_NORMALS));
	CHECK(emdisplacementnormalmode);
	switch (emdisplacementnormalmode->selected)
	{
		case 1:  mat->displ_normal_mode = MaterialNox::NM_FACE;		break;
		case 2:  mat->displ_normal_mode = MaterialNox::NM_SOURCE;	break;
		default: mat->displ_normal_mode = MaterialNox::NM_SMOOTH;	break;
	}

	EMComboBox * emdisplacementsubdivs = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPL_SUBDIVS));
	CHECK(emdisplacementsubdivs);
	mat->displ_subdivs_lvl = emdisplacementsubdivs->selected+1;

	// NORMAL
	EMCheckBox * emnormaltexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_NORMAL_ON));
	CHECK(emnormaltexon);
	mlay->use_tex_normal = emnormaltexon->selected;


	// EMISSION ----------
	EMCheckBox * ememissionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_ON));
	CHECK(ememissionon);
	mlay->type = (ememissionon->selected ? MatLayer::TYPE_EMITTER : MatLayer::TYPE_SHADE);

	EMColorShow * ememissioncolor = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR));
	CHECK(ememissioncolor);
	mlay->color = ememissioncolor->color;

	EMEditSpin * ememissionpower = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_POWER));
	CHECK(ememissionpower);
	mlay->power = ememissionpower->floatValue;

	EMComboBox * ememissionunit = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_UNIT));
	CHECK(ememissionunit);
	mlay->unit = ememissionunit->selected+1;

	EMCheckBox * ememissiontexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON));
	CHECK(ememissiontexon);
	mlay->use_tex_light = ememissiontexon->selected;

	EMComboBox * ememissionblend = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES));
	CHECK(ememissionblend);
	mat->blendIndex = ememissionblend->selected;

	EMCheckBox * ememissionskyportal = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_SKYPORTAL));
	CHECK(ememissionskyportal);
	mat->isSkyPortal = ememissionskyportal->selected;

	// REFLECTANCE ----------
	EMCheckBox * emlinked90 = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_LINKED));
	CHECK(emlinked90);
	mlay->connect90 = emlinked90->selected;

	EMEditSpin * emrough = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS));
	CHECK(emrough);
	mlay->roughness = emrough->floatValue * 0.01f;

	EMColorShow * emrefl0 = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR));
	CHECK(emrefl0);
	mlay->refl0 = emrefl0->color;

	EMColorShow * emrefl90 = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR));
	CHECK(emrefl90);
	mlay->refl90 = emrefl90->color;

	EMCheckBox * emroughtexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON));
	CHECK(emroughtexon);
	mlay->use_tex_rough = emroughtexon->selected;

	EMCheckBox * emrefl0texon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON));
	CHECK(emrefl0texon);
	mlay->use_tex_col0 = emrefl0texon->selected;

	EMCheckBox * emrefl90texon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR_TEX_ON));
	CHECK(emrefl90texon);
	mlay->use_tex_col90 = emrefl90texon->selected;

	EMEditSpin * emaniso = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY));
	CHECK(emaniso);
	mlay->anisotropy = emaniso->floatValue * 0.01f;

	EMEditSpin * emanisoangle = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE));
	CHECK(emanisoangle);
	mlay->aniso_angle = emanisoangle->floatValue;

	EMCheckBox * emanisotexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON));
	CHECK(emanisotexon);
	mlay->use_tex_aniso = emanisotexon->selected;

	EMCheckBox * emanisoangletexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON));
	CHECK(emanisoangletexon);
	mlay->use_tex_aniso_angle = emanisoangletexon->selected;


	// FRESNEL ----------
	EMEditSpin * emior = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_IOR));
	CHECK(emior);
	mlay->fresnel.IOR = emior->floatValue;

	// TRANSMISSION ----------
	EMCheckBox * emtransmissionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_ON));
	CHECK(emtransmissionon);
	mlay->transmOn = emtransmissionon->selected;

	EMCheckBox * emabsorptionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_ON));
	CHECK(emabsorptionon);
	mlay->absOn = emabsorptionon->selected;

	EMCheckBox * emtransmtexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON));
	CHECK(emtransmtexon);
	mlay->use_tex_transm = emtransmtexon->selected;

	EMCheckBox * emdispersionon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_ON));
	CHECK(emdispersionon);
	mlay->dispersionOn = emdispersionon->selected;

	EMCheckBox * emfakeglasson = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_FAKE_GLASS_ON));
	CHECK(emfakeglasson);
	mlay->fakeGlass = emfakeglasson->selected;

	EMCheckBox * emopacitytexon = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_OPACITY_TEX_ON));
	CHECK(emopacitytexon);
	mat->use_tex_opacity = emopacitytexon->selected;

	EMEditSpin * emopacity = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_OPACITY));
	CHECK(emopacity);
	mat->opacity = emopacity->floatValue/100.0f;

	EMColorShow * emtrcolor = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR));
	CHECK(emtrcolor);
	mlay->transmColor = emtrcolor->color;

	EMColorShow * emabsorptioncolor = GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR));
	CHECK(emabsorptioncolor);
	mlay->absColor = emabsorptioncolor->color;

	EMEditSpin * emabsorptiondist = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE));
	CHECK(emabsorptiondist);
	mlay->absDist = emabsorptiondist->floatValue/100.0f;

	EMEditSpin * emdispersionsize = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_DISPERSION_SIZE));
	CHECK(emdispersionsize);
	mlay->dispersionValue = emdispersionsize->floatValue;

	EMCheckBox * emssson = GetEMCheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_SSS_ON));
	CHECK(emssson);
	mlay->sssON = emssson->selected;

	EMEditSpin * emsssdensity = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_SSS_DENSITY));
	CHECK(emsssdensity);
	mlay->sssDens = emsssdensity->floatValue;

	EMEditSpin * emssscolldir = GetEMEditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT_SSS_COLLISION_DIR));
	CHECK(emssscolldir);
	mlay->sssCollDir = emssscolldir->floatValue;

	mat->updateFlags();
	mat->updateWeights();

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::preWindowOpen()
{
	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::postWindowOpen()
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * edMat = rtr->editedMaterial;

	if (rtr->matScenesIndices.objCount < 1)
	{
		MessageBox(hMain, "No test scene for material editor found. May crash.", "", 0);
		return false;
	}

	int scIndex = 0;		// number of scene on combobox
	if (edMat)
	{
		int emInd = edMat->prSceneIndex;
		scIndex = max(0, min(rtr->matScenesIndices.objCount-1, emInd));
	}
	changeScene(scIndex);

	// fill scenes combo
	EMComboBox * emscenes = GetEMComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT_CHOOSE_SCENE));
	emscenes->deleteItems();
	int scNum = rtr->materialEditorSceneFilenames.objCount;

	for (int i=0; i<scNum; i++)
	{
		char * scname = NULL;
		if (i>=rtr->materialEditorSceneNames.objCount)
			scname = rtr->materialEditorSceneFilenames[i];
		else	
			scname = rtr->materialEditorSceneNames[i];
		if (scname)
			emscenes->addItem(scname);
		else
			emscenes->addItem("no name");
	}

	emscenes->selected = scIndex;
	InvalidateRect(emscenes->hwnd, NULL, false);

	materialToGUI();

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::updateMaterialToEditableMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	
	MaterialNox * oldmat = NULL;
	if (rtr->editedMaterial)
		oldmat = rtr->editedMaterial;

	MaterialNox * mat = getEditedMaterial()->copy();
	mat->updateSceneTexturePointers(rtr->curScenePtr);
	CHECK(mat);
	rtr->editedMaterial = mat;
	if (oldmat)
	{
		oldmat->deleteBuffers(true, true, true);
		oldmat->layers.destroyArray();
		oldmat->layers.freeList();
		delete oldmat;
		oldmat = NULL;
	}

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::resetEditedMaterialOnAllScenes()
{
	Raytracer * rtr = Raytracer::getInstance();

	for (int i=0; i<rtr->matScenesIndices.objCount; i++)
	{
		Scene * scPtr = rtr->scenes[rtr->matScenesIndices[i]];
		int m = scPtr->sscene.editableMaterial;
		MaterialNox * mat = scPtr->mats[m];
		if (!mat)
			continue;

		for (int j=0; j<mat->layers.objCount; j++)
			mat->layers[j].resetValues(true);

		scPtr->texManager.updateTextures(scPtr);
	}
	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::resetMaterialDelTextures()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	mat->deleteBuffers(true, true, true);
	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::changeScene(int id)
{
	Raytracer * rtr = Raytracer::getInstance();
	if (id < 0 || id>=rtr->matScenesIndices.objCount)
		return false;

	rtr->curSceneInd = rtr->matScenesIndices[id];
	rtr->curScenePtr = rtr->scenes[rtr->curSceneInd];

	if (rtr->editedMaterial)
	{
		int m = rtr->curScenePtr->sscene.editableMaterial;
		MaterialNox * oldmat = rtr->curScenePtr->mats[m];
		MaterialNox * newmat = rtr->editedMaterial->copy();
		rtr->curScenePtr->mats.setElement(m, newmat);
		rtr->curScenePtr->mats.createArray();
		newmat->id = oldmat->id;
		newmat->layers.createArray();

		
		newmat->tex_opacity.managerID = rtr->curScenePtr->texManager.addTexture(newmat->tex_opacity.fullfilename, false, newmat->tex_opacity.texScPtr, false);
		newmat->tex_displacement.managerID = rtr->curScenePtr->texManager.addTexture(newmat->tex_displacement.fullfilename, false, newmat->tex_displacement.texScPtr, false);
		for (int i=0; i<newmat->layers.objCount; i++)
		{
			MatLayer * mlay = &(newmat->layers[i]);
			mlay->tex_weight.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_weight.fullfilename, false, mlay->tex_weight.texScPtr, false);
			mlay->tex_col0.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_col0.fullfilename,   false, mlay->tex_col0.texScPtr, false);
			mlay->tex_col90.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_col90.fullfilename,  false, mlay->tex_col90.texScPtr, false);
			mlay->tex_light.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_light.fullfilename,  false, mlay->tex_light.texScPtr, false);
			mlay->tex_normal.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_normal.fullfilename, false, mlay->tex_normal.texScPtr, true);
			mlay->tex_transm.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_transm.fullfilename, false, mlay->tex_transm.texScPtr, true);
			mlay->tex_rough.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_rough.fullfilename,  false, mlay->tex_rough.texScPtr, false);
			mlay->tex_aniso.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso.fullfilename, false, mlay->tex_aniso.texScPtr, true);
			mlay->tex_aniso_angle.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso_angle.fullfilename, false, mlay->tex_aniso_angle.texScPtr, true);
		}
		rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);
		newmat->updateSceneTexturePointers(rtr->curScenePtr);

		if (oldmat)
		{
			oldmat->deleteBuffers(true, true, true, true);
			oldmat->layers.destroyArray();
			oldmat->layers.freeList();
			delete oldmat;
			oldmat = NULL;
		}

	}

	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (cam && empv)
		empv->imgMod->copyDataFromAnother(&(cam->iMod));
	
	empv->rgnPtr = &(cam->staticRegion);
	cam->staticRegion.createRegions(cam->width, cam->height);
	cam->staticRegion.overallHits = 0;

	if (!updateLightMeshes())
		MessageBox(hMain, "Watch out - there is no emitter on scene.", "Warning",0);

	return true;
}

//------------------------------------------------------------------------------------------------------

MatLayer * MatEditorWindow::getEditedMatLayer()
{
	MaterialNox * mat = getEditedMaterial();
	if (!mat)
		return false;
	HWND hCurrent = GetDlgItem (hMain, IDC_MATEDIT_LAYERS_LIST);
	EMListView * emlv = GetEMListViewInstance(hCurrent);
	int n = emlv->selected;
	if (n >= mat->layers.objCount  ||  n < 0)
		return NULL;
	return &(mat->layers[n]);
}

//------------------------------------------------------------------------------------------------------

MaterialNox * MatEditorWindow::getEditedMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	int edm = sc->sscene.editableMaterial;
	MaterialNox * mat = sc->mats[edm];
	return mat;
}

//------------------------------------------------------------------------------------------------------

int MatEditorWindow::getEditedLayerNumber()
{
	HWND hCurrent = GetDlgItem (hMain, IDC_MATEDIT_LAYERS_LIST);
	EMListView * emlv = GetEMListViewInstance(hCurrent);
	return emlv->selected;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::fillLayersList()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	HWND hCurrent = GetDlgItem (hMain, IDC_MATEDIT_LAYERS_LIST);
	EMListView * emlv = GetEMListViewInstance(hCurrent);
	emlv->data.freeList();

	emlv->align.freeList();
	emlv->align.add(EMListView::ALIGN_LEFT);
	emlv->align.add(EMListView::ALIGN_RIGHT);
	emlv->align.add(EMListView::ALIGN_RIGHT);
	emlv->align.add(EMListView::ALIGN_CENTER);
	emlv->align.add(EMListView::ALIGN_CENTER);
	emlv->align.add(EMListView::ALIGN_CENTER);
	emlv->align.createArray();

	char buf[64];
	for (int i=0; i<mat->layers.objCount; i++)
	{
		MatLayer * mlay = &(mat->layers[i]);

		emlv->addEmptyRow();

		// name
		sprintf_s(buf, 64, "Layer %d", (i+1));
		emlv->setData(0, i, buf);

		// weight
		if (mlay->tex_weight.isValid()  &&   mlay->use_tex_weight)
			sprintf_s(buf, 64, "tex");
		else
			sprintf_s(buf, 64, "%d", mlay->contribution);
		emlv->setData(1, i, buf);

		// rough
		if (mlay->tex_rough.isValid()  &&  mlay->use_tex_rough)
			sprintf_s(buf, 64, "tex");
		else
			sprintf_s(buf, 64, "%d", (int)(mlay->roughness*100+0.01f));
		emlv->setData(2, i, buf);

		// emission
		if (mlay->type == MatLayer::TYPE_EMITTER)
			emlv->setData(3, i, "x");
		else
			emlv->setData(3, i, "-");

		// transmission
		if (mlay->transmOn)
			emlv->setData(4, i, "x");
		else
			emlv->setData(4, i, "-");

		// sss
		if (mlay->sssON)
			emlv->setData(5, i, "x");
		else
			emlv->setData(5, i, "-");
	}

	InvalidateRect(hCurrent, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::layerAdd()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (mat->layers.objCount < 16)
	{
		MatLayer newmat;
		mat->layers.add(newmat, &(MatLayer::runConstructor));
		mat->layers.createArray();
	}
	else
	{
		MessageBox(hMain, "Sorry, no more than 16 layers.", "Sorry...", 0);
		return false;
	}

	fillLayersList();
	EMListView * emlv = GetEMListViewInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYERS_LIST));
	CHECK(emlv);
	emlv->selected = mat->layers.objCount-1;
	emlv->gotoEnd();
	InvalidateRect(emlv->hwnd, NULL, false);
	layerToGUI(emlv->selected);
	
	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::layerDel()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (mat->layers.objCount <= 1)
	{
		MessageBox(hMain, "Sorry, not less than 1 layer.", "Sorry...", 0);
		return false;
	}

	EMListView * emlv = GetEMListViewInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYERS_LIST));
	CHECK(emlv);
	int toDel = emlv->selected;

	for (int i=toDel; i<mat->layers.objCount-1; i++)
	{
		mat->layers[i] = mat->layers[i+1];
	}

	mat->layers.objCount--;
	mat->layers.available++;
	mat->layers.createArray();

	if (emlv->selected >= mat->layers.objCount)
		emlv->selected = mat->layers.objCount-1;
	fillLayersList();
	layerToGUI(emlv->selected);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::layerClone()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (mat->layers.objCount < 16)
	{
		MatLayer newmat;
		mat->layers.add(newmat, &(MatLayer::runConstructor));
		mat->layers.createArray();
	}
	else
	{
		MessageBox(hMain, "Sorry, no more than 16 layers.", "Sorry...", 0);
		return false;
	}

	EMListView * emlv = GetEMListViewInstance(GetDlgItem(hMain, IDC_MATEDIT_LAYERS_LIST));
	CHECK(emlv);
	int toClone = emlv->selected;

	for (int i=mat->layers.objCount-1; i>toClone; i--)
	{
		mat->layers[i] = mat->layers[i-1];
	}

	// clone textures
	mat->layers[toClone+1].tex_col0 = mat->layers[toClone].tex_col0;
	mat->layers[toClone+1].tex_col90 = mat->layers[toClone].tex_col90;
	mat->layers[toClone+1].tex_rough = mat->layers[toClone].tex_rough;
	mat->layers[toClone+1].tex_weight = mat->layers[toClone].tex_weight;
	mat->layers[toClone+1].tex_light = mat->layers[toClone].tex_light;
	mat->layers[toClone+1].tex_normal = mat->layers[toClone].tex_normal;
	mat->layers[toClone+1].tex_transm = mat->layers[toClone].tex_transm;
	mat->layers[toClone+1].tex_aniso = mat->layers[toClone].tex_aniso;
	mat->layers[toClone+1].tex_aniso_angle = mat->layers[toClone].tex_aniso_angle;

	fillLayersList();
	layerToGUI(emlv->selected);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::getColorUnderPointer(COLORREF & col)
{
	POINT pt = {0,0};
	HWND hwndMouse = WindowFromPoint(pt);
	HDC hdcMouse = GetWindowDC(hwndMouse);
	col = GetPixel(hdcMouse, pt.x, pt.y);
	DeleteDC(hdcMouse);

	return false;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::startRendering()
{
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	CHECK(empv);
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->blendBits = rtr->curScenePtr->evalBlendBits();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	CHECK(cam);
	cam->width = 192;
	cam->height = 192;
	bool ok = cam->initializeCamera(true);
	if (!ok)
		return false;
	empv->otherSource = cam->imgBuff;

	cam->staticRegion.createRegions(cam->width, cam->height);
	cam->staticRegion.overallHits = 0;
	empv->rgnPtr = &(cam->staticRegion);

	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	int prn = rtr->curScenePtr->thrM->getProcessorsNumber();
	if (prn < 1)
		prn = 1;
	#ifdef DEBUG_ONE_THREAD
		prn = 1;
	#endif

	autoCategory = MatCategory::CAT_USER;
	autoID = 0;

	lockBeforeStart();

	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		MaterialNox * m = rtr->curScenePtr->mats[i];
		if (m)
			m->prepareToRender();
	}

	cam->nowRendering = true;

	rtr->curScenePtr->thrM->initThreads(prn, true);
	rtr->curScenePtr->thrM->runThreads();
	rtr->curScenePtr->nowRendering = true;
	runRefreshThread(0.25f);
	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::stopRendering()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->resumeThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	unlockAfterStop();
	rtr->curScenePtr->nowRendering = false;
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	cam->nowRendering = false;
	stopRefreshThread();
	return true;
}

//------------------------------------------------------------------------------------------------------

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

bool MatEditorWindow::refresh()
{
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	updateRenderedImage(empv, true, false, NULL, false);
	InvalidateRect(empv->hwnd, NULL, false);
	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::copyBufferWindowToMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	CHECK(empv);
	CHECK(empv->byteBuff);

	if (!mat->preview)
		mat->preview = new ImageByteBuffer();

	bool ok = mat->preview->copyImageFrom(empv->byteBuff);
	if (!ok)
		return false;

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::copyBufferMaterialToWindow()
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	CHECK(mat->preview);
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	CHECK(empv);

	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();

	bool ok = empv->byteBuff->copyImageFrom(mat->preview);
	if (!ok)
		return false;

	InvalidateRect(empv->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::runRefreshThread(float s)
{
	refreshThreadRunning = true;
	static float rt = s;
	DWORD threadID;
	hRefreshThread = CreateThread( 
			   NULL,
			   0,
			   (LPTHREAD_START_ROUTINE)(RefreshThreadProc),
			   (LPVOID)&rt,
			   0,
			   &threadID);
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::stopRefreshThread()
{
	refreshThreadRunning = false;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::acceptMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	MaterialNox * edMat = getEditedMaterial();
	CHECK(edMat);

	bool ok = copyBufferWindowToMaterial();
	
	if (rtr->editedMaterial)
	{
		rtr->editedMaterial->deleteBuffers(true, true, true);
		rtr->editedMaterial->deleteLayers();
		delete rtr->editedMaterial;
		rtr->editedMaterial = NULL;
	}

	rtr->editedMaterial = edMat->copy();
	
	// fill buffer black for next time
	if (empv  &&  empv->byteBuff)
		empv->byteBuff->clearBuffer();

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::loadMaterial(char * filename)
{
	int error;
	XMLScene xscene;
	MaterialNox * mat = xscene.loadMaterial(filename, error);//, img);

	if (!mat)
	{
		if (error>0)
		{
			char buf[128];
			sprintf_s(buf, 128, "Not loaded\nWatch line %d", error);
			MessageBox(hMain, buf, "", 0);
		}
		else
		MessageBox(hMain, "Not loaded", "", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	if (!mat->preview)
	{
		mat->preview = new ImageByteBuffer();
		mat->preview->allocBuffer(RES_PR_X, RES_PR_Y);
	}

	int n = rtr->curScenePtr->sscene.editableMaterial;
	if (n < 0   ||   n >= rtr->curScenePtr->mats.objCount)
	{
		MessageBox(0, "No material marked for edit.\nScene probably broken.", "", 0);
		delete mat->preview;
		mat->deleteBuffers(true, true, true);
		delete mat;
		return false;
	}

	MaterialNox * oldmat = rtr->curScenePtr->mats[n];
	mat->id = oldmat->id;
	mat->hasEmitters  = false;

	rtr->curScenePtr->mats[n] = mat;

	myNode<MaterialNox *> * mm = rtr->curScenePtr->mats.first;
	for (int i=0; i<n; i++)
	{
		mm = mm->next;
	}

	mm->obj = mat;
	rtr->curScenePtr->mats.createArray();

	materialToGUI();

	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT_PREVIEW));
	if (empv->byteBuff == oldmat->preview)
	{
		empv->byteBuff = NULL;
	}
	oldmat->deleteBuffers(true, true, true);
	delete oldmat;

	if (!updateLightMeshes())
		MessageBox(hMain, "Watch out - there is no emitter on scene.", "Warning",0);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::loadMaterial()
{
	char * filename;
	filename = openFileDialog(hMain, "NXM material (*.nxm)\0*.nxm\0", "Load material", "nxm", 1);
	if (!filename)
		return false;

	autoCategory = MatCategory::CAT_USER;
	autoID = 0;

	return loadMaterial(filename);
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::saveMaterial()
{
	char * filename;
	filename = saveFileDialog(hMain, "NXM material (*.nxm)\0*.nxm\0", "Save material", "nxm", 1);
	if (!filename)
		return false;

	copyBufferWindowToMaterial();

	XMLScene xscene;
	Raytracer * rtr = Raytracer::getInstance();

	bool saved = xscene.saveMaterial(filename, rtr->curScenePtr->mats[0]);
	
	if (saved)
		MessageBox(hMain, "Saved", "", 0);
	else
		MessageBox(hMain, "Not saved", "", 0);

	return saved;
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::updateLightMeshes()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->createLightMeshes();

	return true;
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::copyBlendNamesFrom(BlendSettings * blend)
{
	blendNames.copyFrom(blend);
}

//------------------------------------------------------------------------------------------------------

bool MatEditorWindow::updateDisplacement()
{
	HWND hScene = GetDlgItem(hMain, IDC_MATEDIT_CHOOSE_SCENE);
	EMComboBox * emscene = GetEMComboBoxInstance(hScene);
	CHECK(emscene);
	if (emscene->selected!=4)
	{
		MaterialNox * mat = getEditedMaterial();
		if (mat->displ_depth!=0  &&  mat->tex_displacement.isValid()  &&  mat->use_tex_displacement)
		{
			MessageBox(hMain, "Displacement materials can have preview only on displacement scene.", "Error", 0);
			return false;
		}
		return true;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	sc->removeDisplacementMeshes();
	sc->createDisplacementMeshes();

	sc->sscene.bvh.deleteAllStuff();
	for (int i=0; i<sc->meshes.objCount; i++)
		sc->sscene.bvh.addMesh(&sc->meshes[i]);
	bool bvhok = sc->sscene.bvh.createTree();

	sc->updateSkyportals();
	sc->createLightMeshes();

	return true;
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::lockBeforeTextures()
{
	disableControl(hMain, IDC_MATEDIT_OK);
	disableControl(hMain, IDC_MATEDIT_CANCEL);
	disableControl(hMain, IDC_MATEDIT_START);
	disableControl(hMain, IDC_MATEDIT_STOP);
	disableControl(hMain, IDC_MATEDIT_CHOOSE_SCENE);
	disableControl(hMain, IDC_MATEDIT_ZOOM);
	disableControl(hMain, IDC_MATEDIT_LOADMAT);
	disableControl(hMain, IDC_MATEDIT_SAVEMAT);
	disableControl(hMain, IDC_MATEDIT_ARCHIVE_MAT);
	disableControl(hMain, IDC_MATEDIT_NAME);
	disableControl(hMain, IDC_MATEDIT_LAYERS_LIST);
	disableControl(hMain, IDC_MATEDIT_LAYER_ADD);
	disableControl(hMain, IDC_MATEDIT_LAYER_DEL);
	disableControl(hMain, IDC_MATEDIT_LAYER_RESET);
	disableControl(hMain, IDC_MATEDIT_LAYER_CLONE);
	disableControl(hMain, IDC_MATEDIT_EMISSION_ON);
	disableControl(hMain, IDC_MATEDIT_EMISSION_SKYPORTAL);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_EMISSION_POWER);
	disableControl(hMain, IDC_MATEDIT_EMISSION_UNIT);
	disableControl(hMain, IDC_MATEDIT_IES_LOAD);
	disableControl(hMain, IDC_MATEDIT_IES_UNLOAD);
	disableControl(hMain, IDC_MATEDIT_IES_PREVIEW);
	disableControl(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES);
	disableControl(hMain, IDC_MATEDIT_WEIGHT);
	disableControl(hMain, IDC_MATEDIT_WEIGHT_TEX);
	disableControl(hMain, IDC_MATEDIT_WEIGHT_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_IOR);
	disableControl(hMain, IDC_MATEDIT_FRESNEL);
	disableControl(hMain, IDC_MATEDIT_FRESNEL_ADVANCED);
	disableControl(hMain, IDC_MATEDIT_ROUGHNESS);
	disableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX);
	disableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_REFL90_LINKED);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_ANISOTROPY);
	disableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX);
	disableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_ANISO_ANGLE);
	disableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX);
	disableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_ON);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_ON);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE);
	disableControl(hMain, IDC_MATEDIT_DISPERSION_ON);
	disableControl(hMain, IDC_MATEDIT_FAKE_GLASS_ON);
	disableControl(hMain, IDC_MATEDIT_OPACITY);
	disableControl(hMain, IDC_MATEDIT_OPACITY_TEX);
	disableControl(hMain, IDC_MATEDIT_OPACITY_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_DISPL_ON);
	disableControl(hMain, IDC_MATEDIT_DISPL_DEPTH);
	disableControl(hMain, IDC_MATEDIT_DISPL_TEX);
	disableControl(hMain, IDC_MATEDIT_NORMAL_ON);
	disableControl(hMain, IDC_MATEDIT_NORMAL_TEX);
	disableControl(hMain, IDC_MATEDIT_SSS_ON);
	disableControl(hMain, IDC_MATEDIT_LIB1);
	disableControl(hMain, IDC_MATEDIT_LIB2);
	disableControl(hMain, IDC_MATEDIT_LIB3);
	disableControl(hMain, IDC_MATEDIT_LIB4);
	disableControl(hMain, IDC_MATEDIT_LIB5);
	disableControl(hMain, IDC_MATEDIT_LIB6);
	disableControl(hMain, IDC_MATEDIT_LIB7);
	disableControl(hMain, IDC_MATEDIT_SHOWHIDE_LIB);
	disableControl(hMain, IDC_MATEDIT_LIB_LEFT);
	disableControl(hMain, IDC_MATEDIT_LIB_RIGHT);
	disableControl(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY);
	disableControl(hMain, IDC_MATEDIT_ONLINE_SWITCH);
	disableControl(hMain, IDC_MATEDIT_LOAD_LOCALLIB);
	disableControl(hMain, IDC_MATEDIT_DOWNLOADMAT);
	disableControl(hMain, IDC_MATEDIT_UPLOADMAT);
	disableControl(hMain, IDC_MATEDIT_ADD_LOCALLIB);
	disableControl(hMain, IDC_MATEDIT_DEL_LOCALLIB);
	disableControl(hMain, IDC_MATEDIT_USER_ONLINE);
	
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_EMITTER);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_BRICKS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_CARPAINT);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_CERAMIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_CONCRETE);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_FABRIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_FOOD);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_GLASS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_GROUND);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_LEATHER);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_LIQUIDS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_METAL);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_MINERALS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_ORGANIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_PLASTIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_PORCELAIN);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_SSS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_STONE);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_TILES);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_WALLS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_WOOD);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_OTHER);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_USER);
	disableControl(hMain, IDC_MATEDIT_SSS_ON);
	disableControl(hMain, IDC_MATEDIT_SSS_DENSITY);
	disableControl(hMain, IDC_MATEDIT_SSS_COLLISION_DIR);
	
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::unlockAfterTextures()
{
	enableControl(hMain, IDC_MATEDIT_OK);
	enableControl(hMain, IDC_MATEDIT_CANCEL);
	enableControl(hMain, IDC_MATEDIT_START);
	enableControl(hMain, IDC_MATEDIT_STOP);
	enableControl(hMain, IDC_MATEDIT_CHOOSE_SCENE);
	enableControl(hMain, IDC_MATEDIT_ZOOM);
	enableControl(hMain, IDC_MATEDIT_LOADMAT);
	enableControl(hMain, IDC_MATEDIT_SAVEMAT);
	enableControl(hMain, IDC_MATEDIT_ARCHIVE_MAT);
	enableControl(hMain, IDC_MATEDIT_NAME);
	enableControl(hMain, IDC_MATEDIT_LAYERS_LIST);
	enableControl(hMain, IDC_MATEDIT_LAYER_ADD);
	enableControl(hMain, IDC_MATEDIT_LAYER_DEL);
	enableControl(hMain, IDC_MATEDIT_LAYER_RESET);
	enableControl(hMain, IDC_MATEDIT_LAYER_CLONE);
	enableControl(hMain, IDC_MATEDIT_EMISSION_ON);
	enableControl(hMain, IDC_MATEDIT_EMISSION_SKYPORTAL);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_EMISSION_POWER);
	enableControl(hMain, IDC_MATEDIT_EMISSION_UNIT);
	enableControl(hMain, IDC_MATEDIT_IES_LOAD);
	enableControl(hMain, IDC_MATEDIT_IES_UNLOAD);
	enableControl(hMain, IDC_MATEDIT_IES_PREVIEW);
	enableControl(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES);
	enableControl(hMain, IDC_MATEDIT_WEIGHT);
	enableControl(hMain, IDC_MATEDIT_WEIGHT_TEX);
	enableControl(hMain, IDC_MATEDIT_WEIGHT_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_IOR);
	enableControl(hMain, IDC_MATEDIT_FRESNEL);
	disableControl(hMain, IDC_MATEDIT_FRESNEL_ADVANCED);
	enableControl(hMain, IDC_MATEDIT_ROUGHNESS);
	enableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX);
	enableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_REFL90_LINKED);
	updateLinked90Controls(hMain);
	enableControl(hMain, IDC_MATEDIT_ANISOTROPY);
	enableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX);
	enableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_ANISO_ANGLE);
	enableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX);
	enableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_ON);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_ON);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE);
	enableControl(hMain, IDC_MATEDIT_DISPERSION_ON);
	enableControl(hMain, IDC_MATEDIT_FAKE_GLASS_ON);
	enableControl(hMain, IDC_MATEDIT_OPACITY);
	enableControl(hMain, IDC_MATEDIT_OPACITY_TEX);
	enableControl(hMain, IDC_MATEDIT_OPACITY_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_DISPL_ON);
	enableControl(hMain, IDC_MATEDIT_DISPL_DEPTH);
	enableControl(hMain, IDC_MATEDIT_DISPL_TEX);
	enableControl(hMain, IDC_MATEDIT_NORMAL_ON);
	enableControl(hMain, IDC_MATEDIT_NORMAL_TEX);
	enableControl(hMain, IDC_MATEDIT_SSS_ON);
	enableControl(hMain, IDC_MATEDIT_LIB1);
	enableControl(hMain, IDC_MATEDIT_LIB2);
	enableControl(hMain, IDC_MATEDIT_LIB3);
	enableControl(hMain, IDC_MATEDIT_LIB4);
	enableControl(hMain, IDC_MATEDIT_LIB5);
	enableControl(hMain, IDC_MATEDIT_LIB6);
	enableControl(hMain, IDC_MATEDIT_LIB7);
	enableControl(hMain, IDC_MATEDIT_SHOWHIDE_LIB);
	enableControl(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY);
	enableControl(hMain, IDC_MATEDIT_LIB_LEFT);
	enableControl(hMain, IDC_MATEDIT_LIB_RIGHT);
	enableControl(hMain, IDC_MATEDIT_ONLINE_SWITCH);
	enableControl(hMain, IDC_MATEDIT_LOAD_LOCALLIB);
	enableControl(hMain, IDC_MATEDIT_DOWNLOADMAT);
	enableControl(hMain, IDC_MATEDIT_UPLOADMAT);
	enableControl(hMain, IDC_MATEDIT_ADD_LOCALLIB);
	enableControl(hMain, IDC_MATEDIT_DEL_LOCALLIB);
	enableControl(hMain, IDC_MATEDIT_USER_ONLINE);

	enableControl(hMain, IDC_MATEDIT_LIB_CAT_EMITTER);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_BRICKS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_CARPAINT);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_CERAMIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_CONCRETE);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_FABRIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_FOOD);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_GLASS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_GROUND);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_LEATHER);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_LIQUIDS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_METAL);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_MINERALS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_ORGANIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_PLASTIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_PORCELAIN);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_SSS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_STONE);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_TILES);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_WALLS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_WOOD);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_OTHER);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_USER);
	enableControl(hMain, IDC_MATEDIT_SSS_ON);
	enableControl(hMain, IDC_MATEDIT_SSS_DENSITY);
	enableControl(hMain, IDC_MATEDIT_SSS_COLLISION_DIR);
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::lockBeforeStart()
{
	disableControl(hMain, IDC_MATEDIT_OK);
	disableControl(hMain, IDC_MATEDIT_CANCEL);
	disableControl(hMain, IDC_MATEDIT_START);
	disableControl(hMain, IDC_MATEDIT_CHOOSE_SCENE);
	disableControl(hMain, IDC_MATEDIT_ZOOM	);
	disableControl(hMain, IDC_MATEDIT_LOADMAT);
	disableControl(hMain, IDC_MATEDIT_SAVEMAT);
	disableControl(hMain, IDC_MATEDIT_ARCHIVE_MAT);
	disableControl(hMain, IDC_MATEDIT_NAME);
	disableControl(hMain, IDC_MATEDIT_LAYERS_LIST);
	disableControl(hMain, IDC_MATEDIT_LAYER_ADD);
	disableControl(hMain, IDC_MATEDIT_LAYER_DEL);
	disableControl(hMain, IDC_MATEDIT_LAYER_RESET);
	disableControl(hMain, IDC_MATEDIT_LAYER_CLONE);
	disableControl(hMain, IDC_MATEDIT_EMISSION_ON);
	disableControl(hMain, IDC_MATEDIT_EMISSION_SKYPORTAL);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_EMISSION_POWER);
	disableControl(hMain, IDC_MATEDIT_EMISSION_UNIT);
	disableControl(hMain, IDC_MATEDIT_IES_LOAD);
	disableControl(hMain, IDC_MATEDIT_IES_UNLOAD);
	disableControl(hMain, IDC_MATEDIT_IES_PREVIEW);
	disableControl(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES);
	disableControl(hMain, IDC_MATEDIT_WEIGHT);
	disableControl(hMain, IDC_MATEDIT_WEIGHT_TEX);
	disableControl(hMain, IDC_MATEDIT_WEIGHT_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_IOR);
	disableControl(hMain, IDC_MATEDIT_FRESNEL);
	disableControl(hMain, IDC_MATEDIT_FRESNEL_ADVANCED);
	disableControl(hMain, IDC_MATEDIT_ROUGHNESS);
	disableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX);
	disableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_REFL90_LINKED);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_REFL90_COLOR_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_ANISOTROPY);
	disableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX);
	disableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_ANISO_ANGLE);
	disableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX);
	disableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_ON);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX);
	disableControl(hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_ON);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB);
	disableControl(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE);
	disableControl(hMain, IDC_MATEDIT_DISPERSION_ON);
	disableControl(hMain, IDC_MATEDIT_FAKE_GLASS_ON);
	disableControl(hMain, IDC_MATEDIT_OPACITY);
	disableControl(hMain, IDC_MATEDIT_OPACITY_TEX);
	disableControl(hMain, IDC_MATEDIT_OPACITY_TEX_ON);
	disableControl(hMain, IDC_MATEDIT_DISPL_ON);
	disableControl(hMain, IDC_MATEDIT_DISPL_DEPTH);
	disableControl(hMain, IDC_MATEDIT_DISPL_TEX);
	disableControl(hMain, IDC_MATEDIT_NORMAL_ON);
	disableControl(hMain, IDC_MATEDIT_NORMAL_TEX);
	disableControl(hMain, IDC_MATEDIT_SSS_ON);
	disableControl(hMain, IDC_MATEDIT_LIB1);
	disableControl(hMain, IDC_MATEDIT_LIB2);
	disableControl(hMain, IDC_MATEDIT_LIB3);
	disableControl(hMain, IDC_MATEDIT_LIB4);
	disableControl(hMain, IDC_MATEDIT_LIB5);
	disableControl(hMain, IDC_MATEDIT_LIB6);
	disableControl(hMain, IDC_MATEDIT_LIB7);
	disableControl(hMain, IDC_MATEDIT_SHOWHIDE_LIB);
	disableControl(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY);
	disableControl(hMain, IDC_MATEDIT_LIB_LEFT);
	disableControl(hMain, IDC_MATEDIT_LIB_RIGHT);
	disableControl(hMain, IDC_MATEDIT_ONLINE_SWITCH);
	disableControl(hMain, IDC_MATEDIT_LOAD_LOCALLIB);
	disableControl(hMain, IDC_MATEDIT_DOWNLOADMAT);
	disableControl(hMain, IDC_MATEDIT_UPLOADMAT);
	disableControl(hMain, IDC_MATEDIT_ADD_LOCALLIB);
	disableControl(hMain, IDC_MATEDIT_DEL_LOCALLIB);
	disableControl(hMain, IDC_MATEDIT_USER_ONLINE);

	disableControl(hMain, IDC_MATEDIT_LIB_CAT_EMITTER);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_BRICKS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_CARPAINT);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_CERAMIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_CONCRETE);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_FABRIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_FOOD);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_GLASS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_GROUND);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_LEATHER);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_LIQUIDS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_METAL);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_MINERALS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_ORGANIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_PLASTIC);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_PORCELAIN);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_SSS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_STONE);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_TILES);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_WALLS);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_WOOD);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_OTHER);
	disableControl(hMain, IDC_MATEDIT_LIB_CAT_USER);
	disableControl(hMain, IDC_MATEDIT_SSS_ON);
	disableControl(hMain, IDC_MATEDIT_SSS_DENSITY);
	disableControl(hMain, IDC_MATEDIT_SSS_COLLISION_DIR);
}

//------------------------------------------------------------------------------------------------------

void MatEditorWindow::unlockAfterStop()
{
	enableControl(hMain, IDC_MATEDIT_OK);
	enableControl(hMain, IDC_MATEDIT_CANCEL);
	enableControl(hMain, IDC_MATEDIT_START);
	enableControl(hMain, IDC_MATEDIT_CHOOSE_SCENE);
	enableControl(hMain, IDC_MATEDIT_ZOOM);
	enableControl(hMain, IDC_MATEDIT_LOADMAT);
	enableControl(hMain, IDC_MATEDIT_SAVEMAT);
	enableControl(hMain, IDC_MATEDIT_ARCHIVE_MAT);
	enableControl(hMain, IDC_MATEDIT_NAME);
	enableControl(hMain, IDC_MATEDIT_LAYERS_LIST);
	enableControl(hMain, IDC_MATEDIT_LAYER_ADD);
	enableControl(hMain, IDC_MATEDIT_LAYER_DEL);
	enableControl(hMain, IDC_MATEDIT_LAYER_RESET);
	enableControl(hMain, IDC_MATEDIT_LAYER_CLONE);
	enableControl(hMain, IDC_MATEDIT_EMISSION_ON);
	enableControl(hMain, IDC_MATEDIT_EMISSION_SKYPORTAL);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX);
	enableControl(hMain, IDC_MATEDIT_EMISSION_COLOR_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_EMISSION_POWER);
	enableControl(hMain, IDC_MATEDIT_EMISSION_UNIT);
	enableControl(hMain, IDC_MATEDIT_IES_LOAD);
	enableControl(hMain, IDC_MATEDIT_IES_UNLOAD);
	enableControl(hMain, IDC_MATEDIT_IES_PREVIEW);
	enableControl(hMain, IDC_MATEDIT_EMISSION_BLEND_NAMES);
	enableControl(hMain, IDC_MATEDIT_WEIGHT);
	enableControl(hMain, IDC_MATEDIT_WEIGHT_TEX);
	enableControl(hMain, IDC_MATEDIT_WEIGHT_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_IOR);
	enableControl(hMain, IDC_MATEDIT_FRESNEL);
	disableControl(hMain, IDC_MATEDIT_FRESNEL_ADVANCED);
	enableControl(hMain, IDC_MATEDIT_ROUGHNESS);
	enableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX);
	enableControl(hMain, IDC_MATEDIT_ROUGHNESS_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX);
	enableControl(hMain, IDC_MATEDIT_REFL0_COLOR_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_REFL90_LINKED);
	updateLinked90Controls(hMain);
	enableControl(hMain, IDC_MATEDIT_ANISOTROPY);
	enableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX);
	enableControl(hMain, IDC_MATEDIT_ANISOTROPY_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_ANISO_ANGLE);
	enableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX);
	enableControl(hMain, IDC_MATEDIT_ANISO_ANGLE_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_ON);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_COLOR_TEX);
	enableControl(hMain, IDC_MATEDIT_TRANSMISSION_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_ON);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_COLOR_RGB);
	enableControl(hMain, IDC_MATEDIT_ABSORPTION_DISTANCE);
	enableControl(hMain, IDC_MATEDIT_DISPERSION_ON);
	enableControl(hMain, IDC_MATEDIT_FAKE_GLASS_ON);
	enableControl(hMain, IDC_MATEDIT_OPACITY);
	enableControl(hMain, IDC_MATEDIT_OPACITY_TEX);
	enableControl(hMain, IDC_MATEDIT_OPACITY_TEX_ON);
	enableControl(hMain, IDC_MATEDIT_DISPL_ON);
	enableControl(hMain, IDC_MATEDIT_DISPL_DEPTH);
	enableControl(hMain, IDC_MATEDIT_DISPL_TEX);
	enableControl(hMain, IDC_MATEDIT_NORMAL_ON);
	enableControl(hMain, IDC_MATEDIT_NORMAL_TEX);
	disableControl(hMain, IDC_MATEDIT_SSS_ON);
	enableControl(hMain, IDC_MATEDIT_LIB1);
	enableControl(hMain, IDC_MATEDIT_LIB2);
	enableControl(hMain, IDC_MATEDIT_LIB3);
	enableControl(hMain, IDC_MATEDIT_LIB4);
	enableControl(hMain, IDC_MATEDIT_LIB5);
	enableControl(hMain, IDC_MATEDIT_LIB6);
	enableControl(hMain, IDC_MATEDIT_LIB7);
	enableControl(hMain, IDC_MATEDIT_SHOWHIDE_LIB);
	enableControl(hMain, IDC_MATEDIT_LIB_SEARCH_QUERY);
	enableControl(hMain, IDC_MATEDIT_LIB_LEFT);
	enableControl(hMain, IDC_MATEDIT_LIB_RIGHT);
	enableControl(hMain, IDC_MATEDIT_ONLINE_SWITCH);
	enableControl(hMain, IDC_MATEDIT_LOAD_LOCALLIB);
	enableControl(hMain, IDC_MATEDIT_DOWNLOADMAT);
	enableControl(hMain, IDC_MATEDIT_UPLOADMAT);
	enableControl(hMain, IDC_MATEDIT_ADD_LOCALLIB);
	enableControl(hMain, IDC_MATEDIT_DEL_LOCALLIB);
	enableControl(hMain, IDC_MATEDIT_USER_ONLINE);

	enableControl(hMain, IDC_MATEDIT_LIB_CAT_EMITTER);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_BRICKS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_CARPAINT);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_CERAMIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_CONCRETE);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_FABRIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_FOOD);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_GLASS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_GROUND);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_LEATHER);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_LIQUIDS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_METAL);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_MINERALS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_ORGANIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_PLASTIC);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_PORCELAIN);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_SSS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_STONE);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_TILES);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_WALLS);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_WOOD);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_OTHER);
	enableControl(hMain, IDC_MATEDIT_LIB_CAT_USER);
	enableControl(hMain, IDC_MATEDIT_SSS_ON);
	enableControl(hMain, IDC_MATEDIT_SSS_DENSITY);
	enableControl(hMain, IDC_MATEDIT_SSS_COLLISION_DIR);
}

//------------------------------------------------------------------------------------------------------

