//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NOXplugin.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_NOX_REND_CATEGORY           2
#define IDS_NOX_REND_CLASS_NAME         3
#define IDS_NOX_CAM_PRIMITIVES          4
#define IDS_NOX_MAT_CATEGORY            5
#define IDS_NOX_MAT_CLASS_NAME          6
#define IDS_NOX_TARGET_CLASS_NAME       7
#define IDS_NOX_TARGET_OBJECT_NAME      8
#define IDS_NOX_CAM_TARGET_DEFNAME      9
#define IDS_NOX_CAM_FREE_CLASS_NAME     10
#define IDS_NOX_CAM_CATEGORY            11
#define IDS_NOX_CAM_TARGET_CLASS_NAME   12
#define IDS_PARAMS_SOME_NEEDED_JUNK_CAM 13
#define IDS_NOX_CAM_PARAMS_TITLE        14
#define IDS_NOX_CAM_DOT_TARGET          15
#define IDS_NOX_CAM_DS_CREATE           16
#define IDS_NOX_EXPORTER_CLASS_NAME     17
#define IDS_NOX_EXPORTER_CATEGORY       18
#define IDS_NOX_SUN_DLG_TITLE           19
#define IDS_NOX_SUN_TARGET_CLASS_NAME   20
#define IDS_NOX_SUN_TARGET_DEFNAME      21
#define IDS_NOX_SUN_TARGET_OBJECT_NAME  22
#define IDS_NOX_SUN_CLASS_NAME          23
#define IDS_STRING24                    24
#define IDS_NOX_SUN_CATEGORY            24
#define IDS_NOX_SUN_TARGET_CATEGORY     25
#define IDD_NOX_CAM_PANEL               53
#define IDC_NOX_CAM_RUN_CAMERA_DIALOG   56
#define IDD_NOX_SUNSKY                  57
#define IDC_NOX_SUNSKY_RUN_EDITOR       58
#define IDC_NOX_SUNSKY_HIDE_TARGET      59
#define IDS_PARAMS_SOME_NEEDED_JUNK_REND 87
#define IDD_NOX_EXPORT_DIALOG           101
#define IDD_NOX_PREREND_DIALOG          102
#define IDS_NOX_SUN_TARGET_DLG_TITLE    103
#define IDD_NOX_MAT_TEX_UPDATE          104
#define IDB_NOX_LOGO_SMALL              105
#define IDC_NOX_MAT_RUN_EDITOR          345
#define IDC_NOX_CAM_SHOW_CONE           543
#define IDC_NOX_EXP_PROGRESS            1001
#define IDC_NOX_EXP_INFO                1002
#define IDC_BUTTON1                     1003
#define IDC_NOX_REND_CHFILENAME         1005
#define IDC_NOX_REND_FILENAME           1006
#define IDC_NOX_CAM_MAKE_DEFAULT        1007
#define IDC_NOX_REND_POST_FILENAME      1007
#define IDC_NOX_SUNSKY_ALTITUDE         1008
#define IDC_NOX_REND_POST_CHFILENAME    1008
#define IDC_NOX_SUNSKY_AZIMUTH          1009
#define IDC_NOX_REND_FINAL_FILENAME     1009
#define IDC_NOX_REND_FINAL_CHFILENAME   1010
#define IDC_NOX_SUNSKY_TARGET_ALTITUDE  1011
#define IDC_NOX_REND_BLEND_FILENAME     1011
#define IDC_NOX_SUNSKY_TARGET_AZIMUTH   1012
#define IDC_NOX_REND_BLEND_CHFILENAME   1012
#define IDC_NOX_SUNSKY_TARGET_RESET_ROTATION 1013
#define IDC_NOX_SUNSKY_TARGET_SELECT_SUN 1014
#define IDC_NOX_SUNSKY_SELECT_TARGET    1015
#define IDC_NOX_SUNSKY_SUN_X            1016
#define IDC_NOX_SUNSKY_SUN_Y            1017
#define IDC_NOX_SUNSKY_SUN_Z            1018
#define IDC_NOX_SUNSKY_TARG_X           1019
#define IDC_NOX_SUNSKY_TARG_            1020
#define IDC_NOX_SUNSKY_TARG_Y           1020
#define IDC_NOX_SUNSKY_TARG_Z           1021
#define IDC_NOX_SUNSKY_TARG_SX          1022
#define IDC_NOX_SUNSKY_TARG_SY          1023
#define IDC_NOX_SUNSKY_TARG_SZ          1024
#define IDC_NOX_SUNSKY_TARG_TX          1025
#define IDC_NOX_SUNSKY_TARG_TY          1026
#define IDC_NOX_SUNSKY_TARG_TZ          1027
#define IDC_NOX_MAT_PICK_TEX            1028
#define IDC_NOX_MAT_LAYER_TEX           1031
#define IDC_NOX_MAT_TEX_UPDATE_PROGRESSBAR 1032
#define IDC_NOX_MAT_TEX_UPDATE_TEXT     1033
#define IDC_NOX_MAT_BLEND_NAMES         1034
#define IDC_NOX_REND_BLEND_NAMES        1035
#define IDC_NOX_LOGO                    1036
#define IDC_CAMERA_SETFOCUSDIST         1037
#define IDC_NOX_SUNSKY_ACTIVE           1038
#define IDC_NOX_CAM_EL_STUPIDO_1        1039
#define IDC_NOX_SUNSKY_HIDE_ENVMAP      1039
#define IDC_NOX_REND_SCENE_NAME         1040
#define IDC_NOX_REND_AUTHOR             1041
#define IDC_NOX_REND_WWW                1042
#define IDC_EDIT4                       1043
#define IDC_NOX_REND_EMAIL              1043
#define IDC_BUTTON2                     1044
#define IDC_NOX_REND_OVERRIDE_MATERIAL_ENABLED 1046
#define IDC_NOX_REND_OVERRIDE_MATERIAL_BUTTON 1047
#define IDC_NOX_REND_POST_ENABLE        1048
#define IDC_NOX_REND_FINAL_ENABLE       1049
#define IDC_NOX_REND_BLEND_ENABLE       1050
#define IDC_CHECK1                      1051
#define IDC_NOX_REND_MB_ON              1051
#define IDC_NOX_REND_MB_NUM_FRAMES      1052
#define IDC_COMBO1                      1053
#define IDC_NOX_REND_ENGINE             1053
#define IDD_NOX_MAT_DIALOG              5456
#define IDD_NOX_REND_DIALOG             5458
#define IDD_NOX_SUNSKY_TARGET           5459
#define IDC_NOX_CAM_FOCUSTARGET         5646
#define IDC_NOX_CAM_TARGETED            19258

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1054
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
