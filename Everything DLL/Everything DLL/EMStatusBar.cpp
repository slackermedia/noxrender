#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"

extern HMODULE hDllModule;

void InitEMStatusBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMStatusBar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMStatusBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMStatusBar*);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMStatusBar * GetEMStatusBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMStatusBar * emsb = (EMStatusBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EMStatusBar * emsb = (EMStatusBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emsb;
}

void SetEMStatusBarInstance(HWND hwnd, EMStatusBar *emsb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emsb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emsb);
	#endif
}

EMStatusBar::EMStatusBar(HWND hWnd)
{
	hwnd = hWnd;
	colText = RGB(0,0,0); 
	colBackGnd = RGB(96,160,224);
	colBorderDownRight = RGB(160,204,255);
	colBorderUpLeft = RGB(64,128,192);
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	RECT rect;
	GetWindowRect(hwnd, &rect);
	distLeft = 0;
	distRight = 0;
	distBottom = 0;
	width = rect.right-rect.left;
	height = rect.bottom-rect.top;
	anchorLeft = true;
	anchorRight = true;
	anchorBottom = true;

	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
	{
		captionSize = 0;
	}	
}

EMStatusBar::~EMStatusBar()
{
}

LRESULT CALLBACK EMStatusBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMStatusBar* emsb;
	emsb = GetEMStatusBarInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HFONT hOldFont;
	HPEN hOldPen;

	switch (msg)
	{
		case WM_CREATE:
			emsb = new EMStatusBar(hwnd);
			SetEMStatusBarInstance(hwnd, emsb);
			break;
		case WM_DESTROY:
			{
				delete emsb;
				SetEMStatusBarInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);


			hOldFont =(HFONT) SelectObject(hdc, emsb->hFont);
			SetTextColor(hdc, emsb->colText);
			SetBkColor  (hdc, emsb->colBackGnd);

			SIZE sz;
			GetTextExtentPoint32(hdc, emsb->caption, emsb->captionSize, &sz);
			int posx, posy; 
			posy = (rect.bottom-rect.top-sz.cy)/2;
			posx = 5;

			ExtTextOut(hdc, posx, posy, ETO_OPAQUE, &rect, emsb->caption, emsb->captionSize, 0);


			POINT b1[] = { {0, rect.bottom-1}, {0,0}, {rect.right, 0} };
			POINT b2[] = { {rect.right-1, 1}, {rect.right-1, rect.bottom-1}, {0, rect.bottom-1} };
			
			hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emsb->colBorderUpLeft));
			Polyline(hdc, b1, 3);
			DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emsb->colBorderDownRight)));
			Polyline(hdc, b2, 3);
			DeleteObject(SelectObject(hdc, hOldPen));

			DeleteObject(SelectObject(hdc, hOldFont));

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMStatusBar::updateSize()
{
	RECT parent;
	GetClientRect(GetParent(hwnd), &parent);
	
	SetWindowPos(hwnd, HWND_TOP, 0, parent.bottom-parent.top-height,parent.right-parent.left,height,0);
	InvalidateRect(hwnd, NULL, false);
}


bool EMStatusBar::setCaption(char * newCap)
{
	if (!newCap)
		return false;

	int captionSize1;
	char * caption1;

	captionSize1 = (int)strlen(newCap);
	caption1 = (char *) malloc(captionSize1+1);

	if (caption1)
	{
		sprintf_s(caption1, captionSize1+1, "%s", newCap);
		if (caption)
			free(caption);
		caption = caption1;
		captionSize = captionSize1;
	}
	else
	{
		return false;
	}	

	InvalidateRect(hwnd, NULL, false);
	return true;
}
