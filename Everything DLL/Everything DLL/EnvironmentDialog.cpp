#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "ColorSchemes.h"
#include "DLL.h"
#include "valuesMinMax.h"

extern HMODULE hDllModule;
notifySunSkyPluginCallback * sskyCallback = NULL;

void evalSunAngles(HWND hWnd);
void changeDateAndPos(HWND hWnd);
void informPluginChanges(SunSky * sunsky, EnvSphere * env);


#define ROUND(a) (int)floor(a+0.5)


INT_PTR CALLBACK EnvDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static EnvSphere env;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

			HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
			EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
			emen->bmEarth = (HBITMAP)LoadImage(hDllModule, 
				MAKEINTRESOURCE(IDB_ENV_EARTH_IMG), IMAGE_BITMAP, 0, 0, 0);
			SetWindowPos(hEnv, HWND_TOP, 0,0, 362, 182, SWP_NOOWNERZORDER | SWP_NOMOVE);

			HWND hLong, hLat, hMonth, hDay, hHour, hMinute, hGMT, hTurbidity, hAlbedo, hSunOther, hSunRender, hSunSize, hModel;
			hLong      = GetDlgItem(hWnd, IDC_ENV_LONGITUDE);
			hLat       = GetDlgItem(hWnd, IDC_ENV_LATITUDE);
			hMonth     = GetDlgItem(hWnd, IDC_ENV_MONTH);
			hDay       = GetDlgItem(hWnd, IDC_ENV_DAY);
			hHour      = GetDlgItem(hWnd, IDC_ENV_HOUR);
			hMinute    = GetDlgItem(hWnd, IDC_ENV_MINUTE);
			hGMT       = GetDlgItem(hWnd, IDC_ENV_GMT);
			hTurbidity = GetDlgItem(hWnd, IDC_ENV_TURBIDITY);
			hAlbedo   = GetDlgItem(hWnd, IDC_ENV_ALBEDO);
			hSunOther  = GetDlgItem(hWnd, IDC_ENV_SUN_OTHER_LAYER);
			hSunRender = GetDlgItem(hWnd, IDC_ENV_SUN_RENDER_SUN);
			hSunSize   = GetDlgItem(hWnd, IDC_ENV_SUN_SIZE);
			HWND hEnvEnabled, hEnvTexture, hEnvPower, hEnvBLayer, hEnvAzimuth;
			hEnvEnabled = GetDlgItem(hWnd, IDC_ENV_MAP_ENABLED);
			hEnvTexture = GetDlgItem(hWnd, IDC_ENV_MAP_TEXTURE);
			hEnvPower   = GetDlgItem(hWnd, IDC_ENV_MAP_POWER);
			hEnvBLayer  = GetDlgItem(hWnd, IDC_ENV_MAP_BLENDLAYER);
			hEnvAzimuth = GetDlgItem(hWnd, IDC_ENV_MAP_AZIMUTH);
			hModel		= GetDlgItem(hWnd, IDC_ENV_MODEL);


			Raytracer * rtr = Raytracer::getInstance();
			if (lParam)
			{
				EnvSunSky * src = (EnvSunSky*)lParam;
				if (src->env)
					env = *src->env;
				if (src->sunsky)
					emen->sunsky.copyFrom(src->sunsky);
			}

			EMEditSpin * emes;
			emes = GetEMEditSpinInstance(hLong);
			emes->setValuesToInt((int)(-emen->sunsky.getLongitude()*180/PI), -180, 179, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes = GetEMEditSpinInstance(hLat);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes->setValuesToInt((int)(-emen->sunsky.getLatitude()*180/PI), -89, 89, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes = GetEMEditSpinInstance(hMonth);
			emes->setValuesToInt(emen->sunsky.getMonth(), 1, 12, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes = GetEMEditSpinInstance(hDay);
			emes->setValuesToInt(emen->sunsky.getDay(), 1, 31, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes = GetEMEditSpinInstance(hHour);
			emes->setValuesToInt(emen->sunsky.getHour(), 0, 23, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes = GetEMEditSpinInstance(hMinute);
			emes->setValuesToInt((int)emen->sunsky.getMinute(), 0, 59, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes = GetEMEditSpinInstance(hGMT);
			emes->setValuesToInt((int)(-emen->sunsky.getMeridianForTimeZone()*12/PI), -12, 12, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emes);
			emes = GetEMEditSpinInstance(hSunSize);
			emes->setValuesToFloat(emen->sunsky.sunSize, NOX_ENV_SUN_SIZE_MIN,  NOX_ENV_SUN_SIZE_MAX,  0.1f, 0.01f);
			GlobalWindowSettings::colorSchemes.apply(emes);

			EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hTurbidity);
			emetb->colBackground = COLOR_WINDOW_BACKGROUND;
			emetb->setEditBoxWidth(32);
			emetb->setValuesToInt((int)(emen->sunsky.turbidity*10),20,100, 5);
			GlobalWindowSettings::colorSchemes.apply(emetb);

			EMEditTrackBar * emetba = GetEMEditTrackBarInstance(hAlbedo);
			emetba->colBackground = COLOR_WINDOW_BACKGROUND;
			emetba->setEditBoxWidth(32);
			emetba->setValuesToFloat(emen->sunsky.albedo, NOX_ENV_ALBEDO_MIN, NOX_ENV_ALBEDO_MAX, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emetba);

			EMComboBox * emmodel = GetEMComboBoxInstance(hModel);
			emmodel->deleteItems();
			emmodel->addItem("Preetham");
			emmodel->addItem("Hosek-Wilkie");
			emmodel->selected = emen->sunsky.use_hosek ? 1 : 0;
			GlobalWindowSettings::colorSchemes.apply(emmodel);

			changeDateAndPos(hWnd);
			evalSunAngles(hWnd);
			emen->evalShadow();

			EMCheckBox * emcbsol = GetEMCheckBoxInstance(hSunOther);
			emcbsol->selected = emen->sunsky.sunOtherBlendLayer;
			GlobalWindowSettings::colorSchemes.apply(emcbsol);
			InvalidateRect(hSunOther, NULL, false);

			EMCheckBox * emcbsr = GetEMCheckBoxInstance(hSunRender);
			emcbsr->selected = emen->sunsky.sunOn;
			GlobalWindowSettings::colorSchemes.apply(emcbsr);
			InvalidateRect(hSunRender, NULL, false);

			EMGroupBar * emg;
			emg = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_ENV_STATICBAR1));
			GlobalWindowSettings::colorSchemes.apply(emg);
			emg = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_ENV_STATICBAR2));
			GlobalWindowSettings::colorSchemes.apply(emg);
			emg = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_ENV_STATICBAR3));
			GlobalWindowSettings::colorSchemes.apply(emg);
			emg = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_ENV_STATICBAR4));
			GlobalWindowSettings::colorSchemes.apply(emg);
			emg = GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_ENV_STATICBAR5));
			GlobalWindowSettings::colorSchemes.apply(emg);

			EMButton * emb1 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_ENV_OK));
			EMButton * emb2 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_ENV_CANCEL));
			GlobalWindowSettings::colorSchemes.apply(emb1);
			GlobalWindowSettings::colorSchemes.apply(emb2);

			EMCheckBox * emcbEnvEn = GetEMCheckBoxInstance(hEnvEnabled);
			emcbEnvEn->selected = env.enabled;
			GlobalWindowSettings::colorSchemes.apply(emcbEnvEn);
			EMButton * embTex = GetEMButtonInstance(hEnvTexture);
			embTex->selected = env.tex.isValid();
			GlobalWindowSettings::colorSchemes.apply(embTex);
			EMEditSpin * emesPower = GetEMEditSpinInstance(hEnvPower);
			emesPower->setValuesToFloat(env.powerEV, NOX_ENV_MAP_EV_MIN, NOX_ENV_MAP_EV_MAX, 0.5f, 0.05f);
			GlobalWindowSettings::colorSchemes.apply(emesPower);
			EMEditSpin * emesBlend = GetEMEditSpinInstance(hEnvBLayer);
			emesBlend->setValuesToInt(env.blendlayer, NOX_ENV_MAP_BLEND_MIN, NOX_ENV_MAP_BLEND_MAX, 1, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emesBlend);
			EMEditSpin * emesAzimuth = GetEMEditSpinInstance(hEnvAzimuth);
			emesAzimuth->setValuesToFloat(env.azimuth_shift, NOX_ENV_MAP_AZIMUTH_MIN, NOX_ENV_MAP_AZIMUTH_MAX, 5.0f, 0.1f);
			GlobalWindowSettings::colorSchemes.apply(emesAzimuth);

			EMColorShow * emcs = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_ENV_SUN_COLOR));
			GlobalWindowSettings::colorSchemes.apply(emcs);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_LONGITUDE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_LATITUDE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_DATE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_TIME)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_GMT)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_ALTITUDE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_AZIMUTH)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_VALUE_ALTITUDE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_VALUE_AZIMUTH)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_TURBIDITY)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_ALBEDO)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_SUNOTHERLAYER)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_SUNRENDER)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_SUNSIZE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_ENABLED)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_POWER)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_LAYER)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_ENV_TEXT_MAP_ROTATE)), true);

		}
		break;
	case WM_DESTROY:
		{
			HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
			EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
			DeleteObject(emen->bmEarth);
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
			case IDC_ENV_OK:
				{
					if (wmEvent == BN_CLICKED)
					{
						HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
						EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
						Raytracer * rtr = Raytracer::getInstance();

						EnvSunSky * res = new EnvSunSky();
						res->sunsky = new SunSky;
						res->sunsky->copyFrom(&emen->sunsky);
						res->env = new EnvSphere();
						*res->env = env;
						EndDialog(hWnd, (INT_PTR)res);
					}
				}
				break;
			case IDC_ENV_CANCEL:
				{
					if (wmEvent == BN_CLICKED)
					{
						EndDialog(hWnd, (INT_PTR)NULL);
					}
				}
				break;
			case IDC_ENV_SUN_OTHER_LAYER:
				{
					if (wmEvent == BN_CLICKED)
					{
						HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
						EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
						EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_ENV_SUN_OTHER_LAYER));
						emen->sunsky.sunOtherBlendLayer = emc->selected;
					}
				}
				break;
			case IDC_ENV_SUN_RENDER_SUN:
				{
					if (wmEvent == BN_CLICKED)
					{
						HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
						EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
						EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_ENV_SUN_RENDER_SUN));
						emen->sunsky.sunOn = emc->selected;
					}
				}
				break;
			case IDC_ENV_SUN_SIZE:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					HWND hSize = GetDlgItem(hWnd, IDC_ENV_SUN_SIZE);
					EMEditSpin * emessize = GetEMEditSpinInstance(hSize);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					if (!emen)
						break;
					emen->sunsky.sunSize = emessize->floatValue;
					emen->sunsky.maxCosSunSize = cos(emessize->floatValue/2*PI/180.0f);
					emen->sunsky.sunSizeSteradians = 2*PI*(1-cos(emessize->floatValue/2.0f *PI/180.0f));
				}
				break;
			case IDC_ENV_EARTH:
				{
					if (wmEvent != EN_POSITION_CHANGED)
						break;
				
					HWND hLong = GetDlgItem(hWnd, IDC_ENV_LONGITUDE);
					EMEditSpin * emeslong = GetEMEditSpinInstance(hLong);
					HWND hLat = GetDlgItem(hWnd, IDC_ENV_LATITUDE);
					EMEditSpin * emeslat = GetEMEditSpinInstance(hLat);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					if (!emen)
						break;

					emeslong->setValuesToInt(ROUND(-emen->sunsky.getLongitude()*180/PI) , -180, 179, 1, 0.1f);
					emeslat->setValuesToInt (ROUND(-emen->sunsky.getLatitude()*180/PI)  , -89 , 89 , 1, 0.1f);

					evalSunAngles(hWnd);
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_LONGITUDE:
				{
					if (wmEvent != WM_VSCROLL)
						break;

					HWND hLong = GetDlgItem(hWnd, IDC_ENV_LONGITUDE);
					EMEditSpin * emeslong = GetEMEditSpinInstance(hLong);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);

					changeDateAndPos(hWnd);
					evalSunAngles(hWnd);

					RECT rect;
					GetClientRect(hEnv, &rect);
					InvalidateRect(hEnv, &rect, false);
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_LATITUDE:
				{
					if (wmEvent != WM_VSCROLL)
						break;

					HWND hLat = GetDlgItem(hWnd, IDC_ENV_LATITUDE);
					EMEditSpin * emeslat = GetEMEditSpinInstance(hLat);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);

					changeDateAndPos(hWnd);
					evalSunAngles(hWnd);

					RECT rect;
					GetClientRect(hEnv, &rect);
					InvalidateRect(hEnv, &rect, false);
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_MONTH:
				{
					if (wmEvent != WM_VSCROLL)
						break;

					HWND hMonth = GetDlgItem(hWnd, IDC_ENV_MONTH);
					EMEditSpin * emesmonth = GetEMEditSpinInstance(hMonth);
					HWND hDay= GetDlgItem(hWnd, IDC_ENV_DAY);
					EMEditSpin * emesday = GetEMEditSpinInstance(hDay);

					int maxdays;
					switch (emesmonth->intValue)
					{
						case 1:  maxdays=31; break;
						case 2:  maxdays=28; break;
						case 3:  maxdays=31; break;
						case 4:  maxdays=30; break;
						case 5:  maxdays=31; break;
						case 6:  maxdays=30; break;
						case 7:  maxdays=31; break;
						case 8:  maxdays=31; break;
						case 9:  maxdays=30; break;
						case 10: maxdays=31; break;
						case 11: maxdays=30; break;
						case 12: maxdays=31; break;
						default: maxdays=1; break;
					}

					// number of days in month
					emesday->setValuesToInt(emesday->intValue, 1, maxdays, 1, 0.1f);

					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					emen->evalShadow();

					changeDateAndPos(hWnd);
					evalSunAngles(hWnd);
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_DAY:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					changeDateAndPos(hWnd);
					evalSunAngles(hWnd);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					emen->evalShadow();
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_HOUR:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					changeDateAndPos(hWnd);
					evalSunAngles(hWnd);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					emen->evalShadow();
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_MINUTE:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					changeDateAndPos(hWnd);
					evalSunAngles(hWnd);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					emen->evalShadow();
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_GMT:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					changeDateAndPos(hWnd);
					evalSunAngles(hWnd);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					emen->evalShadow();
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_TURBIDITY:
				{
					if (wmEvent != WM_HSCROLL)
						break;

					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					HWND hTur = GetDlgItem(hWnd, IDC_ENV_TURBIDITY);
					EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hTur);
					emen->sunsky.setSunDir(emen->sunsky.sunDir, emetb->intValue/10.0f);
					
					HWND hCol    = GetDlgItem(hWnd, IDC_ENV_SUN_COLOR);
					EMColorShow * emcs      = GetEMColorShowInstance(hCol);
					Color4 sc = emen->sunsky.getSunColor();
					sc.clamp();
					emcs->color = sc;
					RECT rect;
					GetClientRect(hCol, &rect);
					InvalidateRect(hCol, &rect, false);
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_ALBEDO:
				{
					if (wmEvent != WM_HSCROLL)
						break;

					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					HWND hAe = GetDlgItem(hWnd, IDC_ENV_ALBEDO);
					EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hAe);
					emen->sunsky.albedo = emetb->floatValue;
					emen->sunsky.updateHosekData();
					
					HWND hCol    = GetDlgItem(hWnd, IDC_ENV_SUN_COLOR);
					EMColorShow * emcs      = GetEMColorShowInstance(hCol);
					Color4 sc = emen->sunsky.getSunColor();
					sc.clamp();
					emcs->color = sc;
					RECT rect;
					GetClientRect(hCol, &rect);
					InvalidateRect(hCol, &rect, false);
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			case IDC_ENV_MODEL:
				{
					HWND hModel = GetDlgItem(hWnd, IDC_ENV_MODEL);
					EMComboBox * emModel = GetEMComboBoxInstance(hModel);
					CHECK(emModel);
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					CHECK(emen);
					if (wmEvent==CBN_SELCHANGE)
					{
						if (emModel->selected==1)
							emen->sunsky.use_hosek = true;
						else
							emen->sunsky.use_hosek = false;
					}
				}
				break;
			case IDC_ENV_MAP_ENABLED:
				{
					if (wmEvent != BN_CLICKED)
						break;
					EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_ENV_MAP_ENABLED));
					CHECK(emcb);
					env.enabled = emcb->selected;
				}
				break;
			case IDC_ENV_MAP_TEXTURE:
				{
					if (wmEvent != BN_CLICKED)
						break;

					Raytracer * rtr = Raytracer::getInstance();
					TextureInstance * t = &(env.tex);
					Texture * orig = NULL;
					if (t->managerID>=0)
						orig = rtr->curScenePtr->texManager.textures[t->managerID];
					Texture * tcopy = new Texture();
					tcopy->copyFromOther(orig);
					tcopy->texMod = t->texMod;

					Texture * texres = (Texture *)DialogBoxParam(hDllModule, 
						MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));

					if (texres)
					{
						if (t->filename)
							free(t->filename);
						if (t->fullfilename)
							free(t->fullfilename);
						t->filename = copyString(texres->filename);
						t->fullfilename = copyString(texres->fullfilename);

						rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

						int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
						t->managerID = id;

						t->texMod = texres->texMod;
						texres->freeAllBuffers();
						delete texres;

						EMButton * emb = GetEMButtonInstance(GetDlgItem(hWnd, IDC_ENV_MAP_TEXTURE));
						if (emb)
							emb->selected = t->isValid();
						InvalidateRect(emb->hwnd, NULL, false);

						EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_ENV_MAP_ENABLED));
						if (emcb)
							emcb->selected = t->isValid();
						env.enabled = t->isValid();
						InvalidateRect(emcb->hwnd, NULL, false);
					}
				}
				break;
			case IDC_ENV_MAP_POWER:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_ENV_MAP_POWER));
					CHECK(emes);
					env.powerEV = emes->floatValue;
				}
				break;
			case IDC_ENV_MAP_BLENDLAYER:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_ENV_MAP_BLENDLAYER));
					CHECK(emes);
					env.blendlayer = emes->intValue;
				}
				break;
			case IDC_ENV_MAP_AZIMUTH:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_ENV_MAP_AZIMUTH));
					CHECK(emes);
					env.azimuth_shift = emes->floatValue;
					HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
					EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
					informPluginChanges(&emen->sunsky, &env);
				}
				break;
			}
		}	// end WM_COMMAND
		break;
	case WM_CTLCOLORSTATIC:
		{	// colors for text background
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)bgBrush;
		}
	case WM_CTLCOLORDLG:
		{	// dialog background
			return (INT_PTR)bgBrush;
		}
		break;
	}
	return false;
}

void changeDateAndPos(HWND hWnd)
{
	HWND hMonth  = GetDlgItem(hWnd, IDC_ENV_MONTH);
	HWND hDay    = GetDlgItem(hWnd, IDC_ENV_DAY);
	HWND hHour   = GetDlgItem(hWnd, IDC_ENV_HOUR);
	HWND hMinute = GetDlgItem(hWnd, IDC_ENV_MINUTE);
	HWND hgmt    = GetDlgItem(hWnd, IDC_ENV_GMT);
	HWND hLong   = GetDlgItem(hWnd, IDC_ENV_LONGITUDE);
	HWND hLat    = GetDlgItem(hWnd, IDC_ENV_LATITUDE);
	HWND hEnv    = GetDlgItem(hWnd, IDC_ENV_EARTH);
	HWND hCol    = GetDlgItem(hWnd, IDC_ENV_SUN_COLOR);
	EMEditSpin * emesmonth  = GetEMEditSpinInstance(hMonth);
	EMEditSpin * emesday    = GetEMEditSpinInstance(hDay);
	EMEditSpin * emeshour   = GetEMEditSpinInstance(hHour);
	EMEditSpin * emesminute = GetEMEditSpinInstance(hMinute);
	EMEditSpin * emesgmt    = GetEMEditSpinInstance(hgmt);
	EMEditSpin * emeslong   = GetEMEditSpinInstance(hLong);
	EMEditSpin * emeslat    = GetEMEditSpinInstance(hLat);
	EMEnvironment * emen    = GetEMEnvironmentInstance(hEnv);
	EMColorShow * emcs      = GetEMColorShowInstance(hCol);

	emen->sunsky.setStandardMeridianForTimeZone(-emesgmt->intValue*PI/12);
	emen->sunsky.setDate(emesmonth->intValue, emesday->intValue, 
				emeshour->intValue, (float)emesminute->intValue);

	emen->sunsky.setPosition((float)-emeslong->intValue*(PI)/180.0f, (float)(-emeslat->intValue)*(PI)/180.0f);
	emen->evalSunPosition();
	emen->repaint();

	Color4 sc = emen->sunsky.getSunColor();
	sc.clamp();
	emcs->color = sc;
	RECT rect;
	GetClientRect(hCol, &rect);
	InvalidateRect(hCol, &rect, false);
}

void evalSunAngles(HWND hWnd)
{
	char buff[256];
	HWND hAzimuth, hAltitude;
	hAzimuth  = GetDlgItem(hWnd, IDC_ENV_TEXT_VALUE_AZIMUTH);
	hAltitude = GetDlgItem(hWnd, IDC_ENV_TEXT_VALUE_ALTITUDE);
	HWND hEnv = GetDlgItem(hWnd, IDC_ENV_EARTH);
	EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
	sprintf_s(buff,256, "%d�", 90-(int)(emen->sunsky.thetaS*180.0f/PI));
	SetWindowText(hAltitude, buff);
	sprintf_s(buff,256, "%d�", 180+(int)(emen->sunsky.phiS*180.0f/PI));
	SetWindowText(hAzimuth , buff);
}


EnvSunSky * showPluginSunSkyDialog(HWND hParent, SunSky * sun, EnvSphere * env)
{
	EnvSunSky envsunsky;
	envsunsky.sunsky = sun;
	envsunsky.env = env;

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->runDialogsUnicode)
	{
		return (EnvSunSky*) DialogBoxParamW(hDllModule, MAKEINTRESOURCEW(IDD_ENV_DIALOG), hParent, EnvDlgProc, (LPARAM)(&envsunsky));
	}
	else
	{
		return (EnvSunSky*) DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_ENV_DIALOG), hParent, EnvDlgProc, (LPARAM)(&envsunsky));
	}
	return NULL;
}

void registerSunSkyUpdateNotification(notifySunSkyPluginCallback * callback)
{
	sskyCallback = callback;
}

void informPluginChanges(SunSky * sunsky, EnvSphere * env)
{
	if (!sskyCallback)
		return;
	if (!sunsky)
		return;
	if (!env)
		return;

	(*sskyCallback)(sunsky->getMonth(),  sunsky->getDay(),  sunsky->getHour(),  (int)sunsky->getMinute(),  
				(int)(sunsky->getMeridianForTimeZone()*12/PI), //<-------
				sunsky->getLongitude(), sunsky->getLatitude(), env->azimuth_shift );
}

