#include "RendererMain2.h"
#include "EM2Controls.h"
#include "resource.h"
#include "raytracer.h"
#include "EMControls.h"

void setPositionsOnStartCorrectionTab(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcCorrection(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnStartCorrectionTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = NGCOL_LIGHT_GRAY;
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2GroupBar * emgr_curve = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_GR_CURVE));
				emgr_curve->bgImage = rMain->hBmpBg;
				emgr_curve->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_manipul = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_GR_COLOR_MANIPULATION));
				emgr_manipul->bgImage = rMain->hBmpBg;
				emgr_manipul->setFont(rMain->fonts->em2groupbar, false);

				EM2CheckBox * emcCRed = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_RED));
				emcCRed->bgImage = rMain->hBmpBg;
				emcCRed->colCheckSelected = RGB(255,0,0);
				emcCRed->colCheckUnselected= RGB(96,64,64);
				emcCRed->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcCGreen = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_GREEN));
				emcCGreen->bgImage = rMain->hBmpBg;
				emcCGreen->colCheckSelected = RGB(0,255,0);
				emcCGreen->colCheckUnselected= RGB(64,96,64);
				emcCGreen->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcCBlue = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_BLUE));
				emcCBlue->bgImage = rMain->hBmpBg;
				emcCBlue->colCheckSelected = RGB(0,0,255);
				emcCBlue->colCheckUnselected= RGB(64,64,96);
				emcCBlue->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcCLuminance = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_LUMINANCE));
				emcCLuminance->bgImage = rMain->hBmpBg;
				emcCLuminance->colCheckSelected = RGB(255,255,255);
				emcCLuminance->setFont(rMain->fonts->em2checkbox, false);

				EM2Button * embCReset = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_RESET));
				embCReset->bgImage = rMain->hBmpBg;
				embCReset->setFont(rMain->fonts->em2button, false);

				EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_CURVE));
				Raytracer::getInstance()->curScenePtr->curve = emc;

				EM2Text * emtBri = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_BRIGHTNESS));
				emtBri->bgImage = rMain->hBmpBg;
				emtBri->setFont(rMain->fonts->em2text, false);
				EM2Text * emtCon = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_CONTRAST));
				emtCon->bgImage = rMain->hBmpBg;
				emtCon->setFont(rMain->fonts->em2text, false);
				EM2Text * emtSat = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_SATURATION));
				emtSat->bgImage = rMain->hBmpBg;
				emtSat->setFont(rMain->fonts->em2text, false);
				EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_RED));
				emtRed->bgImage = rMain->hBmpBg;
				emtRed->setFont(rMain->fonts->em2text, false);
				EM2Text * emtGreen = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_GREEN));
				emtGreen->bgImage = rMain->hBmpBg;
				emtGreen->setFont(rMain->fonts->em2text, false);
				EM2Text * emtBlue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_BLUE));
				emtBlue->bgImage = rMain->hBmpBg;
				emtBlue->setFont(rMain->fonts->em2text, false);
				EM2Text * emtTemp = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_TEMPERATURE));
				emtTemp->bgImage = rMain->hBmpBg;
				emtTemp->setFont(rMain->fonts->em2text, false);

				EM2EditTrackBar * emetBri = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_BRIGHTNESS));
				emetBri->setFont(rMain->fonts->em2edittrack, false);
				emetBri->bgImage = rMain->hBmpBg;
				emetBri->setEditBoxWidth(42, 5);
				emetBri->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.0f, 0.1f);

				EM2EditTrackBar * emetCon = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CONTRAST));
				emetCon->setFont(rMain->fonts->em2edittrack, false);
				emetCon->bgImage = rMain->hBmpBg;
				emetCon->setEditBoxWidth(42, 5);
				emetCon->setValuesToFloat(1.0f, 0.0f, 2.0f, 1.0f, 0.1f);

				EM2EditTrackBar * emetSat = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_SATURATION));
				emetSat->setFont(rMain->fonts->em2edittrack, false);
				emetSat->bgImage = rMain->hBmpBg;
				emetSat->setEditBoxWidth(42, 5);
				emetSat->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.0f, 0.1f);

				EM2EditTrackBar * emetRed = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_RED));
				emetRed->setFont(rMain->fonts->em2edittrack, false);
				emetRed->bgImage = rMain->hBmpBg;
				emetRed->setEditBoxWidth(42, 5);
				emetRed->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.0f, 0.1f);

				EM2EditTrackBar * emetGreen = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_GREEN));
				emetGreen->setFont(rMain->fonts->em2edittrack, false);
				emetGreen->bgImage = rMain->hBmpBg;
				emetGreen->setEditBoxWidth(42, 5);
				emetGreen->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.0f, 0.1f);

				EM2EditTrackBar * emetBlue = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_BLUE));
				emetBlue->setFont(rMain->fonts->em2edittrack, false);
				emetBlue->bgImage = rMain->hBmpBg;
				emetBlue->setEditBoxWidth(42, 5);
				emetBlue->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.0f, 0.1f);

				EM2EditTrackBar * emetTemp = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEMPERATURE));
				emetTemp->setFont(rMain->fonts->em2edittrack, false);
				emetTemp->bgImage = rMain->hBmpBg;
				emetTemp->setEditBoxWidth(42, 5);
				emetTemp->setValuesToInt(6000, 1000, 11000, 6000, 100);

				EM2Button * embReset = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CORR_RESET));
				embReset->bgImage = rMain->hBmpBg;
				embReset->setFont(rMain->fonts->em2button, false);

				EM2Button * embApply = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CORR_REFRESH_NOW));
				embApply->bgImage = rMain->hBmpBg;
				embApply->setFont(rMain->fonts->em2button, false);

				EM2CheckBox * emcAuto = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_AUTOREFRESH));
				emcAuto->bgImage = rMain->hBmpBg;
				emcAuto->setFont(rMain->fonts->em2checkbox, false);
				emcAuto->selected = rMain->autoRedrawPost;


			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_TIMER:
			{
				if (wParam == TIMER_POSTPROCESS_ID)
				{
					KillTimer(hWnd, TIMER_POSTPROCESS_ID);
					rMain->refreshRenderOnThread();
				}
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_CORR_REFRESH_NOW:
						{
							CHECK(wmEvent==BN_CLICKED);
							rMain->refreshRenderOnThread();
						}
						break;
					case IDC_REND2_CORR_AUTOREFRESH:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcAutoLays = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabBlend,			IDC_REND2_BLEND_AUTOREFRESH));
							EM2CheckBox * emcAutoPost = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabPost,			IDC_REND2_POST_AUTOREFRESH));
							EM2CheckBox * emcAutoCorr = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabCorrection,	IDC_REND2_CORR_AUTOREFRESH));
							EM2CheckBox * ecur = emcAutoCorr;
							CHECK(ecur);
							rMain->autoRedrawPost = ecur->selected;
							if (emcAutoLays)
								emcAutoLays->selected = ecur->selected;
							if (emcAutoPost)
								emcAutoPost->selected = ecur->selected;
							if (emcAutoCorr)
								emcAutoCorr->selected = ecur->selected;
						}
						break;
					case IDC_REND2_CORR_BRIGHTNESS:
						{
							EM2EditTrackBar * emetBri = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_BRIGHTNESS));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setBrightness(emetBri->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setBrightness(emetBri->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_CONTRAST:
						{
							EM2EditTrackBar * emetCon = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CONTRAST));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setContrast(emetCon->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setContrast(emetCon->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_SATURATION:
						{
							EM2EditTrackBar * emetSat = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_SATURATION));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setSaturation(emetSat->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setSaturation(emetSat->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_RED:
					case IDC_REND2_CORR_GREEN:
					case IDC_REND2_CORR_BLUE:
						{
							EM2EditTrackBar * emetRed   = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_RED));
							EM2EditTrackBar * emetGreen = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_GREEN));
							EM2EditTrackBar * emetBlue  = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_BLUE));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setRGBAdd(emetRed->floatValue, emetGreen->floatValue, emetBlue->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setRGBAdd(emetRed->floatValue, emetGreen->floatValue, emetBlue->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_TEMPERATURE:
						{
							EM2EditTrackBar * emetTemp = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_CORR_TEMPERATURE));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setTemperature((float)emetTemp->intValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setTemperature((float)emetTemp->intValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_RESET:
						{
							CHECK(wmEvent == BN_CLICKED);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							CHECK(empv);  
							CHECK(empv->imgMod);

							EM2EditTrackBar * emetBri =		GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_CORR_BRIGHTNESS));
							EM2EditTrackBar * emetCon =		GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_CORR_CONTRAST));
							EM2EditTrackBar * emetSat =		GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_CORR_SATURATION));
							EM2EditTrackBar * emetRed =		GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_CORR_RED));
							EM2EditTrackBar * emetGre =		GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_CORR_GREEN));
							EM2EditTrackBar * emetBlu =		GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_CORR_BLUE));
							EM2EditTrackBar * emetTem =		GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_CORR_TEMPERATURE));

							emetBri->setFloatValue(emetBri->floatDefaultValue);
							emetCon->setFloatValue(emetCon->floatDefaultValue);
							emetSat->setFloatValue(emetSat->floatDefaultValue);
							emetRed->setFloatValue(emetRed->floatDefaultValue);
							emetGre->setFloatValue(emetGre->floatDefaultValue);
							emetBlu->setFloatValue(emetBlu->floatDefaultValue);
							emetTem->setIntValue(emetTem->intDefaultValue);

							empv->imgMod->setBrightness(emetBri->floatDefaultValue);
							empv->imgMod->setContrast(emetCon->floatDefaultValue);
							empv->imgMod->setSaturation(emetSat->floatDefaultValue);
							empv->imgMod->setRGBAdd(emetRed->floatDefaultValue, emetGre->floatDefaultValue, emetBlu->floatDefaultValue);
							empv->imgMod->setTemperature((float)emetTem->intDefaultValue);

							if (cam)
							{
								cam->iMod.setBrightness(emetBri->floatDefaultValue);
								cam->iMod.setContrast(emetCon->floatDefaultValue);
								cam->iMod.setSaturation(emetSat->floatDefaultValue);
								cam->iMod.setRGBAdd(emetRed->floatDefaultValue, emetGre->floatDefaultValue, emetBlu->floatDefaultValue);
								cam->iMod.setTemperature((float)emetTem->intDefaultValue);
							}

							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);

						}
						break;
					case IDC_REND2_CORR_CURVE_RED:
					case IDC_REND2_CORR_CURVE_GREEN:
					case IDC_REND2_CORR_CURVE_BLUE:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_CURVE));
							EM2CheckBox * emcbr = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_RED));
							EM2CheckBox * emcbg = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_GREEN));
							EM2CheckBox * emcbb = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_BLUE));
							EM2CheckBox * emcbl = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_LUMINANCE));

							bool rgbsel = emcbr->selected || emcbg->selected || emcbb->selected;
							bool recalc = (emcbl->selected==rgbsel);
							emcbl->selected = !rgbsel;
							InvalidateRect(emcbl->hwnd, NULL, false);
							emc->activeRGB[0] = emcbr->selected;
							emc->activeRGB[1] = emcbg->selected;
							emc->activeRGB[2] = emcbb->selected;
							emc->activeRGB[3] = emcbl->selected;
							emc->isLuminocityMode = emcbl->selected;
							InvalidateRect(emc->hwnd, NULL, false);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setCurveCheckboxes(emcbr->selected, emcbg->selected, emcbb->selected, emcbl->selected);
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setCurveCheckboxes(emcbr->selected, emcbg->selected, emcbb->selected, emcbl->selected);

							if (recalc  &&  rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_CURVE_LUMINANCE:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_CURVE));
							EM2CheckBox * emcbr = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_RED));
							EM2CheckBox * emcbg = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_GREEN));
							EM2CheckBox * emcbb = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_BLUE));
							EM2CheckBox * emcbl = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_LUMINANCE));

							emcbr->selected = !emcbl->selected;
							emcbg->selected = !emcbl->selected;
							emcbb->selected = !emcbl->selected;
							InvalidateRect(emcbr->hwnd, NULL, false);
							InvalidateRect(emcbg->hwnd, NULL, false);
							InvalidateRect(emcbb->hwnd, NULL, false);

							emc->activeRGB[0] = emcbr->selected;
							emc->activeRGB[1] = emcbg->selected;
							emc->activeRGB[2] = emcbb->selected;
							emc->activeRGB[3] = emcbl->selected;
							emc->isLuminocityMode = emcbl->selected;
							InvalidateRect(emc->hwnd, NULL, false);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
							{
								cam->iMod.setCurveCheckboxes(emcbr->selected, emcbg->selected, emcbb->selected, emcbl->selected);
							}
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setCurveCheckboxes(emcbr->selected, emcbg->selected, emcbb->selected, emcbl->selected);

							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_CURVE_RESET:
						{
							CHECK(wmEvent==BN_CLICKED);
							EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_CURVE));
							emc->reset();
							InvalidateRect(emc->hwnd, NULL, false);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.delCurveAllPoints();
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							if (empv  &&  empv->imgMod)
								empv->imgMod->delCurveAllPoints();

							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_CORR_CURVE_CURVE:
						{
							CHECK (wmEvent == CU_CHANGED);
							EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_CURVE));
							CHECK(emc);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
							{
								cam->iMod.delCurveAllPoints();
								for (unsigned int i=0; i<emc->pointsR.size(); i++)
									cam->iMod.addRedPoint(emc->pointsR[i].x, emc->pointsR[i].y);
								for (unsigned int i=0; i<emc->pointsG.size(); i++)
									cam->iMod.addGreenPoint(emc->pointsG[i].x, emc->pointsG[i].y);
								for (unsigned int i=0; i<emc->pointsB.size(); i++)
									cam->iMod.addBluePoint(emc->pointsB[i].x, emc->pointsB[i].y);
								for (unsigned int i=0; i<emc->pointsL.size(); i++)
									cam->iMod.addLuminancePoint(emc->pointsL[i].x, emc->pointsL[i].y);
							}

							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							if (empv  &&  empv->imgMod)
							{
								empv->imgMod->delCurveAllPoints();
								for (unsigned int i=0; i<emc->pointsR.size(); i++)
									empv->imgMod->addRedPoint(emc->pointsR[i].x, emc->pointsR[i].y);
								for (unsigned int i=0; i<emc->pointsG.size(); i++)
									empv->imgMod->addGreenPoint(emc->pointsG[i].x, emc->pointsG[i].y);
								for (unsigned int i=0; i<emc->pointsB.size(); i++)
									empv->imgMod->addBluePoint(emc->pointsB[i].x, emc->pointsB[i].y);
								for (unsigned int i=0; i<emc->pointsL.size(); i++)
									empv->imgMod->addLuminancePoint(emc->pointsL[i].x, emc->pointsL[i].y);
							}

							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;

				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelCorrection()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TITLE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgCurve = GetEM2GroupBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_GR_CURVE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_GR_CURVE), bgShiftXRightPanel, 0, emgCurve->bgShiftX, emgCurve->bgShiftY);
	EM2GroupBar * emgColMan = GetEM2GroupBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_GR_COLOR_MANIPULATION));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_GR_COLOR_MANIPULATION), bgShiftXRightPanel, 0, emgColMan->bgShiftX, emgColMan->bgShiftY);

	EM2CheckBox * emcCRed = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_RED));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_RED), bgShiftXRightPanel, 0, emcCRed->bgShiftX, emcCRed->bgShiftY);
	EM2CheckBox * emcCGreen = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_GREEN));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_GREEN), bgShiftXRightPanel, 0, emcCGreen->bgShiftX, emcCGreen->bgShiftY);
	EM2CheckBox * emcCBlue = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_BLUE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_BLUE), bgShiftXRightPanel, 0, emcCBlue->bgShiftX, emcCBlue->bgShiftY);
	EM2CheckBox * emcCLum = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_LUMINANCE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_LUMINANCE), bgShiftXRightPanel, 0, emcCLum->bgShiftX, emcCLum->bgShiftY);

	EM2Button * embCReset = GetEM2ButtonInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_RESET));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_RESET), bgShiftXRightPanel, 0, embCReset->bgShiftX, embCReset->bgShiftY);

	EM2Text * emtBri = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_BRIGHTNESS));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_BRIGHTNESS), bgShiftXRightPanel, 0, emtBri->bgShiftX, emtBri->bgShiftY);
	EM2Text * emtCon = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_CONTRAST));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_CONTRAST), bgShiftXRightPanel, 0, emtCon->bgShiftX, emtCon->bgShiftY);
	EM2Text * emtSat = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_SATURATION));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_SATURATION), bgShiftXRightPanel, 0, emtSat->bgShiftX, emtSat->bgShiftY);
	EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_RED));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_RED), bgShiftXRightPanel, 0, emtRed->bgShiftX, emtRed->bgShiftY);
	EM2Text * emtGreen = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_GREEN));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_GREEN), bgShiftXRightPanel, 0, emtGreen->bgShiftX, emtGreen->bgShiftY);
	EM2Text * emtBlue = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_BLUE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_BLUE), bgShiftXRightPanel, 0, emtBlue->bgShiftX, emtBlue->bgShiftY);
	EM2Text * emtTemp = GetEM2TextInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_TEMPERATURE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEXT_TEMPERATURE), bgShiftXRightPanel, 0, emtTemp->bgShiftX, emtTemp->bgShiftY);

	EM2EditTrackBar * emetBri = GetEM2EditTrackBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_BRIGHTNESS));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_BRIGHTNESS), bgShiftXRightPanel, 0, emetBri->bgShiftX, emetBri->bgShiftY);
	EM2EditTrackBar * emetCon = GetEM2EditTrackBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CONTRAST));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CONTRAST), bgShiftXRightPanel, 0, emetCon->bgShiftX, emetCon->bgShiftY);
	EM2EditTrackBar * emetSat = GetEM2EditTrackBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_SATURATION));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_SATURATION), bgShiftXRightPanel, 0, emetSat->bgShiftX, emetSat->bgShiftY);
	EM2EditTrackBar * emetRed = GetEM2EditTrackBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_RED));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_RED), bgShiftXRightPanel, 0, emetRed->bgShiftX, emetRed->bgShiftY);
	EM2EditTrackBar * emetGreen = GetEM2EditTrackBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_GREEN));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_GREEN), bgShiftXRightPanel, 0, emetGreen->bgShiftX, emetGreen->bgShiftY);
	EM2EditTrackBar * emetBlue = GetEM2EditTrackBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_BLUE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_BLUE), bgShiftXRightPanel, 0, emetBlue->bgShiftX, emetBlue->bgShiftY);
	EM2EditTrackBar * emetTemp = GetEM2EditTrackBarInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEMPERATURE));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEMPERATURE), bgShiftXRightPanel, 0, emetTemp->bgShiftX, emetTemp->bgShiftY);

	EM2CheckBox * emcAuto = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_AUTOREFRESH));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_AUTOREFRESH), bgShiftXRightPanel, 0, emcAuto->bgShiftX, emcAuto->bgShiftY);

	EM2Button * embReset = GetEM2ButtonInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_RESET));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_RESET), bgShiftXRightPanel, 0, embReset->bgShiftX, embReset->bgShiftY);
	EM2Button * embApply = GetEM2ButtonInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_REFRESH_NOW));
	updateControlBgShift(GetDlgItem(hTabCorrection, IDC_REND2_CORR_REFRESH_NOW), bgShiftXRightPanel, 0, embApply->bgShiftX, embApply->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartCorrectionTab(HWND hWnd)
{
	int x, y;
	x = 8;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TITLE),				HWND_TOP,	130+x,		11, 100, 16, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_REFRESH_NOW),			HWND_TOP,	136,		39, 103, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_AUTOREFRESH),			HWND_TOP,	x+360-46,	45,  46, 10, 0);
	
	x=8; y=75;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_GR_CURVE),				HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_RED),			HWND_TOP,	x,		y+33,	90,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_GREEN),			HWND_TOP,	x,		y+63,	90,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_BLUE),			HWND_TOP,	x,		y+93,	90,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_LUMINANCE),		HWND_TOP,	x,		y+123,	90,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_CURVE),			HWND_TOP,	x+360-258,	y+34,	258,	258,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CURVE_RESET),			HWND_TOP,	x,		y+270,	92,		22,			0);


	x=8; y=385;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_GR_COLOR_MANIPULATION),HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_BRIGHTNESS),		HWND_TOP,	x,		y+30,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_BRIGHTNESS),			HWND_TOP,	x+102,	y+28,	258,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_CONTRAST),		HWND_TOP,	x,		y+60,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CONTRAST),				HWND_TOP,	x+102,	y+58,	258,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_SATURATION),		HWND_TOP,	x,		y+90,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_SATURATION),			HWND_TOP,	x+102,	y+88,	258,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_RED),				HWND_TOP,	x,		y+120,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_RED),					HWND_TOP,	x+102,	y+118,	258,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_GREEN),			HWND_TOP,	x,		y+150,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_GREEN),				HWND_TOP,	x+102,	y+148,	258,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_BLUE),			HWND_TOP,	x,		y+180,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_BLUE),					HWND_TOP,	x+102,	y+178,	258,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEXT_TEMPERATURE),		HWND_TOP,	x,		y+210,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEMPERATURE),			HWND_TOP,	x+102,	y+208,	258,	20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_RESET),				HWND_TOP,	x,		y+240,	92,		22,			0);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_BRIGHTNESS),	HWND_TOP,										0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_CONTRAST),		GetDlgItem(hWnd, IDC_REND2_CORR_BRIGHTNESS),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_SATURATION),	GetDlgItem(hWnd, IDC_REND2_CORR_CONTRAST),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_RED),			GetDlgItem(hWnd, IDC_REND2_CORR_SATURATION),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_GREEN),		GetDlgItem(hWnd, IDC_REND2_CORR_RED),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_BLUE),			GetDlgItem(hWnd, IDC_REND2_CORR_GREEN),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CORR_TEMPERATURE),	GetDlgItem(hWnd, IDC_REND2_CORR_BLUE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);

}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillCorrectionTabAll(ImageModifier * iMod)
{
	CHECK(iMod);

	EM2EditTrackBar * emetBri =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabCorrection, IDC_REND2_CORR_BRIGHTNESS));
	EM2EditTrackBar * emetCon =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabCorrection, IDC_REND2_CORR_CONTRAST));
	EM2EditTrackBar * emetSat =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabCorrection, IDC_REND2_CORR_SATURATION));
	EM2EditTrackBar * emetRed =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabCorrection, IDC_REND2_CORR_RED));
	EM2EditTrackBar * emetGre =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabCorrection, IDC_REND2_CORR_GREEN));
	EM2EditTrackBar * emetBlu =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabCorrection, IDC_REND2_CORR_BLUE));
	EM2EditTrackBar * emetTem =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabCorrection, IDC_REND2_CORR_TEMPERATURE));

	emetBri->setFloatValue(iMod->getBrightness());
	emetCon->setFloatValue(iMod->getContrast());
	emetSat->setFloatValue(iMod->getSaturation());
	emetRed->setFloatValue(iMod->getR());
	emetGre->setFloatValue(iMod->getG());
	emetBlu->setFloatValue(iMod->getB());
	emetTem->setIntValue((int)iMod->getTemperature());

	EM2CheckBox * emcRed   = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_RED));
	EM2CheckBox * emcGreen = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_GREEN));
	EM2CheckBox * emcBlue  = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_BLUE));
	EM2CheckBox * emcLum   = GetEM2CheckBoxInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_LUMINANCE));
	bool cr,cg,cb,cl;
	iMod->getCurveCheckboxes(cr,cg,cb,cl);
	emcRed->selected = cr;
	emcGreen->selected = cg;
	emcBlue->selected = cb;
	emcLum->selected = cl;
	InvalidateRect(emcRed->hwnd, NULL, false);
	InvalidateRect(emcGreen->hwnd, NULL, false);
	InvalidateRect(emcBlue->hwnd, NULL, false);
	InvalidateRect(emcLum->hwnd, NULL, false);

	EMCurve * curve = GetEMCurveInstance(GetDlgItem(hTabCorrection, IDC_REND2_CORR_CURVE_CURVE));
	curve->reset();
	curve->activeRGB[0] = cr;
	curve->activeRGB[1] = cg;
	curve->activeRGB[2] = cb;
	curve->activeRGB[3] = cl;
	curve->isLuminocityMode = cl;

	for (int i=0; i<iMod->getCurveRedPointsCount(); i++)
	{
		EMCurve::P2D p;
		iMod->getCurveRedPoint(i, p.x, p.y);
		curve->pointsR.push_back(p);
	}
	for (int i=0; i<iMod->getCurveGreenPointsCount(); i++)
	{
		EMCurve::P2D p;
		iMod->getCurveGreenPoint(i, p.x, p.y);
		curve->pointsG.push_back(p);
	}
	for (int i=0; i<iMod->getCurveBluePointsCount(); i++)
	{
		EMCurve::P2D p;
		iMod->getCurveBluePoint(i, p.x, p.y);
		curve->pointsB.push_back(p);
	}
	for (int i=0; i<iMod->getCurveLuminancePointsCount(); i++)
	{
		EMCurve::P2D p;
		iMod->getCurveLuminancePoint(i, p.x, p.y);
		curve->pointsL.push_back(p);
	}
	curve->createLines();
	InvalidateRect(curve->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabCorrection(bool nowrendering)
{
}

//------------------------------------------------------------------------------------------------
