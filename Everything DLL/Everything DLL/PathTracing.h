#ifndef __PATH_TRACING_H
#define __PATH_TRACING_H

#include "raytracer.h"

//-----------------------------------------------------------------------------------------------------

class PTStack
{
public:
	HitData hd;
	Point3d int_point;
	Vector3d normal_shade_orig;
	Vector3d normal_geom;
	float u,v;

	MaterialNox * mat;
	MatLayer * mlay;
	Triangle * tri;
	Matrix4d inst_mat;
	float rough;
	Color4 brdf;		// including cosine, rrpdf, mlaypdf, excluded dirpdf
	Color4 energy_in;	// from camera without current brdf etc
	float pdf_brdf;
	float pdf_rr;

	bool fake_transmission;
	bool updatePdfBrdfMultilayered();
	bool evalBrdfPdfMultilayered(Color4 &rbrdf, float &rpdf, float &rbpdf);

	PTStack() { mat=NULL; mlay=NULL; tri=NULL; inst_mat.setIdentity(); rough=1.0f; brdf=energy_in=Color4(1,1,1); fake_transmission = false;
				pdf_brdf=1.0f; u=v=1.0f; hd=HitData(); int_point=Point3d(0,0,0); normal_geom=normal_shade_orig=Vector3d(0,1,0); pdf_rr=1.0f; }
	~PTStack() {}
};

//-----------------------------------------------------------------------------------------------------

// this is current version of path tracing 

class PathTracer3
{
public:
	SceneStatic * ss;
	Camera * cam;
	bool * working;
	int threadID;
	bool discard_caustic;
	bool discard_secondary_caustic;
	bool fake_caustic;
	float maxCausticRough;

	void renderingLoop();
	bool isShadowOpacFakeGlass(const float sample_time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, Color4 &attenuation, bool stopOnPortal);
	float findNonOpacFakeTri(const float sample_time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, float &uu, float &vv, Color4 &attenuation, bool stopOnPortal, bool wasNonSpec);
	bool randomNewDirection(PTStack *pt, int lvl, Vector3d &dirOut, Point3d &ipoint);

	PathTracer3(SceneStatic * scs, Camera * camera, bool * running);
	~PathTracer3();
};

//-----------------------------------------------------------------------------------------------------

class ShadeTracer
{
public:
	static int shade_type;
	static const int ST_SMOOTH = 0;
	static const int ST_GEOM = 1;
	static const int ST_NORMAL = 2;
	static const int ST_NORMAL_GEOM = 3;
	SceneStatic * ss;
	Camera * cam;
	bool * working;
	int threadID;

	void renderingLoop();

	ShadeTracer(SceneStatic * scs, Camera * camera, bool * running);
	~ShadeTracer();
};

//-----------------------------------------------------------------------------------------------------

#endif

