#ifndef COLOR_SCHEMES_H
#define COLOR_SCHEMES_H

// this are color schemes for first version of nox gui, mostly not used anymore, but can't be removed yet

#define COLOR_WINDOW_BACKGROUND RGB(128,192,255)
#define COLOR_CONTROLS_OTHER_BACKGROUND RGB(104,168,255)
#define COLOR_CONTROLS_OTHER_BACKGROUND_CLICKED RGB(112,176,255)

#include <windows.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "EMControls.h"

class ThemeManager
{
private:
	xmlDocPtr xmlDoc;
	int errLine;

	bool getColor(char * key, COLORREF &c);
	bool parseEMButton(xmlNodePtr curNode);
	bool parseEMImgButton(xmlNodePtr curNode);
	bool parseEMImgStateButton(xmlNodePtr curNode);
	bool parseEMPView(xmlNodePtr curNode);
	bool parseEMComboBox(xmlNodePtr curNode);
	bool parseEMCheckBox(xmlNodePtr curNode);
	bool parseEMEditTrackBar(xmlNodePtr curNode);
	bool parseEMEditSpin(xmlNodePtr curNode);
	bool parseEMFresnel(xmlNodePtr curNode);
	bool parseEMColorPicker(xmlNodePtr curNode);
	bool parseEMColorShow(xmlNodePtr curNode);
	bool parseEMGroupBar(xmlNodePtr curNode);
	bool parseEMProgressBar(xmlNodePtr curNode);
	bool parseEMStatusBar(xmlNodePtr curNode);
	bool parseEMTabs(xmlNodePtr curNode);
	bool parseEMListView(xmlNodePtr curNode);
	bool parseEMCurve(xmlNodePtr curNode);
	bool parseEMTemperaturePicker(xmlNodePtr curNode);
	bool parseEMLine(xmlNodePtr curNode);
	bool parseEMEditSimple(xmlNodePtr curNode);
	bool parseEMPages(xmlNodePtr curNode);
	bool parseEMScrollbar(xmlNodePtr curNode);
	bool parseEMImagelist(xmlNodePtr curNode);
	bool parseWindow(xmlNodePtr curNode);

public:
	COLORREF WindowBackground;
	COLORREF WindowText;
	COLORREF DialogBackground;
	COLORREF DialogBackgroundSecond;
	COLORREF DialogText;

	COLORREF EMButtonText;
    COLORREF EMButtonBackGnd;
    COLORREF EMButtonBackGndSelected;
	COLORREF EMButtonBackGndClicked;
	COLORREF EMButtonBorderLight;
	COLORREF EMButtonBorderDark;
	COLORREF EMButtonDisabledBackground;
	COLORREF EMButtonDisabledBorderTopLeft;
	COLORREF EMButtonDisabledBorderBottomRight;
	COLORREF EMButtonDisabledText;

	COLORREF EMImgButtonBorder;
	COLORREF EMImgButtonBorderMouseOver;
	COLORREF EMImgButtonBorderClick;
	COLORREF EMImgButtonBorderDisabled;

	COLORREF EMImgStateButtonBorder;
	COLORREF EMImgStateButtonBorderSelected;

	COLORREF EMPViewBorder;
	COLORREF EMPViewBackground;

	COLORREF EMComboBoxBorderTopLeft;
	COLORREF EMComboBoxBorderBottomRight;
	COLORREF EMComboBoxBorderTopLeftClicked;
	COLORREF EMComboBoxBorderBottomRightClicked;
	COLORREF EMComboBoxSelected;
	COLORREF EMComboBoxMouseOver;
	COLORREF EMComboBoxNormal;
	COLORREF EMComboBoxText;
	COLORREF EMComboBoxArrowNormal;
	COLORREF EMComboBoxArrowPressed;
	COLORREF EMComboBoxDisabledArrow;
	COLORREF EMComboBoxDisabledBackground;
	COLORREF EMComboBoxDisabledBorderTopLeft;
	COLORREF EMComboBoxDisabledBorderBottomRight;
	COLORREF EMComboBoxDisabledText;


	COLORREF EMCheckBoxBorderLight;
	COLORREF EMCheckBoxBorderDark;
	COLORREF EMCheckBoxBackground;
	COLORREF EMCheckBoxBackgroundDisabled;
	COLORREF EMCheckBoxXCol;
	COLORREF EMCheckBoxXColDisabled;

	COLORREF EMEditTrackBarBackground;
	COLORREF EMEditTrackBarPathBorderUpLeft;
	COLORREF EMEditTrackBarPathBorderDownRight;
	COLORREF EMEditTrackBarPathBackgroundBefore;
	COLORREF EMEditTrackBarPathBackgroundAfter;
	COLORREF EMEditTrackBarSliderBorderUpLeft;
	COLORREF EMEditTrackBarSliderBorderDownRight;
	COLORREF EMEditTrackBarSliderBackground;
	COLORREF EMEditTrackBarSliderBackgroundClicked;
	COLORREF EMEditTrackBarEditBorderUpLeft;
	COLORREF EMEditTrackBarEditBorderDownRight;
	COLORREF EMEditTrackBarEditBackground;
	COLORREF EMEditTrackBarEditText;
	COLORREF EMEditTrackBarDisabledBorderLight;
	COLORREF EMEditTrackBarDisabledBorderDark;
	COLORREF EMEditTrackBarDisabledBackground;
	COLORREF EMEditTrackBarDisabledEditBackground;
	COLORREF EMEditTrackBarDisabledEditText;

	COLORREF EMEditSpinBackground;
	COLORREF EMEditSpinButtonBorderUpLeft;
	COLORREF EMEditSpinButtonBorderDownRight;
	COLORREF EMEditSpinButtonBackground;
	COLORREF EMEditSpinButtonBackgroundClick;
	COLORREF EMEditSpinArrow;
	COLORREF EMEditSpinArrowClicked;
	COLORREF EMEditSpinEditBorderUpLeft;
	COLORREF EMEditSpinEditBorderDownRight;
	COLORREF EMEditSpinEditBackground;
	COLORREF EMEditSpinEditText;
	COLORREF EMEditSpinDisabledBorderLight;
	COLORREF EMEditSpinDisabledBorderDark;
	COLORREF EMEditSpinDisabledBackground;
	COLORREF EMEditSpinDisabledEditBackground;
	COLORREF EMEditSpinDisabledEditText;
	COLORREF EMEditSpinDisabledArrow;

	COLORREF EMFresnelBorderLeftUp;
	COLORREF EMFresnelBorderRightDown;
	COLORREF EMFresnelBackgroundUp;
	COLORREF EMFresnelBackgroundDown;
	COLORREF EMFresnelPoint;
	COLORREF EMFresnelPointOver;
	COLORREF EMFresnelPointClicked;
	COLORREF EMFresnelLine;

	COLORREF EMColorPickerBorderLeftUp;
	COLORREF EMColorPickerBorderRightDown;
	COLORREF EMColorPickerBackground;
	COLORREF EMColorPickerArrow;
	COLORREF EMColorPickerArrowClicked;

	COLORREF EMColorShowBorderLeftUp;
	COLORREF EMColorShowBorderRightDown;
	COLORREF EMColorShowDragBorderLeftUp;
	COLORREF EMColorShowDragBorderRightDown;

    COLORREF EMGroupBarText;
    COLORREF EMGroupBarBackGnd;
	COLORREF EMGroupBarBorderLight;
	COLORREF EMGroupBarBorderDark;

	COLORREF EMProgressBarBackGnd;
	COLORREF EMProgressBarBorderLight;
	COLORREF EMProgressBarBorderDark;
	COLORREF EMProgressBarPBackGnd;
	COLORREF EMProgressBarPBorderLight;
	COLORREF EMProgressBarPBorderDark;
	COLORREF EMProgressBarDisabledBackground;
	COLORREF EMProgressBarDisabledBorderLight;
	COLORREF EMProgressBarDisabledBorderDark;
	COLORREF EMProgressBarDisabledPBackground;
	COLORREF EMProgressBarDisabledPBorderLight;
	COLORREF EMProgressBarDisabledPBorderDark;
	
    COLORREF EMStatusBarText;
    COLORREF EMStatusBarBackGnd;
	COLORREF EMStatusBarBorderUpLeft;
	COLORREF EMStatusBarBorderDownRight;

    COLORREF EMTabsText;
    COLORREF EMTabsBackGnd;
	COLORREF EMTabsBorderLight;
	COLORREF EMTabsBorderDark;
	COLORREF EMTabsBorderBottom;
	COLORREF EMTabsSelText;
	COLORREF EMTabsSelBackGnd;
	COLORREF EMTabsSelBorderBottom;
	COLORREF EMTabsDisText;
	COLORREF EMTabsDisBackGnd;
	COLORREF EMTabsDisBorderLight;
	COLORREF EMTabsDisBorderDark;
	COLORREF EMTabsDisBorderBottom;
	COLORREF EMTabsDisSelBorderBottom;

	COLORREF EMListViewHeaderBackground;
	COLORREF EMListViewHeaderBorderUpLeft;
	COLORREF EMListViewHeaderBorderDownRight;
	COLORREF EMListViewBackgroundOdd;
	COLORREF EMListViewBackgroundEven;
	COLORREF EMListViewBackgroundSelected;
	COLORREF EMListViewText;
	COLORREF EMListViewTextSelected;
	COLORREF EMListViewMainBorderUpLeft;
	COLORREF EMListViewMainBorderDownRight;
	COLORREF EMListViewTextMouseOver;
	COLORREF EMListViewBackgroundMouseOver;
	COLORREF EMListViewSBTrackBackground;
	COLORREF EMListViewSBBackground;
	COLORREF EMListViewSBBackgroundClicked;
	COLORREF EMListViewSBBorderUpLeft;
	COLORREF EMListViewSBBorderDownRight;

    COLORREF EMCurveBackGnd;
	COLORREF EMCurveDotLines;
	COLORREF EMCurveSelOuter;
	COLORREF EMCurveBorder;
	COLORREF EMCurveLineRed;
	COLORREF EMCurveLineGreen;
	COLORREF EMCurveLineBlue;
	COLORREF EMCurveLineLight;

	COLORREF EMTemperaturePickerBorderLeftUp;
	COLORREF EMTemperaturePickerBorderRightDown;
	COLORREF EMTemperaturePickerSlider;
	COLORREF EMTemperaturePickerText;

	COLORREF EMEditSimpleBorderUpLeft;
	COLORREF EMEditSimpleBorderDownRight;
	COLORREF EMEditSimpleBackground;
	COLORREF EMEditSimpleText;
	COLORREF EMEditSimpleDisabledBorderUpLeft;
	COLORREF EMEditSimpleDisabledBorderDownRight;
	COLORREF EMEditSimpleDisabledBackground;
	COLORREF EMEditSimpleDisabledText;

	COLORREF EMLineBorderLeftUp;
	COLORREF EMLineBorderRightDown;

	COLORREF EMPagesLinkNormal;
	COLORREF EMPagesLinkCurrent;
	COLORREF EMPagesLinkMouseOver;
	COLORREF EMPagesNoLink;
	COLORREF EMPagesBackground;
	
	COLORREF EMScrollbarTrack;
	COLORREF EMScrollbarTrackLight;
	COLORREF EMScrollbarTrackDark;
	COLORREF EMScrollbarSlider;
	COLORREF EMScrollbarSliderMouseOver;
	COLORREF EMScrollbarSliderClicked;
	COLORREF EMScrollbarSliderLight;
	COLORREF EMScrollbarSliderDark;
	COLORREF EMScrollbarButton;
	COLORREF EMScrollbarButtonMouseOver;
	COLORREF EMScrollbarButtonClicked;
	COLORREF EMScrollbarButtonLight;
	COLORREF EMScrollbarButtonDark;
	COLORREF EMScrollbarButtonArrow;
	COLORREF EMScrollbarButtonArrowMouseOver;
	COLORREF EMScrollbarButtonArrowClicked;
	COLORREF EMScrollbarDisabledTrack;
	COLORREF EMScrollbarDisabledTrackLight;
	COLORREF EMScrollbarDisabledTrackDark;
	COLORREF EMScrollbarDisabledSlider;
	COLORREF EMScrollbarDisabledSliderLight;
	COLORREF EMScrollbarDisabledSliderDark;
	COLORREF EMScrollbarDisabledButton;
	COLORREF EMScrollbarDisabledButtonLight;
	COLORREF EMScrollbarDisabledButtonDark;
	COLORREF EMScrollbarDisabledButtonArrow;

	COLORREF EMImageListBg;
	COLORREF EMImageListBgMouseOver;
	COLORREF EMImageListBgSelected;
	COLORREF EMImageListControlBorderLeftUp;
	COLORREF EMImageListControlBorderRightDown;
	COLORREF EMImageListImgBorderLeftUp;
	COLORREF EMImageListImgBorderRightDown;
	COLORREF EMImageListImgBorderSelectedLeftUp;
	COLORREF EMImageListImgBorderSelectedRightDown;
	COLORREF EMImageListText1;
	COLORREF EMImageListText2;
	COLORREF EMImageListText1Selected;
	COLORREF EMImageListText2Selected;
	COLORREF EMImageListDisabledBg;
	COLORREF EMImageListDisabledControlBorderLeftUp;
	COLORREF EMImageListDisabledControlBorderRightDown;
	COLORREF EMImageListDisabledImgBorderLeftUp;
	COLORREF EMImageListDisabledImgBorderRightDown;
	COLORREF EMImageListDisabledText1;
	COLORREF EMImageListDisabledText2;



	char * MaterialWindowIcons;

	bool loadFromFile(char * filename);
	void freeStrings();

	void apply(EMButton * emb);
	void apply(EMImgButton * emib);
	void apply(EMImgStateButton * emisb);
	void apply(EMPView * empv);
	void apply(EMComboBox * emcb);
	void apply(EMCheckBox * emcb);
	void apply(EMEditSpin * emes);
	void apply(EMEditTrackBar * emetb);
	void apply(EMFresnel * emf);
	void apply(EMColorPicker * emcp);
	void apply(EMColorShow * emcs);
	void apply(EMGroupBar * emgb);
	void apply(EMProgressBar * empb);
	void apply(EMStatusBar * emsb);
	void apply(EMTabs * emt);
	void apply(EMListView * emlv);
	void apply(EMText * emt, bool isDialog, bool secondColor=false);
	void apply(EMCurve * emc);
	void apply(EMTemperaturePicker * emtp);
	void apply(EMLine * eml);
	void apply(EMEditSimple * emes);
	void apply(EMPages * emp);
	void apply(EMScrollBar * emsb);
	void apply(EMImageList * emil);


	ThemeManager();
};



#endif

