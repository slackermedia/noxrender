#ifndef __NOX_CAMERA_H__
#define __NOX_CAMERA_H__

#include "Max.h"
#include "istdplug.h"
#include "resource.h"
#include "iparamb2.h"
#include "iparamm2.h"

#define NOX_CAMERA_DEFAULT_AA 2

#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"

extern TCHAR *GetString(int id);


#define NOXCamera_CLASS_ID	Class_ID(0x26ea06b5, 0x7cff7191)
#define NOXCameraTarget_CLASS_ID	Class_ID(0x41390e9e, 0x6f884eef)

class NOXTargetObject: public GeomObject 
{			   
	Mesh targMesh;		

	void GetMatrix(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm);
	void BuildMesh();


public:
	NOXTargetObject();

	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	#ifdef ISMAX2013
		const TCHAR *GetObjectName() { return GetString(IDS_NOX_TARGET_OBJECT_NAME); }
	#else
		TCHAR *GetObjectName() { return GetString(IDS_NOX_TARGET_OBJECT_NAME); }
	#endif

	ObjectState Eval(TimeValue time);
	void InitNodeName(TSTR& s) { s = GetString(IDS_NOX_CAM_TARGET_DEFNAME); }
	int UsesWireColor() { return 1; }
	int IsRenderable() { return 0; }

	RefResult NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message );

	int IntersectRay(TimeValue t, Ray& r, float& at);
	ObjectHandle CreateTriObjRep(TimeValue t);  // for rendering, also for deformation		
	void GetWorldBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel );

	BOOL HasViewDependentBoundingBox() { return true; }

	void DeleteThis() {	 delete this; }
	Class_ID ClassID() { return Class_ID(TARGET_CLASS_ID,0); }  
	void GetClassName(TSTR& s) { s = TSTR(GetString(IDS_NOX_TARGET_CLASS_NAME)); }
	int IsKeyable(){ return 1;}

	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir());

	int CanConvertToType(Class_ID obtype) { return obtype==ClassID(); }

	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);
};



class NOXTargetObjectClassDesc : public ClassDesc2 
{
public:
	int 			IsPublic() { return 0; }
	void *			Create(BOOL loading = FALSE) { return new NOXTargetObject; }
	const TCHAR *	ClassName() { return GetString(IDS_NOX_CAM_TARGET_CLASS_NAME); }
	SClass_ID		SuperClassID() { return GEOMOBJECT_CLASS_ID; }
	Class_ID		ClassID() { return Class_ID( TARGET_CLASS_ID, 0); }
	const TCHAR* 	Category() { return GetString(IDS_NOX_CAM_PRIMITIVES);  }
};

class NOXTargetObjectCreateCallBack: public CreateMouseCallBack 
{
public:
	NOXTargetObject *eob;
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat );
	void SetObj(NOXTargetObject *obj) { eob = obj; }
};

//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCamera : public GenCamera 
{
	public:
		static HWND hParams;
		TrCamera trCam;
		NOXTargetObject * etarg;

		IOResult Load(ILoad *iload);
		IOResult Save(ISave *isave);

		Class_ID ClassID() {return NOXCamera_CLASS_ID;}		
		SClass_ID SuperClassID() { return CAMERA_CLASS_ID; }
		void GetClassName(TSTR& s) {s = GetString(IDS_NOX_CAM_FREE_CLASS_NAME);}

		RefTargetHandle Clone( RemapDir &remap );
		RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

		int NumSubs() { return 0; }
		TSTR SubAnimName(int i) { return GetString(IDS_PARAMS_SOME_NEEDED_JUNK_CAM); }
		Animatable* SubAnim(int i) { return NULL;}//pblock; }

		// get data
		BOOL IsOrtho();
		float GetFOV(TimeValue t, Interval& valid = Interval(0,0));
		float GetTDist(TimeValue t, Interval& valid = Interval(0,0));
		int GetManualClip();
		float GetClipDist(TimeValue t, int which, Interval &valid=Interval(0,0));
		float GetEnvRange(TimeValue t, int which, Interval& valid = Interval(0,0));
		BOOL GetEnvDisplay(void);
		int GetConeState();
		int GetHorzLineState();
		int GetFOVType();
		int Type();
		Control *GetFOVControl();


		// set data
		void SetConeState(int s);
		void SetHorzLineState(int s);
		void Enable(int enab);
		BOOL SetFOVControl(Control *c);
		void SetFOVType(int ft);
		void SetType(int tp);
		void SetOrtho(BOOL b);
		void SetFOV(TimeValue t, float f); 
		void SetTDist(TimeValue t, float f);
		void SetManualClip(int onOff);
		void SetClipDist(TimeValue t, int which, float val);
		void SetEnvRange(TimeValue time, int which, float f);
		void SetEnvDisplay(BOOL b, int notify=TRUE);

		void RenderApertureChanged(TimeValue t);
		CreateMouseCallBack* GetCreateMouseCallBack();
		ObjectState Eval(TimeValue t);
		RefResult EvalCameraState(TimeValue time, Interval& valid, CameraState* cs);
		void BeginEditParams (IObjParam * ip, ULONG flags, Animatable * prev = NULL);
		void EndEditParams(IObjParam * ip, ULONG flags, Animatable * next = NULL);

		Interval ObjectValidity(TimeValue time);		
		void GetWorldBoundBox(TimeValue t, INode *mat, ViewExp* vpt, Box3& box);
		void GetLocalBoundBox(TimeValue t, INode *mat, ViewExp* vpt, Box3& box);
		void GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel);
		void GetConePoints(TimeValue t, Point3* q, float dist);
		void DrawCone(TimeValue t, GraphicsWindow *gw, float dist, int colid, BOOL drawSides, BOOL drawDiags);
		void DrawTargetLine(TimeValue t, GraphicsWindow *gw, float dist, int colid);
		void DrawBox(TimeValue t, GraphicsWindow *gw, Box3 * box);
		int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) ;
		int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
		int DoOwnSelectHilite() { return 1; }
		void SetExtendedDisplay(int flags) { displayFlags = flags; }
		bool createMesh();
		void UpdateTargDistance(TimeValue t, INode* inode);
		void makeActiveOnlyMe(INode * rootNode);


		float tDist;
		int coneState;
		int horzLineState;
		bool envDisplay;
		bool focusConnectedToTarget;
		
		bool tiltON;
		float focusLength;
		int displayFlags;
		Mesh camMesh;
		Control * cntrl;
		bool enabled;

		int NumRefs() { return 0; }
		RefTargetHandle GetReference(int i) { return NULL;}
		void SetReference(int i, RefTargetHandle rtarg) { }

		int	NumParamBlocks() { return 0;}
		IParamBlock2* GetParamBlock(int i) { return NULL;}
		IParamBlock2* GetParamBlockByID(BlockID id) { return NULL;}

		void DeleteThis() { delete this; }		
		
		NOXCamera * NewCamera(int type);
		NOXCamera();
		~NOXCamera();

};

//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCameraClassDesc : public ClassDesc2	// Free Camera
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new NOXCamera(); }
	const TCHAR *	ClassName() { return GetString(IDS_NOX_CAM_FREE_CLASS_NAME); }
	SClass_ID		SuperClassID() { return CAMERA_CLASS_ID; }
	Class_ID		ClassID() { return NOXCamera_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_NOX_CAM_CATEGORY); }
	const TCHAR*	InternalName() { return _T("NOX_Camera_Free"); }
	HINSTANCE		HInstance() { return hInstance; }
};

//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCameraTargetClassDesc : public ClassDesc2	// Target Camera
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new NOXCamera(); }
	int 			BeginCreate(Interface *i);
	int 			EndCreate(Interface *i);
	const TCHAR *	ClassName() { return GetString(IDS_NOX_CAM_TARGET_CLASS_NAME); }
    SClass_ID		SuperClassID() { return CAMERA_CLASS_ID; }
   	Class_ID		ClassID() { return NOXCameraTarget_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_NOX_CAM_CATEGORY); }
	const TCHAR*	InternalName() { return _T("NOX_Camera_Target"); }
	HINSTANCE		HInstance() { return hInstance; }
};


//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCameraCreateMouseCB : public CreateMouseCallBack
{
public:
	NOXCamera * cam;
	int proc(ViewExp *vpt, int msg, int point, int flags, IPoint2 m, Matrix3 &mat);
};

//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCamFocusDistMouseCallback: public MouseCallBack//, ReferenceMaker {
{
public:
	NOXCamera * camObject;
	
	NOXCamFocusDistMouseCallback()	{ camObject = NULL; }
	int proc( HWND hwnd, int msg, int point, int flag, IPoint2 m );
};

class NOXCamFocusDistCommandMode: public CommandMode
{
public:
	int  Class () {return MODIFY_COMMAND;}
	HWND hButton;
	int  ID () {return CID_USER+1732;}
	MouseCallBack *  MouseProc (int *numPoints);
	ChangeForegroundCallback *  ChangeFGProc ();
	BOOL  ChangeFG (CommandMode *oldMode);
	void  EnterMode ();
	void  ExitMode (); 
	static NOXCamFocusDistCommandMode& GetInstance();

	NOXCamera * cam;
	NOXCamFocusDistCommandMode() { cam = NULL; hButton=NULL; }

};

//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCamCreationManager : public MouseCallBack, ReferenceMaker {
private:
	CreateMouseCallBack *createCB;
	INode *camNode,*targNode;
	NOXCamera*camObject;
	NOXTargetObject *targObject;
	int attachedToNode;
	IObjCreate *createInterface;
	ClassDesc *cDesc;
	Matrix3 mat;
	IPoint2 pt0;
	int ignoreSelectionChange;
	int lastPutCount;

	void CreateNewObject();	

	int NumRefs() { return 1; }
	RefTargetHandle GetReference(int i) { return (RefTargetHandle)camNode; } 
	void SetReference(int i, RefTargetHandle rtarg) { camNode = (INode *)rtarg; }

    RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
				PartID& partID,  RefMessage message);

public:

		void Begin( IObjCreate *ioc, ClassDesc *desc );
		void End();
	
	NOXCamCreationManager()	{ ignoreSelectionChange = FALSE; }
	int proc( HWND hwnd, int msg, int point, int flag, IPoint2 m );
};

//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCamCreateMode : public CommandMode 
{
	NOXCamCreationManager proc;
public:
	void Begin( IObjCreate *ioc, ClassDesc *desc ) { proc.Begin( ioc, desc ); }
	void End() { proc.End(); }

	int Class() { return CREATE_COMMAND; }
	int ID() { return CID_USER + 1; }
	MouseCallBack *MouseProc(int *numPoints) { *numPoints = 1000000; return &proc; }
	ChangeForegroundCallback *ChangeFGProc() { return CHANGE_FG_SELECTED; }
	BOOL ChangeFG( CommandMode *oldMode ) { return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }
	void EnterMode();
	void ExitMode();
	BOOL IsSticky() { return FALSE; }
};

//-----------------------------------------------------------------------------------------------------------------------------------------------

class NOXCamParamDialog : public ParamDlg
{
public:
	NOXCamera * ecam;
	HWND hCamPanel;
	HINSTANCE hInst;

	Class_ID ClassID();
	void SetThing (ReferenceTarget *m);
	ReferenceTarget * GetThing();
	void SetTime(TimeValue t);
	void ReloadDialog();
	void DeleteThis();
	void ActivateDlg(BOOL onOff);

	NOXCamParamDialog(NOXCamera* mCam, IMtlParams *imp, HINSTANCE hInstance);
	~NOXCamParamDialog();
};

//--------------------------------------------------------------------------------------------------------------

#endif
