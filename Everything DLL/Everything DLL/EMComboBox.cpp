
#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <stdio.h>
#include "EMControls.h"
#include "log.h"

extern HMODULE hDllModule;

EMComboBox::EMComboBox(HWND hWnd)
{
	hwnd = hWnd;
	opened = false;
	nowOpening = false;
	nowScrolling = false;
	scrollClickedBG = true;
	drawArrow = true;
	leftMargin = 5;
	hght = 20;
	numItems = 0;
	selected = -1;
	mouseOverItem = -1;
	canShowNumRows = 0;


	showSlider = false;
	canShowSlider = true;
	sliderWidth = 16;
	sliderHeight = 0;
	sliderRect.bottom = sliderRect.left = sliderRect.right = sliderRect.top = 0;
	sliderPos = 0;
	startDrawing = 0;

	colBorderTopLeft = RGB(144, 208, 255);
	colBorderBottomRight = RGB(96,144,240);
	colBorderTopLeftClicked = RGB(96,144,240);
	colBorderBottomRightClicked = RGB(144, 208, 255);
	colSelected = RGB(64,128,255);
	colMouseOver = RGB(64,240,192);
	colNormal = RGB(96,160,255);
	colText = RGB(0,0,0);
	colArrowNormal = RGB(0,0,0);
	colArrowPressed = RGB(255,255,255);
	colDisabledArrow = RGB(128,128,128);
	colDisabledBackground = RGB(192,192,192);
	colDisabledBorderTopLeft = RGB(224,224,224);
	colDisabledBorderBottomRight = RGB(160,160,160);
	colDisabledText = RGB(96,96,96);

	hRgnOpened = 0;
	hRgnClosed = 0;
	CreateRegions(hRgnClosed, hRgnOpened);
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

}

EMComboBox::~EMComboBox()
{
	if (hFont)
		DeleteObject(hFont);
	deleteItems();
}

void InitEMComboBox()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMComboBox";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMComboBoxProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMComboBox *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}


EMComboBox * GetEMComboBoxInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMComboBox * emcb = (EMComboBox *)GetWindowLongPtr(hwnd, 0);
	#else
		EMComboBox * emcb = (EMComboBox *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcb;
}

void SetEMComboBoxInstance(HWND hwnd, EMComboBox *emcb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcb);
	#endif
}

LRESULT CALLBACK EMComboBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMComboBox * emcb;
	emcb = GetEMComboBoxInstance(hwnd);
//	RECT rect;
	HDC hdc;
	PAINTSTRUCT ps;
	static int sx, sy, sPos;

    switch(msg)
    {
	case WM_CREATE:
		{
			EMComboBox * emcb1 = new EMComboBox(hwnd);
			SetEMComboBoxInstance(hwnd, (EMComboBox *)emcb1);			
		}
	break;
	case WM_NCDESTROY:
		{
			EMComboBox * emcb1;
			emcb1 = GetEMComboBoxInstance(hwnd);
			delete emcb1;
			SetEMComboBoxInstance(hwnd, 0);
		}
	break;
	case CB_GETCURSEL:
	{
		return (LRESULT)emcb->selected;
	}
	break;
	case WM_SIZE:
	{
		emcb->CreateRegions(emcb->hRgnClosed, emcb->hRgnOpened);
	}
	break;
	case WM_PAINT:
	{
		HDC thdc;
		if (!emcb) 
			break;

		int hght = emcb->hght;
		bool opened = emcb->opened;
		bool nowOpening = emcb->nowOpening;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);

		RECT rect;
		GetClientRect(hwnd, &rect);

		hdc = BeginPaint(hwnd, &ps);
		thdc = CreateCompatibleDC(hdc); 
		HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
		HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

		RECT inner;
		inner.top = inner.left = 0;
		inner.right = rect.right;
		inner.bottom = hght;


		HPEN hOldPen;
		HFONT hOldFont;
		HBRUSH oldBrush;
		SIZE sz;

		hOldFont = (HFONT)SelectObject(thdc, emcb->hFont);

		if (!disabled)
		{
			SetTextColor(thdc, emcb->colText);
			SetBkColor  (thdc, emcb->colNormal);
		}
		else
		{
			SetTextColor(thdc, emcb->colDisabledText);
			SetBkColor  (thdc, emcb->colDisabledBackground);
		}

		// write out string
		int sel = emcb->selected;
		if (sel>-1)
		{
			GetTextExtentPoint32(thdc, emcb->items.objArray[sel], (int)strlen(emcb->items.objArray[sel]), &sz);
			ExtTextOut(thdc, emcb->leftMargin, (hght-sz.cy)/2, ETO_OPAQUE, 
						&inner, emcb->items.objArray[sel], (int)strlen(emcb->items.objArray[sel]), 0);
		}
		else
		{
			GetTextExtentPoint32(thdc, "", 0, &sz);
			ExtTextOut(thdc, emcb->leftMargin, (hght-sz.cy)/2, ETO_OPAQUE, &inner, "", 0, 0);
		}

		if (emcb->drawArrow)
		{
			// draw arrow
			POINT arrow[] = {
				{inner.right-14, hght/2-2}, 
				{inner.right-6,  hght/2-2}, 
				{inner.right-10, hght/2+2}, 
				{inner.right-14, hght/2-2}
			};
			if (disabled)
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colDisabledArrow));
			else
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colArrowNormal));
			Polyline(thdc, arrow, 4);
			DeleteObject(SelectObject(thdc, hOldPen));

			// fill arrow
			if (disabled)
			{
				oldBrush = (HBRUSH)SelectObject(thdc, CreateSolidBrush(emcb->colDisabledArrow));
				ExtFloodFill(thdc, inner.right-10, hght/2, emcb->colDisabledArrow, FLOODFILLBORDER);
			}
			else
			{
				oldBrush = (HBRUSH)SelectObject(thdc, CreateSolidBrush(emcb->colArrowNormal));
				ExtFloodFill(thdc, inner.right-10, hght/2, emcb->colArrowNormal, FLOODFILLBORDER);
			}
			DeleteObject(SelectObject(thdc, oldBrush));
		}

		// draw main border
		POINT mainBorder1[] = {
			{1, hght-2},
			{0, hght-3},
			{0, 2},
			{2, 0},
			//{12,30},
			{inner.right-3, 0},
			{inner.right-1, 2}
		};	// 6
		POINT mainBorder2[] = {
			{inner.right-1, 2},
			{inner.right-1, hght-3},
			{inner.right-3, hght-1},
			{2, hght-1},
			{1, hght-2}
		};	// 5

		if (disabled)
		{
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colDisabledBorderTopLeft));
			Polyline(thdc, mainBorder1, 6);
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colDisabledBorderBottomRight)));
			Polyline(thdc, mainBorder2, 5);
			DeleteObject(SelectObject(thdc, hOldPen));
		}
		else
			if (opened || nowOpening)
			{
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderTopLeftClicked));
				Polyline(thdc, mainBorder1, 6);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderBottomRightClicked)));
				Polyline(thdc, mainBorder2, 5);
				DeleteObject(SelectObject(thdc, hOldPen));
			}
			else
			{
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderTopLeft));
				Polyline(thdc, mainBorder1, 6);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderBottomRight)));
				Polyline(thdc, mainBorder2, 5);
				DeleteObject(SelectObject(thdc, hOldPen));
			}


		// draw opened bar
		if (opened || nowOpening)
		{
			RECT brect;
			brect.left = 1;
			brect.right = rect.right-1;
			int i;
			char * cAddr;
			for (i=0; i<emcb->canShowNumRows; i++)
			{
				brect.top = (i+1)*hght+1;
				brect.bottom = (i+2)*hght+1;

				SetTextColor(thdc, emcb->colText);
				if (i+emcb->startDrawing==emcb->selected)
					SetBkColor  (thdc, emcb->colSelected);
				else
					SetBkColor  (thdc, emcb->colNormal);

				if (i==emcb->mouseOverItem)
					SetBkColor  (thdc, emcb->colMouseOver);

				cAddr = emcb->items[i+emcb->startDrawing];

				GetTextExtentPoint32(thdc, cAddr, (int)strlen(cAddr), &sz);
				ExtTextOut(thdc, emcb->leftMargin, (hght-sz.cy)/2+(i+1)*hght, 
							ETO_OPAQUE, &brect, cAddr, (int)strlen(cAddr), 0);
			}

			// draw outer border of list
			POINT lBorder[] = {
				{0, hght},
				{rect.right-1, hght},
				{rect.right-1, hght*(emcb->canShowNumRows+1)+1},
				{0         , hght*(emcb->canShowNumRows+1)+1},
				{0, hght}
			};	// 5

			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderTopLeftClicked));
			Polyline(thdc, lBorder, 5);
			DeleteObject(SelectObject(thdc, hOldPen));

			// show slider
			if (emcb->canShowSlider && emcb->showSlider)
			{
				RECT inSl;
				inSl.top = emcb->hght;
				inSl.bottom = hght*(emcb->canShowNumRows+1)+2;
				inSl.left = rect.right - emcb->sliderWidth;
				inSl.right = rect.right;

				POINT slBorder[] = {
					{inSl.left, inSl.top},
					{inSl.right-1, inSl.top},
					{inSl.right-1, inSl.bottom-1},
					{inSl.left, inSl.bottom-1},
					{inSl.left, inSl.top}
				};

				// draw background
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderTopLeft));
				Polyline(thdc, slBorder, 5);
				DeleteObject(SelectObject(thdc, hOldPen));

				oldBrush = (HBRUSH)SelectObject(thdc, CreateSolidBrush(emcb->colNormal));
				ExtFloodFill(thdc, inSl.left+1, inSl.top+1, emcb->colBorderTopLeft, FLOODFILLBORDER);
				DeleteObject(SelectObject(thdc, oldBrush));

				RECT sl = emcb->sliderRect;
				POINT slBarBorder[] = {
					{sl.left, sl.top},
					{sl.right-1, sl.top},
					{sl.right-1, sl.bottom-1},
					{sl.left, sl.bottom-1},
					{sl.left, sl.top}
				};

				// draw slider bar
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderTopLeft));
				Polyline(thdc, slBarBorder, 5);
				DeleteObject(SelectObject(thdc, hOldPen));

				oldBrush = (HBRUSH)SelectObject(thdc, CreateSolidBrush(emcb->colNormal));
				ExtFloodFill(thdc, sl.left+1, sl.top+1, emcb->colBorderTopLeft, FLOODFILLBORDER);
				DeleteObject(SelectObject(thdc, oldBrush));

				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID , 1, emcb->colBorderTopLeft));
				POINT line1[] = { {sl.left+4, (sl.top+sl.bottom)/2-4}, {sl.right-4, (sl.top+sl.bottom)/2-4} };
				Polyline(thdc, line1, 2);
				POINT line2[] = { {sl.left+4, (sl.top+sl.bottom)/2  }, {sl.right-4, (sl.top+sl.bottom)/2  } };
				Polyline(thdc, line2, 2);
				POINT line3[] = { {sl.left+4, (sl.top+sl.bottom)/2+4}, {sl.right-4, (sl.top+sl.bottom)/2+4} };
				Polyline(thdc, line3, 2);
				DeleteObject(SelectObject(thdc, hOldPen));
			}	// end slider
		}	// end list bar

		GetClientRect(hwnd, &rect);
		SelectObject(thdc, hOldFont);
		//DeleteObject(SelectObject(thdc, hOldFont));
		BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
		SelectObject(thdc, oldBMP);
		DeleteObject(hbmp);
		DeleteDC(thdc);
		EndPaint(hwnd, &ps);
		ValidateRect(hwnd, &rect);
	}
	break;

	case WM_MOUSEMOVE:
	{
		if (!GetCapture())
			SetCapture(hwnd);
		if (GetCapture() == hwnd)
		{
			if ((!emcb->opened) && (!emcb->nowOpening))
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				rect.bottom = emcb->hght;
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				if (!PtInRect(&rect, pt))
				{
					ReleaseCapture();
					break;
				}
			}
			if (emcb->opened || emcb->nowOpening)
			{
				if (emcb->nowScrolling)
				{
					POINT pt = { LOWORD(lParam), HIWORD(lParam) };
					emcb->sliderPos = sPos + pt.y - sy;

					if (emcb->sliderHeight + emcb->sliderPos > emcb->canShowNumRows*emcb->hght+2)
						emcb->sliderPos = emcb->canShowNumRows*emcb->hght+2 - emcb->sliderHeight;

					if (emcb->sliderPos < 0)
						emcb->sliderPos = 0;

					emcb->sliderRect.top = emcb->hght + emcb->sliderPos;
					emcb->sliderRect.bottom = emcb->sliderHeight + emcb->hght + emcb->sliderPos;
					
					emcb->updateScroll();
					RECT rect;
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
					break;
				}

				int cOver;
				RECT rect;
				RECT inner;
				GetClientRect(hwnd, &rect);
				inner.top = rect.top + emcb->hght + 1;
				inner.left = rect.left + 1;
				inner.right = rect.right - 1;
				inner.bottom = emcb->hght*(emcb->canShowNumRows+1)+2;
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				if (PtInRect(&inner, pt))
				{
					int y = pt.y - 1 - emcb->hght;
					cOver = y/emcb->hght;
					if (emcb->canShowSlider && emcb->showSlider)
					{
						if (pt.x > rect.right - emcb->sliderWidth-1)
							cOver = -1;
					}
				}
				else
					 cOver = -1;
				if (cOver != emcb->mouseOverItem)
				{
					emcb->mouseOverItem = cOver;
					InvalidateRect(hwnd, &rect, false);
				}

			}
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		SetFocus(hwnd);
		if (emcb->opened)
		{
			int cOver;
			RECT rect;
			RECT inner;
			GetClientRect(hwnd, &rect);
			inner.top = rect.top + emcb->hght + 1;
			inner.left = rect.left + 1;
			inner.right = rect.right - 1;
			inner.bottom = emcb->hght*(emcb->canShowNumRows+1)+2;
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (PtInRect(&inner, pt) && emcb->numItems>0)
			{
				int y = pt.y - 1 - emcb->hght;
				cOver = y/emcb->hght;
				if (emcb->canShowSlider && emcb->showSlider)
				{
					if (pt.x > rect.right - emcb->sliderWidth-1)
					{	// sliding
						cOver = -1;
						emcb->nowScrolling = true;
						if (PtInRect(&(emcb->sliderRect), pt))
						{
							emcb->scrollClickedBG = false;
						}
						else
						{
							emcb->scrollClickedBG = true;
							if (pt.y > emcb->sliderRect.bottom)
							{	// lower
								emcb->sliderPos += emcb->sliderHeight;
								if (emcb->sliderHeight + emcb->sliderPos > emcb->canShowNumRows*emcb->hght+2)
									emcb->sliderPos = emcb->canShowNumRows*emcb->hght+2 - emcb->sliderHeight;

								emcb->sliderRect.top = emcb->hght + emcb->sliderPos;
								emcb->sliderRect.bottom = emcb->sliderHeight + emcb->hght + emcb->sliderPos;
							}
							else
							{	// higher
								emcb->sliderPos -= emcb->sliderHeight;
								if (emcb->sliderPos < 0)
									emcb->sliderPos = 0;

								emcb->sliderRect.top = emcb->hght + emcb->sliderPos;
								emcb->sliderRect.bottom = emcb->sliderHeight + emcb->hght + emcb->sliderPos;
							}
							emcb->updateScroll();
						}
						sx = pt.x;
						sy = pt.y;
						sPos = emcb->sliderPos;
					}
				}
			}
			else
			{
				emcb->sliderPos = 0;
				emcb->updateScroll();

				 cOver = -1;
			}
			if (cOver>-1)
			{
				emcb->selected = cOver + emcb->startDrawing;
				emcb->sliderPos = 0;
				emcb->updateScroll();
			}

			if (!emcb->nowScrolling)
			{
				emcb->nowOpening = false;
				emcb->opened = false;
				emcb->CreateRegions(emcb->hRgnClosed, emcb->hRgnOpened);
			}
			GetClientRect(hwnd, &rect);
			InvalidateRect(hwnd, &rect, false);
			if (cOver>-1)
			{
				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, CBN_SELCHANGE), 
						(LPARAM)hwnd);
			}
		}
		else
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			rect.bottom = emcb->hght;
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (PtInRect(&rect, pt))
			{
				emcb->nowOpening = true;

				emcb->CreateRegions(emcb->hRgnClosed, emcb->hRgnOpened);


				emcb->sliderPos = (emcb->selected)*(2+emcb->canShowNumRows*emcb->hght - emcb->sliderHeight)
											/max(1, (emcb->numItems - emcb->canShowNumRows)) +1;
				emcb->updateScroll();

				GetClientRect(hwnd, &rect);
				InvalidateRect(hwnd, &rect, true);
				InvalidateRect(hwnd, &rect, false);
			}

		}

	}
	break;
	case WM_LBUTTONUP:
	{
		if (GetCapture() != hwnd)
			break;
		if (emcb->nowOpening)
		{
			emcb->nowOpening = false;
			emcb->opened = true;
		}
		else
		{
			if (emcb->nowScrolling)
			{
				emcb->nowScrolling = false;
			}
			else
			{
				emcb->sliderPos = 0;
				emcb->updateScroll();
				ReleaseCapture();
			}
		}
	}
	break;
	case WM_MOUSEWHEEL:
	{
		if (emcb->opened)
		{
			if (emcb->canShowSlider && emcb->showSlider)
			{
				if (emcb->nowScrolling)
					break;

				RECT rect;
				GetWindowRect(hwnd, &rect);
				rect.top += emcb->hght;
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				if (PtInRect(&rect, pt))
				{
					GetClientRect(hwnd, &rect);
					int zDelta = (int)wParam;

					if (zDelta<0)
						emcb->sliderPos = (emcb->startDrawing+1)*(2+emcb->canShowNumRows*emcb->hght - emcb->sliderHeight)
											/(emcb->numItems - emcb->canShowNumRows) +1;
					else
						emcb->sliderPos = (emcb->startDrawing-1)*(2+emcb->canShowNumRows*emcb->hght - emcb->sliderHeight)
											/(emcb->numItems - emcb->canShowNumRows) +1;

					if (emcb->sliderHeight + emcb->sliderPos > emcb->canShowNumRows*emcb->hght+2)
						emcb->sliderPos = emcb->canShowNumRows*emcb->hght+2 - emcb->sliderHeight;

					if (emcb->sliderPos < 2)
						emcb->sliderPos = 0;

					emcb->sliderRect.top = emcb->hght + emcb->sliderPos;
					emcb->sliderRect.bottom = emcb->sliderHeight + emcb->hght + emcb->sliderPos;
					
					emcb->updateScroll();
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}
			}
		}
	}
	break;


    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMComboBox::CreateRegions(HRGN &hrgClosed, HRGN &hrgOpened)
{
	if (hrgClosed)
		DeleteObject(hrgClosed);
	if (hrgOpened)
		DeleteObject(hrgOpened);

	RECT rect;
	GetClientRect(hwnd, &rect);
	POINT p1[] = {
		{0, 2},
		{2, 0},
		{rect.right-2, 0},
		{rect.right, 2},
		{rect.right, hght-3},
		{rect.right-3, hght},
		{2, hght},
		{0, hght-3},
	};	// 8

	int hItems = (numItems+1)*hght+2;
	int hCtrl = rect.bottom - ((rect.bottom-2)%hght);
	int hMax = min(hItems, hCtrl);
	canShowNumRows = hMax/hght - 1;

	POINT p2[] = {
		{2, hght},
		{0, hght-3},
		{0, 2},
		{2, 0},
		{rect.right-2, 0},
		{rect.right, 2},
		{rect.right, hght-3},
		{rect.right-3, hght},
		{rect.right, hght},
		{rect.right, hMax},
		{0, hMax},
		{0, hght}
	};

	hrgClosed = CreatePolygonRgn(p1, 8, WINDING);			
	hrgOpened = CreatePolygonRgn(p2, 12, WINDING);			

	if (opened || nowOpening)
		SetWindowRgn(hwnd, hrgOpened, FALSE);
	else
		SetWindowRgn(hwnd, hrgClosed, TRUE);

	showSlider = (numItems > canShowNumRows);
	sliderHeight = (int)(canShowNumRows/(float)numItems*(hMax-hght));
	sliderPos = 0;

	sliderRect.left = rect.right-sliderWidth;
	sliderRect.right = rect.right;
	sliderRect.top = hght + sliderPos;
	sliderRect.bottom = sliderHeight + hght + sliderPos;
}

void EMComboBox::updateScroll()
{
	if (canShowSlider && showSlider)
	{
		startDrawing = (numItems-canShowNumRows)*sliderPos/(2+canShowNumRows*hght - sliderHeight);
		sliderRect.top = hght + sliderPos;
		sliderRect.bottom = sliderHeight + hght + sliderPos;
		if (startDrawing + canShowNumRows > numItems)
			startDrawing = numItems - canShowNumRows;
	}
}

int EMComboBox::addItem(char * caption)
{
	if (!caption)
		return -1;
	if (strlen(caption) == 0)
		return -1;

	char * cpt = (char *)malloc(strlen(caption)+1);
	if (!cpt)
		return -1;

	strcpy_s(cpt, strlen(caption)+1 , caption); 

	if (!items.add(cpt))
		return -1;
	
	if (!items.createArray())
		return -2;

	numItems = items.objCount;
	return items.objCount-1;

}

void EMComboBox::deleteItems()
{
	for (int i=0; i<items.objCount; i++)
	{
		if (items.objArray[i])
			free(items.objArray[i]);
	}
	items.freeList();
	items.destroyArray();
}

