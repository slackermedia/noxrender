
const char * ibPostOpenClProgram = "\
__kernel void SAXPY (__global float* x)\
{\
	const int i = get_global_id (0);\
	x[i*4] = x[i*4]*2;\
}\
\
\
__kernel void applyISOToneGamma(__global float * x, float mpl, float tone, float gamma)\
{\
	const int i = get_global_id (0);\
	float r = x[i*4+0] * mpl;\
	float g = x[i*4+1] * mpl;\
	float b = x[i*4+2] * mpl;\
	r = r / pow(1 + r, tone);\
	g = g / pow(1 + g, tone);\
	b = b / pow(1 + b, tone);\
	x[i*4+0] = pow(r, gamma);\
	x[i*4+1] = pow(g, gamma);\
	x[i*4+2] = pow(b, gamma);\
}\
\
__kernel void applyFilterBoxOld(__global float * src, __global float * dst, int aa, int dwidth, int dheight)\
{\
	const int i = get_global_id (0);\
	int x = i % dwidth;\
	int y = (i - x) / dwidth;\
	int i1 = x*aa+   y*aa*   aa*dwidth;\
	int i2 = x*aa+1+ y*aa*   aa*dwidth;\
	int i3 = x*aa+2+ y*aa*   aa*dwidth;\
	int i4 = x*aa+  (y*aa+1)*aa*dwidth;\
	int i5 = x*aa+1+(y*aa+1)*aa*dwidth;\
	int i6 = x*aa+2+(y*aa+1)*aa*dwidth;\
	int i7 = x*aa+  (y*aa+2)*aa*dwidth;\
	int i8 = x*aa+1+(y*aa+2)*aa*dwidth;\
	int i9 = x*aa+2+(y*aa+2)*aa*dwidth;\
	dst[i*4+0] = (src[i1*4+0]+src[i2*4+0]+src[i3*4+0]+src[i4*4+0]+src[i5*4+0]+src[i6*4+0]+src[i7*4+0]+src[i8*4+0]+src[i9*4+0])/9.0;\
	dst[i*4+1] = (src[i1*4+1]+src[i2*4+1]+src[i3*4+1]+src[i4*4+1]+src[i5*4+1]+src[i6*4+1]+src[i7*4+1]+src[i8*4+1]+src[i9*4+1])/9.0;\
	dst[i*4+2] = (src[i1*4+2]+src[i2*4+2]+src[i3*4+2]+src[i4*4+2]+src[i5*4+2]+src[i6*4+2]+src[i7*4+2]+src[i8*4+2]+src[i9*4+2])/9.0;\
	dst[i*4+3] = 1.0;\
}\
\
__kernel void applyFilterBoxAA2(__global float * src, __global float * dst, int dwidth, int dheight)\
{\
	const int i = get_global_id (0);\
	int x = i % dwidth;\
	int y = (i - x) / dwidth;\
	int i1 = x*2+   y*2*   2*dwidth;\
	int i2 = x*2+1+ y*2*   2*dwidth;\
	int i3 = x*2+  (y*2+1)*2*dwidth;\
	int i4 = x*2+1+(y*2+1)*2*dwidth;\
	dst[i*4+0] = (src[i1*4+0]+src[i2*4+0]+src[i3*4+0]+src[i4*4+0])*0.25;\
	dst[i*4+1] = (src[i1*4+1]+src[i2*4+1]+src[i3*4+1]+src[i4*4+1])*0.25;\
	dst[i*4+2] = (src[i1*4+2]+src[i2*4+2]+src[i3*4+2]+src[i4*4+2])*0.25;\
	dst[i*4+3] = 1.0;\
}\
\
__kernel void applyFilterBoxAA3(__global float * src, __global float * dst, int dwidth, int dheight)\
{\
	const int i = get_global_id (0);\
	int x = i % dwidth;\
	int y = (i - x) / dwidth;\
	int i1 = x*3+   y*3*   3*dwidth;\
	int i2 = x*3+1+ y*3*   3*dwidth;\
	int i3 = x*3+2+ y*3*   3*dwidth;\
	int i4 = x*3+  (y*3+1)*3*dwidth;\
	int i5 = x*3+1+(y*3+1)*3*dwidth;\
	int i6 = x*3+2+(y*3+1)*3*dwidth;\
	int i7 = x*3+  (y*3+2)*3*dwidth;\
	int i8 = x*3+1+(y*3+2)*3*dwidth;\
	int i9 = x*3+2+(y*3+2)*3*dwidth;\
	dst[i*4+0] = (src[i1*4+0]+src[i2*4+0]+src[i3*4+0]+src[i4*4+0]+src[i5*4+0]+src[i6*4+0]+src[i7*4+0]+src[i8*4+0]+src[i9*4+0])/9.0;\
	dst[i*4+1] = (src[i1*4+1]+src[i2*4+1]+src[i3*4+1]+src[i4*4+1]+src[i5*4+1]+src[i6*4+1]+src[i7*4+1]+src[i8*4+1]+src[i9*4+1])/9.0;\
	dst[i*4+2] = (src[i1*4+2]+src[i2*4+2]+src[i3*4+2]+src[i4*4+2]+src[i5*4+2]+src[i6*4+2]+src[i7*4+2]+src[i8*4+2]+src[i9*4+2])/9.0;\
	dst[i*4+3] = 1.0;\
}\
\
__kernel void applyFilterAAW(__global float * src, __global float * dst, float mpl, int aa, int dwidth, int dheight)\
{\
	const int i = get_global_id (0);\
	int x = i % dwidth;\
	int y = (i - x) / dwidth;\
	float r, g, b;\
	r = g = b = 0.0;\
	int x2 = (int)((x+0.5f)*aa);\
	int y2 = (int)((y+0.5f)*aa);\
	float weights[4];\
	float4 colors[4];\
	\
	int k=0;\
	for (int jj=2-aa; jj<=1; jj++)\
	{\
		int y1 = max(0, min(y2+jj, aa*dheight-1));\
		for (int ii=2-aa; ii<=1; ii++)\
		{\
			int x1 = max(0, min(x2+ii, aa*dwidth-1));\
			colors[k].s0 = src[(x1+y1*dwidth*aa)*4+0];\
			colors[k].s1 = src[(x1+y1*dwidth*aa)*4+1];\
			colors[k].s2 = src[(x1+y1*dwidth*aa)*4+2];\
			k++;\
		}\
	}\
	float sumweights = 0;\
	for (int ii=0; ii<aa*aa; ii++)\
	{\
		weights[ii] = 1.0f/(colors[ii].s0*mpl +0.1f) + 1.0f/(colors[ii].s1*mpl +0.1f) + 1.0f/(colors[ii].s2*mpl +0.1f);\
		sumweights += weights[ii];\
		r += colors[ii].s0 * weights[ii];\
		g += colors[ii].s1 * weights[ii];\
		b += colors[ii].s2 * weights[ii];\
	}\
	r = r/sumweights;\
	g = g/sumweights;\
	b = b/sumweights;\
	dst[i*4+0] = r;\
	dst[i*4+1] = g;\
	dst[i*4+2] = b;\
	dst[i*4+3] = 1.0;\
}\
\
__kernel void applyFilterNone(__global float * src, __global float * dst, int aa, int dwidth, int dheight)\
{\
	const int i = get_global_id (0);\
	int x = i % dwidth;\
	int y = (i - x) / dwidth;\
	int i2 = x*aa+1 + (y*aa+1)*aa*dwidth;\
	dst[i*4+0] = src[i2*4+0];\
	dst[i*4+1] = src[i2*4+1];\
	dst[i*4+2] = src[i2*4+2];\
	dst[i*4+3] = 1.0;\
}\
\
__kernel void applyFilter4W(__global float * src, __global float * dst, float4 m, int aa, int dwidth, int dheight)\
{\
	const int i = get_global_id (0);\
	int x = i % dwidth;\
	int y = (i - x) / dwidth;\
	int ppp = 1;\
	int xx1 = max(0, aa*x-2+ppp);\
	int xx2 = min(dwidth*aa-1, aa*x+1+ppp);\
	int yy1 = max(0, aa*y-2+ppp);\
	int yy2 = min(dheight*aa-1, aa*y+1+ppp);\
	float r = 0.0;\
	float g = 0.0;\
	float b = 0.0;\
	float mm[4];\
	mm[0] = m.x;\
	mm[1] = m.y;\
	mm[2] = m.z;\
	mm[3] = m.w;\
	for (int yy=yy1; yy<=yy2; yy++)\
	{\
		for (int xx=xx1; xx<=xx2; xx++)\
		{\
			int i1 = xx+yy*dwidth*aa;\
			r+= src[i1*4+0] * mm[yy-yy1] * mm[xx-xx1];\
			g+= src[i1*4+1] * mm[yy-yy1] * mm[xx-xx1];\
			b+= src[i1*4+2] * mm[yy-yy1] * mm[xx-xx1];\
		}\
	}\
	dst[i*4+0] = r;\
	dst[i*4+1] = g;\
	dst[i*4+2] = b;\
	dst[i*4+3] = 1.0;\
}\
\
__kernel void applyFilterGaussExtreme(__global float * src, __global float * dst, float8 m, int aa, int dwidth, int dheight)\
{\
	const int i = get_global_id (0);\
	int x = i % dwidth;\
	int y = (i - x) / dwidth;\
	int ppp = 1;\
	int xx1 = max(0, aa*x-3+ppp);\
	int xx2 = min(dwidth*aa-1, aa*x+2+ppp);\
	int yy1 = max(0, aa*y-3+ppp);\
	int yy2 = min(dheight*aa-1, aa*y+2+ppp);\
	float r = 0.0;\
	float g = 0.0;\
	float b = 0.0;\
	float mm[4];\
	mm[0] = m.s0;\
	mm[1] = m.s1;\
	mm[2] = m.s2;\
	mm[3] = m.s3;\
	mm[4] = m.s4;\
	mm[5] = m.s5;\
	for (int yy=yy1; yy<=yy2; yy++)\
	{\
		for (int xx=xx1; xx<=xx2; xx++)\
		{\
			int i1 = xx+yy*dwidth*aa;\
			r+= src[i1*4+0] * mm[yy-yy1] * mm[xx-xx1];\
			g+= src[i1*4+1] * mm[yy-yy1] * mm[xx-xx1];\
			b+= src[i1*4+2] * mm[yy-yy1] * mm[xx-xx1];\
		}\
	}\
	dst[i*4+0] = r;\
	dst[i*4+1] = g;\
	dst[i*4+2] = b;\
	dst[i*4+3] = 1.0;\
}\
\
__kernel void applyResponseFunction(__global float * src, __global float * ltR, __global float * ltG, __global float * ltB, int width, int height)\
{\
	const int i = get_global_id (0);\
	float r = fmin(src[i*4+0] * 1023, 1023);\
	float g = fmin(src[i*4+1] * 1023, 1023);\
	float b = fmin(src[i*4+2] * 1023, 1023);\
	int ir = convert_int4(r);\
	int ig = convert_int4(g);\
	int ib = convert_int4(b);\
	float rr = ltR[ir];\
	float gg = ltG[ig];\
	float bb = ltB[ib];\
	src[i*4+0] = rr;\
	src[i*4+1] = gg;\
	src[i*4+2] = bb;\
}\
\
__kernel void applyBrightnessAndContrast(__global float * src, float bri, float con, int width, int height)\
{\
	const int i = get_global_id (0);\
	float r = src[i*4+0] + bri;\
	float g = src[i*4+1] + bri;\
	float b = src[i*4+2] + bri;\
	src[i*4+0] = (r - 0.5f) * con + 0.5f;\
	src[i*4+1] = (g - 0.5f) * con + 0.5f;\
	src[i*4+2] = (b - 0.5f) * con + 0.5f;\
}\
\
__kernel void applyAddRGB(__global float * src, float addR, float addG, float addB, int width, int height)\
{\
	const int i = get_global_id (0);\
	src[i*4+0] = src[i*4+0] + addR;\
	src[i*4+1] = src[i*4+1] + addG;\
	src[i*4+2] = src[i*4+2] + addB;\
}\
\
__kernel void applySaturation(__global float * src, float sat, int width, int height)\
{\
	const int i = get_global_id (0);\
	float r = src[i*4+0];\
	float g = src[i*4+1];\
	float b = src[i*4+2];\
	\
	float maxcol = fmax(fmax(r,g),b);\
	float mincol = fmin(fmin(r,g),b);\
	float lightness = (maxcol+mincol)*0.5f;\
	float chroma = maxcol - mincol;\
	float saturation = chroma / (1-fabs(2*lightness-1));\
	float hue = 0.0f;\
	if (maxcol==r)\
		 hue = (g-b)/chroma;\
	if (maxcol==g)\
		 hue = 2.0f + (b-r)/chroma;\
	if (maxcol==b)\
		 hue = 4.0f + (r-g)/chroma;\
	if (chroma==0.0f)\
		hue = saturation = 0.0f;\
	hue = hue / 6.0f;\
	\
	saturation = fmin(1.0f, fmax(0.0f, saturation+sat));\
	\
	\
	\
	src[i*4+0] = r;\
	src[i*4+1] = g;\
	src[i*4+2] = b;\
}\
\
__kernel void applyTemperature(__global float * src, float mulR, float mulG, float mulB, int width, int height)\
{\
	const int i = get_global_id (0);\
	src[i*4+0] = src[i*4+0] * mulR;\
	src[i*4+1] = src[i*4+1] * mulG;\
	src[i*4+2] = src[i*4+2] * mulB;\
}\
\
__kernel void makeVignetteLookupTable(__global float * vlookup, float aperture, float vignette)\
{\
	const int i = get_global_id (0);\
	float r_1 = 1.0;\
	float r_A = 1.0/aperture;\
	float val = fabs(vignette);\
	float d2 = (float)i/1000.0 * val/50.0;\
	if (d2 <= r_1+r_A)\
	{\
		float kos1 = (r_A*r_A + d2*d2 - r_1*r_1) / (2*d2*r_A);\
		float kos2 = (r_1*r_1 + d2*d2 - r_A*r_A) / (2*d2*r_1);\
		float t1 = kos1*r_A;\
		float t2 = kos2*r_1;\
		\
		float p1_k = (acos(kos1) * r_A*r_A);\
		float p1_t = t1*sqrt(r_A*r_A - t1*t1);\
		float p1 = p1_k - p1_t;\
		float p2_k = (acos(kos2) * r_1*r_1);\
		float p2_t = t2*sqrt(r_1*r_1 - t2*t2);\
		float p2 = p2_k - p2_t;\
		\
		vlookup[i] = max(0.0f, p1+p2)/M_PI;\
	}\
	else\
		vlookup[i] = 0.0f;\
}\
\
__kernel void applyVignette(__global float * src, __global float * vlookup, int width, int height, int owidth, int oheight, int bposx, int bposy)\
{\
	const int i = get_global_id (0);\
	int x = i % width;\
	int y = (i - x) / width;\
	owidth = max(width+bposx, owidth);\
	oheight = max(height+bposy, oheight);\
	int wym = max(owidth, oheight);\
	float fwym = (float)wym;\
	float yy = (y-oheight/2+bposy) / fwym;\
	float xx = (x-owidth/2+bposx) / fwym;\
	float dist2 = xx*xx + yy*yy;\
	float dist = sqrt(dist2);\
	int dd = convert_int4(2*dist*1000);\
	dd = min(max(dd, 0), 1500);\
	float mpl = vlookup[dd];\
	src[i*4+0] = src[i*4+0] * mpl;\
	src[i*4+1] = src[i*4+1] * mpl;\
	src[i*4+2] = src[i*4+2] * mpl;\
}\
\
__kernel void applyCurveRGB(__global float * src, __global float * curve, int width, int height)\
{\
	const int i = get_global_id (0);\
	float rr = src[i*4+0]*255.0;\
	float gg = src[i*4+1]*255.0;\
	float bb = src[i*4+2]*255.0;\
	int j, k;\
	float r,S;\
	\
	j=0; k=128;\
	while (rr > curve[0+j+1])\
		j++;\
	r = (rr - (float)curve[0+j]);\
	rr = curve[k+0+j] + curve[k+32+j]*r + curve[k+64+j]*r*r + curve[k+96+j]*r*r*r;\
	\
	j=0; k=256;\
	while (gg > curve[32+j+1])\
		j++;\
	r = (gg - (float)curve[32+j]);\
	gg = curve[k+0+j] + curve[k+32+j]*r + curve[k+64+j]*r*r + curve[k+96+j]*r*r*r;\
	\
	j=0; k=384;\
	while (bb > curve[64+j+1])\
		j++;\
	r = (bb - (float)curve[64+j]);\
	bb = curve[k+0+j] + curve[k+32+j]*r + curve[k+64+j]*r*r + curve[k+96+j]*r*r*r;\
	\
	src[i*4+0] = rr/255.0;\
	src[i*4+1] = gg/255.0;\
	src[i*4+2] = bb/255.0;\
}\
\
__kernel void applyCurveLumi(__global float * src, __global float * curve, int width, int height)\
{\
	const int i = get_global_id (0);\
	float rr = src[i*4+0];\
	float gg = src[i*4+1];\
	float bb = src[i*4+2];\
	float gamma = 2.4;\
	float pergamma = 1.0/gamma;\
	\
	if ( rr > 0.04045 )\
		rr = pow( ( rr + 0.055 ) / 1.055 , gamma);\
	else\
		rr = rr / 12.92f;\
	if ( gg > 0.04045 )\
		gg = pow( ( gg + 0.055 ) / 1.055 , gamma);\
	else\
		gg = gg / 12.92f;\
	if ( bb > 0.04045 )\
		bb = pow( ( bb + 0.055 ) / 1.055 , gamma);\
	else\
		bb = bb / 12.92f;\
	float X = rr * 0.4124 + gg * 0.3576 + bb * 0.1805;\
	float Y = rr * 0.2126 + gg * 0.7152 + bb * 0.0722;\
	float Z = rr * 0.0193 + gg * 0.1192 + bb * 0.9505;\
	X *= 255;\
	Y *= 255;\
	Z *= 255;\
	float x = X/(X+Y+Z);\
	float y = Y/(X+Y+Z);\
	\
	int j, k;\
	float r,S;\
	j=0;\
	while (Y > curve[96+j+1])\
		j++;\
	r = (Y - (float)curve[96+j]);\
	Y = curve[512+0+j] + curve[512+32+j]*r + curve[512+64+j]*r*r + curve[512+96+j]*r*r*r;\
	\
	X = x*(Y/y);\
	Z = (1-x-y)*(Y/y);\
	X *= 1.0f/255;\
	Y *= 1.0f/255;\
	Z *= 1.0f/255;\
	\
	rr = X *  3.2406 + Y * -1.5372 + Z * -0.4986;\
	gg = X * -0.9689 + Y *  1.8758 + Z *  0.0415;\
	bb = X *  0.0557 + Y * -0.2040 + Z *  1.0570;\
	\
	if ( rr > 0.0031308f )\
		rr = 1.055f * pow( rr , ( pergamma ) ) - 0.055f;\
	else\
		rr = 12.92f * rr;\
	if ( gg > 0.0031308f )\
		gg = 1.055f * pow( gg , ( pergamma ) ) - 0.055f;\
	else\
		gg = 12.92f * gg;\
	if ( bb > 0.0031308f )\
		bb = 1.055f * pow( bb , ( pergamma ) ) - 0.055f;\
	else\
		bb = 12.92f * bb;\
	src[i*4+0] = rr;\
	src[i*4+1] = gg;\
	src[i*4+2] = bb;\
}\
\
__kernel void zeroIntBuffer(__global int * buf)\
{\
	const int i = get_global_id (0);\
	buf[i] = 0;\
}\
\
__kernel void dotsCountNeighbours(__global float * src, __global int * ngb, int radius, float thres, int width, int height)\
{\
	const int i = get_global_id (0);\
	float rr = src[i*4+0];\
	float gg = src[i*4+1];\
	float bb = src[i*4+2];\
	int x = i % width;\
	int y = (i - x) / width;\
	int x1 = max(x-radius, 0);\
	int x2 = min(width-1, x+radius);\
	int y1 = max(y-radius, 0);\
	int y2 = min(height-1, y+radius);\
	\
	int cntr = 0;\
	for (int yy=y1; yy<=y2; yy++)\
	{\
		for (int xx=x1; xx<=x2; xx++)\
		{\
			float r2 = src[4*(xx+yy*width)+0];\
			float g2 = src[4*(xx+yy*width)+1];\
			float b2 = src[4*(xx+yy*width)+2];\
			if (		rr > r2 * thres	||\
						gg > g2 * thres	||\
						bb > b2 * thres	)\
				cntr++;\
		}\
	}\
	ngb[i] = cntr;\
}\
\
__kernel void dotsMapBurned(__global float * src, __global int * ngb, int area, int maxposs, int width, int height)\
{\
	const int i = get_global_id (0);\
	int cntr = ngb[i];\
	int i2 = i-1;\
	while (cntr>area-2-maxposs  &&  i2>0  &&  i2>i-16)\
	{\
		cntr = ngb[i2];\
		float r = src[i*4-4];\
		float g = src[i*4-3];\
		float b = src[i*4-2];\
		src[i*4+0] = src[i2*4+0];\
		src[i*4+1] = src[i2*4+1];\
		src[i*4+2] = src[i2*4+2];\
		i2--;\
	}\
}\
\
__kernel void applyGrain(__global float * src, __global ushort * rnd, float amount, int width, int height)\
{\
	int i = get_global_id(0);\
	uint rand = rnd[i];\
	float f = (float)rand/(float)(0xffff);\
	float p = 2 * f - 1;\
	p *= amount;\
	p += 1.0;\
	src[i*4+0] *= p;\
	src[i*4+1] *= p;\
	src[i*4+2] *= p;\
}\
\
__kernel void applyChromaticAbbPlanar(__global float * src, __global float * dst, float chrRed, float chrGreen, float chrBlue, int width, int height)\
{\
	int i = get_global_id(0);\
	float cx = (float)width/2.0f;\
	float cy = (float)height/2.0f;\
	int x = i % width;\
	int y = (i - x) / width;\
	\
	float ovx = x-cx;\
	float ovy = y-cy;\
	float dxr = ovx*chrRed+cx;\
	float dyr = ovy*chrRed+cy;\
	float dxg = ovx*chrGreen+cx;\
	float dyg = ovy*chrGreen+cy;\
	float dxb = ovx*chrBlue+cx;\
	float dyb = ovy*chrBlue+cy;\
	float fdxr = floor(dxr);\
	float fdyr = floor(dyr);\
	float fdxg = floor(dxg);\
	float fdyg = floor(dyg);\
	float fdxb = floor(dxb);\
	float fdyb = floor(dyb);\
	int idxr = (int)fdxr;\
	int idyr = (int)fdyr;\
	int idxg = (int)fdxg;\
	int idyg = (int)fdyg;\
	int idxb = (int)fdxb;\
	int idyb = (int)fdyb;\
	float wxr = dxr-fdxr;\
	float wyr = dyr-fdyr;\
	float wxg = dxg-fdxg;\
	float wyg = dyg-fdyg;\
	float wxb = dxb-fdxb;\
	float wyb = dyb-fdyb;\
	int idxr2 = min(idxr+1, width-1);\
	int idyr2 = min(idyr+1, height-1);\
	int idxg2 = min(idxg+1, width-1);\
	int idyg2 = min(idyg+1, height-1);\
	int idxb2 = min(idxb+1, width-1);\
	int idyb2 = min(idyb+1, height-1);\
	int ir00 = 4*(idxr +idyr *width);\
	int ir01 = 4*(idxr2+idyr *width);\
	int ir10 = 4*(idxr +idyr2*width);\
	int ir11 = 4*(idxr2+idyr2*width);\
	int ig00 = 4*(idxg +idyg *width);\
	int ig01 = 4*(idxg2+idyg *width);\
	int ig10 = 4*(idxg +idyg2*width);\
	int ig11 = 4*(idxg2+idyg2*width);\
	int ib00 = 4*(idxb +idyb *width);\
	int ib01 = 4*(idxb2+idyb *width);\
	int ib10 = 4*(idxb +idyb2*width);\
	int ib11 = 4*(idxb2+idyb2*width);\
	float rr =	(1.0-wxr)*(1.0-wyr) * (src[ir00+0]) + \
				(1.0-wxr)*(    wyr) * (src[ir10+0]) + \
				(    wxr)*(1.0-wyr) * (src[ir01+0]) + \
				(    wxr)*(    wyr) * (src[ir11+0]);\
	float gg =	(1.0-wxg)*(1.0-wyg) * (src[ig00+1]) + \
				(1.0-wxg)*(    wyg) * (src[ig10+1]) + \
				(    wxg)*(1.0-wyg) * (src[ig01+1]) + \
				(    wxg)*(    wyg) * (src[ig11+1]);\
	float bb =	(1.0-wxb)*(1.0-wyb) * (src[ib00+2]) + \
				(1.0-wxb)*(    wyb) * (src[ib10+2]) + \
				(    wxb)*(1.0-wyb) * (src[ib01+2]) + \
				(    wxb)*(    wyb) * (src[ib11+2]);\
	dst[i*4+0] = rr;\
	dst[i*4+1] = gg;\
	dst[i*4+2] = bb;\
}\
\
__kernel void applyFog(__global float * src, __global float * depthmap, float dist, float4 att, float speed, int exclSky, int width, int height)\
{\
	const int i = get_global_id (0);\
	float rr = src[i*4+0];\
	float gg = src[i*4+1];\
	float bb = src[i*4+2];\
	float dd = depthmap[i];\
	\
	float skydist = 1.0e30;\
	if (dd==1.0e30 && exclSky>0)\
		dd = 0.0;\
	dd = max(0.0, min(dd, dist));\
	float ll = dd/dist;\
	ll = pow(ll, speed);\
	rr = att.x*ll + rr*(1.0-ll);\
	gg = att.y*ll + gg*(1.0-ll);\
	bb = att.z*ll + bb*(1.0-ll);\
	src[i*4+0] = rr;\
	src[i*4+1] = gg;\
	src[i*4+2] = bb;\
}\
\
";
