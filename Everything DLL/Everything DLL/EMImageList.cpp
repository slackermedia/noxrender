
#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;

//------------------------------------------------------------------------------------------------------------------

void InitEMImageList()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMImageList";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMImageListProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = CS_DBLCLKS;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMImageList *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//------------------------------------------------------------------------------------------------------------------

EMImageList * GetEMImageListInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMImageList * emil = (EMImageList *)GetWindowLongPtr(hwnd, 0);
	#else
		EMImageList * emil = (EMImageList *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emil;
}

//------------------------------------------------------------------------------------------------------------------

void SetEMImageListInstance(HWND hwnd, EMImageList *emil)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emil);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emil);
	#endif
}

//------------------------------------------------------------------------------------------------------------------

EMImageList::EMImageList(HWND hWnd):
	imgBuffers(0),
	imgNames1(0),
	imgNames2(0)
{
	hwnd = hWnd;
	lastFocusHWND = 0;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);


	img_width = 128;
	img_height = 128;
	img_cols = 4;
	img_rows = 4;
	img_grid_x = 192;
	img_grid_y = 192;
	scrollGrid = 192;
	text_horiz_margin = 2;
	text_upper_margin = 4;
	corner_round = 16;
	start_x = 0;
	start_y = 0;
	imageSelected = -1;
	imageMouseOver = -1;

	drawImageBorders = true;
	drawControlBorder = true;
	alignToGrid = false;
	scrollVertical = true;
	showNames1 = true;
	showNames2 = true;


	colBg = RGB(64,64,64);
	colBgMouseOver = RGB(72,72,72);
	colBgSelected = RGB(80,80,80);
	colControlBorderLeftUp = RGB(48,48,48);
	colControlBorderRightDown = RGB(80,80,80);
	colImgBorderLeftUp = RGB(96,96,96);
	colImgBorderRightDown = RGB(96,96,96);
	colImgBorderSelectedLeftUp = RGB(112,112,112);
	colImgBorderSelectedRightDown = RGB(112,112,112);

	colText1 = RGB(224,224,224);
	colText2 = RGB(192,192,192);
	colText1Selected = RGB(255,255,255);
	colText2Selected = RGB(240,240,240);
	colDisabledBg = RGB(64,64,64);
	colDisabledControlBorderLeftUp = RGB(56,56,56);
	colDisabledControlBorderRightDown = RGB(72,72,72);
	colDisabledImgBorderLeftUp = RGB(72,72,72);
	colDisabledImgBorderRightDown = RGB(72,72,72);
	colDisabledText1 = RGB(160,160,160);
	colDisabledText2 = RGB(160,160,160);
}

//------------------------------------------------------------------------------------------------------------------

EMImageList::~EMImageList()
{
	if  (hFont)
		DeleteObject(hFont);

	for (int i=0; i<imgBuffers.objCount; i++)
	{
		if (imgBuffers[i] != NULL)
		{
			free(imgBuffers[i]);
			imgBuffers[i] = NULL;
		}
	}
	for (int i=0; i<imgNames1.objCount; i++)
	{
		if (imgNames1[i] != NULL)
		{
			free(imgNames1[i]);
			imgNames1[i] = NULL;
		}
	}
	for (int i=0; i<imgNames2.objCount; i++)
	{
		if (imgNames2[i] != NULL)
		{
			free(imgNames2[i]);
			imgNames2[i] = NULL;
		}
	}
}

//------------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK EMImageListProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMImageList * emil;
	emil = GetEMImageListInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	static int cmx, cmy, cpos;

	switch (msg)
	{
		case WM_CREATE:
			{
				emil = new EMImageList(hwnd);
				SetEMImageListInstance(hwnd, emil);
			}
			break;
		case WM_DESTROY:
			{
				delete emil;
				SetEMImageListInstance(hwnd, 0);
			}
			break;
		case WM_SIZE:
			{
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_PAINT:
			{
				DWORD style;
				style = GetWindowLong(hwnd, GWL_STYLE);
				bool disabled = ((style & WS_DISABLED) > 0);

				hdc = BeginPaint(hwnd, &ps);

				emil->drawControl(hdc, disabled);

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_MOUSEMOVE:
			{
				RECT crect;
				GetClientRect(hwnd, &crect);

				SetCapture(hwnd);

				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);

				int oldMouseOver = emil->imageMouseOver;
				int newMouseOver = emil->findTileMouseOver(px);
				emil->imageMouseOver = newMouseOver;

				if (newMouseOver != oldMouseOver)
				{
					InvalidateRect(emil->hwnd, NULL, false);
					emil->notifyParentMouseOver();
				}

				if (!PtInRect(&crect, px))
					ReleaseCapture();
			}
			break;
		case WM_LBUTTONDBLCLK:
			{
				emil->notifyParentDblClick();
			}
			break;
		case WM_LBUTTONDOWN:
			{
				SetFocus(hwnd);

				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);

				int oldSelected = emil->imageSelected;
				int newSelected = emil->findTileMouseOver(px);
				emil->imageSelected = newSelected;

				if (oldSelected != newSelected)
				{
					InvalidateRect(emil->hwnd, NULL, false);
					emil->notifyParentChangeSelected();
				}
			}
			break;
		case WM_MOUSEWHEEL:
			{
				RECT crect;
				GetClientRect(hwnd, &crect);

				int oldMouseOver = emil->imageMouseOver;

				DWORD wwwp = (DWORD)wParam;
				int zDelta = (int)((short)HIWORD(wParam));
				if (zDelta>0)
				{
					if (emil->scrollVertical)
					{
						emil->start_y = max(0, emil->start_y-50);
					}
					else
					{
						emil->start_x = max(0, emil->start_x-50);
					}
				}
				else
				{
					if (emil->scrollVertical)
					{
						int numEntries = max(emil->imgBuffers.objCount, max(emil->imgNames1.objCount, emil->imgNames2.objCount));
						int rows = (numEntries+emil->img_cols-1)/emil->img_cols;
						int hh = max(0, emil->img_grid_y*rows-crect.bottom);
						emil->start_y = min(hh, emil->start_y+50);
					}
					else
					{
						int numEntries = max(emil->imgBuffers.objCount, max(emil->imgNames1.objCount, emil->imgNames2.objCount));
						int cols = (numEntries+emil->img_rows-1)/emil->img_rows;
						int hh = max(0, emil->img_grid_x*cols-crect.right);
						emil->start_x = min(hh, emil->start_x+50);
					}
				}

				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);
				ScreenToClient(emil->hwnd, &px);
				emil->imageMouseOver = emil->findTileMouseOver(px);
				InvalidateRect(hwnd, NULL, false);
				if (emil->imageMouseOver != oldMouseOver)
					emil->notifyParentMouseOver();
				emil->notifyParentChangePos();

			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//------------------------------------------------------------------------------------------------------------------

bool EMImageList::drawControl(HDC hdc, bool disabled)
{
	// CREATE TEMP HDC
	RECT crect;
	GetClientRect(hwnd, &crect);
	HDC thdc = CreateCompatibleDC(hdc); 
	HBITMAP hbmp = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
	HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);
	HBRUSH tmpBrsh = CreateSolidBrush(RGB(0,0,0));
	HPEN tmpPen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
	HBRUSH oldBrush = (HBRUSH)SelectObject(thdc, tmpBrsh);
	HPEN oldPen = (HPEN)SelectObject(thdc, tmpPen);

	// CREATE PENS AND BRUSHES
	HBRUSH hBrBackground=0, hBrBkgMouseOver=0, hBrBkgSelected=0, hBrNoImage=0;
	HPEN hPenBackground=0, hPenBkgMouseOver=0, hPenBkgSelected=0, hPenControlBorderLeftUp=0, hPenControlBorderRightDown=0,
		hPenImgBorderLeftUp=0, hPenImgBorderRightDown=0, hPenImgBorderSelectedLeftUp=0, hPenImgBorderSelectedRightDown=0,
		hPenText1, hPenText2=0, hPenText1Selected=0, hPenText2Selected=0;

	if (disabled)
	{
		hBrBackground	= CreateSolidBrush(colDisabledBg);
		hBrBkgMouseOver	= CreateSolidBrush(colDisabledBg);
		hBrBkgSelected	= CreateSolidBrush(colDisabledBg);
		hBrNoImage		= CreateSolidBrush(RGB(0,0,0));
		hPenBackground						= CreatePen(PS_SOLID, 1, colDisabledBg);
		hPenBkgMouseOver					= CreatePen(PS_SOLID, 1, colDisabledBg);
		hPenBkgSelected						= CreatePen(PS_SOLID, 1, colDisabledBg);
		hPenControlBorderLeftUp				= CreatePen(PS_SOLID, 1, colDisabledControlBorderLeftUp);
		hPenControlBorderRightDown			= CreatePen(PS_SOLID, 1, colDisabledControlBorderRightDown);
		hPenImgBorderLeftUp					= CreatePen(PS_SOLID, 1, colDisabledImgBorderLeftUp);
		hPenImgBorderRightDown				= CreatePen(PS_SOLID, 1, colDisabledImgBorderRightDown);
		hPenImgBorderSelectedLeftUp			= CreatePen(PS_SOLID, 1, colDisabledImgBorderLeftUp);
		hPenImgBorderSelectedRightDown		= CreatePen(PS_SOLID, 1, colDisabledImgBorderRightDown);
		hPenText1							= CreatePen(PS_SOLID, 1, colDisabledText1);
		hPenText2							= CreatePen(PS_SOLID, 1, colDisabledText2);
		hPenText1Selected					= CreatePen(PS_SOLID, 1, colDisabledText1);
		hPenText2Selected					= CreatePen(PS_SOLID, 1, colDisabledText2);
	}
	else
	{
		hBrBackground	= CreateSolidBrush(colBg);
		hBrBkgMouseOver	= CreateSolidBrush(colBgMouseOver);
		hBrBkgSelected	= CreateSolidBrush(colBgSelected);
		hBrNoImage		= CreateSolidBrush(RGB(0,0,0));
		hPenBackground						= CreatePen(PS_SOLID, 1, colBg);
		hPenBkgMouseOver					= CreatePen(PS_SOLID, 1, colBgMouseOver);
		hPenBkgSelected						= CreatePen(PS_SOLID, 1, colBgSelected);
		hPenControlBorderLeftUp				= CreatePen(PS_SOLID, 1, colControlBorderLeftUp);
		hPenControlBorderRightDown			= CreatePen(PS_SOLID, 1, colControlBorderRightDown);
		hPenImgBorderLeftUp					= CreatePen(PS_SOLID, 1, colImgBorderLeftUp);
		hPenImgBorderRightDown				= CreatePen(PS_SOLID, 1, colImgBorderRightDown);
		hPenImgBorderSelectedLeftUp			= CreatePen(PS_SOLID, 1, colImgBorderSelectedLeftUp);
		hPenImgBorderSelectedRightDown		= CreatePen(PS_SOLID, 1, colImgBorderSelectedRightDown);
		hPenText1							= CreatePen(PS_SOLID, 1, colText1);
		hPenText2							= CreatePen(PS_SOLID, 1, colText2);
		hPenText1Selected					= CreatePen(PS_SOLID, 1, colText1Selected);
		hPenText2Selected					= CreatePen(PS_SOLID, 1, colText2Selected);
	}


	int numEntries = max(imgBuffers.objCount, max(imgNames1.objCount, imgNames2.objCount));

	// fill control
	SelectObject(thdc, hBrBackground);
	SelectObject(thdc, hPenBackground);
	DeleteObject(tmpBrsh);
	DeleteObject(tmpPen);
	Rectangle(thdc, crect.left, crect.top, crect.right, crect.bottom);


	// draw all tiles
	int grid_start_x = max(0, start_x / img_grid_x);
	int grid_start_y = max(0, start_y / img_grid_y);
	int grid_stop_x = (start_x + crect.right)  / img_grid_x;
	int grid_stop_y = (start_y + crect.bottom) / img_grid_y;
	
	if (scrollVertical)
		grid_stop_x = min(grid_stop_x, img_cols-1);
	else
		grid_stop_y = min(grid_stop_y, img_rows-1);

	int nn = 0;
	if (showNames1)
		nn++;
	if (showNames2)
		nn++;
	int bn = 0;
	int pb = 0;
	if (drawImageBorders)
	{
		bn = 2;
		pb = 1;
	}
	int odst = max(0, img_grid_y - bn -img_height - nn*(12+text_upper_margin))/2;

	for (int gy=grid_start_y; gy<=grid_stop_y; gy++)
	{
		for (int gx=grid_start_x; gx<=grid_stop_x; gx++)
		{
			int current = gy * img_cols + gx;
			if (current >= numEntries)
				continue;

			int sx = gx*img_grid_x-start_x;
			int sy = gy*img_grid_y-start_y;
			bool tSelected = (current == imageSelected);
			bool tMouseOver = (current == imageMouseOver) && !tSelected;

			if (tMouseOver)
			{
				SelectObject(thdc, hBrBkgMouseOver);
				SelectObject(thdc, hPenBkgMouseOver);
				RoundRect(thdc, sx, sy, sx+img_grid_x, sy+img_grid_y, corner_round, corner_round);
			}
			if (tSelected)
			{
				SelectObject(thdc, hBrBkgSelected);
				SelectObject(thdc, hPenBkgSelected);
				RoundRect(thdc, sx, sy, sx+img_grid_x, sy+img_grid_y, corner_round, corner_round);
			}

			// draw image
			COLORREF * bufptr = imgBuffers[current];
			if (bufptr != NULL)
			{
				int ix = sx+(img_grid_x-img_width)/2;
				int iy = sy+odst+pb;
				HDC bHdc = CreateCompatibleDC(thdc);
				
				HBITMAP cBmp = CreateCompatibleBitmap(thdc, img_width, img_height);
				HBITMAP oldBmp = (HBITMAP)SelectObject(bHdc, cBmp);

				BITMAPINFOHEADER bmih;
				ZeroMemory(&bmih,sizeof(bmih));
				bmih.biSize = sizeof(BITMAPINFOHEADER);
				bmih.biBitCount = 32;
				bmih.biClrImportant = 0;
				bmih.biClrUsed = 0;
				bmih.biCompression = BI_RGB;
				bmih.biHeight = img_height;
				bmih.biWidth = img_width;
				bmih.biPlanes = 1;
				bmih.biSizeImage = img_width*img_height;
				bmih.biXPelsPerMeter = 0;
				bmih.biYPelsPerMeter = 0;
				BITMAPINFO bmi;
				bmi.bmiHeader = bmih;

				int ccc = SetDIBits(bHdc, cBmp, 0, img_height, (void*)bufptr, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);

				BitBlt(thdc, ix, iy, img_width, img_height, bHdc, 0, 0, SRCCOPY);
				SelectObject(bHdc, oldBmp);
				DeleteObject(cBmp);
				DeleteDC(bHdc);
			}
			else
			{
				SelectObject(thdc, hBrNoImage);
				int ix = sx+(img_grid_x-img_width)/2;
				int iy = sy+odst+pb;
				Rectangle(thdc, ix, iy, ix+img_width, iy+img_height);
			}

			// draw image borders
			if (drawImageBorders)
			{
				int ix = sx+(img_grid_x-img_width)/2;
				int iy = sy+odst;

				POINT ibLeftUp[]	= { {ix-1, iy+img_height+1}, {ix-1, iy}, {ix+img_width+1,iy} };
				POINT ibRightDown[] = { {ix+img_width, iy+1}, {ix+img_width, iy+img_height+1},  {ix-1, iy+img_height+1} };
				if (tSelected)
					SelectObject(thdc, hPenImgBorderSelectedLeftUp);
				else
					SelectObject(thdc, hPenImgBorderLeftUp);
				Polyline(thdc, ibLeftUp, 3);
				if (tSelected)
					SelectObject(thdc, hPenImgBorderSelectedRightDown);
				else
					SelectObject(thdc, hPenImgBorderRightDown);
				Polyline(thdc, ibRightDown, 3);
			}

			// draw text 1
			char * name1 = imgNames1[current];
			if (showNames1  &&  name1)
			{
				RECT nrect;
				SIZE sz;
				HFONT hOldFont = (HFONT)SelectObject(thdc, hFont);
				SetTextColor(thdc, colText1);
				SetBkColor  (thdc, colBg);
				if (tMouseOver)
					SetBkColor  (thdc, colBgMouseOver);
				if (tSelected)
					SetBkColor  (thdc, colBgSelected);
				int ln = (int)strlen(name1);
				GetTextExtentPoint32(thdc, name1, ln, &sz);
				int posx = sx + max(text_horiz_margin, (img_grid_x-sz.cx)/2);
				int posy = sy + img_height + odst + bn + text_upper_margin;
				nrect.left = posx;
				nrect.right = posx+min(sz.cx, img_grid_x-2*text_horiz_margin);
				nrect.top = posy;
				nrect.bottom = posy+sz.cy;
				ExtTextOut(thdc, posx, posy, ETO_OPAQUE|ETO_CLIPPED, &nrect, name1, ln, 0);
				SelectObject(thdc, hOldFont);
			}

			// draw text 2
			char * name2 = imgNames2[current];
			if (showNames2  &&  name2)
			{
				RECT nrect;
				SIZE sz;
				HFONT hOldFont = (HFONT)SelectObject(thdc, hFont);
				SetTextColor(thdc, colText2);
				SetBkColor  (thdc, colBg);
				if (tMouseOver)
					SetBkColor  (thdc, colBgMouseOver);
				if (tSelected)
					SetBkColor  (thdc, colBgSelected);
				int ln = (int)strlen(name2);
				GetTextExtentPoint32(thdc, name2, ln, &sz);
				int posx = sx + max(text_horiz_margin, (img_grid_x-sz.cx)/2);
				int posy = sy + img_height + 2*odst + bn;
				if (showNames1)
					posy += odst+12 + text_upper_margin;
				nrect.left = posx;
				nrect.right = posx+min(sz.cx, img_grid_x-2*text_horiz_margin);
				nrect.top = posy;
				nrect.bottom = posy+sz.cy;
				ExtTextOut(thdc, posx, posy, ETO_OPAQUE, &nrect, name2, ln, 0);
				SelectObject(thdc, hOldFont);
			}
		}
	}

	// draw control borders
	if (drawControlBorder)
	{
		POINT cbLeftUp[]	= { {0, crect.bottom-2}, {0,0}, {crect.right-1,0} };
		POINT cbRightDown[] = { {crect.right-1, 0}, {crect.right-1, crect.bottom-1}, {-1,crect.bottom-1} };
		SelectObject(thdc, hPenControlBorderLeftUp);
		Polyline(thdc, cbLeftUp, 3);
		SelectObject(thdc, hPenControlBorderRightDown);
		Polyline(thdc, cbRightDown, 3);
	}

	// GET BACK TO ORIGINAL BRUSH AND PEN
	SelectObject(thdc, oldBrush);
	SelectObject(thdc, oldPen);

	// DELETE PENS AND BRUSHES
	DeleteObject(hBrBackground);
	DeleteObject(hBrBkgMouseOver);
	DeleteObject(hBrBkgSelected);
	DeleteObject(hBrNoImage);
	DeleteObject(hPenBackground);
	DeleteObject(hPenBkgMouseOver);
	DeleteObject(hPenBkgSelected);
	DeleteObject(hPenControlBorderLeftUp);
	DeleteObject(hPenControlBorderRightDown);
	DeleteObject(hPenImgBorderLeftUp);
	DeleteObject(hPenImgBorderRightDown);
	DeleteObject(hPenImgBorderSelectedLeftUp);
	DeleteObject(hPenImgBorderSelectedRightDown);
	DeleteObject(hPenText1);
	DeleteObject(hPenText2);
	DeleteObject(hPenText1Selected);
	DeleteObject(hPenText2Selected);

	// COPY TO ORIGINAL HDC
	GetClientRect(hwnd, &crect);
	BitBlt(hdc, 0, 0, crect.right, crect.bottom, thdc, 0, 0, SRCCOPY);
	SelectObject(thdc, oldBMP);
	DeleteObject(hbmp);
	DeleteDC(thdc);
	return true;
}

//------------------------------------------------------------------------------------------------------------------

int EMImageList::findTileMouseOver(POINT mpt)
{
	RECT crect;
	GetClientRect(hwnd, &crect);

	if (!PtInRect(&crect, mpt))
		return -1;

	int numEntries = max(imgBuffers.objCount, max(imgNames1.objCount, imgNames2.objCount));

	int tx = (mpt.x+start_x) / img_grid_x;
	int ty = (mpt.y+start_y) / img_grid_y;

	int res = img_cols * ty + tx;

	if (scrollVertical)
	{
		if (tx >= img_cols)
			return -1;
	}
	else
	{
		if (ty >= img_rows)
			return -1;
	}

	if (res >= numEntries)
		return -1;

	return res;
}

//------------------------------------------------------------------------------------------------------------------

bool EMImageList::notifyParentChangePos()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, IL_POS_CHANGED), 
			(LPARAM)hwnd);

	return true;
}

//------------------------------------------------------------------------------------------------------------------

bool EMImageList::notifyParentChangeSelected()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, IL_SELECTION_CHANGED), 
			(LPARAM)hwnd);

	return true;
}

//------------------------------------------------------------------------------------------------------------------

bool EMImageList::notifyParentDblClick()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, IL_DOUBLECLICK), 
			(LPARAM)hwnd);

	return true;
}

//------------------------------------------------------------------------------------------------------------------

bool EMImageList::notifyParentMouseOver()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, IL_MOUSEOVER_CHANGED), 
			(LPARAM)hwnd);

	return true;
}

//------------------------------------------------------------------------------------------------------------------

int EMImageList::getWorkspaceLength()
{
	int numEntries = max(imgBuffers.objCount, max(imgNames1.objCount, imgNames2.objCount));

	if (scrollVertical)
	{
		int cc = max(1, img_cols);
		int numRows = (numEntries+cc-1)/cc;
		return numRows * img_grid_y;
	}
	else
	{
		int rr = max(1, img_rows);
		int numCols = (numEntries+rr-1)/rr;
		return numCols * img_grid_x;
	}

	return 0;
}

//------------------------------------------------------------------------------------------------------------------
	
void EMImageList::verifyPosition()
{
	int wL = getWorkspaceLength();
	RECT crect;
	GetClientRect(hwnd, &crect);

	if (scrollVertical)
		start_y = max(0, min(wL-crect.bottom , start_y));
	else
		start_x = max(0, min(wL-crect.right, start_x));
}

//------------------------------------------------------------------------------------------------------------------

