#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "EM2Controls.h"
#include "noxfonts.h"

// new gui version

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
bool initTemperature2ControlPositions(HWND hWnd);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
bool updateTemperature2ControlBackgrounds(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK TemperatureDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;
	static bool changeOthers = true;
	static int mode_left = 1;
	static ColorHSV guiHSV;

	static unsigned int origtemperature = 6000;
	static unsigned int curtemperature = 6000;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = 360 + wrect.right-wrect.left-crect.right;
				tmprect.bottom = 152 + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);
	
				RECT rect;
				GetClientRect(hWnd, &rect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(rect.right, rect.bottom, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();

				origtemperature = curtemperature = (unsigned int)lParam;


				initTemperature2ControlPositions(hWnd);
				updateTemperature2ControlBackgrounds(hWnd);

				EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEMP2_TITLE));
				emtTitle->align = EM2Text::ALIGN_CENTER;
				emtTitle->bgImage = hBgBMP;
				emtTitle->setFont(fonts->em2paneltitle, false);
				emtTitle->colText = NGCOL_LIGHT_GRAY;

				EM2Text * emtKelvin = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEMP2_TEXT_K));
				emtKelvin->bgImage = hBgBMP;
				emtKelvin->setFont(fonts->em2text, false);
				EM2Text * emtPreset = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEMP2_TEXT_PRESET));
				emtPreset->bgImage = hBgBMP;
				emtPreset->setFont(fonts->em2text, false);

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEMP2_OK));
				embOK->bgImage = hBgBMP;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEMP2_CANCEL));
				embCancel->bgImage = hBgBMP;
				embCancel->setFont(fonts->em2button, false);

				EM2TrackBar * emtTemp = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_TEMP2_TRACKBAR));
				emtTemp->createTemperatureGradient(1000,10000);
				emtTemp->bgImage = hBgBMP;
				emtTemp->setValuesToInt(curtemperature, 1000, 10000, origtemperature);

				EM2EditSpin * emesTemp = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_TEMP2_SPIN));
				emesTemp->bgImage = hBgBMP;
				emesTemp->setFont(fonts->em2button, false);
				emesTemp->setValuesToInt(curtemperature, 1000, 10000, origtemperature, 100, 10.0f);

				EM2ColorShow * emcs = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_TEMP2_COLOR));
				emcs->bgImage = hBgBMP;
				emcs->showShadow = false;
				emcs->showBorder = true;
				emcs->setDeniedDragDrop();
				emcs->showMouseOverOrClick = false;
				emcs->color = TemperatureColor::getGammaColor(origtemperature);

				EM2ComboBox * emcPreset = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_TEMP2_PRESET));
				emcPreset->bgImage = hBgBMP;
				emcPreset->align = EM2ComboBox::ALIGN_RIGHT;
				emcPreset->setFontMain(fonts->em2combomain, false);
				emcPreset->setFontUnwrapped(fonts->em2combounwrap, false);
				emcPreset->rowWidth = 50;
				emcPreset->setNumItems(4);
				emcPreset->maxShownRows = 4;
				emcPreset->setItem(0, "D50", true);
				emcPreset->setItem(1, "D55", true);
				emcPreset->setItem(2, "D65", true);
				emcPreset->setItem(3, "D75", true);
				emcPreset->setHeader("PICK");
				emcPreset->selected = -1;

			}	// end WM_INITDIALOG
			break;
		case WM_CLOSE:
			{
				SendMessage(hWnd, WM_COMMAND, MAKELONG(IDC_TEMP2_CANCEL, BN_CLICKED), (LPARAM)GetDlgItem(hWnd, IDC_TEMP2_CANCEL));
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;

			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_TEMP2_OK:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)(&curtemperature)); 
							}
						}
						break;
					case IDC_TEMP2_CANCEL:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)NULL);
							}
						}
						break;
					case IDC_TEMP2_TRACKBAR:
						{
							CHECK(wmEvent==WM_HSCROLL);
							EM2TrackBar * emtrTemp = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_TEMP2_TRACKBAR));
							EM2EditSpin * emesTemp = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_TEMP2_SPIN));
							EM2ColorShow * emcsCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_TEMP2_COLOR));
							CHECK(emtrTemp);
							CHECK(emesTemp);
							CHECK(emcsCol);
							curtemperature = emtrTemp->intValue;
							emesTemp->setIntValue(curtemperature);
							Color4 col = TemperatureColor::getGammaColor(curtemperature);
							emcsCol->redraw(col);
						}
						break;
					case IDC_TEMP2_SPIN:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2TrackBar * emtrTemp = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_TEMP2_TRACKBAR));
							EM2EditSpin * emesTemp = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_TEMP2_SPIN));
							EM2ColorShow * emcsCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_TEMP2_COLOR));
							CHECK(emtrTemp);
							CHECK(emesTemp);
							CHECK(emcsCol);
							curtemperature = emesTemp->intValue;
							emtrTemp->setIntValue(curtemperature);
							Color4 col = TemperatureColor::getGammaColor(curtemperature);
							emcsCol->redraw(col);

						}
						break;
					case IDC_TEMP2_PRESET:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EM2TrackBar * emtrTemp = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_TEMP2_TRACKBAR));
							EM2EditSpin * emesTemp = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_TEMP2_SPIN));
							EM2ColorShow * emcsCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_TEMP2_COLOR));
							EM2ComboBox * emcPreset = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_TEMP2_PRESET));
							CHECK(emtrTemp);
							CHECK(emesTemp);
							CHECK(emcsCol);
							CHECK(emcPreset);

							switch(emcPreset->selected)
							{
								case 0: curtemperature = 5000;	break;
								case 1: curtemperature = 5500;	break;
								case 2: curtemperature = 6500;	break;
								case 3: curtemperature = 7500;	break;
								default: return false;
							}
							emtrTemp->setIntValue(curtemperature);
							emesTemp->setIntValue(curtemperature);
							Color4 col = TemperatureColor::getGammaColor(curtemperature);
							emcsCol->redraw(col);
							emcPreset->selected = -1;
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool initTemperature2ControlPositions(HWND hWnd)
{
	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_TITLE),			HWND_TOP,	50, 10,		260, 20, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_TRACKBAR),		HWND_TOP,	12, 39,		336, 29, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_COLOR),			HWND_TOP,	12, 77,		74,  20, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_SPIN),			HWND_TOP,	94, 77,		68,  20, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_TEXT_K),		HWND_TOP,	168, 80,	20,  14, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_TEXT_PRESET),	HWND_TOP,	235, 80,	70,  14, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_PRESET),		HWND_TOP,	298, 76,	49,  22, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_OK),			HWND_TOP,	104, 115,	72,  22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEMP2_CANCEL),		HWND_TOP,	185, 115,	72,  22, 0);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateTemperature2ControlBackgrounds(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEMP2_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2Text * emtK = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEMP2_TEXT_K));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_TEXT_K), 0, 0, emtK->bgShiftX, emtK->bgShiftY);
	EM2Text * emtPreset = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEMP2_TEXT_PRESET));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_TEXT_PRESET), 0, 0, emtPreset->bgShiftX, emtPreset->bgShiftY);

	EM2TrackBar * emtrTemp = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_TEMP2_TRACKBAR));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_TRACKBAR), 0, 0, emtrTemp->bgShiftX, emtrTemp->bgShiftY);

	EM2EditSpin * emesTemp = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_TEMP2_SPIN));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_SPIN), 0, 0, emesTemp->bgShiftX, emesTemp->bgShiftY);

	EM2ColorShow * emcsCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_TEMP2_COLOR));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_COLOR), 0, 0, emcsCol->bgShiftX, emcsCol->bgShiftY);

	EM2ComboBox * emcPreset = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_TEMP2_PRESET));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_PRESET), 0, 0, emcPreset->bgShiftX, emcPreset->bgShiftY);

	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEMP2_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);
	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEMP2_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEMP2_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);

	return true;
}

//------------------------------------------------------------------------------------------------
