#include <windows.h>
#include <GdiPlus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;

//----------------------------------------------------------------------

void InitEM2ScrollBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2ScrollBar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2ScrollBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2ScrollBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2ScrollBar * GetEM2ScrollBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2ScrollBar * emsb = (EM2ScrollBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2ScrollBar * emsb = (EM2ScrollBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emsb;
}

//----------------------------------------------------------------------

void SetEM2ScrollBarInstance(HWND hwnd, EM2ScrollBar *emsb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emsb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emsb);
	#endif
}

//----------------------------------------------------------------------

EM2ScrollBar::EM2ScrollBar(HWND hWnd)
{
	hwnd = hWnd;
	RECT crect;
	GetClientRect(hWnd, &crect);
	arrangement = (crect.right>crect.bottom) ? ARR_HORIZONTAL : ARR_VERTICAL;

	sliderClicked = false;
	sliderMouseOver = false;

	rgnTrackBefore = 0;
	rgnTrackAfter = 0;
	rgnSlider = 0;
	rgnEmpty = 0;

	size_work = 1000;
	size_screen = 500;
	workarea_pos = 50;

	path_thickness =	min(crect.right, crect.bottom);
	path_length =		max(crect.right, crect.bottom);
	slider_length = 20;
	slider_length_min = 20;
	slider_pos = 20;

	
	updateSize(crect.right, crect.bottom);
	if (arrangement==EM2ScrollBar::ARR_HORIZONTAL)
		updateRegionsHorizontal();
	else
		updateRegionsVertical();
	

	colBg = NGCOL_BG_MEDIUM;
	colPath = RGB(49,49,49);
	colPathDisabled = RGB(49,49,49);
	colSlider = RGB(87,87,87);
	colSliderMouseOver = RGB(95,95,95);;
	colSliderMouseClicked = RGB(95,95,95);
	colSliderDisabled = RGB(54,54,54);

	alphaPath = 255;
	alphaPathDisabled = 255;
	alphaSliderNormal = 255;
	alphaSliderMouseOver = 255;
	alphaSliderMouseClicked = 255;
	alphaSliderDisabled = 255;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;

}

//----------------------------------------------------------------------

EM2ScrollBar::~EM2ScrollBar()
{
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2ScrollBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2ScrollBar * emsb;
	emsb = GetEM2ScrollBarInstance(hwnd);

	static int cmx, cmy, cpos;

	switch (msg)
	{
		case WM_CREATE:
			{
				emsb = new EM2ScrollBar(hwnd);
				SetEM2ScrollBarInstance(hwnd, emsb);
			}
			break;
		case WM_DESTROY:
			{
				delete emsb;
				SetEM2ScrollBarInstance(hwnd, 0);
			}
			break;
		case WM_SIZE:
			{
				emsb->updateSize(LOWORD(lParam), HIWORD(lParam));
				if (emsb->arrangement==EM2ScrollBar::ARR_HORIZONTAL)
					emsb->updateRegionsHorizontal();
				else
					emsb->updateRegionsVertical();
			}
			break;
		case WM_PAINT:
			{
				RECT rect;
				PAINTSTRUCT ps;
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				GetClientRect(hwnd, &rect);

				// create back buffer
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);

				// copy background image
				if (emsb->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emsb->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emsb->bgShiftX, emsb->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, OPAQUE);
					SetBkColor(hdc, emsb->colBg);
				}

				emsb->drawScrollbar(hdc);

				// copy from back buffer
				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);
			}	// end WM_PAINT
			break;
		case WM_MOUSEMOVE:
			{
				if (!GetCapture())
					SetCapture(hwnd);

				if (emsb->sliderClicked)
				{
					int mx = (short)LOWORD(lParam);
					int my = (short)HIWORD(lParam);
					if (emsb->arrangement == EM2ScrollBar::ARR_HORIZONTAL)
					{
						emsb->slider_pos = cpos + mx-cmx;
						emsb->updateWorkareaPositionFromSlider();
						emsb->updateRegionsHorizontal();
					}
					else
					{
						emsb->slider_pos = cpos + my-cmy;
						emsb->updateWorkareaPositionFromSlider();
						emsb->updateRegionsVertical();
					}
					InvalidateRect(hwnd, NULL, false);
					UpdateWindow(hwnd);
					emsb->notifyParentChange();
				}
				else
				{
					bool need_redraw = false;
					POINT px = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
					if (PtInRegion(emsb->rgnSlider, px.x, px.y))
					{
						if (!emsb->sliderMouseOver)
							need_redraw = true;
						emsb->sliderMouseOver = true;
					}
					else
					{
						if (emsb->sliderMouseOver)
							need_redraw = true;
						emsb->sliderMouseOver = false;
						ReleaseCapture();
					}
					if (need_redraw)
						InvalidateRect(hwnd, NULL, false);
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emsb->sliderMouseOver = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{

				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);

				if (PtInRegion(emsb->rgnSlider, px.x, px.y))
				{
					emsb->sliderClicked = true;
					cmx = px.x;
					cmy = px.y;
					cpos = emsb->slider_pos;
					InvalidateRect(hwnd, NULL, false);
					break;
				}
				else
				{
					if (PtInRegion(emsb->rgnTrackBefore, px.x, px.y))
					{
						emsb->slider_pos -= emsb->slider_length;
						emsb->updateWorkareaPositionFromSlider();
						if (emsb->arrangement == EM2ScrollBar::ARR_HORIZONTAL)
							emsb->updateRegionsHorizontal();
						else
							emsb->updateRegionsVertical();
						InvalidateRect(hwnd, NULL, false);
						emsb->notifyParentChange();
					}
					else
						if (PtInRegion(emsb->rgnTrackAfter, px.x, px.y))
						{
							emsb->slider_pos += emsb->slider_length;
							emsb->updateWorkareaPositionFromSlider();
							if (emsb->arrangement == EM2ScrollBar::ARR_HORIZONTAL)
								emsb->updateRegionsHorizontal();
							else
								emsb->updateRegionsVertical();
							InvalidateRect(hwnd, NULL, false);
							emsb->notifyParentChange();
						}
				}

			}
			break;
		case WM_LBUTTONUP:
			{
				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);
				emsb->sliderClicked = false;
				SendMessage(hwnd, WM_MOUSEMOVE, wParam, lParam);
				ReleaseCapture();
				InvalidateRect(hwnd, NULL, false);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

bool EM2ScrollBar::updateRegionsHorizontal()
{
	RECT rect;
	GetClientRect(hwnd, &rect);

	int l = 0;
	int x1 = 0;
	int x2 = path_length;

	HRGN hNewSliderRgn = 0;
	HRGN hNewUnderSliderRgn = 0;
	HRGN hNewOverSliderRgn = 0;
	HRGN hNewEmptyRgn = 0;
	int pt = path_thickness;
	int slider_end = slider_pos + slider_length;
	if (pt%2)	
	{
		if (slider_length>=path_length)
		{
			POINT sle[] = {
				{	x1-1		,	l+pt/2	}, 
				{	x1+pt/2		,	l+pt	}, 
				{	x2-pt/2-1	,	l+pt	}, 
				{	x2			,	l+pt/2	}, 
				{	x2-pt/2-1	,	l		}, 
				{	x1+pt/2		,	l		}, 
			};
			hNewEmptyRgn = CreatePolygonRgn(sle, 6, WINDING);
		}
		else
		{
			POINT slp[] = {
				{	x1+slider_pos			,	l+pt/2	}, 
				{	x1+slider_pos+pt/2+1	,	l+pt	}, 
				{	x1+slider_end-pt/2-1	,	l+pt	}, 
				{	x1+slider_end			,	l+pt/2	}, 
				{	x1+slider_end-pt/2		,	l		}, 
				{	x1+slider_pos+pt/2		,	l		}, 
			};
			hNewSliderRgn = CreatePolygonRgn(slp, 6, WINDING);

			POINT slu[] = {
				{	x1+slider_end			,	l+pt/2	}, 
				{	x1+slider_end-pt/2-1	,	l+pt	}, 
				{	x2-pt/2-1				,	l+pt	}, 
				{	x2						,	l+pt/2	}, 
				{	x2-pt/2					,	l		}, 
				{	x1+slider_end-pt/2		,	l		}, 
			};
			hNewUnderSliderRgn = CreatePolygonRgn(slu, 6, WINDING);

			POINT slo[] = {
				{	x1						,	l+pt/2	}, 
				{	x1+pt/2+1				,	l+pt	}, 
				{	x1+slider_pos+pt/2+1	,	l+pt	}, 
				{	x1+slider_pos			,	l+pt/2	}, 
				{	x1+slider_pos+pt/2		,	l		}, 
				{	x1+pt/2					,	l		}, 
			};
			hNewOverSliderRgn = CreatePolygonRgn(slo, 6, WINDING);
		}
	}
	else
	{
		if (slider_length>=path_length)
		{
			POINT sle[] = {
				{	x1			,	l+pt/2-1	}, 
				{	x1-1		,	l+pt/2		}, 
				{	x1+pt/2		,	l+pt		}, 
				{	x2-pt/2-1	,	l+pt		}, 
				{	x2			,	l+pt/2		}, 
				{	x2			,	l+pt/2-1	}, 
				{	x2-pt/2		,	l			}, 
				{	x1+pt/2-1	,	l			}, 
			};
			hNewEmptyRgn = CreatePolygonRgn(sle, 8, WINDING);
		}
		else
		{
			POINT slp[] = {
				{	x1+slider_pos			,	l+pt/2-1	}, 
				{	x1+slider_pos			,	l+pt/2		}, 
				{	x1+slider_pos+pt/2		,	l+pt		}, 
				{	x1+slider_end-pt/2-1	,	l+pt		}, 
				{	x1+slider_end			,	l+pt/2		}, 
				{	x1+slider_end			,	l+pt/2-1	}, 
				{	x1+slider_end-pt/2+1	,	l			}, 
				{	x1+slider_pos+pt/2-1	,	l			}, 
			};
			hNewSliderRgn = CreatePolygonRgn(slp, 8, WINDING);

			POINT slu[] = {
				{	x1+slider_end			,	l+pt/2-1	}, 
				{	x1+slider_end			,	l+pt/2		}, 
				{	x1+slider_end-pt/2-1	,	l+pt		}, 
				{	x2-pt/2					,	l+pt		}, 
				{	x2						,	l+pt/2		}, 
				{	x2						,	l+pt/2-1	}, 
				{	x2-pt/2+1				,	l			}, 
				{	x1+slider_end-pt/2+1	,	l			}, 
			};
			hNewUnderSliderRgn = CreatePolygonRgn(slu, 8, WINDING);

			POINT slo[] = {
				{	x1						,	l+pt/2-1	}, 
				{	x1-1					,	l+pt/2		}, 
				{	x1+pt/2					,	l+pt		}, 
				{	x1+slider_pos+pt/2		,	l+pt		},
				{	x1+slider_pos			,	l+pt/2		}, 
				{	x1+slider_pos			,	l+pt/2-1	}, 
				{	x1+slider_pos+pt/2-1	,	l			}, 
				{	x1+pt/2-1				,	l			}, 
			};
			hNewOverSliderRgn = CreatePolygonRgn(slo, 8, WINDING);
		}
	}

	if (rgnSlider)
		DeleteObject(rgnSlider);
	rgnSlider = hNewSliderRgn;

	if (rgnTrackAfter)
		DeleteObject(rgnTrackAfter);
	rgnTrackAfter = hNewUnderSliderRgn;

	if (rgnTrackBefore)
		DeleteObject(rgnTrackBefore);
	rgnTrackBefore = hNewOverSliderRgn;

	if (rgnEmpty)
		DeleteObject(rgnEmpty);
	rgnEmpty = hNewEmptyRgn;

	return true;
}

//----------------------------------------------------------------------

bool EM2ScrollBar::updateRegionsVertical()
{
	RECT rect;
	GetClientRect(hwnd, &rect);

	int l = 0;
	int y1 = 0;
	int y3 = path_length;

	HRGN hNewSliderRgn = 0;
	HRGN hNewUnderSliderRgn = 0;
	HRGN hNewOverSliderRgn = 0;
	HRGN hNewEmptyRgn = 0;
	int pt = path_thickness;
	int slider_end = slider_pos + slider_length;
	if (pt%2)	
	{
		if (slider_length>=path_length)
		{
			POINT sle[] = {
				{l+pt/2		,	y1-1		}, 
				{l+pt		,	y1+pt/2		}, 
				{l+pt		,	y3-pt/2-1	}, 
				{l+pt/2		,	y3			}, 
				{l			,	y3-pt/2-1	}, 
				{l			,	y1+pt/2		}, 
			};
			hNewEmptyRgn = CreatePolygonRgn(sle, 6, WINDING);
		}
		else
		{
			POINT slp[] = {
				{l+pt/2		,	y1+slider_pos-1			}, 
				{l+pt		,	y1+slider_pos+pt/2		}, 
				{l+pt		,	y1+slider_end-pt/2-1	}, 
				{l+pt/2		,	y1+slider_end			}, 
				{l			,	y1+slider_end-pt/2-1	}, 
				{l			,	y1+slider_pos+pt/2		}, 
			};
			hNewSliderRgn = CreatePolygonRgn(slp, 6, WINDING);

			POINT slu[] = {
				{l+pt/2		,	y1+slider_end			}, 
				{l+pt		,	y1+slider_end-pt/2-1	}, 
				{l+pt		,	y3-pt/2-1				}, 
				{l+pt/2		,	y3						}, 
				{l			,	y3-pt/2-1				}, 
				{l			,	y1+slider_end-pt/2-1	}, 
			};
			hNewUnderSliderRgn = CreatePolygonRgn(slu, 6, WINDING);

			POINT slo[] = {
				{l+pt/2		,	y1-1					}, 
				{l+pt		,	y1+pt/2+1				}, 
				{l+pt		,	y1+slider_pos+pt/2+1	}, 
				{l+pt/2		,	y1+slider_pos-1			}, 
				{l			,	y1+slider_pos+pt/2		}, 
				{l			,	y1+pt/2					}, 
			};
			hNewOverSliderRgn = CreatePolygonRgn(slo, 6, WINDING);
		}
	}
	else
	{
		if (slider_length>=path_length)
		{
			POINT sle[] = {
				{l+pt/2-1	,	y1				}, 
				{l+pt/2		,	y1-1			}, 
				{l+pt		,	y1+pt/2			}, 
				{l+pt		,	y3-pt/2-1		}, 
				{l+pt/2		,	y3				}, 
				{l+pt/2-1	,	y3				}, 
				{l			,	y3-pt/2			}, 
				{l			,	y1+pt/2-1		}, 
			};
			hNewEmptyRgn = CreatePolygonRgn(sle, 8, WINDING);
		}
		else
		{
			POINT slp[] = {
				{l+pt/2-1	,	y1+slider_pos			}, 
				{l+pt/2		,	y1+slider_pos-1			}, 
				{l+pt		,	y1+slider_pos+pt/2		}, 
				{l+pt		,	y1+slider_end-pt/2-1	}, 
				{l+pt/2		,	y1+slider_end			}, 
				{l+pt/2-1	,	y1+slider_end			}, 
				{l			,	y1+slider_end-pt/2		}, 
				{l			,	y1+slider_pos+pt/2-1	}, 
			};
			hNewSliderRgn = CreatePolygonRgn(slp, 8, WINDING);

			POINT slu[] = {
				{l+pt/2-1	,	y1+slider_end			}, 
				{l+pt/2		,	y1+slider_end			}, 
				{l+pt		,	y1+slider_end-pt/2-1	}, 
				{l+pt		,	y3-pt/2-1				}, 
				{l+pt/2		,	y3						}, 
				{l+pt/2-1	,	y3						}, 
				{l			,	y3-pt/2					}, 
				{l			,	y1+slider_end-pt/2		}, 
			};
			hNewUnderSliderRgn = CreatePolygonRgn(slu, 8, WINDING);

			POINT slo[] = {
				{l+pt/2-1	,	y1						}, 
				{l+pt/2		,	y1-1					}, 
				{l+pt		,	y1+pt/2					}, 
				{l+pt		,	y1+slider_pos+pt/2		},
				{l+pt/2		,	y1+slider_pos-1			}, 
				{l+pt/2-1	,	y1+slider_pos			}, 
				{l			,	y1+slider_pos+pt/2-1	}, 
				{l			,	y1+pt/2-1				}, 
			};
			hNewOverSliderRgn = CreatePolygonRgn(slo, 8, WINDING);
		}
	}

	if (rgnSlider)
		DeleteObject(rgnSlider);
	rgnSlider = hNewSliderRgn;

	if (rgnTrackAfter)
		DeleteObject(rgnTrackAfter);
	rgnTrackAfter = hNewUnderSliderRgn;

	if (rgnTrackBefore)
		DeleteObject(rgnTrackBefore);
	rgnTrackBefore = hNewOverSliderRgn;

	if (rgnEmpty)
		DeleteObject(rgnEmpty);
	rgnEmpty = hNewEmptyRgn;

	return true;
}

//----------------------------------------------------------------------

void EM2ScrollBar::updateSize(int w, int h)
{
	RECT crect;
	GetClientRect(hwnd, &crect);
	arrangement = (w > h) ? ARR_HORIZONTAL : ARR_VERTICAL;

	path_thickness = (arrangement==ARR_HORIZONTAL) ? h : w;
	path_length = (arrangement==ARR_HORIZONTAL) ? w : h;
	slider_length = max(slider_length_min, path_length * size_screen / max(1,size_work));
		
	slider_pos  = workarea_pos * (path_length-slider_length) / max(1, (size_work-size_screen));
	slider_pos = max(0, min(slider_pos, path_length-slider_length));
}

//----------------------------------------------------------------------

void EM2ScrollBar::updateWorkareaPositionFromSlider()
{
	slider_pos = min(slider_pos, path_length-slider_length);
	slider_pos = max(slider_pos, 0);
	workarea_pos = slider_pos * (size_work-size_screen) / (max(1, path_length-slider_length));
}

//----------------------------------------------------------------------

void EM2ScrollBar::updateSliderFromWorkarea()
{
	slider_pos = workarea_pos * max(1, path_length-slider_length) / max(1, size_work-size_screen);
	slider_pos = min(slider_pos, path_length-slider_length);
	slider_pos = max(slider_pos, 0);

	RECT crect;
	GetClientRect(hwnd, &crect);
	if (crect.right > crect.bottom) 
		updateRegionsHorizontal();
	else
		updateRegionsVertical();
}

//----------------------------------------------------------------------

void EM2ScrollBar::setWorkArea(int visible, int whole)
{
	size_work = max(50, whole);
	size_screen = max(10, visible);
	RECT rect;
	GetClientRect(hwnd, &rect);
	updateSize(rect.right, rect.bottom);
}

//----------------------------------------------------------------------

bool EM2ScrollBar::notifyParentChange()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, SB2_POS_CHANGED), 
			(LPARAM)hwnd);
	return true;
}

//----------------------------------------------------------------------

bool EM2ScrollBar::drawScrollbar(HDC hdc)
{
	if (!hdc)
		return false;

	bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

	#ifdef GUI2_USE_GDIPLUS
	{
		ULONG_PTR gdiplusToken;		// activate gdi+
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		{
			Gdiplus::Graphics graphics(hdc);

			Gdiplus::SolidBrush brushPath(Gdiplus::Color(			disabled ? alphaPathDisabled : alphaPath,		
																								GetRValue(colPath),					GetGValue(colPath),					GetBValue(colPath)				));
			Gdiplus::SolidBrush brushNormal(Gdiplus::Color(			alphaSliderNormal,			GetRValue(colSlider),				GetGValue(colSlider),				GetBValue(colSlider)			));
			Gdiplus::SolidBrush brushMouseOver(Gdiplus::Color(		alphaSliderMouseOver,		GetRValue(colSliderMouseOver),		GetGValue(colSliderMouseOver),		GetBValue(colSliderMouseOver)	));
			Gdiplus::SolidBrush brushMouseClicked(Gdiplus::Color(	alphaSliderMouseClicked,	GetRValue(colSliderMouseClicked),	GetGValue(colSliderMouseClicked),	GetBValue(colSliderMouseClicked)));
			Gdiplus::SolidBrush brushDisabled(Gdiplus::Color(		alphaSliderDisabled,		GetRValue(colSliderDisabled),		GetGValue(colSliderDisabled),		GetBValue(colSliderDisabled)	));

			if (rgnSlider)
			{
				Gdiplus::Region * regionSlider = Gdiplus::Region::FromHRGN(rgnSlider);
				if (disabled)
					graphics.FillRegion(&brushDisabled, regionSlider);
				else
					if (sliderClicked)
						graphics.FillRegion(&brushMouseClicked, regionSlider);
					else
						if (sliderMouseOver)
							graphics.FillRegion(&brushMouseOver, regionSlider);
						else
							graphics.FillRegion(&brushNormal, regionSlider);
				delete regionSlider;
			}
			if (rgnTrackBefore)
			{
				Gdiplus::Region * regionOverSlider = Gdiplus::Region::FromHRGN(rgnTrackBefore);
				graphics.FillRegion(&brushPath, regionOverSlider);
				delete regionOverSlider;
			}
			if (rgnTrackAfter)
			{
				Gdiplus::Region * regionUnderSlider = Gdiplus::Region::FromHRGN(rgnTrackAfter);
				graphics.FillRegion(&brushPath, regionUnderSlider);
				delete regionUnderSlider;
			}
			if (rgnEmpty)
			{
				Gdiplus::Region * regionEmpty = Gdiplus::Region::FromHRGN(rgnEmpty);
				graphics.FillRegion(&brushPath, regionEmpty);
				delete regionEmpty;
			}

		}	// gdi+ variables destructors here
		Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
	}
	#else
	
		HBRUSH hbrs1 = CreateSolidBrush(getMixedColor(colBg, disabled ? alphaPathDisabled : alphaPath, colPath));
		HBRUSH hbrs2 = CreateSolidBrush(getMixedColor(colBg, alphaSliderNormal, colSlider));
		HBRUSH hbrs3 = CreateSolidBrush(getMixedColor(colBg, alphaSliderMouseOver, colSliderMouseOver));
		HBRUSH hbrs4 = CreateSolidBrush(getMixedColor(colBg, alphaSliderMouseClicked, colSliderMouseClicked));
		HBRUSH hbrs5 = CreateSolidBrush(getMixedColor(colBg, alphaSliderDisabled, colSliderDisabled));
	
		if (rgnSlider)
		{
			if (disabled)
				FillRgn(hdc, rgnSlider,			hbrs5);
			else
				if (sliderClicked)
					FillRgn(hdc, rgnSlider,			hbrs4);
				else
					if (sliderMouseOver)
						FillRgn(hdc, rgnSlider,			hbrs3);
					else
						FillRgn(hdc, rgnSlider,			hbrs2);
		}

		if (rgnTrackBefore)
			FillRgn(hdc, rgnTrackBefore,	hbrs1);
		if (rgnTrackAfter)
			FillRgn(hdc, rgnTrackAfter,		hbrs1);
		if (rgnEmpty)
			FillRgn(hdc, rgnEmpty,		hbrs1);
		DeleteObject(hbrs1);
		DeleteObject(hbrs2);
		DeleteObject(hbrs3);
		DeleteObject(hbrs4);
		DeleteObject(hbrs5);
	#endif

	return true;
}

//----------------------------------------------------------------------
