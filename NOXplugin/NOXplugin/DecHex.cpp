#include "max2013predef.h"
#include "NOXExporter.h"

//----------------------------------------------------------------------------------------

bool DecHex::singleHexToDec(unsigned char hex, unsigned char &dec)
{
	unsigned char c = hex;
	if ((c<48 ||c>57) && (c<97 || c>102))
		return false;

	if (c<58)
		dec = c-48;
	else
		dec = c-87;

	return true;
}


//----------------------------------------------------------------------------------------

bool DecHex::hex2ToUchar(unsigned char *h, unsigned char &c)
{
	unsigned char b=0;
	unsigned char d=0;

	if (singleHexToDec(h[0], d))
		b=d;
	else 
		return false;

	if (singleHexToDec(h[1], d))
		b += d*16;
	else 
		return false;
	
	c = b;
	return true;
}


bool DecHex::ucharToHex2(const unsigned char c, unsigned char * h)
{
	if (!h)
		return false;

	char a,b;
	char c1,c2;
	a = c%16;
	b = c/16;
	c1 = singleDecToHex(a);
	c2 = singleDecToHex(b);

	h[0] = c1;
	h[1] = c2;
	
	return true;
}

bool DecHex::ucharToHex2BigEndian(const unsigned char c, unsigned char * h)
{
	if (!h)
		return false;

	char a,b;
	char c1,c2;
	a = c/16;
	b = c%16;
	c1 = singleDecToHex(a);
	c2 = singleDecToHex(b);

	h[0] = c1;
	h[1] = c2;
	
	return true;
}

//----------------------------------------------------------------------------------------

bool DecHex::hex2ToUcharBigEndian(unsigned char *h, unsigned char &c)
{
	unsigned char b=0;
	unsigned char d=0;

	if (singleHexToDec(h[0], d))
		b=d*16;
	else 
		return false;

	if (singleHexToDec(h[1], d))
		b += d;
	else 
		return false;
	
	c = b;
	return true;
}


//----------------------------------------------------------------------------------------

unsigned char DecHex::singleDecToHex(unsigned char dec)
{
	if (dec<10)
		return dec+48;
	else
		return dec+87;
}


//----------------------------------------------------------------------------------------

void DecHex::floatToHex(float f, unsigned char *h)
{
	unsigned char * c;
	c = (unsigned char *)(&f);
	for (unsigned char i=0; i<4; i++)
	{
		h[2*i]   = singleDecToHex(c[i]%16);
		h[2*i+1] = singleDecToHex(c[i]/16);
	}
}


//----------------------------------------------------------------------------------------

bool DecHex::hex8ToFloat(unsigned char * h, float &f)
{
	unsigned char b[4];
	unsigned char d;
	for (unsigned char i=0; i<4; i++)
	{
		b[i]=0;

		if (singleHexToDec(h[2*i], d))
			b[i]=d;
		else 
			return false;

		if (singleHexToDec(h[2*i+1], d))
			b[i] += d*16;
		else 
			return false;
	}
	memcpy(&f, b, 4);
	return true;
}

//----------------------------------------------------------------------------------------

void DecHex::uintToHex(unsigned int ui, unsigned char *h)
{
	unsigned char * c;
	c = (unsigned char *)(&ui);
	for (unsigned char i=0; i<4; i++)
	{
		h[2*i]   = singleDecToHex(c[i]%16);
		h[2*i+1] = singleDecToHex(c[i]/16);
	}
}

//----------------------------------------------------------------------------------------

bool DecHex::hex8ToUint(unsigned char * h, unsigned int &ui)
{
	unsigned char b[4];
	unsigned char d;
	for (unsigned char i=0; i<4; i++)
	{
		b[i]=0;

		if (singleHexToDec(h[2*i], d))
			b[i]=d;
		else 
			return false;

		if (singleHexToDec(h[2*i+1], d))
			b[i] += d*16;
		else 
			return false;
	}
	memcpy(&ui, b, 4);
	return true;
}

//----------------------------------------------------------------------------------------
