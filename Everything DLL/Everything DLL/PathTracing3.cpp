#include "PathTracing.h"
#include <math.h>
#include <stdio.h>
#include "log.h"
#include <float.h>
#include <xmmintrin.h>
#include "embree.h"

bool checkForCaustic(float * arrRough, int cur, bool &isSecondary, float maxRough);
inline bool findLastNonSpecPdf(PTStack * pt, int lvl, float &pdf);
inline bool findPreLastNonSpecPdf(PTStack * pt, int lvl, float maxRough, float &pdf);
inline bool isCaustic(PTStack * pt, int lvl, float maxRough);
inline bool isSecondaryCaustic(PTStack * pt, int lvl, float maxRough);
inline bool isOnlySpecular(PTStack * pt, int lvl, float maxRough);
inline bool isLastSpecular(PTStack * pt, int lvl, float maxRough);	// or camera and can't be joined to random point
inline bool isLDDE(PTStack * pt, int lvl, float maxRough, float &dd_conn_dist, float &cos_t2, float &cos_t3);
inline bool isLightEyePath(PTStack * pt, int lvl);
inline bool checkGotNonSpec(PTStack * pt, int lvl, float maxRough);
float evalWeightForInvisibleLight(PTStack * pt, int lvl);

bool findSSSPoint(const float sample_time, const Point3d &origin, const HitData &hd_in, HitData &hd_out, MaterialNox * mat, MatLayer * mlay, 
				  Matrix4d &instMatrix, int &instanceID, int &iitri, float &fu, float &fv, Color4 &attenuation);

//#define ONLY_BRDF
//#define ONLY_RAND_LIGHT
#define SAME_SS
#define USE_BVH
#define BVH_NON_SSE
//#define IGNORE_FIRST_RAY_FROM_SUN_PORTAL
//#define IGNORE_PDF_TO_SUN_PORTAL
//#define IGNORE_PDF_TO_SUN_NO_INT
//#define IGNORE_CONNECT_TO_SUN
#define FIRST_INT_MULTIPLE_RAYS
//#define FIRST_INT_MULTIPLE_RAYS_AFFECTS_HEURISTICS
#define RR_AFTER_RANDOM
#define EVAL_ALL_MATLAYERS
//#define BRDF_ALL_LAYERS_CORRECT

// ---------------------------  IMPORTANCE RESAMPLING 
#define RIS_ENABLED
#define RIS_MAXTRIES 16
#ifndef RIS_ENABLED
	#ifdef RIS_MAXTRIES
		#undef RIS_MAXTRIES
	#endif
	#define RIS_MAXTRIES 1
#endif
struct RIS_PT
{
	Color4 contribution;
	float weight;
	float weights_so_far;
	float mis;
	int blendlayer;

	Point3d shch_spoint;
	Vector3d shch_dirToLight;
	float shch_ldist;

	RIS_PT() {contribution=Color4(0,0,0); weight=0.0f; mis=1.0f; weights_so_far=0.0f; blendlayer=0; 
				shch_spoint=Point3d(0,0,0); shch_dirToLight=Vector3d(1,1,1); shch_ldist=1.0f;
	}
};

//-----------------------------------------------------------------------------------------------------

#define DEBUG_NOW
#ifdef DEBUG_NOW
	#define DEBUG_INCREASE_PATH_LVL cur_path_lvl++
	#define DEBUG_INCREASE_COUNTER plvl_counters[min(255,cur_path_lvl)]++
	#define DEBUG_ZERO_PATH_LVL cur_path_lvl = 0
#else
	#define DEBUG_INCREASE_PATH_LVL
	#define DEBUG_INCREASE_COUNTER
	#define DEBUG_ZERO_PATH_LVL
#endif

#define DEBUG_LOG_NAN

//-----------------------------------------------------------------------------------------------------

PathTracer3::PathTracer3(SceneStatic * scs, Camera * camera, bool * running)
{
	ss = scs;
	cam = camera;
	working = running;
	threadID = 0;
	discard_caustic = true;
	discard_secondary_caustic = true;
	fake_caustic = true;
	maxCausticRough = 0.0f;
}

PathTracer3::~PathTracer3()
{
}

//-----------------------------------------------------------------------------------------------------

void PathTracer3::renderingLoop()
{
	if (!ss  ||  !cam  ||  !working)
		return;

	PTStack pt[64];

	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	bool usesun = ss->sunsky->sunOn;
	bool usesky = ss->useSunSky;
	bool useskyportals = ss->useSkyPortals;
	int sunchannel = ss->sunsky->sunOtherBlendLayer ? 1 : 0;
	int envchannel = scene->env.blendlayer-1;
	float envmpl = pow(2.0f, scene->env.powerEV);
	float useenv = scene->env.enabled && scene->env.tex.isValid();

	Color4 pixelLayers[16];
	Color4 pixelLayersGI[16];
	for (int i=0; i<16; i++)
	{
		pixelLayers[i] = Color4(0.0f, 0.0f, 0.0f);
		pixelLayersGI[i] = Color4(0.0f, 0.0f, 0.0f);
	}
	bool blendLayersActive[16];
	for (int i=0; i<16; i++)
		blendLayersActive[i] = ((cam->blendBits & (1<<i)) != 0);

	unsigned int cur_path_lvl = 0;
	unsigned long long plvl_counters[256];
	for (int i=0; i<256; i++)
		plvl_counters[i] = 0;
	int randomType = scene->sscene.random_type;

	float arrRough[70];
	float sample_time = 2.5f;
	float mb_time = scene->sscene.motion_blur_time;
	if (!scene->sscene.motion_blur_on)
		mb_time = 0.0;


	char bbuf2[64];
	sprintf_s(bbuf2, 64, "mb time: %f", mb_time);
	Logger::add(bbuf2);
	int cursampleindex = 0;
	bool finished = false;
	RandomGenerator * rg = rtr->getRandomGeneratorForThread();
	int first_samples_max = ss->pt_gi_samples;
	int ris_max_samples_set = ss->pt_ris_samples;
	float pfsm = 1.0f/first_samples_max;
	#ifndef FIRST_INT_MULTIPLE_RAYS
		first_samples_max = 1;
	#endif


	// ---- MAIN LOOP ----
	while (*working  &&  !finished)
	{
		if (scene->sscene.motion_blur_still)
			sample_time = scene->sscene.motion_blur_time;
		else
			#ifdef RAND_PER_THREAD
				sample_time = rg->getRandomFloat() * mb_time;
			#else
				sample_time = rand()/(float)RAND_MAX * mb_time;
			#endif
		Point3d origin = cam->position;
		Vector3d direction;
		float dX, dY;
		cam->randomNewCoords(dX, dY, randomType, finished, true);
		if (finished)
			return;

		direction = cam->getDirection(dX,dY, origin, sample_time);

		bool pt_go_on = true;
		Point3d ipoint = origin;
		Vector3d dirOut = direction;

		float spectr_pos = -1;

		DEBUG_INCREASE_COUNTER;
		cur_path_lvl = 0;
		float sum_d = 0.0f;
		int lvl = -1;
		int first_sample_cur = 0;

		#ifdef FIRST_INT_MULTIPLE_RAYS
			while (pt_go_on  ||  first_sample_cur<first_samples_max)
		#else
			while (pt_go_on)
		#endif
		{
			if (lvl==0  &&  !pt_go_on)
				break;
			lvl++;
			if (lvl>63)
				break;

			if (!pt_go_on)	// restore first intersection
			{
				lvl = 1;
				cur_path_lvl = 1;
				float mlaypdf = 1.0f;
				PTStack * ptpr = pt;
				if (!ptpr->fake_transmission)
				{
					ptpr->hd.clearFlagDelta();
					ptpr->mlay = ptpr->mat->getRandomShadeLayer(ptpr->hd.tU, ptpr->hd.tV, mlaypdf);
					if (!ptpr->mlay)
					{
						pt_go_on = false;
						first_sample_cur++;
						continue;
					}
					ptpr->hd.clearFlagInvalidRandom();
					ptpr->hd.out = ptpr->mlay->randomNewDirection(ptpr->hd, ptpr->pdf_brdf);
					if(ptpr->hd.isFlagInvalidRandom())
					{
						pt_go_on = false;
						continue;
					}
					#ifdef EVAL_ALL_MATLAYERS
						ptpr->updatePdfBrdfMultilayered();
					#else
						Color4 newBrdf = ptpr->mlay->getBRDF(ptpr->hd);
						float cosMpl = (ptpr->hd.isFlagDelta() ? 1.0f : fabs(ptpr->hd.out*ptpr->hd.normal_shade));
						ptpr->brdf = newBrdf * (cosMpl/mlaypdf);
					#endif
					if (ptpr->brdf.isBlack() || ptpr->pdf_brdf==0.0f)
					{
						pt_go_on = false;
						break;
					}

					if (!ptpr->hd.isFlagDelta())
						sum_d = 0.0f;
				}
				pt_go_on = true;
			}
			if (lvl==1)
				first_sample_cur++;

			PTStack * ptp = &(pt[lvl]);
			if (lvl==0)
				ptp->energy_in = Color4(1,1,1);
			else
			{	// recursive, use prev data
				PTStack * ptpr = &(pt[lvl-1]);
				ptp->energy_in = ptpr->energy_in * ptpr->brdf * (1.0f/ptpr->pdf_brdf);
				
				#ifdef DEBUG_LOG_NAN
					if (ptp->energy_in.isNaN())
					{
						char buf[256];
						sprintf_s(buf, 256, "NaN in pt[%d]->energy_in... prev->energy_in=%.5f, %.5f, %.5f   prev->brdf=%.5f, %.5f, %.5f   prev->pdf=%.5f", 
									lvl, ptpr->energy_in.r, ptpr->energy_in.g, ptpr->energy_in.b, ptpr->brdf.r, ptpr->brdf.g, ptpr->brdf.b, ptpr->pdf_brdf);
						Logger::add(buf);
					}
				#endif
				if (lvl==1)
				{
					ptp->energy_in *= pfsm;
					dirOut = ptpr->hd.out;
					ipoint = ptpr->int_point + ptpr->normal_geom * (ptpr->normal_geom*dirOut > 0 ? 0.0001f : -0.0001f);
				}

			}
			cur_path_lvl++;

			// CHECK FOR INTERSECTION
			float bigf = BIGFLOAT;
			int itri = -1;
			float u,v;
			Matrix4d inst_mat;
			int instanceID = 0;
			inst_mat.setIdentity();

			bool isCausticHit = isCaustic(pt, lvl-1, maxCausticRough);
			bool isSecondaryCausticHit = isSecondaryCaustic(pt, lvl-1, maxCausticRough);
			bool isCausticConnect = false;
			bool isSecondaryCausticConnect = false;
			bool onlySpecularHit = isOnlySpecular(pt, lvl-1, maxCausticRough);
			bool lastSpecularHit = isLastSpecular(pt, lvl-1, maxCausticRough);

			float d;
			if (rtr->use_embree)
				d = intersectEmbree(scene, ipoint, dirOut, inst_mat, bigf, instanceID, itri, u,v);
			else
				d = rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(sample_time, ipoint, dirOut, inst_mat, bigf, instanceID, itri, u,v);
			if (d < 0.0f)		// NO INTERSECTION
			{
				bool discard_sun_hit = false;
				if (!usesun)
					discard_sun_hit = true;
				if (lvl>0)
				{
					if (isCausticHit  &&  fake_caustic) 
						discard_sun_hit = true;
					if (isSecondaryCausticHit  &&  discard_secondary_caustic) 
						discard_sun_hit = true;
				}

				// SUN - NO OTHER INTESECTION
				if (!discard_sun_hit)
				{
					bool isSecondary, discard_now = false; 
					bool isCaustic = checkForCaustic(arrRough, cur_path_lvl, isSecondary, maxCausticRough);
					if (isSecondary && discard_secondary_caustic)
						discard_now = true;
					if (!discard_now)
					{
						Vector3d sdir;
						sdir = ss->sunsky->getSunDirection();
						sdir.normalize();
						if (dirOut * sdir >= ss->sunsky->maxCosSunSize)
						{
							Color4 sunenergy = ss->sunsky->getSunColor();
							float brdfpdf = 1.0f;
							findLastNonSpecPdf(pt, lvl-1, brdfpdf);
							float sundirpdf = 1.0f / ss->sunsky->getSunSizeSteradians();
							float weightMIS = brdfpdf*brdfpdf / (sundirpdf*sundirpdf + brdfpdf*brdfpdf);
							if (lastSpecularHit)		// include camera, exclude fake/ghost glass
								weightMIS = 1.0f;

							#ifdef ONLY_BRDF
								weightMIS = 1.0f;
							#endif
							#ifdef ONLY_RAND_LIGHT
								weightMIS = 0.0f;
							#endif
							#ifdef IGNORE_PDF_TO_SUN_NO_INT
								weightMIS = 0.0f;
							#endif

							Color4 suncontr = sunenergy * ptp->energy_in * weightMIS;
							if (!(suncontr.isInf()  ||  suncontr.isNaN()))
							{
								if (isCausticHit  ||  isSecondaryCausticHit)
									pixelLayersGI[sunchannel] += suncontr;
								else
									pixelLayers[sunchannel] += suncontr;
							}
							#ifdef DEBUG_LOG_NAN
							else
							{
								char buf[256];
								sprintf_s(buf, 256, "NaN in brdf->sun... weight=%f  pathBrdf=%.5f, %.5f, %.5f", weightMIS, ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b);
								Logger::add(buf);
							}
							#endif
						}
					}
				}

				// SKY - NO OTHER INTERSECTION
				if (usesky)
				{
					Color4 skycolor = ss->sunsky->getColor(dirOut);
					Color4 skycontr = skycolor * ptp->energy_in;
					if (!(skycontr.isInf()  ||  skycontr.isNaN()))
					{
						if (isCausticHit)
							pixelLayersGI[0] += skycontr;
						else
							pixelLayers[0] += skycontr;
					}
					#ifdef DEBUG_LOG_NAN
					else
					{
						char buf[256];
						sprintf_s(buf, 256, "NaN in brdf->sky... pathBrdf=%.5f, %.5f, %.5f", ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b);
						Logger::add(buf);
					}
					#endif
				}

				// ENV MAP - NO OTHER INTERSECTION
				if (useenv)
				{
					Color4 envcolor = scene->env.getColor(dirOut);
					Color4 envcontr = envcolor * ptp->energy_in * envmpl;
					if (!(envcontr.isInf()  ||  envcontr.isNaN()))
					{
						if (isCausticHit)
							pixelLayersGI[envchannel] += envcontr;
						else
							pixelLayers[envchannel] += envcontr;
					}
					#ifdef DEBUG_LOG_NAN
					else
					{
						char buf[256];
						sprintf_s(buf, 256, "NaN in brdf->sky... pathBrdf=%.5f, %.5f, %.5f", ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b);
						Logger::add(buf);
					}
					#endif
				}


				pt_go_on = false;

			}
			else				// GOT SOMETHING INTERSECTED
			{
				sum_d += d;
				ptp->tri = &(scene->triangles[itri]);
				ptp->inst_mat = inst_mat;
				ptp->normal_shade_orig = inst_mat.getMatrixForNormals() * ptp->tri->evalNormal(u, v);
				ptp->normal_shade_orig.normalize();
				ptp->normal_geom = inst_mat.getMatrixForNormals() * ptp->tri->normal_geom;
				ptp->normal_geom.normalize();
				ptp->u = u;
				ptp->v = v;
				ptp->tri->evalTexUV(u, v, ptp->hd.tU, ptp->hd.tV);
				ptp->int_point = ptp->inst_mat * ptp->tri->getPointFromUV(u, v);
				ptp->hd.clearFlagsAll();
				ptp->hd.in = dirOut * -1.0f;
				ptp->hd.out = dirOut * -1.0f;
				Vector3d uvdirU, uvdirV;
				ptp->tri->getTangentSpaceSmooth(inst_mat, u,v, uvdirU, uvdirV);
				ptp->hd = HitData(dirOut*-1, dirOut*-1, ptp->normal_shade_orig, ptp->normal_geom, uvdirU, uvdirV, ptp->hd.tU, ptp->hd.tV, 0.0f, spectr_pos);
				ptp->hd.adjustNormal();
				ptp->hd.lastDist = d;


				ptp->mat = scene->mats[scene->instances[instanceID].materials[ptp->tri->matInInst]];
				if (!ptp->mat)
				{
					pt_go_on = false;
					continue;
				}

				if (ptp->mat->isSkyPortal   &&   usesky)
				{	// SKYPORTAL WAS HIT
					float dd_conn_dist = 0.0f;
					float cos_t2=1.0f, cos_t3=1.0f, cos_t4=1.0f;
					bool path_connection_gi_like = isLDDE(pt, lvl-1, maxCausticRough, dd_conn_dist, cos_t2, cos_t3);
					bool path_caustic = isLastSpecular(pt, lvl-1, maxCausticRough);
					bool path_caustic_secondary = isSecondaryCaustic(pt, lvl-1, maxCausticRough);
					bool path_L_E = isLightEyePath(pt, lvl-1);
					bool path_pure_spec = isOnlySpecular(pt, lvl-1, maxCausticRough);

					// SUN - ON SKYPORTAL
					if (usesun)
					{
						Vector3d sdir;
						sdir = ss->sunsky->getSunDirection();
						sdir.normalize();
						if (dirOut * sdir >= ss->sunsky->maxCosSunSize)
						{
							Color4 sunenergy = ss->sunsky->getSunColor();
							float brdfpdf;
							findLastNonSpecPdf(pt, lvl-1, brdfpdf);
							float sundirpdf = 1.0f / ss->sunsky->getSunSizeSteradians();
							float weightMIS = brdfpdf*brdfpdf / (sundirpdf*sundirpdf + brdfpdf*brdfpdf);

							if (path_caustic)
							{
								weightMIS = 1.0f;
								if (fake_caustic)
									weightMIS = 0.0f;
								if (discard_secondary_caustic  &&  path_caustic_secondary)
									weightMIS = 0.0f;
							}
							else
							{
								if (path_connection_gi_like  &&  cos_t2>0.0f  &&  cos_t3>0.0f)
								{
									cos_t4 = max(0.0f, ptp->hd.normal_geom * ptp->hd.in);
									if (cos_t4 > 0.0f)
									{
										float pdf_portal = 1.0f/ss->skyPortalsTotalArea / ss->sunsky->getSunSizeSteradians() * cos_t3 / (sum_d*sum_d);
										float preLastPdf;
										findPreLastNonSpecPdf(pt, lvl-1, maxCausticRough, preLastPdf);
										float pdf_sun_conn = preLastPdf * cos_t2 / (dd_conn_dist*dd_conn_dist) / ss->sunsky->getSunSizeSteradians() * cos_t4 / (sum_d*sum_d);
										float pdf_brdf = preLastPdf * cos_t2 / (dd_conn_dist*dd_conn_dist) * brdfpdf * cos_t4 / (sum_d*sum_d);
										weightMIS = pdf_brdf*pdf_brdf / (pdf_portal*pdf_portal + pdf_brdf*pdf_brdf + pdf_sun_conn*pdf_sun_conn);
									}
								}
							}

							#ifdef IGNORE_PDF_TO_SUN_PORTAL
								weightMIS = 0.0f;
							#endif

							Color4 suncontr = sunenergy * ptp->energy_in * weightMIS;
							if (!(suncontr.isInf()  ||  suncontr.isNaN()))
							{
								if (path_caustic   &&  !path_L_E)
									pixelLayersGI[sunchannel] += suncontr;
								else
									pixelLayers[sunchannel] += suncontr;
							}
							#ifdef DEBUG_LOG_NAN
							else
							{
								char buf[256];
								sprintf_s(buf, 256, "NaN in brdf->portal->sun... weight=%f  pathBrdf=%.5f, %.5f, %.5f", weightMIS, ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b);
								Logger::add(buf);
							}
							#endif
						}
					}

					// SKY - ON SKYPORTAL
					{
						float pdfSkyPortal = 1.0f / scene->sscene.skyPortalsTotalArea;
						float lcos = fabs(ptp->normal_geom * dirOut);
						float pdfSkyPortalAng = (lcos>0.0001f ? pdfSkyPortal*sum_d*sum_d/lcos : 0.0f);
						float pdfBrdf;
						findLastNonSpecPdf(pt, lvl-1, pdfBrdf);
						pdfBrdf *= lvl>0 ? pt[lvl-1].pdf_rr : 1.0f;
						float weightMIS = pdfBrdf*pdfBrdf / (pdfBrdf*pdfBrdf + pdfSkyPortalAng*pdfSkyPortalAng);

						if (path_caustic)
						{
							weightMIS = 1.0f;
							if (fake_caustic  &&  !path_L_E  &&  !path_pure_spec)
								weightMIS = 0.0f;
							if (discard_secondary_caustic  &&  path_caustic_secondary)
								weightMIS = 0.0f;
						}

	
						#ifdef ONLY_BRDF
							weightMIS = 1.0f;
						#endif
						#ifdef ONLY_RAND_LIGHT
							weightMIS = 0.0f;
						#endif


						Color4 skycolor = ss->sunsky->getColor(dirOut);
						Color4 toAdd = skycolor * ptp->energy_in * weightMIS;
						if (!(toAdd.isInf()  ||  toAdd.isNaN()))
						{
							if (path_caustic  &&  !path_L_E)
								pixelLayersGI[0] += toAdd;
							else
								pixelLayers[0] += toAdd;
						}
						#ifdef DEBUG_LOG_NAN
						else
						{
							char buf[256];
							sprintf_s(buf, 256, "NaN in brdf->portal->sky... weight=%f  pathBrdf=%.5f, %.5f, %.5f", weightMIS, ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b);
							Logger::add(buf);
						}
						#endif
					}
					pt_go_on = false;
				}	// END SKYPORTAL HIT
				else
				{	// NON-SKYPORTAL TRI/MAT
					Color4 matteCol = Color4(0,0,0);
					bool isMatte = false;
					Color4 matteAdded = Color4(0,0,0);
					Color4 matteWasted = Color4(0,0,0);
					if (ptp->mat->isMatteShadow)
					{
						Vector3d chkdir = (ptp->int_point - cam->position).getNormalized();
						Color4 envcolor = scene->env.getColor(chkdir);
						if (scene->env.enabled  &&  scene->env.tex.isValid())
							matteCol = envcolor * envmpl;
						isMatte = true;
					}

					// OPACITY
					float opacity = ptp->mat->getOpacity(ptp->hd.tU, ptp->hd.tV);
					if (opacity < 0.99999f)
					{
						#ifdef RAND_PER_THREAD
							float rand_opac = rg->getRandomFloat();
						#else
							float rand_opac = (float)rand()/(float)RAND_MAX;
						#endif
						if (rand_opac > opacity)
						{
							ptp->fake_transmission = true;
							ptp->pdf_brdf = 1.0f;
							ptp->pdf_rr = 1.0f;
							ptp->hd.out = dirOut;
							ptp->brdf = Color4(1,1,1);
							ipoint = ptp->int_point + ptp->normal_geom * (ptp->normal_geom*dirOut > 0 ? 0.0001f : -0.0001f);
							pt_go_on = true;
							continue;
						}
					}

					// MAT LAYER HERE
					float randLayerPdf = 1.0f;
					ptp->mlay = ptp->mat->getRandomAnyLayer(ptp->hd.tU, ptp->hd.tV, randLayerPdf);
					if (!ptp->mlay)
					{
						pt_go_on = false;
						continue;
					}

					if (ptp->mlay->type == MatLayer::TYPE_SHADE  ||  isMatte)
					{
						ptp->rough = ptp->mlay->getRough(ptp->hd.tU, ptp->hd.tV);
						arrRough[cur_path_lvl] = ptp->rough;

						//// SSS HERE ----------
						bool doSSS = false;
						Color4 sssAtten = Color4(1,1,1);
						if (ptp->mlay->sssON)
						{
							doSSS = true;
							HitData hd_sss;
							int iSSStri = -1;
							float sssU, sssV;
							Matrix4d sssImatr = inst_mat;
							Point3d sssStart = ptp->int_point;
							bool sssFound = findSSSPoint(sample_time, sssStart, ptp->hd, hd_sss, ptp->mat, ptp->mlay, 
								  sssImatr, instanceID, iSSStri, sssU, sssV, sssAtten);
							if (!sssFound)
							{
								pt_go_on = false;
								continue;
							}
							ptp->hd.in = hd_sss.in;
							ptp->hd.out = hd_sss.out;
							ptp->tri = &(scene->triangles[iSSStri]);
							ptp->int_point = ptp->tri->getPointFromUV(sssU,sssV);
							ptp->int_point = sssImatr * ptp->int_point;
							ptp->normal_shade_orig = sssImatr * ptp->tri->evalNormal(sssU, sssV);
							ptp->normal_geom = sssImatr * ptp->tri->normal_geom;
							ptp->normal_geom.normalize();
							ptp->normal_shade_orig.normalize();
							ptp->hd.normal_geom = ptp->normal_geom;
							ptp->hd.normal_shade = ptp->normal_shade_orig;
							ptp->u = sssU;   ptp->v = sssV;
							ptp->tri->evalTexUV(sssU, sssV, ptp->hd.tU, ptp->hd.tV);
							ptp->tri->getTangentSpaceSmooth(sssImatr, sssU,sssU, ptp->hd.dir_U, ptp->hd.dir_V);

						}

						// NORMAL MAPPING HERE
						if (ptp->mlay->tex_normal.isValid())
						{
							Vector3d vecU, vecV, vecU1, vecV1;
							vecU = ptp->hd.dir_U;
							vecV = ptp->hd.dir_V;

							vecU1 = ptp->normal_shade_orig ^ vecU;
							vecU = vecU1 ^ ptp->normal_shade_orig;
							vecU.normalize();

							vecV1 = ptp->normal_shade_orig ^ vecV;
							vecV = vecV1 ^ ptp->normal_shade_orig;
							vecV.normalize();

							Vector3d nMap = ptp->mlay->getNormalMap(ptp->hd.tU, ptp->hd.tV);
							ptp->normal_shade_orig = vecU * nMap.x + vecV * nMap.y + ptp->normal_shade_orig * nMap.z;
							ptp->normal_shade_orig.normalize();

							ptp->hd.normal_shade = ptp->normal_shade_orig;
							ptp->hd.adjustNormal();
						}

						bool isSecondary, discard_now = false; 
						bool isCaustic1 = checkForCaustic(arrRough, cur_path_lvl, isSecondary, maxCausticRough);
						if (isSecondary && discard_secondary_caustic)
							discard_now = true;


						// SUN ENERGY - TRY TO CONNECT
						bool isEyeSoFar = isLightEyePath(pt, lvl-1);
						if (usesun  &&  (isEyeSoFar  ||  ptp->rough>0.0f  &&   (ptp->rough>maxCausticRough || !fake_caustic)))
						{

							Vector3d sundir = ss->sunsky->getRandomPerturbedSunDirection();
							ptp->hd.out = sundir;
							ptp->hd.clearFlagDelta();
							Color4 brdftosun;
							bool gotNonSpec = lvl>0 ? checkGotNonSpec(pt, lvl-1,maxCausticRough) : false;
							float pdfbrdftosun = 1.0f;
							if (fake_caustic  &&  ptp->rough<=maxCausticRough  &&  gotNonSpec)
								brdftosun = Color4(0,0,0);
							else
							{
								#ifdef EVAL_ALL_MATLAYERS
									float tmpbpdf;
									ptp->evalBrdfPdfMultilayered(brdftosun, pdfbrdftosun, tmpbpdf);
								#else
									brdftosun = ptp->mlay->getBRDF(ptp->hd) * (1.0f/randLayerPdf);
									pdfbrdftosun = ptp->mlay->getProbability(ptp->hd);
								#endif
							}
							if (isMatte)
								brdftosun = Color4(1,1,1);
							if (!brdftosun.isBlack())
							{

								Point3d spoint = ptp->int_point + ptp->normal_geom* (sundir*ptp->normal_geom>0 ? 0.0001f : -0.0001f);
								float dist = BIGFLOAT;
								Color4 att = Color4(1.0f, 1.0f, 1.0f);

								float cos_t4 = 1.0f;
								int iskptri = -1;
								Matrix4d inst_mat1;
								int instanceID1 = 0;
								bool notShadow = !isShadowOpacFakeGlass(sample_time, spoint, sundir, inst_mat1, dist, instanceID1, iskptri, att, true);
								if (!notShadow  &&  iskptri>-1)
								{
									Triangle * tttri = &(rtr->curScenePtr->triangles[iskptri]);
									if (scene->mats[scene->instances[instanceID1].materials[tttri->matInInst]]->isSkyPortal)
									{
										notShadow = true;
										Vector3d pnormg = inst_mat1.getMatrixForNormals() * scene->triangles[iskptri].normal_geom;
										pnormg.normalize();
										cos_t4 = fabs(pnormg * sundir);
									}
								}

								if (isMatte)
								{
									Color4 sunenergy = ss->sunsky->getSunColor();
									sunenergy *= (ptp->normal_shade_orig * sundir);
									sunenergy *=  ss->sunsky->getSunSizeSteradians();
									if (notShadow)
										matteAdded += sunenergy;
									else
										matteWasted += sunenergy;
								}

								if (notShadow  &&  !isMatte)
								{
									float dd_conn_dist=0.0f, cos_t2=1.0f, cos_t3=1.0f;
									bool path_connection_gi_like = isLDDE(pt, lvl, maxCausticRough, dd_conn_dist, cos_t2, cos_t3);

									float rCosOut = ptp->normal_shade_orig * sundir;
									Color4 sunenergy = ss->sunsky->getSunColor();
									#ifdef EVAL_ALL_MATLAYERS
										float cosMpl = 1.0f;
									#else
										float cosMpl = (ptp->hd.isFlagDelta() ? 1.0f : fabs(rCosOut));
									#endif
									float weightMIS = 1.0f;
									if (useskyportals  &&  cur_path_lvl > 1  &&  iskptri>-1)
									{
										Triangle skptri = scene->triangles[iskptri];
										Vector3d skptri_normal_geom = inst_mat1.getMatrixForNormals() * skptri.normal_geom;
										skptri_normal_geom.normalize();
										cos_t4 = fabs(skptri_normal_geom * sundir);
										if (path_connection_gi_like  &&  cos_t2>0.0f  &&  cos_t3>0.0f  &&  cos_t4>0.0f  &&  iskptri>-1)
										{	// portal weight
											float pdfSunGI = 1.0f/ss->skyPortalsTotalArea / ss->sunsky->getSunSizeSteradians() * cos_t3 / (dist*dist);
											float pdfLast= 0.0f;
											findLastNonSpecPdf(pt, lvl-1, pdfLast);
											float pdfSundir = pdfLast * cos_t2 / (dd_conn_dist*dd_conn_dist) * (1.0f/ss->sunsky->getSunSizeSteradians()) * cos_t4 / (dist*dist);
											float pdfBrdf = pdfLast * cos_t2 / (dd_conn_dist*dd_conn_dist) * pdfbrdftosun * cos_t4 / (dist*dist);
											weightMIS = pdfSundir*pdfSundir / (pdfSundir*pdfSundir + pdfBrdf*pdfBrdf + pdfSunGI*pdfSunGI);
										}
										else
										{	// non-portal weight
											float brdfpdf = pdfbrdftosun;
											float sunpdf = 1.0f / ss->sunsky->getSunSizeSteradians();
											weightMIS = sunpdf*sunpdf / (sunpdf*sunpdf + brdfpdf*brdfpdf);
										}
									}
									else
									{
										float sundirpdf = 1.0f / ss->sunsky->getSunSizeSteradians();
										float brdfpdf = ptp->mlay->getProbability(ptp->hd);
										weightMIS = sundirpdf*sundirpdf / (sundirpdf*sundirpdf + brdfpdf*brdfpdf);
									}

									#ifdef ONLY_BRDF
										weightMIS = 0.0f;
									#endif
									#ifdef ONLY_RAND_LIGHT
										weightMIS = 1.0f;
									#endif
									#ifdef IGNORE_CONNECT_TO_SUN
										weightMIS = 0.0f;
									#endif

									Color4 suncontr = sunenergy * sssAtten * brdftosun * cosMpl * ss->sunsky->getSunSizeSteradians() * att * ptp->energy_in * weightMIS;

									if (!(suncontr.isInf()  ||  suncontr.isNaN()))
									{
										if (isCaustic1)
											pixelLayersGI[sunchannel] += suncontr;
										else
											pixelLayers[sunchannel] += suncontr;
									}
									#ifdef DEBUG_LOG_NAN
									else
									{
										char buf[256];
										sprintf_s(buf, 256, "NaN in tri->sun... cos=%4f  weight=%f  pathBrdf=%.5f, %.5f, %.5f  atten=%.5f, %.5f, %.5f", cosMpl, 
														weightMIS, ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b, att.r, att.g, att.b);
										Logger::add(buf);
									}
									#endif
								}
							}
						}	// END SUN

						// SUN FROM PORTAL FOR GI ONLY!
						bool gotNonSpec = checkGotNonSpec(pt, lvl-1,maxCausticRough);
						if (useskyportals  &&  usesun  &&  ptp->rough>maxCausticRough)
						{
							float pdfRandSkyPortal = 1.0f;
							int skpId = scene->randomSkyPortal(pdfRandSkyPortal);
							SkyPortalMesh * skpMesh = scene->skyportalMeshes[skpId];
							if (skpMesh)
							{
								MeshInstance * mInst = &(scene->instances[skpMesh->instNum]);
								Matrix4d instMat8 = mInst->matrix_transform;
								float pdfRandSkpTri;
								int triInMesh = 0;
								int iskptri = skpMesh->randomTriangle(pdfRandSkpTri, triInMesh);
								if (iskptri >= 0  &&  iskptri<scene->triangles.objCount)
								{
									Triangle * skptri = &(scene->triangles[iskptri]);
									float skpu, skpv;
									Point3d skpPoint;
									skptri->randomPoint(skpu, skpv, skpPoint);
									skpPoint = instMat8 * skpPoint;
									
									Vector3d dirtosun = ss->sunsky->getRandomPerturbedSunDirection();
									Vector3d dirfromsun = dirtosun * -1.0f;
									Vector3d skptri_normal_geom = instMat8.getMatrixForNormals() * skptri->normal_geom;
									skptri_normal_geom.normalize();
									Color4 sunenergy = ss->sunsky->getSunColor() * ss->sunsky->getSunSizeSteradians() * ss->skyPortalsTotalArea * fabs(skptri_normal_geom*dirfromsun);

									Point3d ssPoint = skpPoint + skptri_normal_geom * (skptri_normal_geom*dirfromsun>0 ? 0.0001f : -0.0001f);
									
									float bigf2 = BIGFLOAT;
									int i2tri = -1;
									float s2u,s2v;

									Color4 att2 = Color4(1.0f, 1.0f, 1.0f);
									Matrix4d inst_mat2;
									int instanceID2 = 0;
									inst_mat.setIdentity();

									Color4 attFS = Color4(1.0f, 1.0f, 1.0f);
									float dd = findNonOpacFakeTri(sample_time, ssPoint, dirfromsun, inst_mat2, bigf2, instanceID2, i2tri, s2u,s2v, attFS, false, gotNonSpec);

									if (dd > 0.0f)
									{

										Triangle * s2tri = &(scene->triangles[i2tri]);
										MaterialNox * mat2 = scene->mats[scene->instances[instanceID2].materials[s2tri->matInInst]];

										float t2u, t2v;
										Point3d i2point = s2tri->getPointFromUV(s2u, s2v);
										i2point = inst_mat2 * i2point;
										s2tri->evalTexUV(s2u, s2v, t2u, t2v);

										Vector3d norm2 = s2tri->evalNormal(s2u, s2v);
										norm2 = inst_mat2.getMatrixForNormals() * norm2;
										norm2.normalize();
										Vector3d s2tri_normal_geom = inst_mat2.getMatrixForNormals() * s2tri->normal_geom;
										s2tri_normal_geom.normalize();

										float pdf2mlay;
										MatLayer * m2lay = mat2->getRandomShadeLayer(t2u, t2v, pdf2mlay);
										bool conn_go_on = true;
										if (!m2lay)
											conn_go_on = false;
										if (conn_go_on)
											if (fake_caustic  &&  m2lay->getRough(t2u, t2v)<=maxCausticRough)
												conn_go_on = false;


										Vector3d uvdir2U, uvdir2V;
										s2tri->getTangentSpaceSmooth(inst_mat2, s2u,s2v, uvdir2U, uvdir2V);
										HitData hd2(dirtosun, dirtosun, norm2, s2tri_normal_geom, uvdir2U, uvdir2V, t2u, t2v, dd, -1.0f);
										hd2.adjustNormal();

										Vector3d d2fromsun = ptp->int_point - i2point;
										d2fromsun.normalize();
										Vector3d d2tosun = d2fromsun * -1.0f;
										hd2.out = d2fromsun;

										Color4 brdf1, brdf2;
										float pdf1, pdf2b;
										brdf2 = Color4(0.0f, 0.0f, 0.0f);
										#ifdef EVAL_ALL_MATLAYERS
											if (conn_go_on)
											{
												PTStack * ptn = &(pt[lvl+1]);
												ptn->mat = mat2;
												ptn->hd = hd2;
												float pdf2;
												ptn->evalBrdfPdfMultilayered(brdf2, pdf2, pdf2b);
											}
										#else
											if (conn_go_on)
											{
												brdf2 = m2lay->getBRDF(hd2) * (1.0f/pdf2mlay);
												HitData hd3 = hd2;
												hd3.swapDirections();
												pdf2b = m2lay->getProbability(hd3);
											}
										#endif
										if (!(brdf2.isBlack()  ||  brdf2.isInf()  ||  brdf2.isNaN()))
										{
											ptp->hd.out = d2tosun;
											#ifdef EVAL_ALL_MATLAYERS
												float pdf1b;
												ptp->evalBrdfPdfMultilayered(brdf1, pdf1, pdf1b);
											#else
												brdf1 = ptp->mlay->getBRDF(ptp->hd) * (1.0f/randLayerPdf);
												pdf1 = ptp->mlay->getProbability(ptp->hd);
											#endif
											if (!(brdf1.isBlack()  ||  brdf1.isInf()  ||  brdf1.isNaN()))
											{
												Point3d st2 = ptp->int_point + ptp->normal_geom * (ptp->normal_geom*d2tosun>0 ? 0.0001f : -0.0001f);
												Point3d end2 = i2point + s2tri_normal_geom * (s2tri_normal_geom*d2fromsun>0 ? 0.0001f : -0.0001f);
												Vector3d dir2 = end2-st2;
												float md2 = dir2.normalize_return_old_length();

												Color4 att3;
												int iitritemp;
												Matrix4d inst_mat;
												int instanceID3 = 0;

												bool shadow = isShadowOpacFakeGlass(sample_time, st2, dir2, inst_mat, md2, instanceID3, iitritemp, att3, false);
												if (!shadow)
												{
													float weightMIS = 1.0f;
													float cos_t2 = fabs(s2tri_normal_geom * dir2);
													float cos_t3 = fabs(s2tri_normal_geom * dirfromsun);
													float cos_t4 = fabs(skptri_normal_geom * dirfromsun);
													float pdfBrdf = pdf1 * cos_t2 / (md2*md2) * pdf2b * cos_t4 / (dd*dd);
													float pdfSun  = pdf1 * cos_t2 / (md2*md2) * (1.0f/ss->sunsky->getSunSizeSteradians()) * cos_t4 / (dd*dd);
													float pdfGI   = 1.0f/ss->skyPortalsTotalArea * (1.0f/ss->sunsky->getSunSizeSteradians()) * cos_t3 / (dd*dd);
													weightMIS = pdfGI*pdfGI / (pdfGI*pdfGI + pdfBrdf*pdfBrdf + pdfSun*pdfSun);

													#ifdef IGNORE_FIRST_RAY_FROM_SUN_PORTAL
														weightMIS = 0.0f;
													#endif													

													#ifndef EVAL_ALL_MATLAYERS
														brdf1 *= fabs(ptp->hd.normal_shade*d2tosun);
														brdf2 *= fabs(norm2*d2tosun);
													#endif
													Color4 pathBrdf2 = att3 * att2 * brdf1 * brdf2 * ptp->energy_in * (1.0f/(md2*md2));
													Color4 suncontr = sunenergy * pathBrdf2 * sssAtten  * weightMIS * attFS;
													#ifndef IGNORE_FIRST_RAY_FROM_SUN_PORTAL
														if (!(suncontr.isInf()  ||  suncontr.isNaN()))
															pixelLayers[sunchannel] += suncontr;
														#ifdef DEBUG_LOG_NAN
														else
														{
															char buf[256];
															sprintf_s(buf, 256, "NaN in sun->portal->tri->connection... weight=%f  pathBrdf=%.5f, %.5f, %.5f  atten2=%.5f, %.5f, %.5f   atten3=%.5f, %.5f, %.5f",  
																			weightMIS, ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b, att2.r, att2.g, att2.b, att3.r, att3.g, att3.b);
															Logger::add(buf);
														}
														#endif
													#endif
												}
											}
										}
									}
								}
							}
						}	// END SUN SKYPORTAL GI


						// SKY FROM PORTAL
						if (useskyportals  &&  usesky  &&  !discard_now)
						{
							float pdfRandSkyPortal = 1.0f;
							int skpId = scene->randomSkyPortal(pdfRandSkyPortal);
							SkyPortalMesh * skpMesh = scene->skyportalMeshes[skpId];
							if (skpMesh)
							{
								MeshInstance * mInst = &(scene->instances[skpMesh->instNum]);
								Matrix4d instMat7 = mInst->matrix_transform;
								float pdfRandSkpTri;
								int triInMesh = 0;
								int iskptri = skpMesh->randomTriangle(pdfRandSkpTri, triInMesh);
								if (iskptri >= 0  &&  iskptri<scene->triangles.objCount)
								{
									Triangle * skptri = &(scene->triangles[iskptri]);
									float skptriarea = skpMesh->triAreas[triInMesh];
									float skpu, skpv;
									Point3d skpPoint;
									skptri->randomPoint(skpu, skpv, skpPoint);
									skpPoint = instMat7 * skpPoint;

									Vector3d skptri_normal_geom = skptri->normal_geom;
									skptri_normal_geom = instMat7.getMatrixForNormals() * skptri_normal_geom;
									skptri_normal_geom.normalize();

									Vector3d dirToLight = skpPoint - ptp->int_point;
									float ldist = dirToLight.normalize_return_old_length();

									skpPoint = skpPoint + skptri_normal_geom * (skptri_normal_geom * dirToLight < 0 ? 0.0001f : -0.0001f);
									Point3d spoint = ptp->int_point + ptp->normal_geom * (dirToLight*ptp->normal_geom>0 ? 0.0001f : -0.0001f);
									dirToLight = skpPoint-spoint;
									ldist = dirToLight.normalize_return_old_length();

									ptp->hd.clearFlagDelta();
									ptp->hd.out = dirToLight;
									#ifdef EVAL_ALL_MATLAYERS
										Color4 brdftolight;
										float pdfBrdf, pdfBrdfBack;
										ptp->evalBrdfPdfMultilayered(brdftolight, pdfBrdf, pdfBrdfBack);
									#else
										Color4 brdftolight = ptp->mlay->getBRDF(ptp->hd) * (1.0f/randLayerPdf);
										float pdfBrdf = ptp->mlay->getProbability(ptp->hd);
									#endif
									if (!brdftolight.isBlack())
									{
										Color4 att = Color4(1.0f, 1.0f, 1.0f);
										int iitritemp;
										Matrix4d inst_mat;
										int instanceID4 = 0;

										if (!isShadowOpacFakeGlass(sample_time, spoint, dirToLight, inst_mat, ldist, instanceID4, iitritemp, att, false))
										{
											Color4 skycolor  = ss->sunsky->getColor(dirToLight);
											float pdfSkyPortal = 1.0f / scene->sscene.skyPortalsTotalArea;
											float lcos = fabs(skptri_normal_geom * dirToLight);
											if (lcos>0.001f)
											{
										float rrPdf = max(max(brdftolight.r, brdftolight.g), brdftolight.b) / pdfBrdf;
										if (cur_path_lvl<2)
											rrPdf = 1.0f;
										pdfBrdf *= rrPdf;
												float pdfSkyPortalAng = pdfSkyPortal*ldist*ldist/lcos;
												float weightMIS = pdfSkyPortalAng*pdfSkyPortalAng / (pdfSkyPortalAng*pdfSkyPortalAng + pdfBrdf*pdfBrdf);

												#ifdef ONLY_BRDF
													weightMIS = 0.0f;
												#endif
												#ifdef ONLY_RAND_LIGHT
													weightMIS = 1.0f;
												#endif

												bool lastSpec = isLastSpecular(pt, lvl-1, maxCausticRough);
												if (fake_caustic  &&  lastSpec  &&  ptp->rough>maxCausticRough)		//WTF?
													weightMIS = 1.0f;
												#ifdef EVAL_ALL_MATLAYERS
													float ccos = 1.0f;
												#else
													float ccos = fabs(ptp->hd.normal_shade * dirToLight);
												#endif
												float G = lcos  * ccos / (ldist*ldist);
												pdfSkyPortal = 1.0f / ss->skyPortalsTotalArea;

												Color4 skycontr = skycolor * sssAtten * ptp->energy_in * att * brdftolight * (G/pdfSkyPortal) * weightMIS;
												if (!(skycontr.isInf()  ||  skycontr.isNaN()))
												{
													if (isCaustic1)
														pixelLayersGI[0] += skycontr;
													else
														pixelLayers[0] += skycontr;
												}
												#ifdef DEBUG_LOG_NAN
												else
												{
													char buf[256];
													sprintf_s(buf, 256, "NaN in sky portal-tri connection... weight=%f  pathBrdf=%.5f, %.5f, %.5f  atten=%.5f, %.5f, %.5f   G=%.5f",  
																	weightMIS, ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b, att.r, att.g, att.b, G);
													Logger::add(buf);
												}
												#endif
											}
										}
									}
								}
							}
						}	// END SKY FROM PORTAL

						// RANDOM EMITTER
						bool discard_emitter = false;
						if (ptp->rough == 0.0f)
							discard_emitter = true;
						if (ss->lightMeshes->objCount<=0)
							discard_emitter = true;
						bool isCausticJoin = isCaustic(pt, lvl, maxCausticRough);
						if (fake_caustic && isCausticJoin)
							discard_emitter = true;


						if (!discard_emitter)
						{
							RIS_PT ris_array[RIS_MAXTRIES];
							int ris_tries = min(RIS_MAXTRIES, ris_max_samples_set);
							if (lvl>0)
								ris_tries = 1;
							float ris_weights_total = 0.0f;
							float ris_weights_until_now = 0.0f;
							for (int t=0; t<ris_tries; t++)
							{
								// if no contribution or error
								ris_array[t].contribution = Color4(0,0,0);
								ris_array[t].weight = 0.0f;
								ris_array[t].mis = 0.0f;
								ris_array[t].weights_so_far = ris_weights_until_now;
								ris_array[t].blendlayer = 0;

								float lpdf1, lpdf2, llayerpdf;
								int ilm, ilt;
								ilm = ss->randomLightMesh(lpdf1);
								LightMesh *lm = (*ss->lightMeshes)[ilm];
								if (ilm >= 0 && lm)
									ilt = lm->randomTriangle(lpdf2);
								else
									ilt = -1;

								if (fake_caustic  &&  ptp->rough<=maxCausticRough  &&  gotNonSpec)
									ilt = -1;

								if (ilt >= 0)
								{
									MeshInstance * mInst1 = &(scene->instances[lm->instNum]);
									Matrix4d inst_mat6 = mInst1->matrix_transform;
									Triangle ltri = (scene->triangles[ilt]);
									MatLayer * lmlay = NULL;
									MaterialNox * lmat = scene->mats[mInst1->materials[ltri.matInInst]];
									float lu, lv;
									Point3d lPoint;
									ltri.randomPoint(lu, lv, lPoint);
									lmlay = lmat->getRandomEmissionLayer(lu, lv, llayerpdf);
									if (lmlay)
									{
										Vector3d lnormal = ltri.evalNormal(lu, lv);
										lnormal = inst_mat6.getMatrixForNormals() * lnormal;
										lnormal.normalize();
										lPoint = inst_mat6 * lPoint;
										Vector3d dirToLight = lPoint - ptp->int_point;
										lPoint = lPoint + lnormal * (dirToLight*lnormal<0 ?  0.0001f : -0.0001f);
										dirToLight = lPoint - ptp->int_point;
										float ldist = dirToLight.normalize_return_old_length();

										bool thereIsEmissionInDirection = false;
										float iesmpl = 1.0f;
										if (lmlay->ies  &&  lmlay->ies->isValid())
										{
											float ncos = min(1.0f, max(-1.0f, -dirToLight * lnormal));
											float langle = acos(ncos);
											iesmpl = lmlay->ies->getEmission(0.0f, langle);
											thereIsEmissionInDirection = (iesmpl>0.0f);
											iesmpl *= 1.0f / fabs(ncos);
										}
										else
											thereIsEmissionInDirection = (dirToLight * lnormal < 0);
										if (thereIsEmissionInDirection)
										{
											ptp->hd.out = dirToLight;
											ptp->hd.clearFlagDelta();
											#ifdef EVAL_ALL_MATLAYERS
												float brdfPdf, brdfPdfBack;
												Color4 brdftolight;
												ptp->evalBrdfPdfMultilayered(brdftolight, brdfPdf, brdfPdfBack);
											#else
												Color4 brdftolight = ptp->mlay->getBRDF(ptp->hd) * (1.0f/randLayerPdf);
												float brdfPdf = ptp->mlay->getProbability(ptp->hd);
											#endif
											if (!brdftolight.isBlack())
											{
												Point3d spoint = ptp->int_point + ptp->normal_geom * (dirToLight*ptp->normal_geom>0 ? 0.0001f : -0.0001f);
												ldist = (spoint-lPoint).length();

												Color4 att = Color4(1.0f, 1.0f, 1.0f);

												{
													float rCosOut = ptp->hd.normal_shade * dirToLight;
													float lcos = fabs(lnormal * dirToLight);// * -1.0f;

													ldist = (spoint-lPoint).length();
													float llpdf = ss->lightMeshPowers[ilm]/ss->allLightsPower * 1/(*ss->lightMeshes)[ilm]->totalArea;
													float llpdf2 = ss->lightMeshPowersPow[ilm]/ss->allLightsPowerPow * 1/(*ss->lightMeshes)[ilm]->totalArea;
													float brdfPdfToPointPdf = brdfPdf * fabs(lcos)/(ldist*ldist);
													#ifdef FIRST_INT_MULTIPLE_RAYS_AFFECTS_HEURISTICS
														float mplh = lvl==0 ? sqrt((float)first_samples_max) : 1.0f;
														float weightMIS = llpdf2*llpdf2 / (brdfPdfToPointPdf*brdfPdfToPointPdf*mplh + llpdf2*llpdf2);
													#else
														float weightMIS = llpdf2*llpdf2 / (brdfPdfToPointPdf*brdfPdfToPointPdf + llpdf2*llpdf2);
													#endif

													#ifdef ONLY_BRDF
														weightMIS = 0.0f;
													#endif
													#ifdef ONLY_RAND_LIGHT
														weightMIS = 1.0f;
													#endif


													float tlu, tlv;
													ltri.evalTexUV(lu, lv, tlu, tlv);
													#ifdef EVAL_ALL_MATLAYERS
														float cosMpl = 1;
													#else
														float cosMpl = (ptp->hd.isFlagDelta() ? 1.0f : fabs(rCosOut));
													#endif
													float invWg = 1.0f;
													if (lmlay->emissInvisible)
														invWg = evalWeightForInvisibleLight(pt, lvl);
													float opacity = lmat->getOpacity(tlu, tlv);
													Color4 lightenergy = lmlay->getLightColor(tlu, tlv) * (ss->lightMeshPowers[ilm] * (ss->allLightsPowerPow/ss->lightMeshPowersPow[ilm]) 
																				* opacity * iesmpl * cosMpl * lcos / ldist/ldist);
													Color4 lightcontr = lightenergy * sssAtten * brdftolight * att * ptp->energy_in * invWg;
													if (!(lightcontr.isInf()  ||  lightcontr.isNaN()  ||  _isnan(weightMIS) ))
													{
														float ris_w = (lightcontr.r + lightcontr.g + lightcontr.b);
														ris_weights_until_now += ris_w;
														ris_weights_total = ris_weights_until_now;
														ris_array[t].contribution = lightcontr;
														ris_array[t].weight = ris_w;
														ris_array[t].mis = weightMIS;
														ris_array[t].weights_so_far = ris_weights_until_now;
														ris_array[t].blendlayer = lmat->blendIndex;

														ris_array[t].shch_dirToLight = dirToLight;
														ris_array[t].shch_ldist = ldist;
														ris_array[t].shch_spoint = spoint;
													}
													else
													{
														#ifdef DEBUG_LOG_NAN
															char buf[512];
															sprintf_s(buf, 512, "NaN in lightmesh-tri connection... weight=%f  pathBrdf=%.5f, %.5f, %.5f  atten=%.5f, %.5f, %.5f   lightenergy=%.5f, %.5f, %.5f   brdf=%.5f, %.5f, %.5f",
																			weightMIS, ptp->energy_in.r, ptp->energy_in.g, ptp->energy_in.b, att.r, att.g, att.b, lightenergy.r, lightenergy.g, lightenergy.b,
																			brdftolight.r, brdftolight.g, brdftolight.b);
															Logger::add(buf);
														#endif
													}
												}
											}
										}
									}
								}
							}	// end ris tries
							if  (ris_weights_total>0.0f)
							{
								int chosen = -1;
								#ifdef RAND_PER_THREAD
									float a = rg->getRandomFloat();
								#else
									float a = rand()/(float)RAND_MAX;
								#endif
								float aw = a*ris_weights_total;
								for (int i=0; i<ris_tries; i++)
								{
									chosen = i;
									if (ris_array[i].weights_so_far > aw)
										break;
								}

								Color4 att = Color4(1.0f, 1.0f, 1.0f);
								int iitri = -1;
								Matrix4d inst_mat;
								int instanceID5 = 0;
								bool isShadow = isShadowOpacFakeGlass(sample_time, ris_array[chosen].shch_spoint, ris_array[chosen].shch_dirToLight, inst_mat, ris_array[chosen].shch_ldist, instanceID5, iitri, att, false);
								if (!isShadow)
								{
									float ris_pdf = ris_array[chosen].weight / ris_weights_total * ris_tries;
									Color4 toAdd = ris_array[chosen].contribution * ris_array[chosen].mis * (1.0f/ris_pdf) * att;
									if (isCaustic1)
										pixelLayersGI[ris_array[chosen].blendlayer] += toAdd;
									else
										pixelLayers[ris_array[chosen].blendlayer] += toAdd;
								}

							}
						}	// END RANDOM EMITTER


						// MATTE SHADOW
						if (isMatte)
						{
							// env map self shadow
							if (scene->env.enabled  &&  scene->env.tex.isValid())
							{
								float dpdf = 0.0f;
								ptp->hd.clearFlagsAll();
								Vector3d smpldir = ptp->mlay->randomNewDirectionLambert2(ptp->hd, dpdf);
								float dcos = fabs(smpldir * ptp->hd.normal_geom);
								Color4 envcolor = scene->env.getColor(smpldir);
								Color4 scolor = envcolor * envmpl;

								Matrix4d inst_mat9;
								float dist = BIGFLOAT;
								int tinstid = -1, tskptri;
								Color4 tatt = Color4(1,1,1);
								Point3d tpoint = ptp->int_point + ptp->normal_geom* (smpldir*ptp->normal_geom>0 ? 0.0001f : -0.0001f);
								bool isShadow = isShadowOpacFakeGlass(sample_time, tpoint, smpldir, inst_mat9, dist, tinstid, tskptri, tatt, false);
								Color4 contr = scolor * dcos * (1.0f/dpdf);
								if (!isShadow)
								{
									matteAdded += contr;
									pt_go_on = false;
								}
								else
								{
									matteWasted += contr;
									pt_go_on = true;
									dirOut = smpldir;
									ptp->hd.out = smpldir;
									ipoint = ptp->int_point + ptp->normal_geom * (ptp->normal_geom*smpldir > 0 ? 0.0001f : -0.0001f);
									ptp->pdf_brdf = dpdf;
									ptp->pdf_rr = 1.0f;
									ptp->brdf = Color4(1,1,1) * sssAtten * (1/PI) * dcos;
									//ptp->brdf *= PI;
								}
							}
							else
								pt_go_on = false;

							// shadow weight color
							Color4 ew;
							Color4 msum = matteAdded + matteWasted;
							ew.r = matteAdded.r / (msum.r>0.0 ? msum.r : 1.0f);
							ew.g = matteAdded.g / (msum.g>0.0 ? msum.g : 1.0f);
							ew.b = matteAdded.b / (msum.b>0.0 ? msum.b : 1.0f);
							// add contribution
							pixelLayers[envchannel] += ptp->energy_in * matteCol * ew;

							ptp->brdf.r *= matteCol.r/(msum.r>0.0 ? msum.r : 0.0001f);
							ptp->brdf.g *= matteCol.g/(msum.g>0.0 ? msum.g : 0.0001f);
							ptp->brdf.b *= matteCol.b/(msum.b>0.0 ? msum.b : 0.0001f);
							continue;

						}	// END MATTE SHADOW


						if (isMatte)
							Logger::add("this should not happen");

						// ---- PREPARE FOR NEXT PATH

						#ifdef RR_AFTER_RANDOM
							float rrPdf = 1.0f;
							if (1)
						#else
							// RUSSIAN ROULETTE
							#ifdef RAND_PER_THREAD
								float rtemp = rg->getRandomFloat();
							#else
								float rtemp = rand()/(float)RAND_MAX;
							#endif
							float rrPdf = rtr->rayReflectionProbability * 0.01f;
							if (ptp->rough == 0.0f)
								rrPdf = 1.0f;
							if (rtemp < rrPdf)
						#endif
						{
							float newDirPdf;
							ptp->hd.clearFlagInvalidRandom();

							Vector3d newdir;
							bool failedNewDir = false;
							Color4 newBrdf = Color4(1.0f, 1.0f, 1.0f);
							float cosMpl = 1.0f;

							if (ptp->rough>maxCausticRough)
								gotNonSpec = true;

							if (fake_caustic  &&  ptp->rough<=maxCausticRough  &&  gotNonSpec  &&  ptp->mlay->transmOn)
							{	// -- force fake glass through
								newdir = ptp->hd.in * -1;
								newDirPdf = 1.0f;
								cosMpl = 1.0f;
								newBrdf = Color4(1.0f, 1.0f, 1.0f);

								float f = ptp->mlay->fresnel.getValueFresnelFromCos(fabs(ptp->hd.normal_shade*ptp->hd.in));
								Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
								if (ptp->hd.in*ptp->hd.normal_shade < 0   &&   ptp->mlay->absOn)
								{	
									absorption.r = pow(1.0f/max(0.01f, ptp->mlay->gAbsColor.r), -ptp->hd.lastDist/ptp->mlay->absDist);
									absorption.g = pow(1.0f/max(0.01f, ptp->mlay->gAbsColor.g), -ptp->hd.lastDist/ptp->mlay->absDist);
									absorption.b = pow(1.0f/max(0.01f, ptp->mlay->gAbsColor.b), -ptp->hd.lastDist/ptp->mlay->absDist);
								}
								newBrdf = ptp->mlay->getTransmissionColor(ptp->hd.tU, ptp->hd.tV) * absorption * (1-f);
								ptp->hd.setFlagGhostReflection();
								ptp->hd.setFlagDelta();
								ptp->fake_transmission = true;
							}
							else
							{
								if (ptp->mlay->fakeGlass  &&  ptp->rough==0.0f  &&  ptp->mlay->transmOn  &&  gotNonSpec)
								{	// -- there was a fake glass
									newdir = ptp->hd.in * -1;
									ptp->hd.out = newdir;
									newBrdf = ptp->mlay->getBRDF(ptp->hd);
									ptp->hd.setFlagGhostReflection();
									ptp->hd.setFlagDelta();
									cosMpl = 1.0f;
									newDirPdf = 1.0f;
									ptp->fake_transmission = true;
								}
								else
								{	// -- normal random dir
									ptp->hd.clearFlagDelta();
									ptp->hd.clearFlagGhostReflection();
									newdir = ptp->mlay->randomNewDirection(ptp->hd, newDirPdf);
									failedNewDir = ptp->hd.isFlagInvalidRandom();
									ptp->hd.out = newdir;
									newBrdf = ptp->mlay->getBRDF(ptp->hd);
									cosMpl = (ptp->hd.isFlagDelta() ? 1.0f : fabs(newdir*ptp->hd.normal_shade));
									newDirPdf = ptp->mlay->getProbability(ptp->hd);
									ptp->fake_transmission = false;
									#ifdef EVAL_ALL_MATLAYERS
										float brdfPdfBack;
										ptp->evalBrdfPdfMultilayered(newBrdf, newDirPdf, brdfPdfBack);
									#endif
								}
							}

							if (!ptp->hd.isFlagDelta())
								sum_d = 0.0f;

							if (newDirPdf > 0.001f   &&   !failedNewDir)
							{
								if (spectr_pos<=0.0f   &&   ptp->hd.spectr_pos>0.0f)
									newBrdf *= 4.0f;
								spectr_pos = ptp->hd.spectr_pos;

								#ifdef EVAL_ALL_MATLAYERS
									ptp->brdf = newBrdf * (1.0f/rrPdf) * sssAtten;
								#else
									ptp->brdf = newBrdf * (1.0f/rrPdf/randLayerPdf) * cosMpl * sssAtten;
								#endif
								ptp->pdf_brdf = newDirPdf;
								dirOut = newdir;
								ptp->hd.out = newdir;
								ipoint = ptp->int_point + ptp->normal_geom * (ptp->normal_geom*newdir > 0 ? 0.0001f : -0.0001f);
								pt_go_on = true;
								if (cur_path_lvl > 64)
									pt_go_on = false;
							}
							else
							{	// -- new dir random failed
								pt_go_on = false;
							}

							#ifdef RR_AFTER_RANDOM
							if (pt_go_on)	//  RR brdf/pdf
							{
								#ifdef RAND_PER_THREAD
									float rtemp = rg->getRandomFloat();
								#else
									float rtemp = rand()/(float)RAND_MAX;
								#endif
								rrPdf = max(max(ptp->brdf.r, ptp->brdf.g), ptp->brdf.b) / ptp->pdf_brdf;
								rrPdf = min(rrPdf, 1.0f);
								if (cur_path_lvl<2)
									rrPdf = 1.0f;
								if (ptp->rough==0.0f)
									rrPdf = 1.0f;
								if (rtemp>=rrPdf)
									pt_go_on = false;
								else
								{
									ptp->brdf *= 1.0f/rrPdf;
									ptp->pdf_rr = rrPdf;
								}
							}
							#endif
						}
						else	// RR (const) stop by random
						{
							pt_go_on = false;
						}
					}	// END SHADE LAYER TYPE


					// IT WAS EMITTER
					if (ptp->mlay->type == MatLayer::TYPE_EMITTER)
					{
						bool isSecondary, discard_now = false; 
						bool isCaustic1 = checkForCaustic(arrRough, cur_path_lvl-1, isSecondary, maxCausticRough);
						if (isSecondary && discard_secondary_caustic)
							discard_now = true;

						bool discard_hit_emitter = false;
						bool is_path_caustic = isCaustic(pt, lvl-1, maxCausticRough);
						bool is_path_eye_camera = isLightEyePath(pt, lvl-1);
						if (is_path_caustic  &&  fake_caustic  &&  !is_path_eye_camera)
							discard_hit_emitter = true;


						bool thereIsEmissionInDirection = false;
						float iesmpl = 1.0f;
						if (ptp->mlay->ies  &&  ptp->mlay->ies->isValid())
						{
							float ncos = min(1.0f, max(-1.0f, -dirOut * ptp->hd.normal_shade));
							float langle = acos(ncos);
							iesmpl = ptp->mlay->ies->getEmission(0.0f, langle);
							thereIsEmissionInDirection = (iesmpl>0.0f);
							iesmpl *= 1.0f / fabs(ncos);
						}
						else
							thereIsEmissionInDirection = (ptp->hd.normal_shade*dirOut<0);


						if (thereIsEmissionInDirection  &&  !discard_hit_emitter  &&  !discard_now)
						{
							float invWg = 1.0f;
							if (ptp->mlay->emissInvisible)
								invWg = evalWeightForInvisibleLight(pt, lvl-1);
							
							float dist2 = sum_d * sum_d;
							float randLMPdf = ss->evalPDF(instanceID, itri);
							float lastNonSpecReflPdf;
							findLastNonSpecPdf(pt, lvl-1, lastNonSpecReflPdf);
							float brdfPdfToPointPdf = lastNonSpecReflPdf * fabs(ptp->normal_geom*dirOut)/dist2;
							#ifdef FIRST_INT_MULTIPLE_RAYS_AFFECTS_HEURISTICS
								float mplh = lvl==1 ? sqrt((float)first_samples_max) : 1.0f;
								float weightMIS = brdfPdfToPointPdf*brdfPdfToPointPdf * mplh / (brdfPdfToPointPdf*brdfPdfToPointPdf * mplh + randLMPdf*randLMPdf);
							#else
								float weightMIS = brdfPdfToPointPdf*brdfPdfToPointPdf / (brdfPdfToPointPdf*brdfPdfToPointPdf + randLMPdf*randLMPdf);
							#endif
							if (lvl>0 && isLastSpecular(pt, lvl-1, maxCausticRough)  &&  !pt[lvl-1].hd.isFlagGhostReflection())
								weightMIS = 1.0f;
							if (cur_path_lvl == 1)
								weightMIS = 1.0f;

							#ifdef ONLY_BRDF
								weightMIS = 1.0f;
							#endif
							#ifdef ONLY_RAND_LIGHT
								weightMIS = 0.0f;
							#endif

							if (is_path_eye_camera)
								weightMIS = 1.0f;

							Color4 emc = ptp->mlay->getLightColor(ptp->hd.tU, ptp->hd.tV) * ptp->mlay->getIlluminance(ss, instanceID, itri);
							Color4 lightcontr = emc * ptp->energy_in * weightMIS * iesmpl * invWg;
							if (!(lightcontr.isInf()  ||  lightcontr.isNaN()))
							{
								if (isCaustic1)
									pixelLayersGI[ptp->mat->blendIndex] += lightcontr;
								else
									pixelLayers[ptp->mat->blendIndex] += lightcontr;
							}
							#ifdef DEBUG_LOG_NAN
							else
								Logger::add("NaN in brdf->lightmesh");
							#endif
						}

						if (ptp->mlay->emissInvisible)
						{
							pt_go_on = true;
							ptp->brdf = Color4(1,1,1);
							ptp->pdf_brdf = 1.0f;
							ptp->pdf_rr = 1.0f;
							ptp->hd.out = dirOut;
							ipoint = ptp->int_point + ptp->normal_geom * (ptp->normal_geom*dirOut > 0 ? 0.0001f : -0.0001f);
							continue;
						}

						pt_go_on = false;
					}	// END EMITTER LAYER

				}	// END NON-SKYPORTAL
			}	// END INTERSECTED PART
		}	// END WHOLE PATH


		for (int i=0; i<16; i++)
		{
			if (blendLayersActive[i])
			{
				cam->blendBuffDirect[i].addPixelColor((int)(dX+0.5f), (int)(dY+0.5f), pixelLayers[i], true);
				cam->blendBuffGI[i].addPixelColor((int)(dX+0.5f), (int)(dY+0.5f), pixelLayersGI[i], true);
				pixelLayers[i] = Color4(0.0f, 0.0f, 0.0f);
				pixelLayersGI[i] = Color4(0.0f, 0.0f, 0.0f);
			}
		}

		ss->perfCounter++;
	}
}

//-----------------------------------------------------------------------------------------------------

bool PathTracer3::randomNewDirection(PTStack *pt, int lvl, Vector3d &dirOut, Point3d &ipoint)
{
	Raytracer * rtr = Raytracer::getInstance();
	RandomGenerator * rg = rtr->getRandomGeneratorForThread();

	bool pt_go_on = true;

	PTStack * ptp = &(pt[lvl]);
	#ifdef RR_AFTER_RANDOM
		float rrPdf = 1.0f;
		if (1)
	#else
		// RUSSIAN ROULETTE
		#ifdef RAND_PER_THREAD
			float rtemp = rg->getRandomFloat();
		#else
			float rtemp = rand()/(float)RAND_MAX;
		#endif
		float rrPdf = rtr->rayReflectionProbability * 0.01f;
		if (ptp->rough == 0.0f)
			rrPdf = 1.0f;
		if (rtemp < rrPdf)
	#endif
	{
		// PREPARE FOR NEXT PATH
		float newDirPdf;
		ptp->hd.clearFlagInvalidRandom();

		Vector3d newdir;
		bool failedNewDir = false;
		Color4 newBrdf = Color4(1.0f, 1.0f, 1.0f);
		float cosMpl = 1.0f;

		bool gotNonSpec = checkGotNonSpec(pt, lvl-1, maxCausticRough);
		if (fake_caustic  &&  ptp->rough<=maxCausticRough  &&  gotNonSpec  &&  ptp->mlay->transmOn)
		{	// -- force fake glass through
			newdir = ptp->hd.in * -1;
			newDirPdf = 1.0f;
			cosMpl = 1.0f;
			newBrdf = Color4(1.0f, 1.0f, 1.0f);

			float f = ptp->mlay->fresnel.getValueFresnelFromCos(fabs(ptp->hd.normal_shade*ptp->hd.in));
			Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
			if (ptp->hd.in*ptp->hd.normal_shade < 0   &&   ptp->mlay->absOn)
			{	
				absorption.r = pow(1.0f/max(0.01f, ptp->mlay->gAbsColor.r), -ptp->hd.lastDist/ptp->mlay->absDist);
				absorption.g = pow(1.0f/max(0.01f, ptp->mlay->gAbsColor.g), -ptp->hd.lastDist/ptp->mlay->absDist);
				absorption.b = pow(1.0f/max(0.01f, ptp->mlay->gAbsColor.b), -ptp->hd.lastDist/ptp->mlay->absDist);
			}
			newBrdf = ptp->mlay->getTransmissionColor(ptp->hd.tU, ptp->hd.tV) * absorption * (1-f);
			ptp->hd.setFlagGhostReflection();
			ptp->hd.setFlagDelta();
			ptp->fake_transmission = true;
		}
		else
		{
			if (ptp->mlay->fakeGlass  &&  ptp->rough==0.0f  &&  ptp->mlay->transmOn  &&  gotNonSpec)
			{	// -- there was a fake glass
				newdir = ptp->hd.in * -1;
				ptp->hd.out = newdir;
				newBrdf = ptp->mlay->getBRDF(ptp->hd);
				ptp->hd.setFlagGhostReflection();
				ptp->hd.setFlagDelta();
				cosMpl = 1.0f;
				newDirPdf = 1.0f;
				ptp->fake_transmission = true;
			}
			else
			{	// -- normal random dir
				newdir = ptp->mlay->randomNewDirection(ptp->hd, newDirPdf);
				failedNewDir = ptp->hd.isFlagInvalidRandom();
				ptp->hd.clearFlagDelta();
				ptp->hd.clearFlagGhostReflection();
				ptp->hd.out = newdir;
				newBrdf = ptp->mlay->getBRDF(ptp->hd);
				cosMpl = (ptp->hd.isFlagDelta() ? 1.0f : fabs(newdir*ptp->hd.normal_shade));
				newDirPdf = ptp->mlay->getProbability(ptp->hd);
				ptp->fake_transmission = false;
			}
		}

		if (newDirPdf > 0.001f   &&   !failedNewDir)
		{
			float randLayerPdf = 1;	// ZLE
			ptp->brdf = newBrdf * (1.0f/rrPdf/randLayerPdf) * cosMpl;
			ptp->updatePdfBrdfMultilayered();
			ptp->pdf_brdf = newDirPdf;
			dirOut = newdir;
			ipoint = ptp->int_point + ptp->normal_geom * (ptp->normal_geom*newdir > 0 ? 0.0001f : -0.0001f);
			pt_go_on = true;
			if (lvl >= 64)
				pt_go_on = false;
		}
		else
		{	// -- new dir random failed
			pt_go_on = false;
		}

		#ifdef RR_AFTER_RANDOM
		if (pt_go_on)	// -- RR brdf/pdf
		{
			#ifdef RAND_PER_THREAD
				float rtemp = rg->getRandomFloat();
			#else
				float rtemp = rand()/(float)RAND_MAX;
			#endif
			rrPdf = max(max(ptp->brdf.r, ptp->brdf.g), ptp->brdf.b) / ptp->pdf_brdf;
			if (lvl<2)
				rrPdf = 1.0f;
			if (ptp->rough==0.0f)
				rrPdf = 1.0f;
			if (rtemp>=rrPdf)
				pt_go_on = false;
			else
				ptp->brdf *= 1.0f/rrPdf;
		}
		#endif
	}
	else	// RR (const) stop by random
	{
		pt_go_on = false;
	}
	return pt_go_on;
}

//-----------------------------------------------------------------------------------------------------

float PathTracer3::findNonOpacFakeTri(const float sample_time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, float &uu, float &vv, Color4 &attenuation, bool stopOnPortal, bool wasNonSpec)
{
	attenuation = Color4(1.0f, 1.0f, 1.0f);
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	Point3d stPoint = origin;
	float md = maxDist;

	Point3d pEnd;
	Vector3d dir = direction;
	bool evaldir = false;
	if (maxDist<BIGFLOAT)
	{
		evaldir = true;
		pEnd = origin + direction * maxDist;
	}

	float sum_d = 0.0f;

	int ii = 0;
	bool checked = false;
	while (!checked)
	{
		if (ii++>1000)
		{
			Logger::add("findNonOpacFakeTri stucked");
			return -1;
		}

		float u,v, d;
		int itri;
		
		if (evaldir)
		{
			dir = pEnd - stPoint;
			md = dir.normalize_return_old_length();
		}

		Matrix4d inst_mat;
		inst_mat.setIdentity();
		int instID = 0;

		if (rtr->use_embree)
			d = intersectEmbree(scene, stPoint, dir, inst_mat, md, instID, itri, u, v);
		else
			d =	rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(sample_time, stPoint, dir, inst_mat, md, instID, itri, u, v);
		if (d<0 || itri<0)
		{
			return -1;
		}

		sum_d += d;

		Triangle tri = (scene->triangles[itri]);
		MaterialNox * fmat = scene->mats[scene->instances[instID].materials[tri.matInInst]];
		float tU, tV;
		tri.evalTexUV(u, v, tU, tV);
		Point3d inters = tri.getPointFromUV(u,v);
		inters = inst_mat * inters;
		Vector3d tri_normal_geom = inst_mat.getMatrixForNormals() * tri.normal_geom;
		tri_normal_geom.normalize();

		if (fmat->isSkyPortal)
		{
			if (stopOnPortal)
			{
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat; 
				instanceID = instID;
				return sum_d;
			}

			stPoint = inters + tri_normal_geom * ((direction*tri_normal_geom>0) ? 0.0001f : -0.0001f);
			continue;
		}
		
		bool use_fake_caustic = false;
		if (fake_caustic  &&  wasNonSpec)
			for (int i=0; i<fmat->layers.objCount; i++)
				if (fmat->layers[i].transmOn  &&  fmat->layers[i].getRough(tU, tV) <= maxCausticRough)
					use_fake_caustic = true;

		float opacity = fmat->getOpacity(tU, tV);
		if (!fmat->hasFakeGlass   &&   opacity>=1.0f   &&   !use_fake_caustic)
		{
			iitri = itri;
			uu = u;
			vv = v;
			instMatrix = inst_mat; 
			instanceID = instID;
			return sum_d;
		}
			

		Vector3d uvdirU, uvdirV;
		tri.getTangentSpaceSmooth(inst_mat, u,v, uvdirU, uvdirV);

		Vector3d normal = tri.evalNormal(u, v);
		normal = inst_mat.getMatrixForNormals() * normal;
		normal.normalize();

		HitData hd(direction*-1.0f, direction, normal, tri_normal_geom, uvdirU, uvdirV, tU, tV, 0.0f, -1.0f);
		hd.adjustNormal();
		hd.lastDist = d;

		float fpdf = 1;
		MatLayer * flay;
		if (use_fake_caustic)
			flay = fmat->getRandomGlassLayer(tU, tV, fpdf, maxCausticRough);
		else
			flay = fmat->getRandomFakeGlassLayer(tU, tV, fpdf);
		if (flay  &&  fpdf>0.0001f)
		{
			Color4 fbrdf = Color4(1,1,1);
			if (use_fake_caustic)
			{
				Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
				if (hd.in*hd.normal_shade < 0  &&  flay->absOn)
				{	
					absorption.r = pow(1.0f/max(0.01f, flay->gAbsColor.r), -hd.lastDist/flay->absDist);
					absorption.g = pow(1.0f/max(0.01f, flay->gAbsColor.g), -hd.lastDist/flay->absDist);
					absorption.b = pow(1.0f/max(0.01f, flay->gAbsColor.b), -hd.lastDist/flay->absDist);
					absorption = flay->gAbsColor;
				}
				fbrdf = flay->getColor90(hd.tU, hd.tV) * absorption * (1-flay->fresnel.getValueFresnelFromCos(fabs(hd.normal_shade*hd.in)));
			}
			else
				fbrdf = flay->getBRDF(hd);
			
			float pbpdf = 1.0f;

			fbrdf = fbrdf * (1.0f/fpdf) * pbpdf * opacity + Color4(1,1,1) * (1.0f-opacity);
			if (fbrdf.isBlack()  ||   fbrdf.isInf()   ||   fbrdf.isNaN())
			{
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat; 
				instanceID = instID;
				return sum_d;
			}
			attenuation *= fbrdf;
		}
		else
		{
			attenuation *= 1.0f-opacity;
		}

		stPoint = inters + normal * ((direction*normal>0) ? 0.0001f : -0.0001f);
	}

	return -1;
}

//-----------------------------------------------------------------------------------------------------

bool PathTracer3::isShadowOpacFakeGlass(const float sample_time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, Color4 &attenuation, bool stopOnPortal)
{
	attenuation = Color4(1.0f, 1.0f, 1.0f);
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	
	Point3d stPoint = origin;
	float md = maxDist;
	iitri = -1;
	float sum_d = 0.0f;

	Point3d pEnd;
	Vector3d dir = direction;
	bool evaldir = false;
	if (maxDist<BIGFLOAT)
	{
		evaldir = true;
		pEnd = origin + direction * maxDist;
	}
	Vector3d geom_normal = direction;

	bool checked = false;
	int ii = 0;
	while (!checked)
	{
		if (ii++>1000)
		{
			Logger::add("isShadowOpacFakeGlass stucked");
			return false;
		}

		float u,v, d;
		int itri;
		
		if (evaldir)
		{
			dir = pEnd - stPoint;
			md = dir.normalize_return_old_length();
			md -= 0.0001f;
			float dw = dir * geom_normal;
			dir = dir * dw + direction * (1.0f-dw);
			dir.normalize();
		}

		Matrix4d inst_mat;
		inst_mat.setIdentity();
		int instID = 0;

		if (rtr->use_embree)
			d = intersectEmbree(scene, stPoint, dir, inst_mat, md, instID, itri, u,v);
		else
			d = rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(sample_time, stPoint, dir, inst_mat, md, instID, itri, u,v);
		if (d<0 || itri<0)
			return false;

		sum_d += d;

		Triangle tri = (scene->triangles[itri]);
		geom_normal = inst_mat.getMatrixForNormals() * tri.normal_geom;
		geom_normal.normalize();

		MaterialNox * fmat = scene->mats[scene->instances[instID].materials[tri.matInInst]];
		float tU, tV;
		tri.evalTexUV(u, v, tU, tV);
		Point3d inters = tri.getPointFromUV(u,v);
		inters = inst_mat * inters;
		Vector3d normal = inst_mat.getMatrixForNormals() * tri.evalNormal(u, v);
		normal.normalize();

		if (fmat->isSkyPortal)
		{
			if (stopOnPortal)
			{
				iitri = itri;
				instMatrix = inst_mat; 
				instanceID = instID;
				maxDist = sum_d;
				return true;
			}

			stPoint = inters + geom_normal * ((dir*geom_normal>0) ? 0.0001f : -0.0001f);
			continue;
		}

		if (fmat->hasInvisibleEmitter)
		{
			stPoint = inters + geom_normal * ((direction*geom_normal>0) ? 0.0001f : -0.0001f);
			continue;
		}

		bool use_fake_caustic = false;
		if (fake_caustic)
			for (int i=0; i<fmat->layers.objCount; i++)
				if (fmat->layers[i].transmOn  &&  fmat->layers[i].getRough(tU, tV) <= maxCausticRough)
					use_fake_caustic = true;

		float opacity = fmat->getOpacity(tU, tV);
		if (!fmat->hasFakeGlass   &&   opacity>=1.0f   &&   !use_fake_caustic)
		{
			iitri = itri;
			instMatrix = inst_mat; 
			instanceID = instID;
			maxDist = sum_d;
			return true;
		}


		Vector3d uvdirU, uvdirV;
		tri.getTangentSpaceSmooth(inst_mat, u,v, uvdirU, uvdirV);

		HitData hd(direction*-1.0f, direction, normal, geom_normal, uvdirU, uvdirV, tU, tV, 0.0f, -1.0f);
		hd.adjustNormal();
		hd.lastDist = d;

		float fpdf = 1;
		MatLayer * flay = NULL;
		if (use_fake_caustic)
			flay = fmat->getRandomGlassLayer(tU, tV, fpdf, maxCausticRough);
		else
			flay = fmat->getRandomFakeGlassLayer(tU, tV, fpdf);
		if (flay  &&  fpdf>0.0001f)
		{
			Color4 fbrdf = Color4(1,1,1);
			if (use_fake_caustic)
			{
				Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
				if (hd.in*hd.normal_shade < 0  &&  flay->absOn)
				{	
					absorption.r = pow(1.0f/max(0.01f, flay->gAbsColor.r), -hd.lastDist/flay->absDist);
					absorption.g = pow(1.0f/max(0.01f, flay->gAbsColor.g), -hd.lastDist/flay->absDist);
					absorption.b = pow(1.0f/max(0.01f, flay->gAbsColor.b), -hd.lastDist/flay->absDist);
				}
				fbrdf = flay->getColor90(hd.tU, hd.tV) * absorption * (1-flay->fresnel.getValueFresnelFromCos(fabs(hd.normal_shade*hd.in)));
			}
			else
				fbrdf = flay->getBRDF(hd);
			fbrdf = fbrdf * (1.0f) * opacity + Color4(1,1,1) * (1.0f-opacity);

			if (fbrdf.isBlack()  ||   fbrdf.isInf()   ||   fbrdf.isNaN())
			{
				iitri = itri;
				instMatrix = inst_mat; 
				instanceID = instID;
				maxDist = sum_d;
				return true;
			}
			attenuation *= fbrdf;
		}
		else
		{
			attenuation *= 1.0f-opacity;
		}

		stPoint = inters + geom_normal * ((direction*geom_normal>0) ? 0.0001f : -0.0001f);
	}

	return false;
}

//-----------------------------------------------------------------------------------------------------

bool checkForCaustic(float * arrRough, int cur, bool &isSecondary, float maxRough)
{
	isSecondary = false;

	if (arrRough[cur] > maxRough)
		return false;

	int i = cur;
	while (i>1   &&   arrRough[i]<=maxRough)
		i--;

	if (arrRough[i] > maxRough)
	{
		if (i>1)
			isSecondary = true;
		else
			isSecondary = false;
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------------------------------

inline bool isCaustic(PTStack * pt, int lvl, float maxRough)
{
	bool got_spec = false;
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->fake_transmission)
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;

		if (ptp->rough>maxRough)
		{
			if (got_spec)
				return true;
			else
				return false;
		}
		else
		{
			got_spec = true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------

inline bool isSecondaryCaustic(PTStack * pt, int lvl, float maxRough)
{
	bool got_spec = false;
	bool got_nonspec = false;
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->fake_transmission)
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;

		if (ptp->rough>maxRough)
		{
			if (!got_spec)
				return false;
			if (i>0)
				return true;
		}
		else
			got_spec = true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------

inline bool isOnlySpecular(PTStack * pt, int lvl, float maxRough)
{	// or LE
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->fake_transmission)
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;
		if (ptp->rough>maxRough)
			return false;
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------

inline bool isLastSpecular(PTStack * pt, int lvl, float maxRough)
{	// or camera
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->fake_transmission)
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;
		if (ptp->rough>maxRough)
			return false;
		else
			return true;
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------

inline bool isLightEyePath(PTStack * pt, int lvl)
{	// or LE
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->fake_transmission  ||  ptp->rough==0.0f)  
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;
		return false;
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------

inline bool isLDDE(PTStack * pt, int lvl, float maxRough, float &dd_conn_dist, float &cos_t2, float &cos_t3)
{	// for proper MIS weight in portal->diffuse->connect_to->diffuse
	bool got_first = false;
	Point3d p1, p2;
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->fake_transmission)
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;
		if (ptp->rough<=maxRough)
			return false;
		else
			if (got_first)
			{
				p2 = ptp->int_point;
				dd_conn_dist = (p2-p1).length();
				return true;
			}
			else
			{
				got_first = true;
				p1 = ptp->int_point;
				cos_t2 = fabs(ptp->hd.in  * ptp->hd.normal_geom);
				cos_t3 = fabs(ptp->hd.out * ptp->hd.normal_geom);
			}
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------

inline bool findLastNonSpecPdf(PTStack * pt, int lvl, float &pdf)
{
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->rough==0.0f  ||  ptp->fake_transmission)
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;
		pdf = ptp->pdf_brdf;
		return true;
	}
	pdf = 1.0f;
	return false;
}

//-----------------------------------------------------------------------------------------------------

inline bool checkGotNonSpec(PTStack * pt, int lvl, float maxRough)
{
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->mat->hasInvisibleEmitter)
			continue;
		if (ptp->rough>maxRough)
			return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------

inline bool findPreLastNonSpecPdf(PTStack * pt, int lvl, float maxRough, float &pdf)	// for sun gi connection
{
	bool got_first = false;
	for (int i=lvl; i>=0; i--)
	{
		PTStack * ptp = &(pt[i]);
		if (ptp->fake_transmission)
			continue;
		if (ptp->mat->hasInvisibleEmitter)
			continue;
		if (ptp->rough<=maxRough)
			return false;

		if (!got_first)
		{
			got_first = true;
			continue;
		}

		pdf = ptp->pdf_brdf;
		return true;
	}
	pdf = 1.0f;
	return false;
}

//-----------------------------------------------------------------------------------------------------

float evalWeightForInvisibleLight(PTStack * pt, int lvl)
{
	if (lvl<0)
		return 0.0f;

	float res = 1.0f;
	for (int i=0; i<=lvl; i++)
	{
		PTStack * cpt = pt+i;
		if ((cpt->hd.in*cpt->hd.normal_geom) * (cpt->hd.out*cpt->hd.normal_geom) >= 0.0f)
			return 1.0f;	// illuminate reflection
		if (cpt->mlay->type == MatLayer::TYPE_SHADE)
			res *= cpt->rough;
		else
			res = 0;
	}

	return res;
}

//-----------------------------------------------------------------------------------------------------

bool findSSSPoint(const float sample_time, const Point3d &origin, const HitData &hd_in, HitData &hd_out, MaterialNox * mat, MatLayer * mlay, 
				  Matrix4d &instMatrix, int &instanceID, int &iitri, float &fu, float &fv, Color4 &attenuation)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	Vector3d vt1 = hd_in.in * -1;
	HitData hd1(vt1*-1, vt1, hd_in.normal_shade, hd_in.normal_geom, hd_in.dir_U, hd_in.dir_V, hd_in.tU, hd_in.tV, 0.0f, hd_in.spectr_pos);
	float temppdf;
	Vector3d sdir = mlay->randomNewDirectionLambert2(hd1, temppdf);
	Color4 bcolor = mlay->getColor0(hd_in.tU, hd_in.tV);
	Color4 trcolor1 = mlay->getTransmissionColor(hd_in.tU, hd_in.tV);
	RandomGenerator * rg = rtr->getRandomGeneratorForThread();

	Point3d stPoint = origin + hd_in.normal_geom * (sdir*hd_in.normal_geom>0 ? 0.0001f : -0.0001f);
	float totalDist = 0.0f;
	attenuation = Color4(1,1,1);
	for (int i=0; i<50; i++)
	{
		float tMaxDist = BIGFLOAT;
		int tInstID = -1;
		Matrix4d tInstMat;
		int tTriID = -1;
		float t_u, t_v;

		float partDist = 1.0f/max(0.1f, mlay->sssDens);
		#ifdef RAND_PER_THREAD
			float partDistP = partDist/10.0f + partDist*9.0f/10.0f * rg->getRandomFloat();
		#else
			float partDistP = partDist/10.0f + partDist*9.0f/10.0f*rand()/(float)RAND_MAX;
		#endif
		tMaxDist = partDistP;
		float d;
		if (rtr->use_embree)
			d = intersectEmbree(scene, stPoint, sdir, tInstMat, tMaxDist, tInstID, tTriID, t_u, t_v);
		else
			d = rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(sample_time, stPoint, sdir, tInstMat, tMaxDist, tInstID, tTriID, t_u, t_v);
		if (d<0 || tTriID<0  || d>partDist)
		{	
			// nothing, particle reflection and again
			stPoint = stPoint + sdir * partDistP;
			sdir = sdir * mlay->sssCollDir + Vector3d::random() * (1-fabs(mlay->sssCollDir));
			sdir.normalize();
			totalDist += partDistP;
			continue;
		}

		if (instanceID != tInstID)
			return false;
		Triangle tri = (scene->triangles[tTriID]);
		MaterialNox * fmat = scene->mats[scene->instances[tInstID].materials[tri.matInInst]];
		if (fmat != mat)
			return false;
		
		float tU, tV;
		totalDist += d;
		tri.evalTexUV(t_u, t_v, tU, tV);
		Point3d inters = tri.getPointFromUV(t_u,t_v);
		inters = tInstMat * inters;
		Vector3d tri_normal_shade = tInstMat * tri.evalNormal(t_u, t_v);
		Vector3d tri_normal_geom = tInstMat.getMatrixForNormals() * tri.normal_geom;
		tri_normal_geom.normalize();
		tri_normal_shade.normalize();

		Vector3d uvdirU, uvdirV;
		tri.getTangentSpaceSmooth(tInstMat, t_u,t_v, uvdirU, uvdirV);

		HitData rhd(sdir*-1, sdir, tri_normal_shade, tri_normal_geom, uvdirU, uvdirV, tU, tV, totalDist, hd_in.spectr_pos);
		hd_out = rhd;
		instanceID = tInstID;
		iitri = tTriID;
		fu = t_u;  fv = t_v;
		instMatrix = tInstMat;
		Color4 trcolor2 = mlay->getTransmissionColor(tU, tV);
		Color4 trcolor = (trcolor1 + trcolor2) * 0.5f;
		attenuation *= trcolor;

		if (mlay->absOn)
		{
			Color4 absCol = mlay->absColor;
			float absDist = mlay->absDist;
			attenuation.r = pow(1.0f/max(0.01f, absCol.r), -totalDist/absDist);
			attenuation.g = pow(1.0f/max(0.01f, absCol.g), -totalDist/absDist);
			attenuation.b = pow(1.0f/max(0.01f, absCol.b), -totalDist/absDist);
			attenuation *= trcolor;
		}
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------

bool PTStack::updatePdfBrdfMultilayered()
{
	if (!mat)
		return false;

	brdf = Color4(0,0,0);
	float pdf = 0.0f;
	float backpdf = 0.0f;
	float sumw = 0.0f;
	float tcos = fabs(hd.out * hd.normal_shade);
	for (int j=0; j<mat->layers.objCount; j++)
	{
		hd.clearFlagDelta();
		hd.clearFlagSSS();
		MatLayer * tmlay = &(mat->layers[j]);
		if (tmlay->type==MatLayer::TYPE_EMITTER)
			continue;

		Color4 tbrdf;
		float tpdf, tbpdf;
		tmlay->getBRDFandPDFs(hd, tbrdf, tpdf, tbpdf);
		if (!hd.isFlagDelta())
			tbrdf *= tcos;

		if (hd.isFlagDelta() && tpdf > 0)
		{
			pdf_brdf = tpdf;
			brdf = tbrdf;
			return true;
		}

		float w = tmlay->getWeight(hd.tU, hd.tV);
		sumw += w;
		if (tpdf<=0)
			continue;

		#ifdef BRDF_ALL_LAYERS_CORRECT
			if (correctPdf)
				brdf += tbrdf * w * (1.0f/tpdf) ;
			else
				brdf += tbrdf * w;
		#else
			brdf += tbrdf * w;
		#endif
		pdf += tpdf * w;
		backpdf += tbpdf * w;
	}
	pdf_brdf = pdf / sumw;
	#ifdef BRDF_ALL_LAYERS_CORRECT
		if (correctPdf)
			brdf *= pdfAngle;
	#endif
	return true;
}

//-----------------------------------------------------------------------------------------------------

bool PTStack::evalBrdfPdfMultilayered(Color4 &rbrdf, float &rpdf, float &rbpdf)
{
	if (!mat)
		return false;

	rbrdf = Color4(0,0,0);
	rpdf = 0.0f;
	rbpdf = 0.0f;
	float sumw = 0.0f;
	float tcos = fabs(hd.out * hd.normal_shade);
	for (int j=0; j<mat->layers.objCount; j++)
	{
		hd.clearFlagDelta();
		hd.clearFlagSSS();
		MatLayer * tmlay = &(mat->layers[j]);
		if (tmlay->type==MatLayer::TYPE_EMITTER)
			continue;

		Color4 tbrdf;
		float tpdf, tbpdf;
		tmlay->getBRDFandPDFs(hd, tbrdf, tpdf, tbpdf);
		if (!hd.isFlagDelta())
			tbrdf *= tcos;

		if (hd.isFlagDelta() && tpdf > 0)
		{
			rpdf = tpdf;
			rbpdf = tbpdf;
			rbrdf = tbrdf;
			
			return true;
		}

		float w = tmlay->getWeight(hd.tU, hd.tV);
		sumw += w;
		if (tpdf<=0)
			continue;

		#ifdef BRDF_ALL_LAYERS_CORRECT
			if (correctPdf)
				brdf += tbrdf * w * (1.0f/tpdf) ;
			else
				brdf += tbrdf * w;
		#else
			rbrdf += tbrdf * w;
		#endif
		rpdf += tpdf * w;
		rbpdf += tbpdf * w;
	}
	rpdf = rpdf / sumw;
	rbpdf = rbpdf / sumw;
	#ifdef BRDF_ALL_LAYERS_CORRECT
		if (correctPdf)
			brdf *= pdfAngle;
	#endif
	return true;
}

//-----------------------------------------------------------------------------------------------------
