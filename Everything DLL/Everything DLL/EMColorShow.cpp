#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;

void InitEMColorShow()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMColorShow";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMColorShowProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMColorShow *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMColorShow * GetEMColorShowInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMColorShow * emcs = (EMColorShow *)GetWindowLongPtr(hwnd, 0);
	#else
		EMColorShow * emcs = (EMColorShow *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif

    return emcs;
}

void SetEMColorShowInstance(HWND hwnd, EMColorShow *emcs)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcs);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcs);
	#endif
}

EMColorShow::EMColorShow(HWND hWnd)
{
	colBorderLeftUp = RGB(112,176,240);
	colBorderRightDown = RGB(144,208,255);
	colDragBorderLeftUp = RGB(132,196,240);;
	colDragBorderRightDown = RGB(164,228,255);
	showBorder = true;
	allowDragSrc = false;
	allowDragDst = false;
	isDragOver = false;

	hwnd = hWnd;
	color = Color4(0,0,0);
	dialogBrush = CreateSolidBrush(RGB(128,192,255));

	hImageCursor     = (HBITMAP)LoadImage(hDllModule, MAKEINTRESOURCE(IDB_CURSOR_COLOR), IMAGE_BITMAP, 0,0,  LR_CREATEDIBSECTION);
	hImageCursorMask = (HBITMAP)LoadImage(hDllModule, MAKEINTRESOURCE(IDB_CURSOR_COLOR_MASK), IMAGE_BITMAP, 0,0,  LR_CREATEDIBSECTION);
	createCursor(RGB(255,255,255));
}

EMColorShow::~EMColorShow()
{
	if (dialogBrush)
		DeleteObject(dialogBrush);
}

LRESULT CALLBACK EMColorShowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMColorShow* emcs;
	emcs = GetEMColorShowInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			{
				EMColorShow * emcs = new EMColorShow(hwnd);
				SetEMColorShowInstance(hwnd, emcs);
			}
			break;
		case WM_DESTROY:
			{
				delete emcs;
				SetEMColorShowInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			hdc = BeginPaint(hwnd, &ps);

			GetClientRect(hwnd, &rect);
			HRGN rg;
			if (!emcs->showBorder)
			{
				POINT bounding[] = { {0,0}, {rect.right, 0}, {rect.right, rect.bottom}, {0, rect.bottom} };
				rg = CreatePolygonRgn(bounding, 4, WINDING);
			}
			else
			{
				POINT bounding[] = { {1,1}, {rect.right-1, 1}, {rect.right-1, rect.bottom-1}, {1, rect.bottom-1} };
				rg = CreatePolygonRgn(bounding, 4, WINDING);
			}
			emcs->color.clamp();
			HBRUSH fbr = CreateSolidBrush(RGB((char)(int)(emcs->color.r*255),  (char)(int)(emcs->color.g*255),  (char)(int)(emcs->color.b*255)));
			FillRgn(hdc, rg, fbr);

			if ((GetWindowLong(hwnd, GWL_STYLE)) & WS_DISABLED)
			{
				unsigned char r = (unsigned char)min(255, max(0, (int)(emcs->color.r*255)));
				unsigned char g = (unsigned char)min(255, max(0, (int)(emcs->color.g*255)));
				unsigned char b = (unsigned char)min(255, max(0, (int)(emcs->color.b*255)));
				SetBkColor(hdc, RGB(r,g,b));
				HBRUSH dbr = CreateHatchBrush(HS_DIAGCROSS, RGB(255-r,255-g,255-b));
				FillRgn(hdc, rg, dbr);
				DeleteObject(dbr);
			}

			DeleteObject(fbr);
			DeleteObject(rg);

			if (emcs->showBorder)
			{
				POINT b1[] = { {0, rect.bottom-1}, {0,0}, {rect.right, 0}};
				POINT b2[] = { {rect.right-1, 1}, {rect.right-1, rect.bottom-1}, {0, rect.bottom-1}};
				HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emcs->isDragOver ? emcs->colDragBorderLeftUp : emcs->colBorderLeftUp));
				Polyline(hdc, b1, 3);
				DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emcs->isDragOver ? emcs->colDragBorderRightDown : emcs->colBorderRightDown)));
				Polyline(hdc, b2, 3);
				DeleteObject(SelectObject(hdc, hOldPen));
			}

			EndPaint(hwnd, &ps);
		}
		break;
		case WM_LBUTTONDOWN:
		{
			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), (LPARAM)hwnd);
		}
		break;
		case WM_LBUTTONUP:
		{
			SetCursor(LoadCursor(NULL, IDC_ARROW));
			if (GetCapture() == hwnd)
			{
				ReleaseCapture();
				
				if (!emcs->allowDragSrc)
					break;

				POINT pos;
				pos.x = (short)LOWORD(lParam); 
				pos.y = (short)HIWORD(lParam); 
				ClientToScreen(hwnd, &pos);
				
				HWND hOther = WindowFromPoint(pos);
				if (!hOther)
					break;
				if (hOther == hwnd)
					break;

				char cname[128];
				int ok = GetClassName(hOther, cname, 128);	
				if (!ok)
					break;

				if (strcmp(cname, "EMColorShow"))
					break;

				EMColorShow * emdst = GetEMColorShowInstance(hOther);
				emdst->dragNoOver();
				SendMessage(hOther, CS_YOUGOT_DRAG, (WPARAM)hwnd, (LPARAM)&emcs->color);
			}
		}
		break;
		case WM_RBUTTONDOWN:
		{
			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, CL_RCLICKED), (LPARAM)hwnd);
		}
		break;
		case WM_MOUSEMOVE:
		{
			if (wParam & MK_LBUTTON)
			{
				if (GetCapture() == 0)
					SetCapture(hwnd);
				if (GetCapture() == hwnd)
				{
					static HWND lastOther = 0;

					if (!emcs->allowDragSrc)
						break;

					if (GetCursor() != emcs->hCursor)
					{
						unsigned char r,g,b;
						r = (unsigned char)min(255, max(0, 255*emcs->color.r));
						g = (unsigned char)min(255, max(0, 255*emcs->color.g));
						b = (unsigned char)min(255, max(0, 255*emcs->color.b));
						emcs->createCursor(RGB(r,g,b));
						HCURSOR oldCursor = SetCursor((HCURSOR)(emcs->hCursor));
						DeleteObject(oldCursor);
					}

					POINT pos;
					pos.x = (short)LOWORD(lParam); 
					pos.y = (short)HIWORD(lParam); 
					ClientToScreen(hwnd, &pos);
					
					HWND hOther = WindowFromPoint(pos);
					if (!hOther)
					{
						if (lastOther)
						{
							EMColorShow * emcslast = GetEMColorShowInstance(lastOther);
							if (emcslast)
								emcslast->dragNoOver();
						}
						lastOther = 0;
						break;
					}
					if (hOther == hwnd)
					{
						if (lastOther)
						{
							EMColorShow * emcslast = GetEMColorShowInstance(lastOther);
							if (emcslast)
								emcslast->dragNoOver();
						}
						lastOther = 0;
						break;
					}

					char cname[128];
					int ok = GetClassName(hOther, cname, 128);	
					if (!ok)
					{
						if (lastOther)
						{
							EMColorShow * emcslast = GetEMColorShowInstance(lastOther);
							if (emcslast)
								emcslast->dragNoOver();
						}
						lastOther = 0;
						break;
					}
					if (strcmp(cname, "EMColorShow"))
					{
						if (lastOther)
						{
							EMColorShow * emcslast = GetEMColorShowInstance(lastOther);
							if (emcslast)
								emcslast->dragNoOver();
						}
						lastOther = 0;
						break;
					}

					if (lastOther != hOther)
					{
						if (lastOther)
						{
							EMColorShow * emcslast = GetEMColorShowInstance(lastOther);
							if (emcslast)
								emcslast->dragNoOver();
						}
						lastOther = hOther;
						GetEMColorShowInstance(lastOther)->dragOver();
					}


				}

			}
		}
		break;
		case CS_YOUGOT_DRAG:
		{
			if (!emcs->allowDragDst)
				break;
			HWND hSrc = (HWND)wParam;
			Color4 * c = (Color4 *)lParam;
			if (!c)
				break;
			emcs->redraw(*c);

			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, CS_IGOT_DRAG), (LPARAM)hwnd);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMColorShow::redraw(const Color4 &c)
{
	color = c;
	InvalidateRect(hwnd, NULL, false);
}

void EMColorShow::dragOver()
{
	if (allowDragDst)
		isDragOver = true;
	InvalidateRect(hwnd, NULL, false);
}

void EMColorShow::dragNoOver()
{
	isDragOver = false;
	InvalidateRect(hwnd, NULL, false);
}

void EMColorShow::setAllowedDragDrop()
{
	allowDragSrc = true;
	allowDragDst = true;
}

void EMColorShow::setDeniedDragDrop()
{
	allowDragSrc = false;
	allowDragDst = false;
}

bool EMColorShow::createCursor(COLORREF col)
{
	BITMAP bmp;
	GetObject(hImageCursor,sizeof(bmp), &bmp);
	if (!bmp.bmBits  ||  bmp.bmWidth!=32  ||  bmp.bmHeight!=32)
		return false;

	unsigned char newbuf[32*32*4];
	unsigned char r,g,b;
	r = GetRValue(col);
	g = GetGValue(col);
	b = GetBValue(col);

	for (int y=0; y<32; y++)
		for (int x=0; x<128; x++)
			newbuf[y*128+x] = ((unsigned char*)(bmp.bmBits))[(31-y)*128+x];
	for (int y=12; y<21; y++)
		for (int x=14; x<23; x++)
		{
			newbuf[y*128+x*4 + 0] = b;
			newbuf[y*128+x*4 + 1] = g;
			newbuf[y*128+x*4 + 2] = r;
		}

	HBITMAP tempbmp = CreateBitmap(
		32,32,1,32, newbuf);

	ICONINFO ii;
	ii.fIcon = FALSE;
	ii.xHotspot = 0;
	ii.yHotspot = 0;
	ii.hbmColor = tempbmp;
	ii.hbmMask = hImageCursorMask;
	HICON hicon = CreateIconIndirect(&ii);
	hCursor = hicon;

	DeleteObject(tempbmp);

	return true;
}
