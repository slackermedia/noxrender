#ifndef _NOX_DEBUG_H_
#define _NOX_DEBUG_H_


#define LOG_CONSTRUCTORS
#define LOG_PARTS_MAIN
//#define LOG_PARTS_ENUM


#ifdef LOG_CONSTRUCTORS 
	#define ADD_LOG_CONSTR(a)	TrRaytracer::addLog((a))
#else
	#define ADD_LOG_CONSTR(a)
#endif

#ifdef LOG_PARTS_MAIN
	#define ADD_LOG_PARTS_MAIN(a)	TrRaytracer::addLog((a))
#else
	#define ADD_LOG_PARTS_MAIN(a)
#endif

#ifdef LOG_PARTS_ENUM
	#define ADD_LOG_PARTS_ENUM(a)	TrRaytracer::addLog((a))
#else
	#define ADD_LOG_PARTS_ENUM(a)
#endif




#endif
