#define _CRT_RAND_S
#include "MaterialArchiver.h"
#include "XML_IO.h"
#include "log.h"
#include <process.h>
#include "MatEditor.h"
#include "RendererMainWindow.h"

#define TEXINLAY 9

extern char * defaultDirectory;

bool copyTexFile(char * src, char * dst)
{
	if (!src || !dst)
		return false;
	BOOL res = CopyFile(src, dst, false);
	return (res == TRUE);
}

char * checkDuplicates(char * newString, char * existingString, bool freePrevious)
{
	if (!existingString  ||  !newString)
		return newString;

	if (strcmp(newString, existingString))
		return newString;	// different

	// so... the same
	int l = (int)strlen(newString);
	char * newer = (char *)malloc(l+3);
	if (!newer)
	{
		if (freePrevious)
			free(newString);
		return NULL;
	}

	char * kr = strrchr(newString, '.');
	if (kr==NULL)
	{
		sprintf_s(newer, l+3, "%s1", newString);
		if (freePrevious)
			free(newString);
		return newer;
	}
	else
	{
		int lbezkropki = (int)(kr - newString);
		int lpokropce = l - lbezkropki - 1;
		sprintf_s(newer, l+3, "%s", newString);
		sprintf_s(newer+lbezkropki, l+3-lbezkropki,"1.%s", newString+lbezkropki+1);
		if (freePrevious)
			free(newString);
		return newer;
	}
}


bool MatArchiver::archiveMaterial(MaterialNox * mat, char * filename)
{
	CHECK(mat);
	CHECK(filename);

	char temppath[2048];
	int ll = GetTempPath(2048, temppath);
	if (ll > 2048)
		return false;

	if (ll < 3)
		return false;

	char temppathlong[2048];
	int ll2 = GetLongPathName(temppath, temppathlong, 2048);
	if (ll2 > 2048)
		return false;

	if (temppathlong[ll2-1] == '\\'  ||  temppathlong[ll2-1] == '/')
		temppathlong[ll2-1] = 0;

	if (!dirExists(temppathlong))
		return false;

	unsigned int unique;
	while(rand_s(&unique));
	char tempfilename[MAX_PATH];
	GetTempFileName(temppathlong, "mat_", unique, tempfilename);

	if (strlen(tempfilename) < strlen(temppathlong))
		return false;

	Logger::add("Creating material archive...");

	// check for 7zip
	char checkedFilename[2048];
	sprintf_s(checkedFilename, 2048, "%s\\bin\\32\\7zG.exe", defaultDirectory);
	if (!fileExists(checkedFilename))
	{
		MessageBox(0, "Couldn't find 7zip. Can't archive.", "Error", 0);
		return false;
	}
	sprintf_s(checkedFilename, 2048, "%s\\bin\\32\\7z.dll", defaultDirectory);
	if (!fileExists(checkedFilename))
	{
		MessageBox(0, "Couldn't find 7zip. Can't archive.", "Error", 0);
		return false;
	}

	CreateDirectory(tempfilename, NULL);

	char tempfilenamesubtextures[MAX_PATH];
	sprintf_s(tempfilenamesubtextures, MAX_PATH, "%s\\textures", tempfilename);

	CreateDirectory(tempfilenamesubtextures, NULL);
	
	// create list of textures
	bList<char *, 7> toDel(0);
	bList<RenamedTextures, 7> textures(0);
	for (int i=0; i<mat->layers.objCount; i++)
	{
		TextureInstance * texI = NULL;
		for (int j=0; j<TEXINLAY; j++)
		{
			switch (j)
			{
				case 0: texI = &(mat->layers[i].tex_col0); break;
				case 1: texI = &(mat->layers[i].tex_col90); break;
				case 2: texI = &(mat->layers[i].tex_rough); break;
				case 3: texI = &(mat->layers[i].tex_weight); break;
				case 4: texI = &(mat->layers[i].tex_light); break;
				case 5: texI = &(mat->layers[i].tex_normal); break;
				case 6: texI = &(mat->layers[i].tex_transm); break;
				case 7: texI = &(mat->layers[i].tex_aniso); break;
				case 8: texI = &(mat->layers[i].tex_aniso_angle); break;
				default: texI = NULL;
			}

			if (texI)
			{
				if (!texI->isValid()  ||  !texI->filename   ||   !texI->fullfilename)
					continue;
				RenamedTextures rtex;
				rtex.filename = texI->filename;
				rtex.fullfilename = texI->fullfilename;
				rtex.layer = i;
				rtex.type = j;
				textures.add(rtex);
				textures.createArray();
			}
		}

		if (mat->layers[i].ies  &&  mat->layers[i].ies->filename  &&  mat->layers[i].ies->isValid())
		{
			RenamedTextures rtex;
			rtex.filename = getOnlyFile(mat->layers[i].ies->filename);
			rtex.fullfilename = mat->layers[i].ies->filename;
			rtex.layer = i;
			rtex.type = 500;
			textures.add(rtex);
			textures.createArray();
		}
	}

	if (mat->tex_opacity.isValid()  &&  mat->tex_opacity.filename   &&   mat->tex_opacity.fullfilename)
	{
		RenamedTextures rtex;
		rtex.filename = mat->tex_opacity.filename;
		rtex.fullfilename = mat->tex_opacity.fullfilename;
		rtex.layer = 0;
		rtex.type = 20;

		textures.add(rtex);
		textures.createArray();
	}

	if (mat->tex_displacement.isValid()  &&  mat->tex_displacement.filename   &&   mat->tex_displacement.fullfilename)
	{
		RenamedTextures rtex;
		rtex.filename = mat->tex_displacement.filename;
		rtex.fullfilename = mat->tex_displacement.fullfilename;
		rtex.layer = 0;
		rtex.type = 21;

		textures.add(rtex);
		textures.createArray();
	}

	// check for duplicates
	for (int k=0; k<textures.objCount; k++)
	{
		textures[k].newfilename = copyString(textures[k].filename);
		bool copying = true;
		for (int j=0; j<k; j++)
		{
			if (strcmp(textures[k].fullfilename, textures[j].fullfilename) == 0)
			{	// the same
				if (textures[k].newfilename)
					free(textures[k].newfilename);
				textures[k].newfilename = copyString(textures[j].newfilename);
				copying = false;
			}
			else
			{
				textures[k].newfilename = checkDuplicates(textures[k].newfilename, textures[j].newfilename);
			}
		}

		int ll = (int)strlen(textures[k].newfilename) + (int)strlen(textures[k].fullfilename) + 256;
		char * llog = (char *)malloc(ll);
		char * type = NULL;
		switch (textures[k].type)
		{
			case 0:   type="texture color0      "; break;
			case 1:   type="texture color90     "; break;
			case 2:   type="texture roughness   "; break;
			case 3:   type="texture weight      "; break;
			case 4:   type="texture light       "; break;
			case 5:   type="texture normal      "; break;
			case 6:   type="texture transm      "; break;
			case 7:   type="texture aniso       "; break;
			case 8:   type="texture aniso angle "; break;
			case 20:  type="texture opacity     "; break;
			case 21:  type="texture displacement"; break;
			case 500: type="ies                 "; break;
			default:  type="ERROR               "; break;
		}

		if (textures[k].type!=20)
			sprintf_s(llog, ll, " Layer[%d] %s %s : \"%s\" => \"%s\"", textures[k].layer, copying?"copying":"using  ", type, textures[k].fullfilename, textures[k].newfilename);
		else
			sprintf_s(llog, ll, " Material  %s %s : \"%s\" => \"%s\"", copying?"copying":"using  ", type, textures[k].fullfilename, textures[k].newfilename);
		Logger::add(llog);

		if (copying)
		{
			int ld = (int)strlen(textures[k].newfilename) + (int)strlen(tempfilenamesubtextures) + 5;
			char * fullDst = (char *)malloc(ld);
			sprintf_s(fullDst, ld, "%s\\%s", tempfilenamesubtextures, textures[k].newfilename);
			if (copyTexFile(textures[k].fullfilename, fullDst))
			{
				toDel.add(fullDst);
				toDel.createArray();
			}
			else
				Logger::add(" copy failed :(");
		}

		// change texture names in material...
		TextureInstance * texI = NULL;
		switch (textures[k].type)
		{
			case 0:		texI = &(mat->layers[textures[k].layer].tex_col0);   break;
			case 1:		texI = &(mat->layers[textures[k].layer].tex_col90);  break;
			case 2:		texI = &(mat->layers[textures[k].layer].tex_rough);  break;
			case 3:		texI = &(mat->layers[textures[k].layer].tex_weight); break;
			case 4:		texI = &(mat->layers[textures[k].layer].tex_light);  break;
			case 5:		texI = &(mat->layers[textures[k].layer].tex_normal); break;
			case 6:		texI = &(mat->layers[textures[k].layer].tex_transm); break;
			case 7:		texI = &(mat->layers[textures[k].layer].tex_aniso); break;
			case 8:		texI = &(mat->layers[textures[k].layer].tex_aniso_angle); break;
			case 20:	texI = &(mat->tex_opacity);  break;
			case 21:	texI = &(mat->tex_displacement);  break;
		}

		if (texI)
		{
			texI->filename = textures[k].newfilename;
			int tl = (int)strlen(textures[k].newfilename)+32;
			texI->fullfilename = (char *)malloc(tl);
			sprintf_s(texI->fullfilename, tl, "textures\\%s", textures[k].newfilename);
		}
		if (textures[k].type == 500)	// ies
		{
			if (mat->layers[textures[k].layer].ies)
			{
				int tl = (int)strlen(textures[k].newfilename)+32;
				mat->layers[textures[k].layer].ies->filename = (char *)malloc(tl);
				sprintf_s(mat->layers[textures[k].layer].ies->filename, tl, "textures\\%s", textures[k].newfilename);
			}
		}
	}

	// save xml
	char materialFilename[2048];
	sprintf_s(materialFilename, 2048, "%s\\material.nxm", tempfilename);

	Logger::add(" Saving nox material...");
	Raytracer * rtr = Raytracer::getInstance();
	XMLScene xscene;
	bool saved = xscene.saveMaterial(materialFilename, mat);
	
	char delinfobuf[2048];
	if (fileExists(filename))
	{
		sprintf_s(delinfobuf, 2048, " Deleting existing %s", filename);
		Logger::add(delinfobuf);
		DeleteFile(filename);
	}

	if (saved)
	{
		Logger::add(" Compressing (7-zip)...");
		char args[2048];
		sprintf_s(args, 2048, "%s\\bin\\32\\7zG.exe a \"%s\" \"%s\" \"%s\"", defaultDirectory, filename, materialFilename, tempfilenamesubtextures);

		PROCESS_INFORMATION pi;
		STARTUPINFO si = {	sizeof(STARTUPINFO),	NULL,	"",	NULL, 0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0 };

		bool ok1 = (CreateProcess(NULL, args, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS , NULL, NULL, &si, &pi) == TRUE);
		if (ok1)
		{
			WaitForSingleObject(pi.hProcess, INFINITE);
			DWORD exitCode;
			GetExitCodeProcess(pi.hProcess, &exitCode);
			SwitchToThisWindow(MatEditorWindow::hMain, true);
			if (exitCode)	// some error while compressing
			{
				Logger::add(" 7-zip error!");
				saved = false;
			}

			CloseHandle(pi.hThread );
			CloseHandle(pi.hProcess );	
		}
	}
	else
		Logger::add(" Saving error!");

	// clean up
	Logger::add(" Cleaning up...");
	for (int i=0; i<toDel.objCount; i++)
	{
		if (fileExists(toDel[i]))
		{
			sprintf_s(delinfobuf, 2048, "  Deleting %s", toDel[i]);
			Logger::add(delinfobuf);
			DeleteFile(toDel[i]);
		}
	}
	if (dirExists(tempfilenamesubtextures))
	{
		sprintf_s(delinfobuf, 2048, "  Deleting %s", tempfilenamesubtextures);
		Logger::add(delinfobuf);
		RemoveDirectory(tempfilenamesubtextures);
	}
	if (fileExists(materialFilename))
	{
		sprintf_s(delinfobuf, 2048, "  Deleting %s", materialFilename);
		Logger::add(delinfobuf);
		DeleteFile(materialFilename);
	}
	if (dirExists(tempfilename))
	{
		sprintf_s(delinfobuf, 2048, "  Deleting %s", tempfilename);
		Logger::add(delinfobuf);
		RemoveDirectory(tempfilename);
	}

	for (int k=0; k<textures.objCount; k++)
	{
		// change back texture names in material...
		TextureInstance * texI = NULL;
		switch (textures[k].type)
		{
			case 0:		texI = &(mat->layers[textures[k].layer].tex_col0);   break;
			case 1:		texI = &(mat->layers[textures[k].layer].tex_col90);  break;
			case 2:		texI = &(mat->layers[textures[k].layer].tex_rough);  break;
			case 3:		texI = &(mat->layers[textures[k].layer].tex_weight); break;
			case 4:		texI = &(mat->layers[textures[k].layer].tex_light);  break;
			case 5:		texI = &(mat->layers[textures[k].layer].tex_normal); break;
			case 6:		texI = &(mat->layers[textures[k].layer].tex_transm); break;
			case 7:		texI = &(mat->layers[textures[k].layer].tex_aniso); break;
			case 8:		texI = &(mat->layers[textures[k].layer].tex_aniso_angle); break;
			case 20:	texI = &(mat->tex_opacity);  break;
			case 21:	texI = &(mat->tex_displacement);  break;
		}

		if (texI)
		{
			free(texI->fullfilename);
			free(texI->filename);
			texI->filename = textures[k].filename;
			texI->fullfilename = textures[k].fullfilename;
		}

		if (textures[k].type == 500)	// ies
		{
			if (mat->layers[textures[k].layer].ies)
			{
				if (mat->layers[textures[k].layer].ies->filename)
					free(mat->layers[textures[k].layer].ies->filename);
				mat->layers[textures[k].layer].ies->filename = textures[k].fullfilename;
			}
		}

	}
	textures.freeList();
	textures.destroyArray();

	if (!saved)
		return false;

	Logger::add(" Everything looks OK... archiving material complete.");

	return true;
}
