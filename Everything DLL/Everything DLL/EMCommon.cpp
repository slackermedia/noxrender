#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"


//------------------------------------------------------------------------------------------------------------------

inline COLORREF helperGetAvgColor(COLORREF c1, COLORREF c2)
{
	unsigned char r = (GetRValue(c1) + GetRValue(c2))/2;
	unsigned char g = (GetGValue(c1) + GetGValue(c2))/2;
	unsigned char b = (GetBValue(c1) + GetBValue(c2))/2;
	return RGB(r,g,b);
}

//------------------------------------------------------------------------------------------------------------------

void drawBevel(HDC hdc, COLORREF cLeft, COLORREF cTop, COLORREF cRight, COLORREF cBottom, int px1, int py1, int px2, int py2)
{
	HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, cLeft));

	// ---- corners as average color
	MoveToEx(hdc, px1, py2-1, NULL);
	LineTo(hdc, px1, py1);
	DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, helperGetAvgColor(cTop,cLeft))));
	LineTo(hdc, px1+1, py1);
	DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, cTop)));
	LineTo(hdc, px2, py1);
	DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, helperGetAvgColor(cTop,cRight))));
	LineTo(hdc, px2, py1+1);
	DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, cRight)));
	LineTo(hdc, px2, py2);
	DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, helperGetAvgColor(cBottom,cRight))));
	LineTo(hdc, px2-1, py2);
	DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, cBottom)));
	LineTo(hdc, px1, py2);
	DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, helperGetAvgColor(cBottom,cLeft))));
	LineTo(hdc, px1, py2-1);

	DeleteObject((HPEN)SelectObject(hdc, oldPen));
}

//------------------------------------------------------------------------------------------------------------------

void drawGradientHorizontal(HDC hdc, COLORREF cLeft, COLORREF cRight, int px, int py, int w, int h)
{
	int bgr1 = GetRValue(cLeft);
	int bgg1 = GetGValue(cLeft);
	int bgb1 = GetBValue(cLeft);
	int bgr2 = GetRValue(cRight);
	int bgg2 = GetGValue(cRight);
	int bgb2 = GetBValue(cRight);
	HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(0,0,0)));
	for (int j=0; j<w; j++)
	{
		unsigned char r = (bgr2*(j) + bgr1*(w-j-1))/(w-1);
		unsigned char g = (bgg2*(j) + bgg1*(w-j-1))/(w-1);
		unsigned char b = (bgb2*(j) + bgb1*(w-j-1))/(w-1);
		DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(r,g,b))));
		MoveToEx(hdc, j+px, py, NULL);
		LineTo(hdc, j+px, py+h);
	}
	DeleteObject((HPEN)SelectObject(hdc, oldPen));
}

//------------------------------------------------------------------------------------------------------------------

void drawGradientVertical(HDC hdc, COLORREF cTop, COLORREF cBottom, int px, int py, int w, int h)
{
	int bgr1 = GetRValue(cTop);
	int bgg1 = GetGValue(cTop);
	int bgb1 = GetBValue(cTop);
	int bgr2 = GetRValue(cBottom);
	int bgg2 = GetGValue(cBottom);
	int bgb2 = GetBValue(cBottom);
	HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(0,0,0)));
	for (int j=0; j<h; j++)
	{
		unsigned char r = (bgr2*(j) + bgr1*(h-j-1))/(h-1);
		unsigned char g = (bgg2*(j) + bgg1*(h-j-1))/(h-1);
		unsigned char b = (bgb2*(j) + bgb1*(h-j-1))/(h-1);
		DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(r,g,b))));
		MoveToEx(hdc, px, j+py, NULL);
		LineTo(hdc, w+px, j+py);
	}
	DeleteObject((HPEN)SelectObject(hdc, oldPen));
}

//------------------------------------------------------------------------------------------------------------------
