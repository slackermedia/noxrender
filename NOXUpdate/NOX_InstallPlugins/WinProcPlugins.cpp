#include <windows.h>
#include "../NOXUpdate/nox_update.h"

extern HWND hCh_9_32;
extern HWND hCh_9_64;
extern HWND hCh_2008_32;
extern HWND hCh_2008_64;
extern HWND hCh_2009_32;
extern HWND hCh_2009_64;
extern HWND hCh_2010_32;
extern HWND hCh_2010_64;
extern HWND hCh_2011_32;
extern HWND hCh_2011_64;
extern HWND hCh_2012_32;
extern HWND hCh_2012_64;
extern HWND hCh_2013_32;
extern HWND hCh_2013_64;
extern HWND hCh_2014_64;
extern HWND hEdit_9_32;
extern HWND hEdit_9_64;
extern HWND hEdit_2008_32;
extern HWND hEdit_2008_64;
extern HWND hEdit_2009_32;
extern HWND hEdit_2009_64;
extern HWND hEdit_2010_32;
extern HWND hEdit_2010_64;
extern HWND hEdit_2011_32;
extern HWND hEdit_2011_64;
extern HWND hEdit_2012_32;
extern HWND hEdit_2012_64;
extern HWND hEdit_2013_32;
extern HWND hEdit_2013_64;
extern HWND hEdit_2014_64;
extern HWND hDirCh_9_32;
extern HWND hDirCh_9_64;
extern HWND hDirCh_2008_32;
extern HWND hDirCh_2008_64;
extern HWND hDirCh_2009_32;
extern HWND hDirCh_2009_64;
extern HWND hDirCh_2010_32;
extern HWND hDirCh_2010_64;
extern HWND hDirCh_2011_32;
extern HWND hDirCh_2011_64;
extern HWND hDirCh_2012_32;
extern HWND hDirCh_2012_64;
extern HWND hDirCh_2013_32;
extern HWND hDirCh_2013_64;
extern HWND hDirCh_2014_64;
extern HWND hPlInstall;
extern HWND hPlExit;
extern HWND hPlStatusText;
extern HWND hStatus_9_32;
extern HWND hStatus_9_64;
extern HWND hStatus_2008_32;
extern HWND hStatus_2008_64;
extern HWND hStatus_2009_32;
extern HWND hStatus_2009_64;
extern HWND hStatus_2010_32;
extern HWND hStatus_2010_64;
extern HWND hStatus_2011_32;
extern HWND hStatus_2011_64;
extern HWND hStatus_2012_32;
extern HWND hStatus_2012_64;
extern HWND hStatus_2013_32;
extern HWND hStatus_2013_64;
extern HWND hStatus_2014_64;


void chooseDirectory(HWND hEdit)
{
	char dir[2048];
	GetWindowText(hEdit, dir, 2048);
	char * aaa = openDirectoryDialog(hEdit, "Choose plugin directory:", dir);
	if (aaa)
	{
		SetWindowText(hEdit, aaa);
		free(aaa);
	}
}

LRESULT CALLBACK NOXPluginsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		{
		}	// end WM_CREATE
		break;
	case WM_SIZE:
		{
			RECT rect;
			GetClientRect(hWnd, &rect);
			int w = rect.right - rect.left - 185 - 90;
			w = max(w, 60);
			SetWindowPos(hEdit_9_32,    HWND_BOTTOM, 140,10,   w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_9_64,    HWND_BOTTOM, 140,30,   w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2008_32, HWND_BOTTOM, 140,50,   w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2008_64, HWND_BOTTOM, 140,70,   w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2009_32, HWND_BOTTOM, 140,90,   w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2009_64, HWND_BOTTOM, 140,110,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2010_32, HWND_BOTTOM, 140,130,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2010_64, HWND_BOTTOM, 140,150,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2011_32, HWND_BOTTOM, 140,170,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2011_64, HWND_BOTTOM, 140,190,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2012_32, HWND_BOTTOM, 140,210,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2012_64, HWND_BOTTOM, 140,230,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2013_32, HWND_BOTTOM, 140,250,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2013_64, HWND_BOTTOM, 140,270,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			SetWindowPos(hEdit_2014_64, HWND_BOTTOM, 140,290,  w,20,  SWP_NOZORDER | SWP_NOMOVE);
			int p = rect.right - rect.left - 40 - 90;
			p = max(205, p);
			SetWindowPos(hDirCh_9_32,    HWND_BOTTOM, p,11,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_9_64,    HWND_BOTTOM, p,31,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2008_32, HWND_BOTTOM, p,51,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2008_64, HWND_BOTTOM, p,71,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2009_32, HWND_BOTTOM, p,91,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2009_64, HWND_BOTTOM, p,111, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2010_32, HWND_BOTTOM, p,131, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2010_64, HWND_BOTTOM, p,151, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2011_32, HWND_BOTTOM, p,171, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2011_64, HWND_BOTTOM, p,191, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2012_32, HWND_BOTTOM, p,211, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2012_64, HWND_BOTTOM, p,231, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2013_32, HWND_BOTTOM, p,251, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2013_64, HWND_BOTTOM, p,271, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hDirCh_2014_64, HWND_BOTTOM, p,291, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			p = rect.right - rect.left - 40 - 90 + 40;
			SetWindowPos(hStatus_9_32,    HWND_BOTTOM, p,13,   w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_9_64,    HWND_BOTTOM, p,33,   w ,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2008_32, HWND_BOTTOM, p,53,   w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2008_64, HWND_BOTTOM, p,73,   w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2009_32, HWND_BOTTOM, p,93,   w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2009_64, HWND_BOTTOM, p,113,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2010_32, HWND_BOTTOM, p,133,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2010_64, HWND_BOTTOM, p,153,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2011_32, HWND_BOTTOM, p,173,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2011_64, HWND_BOTTOM, p,193,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2012_32, HWND_BOTTOM, p,213,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2012_64, HWND_BOTTOM, p,233,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2013_32, HWND_BOTTOM, p,253,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2013_64, HWND_BOTTOM, p,273,  w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hStatus_2014_64, HWND_BOTTOM, p,293,  w,18,  SWP_NOZORDER | SWP_NOSIZE);

			int c = rect.right - rect.left;
			c = c/2;
			c = max(130, c);
			SetWindowPos(hPlInstall, HWND_BOTTOM, c-120,340, w,18,  SWP_NOZORDER | SWP_NOSIZE);
			SetWindowPos(hPlExit, HWND_BOTTOM, c+20,340, w,18,  SWP_NOZORDER | SWP_NOSIZE);
		}
		break;
	case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
	case WM_SIZING:
		{
			RECT * rect = (RECT *)lParam;
			rect->right = max(rect->right, max(rect->right-rect->left,400)+rect->left);
			rect->bottom = rect->top+415;
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case 201:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_9_32, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_9_32, checked);
						EnableWindow(hDirCh_9_32, checked);
						SetWindowText(hStatus_9_32, (checked ? "install" : "ignored") );
					}
					break;
				case 202:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_9_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_9_64, checked);
						EnableWindow(hDirCh_9_64, checked);
						SetWindowText(hStatus_9_64, (checked ? "install" : "ignored") );
					}
					break;
				case 203:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2008_32, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2008_32, checked);
						EnableWindow(hDirCh_2008_32, checked);
						SetWindowText(hStatus_2008_32, (checked ? "install" : "ignored") );
					}
					break;
				case 204:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2008_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2008_64, checked);
						EnableWindow(hDirCh_2008_64, checked);
						SetWindowText(hStatus_2008_64, (checked ? "install" : "ignored") );
					}
					break;
				case 205:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2009_32, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2009_32, checked);
						EnableWindow(hDirCh_2009_32, checked);
						SetWindowText(hStatus_2009_32, (checked ? "install" : "ignored") );
					}
					break;
				case 206:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2009_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2009_64, checked);
						EnableWindow(hDirCh_2009_64, checked);
						SetWindowText(hStatus_2009_64, (checked ? "install" : "ignored") );
					}
					break;
				case 207:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2010_32, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2010_32, checked);
						EnableWindow(hDirCh_2010_32, checked);
						SetWindowText(hStatus_2010_32, (checked ? "install" : "ignored") );
					}
					break;
				case 208:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2010_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2010_64, checked);
						EnableWindow(hDirCh_2010_64, checked);
						SetWindowText(hStatus_2010_64, (checked ? "install" : "ignored") );
					}
					break;
				case 209:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2011_32, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2011_32, checked);
						EnableWindow(hDirCh_2011_32, checked);
						SetWindowText(hStatus_2011_32, (checked ? "install" : "ignored") );
					}
					break;
				case 210:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2011_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2011_64, checked);
						EnableWindow(hDirCh_2011_64, checked);
						SetWindowText(hStatus_2011_64, (checked ? "install" : "ignored") );
					}
					break;
				case 211:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2012_32, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2012_32, checked);
						EnableWindow(hDirCh_2012_32, checked);
						SetWindowText(hStatus_2012_32, (checked ? "install" : "ignored") );
					}
					break;
				case 212:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2012_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2012_64, checked);
						EnableWindow(hDirCh_2012_64, checked);
						SetWindowText(hStatus_2012_64, (checked ? "install" : "ignored") );
					}
					break;
				case 213:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2013_32, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2013_32, checked);
						EnableWindow(hDirCh_2013_32, checked);
						SetWindowText(hStatus_2013_32, (checked ? "install" : "ignored") );
					}
					break;
				case 214:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2013_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2013_64, checked);
						EnableWindow(hDirCh_2013_64, checked);
						SetWindowText(hStatus_2013_64, (checked ? "install" : "ignored") );
					}
					break;
				case 215:
					{
						bool checked =  (BST_CHECKED ==SendMessage(hCh_2014_64, BM_GETCHECK, 0, 0) );
						EnableWindow(hEdit_2014_64, checked);
						EnableWindow(hDirCh_2014_64, checked);
						SetWindowText(hStatus_2014_64, (checked ? "install" : "ignored") );
					}
					break;
				case 221:
					chooseDirectory(hEdit_9_32);
					break;
				case 222:
					chooseDirectory(hEdit_9_64);
					break;
				case 223:
					chooseDirectory(hEdit_2008_32);
					break;
				case 224:
					chooseDirectory(hEdit_2008_64);
					break;
				case 225:
					chooseDirectory(hEdit_2009_32);
					break;
				case 226:
					chooseDirectory(hEdit_2009_64);
					break;
				case 227:
					chooseDirectory(hEdit_2010_32);
					break;
				case 228:
					chooseDirectory(hEdit_2010_64);
					break;
				case 229:
					chooseDirectory(hEdit_2011_32);
					break;
				case 230:
					chooseDirectory(hEdit_2011_64);
					break;
				case 231:
					chooseDirectory(hEdit_2012_32);
					break;
				case 232:
					chooseDirectory(hEdit_2012_64);
					break;
				case 233:
					chooseDirectory(hEdit_2013_32);
					break;
				case 234:
					chooseDirectory(hEdit_2013_64);
					break;
				case 235:
					chooseDirectory(hEdit_2014_64);
					break;
				case 251:	// INSTALL
					{
						installPlugins();
					}
					break;
				case 252:	// EXIT
					{
						PostQuitMessage(0);
					}
					break;
			}
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	return 0;
}


