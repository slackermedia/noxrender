#ifndef __NOXSunLight__H
#define __NOXSunLight__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include <3dsmaxport.h>
#include <macrorec.h>
#include "shadgen.h"
#include <stdmat.h>

#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"

//----------------------------------------------------------------------------------------------------------------------------------

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;
extern int numSunInstances;

#define NOXSunLight_CLASS_ID	Class_ID(0x1dd501d9, 0x1e0140de)
#define NOXSunTarget_CLASS_ID	Class_ID(0x72344101, 0x7f9f4323)
#define NOXSUNLIGHT_PBLOCK_REF	0

Point3 myNormalize(Point3 a);
INT_PTR CALLBACK NOXSunLightDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK NOXSunLightTargetDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
class NOXSunTarget;

//----------------------------------------------------------------------------------------------------------------------------------

class NOXSunLight : public GenLight
{
	public:
		ExclList excl;

		IOResult Load(ILoad *iload);
		IOResult Save(ISave *isave);

		Class_ID ClassID() {return NOXSunLight_CLASS_ID;}		
		SClass_ID SuperClassID() { return LIGHT_CLASS_ID; }
		void GetClassName(TSTR& s) {s = GetString(IDS_NOX_SUN_CLASS_NAME);}

		RefTargetHandle Clone( RemapDir &remap );
		RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);

		IParamBlock2	*sunlight_pblock;
		int	NumParamBlocks();
		IParamBlock2* GetParamBlock(int i);
		IParamBlock2* GetParamBlockByID(BlockID id);
		void updateFromParamBlock();
		void updateToParamBlock();

		int NumRefs() { return 1; }							
		RefTargetHandle GetReference(int i);				
		void SetReference(int i, RefTargetHandle rtarg);				             

		void DeleteThis();

		NOXSunLight();
		~NOXSunLight();

		void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
		void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
			
		GenLight *  NewLight (int type)											{ return new NOXSunLight(); } 
		RefResult  EvalLightState (TimeValue t, Interval &valid, LightState *cs); 
		int Type()																{ return TDIR_LIGHT; } 
		void SetType(int tp)													{  }
		BOOL IsSpot()															{ return FALSE; }
		BOOL IsDir()															{ return TRUE; }
		void SetUseLight(int onOff)												{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); } 
		BOOL GetUseLight(void)													{ return TRUE; } 
		void SetSpotShape(int s)												{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); } 
		int  GetSpotShape(void)													{ return CIRCLE_LIGHT; } 
		void  SetHotspot (TimeValue time, float f)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetHotspot (TimeValue t, Interval &valid=Interval(0, 0))			{ return 0; } 
		void  SetFallsize(TimeValue time, float f)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetFallsize(TimeValue t, Interval &valid=Interval(0, 0))			{ return 0; }
		void  SetAtten(TimeValue time, int which, float f)						{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetAtten(TimeValue t, int which, Interval &valid=Interval(0, 0))	{ return 1; }
		void  SetTDist(TimeValue time, float f)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetTDist(TimeValue t, Interval &valid=Interval(0, 0))				{ return 100; }
		void   SetRGBColor(TimeValue t, Point3 &rgb)							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		Point3 GetRGBColor(TimeValue t, Interval &valid=Interval(0, 0))			{ return Point3(1.0f, 1.0f, 0.5f); }
		void   SetHSVColor(TimeValue t, Point3 &hsv)							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		Point3 GetHSVColor(TimeValue t, Interval &valid=Interval(0, 0))			{ return Point3(0.164706f, 0.5f, 1.0f); }
		void  SetIntensity(TimeValue time, float f)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetIntensity(TimeValue t, Interval &valid=Interval(0, 0))			{ return 1; }
		void  SetContrast (TimeValue time, float f)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetContrast (TimeValue t, Interval &valid=Interval(0, 0))			{ return 0; }
		void  SetAspect (TimeValue t, float f)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetAspect (TimeValue t, Interval &valid=Interval(0, 0))			{ return 1; }
		void  SetConeDisplay (int s, int notify=TRUE)							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  GetConeDisplay (void)												{ return FALSE; } 
		void  SetUseAtten (int s)												{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  GetUseAtten (void)												{ return FALSE; }
		void  SetAttenDisplay (int s)											{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  GetAttenDisplay (void)											{ return FALSE; }
		void  SetUseAttenNear (int s)											{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  GetUseAttenNear (void)											{ return FALSE; }
		void  SetAttenNearDisplay (int s)										{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  GetAttenNearDisplay (void)										{ return FALSE; }
		void  Enable (int enab)													{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		void  SetMapBias (TimeValue t, float f)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetMapBias (TimeValue t, Interval &valid=Interval(0, 0))			{ return 0; }
		void  SetMapRange (TimeValue t, float f)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetMapRange (TimeValue t, Interval &valid=Interval(0, 0))			{ return 0; }
		void  SetMapSize (TimeValue t, int f)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		int   GetMapSize (TimeValue t, Interval &valid=Interval(0, 0))			{ return 0; }
		void  SetRayBias (TimeValue t, float f)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float GetRayBias (TimeValue t, Interval &valid=Interval(0, 0))			{ return 0; }
		int   GetUseGlobal ()													{ return TRUE; }
		void  SetUseGlobal (int a)												{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		int   GetShadow ()														{ return 0; }
		void  SetShadow (int a)													{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		int   GetShadowType ()													{ return -1; }
		void  SetShadowType (int a)												{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		//void  SetShadowGenerator (ShadowType *s)								{ shadowType = s;} 
		//ShadowType *  GetShadowGenerator ()										{ return shadowType; }
		int   GetAbsMapBias ()													{ return 0; }
		void  SetAbsMapBias (int a)												{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		void  SetAtmosShadows (TimeValue t, int onOff)							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		int  GetAtmosShadows (TimeValue t)										{ return 0; }
		void  SetAtmosOpacity (TimeValue t, float f)							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float  GetAtmosOpacity (TimeValue t, Interval &valid=FOREVER)			{ return 0; }
		void  SetAtmosColAmt (TimeValue t, float f)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float  GetAtmosColAmt (TimeValue t, Interval &valid=FOREVER) 			{ return 0; }
		void  SetUseShadowColorMap (TimeValue t, int onOff) 					{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		int  GetUseShadowColorMap (TimeValue t) 								{ return 0; }
		int  GetOvershoot ()													{ return 1; }
		void  SetOvershoot (int a)												{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		ExclList &  GetExclusionList ()											{ return excl; }
		void  SetExclusionList (ExclList &list)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  SetHotSpotControl (Control *c)									{ return FALSE; }
		BOOL  SetFalloffControl (Control *c)									{ return FALSE; }
		BOOL  SetColorControl (Control *c)										{ return FALSE; }
		Control *  GetHotSpotControl ()											{ return NULL; }
		Control *  GetFalloffControl ()											{ return NULL; }
		Control *  GetColorControl ()											{ return NULL; }
		virtual void  SetAffectDiffuse (BOOL onOff)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  GetAffectDiffuse ()												{ return TRUE; }
		virtual void  SetAffectSpecular (BOOL onOff)							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		BOOL  GetAffectSpecular ()												{ return TRUE; } 
		virtual void  SetDecayType (BOOL onOff)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		virtual BOOL  GetDecayType () 											{ return FALSE; }
		void  SetDecayRadius (TimeValue time, float f)							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float  GetDecayRadius (TimeValue t, Interval &valid=Interval(0, 0)) 	{ return 0; }
		void  SetDiffuseSoft (TimeValue time, float f) 							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float  GetDiffuseSoft (TimeValue t, Interval &valid=Interval(0, 0)) 	{ return 0; }
		void  SetShadColor (TimeValue t, Point3 &rgb) 							{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		Point3  GetShadColor (TimeValue t, Interval &valid=Interval(0, 0)) 		{ return Point3(0,0,0); }
		BOOL  GetLightAffectsShadow ()											{ return FALSE; }
		void  SetLightAffectsShadow (BOOL b)									{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		void  SetShadMult (TimeValue t, float m)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		float  GetShadMult (TimeValue t, Interval &valid=Interval(0, 0))		{ return 1.0f; } 
		virtual void  SetAmbientOnly (BOOL onOff)								{ NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE); }
		virtual BOOL  GetAmbientOnly ()											{ return FALSE; }
		void UpdateTargDistance(TimeValue t, INode* inode);
		NOXSunTarget * getTargetObject();

		void GetMat(TimeValue t, INode* inode, ViewExp *vpt, Matrix3& tm);
		void GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box );
		void GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box );
		int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
		int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
		void DrawTargetLine(TimeValue t, GraphicsWindow *gw, float dist, int colid);

		ObjectState Eval(TimeValue)												{ return ObjectState(this); }
		ObjLightDesc * CreateLightDesc(INode *n , BOOL forceShadow);//			{ return new NOXdirLight(n, forceShadow); }
		CreateMouseCallBack * GetCreateMouseCallBack(void);

		HWND hParams;
		Mesh * sunMesh;
		float tDist;
		bool hiddenTarg;
		bool hiddenEnvMap;
		void buildMeshes();
		void setTargetHidden(bool hide);
		void setEnvHidden(bool hide);
		void selectTarget();
		void updateUIAltitudeAzimuth();
		void updateUIPositions();
		bool getPositions(Point3 &pos, Point3 &tpos);
		bool evalAltitudeAzimuth(float &alt, float &azi);
		bool setTMfromAltAzi(float alt, float azi);
		bool amISelected();
		static int countSuns();
		static int numSuns;
		bool removeUI;
		bool creating;
		Point3 startPoint;
		TrSunSky trsunsky;
		bool evalSunDir(float &x, float &y, float &z);
		bool holdTM();
};

//----------------------------------------------------------------------------------------------------------------------------------

class NOXSunLightClassDesc : public ClassDesc2 {
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE);
	const TCHAR *	ClassName() { return GetString(IDS_NOX_SUN_CLASS_NAME); }
	SClass_ID		SuperClassID() { return LIGHT_CLASS_ID; }
	Class_ID		ClassID() { return NOXSunLight_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_NOX_SUN_CATEGORY); }
	const TCHAR*	InternalName() { return _T("NOXSunLight"); }
	HINSTANCE		HInstance() { return hInstance; }
};

//----------------------------------------------------------------------------------------------------------------------------------

class NOXSunLightCreateCallBack: public CreateMouseCallBack 
{
	NOXSunLight *ob;
	NOXSunTarget * targObject;
	INode * targNode;
	INode * sunNode;
public:
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat);
	void SetObj(NOXSunLight *obj) 
			{ 
				ob = obj; 
				targObject = NULL; 
				targNode = NULL; 
				sunNode = NULL; 
			}
};


//----------------------------------------------------------------------------------------------------------------------------------

class NOXObjLightDesc : public ObjLightDesc 
{
public:

	int Update(TimeValue t, const RendContext& rc, RenderGlobalContext *rgc, BOOL shadows, BOOL shadowGeomChanged);

	NOXObjLightDesc(INode *inode) : ObjLightDesc(inode) { }
	~NOXObjLightDesc() {}
};

//----------------------------------------------------------------------------------------------------------------------------------

class NOXSunTarget: public GeomObject 
{			   
	Mesh targMesh;		
	Mesh meshN;
	Mesh meshS;
	Mesh meshE;
	Mesh meshW;
	Mesh sphMesh;
	DWORD_PTR texPtrEnv;

	float tempHither, tempYon;
	float cpx, cpy, cpz;


	void GetMatrix(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm);
	void GetEnvSphereMatrix(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm);
	void GetLetterMatrix(TimeValue t, INode* inode, ViewExp* vpt, int side, Matrix3& tm);
	void BuildMesh();
	void RebuildSphereMesh(float scale);
	void deleteMeshes();


public:
	NOXSunTarget();
	~NOXSunTarget();

	bool LoadEnvMap(char * filename);
	bool updateEnvMap();
	bool deleteEnvMap();

	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags, Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);

	#ifdef ISMAX2013
	const MCHAR * GetObjectName() { return GetString(IDS_NOX_SUN_TARGET_OBJECT_NAME); }
	#else
	virtual MCHAR * GetObjectName() { return GetString(IDS_NOX_SUN_TARGET_OBJECT_NAME); }
	#endif

	ObjectState Eval(TimeValue time);
	void InitNodeName(TSTR& s) { s = GetString(IDS_NOX_SUN_TARGET_DEFNAME); }
	ObjectHandle ApplyTransform(Matrix3& matrix);
	int UsesWireColor() { return 1; }
	int IsRenderable() { return 0; }

	int IntersectRay(TimeValue t, Ray& r, float& at);
	ObjectHandle CreateTriObjRep(TimeValue t);
	void GetWorldBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel );

	BOOL HasViewDependentBoundingBox() { return true; }

	void DeleteThis() {	 delete this; }
	Class_ID ClassID() {return NOXSunTarget_CLASS_ID;}		
	SClass_ID SuperClassID() { return TARGET_CLASS_ID; }
	void GetClassName(TSTR& s) { s = TSTR(GetString(IDS_NOX_SUN_TARGET_CLASS_NAME)); }
	int IsKeyable(){ return 1;}

	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir());
	RefResult NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message )
				{ return(REF_SUCCEED); }

	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);
	HWND hParams;
	bool amIHidden;
	bool envMapHidden;
	void updateUIAltitudeAzimuth();
	void updateUIPositions();
	NOXSunLight * getSunObject();
	void resetRotation();
	bool setTMfromAltAzi(float alt, float azi);
	void selectSun();
	bool amISelected();
	bool evalDirections(Point3 &dirN, Point3 &dirE);
};

//----------------------------------------------------------------------------------------------------------------------------------

class NOXSunTargetClassDesc : public ClassDesc2 
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new NOXSunTarget(); }
	const TCHAR *	ClassName() { return GetString(IDS_NOX_SUN_TARGET_CLASS_NAME); }
	SClass_ID		SuperClassID() { return TARGET_CLASS_ID; }
	Class_ID		ClassID() { return NOXSunTarget_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_NOX_SUN_CATEGORY); }
	const TCHAR*	InternalName() { return _T("NOXSunTarget"); }
	HINSTANCE		HInstance() { return hInstance; }
};

//----------------------------------------------------------------------------------------------------------------------------------

class NOXSunTargetObjectCreateCallback: public CreateMouseCallBack 
{
public:
	NOXSunTarget * eob;
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat );
	void SetObj(NOXSunTarget * obj) { eob = obj; }
};

//----------------------------------------------------------------------------------------------------------------------------------

class SceneEnumForSun : public ITreeEnumProc
{
public:
	ExpInterface *expInterface;
	int callback(INode *  node);
	SceneEnumForSun(ExpInterface *ei) { expInterface = ei;}
};

//----------------------------------------------------------------------------------------------------------------------------------

class NOXSunPostLoad : public PostLoadCallback
{
	NOXSunLight * sun;
public:
	NOXSunPostLoad(NOXSunLight * sunlight)  { sun = sunlight; }
	~NOXSunPostLoad()  {}
	void proc(ILoad * iload);
};

//----------------------------------------------------------------------------------------------------------------------------------

#endif
