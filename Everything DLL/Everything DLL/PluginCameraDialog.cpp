#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <math.h>
#include "valuesMinMax.h"

extern HMODULE hDllModule;
static notifyCameraPluginCallback * camFocalCallback = NULL;

void notifyPlugin(HWND hDlg)
{
	HWND hLength				= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_FOCAL_LENGTH);
	EMEditSpin * emlen			= GetEMEditSpinInstance(hLength);
	float fovHori				= 360.0f/PI*atan(18.0f/emlen->floatValue);
	HWND hAutoFocus				= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_AUTOFOCUS);
	HWND hFocusDistance			= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_FOCUS_DISTANCE);
	EMCheckBox * emautof		= GetEMCheckBoxInstance(hAutoFocus);
	EMEditSpin * emfdist		= GetEMEditSpinInstance(hFocusDistance);
	float fdist = emautof->selected ? -1 : emfdist->floatValue;
	if (camFocalCallback)
		(*camFocalCallback)(fovHori*PI/180.0f, fdist);
}

void fillAperture(HWND hDialog, Camera * cam)
{
	EMEditSpin * emApertCp	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_APERTURE_COPY));
	EMEditSpin * emBlNum	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_NUM));
	EMEditSpin * emBlAngle	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_ANGLE));
	EMEditSpin * emBlRadius	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_RADIUS));
	EMCheckBox * emBlRound	= GetEMCheckBoxInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_ROUND));
	EMPView * empv			= GetEMPViewInstance(   GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_APERTURE_SHAPE));
	emApertCp->setValuesToFloat(cam->aperture,  NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 0.4f, 0.1f);
	emBlNum->setValuesToInt(cam->apBladesNum, NOX_CAM_BLADES_NUM_MIN, NOX_CAM_BLADES_NUM_MAX, 1, 0.02f);
	emBlAngle->setValuesToFloat(cam->apBladesAngle, NOX_CAM_BLADES_ANGLE_MIN, NOX_CAM_BLADES_ANGLE_MAX, 1, 0.02f);
	emBlRadius->setValuesToFloat(cam->apBladesRadius, NOX_CAM_BLADES_RADIUS_MIN, NOX_CAM_BLADES_RADIUS_MAX, 1, 0.02f);
	emBlRound->selected = cam->apBladesRound;
	InvalidateRect(emBlRound->hwnd, NULL, false);
	cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
	cam->diaphSampler.createApertureShapePreview(empv->byteBuff);
	InvalidateRect(empv->hwnd, NULL, false);
}

void updateBokeh(HWND hDialog, Camera * cam)
{
	EMCheckBox * emDofOn		= GetEMCheckBoxInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_DOF_ON));
	EMEditSpin * emRingBalance	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_RING_BALANCE));
	EMEditSpin * emRingSize		= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_RING_SIZE));
	EMEditSpin * emFlatten		= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_FLATTEN));
	EMEditSpin * emVignette		= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_VIGNETTE));
	cam->dofOnTemp = emDofOn->selected;
	cam->diaphSampler.intBokehRingBalance	= emRingBalance->intValue;
	cam->diaphSampler.bokehRingBalance		= 1.0f + emRingBalance->intValue/21.0f;
	cam->diaphSampler.intBokehRingSize		= emRingSize->intValue;
	cam->diaphSampler.bokehRingSize			= emRingSize->intValue * 0.01f;
	cam->diaphSampler.intBokehFlatten		= emFlatten->intValue;
	cam->diaphSampler.bokehFlatten			= emFlatten->intValue * 0.01f;
	cam->diaphSampler.intBokehVignette		= emVignette->intValue;
	cam->diaphSampler.bokehVignette			= emVignette->intValue * 0.01f;
}

void updateAperture(HWND hDialog, Camera * cam)
{
	EMEditSpin * emApertCp	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_APERTURE_COPY));
	EMEditSpin * emBlNum	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_NUM));
	EMEditSpin * emBlAngle	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_ANGLE));
	EMEditSpin * emBlRadius	= GetEMEditSpinInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_RADIUS));
	EMCheckBox * emBlRound	= GetEMCheckBoxInstance(GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_BLADES_ROUND));
	EMPView * empv			= GetEMPViewInstance(   GetDlgItem(hDialog, IDC_PLUGIN_CAMERA_APERTURE_SHAPE));
	cam->aperture = emApertCp->floatValue;
	cam->apBladesNum = emBlNum->intValue;
	cam->apBladesAngle = emBlAngle->floatValue;
	cam->apBladesRound = emBlRound->selected;
	cam->apBladesRadius = emBlRadius->floatValue;
	cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
	cam->diaphSampler.createApertureShapePreview(empv->byteBuff);
	InvalidateRect(empv->hwnd, NULL, false);
}

void updateFOVs(HWND hDlg, float leng, bool changeFocal)
{
	float fovHori = 360.0f/PI*atan(18.0f/leng);
	float fovVert = 360.0f/PI*atan(12.0f/leng);
	float fovDiag = 360.0f/PI*atan(21.633307652783935758715327604823f / leng);
	HWND hDiag = GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_TEXT_VALUE_DIAGONAL_FOV);
	HWND hHori = GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_TEXT_VALUE_HORIZONTAL_FOV);
	HWND hVert = GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_TEXT_VALUE_VERTICAL_FOV);
	char buf[64];
	sprintf_s(buf, 64, "%.2f�", fovDiag);
	SetWindowText(hDiag, buf);
	sprintf_s(buf, 64, "%.2f�", fovHori);
	SetWindowText(hHori, buf);
	sprintf_s(buf, 64, "%.2f�", fovVert);
	SetWindowText(hVert, buf);
	HWND hFocal = GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_FOCAL_LENGTH);
	EMEditSpin * emes1 = GetEMEditSpinInstance(hFocal);
	if (changeFocal)
		emes1->setValuesToFloat(leng, NOX_CAM_FOCAL_MIN, NOX_CAM_FOCAL_MAX, 1, 0.5f);
	notifyPlugin(hDlg);
}

void updateExposure(HWND hDlg, Camera * cam)
{
	HWND hAutoExp			= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_AUTOEXPOSURE);
	HWND hAutoExpType		= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_AUTOEXP_TYPE);
	HWND hISO				= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_ISO);
	HWND hShutter			= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_SHUTTER);
	HWND hAperture			= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_APERTURE);
	EMCheckBox * emauto		= GetEMCheckBoxInstance(hAutoExp);
	EMComboBox * emtype		= GetEMComboBoxInstance(hAutoExpType);
	EMEditSpin * emISO		= GetEMEditSpinInstance(hISO);
	EMEditSpin * emShutter	= GetEMEditSpinInstance(hShutter);
	EMEditSpin * emAperture	= GetEMEditSpinInstance(hAperture);
	if (cam)
	{
		cam->autoexposure = emauto->selected;
		cam->autoexposureType = emtype->selected;
		cam->ISO = emISO->floatValue;
		cam->shutter = emShutter->floatValue;
		cam->aperture = emAperture->floatValue;
	}

	if (emauto->selected)
		EnableWindow(hAutoExpType, true);
	else
		EnableWindow(hAutoExpType, false);
	InvalidateRect(hAutoExpType, NULL, false);

}


void updateFocus(HWND hDlg, Camera * cam)
{
	HWND hAutoFocus			= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_AUTOFOCUS);
	HWND hFocusDistance		= GetDlgItem(hDlg, IDC_PLUGIN_CAMERA_FOCUS_DISTANCE);
	EMCheckBox * emauto		= GetEMCheckBoxInstance(hAutoFocus);
	EMEditSpin * emdist		= GetEMEditSpinInstance(hFocusDistance);
	if (cam)
	{
		cam->autoFocus = emauto->selected;
		cam->focusDist = emdist->floatValue;
	}

	if (emauto->selected)
		EnableWindow(hFocusDistance, false);
	else
		EnableWindow(hFocusDistance, true);
	InvalidateRect(hAutoFocus, NULL, false);
	InvalidateRect(hFocusDistance, NULL, false);

	notifyPlugin(hDlg);
}

INT_PTR CALLBACK PluginCameraDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Camera * plCam = NULL;
	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (lParam)
				plCam = (Camera*)lParam;
			else
				EndDialog(hWnd, (INT_PTR)(0));
			HWND hGr1				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_GR_OPTICS);
			HWND hGr2				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_GR_EXPOSURE);
			HWND hGr3				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_GR_DOF);
			HWND hGr4				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_GR_APERTURE);
			HWND hGr5				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_GR_BOKEH);
			HWND hFocal				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FOCAL_LENGTH);
			HWND hFP15				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP15);
			HWND hFP20				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP20);
			HWND hFP24				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP24);
			HWND hFP28				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP28);
			HWND hFP35				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP35);
			HWND hFP50				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP50);
			HWND hFP85				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP85);
			HWND hFP135				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP135);
			HWND hFP200				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FP200);
			HWND hAutoExp			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_AUTOEXPOSURE);
			HWND hAutoExpType		= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_AUTOEXP_TYPE);
			HWND hISO				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_ISO);
			HWND hAperture			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_APERTURE);
			HWND hShutter			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_SHUTTER);
			HWND hAutoFocus			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_AUTOFOCUS);
			//HWND hDOFMode			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_DOF_MODE);
			HWND hDOFdist			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FOCUS_DISTANCE);
			HWND hApertureCopy		= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_APERTURE_COPY);
			HWND hBlNum				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_BLADES_NUM);
			HWND hBlAngle			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_BLADES_ANGLE);
			HWND hBlRound			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_BLADES_ROUND);
			HWND hBlRadius			= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_BLADES_RADIUS);
			HWND hBokehDofON		= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_DOF_ON);
			HWND hBokehRBalance		= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_RING_BALANCE);
			HWND hBokehRSize		= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_RING_SIZE);
			HWND hBokehFlatten		= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FLATTEN);
			HWND hBokehVignette		= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_VIGNETTE);
			HWND hShape				= GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_APERTURE_SHAPE);

			EMGroupBar * emgb1		= GetEMGroupBarInstance(hGr1);
			EMGroupBar * emgb2		= GetEMGroupBarInstance(hGr2);
			EMGroupBar * emgb3		= GetEMGroupBarInstance(hGr3);
			EMGroupBar * emgb4		= GetEMGroupBarInstance(hGr4);
			EMGroupBar * emgb5		= GetEMGroupBarInstance(hGr5);
			EMEditSpin * emes1		= GetEMEditSpinInstance(hFocal);
			EMButton * emfp15		= GetEMButtonInstance(hFP15);
			EMButton * emfp20		= GetEMButtonInstance(hFP20);
			EMButton * emfp24		= GetEMButtonInstance(hFP24);
			EMButton * emfp28		= GetEMButtonInstance(hFP28);
			EMButton * emfp35		= GetEMButtonInstance(hFP35);
			EMButton * emfp50		= GetEMButtonInstance(hFP50);
			EMButton * emfp85		= GetEMButtonInstance(hFP85);
			EMButton * emfp135		= GetEMButtonInstance(hFP135);
			EMButton * emfp200		= GetEMButtonInstance(hFP200);
			EMCheckBox * emcbAuto	= GetEMCheckBoxInstance(hAutoExp);
			EMComboBox * emcombo	= GetEMComboBoxInstance(hAutoExpType);
			EMEditSpin * emISO		= GetEMEditSpinInstance(hISO);
			EMEditSpin * emAperture	= GetEMEditSpinInstance(hAperture);
			EMEditSpin * emShutter	= GetEMEditSpinInstance(hShutter);
			EMCheckBox * emcbAutoF	= GetEMCheckBoxInstance(hAutoFocus);
			EMEditSpin * emdofdist	= GetEMEditSpinInstance(hDOFdist);
			EMEditSpin * emApertCp	= GetEMEditSpinInstance(hApertureCopy);
			EMEditSpin * emBlNum	= GetEMEditSpinInstance(hBlNum);
			EMEditSpin * emBlAngle	= GetEMEditSpinInstance(hBlAngle);
			EMEditSpin * emBlRadius	= GetEMEditSpinInstance(hBlRadius);
			EMCheckBox * emBlRound	= GetEMCheckBoxInstance(hBlRound);
			EMCheckBox * emDofOn		= GetEMCheckBoxInstance(hBokehDofON);
			EMEditSpin * emRingBalance	= GetEMEditSpinInstance(hBokehRBalance);
			EMEditSpin * emRingSize		= GetEMEditSpinInstance(hBokehRSize);
			EMEditSpin * emFlatten		= GetEMEditSpinInstance(hBokehFlatten);
			EMEditSpin * emVignette		= GetEMEditSpinInstance(hBokehVignette);

			EMPView * empv			= GetEMPViewInstance(hShape);
			empv->byteBuff->allocBuffer(96,96);
			empv->byteBuff->clearBuffer();
			empv->dontLetUserChangeZoom = true;

			GlobalWindowSettings::colorSchemes.apply(emgb1);
			GlobalWindowSettings::colorSchemes.apply(emgb2);
			GlobalWindowSettings::colorSchemes.apply(emgb3);
			GlobalWindowSettings::colorSchemes.apply(emgb4);
			GlobalWindowSettings::colorSchemes.apply(emgb5);
			GlobalWindowSettings::colorSchemes.apply(emes1);
			GlobalWindowSettings::colorSchemes.apply(emfp15);
			GlobalWindowSettings::colorSchemes.apply(emfp20);
			GlobalWindowSettings::colorSchemes.apply(emfp24);
			GlobalWindowSettings::colorSchemes.apply(emfp28);
			GlobalWindowSettings::colorSchemes.apply(emfp35);
			GlobalWindowSettings::colorSchemes.apply(emfp50);
			GlobalWindowSettings::colorSchemes.apply(emfp85);
			GlobalWindowSettings::colorSchemes.apply(emfp135);
			GlobalWindowSettings::colorSchemes.apply(emfp200);
			GlobalWindowSettings::colorSchemes.apply(emcbAuto);
			GlobalWindowSettings::colorSchemes.apply(emcombo);
			GlobalWindowSettings::colorSchemes.apply(emISO);
			GlobalWindowSettings::colorSchemes.apply(emAperture);
			GlobalWindowSettings::colorSchemes.apply(emShutter);
			GlobalWindowSettings::colorSchemes.apply(emdofdist);
			GlobalWindowSettings::colorSchemes.apply(emcbAutoF);
			GlobalWindowSettings::colorSchemes.apply(emApertCp);
			GlobalWindowSettings::colorSchemes.apply(emBlNum);
			GlobalWindowSettings::colorSchemes.apply(emBlAngle);
			GlobalWindowSettings::colorSchemes.apply(emBlRadius);
			GlobalWindowSettings::colorSchemes.apply(emBlRound);
			GlobalWindowSettings::colorSchemes.apply(emDofOn);
			GlobalWindowSettings::colorSchemes.apply(emRingBalance);
			GlobalWindowSettings::colorSchemes.apply(emRingSize);
			GlobalWindowSettings::colorSchemes.apply(emFlatten);
			GlobalWindowSettings::colorSchemes.apply(emVignette);
			GlobalWindowSettings::colorSchemes.apply(empv);
			emcombo->addItem("Spot");
			emcombo->addItem("Center weighted");
			emcombo->addItem("Matrix");
			emcombo->selected = plCam->autoexposureType;
			emISO->setValuesToFloat(plCam->ISO, NOX_CAM_ISO_MIN, NOX_CAM_ISO_MAX, 1, 5);
			emAperture->setValuesToFloat(plCam->aperture, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 0.4f, 0.1f);
			emShutter->setValuesToFloat(plCam->shutter, NOX_CAM_SHUTTER_MIN, NOX_CAM_SHUTTER_MAX, 1, 10);
			emcbAuto->selected = plCam->autoexposure;
			if (emcbAuto->selected)
				EnableWindow(hAutoExpType, true);
			else
				EnableWindow(hAutoExpType, false);

			emdofdist->setValuesToFloat(plCam->focusDist, NOX_CAM_FOCUS_MIN, NOX_CAM_FOCUS_MAX, 1, 10);
			emcbAutoF->selected = plCam->autoFocus;
			InvalidateRect(emcombo->hwnd, NULL, false);
			InvalidateRect(emcbAuto->hwnd, NULL, false);
			InvalidateRect(emcbAutoF->hwnd, NULL, false);

			emDofOn->selected = plCam->dofOnTemp;
			InvalidateRect(emDofOn->hwnd, NULL, false);
			emRingBalance->setValuesToInt(plCam->diaphSampler.intBokehRingBalance, NOX_CAM_BOKEH_BALANCE_MIN, NOX_CAM_BOKEH_BALANCE_MAX, 1, 0.1f);
			emRingSize->setValuesToInt(plCam->diaphSampler.intBokehRingSize, NOX_CAM_BOKEH_RING_SIZE_MIN, NOX_CAM_BOKEH_RING_SIZE_MAX, 1, 0.1f);
			emFlatten->setValuesToInt(plCam->diaphSampler.intBokehFlatten, NOX_CAM_BOKEH_FLATTEN_MIN, NOX_CAM_BOKEH_FLATTEN_MAX, 1, 0.1f);
			emVignette->setValuesToInt(plCam->diaphSampler.intBokehVignette, NOX_CAM_BOKEH_VIGNETTE_MIN, NOX_CAM_BOKEH_VIGNETTE_MAX, 1, 0.1f);


			float f = 0.5f * 36 / tan(plCam->angle * PI / 360.0f);
			updateFOVs(hWnd, f, true);
			updateFocus(hWnd, plCam);




			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_FOCAL_LENGTH)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_HORIZONTAL_FOV)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VERTICAL_FOV)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_DIAGONAL_FOV)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VALUE_HORIZONTAL_FOV)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VALUE_VERTICAL_FOV)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VALUE_DIAGONAL_FOV)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_AUTOEXP)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_ISO)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_APERTURE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_SHUTER)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_MODE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_DISTANCE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_APERTURE_COPY)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_BLADES)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_ANGLE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_ROUND_BLADE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_RADIUS)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_DOF)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_RING_BALANCE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_RING_SIZE)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_FLATTEN)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VIGNETTE)), true);
			GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VALUE_DIAGONAL_FOV))->align = EMText::ALIGN_RIGHT;
			GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VALUE_HORIZONTAL_FOV))->align = EMText::ALIGN_RIGHT;
			GetEMTextInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_TEXT_VALUE_VERTICAL_FOV))->align = EMText::ALIGN_RIGHT;

			fillAperture(hWnd, plCam);
		}
		break;
	case WM_CLOSE:
		{
			camFocalCallback = NULL;
			EndDialog(hWnd, (INT_PTR)(0));
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_PLUGIN_CAMERA_FP15:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(15);	updateFOVs(hWnd, 15, true);		break;
				case IDC_PLUGIN_CAMERA_FP20:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(20);	updateFOVs(hWnd, 20, true);		break;
				case IDC_PLUGIN_CAMERA_FP24:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(24);	updateFOVs(hWnd, 24, true);		break;
				case IDC_PLUGIN_CAMERA_FP28:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(28);	updateFOVs(hWnd, 28, true);		break;
				case IDC_PLUGIN_CAMERA_FP35:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(35);	updateFOVs(hWnd, 35, true);		break;
				case IDC_PLUGIN_CAMERA_FP50:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(50);	updateFOVs(hWnd, 50, true);		break;
				case IDC_PLUGIN_CAMERA_FP85:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(85);	updateFOVs(hWnd, 85, true);		break;
				case IDC_PLUGIN_CAMERA_FP135:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(135);	updateFOVs(hWnd, 135, true);	break;
				case IDC_PLUGIN_CAMERA_FP200:	if (wmEvent!=BN_CLICKED) break;		if (plCam) plCam->setLength(200);	updateFOVs(hWnd, 200, true);	break;
				case IDC_PLUGIN_CAMERA_FOCAL_LENGTH: 
				{
					if (wmEvent != WM_VSCROLL)
						break;
					HWND hFocal = GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_FOCAL_LENGTH);
					EMEditSpin * emes1 = GetEMEditSpinInstance(hFocal);
					bool update = false;
					static float focal = 0;
					if (focal != emes1->floatValue)
						update = true;
					focal = emes1->floatValue;
					if (plCam) plCam->setLength(focal);
					if (update)
						updateFOVs(hWnd, focal, false);
				}
				break;
				case IDC_PLUGIN_CAMERA_AUTOEXPOSURE:
				{
					updateExposure(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_AUTOEXP_TYPE:
				{
					updateExposure(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_APERTURE:
				{
					EMEditSpin * emApertCp	= GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_APERTURE_COPY));
					EMEditSpin * emApert	= GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_APERTURE));
					emApertCp->setValuesToFloat(emApert->floatValue, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 0.4f, 0.1f);
					updateExposure(hWnd, plCam);
					updateAperture(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_ISO:
				{
					updateExposure(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_SHUTTER:
				{
					updateExposure(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_AUTOFOCUS:
				{
					updateFocus(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_FOCUS_DISTANCE:
				{
					updateFocus(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_APERTURE_COPY:
				{
					EMEditSpin * emApertCp	= GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_APERTURE_COPY));
					EMEditSpin * emApert	= GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_PLUGIN_CAMERA_APERTURE));
					emApert->setValuesToFloat(emApertCp->floatValue, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 0.4f, 0.1f);
					updateAperture(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_BLADES_NUM:
				{
					updateAperture(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_BLADES_ANGLE:
				{
					updateAperture(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_BLADES_ROUND:
				{
					updateAperture(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_BLADES_RADIUS:
				{
					updateAperture(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_DOF_ON:
				{
					updateBokeh(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_RING_BALANCE:
				{
					updateBokeh(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_RING_SIZE:
				{
					updateBokeh(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_FLATTEN:
				{
					updateBokeh(hWnd, plCam);
				}
				break;
				case IDC_PLUGIN_CAMERA_VIGNETTE:
				{
					updateBokeh(hWnd, plCam);
				}
				break;

			}
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			static HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			return (INT_PTR)hBr;
		}
	case WM_CTLCOLORDLG:
		{
			static HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			return (INT_PTR)hBr;
		}
		break;
	}
	return false;
}

Camera * showPluginCameraDialog(HWND hParent, Camera *cam)
{
	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->runDialogsUnicode)
	{
		return (Camera*) DialogBoxParamW(hDllModule, MAKEINTRESOURCEW(IDD_PLUGIN_CAMERA), hParent, PluginCameraDlgProc, (LPARAM)(cam));
	}
	else
	{
		return (Camera*) DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_PLUGIN_CAMERA), hParent, PluginCameraDlgProc, (LPARAM)(cam));
	}
}



void registerCameraUpdateNotification(notifyCameraPluginCallback * callback)
{
	camFocalCallback = callback;
}


