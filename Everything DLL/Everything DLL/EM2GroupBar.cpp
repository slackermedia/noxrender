#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"

//#define GUI2_USE_GDIPLUS

extern HMODULE hDllModule;

//----------------------------------------------------------------------

void InitEM2GroupBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2GroupBar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2GroupBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2GroupBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2GroupBar * GetEM2GroupBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2GroupBar * emgb = (EM2GroupBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2GroupBar * emgb = (EM2GroupBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emgb;
}

//----------------------------------------------------------------------

void SetEM2GroupBarInstance(HWND hwnd, EM2GroupBar *emgb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emgb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emgb);
	#endif
}

//----------------------------------------------------------------------

EM2GroupBar::EM2GroupBar(HWND hWnd)
{
	hwnd = hWnd;
	colText = NGCOL_LIGHT_GRAY;
	colBackGnd = RGB(0x3a,0x3a,0x3a);
	colTextDisabled = NGCOL_TEXT_DISABLED;
	colTextDisabled = NGCOL_DARK_GRAY;
	colTextShadow = RGB(30,30,30);
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfont = true;
	hAlign = HALIGN_CENTER;
	vAlign = VALIGN_CENTER;

	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
	{
		captionSize = 0;
	}	

	pseudoDisabled = false;
	colBackGndAlpha = RGB(128,128,128);
	alphaBg = 64;
	bgImage = 0;
	bgShiftX = bgShiftY = 0;
	#ifdef GUI2_DRAW_TEXT_SHADOWS
		drawTextShadow = true;
	#else
		drawTextShadow = false;
	#endif
}

//----------------------------------------------------------------------

EM2GroupBar::~EM2GroupBar()
{
	if  (hFont  &&  autodelfont)
		DeleteObject(hFont);
	hFont = 0;
	if (caption)
		free(caption);
	caption = NULL;
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2GroupBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2GroupBar * emgb = GetEM2GroupBarInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HFONT hOldFont;
	
	switch (msg)
	{
		case WM_CREATE:
			emgb = new EM2GroupBar(hwnd);
			SetEM2GroupBarInstance(hwnd, emgb);
			break;
		case WM_DESTROY:
			{
				delete emgb;
				SetEM2GroupBarInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);
			bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) != 0);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);

			
			if (emgb->bgImage)
			{
				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP * curBitmap = &emgb->bgImage;
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, *curBitmap);
				BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emgb->bgShiftX, emgb->bgShiftY, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);
				SetBkMode(hdc, TRANSPARENT);
			}
			else
			{
				HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
				HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, hOldBrush));
				DeleteObject(SelectObject(hdc, hOldPen2));
				SetBkMode(hdc, TRANSPARENT);
			}

			#ifdef GUI2_USE_GDIPLUS
			{
				ULONG_PTR gdiplusToken;		// activate gdi+
				Gdiplus::GdiplusStartupInput gdiplusStartupInput;
				Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
				{
					Gdiplus::Graphics graphics(hdc);
					Gdiplus::SolidBrush brush1(Gdiplus::Color(emgb->alphaBg, GetRValue(emgb->colBackGndAlpha), GetGValue(emgb->colBackGndAlpha), GetBValue(emgb->colBackGndAlpha)));
					graphics.FillRectangle(&brush1, 0,0, rect.right, rect.bottom);
				}	// gdi+ variables destructors here
				Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
			}
			#else
				HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emgb->colBackGnd));
				HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emgb->colBackGnd));
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, hOldBrush));
				DeleteObject(SelectObject(hdc, hOldPen2));
			#endif

			hOldFont = (HFONT)SelectObject(hdc, emgb->getFont());
			SetBkColor(hdc, emgb->colBackGndAlpha);

			SIZE sz;
			GetTextExtentPoint32(hdc, emgb->caption, emgb->captionSize, &sz);
			int posx, posy; 
			switch (emgb->vAlign)
			{
				case EM2GroupBar::VALIGN_TOP:
					posy = 2;
					break;
				case EM2GroupBar::VALIGN_CENTER:
					posy = (rect.bottom-rect.top-sz.cy)/2;
					break;
				case EM2GroupBar::VALIGN_BOTTOM:
					posy = rect.bottom - sz.cy - 2;
					break;
			}
			switch (emgb->hAlign)
			{
				case EM2GroupBar::HALIGN_LEFT:
					posx = 5;
					break;
				case EM2GroupBar::HALIGN_CENTER:
					posx = (rect.right-rect.left-sz.cx)/2;
					break;
				case EM2GroupBar::HALIGN_RIGHT:
					posx = rect.right - sz.cx - 5;
					break;
			}

			if (emgb->drawTextShadow)
			{
				SetTextColor(hdc, emgb->colTextShadow);
				ExtTextOut(hdc, posx+1, posy, ETO_CLIPPED, &rect, emgb->caption, emgb->captionSize, 0);
				ExtTextOut(hdc, posx-1, posy, ETO_CLIPPED, &rect, emgb->caption, emgb->captionSize, 0);
				ExtTextOut(hdc, posx, posy+1, ETO_CLIPPED, &rect, emgb->caption, emgb->captionSize, 0);
				ExtTextOut(hdc, posx, posy-1, ETO_CLIPPED, &rect, emgb->caption, emgb->captionSize, 0);
			}

			if (emgb->pseudoDisabled  ||  disabled)
				SetTextColor(hdc, emgb->colTextDisabled);
			else
				SetTextColor(hdc, emgb->colText);
			ExtTextOut(hdc, posx, posy, ETO_CLIPPED, &rect, emgb->caption, emgb->captionSize, 0);

			SelectObject(hdc, hOldFont);

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

bool EM2GroupBar::setFont(HFONT hNewFont, bool autodel)
{
	if (autodelfont)
		DeleteObject(hFont);
	hFont = hNewFont;
	autodelfont = autodel;
	return true;
}

//----------------------------------------------------------------------

HFONT EM2GroupBar::getFont()
{
	return hFont;
}

//----------------------------------------------------------------------
