
#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <gdiplus.h>
#include <math.h>
#include <stdio.h>
#include "EMControls.h"
#include "log.h"
#include <commctrl.h>
#include <xmmintrin.h>
#include <wingdi.h>

extern bool SUPPORT_SSSE3;

extern HMODULE hDllModule;

#define LUMINANCE(c) 0.2126f*c.r+0.7152f*c.g+0.0722f*c.b

void InitEMPView()
{
	WNDCLASSEX wc;

	wc.cbSize         = sizeof(wc);
	wc.lpszClassName  = "EMPView";
	wc.hInstance      = hDllModule;
	wc.lpfnWndProc    = EMPViewProc;
	wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
	wc.hIcon          = 0;
	wc.lpszMenuName   = 0;
	wc.hbrBackground  = (HBRUSH)0;
	wc.style          = CS_DBLCLKS;
	wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMPView *);
	wc.hIconSm        = 0;

	RegisterClassEx(&wc);
}


EMPView * GetEMPViewInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMPView * empv = (EMPView *)GetWindowLongPtr(hwnd, 0);
	#else
		EMPView * empv = (EMPView *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return empv;
}

void SetEMPViewInstance(HWND hwnd, EMPView *empv)
{
	#ifdef _WIN64
	    SetWindowLongPtr(hwnd, 0, (LONG_PTR)empv);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)empv);
	#endif
}

EMPView::EMPView(HWND hWnd)
{
	colBorder = RGB(0,0,0);
	colBackground = RGB(64,64,64);
	zoom = 1.0f;
	hwnd = hWnd;
	mx = my = 0.0f;
	showBorder = true;
	otherSource = 0;
	imgBuff = 0;
	imgMod = new ImageModifier(2.2f, 0.0f, 1);
	multiplier = 1;
	use_tone_mapping = false;
	reserved1 = NULL;
	reserved2 = NULL;
	gamma = 2.2f;
	tone_bias = 0.85f;
	useGaussianBlur = false;
	gaussianBlurR = 0;
	nowDrawing = false;
	drawNice = false;
	mode = MODE_SHOW;
	lastDroppedFilename = NULL;

	showRegions = true;
	lmClicked = false;
	rmClicked = false;
	dontLetUserChangeZoom = false;
	hToolTip = 0;
	toolTxt = NULL;
	rgnPtr = NULL;

	hs_posx = 5;
	hs_posy = 5;
	hs_width = 256;
	hs_height = 100;
	hs_margin = 5;
	show_histogram = false;
	showBuckets = false;
	bucketsCoords = NULL;
	nBuckets = 0;

	useImageOpacity = false;
	opacityToParent = false;

	hyperlink = NULL;


	for (int i=0; i<768; i++)
		histogram[i] = 0;
}

EMPView::~EMPView()
{
	delToolTip();
	rgnPtr = NULL;
	if (lastDroppedFilename)
		free(lastDroppedFilename);
	lastDroppedFilename = NULL;
}



LRESULT CALLBACK EMPViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMPView * empv;
	empv = GetEMPViewInstance(hwnd);
	if (empv)
		empv->nowDrawing = false;

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;

	static int cpx = 0;
	static int cpy = 0;
	static float cmx = 0;
	static float cmy = 0;

	switch (msg)
	{
		case WM_CREATE:
			GetClientRect(hwnd, &rect);
			empv = new EMPView(hwnd);
			SetEMPViewInstance(hwnd, empv);
			empv->imgBuff = new ImageBuffer();
			empv->imgBuff->allocBuffer(rect.right-rect.left, rect.bottom-rect.top);
			empv->imgBuff->clearBuffer();
			empv->byteBuff = new ImageByteBuffer();
			empv->byteBuff->allocBuffer(rect.right-rect.left, rect.bottom-rect.top);
			empv->byteBuff->clearBuffer();
			break;
		case WM_NCDESTROY:
			{
				delete empv;
				SetEMPViewInstance(hwnd, 0);
			}
			break;
		case WM_SIZE:
			{
				// correct shift to be in bounds
				ImageBuffer * ib;
				if (empv->otherSource)
					ib = empv->otherSource;
				else
					ib = empv->imgBuff;
				GetWindowRect(hwnd, &rect);
				if (empv->mx > ib->width/(float)(rect.right-rect.left)-1.0f/empv->zoom)
					empv->mx = ib->width/(float)(rect.right-rect.left)-1.0f/empv->zoom;
				if (empv->my > ib->height/(float)(rect.bottom-rect.top)-1.0f/empv->zoom)
					empv->my = ib->height/(float)(rect.bottom-rect.top)-1.0f/empv->zoom;
				if (empv->mx < 0.0f)
					empv->mx = 0.0f;
				if (empv->my < 0.0f)
					empv->my = 0.0f;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_DROPFILES:
			{
				HDROP hDrop = (HDROP)wParam;
				char droppedfilename[2048];
				unsigned int numFiles = DragQueryFile(hDrop, 0xFFFFFFFF, droppedfilename, 2048);
				if (numFiles > 1)
					MessageBox(hwnd, "No multiple files allowed.", "Error", 0);
				else
				{
					int copied = DragQueryFile(hDrop, 0, droppedfilename, 2048);
					if (copied > 0)
					{
						char * newName = copyString(droppedfilename);
						if (newName)
						{
							if (empv->lastDroppedFilename)
								free(empv->lastDroppedFilename);
							empv->lastDroppedFilename = newName;

							LONG wID;
							wID = GetWindowLong(hwnd, GWL_ID);
							SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PV_DROPPED_FILE), (LPARAM)hwnd);
						}
					}
				}
				
				DragFinish(hDrop);
			}
			break;
		case WM_PAINT:
			{
				empv->nowDrawing = true;
				hdc = BeginPaint(hwnd, &ps);

				// create
				RECT rect;
				GetClientRect(hwnd, &rect);
				HDC thdc = CreateCompatibleDC(hdc); 
				HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
				HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

				bool shiftClicked = (GetKeyState(VK_LSHIFT)& 0x8000) || (GetKeyState(VK_RSHIFT)&0x8000);
				empv->copyByteBufferToDevice(hwnd, thdc);

				empv->drawBuckets(thdc);

				// draw region borders
				if (empv->showRegions    ||    empv->mode == EMPView::MODE_REGION_AIRBRUSH   ||   empv->mode == EMPView::MODE_REGION_RECT)
					empv->drawRegionBorders(thdc);

				// draw airbrush shape
				if (empv->mode == EMPView::MODE_REGION_AIRBRUSH    &&   (empv->lmClicked  ||  empv->rmClicked)   &&   !shiftClicked)
				{
					POINT p;
					GetCursorPos(&p);
					ScreenToClient(hwnd, &p);
					empv->drawAirShadow(thdc, p.x, p.y);
				}

				// draw rectangle shape
				if (empv->mode == EMPView::MODE_REGION_RECT   &&   (empv->lmClicked  ||  empv->rmClicked)   &&   !shiftClicked)
				{
					POINT p;
					GetCursorPos(&p);
					ScreenToClient(hwnd, &p);
					empv->drawRectShadow(thdc, cpx, cpy, p.x, p.y);
				}

				// draw control's border
				if (empv->showBorder)
				{
					GetClientRect(hwnd, &rect);
					HPEN oldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, empv->colBorder));
					POINT border[] = {	{0,0}, {rect.right-1,0}, 
										{rect.right-1, rect.bottom-1}, 
										{0, rect.bottom-1}, {0,0} };
					Polyline(thdc, border, 5);
					DeleteObject(SelectObject(thdc, oldPen));
				}


				if (empv->show_histogram)
					empv->drawHistogram(thdc);


				if (empv->opacityToParent)
				{
					BLENDFUNCTION blendFunction;
					blendFunction.BlendOp = AC_SRC_OVER;
					blendFunction.BlendFlags = 0;
					blendFunction.SourceConstantAlpha = 255;
					blendFunction.AlphaFormat = AC_SRC_ALPHA;
					AlphaBlend(hdc, 0, 0,  rect.right, rect.bottom,  thdc, 0, 0, rect.right, rect.bottom, blendFunction);
				}
				else
				{
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);
				}

				SelectObject(thdc, oldBMP);
				DeleteObject(hbmp);
				DeleteDC(thdc);
				EndPaint(hwnd, &ps);

				empv->nowDrawing = false;
			}
			break;
		case WM_LBUTTONDOWN:
			{
				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PV_CLICKED), (LPARAM)hwnd);

				SetFocus(hwnd);
				empv->lmClicked = true;
				if (!GetCapture())
				{	// nothing is pressed
					if (empv->mode == EMPView::MODE_HYPERLINK  &&  empv->hyperlink)
					{
						ShellExecute(hwnd, "open", empv->hyperlink, NULL, NULL, SW_SHOWNORMAL);
						break;
					}
					SetCapture(hwnd);
					if (empv->mode == EMPView::MODE_PICK   &&   !(wParam&MK_SHIFT))
					{
						int xx = (short)LOWORD(lParam); 
						int yy = (short)HIWORD(lParam); 
						empv->notifyPickPixel(xx, yy);
					}
					if (empv->mode == EMPView::MODE_SHOW   ||   (wParam&MK_SHIFT))
					{
						cpx = (short)LOWORD(lParam); 
						cpy = (short)HIWORD(lParam); 
						cmx = empv->mx;
						cmy = empv->my;
						SetCursor(LoadCursor(NULL, IDC_HAND));
					}
					if (empv->mode == EMPView::MODE_REGION_AIRBRUSH    &&    !(wParam&MK_SHIFT))
					{
						if (!empv->rgnPtr)
							break;
						if (!empv->rgnPtr->regions)
							break;

						POINT pos;
						pos.x = (short)LOWORD(lParam); 
						pos.y = (short)HIWORD(lParam); 

						RECT rect;
						GetClientRect(hwnd, &rect);
						int cx = rect.right-rect.left;
						int cy = rect.bottom-rect.top;
						int tx = (int)(pos.x/empv->zoom + empv->mx*cx);
						int ty = (int)(pos.y/empv->zoom + empv->my*cy);
						int trX = tx/RSIZE;
						int trY = ty/RSIZE;
						int pp = empv->rgnPtr->airbrush_size/RSIZE+2;
						int tmx, tmy;
						int i,j;

						for (i=trX-pp; i<trX+pp; i++)
							for (j=trY-pp; j<trY+pp; j++)
							{
								if (j<0 || i<0 || j>=empv->rgnPtr->rnY || i>=empv->rgnPtr->rnX)
									continue;
								tmx = i*RSIZE+RSIZE/2 - tx;
								tmy = j*RSIZE+RSIZE/2 - ty;
								if (tmx*tmx + tmy*tmy < empv->rgnPtr->airbrush_size*empv->rgnPtr->airbrush_size)
									empv->rgnPtr->regions[i + j*empv->rgnPtr->rnX] = true;
							}

						empv->notifyRegionsChanged();

						GetClientRect(hwnd, &rect);
						InvalidateRect(hwnd, &rect, false);
					}
				}
				if (empv->mode == EMPView::MODE_REGION_RECT   &&   !(wParam&MK_SHIFT))
				{
					if (!empv->rgnPtr)
						break;
					if (!empv->rgnPtr->regions)
						break;

					SetCursor(LoadCursor(NULL, IDC_CROSS));
					if (empv->rmClicked)
					{
						empv->updateRegionsRect(cpx, cpy, (short)LOWORD(lParam), (short)HIWORD(lParam), false);
						empv->rmClicked = false;
						empv->lmClicked = false;
						empv->rgnPtr->makeRegionsList();
						empv->notifyRegionsChanged();
					}
					cpx = (short)LOWORD(lParam); 
					cpy = (short)HIWORD(lParam); 
					// repaint
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}
			}
			break;
		case WM_LBUTTONUP:
			{
				if (GetCapture() == hwnd)
				{	// this window
				}
				ReleaseCapture();
				if (empv->mode == EMPView::MODE_REGION_AIRBRUSH   &&   !(wParam&MK_SHIFT))
				{
					if (!empv->rgnPtr)
						break;
					empv->rgnPtr->makeRegionsList();
					empv->notifyRegionsChanged();
				}
				if (empv->mode == EMPView::MODE_REGION_RECT    &&    !(wParam&MK_SHIFT))
				{
					if (!empv->rgnPtr)
						break;
					if (!empv->rgnPtr->regions)
						break;
					if (empv->lmClicked)
						empv->updateRegionsRect(cpx, cpy, (short)LOWORD(lParam), (short)HIWORD(lParam), true);
					empv->rgnPtr->makeRegionsList();
					empv->notifyRegionsChanged();
					// repaint
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}
				empv->lmClicked = false;
				
				if (empv->opacityToParent)
				{
					RECT wrect;
					GetWindowRect(hwnd, &wrect);
					POINT p;
					p.x = wrect.left;	p.y = wrect.top;
					ScreenToClient(GetParent(hwnd), &p);
					wrect.right = wrect.right + p.x - wrect.left;
					wrect.bottom = wrect.bottom + p.y - wrect.top;
					wrect.left = p.x;
					wrect.top = p.y;
					InvalidateRect(GetParent(hwnd), &wrect, false);
				}

				RECT crect;
				GetClientRect(hwnd, &crect);
				InvalidateRect(hwnd, &crect, true);
			}
			break;
		case WM_MBUTTONDOWN:
			{
				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PV_MIDDLECLICKED), (LPARAM)hwnd);
			}
			break;
		case WM_RBUTTONDOWN:
			{
				SetFocus(hwnd);
				empv->rmClicked = true;
				if (!GetCapture())
					SetCapture(hwnd);

				if (empv->mode == EMPView::MODE_REGION_AIRBRUSH   &&   !(wParam&MK_SHIFT))
				{
					if (!empv->rgnPtr)
						break;
					if (!empv->rgnPtr->regions)
						break;

					POINT pos;
					pos.x = (short)LOWORD(lParam); 
					pos.y = (short)HIWORD(lParam); 

					RECT rect;
					GetClientRect(hwnd, &rect);
					int cx = rect.right-rect.left;
					int cy = rect.bottom-rect.top;
					int tx = (int)(pos.x/empv->zoom + empv->mx*cx);
					int ty = (int)(pos.y/empv->zoom + empv->my*cy);
					int trX = tx/RSIZE;
					int trY = ty/RSIZE;
					int pp = empv->rgnPtr->airbrush_size/RSIZE+2;
					int tmx, tmy;
					int i,j;

					for (i=trX-pp; i<trX+pp; i++)
						for (j=trY-pp; j<trY+pp; j++)
						{
							if (j<0 || i<0 || j>=empv->rgnPtr->rnY || i>=empv->rgnPtr->rnX)
								continue;
							tmx = i*RSIZE+RSIZE/2 - tx;
							tmy = j*RSIZE+RSIZE/2 - ty;
							if (tmx*tmx + tmy*tmy < empv->rgnPtr->airbrush_size*empv->rgnPtr->airbrush_size)
								empv->rgnPtr->regions[i + j*empv->rgnPtr->rnX] = false;
						}

					empv->notifyRegionsChanged();

					// repaint
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}

				if (empv->mode == EMPView::MODE_REGION_RECT    &&    !(wParam&MK_SHIFT))
				{
					if (!empv->rgnPtr)
						break;
					if (!empv->rgnPtr->regions)
						break;
					if (empv->lmClicked)
					{
						empv->rmClicked = false;
						empv->lmClicked = false;
						empv->updateRegionsRect(cpx, cpy, (short)LOWORD(lParam), (short)HIWORD(lParam), true);
						empv->rgnPtr->makeRegionsList();
						empv->notifyRegionsChanged();
					}
					cpx = (short)LOWORD(lParam); 
					cpy = (short)HIWORD(lParam); 
					// repaint
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}

			}
			break;
		case WM_RBUTTONUP:
			{
				ReleaseCapture();
				if (empv->mode == EMPView::MODE_REGION_AIRBRUSH   &&   !(wParam&MK_SHIFT))
				{
					if (empv->rgnPtr)
						empv->rgnPtr->makeRegionsList();
				}
				if (empv->mode == EMPView::MODE_REGION_RECT   &&   !(wParam&MK_SHIFT))
				{
					if (!empv->rgnPtr)
						break;
					if (!empv->rgnPtr->regions)
						break;
					if (empv->rmClicked)
						empv->updateRegionsRect(cpx, cpy, (short)LOWORD(lParam), (short)HIWORD(lParam), false);
					empv->rgnPtr->makeRegionsList();
					empv->notifyRegionsChanged();
					// repaint
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}
				empv->rmClicked = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_CAPTURECHANGED:
			{
			}
			break;
		case WM_MOUSEMOVE:
			{
				if (empv->mode==EMPView::MODE_REGION_AIRBRUSH  ||  empv->mode==EMPView::MODE_REGION_RECT  ||  empv->mode==EMPView::MODE_PICK)
				{
					SetCursor(LoadCursor (NULL, IDC_CROSS));
				}
				else
				{
					if (wParam|MK_LBUTTON)
					{}//SetCursor(LoadCursor(NULL, IDC_HAND));
					else
						SetCursor(LoadCursor(NULL, IDC_ARROW));
				}
				if (GetCapture() == hwnd   &&   (wParam|MK_LBUTTON) )
				{	// current window was clicked
					if (empv->mode == EMPView::MODE_SHOW   ||   (wParam&MK_SHIFT))
					{	// image shifting
						GetClientRect(hwnd, &rect);
						POINT pos;
						pos.x = (short)LOWORD(lParam); 
						pos.y = (short)HIWORD(lParam); 

						// shift mouse on borders
						int xRes = GetSystemMetrics(SM_CXSCREEN);
						int yRes = GetSystemMetrics(SM_CYSCREEN);
						POINT pt1 = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
						ClientToScreen(hwnd, &pt1);
						// x
						if (pt1.y > yRes-2)
						{
							SetCursorPos(pt1.x, 1);
							cpy -= yRes;
							break;
						}
						if (pt1.y < 1)
						{
							SetCursorPos(pt1.x, yRes-2);
							cpy += yRes;
							break;
						}
						//y
						if (pt1.x > xRes-2)
						{
							SetCursorPos(1, pt1.y);
							cpx -= xRes;
							break;
						}
						if (pt1.x < 1)
						{
							SetCursorPos(xRes-2, pt1.y);
							cpx += xRes;
							break;
						}
						

						// calc new image shift
						empv->mx = cmx + (cpx-pos.x)/(float)rect.right/empv->zoom;
						empv->my = cmy + (cpy-pos.y)/(float)rect.bottom/empv->zoom;

						ImageBuffer * ib;
						if (empv->otherSource)
							ib = empv->otherSource;
						else
							ib = empv->imgBuff;

						// correct shift to be in bounds
						if (empv->byteBuff)
						{
							if (empv->mx > empv->byteBuff->width/(float)rect.right-1.0f/empv->zoom)
								empv->mx = empv->byteBuff->width/(float)rect.right-1.0f/empv->zoom;
							if (empv->my > empv->byteBuff->height/(float)rect.bottom-1.0f/empv->zoom)
								empv->my = empv->byteBuff->height/(float)rect.bottom-1.0f/empv->zoom;
						}
						if (empv->mx < 0.0f)
							empv->mx = 0.0f;
						if (empv->my < 0.0f)
							empv->my = 0.0f;

						GetClientRect(hwnd, &rect);
						InvalidateRect(hwnd, &rect, false);
					}
				}

				if (empv->mode == EMPView::MODE_REGION_AIRBRUSH   &&   !(wParam&MK_SHIFT))
				{
					bool lPressed = (wParam == MK_LBUTTON);
					bool rPressed = (wParam == MK_RBUTTON);
					if ((lPressed)   ||   (rPressed))
					{
						if (!empv->rgnPtr)
							break;
						if (!empv->rgnPtr->regions)
							break;

						POINT pos;
						pos.x = (short)LOWORD(lParam); 
						pos.y = (short)HIWORD(lParam); 

						RECT rect;
						GetClientRect(hwnd, &rect);
						int cx = rect.right-rect.left;
						int cy = rect.bottom-rect.top;
						int tx = (int)(pos.x/empv->zoom + empv->mx*cx);
						int ty = (int)(pos.y/empv->zoom + empv->my*cy);
						int trX = tx/RSIZE;
						int trY = ty/RSIZE;
						int pp = empv->rgnPtr->airbrush_size/RSIZE+2;
						int tmx, tmy;
						int i,j;

						for (i=trX-pp; i<trX+pp; i++)
							for (j=trY-pp; j<trY+pp; j++)
							{
								if (j<0 || i<0 || j>=empv->rgnPtr->rnY || i>=empv->rgnPtr->rnX)
									continue;
								tmx = i*RSIZE+RSIZE/2 - tx;
								tmy = j*RSIZE+RSIZE/2 - ty;
								if (tmx*tmx + tmy*tmy < empv->rgnPtr->airbrush_size*empv->rgnPtr->airbrush_size)
								{
									if (lPressed)
										empv->rgnPtr->regions[i + j*empv->rgnPtr->rnX] = true;
									if (rPressed)
										empv->rgnPtr->regions[i + j*empv->rgnPtr->rnX] = false;
								}
							}

						empv->notifyRegionsChanged();

						GetClientRect(hwnd, &rect);
						InvalidateRect(hwnd, &rect, false);

					}
				}

				if (empv->mode == EMPView::MODE_REGION_RECT   &&   !(wParam&MK_SHIFT))
				{
					if (!empv->rgnPtr)
						break;
					if (!empv->rgnPtr->regions)
						break;
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}

				// send WM_MOUSEMOVE message to parent
				POINT pos;
				pos.x = (short)LOWORD(lParam);
				pos.y = (short)HIWORD(lParam);
				ClientToScreen(hwnd, &pos);
				ScreenToClient(GetParent(hwnd), &pos);
				LPARAM s = MAKELONG(pos.x, pos.y);
				SendMessage(GetParent(hwnd), WM_MOUSEMOVE, wParam, s);
			}
			break;
		case WM_MOUSEWHEEL:
			{
				// a button is still pressed
				if (GetCapture())
					break;

				if (empv->dontLetUserChangeZoom)
					break;

				// mouse wheeled outside control
				POINT pos;
				pos.x = (short)LOWORD(lParam); 
				pos.y = (short)HIWORD(lParam); 
				GetWindowRect(hwnd, &rect);
				if (!PtInRect(&rect, pos))
					break;

				int xPos = (short)LOWORD(lParam); 
				int yPos = (short)HIWORD(lParam); 
				
				// count mouse u,v on control
				GetWindowRect(hwnd, &rect);
				float lastzoom = empv->zoom;
				float ou = (xPos-rect.left)/(float)(rect.right-rect.left);
				float ov = (yPos-rect.top)/(float)(rect.bottom-rect.top);

				int zDelta = (int)wParam;
				if (zDelta>0)
				{	// more zoom
					empv->zoom *= 1.1892071150027210667174999705605f;		// 2^(1/4)
					if (empv->zoom > 16.0f)
						empv->zoom = 16.0f;
				}
				else
				{	// less zoom
					empv->zoom *= 0.84089641525371454303112547623321f;		// 0.5^(1/4)
					if (empv->zoom < 0.0625f)
						empv->zoom = 0.0625f;
				}

				if (fabs(empv->zoom - 1) < 0.01f)
					empv->zoom = 1;

				// not less than original size
				float u = (empv->mx*lastzoom + ou)/lastzoom;
				float v = (empv->my*lastzoom + ov)/lastzoom;
				float mx = (u*empv->zoom-ou)/empv->zoom;
				float my = (v*empv->zoom-ov)/empv->zoom;
				empv->mx = mx;
				empv->my = my;

				ImageBuffer * ib;
				if (empv->otherSource)
					ib = empv->otherSource;
				else
					ib = empv->imgBuff;

				if (empv->mx > empv->byteBuff->width/(float)(rect.right-rect.left)-1.0f/empv->zoom)
					empv->mx = empv->byteBuff->width/(float)(rect.right-rect.left)-1.0f/empv->zoom;
				if (empv->my > empv->byteBuff->height/(float)(rect.bottom-rect.top)-1.0f/empv->zoom)
					empv->my = empv->byteBuff->height/(float)(rect.bottom-rect.top)-1.0f/empv->zoom;
				if (empv->mx < 0.0f)
					empv->mx = 0.0f;
				if (empv->my < 0.0f)
					empv->my = 0.0f;
				
				// repaint
				GetClientRect(hwnd, &rect);
				InvalidateRect(hwnd, &rect, false);
			}
			break;
		case WM_LBUTTONDBLCLK:
			{
				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PV_DOUBLECLICKED), (LPARAM)hwnd);
			}
			break;
		case WM_KEYDOWN:
			{
				if (wParam == 67)     //C
				{
					if(GetAsyncKeyState(VK_CONTROL))
					{
						LONG wID;
						wID = GetWindowLong(hwnd, GWL_ID);
						SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PV_CTRL_C), (LPARAM)hwnd);
					}
				}
			}
			break;


		case WM_NOTIFY:
		{
			if (((LPNMHDR)lParam)->code == TTN_GETDISPINFO)
			{
				LPNMTTDISPINFO lpnmtdi = (LPNMTTDISPINFO) lParam;

				lpnmtdi->lpszText = empv->toolTxt;
				lpnmtdi->hinst = NULL;
				lpnmtdi->uFlags = TTF_DI_SETITEM;

				SendMessage (((LPNMHDR)lParam)->hwndFrom, TTM_SETMAXTIPWIDTH, 0, 300);

				return 0;
			} 
			return DefWindowProc(hwnd, msg, wParam, lParam);
		}
		break;
	}

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMPView::copyByteBufferToDevice(HWND hwnd, HDC &hdc)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	int cx = rect.right-rect.left;
	int cy = rect.bottom-rect.top;

	if (!byteBuff)
	{
		HBRUSH tbr = CreateSolidBrush(RGB(0,0,0));
		HPEN tpen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
		HBRUSH o_br  = (HBRUSH)SelectObject(hdc, tbr);
		HPEN o_pen = (HPEN)SelectObject(hdc, tpen);
		Rectangle(hdc, 0,0, cx,cy);
		DeleteObject((HBRUSH)SelectObject(hdc, o_br));
		DeleteObject((HPEN)SelectObject(hdc, o_pen));
		return;
	}

	ImageByteBuffer * originalBuffer = byteBuff;
	originalBuffer->lockBuffer();

	if (zoom < 0.0625f)
		zoom = 0.0625f;
	if (zoom > 16)
		zoom = 16;

	HDC mydc = CreateCompatibleDC(hdc); 

	if (mx > byteBuff->width/(float)rect.right-1.0f/zoom)
		mx = byteBuff->width/(float)rect.right-1.0f/zoom;
	if (my > byteBuff->height/(float)rect.bottom-1.0f/zoom)
		my = byteBuff->height/(float)rect.bottom-1.0f/zoom;
	if (mx < 0.0f)
		mx = 0.0f;
	if (my < 0.0f)
		my = 0.0f;

	if (zoom * byteBuff->width < rect.right)
		mx = (0.5f*byteBuff->width/rect.right    -0.5f/zoom);
	if (zoom * byteBuff->height < rect.bottom)
		my = (0.5f*byteBuff->height/rect.bottom   -0.5f/zoom);

	HBITMAP hbmp, oldBmp;
	unsigned char * buf;
	unsigned char * line;
	#ifdef BBUF_SSE
		int ccx = ((cx+3)/4)*4;
		int lineSize = ccx*4;
		buf = (unsigned char *)_aligned_malloc(cy*lineSize, 16);
	#else
		int lineSize = cx*4;
		buf = (unsigned char *)malloc (cy*lineSize);
	#endif

	line = buf;
	int tx,ty;
	unsigned int bgVal;
	((unsigned char*)(&bgVal))[0] = GetBValue(colBackground);
	((unsigned char*)(&bgVal))[1] = GetGValue(colBackground);
	((unsigned char*)(&bgVal))[2] = GetRValue(colBackground);
	((unsigned char*)(&bgVal))[3] = 255;

	bool noZoom = false;
	if (fabs(zoom-1.0f) < 0.0001)
		zoom = 1;

	bool zoomIsOne = (fabs(zoom-1) < 0.01f);

	if (drawNice  &&  !zoomIsOne)
	{
		for (int i=0; i<cy; i++)
		{
			for (int j=0; j<cx; j++)
			{
				int ox1, ox2, oy1, oy2;
				float rx1, rx2, ry1, ry2;

				rx1 = (j/zoom + mx*cx);
				ry1 = ((cy-i-1)/zoom + my*cy);
				rx2 = ((j+1)/zoom + mx*cx);
				ry2 = ((cy-i)/zoom + my*cy);
				ox1 = (int)floor(rx1);
				ox2 = (int)ceil(rx2);
				oy1 = (int)floor(ry1);
				oy2 = (int)ceil(ry2);

				float r = 0;
				float g = 0;
				float b = 0;
				float pp = 0;
				float wx,wy;

				for (int y=oy1; y<oy2; y++)
				{
					wy = 1.0f;
					if (y==oy1)
						wy = oy1 - ry1 + 1;
					if (y==oy2-1)
						wy = ry2 - oy2 + 1;
					if (ry2-ry1<1  &&  oy1==oy2-1)
						wy = 1.0f/zoom;
				

					for (int x=ox1; x<ox2; x++)
					{
						wx = 1.0f;
						if (x==ox1)
							wx = ox1 - rx1 + 1;
						if (x==ox2-1)
							wx = rx2 - ox2 + 1;
						if (rx2-rx1<1  &&  ox1==ox2-1)
							wx = 1.0f/zoom;
	
						float wg = wx * wy;

						if (x<byteBuff->width   &&   x>=0   &&   y<byteBuff->height   &&   y>=0)
						{
							pp++;
							r += GetRValue(byteBuff->imgBuf[y][x]) * wg;
							g += GetGValue(byteBuff->imgBuf[y][x]) * wg;
							b += GetBValue(byteBuff->imgBuf[y][x]) * wg;
						}
						else
						{
							r += GetRValue(colBackground) * wg;
							g += GetGValue(colBackground) * wg;
							b += GetBValue(colBackground) * wg;
						}
					}
				}

				pp = zoom * zoom;
				if (pp>0)
				{
					r *= pp;
					g *= pp;
					b *= pp;
				}

				line[4*j+0] = (unsigned char)(int)min(255, max(0, b));
				line[4*j+1] = (unsigned char)(int)min(255, max(0, g));
				line[4*j+2] = (unsigned char)(int)min(255, max(0, r));
				line[4*j+3] = 255;
			}
			line += lineSize;
		}
	}
	else
	{
		if (zoomIsOne  ||  noZoom)
		{
			float mycy = my*cy;
			float mxcx = mx*cx;
			int imycy = (int)mycy;
			int imxcx = (int)mxcx;
			if (noZoom)
				imycy = imxcx = 0;



			if (useImageOpacity)	// ----- ALPHA/OPACITY
			{
				unsigned char bgrR = ((bgVal&0xff0000)>>16);
				unsigned char bgrG = ((bgVal&0xff00)>>8);
				unsigned char bgrB = ((bgVal&0xff));
				for (int i=0; i<cy; i++)
				{
					ty = (cy-i-1) + imycy;
					for (int j=0; j<cx; j++)
					{
						tx = j + imxcx;
						if (tx>=byteBuff->width || tx<0 || ty>=byteBuff->height || ty<0)
						{
							(unsigned int&)line[4*j] = 0;
						}
						else
						{
							unsigned int bbb = byteBuff->imgBuf[ty][tx];
							if (opacityToParent)
							{
								unsigned char a = (bbb&0xff000000)>>24;
								unsigned char b = (unsigned char)(((bbb&0xff0000)>>16)*a/255);
								unsigned char g = (unsigned char)(((bbb&0xff00)>>8)*a/255);
								unsigned char r = (unsigned char)(((bbb&0xff))*a/255);
								(unsigned int&)line[4*j] = (r<<16)  |  (g<<8)  |  (b)  |  (a<<24);
							}
							else
							{
								unsigned char alpha = ((bbb&0xff000000)>>24);
								unsigned char r = ((bbb&0xff))			* alpha / 255 + bgrR * (255-alpha) / 255;
								unsigned char g = ((bbb&0xff00)>>8)		* alpha / 255 + bgrG * (255-alpha) / 255;
								unsigned char b = ((bbb&0xff0000)>>16)	* alpha / 255 + bgrB * (255-alpha) / 255;
								(unsigned int&)line[4*j] = (r<<16)  |  (g<<8)  |  (b)  |  0xff000000;
							}
						}
					}
					line += lineSize;
				}
			}
			else
			{
				for (int i=0; i<cy; i++)
				{
					ty = (cy-i-1) + imycy;
					for (int j=0; j<cx; j++)
					{
						tx = j + imxcx;
						if (tx>=byteBuff->width || tx<0 || ty>=byteBuff->height || ty<0)
						{
							(unsigned int&)line[4*j] = bgVal;
						}
						else
						{
							unsigned int bbb = byteBuff->imgBuf[ty][tx];
							(unsigned int&)line[4*j] = ((bbb&0xff)<<16)  |  (bbb&0xff00)  |  ((bbb&0xff0000)>>16)  |  0xff000000;
						}
					}
					line += lineSize;
				}
			}
		}
		else
		{
			float mycy = my*cy;
			float mxcx = mx*cx;

			for (int i=0; i<cy; i++)
			{
				ty = (int)((cy-i-1)/zoom + mycy);
				for (int j=0; j<cx; j++)
				{
					tx = (int)(j/zoom + mxcx);
					if (tx>=byteBuff->width  ||  tx<0  ||  ty>=byteBuff->height  ||  ty<0)
					{
						(unsigned int&)line[4*j] = bgVal;
					}
					else
					{
						unsigned int bbb = byteBuff->imgBuf[ty][tx];
						(unsigned int&)line[4*j] = ((bbb&0xff)<<16)  |  (bbb&0xff00)  |  ((bbb&0xff0000)>>16)  |  0xff000000;

					}
				}
				line += lineSize;
			}
		}

	}

	originalBuffer->unlockBuffer();


	if (useImageOpacity  &&  opacityToParent)
	{
		BITMAPV5HEADER h5;
		ZeroMemory(&h5, sizeof(h5));
		h5.bV5Size = sizeof(BITMAPV5HEADER);
		h5.bV5Width = ccx; 
		h5.bV5Height = cy; 
		h5.bV5Planes = 1;
		h5.bV5BitCount = 32;
		h5.bV5Compression = BI_RGB;
		h5.bV5SizeImage = lineSize*cy;
		h5.bV5XPelsPerMeter = 96;
		h5.bV5YPelsPerMeter = 96;
		h5.bV5ClrUsed = 0;
		h5.bV5ClrImportant = 0;
		h5.bV5RedMask = 0x00FF0000;
		h5.bV5GreenMask = 0x0000FF00;
		h5.bV5BlueMask = 0x000000FF;
		h5.bV5AlphaMask = 0xFF000000;
		h5.bV5CSType = LCS_WINDOWS_COLOR_SPACE;
		h5.bV5Intent = LCS_GM_BUSINESS; 
		h5.bV5ProfileData = 0; 
		h5.bV5ProfileSize = 0; 
		h5.bV5Reserved = 0; 
	
		hbmp = CreateCompatibleBitmap(hdc, cx, cy);
		int ccc = SetDIBits(mydc, hbmp, 0, cy, buf, (BITMAPINFO*)&h5, DIB_RGB_COLORS);

		oldBmp = (HBITMAP)SelectObject(mydc, hbmp);

		BLENDFUNCTION blendFunction;
		blendFunction.BlendOp = AC_SRC_OVER;
		blendFunction.BlendFlags = 0;
		blendFunction.SourceConstantAlpha = 255;
		blendFunction.AlphaFormat = AC_SRC_ALPHA;
		AlphaBlend(hdc, 0, 0,  rect.right, rect.bottom,  mydc, 0, 0, rect.right, rect.bottom, blendFunction);

	}
	else
	{
		BITMAPINFOHEADER bmih;
		ZeroMemory(&bmih, sizeof(bmih));
		bmih.biSize = sizeof(BITMAPINFOHEADER);
		bmih.biBitCount = 32;
		bmih.biClrImportant = 0;
		bmih.biClrUsed = 0;
		bmih.biCompression =  BI_RGB;
		bmih.biHeight = cy;
		bmih.biWidth = ccx;
		bmih.biPlanes = 1;
		bmih.biSizeImage = lineSize*cy;
		bmih.biXPelsPerMeter = 0;
		bmih.biYPelsPerMeter = 0;

		BITMAPINFO bmi;
		bmi.bmiHeader = bmih;

		hbmp = CreateCompatibleBitmap(hdc, cx, cy);
		int ccc = SetDIBits(mydc, hbmp, 0, cy, buf, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);

		oldBmp = (HBITMAP)SelectObject(mydc, hbmp);

		BitBlt(hdc, 0, 0, rect.right, rect.bottom, mydc, 0, 0, SRCCOPY);
	}
	
	SelectObject(mydc, oldBmp);
	DeleteObject(hbmp);
	DeleteDC(mydc);

	#ifdef BBUF_SSE
		_aligned_free(buf);
	#else
		free(buf);
	#endif
}



//-------------------------------------------------------------------------------------------------------------

void EMPView::copyBufferToDevice(HWND hwnd, HDC &hdc, float zoom, float &mX, float &mY, COLORREF bgCol)	// not used anymore!!!!
{ 
	if (zoom < 0.0625f)
		zoom = 0.0625f;
	if (zoom > 16)
		zoom = 16;

	HDC mydc = CreateCompatibleDC(hdc); 

	RECT rect;
	GetClientRect(hwnd, &rect);
	int cx = rect.right-rect.left;
	int cy = rect.bottom-rect.top;

	ImageBuffer * ib;
	if (otherSource)
		ib = otherSource;
	else
		ib = imgBuff;

	if (zoom * ib->width < rect.right)
		mX = (0.5f*ib->width/rect.right    -0.5f/zoom);
	if (zoom * ib->height < rect.bottom)
		mY = (0.5f*ib->height/rect.bottom   -0.5f/zoom);
	
	int ix = ib->width;
	int iy = ib->height;

	HBITMAP hbmp, oldBmp;
	unsigned char * buf;
	unsigned char * line;

	int lineSize = cx*4;

	buf = (unsigned char *)malloc (cy*lineSize);
	line = buf;
	Color4 * tcol;
	Color4 wcol;
	int tx,ty;

	bool noZoom = false;
	if (fabs(zoom-1.0f) < 0.0001)
		zoom = 1;

	float gc = 1.0f/gamma;

	// find world color and max world color
	Color4 wc = Color4(0,0,0);
	Color4 cc;
	float fwcm = 0;
	float fwc;
	int rr = 10;
	for (int i=0; i<iy/rr; i++)
	{
		for (int j=0; j<ix/rr; j++)
		{
			cc = ib->imgBuf[i*rr][j*rr];
			fwc = 0.2126f * cc.r    +    0.7152f * cc.g    +    0.0722f * cc.b;

			fwcm = max(fwcm, fwc);
			wc += cc;
		}
	}
	fwcm *= multiplier;
	wc  =  wc * (1.0f/(ix/rr)*(iy/rr)) * multiplier;
	fwc = fwc * (1.0f/(ix/rr)*(iy/rr)) * multiplier;
	float mm = log(10.0f)/log(fwcm+1);
	float lexp = (-log(tone_bias));
	float rfwcm = 8/pow(fwcm, lexp);
	Color4 luminanceWeight = Color4(0.2126f, 0.7152f, 0.0722f, 0.0f);
	// end find world color

	for (int i=0; i<cy; i++)
	{
		for (int j=0; j<cx; j++)
		{
			if (noZoom)
			{
				tx = j;
				ty = i;
			}
			else
			{
				tx = (int)(j/zoom+mX*cx);
				ty = (int)(i/zoom+mY*cy);
			}
			if (tx>=ib->width || tx<0 || ty>=ib->height || ty<0)
			{
				line[4*j]   = GetBValue(bgCol);
				line[4*j+1] = GetGValue(bgCol);
				line[4*j+2] = GetRValue(bgCol);
				line[4*j+3] = 255;
			}
			else
			{
				if (ib->hitsBuf[ty][tx] < 1)
				{
					line[4*j]   = GetBValue(bgCol);
					line[4*j+1] = GetGValue(bgCol);
					line[4*j+2] = GetRValue(bgCol);
					line[4*j+3] = 255;
				}
				else
				{

					tcol = &(ib->imgBuf[ty][tx]);
					float p = 1.0f/ib->hitsBuf[ty][tx]*multiplier;
					wcol = *tcol * p;
					if (use_tone_mapping)
					{
						// photographic - each channel on it's own .. with coefficient
						wcol.r = wcol.r/pow(1+wcol.r, tone_bias);
						wcol.g = wcol.g/pow(1+wcol.g, tone_bias);
						wcol.b = wcol.b/pow(1+wcol.b, tone_bias);
					}
					//gamma
					if (gamma > 1)
					{
						wcol.r = pow(wcol.r, gc);
						wcol.g = pow(wcol.g, gc);
						wcol.b = pow(wcol.b, gc);
					}

					line[4*j]   = (unsigned char)max(0,min(255,(unsigned int)(wcol.b*255)));
					line[4*j+1] = (unsigned char)max(0,min(255,(unsigned int)(wcol.g*255)));
					line[4*j+2] = (unsigned char)max(0,min(255,(unsigned int)(wcol.r*255)));
					line[4*j+3] = 255;
				}
			}
		}
		line += lineSize;
	}

	hbmp = CreateBitmap(cx, cy, 1, 32, buf);
	oldBmp = (HBITMAP)SelectObject(mydc, hbmp);
	BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, mydc, 0, 0, SRCCOPY);
	SelectObject(mydc, oldBmp);
	DeleteObject(hbmp);
	DeleteDC(mydc);
	free(buf);
}

void EMPView::drawHistogram(HDC &hdc)
{
	if (!byteBuff)
		return;
	if (hs_width + hs_posx > byteBuff->width)
		return;
	if (hs_height + hs_posy > byteBuff->height)
		return;

	//// activate gdi+
	ULONG_PTR gdiplusToken;
	ULONG_PTR gdiplusBGThreadToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartupOutput gdiplusStartupOutput;
	gdiplusStartupInput.SuppressBackgroundThread = TRUE;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, &gdiplusStartupOutput);
	Gdiplus::Status stat = gdiplusStartupOutput.NotificationHook(&gdiplusBGThreadToken); 
	if (stat != Gdiplus::Ok)
		return;

	Gdiplus::Graphics * gr = Gdiplus::Graphics::FromHDC(hdc);
	Gdiplus::SolidBrush  * halfBrush = new Gdiplus::SolidBrush(Gdiplus::Color(128, 0, 0, 0));

	gr->FillRectangle(halfBrush,  hs_posx, hs_posy, hs_width+2*hs_margin, hs_height+2*hs_margin);

	// release gdi+
	delete gr;
	delete halfBrush;
	gdiplusStartupOutput.NotificationUnhook(gdiplusBGThreadToken);
	Gdiplus::GdiplusShutdown(gdiplusToken);

	HPEN blackPen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
	HPEN redPen =	CreatePen(PS_SOLID, 1, RGB(212,0,0));
	HPEN greenPen = CreatePen(PS_SOLID, 1, RGB(0,212,0));
	HPEN bluePen =	CreatePen(PS_SOLID, 1, RGB(0,0,212));

	SetROP2(hdc, R2_COPYPEN);
	HPEN oldpen = (HPEN)SelectObject(hdc, blackPen);
	for (int i=0; i<256; i++)
	{
		unsigned int r = min(100, histogram[i*3+0]);
		unsigned int g = min(100, histogram[i*3+1]);
		unsigned int b = min(100, histogram[i*3+2]);
		unsigned int m  = max(max(r,g),b);
		POINT op;
		MoveToEx(hdc, hs_margin+hs_posx+i, hs_margin+hs_height+hs_posy, &op);
		LineTo(hdc, hs_margin+hs_posx+i, hs_margin+hs_height+hs_posy-m);
	}

	SetROP2(hdc, R2_MERGEPEN);
	for (int i=0; i<256; i++)
	{
		unsigned int r = min(100, histogram[i*3+0]);
		unsigned int g = min(100, histogram[i*3+1]);
		unsigned int b = min(100, histogram[i*3+2]);
		POINT op;
		int x = hs_margin + hs_posx + i;
		int y = hs_margin + hs_height + hs_posy;
		SelectObject(hdc, redPen);
		MoveToEx(hdc, x, y, &op);
		LineTo(  hdc, x, y-r);
		SelectObject(hdc, greenPen);
		MoveToEx(hdc, x, y, &op);
		LineTo(  hdc, x, y-g);
		SelectObject(hdc, bluePen);
		MoveToEx(hdc, x, y, &op);
		LineTo(  hdc, x, y-b);
	}

	SetROP2(hdc, R2_COPYPEN);
}

bool EMPView::updateHistogram()
{
	for (int i=0; i<768; i++)
		histogram[i] = 0;

	if (!byteBuff)
		return false;

	unsigned int per = byteBuff->width * byteBuff->height / 256 / 50;
	per = max(1, per);

	for (int y=0; y<byteBuff->height; y++)
		for (int x=0; x<byteBuff->width; x++)
		{
			COLORREF csrc = byteBuff->imgBuf[y][x];
			unsigned char r = GetRValue(csrc);
			unsigned char g = GetGValue(csrc);
			unsigned char b = GetBValue(csrc);

			histogram[r*3+0]++;
			histogram[g*3+1]++;
			histogram[b*3+2]++;
		}

	for (int i=0; i<768; i++)
		histogram[i] /= per;

	return true;
}

void EMPView::drawAirShadow(HDC &hdc, int x, int y)
{
	HPEN lPen = (HPEN)SelectObject(hdc, CreatePen(PS_NULL,1,RGB(127,127,127)));
	HBRUSH lBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(127,127,127)));
	SetROP2(hdc, R2_NOTXORPEN);
	if (!rgnPtr)
		return;
	int a_size = rgnPtr->airbrush_size;

	Ellipse(hdc, 
				x-(int)((a_size-RSIZE/2)*zoom), 
				y-(int)((a_size-RSIZE/2)*zoom), 
				x+(int)((a_size-RSIZE/2)*zoom), 
				y+(int)((a_size-RSIZE/2)*zoom));

	SetROP2(hdc, R2_COPYPEN);
	DeleteObject(SelectObject(hdc, lBrush));
	DeleteObject(SelectObject(hdc, lPen));
}

void EMPView::drawRectShadow(HDC &hdc, int x1, int y1, int x2, int y2)
{
	HPEN lPen = (HPEN)SelectObject(hdc, CreatePen(PS_NULL,1,RGB(127,127,127)));
	HBRUSH lBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(127,127,127)));
	SetROP2(hdc, R2_NOTXORPEN);

	Rectangle(hdc, x1, y1, x2, y2);

	SetROP2(hdc, R2_COPYPEN);
	DeleteObject(SelectObject(hdc, lBrush));
	DeleteObject(SelectObject(hdc, lPen));
}

void EMPView::drawRegionBorders(HDC &hdc)
{
	if (!rgnPtr)
		return;
	if (!rgnPtr->regions)
		return;

	RECT rect;
	GetClientRect(hwnd, &rect);
	int cx = rect.right-rect.left;
	int cy = rect.bottom-rect.top;
	int x1,x2,y1,y2;

	HPEN hpCur = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(127,127,127)));
	SetROP2(hdc, R2_NOTXORPEN);

	int i,j;
	for (j=0; j<rgnPtr->rnY; j++)
	{
		for (i=0; i<rgnPtr->rnX; i++)
		{
			if (rgnPtr->regions[i+j*rgnPtr->rnX])
			{
				// left
				if (i>0)
					if (!rgnPtr->regions[(i-1)+(j)*rgnPtr->rnX])
					{
						x1 = (int)((i*RSIZE-mx*cx)*zoom+0.5f)-1;
						x2 = (int)((i*RSIZE-mx*cx)*zoom+0.5f)-1;
						y1 = (int)((j*RSIZE-my*cy)*zoom+0.5f);
						y2 = (int)(((j+1)*RSIZE-my*cy)*zoom+0.5f);
						if (j == rgnPtr->rnY-1)
							y2 = (int)((byteBuff->height-my*cy)*zoom+0.5f);
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
					}
				if (i==0)
				{
						x1 = (int)((i*RSIZE-mx*cx)*zoom+0.5f)-1;
						x2 = (int)((i*RSIZE-mx*cx)*zoom+0.5f)-1;
						y1 = (int)((j*RSIZE-my*cy)*zoom+0.5f);
						y2 = (int)(((j+1)*RSIZE-my*cy)*zoom+0.5f);
						if (j == rgnPtr->rnY-1)
							y2 = (int)((byteBuff->height-my*cy)*zoom+0.5f);
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
				}
				// right
				if (i>-1 && i<rgnPtr->rnX-1)
					if (!rgnPtr->regions[(i+1)+(j)*rgnPtr->rnX])
					{
						x1 = (int)(((i+1)*RSIZE-mx*cx)*zoom+0.5f);
						x2 = (int)(((i+1)*RSIZE-mx*cx)*zoom+0.5f);
						y1 = (int)((j*RSIZE-my*cy)*zoom+0.5f);
						y2 = (int)(((j+1)*RSIZE-my*cy)*zoom+0.5f);
						if (j == rgnPtr->rnY-1)
							y2 = (int)((byteBuff->height-my*cy)*zoom+0.5f);
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
					}
				if (i == rgnPtr->rnX-1)
				{
						x1 = x2 = (int)((byteBuff->width-mx*cx)*zoom+0.5f);
						y1 = (int)((j*RSIZE-my*cy)*zoom+0.5f);
						y2 = (int)(((j+1)*RSIZE-my*cy)*zoom+0.5f);
						if (j == rgnPtr->rnY-1)
							y2 = (int)((byteBuff->height-my*cy)*zoom+0.5f);
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
				}
				// up
				if (j>0)
					if (!rgnPtr->regions[(i)+(j-1)*rgnPtr->rnX])
					{
						x1 = (int)((i*RSIZE-mx*cx)*zoom+0.5f);
						x2 = (int)(((i+1)*RSIZE-mx*cx)*zoom+0.5f);
						if (i == rgnPtr->rnX-1)
							x2 = (int)((byteBuff->width-mx*cx)*zoom+0.5f);
						y1 = (int)((j*RSIZE-my*cy)*zoom+0.5f)-1;
						y2 = (int)((j*RSIZE-my*cy)*zoom+0.5f)-1;
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
					}
				if (j==0)
				{
						x1 = (int)((i*RSIZE-mx*cx)*zoom+0.5f);
						x2 = (int)(((i+1)*RSIZE-mx*cx)*zoom+0.5f);
						if (i == rgnPtr->rnX-1)
							x2 = (int)((byteBuff->width-mx*cx)*zoom+0.5f);
						y1 = (int)((j*RSIZE-my*cy)*zoom+0.5f)-1;
						y2 = (int)((j*RSIZE-my*cy)*zoom+0.5f)-1;
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
				}
				// down
				if (j>-1  &&  j<rgnPtr->rnY-1)
					if (!rgnPtr->regions[(i)+(j+1)*rgnPtr->rnX])
					{
						x1 = (int)((i*RSIZE-mx*cx)*zoom+0.5f);
						x2 = (int)(((i+1)*RSIZE-mx*cx)*zoom+0.5f);
						if (i == rgnPtr->rnX-1)
							x2 = (int)((byteBuff->width-mx*cx)*zoom+0.5f);
						y1 = (int)(((j+1)*RSIZE-my*cy)*zoom+0.5f);
						y2 = (int)(((j+1)*RSIZE-my*cy)*zoom+0.5f);
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
					}
				if (j == rgnPtr->rnY-1)
				{
						x1 = (int)((i*RSIZE-mx*cx)*zoom+0.5f);
						x2 = (int)(((i+1)*RSIZE-mx*cx)*zoom+0.5f);
						if (i == rgnPtr->rnX-1)
							x2 = (int)((byteBuff->width-mx*cx)*zoom+0.5f);
						y1 = y2 = (int)((byteBuff->height-my*cy)*zoom+0.5f);
						MoveToEx(hdc, x1,y1, NULL);
						LineTo(hdc, x2,y2);
				}
			}
		}
	}
	SetROP2(hdc, R2_COPYPEN);
	DeleteObject(SelectObject(hdc, hpCur));

}

#define ROUND(a) (int)floor(a+0.5)

void EMPView::drawBuckets(HDC &hdc)
{
	if (!bucketsCoords  ||  nBuckets<1)
		return;

	RECT rect;
	GetClientRect(hwnd, &rect);
	int cx = rect.right-rect.left;
	int cy = rect.bottom-rect.top;

	HPEN hpCur = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(255,255,255)));
	int s = (int)(4*zoom);

	for (int i=0; i<nBuckets; i++)
	{
		int x1 = bucketsCoords[i*4+0];
		int y1 = bucketsCoords[i*4+1];
		int x2 = bucketsCoords[i*4+2];
		int y2 = bucketsCoords[i*4+3];

		if (x1==-1 && x2==-1 && y1==-1 && y2==-1)
			continue;

		if (zoom==1.0f)
		{
			x1 = x1-(int)(mx*cx);
			y1 = y1-(int)(my*cy);
			x2 = x2+1-(int)(mx*cx)-1;
			y2 = y2+1-(int)(my*cy)-1;
		}
		else
		{
			x1 = ROUND((x1-mx*cx)*zoom);
			y1 = ROUND((y1-my*cy)*zoom);
			x2 = ROUND((x2+1-mx*cx)*zoom)-1;
			y2 = ROUND((y2+1-my*cy)*zoom)-1;
		}

		MoveToEx(hdc, x1,y1+s, NULL);
		LineTo(hdc, x1,y1);
		LineTo(hdc, x1+s+1,y1);

		MoveToEx(hdc, x1,y2-s, NULL);
		LineTo(hdc, x1,y2);
		LineTo(hdc, x1+s+1,y2);

		MoveToEx(hdc, x2,y1+s, NULL);
		LineTo(hdc, x2,y1);
		LineTo(hdc, x2-s-1,y1);

		MoveToEx(hdc, x2,y2-s, NULL);
		LineTo(hdc, x2,y2);
		LineTo(hdc, x2-s-1,y2);
	}

	DeleteObject(SelectObject(hdc, hpCur));
}

bool EMPView::getColor(int x, int y, bool image_modified, Color4 &c, int & imagex, int &imagey, int aa)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	int cx = rect.right-rect.left;
	int cy = rect.bottom-rect.top;
	if (x<0 || x>=cx || y<0 || y>=cy)
		return false;

	ImageBuffer * ib;
	if (otherSource)
		ib = otherSource;
	else
		ib = imgBuff;

	imagex = (int)(x/zoom+mx*cx);
	imagey = (int)(y/zoom+my*cy);
	int tx = (int)(x/zoom+mx*cx)*aa;
	int ty = (int)(y/zoom+my*cy)*aa;

	if (tx>=ib->width || tx<0 || ty>=ib->height || ty<0)
		return false;

	int hits = ib->hitsBuf[ty][tx];
	if (hits > 0)
		c = (ib->imgBuf[ty][tx]);
	else
		c = Color4(0,0,0);

	return true;
}

void EMPView::updateRegionsRect(int x1, int y1, int x2, int y2, bool activate)
{
	if (!rgnPtr)
		return;
	if (!rgnPtr->regions)
		return;

	int X1,X2,Y1,Y2;
	if (x1>x2)
	{
		X1 = x2;
		X2 = x1;
	}
	else
	{
		X1 = x1;
		X2 = x2;
	}
	if (y1>y2)
	{
		Y1 = y2;
		Y2 = y1;
	}
	else
	{
		Y1 = y1;
		Y2 = y2;
	}

	
	RECT rect;
	GetClientRect(hwnd, &rect);
	int cx = rect.right-rect.left;
	int cy = rect.bottom-rect.top;

	int tx1,tx2,ty1,ty2;
	tx1 = (int)(X1/zoom + mx*cx)/RSIZE;
	tx2 = (int)(X2/zoom + mx*cx)/RSIZE+1;
	ty1 = (int)(Y1/zoom + my*cy)/RSIZE;
	ty2 = (int)(Y2/zoom + my*cy)/RSIZE+1;
	tx1 = min(rgnPtr->rnX, max(0, tx1));
	tx2 = min(rgnPtr->rnX, max(0, tx2));
	ty1 = min(rgnPtr->rnY, max(0, ty1));
	ty2 = min(rgnPtr->rnY, max(0, ty2));

	int i,j;
	for(i=tx1; i<tx2; i++)
		for(j=ty1; j<ty2; j++)
			rgnPtr->regions[j*rgnPtr->rnX+i] = activate;
}

bool EMPView::createRegions()
{
	ImageBuffer * ib;
	if (otherSource)
		ib = otherSource;
	else
		ib = imgBuff;

	int ibx = max(1, ib->width);
	int iby = max(1, ib->height);
	if (rgnPtr)
		return rgnPtr->createRegions(ibx, iby);
	return false;
}


void EMPView::notifyRegionsChanged()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PV_REGIONS_CHANGED), (LPARAM)hwnd);
}

void EMPView::notifyPickPixel(int ctrlX, int ctrlY)
{
	ppx = ctrlX;
	ppy = ctrlY;

	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PV_PIXEL_PICKED), (LPARAM)hwnd);
}

bool EMPView::evalControlPosToImagePos(int cx, int cy, float &ix, float &iy)
{
	RECT crect;
	GetClientRect(hwnd, &crect);
	ix = cx/zoom + mx*crect.right;
	iy = cy/zoom + my*crect.bottom;


	return true;
}

void EMPView::zoomIn()
{
	float lastzoom = zoom;
	zoom *= 1.1892071150027210667174999705605f;		// 2^(1/4)
	if (zoom > 16.0f)
		zoom = 16.0f;
	if (fabs(zoom - 1) < 0.01f)
		zoom = 1;

	float u = (mx*lastzoom + 0.5f)/lastzoom;
	float v = (my*lastzoom + 0.5f)/lastzoom;
	mx = (u*zoom-0.5f)/zoom;
	my = (v*zoom-0.5f)/zoom;

	InvalidateRect(hwnd, NULL, false);
}

void EMPView::zoomOut()
{
	float lastzoom = zoom;
	zoom *= 0.84089641525371454303112547623321f;		// 0.5^(1/4)
	if (zoom < 0.0625f)
		zoom = 0.0625f;
	if (fabs(zoom - 1) < 0.01f)
		zoom = 1;

	float u = (mx*lastzoom + 0.5f)/lastzoom;
	float v = (my*lastzoom + 0.5f)/lastzoom;
	mx = (u*zoom-0.5f)/zoom;
	my = (v*zoom-0.5f)/zoom;

	InvalidateRect(hwnd, NULL, false);
}

void EMPView::zoom100()
{
	float lastzoom = zoom;
	zoom = 1;

	float u = (mx*lastzoom + 0.5f)/lastzoom;
	float v = (my*lastzoom + 0.5f)/lastzoom;
	mx = (u*zoom-0.5f)/zoom;
	my = (v*zoom-0.5f)/zoom;

	RECT rect;
	GetClientRect(hwnd, &rect);
	if (mx > byteBuff->width/(float)(rect.right-rect.left)-1.0f/zoom)
		mx = byteBuff->width/(float)(rect.right-rect.left)-1.0f/zoom;
	if (my > byteBuff->height/(float)(rect.bottom-rect.top)-1.0f/zoom)
		my = byteBuff->height/(float)(rect.bottom-rect.top)-1.0f/zoom;
	if (mx < 0.0f)
		mx = 0.0f;
	if (my < 0.0f)
		my = 0.0f;

	InvalidateRect(hwnd, NULL, false);
}

void EMPView::zoomFit()
{
	unsigned int iwidth, iheight;
	ImageBuffer * ib;
	if (byteBuff  &&  byteBuff->width>0  &&  byteBuff->height>0)
	{
		iwidth = byteBuff->width;
		iheight = byteBuff->height;
	}
	else
	{
		if (otherSource)
			ib = otherSource;
		else
			ib = imgBuff;
		if (!ib    ||    ib->width==0    ||    ib->height==0)
			return;
		iwidth = ib->width;
		iheight = ib->height;
	}

	RECT rect;
	GetClientRect(hwnd, &rect);
	int w = (rect.right-rect.left);
	int h = (rect.bottom-rect.top);
	if (w<1 || h<1)
		return;

	float z1,z2;
	z1 = (float) w /  iwidth;
	z2 = (float) h /  iheight;
	z1 = min(z1,z2);
	zoom = z1;

	// correct shift to be in bounds
	if (mx > iwidth/(float)w-1.0f/zoom)
		mx = iwidth/(float)w-1.0f/zoom;
	if (my > iheight/(float)h-1.0f/zoom)
		my = iheight/(float)h-1.0f/zoom;
	if (mx < 0.0f)
		mx = 0.0f;
	if (my < 0.0f)
		my = 0.0f;

	InvalidateRect(hwnd, NULL, false);
}

void EMPView::zoomByteFit(bool repaint)
{
	if (!byteBuff    ||    !byteBuff->imgBuf   ||   byteBuff->width < 1    ||    byteBuff->height < 1)
		return;

	RECT rect;
	GetClientRect(hwnd, &rect);
	int w = (rect.right-rect.left);
	int h = (rect.bottom-rect.top);
	if (w<1 || h<1)
		return;

	float z1,z2;
	z1 = (float) w /  byteBuff->width;
	z2 = (float) h /  byteBuff->height;
	z1 = min(z1,z2);
	zoom = z1;

	// correct shift to be in bounds
	if (mx > byteBuff->width/(float)w-1.0f/zoom)
		mx = byteBuff->width/(float)w-1.0f/zoom;
	if (my > byteBuff->height/(float)h-1.0f/zoom)
		my = byteBuff->height/(float)h-1.0f/zoom;
	if (mx < 0.0f)
		mx = 0.0f;
	if (my < 0.0f)
		my = 0.0f;

	if (repaint)
		InvalidateRect(hwnd, NULL, false);
}

void EMPView::zoomResFit(int zwidth, int zheight, bool repaint)
{
	if (zwidth < 1    ||    zheight < 1)
		return;

	RECT rect;
	GetClientRect(hwnd, &rect);
	int w = (rect.right-rect.left);
	int h = (rect.bottom-rect.top);
	if (w<1 || h<1)
		return;

	float z1,z2;
	z1 = (float) w / zwidth;
	z2 = (float) h / zheight;
	z1 = min(z1,z2);
	zoom = z1;

	// correct shift to be in bounds
	if (mx > zwidth/(float)w-1.0f/zoom)
		mx = zwidth/(float)w-1.0f/zoom;
	if (my > zheight/(float)h-1.0f/zoom)
		my = zheight/(float)h-1.0f/zoom;
	if (mx < 0.0f)
		mx = 0.0f;
	if (my < 0.0f)
		my = 0.0f;

	if (repaint)
		InvalidateRect(hwnd, NULL, false);
}

bool EMPView::resize(int newWidth, int newHeight, bool increaseForBorder)
{
	if (increaseForBorder)
		SetWindowPos(hwnd, HWND_TOP, 0,0, newWidth+2, newHeight+2, SWP_NOMOVE);
	else
		SetWindowPos(hwnd, HWND_TOP, 0,0, newWidth, newHeight, SWP_NOMOVE);

	imgBuff->freeBuffer();
	imgBuff->allocBuffer(newWidth, newHeight);
	imgBuff->clearBuffer();

	byteBuff->freeBuffer();
	byteBuff->allocBuffer(newWidth, newHeight);
	byteBuff->clearBuffer();

	return true;
}

HBITMAP EMPView::getRenderHBitmap()
{
	HBITMAP result;
	if (!byteBuff   ||   !byteBuff->imgBuf)
		return 0;
	int w = byteBuff->width;
	int h = byteBuff->height;
	unsigned char * buf = (unsigned char *)malloc(w*h*4);
	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			buf[4*(y*w+x)+0] = GetBValue(byteBuff->imgBuf[y][x]);
			buf[4*(y*w+x)+1] = GetGValue(byteBuff->imgBuf[y][x]);
			buf[4*(y*w+x)+2] = GetRValue(byteBuff->imgBuf[y][x]);
			buf[4*(y*w+x)+3] = 255;
		}
	result = CreateBitmap(w,h, 1, 32, buf);
	free(buf);
	return result;
}

void EMPView::delToolTip()
{
	if (toolTxt)
		free(toolTxt);
	toolTxt = NULL;

	if (hToolTip)
	{
		TOOLINFO ti;
		bool OK = (0!=SendMessage(hwnd, TTM_GETTOOLINFO, 0, (LPARAM)&ti));
		if (OK)
			SendMessage(hToolTip, TTM_DELTOOL, 0, (LPARAM)&ti);
		DestroyWindow(hToolTip);
	}
	hToolTip = 0;
}

void EMPView::setToolTip(char * text)
{
	if (!hToolTip)
	{
		hToolTip = CreateWindowEx (WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,//  | TTS_BALLOON,     
				    CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwnd, NULL, hDllModule, NULL);
	}

	SetWindowPos (hToolTip, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	TOOLINFO ti;
	ti.cbSize = sizeof (TOOLINFO);
	ti.uFlags = TTF_SUBCLASS | TTF_IDISHWND;
	ti.hwnd = hwnd;
	ti.hinst = hDllModule;
	ti.uId = (UINT_PTR) hwnd;
	ti.lpszText = LPSTR_TEXTCALLBACK;

	char * oldtxt = toolTxt;
	toolTxt = copyString(text);
	if (oldtxt)
		free(oldtxt);


	RECT rect;
	GetClientRect (hwnd, &rect);

	ti.rect.left = rect.left;    
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
	ti.rect.bottom = rect.bottom;

	SendMessage (hToolTip, TTM_ADDTOOL, 0, (LPARAM)&ti);
}

void EMPView::allowDragFiles()	
{ 
	DragAcceptFiles(hwnd, true); 
}

void EMPView::denyDragFiles()	
{ 
	DragAcceptFiles(hwnd, false); 
}


