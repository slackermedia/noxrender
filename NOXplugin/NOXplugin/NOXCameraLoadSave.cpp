#include "max2013predef.h"
#include "NOXCamera.h"
#include <macrorec.h>
#include <decomp.h>
#include "noxdebug.h"

#define SCHECK(r) if (r!=IO_OK) return r;

#define NOX_CAM_CHUNK_CAMERA 0x4100
#define NOX_CAM_CHUNK_ISO 0x4101
#define NOX_CAM_CHUNK_APERTURE 0x4102
#define NOX_CAM_CHUNK_SHUTTER 0x4103
#define NOX_CAM_CHUNK_AUTOEXP_ON 0x4104
#define NOX_CAM_CHUNK_AUTOEXP_TYPE 0x4105
#define NOX_CAM_CHUNK_AUTOFOCUS_ON 0x4106
#define NOX_CAM_CHUNK_FOCUS_DISTANCE 0x4107
#define NOX_CAM_CHUNK_FOCUS_TO_TARGET 0x4108
#define NOX_CAM_CHUNK_IS_ACTIVE 0x4109
#define NOX_CAM_CHUNK_IS_TARGETED 0x410A
#define NOX_CAM_CHUNK_SHOW_CONE 0x410B
#define NOX_CAM_CHUNK_BLADES_ROUND  0x410C
#define NOX_CAM_CHUNK_BLADES_NUMBER 0x410D
#define NOX_CAM_CHUNK_BLADES_ANGLE  0x410E
#define NOX_CAM_CHUNK_BLADES_RADIUS 0x410F
#define NOX_CAM_CHUNK_FOCAL 0x4110
#define NOX_CAM_CHUNK_DOF_ON 0x4111
#define NOX_CAM_CHUNK_BOKEH_RING_BALANCE 0x4112
#define NOX_CAM_CHUNK_BOKEH_RING_SIZE 0x4113
#define NOX_CAM_CHUNK_BOKEH_FLATTEN 0x4114
#define NOX_CAM_CHUNK_BOKEH_VIGNETTE 0x4115


//------------------------------------------------------------------------------------

IOResult NOXCamera::Load(ILoad *iload) 
{
	IOResult res;
	int id;
	unsigned long rd;
	float tfloat;
	int tint;
	//float t4float[4];


	ADD_LOG_PARTS_MAIN("NOXCamera::Load");


	while (IO_OK == (res=iload->OpenChunk())) 
	{
		switch(id = iload->CurChunkID())  
		{
			case NOX_CAM_CHUNK_CAMERA:
				while (IO_OK == (res=iload->OpenChunk()))
				{
					switch(id = iload->CurChunkID())  
					{
						// FOCAL LENGTH
						case NOX_CAM_CHUNK_FOCAL:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setFOV(tfloat);
						}
						break;

						// ISO
						case NOX_CAM_CHUNK_ISO:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setISO(tfloat);
						}
						break;

						// APERTURE
						case NOX_CAM_CHUNK_APERTURE:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setAperture(tfloat);
						}
						break;

						// SHUTTER
						case NOX_CAM_CHUNK_SHUTTER:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setShutter(tfloat);
						}
						break;

						// AUTO EXPOSURE ON
						case NOX_CAM_CHUNK_AUTOEXP_ON:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setAutoExpOn(0 != tint);
						}
						break;

						// AUTO EXPOSURE TYPE
						case NOX_CAM_CHUNK_AUTOEXP_TYPE:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setAutoExpType(tint);
						}
						break;

						// AUTO FOCUS ON
						case NOX_CAM_CHUNK_AUTOFOCUS_ON:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setAutoFocusOn(0 != tint);
						}
						break;

						// FOCUS DISTANCE
						case NOX_CAM_CHUNK_FOCUS_DISTANCE:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setFocusDistance(tfloat);
						}
						break;

						// FOCUS CONNECTED TO TARGET
						case NOX_CAM_CHUNK_FOCUS_TO_TARGET:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							focusConnectedToTarget = (0 != tint);
						}
						break;

						// IS ACTIVE
						case NOX_CAM_CHUNK_IS_ACTIVE:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setActive(0 != tint);
						}
						break;

						// IS TARGETED
						case NOX_CAM_CHUNK_IS_TARGETED:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							SetType(tint);
						}
						break;

						// SHOW CONE
						case NOX_CAM_CHUNK_SHOW_CONE:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							SetConeState(tint);
						}
						break;

						// BLADES NUMBER
						case NOX_CAM_CHUNK_BLADES_NUMBER:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setBladesNumber(tint);
						}
						break;

						// BLADES ANGLE
						case NOX_CAM_CHUNK_BLADES_ANGLE:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setBladesAngle(tfloat);
						}
						break;

						// BLADES ROUND
						case NOX_CAM_CHUNK_BLADES_ROUND:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setBladesRound(0 != tint);
						}
						break;

						// BLADES RADIUS
						case NOX_CAM_CHUNK_BLADES_RADIUS:
						{
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setBladesRadius(tfloat);
						}
						break;

						// BOKEH RING BALANCE
						case NOX_CAM_CHUNK_BOKEH_RING_BALANCE:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setBokehRingBalance(tint);
						}
						break;

						// BOKEH RING SIZE
						case NOX_CAM_CHUNK_BOKEH_RING_SIZE:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setBokehRingSize(tint);
						}
						break;

						// BOKEH FLATTEN
						case NOX_CAM_CHUNK_BOKEH_FLATTEN:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setBokehFlatten(tint);
						}
						break;

						// BOKEH VIGNETTE
						case NOX_CAM_CHUNK_BOKEH_VIGNETTE:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setVignette(tint);
						}
						break;

						// DOF ON
						case NOX_CAM_CHUNK_DOF_ON:
						{
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK)	
								return res;	
							trCam.setDofON(0 != tint);
						}
						break;
					}
					iload->CloseChunk();
				}	// end while in chunk camera
			break;
		}
		iload->CloseChunk();
	}

	ADD_LOG_PARTS_MAIN("NOXCamera::Load done");

	return IO_OK;
}

//------------------------------------------------------------------------------------

IOResult NOXCamera::Save(ISave *isave) 
{
	IOResult res;
	unsigned long wr;
	float tfloat;
	int tint;

	ADD_LOG_PARTS_MAIN("NOXCamera::Save");

	// CAMERA
	isave->BeginChunk(NOX_CAM_CHUNK_CAMERA);

		// ISO
		isave->BeginChunk(NOX_CAM_CHUNK_FOCAL);
			tfloat = trCam.getFOV();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// ISO
		isave->BeginChunk(NOX_CAM_CHUNK_ISO);
			tfloat = trCam.getISO();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// APERTURE
		isave->BeginChunk(NOX_CAM_CHUNK_APERTURE);
			tfloat = trCam.getAperture();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// SHUTTER
		isave->BeginChunk(NOX_CAM_CHUNK_SHUTTER);
			tfloat = trCam.getShutter();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// AUTO EXPOSURE ON
		isave->BeginChunk(NOX_CAM_CHUNK_AUTOEXP_ON);
			tint = trCam.getAutoExpOn() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// AUTO EXPOSURE TYPE
		isave->BeginChunk(NOX_CAM_CHUNK_AUTOEXP_TYPE);
			tint = trCam.getAutoExpType();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// AUTO FOCUS ON
		isave->BeginChunk(NOX_CAM_CHUNK_AUTOFOCUS_ON);
			tint = trCam.getAutoFocusOn() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// FOCUS DISTANCE
		isave->BeginChunk(NOX_CAM_CHUNK_FOCUS_DISTANCE);
			tfloat = trCam.getFocusDistance();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// FOCUS CONNECTED TO TARGET
		isave->BeginChunk(NOX_CAM_CHUNK_FOCUS_TO_TARGET);
			tint = focusConnectedToTarget ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// IS ACTIVE
		isave->BeginChunk(NOX_CAM_CHUNK_IS_ACTIVE);
			tint = trCam.getIsActive() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// IS TARGETED
		isave->BeginChunk(NOX_CAM_CHUNK_IS_TARGETED);
			tint = Type();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// SHOW CONE
		isave->BeginChunk(NOX_CAM_CHUNK_SHOW_CONE);
			tint = GetConeState();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BLADES NUMBER
		isave->BeginChunk(NOX_CAM_CHUNK_BLADES_NUMBER);
			tint = trCam.getBladesNumber();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BLADES ANGLE
		isave->BeginChunk(NOX_CAM_CHUNK_BLADES_ANGLE);
			tfloat = trCam.getBladesAngle();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BLADES ROUND
		isave->BeginChunk(NOX_CAM_CHUNK_BLADES_ROUND);
			tint = trCam.getBladesRound() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BLADES RADIUS
		isave->BeginChunk(NOX_CAM_CHUNK_BLADES_RADIUS);
			tfloat = trCam.getBladesRadius();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BOKEH RING BALANCE
		isave->BeginChunk(NOX_CAM_CHUNK_BOKEH_RING_BALANCE);
			tint = trCam.getBokehRingBalance();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BOKEH RING SIZE
		isave->BeginChunk(NOX_CAM_CHUNK_BOKEH_RING_SIZE);
			tint = trCam.getBokehRingSize();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BOKEH FLATTEN
		isave->BeginChunk(NOX_CAM_CHUNK_BOKEH_FLATTEN);
			tint = trCam.getBokehFlatten();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BOKEH VIGNETTE
		isave->BeginChunk(NOX_CAM_CHUNK_BOKEH_VIGNETTE);
			tint = trCam.getVignette();
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// DOF ON
		isave->BeginChunk(NOX_CAM_CHUNK_DOF_ON);
			tint = trCam.getDofON() ? 1 : 0;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

	isave->EndChunk();	// end camera

	ADD_LOG_PARTS_MAIN("NOXCamera::Save done");

	return IO_OK;
}

//------------------------------------------------------------------------------------
