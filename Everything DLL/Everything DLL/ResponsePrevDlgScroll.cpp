
#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "log.h"
#include "ColorSchemes.h"
#include "CameraResponse.h"


int currentResponseFunction = 0;

extern HMODULE hDllModule;
bool threadsFinished[32];
HWND hPrevControls[NOX_NUM_RESPONSE_FILTERS+1];
HWND hNameControls[NOX_NUM_RESPONSE_FILTERS+1];
ImageByteBuffer* oldControlBuffers[NOX_NUM_RESPONSE_FILTERS+1];
LRESULT CALLBACK ResponsePreviewSubWindowDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void InitResponsePreviewSubWindowClass();
void unlockPrevDlgControls();
bool runMainPreviewThread(int numThreads);
bool runWorkerPreviewThread(int id);
bool runUnlockerPreviewThread();
void setResponseFunction(int num);
DWORD WINAPI MainPreviewThreadProc(LPVOID lpParameter);
DWORD WINAPI WorkerPreviewThreadProc(LPVOID lpParameter);
DWORD WINAPI UnlockerPreviewThreadProc(LPVOID lpParameter);

ImageBuffer * prePostImage = NULL;
int num_threads = 4;
HWND hMainPrevWindow = 0;

//---------------------------------------------------------------------------------------------------------------------------

void InitResponsePreviewSubWindowClass()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "respprevSubWindow";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = ResponsePreviewSubWindowDlgProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = CS_HREDRAW | CS_VREDRAW;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = 0;
    wc.hIconSm        = 0;
	
	static bool initiatedResponsePreviewSubWindowClass = false;
	if (!initiatedResponsePreviewSubWindowClass)
	{
		ATOM a = RegisterClassEx(&wc);
		initiatedResponsePreviewSubWindowClass = true;
	}
}

//---------------------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK ResponsePreviewSubWindowDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH hBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
			}
			break;
		case WM_DESTROY:
			{
				DeleteObject(hBrush);
				hBrush = 0;
				for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
				{
					EMPView * empv = GetEMPViewInstance(hPrevControls[i]);
					if (empv)
					{
						if (empv->byteBuff)
						{
							empv->byteBuff->freeBuffer();
							delete empv->byteBuff;
						}
						empv->byteBuff = NULL;
						if (empv->imgBuff)
						{
							empv->imgBuff->freeBuffer();
							delete empv->imgBuff;
						}
						empv->imgBuff = NULL;
					}
					DestroyWindow(hPrevControls[i]);
					hPrevControls[i] = 0;

					DestroyWindow(hNameControls[i]);
					hNameControls[i] = 0;

				}
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);

				if (wmEvent == PV_CLICKED)
				{
					if (wmId >= 100    &&    wmId <= 100+NOX_NUM_RESPONSE_FILTERS)
					{
						setResponseFunction(wmId-100);
					}
				}
				if (wmEvent == PV_DOUBLECLICKED)
				{
					if (wmId >= 100    &&    wmId <= 100+NOX_NUM_RESPONSE_FILTERS)
					{
						setResponseFunction(wmId-100);
						EndDialog(hMainPrevWindow, (INT_PTR)(currentResponseFunction));
					}
				}
			}
			break;
		case WM_MOUSEMOVE:
			{
				SetFocus(hWnd);
			}
			break;
		case WM_MOUSEWHEEL:
			{
				EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hMainPrevWindow, IDC_RESP2_PREV_SCROLLBAR));
				if (!emsb)
					break;

				int zDelta = (int)wParam;
				if (zDelta>0)
				{
					int p = max(0, emsb->workarea_pos - 210);
					emsb->workarea_pos = p;
					emsb->updateSize();
					emsb->updateRegions();
					InvalidateRect(emsb->hwnd, NULL, false);

					for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
					{
						int col = i%4;
						int row = i/4;
						SetWindowPos(hPrevControls[i], HWND_TOP, col*250+10, row*210+10-p,   240, 180,  SWP_NOOWNERZORDER | SWP_NOSIZE);
						SetWindowPos(hNameControls[i], HWND_TOP, col*250+10, row*210+194-p,  240, 18,  SWP_NOOWNERZORDER | SWP_NOSIZE);
					}
				}
				else
				{
					int p = min(emsb->size_work-emsb->size_screen, emsb->workarea_pos + 210);
					emsb->workarea_pos = p;
					emsb->updateSize();
					emsb->updateRegions();
					InvalidateRect(emsb->hwnd, NULL, false);

					for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
					{
						int col = i%4;
						int row = i/4;
						SetWindowPos(hPrevControls[i], HWND_TOP, col*250+10, row*210+10-p,   240, 180,  SWP_NOOWNERZORDER | SWP_NOSIZE);
						SetWindowPos(hNameControls[i], HWND_TOP, col*250+10, row*210+194-p,  240, 18,  SWP_NOOWNERZORDER | SWP_NOSIZE);
					}
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)hBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)hBrush;
			}
			break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

//---------------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK ResponsePreviewDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH hBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				hMainPrevWindow = hWnd;
				int curFunc = (int)lParam;
				InitResponsePreviewSubWindowClass();

				for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
					oldControlBuffers[i] = NULL;

				hBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

				EMButton    * emOK		= GetEMButtonInstance(GetDlgItem(hWnd, IDC_RESP2_PREV_OK));
				EMButton    * emCancel	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_RESP2_PREV_CANCEL));
				EMScrollBar * emsb		= GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_RESP2_PREV_SCROLLBAR));
				
				emsb->grid = 210;
				emsb->size_work = ((NOX_NUM_RESPONSE_FILTERS+1+3)/4)*210+10;
				emsb->size_screen = 640;
				int start = max(0, min(emsb->size_work-emsb->size_screen, (curFunc/4)*210));
				emsb->workarea_pos = start;
				emsb->updateSize();
				emsb->updateRegions();

				GlobalWindowSettings::colorSchemes.apply(emOK);
				GlobalWindowSettings::colorSchemes.apply(emCancel);
				GlobalWindowSettings::colorSchemes.apply(emsb);

				RECT crect, wrect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				int bx = (wrect.right-wrect.left) - (crect.right-crect.left);
				int by = (wrect.bottom-wrect.top) - (crect.bottom-crect.top);

				SetWindowPos(hWnd, HWND_TOP,  0, 0,  1030+bx, 690+by,   SWP_NOOWNERZORDER | SWP_NOMOVE);

				SetWindowPos(emsb->hwnd,		HWND_TOP,  1010, 0,   20, 690,   SWP_NOOWNERZORDER);
				SetWindowPos(emOK->hwnd,		HWND_TOP,  400, 650,  100, 26,   SWP_NOOWNERZORDER);
				SetWindowPos(emCancel->hwnd,	HWND_TOP,  510, 650,  100, 26,   SWP_NOOWNERZORDER);

				HWND hSubWindow = CreateWindow("respprevSubWindow", "subwindow", WS_CHILD | WS_VISIBLE ,
						0, 0, 1010, 640, hWnd, (HMENU)(60), hDllModule, 0);


				ShowWindow(hSubWindow, SW_SHOW);
				UpdateWindow(hSubWindow); 


				for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
				{
					int col = i%4;
					int row = i/4;
					hPrevControls[i] = CreateWindow("EMPView", "", WS_CHILD | WS_VISIBLE ,
								col*250+10, row*210+10-start, 240, 180, hSubWindow, (HMENU)(short)(100+i), hDllModule, 0);
					
					EMPView * emFPrev = GetEMPViewInstance(hPrevControls[i]);
					emFPrev->dontLetUserChangeZoom = true;
					GlobalWindowSettings::colorSchemes.apply(emFPrev);

					char * fname = getResponseFunctionName(i);
					hNameControls[i] = CreateWindow("EMText", fname, WS_CHILD | WS_VISIBLE ,
								col*250+10,  row*210+194-start,  240, 18, hSubWindow, (HMENU)(short)(300+i), hDllModule, 0);

					EMText * emt = GetEMTextInstance(hNameControls[i]);
					emt->align = EMText::ALIGN_CENTER;
					GlobalWindowSettings::colorSchemes.apply(emt, true);
				}

				setResponseFunction(curFunc);

				#define ONE_THREAD

				#ifdef ONE_THREAD
					MainPreviewThreadProc(0);
					num_threads = 1;
					WorkerPreviewThreadProc(0);
				#else
					int numProc = ThreadManager::getProcessorsNumber();
					runMainPreviewThread(numProc);
				#endif
			}
			break;
		case WM_DESTROY:
			{
				DeleteObject(hBrush);
				hBrush = 0;
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)(-1));
			}
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_RESP2_PREV_OK:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)(currentResponseFunction));
							}
						}
						break;
					case IDC_RESP2_PREV_CANCEL:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)(-1));
							}
						}
						break;
					case IDC_RESP2_PREV_SCROLLBAR:
						{
							if (wmEvent != SB_POS_CHANGED)
								break;

							EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_RESP2_PREV_SCROLLBAR));
							int p = emsb->workarea_pos;

							for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
							{
								int col = i%4;
								int row = i/4;
								SetWindowPos(hPrevControls[i], HWND_TOP, col*250+10, row*210+10-p,   240, 180,  SWP_NOOWNERZORDER | SWP_NOSIZE);
								SetWindowPos(hNameControls[i], HWND_TOP, col*250+10, row*210+194-p,  240, 18,  SWP_NOOWNERZORDER | SWP_NOSIZE);
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)hBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)hBrush;
			}
			break;
	}
	return false;
}

void unlockPrevDlgControls()
{
	enableControl(hMainPrevWindow, IDC_RESP2_PREV_OK);
	enableControl(hMainPrevWindow, IDC_RESP2_PREV_CANCEL);
	for (int i=0; i<32; i++)
		threadsFinished[i] = true;

	Sleep(2000);

	for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
	{
		ImageByteBuffer * ibuf = oldControlBuffers[i];
		if (ibuf)
		{
			ibuf->freeBuffer();
			delete ibuf;
		}
		oldControlBuffers[i] = NULL;
	}
}

bool runMainPreviewThread(int numThreads)
{
	DWORD threadId;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(MainPreviewThreadProc),
            (LPVOID)(INT_PTR)numThreads,
            0,
            &threadId);

	return true;
}

bool runWorkerPreviewThread(int id)
{
	DWORD threadId;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(WorkerPreviewThreadProc),
            (LPVOID)(INT_PTR)id,
            0,
            &threadId);

	return true;
}

bool runUnlockerPreviewThread()
{
	DWORD threadId;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(UnlockerPreviewThreadProc),
            (LPVOID)(INT_PTR)0,
            0,
            &threadId);

	return true;
}

void setResponseFunction(int num)
{
	EMPView * empvOld = GetEMPViewInstance(hPrevControls[currentResponseFunction]);
	currentResponseFunction = min(NOX_NUM_RESPONSE_FILTERS , max(0, num));
	EMPView * empvNew = GetEMPViewInstance(hPrevControls[currentResponseFunction]);
	if (empvOld)
	{
		empvOld->colBorder = GlobalWindowSettings::colorSchemes.EMPViewBorder;
		InvalidateRect(empvOld->hwnd, NULL, false);
	}
	if (empvNew)
	{
		empvNew->colBorder = RGB(255,0,0);
		InvalidateRect(empvNew->hwnd, NULL, false);
	}
}


DWORD WINAPI UnlockerPreviewThreadProc(LPVOID lpParameter)
{
	bool stillCreating = true;
	do
	{
		stillCreating = false;
		for (int i=0; i<32; i++)
		{
			if (!threadsFinished[i])
				stillCreating = true;
		}
		Sleep(100);
	}
	while (stillCreating);

	unlockPrevDlgControls();
	return 0;
}


DWORD WINAPI MainPreviewThreadProc(LPVOID lpParameter)
{
	disableControl(hMainPrevWindow, IDC_RESP2_PREV_OK);
	disableControl(hMainPrevWindow, IDC_RESP2_PREV_CANCEL);

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (!sc)
	{
		unlockPrevDlgControls();
		return 0;
	}
	
	Camera * cam = sc->getActiveCamera();
	if (!cam)
	{
		unlockPrevDlgControls();
		return 0;
	}
	bool giperpixel = (cam->gi_type == Camera::GI_TYPE_PT);
	ImageBuffer * srcBigImage = cam->blendBuffers(true, true, giperpixel);
	if (!srcBigImage)
	{
		unlockPrevDlgControls();
		return 0;
	}

	float rtX = 240.0f / srcBigImage->width;
	float rtY = 180.0f / srcBigImage->height;
	int w = 240;
	int h = 180;
	if (rtX > rtY)
		w = (int)(180.0f * srcBigImage->width/srcBigImage->height);
	else
		h = (int)(240.0f * srcBigImage->height/srcBigImage->width);

	if (prePostImage)
	{
		prePostImage->freeBuffer();
		delete prePostImage;
	}

	prePostImage = srcBigImage->copyResize(w, h);

	srcBigImage->freeBuffer();
	delete srcBigImage;
	srcBigImage = NULL;

	if (!prePostImage)
	{
		unlockPrevDlgControls();
		return 0;
	}


	for (int i=0; i<32; i++)
		threadsFinished[i] = true;

	int numThreads = min(32, max(1, (int)(INT_PTR)lpParameter));
	num_threads = numThreads;
	for (int i=0; i<numThreads; i++)
	{
		threadsFinished[i] = false;
		runWorkerPreviewThread(i);
	}
	runUnlockerPreviewThread();

	return 0;
}

DWORD WINAPI WorkerPreviewThreadProc(LPVOID lpParameter)
{
	int thrID = (int)(INT_PTR)lpParameter;
	if (!prePostImage  ||  !prePostImage->imgBuf)
	{
		threadsFinished[thrID] = true;
		return 0;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (!sc)
	{
		threadsFinished[thrID] = true;
		return 0;
	}
	
	Camera * cam = sc->getActiveCamera();
	if (!cam)
	{
		threadsFinished[thrID] = true;
		return 0;
	}

	ImageModifier iMod;
	iMod.copyDataFromAnother(&cam->iMod);

	int w = prePostImage->width;
	int h = prePostImage->height;
	for (int i=thrID; i<=NOX_NUM_RESPONSE_FILTERS; i+=num_threads)
	{
		iMod.setResponseFunctionNumber(i);
		ImageBuffer * pp = prePostImage->doPostProcess(1, &iMod, NULL, cam->depthBuf, "Creating previews...");
		if (!pp)
		{
			threadsFinished[thrID] = true;
			return 0;
		}

		EMPView * empv = GetEMPViewInstance(hPrevControls[i]);
		if (!empv)
		{
			threadsFinished[thrID] = true;
			return 0;
		}

		ImageByteBuffer * newBuffer = new ImageByteBuffer();
		newBuffer->allocBuffer(w,h);
		pp->copyToImageByteBuffer(newBuffer, RGB(0,0,0));
		oldControlBuffers[i] = empv->byteBuff;
		empv->byteBuff = newBuffer;

		pp->freeBuffer();
		delete pp;
		pp = NULL;
		InvalidateRect(empv->hwnd, NULL, false);
	}

	threadsFinished[thrID] = true;

	return 0;
}


