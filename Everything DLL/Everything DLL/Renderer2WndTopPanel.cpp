#include "RendererMain2.h"
#include "EM2Controls.h"
#include "resource.h"

void setPositionsOnTopPanel(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcTopPanel(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnTopPanel(hWnd);
				rMain->updateBgShiftsPanelTop();

				EM2Busy * emBusy = GetEM2BusyInstance(GetDlgItem(hWnd, IDC_REND2_TOP_ANIM));
				emBusy->bgImage = rMain->hBmpBg;
				emBusy->numBoxes = 5;
				emBusy->boxSize = 6;
				emBusy->boxSizeActive= 8;
				emBusy->boxTopMargin = 5;
				emBusy->boxLeftMargin = 5;

				EM2Button * embRender = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_TOP_RENDER));
				embRender->bgImage = rMain->hBmpBg;
				embRender->alphaBg = 0;
				embRender->alphaBgMouseOver = 0;
				embRender->alphaBgSelected = 0;
				embRender->drawBorder = false;
				embRender->setFont(rMain->fonts->em2topbuttons, false);
				EnableWindow(embRender->hwnd, FALSE);

				EM2Button * embResume = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_TOP_RESUME));
				embResume->bgImage = rMain->hBmpBg;
				embResume->alphaBg = 0;
				embResume->alphaBgMouseOver = 0;
				embResume->alphaBgSelected = 0;
				embResume->drawBorder = false;
				embResume->setFont(rMain->fonts->em2topbuttons, false);
				EnableWindow(embResume->hwnd, FALSE);

				EM2Button * embStop = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_TOP_STOP));
				embStop->bgImage = rMain->hBmpBg;
				embStop->alphaBg = 0;
				embStop->alphaBgMouseOver = 0;
				embStop->alphaBgSelected = 0;
				embStop->drawBorder = false;
				embStop->setFont(rMain->fonts->em2topbuttons, false);

				EM2Line * eml1 = GetEM2LineInstance(GetDlgItem(hWnd, 5348));
				eml1->bgImage = rMain->hBmpBg;

				EM2Line * eml2 = GetEM2LineInstance(GetDlgItem(hWnd, 5349));
				eml2->bgImage = rMain->hBmpBg;

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		//case WM_ERASEBKGND:
		//	{
		//		return 1;
		//	}
		//	break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				//if (ur)
				//	rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXTopPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_TOP_RENDER:
						{
							rMain->startRender(false);
						}
						break;
					case IDC_REND2_TOP_RESUME:
						{
							rMain->startRender(true);
						}
						break;
					case IDC_REND2_TOP_STOP:
						{
							rMain->stopRender();
							rMain->refreshRenderOnThread();
							if (rMain->hStopThreadId)
							{
								DWORD exitCode = 0;
								TerminateThread(rMain->hStopThreadId, exitCode);
								rMain->hStopThreadId = 0;
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelTop()
{
	EM2Busy * emBusy = GetEM2BusyInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_ANIM));
	updateControlBgShift(GetDlgItem(hTopPanel, IDC_REND2_TOP_ANIM), bgShiftXTopPanel, 0, emBusy->bgShiftX, emBusy->bgShiftY);

	EM2Button * embRender = GetEM2ButtonInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_RENDER));
	updateControlBgShift(GetDlgItem(hTopPanel, IDC_REND2_TOP_RENDER), bgShiftXTopPanel, 0, embRender->bgShiftX, embRender->bgShiftY);
	EM2Button * embResume = GetEM2ButtonInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_RESUME));
	updateControlBgShift(GetDlgItem(hTopPanel, IDC_REND2_TOP_RESUME), bgShiftXTopPanel, 0, embResume->bgShiftX, embResume->bgShiftY);
	EM2Button * embStop = GetEM2ButtonInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_STOP));
	updateControlBgShift(GetDlgItem(hTopPanel, IDC_REND2_TOP_STOP), bgShiftXTopPanel, 0, embStop->bgShiftX, embStop->bgShiftY);

	EM2Line * emL1 = GetEM2LineInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_LINE1));
	updateControlBgShift(GetDlgItem(hTopPanel, IDC_REND2_TOP_LINE1), bgShiftXTopPanel, 0, emL1->bgShiftX, emL1->bgShiftY);
	EM2Line * emL2 = GetEM2LineInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_LINE2));
	updateControlBgShift(GetDlgItem(hTopPanel, IDC_REND2_TOP_LINE2), bgShiftXTopPanel, 0, emL2->bgShiftX, emL2->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnTopPanel(HWND hWnd)
{
	int x = 0, y = 0;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_TOP_ANIM),		HWND_TOP,	x+0,	y+7,			57,		16,			SWP_FRAMECHANGED);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_TOP_RENDER),	HWND_TOP,	x+0,	y+7,			57,		16,			SWP_FRAMECHANGED);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_TOP_RESUME),	HWND_TOP,	x+62,	y+7,			57,		16,			SWP_FRAMECHANGED);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_TOP_STOP),		HWND_TOP,	x+124,	y+7,			41,		16,			SWP_FRAMECHANGED);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_TOP_LINE1),		HWND_TOP,	x+57,	y+3,			5,		23,			SWP_FRAMECHANGED);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_TOP_LINE2),		HWND_TOP,	x+119,	y+3,			5,		23,			SWP_FRAMECHANGED);
}

//------------------------------------------------------------------------------------------------
