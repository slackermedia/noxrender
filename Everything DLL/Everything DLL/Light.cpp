#include "raytracer.h"
#include <stdio.h>
#include "log.h"

//--------------------------------------------------------------------------------------------------

LightMesh::LightMesh() :
	tris(0)
{
	triAreas = NULL;
	triAreasSum = NULL;
	tris.createArray();
}

//--------------------------------------------------------------------------------------------------

LightMesh::~LightMesh()
{
	if (triAreas)
		free(triAreas);
	if (triAreasSum)
		free(triAreasSum);
	triAreas = NULL;
	triAreasSum = NULL;
	tris.freeList();
	tris.destroyArray();
}

//--------------------------------------------------------------------------------------------------

void LightMesh::freeArrays()
{
	if (triAreas)
		free(triAreas);
	triAreas = NULL;
	if (triAreasSum)
		free(triAreasSum);
	triAreasSum = NULL;

	tris.destroyArray();
	tris.freeList();
}

//--------------------------------------------------------------------------------------------------

int LightMesh::randomTriangle(float &prob)
{
	prob = 1;
	if (numTris <= 0)
		return -1;

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = getMarsaglia()/(float)(unsigned int)(MAXMARS);
	#endif
	a *= totalArea;
	int chosen = 0;
	float area = 1;

	if (numTris > 16)
	{
		int tmin = 0;
		int tmax = numTris-1;
		int tavg = (tmin + tmax) / 2;
		if (a <= triAreasSum[0])
		{
			prob = triAreas[0]/totalArea;
			return tris[0];
		}
		if (triAreasSum[numTris-1]<=a)
		{
			prob = triAreas[numTris-1]/totalArea;
			return tris[numTris-1];
		}

		for (int i=0; i<40; i++)
		{
			if (a <= triAreasSum[tavg])
			{
				if (a>triAreasSum[tavg-1])
				{
					chosen = tavg;
					prob = triAreas[chosen]/totalArea;
					return tris[chosen];
				}
				tmax = tavg-1;
			}
			else
			{
				if (a <= triAreasSum[tavg+1])
				{
					chosen = tavg+1;
					prob = triAreas[chosen]/totalArea;
					return tris[chosen];
				}
				tmin = tavg+1;
			}
			tavg = (tmin + tmax) / 2;
		}

		prob = triAreas[chosen]/totalArea;
		return tris[chosen];
	}
	else
	{
		float soFar = 0;
		for (int i=0; i<numTris; i++)
		{
			soFar += triAreas[i];
			chosen = i;
			if (a < soFar)
				break;
		}
		
		prob = triAreas[chosen]/totalArea;
		return tris[chosen];
	}
	prob = triAreas[chosen]/totalArea;
	return tris[chosen];
}

//--------------------------------------------------------------------------------------------------

void Scene::createLightMeshes()
{
	// destroy old ones
	for (int i=0; i<lightMeshes.objCount; i++)
		delete lightMeshes[i];
	lightMeshes.freeList();

	// update materials emitting bit
	for (int i=0; i<mats.objCount; i++)
	{
		MaterialNox * mat = mats[i];
		if (!mat)
			continue;
		for (int j=0; j<mat->layers.objCount; j++)
		{
			MatLayer * mlay = &(mat->layers[j]);
			if (mlay->type != MatLayer::TYPE_EMITTER)
				continue;
			mat->hasEmitters = true;
		}
	}

	// enum each instance
	for (int i=0; i<instances.objCount; i++)
	{
		MeshInstance * mInst = &(instances[i]);
		bool gotEmitter = false;
		for (int j=0; j<mInst->materials.objCount; j++)
		{
			int mId = mInst->materials[j];
			MaterialNox * mat = mats[mId];
			if (mat->hasEmitters)
				gotEmitter = true;
		}
		if (!gotEmitter)
			continue;


		LightMesh * tlm = new LightMesh();
		tlm->instNum = i;
		lightMeshes.add(tlm);
		lightMeshes.createArray();
		LightMesh * lm = lightMeshes[lightMeshes.objCount-1];


		Mesh * mSrc = &(meshes[mInst->meshID]);
		for (int j=mSrc->firstTri; j<=mSrc->lastTri; j++)
		{
			Triangle * tri = &(triangles[j]);
			MaterialNox * mat = (mats[mInst->materials[tri->matInInst]]);
			if (mat->hasEmitters)
				lm->tris.add(j);
		}

		lm->tris.createArray();
	}

	lightMeshes.createArray();
	sscene.lightsTotalArea = 0;

	// enum each lightmesh
	for (int i=0; i<lightMeshes.objCount; i++)
	{
		LightMesh * lm = lightMeshes[i];
		MeshInstance * mInst = &(instances[lm->instNum]);
		
		// total area of mesh will be needed for pdf and redistribution of energy for triangles
		lm->numTris = lm->tris.objCount;
		lm->triAreas = (float *)malloc(sizeof(float)*lm->numTris);
		lm->triAreasSum = (float *)malloc(sizeof(float)*lm->numTris);
		lm->totalArea = 0;
		for (int j=0; j<lm->numTris; j++)
		{	// sum area of all triangles in mesh
			Triangle tri;
			tri.V1 = mInst->matrix_transform * triangles[lm->tris[j]].V1;
			tri.V2 = mInst->matrix_transform * triangles[lm->tris[j]].V2;
			tri.V3 = mInst->matrix_transform * triangles[lm->tris[j]].V3;
			float area = tri.evaluateArea();
			lm->triAreas[j] = area;
			lm->totalArea  += lm->triAreas[j];
			lm->triAreasSum[j] = lm->totalArea;
		}
		sscene.lightsTotalArea += lm->totalArea;

		lm->totalPower = 0;
		for (int j=0; j<lm->numTris; j++)
		{
			Triangle * tri = &(triangles[lm->tris[j]]);
			MaterialNox * mat = mats[mInst->materials[tri->matInInst]];
			for (int k=0; k<mat->layers.objCount; k++)
			{	// sum power in all layers
				MatLayer * mlay = &(mat->layers[k]);
				if (mlay->type == MatLayer::TYPE_EMITTER)
				{	// layer is an emitter
					float p = 0;
					switch (mlay->unit)
					{
						case MatLayer::EMISSION_LUMEN: 
							p = mlay->power * lm->triAreas[j] / lm->totalArea;
							break;
						case MatLayer::EMISSION_LUX: 
							p = mlay->power * lm->triAreas[j];
							break;
						case MatLayer::EMISSION_WATT: 
							p = mlay->power * 17.5f * lm->triAreas[j] / lm->totalArea;;
							break;
						case MatLayer::EMISSION_WATT_PER_SQR_METER: 
							p = mlay->power * 17.5f * lm->triAreas[j];
							break;
					}

					float mm = mlay->contribution * 0.01f * p;
					lm->totalPower += mm;
				}
			}
		}
	}

	// create list of each light mesh powers - for speedup purposes
	sscene.lightMeshPowers = (float *)malloc(sizeof(float)*lightMeshes.objCount);
	sscene.lightMeshSumPowers = (float *)malloc(sizeof(float)*(lightMeshes.objCount+1));
	sscene.allLightsPower = 0;
	sscene.lightMeshPowersPow = (float *)malloc(sizeof(float)*lightMeshes.objCount);
	sscene.lightMeshSumPowersPow = (float *)malloc(sizeof(float)*(lightMeshes.objCount+1));
	sscene.allLightsPowerPow = 0;
	for (int i=0; i<lightMeshes.objCount; i++)
	{
		sscene.lightMeshPowers[i] = lightMeshes[i]->totalPower;
		sscene.lightMeshSumPowers[i] = sscene.allLightsPower;
		sscene.allLightsPower += sscene.lightMeshPowers[i];
		// power version
		sscene.lightMeshPowersPow[i] = pow(lightMeshes[i]->totalPower, lmesh_pow);
		sscene.lightMeshSumPowersPow[i] = sscene.allLightsPowerPow;
		sscene.allLightsPowerPow += sscene.lightMeshPowersPow[i];
	}
	sscene.lightMeshSumPowers[lightMeshes.objCount] = sscene.allLightsPower;
	sscene.lightMeshSumPowersPow[lightMeshes.objCount] = sscene.allLightsPowerPow;
	sscene.logLightMeshStats();

}

//--------------------------------------------------------------------------------------------------

void SceneStatic::logLightMeshStats()
{
	char buf[512];
	sprintf_s(buf, 512, "Lightmeshes count: %d\nTotal power: %f\nTotal area: %f", (*lightMeshes).objCount, allLightsPower, lightsTotalArea);
	Logger::add(buf);

	for (int i=0; i<(*lightMeshes).objCount; i++)
	{
		LightMesh * lm = (*lightMeshes)[i];
		sprintf_s(buf, 512, " Mesh %d: num tris: %6d   tris area: %f    power: %f lm    power/area: %f lm/m2", i, lm->numTris, lm->totalArea, lightMeshPowers[i], (lightMeshPowers[i]/lm->totalArea) );
		Logger::add(buf);
	}
}

//--------------------------------------------------------------------------------------------------

int SceneStatic::randomLightMesh(float &prob)
{
	prob = 1;
	if (lightMeshes->objCount <= 0)
		return -1;

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = getMarsaglia()/(float)(unsigned int)(MAXMARS);
	#endif

	a *= allLightsPowerPow;
	int i;
	int chosen = -1;
	float soFar = 0;
	int numMeshes = lightMeshes->objCount;

	// tip: lightMeshSumPowersPow[0] = 0

	if (numMeshes > 128)
	{
		int tmin = 0;
		int tmax = numMeshes-1;
		float ta1 = lightMeshSumPowersPow[tmin];
		float ta2 = lightMeshSumPowersPow[tmax];
		if (a>=ta2)
		{
			chosen = numMeshes-1;
			prob = lightMeshPowersPow[chosen]/allLightsPowerPow;
			return chosen;
		}

		while (tmax-1>tmin)
		{
			int tavg = (tmin+tmax)/2;
			float ta3 = lightMeshSumPowersPow[tavg];
			if (a>ta3)
			{
				ta1 = ta3;
				tmin = tavg;
			}
			else
			{
				ta2 = ta3;
				tmax = tavg;
			}
		}
		if (a<ta2)
		{
			prob = lightMeshPowersPow[tmin]/allLightsPowerPow;
			return tmin;
		}
		else 
		{
			prob = lightMeshPowersPow[tmax]/allLightsPowerPow;
			return tmax;
		}
	}
	else
	{
		float soFar = 0;
		for (int i=0; i<numMeshes; i++)
		{
			soFar += lightMeshPowersPow[i];
			chosen = i;
			if (a <= soFar)
				break;
		}
		prob = lightMeshPowersPow[chosen]/allLightsPowerPow;
		return chosen;
	}
	prob = lightMeshPowersPow[chosen]/allLightsPowerPow;
	return chosen;

	for (i=0; i<lightMeshes->objCount; i++)
	{
		soFar = lightMeshSumPowers[i+1];
		chosen = i;
		if (a < soFar)
			break;
	}

	prob = lightMeshPowersPow[chosen]/allLightsPowerPow;
	return chosen;
}

//--------------------------------------------------------------------------------------------------

LightMesh * LightMesh::copy()
{
	LightMesh * lm = new LightMesh();
	if (!lm)
		return 0;

	lm->totalArea = totalArea;
	lm->totalPower = totalPower;
	lm->numTris = numTris;
	lm->instNum = instNum;

	lm->triAreas = (float *)malloc(sizeof(float)*numTris);
	lm->triAreasSum = (float *)malloc(sizeof(float)*numTris);
	memcpy(lm->triAreas, triAreas, sizeof(float)*numTris);
	memcpy(lm->triAreasSum, triAreasSum, sizeof(float)*numTris);

	for (int i=0; i<tris.objCount; i++)
		lm->tris.add(tris[i]);
	lm->tris.createArray();

	return lm;
}

//--------------------------------------------------------------------------------------------------

void LightMesh::deleteTris()	// deprecated
{
	if (triAreas)
		free(triAreas);
	tris.destroyArray();
}

//--------------------------------------------------------------------------------------------------

float SceneStatic::evalPDF(int instID, int tri)
{
	int ilm = -1;
	for (int i=0; i<lightMeshes->objCount; i++)
	{
		LightMesh * lm = (*lightMeshes)[i];
		if (lm->instNum!=instID)
			continue;
		ilm = i;
	}

	if (ilm < 0)
	{
		Logger::add("can't find emitting mesh with given triangle -> can't eval probability");
		return 0;
	}

	float prob = lightMeshPowersPow[ilm]/allLightsPowerPow;
	LightMesh * lm = (*lightMeshes)[ilm];
	return prob/lm->totalArea;
}

//--------------------------------------------------------------------------------------------------

float SceneStatic::evalPDF(int tri)	// deprecated, no instancing
{
	int ilm = -1;
	for (int i=0; i<lightMeshes->objCount; i++)
	{
		LightMesh * lm = (*lightMeshes)[i];
		for (int j=0; j<lm->tris.objCount; j++)
			if (lm->tris[j] == tri)
				ilm = i;
		if (ilm >= 0)
			break;
	}

	if (ilm < 0)
	{
		Logger::add("can't find lightmesh with given instance id -> can't eval probability");
		return 0;
	}

	float prob = lightMeshPowersPow[ilm]/allLightsPowerPow;
	LightMesh * lm = (*lightMeshes)[ilm];
	return prob/lm->totalArea;
}

//--------------------------------------------------------------------------------------------------

int SceneStatic::findLightMesh(int nTri)	// deprecated, no instancing
{
	for (int i=0; i<lightMeshes->objCount; i++)
	{
		LightMesh * lm = (*lightMeshes)[i];
		for (int j=0; j<lm->tris.objCount; j++)
			if (nTri == lm->tris[j])
				return i;
	}
	return -1;
}

//--------------------------------------------------------------------------------------------------

int SceneStatic::findLightMeshByInstanceAndTri(int instanceID, int nTri)
{
	for (int i=0; i<lightMeshes->objCount; i++)
	{
		LightMesh * lm = (*lightMeshes)[i];
		if (lm->instNum==instanceID)
			return i;
	}
	return -1;
}

//--------------------------------------------------------------------------------------------------

