#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>

#define clamp(x,a,b)   max((a),min((x),(b)))

float getFresnelTemp(float IOR, float kos, bool goingIn);
float getFresnelTemp(float IOR, float kos);

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionMicrofacetAniso(HitData & hd, float &probability)
{
	float alphag = getRough(hd.tU, hd.tV);
	float aniso = getAnisotropy(hd.tU, hd.tV);
	float anisoangle = getAnisoAngle(hd.tU, hd.tV) * PI/180.0f;
	float alphag_x=alphag, alphag_y=alphag;
	if (aniso>0.0f)
		alphag_y = max(0.01f, alphag*(1-aniso));
	else
		alphag_x = max(0.01f, alphag*(1+aniso));

	Vector3d uvdperp = hd.normal_shade^ hd.dir_U;
	uvdperp.normalize();
	Vector3d uvdir2a = uvdperp ^ hd.normal_shade;
	uvdir2a.normalize();
	Vector3d uvdir2 = uvdir2a*cos(anisoangle) + uvdperp*sin(anisoangle);
	uvdir2.normalize();
	Vector3d uvdperp2 = uvdir2a*-sin(anisoangle) + uvdperp*cos(anisoangle);
	uvdperp2.normalize();

	RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
	#ifdef RAND_PER_THREAD
		float a = rg->getRandomFloat();
		float b = rg->getRandomFloat();
	#else
		float a = rand()/(float)RAND_MAX;
		float b = rand()/(float)RAND_MAX;
	#endif

	float phiH_ = 0;
	if (b>0.5f)
		phiH_ = atan(alphag_y/alphag_x*tan(2*PI*(b-0.5f)));
	else
		phiH_ = atan(alphag_y/alphag_x*tan(2*PI*b))+PI;
	float sinPhiH_ = sin(phiH_);
	float cosPhiH_ = cos(phiH_);
	float sin2PhiH_ = sinPhiH_ * sinPhiH_;
	float cos2PhiH_ = cosPhiH_ * cosPhiH_;

	float tanthetaH = sqrt(-log(a)/( cos2PhiH_/alphag_x/alphag_x + sin2PhiH_/alphag_y/alphag_y ));
	float thetaH = atan(tanthetaH);
	float sinThetaH = sin(thetaH);
	float cosThetaH = cos(thetaH);

	Vector3d H_ = uvdir2*cosPhiH_ + uvdperp2*sinPhiH_;
	H_.normalize();
	Vector3d H = H_*sinThetaH + hd.normal_shade*cosThetaH;
	H.normalize();

	Vector3d res1 = hd.in.reflect(H);
	res1.normalize();
	if ((res1*hd.normal_geom)*(hd.in*hd.normal_geom) < 0)
	{
		hd.out = res1;
		probability = 0.0f;
		hd.setFlagInvalidRandom();
		return hd.out;
	}

	hd.out = res1;
	probability = getProbabilityMicrofacetAniso(hd);

	return hd.out;
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityMicrofacetAniso(HitData & hd)
{
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);
	if (!sameSide)
		return 0.0f;

	Vector3d H = hd.in + hd.out;
	H.normalize();	
	if (H*hd.normal_shade < 0)
		H.invert();

	float hMulIn = H*hd.in;
	float hMulOut = H*hd.out;
	float hhio = hMulIn*hMulOut;
	if (hhio < 0)
		return 0;

	float alphag = getRough(hd.tU, hd.tV);
	float aniso = getAnisotropy(hd.tU, hd.tV);
	float anisoangle = getAnisoAngle(hd.tU, hd.tV) * PI/180.0f;
	float alphag_x=alphag, alphag_y=alphag;
	if (aniso>0.0f)
		alphag_y = max(0.01f, alphag*(1-aniso));
	else
		alphag_x = max(0.01f, alphag*(1+aniso));

	Vector3d uvdperp = hd.normal_shade ^ hd.dir_U;
	uvdperp.normalize();
	Vector3d uvdir2a = uvdperp ^ hd.normal_shade;
	uvdir2a.normalize();
	Vector3d uvdir2 = uvdir2a*cos(anisoangle) + uvdperp*sin(anisoangle);
	uvdir2.normalize();
	Vector3d uvdperp2 = uvdir2a*-sin(anisoangle) + uvdperp*cos(anisoangle);
	uvdperp2.normalize();

	Vector3d H_ = H+hd.normal_shade*(H*hd.normal_shade*-1);
	H_.normalize();

	float cosPhiH_ = H_ * uvdir2;
	float cos2PhiH_ = cosPhiH_*cosPhiH_;
	float sin2PhiH_ = 1.0f - cos2PhiH_;
	float cosThetaH = fabs(H*hd.normal_shade);
	float cos2ThetaH = cosThetaH * cosThetaH;
	float sin2ThetaH = (1-cos2ThetaH);
	float tan2ThetaH = sin2ThetaH/cos2ThetaH;

	float m = 4*PI*alphag_x*alphag_y*cos2ThetaH*cosThetaH*fabs(H*hd.out);
	float l = exp(-tan2ThetaH*(cos2PhiH_/(alphag_x*alphag_x) + sin2PhiH_/(alphag_y*alphag_y) ));
	float D = l/m;
	return D;
}

//-------------------------------------------------------------------------------------------

Color4 MatLayer::getBRDFMicrofacetAniso(HitData & hd)
{
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);
	if (!sameSide)
		return Color4(0,0,0);

	float alphag = getRough(hd.tU, hd.tV);
	float aniso = getAnisotropy(hd.tU, hd.tV);
	float anisoangle = getAnisoAngle(hd.tU, hd.tV) * PI/180.0f;
	float alphag_x=alphag, alphag_y=alphag;
	if (aniso>0.0f)
		alphag_y = max(0.01f, alphag*(1-aniso));
	else
		alphag_x = max(0.01f, alphag*(1+aniso));

	Vector3d H = hd.in + hd.out;
	H.normalize();
	if (H*hd.normal_shade < 0)
		H.invert();
	if ( (H*hd.in) * (H*hd.out) < 0 )
		return Color4(0,0,0);
	Vector3d H_ = H+hd.normal_shade*(H*hd.normal_shade*-1);
	H_.normalize();

	Vector3d uvdperp = hd.normal_shade ^ hd.dir_U;
	uvdperp.normalize();
	Vector3d uvdir2a = uvdperp ^ hd.normal_shade;
	uvdir2a.normalize();
	Vector3d uvdir2 = uvdir2a*cos(anisoangle) + uvdperp*sin(anisoangle);
	uvdir2.normalize();
	Vector3d uvdperp2 = uvdir2a*-sin(anisoangle) + uvdperp*cos(anisoangle);
	uvdperp2.normalize();

	float cosPhiH_ = H_ * uvdir2;
	float cos2PhiH_ = cosPhiH_*cosPhiH_;
	float sin2PhiH_ = 1.0f - cos2PhiH_;
	float cosThetaH = fabs(H*hd.normal_shade);
	float cos2ThetaH = cosThetaH * cosThetaH;
	float sin2ThetaH = (1-cos2ThetaH);
	float tan2ThetaH = sin2ThetaH/cos2ThetaH;
	float pp = alphag*alphag + tan2ThetaH;
	float m = PI*alphag_x*alphag_y*cos2ThetaH*cos2ThetaH;
	float l = exp(-tan2ThetaH*(cos2PhiH_/(alphag_x*alphag_x) + sin2PhiH_/(alphag_y*alphag_y) ));
	float D = l/m;

	float cosTheta_i = fabs(hd.in  * hd.normal_shade);
	float cosTheta_o = fabs(hd.out * hd.normal_shade);
	cosTheta_i = min(1, cosTheta_i);
	cosTheta_o = min(1, cosTheta_o);
	float tan2Theta_i = (1-cosTheta_i*cosTheta_i)/(cosTheta_i*cosTheta_i);
	float tan2Theta_o = (1-cosTheta_o*cosTheta_o)/(cosTheta_o*cosTheta_o);


	float f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H),  true);
	float FF = f;
	float aniso_alpha = 0.5f;
	float brdf =  D * FF * 0.25f / pow(cosTheta_i*cosTheta_o, aniso_alpha) / fabs(hd.out * H);
	float nthI = fabs(FastMath::acos(cosTheta_i))/PI*2.0f;
	float nthO = fabs(FastMath::acos(cosTheta_o))/PI*2.0f;

	return (getColor0(hd.tU, hd.tV)*(1-nthI) + (getColor90(hd.tU, hd.tV)*(1-alphag)+getColor0(hd.tU, hd.tV)*alphag)*nthI) * brdf;

	return getColor0(hd.tU, hd.tV)* brdf;
}

//-------------------------------------------------------------------------------------------
#define EQUALS_ROUND_SUB(a, b, delta) (fabs((a)-(b))<(delta))
#define EQUALS_ROUND_DIV(a, b, delta) (fabs((a)/(b)-1)<(delta))
#define EQUALS_ROUND(a, b, delta) ((EQUALS_ROUND_SUB(a, b, delta)) && (EQUALS_ROUND_DIV(a, b, delta)))

bool MatLayer::getBRDFandPDFsMicrofacetAniso(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b)
{
	brdf = getBRDFMicrofacetAniso(hd);
	pdf_f = getProbabilityMicrofacetAniso(hd);
	HitData hdback = hd;
	hdback.swapDirections();
	pdf_b = getProbabilityMicrofacetAniso(hdback);
	return true;

	bool goingOut = (hd.in*hd.normal_shade < 0   &&   hd.out*hd.normal_shade > 0);
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);

	float cosThetaInShade  = hd.normal_shade*hd.in;
	float cosThetaOutShade = hd.normal_shade*hd.out;
	float cosThetaInGeom   = hd.normal_geom *hd.in;
	float cosThetaOutGeom  = hd.normal_geom *hd.out;

	if ( (!transmOn  &&  cosThetaInGeom*cosThetaOutGeom<=0)  ||  cosThetaInShade==0  ||  cosThetaOutShade==0)
	{
		brdf = Color4(0.0f, 0.0f, 0.0f);
		pdf_f = pdf_b = 0.0f;
	}
	else
	{
		float alphag = getRough(hd.tU, hd.tV);
		float alphag2 = alphag * alphag;
		Vector3d H;
		if (sameSide)
			H = hd.in + hd.out;
		else
			if (goingOut)
				H = hd.in * fresnel.IOR + hd.out;
			else
				H = hd.in + hd.out * fresnel.IOR;
		H.normalize();	
		if (H*hd.normal_shade < 0)	
			H.invert();

		float hMulIn = H*hd.in;
		float hMulOut = H*hd.out;
		float hhio = hMulIn*hMulOut;
		if ( (hhio>0  &&  !sameSide)  ||  (hhio<0  &&  sameSide))
		{
			brdf = Color4(0.0f, 0.0f, 0.0f);
			pdf_f = pdf_b = 0.0f;
		}
		else
		{
			float cosThetaH = fabs(H*hd.normal_shade);
			float cos2ThetaH = cosThetaH * cosThetaH;
			float sin2ThetaH = (1-cos2ThetaH);
			float tan2ThetaH = sin2ThetaH/cos2ThetaH;
			float pp = alphag2 + tan2ThetaH;
			float m = PI*alphag*alphag*cos2ThetaH*cos2ThetaH;
			float l = exp(-tan2ThetaH/alphag/alphag);
			float D = l/m;
			float jacobian_f = 1.0f, jacobian_b = 1.0f;
 
			if (transmOn && !sameSide)
			{
				if (goingOut)
				{
					jacobian_f = fabs(hd.out*H)/pow(((hd.in*H)*fresnel.IOR + (hd.out*H)),2);
					jacobian_b = fresnel.IOR*fresnel.IOR*fabs(hd.in*H)/pow((hd.out*H + fresnel.IOR*(hd.in*H)), 2);
				}
				else
				{
					jacobian_f = fresnel.IOR*fresnel.IOR*fabs(hd.out*H)/pow((hd.in*H + fresnel.IOR*(hd.out*H)), 2);
					jacobian_b = fabs(hd.in*H)/pow(((hd.out*H)*fresnel.IOR + (hd.in*H)),2);
				}
			}
			else
			{
				jacobian_f = 0.25f/fabs(hd.out * H);
				jacobian_b = 0.25f/fabs(hd.in * H);
 			}

			bool fgoingIn = (hd.in*hd.normal_shade > 0) || !transmOn;
			float f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H), fgoingIn );
			float FT = 1.0f;
			if (transmOn)
				FT = sameSide ? f : 1.0f - f;

			pdf_f = D * cosThetaH * jacobian_f * FT;
			pdf_b = D * cosThetaH * jacobian_b * FT;

			float cosTheta_i = min(1, fabs(hd.in  * hd.normal_shade));
			float cosTheta_o = min(1, fabs(hd.out * hd.normal_shade));
			float tan2Theta_i = (1-cosTheta_i*cosTheta_i)/(cosTheta_i*cosTheta_i);
			float tan2Theta_o = (1-cosTheta_o*cosTheta_o)/(cosTheta_o*cosTheta_o);

			//G_i
			float kk,G_i,G_o,aa;
			kk  = (H*hd.in/(hd.in*hd.normal_shade)>0 ? 1.0f : 0.0f);
			aa = 1.0f / (alphag * sqrt(tan2Theta_i));
			if (aa<1.6f)
				G_i = kk * (3.535f*aa + 2.181f*aa*aa) / (1 + 2.276f*aa + 2.577f*aa*aa);
			else
				G_i = kk;

			//G_o
			kk  = (H*hd.out/(hd.out*hd.normal_shade)>0 ? 1.0f : 0.0f);
			aa = 1.0f / (alphag * sqrt(tan2Theta_o));
			if (aa<1.6f)
				G_o = kk * (3.535f*aa + 2.181f*aa*aa) / (1 + 2.276f*aa + 2.577f*aa*aa);
			else
				G_o = kk;


			Color4 absorption = Color4(1,1,1);
			if (transmOn  &&  absOn  &&  (hd.in*hd.normal_shade<0))	// warning: non-symmetric
			{	// internal reflection or transmission going out - absorb part of energy
				absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
				absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
				absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
			}

			if (transmOn && !sameSide)
			{
				float brdfm;
				if (goingOut)
				{
					brdfm = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
							(1-f)*G_i*G_o*D/(pow((hd.in*H) * fresnel.IOR + (hd.out*H),2));
				}
				else
				{
					brdfm = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
							fresnel.IOR*fresnel.IOR*(1-f)*G_i*G_o*D/(pow((hd.in*H) + fresnel.IOR * (hd.out*H),2));
				}
				brdf = getTransmissionColor(hd.tU, hd.tV)*brdfm*absorption;
			}
			else
			{
				float brdfm = G_i*G_o * D * f * 0.25f / cosTheta_i / cosTheta_o;
				float nthI = fabs(FastMath::acos(cosTheta_i))/PI*2.0f;
				float nthO = fabs(FastMath::acos(cosTheta_o))/PI*2.0f;
				brdf = (getColor0(hd.tU, hd.tV)*(1-nthI) + (getColor90(hd.tU, hd.tV)*(1-alphag)+getColor0(hd.tU, hd.tV)*alphag)*nthI) * absorption * brdfm;
			}

			if (brdf.isNaN())
			{
				char buf[256];
				sprintf_s(buf, 256, "brdf nan... G_i=%f  G_o=%f  D=%f  f=%f  cosTheta_i=%f  cosTheta_o=%f", G_i, G_o, D, f, cosTheta_i, cosTheta_o);
				Logger::add(buf);
			}

		}
	}

	//#define VERIFY_MATLAYER_BRDF_PDFS

	#ifdef VERIFY_MATLAYER_BRDF_PDFS
		Color4 vbrdf = getBRDFMicrofacetAniso(hd);
		float vpdf_f = getProbabilityMicrofacetAniso(hd);
		HitData hdback = hd;
		hdback.swapDirections();
		float vpdf_b = getProbabilityMicrofacetAniso(hdback);
		if (!EQUALS_ROUND(vbrdf.r, brdf.r, 0.05f)  ||  !EQUALS_ROUND(vbrdf.g, brdf.g, 0.05f)  ||  !EQUALS_ROUND(vbrdf.b, brdf.b, 0.05f))
		{
			char buf[256];
			sprintf_s(buf, 256, "brdf forward verify failed on beckmann.. cos_in=%f  cos_out=%f  brdf_new=%f %f %f  brdf_old=%f %f %f", cosThetaInShade, cosThetaOutShade,
				brdf.r, brdf.g, brdf.b, vbrdf.r, vbrdf.g, vbrdf.b );
			Logger::add(buf);
		}
		if (!EQUALS_ROUND(vpdf_f, pdf_f, 0.02f))
		{
			char buf[256];
			sprintf_s(buf, 256, "pdf forward verify failed on beckmann.. cos_in=%f  cos_out=%f  fpdf_new=%f  fpdf_old=%f", cosThetaInShade, cosThetaOutShade, pdf_f, vpdf_f);
			Logger::add(buf);
		}
		if (!EQUALS_ROUND(vpdf_b, pdf_b, 0.02f))
		{
			char buf[256];
			sprintf_s(buf, 256, "pdf backward verify failed on beckmann.. cos_in=%f  cos_out=%f  bpdf_new=%f  bpdf_old=%f", cosThetaInShade, cosThetaOutShade, pdf_b, vpdf_b);
			Logger::add(buf);
		}
	#endif

	return true;
}

//-------------------------------------------------------------------------------------------
