#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "IES.h"
#include "log.h"

bool drawGraphIES(ImageByteBuffer * imgbuf, IesNOX * ies);
bool fillIESData(HWND hWnd, IesNOX * ies);

//------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK IESDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH hBrush = 0;
	static IesNOX * ies = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				hBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				IesNOX * oIES = (IesNOX *)lParam;
				ies = new IesNOX();
				if (oIES)
					*ies = *oIES;

				EMButton   * emOK			= GetEMButtonInstance(GetDlgItem(hWnd, IDC_IES_OK));
				EMButton   * emCancel		= GetEMButtonInstance(GetDlgItem(hWnd, IDC_IES_CANCEL));
				EMButton   * emLoad			= GetEMButtonInstance(GetDlgItem(hWnd, IDC_IES_LOAD));
				EMButton   * emUnload		= GetEMButtonInstance(GetDlgItem(hWnd, IDC_IES_RELEASE));
				EMPView    * emGraph		= GetEMPViewInstance(GetDlgItem(hWnd, IDC_IES_PREVIEW));
				EMText	   * emText			= GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_FILE));
				EMText	   * emFilename		= GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_FILENAME));
				EMText	   * emTextRated	= GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_RATED_POWER));
				EMText	   * emTextLumens	= GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_LUMENS));
				EMText	   * emTextPower	= GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_POWER));
				RECT crect;
				GetClientRect(GetDlgItem(hWnd, IDC_IES_PREVIEW), &crect);
				GlobalWindowSettings::colorSchemes.apply(emOK);
				GlobalWindowSettings::colorSchemes.apply(emCancel);
				GlobalWindowSettings::colorSchemes.apply(emGraph);
				GlobalWindowSettings::colorSchemes.apply(emLoad);
				GlobalWindowSettings::colorSchemes.apply(emUnload);
				GlobalWindowSettings::colorSchemes.apply(emText, true, false);
				GlobalWindowSettings::colorSchemes.apply(emFilename, true, false);
				GlobalWindowSettings::colorSchemes.apply(emTextRated, true, false);
				GlobalWindowSettings::colorSchemes.apply(emTextLumens, true, false);
				GlobalWindowSettings::colorSchemes.apply(emTextPower, true, false);
				emTextPower->align = EMText::ALIGN_RIGHT;
				InvalidateRect(emTextPower->hwnd, NULL, false);

				if (!emGraph->byteBuff)
					emGraph->byteBuff = new ImageByteBuffer();
				emGraph->byteBuff->allocBuffer(crect.right, crect.bottom);
				emGraph->byteBuff->clearBuffer();
				emGraph->dontLetUserChangeZoom = true;
				drawGraphIES(emGraph->byteBuff, ies);
				InvalidateRect(emGraph->hwnd, NULL, false);

				fillIESData(hWnd, ies);
			}
			break;
		case WM_DESTROY:
			{
				DeleteObject(hBrush);
				hBrush = 0;
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)(-1));
			}
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
				case IDC_IES_OK:
					{
						if (wmEvent == BN_CLICKED)
						{
							if (!ies->isValid())
							{
								ies->releaseStuff();
								delete ies;
								ies = NULL;
								EndDialog(hWnd, (INT_PTR)(0));
							}
							else
							{
								IesNOX * r_ies = ies;
								ies = NULL;
								EndDialog(hWnd, (INT_PTR)(r_ies));
							}
						}
					}
					break;
				case IDC_IES_CANCEL:
					{
						if (wmEvent == BN_CLICKED)
						{
							EndDialog(hWnd, (INT_PTR)(-1));
						}
					}
					break;
				case IDC_IES_LOAD:
					{
						if (wmEvent == BN_CLICKED)
						{
							char * fname = openIesFileDialog(hWnd);
							if (fname)
							{
								ies->releaseStuff();
								bool ok = ies->parseFile(fname);
								if (!ok)
								{
									Logger::add("IES parse failed... error:");
									Logger::add(ies->errorBuf);
									MessageBox(0, ies->errorBuf, "IES parsing error", 0);
								}
							}

							EMPView * emGraph = GetEMPViewInstance(GetDlgItem(hWnd, IDC_IES_PREVIEW));
							drawGraphIES(emGraph->byteBuff, ies);
							InvalidateRect(emGraph->hwnd, NULL, false);
							
							fillIESData(hWnd, ies);

						}
					}
					break;
				case IDC_IES_RELEASE:
					{
						ies->releaseStuff();
						EMPView * emGraph = GetEMPViewInstance(GetDlgItem(hWnd, IDC_IES_PREVIEW));
						drawGraphIES(emGraph->byteBuff, ies);
						InvalidateRect(emGraph->hwnd, NULL, false);
						EMText * emFilename = GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_FILENAME));
						fillIESData(hWnd, ies);
					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)hBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)hBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------------

bool drawGraphIES(ImageByteBuffer * imgbuf, IesNOX * ies)
{
	CHECK(imgbuf);
	//CHECK(ies);
	imgbuf->clearBuffer();

	bool showLines = true;
	int llength = 6;
	COLORREF linesColor = RGB(48,48,48);
	int w = imgbuf->width;
	int h = imgbuf->height;
	int cx = w/2;
	int cy = h/2;

	if (showLines)
	{
		for (int x=0; x<w; x++)
		{
			int y = cy;
			int a = x%(llength*2)/llength;
			COLORREF col = linesColor*(1-a);
			imgbuf->imgBuf[y][x] = col;
		}
		for (int y=0; y<h; y++)
		{
			int x = cx;
			int a = y%(llength*2)/llength;
			COLORREF col = linesColor*(a);
			imgbuf->imgBuf[y][x] = col;
		}
	}

	if (!ies)
		return true;

	if (ies->numAnglesH < 1)
		return true;
	if (ies->numAnglesV < 2)
		return true;

	float b = ies->ballast;
	float cm = ies->candelaMultiplier;
	float mm = ies->getMaxValue();
	int num = ies->numAnglesV;
	int vd = min(cx,cy);

	for (int i=0; i<num-1; i++)
	{
		float p1 = ies->values[0][i];
		float p2 = ies->values[0][(i+1)%num];
		float a1 = ies->anglesV[i] * PI / 180.0f;
		float a2 = ies->anglesV[(i+1)%num] * PI / 180.0f;

		float d1 = p1 * b * cm / mm * 0.9f;
		float d2 = p2 * b * cm / mm * 0.9f;
		float x1 = sin(a1) * d1 * vd + cx;
		float y1 = cos(a1) * d1 * vd + cy;
		float x2 = sin(a2) * d2 * vd + cx;
		float y2 = cos(a2) * d2 * vd + cy;

		if (x1>=0 && x1<w && y1>=0 && y1<h)
			if (x2>=0 && x2<w && y2>=0 && y2<h)
			{
				if ( fabs(y2-y1) > fabs(x2-x1) )
				{
					int iy1 = (int)min(y1, y2);
					int iy2 = (int)max(y1, y2);
					if (iy2==iy1)
						imgbuf->imgBuf[iy1][(int)x1] = RGB(255,0,0);
					else
						for (int i=iy1; i<=iy2; i++)
						{
							int x = (int)( (x2-x1) * (i-y1)/(y2-y1) + x1 );
							imgbuf->imgBuf[i][x] = RGB(255,0,0);
						}
				}
				else
				{
					int ix1 = (int)min(x1, x2);
					int ix2 = (int)max(x1, x2);
					if (ix2==ix1)
						imgbuf->imgBuf[(int)y1][ix1] = RGB(255,0,0);
					else
						for (int i=ix1; i<=ix2; i++)
						{
							int y = (int)( (y2-y1) * (i-x1)/(x2-x1) + y1 );
							imgbuf->imgBuf[y][i] = RGB(255,0,0);
						}
				}
			}
	}

	for (int i=0; i<num-1; i++)
	{
		float p1 = ies->values[0][i];
		float p2 = ies->values[0][(i+1)%num];
		float a1 = ies->anglesV[i] * PI / 180.0f;
		float a2 = ies->anglesV[(i+1)%num] * PI / 180.0f;

		float d1 = p1 * b * cm / mm * 0.9f;
		float d2 = p2 * b * cm / mm * 0.9f;
		float x1 = -sin(a1) * d1 * vd + cx;
		float y1 = cos(a1) * d1 * vd + cy;
		float x2 = -sin(a2) * d2 * vd + cx;
		float y2 = cos(a2) * d2 * vd + cy;

		if (x1>=0 && x1<w && y1>=0 && y1<h)
			if (x2>=0 && x2<w && y2>=0 && y2<h)
			{
				if ( fabs(y2-y1) > fabs(x2-x1) )
				{
					int iy1 = (int)min(y1, y2);
					int iy2 = (int)max(y1, y2);
					if (iy2==iy1)
						imgbuf->imgBuf[iy1][(int)x1] = RGB(255,0,0);
					else
						for (int i=iy1; i<=iy2; i++)
						{
							int x = (int)( (x2-x1) * (i-y1)/(y2-y1) + x1 );
							imgbuf->imgBuf[i][x] = RGB(255,0,0);
						}
				}
				else
				{
					int ix1 = (int)min(x1, x2);
					int ix2 = (int)max(x1, x2);
					if (ix2==ix1)
						imgbuf->imgBuf[(int)y1][ix1] = RGB(255,0,0);
					else
						for (int i=ix1; i<=ix2; i++)
						{
							int y = (int)( (y2-y1) * (i-x1)/(x2-x1) + y1 );
							imgbuf->imgBuf[y][i] = RGB(255,0,0);
						}
				}
			}
	}

	return true;
}

//------------------------------------------------------------------------------------------------------

bool fillIESData(HWND hWnd, IesNOX * ies)
{
	if (!ies)
		return false;

	EMText * emFilename = GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_FILENAME));
	EMText * emTextPower = GetEMTextInstance(GetDlgItem(hWnd, IDC_IES_TEXT_POWER));

	if (ies->filename)
	{
		char * ofname = getOnlyFile(ies->filename);
		if (ofname)
		{
			emFilename->changeCaption(ofname);
			free(ofname);
		}
		else
			emFilename->changeCaption("error getting filename");
	}
	else
		emFilename->changeCaption("none");

	int numL = max(1, ies->numLamps);
	float power = ies->lumensPerLamp * numL;
	char buf[64];
	sprintf_s(buf, "%.2f", power);
	emTextPower->changeCaption(buf);

	return true;
}

//------------------------------------------------------------------------------------------------------
