#ifndef __BINARY_SCENE_H
#define __BINARY_SCENE_H

#include "BinaryValues.h"
#include <stdio.h>
#include "raytracer.h"

class BinaryScene
{
public:
	FILE * file;
	char * directory;

	Point3d cbMin, cbMax;
	int triCounter;
	int curMeshNum;	
	int cTri;
	int singleTriSize;
	unsigned int totalTris;
	unsigned int soFarTris;
	bool showMessages;
	bool addCamBuffs;
	int mrgCamID;



	char * getValidTexPath(char * fname);

	// LOAD STUFF
	bool LoadScene(char * filename, bool merge=false);
	bool checkHeader(char * filename);
	bool ignoreChunk();
	bool parseSceneChunk(bool merge);
	bool parseUnknownChunk(unsigned short chunkID, unsigned int sizesize=4);
	bool parseGeometryChunk();
	bool parseMeshChunk();
	bool parseMeshNameChunk(Mesh * mesh);
	bool parseInstanceChunk();
	bool parseInstanceSourceChunk();
	bool parseTriangleChunk();
	bool parseTriangleFastChunk();
	bool parseCameraChunk(bool merge);
	bool parseCameraNameChunk(Camera * cam);
	bool parseCameraBuffersChunk(Camera * cam);
	bool parseCameraDepthBufChunk(Camera * cam);
	bool parseMaterialsChunk();
	bool parseMaterialChunk();
	bool parseMaterialPreviewChunk(MaterialNox * mat);
	bool parseMaterialNameChunk(MaterialNox * mat);
	bool parseMatLayerChunk(MaterialNox * mat);
	bool parseTextureChunk(MaterialNox * mat, MatLayer * mlay);
	bool parseTextureFileChunk(MaterialNox * mat, MatLayer * mlay);
	bool parseTextPostChunk(TexModifer * texmod);
	bool parseNormPostChunk(NormalModifier * normmod);
	bool parseSunskyChunk();
	bool parseBlendsChunk(BlendSettings * blend);
	bool parseUsercolorsChunk();
	bool parseCameraPostChunk(ImageModifier * iMod);
	bool parseCameraFinalPostChunk(FinalModifier * fMod, char ** texDiaph, char ** texObst);
	bool parseRendererSettingsChunk();
	bool parseSceneInfoChunk();
	bool parseSceneSettingsChunk();
	bool parseEnvironmentMapChunk();
	bool parseUserPresets();
	bool parseUserPreset(PresetPost * preset);
	
	bool loadBuffer(void * buf, unsigned long long bsize, unsigned int chunksize);

	// SAVE STUFF
	unsigned long long geometrySize;
	unsigned long long sceneSize;
	unsigned long long fileSize;
	unsigned long long materialsSize;

	bool SaveScene(char * filename, bool addCamBuffers, bool silentMode=false);
	bool insertScenePart();
	bool insertGeometryPart();
	bool insertInstancePart(unsigned int iId);
	bool insertInstanceSourcePart(unsigned int mId);
	bool insertMeshPart(unsigned int mNum);
	bool insertTrianglePart(unsigned int tNum);
	bool insertMaterialsAllPart();
	bool insertMaterialSinglePart(unsigned int mNum);
	bool insertMatLayerPart(unsigned int mNum, unsigned int lNum);
	bool insertMatPreviewPart(unsigned int mNum);
	bool insertCameraPart(unsigned int cNum);
	bool insertCameraBuffersPart(unsigned int cNum);
	bool insertCameraDepthBufPart(unsigned int cNum);
	bool insertSunskyPart();
	bool insertBlendLayersPart(BlendSettings * blend);
	bool insertUserColorsPart();
	bool insertCameraPostPart(ImageModifier * iMod);
	bool insertCameraFinalPostPart(FinalModifier * fMod, char * texDiaph, char * texObst);
	bool insertRendererSettingsPart();
	bool insertSceneInfoPart();
	bool insertSceneSettingsPart();
	bool insertTexturePart(unsigned int mNum, unsigned int lNum, unsigned int slot);
	bool insertEnvironmentMapPart();
	bool insertUserPresets();
	bool insertUserPreset(PresetPost * preset);



	unsigned long long evalSizeScene();
	unsigned long long evalSizeGeometry();
	unsigned long long evalSizeInstancePart(unsigned int iId);
	unsigned long long evalSizeInstanceSourcePart(unsigned int mId);
	unsigned long long evalSizeMesh(unsigned int num);
	unsigned long long evalSizeTriangle();
	unsigned long long evalSizeCamera(unsigned int num);
	unsigned long long evalSizeCameraBuffers(unsigned int num);
	unsigned long long evalSizeCameraDepthBuf(unsigned int num);
	unsigned long long evalSizeMaterials();
	unsigned long long evalSizeMaterial(unsigned int num);
	unsigned long long evalSizeMaterialPreview(unsigned int num);
	unsigned long long evalSizeMaterialLayer(unsigned int num, unsigned int numlayer);
	unsigned long long evalSizeTexture(unsigned int num, unsigned int numlayer, unsigned int slot);
	unsigned long long evalSizeSunsky();
	unsigned long long evalSizeBlendLayers(BlendSettings * blend);
	unsigned long long evalSizeUserColors();
	unsigned long long evalSizePost(ImageModifier * iMod);
	unsigned long long evalSizeRendererSettings();
	unsigned long long evalSizeSceneInfo();
	unsigned long long evalSizeSceneSettings();
	unsigned long long evalSizeCameraFinalPost(FinalModifier * fMod, char * texDiaph, char * texObst);
	unsigned long long evalSizeEnvironmentMap();
	unsigned long long evalSizeUserPresets();
	unsigned long long evalSizeUserPreset(PresetPost * preset);




	BinaryScene();
	~BinaryScene();

};

#endif
