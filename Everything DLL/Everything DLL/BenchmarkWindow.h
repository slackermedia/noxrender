#include <windows.h>
#include "ColorSchemes.h"

#define TIMER_STOP_ID 101

#define CHECK_BENCHMARK_LICENSE
#define CHECK_BENCHMARK_SCENE

#define font1Filename "OpenSans-Light.ttf"
#define font1Name "Open Sans Light"
#define coresthreadstext "CORES / THREADS: "
#define cputext "CPU: "
#define CPUFONTTRIES 4
#define text_code "CODE: "
#define text_code_value "PRZEMYS�AW MARCHLEWSKI"
#define text_gui "GUI: "
#define text_gui_value "MICHA� FRANCZAK"
#define text_scene "SCENE: "
#define text_scene_value "ADAM ZIMIRSKI"
#define text_scene2 "CONVERSION: "
#define text_scene2_value "ZBIGNIEW \"ZED\" RATAJCZAK"

#define NBMODE_NORMAL 0
#define NBMODE_ABOUT 1
#define NBMODE_LICENSE 2
#define NBMODE_LOADING 3
#define NBMODE_RENDERING 4

INT_PTR CALLBACK UploadBenchResDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

//----------------------------------------------------------------------------------------------------------------

struct BenchResult
{
	char * cpuname;
	int nox_ver_major;
	int nox_ver_minor;
	int nox_ver_build;
	int num_cores;
	int num_threads;
	int num_cpus;
	unsigned int session_id;
	float result;

	BenchResult() { cpuname=NULL; result=0.0f; nox_ver_major=0; nox_ver_minor=0; nox_ver_build=0; num_cores=0; num_threads=0; num_cpus=0; session_id=0; }
};

//----------------------------------------------------------------------------------------------------------------

class BenchmarkWindow
{
public:
	static HINSTANCE hInstance2;
	static HMODULE hModule;

	static HFONT hFontYourScore;
	static HFONT hFontResult;
	static HFONT hFontHardware;
	static HFONT hFontCPU[CPUFONTTRIES];
	static HFONT hFontCPUName[CPUFONTTRIES];
	static HFONT hFontCoresThreads[CPUFONTTRIES];
	static HFONT hFontCoresThreadsValue[CPUFONTTRIES];
	static HFONT hFontResultsComparison;
	static HFONT hFontVisitResultsPage;
	static HFONT hFontEngineVersion;
	static HFONT hFontAbout;
	static HFONT hFontBars;
	static int cpuFontChosen;
	static HFONT hFontCrEngine;
	static HFONT hFontCrBuild;
	static HFONT hFontCrCredits;
	static HFONT hFontCrCode;
	static HFONT hFontCrCodeValue;
	static HFONT hFontCrGui;
	static HFONT hFontCrGuiValue;
	static HFONT hFontCrScene;
	static HFONT hFontCrSceneValue;
	static HFONT hFontCrContactUs;
	static HFONT hFontCrEmail;
	static HFONT hFontLicense;
	static HFONT hFontProgress;
	static HFONT hFontMessageText;
	static HFONT hFontMessageButton;


	static HFONT hFontGui;
	static HBITMAP hBmpBg;
	static HBITMAP hBmpBgEmpty;
	static HBITMAP hBmpBgLic;
	static HBITMAP hBmpButtons;
	static HBRUSH backgroundBrush;
	static ThemeManager theme;

	static HWND hMain;
	static HWND hImage;
	static HWND hStartStop;
	static HWND hResultValue;
	static HWND hSendResult;
	static HWND hViewResults;
	static HWND hCompRes;
	static HWND hCpuText;
	static HWND hCpuValue;
	static HWND hCoresThreadsText;
	static HWND hCoresThreadsValue;
	static HWND hVersion;
	static HWND hAbout;
	static HWND hProgress;
	static HWND hLoadingTxt;

	static HWND hLicText;
	static HWND hLicAccept;
	static HWND hLicScroll;

	static HWND hCrEngine;
	static HWND hCrBuild;
	static HWND hCrCredits;
	static HWND hCrCode;
	static HWND hCrCodeName;
	static HWND hCrGui;
	static HWND hCrGuiName;
	static HWND hCrScene;
	static HWND hCrSceneName;
	static HWND hCrScene2;
	static HWND hCrScene2Name;
	static HWND hCrContactUs;
	static HWND hCrEmail;
	static HWND hCrLogo;
	
	static int cmode;
	static bool about_mode;
	static bool license_mode;
	static bool loading_mode;
	static bool dll_ok;
	static bool scene_hash_ok;
	static bool refreshThrRunningB;
	static int license_pos;
	static int license_height;
	static int  refreshTimeB;
	static bool stoppingPassOn;
	static float bResult;
	static unsigned int bTimeStart;
	static BenchResult bsRes;
	static char * cpuNameTextFormatted;
	static char * coresThreadsCountText;
	static char * versionText;

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK LicParentWindowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	static DWORD WINAPI LoadSceneThreadProc(LPVOID lpParameter);
	ATOM MyRegisterClass(HINSTANCE hInstance);
	static ATOM LicenseRegisterClass(HINSTANCE hInstance);
	DECLDIR static bool createWindow(HWND hParent, int vMajor, int vMinor, int vBuild, int vComp);
	DECLDIR static HWND getHWND();
	static bool startLoadingThread();
	static void notifyProgressBarCallback(char * message, float progress);

	static void BenchmarkWindow::runRefreshThread();
	static DWORD WINAPI RefreshThreadProc(LPVOID lpParameter);
	static bool refreshRenderedImage(RedrawRegion * rgn=NULL);
	static void stoppedRenderingCallback(void);
	static bool startRender();
	static bool stopRender(bool byUser);
	static bool sendResultToServer();

	static bool loadFonts();
	static void unloadFonts();
	static int getTextWidth(char * txt, HFONT hFont);
	static void updateCpuCoresThreadsPositions();
	static void updateFrameVersionPositions();
	static void updateCreditsPositions();
	static char * formatCpuName(char * cpu);
	static bool addResToBars(float f);

	static void setMode(int newmode);
	static void checkAndAddLicense();
	static bool licenseAlreadyAccepted();
	static bool addLicenseAccepted();
	static bool evalSceneHash(char * filename, char * hash);



	DECLDIR BenchmarkWindow(HINSTANCE hInst);
	DECLDIR ~BenchmarkWindow();
};

//----------------------------------------------------------------------------------------------------------------
