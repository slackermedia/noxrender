#include "max2013predef.h"
#include "NOXMaterial.h"

#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"
#include "noxdebug.h"

//------------------------------------------------------------------------------------------------------------------------

#define NOX_MTL_CHUNK_MATERIAL 0x4000
#define NOX_MTL_CHUNK_MATNAME 0x4001
#define NOX_MTL_CHUNK_SKYPORTAL 0x4051
#define NOX_MTL_CHUNK_MATTE_SHADOW 0x4061
#define NOX_MTL_CHUNK_PREVIEW 0x4020
#define NOX_MTL_CHUNK_PREVIEW_X 0x4021
#define NOX_MTL_CHUNK_PREVIEW_Y 0x4022
#define NOX_MTL_CHUNK_PREVIEW_BUFFER 0x4027
#define NOX_MTL_CHUNK_LAYER 0x4005
#define NOX_MTL_CHUNK_LAYER_TYPE 0x4006
#define NOX_MTL_CHUNK_LAYER_CONTRIBUTION 0x4007
#define NOX_MTL_CHUNK_LAYER_EMISSION_COLOR 0x400A
#define NOX_MTL_CHUNK_LAYER_EMISSION_POWER 0x400B
#define NOX_MTL_CHUNK_LAYER_EMISSION_TEMPERATURE 0x400C
#define NOX_MTL_CHUNK_LAYER_EMISSION_UNIT 0x400D
#define NOX_MTL_CHUNK_LAYER_EMISSION_IES 0x400E
#define NOX_MTL_CHUNK_LAYER_ROUGHNESS 0x4011
#define NOX_MTL_CHUNK_LAYER_REFL0 0x4012
#define NOX_MTL_CHUNK_LAYER_REFL90 0x4013
#define NOX_MTL_CHUNK_LAYER_CONNECT90 0x404B
#define NOX_MTL_CHUNK_LAYER_ANISO 0x4057
#define NOX_MTL_CHUNK_LAYER_ANISO_ANGLE 0x4058
#define NOX_MTL_CHUNK_LAYER_BRDF_TYPE 0x4014
#define NOX_MTL_CHUNK_LAYER_TRANSMISSION_ON 0x4015
#define NOX_MTL_CHUNK_LAYER_TRANSMISSION_COLOR 0x404A
#define NOX_MTL_CHUNK_LAYER_ABSORPTION_ON 0x4016
#define NOX_MTL_CHUNK_LAYER_ABSORPTION_COLOR 0x4017
#define NOX_MTL_CHUNK_LAYER_ABSORPTION_DISTANCE 0x4018
#define NOX_MTL_CHUNK_LAYER_FRESNEL_IOR 0x4019
#define NOX_MTL_CHUNK_LAYER_TEX_REFL0 0x401A
#define NOX_MTL_CHUNK_LAYER_USE_TEX_REFL0 0x401B
#define NOX_MTL_CHUNK_LAYER_TEX_REFL90 0x401C
#define NOX_MTL_CHUNK_LAYER_USE_TEX_REFL90 0x401D
#define NOX_MTL_CHUNK_LAYER_TEX_ANISO 0x4059
#define NOX_MTL_CHUNK_LAYER_USE_TEX_ANISO 0x405A
#define NOX_MTL_CHUNK_LAYER_TEX_ANISO_ANGLE 0x405B
#define NOX_MTL_CHUNK_LAYER_USE_TEX_ANISO_ANGLE 0x405C
#define NOX_MTL_CHUNK_LAYER_TEX_TRANSMISSION 0x4047
#define NOX_MTL_CHUNK_LAYER_USE_TEX_TRANSMISSION 0x4048
#define NOX_MTL_CHUNK_LAYER_TEX_ROUGH 0x401E
#define NOX_MTL_CHUNK_LAYER_USE_TEX_ROUGH 0x401F
#define NOX_MTL_CHUNK_LAYER_TEX_WEIGHT 0x4020
#define NOX_MTL_CHUNK_LAYER_USE_TEX_WEIGHT 0x4021
#define NOX_MTL_CHUNK_LAYER_FAKE_GLASS 0x4022
#define NOX_MTL_CHUNK_VP_TEX_LAYER 0x4023
#define NOX_MTL_CHUNK_VP_TEX_TEX 0x4024
#define NOX_MTL_CHUNK_LAYER_TEX_LIGHT 0x4025
#define NOX_MTL_CHUNK_LAYER_USE_TEX_LIGHT 0x4026
#define NOX_MTL_CHUNK_LAYER_TEX_NORMAL 0x4042
#define NOX_MTL_CHUNK_LAYER_USE_TEX_NORMAL 0x4043
#define NOX_MTL_CHUNK_BLEND_NUMBER 0x4028
#define NOX_MTL_CHUNK_LAYER_SSS_ON 0x4044
#define NOX_MTL_CHUNK_LAYER_SSS_DENSITY 0x4045
#define NOX_MTL_CHUNK_LAYER_SSS_COLL_DIR 0x4046
#define NOX_MTL_CHUNK_OPACITY 0x4029
#define NOX_MTL_CHUNK_TEX_OPACITY 0x4030
#define NOX_MTL_CHUNK_USE_TEX_OPACITY 0x4031
#define NOX_MTL_CHUNK_DISPLACEMENT_DEPTH 0x404E
#define NOX_MTL_CHUNK_DISPLACEMENT_SUBDIVS 0x404F
#define NOX_MTL_CHUNK_DISPLACEMENT_SHIFT 0x4053
#define NOX_MTL_CHUNK_DISPLACEMENT_CONTINUITY 0x4054
#define NOX_MTL_CHUNK_DISPLACEMENT_NORMAL_MODE 0x4055
#define NOX_MTL_CHUNK_DISPLACEMENT_IGNORE_SCALE 0x4056
#define NOX_MTL_CHUNK_TEX_DISPLACEMENT 0x404C
#define NOX_MTL_CHUNK_USE_TEX_DISPLACEMENT 0x404D

#define NOX_MTL_CHUNK_TEX_FILTER_OPACITY 0x402F
#define NOX_MTL_CHUNK_TEX_FILTER_DISPLACEMENT 0x4050
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_WEIGHT 0x402A
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_LIGHT 0x402B
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_REFL0 0x402C
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_REFL90 0x402D
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_TRANSMISSION 0x4049
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_ANIZO 0x405F
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_ANIZO_ANGLE 0x4060
#define NOX_MTL_CHUNK_LAYER_TEX_FILTER_ROUGH 0x402E
#define NOX_MTL_CHUNK_TEX_PRECALC_OPACITY 0x4032
#define NOX_MTL_CHUNK_LAYER_TEX_PRECALC_WEIGHT 0x4033
#define NOX_MTL_CHUNK_LAYER_TEX_PRECALC_LIGHT 0x4034
#define NOX_MTL_CHUNK_LAYER_TEX_PRECALC_REFL0 0x4035
#define NOX_MTL_CHUNK_LAYER_TEX_PRECALC_REFL90 0x4036
#define NOX_MTL_CHUNK_LAYER_TEX_PRECALC_ROUGH 0x4037
#define NOX_MTL_CHUNK_TEX_POST_OPACITY 0x4038
#define NOX_MTL_CHUNK_TEX_POST_DISPLACEMENT 0x4052
#define NOX_MTL_CHUNK_LAYER_TEX_POST_WEIGHT 0x4039
#define NOX_MTL_CHUNK_LAYER_TEX_POST_LIGHT 0x403A
#define NOX_MTL_CHUNK_LAYER_TEX_POST_REFL0 0x403B
#define NOX_MTL_CHUNK_LAYER_TEX_POST_REFL90 0x403C
#define NOX_MTL_CHUNK_LAYER_TEX_POST_TRANSMISSION 0x403F
#define NOX_MTL_CHUNK_LAYER_TEX_POST_ROUGH 0x403D
#define NOX_MTL_CHUNK_LAYER_TEX_POST_NORMAL 0x403E
#define NOX_MTL_CHUNK_LAYER_TEX_POST_ANISO 0x405D
#define NOX_MTL_CHUNK_LAYER_TEX_POST_ANISO_ANGLE 0x405E
#define NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS 0x4080
#define NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST 0x4081
#define NOX_MTL_CHUNK_TEXTURE_POST_SATURATION 0x4082
#define NOX_MTL_CHUNK_TEXTURE_POST_HUE 0x4083
#define NOX_MTL_CHUNK_TEXTURE_POST_RED 0x4084
#define NOX_MTL_CHUNK_TEXTURE_POST_GREEN 0x4085
#define NOX_MTL_CHUNK_TEXTURE_POST_BLUE 0x4086
#define NOX_MTL_CHUNK_TEXTURE_POST_GAMMA 0x4087
#define NOX_MTL_CHUNK_TEXTURE_POST_INVERT 0x4088
#define NOX_MTL_CHUNK_LAYER_DISPERSION_VALUE 0x4040
#define NOX_MTL_CHUNK_LAYER_DISPERSION_ON 0x4041
#define NOX_MTL_CHUNK_TEXTURE_POSTNORM_POWER 0x4090
#define NOX_MTL_CHUNK_TEXTURE_POSTNORM_INVERT_X 0x4091
#define NOX_MTL_CHUNK_TEXTURE_POSTNORM_INVERT_Y 0x4092

#define SCHECK(r) if (r!=IO_OK) return r;

//------------------------------------------------------------------------------------------------------------------------

IOResult NOXMaterial::Save(ISave *isave) 
{
	ADD_LOG_PARTS_MAIN("NOXMaterial::Save");

	IOResult res;
	unsigned long wr;
	float tfloat;
	int tint;
	float t4float[4];
	const TCHAR * ctemp = GetName();//mat.getName();
	char * ctemp_ansi = getAnsiCopy1(ctemp);

	// MATERIAL
	isave->BeginChunk(NOX_MTL_CHUNK_MATERIAL);
	
	// MATERIAL NAME
	if (ctemp)
	{
		isave->BeginChunk(NOX_MTL_CHUNK_MATNAME);
		//res = isave->WriteCString(ctemp);
		res = isave->WriteCString(ctemp_ansi);
		SCHECK(res);
		isave->EndChunk();
	}
	if (ctemp_ansi)
		free(ctemp_ansi);

	// MATERIAL SKYPORTAL
	isave->BeginChunk(NOX_MTL_CHUNK_SKYPORTAL);
		tint = mat.getSkyportal() ? 1 : 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();

	// MATERIAL MATTE SHADOW
	isave->BeginChunk(NOX_MTL_CHUNK_MATTE_SHADOW);
		tint = mat.getMatteShadow() ? 1 : 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();

	// MATERIAL BLEND LAYER NUMBER
	isave->BeginChunk(NOX_MTL_CHUNK_BLEND_NUMBER);
		tint = mat.getBlendLayerNumber();
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// OPACITY
	isave->BeginChunk(NOX_MTL_CHUNK_OPACITY);
		tfloat = mat.getOpacity();
		res = isave->Write(&tfloat, sizeof(float), &wr);
		SCHECK(res);
	isave->EndChunk();
	// USE TEX OPACITY
	isave->BeginChunk(NOX_MTL_CHUNK_USE_TEX_OPACITY);
		if (mat.getUseTexOpacity())
			tint = 1;
		else
			tint = 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX OPACITY
	char * cOpac = mat.getTexOpacity();
	if (cOpac)
	{
		isave->BeginChunk(NOX_MTL_CHUNK_TEX_OPACITY);
		res = isave->WriteCString(cOpac);
		SCHECK(res);
		isave->EndChunk();
	}
	// TEX OPACITY FILTER
	isave->BeginChunk(NOX_MTL_CHUNK_TEX_FILTER_OPACITY);
		tint = mat.getTexPostFilterOpacity() ? 1 : 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// USE TEX DISPLACEMENT
	isave->BeginChunk(NOX_MTL_CHUNK_USE_TEX_DISPLACEMENT);
		if (mat.getUseTexDisplacement())
			tint = 1;
		else
			tint = 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX DISPLACEMENT
	char * cDisp = mat.getTexDisplacement();
	if (cDisp)
	{
		isave->BeginChunk(NOX_MTL_CHUNK_TEX_DISPLACEMENT);
		res = isave->WriteCString(cDisp);
		SCHECK(res);
		isave->EndChunk();
	}
	// TEX DISPLACEMENT SUBDIVS
	isave->BeginChunk(NOX_MTL_CHUNK_DISPLACEMENT_SUBDIVS);
		tint = mat.getDisplacementSubdivs();
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX DISPLACEMENT DEPTH
	isave->BeginChunk(NOX_MTL_CHUNK_DISPLACEMENT_DEPTH);
		tfloat = mat.getDisplacementDepth();
		res = isave->Write(&tfloat, sizeof(float), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX DISPLACEMENT SHIFT
	isave->BeginChunk(NOX_MTL_CHUNK_DISPLACEMENT_SHIFT);
		tfloat = mat.getDisplacementShift();
		res = isave->Write(&tfloat, sizeof(float), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX DISPLACEMENT NORMAL MODE
	isave->BeginChunk(NOX_MTL_CHUNK_DISPLACEMENT_NORMAL_MODE);
		tint = mat.getDisplacementNormalMode();
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX DISPLACEMENT CONTINUITY
	isave->BeginChunk(NOX_MTL_CHUNK_DISPLACEMENT_CONTINUITY);
	tint = mat.getDisplacementContinuity() ? 1 : 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX DISPLACEMENT IGNORE SCALE
	isave->BeginChunk(NOX_MTL_CHUNK_DISPLACEMENT_IGNORE_SCALE);
	tint = mat.getDisplacementIgnoreScale() ? 1 : 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();
	// TEX DISPLACEMENT FILTER
	isave->BeginChunk(NOX_MTL_CHUNK_TEX_FILTER_DISPLACEMENT);
		tint = mat.getTexPostFilterDisplacement() ? 1 : 0;
		res = isave->Write(&tint, sizeof(int), &wr);
		SCHECK(res);
	isave->EndChunk();

	// TEX OPACITY POST
	isave->BeginChunk(NOX_MTL_CHUNK_TEX_POST_OPACITY);
		// TEX POST BRIGHTNESS
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
			tfloat = mat.getTexPostBrightnessOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST CONTRAST
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
			tfloat = mat.getTexPostContrastOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST SATURATION
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
			tfloat = mat.getTexPostSaturationOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST HUE
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
			tfloat = mat.getTexPostHueOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST RED
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
			tfloat = mat.getTexPostRedOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST GREEN
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
			tfloat = mat.getTexPostGreenOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST BLUE
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
			tfloat = mat.getTexPostBlueOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST GAMMA
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
			tfloat = mat.getTexPostGammaOpacity();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST INVERT
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
			tint = mat.getTexPostInvertOpacity() ? 1 : 0; 
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();
	isave->EndChunk();
	// END TEX POST OPACITY

	// TEX DISPLACEMENT POST
	isave->BeginChunk(NOX_MTL_CHUNK_TEX_POST_DISPLACEMENT);
		// TEX POST BRIGHTNESS
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
			tfloat = mat.getTexPostBrightnessDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST CONTRAST
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
			tfloat = mat.getTexPostContrastDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST SATURATION
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
			tfloat = mat.getTexPostSaturationDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST HUE
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
			tfloat = mat.getTexPostHueDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST RED
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
			tfloat = mat.getTexPostRedDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST GREEN
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
			tfloat = mat.getTexPostGreenDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST BLUE
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
			tfloat = mat.getTexPostBlueDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST GAMMA
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
			tfloat = mat.getTexPostGammaDisplacement();
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();
		// TEX POST INVERT
		isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
			tint = mat.getTexPostInvertDisplacement() ? 1 : 0; 
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();
	isave->EndChunk();
	// END TEX POST DISPLACEMENT

	// PREVIEW
	if (mat.getPreviewExists())
	{
		isave->BeginChunk(NOX_MTL_CHUNK_PREVIEW);
			// PREVIEW WIDTH
			isave->BeginChunk(NOX_MTL_CHUNK_PREVIEW_X);
				tint = mat.getPreviewWidth();
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// PREVIEW HEIGHT
			isave->BeginChunk(NOX_MTL_CHUNK_PREVIEW_Y);
				tint = mat.getPreviewHeight();
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			
			// PREVIEW BUFFER
			isave->BeginChunk(NOX_MTL_CHUNK_PREVIEW_BUFFER);
				int bs = mat.getPreviewBufSize();
				void * buf = mat.getPreviewBuffer();
				#ifdef ISMAX2013
					res = isave->WriteVoid(buf, bs, &wr);
				#else
					res = isave->Write(buf, bs, &wr);
				#endif
				free(buf);
				SCHECK(res);
			isave->EndChunk();
		isave->EndChunk();
	}

	int numLayers = mat.getLayersNumber();
	for (int i=0; i< numLayers; i++)
	{
		// LAYER
		isave->BeginChunk(NOX_MTL_CHUNK_LAYER);
			// LAYER TYPE
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TYPE);
				tint = mat.getLayerType(i);
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER CONTRIBUTION
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_CONTRIBUTION);
				tint = mat.getLayerContribution(i);
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER EMISSION COLOR
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_EMISSION_COLOR);
				mat.getLayerEmissionColor(i, t4float[0], t4float[1], t4float[2], t4float[3]);
				res = isave->Write(t4float, 4*sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER EMISSION POWER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_EMISSION_POWER);
				tfloat = mat.getLayerEmissionPower(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER EMISSION TEMPERATURE
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_EMISSION_TEMPERATURE);
				tint = mat.getLayerEmissionTemperature(i);
				res = isave->Write(&tint , sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER EMISSION UNIT
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_EMISSION_UNIT);
				tint = mat.getLayerEmissionUnit(i);
				res = isave->Write(&tint , sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER EMISSION IES
			if (char * c = mat.getLayerEmissionIes(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_EMISSION_IES);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER ROUGHNESS
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_ROUGHNESS);
				tfloat = mat.getLayerRoughness(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER REFLECTION 0
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_REFL0);
				mat.getLayerColor0(i, t4float[0], t4float[1], t4float[2], t4float[3]);
				res = isave->Write(t4float, 4*sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER REFLECTION 90
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_REFL90);
				mat.getLayerColor90(i, t4float[0], t4float[1], t4float[2], t4float[3]);
				res = isave->Write(t4float, 4*sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER CONNNECT 90
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_CONNECT90);
				if (mat.getLayerConnect90(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER ANISO
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_ANISO);
				tfloat = mat.getLayerAniso(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER ANISO ANGLE
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_ANISO_ANGLE);
				tfloat = mat.getLayerAnisoAngle(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER BRDF TYPE
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_BRDF_TYPE);
				tint = mat.getLayerBrdfType(i);
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TRANSMISSION ON
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TRANSMISSION_ON);
				if (mat.getLayerTransmOn(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TRANSMISSION COLOR
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TRANSMISSION_COLOR);
				mat.getLayerTransmColor(i, t4float[0], t4float[1], t4float[2], t4float[3]);
				res = isave->Write(t4float, 4*sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER FAKEGLASS ON
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_FAKE_GLASS);
				if (mat.getLayerFakeGlass(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER ABSORPTION ON
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_ABSORPTION_ON);
				if (mat.getLayerAbsOn(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER ABSORPTION COLOR
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_ABSORPTION_COLOR);
				mat.getLayerAbsColor(i, t4float[0], t4float[1], t4float[2], t4float[3]);
				res = isave->Write(t4float, 4*sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER ABSORPTION DISTANCE
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_ABSORPTION_DISTANCE);
				tfloat = mat.getLayerAbsDist(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER FRESNEL IOR
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_FRESNEL_IOR);
				tfloat = mat.getLayerFresnelIOR(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER DISPERSION ON
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_DISPERSION_ON);
				if (mat.getLayerDispersionOn(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER DISPERSION VALUE
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_DISPERSION_VALUE);
				tfloat = mat.getLayerDispersionValue(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER SSS ON
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_SSS_ON);
				if (mat.getLayerSSSOn(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER SSS DENSITY
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_SSS_DENSITY);
				tfloat = mat.getLayerSSSDensity(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER SSS COLLISION DIRECTION
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_SSS_COLL_DIR);
				tfloat = mat.getLayerSSSCollDirection(i);
				res = isave->Write(&tfloat, sizeof(float), &wr);
				SCHECK(res);
			isave->EndChunk();

			// LAYER TEX REFL0
			if (char * c = mat.getLayerTexRefl0(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_REFL0);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX REFL0
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_REFL0);
				if (mat.getLayerUseTexRefl0(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX REFL90
			if (char * c = mat.getLayerTexRefl90(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_REFL90);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX REFL90
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_REFL90);
				if (mat.getLayerUseTexRefl90(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX TRANSMISSION
			if (char * c = mat.getLayerTexTransm(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_TRANSMISSION);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX TRANSM
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_TRANSMISSION);
				if (mat.getLayerUseTexTransm(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX ANISO
			if (char * c = mat.getLayerTexAniso(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_ANISO);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX ANISO
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_ANISO);
				if (mat.getLayerUseTexAniso(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX ANISO ANGLE
			if (char * c = mat.getLayerTexAnisoAngle(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_ANISO_ANGLE);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX ANISO ANGLE
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_ANISO_ANGLE);
				if (mat.getLayerUseTexAnisoAngle(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX ROUGH
			if (char * c = mat.getLayerTexRough(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_ROUGH);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX ROUGH
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_ROUGH);
				if (mat.getLayerUseTexRough(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX WEIGHT
			if (char * c = mat.getLayerTexWeight(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_WEIGHT);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX WEIGHT
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_WEIGHT);
				if (mat.getLayerUseTexWeight(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX LIGHT
			if (char * c = mat.getLayerTexLight(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_LIGHT);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX LIGHT
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_LIGHT);
				if (mat.getLayerUseTexLight(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// LAYER TEX NORMAL
			if (char * c = mat.getLayerTexNormal(i))
			{
				isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_NORMAL);
				res = isave->WriteCString(c);
				SCHECK(res);
				isave->EndChunk();
			}
			// LAYER USE TEX NORMAL
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_USE_TEX_NORMAL);
				if (mat.getLayerUseTexNormal(i))
					tint = 1;
				else
					tint = 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// TEX LAYER REFL0 FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_REFL0);
				tint = mat.getLayerTexPostFilterRefl0(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// TEX LAYER REFL0 POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_REFL0);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaRefl0(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertRefl0(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST REFL0


			// TEX LAYER REFL90 FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_REFL90);
				tint = mat.getLayerTexPostFilterRefl90(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// TEX LAYER REFL90 POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_REFL90);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaRefl90(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertRefl90(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST REFL90


			// TEX LAYER TRANSMISSION FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_TRANSMISSION);
				tint = mat.getLayerTexPostFilterTransm(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// TEX LAYER TRANSMISSION POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_TRANSMISSION);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaTransm(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertTransm(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST TRANSMISSION

			// TEX LAYER ANISO FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_ANIZO);
				tint = mat.getLayerTexPostFilterAniso(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// TEX LAYER ANISO POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_ANISO);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaAniso(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertAniso(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST ANISO

			// TEX LAYER ANISO ANGLE FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_ANIZO_ANGLE);
				tint = mat.getLayerTexPostFilterAnisoAngle(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();
			// TEX LAYER ANISO ANGLE POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_ANISO_ANGLE);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaAnisoAngle(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertAnisoAngle(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST ANISO

			// TEX LAYER ROUGH FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_ROUGH);
				tint = mat.getLayerTexPostFilterRough(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// TEX LAYER ROUGH POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_ROUGH);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaRough(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertRough(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST ROUGH


			// TEX LAYER WEIGHT FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_WEIGHT);
				tint = mat.getLayerTexPostFilterWeight(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// TEX LAYER WEIGHT POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_WEIGHT);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaWeight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertWeight(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST WEIGHT


			// TEX LAYER LIGHT FILTER
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_FILTER_LIGHT);
				tint = mat.getLayerTexPostFilterLight(i) ? 1 : 0;
				res = isave->Write(&tint, sizeof(int), &wr);
				SCHECK(res);
			isave->EndChunk();

			// TEX LAYER LIGHT POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_LIGHT);
				// TEX POST BRIGHTNESS
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS);
					tfloat = mat.getLayerTexPostBrightnessLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST CONTRAST
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST);
					tfloat = mat.getLayerTexPostContrastLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST SATURATION
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_SATURATION);
					tfloat = mat.getLayerTexPostSaturationLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST HUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_HUE);
					tfloat = mat.getLayerTexPostHueLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST RED
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_RED);
					tfloat = mat.getLayerTexPostRedLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GREEN
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GREEN);
					tfloat = mat.getLayerTexPostGreenLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST BLUE
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_BLUE);
					tfloat = mat.getLayerTexPostBlueLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST GAMMA
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_GAMMA);
					tfloat = mat.getLayerTexPostGammaLight(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX POST INVERT
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POST_INVERT);
					tint = mat.getLayerTexPostInvertLight(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST LIGHT

			// TEX LAYER NORMAL POST
			isave->BeginChunk(NOX_MTL_CHUNK_LAYER_TEX_POST_NORMAL);
				// TEX NORMAL POST POWER
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POSTNORM_POWER);
					tfloat = mat.getLayerTexPostNormalPower(i);
					res = isave->Write(&tfloat, sizeof(float), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX NORMAL POST INVERT X
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POSTNORM_INVERT_X);
					tint = mat.getLayerTexPostNormalInvertX(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
				// TEX NORMAL POST INVERT Y
				isave->BeginChunk(NOX_MTL_CHUNK_TEXTURE_POSTNORM_INVERT_Y);
					tint = mat.getLayerTexPostNormalInvertY(i) ? 1 : 0; 
					res = isave->Write(&tint, sizeof(int), &wr);
					SCHECK(res);
				isave->EndChunk();
			isave->EndChunk();
			// END TEX LAYER POST NORMAL

		isave->EndChunk();	// end material layer
	}

		// VIEWPORT TEXTURE LAYER
		isave->BeginChunk(NOX_MTL_CHUNK_VP_TEX_LAYER);
			tint = curLayerNumber;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();
		// VIEWPORT TEXTURE
		isave->BeginChunk(NOX_MTL_CHUNK_VP_TEX_TEX);
			tint = curTextureNumber;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

	isave->EndChunk();	// end material

	ADD_LOG_PARTS_MAIN("NOXMaterial::Save done");

	return IO_OK;
}	

//------------------------------------------------------------------------------------------------------------------------

IOResult NOXMaterial::Load(ILoad *iload) 
{
	ADD_LOG_PARTS_MAIN("NOXMaterial::Load");

	IOResult res;
	int id;
	unsigned long rd;
	float tfloat;
	int tint;
	float t4float[4];

	mat.curLay = NULL;
	mat.llay = -1;

	int w1 = 0;
	int h1 = 0;
	void * buf = NULL;
	int bs = 0;

	iload->RegisterPostLoadCallback(new NOXMatPostLoad(this));

	while (IO_OK == (res=iload->OpenChunk())) 
	{
		switch(id = iload->CurChunkID())  
		{
			case NOX_MTL_CHUNK_MATERIAL:
				while (IO_OK == (res=iload->OpenChunk()))
				{
					switch(id = iload->CurChunkID())  
					{
						// MATERIAL NAME
						case NOX_MTL_CHUNK_MATNAME:
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp)
							{
								mat.setName(ctemp);
								MSTR newname;
								#ifdef ISMAX2013
									newname = MSTR::FromACP(ctemp, strlen(ctemp));
								#else
									newname = ctemp;
								#endif
								SetName(newname);
							}
						break;

						case NOX_MTL_CHUNK_SKYPORTAL:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setSkyportal(tint!=0);
						break;

						case NOX_MTL_CHUNK_MATTE_SHADOW:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setMatteShadow(tint!=0);
						break;

						
						// MATERIAL BLEND LAYER NUMBER
						case NOX_MTL_CHUNK_BLEND_NUMBER:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setBlendLayerNumber(tint);
						break;

						// VIEWPORT TEXTURE LAYER
						case NOX_MTL_CHUNK_VP_TEX_LAYER:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							curLayerNumber = tint;
						break;

						// VIEWPORT TEXTURE
						case NOX_MTL_CHUNK_VP_TEX_TEX:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							curTextureNumber = tint;
						break;

						// OPACITY
						case NOX_MTL_CHUNK_OPACITY:
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setOpacity(tfloat);
						break;

						// USE TEX OPACITY
						case NOX_MTL_CHUNK_USE_TEX_OPACITY:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setUseTexOpacity(tint!=0);
						break;

						// TEX OPACITY
						case NOX_MTL_CHUNK_TEX_OPACITY:
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp)
								mat.setTexOpacity(ctemp);
						break;

						// USE TEX DISPLACEMENT
						case NOX_MTL_CHUNK_USE_TEX_DISPLACEMENT:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setUseTexDisplacement(tint!=0);
						break;

						// TEX DISPLACEMENT
						case NOX_MTL_CHUNK_TEX_DISPLACEMENT:
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp)
								mat.setTexDisplacement(ctemp);
						break;

						// DISPLACEMENT DEPTH
						case NOX_MTL_CHUNK_DISPLACEMENT_DEPTH:
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setDisplacementDepth(tfloat);
						break;

						// DISPLACEMENT SUBDIVS
						case NOX_MTL_CHUNK_DISPLACEMENT_SUBDIVS:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setDisplacementSubdivs(tint);
						break;

						// DISPLACEMENT SHIFT
						case NOX_MTL_CHUNK_DISPLACEMENT_SHIFT:
							res = iload->Read(&tfloat, sizeof(float), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setDisplacementShift(tfloat);
						break;

						// DISPLACEMENT NORMAL MODE
						case NOX_MTL_CHUNK_DISPLACEMENT_NORMAL_MODE:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setDisplacementNormalMode(tint);
						break;

						// DISPLACEMENT CONTINUITY
						case NOX_MTL_CHUNK_DISPLACEMENT_CONTINUITY:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setDisplacementContinuity(tint!=0);
						break;

						// DISPLACEMENT IGNORE SCALE
						case NOX_MTL_CHUNK_DISPLACEMENT_IGNORE_SCALE:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setDisplacementIgnoreScale(tint!=0);
						break;

						// TEX FILTER OPACITY 
						case NOX_MTL_CHUNK_TEX_FILTER_OPACITY:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setTexFilterOpacity(tint!=0);
						break;
						// TEX PRECALCULATE OPACITY 
						case NOX_MTL_CHUNK_TEX_PRECALC_OPACITY:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setTexPrecalculateOpacity(tint!=0);
						break;
						// TEX POST OPACITY
						case NOX_MTL_CHUNK_TEX_POST_OPACITY:
							while (IO_OK == (res=iload->OpenChunk()))
							{
								switch(id = iload->CurChunkID())  
								{
									// TEX POST BRIGHTNESS
									case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostBrightnessOpacity(tfloat);
									break;

									// TEX POST CONTRAST
									case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostContrastOpacity(tfloat);
									break;

									// TEX POST SATURATION
									case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostSaturationOpacity(tfloat);
									break;

									// TEX POST HUE
									case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostHueOpacity(tfloat);
									break;

									// TEX POST RED
									case NOX_MTL_CHUNK_TEXTURE_POST_RED:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostRedOpacity(tfloat);
									break;

									// TEX POST GREEN
									case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostGreenOpacity(tfloat);
									break;

									// TEX POST BLUE
									case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostBlueOpacity(tfloat);
									break;

									// TEX POST GAMMA
									case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostGammaOpacity(tfloat);
									break;

									// TEX POST INVERT
									case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostInvertOpacity(tint!=0);
									break;
								}
								iload->CloseChunk();
							}	// end post opacity while
						break;

						// TEX FILTER DISPLACEMENT 
						case NOX_MTL_CHUNK_TEX_FILTER_DISPLACEMENT:
							res = iload->Read(&tint, sizeof(int), &rd);
							if (res!=IO_OK) 
								return res;	
							mat.setTexFilterDisplacement(tint!=0);
						break;

						// TEX POST DISPLACEMENT
						case NOX_MTL_CHUNK_TEX_POST_DISPLACEMENT:
							while (IO_OK == (res=iload->OpenChunk()))
							{
								switch(id = iload->CurChunkID())  
								{
									// TEX POST BRIGHTNESS
									case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostBrightnessDisplacement(tfloat);
									break;

									// TEX POST CONTRAST
									case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostContrastDisplacement(tfloat);
									break;

									// TEX POST SATURATION
									case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostSaturationDisplacement(tfloat);
									break;

									// TEX POST HUE
									case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostHueDisplacement(tfloat);
									break;

									// TEX POST RED
									case NOX_MTL_CHUNK_TEXTURE_POST_RED:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostRedDisplacement(tfloat);
									break;

									// TEX POST GREEN
									case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostGreenDisplacement(tfloat);
									break;

									// TEX POST BLUE
									case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostBlueDisplacement(tfloat);
									break;

									// TEX POST GAMMA
									case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostGammaDisplacement(tfloat);
									break;

									// TEX POST INVERT
									case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setTexPostInvertDisplacement(tint!=0);
									break;
								}
								iload->CloseChunk();
							}	// end post displacement while
						break;




						// PREVIEW
						case NOX_MTL_CHUNK_PREVIEW:
							while (IO_OK == (res=iload->OpenChunk()))
							{
								switch(id = iload->CurChunkID())  
								{
									// PREVIEW WIDTH
									case NOX_MTL_CHUNK_PREVIEW_X:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										w1 = tint;
									break;

									// PREVIEW HEIGHT
									case NOX_MTL_CHUNK_PREVIEW_Y:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										h1 = tint;
									break;

									// PREVIEW BUFFER
									case NOX_MTL_CHUNK_PREVIEW_BUFFER:
										bs = (int)iload->CurChunkLength();
										if (bs < 1)
											break;

										buf = malloc(bs);
										if (!buf)
											break;
										#ifdef ISMAX2013
											res = iload->ReadVoid(buf, bs, &rd);
										#else
											res = iload->Read(buf, bs, &rd);
										#endif
										if (res!=IO_OK) 
											return res;

										if (rd != bs)
										{
											free(buf);
											buf = NULL;
											break;
										}
									break;
								}
								iload->CloseChunk();
							}	// end preview while
							if (w1>0  &&  h1>0  &&  buf)
							{
								mat.setNewPreview(w1,h1, (unsigned char *)buf);
								int bs1 = mat.getPreviewBufSize();
								void * mbuf = mat.getPreviewBuffer();
								if (bs1 != bs   ||   !mbuf)
								{
									free(buf);
									buf = NULL;
								}
								memcpy(mbuf, buf, bs);
								free(buf);
								buf = NULL;
							}

						break;

						// LAYER
						case NOX_MTL_CHUNK_LAYER:
							mat.addLayer();
							while (IO_OK == (res=iload->OpenChunk()))
							{
								char * ctemp;
								switch(id = iload->CurChunkID())  
								{
									// LAYER TYPE
									case NOX_MTL_CHUNK_LAYER_TYPE:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerType(tint);
									break;
									// LAYER CONTRIBUTION
									case NOX_MTL_CHUNK_LAYER_CONTRIBUTION:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerContribution(tint);
									break;
									// LAYER EMISSION COLOR
									case NOX_MTL_CHUNK_LAYER_EMISSION_COLOR:
										#ifdef ISMAX2013
											res = iload->ReadVoid(&t4float, 4*sizeof(float), &rd);
										#else
											res = iload->Read(&t4float, 4*sizeof(float), &rd);
										#endif
										if (res!=IO_OK) 
											return res;	
										mat.setLayerEmissionColor(t4float[0], t4float[1], t4float[2], t4float[3]);
									break;
									// LAYER EMISSION POWER
									case NOX_MTL_CHUNK_LAYER_EMISSION_POWER:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerEmissionPower(tfloat);
									break;
									// LAYER EMISSION TEMPERATURE
									case NOX_MTL_CHUNK_LAYER_EMISSION_TEMPERATURE:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerEmissionTemperature(tint);
									break;
									// LAYER EMISSION UNIT
									case NOX_MTL_CHUNK_LAYER_EMISSION_UNIT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerEmissionUnit(tint);
									break;
									// LAYER EMISSION IES
									case NOX_MTL_CHUNK_LAYER_EMISSION_IES:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerEmissionIes(ctemp);
									break;
									// LAYER ROUGHNESS
									case NOX_MTL_CHUNK_LAYER_ROUGHNESS:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerRoughness(tfloat);
									break;
									// LAYER REFLECTION 0
									case NOX_MTL_CHUNK_LAYER_REFL0:
										#ifdef ISMAX2013
											res = iload->ReadVoid(&t4float, 4*sizeof(float), &rd);
										#else
											res = iload->Read(&t4float, 4*sizeof(float), &rd);
										#endif
										if (res!=IO_OK) 
											return res;	
										mat.setLayerColor0(t4float[0], t4float[1], t4float[2], t4float[3]);
									break;
									// LAYER REFLECTION 90
									case NOX_MTL_CHUNK_LAYER_REFL90:
										#ifdef ISMAX2013
											res = iload->ReadVoid(&t4float, 4*sizeof(float), &rd);
										#else
											res = iload->Read(&t4float, 4*sizeof(float), &rd);
										#endif
										if (res!=IO_OK) 
											return res;	
										mat.setLayerColor90(t4float[0], t4float[1], t4float[2], t4float[3]);
									break;
									// LAYER CONNECT 90
									case NOX_MTL_CHUNK_LAYER_CONNECT90:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerConnect90(tint);
									break;
									// LAYER ANISO
									case NOX_MTL_CHUNK_LAYER_ANISO:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerAniso(tfloat);
									break;
									// LAYER ANISO ANGLE
									case NOX_MTL_CHUNK_LAYER_ANISO_ANGLE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerAnisoAngle(tfloat);
									break;
									// LAYER BRDF TYPE
									case NOX_MTL_CHUNK_LAYER_BRDF_TYPE:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerBrdfType(tint);
									break;
									// LAYER TRANSMISSION ON
									case NOX_MTL_CHUNK_LAYER_TRANSMISSION_ON:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTransmOn(tint);
									break;
									// LAYER TRANSMISSION COLOR
									case NOX_MTL_CHUNK_LAYER_TRANSMISSION_COLOR:
										#ifdef ISMAX2013
											res = iload->ReadVoid(&t4float, 4*sizeof(float), &rd);
										#else
											res = iload->Read(&t4float, 4*sizeof(float), &rd);
										#endif
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTransmColor(t4float[0], t4float[1], t4float[2], t4float[3]);
									break;
									// LAYER FAKE GLASS ON
									case NOX_MTL_CHUNK_LAYER_FAKE_GLASS:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerFakeGlass(tint);
									break;
									// LAYER ABSORPTION ON
									case NOX_MTL_CHUNK_LAYER_ABSORPTION_ON:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerAbsOn(tint);
									break;
									// LAYER ABSORPTION COLOR
									case NOX_MTL_CHUNK_LAYER_ABSORPTION_COLOR:
										#ifdef ISMAX2013										
											res = iload->ReadVoid(&t4float, 4*sizeof(float), &rd);
										#else
											res = iload->Read(&t4float, 4*sizeof(float), &rd);
										#endif
										if (res!=IO_OK) 
											return res;	
										mat.setLayerAbsColor(t4float[0], t4float[1], t4float[2], t4float[3]);
									break;
									// LAYER ABSORPTION DISTANCE
									case NOX_MTL_CHUNK_LAYER_ABSORPTION_DISTANCE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerAbsDist(tfloat);
									break;
									// LAYER FRESNEL IOR
									case NOX_MTL_CHUNK_LAYER_FRESNEL_IOR:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerFresnelIOR(tfloat);
									break;
									// LAYER DISPERSION VALUE
									case NOX_MTL_CHUNK_LAYER_DISPERSION_VALUE:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerDispersionValue(tfloat);
									break;
									// LAYER DISPERSION ON
									case NOX_MTL_CHUNK_LAYER_DISPERSION_ON:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerDispersionOn(tint);
									break;
									// LAYER DISPERSION ON
									case NOX_MTL_CHUNK_LAYER_SSS_ON:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerSSSOn(tint);
									break;
									// LAYER SSS DENSITY
									case NOX_MTL_CHUNK_LAYER_SSS_DENSITY:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerSSSDensity(tfloat);
									break;
									// LAYER SSS COLLISION DIRECTION
									case NOX_MTL_CHUNK_LAYER_SSS_COLL_DIR:
										res = iload->Read(&tfloat, sizeof(float), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerSSSCollDirection(tfloat);
									break;

									// LAYER USE TEX REFL0
									case NOX_MTL_CHUNK_LAYER_USE_TEX_REFL0:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexRefl0(tint!=0);
									break;

									// LAYER TEX REFL0
									case NOX_MTL_CHUNK_LAYER_TEX_REFL0:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexRefl0(ctemp);
									break;

									// LAYER USE TEX REFL90
									case NOX_MTL_CHUNK_LAYER_USE_TEX_REFL90:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexRefl90(tint!=0);
									break;

									// LAYER TEX REFL90
									case NOX_MTL_CHUNK_LAYER_TEX_REFL90:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexRefl90(ctemp);
									break;

									// LAYER USE TEX TRANSMISSION
									case NOX_MTL_CHUNK_LAYER_USE_TEX_TRANSMISSION:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexTransm(tint!=0);
									break;

									// LAYER TEX TRANSMISSION
									case NOX_MTL_CHUNK_LAYER_TEX_TRANSMISSION:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexTransm(ctemp);
									break;

									// LAYER USE TEX ANISO
									case NOX_MTL_CHUNK_LAYER_USE_TEX_ANISO:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexAniso(tint!=0);
									break;

									// LAYER TEX ANISO
									case NOX_MTL_CHUNK_LAYER_TEX_ANISO:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexAniso(ctemp);
									break;

									// LAYER USE TEX ANISO ANGLE
									case NOX_MTL_CHUNK_LAYER_USE_TEX_ANISO_ANGLE:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexAnisoAngle(tint!=0);
									break;

									// LAYER TEX ANISO ANGLE
									case NOX_MTL_CHUNK_LAYER_TEX_ANISO_ANGLE:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexAnisoAngle(ctemp);
									break;


									// LAYER USE TEX ROUGH
									case NOX_MTL_CHUNK_LAYER_USE_TEX_ROUGH:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexRough(tint!=0);
									break;

									// LAYER TEX ROUGH
									case NOX_MTL_CHUNK_LAYER_TEX_ROUGH:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexRough(ctemp);
									break;

									// LAYER USE TEX WEIGHT
									case NOX_MTL_CHUNK_LAYER_USE_TEX_WEIGHT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexWeight(tint!=0);
									break;

									// LAYER TEX WEIGHT
									case NOX_MTL_CHUNK_LAYER_TEX_WEIGHT:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexWeight(ctemp);
									break;

									// LAYER USE TEX LIGHT
									case NOX_MTL_CHUNK_LAYER_USE_TEX_LIGHT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexLight(tint!=0);
									break;

									// LAYER TEX LIGHT
									case NOX_MTL_CHUNK_LAYER_TEX_LIGHT:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexLight(ctemp);
									break;

									// LAYER USE TEX NORMAL
									case NOX_MTL_CHUNK_LAYER_USE_TEX_NORMAL:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerUseTexNormal(tint!=0);
									break;

									// LAYER TEX NORMAL
									case NOX_MTL_CHUNK_LAYER_TEX_NORMAL:
										res = iload->ReadCStringChunk(&ctemp);
										if (res!=IO_OK) 
											return res;	
										if (ctemp)
											mat.setLayerTexNormal(ctemp);
									break;


									// TEX FILTER REFL0 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_REFL0:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterRefl0(tint!=0);
									break;
									// TEX PRECALCULATE REFL0
									case NOX_MTL_CHUNK_LAYER_TEX_PRECALC_REFL0:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexPrecalculateRefl0(tint!=0);
									break;
									// TEX POST REFL0
									case NOX_MTL_CHUNK_LAYER_TEX_POST_REFL0:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessRefl0(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastRefl0(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationRefl0(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueRefl0(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedRefl0(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenRefl0(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueRefl0(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaRefl0(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertRefl0(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;


									// TEX FILTER REFL90 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_REFL90:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterRefl90(tint!=0);
									break;
									// TEX PRECALCULATE REFL90
									case NOX_MTL_CHUNK_LAYER_TEX_PRECALC_REFL90:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexPrecalculateRefl90(tint!=0);
									break;
									// TEX POST REFL90
									case NOX_MTL_CHUNK_LAYER_TEX_POST_REFL90:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessRefl90(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastRefl90(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationRefl90(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueRefl90(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedRefl90(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenRefl90(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueRefl90(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaRefl90(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertRefl90(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;


									// TEX FILTER TRANSMISSION 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_TRANSMISSION:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterTransm(tint!=0);
									break;
									// TEX POST REFL0
									case NOX_MTL_CHUNK_LAYER_TEX_POST_TRANSMISSION:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessTransm(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastTransm(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationTransm(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueTransm(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedTransm(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenTransm(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueTransm(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaTransm(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertTransm(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;


									// TEX FILTER ANISO 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_ANIZO:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterAniso(tint!=0);
									break;
									// TEX POST ANISO
									case NOX_MTL_CHUNK_LAYER_TEX_POST_ANISO:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessAniso(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastAniso(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationAniso(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueAniso(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedAniso(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenAniso(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueAniso(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaAniso(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertAniso(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;


									// TEX FILTER ANISO 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_ANIZO_ANGLE:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterAnisoAngle(tint!=0);
									break;
									// TEX POST ANISO
									case NOX_MTL_CHUNK_LAYER_TEX_POST_ANISO_ANGLE:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessAnisoAngle(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastAnisoAngle(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationAnisoAngle(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueAnisoAngle(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedAnisoAngle(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenAnisoAngle(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueAnisoAngle(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaAnisoAngle(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertAnisoAngle(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;


									// TEX FILTER ROUGH 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_ROUGH:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterRough(tint!=0);
									break;
									// TEX PRECALCULATE ROUGH
									case NOX_MTL_CHUNK_LAYER_TEX_PRECALC_ROUGH:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexPrecalculateRough(tint!=0);
									break;
									// TEX POST ROUGH
									case NOX_MTL_CHUNK_LAYER_TEX_POST_ROUGH:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessRough(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastRough(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationRough(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueRough(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedRough(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenRough(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueRough(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaRough(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertRough(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;


									// TEX FILTER WEIGHT 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_WEIGHT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterWeight(tint!=0);
									break;
									// TEX PRECALCULATE WEIGHT
									case NOX_MTL_CHUNK_LAYER_TEX_PRECALC_WEIGHT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexPrecalculateWeight(tint!=0);
									break;
									// TEX POST WEIGHT
									case NOX_MTL_CHUNK_LAYER_TEX_POST_WEIGHT:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessWeight(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastWeight(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationWeight(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueWeight(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedWeight(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenWeight(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueWeight(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaWeight(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertWeight(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;


									// TEX FILTER LIGHT 
									case NOX_MTL_CHUNK_LAYER_TEX_FILTER_LIGHT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexFilterLight(tint!=0);
									break;
									// TEX PRECALCULATE LIGHT
									case NOX_MTL_CHUNK_LAYER_TEX_PRECALC_LIGHT:
										res = iload->Read(&tint, sizeof(int), &rd);
										if (res!=IO_OK) 
											return res;	
										mat.setLayerTexPrecalculateLight(tint!=0);
									break;
									// TEX POST LIGHT
									case NOX_MTL_CHUNK_LAYER_TEX_POST_LIGHT:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX POST BRIGHTNESS
												case NOX_MTL_CHUNK_TEXTURE_POST_BRIGHTNESS:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBrightnessLight(tfloat);
												break;

												// TEX POST CONTRAST
												case NOX_MTL_CHUNK_TEXTURE_POST_CONTRAST:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostContrastLight(tfloat);
												break;

												// TEX POST SATURATION
												case NOX_MTL_CHUNK_TEXTURE_POST_SATURATION:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostSaturationLight(tfloat);
												break;

												// TEX POST HUE
												case NOX_MTL_CHUNK_TEXTURE_POST_HUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostHueLight(tfloat);
												break;

												// TEX POST RED
												case NOX_MTL_CHUNK_TEXTURE_POST_RED:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostRedLight(tfloat);
												break;

												// TEX POST GREEN
												case NOX_MTL_CHUNK_TEXTURE_POST_GREEN:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGreenLight(tfloat);
												break;

												// TEX POST BLUE
												case NOX_MTL_CHUNK_TEXTURE_POST_BLUE:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostBlueLight(tfloat);
												break;

												// TEX POST GAMMA
												case NOX_MTL_CHUNK_TEXTURE_POST_GAMMA:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostGammaLight(tfloat);
												break;

												// TEX POST INVERT
												case NOX_MTL_CHUNK_TEXTURE_POST_INVERT:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostInvertLight(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;

									// TEX POST NORMAL
									case NOX_MTL_CHUNK_LAYER_TEX_POST_NORMAL:
										while (IO_OK == (res=iload->OpenChunk()))
										{
											switch(id = iload->CurChunkID())  
											{
												// TEX NORMAL POST POWER
												case NOX_MTL_CHUNK_TEXTURE_POSTNORM_POWER:
													res = iload->Read(&tfloat, sizeof(float), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostNormalPower(tfloat);
												break;
												// TEX NORMAL POST INVERT X
												case NOX_MTL_CHUNK_TEXTURE_POSTNORM_INVERT_X:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostNormalInvertX(tint!=0);
												break;
												// TEX NORMAL POST INVERT Y
												case NOX_MTL_CHUNK_TEXTURE_POSTNORM_INVERT_Y:
													res = iload->Read(&tint, sizeof(int), &rd);
													if (res!=IO_OK) 
														return res;	
													mat.setLayerTexPostNormalInvertY(tint!=0);
												break;
											}
											iload->CloseChunk();
										}	// end tex post
									break;

								}
								iload->CloseChunk();
							}	// end while in chunk layer
						break;
					}
					iload->CloseChunk();
				}	// end while in chunk material
			break;
		}
		iload->CloseChunk();

	}

	ADD_LOG_PARTS_MAIN("NOXMaterial::Load done");

	return IO_OK;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXMatPostLoad::proc(ILoad * iload)
{
	mat->getTexturesFromNOX();
	mat->changeTexture(mat->curTextureNumber, mat->curLayerNumber);
}

//------------------------------------------------------------------------------------------------------------------------
