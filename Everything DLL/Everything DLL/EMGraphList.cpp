#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif


#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;

//--------------------------------------------------------------------------------------------------

void InitEMGraphList()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMGraphList";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMGraphListProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = CS_DBLCLKS;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMGraphList *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//--------------------------------------------------------------------------------------------------

EMGraphList * GetEMGraphListInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMGraphList * emlv = (EMGraphList *)GetWindowLongPtr(hwnd, 0);
	#else
		EMGraphList * emlv = (EMGraphList *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emlv;
}

//--------------------------------------------------------------------------------------------------

void SetEMGraphListInstance(HWND hwnd, EMGraphList *emgl)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emgl);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emgl);
	#endif
}

//--------------------------------------------------------------------------------------------------

EMGraphList::EMGraphList(HWND hWnd):
entries(0), ids_sorted(0)
{
	hwnd = hWnd;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	colBackground = RGB(40,40,40);
	colBars = RGB(96,96,96);
	colText = RGB(207,207,207);
	colBarBgUp = RGB(37,37,37);
	colBarBgDown = RGB(47,47,47);

	colDisText = RGB(127,123,121);
	colDisBarUp = RGB(39,39,39);
	colDisBarDown = RGB(29,29,29);

	
	drawShadowGdiplus = true;
	bgTransparent = false;
	drawNumbers = true;

	paddingVert = 11;
	barHeight = 19;
	bmx = 20;
	bmy = 5;
	bmr = 5;

	clearData();
}

//--------------------------------------------------------------------------------------------------

EMGraphList::~EMGraphList()
{
}

//--------------------------------------------------------------------------------------------------

LRESULT CALLBACK EMGraphListProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMGraphList* emgl;
	emgl = GetEMGraphListInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	static int ly=0;
	static int lsbst=0;

	switch (msg)
	{
		case WM_CREATE:
			{
				EMGraphList * emgl1 = new EMGraphList(hwnd);
				SetEMGraphListInstance(hwnd, emgl1);
			}
			break;
		case WM_DESTROY:
			{
				delete emgl;
				SetEMGraphListInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			DWORD style;
			style = GetWindowLong(hwnd, GWL_STYLE);
			bool disabled = ((style & WS_DISABLED) > 0);
			GetClientRect(hwnd, &rect);

			float maxval = 0.0f;
			for (int i=0; i<emgl->entries.objCount; i++)
				maxval = max(maxval, emgl->entries[i].value);

			// check sorted ids
			bool ids_ok = (emgl->entries.objCount == emgl->ids_sorted.objCount);
			int idsc = emgl->ids_sorted.objCount;
			for (int i=0; i<idsc; i++)
			{
				if (emgl->ids_sorted[i]<0 || emgl->ids_sorted[i]>=idsc)
					ids_ok = false;
			}

			HDC ohdc = BeginPaint(hwnd, &ps);

			//// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			HBITMAP oldBMP = (HBITMAP)SelectObject(hdc, backBMP);

			HBRUSH oldbrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(0xff000000|RGB(255,255,255)));
			HPEN oldpen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID,1,0xff000000|RGB(255,255,255)));
			HFONT oldfont = (HFONT)SelectObject(hdc, emgl->hFont);

			DeleteObject((HBRUSH)SelectObject(hdc, CreateSolidBrush(emgl->colBackground)));
			if (!emgl->bgTransparent)
				Rectangle(hdc, -1, -1, rect.right+1, rect.bottom+1);

			SetBkMode(hdc, TRANSPARENT);
			SetTextColor(hdc, emgl->colText);
			char valbuf[64];
			DeleteObject((HBRUSH)SelectObject(hdc, GetStockObject(NULL_BRUSH)));

			int bgr1 = GetRValue(emgl->colBarBgUp);
			int bgg1 = GetGValue(emgl->colBarBgUp);
			int bgb1 = GetBValue(emgl->colBarBgUp);
			int bgr2 = GetRValue(emgl->colBarBgDown);
			int bgg2 = GetGValue(emgl->colBarBgDown);
			int bgb2 = GetBValue(emgl->colBarBgDown);

			for (int i1=0; i1<emgl->entries.objCount; i1++)
			{
				int ii = ids_ok ? emgl->ids_sorted[i1] : i1;
				int v = i1*(emgl->barHeight+emgl->paddingVert) + emgl->bmy;
				int x = (int)ceil((rect.right-2-emgl->bmx-emgl->bmr) * emgl->entries[ii].value/maxval)+1+emgl->bmx;
				
				COLORREF cbu = disabled ? emgl->colDisBarUp : emgl->entries[ii].colBarUp;
				COLORREF cbd = disabled ? emgl->colDisBarDown : emgl->entries[ii].colBarDown;

				int r1 = GetRValue(cbu);
				int g1 = GetGValue(cbu);
				int b1 = GetBValue(cbu);
				int r2 = GetRValue(cbd);
				int g2 = GetGValue(cbd);
				int b2 = GetBValue(cbd);

				// BAR BACKGROUND
				int hh = emgl->barHeight;
				for (int j=0; j<hh; j++)
				{
					unsigned char r = (bgr2*(j) + bgr1*(hh-j-1))/(hh-1);
					unsigned char g = (bgg2*(j) + bgg1*(hh-j-1))/(hh-1);
					unsigned char b = (bgb2*(j) + bgb1*(hh-j-1))/(hh-1);
					DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(r,g,b))));
					MoveToEx(hdc, emgl->bmx,v+j, NULL);
					LineTo(hdc, rect.right-emgl->bmr, v+j);
				}

				// BAR
				for (int j=0; j<hh; j++)
				{
					unsigned char r = (r2*(j) + r1*(hh-j-1))/(hh-1);
					unsigned char g = (g2*(j) + g1*(hh-j-1))/(hh-1);
					unsigned char b = (b2*(j) + b1*(hh-j-1))/(hh-1);
					DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(r,g,b))));
					MoveToEx(hdc, emgl->bmx,v+j, NULL);
					LineTo(hdc, x+1, v+j);
				}
				if (!disabled)
				{
					DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emgl->entries[ii].colBorder)));
					Rectangle(hdc, emgl->bmx, v, x+1, v+hh);
				}

				SIZE sz;
				int l;
				char * txt = emgl->entries[ii].name;
				if (txt && strlen(txt)>0)
				{
					l = (int)strlen(txt);
					GetTextExtentPoint32(hdc, txt, l, &sz);
					SetTextColor(hdc, disabled ? emgl->colDisText : emgl->entries[ii].colText);
					ExtTextOut(hdc, emgl->bmx+4,v+(emgl->barHeight-sz.cy)/2,  0, NULL, txt, l, 0);
				}

				// value
				sprintf_s(valbuf, 64, "%.1f", emgl->entries[ii].value);
				l = (int)strlen(valbuf);
				GetTextExtentPoint32(hdc, valbuf, l, &sz);
				SetTextColor(hdc, disabled ? emgl->colDisText : emgl->colText);
				ExtTextOut(hdc, rect.right-sz.cx-emgl->bmr-4 ,v+(emgl->barHeight-sz.cy)/2,  0, NULL, valbuf, l, 0);

				// numbers
				if (emgl->drawNumbers)
				{
					sprintf_s(valbuf, 64, "%d.", i1+1);
					l = (int)strlen(valbuf);
					GetTextExtentPoint32(hdc, valbuf, l, &sz);
					SetTextColor(hdc, disabled ? emgl->colDisText : emgl->colText);
					ExtTextOut(hdc, emgl->bmx-sz.cx-4 ,v+(emgl->barHeight-sz.cy)/2,  0, NULL, valbuf, l, 0);
				}
			}

			DeleteObject((HPEN)SelectObject(hdc, oldpen));
			DeleteObject((HBRUSH)SelectObject(hdc, oldbrush));
			SelectObject(hdc, oldfont);


			#define CORRECTION_FOR_OPAQUE2
			#ifdef CORRECTION_FOR_OPAQUE2
			if (emgl->bgTransparent)
			{
				char * ttbuf = (char*)malloc(4 * rect.right * rect.bottom);
				int cx = rect.right;
				int cy = rect.bottom;
				std::vector<COLORREF> pixels(cx * cy);
				BITMAPINFOHEADER bmpInfo = {0};
				bmpInfo.biSize = sizeof(bmpInfo);
				bmpInfo.biWidth = cx;
				bmpInfo.biHeight = -int(cy);
				bmpInfo.biPlanes = 1;
				bmpInfo.biBitCount = 32;
				bmpInfo.biCompression = BI_RGB;
				GetDIBits(hdc, backBMP, 0, cy, &pixels[0], (LPBITMAPINFO)&bmpInfo, DIB_RGB_COLORS);
				for (int i=0; i<cx*cy; i++)
				{
					COLORREF & pixel = pixels[i];
					if(pixel != 0) // black pixels stay transparent
						pixel |= 0xFF000000; // set alpha channel to 100%
				}
				SetDIBits(hdc, backBMP, 0, cy, &pixels[0], (LPBITMAPINFO)&bmpInfo, DIB_RGB_COLORS);
				free(ttbuf);
			}
			#endif

			if (emgl->drawShadowGdiplus  &&  !disabled)	// gdi+ for transparent shadow
			{
				// activate gdi+
				ULONG_PTR gdiplusToken;
				Gdiplus::GdiplusStartupInput gdiplusStartupInput;
				Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
				{
					Gdiplus::Graphics graphics(hdc);
					Gdiplus::Pen s1pen(Gdiplus::Color(48, 0,0,0));
					Gdiplus::Pen s2pen(Gdiplus::Color(12, 0,0,0));
					for (int i1=0; i1<emgl->entries.objCount; i1++)
					{
						int ii = ids_ok ? emgl->ids_sorted[i1] : i1;
						int v = i1*(emgl->barHeight+emgl->paddingVert) + emgl->bmy;
						int x = (int)ceil((rect.right-2-emgl->bmx-emgl->bmr) * emgl->entries[ii].value/maxval)+1+emgl->bmx;
						graphics.DrawRectangle(&s1pen, (int)emgl->bmx-1, (int)v-1, (int)x-emgl->bmx+2, (int)emgl->barHeight+1);
						graphics.DrawRectangle(&s2pen, (int)emgl->bmx-2, (int)v-2, (int)x-emgl->bmx+4, (int)emgl->barHeight+3);
					}
				}	// gdi+ variables destructors here
				// release gdi+
				Gdiplus::GdiplusShutdown(gdiplusToken);
			}

			// copy from back buffer
			GetClientRect(hwnd, &rect);

			if (emgl->bgTransparent)
			{
				BLENDFUNCTION blendFunction;
				blendFunction.BlendOp = AC_SRC_OVER;
				blendFunction.BlendFlags = 0;
				blendFunction.SourceConstantAlpha = 255;
				blendFunction.AlphaFormat = AC_SRC_ALPHA;
				AlphaBlend(ohdc, 0, 0,  rect.right, rect.bottom,  hdc, 0, 0, rect.right, rect.bottom, blendFunction);
			}
			else
			{
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			}

			SelectObject(hdc, oldBMP);
			DeleteObject(backBMP);
			DeleteDC(hdc); 

			EndPaint(hwnd, &ps);
		}	// end WM_PAINT
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//--------------------------------------------------------------------------------------------------

bool EMGraphList::addData(Record r)
{
	entries.add(r);
	entries.createArray();
	int id = entries.objCount-1;
	ids_sorted.add(id);
	ids_sorted.createArray();

	return true;
}

//--------------------------------------------------------------------------------------------------

bool EMGraphList::clearData()
{
	for (int i=0; i<entries.objCount; i++)
	{
		if (entries[i].name)
			free(entries[i].name);
		entries[i].name = NULL;
	}
	entries.freeList();
	entries.destroyArray();
	return true;
}

//--------------------------------------------------------------------------------------------------

bool EMGraphList::sortIDs(bool descending)
{
	if (entries.objCount!=ids_sorted.objCount)
		return false;
	int c = ids_sorted.objCount;
	for (int i=0; i<c; i++)
		if (ids_sorted[i]<0 || ids_sorted[i]>=c)
			return false;
	bool not_sorted = true;
	while (not_sorted)
	{
		not_sorted = false;
		for (int i=0; i<c-1; i++)
		{
			float val1 = entries[ids_sorted[i]].value;
			float val2 = entries[ids_sorted[i+1]].value;
			if ( (descending  &&  (val1<val2))  ||  (!descending  &&  (val1>val2)) )
			{
				not_sorted = true;
				bool ok = ids_sorted.swapElements(i, i+1);
				if (!ok)
					return false;
			}
		}
	}

	return true;
}

//--------------------------------------------------------------------------------------------------

bool EMGraphList::resetSortedIDs()
{
	int c = entries.objCount;
	ids_sorted.freeList();
	for (int i=0; i<c; i++)
		ids_sorted.add(i);
	ids_sorted.createArray();
	return true;
}

//--------------------------------------------------------------------------------------------------

bool EMGraphList::removeLastEntry(bool sort, bool descending)
{
	entries.trimLastObjects(1);
	entries.createArray();
	resetSortedIDs();
	if (sort)
		sortIDs(descending);
	return true;
}

//--------------------------------------------------------------------------------------------------
