#include "raytracer.h"
#include "log.h"
#include <time.h>
#include "myList.h"

// from displacement
bool samePoint(const Point3d &p1, const Point3d &p2);		
bool sameNormal(const Vector3d &n1, const Vector3d &n2);

//#define NO_UV_DIR_SMOOTH
#define USE_UV_BLIST
#define PRINT_UV_STATS
//#define USE_UV_RECURSIVE

struct UVvertex
{
	Point3d pos;
	Vector3d normal;
	Vector3d udir;
	Vector3d vdir;
	UVvertex() { pos=Point3d(0,0,0); normal=Vector3d(0,1,0); udir=vdir=Vector3d(0,0,0); }
};


typedef bList<UVvertex, 8> UVdatablock;
int datablockscount = 0;
// -------------------------- OCTREE VERSION

#define OUVCOUNT 96
struct OctreeUV;
typedef OctreeUV * OctreeUVptr;

struct OctreeUV
{
	OctreeUVptr children[2][2][2];
	#ifdef USE_UV_BLIST
		UVdatablock data;
	#else
		UVvertex * data;
	#endif

	float bmin[3];
	float bmax[3];
	int numData;
	int isLeaf;

	bool createChildren(bool moveData);
	int  addItemNonRecursive(const UVvertex &uvw);
	bool addItem(const UVvertex &uvw, bool compareExisting);
	void deleteChildren();
	void normalizeShit();
	UVvertex * findData(UVvertex * orig);
	UVvertex * findDataNonRecursive(UVvertex * orig);

	OctreeUV();
	~OctreeUV();
};

OctreeUV::OctreeUV() 
#ifdef USE_UV_BLIST
	: data(16)
#endif
{ 
	children[0][0][0] = NULL;
	children[0][0][1] = NULL;
	children[0][1][0] = NULL;
	children[0][1][1] = NULL;
	children[1][0][0] = NULL;
	children[1][0][1] = NULL;
	children[1][1][0] = NULL;
	children[1][1][1] = NULL;
	isLeaf = true;
	numData = 0;
	#ifndef USE_UV_BLIST
		data = new UVvertex[OUVCOUNT];
	#endif
	datablockscount++;
}

OctreeUV::~OctreeUV()
{
	deleteChildren();
	#ifdef USE_UV_BLIST
		data.freeList();
		data.destroyArray();
	#else
		if (data)
			delete [] data;
		data = NULL;
	#endif
}

void OctreeUV::deleteChildren()
{
	for (int i=0; i<2; i++)
		for (int j=0; j<2; j++)
			for (int k=0; k<2; k++)
			{
				if (children[i][j][k])
					delete children[i][j][k];
				children[i][j][k] = NULL;
			}
	isLeaf = true;
	numData = 0;
}

void OctreeUV::normalizeShit()
{
	if (isLeaf)
	{
		for (int i=0; i<numData; i++)
		{
			data[i].udir.normalize();
			data[i].vdir.normalize();
		}
	}
	else
	{
		for (int i=0; i<2; i++)
			for (int j=0; j<2; j++)
				for (int k=0; k<2; k++)
					children[i][j][k]->normalizeShit();
	}
}

bool OctreeUV::createChildren(bool moveData)
{
	float half[3];
	for (int i=0; i<3; i++)
		half[i] = (bmax[i]+bmin[i])/2;
	for (int i=0; i<2; i++)
		for (int j=0; j<2; j++)
			for (int k=0; k<2; k++)
			{
				children[i][j][k] = new OctreeUV();
				children[i][j][k]->bmin[0] = i ? half[0] : bmin[0];
				children[i][j][k]->bmin[1] = j ? half[1] : bmin[1];
				children[i][j][k]->bmin[2] = k ? half[2] : bmin[2];
				children[i][j][k]->bmax[0] = i ? bmax[0] : half[0];
				children[i][j][k]->bmax[1] = j ? bmax[1] : half[1];
				children[i][j][k]->bmax[2] = k ? bmax[2] : half[2];
			}
	if (moveData)
	{
		for (int i=0; i<numData; i++)
		{
			Point3d pos = data[i].pos;
			int x = (pos.x < half[0]) ? 0 : 1;
			int y = (pos.y < half[1]) ? 0 : 1;
			int z = (pos.z < half[2]) ? 0 : 1;
			children[x][y][z]->addItem(data[i], false);
		}
		numData = 0;
		#ifdef USE_UV_BLIST
			data.freeList();
			data.destroyArray();
		#else
			if (data)
				delete [] data;
			data = NULL;
		#endif
		datablockscount--;
	}
	isLeaf = false;
	return true;
}

bool samePointEps(const Point3d &p1, const Point3d &p2, float epsilon)
{
	if (fabs(p1.x-p2.x)>epsilon)
		return false;
	if (fabs(p1.y-p2.y)>epsilon)
		return false;
	if (fabs(p1.z-p2.z)>epsilon)
		return false;
	return true;
}

//---------------------------------------------------------------------

bool sameNormalEps(const Vector3d &n1, const Vector3d &n2, float epsilon)
{
	if (fabs(n1.x-n2.x)>epsilon)
		return false;
	if (fabs(n1.y-n2.y)>epsilon)
		return false;
	if (fabs(n1.z-n2.z)>epsilon)
		return false;
	return true;
}


int OctreeUV::addItemNonRecursive(const UVvertex &uvw)
{
	OctreeUV * node = this;
	int maxlvl = 0;
	while (!node->isLeaf)
	{
		float half[] = {(node->bmax[0]+node->bmin[0])/2, (node->bmax[1]+node->bmin[1])/2, (node->bmax[2]+node->bmin[2])/2};
		int x = (uvw.pos.x < half[0]) ? 0 : 1;
		int y = (uvw.pos.y < half[1]) ? 0 : 1;
		int z = (uvw.pos.z < half[2]) ? 0 : 1;
		node = node->children[x][y][z];
		maxlvl++;

		if (node->isLeaf  &&  node->numData==OUVCOUNT+maxlvl)
		{
			float half[] = {(node->bmax[0]+node->bmin[0])/2, (node->bmax[1]+node->bmin[1])/2, (node->bmax[2]+node->bmin[2])/2};
			for (int i=0; i<2; i++)
				for (int j=0; j<2; j++)
					for (int k=0; k<2; k++)
					{
						node->children[i][j][k] = new OctreeUV();
						node->children[i][j][k]->bmin[0] = i ? half[0] : node->bmin[0];
						node->children[i][j][k]->bmin[1] = j ? half[1] : node->bmin[1];
						node->children[i][j][k]->bmin[2] = k ? half[2] : node->bmin[2];
						node->children[i][j][k]->bmax[0] = i ? node->bmax[0] : half[0];
						node->children[i][j][k]->bmax[1] = j ? node->bmax[1] : half[1];
						node->children[i][j][k]->bmax[2] = k ? node->bmax[2] : half[2];
					}

			for (int i=0; i<node->numData; i++)
			{
				Point3d pos = node->data[i].pos;
				int x = (pos.x < half[0]) ? 0 : 1;
				int y = (pos.y < half[1]) ? 0 : 1;
				int z = (pos.z < half[2]) ? 0 : 1;
				#ifdef USE_UV_BLIST
					node->children[x][y][z]->data.add(node->data[i]);
				#else
					node->children[x][y][z]->data[node->children[x][y][z]->numData] = node->data[i];
				#endif
				node->children[x][y][z]->numData++;
			}
			#ifdef USE_UV_BLIST
				for (int i=0; i<2; i++)
					for (int j=0; j<2; j++)
						for (int k=0; k<2; k++)
							node->children[i][j][k]->data.createArray();
				node->data.freeList();
				node->data.destroyArray();
			#else
				if (node->data)
					delete [] node->data;
				node->data = NULL;
			#endif
			node->numData = 0;
			datablockscount--;
			node->isLeaf = false;
		}
	}

	float eps = 0.0001f ;
	for (int i=0; i<node->numData; i++)
	{
		if (samePointEps(node->data[i].pos, uvw.pos, eps)   &&   sameNormalEps(node->data[i].normal, uvw.normal, eps))
		{
			node->data[i].udir += uvw.udir;
			node->data[i].vdir += uvw.vdir;
			return maxlvl;
		}
	}

	#ifdef USE_UV_BLIST	
		node->data.add(uvw);
		node->data.createArray();
	#else
		node->data[numData] = uvw;
	#endif
	node->numData++;

	return maxlvl;
}

bool OctreeUV::addItem(const UVvertex &uvw, bool compareExisting)
{
	float half[] = {(bmax[0]+bmin[0])/2, (bmax[1]+bmin[1])/2, (bmax[2]+bmin[2])/2};

	if (isLeaf  &&  numData==OUVCOUNT)
		createChildren(true);

	if (!isLeaf)
	{
		int x = (uvw.pos.x < half[0]) ? 0 : 1;
		int y = (uvw.pos.y < half[1]) ? 0 : 1;
		int z = (uvw.pos.z < half[2]) ? 0 : 1;
		return children[x][y][z]->addItem(uvw, compareExisting);
	}

	if (compareExisting)
	{
		for (int i=0; i<numData; i++)
		{
			if (samePoint(data[i].pos, uvw.pos)   &&   sameNormal(data[i].normal, uvw.normal))
			{
				data[i].udir += uvw.udir;
				data[i].vdir += uvw.vdir;
				return true;
			}
		}
	}

	#ifdef USE_UV_BLIST	
		data.add(uvw);
		data.createArray();
	#else
		data[numData] = uvw;
	#endif
	numData++;

	return true;
}

UVvertex * OctreeUV::findDataNonRecursive(UVvertex * orig)
{
	OctreeUV * node = this;
	while (!node->isLeaf)
	{
		float half[] = {(node->bmax[0]+node->bmin[0])/2, (node->bmax[1]+node->bmin[1])/2, (node->bmax[2]+node->bmin[2])/2};
		int x = (orig->pos.x < half[0]) ? 0 : 1;
		int y = (orig->pos.y < half[1]) ? 0 : 1;
		int z = (orig->pos.z < half[2]) ? 0 : 1;
		node = node->children[x][y][z];
	}

	for (int i=0; i<node->numData; i++)
	{
		if (samePoint(node->data[i].pos, orig->pos)   &&   sameNormal(node->data[i].normal, orig->normal))
			return &(node->data[i]);
	}

	return NULL;
}

UVvertex * OctreeUV::findData(UVvertex * orig)
{
	if (isLeaf)
	{
		for (int i=0; i<numData; i++)
		{
			if (samePoint(data[i].pos, orig->pos)   &&   sameNormal(data[i].normal, orig->normal))
				return &(data[i]);
		}
	}
	else
	{
		float half[] = {(bmax[0]+bmin[0])/2, (bmax[1]+bmin[1])/2, (bmax[2]+bmin[2])/2};
		int x = (orig->pos.x < half[0]) ? 0 : 1;
		int y = (orig->pos.y < half[1]) ? 0 : 1;
		int z = (orig->pos.z < half[2]) ? 0 : 1;
		UVvertex * res = children[x][y][z]->findData(orig);
		if (res)
			return res;
	}
	return NULL;
}

// ---  NON-RECURSIVE VERSION

bool Mesh::evalUVdirs()
{
	datablockscount = 0;
	char progresstext[512];
	sprintf_s(progresstext, 512, "Evaluating UV dirs for %s", name);

	bool do_not_continue = false;
	int trimpl, tic;
	char buf[256];
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	sprintf_s(buf, 256, "Evaluating UV directions for mesh %s - %d triangles", name, (lastTri-firstTri+1) );
	Logger::add(buf);

	float mxmin=BIGFLOAT,  mymin=BIGFLOAT,  mzmin=BIGFLOAT; 
	float mxmax=-BIGFLOAT, mymax=-BIGFLOAT, mzmax=-BIGFLOAT; 

	DWORD time1 = GetTickCount();

	int ntris = lastTri-firstTri+1;
	if (ntris<1)
		return true;
	for (int i=0; i<ntris; i++)
	{
		int ii = i+firstTri;
		Triangle * tri = &(sc->triangles[ii]);
		mxmin = min(min(mxmin, tri->V1.x),   min(tri->V2.x, tri->V3.x));
		mxmax = max(max(mxmax, tri->V1.x),   max(tri->V2.x, tri->V3.x));
		mymin = min(min(mymin, tri->V1.y),   min(tri->V2.y, tri->V3.y));
		mymax = max(max(mymax, tri->V1.y),   max(tri->V2.y, tri->V3.y));
		mzmin = min(min(mzmin, tri->V1.z),   min(tri->V2.z, tri->V3.z));
		mzmax = max(max(mzmax, tri->V1.z),   max(tri->V2.z, tri->V3.z));
		bool ok = tri->evalUVvectors(tri->dirU1, tri->dirU2, tri->dirU3, tri->dirV1, tri->dirV2, tri->dirV3);
		if (!ok)
		{
			tri->getBadUVdirSmooth(tri->N1, tri->dirU1, tri->dirV1);
			tri->getBadUVdirSmooth(tri->N2, tri->dirU2, tri->dirV2);
			tri->getBadUVdirSmooth(tri->N3, tri->dirU3, tri->dirV3);
		}
	}
	
	// extend to avoid on border vertices problems
	mxmax = mxmax+(mxmax-mxmin)*0.001f;
	mxmin = mxmin-(mxmax-mxmin)*0.001f;
	mymax = mymax+(mymax-mymin)*0.001f;
	mymin = mymin-(mymax-mymin)*0.001f;
	mzmax = mzmax+(mzmax-mzmin)*0.001f;
	mzmin = mzmin-(mzmax-mzmin)*0.001f;
	mxmax = mxmax+0.001f;
	mxmin = mxmin-0.001f;
	mymax = mymax+0.001f;
	mymin = mymin-0.001f;
	mzmax = mzmax+0.001f;
	mzmin = mzmin-0.001f;

	#ifdef PRINT_UV_STATS
		DWORD time2 = GetTickCount();
		sprintf_s(buf, 256, "First stage took %d ms.", (time2-time1));
		Logger::add(buf);
		sprintf_s(buf, 256, "Vertex size: %d ... Node size: %d", sizeof(UVvertex), sizeof(OctreeUV));
		Logger::add(buf);
	#endif


	#ifdef NO_UV_DIR_SMOOTH
		return true;
	#endif

	// create Octree
	OctreeUV * tree = new OctreeUV();
	tree->bmin[0] = mxmin;
	tree->bmin[1] = mymin;
	tree->bmin[2] = mzmin;
	tree->bmax[0] = mxmax;
	tree->bmax[1] = mymax;
	tree->bmax[2] = mzmax;
	tree->createChildren(true);

	trimpl = ntris/70;
	tic = 1;

	int maxlevel = 0;
	for (int i=0; i<ntris; i++)
	{
		int ii = i+firstTri;
		Triangle * tri = &(sc->triangles[ii]);
		if (tic*trimpl<i)
		{
			Raytracer * rtr = Raytracer::getInstance();
			rtr->curScenePtr->notifyProgress(progresstext, (float)tic);
			if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
			{
				do_not_continue = true;
				break;
			}
			tic++;
		}

		UVvertex uvw;
		uvw.pos = tri->V1;
		uvw.normal = tri->N1;
		uvw.udir = tri->dirU1;
		uvw.vdir = tri->dirV1;
		#ifdef USE_UV_RECURSIVE
			tree->addItem(uvw, true);
		#else
			int m1 = tree->addItemNonRecursive(uvw);
		#endif
		uvw.pos = tri->V2;
		uvw.normal = tri->N2;
		uvw.udir = tri->dirU2;
		uvw.vdir = tri->dirV2;
		#ifdef USE_UV_RECURSIVE
			tree->addItem(uvw, true);
		#else
			int m2 = tree->addItemNonRecursive(uvw);
		#endif
		uvw.pos = tri->V3;
		uvw.normal = tri->N3;
		uvw.udir = tri->dirU3;
		uvw.vdir = tri->dirV3;
		#ifdef USE_UV_RECURSIVE
			tree->addItem(uvw, true);
		#else
			int m3 = tree->addItemNonRecursive(uvw);
		#endif
	}

	if (do_not_continue)
	{
		delete tree;
		return false;
	}

	#ifdef PRINT_UV_STATS
		DWORD time3 = GetTickCount();
		sprintf_s(buf, 256, "Second stage took %d ms.", (time3-time2));
		Logger::add(buf);
	#endif

	tree->normalizeShit();

	#ifdef PRINT_UV_STATS
		DWORD time4 = GetTickCount();
		sprintf_s(buf, 256, "Third stage took %d ms.", (time4-time3));
		Logger::add(buf);
		sprintf_s(buf, 256, "Used %d data blocks.", 	datablockscount);
		Logger::add(buf);
	#endif

	tic=1;
	trimpl=ntris/30;
	for (int i=0; i<ntris; i++)
	{
		int ii = i+firstTri;
		Triangle * tri = &(sc->triangles[ii]);
		if (tic*trimpl<i)
		{
			Raytracer * rtr = Raytracer::getInstance();
			rtr->curScenePtr->notifyProgress(progresstext, (float)tic+70);
			if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
			{
				do_not_continue = true;
				break;
			}
			tic++;
		}

		UVvertex uv1, uv2, uv3;
		uv1.normal = tri->N1;
		uv1.pos = tri->V1;
		uv2.normal = tri->N2;
		uv2.pos = tri->V2;
		uv3.normal = tri->N3;
		uv3.pos = tri->V3;

		#ifdef USE_UV_RECURSIVE
		if (UVvertex * ruv1 = tree->findData(&uv1))
		#else
		if (UVvertex * ruv1 = tree->findDataNonRecursive(&uv1))
		#endif
		{
			tri->dirU1 = ruv1->udir;
			tri->dirV1 = ruv1->vdir;
		}
		#ifdef USE_UV_RECURSIVE
		if (UVvertex * ruv2 = tree->findData(&uv2))
		#else
		if (UVvertex * ruv2 = tree->findDataNonRecursive(&uv2))
		#endif
		{
			tri->dirU2 = ruv2->udir;
			tri->dirV2 = ruv2->vdir;
		}
		#ifdef USE_UV_RECURSIVE
		if (UVvertex * ruv3 = tree->findData(&uv3))
		#else
		if (UVvertex * ruv3 = tree->findDataNonRecursive(&uv3))
		#endif
		{
			tri->dirU3 = ruv3->udir;
			tri->dirV3 = ruv3->vdir;
		}
	}

	#ifdef PRINT_UV_STATS
		DWORD time5 = GetTickCount();
		sprintf_s(buf, 256, "Fourth stage took %d ms.", (time5-time4));
		Logger::add(buf);
		sprintf_s(buf, 256, "Total time %d ms.", (time5-time1));
		Logger::add(buf);
	#endif

	delete tree;

	return !do_not_continue;
}

