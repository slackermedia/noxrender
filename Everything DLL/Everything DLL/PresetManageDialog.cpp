#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "EM2Controls.h"
#include "noxfonts.h"

extern HMODULE hDllModule;

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
bool initPresetManageControlPositions(HWND hWnd);
bool updatePresetManageControlBackgrounds(HWND hWnd);
void managePresetsFill(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK PresetManageDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = 300 + wrect.right-wrect.left-crect.right;
				tmprect.bottom = 294 + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);
	
				RECT rect;
				GetClientRect(hWnd, &rect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(rect.right, rect.bottom, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();

				initPresetManageControlPositions(hWnd);
				updatePresetManageControlBackgrounds(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_TITLE));
				emtitle->bgImage = hBgBMP;
				emtitle->setFont(fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST));
				emlv->showHeader = false;
				emlv->setColNumAndClearData(1, 200);
				emlv->setHeightHeader(0);
				emlv->setHeightData(26);
				emlv->prepareWindow();
				emlv->updateSliderRegions();
				emlv->align[0] = EM2ListView::ALIGN_LEFT;
				emlv->setFontHeader(fonts->em2listheader, false);
				emlv->setFontData(fonts->em2listentry, false);
				emlv->bgImage = hBgBMP;

				managePresetsFill(hWnd);

				EM2Button * embrename = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_RENAME));
				embrename->bgImage = hBgBMP;
				embrename->setFont(fonts->em2button, false);

				EM2Button * embremove = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_REMOVE));
				embremove->bgImage = hBgBMP;
				embremove->setFont(fonts->em2button, false);

				EM2Button * embup = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_UP));
				embup->bgImage = hBgBMP;
				embup->setFont(fonts->em2button, false);

				EM2Button * embdown = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_DOWN));
				embdown->bgImage = hBgBMP;
				embdown->setFont(fonts->em2button, false);

				EM2Button * embclose = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_CLOSE));
				embclose->bgImage = hBgBMP;
				embclose->setFont(fonts->em2button, false);


			}	// end WM_INITDIALOG
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;

			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_MANAGESPRESETS_CLOSE:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)0); 
							}
						}
						break;
					case IDC_MANAGESPRESETS_REMOVE:
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							int n = sc->presets.objCount;

							EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST));
							CHECK(emlv);
							CHECK(emlv->selected>-1);
							CHECK(emlv->selected<n);

							sc->presets.removeElement(emlv->selected);
							sc->presets.createArray();
							managePresetsFill(hWnd);
							InvalidateRect(emlv->hwnd, NULL, false);

						}
						break;
					case IDC_MANAGESPRESETS_RENAME:
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							int n = sc->presets.objCount;

							EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST));
							CHECK(emlv);
							CHECK(emlv->selected>-1);
							CHECK(emlv->selected<n);

							char * name = sc->presets[emlv->selected]->name;
							if (!name)
								name = "";

							char * newname = runPresetAddDialog(hWnd, name);
							if (newname)
							{
								PresetPost * pr = sc->presets[emlv->selected];
								if (pr->name)
									free(pr->name);
								pr->name = newname;

								managePresetsFill(hWnd);
								InvalidateRect(emlv->hwnd, NULL, false);
							}

						}
						break;
					case IDC_MANAGESPRESETS_UP:
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							int n = sc->presets.objCount;

							EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST));
							CHECK(emlv);
							CHECK(emlv->selected>0);
							CHECK(emlv->selected<n);

							PresetPost * pr1 = sc->presets[emlv->selected];
							PresetPost * pr2 = sc->presets[emlv->selected-1];
							sc->presets[emlv->selected]   = pr2;
							sc->presets[emlv->selected-1] = pr1;
							emlv->setData(0, emlv->selected, pr2->name);
							emlv->setData(0, emlv->selected-1, pr1->name);
							emlv->selected--;
							emlv->slideToSelected();

							InvalidateRect(emlv->hwnd, NULL, false);
						}
						break;
					case IDC_MANAGESPRESETS_DOWN:
						{
							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							int n = sc->presets.objCount;

							EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST));
							CHECK(emlv);
							CHECK(emlv->selected>-1);
							CHECK(emlv->selected<n-1);

							PresetPost * pr1 = sc->presets[emlv->selected];
							PresetPost * pr2 = sc->presets[emlv->selected+1];
							sc->presets[emlv->selected]   = pr2;
							sc->presets[emlv->selected+1] = pr1;
							emlv->setData(0, emlv->selected, pr2->name);
							emlv->setData(0, emlv->selected+1, pr1->name);
							emlv->selected++;
							emlv->slideToSelected();

							InvalidateRect(emlv->hwnd, NULL, false);

						}
						break;

				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool initPresetManageControlPositions(HWND hWnd)
{
	RECT crect;
	GetClientRect(hWnd, &crect);

	SetWindowPos(GetDlgItem(hWnd, IDC_MANAGESPRESETS_TITLE),			HWND_TOP,	10,  11,	crect.right-20, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST),				HWND_TOP,	10,  38,	180, 210, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_MANAGESPRESETS_RENAME),			HWND_TOP,	200, 39,	90, 24, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_MANAGESPRESETS_REMOVE),			HWND_TOP,	200, 65,	90, 24, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_MANAGESPRESETS_UP),				HWND_TOP,	200, 91,	90, 24, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_MANAGESPRESETS_DOWN),				HWND_TOP,	200, 117,	90, 24, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_MANAGESPRESETS_CLOSE),			HWND_TOP,	105, 259,	90, 24, 0);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updatePresetManageControlBackgrounds(HWND hWnd)
{
	EM2Text * emttitle= GetEM2TextInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_MANAGESPRESETS_TITLE), 0, 0, emttitle->bgShiftX, emttitle->bgShiftY);

	EM2Button * embrename = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_RENAME));
	updateControlBgShift(GetDlgItem(hWnd, IDC_MANAGESPRESETS_RENAME), 0, 0, embrename->bgShiftX, embrename->bgShiftY);

	EM2Button * embremove = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_REMOVE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_MANAGESPRESETS_REMOVE), 0, 0, embremove->bgShiftX, embremove->bgShiftY);

	EM2Button * embup = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_UP));
	updateControlBgShift(GetDlgItem(hWnd, IDC_MANAGESPRESETS_UP), 0, 0, embup->bgShiftX, embup->bgShiftY);

	EM2Button * embdown = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_DOWN));
	updateControlBgShift(GetDlgItem(hWnd, IDC_MANAGESPRESETS_DOWN), 0, 0, embdown->bgShiftX, embdown->bgShiftY);

	EM2Button * embclose = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_CLOSE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_MANAGESPRESETS_CLOSE), 0, 0, embclose->bgShiftX, embclose->bgShiftY);

	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST));
	updateControlBgShift(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST), 0, 0, emlv->bgShiftX, emlv->bgShiftY);

	return true;
}

//------------------------------------------------------------------------------------------------

void managePresetsFill(HWND hWnd)
{
	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_MANAGESPRESETS_LIST));
	if (!emlv)
		return;

	emlv->freeEntries();
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	int n = sc->presets.objCount;
	for (int i=0; i<n; i++)
	{
		char * pname = "Unnamed";
		PresetPost * pr = sc->presets[i];
		if (pr  &&  pr->name  &&  strlen(pr->name)>0)
			pname = pr->name;
		emlv->addEmptyRow();
		emlv->setData(0,i, pname);
	}
	emlv->calcSB();
	emlv->updateSliderRegions();
	InvalidateRect(hWnd, NULL, false);
}

//------------------------------------------------------------------------------------------------

void runPresetManageDialog(HWND hWnd)
{
	DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_MANAGE_PRESET_DIALOG), hWnd, PresetManageDlgProc,(LPARAM)( 0 ));
}

//------------------------------------------------------------------------------------------------
