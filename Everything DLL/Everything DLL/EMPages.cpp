#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;

#define EMPAGES_MODE_MEASURE 1
#define EMPAGES_MODE_DRAW 2

int measureOrDrawNumbers(HDC thdc, EMPages * emp, int mode, POINT px);


void InitEMPages()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMPages";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMPagesProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMPages *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMPages * GetEMPagesInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMPages * emp = (EMPages *)GetWindowLongPtr(hwnd, 0);
	#else
		EMPages * emp = (EMPages *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emp;
}

void SetEMPagesInstance(HWND hwnd, EMPages *emp)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emp);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emp);
	#endif
}

EMPages::EMPages(HWND hWnd)
{
	hwnd = hWnd;
	colLinkNormal		= RGB(96,96,240);
	colLinkCurrent		= RGB(255,255,255);
	colLinkMouseOver	= RGB(250,64,64);
	colNoLink			= RGB(160,160,160);
	colBackground		= RGB(64, 64, 64);
	noLinkText = copyString("no results");
	preText = copyString("");
	numPages = 1;
	curPage = 1;
	mouseOver = 0;
	lastMouseOver = -2;
	align = ALIGN_RIGHT;
	leftMargin = 0;

	HFONT hTempFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hBoldFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	DeleteObject(hTempFont);

	for (int i=0; i<8; i++)
	{
		nrects[i].left = 1000;
		nrects[i].right = 1000;
		nrects[i].top = 1000;
		nrects[i].bottom = 1000;
		numbers[i] = 0;
	}


}

EMPages::~EMPages()
{
	if (noLinkText)
		free(noLinkText);
	noLinkText = NULL;
	if (preText)
		free(preText);
	preText = NULL;
	if (hFont)
		DeleteObject(hFont);
	if (hBoldFont)
		DeleteObject(hBoldFont);
	hBoldFont = 0;
}

LRESULT CALLBACK EMPagesProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMPages * emp;
	emp = GetEMPagesInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	HDC thdc;
	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			{
				emp = new EMPages(hwnd);
				SetEMPagesInstance(hwnd, emp);

				POINT px = {0,0};
				int m = emp->getMouseOver(px);
				emp->mouseOver = m;
				SendMessage(hwnd, WM_MOUSEMOVE, 0, 0);
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_DESTROY:
			{
				delete emp;
				SetEMPagesInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
			{
				hdc = BeginPaint(hwnd, &ps);
				GetClientRect(hwnd, &rect);

				// CREATE TEMP HDC
				thdc = CreateCompatibleDC(hdc); 
				HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
				HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

				POINT px = {0,0};
				measureOrDrawNumbers(thdc, emp, EMPAGES_MODE_MEASURE, px);
				measureOrDrawNumbers(thdc, emp, EMPAGES_MODE_MEASURE, px);

				measureOrDrawNumbers(thdc, emp, EMPAGES_MODE_DRAW, px);


				// COPY TO ORIGINAL HDC
				GetClientRect(hwnd, &rect);
				BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
				SelectObject(thdc, oldBMP);
				DeleteObject(hbmp);
				DeleteDC(thdc);
				EndPaint(hwnd, &ps);
			}
			break;
		case WM_MOUSEMOVE:
			{
				SetCapture(emp->hwnd);
				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);
				int m = emp->getMouseOver(px);
				emp->mouseOver = m;
				if (emp->lastMouseOver != emp->mouseOver)
					InvalidateRect(hwnd, NULL, false);
				emp->lastMouseOver = emp->mouseOver;
				if (m==0)
				{
					SetCursor(LoadCursor(NULL, IDC_ARROW));
					ReleaseCapture();
				}
				else
					SetCursor(LoadCursor(NULL, IDC_HAND));
			}
			break;
		case WM_LBUTTONDOWN:
			{
				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);
				int m = emp->getMouseOver(px);
				if (m==0)
					break;
				emp->curPage = m;
				InvalidateRect(hwnd, NULL, false);
				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, PG_CLICKED), (LPARAM)hwnd);
				SendMessage(hwnd, WM_MOUSEMOVE, 0, lParam);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMPages::setCurrentPage(int cp, int np)
{
	curPage = cp;
	numPages = np;
	InvalidateRect(hwnd, NULL, false);
}

int measureOrDrawNumbers(HDC thdc, EMPages * emp, int mode, POINT px)
{
	CHECK(emp);

	RECT rect;
	GetClientRect(emp->hwnd, &rect);

	HFONT hOldFont = (HFONT)SelectObject(thdc, emp->hFont);
	SetBkColor  (thdc, emp->colBackground);

	int nwidth = 0;
	int np = emp->numPages;
	int cp = emp->curPage;

	bool draw = false;
	bool measure = false;
	switch (mode)
	{
		case EMPAGES_MODE_MEASURE:
			draw = false;
			measure = true;
			break;
		case EMPAGES_MODE_DRAW:
			draw = true;
			measure = false;
			break;
	}

	int nmouseover = 0;

	cp = max(1, min(cp, np));
	int odl = 9;
	int lM = emp->leftMargin;

	if (draw)
	{	// reset mouseover rects
		for (int i=0; i<8; i++)
		{
			emp->nrects[i].left = 1000;
			emp->nrects[i].right = 1000;
			emp->nrects[i].top = 1000;
			emp->nrects[i].bottom = 1000;
			emp->numbers[i] = 0;
		}
	}
	int rn = 0;



	if (emp->preText)	// pre text
	{
		SelectObject(thdc, emp->hFont);
		SIZE sz;
		int l = (int)strlen(emp->preText);
		GetTextExtentPoint32(thdc, emp->preText, l, &sz);
		if (draw)
		{
			SetTextColor(thdc, emp->colNoLink);
			ExtTextOut(thdc, lM+nwidth, 0, ETO_OPAQUE, &rect, emp->preText, l, 0);
		}
		nwidth += sz.cx + odl;
	}

	if (np < 1)
	{
		SIZE sz;
		char * nores = emp->noLinkText;
		if (!nores)
			nores = "no results";
		if (strlen(nores)<1)
			nores = "no results";
		int s = (int)strlen(nores);
		SetTextColor(thdc, emp->colNoLink);
		GetTextExtentPoint32(thdc, nores, s, &sz);
		if (draw)
			if (emp->preText)
				ExtTextOut(thdc, lM, 0, ETO_CLIPPED, &rect, nores, s, 0);
			else
				ExtTextOut(thdc, lM, 0, ETO_OPAQUE, &rect, nores, s, 0);
		nwidth += sz.cx + odl;
	}
	else
	{
		SetTextColor(thdc, emp->colLinkNormal);
		SIZE sz;
		char ppp[16];
		sprintf_s(ppp, 16, "");
		int l = (int)strlen(ppp);

		// 1 - always
		if (cp==1)
			SelectObject(thdc, emp->hBoldFont);
		sprintf_s(ppp, 16, "1");
		l = (int)strlen(ppp);
		GetTextExtentPoint32(thdc, ppp, l, &sz);
		if (draw)
		{
			SetTextColor(thdc, emp->colLinkNormal);
			if (cp==1)
				SetTextColor(thdc, emp->colLinkCurrent);
			if (emp->mouseOver == 1)
				SetTextColor(thdc, emp->colLinkMouseOver);
			if (emp->preText)
				ExtTextOut(thdc, lM+nwidth, 0, ETO_CLIPPED, &rect, ppp, l, 0);
			else
				ExtTextOut(thdc, lM+nwidth, 0, ETO_OPAQUE, &rect, ppp, l, 0);
			emp->nrects[rn].left = lM+nwidth-1;
			emp->nrects[rn].top =  0;
			emp->nrects[rn].right = lM+nwidth+sz.cx+1;
			emp->nrects[rn].bottom = sz.cy;
			emp->numbers[rn] = 1;
			rn++;
		}
		nwidth += sz.cx + odl;

		SelectObject(thdc, emp->hFont);

		if (cp > 3)	// dots
		{
			sprintf_s(ppp, 16, "...");
			l = (int)strlen(ppp);
			GetTextExtentPoint32(thdc, ppp, l, &sz);
			if (draw)
			{
				SetTextColor(thdc, emp->colNoLink);
				ExtTextOut(thdc, lM+nwidth, 0, ETO_CLIPPED, &rect, ppp, l, 0);
			}
			nwidth += sz.cx + odl;
		}

		if (cp > 2)	// prev
		{
			sprintf_s(ppp, 16, "%d", cp-1);
			l = (int)strlen(ppp);
			GetTextExtentPoint32(thdc, ppp, l, &sz);
			if (draw)
			{
				SetTextColor(thdc, emp->colLinkNormal);
				if (emp->mouseOver == cp-1)
					SetTextColor(thdc, emp->colLinkMouseOver);
				ExtTextOut(thdc, lM+nwidth, 0, ETO_CLIPPED, &rect, ppp, l, 0);
				emp->nrects[rn].left = lM+nwidth-1;
				emp->nrects[rn].top =  0;
				emp->nrects[rn].right = lM+nwidth+sz.cx+1;
				emp->nrects[rn].bottom = sz.cy;
				emp->numbers[rn] = cp-1;
				rn++;
			}
			nwidth += sz.cx + odl;
		}

		SelectObject(thdc, emp->hBoldFont);
		if (cp > 1)	// cur
		{
			sprintf_s(ppp, 16, "%d", cp);
			l = (int)strlen(ppp);
			GetTextExtentPoint32(thdc, ppp, l, &sz);
			if (draw)
			{
				SetTextColor(thdc, emp->colLinkCurrent);
				if (emp->mouseOver == cp)
					SetTextColor(thdc, emp->colLinkMouseOver);
				ExtTextOut(thdc, lM+nwidth, 0, ETO_CLIPPED, &rect, ppp, l, 0);
				emp->nrects[rn].left = lM+nwidth-1;
				emp->nrects[rn].top =  0;
				emp->nrects[rn].right = lM+nwidth+sz.cx+1;
				emp->nrects[rn].bottom = sz.cy;
				emp->numbers[rn] = cp;
				rn++;
			}
			nwidth += sz.cx + odl;
		}

		SelectObject(thdc, emp->hFont);
		if (cp < np-1)	// next
		{
			sprintf_s(ppp, 16, "%d", cp+1);
			l = (int)strlen(ppp);
			GetTextExtentPoint32(thdc, ppp, l, &sz);
			if (draw)
			{
				SetTextColor(thdc, emp->colLinkNormal);
				if (emp->mouseOver == cp+1)
					SetTextColor(thdc, emp->colLinkMouseOver);
				ExtTextOut(thdc, lM+nwidth, 0, ETO_CLIPPED, &rect, ppp, l, 0);
				emp->nrects[rn].left = lM+nwidth-1;
				emp->nrects[rn].top =  0;
				emp->nrects[rn].right = lM+nwidth+sz.cx+1;
				emp->nrects[rn].bottom = sz.cy;
				emp->numbers[rn] = cp+1;
				rn++;
			}
			nwidth += sz.cx + odl;
		}

		if (cp < np-2)	// dots again
		{
			sprintf_s(ppp, 16, "...");
			l = (int)strlen(ppp);
			GetTextExtentPoint32(thdc, ppp, l, &sz);
			if (draw)
			{
				SetTextColor(thdc, emp->colNoLink);
				ExtTextOut(thdc, lM+nwidth, 0, ETO_CLIPPED, &rect, ppp, l, 0);
			}
			nwidth += sz.cx + odl;
		}

		if (cp < np)	// last
		{
			sprintf_s(ppp, 16, "%d", np);
			l = (int)strlen(ppp);
			GetTextExtentPoint32(thdc, ppp, l, &sz);
			if (draw)
			{
				SetTextColor(thdc, emp->colLinkNormal);
				if (emp->mouseOver == np)
					SetTextColor(thdc, emp->colLinkMouseOver);
				ExtTextOut(thdc, lM+nwidth, 0, ETO_CLIPPED, &rect, ppp, l, 0);
				emp->nrects[rn].left = lM+nwidth-1;
				emp->nrects[rn].top =  0;
				emp->nrects[rn].right = lM+nwidth+sz.cx+1;
				emp->nrects[rn].bottom = sz.cy;
				emp->numbers[rn] = np;
				rn++;
			}
			nwidth += sz.cx + odl;
		}
	}

	DeleteObject(SelectObject(thdc, hOldFont));

	if (measure)
		switch (emp->align)
		{
			case EMPages::ALIGN_LEFT:
				emp->leftMargin = 0;
				break;
			case EMPages::ALIGN_RIGHT:
				emp->leftMargin = rect.right - nwidth;
				break;
			case EMPages::ALIGN_CENTER:
				emp->leftMargin = (rect.right - nwidth)/2;
				break;
		}

	return 0;

}

int EMPages::getMouseOver(POINT px)
{
	for (int i=0; i<8; i++)
		if (PtInRect(&(nrects[i]), px))
			return numbers[i];
	return 0;
}

