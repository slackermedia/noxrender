#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mbstring.h>
#include "ies.h"
#include "log.h"
#include "myList.h"
#include "log_settings.h"
#include <math.h>
#include "raytracer.h"

extern HMODULE hDllModule;
extern _locale_t noxLocale;

#define PI 3.1415926535897932384626433832795f
//#define IES_LOG_STEP(a)
#define IES_LOG_STEP(a) Logger::add(a)

#define CHECK_EOF(i, num) if (i>=num) { errorBuf = copyString("Unexpected end of file. Insufficient data."); return false; }

//-----------------------------------------------------------------------

IesNOX::IesNOX()
{
	#ifdef LOG_CONSTR_IES
		char buf[64];
		sprintf_s(buf, 64, "IES constructor at %lld", (INT_PTR)this);
		Logger::add(buf);
	#endif

	exportedFilename = NULL;
	exportedArrayID = -1;

	filename = NULL;
	file = NULL;
	errorBuf = NULL;
	anglesV = NULL;
	anglesH = NULL;
	values = NULL;
	numAnglesH = 0;
	numAnglesV = 0;
	numLamps = 0;
	lumensPerLamp = 0.0f ;
	candelaMultiplier = 0.0f;
	photometricType = 0;
	unitsType = 0;
	luminaireWidth = 0.0f;
	luminaireLength = 0.0f;
	luminaireHeight = 0.0f;
	ballast = 0.0f;
	ballastP = 0.0f;
	inputWatts = 0.0f;
	shapeVolume = 0.0f;
}

//-----------------------------------------------------------------------

IesNOX::~IesNOX()
{
	releaseStuff();
}

//-----------------------------------------------------------------------

IesNOX & IesNOX::operator= (const IesNOX & ies)
{	
	if (this == &ies) 
		return *this;

	#ifdef LOG_CONSTR_IES
		char buf[64];
		sprintf_s(buf, 64, "IES copy constructor at %lld from %lld ", (INT_PTR)this, (INT_PTR)&ies);
		Logger::add(buf);
	#endif

	file = NULL;
	releaseStuff(true);

	exportedArrayID = ies.exportedArrayID;
	exportedFilename = copyString(ies.exportedFilename);
	filename = copyString(ies.filename);
	errorBuf = copyString(ies.errorBuf);
	numLamps = ies.numLamps;
	lumensPerLamp = ies.lumensPerLamp;
	candelaMultiplier = ies.candelaMultiplier;
	numAnglesV = ies.numAnglesV;
	numAnglesH = ies.numAnglesH;
	photometricType = ies.photometricType;
	unitsType = ies.unitsType;
	luminaireWidth = ies.luminaireWidth;
	luminaireLength = ies.luminaireLength;
	luminaireHeight = ies.luminaireHeight;
	ballast = ies.ballast;
	ballastP = ies.ballastP;
	inputWatts = ies.inputWatts;

	if (ies.anglesV)
	{
		anglesV = (float *)malloc(sizeof(float)*ies.numAnglesV);
		if (anglesV)
			for (int i=0; i<ies.numAnglesV; i++)
				anglesV[i] = ies.anglesV[i];
	}
	else
		anglesV = NULL;

	if (ies.anglesH)
	{
		anglesH = (float *)malloc(sizeof(float)*ies.numAnglesH);
		if (anglesH)
			for (int i=0; i<ies.numAnglesH; i++)
				anglesH[i] = ies.anglesH[i];
	}
	else
		anglesH = NULL;

	if (ies.values)
	{
		values = (float **)malloc(sizeof(float *) * ies.numAnglesH);
		if (values)
		{
			for (int i=0; i<ies.numAnglesH; i++)
			{
				if (ies.values[i])
				{
					values[i] = (float*)malloc(sizeof(float)*ies.numAnglesV);
					if (values[i])
					{
						for (int j=0; j<ies.numAnglesV; j++)
							values[i][j] = ies.values[i][j];
					}
				}
				else
					values[i] = NULL;
			}
		}
	}
	else
		values = NULL;

	return *this;
}

//-----------------------------------------------------------------------

IesNOX::IesNOX(const IesNOX & ies)
{
	#ifdef LOG_CONSTR_IES
		char buf[64];
		sprintf_s(buf, 64, "IES constructor copy at %lld from %lld", (INT_PTR)this, (INT_PTR)&ies);
		Logger::add(buf);
	#endif

	exportedFilename = NULL;
	filename = NULL;
	file = NULL;
	errorBuf = NULL;
	anglesV = NULL;
	anglesH = NULL;
	values = NULL;
	numAnglesH = 0;
	numAnglesV = 0;

	*this = ies;
}

//-----------------------------------------------------------------------

bool IesNOX::isValid()
{
	if (numAnglesH<1 || numAnglesV<1 || values==NULL || anglesH==NULL || anglesV==NULL || filename==NULL)
		return false;
	return true;
}

//-----------------------------------------------------------------------

bool IesNOX::parseFile(char * fname)
{
	if (!fname)
		return false;
	file = NULL;
	filename = copyString(fname);

	if (fopen_s( &file, filename, "r"))
	{
		errorBuf = copyString("Can't open file.");
		return false;
	}

	int iestype = parseHeader();

	if (iestype<0)
	{
		fclose(file);
		file = NULL;
		releaseStuff(false);
		return false;
	}

	bool ok = true;

	//if (iestype==0)
	//	ok = parseIES86();

	//if (iestype==1)
	//	ok = parseIES91();

	//if (iestype==2)
		ok = parseIES95();

	if (!ok)
	{
		fclose(file);
		releaseStuff(false);
		return false;
	}

	evalVolume();

	fclose(file);
	file = NULL;
	return true;
}

//-----------------------------------------------------------------------

int IesNOX::parseHeader()
{
	if (!file)
	{
		errorBuf = copyString("File not opened.");
		return -1;
	}

	char lbuf[2048];
	fseek(file, 0, SEEK_SET);

	if (!fgets(lbuf, 2048, file))
	{
		errorBuf = copyString("File seems to be empty.");
		return -1;
	}

	if (_strupr_s(lbuf, 2048))
	{
		errorBuf = copyString("String operations error.");
		return -1;
	}

	if (char * nl = strpbrk(lbuf, "\n"))
		*nl = 0;


	if (!strcmp(lbuf, "IESNA91"))
	{
		return 1;
	}

	if (!strcmp(lbuf, "IESNA:LM-63-1995"))
	{
		return 2;
	}

	fseek(file, 0, SEEK_SET);
	return 0;
}

//-----------------------------------------------------------------------

bool IesNOX::parseIES86()
{
	Logger::add("Parsing IES 1986");


	return true;
}

//-----------------------------------------------------------------------

bool IesNOX::parseIES91()
{
	Logger::add("Parsing IES 1991");

	return true;
}

//-----------------------------------------------------------------------

bool IesNOX::parseIES95()
{
	Logger::add("Parsing IES 1995");
	if (!file)
	{
		errorBuf = copyString("File not opened.");
		return false;
	}

	char lbuf[2048];
	bool noTilt = true;
	while (noTilt)
	{
		if (!fgets(lbuf, 2048, file))
		{
			errorBuf = copyString("End of file - no TILT keyword.");
			return false;
		}
		int cmpidx = _mbsnbicmp((unsigned char *)lbuf, (unsigned char *)"TILT=", 5);
		if (cmpidx==0)
			noTilt = false;
	}
	Logger::add("got TILT");
	Logger::add(lbuf);

	int cmpidx = _mbsnbicmp((unsigned char *)lbuf, (unsigned char *)"TILT=NONE", 9);
	if (cmpidx!=0)
	{
		errorBuf = copyString("Only TILT=NONE ies file supported.");
		return false;
	}

	// get all the segments
	bList<char *, 16> segments(0);
	while (fgets(lbuf, 2048, file))
	{
		char namedelimiters[] = " \n\t";
		char * nametoken = NULL;
		char * namenexttoken = NULL;
		nametoken = strtok_s(lbuf, namedelimiters, &namenexttoken);
		while (nametoken)
		{
			char * ccc = copyString(nametoken);
			segments.add(ccc);
			segments.createArray();
			nametoken = strtok_s(NULL, namedelimiters, &namenexttoken);
		}
	}
	segments.createArray();
	int numData = segments.objCount;

	// PARSE SEGMENTS --------------------------------
	IES_LOG_STEP("Parsing stuff....");

	int lId = 0;
	__int64 ival;
	double fval;
	char * addr;
	char * strconv;

	// NUMBER OF LAMPS - INT
	IES_LOG_STEP("  number of lamps");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	ival = _strtoi64(strconv, &addr, 10);
	if ((addr==strconv)  || ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to int)
		errorBuf = copyString("Number of lamps: conversion to int error.");
		return false;
	}
	numLamps = (int)ival;
	if (numLamps<1)
	{
		errorBuf = copyString("Invalid number of lamps.");
		return false;
	}

	// LUMENS PER LAMP - FLOAT
	IES_LOG_STEP("  lumens per lamp");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Lumens per lamp: conversion to float error.");
		return false;
	}
	lumensPerLamp = (float)fval;

	// CANDELA MULTIPLIER
	IES_LOG_STEP("  candela multiplier");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Candela multiplier: conversion to float error.");
		return false;
	}
	candelaMultiplier = (float)fval;

	// NUMBER OF VERTICAL ANGLES - INT
	IES_LOG_STEP("  number of vertical angles");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	ival = _strtoi64(strconv, &addr, 10);
	if ((addr==strconv)  || ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to int)
		errorBuf = copyString("Number of vertical angles: conversion to int error.");
		return false;
	}
	numAnglesV = (int)ival;
	if (numAnglesV<1)
	{
		errorBuf = copyString("Invalid number of vertical angles.");
		return false;
	}

	// NUMBER OF HORIZONTAL ANGLES - INT
	IES_LOG_STEP("  number of horizontal angles");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	ival = _strtoi64(strconv, &addr, 10);
	if ((addr==strconv)  || ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to int)
		errorBuf = copyString("Number of horizontal angles: conversion to int error.");
		return false;
	}
	numAnglesH = (int)ival;
	if (numAnglesH<1)
	{
		errorBuf = copyString("Invalid number of horizontal angles.");
		return false;
	}

	// PHOTOMETRIC TYPE - INT
	IES_LOG_STEP("  photometric type");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	ival = _strtoi64(strconv, &addr, 10);
	if ((addr==strconv)  || ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to int)
		errorBuf = copyString("Photometric type: conversion to int error.");
		return false;
	}
	photometricType = (int)ival;
	if (photometricType<1  ||  photometricType>3)
	{
		errorBuf = copyString("Invalid photometric type value.");
		return false;
	}

	// UNITS TYPE - INT
	IES_LOG_STEP("  units type");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	ival = _strtoi64(strconv, &addr, 10);
	if ((addr==strconv)  || ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to int)
		errorBuf = copyString("Units type: conversion to int error.");
		return false;
	}
	unitsType = (int)ival;
	if (unitsType<1  ||  photometricType>2)
	{
		errorBuf = copyString("Invalid photometric type value.");
		return false;
	}

	// LUMINAIRE WIDTH - FLOAT
	IES_LOG_STEP("  luminaire width");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Luminaire width: conversion to float error.");
		return false;
	}
	luminaireWidth = (float)fval;

	// LUMINAIRE LENGTH - FLOAT
	IES_LOG_STEP("  luminaire length");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Luminaire length: conversion to float error.");
		return false;
	}
	luminaireLength = (float)fval;

	// LUMINAIRE HEIGHT - FLOAT
	IES_LOG_STEP("  luminaire height");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Luminaire height: conversion to float error.");
		return false;
	}
	luminaireHeight = (float)fval;

	// BALLAST - FLOAT
	IES_LOG_STEP("  ballast");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Ballast factor: conversion to float error.");
		return false;
	}
	ballast = (float)fval;

	// BALLAST PHOTOMETRIC - FLOAT - NOT USED
	IES_LOG_STEP("  ballast photometric");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Ballast photometric factor: conversion to float error.");
		return false;
	}
	ballastP = (float)fval;

	// INPUT WATTS - FLOAT
	IES_LOG_STEP("  input watts");
	CHECK_EOF(lId, numData)
	strconv = segments[lId++];
	fval = _strtod_l(strconv, &addr, noxLocale);
	if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
	{	// conversion error (to float)
		errorBuf = copyString("Input Watts: conversion to float error.");
		return false;
	}
	inputWatts = (float)fval;


	// MEMORY ALLOCATION - ANGLES
	IES_LOG_STEP("  allocating memory for angles");
	anglesV = (float *)malloc(sizeof(float)*max(1, numAnglesV));
	anglesH = (float *)malloc(sizeof(float)*max(1, numAnglesH));

	// VERTICAL ANGLES - FLOATs
	IES_LOG_STEP("  vertical angles");
	for (int i=0; i<numAnglesV; i++)
	{
		CHECK_EOF(lId, numData)
		strconv = segments[lId++];
		fval = _strtod_l(strconv, &addr, noxLocale);
		if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
		{	// conversion error (to float)
			errorBuf = copyString("Vertical angle: conversion to float error.");
			return false;
		}
		anglesV[i] = (float)fval;
	}

	// HORIZONTAL ANGLES - FLOATs
	IES_LOG_STEP("  horizontal angles");
	for (int i=0; i<numAnglesH; i++)
	{
		CHECK_EOF(lId, numData)
		strconv = segments[lId++];
		fval = _strtod_l(strconv, &addr, noxLocale);
		if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
		{	// conversion error (to float)
			errorBuf = copyString("Horizontal angle: conversion to float error.");
			return false;
		}
		anglesH[i] = (float)fval;
	}

	// MEMORY ALLOCATION - DATA
	IES_LOG_STEP("  allocating memory for emission values");
	numAnglesH = max(numAnglesH, 1);
	numAnglesV = max(numAnglesV, 1);
	values = (float**)malloc(sizeof(float *)*numAnglesH);
	for (int i=0; i<numAnglesH; i++)
		values[i] = (float*)malloc(sizeof(float)*numAnglesV);

	// EMISSION - FLOAT
	IES_LOG_STEP("  emission values");
	for (int i=0; i<numAnglesH; i++)
	{
		for (int j=0; j<numAnglesV; j++)
		{
			CHECK_EOF(lId, numData)
			strconv = segments[lId++];
			fval = _strtod_l(strconv, &addr, noxLocale);
			if ((addr==strconv)  ||  ((unsigned int)(addr-strconv)<strlen(strconv)) )
			{	// conversion error (to float)
				errorBuf = copyString("Direction emission: conversion to float error.");
				return false;
			}
			values[i][j] = (float)fval;
		}
	}
	IES_LOG_STEP("Parsing done");

	return true;
}

//-----------------------------------------------------------------------

void IesNOX::releaseStuff(bool delErrorLog)
{
	if (delErrorLog)
	{
		if (errorBuf)
			free(errorBuf);
		errorBuf = NULL;
	}
	if (filename)
		free(filename);
	if (exportedFilename)
		free(exportedFilename);
	if (anglesV)
		free(anglesV);
	if (anglesH)
		free(anglesH);
	if (values)
	{
		for (int i=0; i<numAnglesH; i++)
		{
			if (values[i])
				free(values[i]);
			values[i] = NULL;
		}
		free(values);
	}

	file = NULL;
	filename = NULL;
	exportedFilename = NULL;
	anglesV = NULL;
	anglesH = NULL;
	values = NULL;
	numAnglesH = 0;
	numAnglesV = 0;
	exportedArrayID = -1;
	numLamps = 0;
	lumensPerLamp = 0.0f;
}

//-----------------------------------------------------------------------

float IesNOX::getEmission(float hAngle, float vAngle)
{
	// V ANGLE: 0 - normal dir
	//			PI - -normal dir
	// H ANGLE - ignored
	if (vAngle<0 || vAngle>PI)
		return 0.0f;
	if (numAnglesV < 2)
		return 0.0f;
	float vDeg = vAngle * 180.0f/PI;

	if (vDeg<anglesV[0]  ||  vDeg>=anglesV[numAnglesV-1])
		return 0.0f;

	for (int i=0; i<numAnglesV; i++)
	{
		if (vDeg>anglesV[i+1])
			continue;
		float w = (vDeg-anglesV[i]) / (anglesV[i+1]-anglesV[i]);
		return ( values[0][i]*(1-w) + values[0][i+1]*w ) * (0.5f/PI);
		return values[0][i];
	}

	return 0.0f;
}

//-----------------------------------------------------------------------

float IesNOX::getMaxValue()
{
	float res = 0.0f;
	for (int i=0; i<numAnglesV; i++)
		res = max(res, values[0][i]);
	res *= candelaMultiplier * ballast;
	return res;
}

//-----------------------------------------------------------------------

char * openIesFileDialog(HWND hwnd)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hDllModule;
	ofn.lpstrFilter = "IES file\0*.ies\0All files\0*.*\0";
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = (char *)malloc(1024);
	ofn.lpstrFile[0] = 0;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = "";
	ofn.lpstrTitle = "Load IES file";
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = NULL;
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST;

	bool res = (GetOpenFileName(&ofn) == TRUE);
	if (res)
	{
		return ofn.lpstrFile;
	}
	else
	{
		free(ofn.lpstrFile);
		return NULL;
	}
	
	return NULL;
}

//-----------------------------------------------------------------------

bool IesNOX::evalVolume()
{
	float vol = 0;
	for (int i=0; i<360; i++)
	{
		float a = i*PI/360;
		float s = sin(a);
		float c = getEmission(0, a);
		vol += c*s*PI;
	}

	shapeVolume = vol * 2 * PI/360;
		
	return true;
}

//-----------------------------------------------------------------------

float IesNOX::randomDirectionAngle(float &pdf)
{
	#define IESRTRIES 30
	float powers[IESRTRIES];
	float powersuntil[IESRTRIES];
	float angles[IESRTRIES];

	float sumpower = 0.0f;
	float sum_all = 0.0f;
	float sum_ok = 0.0f;
	for (int i=0; i<IESRTRIES; i++)
	{
		float rnd, srnd, power = 0;
		while (power <= 0)
		{
			#ifdef RAND_PER_THREAD
				rnd = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
			#else
				rnd = ((float)rand()/(float)RAND_MAX);
			#endif
			srnd = acos(1-2*rnd);
			power = getEmission(0, srnd);
			sum_all += sin(srnd);
		}
		sum_ok += sin(srnd);
		powers[i] = power;
		angles[i] = srnd;
		sumpower += power;
		powersuntil[i] = sumpower;
	}

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = ((float)rand()/(float)RAND_MAX);
	#endif
	a *= sumpower;
	for (int i=0; i<IESRTRIES; i++)
	{
		if (a<powersuntil[i])
		{
			pdf = powers[i]/sumpower;
			pdf *= sum_all/sum_ok;
			pdf *= IESRTRIES;
			pdf /= 4*PI;
			return angles[i];
		}
	}

	pdf = powers[IESRTRIES-1]/sumpower;
	pdf *= IESRTRIES;
	pdf *= sum_all/sum_ok;
	pdf /= 4*PI;
	return angles[IESRTRIES-1];
}

//-----------------------------------------------------------------------

float IesNOX::getProbability(float angle)
{
	float res = getEmission(0, angle);
	res /= shapeVolume;
	return res;
}

//-----------------------------------------------------------------------
