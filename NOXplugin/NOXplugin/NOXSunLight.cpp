#include "max2013predef.h"
#include "NOXSunLight.h"
#include <decomp.h> 
#include "noxdebug.h"

#if MAX_RELEASE<13000
	#include <maxscrpt\maxscrpt.h>		// 2011 and lower
	#include <maxscrpt\definsfn.h>
#else
	#include <maxscript\maxscript.h>	// 2012 and higher
	#include <maxscript\macros\define_instantiation_functions.h>
#endif

#include <paramtype.h>

int NOXSunLight::numSuns = 0;
static NOXSunLightClassDesc NOXSunLightDesc;
ClassDesc2* GetNOXSunLightDesc() { return &NOXSunLightDesc; }
int numSunInstances = 0;

NOXSunLightCreateCallBack noxSunCCB;

INode * findExistingNOXSunLight(INode * node);

#if MAX_RELEASE<15000
	#define p_end end
#endif

enum { slp_sun_on, slp_sky_on, slp_sky_model, slp_env_on, slp_env_tex };
//------------------------------------------------------------------------------------------------------------------------

static ParamBlockDesc2 noxsunlight_param_blk ( 
	0, _T("params"),  0, &NOXSunLightDesc, P_AUTO_CONSTRUCT, NOXSUNLIGHT_PBLOCK_REF, 

	slp_sun_on,		_T("sun_on"),			TYPE_BOOL, 		P_ANIMATABLE,    0, p_end,
	slp_sky_on,		_T("sky_on"),			TYPE_BOOL, 		P_ANIMATABLE,    0, p_end,
	slp_sky_model,	_T("sky_model"),		TYPE_INT, 		P_ANIMATABLE,    0, p_end,
	slp_env_on,		_T("env_on"),			TYPE_BOOL, 		P_ANIMATABLE,    0, p_end,
	slp_env_tex,	_T("env_tex"),			TYPE_STRING, 	P_ANIMATABLE,    0, p_end,

	p_end
);

//------------------------------------------------------------------------------------------------------------------------

int NOXSunLightCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat) 
{
	Point3 p0;
	#ifdef _OSNAP
	if (msg == MOUSE_FREEMOVE)
	{
		#ifdef _3D_CREATE
			vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
		#else
			vpt->SnapPreview(m,m,NULL, SNAP_IN_PLANE);
		#endif
	}
	#endif

	if (msg==MOUSE_POINT)
	{
		if ( point == 0 )
		{
			int num = NOXSunLight::countSuns();
			if (num > 1)
			{
				MessageBox(0, _T("Sun already exists."), _T("Sun"), 0);
				ob->removeUI = true;
				return CREATE_ABORT;
			}


			ULONG handle;
			ob->NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
			sunNode = GetCOREInterface()->GetINodeByHandle( handle );
			if ( sunNode ) 
			{
				Point3 color = GetUIColor( COLOR_LIGHT_OBJ );	// yellow wire color
				sunNode->SetWireColor( RGB( color.x*255.0f, color.y*255.0f, color.z*255.0f ) );
			}

			IObjCreate *createInterface = GetCOREInterface()->GetIObjCreate();
			targObject = new NOXSunTarget;
			targNode = createInterface->CreateObjectNode( targObject);
			createInterface->BindToTarget(sunNode,targNode);			
			ob->startPoint = sunNode->GetNodeTM(GetCOREInterface()->GetTime()).GetTrans();

			#ifdef _3D_CREATE
				mat.SetTrans( vpt->SnapPoint(m,m,NULL,SNAP_IN_3D) );
			#else
				mat.SetTrans( vpt->SnapPoint(m,m,NULL,SNAP_IN_PLANE) );
			#endif
		}

		if (point==1) 
		{
			ob->creating = false;
			return MOUSE_STOP;
		}
	}

	if (msg == MOUSE_MOVE)
	{
		if (point == 1)
		{
			TimeValue curtime = GetCOREInterface()->GetIObjCreate()->GetTime();
			ob->Eval(curtime);
			ob->UpdateTargDistance(curtime, sunNode);
			IObjCreate *createInterface = GetCOREInterface()->GetIObjCreate();
			Matrix3 matr = IdentityTM();
			matr.SetTrans( vpt->SnapPoint(m,m,NULL,SNAP_IN_3D) );
			createInterface->SetNodeTMRelConstPlane(targNode, matr);
			targNode->SetNodeTM(curtime, matr);
		}
		return TRUE;
	}

	if (msg == MOUSE_ABORT)
	{
		ob->creating = false;
		return CREATE_ABORT;
	}

	return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------

enum { noxsunlight_params };
enum { pb_spin };

//------------------------------------------------------------------------------------------------------------------------

RefTargetHandle NOXSunLight::GetReference(int i) 
{
	if (i==0)
		return sunlight_pblock;
	return NULL;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::SetReference(int i, RefTargetHandle rtarg) 
{
	if (i==0)
		sunlight_pblock = (IParamBlock2 *)rtarg;
}

//------------------------------------------------------------------------------------------------------------------------

int	NOXSunLight::NumParamBlocks() 
{
	return 1;
}

//------------------------------------------------------------------------------------------------------------------------

IParamBlock2* NOXSunLight::GetParamBlock(int i)
{
	if (i==0)
		return sunlight_pblock;
	return NULL;
}

//------------------------------------------------------------------------------------------------------------------------

IParamBlock2* NOXSunLight::GetParamBlockByID(BlockID id)
{
	if (id==0)
		return sunlight_pblock;
	return NULL;
}

//------------------------------------------------------------------------------------------------------------------------

CreateMouseCallBack * NOXSunLight::GetCreateMouseCallBack(void)
{
	noxSunCCB.SetObj(this);
	return &noxSunCCB;
}

//------------------------------------------------------------------------------------------------------------------------

RefResult NOXSunLight::EvalLightState (TimeValue t, Interval &valid, LightState *cs)
{
	cs->type = DIRECT_LGT;
	cs->color = Point3(1.0f, 1.0f, 0.5f);
	cs->intens = 1;
	cs->attenStart = cs->attenEnd = 0.0f;
	cs->on = TRUE;
	cs->affectSpecular = TRUE;
	cs->affectDiffuse = TRUE;
	cs->ambientOnly = FALSE;
	cs->aspect = 1;
	cs->on = TRUE;

	cs->shape = GetSpotShape();
	cs->aspect = GetAspect(t, valid);
	cs->overshoot = GetOvershoot();
	cs->shadow = GetShadow();
	cs->useNearAtten = GetUseAttenNear();
	cs->nearAttenStart = GetAtten(t, ATTEN1_START, valid);
	cs->nearAttenEnd = GetAtten(t, ATTEN1_END, valid);
	cs->useAtten = GetUseAtten();
	cs->attenStart = GetAtten(t, ATTEN_START, valid);
	cs->attenEnd = GetAtten(t, ATTEN_END, valid);

	valid = Interval(0,0);

	return REF_SUCCEED;
}

//------------------------------------------------------------------------------------------------------------------------

RefTargetHandle NOXSunLight::Clone( RemapDir &remap )
{
	NOXSunLight * newob = new NOXSunLight();
	BaseClone(this, newob, remap);
	newob->ReplaceReference(0, remap.CloneRef(sunlight_pblock));
	return newob;
}

//------------------------------------------------------------------------------------------------------------------------

RefResult NOXSunLight::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	switch (message)
	{
		case REFMSG_CHANGE:
		{
		}
		break;
	}
	return REF_SUCCEED;
}

//------------------------------------------------------------------------------------------------------------------------

ObjLightDesc * NOXSunLight::CreateLightDesc(INode *n , BOOL forceShadow)				
{ 
	return new NOXObjLightDesc(n);
	return (ObjLightDesc *)NULL;
}

//------------------------------------------------------------------------------------------------------------------------

NOXSunLight::NOXSunLight()
{
	ADD_LOG_CONSTR("NOXSunLight Constructor");
	sunMesh = new Mesh();
	hParams = 0;
	buildMeshes();
	removeUI = false;
	creating = true;
	sunlight_pblock = NULL;

	NOXSunLightDesc.MakeAutoParamBlocks(this); 
}

//------------------------------------------------------------------------------------------------------------------------

NOXSunLight::~NOXSunLight()
{
	if (sunMesh)
		delete sunMesh;
	sunMesh = NULL;
}

//------------------------------------------------------------------------------------------------------------------------

def_visible_primitive(nox_create_sunsky, "nox_create_sunsky");
Value * nox_create_sunsky_cf(Value** arg_list, int count)
{
	ADD_LOG_PARTS_MAIN("nox_create_sunsky called from script");

	if (findExistingNOXSunLight(GetCOREInterface()->GetRootNode()))
	{
		ADD_LOG_PARTS_MAIN("Sunsky already exists - won't create");
		return &false_value;
	}

	NOXSunLight * sunObject = new NOXSunLight();
	INode * sunNode = GetCOREInterface()->CreateObjectNode(sunObject); 

	IObjCreate *createInterface = GetCOREInterface()->GetIObjCreate();
	NOXSunTarget * targObject = new NOXSunTarget;
	INode * targNode = createInterface->CreateObjectNode( targObject);

	createInterface->BindToTarget(sunNode,targNode);			

	return &true_value;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::updateFromParamBlock()
{
	BOOL b;
	int ii;
	char buf[256];
	ADD_LOG_PARTS_MAIN("udpating from param block stuff");
	TimeValue ctime = GetCOREInterface()->GetTime();

	b = sunlight_pblock->GetInt(slp_sun_on, ctime, 0);
	sprintf_s(buf, 256, "sun is %s", b?"on":"off");
	ADD_LOG_PARTS_MAIN(buf);
	trsunsky.setSunON(b ? true : false);

	b = sunlight_pblock->GetInt(slp_sky_on, ctime, 0);
	sprintf_s(buf, 256, "sky is %s", b?"on":"off");
	ADD_LOG_PARTS_MAIN(buf);
	trsunsky.setSunSkyON(b ? true : false);

	ii = sunlight_pblock->GetInt(slp_sky_model, ctime, 0);
	sprintf_s(buf, 256, "sky model is %d", ii);
	ADD_LOG_PARTS_MAIN(buf);
	trsunsky.setModel(ii);

	b = sunlight_pblock->GetInt(slp_env_on, ctime, 0);
	sprintf_s(buf, 256, "env is %s", b?"on":"off");
	ADD_LOG_PARTS_MAIN(buf);
	trsunsky.setEnvEnabled(b ? true : false);

	const TCHAR * tx1t = sunlight_pblock->GetStr(slp_env_tex, ctime, 0);
	char * tx1a = getAnsiCopy1(tx1t);
	sprintf_s(buf, 256, "env tex is %s", tx1a);
	ADD_LOG_PARTS_MAIN(buf);
	trsunsky.setEnvTexFilename(tx1a);
	if (tx1a)
		free(tx1a);

	GetCOREInterface()->RedrawViews(GetCOREInterface()->GetTime());
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::updateToParamBlock()
{
	Color c;
	BOOL b;
	int ii;

	char buf[256];
	ADD_LOG_PARTS_MAIN("udpating to param block stuff");
	TimeValue ctime = GetCOREInterface()->GetTime();

	b = trsunsky.getSunON();
	sunlight_pblock->SetValue(slp_sun_on, ctime, b, 0);
	sprintf_s(buf, 256, "setting paramblock sun %s", b?"on":"off");
	ADD_LOG_PARTS_MAIN(buf);

	b = trsunsky.getSunSkyON();
	sunlight_pblock->SetValue(slp_sky_on, ctime, b, 0);
	sprintf_s(buf, 256, "setting paramblock sky %s", b?"on":"off");
	ADD_LOG_PARTS_MAIN(buf);

	ii = trsunsky.getModel();
	sunlight_pblock->SetValue(slp_sky_model, ctime, ii, 0);
	sprintf_s(buf, 256, "setting paramblock sky model %d", ii);
	ADD_LOG_PARTS_MAIN(buf);

	b = trsunsky.getEnvEnabled();
	sunlight_pblock->SetValue(slp_env_on, ctime, b, 0);
	sprintf_s(buf, 256, "setting paramblock env %s", b?"on":"off");
	ADD_LOG_PARTS_MAIN(buf);

	char * envtex = trsunsky.getEnvTexFilename();
	sunlight_pblock->SetValue(slp_env_tex, ctime, b, 0);
	sprintf_s(buf, 256, "setting paramblock env tex %s", envtex);
	ADD_LOG_PARTS_MAIN(buf);
}

//------------------------------------------------------------------------------------------------------------------------

def_visible_primitive(nox_update_sunsky_from_paramblock, "nox_update_sunsky_from_paramblock");
Value * nox_update_sunsky_from_paramblock_cf(Value** arg_list, int count)
{
	ADD_LOG_PARTS_MAIN("nox_update_sunsky_from_paramblock called from script");
	if (count!=1)
	{
		ADD_LOG_PARTS_MAIN("... with no argument, abort");
		return &false_value;
	}
	
	INode * inode= arg_list[0]->to_node();
	if (!inode)
	{
		ADD_LOG_PARTS_MAIN("  ... with non-node argument, abort");
		return &false_value;
	}

	ObjectState objSt = inode->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
	{
		ADD_LOG_PARTS_MAIN("  ... argument node has no object, abort");
		return &false_value;
	}

	NOXSunLight * sobj = NULL;
	if (objSt.obj && objSt.obj->CanConvertToType(NOXSunLight_CLASS_ID))
		sobj = (NOXSunLight*)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);

	if (!sobj)
	{
		ADD_LOG_PARTS_MAIN("  ... with non-nox-sunsky in argument node, abort");
		return &false_value;
	}

	sobj->updateFromParamBlock();

	return &true_value;
}

//------------------------------------------------------------------------------------------------------------------------

def_visible_primitive(nox_update_sunsky_to_paramblock, "nox_update_sunsky_to_paramblock");
Value * nox_update_sunsky_to_paramblock_cf(Value** arg_list, int count)
{
	ADD_LOG_PARTS_MAIN("nox_update_sunsky_to_paramblock called from script");
	if (count!=1)
	{
		ADD_LOG_PARTS_MAIN("... with no argument, abort");
		return &false_value;
	}
	
	INode * inode= arg_list[0]->to_node();
	if (!inode)
	{
		ADD_LOG_PARTS_MAIN("  ... with non-node argument, abort");
		return &false_value;
	}

	ObjectState objSt = inode->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
	{
		ADD_LOG_PARTS_MAIN("  ... argument node has no object, abort");
		return false;
	}

	NOXSunLight * sobj = NULL;
	if (objSt.obj && objSt.obj->CanConvertToType(NOXSunLight_CLASS_ID))
		sobj = (NOXSunLight*)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);

	if (!sobj)
	{
		ADD_LOG_PARTS_MAIN("  ... with non-nox-sunsky in argument node, abort");
		return &false_value;
	}

	sobj->updateToParamBlock();

	return &true_value;
}

//------------------------------------------------------------------------------------------------------------------------

INode * findExistingNOXSunLight(INode * node)
{
	for (int i=0; i<node->NumberOfChildren(); i++)
	{
		INode * nn = node->GetChildNode(i);
		if (!nn)
			continue;
		ObjectState os = nn->EvalWorldState(GetCOREInterface()->GetTime());

		if (os.obj->ClassID()==NOXSunLight_CLASS_ID  &&  os.obj->SuperClassID()==LIGHT_CLASS_ID)
			return nn;
	}
	return NULL;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::buildMeshes()
{
	float R = 18;		// sun radius
	int nX = 12;		// phi segments num
	int nY = 7;			// theta segments num
	float pR = 35;		// sunray end radius
	float pr = 25;		// sunray start radius
	int p = nX/2;		// num sunrays
	int nverts = nX*nY;
	int nfaces = nX*(nY-1)*2;
	sunMesh->setNumVerts(nverts + 3*p);
	sunMesh->setNumFaces(nfaces + p);
	for (int y=0; y<nY; y++)
		for (int x=0; x<nX; x++)
		{
			float phi = x/(float)nX*2*PI;
			float theta = y/(float)(nY-1)*PI;
			float sintheta = sin(theta);
			float costheta = cos(theta);
			float sinphi = sin(phi);
			float cosphi = cos(phi);
			sunMesh->setVert(x+y*nX , Point3(R*sintheta*cosphi, R*sintheta*sinphi, R*costheta));
		}
	for (int y=0; y<nY-1; y++)
		for (int x=0; x<nX; x++)
		{
			int c = x+y*nX;
			sunMesh->faces[2*c+0].setVerts(
				x+(y+1)*nX,
				(x+1)%nX+y*nX,
				x+y*nX);
			sunMesh->faces[2*(x+y*nX)+1].setVerts(
				x+(y+1)*nX,
				(x+1)%nX+(y+1)*nX,
				(x+1)%nX+y*nX);
		}
	for (int i=0; i<nfaces; i++) {
		sunMesh->faces[i].setSmGroup(0);
		sunMesh->faces[i].setEdgeVisFlags(i%2, 1, (i+1)%2);
	}

	for (int i=0; i<p; i++)
	{
		float phi1 = (i*2+0   )/(float)nX*2*PI;
		float phi2 = (i*2+1   )/(float)nX*2*PI;
		float phi3 = (i*2+0.5f)/(float)nX*2*PI;
		float sinphi1 = sin(phi1);
		float cosphi1 = cos(phi1);
		float sinphi2 = sin(phi2);
		float cosphi2 = cos(phi2);
		float sinphi3 = sin(phi3);
		float cosphi3 = cos(phi3);
		sunMesh->setVert(nverts+i*3+0, Point3(pr*cosphi1, pr*sinphi1, 0.0f));
		sunMesh->setVert(nverts+i*3+1, Point3(pr*cosphi2, pr*sinphi2, 0.0f));
		sunMesh->setVert(nverts+i*3+2, Point3(pR*cosphi3, pR*sinphi3, 0.0f));

		sunMesh->faces[nfaces+i].setVerts(
				nverts+i*3+0,
				nverts+i*3+1,
				nverts+i*3+2);
		sunMesh->faces[nfaces+i].setSmGroup(0);
		sunMesh->faces[nfaces+i].setEdgeVisFlags(1,1,1);
	}

	sunMesh->buildNormals();
	sunMesh->EnableEdgeList(1);
}

//------------------------------------------------------------------------------------------------------------------------

static void RemoveScaling(Matrix3 &tm) 
{
	AffineParts ap;
	decomp_affine(tm, &ap);
	tm.IdentityMatrix();
	tm.SetRotate(ap.q);
	tm.SetTrans(ap.t);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::GetMat(TimeValue t, INode* inode, ViewExp *vpt, Matrix3& tm) 
{
	tm = inode->GetObjectTM(t);
	RemoveScaling(tm);
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(tm.GetTrans())/(float)360.0;
	tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	if (!sunMesh)
		return;
	Point3 loc = inode->GetObjectTM(t).GetTrans();
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(loc) / 360.0f;
	box = sunMesh->getBoundingBox();//same as GetDeformBBox
	box.Scale(scaleFactor);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	if (!sunMesh)
		return;

	UpdateTargDistance(t, inode);
	holdTM();

	int nv;
	Matrix3 tm, ttm;
	GetMat(t, inode,vpt,tm);
	Point3 loc = tm.GetTrans();
	nv = sunMesh->getNumVerts();
	box.Init();
	box.IncludePoints(sunMesh->verts,nv,&tm);
	
	inode->GetTargetTM(t, ttm);
	Point3 pt = ttm.GetTrans();
	tm = IdentityTM();
	box.IncludePoints(&pt, 1, &tm);

	updateUIAltitudeAzimuth();
	updateUIPositions();
	//if (tDist > 100)
	//	theHold.Cancel();
}

//------------------------------------------------------------------------------------------------------------------------

int NOXSunLight::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) 
{
	HitRegion hitRegion;
	DWORD savedLimits;
	int res = 0;
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();	
	Material *mtl = gw->getMaterial();
	MakeHitRegion(hitRegion,type,crossing,4,p);	
	gw->setRndLimits( ((savedLimits = gw->getRndLimits()) | GW_PICK) & ~(GW_ILLUM|GW_BACKCULL));
	GetMat(t,inode,vpt,m);
	gw->setTransform(m);
	// if we get a hit on the mesh, we're done
	res = sunMesh->select( gw, mtl, &hitRegion, flags & HIT_ABORTONHIT);
	// if not, check the target line, and set the pair flag if it's hit
	if( !res )	{
		// this special case only works with point selection of targeted lights
		if((type != HITTYPE_POINT) || !inode->GetTarget())
			return 0;
		// don't let line be active if only looking at selected stuff and target isn't selected
		if((flags & HIT_SELONLY) && !inode->GetTarget()->Selected() )
			return 0;
		gw->clearHitCode();
	}
	gw->setRndLimits(savedLimits);
	return res;
}

//------------------------------------------------------------------------------------------------------------------------

int NOXSunLight::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) 
{
	UpdateTargDistance(t, inode);
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();
	GetMat(t,inode,vpt,m);
	gw->setTransform(m);
	DWORD rlim = gw->getRndLimits();
	gw->setRndLimits(GW_WIREFRAME|GW_EDGES_ONLY|GW_BACKCULL|(gw->getRndMode() & GW_Z_BUFFER));
	if (inode->Selected())
		gw->setColor( LINE_COLOR, GetSelColor());
	else 
		if(!inode->IsFrozen() && !inode->Dependent())	
		{
			Color color(inode->GetWireColor());
			gw->setColor( LINE_COLOR, color );
		}
	sunMesh->render( gw, gw->getMaterial(),
		(flags&USE_DAMAGE_RECT) ? &vpt->GetDammageRect() : NULL, COMP_ALL);	

	if (!hiddenTarg)
	{
		gw->setTransform(inode->GetObjectTM(t));
		DrawTargetLine(t, gw, tDist, COLOR_CAMERA_CLIP);
	}
	gw->setRndLimits(rlim);
	return 0 ;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::UpdateTargDistance(TimeValue t, INode* inode)
{
	Point3 pt,v[2];
	Matrix3 tmat;
	if (inode->GetTargetTM(t,tmat)) 
	{
		pt = tmat.GetTrans();
		Matrix3 tm = inode->GetObjectTM(t);
		float den = Length(tm.GetRow(2));
		tDist = (den!=0)?Length(tm.GetTrans()-pt)/den : 0.0f;
	}
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::DrawTargetLine(TimeValue t, GraphicsWindow *gw, float dist, int colid)
{
	Point3 u[2];
	gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_CONE));
	u[0] = Point3(0.0f, 0.0f, 0.0f);
	u[1] = Point3(0.0f, 0.0f, -dist);
	gw->polyline( 2, u, NULL, NULL, FALSE, NULL );
}

//------------------------------------------------------------------------------------------------------------------------

NOXSunTarget * NOXSunLight::getTargetObject()
{
	INode * inode = NULL;
	INode * tnode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return NULL;
	tnode = inode->GetTarget();
	if (!tnode)
		return NULL;

	ObjectState objSt = tnode->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return NULL;

	if (objSt.obj->SuperClassID() != TARGET_CLASS_ID)
		return NULL;

	if (objSt.obj && objSt.obj->CanConvertToType(NOXSunTarget_CLASS_ID))
	{
		NOXSunTarget * tobj = (NOXSunTarget *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunTarget_CLASS_ID);
		return tobj;
	}

	return NULL;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::setTargetHidden(bool hide)
{
	hiddenTarg = hide;
	trsunsky.targetHidden = hide;
	NOXSunTarget * nst = getTargetObject();
	if (nst)
		nst->amIHidden = hide;
	
	TimeValue t = GetCOREInterface()->GetTime();
	GetCOREInterface()->ForceCompleteRedraw();
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::setEnvHidden(bool hide)
{
	hiddenEnvMap = hide;
	trsunsky.envMapHidden = hide;
	NOXSunTarget * nst = getTargetObject();
	if (nst)
		nst->envMapHidden = hide;
	
	TimeValue t = GetCOREInterface()->GetTime();
	GetCOREInterface()->ForceCompleteRedraw();
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::updateUIAltitudeAzimuth()
{
	HWND hAlt = GetDlgItem(hParams, IDC_NOX_SUNSKY_ALTITUDE);
	HWND hAzi = GetDlgItem(hParams, IDC_NOX_SUNSKY_AZIMUTH);

	float alt, azi;
	evalAltitudeAzimuth(alt, azi);
	int ialt = (int)(180.0f*alt/PI);
	int iazi = (int)(180.0f*azi/PI);
	TCHAR buf[64];
	_stprintf_s(buf, 64, _T("%d�"), (ialt));
	SetWindowText(hAlt, buf);
	_stprintf_s(buf, 64, _T("%d�"), iazi);
	SetWindowText(hAzi, buf);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::updateUIPositions()
{
	HWND hSx = GetDlgItem(hParams, IDC_NOX_SUNSKY_SUN_X);
	HWND hSy = GetDlgItem(hParams, IDC_NOX_SUNSKY_SUN_Y);
	HWND hSz = GetDlgItem(hParams, IDC_NOX_SUNSKY_SUN_Z);
	HWND hTx = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_X);
	HWND hTy = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_Y);
	HWND hTz = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_Z);

	Point3 pos, tpos;
	if (!getPositions(pos, tpos))
		return;

	TCHAR buf[64];
	_stprintf_s(buf, 64, _T("%.2f"), pos.x);
	SetWindowText(hSx, buf);
	_stprintf_s(buf, 64, _T("%.2f"), pos.y);
	SetWindowText(hSy, buf);
	_stprintf_s(buf, 64, _T("%.2f"), pos.z);
	SetWindowText(hSz, buf);
	_stprintf_s(buf, 64, _T("%.2f"), tpos.x);
	SetWindowText(hTx, buf);
	_stprintf_s(buf, 64, _T("%.2f"), tpos.y);
	SetWindowText(hTy, buf);
	_stprintf_s(buf, 64, _T("%.2f"), tpos.z);
	SetWindowText(hTz, buf);
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunLight::getPositions(Point3 &pos, Point3 &tpos)
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return false;

	Matrix3 tmat;
	int rr = inode->GetTargetTM(GetCOREInterface()->GetTime(), tmat);
	if (!rr)
		return false;

	pos = inode->GetNodeTM(GetCOREInterface()->GetTime()).GetTrans();
	tpos = tmat.GetTrans();
	return true;
}

//------------------------------------------------------------------------------------------------------------------------

Point3 myNormalize(Point3 a)
{
	float d = a.x*a.x + a.y*a.y + a.z*a.z;
	if (d > 0)
	{
		d = (float)(1.0/sqrt(d));
		a *= d;
	}
	return a;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunLight::evalAltitudeAzimuth(float &alt, float &azi)
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return NULL;

	Matrix3 mat, tmat;
	TimeValue t = GetCOREInterface()->GetTime();
	inode->GetTargetTM(t, tmat);
	mat = inode->GetNodeTM(t);

	Point3 orig = mat.GetTrans();
	Point3 origT = tmat.GetTrans();
	Point3 toSun = orig - origT;
	toSun = myNormalize(toSun);


	mat.NoTrans();
	mat.NoScale();
	tmat.NoTrans();
	tmat.NoScale();

	Point3 dirN  = Point3(0,-1,0) * tmat;
	Point3 dirS  = Point3(0, 1,0) * tmat;
	Point3 dirW  = Point3( 1,0,0) * tmat;
	Point3 dirE  = Point3(-1,0,0) * tmat;
	Point3 dirUp = Point3( 0,0,1) * tmat;

	float costh = dirUp % toSun;	// dot
	Point3 toSun_ = toSun - dirUp*costh;
	toSun_ = myNormalize(toSun_);
	float x,y;
	x = dirS % toSun_;
	y = dirW % toSun_;
	float tt = atan2(y,x) + PI;

	alt = PI/2 - acos(costh);
	azi = tt;

	return true;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunLight::setTMfromAltAzi(float alt, float azi)
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return false;

	Matrix3 mat, tmat;
	TimeValue t = GetCOREInterface()->GetTime();
	inode->GetTargetTM(t, tmat);
	mat = inode->GetNodeTM(t);
	Point3 tpos = tmat.GetTrans();

	tmat.NoTrans();
	tmat.NoScale();

	Point3 dirN  = Point3(0,-1,0) * tmat;
	Point3 dirS  = Point3(0, 1,0) * tmat;
	Point3 dirW  = Point3( 1,0,0) * tmat;
	Point3 dirE  = Point3(-1,0,0) * tmat;
	Point3 dirUp = Point3( 0,0,1) * tmat;

	if (_isnan(azi)  ||  _isnan(alt))
		MessageBox(0, _T("Azimuth correction fail."), _T("Error"), 0);
	Point3 toSun, toSun_;
	toSun_ = sin(azi)*dirE + cos(azi)*dirN;
	toSun_ = myNormalize(toSun_);
	toSun = cos(alt)*toSun_ + sin(alt)*dirUp;
	toSun = myNormalize(toSun);
	toSun *= tDist;
	toSun = toSun + tpos;
	mat.SetTrans(toSun);

	inode->SetNodeTM(t, mat);

	return true;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::selectTarget()
{
	INode * inode = NULL;
	INode * tnode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return;
	tnode = inode->GetTarget();
	if (!tnode)
		return;

	GetCOREInterface()->SelectNode(tnode);
	GetCOREInterface()->ForceCompleteRedraw();
}

//------------------------------------------------------------------------------------------------------------------------

void * NOXSunLightClassDesc::Create(BOOL loading)
{
	return new NOXSunLight(); 
}

void NOXSunLight::DeleteThis() 
{
	delete this; 
}

//------------------------------------------------------------------------------------------------------------------------

int NOXSunLight::countSuns()
{
	numSuns = 0;
	SceneEnumForSun sEnum(NULL);
	sEnum.callback(GetCOREInterface()->GetRootNode());
	return numSuns;
}

//------------------------------------------------------------------------------------------------------------------------

int SceneEnumForSun::callback(INode *  node)
{
	int i;
	for (i=0; i<node->NumberOfChildren(); i++)
	{
		callback(node->GetChildNode(i));
	}
	
	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return TREE_ABORT;

	if (objSt.obj && objSt.obj->CanConvertToType(NOXSunLight_CLASS_ID))
		NOXSunLight::numSuns++;

	return TREE_CONTINUE;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunLight::evalSunDir(float &x, float &y, float &z)
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return false;

	Matrix3 matr, tmatr, tempmatr;
	TimeValue t = GetCOREInterface()->GetTime();
	inode->GetTargetTM(t, tmatr);
	matr = inode->GetNodeTM(t);
	
	tempmatr = matr;
	tempmatr.NoRot();
	tempmatr.NoScale();
	Point3 sPos = tempmatr.GetTrans();

	tempmatr = tmatr;
	tempmatr.NoRot();
	tempmatr.NoScale();
	Point3 tPos = tempmatr.GetTrans();

	Point3 sDir = sPos - tPos;
	sDir = myNormalize(sDir);

	Matrix3 itmatr = tmatr;
	itmatr.NoTrans();
	itmatr.NoScale();
	itmatr.Invert();
	sDir = itmatr * sDir;

	x = sDir.x;
	y = sDir.y;
	z = sDir.z;

	return true;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunLight::holdTM()
{

	INode * inode = NULL;
	INode * tnode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return false;
	tnode = inode->GetTarget();
	if (!tnode)
		return false;

	// node selected stuff
	bool meSelected = amISelected();
	NOXSunTarget* targ = getTargetObject();
	bool targSelected;
	if (targ)
		targSelected = targ->amISelected();
	else
		targSelected = false;

	if (!targSelected  &&  !meSelected)
		return false;

	// get altitude and azimuth
	float alt, azi;
	evalAltitudeAzimuth(alt, azi);

	// correct azimuth if needed
	bool changed = trsunsky.correctDirection(alt, azi);
	if (changed)
	{
		if (creating)
			targ->setTMfromAltAzi(alt,azi);
		else
			if (meSelected)
				setTMfromAltAzi(alt, azi);
			else
				if (targSelected)
					targ->setTMfromAltAzi(alt,azi);
				else
					return false;
	}

	return changed;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunLight::amISelected()
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return false;
	if (inode->Selected())
		return true;
	else
		return false;
}

//------------------------------------------------------------------------------------------------------------------------

int NOXObjLightDesc::Update(TimeValue t, const RendContext& rc, RenderGlobalContext *rgc, BOOL shadows, BOOL shadowGeomChanged)
{
	if (inode) {
		Interval valid;
		ObjectState os = inode->EvalWorldState(t);
		assert(os.obj->SuperClassID()==LIGHT_CLASS_ID);
		LightObject* lob = (LightObject *)os.obj;
		lob->EvalLightState(t, valid, &ls);
		lightToWorld = inode->GetObjTMAfterWSM(t);
		worldToLight = Inverse(lightToWorld);
		affectDiffuse = ls.affectDiffuse;
		affectSpecular = ls.affectSpecular;
		ambientOnly = ls.ambientOnly;
	}
	else {
		lightToWorld.IdentityMatrix();
		worldToLight.IdentityMatrix();
	}
	return 1;
}

//------------------------------------------------------------------------------------------------------------------------

