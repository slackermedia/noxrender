#include <windows.h>
#include "resource.h"
#include "MatEditor.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <math.h>
#include "log.h"
#include "valuesMinMax.h"
#include "MaterialArchiver.h"
#include <curl/curl.h>
#include "XML_IO.h"
#include "ies.h"

extern HMODULE hDllModule;
void updateLinked90Controls(HWND hWnd);

//------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK MatEditorWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int mat_mode = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				mat_mode = (int)lParam;
				hMain = hWnd;
				backgroundBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.WindowBackground);
				#ifdef _WIN64
					SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG_PTR)(backgroundBrush));
				#else
					SetClassLong(hWnd, GCL_HBRBACKGROUND, (LONG)(LONG_PTR)(backgroundBrush));
				#endif
				Logger::add("Material Editor window created.");
	
				curl_global_init(CURL_GLOBAL_ALL); 
				applyControlStyles();
				postWindowOpen();
				unlockAfterStop();
				showLibrary(false);
				onlinematlib.createTempDir();
				localmatlib.createMaterialsList();

				HWND hPr = GetDlgItem(hMain, IDC_MATEDIT_PREVIEW);
				EMPView * empv = GetEMPViewInstance(hPr);
				empv->allowDragFiles();

				autoCategory = MatCategory::CAT_USER;
				autoID = 0;

				if (autoLoadFile)
				{
					loadMaterial(autoLoadFile);
				}

				Raytracer::getInstance()->curScenePtr->texManager.logTextures();
				updateLinked90Controls(hWnd);
				needToUpdateDisplacement = true;
			}
			break;
		case WM_DESTROY:
			{
				curl_global_cleanup();
				Logger::add("Cleaning up online material library temporary files...");
				onlinematlib.deleteAllTempFiles(true);
				releaseMatLibBuffers();

				if (mat_mode == MatEditorWindow::MODE_ALONE)
				{
					Logger::add("Closing Material Editor window.");
					Logger::closeFile();
					PostQuitMessage(0);
				}
			}
			break;
		case WM_CLOSE:
			{
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				HBRUSH hBr1 = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackgroundSecond);
				DeleteObject(SelectObject(hdc, hBr1));
				HPEN hPen1 = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackgroundSecond);
				DeleteObject(SelectObject(hdc, hPen1));
				Rectangle(hdc, 213, 192, 302, 510);
				Rectangle(hdc, 490, 32,  564, 510);

				// refl 0/90 connect
				MoveToEx(hdc, 622, 70, NULL);
				LineTo(hdc, 630, 70);
				LineTo(hdc, 630, 93);
				LineTo(hdc, 621, 93);
				MoveToEx(hdc, 699, 70, NULL);
				LineTo(hdc, 691, 70);
				LineTo(hdc, 691, 93);
				LineTo(hdc, 700, 93);
				MoveToEx(hdc, 630, 82, NULL);
				LineTo(hdc, 638, 82);
				MoveToEx(hdc, 691, 82, NULL);
				LineTo(hdc, 683, 82);

				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));



				EndPaint(hWnd, &ps);
			}
			break;

		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_MATEDIT_OK:						//-------------------------------------------------------------------------
						{
							if(wmEvent != BN_CLICKED)
								break;

							GUInameToMaterial();

							if (mat_mode==MODE_ALONE  &&  saveOnOK)
							{
								if (autoLoadFile)
								{
									copyBufferWindowToMaterial();
									XMLScene xscene;
									Raytracer * rtr = Raytracer::getInstance();
									bool saved = xscene.saveMaterial(autoLoadFile, rtr->curScenePtr->mats[0]);
								}
							}

							acceptMaterial();
							resetMaterialDelTextures();		// must be done before changing scene

							EndDialog(hWnd, (INT_PTR)1);
						}
						break;
					case IDC_MATEDIT_CANCEL:					//-------------------------------------------------------------------------
						{
							if(wmEvent != BN_CLICKED)
								break;
							resetMaterialDelTextures();
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;
					case IDC_MATEDIT_PREVIEW:					//-------------------------------------------------------------------------
						{
							if (wmEvent==PV_DROPPED_FILE)
							{
								HWND hPr = GetDlgItem(hMain, IDC_MATEDIT_PREVIEW);
								EMPView * empv = GetEMPViewInstance(hPr);
								if (empv->lastDroppedFilename)
								{
									bool loadOK = loadMaterial(empv->lastDroppedFilename);
									if (!loadOK)
									{
										MessageBox(hMain, "Loading failed. Probably not a material file.", "", 0);
									}
								}
								if (empv->lastDroppedFilename)
									free(empv->lastDroppedFilename);
								empv->lastDroppedFilename = NULL;
							}
						}
					case IDC_MATEDIT_START:						//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							bool canRender = true;
							if (needToUpdateDisplacement)
								canRender = updateDisplacement();
							needToUpdateDisplacement = false;
							if (canRender)
								startRendering();
						}
						break;
					case IDC_MATEDIT_STOP:						//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							stopRendering();
						}
						break;
					case IDC_MATEDIT_REFRESH:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							refresh();
						}
						break;
					case IDC_MATEDIT_ZOOM:						//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int res = 0;
							Raytracer * rtr = Raytracer::getInstance();

							bool canRender = true;
							if (needToUpdateDisplacement)
								canRender = updateDisplacement();
							needToUpdateDisplacement = false;
							if (!canRender)
								break;

							if (rtr->runDialogsUnicode)
							{
								res = (int)DialogBoxParamW(hDllModule,
											MAKEINTRESOURCEW(IDD_PREVIEW_DIALOG), hWnd, PreviewZoomDlgProc,(LPARAM)(NULL));
							}
							else
							{
								res = (int)DialogBoxParam(hDllModule,
											MAKEINTRESOURCE(IDD_PREVIEW_DIALOG), hWnd, PreviewZoomDlgProc,(LPARAM)(NULL));
							}
							if (res)
							{
								autoCategory = MatCategory::CAT_USER;
								autoID = 0;
								copyBufferMaterialToWindow();
							}
						}
						break;

					case IDC_MATEDIT_CHOOSE_SCENE:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EMComboBox * emcb = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_CHOOSE_SCENE));
							CHECK(emcb);
							int scNum = emcb->selected;

							updateMaterialToEditableMaterial();
							resetMaterialDelTextures();		// must be before changing scene
							changeScene(scNum);

							MaterialNox * mat = getEditedMaterial();
							mat->prSceneIndex = scNum;
							needToUpdateDisplacement = true;
						}
						break;


					case IDC_MATEDIT_UPLOADMAT:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							copyBufferWindowToMaterial();
							uploadMaterial();
						}
						break;
					case IDC_MATEDIT_LOADMAT:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							loadMaterial();
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_SAVEMAT:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							saveMaterial();
						}
						break;
					case IDC_MATEDIT_ARCHIVE_MAT:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							copyBufferWindowToMaterial();
							char * toSave = saveFileDialog(hMain, "7-zip file (*.7z)\0*.7z\0", "Save archived material", "7z", 1);
							if (toSave)
							{
								MatArchiver mArch;
								bool res = mArch.archiveMaterial(getEditedMaterial(), toSave);
								if (res)
									MessageBox(hMain, "Successfully archived.", "Archived", 0);
								else
									MessageBox(hMain, "Error. Not archived.\n", "Error", 0);
							}
						}
						break;
					case IDC_MATEDIT_NAME:						//-------------------------------------------------------------------------
						{
							CHECK(wmEvent == ES_RETURN  ||  wmEvent == ES_LOST_FOCUS);
							GUInameToMaterial();
						}
						break;

					case IDC_MATEDIT_LAYERS_LIST:				//-------------------------------------------------------------------------
						{
							if (wmEvent == LV_SELECTION_CHANGED)
							{
								int n = getEditedLayerNumber();
								layerToGUI(n);
							}
						}
						break;
					case IDC_MATEDIT_LAYER_ADD:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							layerAdd();
							autoCategory = MatCategory::CAT_USER;
							autoID = 0;
						}
						break;
					case IDC_MATEDIT_LAYER_DEL:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							layerDel();
							autoCategory = MatCategory::CAT_USER;
							autoID = 0;
						}
						break;
					case IDC_MATEDIT_LAYER_CLONE:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							layerClone();
							autoCategory = MatCategory::CAT_USER;
							autoID = 0;
						}
						break;
					case IDC_MATEDIT_LAYER_RESET:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							mlay->resetValues(true);
							int n = getEditedLayerNumber();
							layerToGUI(n);
							fillLayersList();
							autoCategory = MatCategory::CAT_USER;
							autoID = 0;
						}
						break;

					case IDC_MATEDIT_WEIGHT_TEX_ON:				//-------------------------------------------------------------------------
					case IDC_MATEDIT_WEIGHT:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL   ||   wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_MATEDIT_WEIGHT));
							CHECK(emlv);
							CHECK(emes);
							bool tex = getEditedMatLayer()->use_tex_weight  &&  getEditedMatLayer()->tex_weight.isValid();
							char buf[64];
							if (tex)
								sprintf_s(buf, 64, "tex");
							else
								sprintf_s(buf, 64, "%d", emes->intValue);
							emlv->setData(1, n, buf);
							InvalidateRect(emlv->hwnd, NULL, false);
						}
						break;
					case IDC_MATEDIT_WEIGHT_TEX:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_weight);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;
								t->texMod = texres->texMod;
								mlay->use_tex_weight = texres->bmp.isValid();
								texres->texMod.gamma = 1.0f;		// pixel value counts
								texres->texMod.updateGamma(1.0f);	// pixel value counts
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}
							
							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();

							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_MATEDIT_WEIGHT));
							bool tex_on = getEditedMatLayer()->use_tex_weight  &&  getEditedMatLayer()->tex_weight.isValid();
							char buf[64];
							if (tex_on)
								sprintf_s(buf, 64, "tex");
							else
								sprintf_s(buf, 64, "%d", emes->intValue);
							emlv->setData(1, n, buf);
							InvalidateRect(emlv->hwnd, NULL, false);

						}
						break;

					case IDC_MATEDIT_DISPL_DEPTH:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DISPL_SUBDIVS:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DISPL_CONTINUITY:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DISPL_SHIFT:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DISPL_NORMALS:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DISPL_IGNORE_SCALE:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DISPL_ON:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DISPL_TEX:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MaterialNox * mat = getEditedMaterial();
							CHECK(mat);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mat->tex_displacement);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								mat->use_tex_displacement = texres->bmp.isValid();
								texres->texMod.gamma = 1.0f;		// pixel value counts
								texres->texMod.updateGamma(1.0f);	// pixel value counts
								texres->freeAllBuffers();
								delete texres;

								needToUpdateDisplacement = true;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;


					case IDC_MATEDIT_NORMAL_TEX:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_normal);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->normMod = t->normMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
											MAKEINTRESOURCEW(IDD_NORMALMAP_DIALOG), hWnd, NormalDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
											MAKEINTRESOURCE(IDD_NORMALMAP_DIALOG), hWnd, NormalDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, true);
								t->managerID = id;

								t->normMod = texres->normMod;
								mlay->use_tex_normal = texres->bmp.isValid();
								texres->texMod.gamma = 1.0f;		// pixel value counts
								texres->texMod.updateGamma(1.0f);	// pixel value counts
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;
					case IDC_MATEDIT_NORMAL_ON:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_EMISSION_ON:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_EMISSION_ON));
							EMCheckBox * emcb2 = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_EMISSION_SKYPORTAL));
							CHECK(emlv);
							CHECK(emcb);
							CHECK(emcb2);
							if (emcb->selected  ||  emcb2->selected)
								emlv->setData(3, n, "x");
							else
								emlv->setData(3, n, "-");
							InvalidateRect(emlv->hwnd, NULL, false);

							Raytracer * rtr = Raytracer::getInstance();
							rtr->curScenePtr->updateSkyportals();
							if (!rtr->curScenePtr->updateLightMeshes())
								MessageBox(hMain, "Watch out - there is no emitter on scene.", "Warning",0);

						}
						break;
					case IDC_MATEDIT_EMISSION_SKYPORTAL:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_EMISSION_ON));
							EMCheckBox * emcb2 = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_EMISSION_SKYPORTAL));
							CHECK(emlv);
							CHECK(emcb);
							CHECK(emcb2);
							if (emcb->selected  ||  emcb2->selected)
								emlv->setData(3, n, "x");
							else
								emlv->setData(3, n, "-");
							InvalidateRect(emlv->hwnd, NULL, false);

							Raytracer * rtr = Raytracer::getInstance();
							rtr->curScenePtr->updateSkyportals();
							if (!rtr->curScenePtr->updateLightMeshes())
								MessageBox(hMain, "Watch out - there is no emitter on scene.", "Warning",0);
						}
						break;
					case IDC_MATEDIT_EMISSION_COLOR:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CS_IGOT_DRAG);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_EMISSION_COLOR_RGB:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);
							Color4 c = mlay->color;
							Color4 * res = NULL;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								res = (Color4 *)DialogBoxParamW(hDllModule,
										MAKEINTRESOURCEW(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							else
							{
								res = (Color4 *)DialogBoxParam(hDllModule,
										MAKEINTRESOURCE(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							if (res)
							{	// valid pointer returned
								GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR))->redraw(*res);
								int n = getEditedLayerNumber();
								GUItoLayer(n);
							}
						}
						break;
					case IDC_MATEDIT_EMISSION_COLOR_TEMPERATURE://-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);
							int tt = mlay->emitterTemperature;
							int res = 0;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								res = (int)DialogBoxParamW(hDllModule, 
										MAKEINTRESOURCEW(IDD_TEMPERATURE_DIALOG), hWnd, TemperatureDlgProc,(LPARAM)( tt ));
							}
							else
							{
								res = (int)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_TEMPERATURE_DIALOG), hWnd, TemperatureDlgProc,(LPARAM)( tt ));
							}

							if (res)
							{	// valid pointer returned
								mlay->emitterTemperature = res;
								Color4 c = TemperatureColor::getGammaColor(res);
								GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_EMISSION_COLOR))->redraw(c);
								int n = getEditedLayerNumber();
								GUItoLayer(n);
							}
						}
						break;
					case IDC_MATEDIT_EMISSION_COLOR_TEX_ON:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_EMISSION_COLOR_TEX:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_light);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								mlay->use_tex_light = texres->bmp.isValid();
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;
					case IDC_MATEDIT_EMISSION_POWER:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							updateLightMeshes();
						}
						break;
					case IDC_MATEDIT_EMISSION_UNIT:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							updateLightMeshes();
						}
						break;
					case IDC_MATEDIT_EMISSION_BLEND_NAMES:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							updateLightMeshes();
						}
						break;
					case IDC_MATEDIT_IES_LOAD:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							char * fname = openIesFileDialog(hMain);
							if (fname)
							{
								MatLayer * mlay = getEditedMatLayer();
								IesNOX * ies = new IesNOX();
								bool ok = ies->parseFile(fname);
								if (!ok)
								{
									Logger::add("Parse failed... error:");
									Logger::add(ies->errorBuf);
									MessageBox(0, ies->errorBuf, "Parsing error", 0);
									delete ies;
								}
								else
								{
									if (mlay->ies)
									{
										mlay->ies->releaseStuff();
										delete mlay->ies;
									}
									mlay->ies = ies;
								}
								int n = getEditedLayerNumber();
								layerToGUI(n);	// lit ies button
							}
						}
						break;
					case IDC_MATEDIT_IES_UNLOAD:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							if (mlay->ies)
							{
								mlay->ies->releaseStuff();
								delete mlay->ies;
								mlay->ies = NULL;
							}
							int n = getEditedLayerNumber();
							layerToGUI(n);	// lit ies button
						}
						break;
					case IDC_MATEDIT_IES_PREVIEW:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();

							INT_PTR resIes = 0;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								resIes = DialogBoxParamW(hDllModule, 
										MAKEINTRESOURCEW(IDD_IES_PREVIEW), hWnd, IESDlgProc,(LPARAM)(mlay->ies));
							}
							else
							{
								resIes = DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_IES_PREVIEW), hWnd, IESDlgProc,(LPARAM)(mlay->ies));
							}
							if (resIes==0)	// OK, but no ies
							{
								if (mlay->ies)
								{
									mlay->ies->releaseStuff();
									delete mlay->ies;
									mlay->ies = NULL;
								}
							}
							else
								if (resIes==-1)	// Cancel
								{
								}
								else
								{	// OK, with ies
									if (mlay->ies)
									{
										mlay->ies->releaseStuff();
										delete mlay->ies;
									}
									mlay->ies = (IesNOX*)resIes;
								}
							int n = getEditedLayerNumber();
							layerToGUI(n);	// lit ies button
						}
						break;

					case IDC_MATEDIT_ROUGHNESS_TEX_ON:			//-------------------------------------------------------------------------
					case IDC_MATEDIT_ROUGHNESS:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL   ||   wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_MATEDIT_ROUGHNESS));
							CHECK(emlv);
							CHECK(emes);
							bool tex_ok = getEditedMatLayer()->use_tex_rough  &&  getEditedMatLayer()->tex_rough.isValid();
							char buf[64];
							if (tex_ok)
								sprintf_s(buf, 64, "tex");
							else
								sprintf_s(buf, 64, "%d", (int)(emes->floatValue+0.01f));
							emlv->setData(2, n, buf);
							InvalidateRect(emlv->hwnd, NULL, false);
						}
						break;
					case IDC_MATEDIT_REFL90_LINKED:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);

							updateLinked90Controls(hWnd);
						}
						break;
					case IDC_MATEDIT_REFL0_COLOR_RGB:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);
							Color4 c = mlay->refl0;
							Color4 * res = NULL;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								res = (Color4 *)DialogBoxParamW(hDllModule, 
										MAKEINTRESOURCEW(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							else
							{
								res = (Color4 *)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}

							if (res)
							{	// valid pointer returned
								GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL0_COLOR))->redraw(*res);
								int n = getEditedLayerNumber();
								GUItoLayer(n);
							}
						}
						break;
					case IDC_MATEDIT_REFL90_COLOR_RGB:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);
							Color4 c = mlay->refl90;
							Color4 * res = NULL;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								res = (Color4 *)DialogBoxParamW(hDllModule, 
										MAKEINTRESOURCEW(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							else
							{
								res = (Color4 *)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							if (res)
							{	// valid pointer returned
								GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_REFL90_COLOR))->redraw(*res);
								int n = getEditedLayerNumber();
								GUItoLayer(n);
							}
						}
						break;
					case IDC_MATEDIT_REFL0_COLOR_TEX_ON:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_REFL90_COLOR_TEX_ON:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_REFL0_COLOR:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CS_IGOT_DRAG);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_REFL90_COLOR:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CS_IGOT_DRAG);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_ANISOTROPY:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_ANISO_ANGLE:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_ANISOTROPY_TEX_ON:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_ANISO_ANGLE_TEX_ON:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;

					case IDC_MATEDIT_REFL0_COLOR_TEX:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_col0);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;
								Logger::add(texres->fullfilename);

								getEditedMaterial()->logMaterial();

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								t->texMod = texres->texMod;
								mlay->use_tex_col0 = texres->bmp.isValid();
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;
					case IDC_MATEDIT_REFL90_COLOR_TEX:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_col90);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								mlay->use_tex_col90 = texres->bmp.isValid();
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();

						}
						break;
					case IDC_MATEDIT_ROUGHNESS_TEX:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_rough);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								mlay->use_tex_rough = texres->bmp.isValid();
								texres->texMod.gamma = 1.0f;		// pixel value counts
								texres->texMod.updateGamma(1.0f);	// pixel value counts
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();

							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_MATEDIT_ROUGHNESS));
							CHECK(emlv);
							CHECK(emes);
							bool tex_ok = getEditedMatLayer()->use_tex_rough  &&  getEditedMatLayer()->tex_rough.isValid();
							char buf[64];
							if (tex_ok)
								sprintf_s(buf, 64, "tex");
							else
								sprintf_s(buf, 64, "%d", (int)(emes->floatValue+0.01f));
							emlv->setData(2, n, buf);
							InvalidateRect(emlv->hwnd, NULL, false);

						}
						break;

					case IDC_MATEDIT_ANISOTROPY_TEX:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_aniso);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								mlay->use_tex_aniso = texres->bmp.isValid();
								texres->texMod.gamma = 1.0f;		// pixel value counts
								texres->texMod.updateGamma(1.0f);	// pixel value counts
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;
					case IDC_MATEDIT_ANISO_ANGLE_TEX:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_aniso_angle);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								mlay->use_tex_aniso_angle = texres->bmp.isValid();
								texres->texMod.gamma = 1.0f;		// pixel value counts
								texres->texMod.updateGamma(1.0f);	// pixel value counts
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;

					case IDC_MATEDIT_IOR:						//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_MATEDIT_IOR));
							EMFresnel  * emfr = GetEMFresnelInstance( GetDlgItem(hWnd, IDC_MATEDIT_FRESNEL));
							CHECK(emes);
							CHECK(emfr);
							emfr->IOR = emes->floatValue;
							InvalidateRect(emfr->hwnd, NULL, false);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_FRESNEL:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							EMEditSpin * emes = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_MATEDIT_IOR));
							EMFresnel  * emfr = GetEMFresnelInstance( GetDlgItem(hWnd, IDC_MATEDIT_FRESNEL));
							CHECK(emes);
							CHECK(emfr);
							emes->setValuesToFloat(emfr->IOR,  NOX_MAT_IOR_MIN, NOX_MAT_IOR_MAX, 0.1f, 0.01f);
							InvalidateRect(emfr->hwnd, NULL, false);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;

					case IDC_MATEDIT_TRANSMISSION_ON:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_TRANSMISSION_ON));
							CHECK(emlv);
							CHECK(emcb);
							if (emcb->selected)
								emlv->setData(4, n, "x");
							else
								emlv->setData(4, n, "-");
							InvalidateRect(emlv->hwnd, NULL, false);
						}
						break;
					case IDC_MATEDIT_TRANSMISSION_TEX_ON:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_TRANSMISSION_COLOR_TEX:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);
							//Texture * t = &(mlay->tex_col0);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mlay->tex_transm);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;
								Logger::add(texres->fullfilename);

								getEditedMaterial()->logMaterial();

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								t->texMod = texres->texMod;
								mlay->use_tex_transm = texres->bmp.isValid();
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;
					case IDC_MATEDIT_DISPERSION_ON:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_FAKE_GLASS_ON:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_ABSORPTION_ON:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_ABSORPTION_COLOR:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CS_IGOT_DRAG);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_TRANSMISSION_COLOR:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==CS_IGOT_DRAG);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_TRANSMISSION_COLOR_RGB:	//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);
							Color4 c = mlay->transmColor;
							Color4 * res = NULL;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								res = (Color4 *)DialogBoxParamW(hDllModule, 
										MAKEINTRESOURCEW(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							else
							{
								res = (Color4 *)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							if (res)
							{
								GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_TRANSMISSION_COLOR))->redraw(*res);
								int n = getEditedLayerNumber();
								GUItoLayer(n);
							}
						}
						break;
					case IDC_MATEDIT_ABSORPTION_COLOR_RGB:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							MatLayer * mlay = getEditedMatLayer();
							CHECK(mlay);
							Color4 c = mlay->absColor;
							Color4 * res = NULL;
							Raytracer * rtr = Raytracer::getInstance();
							if (rtr->runDialogsUnicode)
							{
								res = (Color4 *)DialogBoxParamW(hDllModule, 
										MAKEINTRESOURCEW(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							else
							{
								res = (Color4 *)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_COLOR_DIALOG), hWnd, ColorDlgProc,(LPARAM)( &c ));
							}
							if (res)
							{
								GetEMColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT_ABSORPTION_COLOR))->redraw(*res);
								int n = getEditedLayerNumber();
								GUItoLayer(n);
							}
						}
						break;
					case IDC_MATEDIT_ABSORPTION_DISTANCE:		//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_DISPERSION_SIZE:			//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;

					case IDC_MATEDIT_OPACITY:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_OPACITY_TEX_ON:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_OPACITY_TEX:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);

							lockBeforeTextures();

							MaterialNox * mat = getEditedMaterial();
							CHECK(mat);

							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(mat->tex_opacity);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = NULL;
							if (rtr->runDialogsUnicode)
							{
								texres = (Texture *)DialogBoxParamW(hDllModule, 
													MAKEINTRESOURCEW(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}
							else
							{
								texres = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE_DIALOG), hWnd, TexDlgProc,(LPARAM)( tcopy ));
							}

							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								mat->use_tex_opacity = texres->bmp.isValid();
								texres->texMod.gamma = 1.0f;		// pixel value counts
								texres->texMod.updateGamma(1.0f);	// pixel value counts
								texres->freeAllBuffers();
								delete texres;
							}
							else
							{
							}

							int n = getEditedLayerNumber();
							layerToGUI(n);	// exceptionally other direction

							autoCategory = MatCategory::CAT_USER;
							autoID = 0;

							unlockAfterTextures();
						}
						break;

					case IDC_MATEDIT_SSS_ON:					//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							int n = getEditedLayerNumber();
							GUItoLayer(n);

							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_MATEDIT_LAYERS_LIST));
							EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_SSS_ON));
							CHECK(emlv);
							CHECK(emcb);
							if (emcb->selected)
								emlv->setData(5, n, "x");
							else
								emlv->setData(5, n, "-");
							InvalidateRect(emlv->hwnd, NULL, false);
						}
						break;
					case IDC_MATEDIT_SSS_DENSITY:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;
					case IDC_MATEDIT_SSS_COLLISION_DIR:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==WM_VSCROLL);
							int n = getEditedLayerNumber();
							GUItoLayer(n);
						}
						break;

					// LIBRARY ============================================================================================================
					case IDC_MATEDIT_SHOWHIDE_LIB:				//-------------------------------------------------------------------------
						{
							CHECK(wmEvent==BN_CLICKED);
							EMImgStateButton * ems = GetEMImgStateButtonInstance(GetDlgItem(hWnd, IDC_MATEDIT_SHOWHIDE_LIB));
							showLibrary(ems->stateOn);
							if (ems->stateOn)
							{
								clickedLibTrayOpen();
							}
						}
						break;
					case IDC_MATEDIT_ONLINE_SWITCH:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibOnline();
						}
						break;
					case IDC_MATEDIT_ADD_LOCALLIB:
						{
							CHECK(wmEvent==BN_CLICKED);
							localmatlib.saveToLibrary();
						}
						break;
					case IDC_MATEDIT_LOAD_LOCALLIB:
						{
							CHECK(wmEvent==BN_CLICKED);
							int which = findSelectedMatInLib();
							clickedTwiceLibPreview(which);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_DOWNLOADMAT:
						{
							CHECK(wmEvent==BN_CLICKED);
							int which = findSelectedMatInLib();
							clickedTwiceLibPreview(which);
							needToUpdateDisplacement = true;
						}
						break;
					case IDC_MATEDIT_LIB_RIGHT:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibNext();
						}
						break;
					case IDC_MATEDIT_LIB_LEFT:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibPrev();
						}
						break;
					case IDC_MATEDIT_LIB_PAGES:
						{
							CHECK(wmEvent==PG_CLICKED);
							clickedLibPage();
						}
						break;
					case IDC_MATEDIT_DEL_LOCALLIB:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(libMode==1);
							if (localmatlib.queryMode != LocalMatLib::MODE_CATEGORY)
							{
								MessageBox(0, "No deleting in search mode.", "Error", 0);
								break;
							}

							int which = findSelectedMatInLib();
							if (which>=0  &&  which<7)
								localmatlib.removeFromLibrary(localmatlib.currentCategory, localmatlib.currentPage*7+which);
						}
						break;
					case IDC_MATEDIT_USER_ONLINE:
						{
							CHECK(wmEvent==BN_CLICKED);
							invokeUserPassDialog();
						}
						break;


					case IDC_MATEDIT_LIB_SEARCH_QUERY:			//-------------------------------------------------------------------------
						{
							if (wmEvent == ES_RETURN)
							{
								unclickAllCategories(-1);

								char * sq = getSearchText();
								if (libMode==1)
									findMaterialsInLocalLib(sq, 0);
								else
									onlinematlib.downloadXMLinfoBySearch(sq, 0);

								if (sq)
									free(sq);
							}
						}
						break;
					case IDC_MATEDIT_LIB_SEARCH_OK:			//-------------------------------------------------------------------------
						{
							if (wmEvent == BN_CLICKED)
							{
								unclickAllCategories(-1);

								char * sq = getSearchText();
								if (libMode==1)
									findMaterialsInLocalLib(sq, 0);
								else
									onlinematlib.downloadXMLinfoBySearch(sq, 0);

								if (sq)
									free(sq);
							}
						}
						break;

					case IDC_MATEDIT_LIB1:
						{
							if (wmEvent==PV_CLICKED)
								clickedLibPreview(0);
							if (wmEvent==PV_DOUBLECLICKED)
							{
								clickedTwiceLibPreview(0);
								needToUpdateDisplacement = true;
							}
						}
						break;
					case IDC_MATEDIT_LIB2:
						{
							if (wmEvent==PV_CLICKED)
								clickedLibPreview(1);
							if (wmEvent==PV_DOUBLECLICKED)
							{
								clickedTwiceLibPreview(1);
								needToUpdateDisplacement = true;
							}
						}
						break;
					case IDC_MATEDIT_LIB3:
						{
							if (wmEvent==PV_CLICKED)
								clickedLibPreview(2);
							if (wmEvent==PV_DOUBLECLICKED)
							{
								clickedTwiceLibPreview(2);
								needToUpdateDisplacement = true;
							}
						}
						break;
					case IDC_MATEDIT_LIB4:
						{
							if (wmEvent==PV_CLICKED)
								clickedLibPreview(3);
							if (wmEvent==PV_DOUBLECLICKED)
							{
								clickedTwiceLibPreview(3);
								needToUpdateDisplacement = true;
							}
						}
						break;
					case IDC_MATEDIT_LIB5:
						{
							if (wmEvent==PV_CLICKED)
								clickedLibPreview(4);
							if (wmEvent==PV_DOUBLECLICKED)
							{
								clickedTwiceLibPreview(4);
								needToUpdateDisplacement = true;
							}
						}
						break;
					case IDC_MATEDIT_LIB6:
						{
							if (wmEvent==PV_CLICKED)
								clickedLibPreview(5);
							if (wmEvent==PV_DOUBLECLICKED)
							{
								clickedTwiceLibPreview(5);
								needToUpdateDisplacement = true;
							}
						}
						break;
					case IDC_MATEDIT_LIB7:
						{
							if (wmEvent==PV_CLICKED)
								clickedLibPreview(6);
							if (wmEvent==PV_DOUBLECLICKED)
							{
								clickedTwiceLibPreview(6);
								needToUpdateDisplacement = true;
							}
						}
						break;

					// CATEGORIES HERE: ---------------------------------------
					case IDC_MATEDIT_LIB_CAT_EMITTER:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_EMITTER);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_BRICKS:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_BRICKS);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_CARPAINT:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_CARPAINT);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_CERAMIC:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_CERAMIC);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_CONCRETE:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_CONCRETE);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_FABRIC:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_FABRIC);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_FOOD:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_FOOD);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_GLASS:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_GLASS);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_GROUND:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_GROUND);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_LEATHER:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_LEATHER);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_LIQUIDS:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_LIQUIDS);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_METAL:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_METAL);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_MINERALS:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_MINERALS_GEMS);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_ORGANIC:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_ORGANIC);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_PLASTIC:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_PLASTIC);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_PORCELAIN:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_PORCELAIN);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_SSS:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_SSS);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_STONE:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_STONE);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_TILES:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_TILES);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_WALLS:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_WALLS);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_WOOD:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_WOOD);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_OTHER:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_OTHER);
						}
						break;
					case IDC_MATEDIT_LIB_CAT_USER:
						{
							CHECK(wmEvent==BN_CLICKED);
							clickedLibCategory(MatCategory::CAT_USER);
							// TODO

						}
						break;
					// CATEGORIES END HERE ================================================================================================


				}	// end switch
			}		// end WM_COMMAND
			break;


		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)backgroundBrush;
			}
			break;
		default:
			{
				return 0;
			}
			break;
	}
	return 1;
}

//------------------------------------------------------------------------------------------------------

DWORD WINAPI MatEditorWindow::RefreshThreadProc(LPVOID lpParameter)
{
	float s = *((float*)lpParameter);

	clock_t lTime = clock();
	clock_t cTime;
	double diff;
	HWND hPr = GetDlgItem(hMain, IDC_MATEDIT_PREVIEW);
	EMPView * empv = GetEMPViewInstance(hPr);

	while (refreshThreadRunning)
	{
		Sleep(50);

		cTime = clock();
		diff  = (cTime - lTime)/(double)CLOCKS_PER_SEC;
		if (diff > s)
		{
			refresh();
			lTime = clock();
		}
	}

	return 0;
}

//------------------------------------------------------------------------------------------------------

void updateLinked90Controls(HWND hWnd)
{
	EMCheckBox * emlinked90 = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_MATEDIT_REFL90_LINKED));
	if (!emlinked90)
		return;
	if (emlinked90->selected)
	{
		disableControl(hWnd, IDC_MATEDIT_REFL90_COLOR);
		disableControl(hWnd, IDC_MATEDIT_REFL90_COLOR_RGB);
		disableControl(hWnd, IDC_MATEDIT_REFL90_COLOR_TEX);
		disableControl(hWnd, IDC_MATEDIT_REFL90_COLOR_TEX_ON);
	}
	else
	{
		enableControl(hWnd, IDC_MATEDIT_REFL90_COLOR);
		enableControl(hWnd, IDC_MATEDIT_REFL90_COLOR_RGB);
		enableControl(hWnd, IDC_MATEDIT_REFL90_COLOR_TEX);
		enableControl(hWnd, IDC_MATEDIT_REFL90_COLOR_TEX_ON);
	}
}
