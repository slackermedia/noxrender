#include <windows.h>
#include <GdiPlus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"
#include "resource.h"

extern HMODULE hDllModule;

//----------------------------------------------------------------------

void InitEM2Line()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2Line";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2LineProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2Line *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2Line * GetEM2LineInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2Line * eml = (EM2Line *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2Line * eml = (EM2Line *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return eml;
}

//----------------------------------------------------------------------

void SetEM2LineInstance(HWND hwnd, EM2Line *eml)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)eml);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)eml);
	#endif
}

//----------------------------------------------------------------------

EM2Line::EM2Line(HWND hWnd)
{
	hwnd = hWnd;

	colBackground = NGCOL_TEXT_BACKGROUND;
	colLine = RGB(200,200,200);
	colShadow1 = RGB(0,0,0);
	colShadow2 = RGB(0,0,0);
	alphaLine = 96;
	alphaShadow1 = 72;
	alphaShadow2 = 36;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;
}

//----------------------------------------------------------------------

EM2Line::~EM2Line()
{
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2LineProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2Line * eml;
	eml = GetEM2LineInstance(hwnd);

	switch (msg)
	{
		case WM_CREATE:
			eml = new EM2Line(hwnd);
			SetEM2LineInstance(hwnd, eml);
			break;
		case WM_DESTROY:
			{
				delete eml;
				SetEM2LineInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
			{
				if (!eml) 
					break;

				RECT rect;
				PAINTSTRUCT ps;

				GetClientRect(hwnd, &rect);
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);

				if (eml->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, eml->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, eml->bgShiftX, eml->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, eml->colBackground));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(eml->colBackground));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, OPAQUE);
				}

				bool horizontal = (rect.right>rect.bottom);


				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);
						Gdiplus::Pen s1pen(Gdiplus::Color(eml->alphaLine, GetRValue(eml->colLine), GetGValue(eml->colLine), GetBValue(eml->colLine)));
						Gdiplus::Pen s2pen(Gdiplus::Color(eml->alphaShadow1, GetRValue(eml->colShadow1), GetGValue(eml->colShadow1), GetBValue(eml->colShadow1)));
						Gdiplus::Pen s3pen(Gdiplus::Color(eml->alphaShadow2, GetRValue(eml->colShadow2), GetGValue(eml->colShadow2), GetBValue(eml->colShadow2)));

						if (horizontal)
						{
							graphics.DrawLine(&s1pen, 2, 2, rect.right-3, 2);
							Gdiplus::Point pts3[] = {
										Gdiplus::Point(2, 3), 
										Gdiplus::Point(rect.right-3, 3),
										Gdiplus::Point(rect.right-2, 2),
										Gdiplus::Point(rect.right-3, 1),
										Gdiplus::Point(2, 1),
										Gdiplus::Point(1, 2),
										Gdiplus::Point(2, 3) };
							graphics.DrawLines(&s2pen, pts3, 7);
							Gdiplus::Point pts4[] = {
										Gdiplus::Point(2, 4), 
										Gdiplus::Point(rect.right-3, 4),
										Gdiplus::Point(rect.right-1, 2),
										Gdiplus::Point(rect.right-3, 0),
										Gdiplus::Point(2, 0),
										Gdiplus::Point(0, 2),
										Gdiplus::Point(2, 4) };
							graphics.DrawLines(&s3pen, pts4, 7);
						}
						else
						{
							graphics.DrawLine(&s1pen, 2, 2, 2, rect.bottom-3);
							Gdiplus::Point pts3[] = {
										Gdiplus::Point(3, 2), 
										Gdiplus::Point(3, rect.bottom-3),
										Gdiplus::Point(2, rect.bottom-2),
										Gdiplus::Point(1, rect.bottom-3),
										Gdiplus::Point(1, 2),
										Gdiplus::Point(2, 1),
										Gdiplus::Point(3, 2) };
							graphics.DrawLines(&s2pen, pts3, 7);
							Gdiplus::Point pts4[] = {
										Gdiplus::Point(4, 2),
										Gdiplus::Point(4, rect.bottom-3),
										Gdiplus::Point(2, rect.bottom-1),
										Gdiplus::Point(0, rect.bottom-3),
										Gdiplus::Point(0, 2),
										Gdiplus::Point(2, 0),
										Gdiplus::Point(4, 2) };
							graphics.DrawLines(&s3pen, pts4, 7);
						}

					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					COLORREF col1 = getMixedColor(eml->colBackground, eml->alphaLine,		eml->colLine);
					COLORREF col2 = getMixedColor(eml->colBackground, eml->alphaShadow1,	eml->colShadow1);
					COLORREF col3 = getMixedColor(eml->colBackground, eml->alphaShadow2,	eml->colShadow2);
					if (horizontal)
					{
						HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, col1));
						MoveToEx(hdc, 2, 2, NULL);
						LineTo(hdc, rect.right-2, 2);

						DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, col2)));
						MoveToEx(hdc, 2, 3, NULL);
						LineTo(hdc,   rect.right-3, 3);
						LineTo(hdc,   rect.right-2, 2);
						LineTo(hdc,   rect.right-3, 1);
						LineTo(hdc,   2, 1);
						LineTo(hdc,   1, 2);
						LineTo(hdc,   2, 3);

						DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, col3)));
						MoveToEx(hdc, 2, 4, NULL);
						LineTo(hdc,   rect.right-3, 4);
						LineTo(hdc,   rect.right-1, 2);
						LineTo(hdc,   rect.right-3, 0);
						LineTo(hdc,   2, 0);
						LineTo(hdc,   0, 2);
						LineTo(hdc,   2, 4);

						DeleteObject(SelectObject(hdc, hOldPen));
					}
					else
					{
						HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, col1));
						MoveToEx(hdc, 2, 2, NULL);
						LineTo(hdc, 2, rect.bottom-2);

						DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, col2)));
						MoveToEx(hdc, 3, 2, NULL);
						LineTo(hdc,   3, rect.bottom-3);
						LineTo(hdc,   2, rect.bottom-2);
						LineTo(hdc,   1, rect.bottom-3);
						LineTo(hdc,   1, 2);
						LineTo(hdc,   2, 1);
						LineTo(hdc,   3, 2);

						DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, col3)));
						MoveToEx(hdc, 4, 2, NULL);
						LineTo(hdc,   4, rect.bottom-3);
						LineTo(hdc,   2, rect.bottom-1);
						LineTo(hdc,   0, rect.bottom-3);
						LineTo(hdc,   0, 2);
						LineTo(hdc,   2, 0);
						LineTo(hdc,   4, 2);

						DeleteObject(SelectObject(hdc, hOldPen));
					}
				#endif

				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------
