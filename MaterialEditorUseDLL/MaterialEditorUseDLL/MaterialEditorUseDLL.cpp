#define _WIN32_WINNT 0x0502
//#include <delayhlp.cpp>
#include <windows.h>
//#include <delayimp.h>
#include <tchar.h>
#include "Resource."
#include "MaterialEditorUseDLL.h"
#include "../../Everything DLL/Everything DLL/MatEditor.h"
#include "../../Everything DLL/Everything DLL/MatEditor2.h"
#include "../../Everything DLL/Everything DLL/MaterialEditorMainWindow.h"
#include "../../Everything DLL/Everything DLL/RendererMainWindow.h"

#include "../../Everything DLL/Everything DLL/RendererMain2.h"

//#pragma comment(lib, "libxml2.lib")
//#pragma comment(lib, "../../Everything DLL/Release/EverythingDLL.lib")
//#pragma comment(lib, "DelayImp.lib")



HINSTANCE hInst;

//-------------------------------------------------------------------------------------------------------------

TCHAR * copyAnsiToTchar(char * src)
{
	if (!src)
		return NULL;

	int l = (int)strlen(src);
	TCHAR * res = NULL;

	#ifdef UNICODE
		res = (wchar_t *)malloc(2*l+2);
		MultiByteToWideChar(CP_ACP, 0, src, l+1, res, l+1);
	#else
		res = (char *)malloc(l+1);
		strcpy_s(res, l+1, src);
	#endif

	return res;
}

//-------------------------------------------------------------------------------------------------------------

bool doesDirExist(TCHAR * dirName)
{
	bool res;
	HANDLE hDir;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hDir = FindFirstFile(dirName, &fd);
	
	if (hDir == INVALID_HANDLE_VALUE   ||   !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hDir != INVALID_HANDLE_VALUE)
		FindClose(hDir);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

bool doesFileExist(TCHAR * filename)		// will return false on dir
{
	if (!filename)
		return false;

	bool res;
	HANDLE hFile;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hFile = FindFirstFile(filename, &fd);
	
	if (hFile == INVALID_HANDLE_VALUE   ||   (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hFile != INVALID_HANDLE_VALUE)
		FindClose(hFile);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

TCHAR * getRendererDirectory()
{
	// get default directory
	DWORD rsize = 2048;
	TCHAR buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software"), 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, _T("Evermotion"), 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, _T("Renderer"), 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres = RegQueryValueEx(key3, _T("directory"), NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
	{	// if OK
	}
	else
	{
		return NULL;
	}

	size_t ss = _tcsclen(buffer)+1;
	TCHAR * res = (TCHAR *)malloc(ss*sizeof(TCHAR));
	if (res)
		_stprintf_s(res, ss, _T("%s"), buffer);

	return res;
}

bool checkRendererDirs(TCHAR * dir)
{
	TCHAR dirName[2560];
	_stprintf_s(dirName, 2560, _T("%s"), dir);
	if (!doesDirExist(dirName))
	{
		return false;
	}

	#ifdef _WIN64
		_stprintf_s(dirName, 2560, _T("%s\\dll\\64"), dir);
	#else
		_stprintf_s(dirName, 2560, _T("%s\\dll\\32"), dir);
	#endif
	if (!doesDirExist(dirName))
	{
		return false;
	}

	return true;
}

void setDir()
{
	// get win version and set dll directory search
	OSVERSIONINFO osver;
	osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx (&osver);

	//char bufos[64];
	//sprintf_s(bufos, 64, "OS ver. %d.%d", osver.dwMajorVersion, osver.dwMinorVersion);
	//MessageBox(0, bufos, "", 0);


	if ((osver.dwMajorVersion > 5)  ||
		(osver.dwMajorVersion == 5   &&   osver.dwMinorVersion >= 1))
	{
		TCHAR * dir = getRendererDirectory();
		if (dir)
		{
			if (checkRendererDirs(dir))
			{
				TCHAR dirName[2560];
				#ifdef _WIN64
					_stprintf_s(dirName, 2560, _T("%s\\dll\\64"), dir);
				#else
					_stprintf_s(dirName, 2560, _T("%s\\dll\\32"), dir);
				#endif
				BOOL ss = SetDllDirectory(dirName);
				if (ss == FALSE)
					MessageBox(0, _T("Directory setting for DLLs failed.\nInstructions later."), _T(""), 0);
				//MessageBox(0, dirName, "", 0);
			}
			else
			{
				TCHAR c[2560];
				#ifdef _WIN64
					_stprintf_s(c, 2560, _T("Directory\n%s\\dll\\64\ndoes not exist.\nApplication may fail to find DLLs and crash."), dir);
				#else
					_stprintf_s(c, 2560, _T("Directory\n%s\\dll\\32\ndoes not exist.\nApplication may fail to find DLLs and crash."), dir);
				#endif
				MessageBox(0, c, _T("Warning"), MB_ICONEXCLAMATION);
			}
			free(dir);
		}
		else
		{
			MessageBox(0, _T("Directory not set in registry.\nApplication may fail to find DLLs and crash."), _T("Warning"), MB_ICONEXCLAMATION);
		}
	}
}


//-------------------------------------------------------------------------------------------------------------

HWND createRendererWindow(HINSTANCE hInstance, char * openfilename, bool startRendering)
{
	RendererMainWindow rendererMain(hInstance);
	rendererMain.create(false, 0);
	if (openfilename)
		rendererMain.tryLoadSceneDroppedAsync(openfilename, startRendering);
	return rendererMain.getHWND();
}

//-------------------------------------------------------------------------------------------------------------

HWND createRenderer2Window(HINSTANCE hInstance, char * openfilename, bool startRendering, bool legacyMode)
{
	if (legacyMode)
		RendererMain2::setLegacyMode(legacyMode);
	RendererMain2::create(false, 0, hInstance);
	RendererMain2 * rendererMain = RendererMain2::getInstance();
	if (!rendererMain)
	{
		MessageBox(0, _T("NOX window creation failed."), _T("Error"), 0);
		return 0;
	}
	if (openfilename)
		rendererMain->tryLoadSceneDroppedAsync(openfilename, startRendering);
	return rendererMain->getHWND();
}

//-------------------------------------------------------------------------------------------------------------

HWND createMatEditorWindow(HINSTANCE hInstance, char * openfilename, bool saveMode)
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->loadMatEditorScenes(NULL);

	MatEditorWindow matEdit(hInstance);
	matEdit.setSaveMode(saveMode);
	bool accept = matEdit.createWindow(0, MatEditorWindow::MODE_ALONE, openfilename);
	//MessageBox(0, "window created", "", 0);
	if (openfilename)
	{
		//matEdit.autoCategory = MatCategory::CAT_USER;
		//matEdit.autoID = 0;
		//matEdit.loadMaterial(openfilename);
	}
	return matEdit.getHWND();
}

//-------------------------------------------------------------------------------------------------------------

HWND createMatEditor2Window(HINSTANCE hInstance, char * openfilename, bool saveMode, bool legacyMode)
{
	if (legacyMode)
		MatEditor2::setLegacyMode(legacyMode);
	Raytracer * rtr = Raytracer::getInstance();
	rtr->loadMatEditorScenes(NULL);

	MatEditor2 * matEdit = MatEditor2::getInstance();
	matEdit->setSaveMode(saveMode);
	matEdit->createWindow(hInstance, 0, MatEditor2::MODE_ALONE, openfilename);
	return matEdit->getHWND();

	//MatEditorWindow matEdit(hInstance);
	//matEdit.setSaveMode(saveMode);
	//bool accept = matEdit.createWindow(0, MatEditorWindow::MODE_ALONE, openfilename);
	////MessageBox(0, "window created", "", 0);
	//if (openfilename)
	//{
	//	//matEdit.autoCategory = MatCategory::CAT_USER;
	//	//matEdit.autoID = 0;
	//	//matEdit.loadMaterial(openfilename);
	//}
	//return matEdit.getHWND();
}

//-------------------------------------------------------------------------------------------------------------

#define NEW_GUI
//#define RENDERER

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// lpCmdLine non-unicode, use GetCommandLine for unicode
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG msg;
	HACCEL hAccelTable;
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDR_ACCELERATOR1));



	setDir();

	//MessageBox(0, "Checking dlls.", "", 0);

	//char tttt[64];
	//sprintf_s(tttt,64, "args: %d", __argc);
	//MessageBox(0, tttt, "", 0);
	//for (int i=0; i<__argc; i++)
	//	MessageBox(0, __argv[i], "", 0);

	bool startRendering = false;
	bool saveOnOK = false;
	bool gotFile = false;
	bool legacyMode = false;
	char * filename = NULL;


	for (int i=1; i<__argc; i++)
	{
		if (!__argv[i])
			continue;
		if (strlen(__argv[i])<2)
			continue;

		if (__argv[i][0] == '-')
		{
			if (0==strcmp(__argv[i], "-save"))
				saveOnOK = true;
			if (0==strcmp(__argv[i], "-start"))
				startRendering = true;
			if (0==strcmp(__argv[i], "-legacy"))
				legacyMode = true;
		}
		else
		{
			unsigned int cmdSize = (unsigned int)strlen(__argv[i]);
			if (cmdSize > 1)
			{
				if (__argv[i][0]=='"'   &&   __argv[i][cmdSize-1]=='"')
				{
					filename = (char *)malloc(cmdSize+1);
					if (filename)
					{
						sprintf_s(filename, cmdSize+1, "%s", __argv[i]+1);
						filename[cmdSize-2] = 0;
					}
				}
				else
				{
					filename = (char *)malloc(cmdSize+1);
					if (filename)
						sprintf_s(filename, cmdSize+1, "%s", __argv[i]);
				}

				TCHAR * tfilename = copyAnsiToTchar(filename);
				if (doesFileExist(tfilename))
				{
					gotFile = true;
				}
			}
		}
	}

	//if (saveOnOK)
	//	MessageBox(0, "save mode", "", 0);


	//bool gotFile = false;
	//char * filename = NULL;
	//if (lpCmdLine)
	//{
	//	unsigned int cmdSize = (unsigned int)strlen(lpCmdLine);
	//	if (strlen(lpCmdLine) > 1)
	//	{
	//		if (lpCmdLine[0]=='"'   &&   lpCmdLine[cmdSize-1]=='"')
	//		{
	//			filename = (char *)malloc(cmdSize+1);
	//			if (filename)
	//			{
	//				sprintf_s(filename, cmdSize+1, "%s", lpCmdLine+1);
	//				filename[cmdSize-2] = 0;
	//			}
	//		}
	//		else
	//		{
	//			filename = (char *)malloc(cmdSize+1);
	//			if (filename)
	//				sprintf_s(filename, cmdSize+1, "%s", lpCmdLine);
	//		}

	//		if (doesFileExists(filename))
	//		{
	//			gotFile = true;
	//		}
	//	}
	//}


	///// o dziwo dziala bez tego
	//#ifdef _WIN64
	//	if (FAILED(__HrLoadAllImportsForDll("EverythingDLL64.dll"))) 
	//		MessageBox(0, _T("There might be a problem.\nEverythingDLL64.dll not found."), _T(""),0);
	//#else
	//	if (FAILED(__HrLoadAllImportsForDll("EverythingDLL32.dll"))) 
	//		MessageBox(0, _T("There might be a problem.\nEverythingDLL32.dll not found."), _T(""),0);
	//#endif



	HWND hWindow;

	#ifdef RENDERER
		#ifdef NEW_GUI
			if (gotFile)
				hWindow = createRenderer2Window(hInstance, filename, startRendering, legacyMode);
			else
				hWindow = createRenderer2Window(hInstance, NULL, saveOnOK, legacyMode);
		#else
			if (gotFile)
				hWindow = createRendererWindow(hInstance, filename, startRendering);
			else
				hWindow = createRendererWindow(hInstance, NULL, saveOnOK);
		#endif
	#else
		#ifdef NEW_GUI
			if (gotFile)
				hWindow = createMatEditor2Window(hInstance, filename, saveOnOK, legacyMode);
			else
				hWindow = createMatEditor2Window(hInstance, NULL, saveOnOK, legacyMode);
		#else
			if (gotFile)
				hWindow = createMatEditorWindow(hInstance, filename, saveOnOK);
			else
				hWindow = createMatEditorWindow(hInstance, NULL, saveOnOK);
		#endif
	#endif

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(hWindow, hAccelTable, &msg))
			//////////!IsDialogMessage(hDialog, &Msg)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

