#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>




INT_PTR CALLBACK RendererMainWindow::ObjectsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				HWND hListObjects  = GetDlgItem(hWnd, IDC_REND_OBJECTS_LISTVIEW);
				EMListView * emlvObj  = GetEMListViewInstance(hListObjects);
				theme.apply(emlvObj);
				emlvObj->setColNumAndClearData(3, 100);
				emlvObj->colWidths[0] = 25;
				emlvObj->colWidths[2] = 50;
				emlvObj->headers.add((char*)malloc(64));
				emlvObj->headers.add((char*)malloc(64));
				emlvObj->headers.add((char*)malloc(64));
				emlvObj->headers.createArray();
				sprintf_s(emlvObj->headers[0], 64, "ID");
				sprintf_s(emlvObj->headers[1], 64, "Name");
				sprintf_s(emlvObj->headers[2], 64, "Num tris");

				HWND hListInstances = GetDlgItem(hWnd, IDC_REND_OBJECTS_INSTANCES_LIST);
				EMListView * emlvInst  = GetEMListViewInstance(hListInstances);
				theme.apply(emlvInst);
				emlvInst->setColNumAndClearData(2, 100);
				emlvInst->headers.add((char*)malloc(64));
				emlvInst->headers.add((char*)malloc(64));
				emlvInst->headers.createArray();
				sprintf_s(emlvInst->headers[0], 64, "Name");
				sprintf_s(emlvInst->headers[1], 64, "Source");
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_OBJECTS_INSTANCES_LIST:
						{
							if (wmEvent == LV_SELECTION_CHANGED)
							{
								HWND hListObjects  = GetDlgItem(hWnd, IDC_REND_OBJECTS_LISTVIEW);
								EMListView * emlvObj  = GetEMListViewInstance(hListObjects);
								HWND hListInstances = GetDlgItem(hWnd, IDC_REND_OBJECTS_INSTANCES_LIST);
								EMListView * emlvInst  = GetEMListViewInstance(hListInstances);
								if (emlvInst->selected>=0)
									emlvObj->selected = Raytracer::getInstance()->curScenePtr->instances[emlvInst->selected].meshID;
								else
									emlvObj->selected = -1;
								InvalidateRect(hListObjects, NULL, false);
							}
							if (wmEvent == LV_DOUBLECLIKED)
							{
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}
