#include <Windows.h>
#include "myList.h"
#include "Color4.h"

// this is header for new gui custom controls

#define EMT_TAB_CLOSED 51973
#define EMT_TAB_OPENED 51972
#define LV_MOUSEOVER_CHANGED 51994
#define LV_SELECTION_CHANGED 51993
#define LV_DOUBLECLIKED 51992
#define ES_LOST_FOCUS 51984
#define ES_TAB 51983
#define ES_TAB_SHIFT 51982
#define ES_RETURN 51988
#define CS_YOUGOT_DRAG 51987
#define CS_IGOT_DRAG 51986
#define CL_RCLICKED 51989
#define SB2_POS_CHANGED 51980

#define GUI2_USE_GDIPLUS
#define GUI2_DRAW_TEXT_SHADOWS

#define NGCOL_YELLOW RGB(0xd5,0xa2,0x08)
#define NGCOL_BG_LIGHT RGB(35,35,35)
#define NGCOL_BG_DARK RGB(30,30,30)
#define NGCOL_BG_MEDIUM RGB(33,33,33)
#define more2 0
#define NGCOL_LIGHT_GRAY RGB(0xcf+more2,0xcf+more2,0xcf+more2)
#define more 0
#define NGCOL_DARK_GRAY RGB(0x88+more,0x84+more,0x81+more)
#define NGCOL_TEXT_BACKGROUND NGCOL_BG_MEDIUM
#define NGCOL_TEXT_DISABLED RGB(0x50, 0x50, 0x50)

#define BUTTON_SELECT(hwnd, sel) GetEM2ButtonInstance(hwnd)->selected = sel; InvalidateRect(hwnd, NULL, FALSE)
#define GROUPBAR_ACTIVATE(hwnd, active) GetEM2GroupBarInstance(hwnd)->pseudoDisabled = !active; InvalidateRect(hwnd, NULL, FALSE)

COLORREF getMixedColor(COLORREF bgCol, unsigned char alpha, COLORREF fgCol);
HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
int cutTextToFitWidthAddEllipsis(HDC hdc, char * txt, int width);

class EM2Elements;
class EM2Button;
class EM2Tabs;
class EM2Text;
class EM2GroupBar;
class EM2CheckBox;
class EM2ListView;
class EM2EditSpin;
class EM2EditTrackBar;
class EM2ComboBox;
class EM2ColorShow;
class EM2EditSimple;
class EM2ImgButton;
class EM2Line;
class EM2Busy;
class EM2TrackBar;
class EM2Color2D;
class EM2ScrollBar;

//----------------------------------------------------------------------

class EM2Elements
{
public:
	static EM2Elements * getInstance();
	HBITMAP getHBitmap();
	HBITMAP * getHBitmapPtr();
	HBITMAP getCircleBitmap();
private:
	HBITMAP hImage;
	HBITMAP hCircleProgress;
	static EM2Elements * elInstance;
	EM2Elements();
	~EM2Elements();
};

//----------------------------------------------------------------------

void InitEM2Button();
LRESULT CALLBACK EM2ButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2Button * GetEM2ButtonInstance(HWND hwnd);
void SetEM2ButtonInstance(HWND hwnd, EM2Button *emb);

class EM2Button
{
public:
    COLORREF colText;
    COLORREF colTextMouseOver;
    COLORREF colTextSelected;
    COLORREF colTextDisabled;
	COLORREF colTextShadow;

    COLORREF colBackGnd;
    COLORREF colBackGndMouseOver;
    COLORREF colBackGndSelected;
	COLORREF colBackGndDisabled;

	COLORREF colBorderLeft;
	COLORREF colBorderTop;
	COLORREF colBorderRight;
	COLORREF colBorderBottom;

	COLORREF colBorderLeftMouseOver;
	COLORREF colBorderTopMouseOver;
	COLORREF colBorderRightMouseOver;
	COLORREF colBorderBottomMouseOver;

	COLORREF colBorderLeftSelected;
	COLORREF colBorderTopSelected;
	COLORREF colBorderRightSelected;
	COLORREF colBorderBottomSelected;

	COLORREF colBorderLeftDisabled;
	COLORREF colBorderTopDisabled;
	COLORREF colBorderRightDisabled;
	COLORREF colBorderBottomDisabled;

	unsigned char alphaBg;
	unsigned char alphaBgMouseOver;
	unsigned char alphaBgSelected;
	unsigned char alphaBgDisabled;

	unsigned char alphaBorder;
	unsigned char alphaBorderMouseOver;
	unsigned char alphaBorderSelected;
	unsigned char alphaBorderDisabled;


	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	bool drawBorder;
	bool drawTextShadow;

	bool ellipsis;
	int captionSize;
	char * caption;

	bool selected;
	bool mouseClicked;
	bool mouseOver;

	bool twoState;
	bool ts_allow_unclick;
	bool useCursors;

    HWND hwnd;

	EM2Button(HWND hWnd);
	~EM2Button();

	bool changeCaption(char * newCaption);

	bool setFont(HFONT hNewFont, bool autodel);
	HFONT getFont();
private:
    HFONT hFont;
	bool autodelfont;
};

//----------------------------------------------------------------------
//	EM2 Tabs

void InitEM2Tabs();
LRESULT CALLBACK EM2TabsProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2Tabs * GetEM2TabsInstance(HWND hwnd);
void SetEM2TabsInstance(HWND hwnd, EM2Tabs *emt);

class EM2Tabs
{
public:
	HWND hwnd;
    COLORREF colText;
    COLORREF colBackGnd;
	COLORREF colSelText;
	COLORREF colDisText;
	COLORREF colTextShadow;

	int last_selected;
	int selected;
	int mouseover;
	bool drawTextShadow;
	bool useCursors;
	bool allowNone;
	int numTabs;
	char ** tabnames;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	bool setTabsCount(int newcount);
	bool setTabText(int id, char * txt);
	void releaseTabs();
	int getUnderMouseTabNumber(int x, int y);

	EM2Tabs(HWND hWnd);
	~EM2Tabs();

	bool setFont(HFONT hNewFont, bool autodel);
	HFONT getFont();
private:
    HFONT hFont;
	bool autodelfont;
};

//----------------------------------------------------------------------

void InitEM2Text();
LRESULT CALLBACK EM2TextProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2Text * GetEM2TextInstance(HWND hwnd);
void SetEM2TextInstance(HWND hwnd, EM2Text *emt);

class EM2Text
{
public:
	static const int ALIGN_LEFT = 0;
	static const int ALIGN_RIGHT = 1;
	static const int ALIGN_CENTER = 2;

	HWND     hwnd;

	COLORREF colText;
	COLORREF colTextShadow;
    COLORREF colBackGnd;
    COLORREF colDisabledText;
	COLORREF colLink;
	COLORREF colLinkMouseOver;
	COLORREF colBgMouseOver;
	unsigned char alphaBgMouseOver;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	int captionSize;
	char * caption;
	char * linkAddress;
	int align;
	int marginTop;
	int marginHori;
	RECT textRect;
	bool isItHyperLink;
	bool wholeAreaMouseOver;
	bool isMouseOverNow;
	bool drawTextShadow;
	bool forwardMouseScroll;
	bool useCursors;


	EM2Text(HWND hWnd);
	~EM2Text();

	bool changeCaption(char * newCaption);
	bool setHyperLink(char * newAddress);
	bool evalTextRect(HDC &hdc, RECT * rect);

	bool setFont(HFONT hNewFont, bool autodel);
	HFONT getFont();
private:
    HFONT hFont;
	bool autodelfont;
};

//----------------------------------------------------------------------

void InitEM2GroupBar();
LRESULT CALLBACK EM2GroupBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2GroupBar * GetEM2GroupBarInstance(HWND hwnd);
void SetEM2GroupBarInstance(HWND hwnd, EM2GroupBar *emgb);

class EM2GroupBar
{
public:
	static const char VALIGN_TOP    = 1;
	static const char VALIGN_CENTER = 2;
	static const char VALIGN_BOTTOM = 3;
	static const char HALIGN_LEFT   = 1;
	static const char HALIGN_CENTER = 2;
	static const char HALIGN_RIGHT  = 3;

	HWND hwnd;
    COLORREF colText;
    COLORREF colTextDisabled;
    COLORREF colBackGnd;
	COLORREF colBackGndAlpha;
	COLORREF colTextShadow;
	unsigned char alphaBg;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	int captionSize;
	char * caption;
	char hAlign;
	char vAlign;
	bool drawTextShadow;
	bool pseudoDisabled;

	EM2GroupBar(HWND hWnd);
	~EM2GroupBar();

	bool setFont(HFONT hNewFont, bool autodel);
	HFONT getFont();
private:
    HFONT hFont;
	bool autodelfont;
};

//----------------------------------------------------------------------

void InitEM2CheckBox();
LRESULT CALLBACK EM2CheckBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2CheckBox * GetEM2CheckBoxInstance(HWND hwnd);
void SetEM2CheckBoxInstance(HWND hwnd, EM2CheckBox *emb);

class EM2CheckBox
{
public:
	COLORREF colBackground;
	COLORREF colText;
	COLORREF colTextDisabled;
	COLORREF colCheckDisabled;
	COLORREF colCheckSelected;
	COLORREF colCheckUnselected;
	COLORREF colCheckMouseOver;
	COLORREF colTextMouseOver;
	COLORREF colTextSelected;
	COLORREF colTextShadow;
	COLORREF colCheckShadow;

	bool mouseOver;
	bool selected;
	bool drawTextShadow;
	bool useCursors;
	HWND hwnd;
	RECT clickRect;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	int captionSize;
	char * caption;


	EM2CheckBox(HWND hWnd);
	~EM2CheckBox();

	bool setFont(HFONT hNewFont, bool autodel);
	HFONT getFont();
private:
    HFONT hFont;
	bool autodelfont;
};

//----------------------------------------------------------------------

void InitEM2ListView();
LRESULT CALLBACK EM2ListViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2ListView * GetEM2ListViewInstance(HWND hwnd);
void SetEM2ListViewInstance(HWND hwnd, EM2ListView *emlv);

class EM2ListView
{
public:
	HWND hwnd;
	HRGN hRegion;

	unsigned char alphaBgHeader;
	unsigned char alphaBgDataOdd;
	unsigned char alphaBgDataEven;
	unsigned char alphaBgDataSelected;
	unsigned char alphaBgDataMouseOver;	//not used
	unsigned char alphaBorderMouseOver;

	COLORREF colBackgroundHeader;
	COLORREF colBackgroundOdd;
	COLORREF colBackgroundEven;
	COLORREF colBackgroundSelected;
	COLORREF colBackgroundMouseOver;	// not used
	COLORREF colBorderMouseOver;

	COLORREF colText;
	COLORREF colTextHeader;
	COLORREF colTextMouseOver;
	COLORREF colTextSelected;
	COLORREF colTextShadow;
	COLORREF colTextDisabled;
	COLORREF colTextDisabledSelected;
	COLORREF colTextDisabledHeader;

	unsigned char alphaSBSlider;
	unsigned char alphaSBBackground;
	COLORREF colSBSlider;
	COLORREF colSBSliderMouseOver;
	COLORREF colSBSliderClicked;
	COLORREF colSBBackground;

private:
	int rowHeight;
	int headerHeight;
public:
	int maxShownRows;
	int maxShownRowsAndHeader;
	int selected;
	int mouseOver;
	int firstShown;
	bool drawTextShadow;
	bool forceDrawScrollbar;
	bool useCursors;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

private:
	bList<char ** , 16> data;
	char ** headers;
public:
	int * colWidths;
	char * align;
	char * alignHeader;
	int * margins;
	int * marginsHeader;
	int numCols;

	bool addEmptyRow();
	bool setData(int col, int row, char * str);
	bool setHeaderName(int col, char * str);
	bool setColNumAndClearData(int num, int width);
	void drawScrollBar(HDC hdc);
	void freeEntries();
	char * getData(int col, int row);
	char * getHeaderName(int col);
	int getNumRows() { return data.objCount; }

	bool setHeightHeader(int h);
	bool setHeightData(int h);
	int getHeightHeader();
	int getHeightData();

	// scrollbar data
	int spaceToSB;
	int minSbH;
	int sbWidth;
	int sbStart;
	int sbEnd;
	bool sliding;
	void calcSB();
	void calcFP();
	// end scrollbar data
	bool showHeader;
	HRGN hRegEmpty;
	HRGN hRegSlider;
	HRGN hRegOverSlider;
	HRGN hRegUnderSlider;
	void updateSliderRegions();

	void notifySelectionChanged();
	void notifyMouseOverChanged();
	void notifyDoubleClick();
	void prepareWindow();
	void gotoEnd();
	void Refresh();
	void slideToSelected();

	static const char ALIGN_LEFT   = 0;
	static const char ALIGN_CENTER = 1;
	static const char ALIGN_RIGHT  = 2;

	EM2ListView(HWND hWnd);
	~EM2ListView();

	bool setFontData(HFONT hNewFont, bool autodel);
	bool setFontHeader(HFONT hNewFont, bool autodel);
	HFONT getFontData();
	HFONT getFontHeader();
private:
    HFONT hFontData;
    HFONT hFontHeader;
	bool autodelfontdata;
	bool autodelfontheader;

};

//---------------------------------------------------------------------

void InitEM2EditSpin();
LRESULT CALLBACK EM2EditSpinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2EditSpin * GetEM2EditSpinInstance(HWND hwnd);
void SetEM2EditSpinInstance(HWND hwnd, EM2EditSpin *emes);

class EM2EditSpin
{
public:
	static const int ALIGN_LEFT = 0;
	static const int ALIGN_RIGHT = 1;
	static const int ALIGN_CENTER = 2;

	COLORREF colBackground;
	COLORREF colEditBackground;
	COLORREF colEditText;
	COLORREF colEditBorderOuter;
	COLORREF colEditBorderLeft;
	COLORREF colEditBorderTop;
	COLORREF colEditBorderRight;
	COLORREF colEditBorderBottom;
	COLORREF colDisabledEditBorderOuter;
	COLORREF colDisabledEditBackground;
	COLORREF colDisabledEditText;

	HBRUSH hEditBrush;
	HBRUSH hEditDisabledBrush;
	HWND hwnd;
	HWND hEdit;
	RECT rEdit, rUpper, rLower;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	bool allowFloat;

	bool nowScrolling;
	bool clicked1;
	bool clicked2;
	bool overButton1;
	bool overButton2;

	int height;
	int editRectWidth;
	int buttonToEditSpace;

	int minIntValue;
	int maxIntValue;
	int intValue;
	int intChangeValue;
	int intDefaultValue;
	float intMouseChangeValue;

	float minFloatValue;
	float maxFloatValue;
	float floatValue;
	float floatChangeValue;
	float floatMouseChangeValue;
	float floatDefaultValue;
	int floatAfterDot;

	EM2EditSpin(HWND hWnd);
	~EM2EditSpin();

	void updateChangedValue();
	void floatToEditbox(float value);
	void intToEditbox(int value);
	void setValuesToInt(int value, int minVal, int maxVal, int defaultValue, int changeValue, float mouseChangeValue);
	void setValuesToFloat(float value, float minVal, float maxVal, float defaultValue, float changeValue, float mouseChangeValue);
	void setIntValue(int value);
	void setFloatValue(float value);
	void notifyScrolling();
	void updateButtonRect(RECT * clientRect);

	void setAlign(int newalign);
	bool setFont(HFONT hNewFont, bool autodel);
	HFONT getFont();
private:
    HFONT hFont;
	bool autodelfont;
};

//---------------------------------------------------------------------

void InitEM2EditTrackBar();
LRESULT CALLBACK EM2EditTrackBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2EditTrackBar * GetEM2EditTrackBarInstance(HWND hwnd);
void SetEM2EditTrackBarInstance(HWND hwnd, EM2EditTrackBar *emetb);

class EM2EditTrackBar
{
public:
	static const int ALIGN_LEFT = 0;
	static const int ALIGN_RIGHT = 1;
	static const int ALIGN_CENTER = 2;

	COLORREF colEditBackground;
	COLORREF colEditText;

	COLORREF colDisabledEditBackground;
	COLORREF colDisabledEditText;
	COLORREF colDisabledEditBorderOuter;

	COLORREF colEditBorderOuter;
	COLORREF colEditBorderLeft;
	COLORREF colEditBorderTop;
	COLORREF colEditBorderRight;
	COLORREF colEditBorderBottom;

	COLORREF colPathBackgroundBefore;
	COLORREF colPathBackgroundAfter;
	COLORREF colPathBorder;
	unsigned char alphaPathBefore;
	unsigned char alphaPathAfter;
	unsigned char alphaPathBorder;

	COLORREF colDisabledPathBg;
	COLORREF colDisabledPathBorder;
	unsigned char alphaDisabledPath;
	unsigned char alphaDisabledPathBorder;


	RECT rectSlider, rectBefore, rectAfter;
	void updateMouseRectangles();

	HBRUSH hEditBrush;
	HBRUSH hEditDisabledBrush;
	HWND hwnd;
	HWND hEdit;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	bool allowFloat;
	bool nowScrolling;
	bool nowChanged;
	bool nowMouseOver;
	bool useCursors;

	int height;
	int editRectWidth;
	int spaceAfterEdit;
	int pathHeight;
	int pathXStart;
	int pathXEnd;
	int pathLength;
	int sliderWidth;
	int sliderHeight;
	int sliderPos;

	int minIntValue;
	int maxIntValue;
	int intValue;
	int intDefaultValue;
	int intFastValue;

	float minFloatValue;
	float maxFloatValue;
	float floatValue;
	float floatDefaultValue;
	float floatFastValue;
	int floatAfterDot;


	EM2EditTrackBar(HWND hWnd);
	~EM2EditTrackBar();

	void setEditBoxWidth(int w, int spaceAfter);
	void notifyParent();
	void resize(RECT * nrect);
	void floatToEditbox(float value);
	void intToEditbox(int value);
	void setValuesToInt(int value, int minVal, int maxVal, int defaultValue, int fastValue);
	void setValuesToFloat(float value, float minVal, float maxVal, float defaultValue, float fastValue);
	void setIntValue(int value);
	void setFloatValue(float value);


	void setAlign(int newalign);
	bool setFont(HFONT hNewFont, bool autodel);
	HFONT getFont();
private:
    HFONT hFont;
	bool autodelfont;
};

//---------------------------------------------------------------------

void InitEM2ComboBox();
LRESULT CALLBACK EM2ComboBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK EM2ComboBoxUnwrappedProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2ComboBox * GetEM2ComboBoxInstance(HWND hwnd);
void SetEM2ComboBoxInstance(HWND hwnd, EM2ComboBox *emcb);

class EM2ComboBox
{
public:
	static const int ALIGN_LEFT = 0;
	static const int ALIGN_RIGHT = 1;
	static const int ALIGN_CENTER = 2;

	HWND hwnd;
	HBITMAP bgImage;
	int bgShiftX, bgShiftY;
	bool drawTextShadow;

	COLORREF colText;
	COLORREF colTextShadow;
	COLORREF colTextMouseOver;
	COLORREF colTextClicked;
	COLORREF colTextDisabled;
	COLORREF colUnwrapBorder;
	COLORREF colUnwrapBg;
	COLORREF colUnwrapBgMouseOver;
	COLORREF colUnwrapBgSelected;
	COLORREF colUnwrapText;
	COLORREF colUnwrapTextMouseOver;
	COLORREF colUnwrapTextSelected;
	COLORREF colUnwrapTextDisabled;
	COLORREF colUnwrapTextShadow;
	COLORREF colUnwrapSlider;
	COLORREF colUnwrapSliderMouseOver;
	COLORREF colUnwrapSliderClicked;

	char * overwrittenHeader;
	bool * enabledItems;
	char ** items;
	int numItems;
	int selected;
	int mouseOverItem;
	bool isUnwrapped;
	bool mouseOverMain;
	bool ellipsis;
	bool moveArrow;
	bool useCursors;
	int shiftUnwrapRight;

	int align;
	int alignUnwrap;

	int maxShownRows;
	int rowHeight;
	int rowWidth;
	int rowStart;

	RECT mouseRect;
	bool nowScrolling;
	bool mouseOverSlider;
	int scrollPos;
	int scrollHeight;
	int maxScrollPos;
	HRGN hRgnSlider;

	void updateScrollDimensions(HWND hUnwrapped);
	void updateScrollFromPos();
	void updatePosFromScroll();
	void updateRegionsScroll(HWND hUnwraped);
	bool setNumItems(int newnum);
	bool setItem(int id, char * caption, bool enabled);
	void deleteItems();
	bool setHeader(char * newname);

	EM2ComboBox(HWND hWnd);
	~EM2ComboBox();

	bool setFontMain(HFONT hNewFont, bool autodel);
	bool setFontUnwrapped(HFONT hNewFont, bool autodel);
	HFONT getFontMain();
	HFONT getFontUnwrapped();
private:
    HFONT hFontMain;
    HFONT hFontUnwrapped;
	bool autodelfontmain;
	bool autodelfontunwrapped;

};

//---------------------------------------------------------------------

void InitEM2ColorShow();
LRESULT CALLBACK EM2ColorShowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2ColorShow * GetEM2ColorShowInstance(HWND hwnd);
void SetEM2ColorShowInstance(HWND hwnd, EM2ColorShow *emcs);

class EM2ColorShow
{
public:
	Color4 color;
	COLORREF colBorder;
	COLORREF colBorderDrag;
	COLORREF colBorderClick;
	COLORREF colBorderMouseOver;
	COLORREF colShadow1;
	COLORREF colShadow2;
	COLORREF colDisabled;
	unsigned char alphaShadow1;
	unsigned char alphaShadow2;
	unsigned char alphaBorder;
	unsigned char alphaDisabled;

	HWND hwnd;
	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	bool showBorder;
	bool showShadow;
	bool allowDragSrc;
	bool allowDragDst;
	bool showMouseOverOrClick;
	bool isDragOver;
	bool isClicked;
	bool isMouseOver;
	bool useCursors;

	HANDLE hCursor;
	HBITMAP hImageCursor;
	HBITMAP hImageCursorMask;
	bool createCursor(COLORREF col);


	void dragOver();
	void dragNoOver();
	void setAllowedDragDrop();
	void setDeniedDragDrop();
	void redraw(const Color4 &c);
	EM2ColorShow(HWND hWnd);
	~EM2ColorShow();
};

//---------------------------------------------------------------------

void InitEM2EditSimple();
LRESULT CALLBACK EM2EditSimpleProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2EditSimple * GetEM2EditSimpleInstance(HWND hwnd);
void SetEM2EditSimpleInstance(HWND hwnd, EM2EditSimple *emes);

class EM2EditSimple
{
public:
	static const int ALIGN_LEFT = 0;
	static const int ALIGN_RIGHT = 1;
	static const int ALIGN_CENTER = 2;

	COLORREF colBorderLeft;
	COLORREF colBorderUp;
	COLORREF colBorderRight;
	COLORREF colBorderDown;
	COLORREF colBackground;
	COLORREF colText;
	COLORREF colTextMouseOver;
	COLORREF colTextEdit;
	COLORREF colDisabledBackground;
	COLORREF colDisabledText;
	COLORREF colBackgroundEdit;

	HBRUSH hBrush;
	HBRUSH hDisabledBrush;
	HWND hwnd;
	HWND hEdit;
	HBITMAP bgImage;
	int bgShiftX, bgShiftY;
	RECT rectText; 
	
	bool isMouseOver;
	int margin;

	void setEditBgColor(COLORREF newcol);
	int getAlign();
	void setAlign(int newalign);
	char * getText();
	bool setText(char * text);
	bool hasFocus();
	EM2EditSimple(HWND hWnd);
	~EM2EditSimple();

	bool setFontMain(HFONT hNewFont, bool autodel);
	bool setFontEdit(HFONT hNewFont, bool autodel);
	HFONT getFontMain();
	HFONT getFontEdit();
private:
    HFONT hFontMain;
    HFONT hFontEdit;
	bool autodelfontmain;
	bool autodelfontedit;
	int align;
};

//---------------------------------------------------------------------

void InitEM2ImgButton();
LRESULT CALLBACK EM2ImgButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2ImgButton * GetEM2ImgButtonInstance(HWND hwnd);
void SetEM2ImgButtonInstance(HWND hwnd, EM2ImgButton *emb);

class EM2ImgButton
{
public:
	COLORREF colBackground;
	bool clicked;
	bool mouseIn;
	bool selected;
	bool two_state;
	bool ts_allow_unclick;
	bool useCursors;

	int pNx, pNy, pMx, pMy, pCx, pCy, pDx, pDy;
	HWND hwnd;
	HWND hToolTip;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	HBITMAP * bmImage;

	void setToolTip(char * text);

	EM2ImgButton(HWND hWnd);
	~EM2ImgButton();
};

//---------------------------------------------------------------------

void InitEM2Line();
LRESULT CALLBACK EM2LineProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2Line * GetEM2LineInstance(HWND hwnd);
void SetEM2LineInstance(HWND hwnd, EM2Line *eml);

class EM2Line
{
public:
	COLORREF colBackground;
	COLORREF colLine;
	COLORREF colShadow1;
	COLORREF colShadow2;
	unsigned char alphaLine;
	unsigned char alphaShadow1;
	unsigned char alphaShadow2;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	HWND hwnd;

	EM2Line(HWND hWnd);
	~EM2Line();
};

//---------------------------------------------------------------------

void InitEM2Busy();
LRESULT CALLBACK EM2BusyProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2Busy * GetEM2BusyInstance(HWND hwnd);
void SetEM2BusyInstance(HWND hwnd, EM2Busy *emcs);

class EM2Busy
{
public:
	HWND hwnd;
	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	COLORREF colNormal;
	COLORREF colActive;
	unsigned char alphaNormal;
	unsigned char alphaActive;

	unsigned int boxSize;
	unsigned int boxDist;
	unsigned int boxSizeActive;
	unsigned int boxLeftMargin;
	unsigned int boxTopMargin;
	unsigned int numBoxes;

	int current;
	bool nowAnim;
	unsigned int timer;
	unsigned long long timerID;

	bool startAnim();
	bool stopAnim();

	EM2Busy(HWND hWnd);
	~EM2Busy();
};

//---------------------------------------------------------------------

void InitEM2TrackBar();
LRESULT CALLBACK EM2TrackBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2TrackBar * GetEM2TrackBarInstance(HWND hwnd);
void SetEM2TrackBarInstance(HWND hwnd, EM2TrackBar *emetb);

class EM2TrackBar
{
public:
	COLORREF colSlU;
	COLORREF colSlUR;
	COLORREF colSlR;
	COLORREF colSlRD;
	COLORREF colSlD;
	COLORREF colSlDL;
	COLORREF colSlL;
	COLORREF colSlLU;
	COLORREF colSlC;
	COLORREF colBorder;

	unsigned char alphaBorder;

	HWND hwnd;
	RECT rectSlider, rectBefore, rectAfter;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	HBITMAP bgGradient;
	bool allowFloat;
	bool isHorizontal;
	bool invert;

	int minIntValue;
	int maxIntValue;
	int intValue;
	int intDefaultValue;

	float minFloatValue;
	float maxFloatValue;
	float floatValue;
	float floatDefaultValue;

	EM2TrackBar(HWND hWnd);
	~EM2TrackBar();

	bool posFromMouse(int mx, int my);
	bool createGradient(COLORREF col1, COLORREF col2);
	bool createHueRainbowGradient();
	bool createTemperatureGradient(int tempstart, int tempstop);
	void notifyParent();
	void resize(RECT * nrect);
	void setValuesToInt(int value, int minVal, int maxVal, int defaultValue);
	void setValuesToFloat(float value, float minVal, float maxVal, float defaultValue);
	void setIntValue(int value);
	void setFloatValue(float value);

};

//---------------------------------------------------------------------

void InitEM2Color2D();
LRESULT CALLBACK EM2Color2DProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2Color2D * GetEM2Color2DInstance(HWND hwnd);
void SetEM2Color2DInstance(HWND hwnd, EM2Color2D *emcp);

class EM2Color2D
{
public:
	static const int MODE_HS_V = 0;
	static const int MODE_HV_S = 1;
	static const int MODE_SH_V = 2;
	static const int MODE_SV_H = 3;
	static const int MODE_VS_H = 4;
	static const int MODE_VH_S = 5;
	HWND hwnd;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	COLORREF colBorder;
	COLORREF colBackground;
	unsigned char alphaBorder;
	bool showBorder;

	int px, py;
	void setPosition(float x, float y);
	bool getPosition(float &x, float &y);

	bool generateRainbow(int mode, ColorHSV hsv);
	void notifyParent();

	int defX, defY;
	void setDefaultPos(float x, float y);

	HBITMAP hRainbow;

	EM2Color2D(HWND hWnd);
	~EM2Color2D();
};

//---------------------------------------------------------------------

void InitEM2ScrollBar();
LRESULT CALLBACK EM2ScrollBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EM2ScrollBar * GetEM2ScrollBarInstance(HWND hwnd);
void SetEM2ScrollBarInstance(HWND hwnd, EM2ScrollBar *emsb);

class EM2ScrollBar
{
public:
	static const int ARR_VERTICAL = 0;		// pionowy
	static const int ARR_HORIZONTAL = 1;	// poziomy

	HWND hwnd;

	HBITMAP bgImage;
	int bgShiftX, bgShiftY;

	HRGN rgnTrackBefore;
	HRGN rgnTrackAfter;
	HRGN rgnSlider;
	HRGN rgnEmpty;

	int arrangement;
	int path_thickness;
	int path_length;
	int slider_length;
	int slider_length_min;
	int slider_pos;
	int workarea_pos;	// how much visible part is moved from top or left (in pixels)
	int size_work;		// whole size with not visible part
	int size_screen;	// only visible part size

	COLORREF colBg;
	COLORREF colPath;
	COLORREF colPathDisabled;
	COLORREF colSlider;
	COLORREF colSliderMouseOver;
	COLORREF colSliderMouseClicked;
	COLORREF colSliderDisabled;
	unsigned char alphaPath;
	unsigned char alphaPathDisabled;
	unsigned char alphaSliderNormal;
	unsigned char alphaSliderMouseOver;
	unsigned char alphaSliderMouseClicked;
	unsigned char alphaSliderDisabled;

	bool sliderClicked;
	bool sliderMouseOver;

	bool updateRegionsHorizontal();
	bool updateRegionsVertical();
	void updateSize(int w, int h);
	void updateWorkareaPositionFromSlider();
	void updateSliderFromWorkarea();
	bool drawScrollbar(HDC hdc);
	void setWorkArea(int visible, int whole);

	bool notifyParentChange();

	EM2ScrollBar(HWND hWnd);
	~EM2ScrollBar();
};

//---------------------------------------------------------------------
