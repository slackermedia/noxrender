#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <Shlobj.h>
#include "EM2Controls.h"
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "noxfonts.h"
#include "log.h"
#include "valuesMinMax.h"
#include "RendererMain2.h"

// new gui version 

//------------------------------------------------------------------------------------------------

extern HMODULE hDllModule;
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
void pickTexDlg2SetPositionsOnStart(HWND hWnd);
void pickTexDlg2UpdateBgShifts(HWND hWnd);

void createTexturesListPT2(unsigned int prevSizeX, unsigned int prevSizeY);
void fillGUITexturesListPT2(HWND hDlg);
void releaseTexturesListPT2();
int findPT2IDFromFilename(char * ffname);

bList<PickedTexture *, 16> loadedTexturesListPT2(0);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK TexPicker2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBmp = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBmp = generateBackgroundPattern(1920, 1200, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();
				pickTexDlg2SetPositionsOnStart(hWnd);
				pickTexDlg2UpdateBgShifts(hWnd);

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_PICKTEX_OK));
				embOK->bgImage = hBgBmp;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_PICKTEX_CANCEL));
				embCancel->bgImage = hBgBmp;
				embCancel->setFont(fonts->em2button, false);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_PICKTEX_TITLE));
				emtitle->bgImage = hBgBmp;
				emtitle->setFont(fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2GroupBar * emgrSelTex = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_PICKTEX_GR_SELECTED_TEX));
				emgrSelTex->bgImage = hBgBmp;
				emgrSelTex->setFont(fonts->em2groupbar, false);

				EM2GroupBar * emgrListTex = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_PICKTEX_GR_LIST_OF_TEX));
				emgrListTex->bgImage = hBgBmp;
				emgrListTex->setFont(fonts->em2groupbar, false);

				EM2Text * emtpath = GetEM2TextInstance(GetDlgItem(hWnd, IDC_PICKTEX_PATH));
				emtpath->bgImage = hBgBmp;
				emtpath->setFont(fonts->em2text, false);

				EM2Button * embOpenFolder = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_PICKTEX_OPEN_FOLDER));
				embOpenFolder->bgImage = hBgBmp;
				embOpenFolder->setFont(fonts->em2button, false);

				EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_PICKTEX_LIST));
				emlv->setFontHeader(fonts->em2listheader, false);
				emlv->setFontData(fonts->em2listentry, false);
				emlv->bgImage = hBgBmp;
				emlv->setColNumAndClearData(2, 200);
				emlv->colWidths[0] = 266;
				emlv->colWidths[1] = 80;
				emlv->prepareWindow(); //?
				emlv->setHeightData(26);
				emlv->setHeightHeader(26);
				emlv->setHeaderName(0, "FILENAME");
				emlv->setHeaderName(1, "RESOLUTION");
				emlv->alignHeader[0] = EM2ListView::ALIGN_LEFT;
				emlv->alignHeader[1] = EM2ListView::ALIGN_CENTER;
				emlv->align[0] = EM2ListView::ALIGN_LEFT;
				emlv->align[1] = EM2ListView::ALIGN_CENTER;
				emlv->marginsHeader[0] = 9;
				emlv->margins[0] = 9;


				EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_PICKTEX_PREVIEW));
				empv->dontLetUserChangeZoom = true;
				empv->colBackground = RGB(0,0,0);

				createTexturesListPT2(360, 360);
				fillGUITexturesListPT2(hWnd);

				char * fname = (char *)lParam;
				int id = findPT2IDFromFilename(fname);
				if (id<0)
					id = 0;

				if (loadedTexturesListPT2.objCount>0)
				{
					emlv->selected = id;
					InvalidateRect(emlv->hwnd, NULL, false);

					char fname2[2048];
					fname2[0] = 0;
					char * fptr = NULL;
					GetFullPathName(fname, 2048, fname2, &fptr);
					SetWindowText(GetDlgItem(hWnd, IDC_PICKTEX_PATH), fname2);

					if (empv)
					{
						empv->byteBuff = &loadedTexturesListPT2[id]->preview;
						InvalidateRect(empv->hwnd, NULL, false);
					}

					if (emlv->maxShownRows>=loadedTexturesListPT2.objCount)
					{
						emlv->forceDrawScrollbar = false;
						emlv->colWidths[0] = 280;
					}
				}
			}
			break;
		case WM_CLOSE:
			{
				SendMessage(hWnd, WM_COMMAND, MAKELONG(IDC_PICKTEX_CANCEL, BN_CLICKED), (LPARAM)GetDlgItem(hWnd, IDC_PICKTEX_CANCEL));
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBmp)
					DeleteObject(hBgBmp);
				hBgBmp = 0;

				EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_PICKTEX_PREVIEW));
				empv->byteBuff = NULL;
				releaseTexturesListPT2();
			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBmp)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBmp);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}
				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_PICKTEX_OK:
						{
							char * result = NULL;
							EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_PICKTEX_LIST));
							if (emlv  &&  emlv->selected>-1  &&  emlv->selected<loadedTexturesListPT2.objCount)
								result = copyString(loadedTexturesListPT2[emlv->selected]->fullfilename);
							EndDialog(hWnd, (INT_PTR)result);
						}
						break;
					case IDC_PICKTEX_CANCEL:
						{
							EndDialog(hWnd, (INT_PTR)(NULL));
						}
						break;
					case IDC_PICKTEX_LIST:
						{
							HWND hList= GetDlgItem(hWnd, IDC_PICKTEX_LIST);
							EM2ListView * emlv = GetEM2ListViewInstance(hList);
							if (!emlv)
								break;
							if (wmEvent==LV_SELECTION_CHANGED)
							{
							}
							if (wmEvent==LV_MOUSEOVER_CHANGED)
							{
								int id = emlv->mouseOver;
								if (id<0)
									id = emlv->selected;
								if (id<0  ||  id>=loadedTexturesListPT2.objCount)
									break;

								PickedTexture * pt = loadedTexturesListPT2[id];
								if (!pt)
									break;
								EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_PICKTEX_PREVIEW));
								if (empv)
								{
									empv->byteBuff = &(pt->preview);
									InvalidateRect(empv->hwnd, NULL, false);
								}

								HWND hftext = GetDlgItem(hWnd, IDC_PICKTEX_PATH);
								char fname2[2048];
								fname2[0] = 0;
								char * fptr = NULL;
								GetFullPathName(pt->fullfilename, 2048, fname2, &fptr);
								SetWindowText(hftext, fname2);
							}
						}
						break;
					case IDC_PICKTEX_OPEN_FOLDER:
						{
							HWND hList = GetDlgItem(hWnd, IDC_PICKTEX_LIST);
							EM2ListView * emlv = GetEM2ListViewInstance(hList);
							if (!emlv)
								break;
							if (emlv->selected<0  ||  emlv->selected>=loadedTexturesListPT2.objCount)
								break;

							char * fname = loadedTexturesListPT2[emlv->selected]->fullfilename;
							if (!fname)
								break;

							char fname2[2048];
							fname2[0] = 0;
							char * fptr = NULL;
							GetFullPathName(fname, 2048, fname2, &fptr);

							HRESULT hres = CoInitialize(NULL);

							ITEMIDLIST __unaligned *pIDL = ILCreateFromPath(fname2) ;
							if(NULL != pIDL)
							{
								SHOpenFolderAndSelectItems(pIDL, 0, 0, 0) ;
								ILFree(pIDL) ;
							}
							else
							{
								Logger::add("ILCreateFromPath failed");
							}
							CoUninitialize();
						}
						break;
				}
			}	// end WM_COMMAND
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

void pickTexDlg2UpdateBgShifts(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_PICKTEX_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgrPrev = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_PICKTEX_GR_SELECTED_TEX));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_GR_SELECTED_TEX), 0, 0, emgrPrev->bgShiftX, emgrPrev->bgShiftY);
	EM2GroupBar * emgrList = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_PICKTEX_GR_LIST_OF_TEX));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_GR_LIST_OF_TEX), 0, 0, emgrList->bgShiftX, emgrList->bgShiftY);

	EM2Text * emtPath = GetEM2TextInstance(GetDlgItem(hWnd, IDC_PICKTEX_PATH));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_PATH), 0, 0, emtPath->bgShiftX, emtPath->bgShiftY);

	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_PICKTEX_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);
	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_PICKTEX_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);
	EM2Button * embOpenFolder = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_PICKTEX_OPEN_FOLDER));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_OPEN_FOLDER), 0, 0, embOpenFolder->bgShiftX, embOpenFolder->bgShiftY);

	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_PICKTEX_LIST));
	updateControlBgShift(GetDlgItem(hWnd, IDC_PICKTEX_LIST), 0, 0, emlv->bgShiftX, emlv->bgShiftY);
}

//------------------------------------------------------------------------------------------------

void pickTexDlg2SetPositionsOnStart(HWND hWnd)
{
	RECT tmprect, wrect,crect;
	GetWindowRect(hWnd, &wrect);
	GetClientRect(hWnd, &crect);
	tmprect.left = 0;		tmprect.top = 0;
	tmprect.right = 774 + wrect.right-wrect.left-crect.right;
	tmprect.bottom = 542 + wrect.bottom-wrect.top-crect.bottom;
	SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);

	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_TITLE), HWND_TOP, 250,11, 314, 17, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_GR_SELECTED_TEX),			HWND_TOP,			18,		40,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_GR_LIST_OF_TEX),			HWND_TOP,			396,	40,			360,	16,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_PREVIEW),					HWND_TOP,			18,		74,			360,	360,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_LIST),					HWND_TOP,			396,	66,			360,	368,		0);


	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_PATH),					HWND_TOP,			18,		448,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_OPEN_FOLDER),				HWND_TOP,			18,		475,		90,		22,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_OK),						HWND_TOP,			306,	505,		72,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_PICKTEX_CANCEL),					HWND_TOP,			396,	505,		72,		22,			0);


}

//------------------------------------------------------------------------------------------------

void fillGUITexturesListPT2(HWND hDlg)
{
	if (!hDlg)
		return;
	HWND hList= GetDlgItem(hDlg, IDC_PICKTEX_LIST);
	EM2ListView * emlv = GetEM2ListViewInstance(hList);
	if (!emlv)
		return;
	emlv->freeEntries();

	int ntex = loadedTexturesListPT2.objCount;
	char buf[64];
	for (int i=0; i<ntex; i++)
	{
		emlv->addEmptyRow();
		emlv->setData(0, i, loadedTexturesListPT2[i]->filename);
		sprintf_s(buf, 64, "%d X %d", loadedTexturesListPT2[i]->width, loadedTexturesListPT2[i]->height);
		emlv->setData(1, i, buf);
	}

}

//----------------------------------------------------------------------------------------------

bool addTextureToListPT2(Texture *tex, unsigned int prevSizeX, unsigned int prevSizeY)
{
	CHECK(tex);
	CHECK(tex->isValid());
	char * ffname = tex->fullfilename;
	CHECK(ffname);
	int n = loadedTexturesListPT2.objCount;
	for (int i=0; i<n; i++)
	{
		if (!loadedTexturesListPT2[i])
			continue;
		if (!loadedTexturesListPT2[i]->fullfilename)
			continue;
		if (!strcmp(ffname, loadedTexturesListPT2[i]->fullfilename))
			return false;	// already have that tex on list, no copy needed
	}

	PickedTexture * pt = new PickedTexture();
	pt->filename = copyString(tex->filename);
	pt->fullfilename = copyString(tex->fullfilename);

	int ow = tex->bmp.getWidth();
	int oh = tex->bmp.getHeight();
	int dw, dh;
	if (ow*prevSizeY>oh*prevSizeX)
	{
		dw = prevSizeX;
		dh = min(prevSizeY, prevSizeX*oh/ow);
	}
	else
	{
		dw = min(prevSizeX, prevSizeY*ow/oh);
		dh = prevSizeY;
	}
	pt->preview.allocBuffer(dw, dh);
	for (int y=0; y<dh; y++)
		for (int x=0; x<dw; x++)
		{
			int ox = min(x*ow/(int)dw, ow-1);
			int oy = min(y*oh/(int)dh, oh-1);
			Color4 oc = tex->bmp.getColorUnprocessed(ox, oy);
			unsigned char cr = (unsigned char)max(0.0f, min(oc.r*255, 255.0f));
			unsigned char cg = (unsigned char)max(0.0f, min(oc.g*255, 255.0f));
			unsigned char cb = (unsigned char)max(0.0f, min(oc.b*255, 255.0f));
			pt->preview.imgBuf[y][x] = RGB(cr, cg, cb);
		}

	pt->width = tex->bmp.getWidth();
	pt->height = tex->bmp.getHeight();

	loadedTexturesListPT2.add(pt);
	loadedTexturesListPT2.createArray();

	return true;
}

//----------------------------------------------------------------------------------------------

void createTexturesListPT2(unsigned int prevSizeX, unsigned int prevSizeY)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();
	int mainSceneId = -1;
	if (rMain->sceneID > -1)
		mainSceneId = rMain->sceneID;	// renderer main window loaded scene 
	else
	{		//find max plugin scene
		if (rtr->pluginMaxSceneId>-1)
			mainSceneId = rtr->pluginMaxSceneId;
	}

	// main scene textures
	if (mainSceneId > -1)
	{
		Scene * sc = rtr->scenes[mainSceneId];
		if (!sc)
		{
			Logger::add("Error: TexturePicker: NULL scene on raytracer list.");
			return;
		}
		int ntex = sc->texManager.textures.objCount;
		for (int i=0; i<ntex; i++)
			addTextureToListPT2(sc->texManager.textures[i], prevSizeX, prevSizeY);
	}

	// Material from Mat Editor
	MaterialNox * mat = NULL;
	Scene * sc = rtr->curScenePtr;
	if (sc)
	{
		int edm = sc->sscene.editableMaterial;
		if (edm>-1)
			mat = sc->mats[edm];
	}
	if (mat)
	{
		int id;
		id = mat->tex_displacement.managerID;
		addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
		id = mat->tex_opacity.managerID;
		addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
		
		int nl = mat->layers.objCount;
		for (int i=0; i<nl; i++)
		{
			MatLayer * mlay = &(mat->layers[i]);

			id = mlay->tex_col0.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_col90.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_rough.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_transm.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_weight.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_normal.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_light.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_aniso.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_aniso_angle.managerID;
			addTextureToListPT2( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
		}
	}
}

//----------------------------------------------------------------------------------------------

void releaseTexturesListPT2()
{
	int n = loadedTexturesListPT2.objCount;
	for (int i=0; i<n; i++)
	{
		PickedTexture * pt = loadedTexturesListPT2[i];
		if (!pt)
			continue;
		if (pt->fullfilename)
			free(pt->fullfilename);
		if (pt->filename)
			free(pt->filename);
		pt->fullfilename = NULL;
		pt->filename = NULL;
		pt->preview.freeBuffer();
		delete pt;
	}
	loadedTexturesListPT2.freeList();
	loadedTexturesListPT2.createArray();
}

//----------------------------------------------------------------------------------------------

int findPT2IDFromFilename(char * ffname)
{
	if (!ffname)
		return -1;

	int n = loadedTexturesListPT2.objCount;
	for (int i=0; i<n; i++)
	{
		if (!loadedTexturesListPT2[i])
			continue;
		if (!loadedTexturesListPT2[i]->fullfilename)
			continue;
		if (strcmp(ffname, loadedTexturesListPT2[i]->fullfilename))
			continue;
		return i;
	}
	return -1;
}

//----------------------------------------------------------------------------------------------

