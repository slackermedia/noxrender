#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif


#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"

extern HMODULE hDllModule;
void tempCheckIntegrity(int a);

#define MINSBH 32

void InitEMListView()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMListView";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMListViewProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = CS_DBLCLKS;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMListView *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMListView * GetEMListViewInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMListView * emlv = (EMListView *)GetWindowLongPtr(hwnd, 0);
	#else
		EMListView * emlv = (EMListView *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emlv;
}

void SetEMListViewInstance(HWND hwnd, EMListView *emlv)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emlv);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emlv);
	#endif
}

EMListView::EMListView(HWND hWnd) :
headers(0),
data(0),
colWidths(0),
align(0)
{
	hwnd = hWnd;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hRegion = 0;

	colHeaderBackground			= RGB(96,96,96);
	colHeaderBorderUpLeft		= RGB(128,128,128);
	colHeaderBorderDownRight	= RGB(64,64,64);
	colBackgroundOdd			= RGB(80,80,80);
	colBackgroundEven			= RGB(74,74,74);
	colBackgroundSelected		= RGB(0,0,255);
	colText						= RGB(255,255,255);
	colTextSelected				= RGB(255,255,255);
	colMainBorderUpLeft			= RGB(48,48,48);
	colMainBorderDownRight		= RGB(128,128,128);
	colTextMouseOver			= RGB(255,255,128);
	colBackgroundMouseOver		= RGB(64,128,192);
	colSBTrackBackground		= RGB(88,88,88);
	colSBBackground				= RGB(96,96,96);
	colSBBackgroundClicked		= RGB(112,112,112);
	colSBBorderUpLeft			= RGB(128,128,128);
	colSBBorderDownRight		= RGB(48,48,48);

	numCols = 0;
	rowHeight = 18;
	selected = 0;
	mouseOver = -1;
	firstShown = 0;
	showHeader = true;

	sbWidth = 18;
	sliding = false;

	prepareWindow();
}

EMListView::~EMListView()
{
}


LRESULT CALLBACK EMListViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMListView* emlv;
	emlv = GetEMListViewInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HFONT hOldFont;
	HPEN hOldPen;
	static int ly=0;
	static int lsbst=0;

	switch (msg)
	{
		case WM_CREATE:
			{
				EMListView * emlv1 = new EMListView(hwnd);
				SetEMListViewInstance(hwnd, emlv1);
			}
			break;
		case WM_DESTROY:
			{
				delete emlv;
				SetEMListViewInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			DWORD style;
			style = GetWindowLong(hwnd, GWL_STYLE);
			bool disabled = ((style & WS_DISABLED) > 0);

			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);

			// begin here

			HPEN hPenHeadUpLeft         = CreatePen(PS_SOLID, 1, emlv->colHeaderBorderUpLeft);
			HPEN hPenHeadDownRight      = CreatePen(PS_SOLID, 1, emlv->colHeaderBorderDownRight);
			HPEN hPenMainUpLeft         = CreatePen(PS_SOLID, 1, emlv->colMainBorderUpLeft);
			HPEN hPenMainDownRight      = CreatePen(PS_SOLID, 1, emlv->colMainBorderDownRight);

			HBRUSH hBrHeader     = CreateSolidBrush(emlv->colHeaderBackground);
			HBRUSH hBrOdd        = CreateSolidBrush(emlv->colBackgroundOdd);
			HBRUSH hBrEven       = CreateSolidBrush(emlv->colBackgroundEven);
			HBRUSH hBrSelected   = CreateSolidBrush(emlv->colBackgroundSelected);
			HBRUSH hBrMouseOver  = CreateSolidBrush(emlv->colBackgroundMouseOver);
			hOldFont = (HFONT)SelectObject(hdc, emlv->hFont);
			hOldPen = (HPEN)SelectObject(hdc, hPenHeadUpLeft);
			SetTextColor(hdc, emlv->colText);
			SetBkColor  (hdc, emlv->colHeaderBackground);

			int w = rect.right-rect.left;
			int h = (emlv->maxShownRowsAndHeader)*emlv->rowHeight+2;

			{
				POINT bm1[] = {		{0, h-1},  {0, 0},      {w  , 0}   };
				POINT bm2[] = {		{w-1, 1},  {w-1, h-1},  {0, h-1}   };
				SelectObject(hdc, hPenMainUpLeft);
				Polyline(hdc, bm1, 3);
				SelectObject(hdc, hPenMainDownRight);
				Polyline(hdc, bm2, 3);
			}

			SIZE sz;
			if (emlv->showHeader)
			{
				RECT crect = {1,1,rect.right-1,emlv->rowHeight+1};
				FillRect(hdc, &crect, hBrHeader);
				int lCur = 1;
				for (int i=0; i<emlv->numCols; i++)
				{
					if (i >= emlv->headers.objCount)
						break;
					if (i >= emlv->colWidths.objCount)
						break;
					char * header = emlv->headers[i];
					if (!header)
						break;
					int lng = (int)strlen(header);
					crect.top = 1;
					crect.bottom = emlv->rowHeight;
					crect.left = lCur;
					crect.right = lCur + emlv->colWidths[i];

					GetTextExtentPoint32(hdc, header, lng, &sz);
					ExtTextOut(hdc, lCur+1, (emlv->rowHeight-sz.cy)/2, ETO_OPAQUE, &crect, header, lng, 0);

					POINT b1[] = { {crect.left,  crect.bottom} , {crect.left,  crect.top} ,     {crect.right, crect.top} };
					POINT b2[] = { {crect.right-1, crect.top+1}    , {crect.right-1, crect.bottom} ,  {crect.left, crect.bottom} };
					SelectObject(hdc, hPenHeadUpLeft);
					Polyline(hdc, b1, 3);
					SelectObject(hdc, hPenHeadDownRight);
					Polyline(hdc, b2, 3);

					lCur += emlv->colWidths[i] + 1;
				}
			}

			int fs = emlv->firstShown;
			for (int j=0; j<emlv->maxShownRows; j++)
			{
				RECT crect = {1, j*emlv->rowHeight+emlv->headerHeight+1, w-1, (j+1)*emlv->rowHeight+emlv->headerHeight+1};
				if ((j+fs)%2)
				{
					SetBkColor(hdc, emlv->colBackgroundOdd);
					SetTextColor(hdc, emlv->colText);
					FillRect(hdc, &crect, hBrOdd);
				}
				else
				{
					SetBkColor(hdc, emlv->colBackgroundEven);
					SetTextColor(hdc, emlv->colText);
					FillRect(hdc, &crect, hBrEven);
				}
				if (j+fs == emlv->selected)
				{
					SetBkColor(hdc, emlv->colBackgroundSelected);
					SetTextColor(hdc, emlv->colTextSelected);
					FillRect(hdc, &crect, hBrSelected);
				}
				if (j+fs == emlv->mouseOver)
				{
					SetBkColor(hdc, emlv->colBackgroundMouseOver);
					SetTextColor(hdc, emlv->colTextMouseOver);
					FillRect(hdc, &crect, hBrMouseOver);
				}

				if (j+fs >= emlv->data.objCount)
					continue;

				int curL = 1;
				for (int i=0; i<emlv->numCols; i++)
				{
					if (emlv->colWidths.objCount <= i)
						break;
					char * ddata = (emlv->data[j+fs])[i];
					if (!ddata)
						continue;
						int cLen = (int)strlen(ddata);
					if (cLen < 1)
						continue;
					
					RECT crect = {curL+1, j*emlv->rowHeight+emlv->headerHeight+3, curL+emlv->colWidths[i]-2, (j+1)*emlv->rowHeight+emlv->headerHeight};
					GetTextExtentPoint32(hdc, ddata, cLen, &sz);

					switch (emlv->align[i])
					{
						case EMListView::ALIGN_LEFT:
							ExtTextOut(hdc, curL+1, (emlv->rowHeight-sz.cy)/2+crect.top, ETO_CLIPPED, &crect, ddata, cLen, 0);
							break;
						case EMListView::ALIGN_CENTER:
							ExtTextOut(hdc, curL+(emlv->colWidths[i]-sz.cx-2)/2, (emlv->rowHeight-sz.cy)/2+crect.top, ETO_CLIPPED, &crect, ddata, cLen, 0);
							break;
						case EMListView::ALIGN_RIGHT:
							ExtTextOut(hdc, curL+emlv->colWidths[i]-sz.cx-2, (emlv->rowHeight-sz.cy)/2+crect.top, ETO_CLIPPED, &crect, ddata, cLen, 0);
							break;
					}
					
					curL += emlv->colWidths[i] + 1;
				}
			}

			emlv->drawScrollBar(hdc);

			SelectObject(hdc, hOldFont);
			SelectObject(hdc, hOldPen);

			DeleteObject(hPenHeadUpLeft);
			DeleteObject(hPenHeadDownRight);
			DeleteObject(hPenMainUpLeft);
			DeleteObject(hPenMainDownRight);

			DeleteObject(hBrHeader);
			DeleteObject(hBrOdd);
			DeleteObject(hBrEven);
			DeleteObject(hBrSelected);
			DeleteObject(hBrMouseOver);

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}	// end WM_PAINT
		break;
		case WM_MOUSEMOVE:
		{
			if (!GetCapture())
				SetCapture(hwnd);

			if (emlv->sliding)
			{
				int my = (short)HIWORD(lParam);
				emlv->sbStart = lsbst + my-ly;
				emlv->calcFP();
				InvalidateRect(hwnd, NULL, false);
			}
			else
			{
				int newMouseOver = emlv->mouseOver;
				int mx = (short)LOWORD(lParam);
				int my = (short)HIWORD(lParam);
				GetClientRect(hwnd, &rect);
				int w = rect.right - rect.left;
				int h = (emlv->maxShownRowsAndHeader)*emlv->rowHeight+2;

				int sls = (emlv->maxShownRows<emlv->data.objCount) ? emlv->sbWidth : 0;
				if (mx < 1   ||   mx > w-2   ||   my < 1   ||   my > h-1) 
				{	// outside and borders
					newMouseOver = -1;
					if (!emlv->sliding)
						ReleaseCapture();
				}
				else
				{
					if (mx > w-2-sls)
					{	// on scrollbar however not sliding
						newMouseOver = -1;
					}
					else
					{	
						if (my < emlv->headerHeight+1)
						{	// on header
							newMouseOver = -1;
						}
						else
						{	// on data
							newMouseOver = (my-1-emlv->headerHeight)/emlv->rowHeight+emlv->firstShown;
							if (newMouseOver >= emlv->data.objCount)
							{
								newMouseOver = -1;
							}
						}
					}

				}

				if (newMouseOver != emlv->mouseOver)
				{
					emlv->mouseOver = newMouseOver;
					InvalidateRect(hwnd, NULL, false);
					emlv->notifyMouseOverChanged();
				}
			}
		}
		break;
		case WM_LBUTTONDOWN:
		{
			SetFocus(hwnd);
			int mx = (short)LOWORD(lParam);
			int my = (short)HIWORD(lParam);
			GetClientRect(hwnd, &rect);
			int w = rect.right - rect.left;
			int h = emlv->maxShownRowsAndHeader * emlv->rowHeight + 2;

			if (mx < 1   ||   mx > w-2   ||   my < 1   ||   my > h-1)
				break;

			int sls = (emlv->maxShownRows<emlv->data.objCount) ? emlv->sbWidth : 0;
			if (mx > w-2-sls)
			{	// on trackbar
				if (my > emlv->sbStart   &&   my <= emlv->sbEnd)
				{
					emlv->sliding = true;
					ly = my;
					lsbst = emlv->sbStart;
					InvalidateRect(hwnd, NULL, false);
				}
				else
				{
					if (my <= emlv->sbStart)
					{
						emlv->firstShown -= emlv->maxShownRows;
						emlv->firstShown = max(0, emlv->firstShown);
						emlv->calcSB();
					}
					else
					{
						emlv->firstShown += emlv->maxShownRows;
						emlv->firstShown = min(emlv->firstShown,  emlv->data.objCount-emlv->maxShownRows);
						emlv->calcSB();
					}
					InvalidateRect(hwnd, NULL, false);
				}
			}
			else
				if (emlv->mouseOver>-1)
				{	// data selection
					emlv->selected = emlv->mouseOver;
					InvalidateRect(hwnd, NULL, false);
					emlv->notifySelectionChanged();
				}
		}
		break;
		case WM_LBUTTONUP:
		{
			emlv->sliding = false;
			int mx = (short)LOWORD(lParam);
			int my = (short)HIWORD(lParam);
			GetClientRect(hwnd, &rect);
			int w = rect.right-rect.left;
			int h = (emlv->maxShownRowsAndHeader)*emlv->rowHeight+2;

			if (mx < 0   ||   mx > w   ||   my < 0   ||   my > h)
				ReleaseCapture();
			InvalidateRect(hwnd, NULL, false);
		}
		break;
		case WM_LBUTTONDBLCLK:
		{
			if (emlv->mouseOver>-1)
			{
				emlv->selected = emlv->mouseOver;
				InvalidateRect(hwnd, NULL, false);
				emlv->notifyDoubleClick();
			}
		}
		break;
		case WM_MOUSEWHEEL:
		{
			int zDelta = (int)wParam;
			if (zDelta > 0)
			{
				emlv->firstShown--;
				emlv->firstShown = max(0, emlv->firstShown);
				if (emlv->data.objCount > 0)
					emlv->calcSB();
			}
			else
			{
				emlv->firstShown++;
				emlv->firstShown = min(emlv->firstShown,  emlv->data.objCount-emlv->maxShownRows);
				if (emlv->data.objCount > 0)
					emlv->calcSB();
			}
			InvalidateRect(hwnd, NULL, false);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMListView::calcSB()
{
	int h = (maxShownRowsAndHeader)*rowHeight;
	if (data.objCount < 1   ||   data.objCount == maxShownRows)
	{
		sbStart = 0;
		sbEnd = h;
		return;
	}
	int bar = (h*maxShownRows)/data.objCount;
	bar = max(32, bar);
	int rest = h - bar;
	sbStart = firstShown * rest / (data.objCount - maxShownRows);
	sbEnd = sbStart + bar;
}

void EMListView::calcFP()
{
	int h = (maxShownRowsAndHeader)*rowHeight;
	if (data.objCount < 1)
	{
		firstShown = 0;
		sbStart = 0;
		sbEnd = h;
		return;
	}
	int bar = (h*maxShownRows)/data.objCount;
	bar = max(32, bar);
	int rest = h - bar;
	firstShown = sbStart*(data.objCount-maxShownRows)/rest;
	firstShown = max(0, min(firstShown, data.objCount - maxShownRows));
	sbEnd = sbStart + bar;
}

void EMListView::drawScrollBar(HDC hdc)
{
	if (data.objCount <= maxShownRows)
	{
		firstShown = 0;
		return;
	}

	RECT crect;
	GetClientRect(hwnd, &crect);

	if (firstShown + maxShownRows > data.objCount)
		firstShown = data.objCount - maxShownRows;

	calcSB();
	RECT bf = {crect.right-sbWidth-1, 1, crect.right-1, (maxShownRowsAndHeader)*rowHeight+1};
	RECT bb = {crect.right-sbWidth-1, sbStart+1, crect.right-1, sbEnd+1};

	HBRUSH hbrtbg = CreateSolidBrush(colSBTrackBackground);
	HBRUSH hbrbgn = CreateSolidBrush(colSBBackground);
	HBRUSH hbrbgs = CreateSolidBrush(colSBBackgroundClicked);
	FillRect(hdc, &bf, hbrtbg);
	if (sliding)
		FillRect(hdc, &bb, hbrbgs);
	else
		FillRect(hdc, &bb, hbrbgn);
	DeleteObject(hbrbgn);
	DeleteObject(hbrbgs);
	DeleteObject(hbrtbg);

	HPEN hPenUpLeft    = CreatePen(PS_SOLID, 1, colSBBorderUpLeft);
	HPEN hPenDownRight = CreatePen(PS_SOLID, 1, colSBBorderDownRight);
	HPEN hPenOld = (HPEN)SelectObject(hdc, hPenUpLeft);
	POINT p1[] =	{	{crect.right-sbWidth-1,  sbEnd}, 
						{crect.right-sbWidth-1,  sbStart+1}, 
						{crect.right-1,  sbStart+1}		};
	POINT p2[] =	{	{crect.right-2,  sbStart+2}, 
						{crect.right-2,  sbEnd}, 
						{crect.right-sbWidth-1,  sbEnd}		};

	Polyline(hdc, p1, 3);
	SelectObject(hdc, hPenDownRight);
	Polyline(hdc, p2, 3);

	int cy = (sbStart + sbEnd) / 2;
	int l = crect.right-sbWidth;
	int r = crect.right;

				POINT line1a[] = { {l+4, cy-5}, {r-5, cy-5} };
				POINT line1b[] = { {l+4, cy-4}, {r-5, cy-4} };
				SelectObject(hdc, hPenDownRight);
				Polyline(hdc, line1a, 2);
				SelectObject(hdc, hPenUpLeft);
				Polyline(hdc, line1b, 2);
				POINT line2a[] = { {l+4, cy  }, {r-5, cy  } };
				POINT line2b[] = { {l+4, cy+1}, {r-5, cy+1} };
				SelectObject(hdc, hPenDownRight);
				Polyline(hdc, line2a, 2);
				SelectObject(hdc, hPenUpLeft);
				Polyline(hdc, line2b, 2);
				POINT line3a[] = { {l+4, cy+5}, {r-5, cy+5} };
				POINT line3b[] = { {l+4, cy+6}, {r-5, cy+6} };
				SelectObject(hdc, hPenDownRight);
				Polyline(hdc, line3a, 2);
				SelectObject(hdc, hPenUpLeft);
				Polyline(hdc, line3b, 2);


	DeleteObject(hPenUpLeft);
	DeleteObject(hPenDownRight);
}

//----------------------

void EMListView::prepareWindow()
{
	RECT crect;
	GetClientRect(hwnd, &crect);
	int cHeight = crect.bottom - crect.top - 2;
	maxShownRows = maxShownRowsAndHeader = cHeight/rowHeight;
	if (showHeader)
	{
		maxShownRows--;
		headerHeight = rowHeight;
	}
	else
		headerHeight = 0;

	HRGN nReg = CreateRectRgn(0,0, crect.right-crect.left,  (maxShownRowsAndHeader)*rowHeight+2);
	if (nReg)
	{
		SetWindowRgn(hwnd, nReg, TRUE);
		if (hRegion)
			DeleteObject(hRegion);
		hRegion = nReg;		
	}


}

bool EMListView::addEmptyRow()
{
	char ** row = (char **)malloc(sizeof(char*)*numCols);
	if (!row)
		return false;
	
	data.add(row);
	data.createArray();
	int last = data.objCount-1;
	for (int i=0; i<numCols; i++)
		data[last][i] = NULL;

	return true;
}

bool EMListView::setData(int col, int row, char * str)
{
	if (!str)
		return false;
	if (row >= data.objCount)
		return false;
	if (col >= numCols)
		return false;
	if (!data[row])
		return false;

	int ll = (int)strlen(str)+1;
	char * buf = (char*)malloc(ll);
	if (!buf)
		return false;

	char * olddata = data[row][col];
	data[row][col] = buf;
	if (olddata)
		free(olddata);
	
	sprintf_s(data[row][col], ll, "%s", str);
	
	return true;
}

void EMListView::freeEntries()
{
	for (int i=0; i<data.objCount; i++)
	{
		if (data[i])
		{
			for (int j=0; j<numCols; j++)
			{
				if (data[i][j])
				{
					free(data[i][j]);
					data[i][j] = NULL;
				}
			}
		}
	}
	data.freeList();
	data.createArray();
}

void EMListView::setColNumAndClearData(int num, int width)
{
	freeEntries();
	numCols = num;

	colWidths.freeList();
	for (int i=0; i<numCols; i++)
		colWidths.add(width);
	colWidths.createArray();
	align.freeList();
	for (int i=0; i<numCols; i++)
		align.add(ALIGN_LEFT);
	align.createArray();
}

void EMListView::notifySelectionChanged()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, LV_SELECTION_CHANGED), (LPARAM)hwnd);
}

void EMListView::notifyMouseOverChanged()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, LV_MOUSEOVER_CHANGED), (LPARAM)hwnd);
}

void EMListView::notifyDoubleClick()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, LV_DOUBLECLIKED), (LPARAM)hwnd);
}

void EMListView::gotoEnd()
{
	firstShown = data.objCount - maxShownRows;
	firstShown = max(0, firstShown);
}

void EMListView::Refresh()
{
	InvalidateRect(hwnd, NULL, false);
}

