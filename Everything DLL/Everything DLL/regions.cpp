#define _CRT_RAND_S
#include "regions.h"
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "log.h"
#include "raytracer.h"

ImageRegion::ImageRegion()
{
	regions = NULL;
	hits = NULL;
	regionsExists = false;
	airbrush_size = 15;
	regionsSelected = NULL;
	numRegionsSelected = 0;
	rnX = rnY = 0;
	ibX = ibY = 1;
	overallHits = 0;

}

ImageRegion::~ImageRegion()
{
	regionsExists = false;
	numRegionsSelected = 0;
	if (regions)
		free(regions);
	regions = NULL;
	if (hits)
		free(hits);
	hits = NULL;
	if (regionsSelected)
		free(regionsSelected);
	regionsSelected = NULL;
}


bool ImageRegion::randomPointFromRegions(float &x, float &y)
{
	if (!regions)
		return false;
	if (!regionsExists)
		return false;
	if (numRegionsSelected < 1)
		return false;

	#ifdef RAND_PER_THREAD
		RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
		unsigned int iReg = rg->getRandomInt(1000000);
		iReg = iReg % max(1, numRegionsSelected);
		int rReg = regionsSelected[iReg];
	#else
		unsigned int iReg;
		while(rand_s(&iReg));
		int rReg = (int)((double)iReg / (double)UINT_MAX * (double)numRegionsSelected);
		rReg = regionsSelected[rReg];
	#endif

	int rx = rReg%rnX;
	int ry = rReg/rnX;

	#ifdef RAND_PER_THREAD
		double xx = rg->getRandomDouble()*RSIZE;
		double yy = rg->getRandomDouble()*RSIZE;
	#else
		while(rand_s(&iReg));
		double xx = ((double)iReg/(double)UINT_MAX*RSIZE);
		while(rand_s(&iReg));
		double yy = ((double)iReg/(double)UINT_MAX*RSIZE);
	#endif

	xx += RSIZE*rx;
	yy += RSIZE*ry;

	x = (float)(xx/(double)ibX);
	y = (float)(yy/(double)ibY);

	if (x>1 || y>1)
		return randomPointFromRegions(x,y);
	
	return true;
}

bool ImageRegion::makeRegionsList()
{
	bool re = false;
	
	int * newRegionsSelected = NULL;
	newRegionsSelected = (int *)malloc(sizeof(int)*rnX*rnY);
	if (!newRegionsSelected)
		return false;
	int nNumReg = 0;
	
	int i;
	for (i=0; i<rnX*rnY; i++)
		if (regions[i])
		{
			re = true;
			newRegionsSelected[nNumReg] = i;
			nNumReg++;
		}
	
	int * toDel = regionsSelected;
	regionsSelected = newRegionsSelected;
	numRegionsSelected = nNumReg;
	if (toDel)
		free(toDel);
	regionsExists = re;
	return true;
}


bool ImageRegion::createRegions(int bwidth, int bheight)
{
	if (regions)
		free (regions);
	if (hits)
		free (hits);

	int ix = ibX = bwidth;
	int iy = ibY = bheight;

	rnX = ix/RSIZE;
	if (ix%RSIZE)
		rnX++;
	rnY = iy/RSIZE;
	if (iy%RSIZE)
		rnY++;

	regions = (bool *)malloc(rnX*rnY);
	for (int i=0; i<rnX*rnY; i++)
		regions[i] = false;

	hits = (double *)malloc(rnX*rnY*sizeof(double));
	for (int i=0; i<rnX*rnY; i++)
		hits[i] = 0;
		return true;
}


void ImageRegion::clearRegions()
{
	if (!regions)
		return;
	for (int i=0; i<rnX; i++)
		for (int j=0; j<rnY; j++)
			regions[j*rnX+i] = false;
	makeRegionsList();
}

void ImageRegion::fillRegions()
{
	if (!regions)
		return;
	for (int i=0; i<rnX; i++)
		for (int j=0; j<rnY; j++)
			regions[j*rnX+i] = true;
	makeRegionsList();
}

void ImageRegion::inverseRegions()
{
	if (!regions)
		return;
	for (int i=0; i<rnX; i++)
		for (int j=0; j<rnY; j++)
			regions[j*rnX+i] = !regions[j*rnX+i];
	makeRegionsList();
}

void ImageRegion::checkRegions()
{
	if (!regions)
	{
		regionsExists = false;
		return;
	}

	for (int i=0; i<rnX; i++)
		for (int j=0; j<rnY; j++)
			if (regions[j*rnX+i])
			{
				regionsExists = true;
				return;
			}
	regionsExists = false;
}

void ImageRegion::copyFast(ImageRegion &src, ImageRegion &dst)
{
	memcpy(dst.regionsSelected, src.regionsSelected, sizeof(int)*src.rnX*src.rnY);
	dst.numRegionsSelected = src.numRegionsSelected;
	memcpy(dst.hits, src.hits, sizeof(double)*src.rnX*src.rnY);
}

bool ImageRegion::isRegionSelected(int px, int py)
{
	int rx = px/RSIZE;
	int ry = py/RSIZE;
	return regions[ry*rnX+rx];
}

void ImageRegion::incrementHits(int num)
{
	overallHits++;
	return;
	if (regionsExists)
	{
		double toadd = (double)num / (double)numRegionsSelected;
		for (int i=0; i<numRegionsSelected; i++)
			hits[regionsSelected[i]] += toadd;
	}
	else
	{
		int all = rnX*rnY;
		double toadd = (double)num / (double)(all);
		for (int i=0; i<all; i++)
			hits[i] += toadd;
	}
}

