#include "max2013predef.h"
#include "NOXRenderer.h"

#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"
#include "noxdebug.h"


#define NOX_REND_CHUNK_RENDERER 0x4000
#define NOX_REND_CHUNK_FILENAME 0x4001
#define NOX_REND_CHUNK_USERCOLORS 0x4002
#define NOX_REND_CHUNK_ENGINE 0x4003

#define NOX_REND_CHUNK_OVERRIDE_MAT_ON 0x4021

#define NOX_REND_CHUNK_FILE_POST 0x4031
#define NOX_REND_CHUNK_FILE_POST_ENABLED 0x4032
#define NOX_REND_CHUNK_FILE_BLEND 0x4033
#define NOX_REND_CHUNK_FILE_BLEND_ENABLED 0x4034
#define NOX_REND_CHUNK_FILE_FINAL 0x4035
#define NOX_REND_CHUNK_FILE_FINAL_ENABLED 0x4036

#define NOX_REND_CHUNK_FILE_MB_ON 0x4041
#define NOX_REND_CHUNK_FILE_MB_SHUTTER 0x4042

#define NOX_REND_CHUNK_SCENE_NAME 0x4010
#define NOX_REND_CHUNK_SCENE_AUTHOR_NAME 0x4011
#define NOX_REND_CHUNK_SCENE_AUTHOR_EMAIL 0x4012
#define NOX_REND_CHUNK_SCENE_AUTHOR_WWW 0x4013

#define NOX_REND_CHUNK_BLEND_NAME1 0x4310
#define NOX_REND_CHUNK_BLEND_NAME2 0x4311
#define NOX_REND_CHUNK_BLEND_NAME3 0x4312
#define NOX_REND_CHUNK_BLEND_NAME4 0x4313
#define NOX_REND_CHUNK_BLEND_NAME5 0x4314
#define NOX_REND_CHUNK_BLEND_NAME6 0x4315
#define NOX_REND_CHUNK_BLEND_NAME7 0x4316
#define NOX_REND_CHUNK_BLEND_NAME8 0x4317
#define NOX_REND_CHUNK_BLEND_NAME9 0x4318
#define NOX_REND_CHUNK_BLEND_NAME10 0x4319
#define NOX_REND_CHUNK_BLEND_NAME11 0x431A
#define NOX_REND_CHUNK_BLEND_NAME12 0x431B
#define NOX_REND_CHUNK_BLEND_NAME13 0x431C
#define NOX_REND_CHUNK_BLEND_NAME14 0x431D
#define NOX_REND_CHUNK_BLEND_NAME15 0x431E
#define NOX_REND_CHUNK_BLEND_NAME16 0x431F

#define SCHECK(r) if (r!=IO_OK) return r;

IOResult NOXRenderer::Load(ILoad *iload) 
{
	ADD_LOG_PARTS_MAIN("NOXRenderer::Load");

	IOResult res;
	int id;
	unsigned long rd;
	int blend = -1;

	while (IO_OK == (res=iload->OpenChunk())) 
	{
		switch(id = iload->CurChunkID())  
		{
			case NOX_REND_CHUNK_RENDERER:
				while (IO_OK == (res=iload->OpenChunk()))
				{
					switch(id = iload->CurChunkID())  
					{
						// FILENAME
						case NOX_REND_CHUNK_FILENAME:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (filename)
										free(filename);
									filename = atemp;
								}
							}
						}
						break;

						// SCENE NAME
						case NOX_REND_CHUNK_SCENE_NAME:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (scene_name)
										free(scene_name);
									scene_name = atemp;
								}
							}
						}
						break;

						// AUTHOR NAME
						case NOX_REND_CHUNK_SCENE_AUTHOR_NAME:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (scene_author_name)
										free(scene_author_name);
									scene_author_name = atemp;
								}
							}
						}
						break;

						// AUTHOR EMAIL
						case NOX_REND_CHUNK_SCENE_AUTHOR_EMAIL:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (scene_author_email)
										free(scene_author_email);
									scene_author_email = atemp;
								}
							}
						}
						break;

						// AUTHOR EMAIL
						case NOX_REND_CHUNK_SCENE_AUTHOR_WWW:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (scene_author_www)
										free(scene_author_www);
									scene_author_www = atemp;
								}
							}
						}
						break;

						// USER COLORS
						case NOX_REND_CHUNK_USERCOLORS:
						{
							int bs = (int)iload->CurChunkLength();
							if (bs != sizeof(float)*54)
								break;
							float colors[54];
							res = iload->Read(colors, bs, &rd);
							if (res!=IO_OK) 
								return res;
							if (rd != bs)
								break;
							for (int i=0; i<18; i++)
							{
								float r,g,b;
								r = colors[i*3+0];
								g = colors[i*3+1];
								b = colors[i*3+2];
								TrRaytracer::setUserColors(i, r,g,b);
							}
						}
						break;

						// OVERRIDE MATERIAL CHECKBOX
						case NOX_REND_CHUNK_OVERRIDE_MAT_ON:
							{
								int tint;
								res = iload->Read(&tint, sizeof(int), &rd);
								if (res!=IO_OK) 
									return res;	
								overrideMatEnabled = (tint!=0);
							}
							break;

						// POST FILE
						case NOX_REND_CHUNK_FILE_POST:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (filename_post)
										free(filename_post);
									filename_post = atemp;
								}
							}
						}
						break;

						// BLEND FILE
						case NOX_REND_CHUNK_FILE_BLEND:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (filename_blend)
										free(filename_blend);
									filename_blend = atemp;
								}
							}
						}
						break;

						// FINAL FILE
						case NOX_REND_CHUNK_FILE_FINAL:
						{
							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (ctemp  &&  strlen(ctemp)>0)
							{
								int l = (int)strlen(ctemp)+1;
								char * atemp = (char*)malloc(l);
								if (atemp)
								{
									sprintf_s(atemp, l , "%s", ctemp);
									if (filename_final)
										free(filename_final);
									filename_final = atemp;
								}
							}
						}
						break;

						// POST FILE ENABLED CHECKBOX
						case NOX_REND_CHUNK_FILE_POST_ENABLED:
							{
								int tint;
								res = iload->Read(&tint, sizeof(int), &rd);
								if (res!=IO_OK) 
									return res;	
								use_file_post = (tint!=0);
							}
							break;

						// BLEND FILE ENABLED CHECKBOX
						case NOX_REND_CHUNK_FILE_BLEND_ENABLED:
							{
								int tint;
								res = iload->Read(&tint, sizeof(int), &rd);
								if (res!=IO_OK) 
									return res;	
								use_file_blend = (tint!=0);
							}
							break;

						// FINAL FILE ENABLED CHECKBOX
						case NOX_REND_CHUNK_FILE_FINAL_ENABLED:
							{
								int tint;
								res = iload->Read(&tint, sizeof(int), &rd);
								if (res!=IO_OK) 
									return res;	
								use_file_final = (tint!=0);
							}
							break;
						// MOTION BLUR ENABLED
						case NOX_REND_CHUNK_FILE_MB_ON:
							{
								int tint;
								res = iload->Read(&tint, sizeof(int), &rd);
								if (res!=IO_OK) 
									return res;	
								mb_on = (tint!=0);
							}
							break;

						// MOTION BLUR SHUTTER
						case NOX_REND_CHUNK_FILE_MB_SHUTTER:
							{
								float tfloat;
								res = iload->Read(&tfloat, sizeof(float), &rd);
								if (res!=IO_OK) 
									return res;	
								mb_duration = tfloat;
							}
							break;

						// ENGINE
						case NOX_REND_CHUNK_ENGINE:
							{
								int tint;
								res = iload->Read(&tint, sizeof(int), &rd);
								if (res!=IO_OK) 
									return res;	
								engine = tint;
							}
							break;

						// BLEND LAYER NAMES
						case NOX_REND_CHUNK_BLEND_NAME1:
						case NOX_REND_CHUNK_BLEND_NAME2:
						case NOX_REND_CHUNK_BLEND_NAME3:
						case NOX_REND_CHUNK_BLEND_NAME4:
						case NOX_REND_CHUNK_BLEND_NAME5:
						case NOX_REND_CHUNK_BLEND_NAME6:
						case NOX_REND_CHUNK_BLEND_NAME7:
						case NOX_REND_CHUNK_BLEND_NAME8:
						case NOX_REND_CHUNK_BLEND_NAME9:
						case NOX_REND_CHUNK_BLEND_NAME10:
						case NOX_REND_CHUNK_BLEND_NAME11:
						case NOX_REND_CHUNK_BLEND_NAME12:
						case NOX_REND_CHUNK_BLEND_NAME13:
						case NOX_REND_CHUNK_BLEND_NAME14:
						case NOX_REND_CHUNK_BLEND_NAME15:
						case NOX_REND_CHUNK_BLEND_NAME16:
						{
							blend = -1;
							switch (id)
							{
								case NOX_REND_CHUNK_BLEND_NAME1:	blend = 0; break;
								case NOX_REND_CHUNK_BLEND_NAME2:	blend = 1; break;
								case NOX_REND_CHUNK_BLEND_NAME3:	blend = 2; break;
								case NOX_REND_CHUNK_BLEND_NAME4:	blend = 3; break;
								case NOX_REND_CHUNK_BLEND_NAME5:	blend = 4; break;
								case NOX_REND_CHUNK_BLEND_NAME6:	blend = 5; break;
								case NOX_REND_CHUNK_BLEND_NAME7:	blend = 6; break;
								case NOX_REND_CHUNK_BLEND_NAME8:	blend = 7; break;
								case NOX_REND_CHUNK_BLEND_NAME9:	blend = 8; break;
								case NOX_REND_CHUNK_BLEND_NAME10:	blend = 9; break;
								case NOX_REND_CHUNK_BLEND_NAME11:	blend = 10; break;
								case NOX_REND_CHUNK_BLEND_NAME12:	blend = 11; break;
								case NOX_REND_CHUNK_BLEND_NAME13:	blend = 12; break;
								case NOX_REND_CHUNK_BLEND_NAME14:	blend = 13; break;
								case NOX_REND_CHUNK_BLEND_NAME15:	blend = 14; break;
								case NOX_REND_CHUNK_BLEND_NAME16:	blend = 15; break;
							}

							char * ctemp;
							res = iload->ReadCStringChunk(&ctemp);
							if (res!=IO_OK) 
								return res;	
							if (blend >= 0)
								if (ctemp  &&  strlen(ctemp)>0)
								{
									TrMaterial tmat;
									tmat.setBlendLayerName(blend, ctemp);
								}
						}
						break;


					}
					iload->CloseChunk();
				}	// end while in chunk renderer
			break;
		}
		iload->CloseChunk();
	}

	ADD_LOG_PARTS_MAIN("NOXRenderer::Load done");

	return IO_OK;
}

IOResult NOXRenderer::Save(ISave *isave) 
{
	ADD_LOG_PARTS_MAIN("NOXRenderer::Save");

	IOResult res;
	unsigned long wr;
	float tfloat;
	int tint;

	// RENDERER
	isave->BeginChunk(NOX_REND_CHUNK_RENDERER);

		// FILENAME
		if (filename)
		{
			isave->BeginChunk(NOX_REND_CHUNK_FILENAME);
			res = isave->WriteCString(filename);
			SCHECK(res);
			isave->EndChunk();
		}

		// SCENE NAME
		if (scene_name)
		{
			isave->BeginChunk(NOX_REND_CHUNK_SCENE_NAME);
			res = isave->WriteCString(scene_name);
			SCHECK(res);
			isave->EndChunk();
		}

		// AUTHOR
		if (scene_author_name)
		{
			isave->BeginChunk(NOX_REND_CHUNK_SCENE_AUTHOR_NAME);
			res = isave->WriteCString(scene_author_name);
			SCHECK(res);
			isave->EndChunk();
		}

		// EMAIL
		if (scene_author_email)
		{
			isave->BeginChunk(NOX_REND_CHUNK_SCENE_AUTHOR_EMAIL);
			res = isave->WriteCString(scene_author_email);
			SCHECK(res);
			isave->EndChunk();
		}

		// WWW
		if (scene_author_www)
		{
			isave->BeginChunk(NOX_REND_CHUNK_SCENE_AUTHOR_WWW);
			res = isave->WriteCString(scene_author_www);
			SCHECK(res);
			isave->EndChunk();
		}
		
		// OVERRIDE MATERIAL CHECKBOX
		isave->BeginChunk(NOX_REND_CHUNK_OVERRIDE_MAT_ON);
		tint = overrideMatEnabled ? 1 : 0; 
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// FILE POST
		if (filename_post)
		{
			isave->BeginChunk(NOX_REND_CHUNK_FILE_POST);
			res = isave->WriteCString(filename_post);
			SCHECK(res);
			isave->EndChunk();
		}

		// FILE POST
		if (filename_blend)
		{
			isave->BeginChunk(NOX_REND_CHUNK_FILE_BLEND);
			res = isave->WriteCString(filename_blend);
			SCHECK(res);
			isave->EndChunk();
		}

		// FILE POST
		if (filename_final)
		{
			isave->BeginChunk(NOX_REND_CHUNK_FILE_FINAL);
			res = isave->WriteCString(filename_final);
			SCHECK(res);
			isave->EndChunk();
		}

		// POST FILE ENABLED CHECKBOX
		isave->BeginChunk(NOX_REND_CHUNK_FILE_POST_ENABLED);
			tint = use_file_post ? 1 : 0; 
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// BLEND FILE ENABLED CHECKBOX
		isave->BeginChunk(NOX_REND_CHUNK_FILE_BLEND_ENABLED);
			tint = use_file_blend ? 1 : 0; 
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// FINAL FILE ENABLED CHECKBOX
		isave->BeginChunk(NOX_REND_CHUNK_FILE_FINAL_ENABLED);
			tint = use_file_final ? 1 : 0; 
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// MOTION BLUR ENABLED 
		isave->BeginChunk(NOX_REND_CHUNK_FILE_MB_ON);
			tint = mb_on ? 1 : 0; 
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		// MOTION BLUR SHUTTER 
		isave->BeginChunk(NOX_REND_CHUNK_FILE_MB_SHUTTER);
			tfloat = mb_duration; 
			res = isave->Write(&tfloat, sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// USER COLORS
		isave->BeginChunk(NOX_REND_CHUNK_USERCOLORS);
			float colors[54];
			for (int i=0; i<18; i++)
			{
				float r,g,b;
				TrRaytracer::getUserColors(i, r,g,b);
				colors[i*3+0] = r;
				colors[i*3+1] = g;
				colors[i*3+2] = b;
			}
			res = isave->Write(colors, 54*sizeof(float), &wr);
			SCHECK(res);
		isave->EndChunk();

		// MOTION BLUR ENABLED 
		isave->BeginChunk(NOX_REND_CHUNK_ENGINE);
			tint = engine;
			res = isave->Write(&tint, sizeof(int), &wr);
			SCHECK(res);
		isave->EndChunk();

		TrMaterial tmat;
		for (int i=0; i<16; i++)
		{
			int nnn = -1;
			switch (i)
			{
				case 0:  nnn = NOX_REND_CHUNK_BLEND_NAME1; break;
				case 1:  nnn = NOX_REND_CHUNK_BLEND_NAME2; break;
				case 2:  nnn = NOX_REND_CHUNK_BLEND_NAME3; break;
				case 3:  nnn = NOX_REND_CHUNK_BLEND_NAME4; break;
				case 4:  nnn = NOX_REND_CHUNK_BLEND_NAME5; break;
				case 5:  nnn = NOX_REND_CHUNK_BLEND_NAME6; break;
				case 6:  nnn = NOX_REND_CHUNK_BLEND_NAME7; break;
				case 7:  nnn = NOX_REND_CHUNK_BLEND_NAME8; break;
				case 8:  nnn = NOX_REND_CHUNK_BLEND_NAME9; break;
				case 9:  nnn = NOX_REND_CHUNK_BLEND_NAME10; break;
				case 10: nnn = NOX_REND_CHUNK_BLEND_NAME11; break;
				case 11: nnn = NOX_REND_CHUNK_BLEND_NAME12; break;
				case 12: nnn = NOX_REND_CHUNK_BLEND_NAME13; break;
				case 13: nnn = NOX_REND_CHUNK_BLEND_NAME14; break;
				case 14: nnn = NOX_REND_CHUNK_BLEND_NAME15; break;
				case 15: nnn = NOX_REND_CHUNK_BLEND_NAME16; break;
			}

			if (nnn>=0)
			{
				isave->BeginChunk(nnn);
				res = isave->WriteCString(tmat.getBlendLayerName(i));
				SCHECK(res);
				isave->EndChunk();
			}
		}

	isave->EndChunk();	// end renderer

	ADD_LOG_PARTS_MAIN("NOXRenderer::Save done");

	return IO_OK;
}
