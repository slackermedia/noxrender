#include <windows.h>
#include <stdio.h>
#include "EMControls.h"
#include <commctrl.h>

extern HMODULE hDllModule;

EMImgStateButton::EMImgStateButton(HWND hWnd)
{
	colBorder = RGB(128,128,128);
	colBorderSelected = RGB(255,255,255);
	stateOn = false;
	allowUnclick = false;
	hwnd = hWnd;
	hToolTip = 0;
	bmStateOn = 0;
	bmStateOff = 0;
	bmAll = 0;
	pNx = pNy = pMx = pMy = pCx = pCy = pDx = pDy = 0;

}

EMImgStateButton::~EMImgStateButton()
{
	if (hToolTip)
		DestroyWindow(hToolTip);
	hToolTip = 0;
}

void InitEMImgStateButton()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMImgStateButton";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMImgStateButtonProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMImgStateButton *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}


EMImgStateButton * GetEMImgStateButtonInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMImgStateButton * emisb = (EMImgStateButton *)GetWindowLongPtr(hwnd, 0);
	#else
		EMImgStateButton * emisb = (EMImgStateButton *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emisb;
}

void SetEMImgStateButtonInstance(HWND hwnd, EMImgStateButton *emisb)
{
	#ifdef _WIN64
	    SetWindowLongPtr(hwnd, 0, (LONG_PTR)emisb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emisb);
	#endif
}

LRESULT CALLBACK EMImgStateButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMImgStateButton * emisb;
	emisb = GetEMImgStateButtonInstance(hwnd);

    switch(msg)
    {
	case WM_CREATE:
		{
			EMImgStateButton * emisb1 = new EMImgStateButton(hwnd);
			SetEMImgStateButtonInstance(hwnd, (EMImgStateButton *)emisb1);			
		}
	break;
	case WM_NCDESTROY:
		{
			EMImgStateButton * emisb1;
			emisb1 = GetEMImgStateButtonInstance(hwnd);
			delete emisb1;
			SetEMImgStateButtonInstance(hwnd, 0);
		}
	break;
	case WM_PAINT:
	{
		if (!emisb) 
			break;

		RECT rect;
		HDC hdc;
		PAINTSTRUCT ps;
		HANDLE hOldPen;

		hdc = BeginPaint(hwnd, &ps);

		GetClientRect(hwnd, &rect);
		POINT border[] = {	{0, rect.bottom-1}, 
							{0,0}, 
							{rect.right-1,0}, 
							{rect.right-1,rect.bottom-1}, 
							{0, rect.bottom-1} };	// 5
		POINT border2[] = {	{1, rect.bottom-2}, 
							{1,1}, 
							{rect.right-2,1}, 
							{rect.right-2,rect.bottom-2}, 
							{1, rect.bottom-2} };	// 5
		
		int rx, ry;
		rx = rect.right;
		ry = rect.bottom;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);
		
		if (emisb->bmAll)
		{
			if (disabled)
			{
				HDC bmpdc = CreateCompatibleDC(hdc); 
				SelectObject(bmpdc, *(emisb->bmAll)); 
				BitBlt(hdc, 0, 0, rx, ry, bmpdc, emisb->pDx, emisb->pDy, SRCCOPY);
				DeleteDC(bmpdc); 
			}
			else
			{
				if (emisb->stateOn)
				{
					HDC bmpdc = CreateCompatibleDC(hdc); 
					SelectObject(bmpdc, *(emisb->bmAll)); 
					BitBlt(hdc, 0, 0, rx, ry, bmpdc, emisb->pCx, emisb->pCy, SRCCOPY);
					DeleteDC(bmpdc); 
				}
				else
				{
					HDC bmpdc = CreateCompatibleDC(hdc); 
					SelectObject(bmpdc, *(emisb->bmAll)); 
					BitBlt(hdc, 0, 0, rx, ry, bmpdc, emisb->pNx, emisb->pNy, SRCCOPY);
					DeleteDC(bmpdc); 
				}
			}
		}
		if (emisb->stateOn)
		{
			hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emisb->colBorderSelected));
			Polyline(hdc, border, 5);
			Polyline(hdc, border2, 5);
			DeleteObject(SelectObject(hdc, hOldPen));
		}
		else
		{
			hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emisb->colBorder));
			Polyline(hdc, border, 5);
			DeleteObject(SelectObject(hdc, hOldPen));
		}
		
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_LBUTTONDOWN:
	{
		if (!GetCapture())
		{
			if (!emisb->stateOn)
				emisb->stateOn = true;
			else
				if (emisb->allowUnclick)
					emisb->stateOn = false;
			RECT rect;
			GetClientRect(hwnd, &rect);
			InvalidateRect(hwnd, &rect, true);
			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), (LPARAM)hwnd);
		}
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMImgStateButton::setToolTip(char * text)
{
	if (!hToolTip)
	{
		hToolTip = CreateWindowEx (WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,     
				    CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwnd, NULL, hDllModule, NULL);
	}

	SetWindowPos (hToolTip, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	TOOLINFO ti;
	ti.cbSize = sizeof (TOOLINFO);
	ti.uFlags = TTF_SUBCLASS | TTF_IDISHWND;
	ti.hwnd = hToolTip;
	ti.hinst = NULL;
	ti.uId = (UINT_PTR) hwnd;
	ti.lpszText = (LPSTR) text;

	RECT rect;
	GetClientRect (hwnd, &rect);

	ti.rect.left = rect.left;    
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
	ti.rect.bottom = rect.bottom;

	SendMessage (hToolTip, TTM_ADDTOOL, 0, (LPARAM)&ti);
}

