#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "EM2Controls.h"
#include "noxfonts.h"

// new gui version

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
bool changeTrackbarsGradients(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in);
bool updateLeftGradients(HWND hWnd, int mode, bool ignore2d, Color4 * rgb_in, ColorHSV * hsv_in);
bool updateColorTrackbarValuesRGB(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in);
bool updateColorTrackbarValuesHSV(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in);
bool updateColorSpinValuesRGB(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in);
bool updateColorSpinValuesHSV(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in);
bool updateColorLeft2D(HWND hWnd, int mode, Color4 * rgb_in, ColorHSV * hsv_in);
bool initColor2ControlPositions(HWND hWnd);
bool updateColor2ControlBackgrounds(HWND hWnd);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
void Color2DlgInvalidArrows(HWND hWnd);
bool updateColorNew(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in);


#define ARROW_MARGIN 2

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK ColorDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;
	static bool changeOthers = true;
	static int mode_left = 0;
	static ColorHSV guiHSV;

	static Color4 origcolor = Color4(1,1,1);
	static Color4 currentcolor = Color4(1,1,1);

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = 634 + wrect.right-wrect.left-crect.right;
				tmprect.bottom = 389 + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);
	
				RECT rect;
				GetClientRect(hWnd, &rect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(rect.right, rect.bottom, 0,0, RGB(40,40,40), RGB(35,35,35));

				Color4 * col = (Color4*)lParam;
				if (col)
					origcolor = currentcolor = *col;

				NOXFontManager * fonts = NOXFontManager::getInstance();

				initColor2ControlPositions(hWnd);
				updateColor2ControlBackgrounds(hWnd);

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_COLOR2_OK));
				embOK->bgImage = hBgBMP;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_COLOR2_CANCEL));
				embCancel->bgImage = hBgBMP;
				embCancel->setFont(fonts->em2button, false);

				ColorHSV hsv = guiHSV = currentcolor.toColorHSV();
				unsigned char colr = (unsigned char)min(255, max( 0, currentcolor.r*255));
				unsigned char colg = (unsigned char)min(255, max( 0, currentcolor.g*255));
				unsigned char colb = (unsigned char)min(255, max( 0, currentcolor.b*255));
				unsigned char colh = (unsigned char)min(255, max( 0, hsv.h*255));
				unsigned char cols = (unsigned char)min(255, max( 0, hsv.s*255));
				unsigned char colv = (unsigned char)min(255, max( 0, hsv.v*255));

				EM2TrackBar * emtbRed = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_RED));
				emtbRed->createGradient(RGB(0,0,0), RGB(255,0,0));
				emtbRed->bgImage = hBgBMP;
				emtbRed->setValuesToInt(colr, 0, 255, colr);
				EM2TrackBar * emtbGreen = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_GREEN));
				emtbGreen->createGradient(RGB(0,0,0), RGB(0,255,0));
				emtbGreen->bgImage = hBgBMP;
				emtbGreen->setValuesToInt(colg, 0, 255, colg);
				EM2TrackBar * emtbBlue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_BLUE));
				emtbBlue->createGradient(RGB(0,0,0), RGB(0,0,255));
				emtbBlue->bgImage = hBgBMP;
				emtbBlue->setValuesToInt(colb, 0, 255, colb);

				EM2TrackBar * emtbHue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE));
				emtbHue->createHueRainbowGradient();
				emtbHue->bgImage = hBgBMP;
				emtbHue->setValuesToInt(colh, 0, 255, colh);
				EM2TrackBar * emtbSat = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_SATURATION));
				emtbSat->createGradient(RGB(128,128,128), RGB(255,0,0));
				emtbSat->bgImage = hBgBMP;
				emtbSat->setValuesToInt(cols, 0, 255, cols);
				EM2TrackBar * emtbValue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_VALUE));
				emtbValue->createGradient(RGB(0,0,0), RGB(255,255,255));
				emtbValue->bgImage = hBgBMP;
				emtbValue->setValuesToInt(colv, 0, 255, colv);

				EM2EditSpin * emesRed = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED));
				emesRed->bgImage = hBgBMP;
				emesRed->setFont(fonts->em2button, false);
				emesRed->setValuesToInt(colr, 0, 255, colr, 1, 0.1f);
				EM2EditSpin * emesGreen = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN));
				emesGreen->bgImage = hBgBMP;
				emesGreen->setFont(fonts->em2button, false);
				emesGreen->setValuesToInt(colg, 0, 255, colg, 1, 0.1f);
				EM2EditSpin * emesBlue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE));
				emesBlue->bgImage = hBgBMP;
				emesBlue->setFont(fonts->em2button, false);
				emesBlue->setValuesToInt(colb, 0, 255, colb, 1, 0.1f);

				EM2EditSpin * emesHue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE));
				emesHue->bgImage = hBgBMP;
				emesHue->setFont(fonts->em2button, false);
				emesHue->setValuesToInt(colh, 0, 255, colh, 1, 0.1f);
				EM2EditSpin * emesSat = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION));
				emesSat->bgImage = hBgBMP;
				emesSat->setFont(fonts->em2button, false);
				emesSat->setValuesToInt(cols, 0, 255, cols, 1, 0.1f);
				EM2EditSpin * emesValue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_VALUE));
				emesValue->bgImage = hBgBMP;
				emesValue->setFont(fonts->em2button, false);
				emesValue->setValuesToInt(colv, 0, 255, colv, 1, 0.1f);

				EM2Color2D * emc2 = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));
				emc2->bgImage = hBgBMP;
				emc2->generateRainbow(mode_left, hsv);
				switch (mode_left)
				{
					case EM2Color2D::MODE_HS_V:		emc2->setPosition(hsv.h, hsv.s);	emc2->setDefaultPos(hsv.h, hsv.s);	break;
					case EM2Color2D::MODE_SH_V:		emc2->setPosition(hsv.s, hsv.h);	emc2->setDefaultPos(hsv.s, hsv.h);	break;
					case EM2Color2D::MODE_HV_S:		emc2->setPosition(hsv.h, hsv.v);	emc2->setDefaultPos(hsv.h, hsv.v);	break;
					case EM2Color2D::MODE_VH_S:		emc2->setPosition(hsv.v, hsv.h);	emc2->setDefaultPos(hsv.v, hsv.h);	break;
					case EM2Color2D::MODE_VS_H:		emc2->setPosition(hsv.v, hsv.s);	emc2->setDefaultPos(hsv.v, hsv.s);	break;
					case EM2Color2D::MODE_SV_H:		emc2->setPosition(hsv.s, hsv.v);	emc2->setDefaultPos(hsv.s, hsv.v);	break;
				}

				EM2TrackBar * emtbThird = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_THIRD));
				emtbThird->bgImage = hBgBMP;
				emtbThird->invert = true;
				emtbThird->isHorizontal = false;
				switch (mode_left)
				{
					case EM2Color2D::MODE_HS_V:
					case EM2Color2D::MODE_SH_V:
						emtbThird->setValuesToInt(colv, 0, 255, colv);
						break;
					case EM2Color2D::MODE_HV_S:
					case EM2Color2D::MODE_VH_S:
						emtbThird->setValuesToInt(cols, 0, 255, cols);
						break;
					case EM2Color2D::MODE_VS_H:
					case EM2Color2D::MODE_SV_H:
						emtbThird->setValuesToInt(colh, 0, 255, colh);
						break;
				}

				changeTrackbarsGradients(hWnd, &currentcolor, NULL);
				updateLeftGradients(hWnd, mode_left, false, &currentcolor, NULL);

				EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_TITLE));
				emtTitle->align = EM2Text::ALIGN_CENTER;
				emtTitle->bgImage = hBgBMP;
				emtTitle->setFont(fonts->em2paneltitle, false);
				emtTitle->colText = NGCOL_LIGHT_GRAY;

				EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_RED));
				emtRed->bgImage = hBgBMP;
				emtRed->setFont(fonts->em2text, false);
				EM2Text * emtGreen = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_GREEN));
				emtGreen->bgImage = hBgBMP;
				emtGreen->setFont(fonts->em2text, false);
				EM2Text * emtBlue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_BLUE));
				emtBlue->bgImage = hBgBMP;
				emtBlue->setFont(fonts->em2text, false);
				EM2Text * emtHue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_HUE));
				emtHue->bgImage = hBgBMP;
				emtHue->setFont(fonts->em2text, false);
				EM2Text * emtSat = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_SATURATION));
				emtSat->bgImage = hBgBMP;
				emtSat->setFont(fonts->em2text, false);
				EM2Text * emtVal = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_VALUE));
				emtVal->bgImage = hBgBMP;
				emtVal->setFont(fonts->em2text, false);

				EM2ColorShow * emcsOld = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_COLOR_OLD));
				emcsOld->bgImage = hBgBMP;
				emcsOld->color = currentcolor;
				emcsOld->allowDragSrc = true;
				EM2ColorShow * emcsNew = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_COLOR_NEW));
				emcsNew->bgImage = hBgBMP;
				emcsNew->color = currentcolor;
				emcsNew->allowDragSrc = true;
				emcsNew->allowDragDst = true;

				EM2ColorShow * emcsLib[12];
				emcsLib[0 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB1));
				emcsLib[1 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB2));
				emcsLib[2 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB3));
				emcsLib[3 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB4));
				emcsLib[4 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB5));
				emcsLib[5 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB6));
				emcsLib[6 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB7));
				emcsLib[7 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB8));
				emcsLib[8 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB9));
				emcsLib[9 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB10));
				emcsLib[10] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB11));
				emcsLib[11] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB12));
				for (int i=0; i<12; i++)
				{
					emcsLib[i]->bgImage = hBgBMP;
					emcsLib[i]->color = UserColors::colors[i];
					emcsLib[i]->allowDragSrc = true;
					emcsLib[i]->allowDragDst = true;
				}


				EM2Text * emtNew = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_NEW));
				emtNew->bgImage = hBgBMP;
				emtNew->setFont(fonts->em2text, false);
				EM2Text * emtPrev = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_PREV));
				emtPrev->bgImage = hBgBMP;
				emtPrev->setFont(fonts->em2text, false);
				EM2Text * emtLib = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_LIB));
				emtLib->bgImage = hBgBMP;
				emtLib->setFont(fonts->em2text, false);
				EM2Text * emtDnD = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_DND));
				emtDnD->bgImage = hBgBMP;
				emtDnD->align = EM2Text::ALIGN_CENTER;
				emtDnD->setFont(fonts->em2text, false);

				EM2ComboBox * emcbHSV = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE_SAT_VAL));
				emcbHSV->bgImage = hBgBMP;
				emcbHSV->setFontMain(fonts->em2combomain, false);
				emcbHSV->setFontUnwrapped(fonts->em2combounwrap, false);
				emcbHSV->selected = mode_left;
				emcbHSV->maxShownRows = 6;
				emcbHSV->setNumItems(6);
				emcbHSV->setItem(0, "HUE / SATURATION", true);
				emcbHSV->setItem(1, "HUE / VALUE", true);
				emcbHSV->setItem(2, "SATURATION / HUE", true);
				emcbHSV->setItem(3, "SATURATION / VALUE", true);
				emcbHSV->setItem(4, "VALUE / SATURATION", true);
				emcbHSV->setItem(5, "VALUE / HUE", true);

			}	// end WM_INITDIALOG
			break;
		case WM_CLOSE:
			{
				SendMessage(hWnd, WM_COMMAND, MAKELONG(IDC_COLOR2_CANCEL, BN_CLICKED), (LPARAM)GetDlgItem(hWnd, IDC_COLOR2_CANCEL));
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;

			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}


				EM2Color2D * em2d = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));
				EM2TrackBar * em2tb = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_THIRD));
				if (em2d && em2tb)
				{
					int bw1 = 1, bw2 = 1;
					RECT rect2, rect3;
					GetClientRect(em2d->hwnd, &rect2);
					GetClientRect(em2tb->hwnd, &rect3);
					POINT p2={0,0}, p3={0,0};
					MapWindowPoints(em2d->hwnd, hWnd, &p2, 1);
					MapWindowPoints(em2tb->hwnd, hWnd, &p3, 1);


					float v1,v2,v3;
					switch (mode_left)
					{
						case EM2Color2D::MODE_HS_V:	v1=guiHSV.h; v2=guiHSV.s; v3=guiHSV.v; break;
						case EM2Color2D::MODE_SH_V:	v1=guiHSV.s; v2=guiHSV.h; v3=guiHSV.v; break;
						case EM2Color2D::MODE_HV_S:	v1=guiHSV.h; v2=guiHSV.v; v3=guiHSV.s; break;
						case EM2Color2D::MODE_VH_S:	v1=guiHSV.v; v2=guiHSV.h; v3=guiHSV.s; break;
						case EM2Color2D::MODE_VS_H:	v1=guiHSV.v; v2=guiHSV.s; v3=guiHSV.h; break;
						case EM2Color2D::MODE_SV_H:	v1=guiHSV.s; v2=guiHSV.v; v3=guiHSV.h; break;
						default:					v1=guiHSV.h; v2=guiHSV.s; v3=guiHSV.v; break;
					}
					v2 = 1.0f - v2;
					v3 = 1.0f - v3;

					int a1p = (int)((rect2.right-1-bw1*2)*v1) + bw1 + p2.x - xx;
					int a2p = (int)((rect2.bottom-1-bw1*2)*v2) + bw1 + p2.y - yy;
					int a3p = (int)((rect3.bottom-1-bw2*2-2)*v3) + bw2+1 + p3.y - yy;

					HPEN hOldPen, hPenLight, hPenDark;
					hPenLight = CreatePen(PS_SOLID, 1, RGB(125,125,125));
					hPenDark = CreatePen(PS_SOLID, 1, RGB(66,66,66));
					hOldPen = (HPEN)SelectObject(hdc, hPenLight);


					SelectObject(hdc, hPenLight);
					MoveToEx(hdc,	a1p-4, p2.y-ARROW_MARGIN-2-4-yy, NULL);
					LineTo(hdc,		a1p+0, p2.y-ARROW_MARGIN-2-0-yy);
					LineTo(hdc,		a1p+5, p2.y-ARROW_MARGIN-2-5-yy);
					SelectObject(hdc, hPenDark);
					MoveToEx(hdc,	a1p-5, p2.y-ARROW_MARGIN-1-5-yy, NULL);
					LineTo(hdc,		a1p+0, p2.y-ARROW_MARGIN-1-0-yy);
					LineTo(hdc,		a1p+6, p2.y-ARROW_MARGIN-1-6-yy);

					SelectObject(hdc, hPenLight);
					MoveToEx(hdc,	p2.x-ARROW_MARGIN-2-4-xx, a2p-4, NULL);
					LineTo(hdc,		p2.x-ARROW_MARGIN-2-0-xx, a2p);
					LineTo(hdc,		p2.x-ARROW_MARGIN-2-5-xx, a2p+5);
					SelectObject(hdc, hPenDark);
					MoveToEx(hdc,	p2.x-ARROW_MARGIN-1-5-xx, a2p-5, NULL);
					LineTo(hdc,		p2.x-ARROW_MARGIN-1-0-xx, a2p);
					LineTo(hdc,		p2.x-ARROW_MARGIN-1-6-xx, a2p+6);

					SelectObject(hdc, hPenLight);
					MoveToEx(hdc,	p3.x+ARROW_MARGIN+2+4+rect3.right-1-xx, a3p-4, NULL);
					LineTo(hdc,		p3.x+ARROW_MARGIN+2+0+rect3.right-1-xx, a3p);
					LineTo(hdc,		p3.x+ARROW_MARGIN+2+5+rect3.right-1-xx, a3p+5);
					SelectObject(hdc, hPenDark);
					MoveToEx(hdc,	p3.x+ARROW_MARGIN+1+5+rect3.right-1-xx, a3p-5, NULL);
					LineTo(hdc,		p3.x+ARROW_MARGIN+1+0+rect3.right-1-xx, a3p);
					LineTo(hdc,		p3.x+ARROW_MARGIN+1+6+rect3.right-1-xx, a3p+6);


					SelectObject(hdc, hOldPen);
					DeleteObject(hPenLight);
					DeleteObject(hPenDark);
				}


				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_COLOR2_OK:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)(&currentcolor)); // chce (Color4 *)
							}
						}
						break;
					case IDC_COLOR2_CANCEL:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)NULL);
							}
						}
						break;
					case IDC_COLOR2_RED:
					case IDC_COLOR2_GREEN:
					case IDC_COLOR2_BLUE:
						{
							CHECK(wmEvent == WM_HSCROLL);
							Color4 rgb;
							rgb.r = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_RED))->intValue/255.0f;
							rgb.g = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_GREEN))->intValue/255.0f;
							rgb.b = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_BLUE))->intValue/255.0f;
							if (changeOthers)
								changeTrackbarsGradients(hWnd, &rgb, NULL);
							updateLeftGradients(hWnd, mode_left, false, &rgb, NULL);

							//updateColorTrackbarValuesRGB(hWnd, &rgb, NULL);
							updateColorTrackbarValuesHSV(hWnd, &rgb, NULL);
							updateColorSpinValuesRGB(hWnd, &rgb, NULL);
							updateColorSpinValuesHSV(hWnd, &rgb, NULL);
							updateColorLeft2D(hWnd, mode_left, &rgb, NULL);
							updateColorNew(hWnd, &rgb, NULL);

							currentcolor = rgb;
							guiHSV = rgb.toColorHSV();
							Color2DlgInvalidArrows(hWnd);
						}
						break;
					case IDC_COLOR2_SPIN_RED:
					case IDC_COLOR2_SPIN_GREEN:
					case IDC_COLOR2_SPIN_BLUE:
						{
							CHECK(wmEvent == WM_VSCROLL);
							Color4 rgb;
							rgb.r = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED))->intValue/255.0f;
							rgb.g = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN))->intValue/255.0f;
							rgb.b = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE))->intValue/255.0f;
							if (changeOthers)
								changeTrackbarsGradients(hWnd, &rgb, NULL);
							updateLeftGradients(hWnd, mode_left, false, &rgb, NULL);

							updateColorTrackbarValuesRGB(hWnd, &rgb, NULL);
							updateColorTrackbarValuesHSV(hWnd, &rgb, NULL);
							//updateColorSpinValuesRGB(hWnd, &rgb, NULL);
							updateColorSpinValuesHSV(hWnd, &rgb, NULL);
							updateColorLeft2D(hWnd, mode_left, &rgb, NULL);
							updateColorNew(hWnd, &rgb, NULL);

							currentcolor = rgb;
							guiHSV = rgb.toColorHSV();
							Color2DlgInvalidArrows(hWnd);
						}
						break;
					case IDC_COLOR2_HUE:
					case IDC_COLOR2_SATURATION:
					case IDC_COLOR2_VALUE:
						{
							CHECK(wmEvent == WM_HSCROLL);
							ColorHSV hsv;
							hsv.h = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE))->intValue/255.0f;
							hsv.s = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_SATURATION))->intValue/255.0f;
							hsv.v = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_VALUE))->intValue/255.0f;
							if (changeOthers)
								changeTrackbarsGradients(hWnd, NULL, &hsv);
							updateLeftGradients(hWnd, mode_left, false, NULL, &hsv);

							updateColorTrackbarValuesRGB(hWnd, NULL, &hsv);
							//updateColorTrackbarValuesHSV(hWnd, NULL, &hsv);
							updateColorSpinValuesRGB(hWnd, NULL, &hsv);
							updateColorSpinValuesHSV(hWnd, NULL, &hsv);
							updateColorLeft2D(hWnd, mode_left, NULL, &hsv);
							updateColorNew(hWnd, NULL, &hsv);

							currentcolor = hsv.toColor4();
							guiHSV = hsv;
							Color2DlgInvalidArrows(hWnd);
						}
						break;
					case IDC_COLOR2_SPIN_HUE:
					case IDC_COLOR2_SPIN_SATURATION:
					case IDC_COLOR2_SPIN_VALUE:
						{
							CHECK(wmEvent == WM_VSCROLL);
							ColorHSV hsv;
							hsv.h = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE))->intValue/255.0f;
							hsv.s = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION))->intValue/255.0f;
							hsv.v = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_VALUE))->intValue/255.0f;
							if (changeOthers)
								changeTrackbarsGradients(hWnd, NULL, &hsv);
							updateLeftGradients(hWnd, mode_left, false, NULL, &hsv);

							updateColorTrackbarValuesRGB(hWnd, NULL, &hsv);
							updateColorTrackbarValuesHSV(hWnd, NULL, &hsv);
							updateColorSpinValuesRGB(hWnd, NULL, &hsv);
							updateColorLeft2D(hWnd, mode_left, NULL, &hsv);
							updateColorNew(hWnd, NULL, &hsv);

							currentcolor = hsv.toColor4();
							guiHSV = hsv;
							Color2DlgInvalidArrows(hWnd);
						}
						break;
					case IDC_COLOR2_HUE_SAT_VAL:
						{
							CHECK(CBN_SELCHANGE==wmEvent);
							EM2ComboBox * emcHSV = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE_SAT_VAL));
							mode_left = emcHSV->selected;
							EM2Color2D * em2d = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));

							ColorHSV hsv = currentcolor.toColorHSV();
							ColorHSV ohsv = origcolor.toColorHSV();

							updateLeftGradients(hWnd, mode_left, false, NULL, &hsv);
							updateColorLeft2D(hWnd, mode_left, &currentcolor, NULL);
							Color2DlgInvalidArrows(hWnd);
						}
						break;
					case IDC_COLOR2_2D:
					case IDC_COLOR2_THIRD:
						{
							CHECK(wmEvent == WM_VSCROLL  ||   wmEvent == WM_HSCROLL);
							EM2TrackBar * eThird = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_THIRD));
							EM2Color2D * e2D = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));
							float x,y,z;
							e2D->getPosition(x,y);
							z = eThird->intValue/255.0f;

							ColorHSV hsv;
							switch (mode_left)
							{
								case EM2Color2D::MODE_HS_V:		hsv.h=x; hsv.s=y; hsv.v=z; break;
								case EM2Color2D::MODE_SH_V:		hsv.s=x; hsv.h=y; hsv.v=z; break;
								case EM2Color2D::MODE_HV_S:		hsv.h=x; hsv.v=y; hsv.s=z; break;
								case EM2Color2D::MODE_VH_S:		hsv.v=x; hsv.h=y; hsv.s=z; break;
								case EM2Color2D::MODE_VS_H:		hsv.v=x; hsv.s=y; hsv.h=z; break;
								case EM2Color2D::MODE_SV_H:		hsv.s=x; hsv.v=y; hsv.h=z; break;
							}

							if (changeOthers)
								changeTrackbarsGradients(hWnd, NULL, &hsv);
							updateLeftGradients(hWnd, mode_left, (wmId==IDC_COLOR2_2D), NULL, &hsv);

							updateColorTrackbarValuesRGB(hWnd, NULL, &hsv);
							updateColorTrackbarValuesHSV(hWnd, NULL, &hsv);
							updateColorSpinValuesRGB(hWnd, NULL, &hsv);
							updateColorSpinValuesHSV(hWnd, NULL, &hsv);
							updateColorNew(hWnd, NULL, &hsv);

							currentcolor = hsv.toColor4();
							guiHSV = hsv;
							Color2DlgInvalidArrows(hWnd);
						}
						break;
					case IDC_COLOR2_COLOR_NEW:
						{
							EM2ColorShow * ecs = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_COLOR_NEW));
							Color4 rgb = ecs->color;

							if (changeOthers)
								changeTrackbarsGradients(hWnd, &rgb, NULL);
							updateLeftGradients(hWnd, mode_left, false, &rgb, NULL);

							updateColorTrackbarValuesRGB(hWnd, &rgb, NULL);
							updateColorTrackbarValuesHSV(hWnd, &rgb, NULL);
							updateColorSpinValuesRGB(hWnd, &rgb, NULL);
							updateColorSpinValuesHSV(hWnd, &rgb, NULL);
							updateColorLeft2D(hWnd, mode_left, &rgb, NULL);

							currentcolor = rgb;
							guiHSV = rgb.toColorHSV();
							Color2DlgInvalidArrows(hWnd);
						}
						break;
					case IDC_COLOR2_LIB1:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[0 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB1 ))->color;	break;
					case IDC_COLOR2_LIB2:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[1 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB2 ))->color;	break;
					case IDC_COLOR2_LIB3:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[2 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB3 ))->color;	break;
					case IDC_COLOR2_LIB4:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[3 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB4 ))->color;	break;
					case IDC_COLOR2_LIB5:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[4 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB5 ))->color;	break;
					case IDC_COLOR2_LIB6:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[5 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB6 ))->color;	break;
					case IDC_COLOR2_LIB7:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[6 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB7 ))->color;	break;
					case IDC_COLOR2_LIB8:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[7 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB8 ))->color;	break;
					case IDC_COLOR2_LIB9:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[8 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB9 ))->color;	break;
					case IDC_COLOR2_LIB10:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[9 ] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB10))->color;	break;
					case IDC_COLOR2_LIB11:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[10] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB11))->color;	break;
					case IDC_COLOR2_LIB12:	CHECK(wmEvent==CS_IGOT_DRAG);	UserColors::colors[11] = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB12))->color;	break;

				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool updateLeftGradients(HWND hWnd, int mode, bool ignore2d, Color4 * rgb_in, ColorHSV * hsv_in)
{
	if (!rgb_in  &&  !hsv_in)
		return false;

	EM2Color2D * em2d = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));
	EM2TrackBar * eOther = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_THIRD));

	ColorHSV hsv;
	if (rgb_in)
	{
		hsv = rgb_in->toColorHSV();
	}
	else
	{
		hsv = *hsv_in;
	}

	ColorHSV hsvtemp = hsv;
	switch (mode)
	{
		case EM2Color2D::MODE_HS_V:
		case EM2Color2D::MODE_SH_V:
			{
				hsvtemp.v = 0;
				Color4 rgbvalmin = hsvtemp.toColor4();
				hsvtemp.v = 1;
				Color4 rgbvalmax = hsvtemp.toColor4();
				eOther->createGradient(rgbvalmin.toColorRef(), rgbvalmax.toColorRef());
				InvalidateRect(eOther->hwnd, NULL, false);
			}
			break;
		case EM2Color2D::MODE_HV_S:
		case EM2Color2D::MODE_VH_S:
			{
				hsvtemp.s = 0;
				Color4 rgbsatmin = hsvtemp.toColor4();
				hsvtemp.s = 1;
				Color4 rgbsatmax = hsvtemp.toColor4();
				eOther->createGradient(rgbsatmin.toColorRef(), rgbsatmax.toColorRef());
				InvalidateRect(eOther->hwnd, NULL, false);
			}
			break;
		case EM2Color2D::MODE_VS_H:
		case EM2Color2D::MODE_SV_H:
			eOther->createHueRainbowGradient();
			InvalidateRect(eOther->hwnd, NULL, false);
			break;
	}

	if (!ignore2d)
	{
		em2d->generateRainbow(mode, hsv);
		InvalidateRect(em2d->hwnd, NULL, false);
	}



	return true;
}

//------------------------------------------------------------------------------------------------

bool changeTrackbarsGradients(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in)
{
	if (!rgb_in  &&  !hsv_in)
		return false;

	EM2TrackBar * eRed = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_RED));
	EM2TrackBar * eGreen = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_GREEN));
	EM2TrackBar * eBlue= GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_BLUE));
	EM2TrackBar * eHue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE));
	EM2TrackBar * eSat = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_SATURATION));
	EM2TrackBar * eVal = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_VALUE));

	Color4 rgb;
	ColorHSV hsv;
	if (rgb_in)
	{
		rgb = *rgb_in;
		hsv = rgb_in->toColorHSV();
	}
	else
	{
		rgb = hsv_in->toColor4();
		hsv = *hsv_in;
	}

	unsigned char cr, cg, cb;
	cr = (unsigned char)(int)min(255.0f, max(0.0f, rgb.r*255));
	cg = (unsigned char)(int)min(255.0f, max(0.0f, rgb.g*255));
	cb = (unsigned char)(int)min(255.0f, max(0.0f, rgb.b*255));
	eRed->createGradient(	RGB(0,cg,cb), RGB(255,cg,cb));
	eGreen->createGradient(	RGB(cr,0,cb), RGB(cr,255,cb));
	eBlue->createGradient(	RGB(cr,cg,0), RGB(cr,cg,255));
	InvalidateRect(eRed->hwnd, NULL, false);
	InvalidateRect(eGreen->hwnd, NULL, false);
	InvalidateRect(eBlue->hwnd, NULL, false);

	ColorHSV hsvtemp = hsv;
	hsvtemp.s = 0;
	Color4 rgbsatmin = hsvtemp.toColor4();
	hsvtemp.s = 1;
	Color4 rgbsatmax = hsvtemp.toColor4();
	eSat->createGradient(rgbsatmin.toColorRef(), rgbsatmax.toColorRef());
	InvalidateRect(eSat->hwnd, NULL, false);

	hsvtemp = hsv;
	hsvtemp.v = 0;
	Color4 rgbvalmin = hsvtemp.toColor4();
	hsvtemp.v = 1;
	Color4 rgbvalmax = hsvtemp.toColor4();
	eVal->createGradient(rgbvalmin.toColorRef(), rgbvalmax.toColorRef());
	InvalidateRect(eVal->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateColorTrackbarValuesRGB(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in)
{
	if (!rgb_in  &&  !hsv_in)
		return false;

	EM2TrackBar * eRed = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_RED));
	EM2TrackBar * eGreen = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_GREEN));
	EM2TrackBar * eBlue= GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_BLUE));

	Color4 rgb;
	if (rgb_in)
		rgb = *rgb_in;
	else
		rgb = hsv_in->toColor4();

	unsigned char cr, cg, cb;
	cr = (unsigned char)(int)min(255.0f, max(0.0f, rgb.r*255));
	cg = (unsigned char)(int)min(255.0f, max(0.0f, rgb.g*255));
	cb = (unsigned char)(int)min(255.0f, max(0.0f, rgb.b*255));

	eRed->setIntValue(cr);
	eGreen->setIntValue(cg);
	eBlue->setIntValue(cb);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateColorTrackbarValuesHSV(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in)
{
	if (!rgb_in  &&  !hsv_in)
		return false;

	EM2TrackBar * eHue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE));
	EM2TrackBar * eSat = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_SATURATION));
	EM2TrackBar * eVal = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_VALUE));

	ColorHSV hsv;
	if (rgb_in)
		hsv = rgb_in->toColorHSV();
	else
		hsv = *hsv_in;

	unsigned char ch, cs, cv;
	ch = (unsigned char)(int)min(255.0f, max(0.0f, hsv.h*255));
	cs = (unsigned char)(int)min(255.0f, max(0.0f, hsv.s*255));
	cv = (unsigned char)(int)min(255.0f, max(0.0f, hsv.v*255));

	eHue->setIntValue(ch);
	eSat->setIntValue(cs);
	eVal->setIntValue(cv);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateColorSpinValuesRGB(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in)
{
	if (!rgb_in  &&  !hsv_in)
		return false;

	EM2EditSpin * eRed = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED));
	EM2EditSpin * eGreen = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN));
	EM2EditSpin * eBlue= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE));

	Color4 rgb;
	if (rgb_in)
		rgb = *rgb_in;
	else
		rgb = hsv_in->toColor4();

	unsigned char cr, cg, cb;
	cr = (unsigned char)(int)min(255.0f, max(0.0f, rgb.r*255));
	cg = (unsigned char)(int)min(255.0f, max(0.0f, rgb.g*255));
	cb = (unsigned char)(int)min(255.0f, max(0.0f, rgb.b*255));

	eRed->setIntValue(cr);
	eGreen->setIntValue(cg);
	eBlue->setIntValue(cb);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateColorSpinValuesHSV(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in)
{
	if (!rgb_in  &&  !hsv_in)
		return false;

	EM2EditSpin * eHue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE));
	EM2EditSpin * eSat = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION));
	EM2EditSpin * eVal = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_VALUE));

	ColorHSV hsv;
	if (rgb_in)
		hsv = rgb_in->toColorHSV();
	else
		hsv = *hsv_in;

	unsigned char ch, cs, cv;
	ch = (unsigned char)(int)min(255.0f, max(0.0f, hsv.h*255));
	cs = (unsigned char)(int)min(255.0f, max(0.0f, hsv.s*255));
	cv = (unsigned char)(int)min(255.0f, max(0.0f, hsv.v*255));

	eHue->setIntValue(ch);
	eSat->setIntValue(cs);
	eVal->setIntValue(cv);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateColorNew(HWND hWnd, Color4 * rgb_in, ColorHSV * hsv_in)
{
	if (!rgb_in  &&  !hsv_in)
		return false;

	Color4 rgb;
	if (rgb_in)
		rgb = *rgb_in;
	else
		rgb = hsv_in->toColor4();

	EM2ColorShow * emcs = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_COLOR_NEW));
	emcs->color = rgb;
	InvalidateRect(emcs->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateColorLeft2D(HWND hWnd, int mode, Color4 * rgb_in, ColorHSV * hsv_in)
{
	EM2Color2D * emc2 = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));
	EM2TrackBar * eThird = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_THIRD));

	ColorHSV hsv;
	if (rgb_in)
		hsv = rgb_in->toColorHSV();
	else
		hsv = *hsv_in;

	unsigned char ch, cs, cv;
	ch = (unsigned char)(int)min(255.0f, max(0.0f, hsv.h*255));
	cs = (unsigned char)(int)min(255.0f, max(0.0f, hsv.s*255));
	cv = (unsigned char)(int)min(255.0f, max(0.0f, hsv.v*255));

	switch (mode)
	{
		case EM2Color2D::MODE_HS_V:
			emc2->setPosition(hsv.h, hsv.s);
			eThird->setIntValue(cv);
			break;
		case EM2Color2D::MODE_SH_V:
			emc2->setPosition(hsv.s, hsv.h);
			eThird->setIntValue(cv);
			break;
		case EM2Color2D::MODE_HV_S:
			emc2->setPosition(hsv.h, hsv.v);
			eThird->setIntValue(cs);
			break;
		case EM2Color2D::MODE_VH_S:
			emc2->setPosition(hsv.v, hsv.h);
			eThird->setIntValue(cs);
			break;
		case EM2Color2D::MODE_VS_H:
			emc2->setPosition(hsv.v, hsv.s);
			eThird->setIntValue(ch);
			break;
		case EM2Color2D::MODE_SV_H:
			emc2->setPosition(hsv.s, hsv.v);
			eThird->setIntValue(ch);
			break;
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool initColor2ControlPositions(HWND hWnd)
{
	int x, y;
	x = 0;	y = 0;

	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_TITLE), HWND_TOP, x+214,  y+12, 200, 20, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_2D), HWND_TOP, x+14,  y+41, 294, 311, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_HUE_SAT_VAL), HWND_TOP, x+157,  y+356, 150, 20, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_THIRD), HWND_TOP, x+325,  y+41, 23, 311, 0);


	x = 370;	y = 40;

	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_RED),			HWND_TOP, x+5,		y+10,	40, 16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_GREEN),		HWND_TOP, x+5,		y+40,	40, 16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_BLUE),		HWND_TOP, x+5,		y+70,	40, 16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_HUE),			HWND_TOP, x+5,		y+110,	40, 16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_SATURATION),	HWND_TOP, x+5,		y+140,	40, 16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_VALUE),		HWND_TOP, x+5,		y+170,	40, 16,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_RED),				HWND_TOP, x+47,		y+8,	154, 20,	0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_GREEN),			HWND_TOP, x+47,		y+38,	154, 20,	0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_BLUE),				HWND_TOP, x+47,		y+68,	154, 20,	0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_HUE),				HWND_TOP, x+47,		y+108,	154, 20,	0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SATURATION),		HWND_TOP, x+47,		y+138,	154, 20,	0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_VALUE),			HWND_TOP, x+47,		y+168,	154, 20,	0);

	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED),			HWND_TOP, x+210,	y+8,	48, 20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN),		HWND_TOP, x+210,	y+38,	48, 20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE),		HWND_TOP, x+210,	y+68,	48, 20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE),			HWND_TOP, x+210,	y+108,	48, 20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION),	HWND_TOP, x+210,	y+138,	48, 20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_VALUE),		HWND_TOP, x+210,	y+168,	48, 20,		0);

	x = 370;	y = 240;
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_COLOR_NEW),		HWND_TOP, x+48,		y+8,	74, 21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_COLOR_OLD),		HWND_TOP, x+127,	y+8,	74, 21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_DND),			HWND_TOP, x+48,		y+29,	153,16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_NEW),			HWND_TOP, x+5,		y+9,	30,	16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_PREV),		HWND_TOP, x+213,	y+9,	30,	16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_TEXT_LIB),			HWND_TOP, x+5,		y+63,	30,	16,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB1),				HWND_TOP, x+48,		y+48,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB2),				HWND_TOP, x+74,		y+48,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB3),				HWND_TOP, x+101,	y+48,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB4),				HWND_TOP, x+127,	y+48,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB5),				HWND_TOP, x+154,	y+48,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB6),				HWND_TOP, x+180,	y+48,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB7),				HWND_TOP, x+48,		y+75,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB8),				HWND_TOP, x+74,		y+75,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB9),				HWND_TOP, x+101,	y+75,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB10),			HWND_TOP, x+127,	y+75,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB11),			HWND_TOP, x+154,	y+75,	21,	21,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_LIB12),			HWND_TOP, x+180,	y+75,	21,	21,		0);

	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_OK),				HWND_TOP, 418,		353,	72,	22,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_CANCEL),			HWND_TOP, 499,		353,	71,	22,		0);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED),			HWND_TOP,										0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN),		GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE),		GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE),			GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION),	GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_COLOR2_SPIN_VALUE),		GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateColor2ControlBackgrounds(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	// texts
	EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_RED));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_RED), 0, 0, emtRed->bgShiftX, emtRed->bgShiftY);
	EM2Text * emtGreen = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_GREEN));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_GREEN), 0, 0, emtGreen->bgShiftX, emtGreen->bgShiftY);
	EM2Text * emtBlue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_BLUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_BLUE), 0, 0, emtBlue->bgShiftX, emtBlue->bgShiftY);
	EM2Text * emtHue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_HUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_HUE), 0, 0, emtHue->bgShiftX, emtHue->bgShiftY);
	EM2Text * emtSat = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_SATURATION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_SATURATION), 0, 0, emtSat->bgShiftX, emtSat->bgShiftY);
	EM2Text * emtValue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_VALUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_VALUE), 0, 0, emtValue->bgShiftX, emtValue->bgShiftY);
	EM2Text * emtNew = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_NEW));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_NEW), 0, 0, emtNew->bgShiftX, emtNew->bgShiftY);
	EM2Text * emtPrev = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_PREV));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_PREV), 0, 0, emtPrev->bgShiftX, emtPrev->bgShiftY);
	EM2Text * emtLib = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_LIB));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_LIB), 0, 0, emtLib->bgShiftX, emtLib->bgShiftY);
	EM2Text * emtDnD = GetEM2TextInstance(GetDlgItem(hWnd, IDC_COLOR2_TEXT_DND));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_TEXT_DND), 0, 0, emtDnD->bgShiftX, emtDnD->bgShiftY);

	// spins
	EM2EditSpin * emsRed = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_SPIN_RED), 0, 0, emsRed->bgShiftX, emsRed->bgShiftY);
	EM2EditSpin * emsGreen = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_SPIN_GREEN), 0, 0, emsGreen->bgShiftX, emsGreen->bgShiftY);
	EM2EditSpin * emsBlue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_SPIN_BLUE), 0, 0, emsBlue->bgShiftX, emsBlue->bgShiftY);
	EM2EditSpin * emsHue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_SPIN_HUE), 0, 0, emsHue->bgShiftX, emsHue->bgShiftY);
	EM2EditSpin * emsSat = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_SPIN_SATURATION), 0, 0, emsSat->bgShiftX, emsSat->bgShiftY);
	EM2EditSpin * emsValue = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_COLOR2_SPIN_VALUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_SPIN_VALUE), 0, 0, emsValue->bgShiftX, emsValue->bgShiftY);

	// trackbars
	EM2TrackBar * emtrRed = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_RED));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_RED), 0, 0, emtrRed->bgShiftX, emtrRed->bgShiftY);
	EM2TrackBar * emtrGreen = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_GREEN));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_GREEN), 0, 0, emtrGreen->bgShiftX, emtrGreen->bgShiftY);
	EM2TrackBar * emtrBlue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_BLUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_BLUE), 0, 0, emtrBlue->bgShiftX, emtrBlue->bgShiftY);
	EM2TrackBar * emtrHue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_HUE), 0, 0, emtrHue->bgShiftX, emtrHue->bgShiftY);
	EM2TrackBar * emtrSat = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_SATURATION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_SATURATION), 0, 0, emtrSat->bgShiftX, emtrSat->bgShiftY);
	EM2TrackBar * emtrValue = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_VALUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_VALUE), 0, 0, emtrValue->bgShiftX, emtrValue->bgShiftY);

	// left
	EM2TrackBar * emtrThird = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_THIRD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_THIRD), 0, 0, emtrThird->bgShiftX, emtrThird->bgShiftY);
	EM2Color2D * emC2D = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_2D), 0, 0, emC2D->bgShiftX, emC2D->bgShiftY);

	// color
	EM2ColorShow * emcNew = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_COLOR_NEW));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_COLOR_NEW), 0, 0, emcNew->bgShiftX, emcNew->bgShiftY);
	EM2ColorShow * emcOld = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_COLOR_OLD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_COLOR_OLD), 0, 0, emcOld->bgShiftX, emcOld->bgShiftY);

	EM2ColorShow * emcLib1  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB1));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB1), 0, 0, emcLib1->bgShiftX, emcLib1->bgShiftY);
	EM2ColorShow * emcLib2  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB2));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB2), 0, 0, emcLib2->bgShiftX, emcLib2->bgShiftY);
	EM2ColorShow * emcLib3  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB3));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB3), 0, 0, emcLib3->bgShiftX, emcLib3->bgShiftY);
	EM2ColorShow * emcLib4  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB4));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB4), 0, 0, emcLib4->bgShiftX, emcLib4->bgShiftY);
	EM2ColorShow * emcLib5  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB5));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB5), 0, 0, emcLib5->bgShiftX, emcLib5->bgShiftY);
	EM2ColorShow * emcLib6  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB6));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB6), 0, 0, emcLib6->bgShiftX, emcLib6->bgShiftY);
	EM2ColorShow * emcLib7  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB7));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB7), 0, 0, emcLib7->bgShiftX, emcLib7->bgShiftY);
	EM2ColorShow * emcLib8  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB8));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB8), 0, 0, emcLib8->bgShiftX, emcLib8->bgShiftY);
	EM2ColorShow * emcLib9  = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB9));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB9), 0, 0, emcLib9->bgShiftX, emcLib9->bgShiftY);
	EM2ColorShow * emcLib10 = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB10));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB10), 0, 0, emcLib10->bgShiftX, emcLib10->bgShiftY);
	EM2ColorShow * emcLib11 = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB11));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB11), 0, 0, emcLib11->bgShiftX, emcLib11->bgShiftY);
	EM2ColorShow * emcLib12 = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_COLOR2_LIB12));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_LIB12), 0, 0, emcLib12->bgShiftX, emcLib12->bgShiftY);

	// buttons
	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_COLOR2_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);
	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_COLOR2_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);

	EM2ComboBox * emcHSV = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_COLOR2_HUE_SAT_VAL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_COLOR2_HUE_SAT_VAL), 0, 0, emcHSV->bgShiftX, emcHSV->bgShiftY);

	return true;
}

//------------------------------------------------------------------------------------------------

void Color2DlgInvalidArrows(HWND hWnd)
{
	EM2Color2D * em2d = GetEM2Color2DInstance(GetDlgItem(hWnd, IDC_COLOR2_2D));
	EM2TrackBar * em2tb = GetEM2TrackBarInstance(GetDlgItem(hWnd, IDC_COLOR2_THIRD));
	if (em2d && em2tb)
	{
		RECT rect2, rect3;
		RECT r1, r2, r3;
		GetClientRect(em2d->hwnd, &rect2);
		GetClientRect(em2tb->hwnd, &rect3);
		POINT p2={0,0}, p3={0,0};
		MapWindowPoints(em2d->hwnd, hWnd, &p2, 1);
		MapWindowPoints(em2tb->hwnd, hWnd, &p3, 1);

		r1.left		= p2.x + rect2.left - 15;
		r1.right	= p2.x + rect2.left;
		r1.top		= p2.y + rect2.top;
		r1.bottom	= p2.y + rect2.bottom+10;
		r2.left		= p2.x + rect2.left - 15;
		r2.right	= p2.x + rect2.right + 10;
		r2.top		= p2.y + rect2.top-15;
		r2.bottom	= p2.y + rect2.top;
		r3.left		= p3.x + rect3.right;
		r3.right	= p3.x + rect3.right + 15;
		r3.top		= p3.y + rect3.top-10;
		r3.bottom	= p3.y + rect3.bottom+10;

		InvalidateRect(hWnd, &r1, false);
		InvalidateRect(hWnd, &r2, false);
		InvalidateRect(hWnd, &r3, false);
	}
	else
		InvalidateRect(hWnd, NULL, false);
}

//------------------------------------------------------------------------------------------------
