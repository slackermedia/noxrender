#include <windows.h>
#include <GdiPlus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"
#include "resource.h"

extern HMODULE hDllModule;

//---------------------------------------------------------------------

void InitEM2Color2D()
{
    WNDCLASSEX wc;
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2Color2D";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2Color2DProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2Color2D *);
    wc.hIconSm        = 0;
    RegisterClassEx(&wc);
}

//---------------------------------------------------------------------

EM2Color2D * GetEM2Color2DInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2Color2D * emcp = (EM2Color2D *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2Color2D * emcp = (EM2Color2D *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcp;
}

//---------------------------------------------------------------------

void SetEM2Color2DInstance(HWND hwnd, EM2Color2D *emcp)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcp);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcp);
	#endif
}

//---------------------------------------------------------------------

EM2Color2D::EM2Color2D(HWND hWnd)
{
	hwnd = hWnd;

	colBackground = NGCOL_BG_MEDIUM;
	colBorder = RGB(0,0,0);
	alphaBorder = 56;
	showBorder = true;

	px = py = 0;
	hRainbow = 0;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;

	defX = defY = 0;

	generateRainbow(MODE_HS_V, ColorHSV(1,1,0.5f));
}

//---------------------------------------------------------------------

EM2Color2D::~EM2Color2D()
{
	if (hRainbow)
		DeleteObject(hRainbow);
	hRainbow = 0;
}

//---------------------------------------------------------------------

LRESULT CALLBACK EM2Color2DProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2Color2D * emc = GetEM2Color2DInstance(hwnd);
	static int sx, sy;

	switch (msg)
	{
		case WM_CREATE:
			emc = new EM2Color2D(hwnd);
			SetEM2Color2DInstance(hwnd, emc);
			break;
		case WM_DESTROY:
			{
				delete emc;
				SetEM2Color2DInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
			{
				if (!emc) 
					break;

				RECT rect;
				HDC hdc;
				PAINTSTRUCT ps;

				GetClientRect(hwnd, &rect);
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);
				int bw = emc->showBorder ? 1 : 0;


				if (emc->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emc->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emc->bgShiftX, emc->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emc->colBackground));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emc->colBackground));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, OPAQUE);
				}

				if (emc->hRainbow)
				{
					HDC thdc = CreateCompatibleDC(hdc);
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emc->hRainbow);
					BitBlt(hdc, bw, bw, rect.right-2*bw, rect.bottom-2*bw, thdc, 0, 0, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}

				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);
						if (emc->showBorder)
						{
							Gdiplus::Color cBorder = Gdiplus::Color(emc->alphaBorder, GetRValue(emc->colBorder), GetGValue(emc->colBorder), GetBValue(emc->colBorder));
							Gdiplus::Pen bpen(cBorder);
							graphics.DrawRectangle(&bpen, 0,0, rect.right-1, rect.bottom-1);
						}
						Gdiplus::Color cCross = Gdiplus::Color(128, 255,255,255);
						Gdiplus::Pen bpen2(cCross);
						int d = 7;
						graphics.DrawLine(&bpen2, 
							max(bw, emc->px+bw-d), max(bw, emc->py+bw), 
							min(rect.right-bw-1, emc->px+bw+d), min(rect.bottom-bw-1, emc->py+bw)	);

						graphics.DrawLine(&bpen2, 
							max(bw, emc->px+bw), max(bw, emc->py+bw-d), 
							min(rect.right-bw-1, emc->px+bw), min(rect.bottom-bw-1, emc->py+bw+d)	);

					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
				{
					if (emc->showBorder)
					{
						HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
						COLORREF colborder = getMixedColor(emc->colBackground, emc->alphaBorder, emc->colBorder);
						HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, colborder));
						Rectangle(hdc, 0,0, rect.right, rect.bottom);
						SelectObject(hdc, oldPen);
						SelectObject(hdc, oldBrush);
					}
				}
				#endif


				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				int bw = emc->showBorder ? 1 : 0;
				if (!GetCapture())
					SetCapture(hwnd);
				POINT pt = {(short)LOWORD(lParam), (short)HIWORD(lParam)};
				pt.x = max(0, min(rect.right-2*bw-1,  pt.x-bw));
				pt.y = max(0, min(rect.bottom-2*bw-1, pt.y-bw));
				sx = emc->px;
				sy = emc->py;
				emc->px = pt.x;
				emc->py = pt.y;
				emc->notifyParent();
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONUP:
			{
				ReleaseCapture();
			}
			break;
		case WM_RBUTTONUP:
			{
				emc->px = emc->defX;
				emc->py = emc->defY;
				emc->notifyParent();
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_MOUSEMOVE:
			{
				if (!(wParam & MK_LBUTTON))
					break;
				RECT rect;
				GetClientRect(hwnd, &rect);
				int bw = emc->showBorder ? 1 : 0;
				POINT pt = {(short)LOWORD(lParam), (short)HIWORD(lParam)};
				if (wParam & MK_SHIFT)
				{
					if (abs(pt.x-sx) > abs(pt.y-sy))
						pt.y = sy+bw;
					else
						pt.x = sx+bw;
				}
				pt.x = max(0, min(rect.right-2*bw-1,  pt.x-bw));
				pt.y = max(0, min(rect.bottom-2*bw-1, pt.y-bw));
				emc->px = pt.x;
				emc->py = pt.y;
				InvalidateRect(hwnd, NULL, false);
				emc->notifyParent();
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//---------------------------------------------------------------------

void EM2Color2D::notifyParent()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_VSCROLL), (LPARAM)hwnd);
}

//---------------------------------------------------------------------

void EM2Color2D::setPosition(float x, float y)
{
	x = min(1.0f, max(0.0f, x));
	y = min(1.0f, max(0.0f, y));
	int bw = showBorder ? 1 : 0;
	RECT rect;
	GetClientRect(hwnd, &rect);
	px = (int)(x*(rect.right-1-2*bw));
	py = (int)((1.0f-y)*(rect.bottom-1-2*bw));
	InvalidateRect(hwnd, NULL, false);
}

//---------------------------------------------------------------------

void EM2Color2D::setDefaultPos(float x, float y)
{
	x = min(1.0f, max(0.0f, x));
	y = min(1.0f, max(0.0f, y));
	int bw = showBorder ? 1 : 0;
	RECT rect;
	GetClientRect(hwnd, &rect);
	defX = (int)(x*(rect.right-1-2*bw));
	defY = (int)((1.0f-y)*(rect.bottom-1-2*bw));
}

//---------------------------------------------------------------------

bool EM2Color2D::getPosition(float &x, float &y)
{
	int bw = showBorder ? 1 : 0;
	RECT rect;
	GetClientRect(hwnd, &rect);
	x = px/(float)(rect.right-1-2*bw);
	y = 1.0f-py/(float)(rect.bottom-1-2*bw);
	return true;
}

//---------------------------------------------------------------------

bool EM2Color2D::generateRainbow(int mode, ColorHSV hsv)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	int bw = showBorder ? 1 : 0;
	int w = rect.right - 2*bw;
	int h = rect.bottom - 2*bw;

	unsigned char * buf = (unsigned char *)malloc(w*h*4);
	if (!buf)
		return false;

	switch (mode)
	{
		case MODE_HS_V:
			{
				for (int y=0; y<h; y++)
				{
					float fy = (float)y/(float)(h-1);
					hsv.s = 1.0f-fy;
					for (int x=0; x<w; x++)
					{
						float fx = (float)x/(float)(w-1);
						hsv.h = fx;
						Color4 rgb = hsv.toColor4();
						buf[(y*w+x)*4+0] = (unsigned char)max(0.0f, min(255.0f, rgb.b*255.0f));
						buf[(y*w+x)*4+1] = (unsigned char)max(0.0f, min(255.0f, rgb.g*255.0f));
						buf[(y*w+x)*4+2] = (unsigned char)max(0.0f, min(255.0f, rgb.r*255.0f));
						buf[(y*w+x)*4+3] = 255;
					}
				}
			}
			break;
		case MODE_HV_S:
			{
				for (int y=0; y<h; y++)
				{
					float fy = (float)y/(float)(h-1);
					hsv.v = 1.0f-fy;
					for (int x=0; x<w; x++)
					{
						float fx = (float)x/(float)(w-1);
						hsv.h = fx;
						Color4 rgb = hsv.toColor4();
						buf[(y*w+x)*4+0] = (unsigned char)max(0.0f, min(255.0f, rgb.b*255.0f));
						buf[(y*w+x)*4+1] = (unsigned char)max(0.0f, min(255.0f, rgb.g*255.0f));
						buf[(y*w+x)*4+2] = (unsigned char)max(0.0f, min(255.0f, rgb.r*255.0f));
						buf[(y*w+x)*4+3] = 255;
					}
				}
			}
			break;
		case MODE_SH_V:
			{
				for (int y=0; y<h; y++)
				{
					float fy = (float)y/(float)(h-1);
					hsv.h = 1.0f-fy;
					for (int x=0; x<w; x++)
					{
						float fx = (float)x/(float)(w-1);
						hsv.s = fx;
						Color4 rgb = hsv.toColor4();
						buf[(y*w+x)*4+0] = (unsigned char)max(0.0f, min(255.0f, rgb.b*255.0f));
						buf[(y*w+x)*4+1] = (unsigned char)max(0.0f, min(255.0f, rgb.g*255.0f));
						buf[(y*w+x)*4+2] = (unsigned char)max(0.0f, min(255.0f, rgb.r*255.0f));
						buf[(y*w+x)*4+3] = 255;
					}
				}
			}
			break;
		case MODE_SV_H:
			{
				for (int y=0; y<h; y++)
				{
					float fy = (float)y/(float)(h-1);
					hsv.v = 1.0f-fy;
					for (int x=0; x<w; x++)
					{
						float fx = (float)x/(float)(w-1);
						hsv.s = fx;
						Color4 rgb = hsv.toColor4();
						buf[(y*w+x)*4+0] = (unsigned char)max(0.0f, min(255.0f, rgb.b*255.0f));
						buf[(y*w+x)*4+1] = (unsigned char)max(0.0f, min(255.0f, rgb.g*255.0f));
						buf[(y*w+x)*4+2] = (unsigned char)max(0.0f, min(255.0f, rgb.r*255.0f));
						buf[(y*w+x)*4+3] = 255;
					}
				}
			}
			break;
		case MODE_VS_H:
			{
				for (int y=0; y<h; y++)
				{
					float fy = (float)y/(float)(h-1);
					hsv.s = 1.0f-fy;
					for (int x=0; x<w; x++)
					{
						float fx = (float)x/(float)(w-1);
						hsv.v = fx;
						Color4 rgb = hsv.toColor4();
						buf[(y*w+x)*4+0] = (unsigned char)max(0.0f, min(255.0f, rgb.b*255.0f));
						buf[(y*w+x)*4+1] = (unsigned char)max(0.0f, min(255.0f, rgb.g*255.0f));
						buf[(y*w+x)*4+2] = (unsigned char)max(0.0f, min(255.0f, rgb.r*255.0f));
						buf[(y*w+x)*4+3] = 255;
					}
				}
			}
			break;
		case MODE_VH_S:
			{
				for (int y=0; y<h; y++)
				{
					float fy = (float)y/(float)(h-1);
					hsv.h = 1.0f-fy;
					for (int x=0; x<w; x++)
					{
						float fx = (float)x/(float)(w-1);
						hsv.v = fx;
						Color4 rgb = hsv.toColor4();
						buf[(y*w+x)*4+0] = (unsigned char)max(0.0f, min(255.0f, rgb.b*255.0f));
						buf[(y*w+x)*4+1] = (unsigned char)max(0.0f, min(255.0f, rgb.g*255.0f));
						buf[(y*w+x)*4+2] = (unsigned char)max(0.0f, min(255.0f, rgb.r*255.0f));
						buf[(y*w+x)*4+3] = 255;
					}
				}
			}
			break;
	}

	HBITMAP hbmp = CreateBitmap(w,h, 1, 32, buf);
	if (hbmp)
	{
		if (hRainbow)
			DeleteObject(hRainbow);
		hRainbow = hbmp;
	}

	free(buf);

	return true;
}

//---------------------------------------------------------------------
