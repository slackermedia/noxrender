#include <windows.h>
#include "raytracer.h"
#include "log.h"
#include <xmmintrin.h>

//#define DEBUG_BVH
#define REVERSE_MATRIX_MULT

union MaskSSE
{
	__m128 vf;
	__m128i vi;
} ;

inline void TransposeSSE(__m128 &A, __m128 &B, __m128 &C, __m128 &D);
bool checkBVHTree(BVH::Node * t);

//---------------------------------------------------------------------

BVH::BVH() :
	meshes(16)
{
	root = NULL;
}

//---------------------------------------------------------------------

BVH::~BVH()
{
}

//---------------------------------------------------------------------

bool BVH::addMesh(Mesh * mesh)
{
	CHECK(mesh);
	BSPTree * bsp = new BSPTree();
	CHECK(bsp);
	bsp->name = copyString(mesh->name);
	bsp->addTrisMesh(mesh);
	bsp->makeBSP();
	meshes.add(bsp);
	meshes.createArray();
	return true;
}

//---------------------------------------------------------------------

MeshInstance::MeshInstance() 
	: matsFromFile(0), materials(0) 
{ 
	meshID = 0; 
	instSrcID = -1; 
	matrix_transform.setIdentity(); 
	matrix_inv.setIdentity(); 
	matrix_offset.setIdentity();
	matrix_offset_inv.setIdentity();
	name = NULL; 
	mb_velocity = Vector3d(0,0,0);
	mb_rot_axis = Vector3d(0,0,0);
	mb_rot_vel = 0.0f;
	mb_on = false;
	has_displacement = false;
	displace_orig_ID = -1;
}

//---------------------------------------------------------------------

MeshInstance::MeshInstance(const MeshInstance &mInst)
	: matsFromFile(0), materials(0)
{
	meshID = mInst.meshID;
	instSrcID = mInst.instSrcID;
	matrix_transform = mInst.matrix_transform;
	matrix_inv = mInst.matrix_inv;
	matrix_offset = mInst.matrix_offset;
	matrix_offset_inv = mInst.matrix_offset_inv;
	name = copyString(mInst.name);
	mb_on = mInst.mb_on;
	mb_velocity = mInst.mb_velocity;
	mb_rot_axis = mInst.mb_rot_axis;
	mb_rot_vel = mInst.mb_rot_vel;
	has_displacement = mInst.has_displacement;
	displace_orig_ID = mInst.displace_orig_ID;
	for (int i=0; i<mInst.matsFromFile.objCount; i++)
		matsFromFile.add(mInst.matsFromFile[i]);
	matsFromFile.createArray();
	for (int i=0; i<mInst.materials.objCount; i++)
		materials.add(mInst.materials[i]);
	materials.createArray();
}

//---------------------------------------------------------------------

MeshInstance& MeshInstance::operator= (const MeshInstance& mInst)
{
	if (this == &mInst) 
		return *this;

	meshID = mInst.meshID;
	instSrcID = mInst.instSrcID;
	has_displacement = mInst.has_displacement;
	displace_orig_ID = mInst.displace_orig_ID;
	matrix_transform = mInst.matrix_transform;
	matrix_inv = mInst.matrix_inv;
	mb_on = mInst.mb_on;
	mb_velocity = mInst.mb_velocity;
	mb_rot_axis = mInst.mb_rot_axis;
	mb_rot_vel = mInst.mb_rot_vel;
	matrix_offset = mInst.matrix_offset;
	matrix_offset_inv = mInst.matrix_offset_inv;
	if (name)
		free(name);
	name = copyString(mInst.name);
	matsFromFile.freeList();
	for (int i=0; i<mInst.matsFromFile.objCount; i++)
		matsFromFile.add(mInst.matsFromFile[i]);
	matsFromFile.createArray();
	materials.freeList();
	for (int i=0; i<mInst.materials.objCount; i++)
		materials.add(mInst.materials[i]);
	materials.createArray();

	return *this;
}

//---------------------------------------------------------------------
bool BVH::createTree()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	int numinst = scene->instances.objCount;
	int nummbinst = 0;

	char buf[256];

	float mb_time = scene->sscene.motion_blur_max_time;

	int nummeshes = scene->meshes.objCount;
	__m128 ** meshesSSE = (__m128 **)malloc(sizeof(void*) * nummeshes);
	unsigned int * nummeshtris = (unsigned int *)malloc(sizeof(unsigned int)*nummeshes);
	for (int i=0; i<nummeshes; i++)
	{
		unsigned int tnum1 = scene->meshes[i].lastTri - scene->meshes[i].firstTri + 1;
		unsigned int tnum2 = tnum1;
		if (tnum1%4)
			tnum2 += 4-(tnum2%4);
		int tnum3 = tnum1/4;
		int tnum4 = tnum2/4;
		meshesSSE[i] = (__m128*)_aligned_malloc(tnum2*36, 16);
		nummeshtris[i] = tnum2;

		__m128 * meshSSE = meshesSSE[i];

		int st = scene->meshes[i].firstTri;
		for (int j=0; j<tnum3; j++)
		{
			meshSSE[9*j+0].m128_f32[0] = scene->triangles[st+4*j+0].V1.x;
			meshSSE[9*j+0].m128_f32[1] = scene->triangles[st+4*j+0].V2.x;
			meshSSE[9*j+0].m128_f32[2] = scene->triangles[st+4*j+0].V3.x;
			meshSSE[9*j+0].m128_f32[3] = scene->triangles[st+4*j+1].V1.x;
			meshSSE[9*j+1].m128_f32[0] = scene->triangles[st+4*j+0].V1.y;
			meshSSE[9*j+1].m128_f32[1] = scene->triangles[st+4*j+0].V2.y;
			meshSSE[9*j+1].m128_f32[2] = scene->triangles[st+4*j+0].V3.y;
			meshSSE[9*j+1].m128_f32[3] = scene->triangles[st+4*j+1].V1.y;
			meshSSE[9*j+2].m128_f32[0] = scene->triangles[st+4*j+0].V1.z;
			meshSSE[9*j+2].m128_f32[1] = scene->triangles[st+4*j+0].V2.z;
			meshSSE[9*j+2].m128_f32[2] = scene->triangles[st+4*j+0].V3.z;
			meshSSE[9*j+2].m128_f32[3] = scene->triangles[st+4*j+1].V1.z;

			meshSSE[9*j+3].m128_f32[0] = scene->triangles[st+4*j+1].V2.x;
			meshSSE[9*j+3].m128_f32[1] = scene->triangles[st+4*j+1].V3.x;
			meshSSE[9*j+3].m128_f32[2] = scene->triangles[st+4*j+2].V1.x;
			meshSSE[9*j+3].m128_f32[3] = scene->triangles[st+4*j+2].V2.x;
			meshSSE[9*j+4].m128_f32[0] = scene->triangles[st+4*j+1].V2.y;
			meshSSE[9*j+4].m128_f32[1] = scene->triangles[st+4*j+1].V3.y;
			meshSSE[9*j+4].m128_f32[2] = scene->triangles[st+4*j+2].V1.y;
			meshSSE[9*j+4].m128_f32[3] = scene->triangles[st+4*j+2].V2.y;
			meshSSE[9*j+5].m128_f32[0] = scene->triangles[st+4*j+1].V2.z;
			meshSSE[9*j+5].m128_f32[1] = scene->triangles[st+4*j+1].V3.z;
			meshSSE[9*j+5].m128_f32[2] = scene->triangles[st+4*j+2].V1.z;
			meshSSE[9*j+5].m128_f32[3] = scene->triangles[st+4*j+2].V2.z;

			meshSSE[9*j+6].m128_f32[0] = scene->triangles[st+4*j+2].V3.x;
			meshSSE[9*j+6].m128_f32[1] = scene->triangles[st+4*j+3].V1.x;
			meshSSE[9*j+6].m128_f32[2] = scene->triangles[st+4*j+3].V2.x;
			meshSSE[9*j+6].m128_f32[3] = scene->triangles[st+4*j+3].V3.x;
			meshSSE[9*j+7].m128_f32[0] = scene->triangles[st+4*j+2].V3.y;
			meshSSE[9*j+7].m128_f32[1] = scene->triangles[st+4*j+3].V1.y;
			meshSSE[9*j+7].m128_f32[2] = scene->triangles[st+4*j+3].V2.y;
			meshSSE[9*j+7].m128_f32[3] = scene->triangles[st+4*j+3].V3.y;
			meshSSE[9*j+8].m128_f32[0] = scene->triangles[st+4*j+2].V3.z;
			meshSSE[9*j+8].m128_f32[1] = scene->triangles[st+4*j+3].V1.z;
			meshSSE[9*j+8].m128_f32[2] = scene->triangles[st+4*j+3].V2.z;
			meshSSE[9*j+8].m128_f32[3] = scene->triangles[st+4*j+3].V3.z;
		}

		if (tnum1%4)
		{
			int tmax = tnum1-1;
			meshSSE[9*tnum3+0].m128_f32[0] = scene->triangles[st+min(4*tnum3+0, tmax)].V1.x;
			meshSSE[9*tnum3+0].m128_f32[1] = scene->triangles[st+min(4*tnum3+0, tmax)].V2.x;
			meshSSE[9*tnum3+0].m128_f32[2] = scene->triangles[st+min(4*tnum3+0, tmax)].V3.x;
			meshSSE[9*tnum3+0].m128_f32[3] = scene->triangles[st+min(4*tnum3+1, tmax)].V1.x;
			meshSSE[9*tnum3+1].m128_f32[0] = scene->triangles[st+min(4*tnum3+0, tmax)].V1.y;
			meshSSE[9*tnum3+1].m128_f32[1] = scene->triangles[st+min(4*tnum3+0, tmax)].V2.y;
			meshSSE[9*tnum3+1].m128_f32[2] = scene->triangles[st+min(4*tnum3+0, tmax)].V3.y;
			meshSSE[9*tnum3+1].m128_f32[3] = scene->triangles[st+min(4*tnum3+1, tmax)].V1.y;
			meshSSE[9*tnum3+2].m128_f32[0] = scene->triangles[st+min(4*tnum3+0, tmax)].V1.z;
			meshSSE[9*tnum3+2].m128_f32[1] = scene->triangles[st+min(4*tnum3+0, tmax)].V2.z;
			meshSSE[9*tnum3+2].m128_f32[2] = scene->triangles[st+min(4*tnum3+0, tmax)].V3.z;
			meshSSE[9*tnum3+2].m128_f32[3] = scene->triangles[st+min(4*tnum3+1, tmax)].V1.z;
			meshSSE[9*tnum3+3].m128_f32[0] = scene->triangles[st+min(4*tnum3+1, tmax)].V2.x;
			meshSSE[9*tnum3+3].m128_f32[1] = scene->triangles[st+min(4*tnum3+1, tmax)].V3.x;
			meshSSE[9*tnum3+3].m128_f32[2] = scene->triangles[st+min(4*tnum3+2, tmax)].V1.x;
			meshSSE[9*tnum3+3].m128_f32[3] = scene->triangles[st+min(4*tnum3+2, tmax)].V2.x;
			meshSSE[9*tnum3+4].m128_f32[0] = scene->triangles[st+min(4*tnum3+1, tmax)].V2.y;
			meshSSE[9*tnum3+4].m128_f32[1] = scene->triangles[st+min(4*tnum3+1, tmax)].V3.y;
			meshSSE[9*tnum3+4].m128_f32[2] = scene->triangles[st+min(4*tnum3+2, tmax)].V1.y;
			meshSSE[9*tnum3+4].m128_f32[3] = scene->triangles[st+min(4*tnum3+2, tmax)].V2.y;
			meshSSE[9*tnum3+5].m128_f32[0] = scene->triangles[st+min(4*tnum3+1, tmax)].V2.z;
			meshSSE[9*tnum3+5].m128_f32[1] = scene->triangles[st+min(4*tnum3+1, tmax)].V3.z;
			meshSSE[9*tnum3+5].m128_f32[2] = scene->triangles[st+min(4*tnum3+2, tmax)].V1.z;
			meshSSE[9*tnum3+5].m128_f32[3] = scene->triangles[st+min(4*tnum3+2, tmax)].V2.z;
			meshSSE[9*tnum3+6].m128_f32[0] = scene->triangles[st+min(4*tnum3+2, tmax)].V3.x;
			meshSSE[9*tnum3+6].m128_f32[1] = scene->triangles[st+min(4*tnum3+3, tmax)].V1.x;
			meshSSE[9*tnum3+6].m128_f32[2] = scene->triangles[st+min(4*tnum3+3, tmax)].V2.x;
			meshSSE[9*tnum3+6].m128_f32[3] = scene->triangles[st+min(4*tnum3+3, tmax)].V3.x;
			meshSSE[9*tnum3+7].m128_f32[0] = scene->triangles[st+min(4*tnum3+2, tmax)].V3.y;
			meshSSE[9*tnum3+7].m128_f32[1] = scene->triangles[st+min(4*tnum3+3, tmax)].V1.y;
			meshSSE[9*tnum3+7].m128_f32[2] = scene->triangles[st+min(4*tnum3+3, tmax)].V2.y;
			meshSSE[9*tnum3+7].m128_f32[3] = scene->triangles[st+min(4*tnum3+3, tmax)].V3.y;
			meshSSE[9*tnum3+8].m128_f32[0] = scene->triangles[st+min(4*tnum3+2, tmax)].V3.z;
			meshSSE[9*tnum3+8].m128_f32[1] = scene->triangles[st+min(4*tnum3+3, tmax)].V1.z;
			meshSSE[9*tnum3+8].m128_f32[2] = scene->triangles[st+min(4*tnum3+3, tmax)].V2.z;
			meshSSE[9*tnum3+8].m128_f32[3] = scene->triangles[st+min(4*tnum3+3, tmax)].V3.z;
		}
	}

	// FIND BOUNDING BOX - SSE ALIGNED
	BVHMBInstance * instances = (BVHMBInstance *)malloc(sizeof(BVHMBInstance) * numinst);
	for (int i=0; i<numinst; i++)
	{
		sprintf_s(buf, 256, "Evaluating boundary of instance %s", scene->instances[i].name);
		Logger::add(buf);
		scene->notifyProgress(buf, i*100.0f/(float)numinst);
		if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
		{
			Logger::add("Aborted by user!!!");
			// free temp stuff
			for (int i=0; i<nummeshes; i++)
				_aligned_free(meshesSSE[i]);
			free(meshesSSE);
			free(nummeshtris);
			meshesSSE = NULL;
			nummeshtris = NULL;
			return false;
		}
		instances[i].matr_tr = scene->instances[i].matrix_transform;
		instances[i].matr_inv = scene->instances[i].matrix_inv;
		int meshID = scene->instances[i].meshID;
		int s1 = scene->meshes[meshID].firstTri;
		int s2 = scene->meshes[meshID].lastTri;

		__m128 minV = _mm_set1_ps(BIGFLOAT);
		__m128 maxV = _mm_set1_ps(-BIGFLOAT);

		__m128 minVX = _mm_set1_ps(BIGFLOAT);
		__m128 minVY = _mm_set1_ps(BIGFLOAT);
		__m128 minVZ = _mm_set1_ps(BIGFLOAT);
		__m128 maxVX = _mm_set1_ps(-BIGFLOAT);
		__m128 maxVY = _mm_set1_ps(-BIGFLOAT);
		__m128 maxVZ = _mm_set1_ps(-BIGFLOAT);

		__m128 * meshSSE = meshesSSE[meshID];
		int t2 = nummeshtris[meshID];
		int t3 = t2/4;
		int maxmb = 1;
		if (scene->instances[i].mb_on)
			maxmb = 10;
		for (int mb=0; mb<maxmb; mb++)
		{
			Matrix4d m = instances[i].matr_tr.toFullMatrix();
			Matrix4d m_o = scene->instances[i].matrix_offset;
			Matrix4d m_oi = scene->instances[i].matrix_offset_inv;

			float ctime = 0;
			if (maxmb>1)	// get motion blur matrix
			{
				ctime = mb/(float)(maxmb-1) * mb_time;
				Matrix4d m_mb;
				m_mb.setIdentity();
				if (scene->instances[i].mb_rot_vel!=0.0f)
					m_mb.setRotation(scene->instances[i].mb_rot_axis, scene->instances[i].mb_rot_vel*ctime);
				m_mb.M03 = scene->instances[i].mb_velocity.x * ctime;
				m_mb.M13 = scene->instances[i].mb_velocity.y * ctime;
				m_mb.M23 = scene->instances[i].mb_velocity.z * ctime;

				#ifdef REVERSE_MATRIX_MULT
					m = m * m_oi * m_mb * m_o;
				#else
					m = m_mb * m;	// orig
				#endif
			}

			__m128 M00 = _mm_set1_ps(m.M00);
			__m128 M01 = _mm_set1_ps(m.M01);
			__m128 M02 = _mm_set1_ps(m.M02);
			__m128 M03 = _mm_set1_ps(m.M03);
			__m128 M10 = _mm_set1_ps(m.M10);
			__m128 M11 = _mm_set1_ps(m.M11);
			__m128 M12 = _mm_set1_ps(m.M12);
			__m128 M13 = _mm_set1_ps(m.M13);
			__m128 M20 = _mm_set1_ps(m.M20);
			__m128 M21 = _mm_set1_ps(m.M21);
			__m128 M22 = _mm_set1_ps(m.M22);
			__m128 M23 = _mm_set1_ps(m.M23);
			__m128 M30 = _mm_set1_ps(m.M30);
			__m128 M31 = _mm_set1_ps(m.M31);
			__m128 M32 = _mm_set1_ps(m.M32);
			__m128 M33 = _mm_set1_ps(m.M33);

			for (int j=0; j<t3; j++)
			{
				for (int k=0; k<3; k++)
				{
					__m128 sv1 = meshSSE[j*9+3*k];		// x x x x
					__m128 sv2 = meshSSE[j*9+3*k+1];	// y y y y
					__m128 sv3 = meshSSE[j*9+3*k+2];	// z z z z
					__m128 vv1a = _mm_mul_ps(sv1, M00);
					__m128 vv1b = _mm_mul_ps(sv2, M01);
					__m128 vv1c = _mm_mul_ps(sv3, M02);
					__m128 vv2a = _mm_mul_ps(sv1, M10);
					__m128 vv2b = _mm_mul_ps(sv2, M11);
					__m128 vv2c = _mm_mul_ps(sv3, M12);
					__m128 vv3a = _mm_mul_ps(sv1, M20);
					__m128 vv3b = _mm_mul_ps(sv2, M21);
					__m128 vv3c = _mm_mul_ps(sv3, M22);
					__m128 vv1d = _mm_add_ps(vv1a, vv1b);
					__m128 vv1e = _mm_add_ps(vv1c, M03);
					__m128 vv2d = _mm_add_ps(vv2a, vv2b);
					__m128 vv2e = _mm_add_ps(vv2c, M13);
					__m128 vv3d = _mm_add_ps(vv3a, vv3b);
					__m128 vv3e = _mm_add_ps(vv3c, M23);
					__m128 dvx = _mm_add_ps(vv1d, vv1e);
					__m128 dvy = _mm_add_ps(vv2d, vv2e);
					__m128 dvz = _mm_add_ps(vv3d, vv3e);
					minVX = _mm_min_ps(minVX, dvx);
					maxVX = _mm_max_ps(maxVX, dvx);
					minVY = _mm_min_ps(minVY, dvy);
					maxVY = _mm_max_ps(maxVY, dvy);
					minVZ = _mm_min_ps(minVZ, dvz);
					maxVZ = _mm_max_ps(maxVZ, dvz);
				}
			}
		}
		instances[i].minx = min(min(minVX.m128_f32[0], minVX.m128_f32[1]), min(minVX.m128_f32[2], minVX.m128_f32[3]));
		instances[i].miny = min(min(minVY.m128_f32[0], minVY.m128_f32[1]), min(minVY.m128_f32[2], minVY.m128_f32[3]));
		instances[i].minz = min(min(minVZ.m128_f32[0], minVZ.m128_f32[1]), min(minVZ.m128_f32[2], minVZ.m128_f32[3]));
		instances[i].maxx = max(max(maxVX.m128_f32[0], maxVX.m128_f32[1]), max(maxVX.m128_f32[2], maxVX.m128_f32[3]));
		instances[i].maxy = max(max(maxVY.m128_f32[0], maxVY.m128_f32[1]), max(maxVY.m128_f32[2], maxVY.m128_f32[3]));
		instances[i].maxz = max(max(maxVZ.m128_f32[0], maxVZ.m128_f32[1]), max(maxVZ.m128_f32[2], maxVZ.m128_f32[3]));
		instances[i].offset_right = meshID;
		instances[i].offset_left = 0x80000000 | (unsigned int)i;

		if (scene->instances[i].mb_on) 
		{
			instances[i].offset_left |= 0x40000000;
			nummbinst++;
			instances[i].matr_tr = scene->instances[i].matrix_transform * scene->instances[i].matrix_offset_inv;	// M3
			instances[i].matr_inv = scene->instances[i].matrix_offset * scene->instances[i].matrix_inv;	// M3^-1
		}
		instances[i].mb_angle_vel = scene->instances[i].mb_rot_vel;
		instances[i].mb_angle_x = scene->instances[i].mb_rot_axis.x;
		instances[i].mb_angle_y = scene->instances[i].mb_rot_axis.y;
		instances[i].mb_angle_z = scene->instances[i].mb_rot_axis.z;
		instances[i].mb_vel_x = scene->instances[i].mb_velocity.x;
		instances[i].mb_vel_y = scene->instances[i].mb_velocity.y;
		instances[i].mb_vel_z = scene->instances[i].mb_velocity.z;
		instances[i].matr_offset = scene->instances[i].matrix_offset;
		instances[i].matr_offset_inv = scene->instances[i].matrix_offset_inv;
	}

	for (int i=0; i<nummeshes; i++)
		_aligned_free(meshesSSE[i]);
	free(meshesSSE);
	free(nummeshtris);
	meshesSSE = NULL;
	nummeshtris = NULL;

	// ADD INSTANCES TO DYNAMIC TREE
	myList<BVH::Node *> nodes;
	for (int i=0; i<numinst; i++)
	{
		BVH::Node * node = new BVH::Node();
		node->left = NULL;
		node->right = NULL;
		node->offsettoroot = 0;
		node->ptr = &(instances[i]);
		node->type = BVH::Node::TYPE_INSTANCE;
		if (instances[i].offset_left & 0x40000000)
			node->type = BVH::Node::TYPE_INSTANCE_MB;
		nodes.add(node);
	}
	nodes.createArray();

	// CREATE BVH TREE, SEGREGATE
	
	int numnodes = nodes.objCount;
	float * bestSAHs = (float *)malloc(nodes.objCount*sizeof(float));
	int * bestIDs = (int *)malloc(nodes.objCount*sizeof(int));
	int * toRefresh = (int *)malloc(nodes.objCount*sizeof(int));
	int numRefresh;

	for (int i=0; i<numnodes; i++)
		bestSAHs[i] = BIGFLOAT;
	for (int i=0; i<numnodes; i++)
		bestIDs[i] = -1;

	for (int i=0; i<numnodes; i++)
	{
		BVHSplit * bnode1 = (BVHSplit*)nodes[i]->ptr;
		for (int j=i+1; j<numnodes; j++)
		{
			float jminx, jminy, jminz, jmaxx, jmaxy, jmaxz;
			BVHSplit * bnode2 = (BVHSplit*)nodes[j]->ptr;
			jminx = min(bnode1->minx, bnode2->minx);
			jminy = min(bnode1->miny, bnode2->miny);
			jminz = min(bnode1->minz, bnode2->minz);
			jmaxx = max(bnode1->maxx, bnode2->maxx);
			jmaxy = max(bnode1->maxy, bnode2->maxy);
			jmaxz = max(bnode1->maxz, bnode2->maxz);

			float x1 = jmaxx-jminx;
			float y1 = jmaxy-jminy;
			float z1 = jmaxz-jminz;
			float pSAH = (x1*y1 + x1*z1 + y1*z1);

			if (pSAH <= bestSAHs[i])
			{
				bestSAHs[i] = pSAH;
				bestIDs[i] = j;
			}
			if (pSAH <= bestSAHs[j])
			{
				bestSAHs[j] = pSAH;
				bestIDs[j] = i;
			}
		}
	}

	int orig_nodes = nodes.objCount;
	bool still_inst = (nodes.objCount>1);
	int num_splits = 0;
	while (still_inst)
	{
		sprintf_s(buf, 256, "Creating BVH, nodes left: %d", nodes.objCount);
		Logger::add(buf);

		scene->notifyProgress(buf, (orig_nodes-nodes.objCount)*100.0f/(float)orig_nodes);
		if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
		{
			Logger::add("Aborted by user!!!");
			return false;
		}

		// find lowest SAH connection
		numRefresh = 0;
		int curNum = nodes.objCount;
		int best_i = -1;
		int best_j = -1;
		float bestSAH = BIGFLOAT;
		for (int i=0; i<curNum; i++)
		{
			if (bestSAHs[i] <= bestSAH)
			{
				bestSAH = bestSAHs[i];
				best_i = i;
				best_j = bestIDs[i];
			}
		}
		
		if (best_i==best_j)
		{
			MessageBox(0, "Error. Removing same BVH node twice.", "Error", 0);
			if (best_i==-1)
				MessageBox(0, "Error. -1 -1", "Error", 0);	

		}

		int r_i = min(best_i, best_j);
		int r_j = max(best_i, best_j);

		// create and initialize new node
		BVHSplit * bnode3 = (BVHSplit*)nodes[r_i]->ptr;
		BVHSplit * bnode4 = (BVHSplit*)nodes[r_j]->ptr;
		BVH::BVHSplit * bsplit = new BVH::BVHSplit();
		bsplit->minx = min(bnode3->minx, bnode4->minx);
		bsplit->maxx = max(bnode3->maxx, bnode4->maxx);
		bsplit->miny = min(bnode3->miny, bnode4->miny);
		bsplit->maxy = max(bnode3->maxy, bnode4->maxy);
		bsplit->minz = min(bnode3->minz, bnode4->minz);
		bsplit->maxz = max(bnode3->maxz, bnode4->maxz);
		BVH::Node * newnode = new BVH::Node();
		newnode->left = nodes[r_i];
		newnode->right = nodes[r_j];
		newnode->ptr = bsplit;
		newnode->offsettoroot = 0;
		newnode->type = BVH::Node::TYPE_SPLIT;

		#ifdef DEBUG_BVH
			sprintf_s(buf, 256, "removing %d and %d", r_i, r_j);
			Logger::add(buf);
		#endif
		// remove nodes from array
		nodes.removeElement(r_j);
		nodes.createArray();
		nodes.removeElement(r_i);
		nodes.createArray();

		// change connections
		for (int i=0; i<curNum; i++)
		{
			if (bestIDs[i]==r_j  ||  bestIDs[i]==r_i)
			{
				if (i==r_i  ||  i==r_j)
					continue;
				int t = i;
				if (i>r_i)
					t--;
				if (i>r_j)
					t--;
				toRefresh[numRefresh++] = t;
			}
		}
		for (int i=0; i<curNum; i++)
		{
			if (bestIDs[i]>r_j)
				bestIDs[i]--;
		}
		for (int i=0; i<curNum; i++)
			if (bestIDs[i]>r_i)
				bestIDs[i]--;

		// remove SAH preevaled and connections for removed i and j
		for (int i=r_j; i<curNum-1; i++)
			bestIDs[i] = bestIDs[i+1];
		for (int i=r_i; i<curNum-2; i++)
			bestIDs[i] = bestIDs[i+1];
		for (int i=r_j; i<curNum-1; i++)
			bestSAHs[i] = bestSAHs[i+1];
		for (int i=r_i; i<curNum-2; i++)
			bestSAHs[i] = bestSAHs[i+1];

		// add new node to array
		nodes.add(newnode);
		nodes.createArray();

		toRefresh[numRefresh++] = nodes.objCount-1;
		for (int i=0; i<numRefresh; i++)
		{
			int l = toRefresh[i];
			#ifdef DEBUG_BVH
				sprintf_s(buf, 256, "Refreshing %d", l);
				Logger::add(buf);
			#endif
			bestSAHs[l] = BIGFLOAT;
			BVHSplit * bnode1 = (BVHSplit*)nodes[l]->ptr;
			for (int j=0; j<l; j++)
			{
				float jminx, jminy, jminz, jmaxx, jmaxy, jmaxz;
				BVHSplit * bnode2 = (BVHSplit*)nodes[j]->ptr;
				jminx = min(bnode1->minx, bnode2->minx);
				jminy = min(bnode1->miny, bnode2->miny);
				jminz = min(bnode1->minz, bnode2->minz);
				jmaxx = max(bnode1->maxx, bnode2->maxx);
				jmaxy = max(bnode1->maxy, bnode2->maxy);
				jmaxz = max(bnode1->maxz, bnode2->maxz);

				float x1 = jmaxx-jminx;
				float y1 = jmaxy-jminy;
				float z1 = jmaxz-jminz;
				float pSAH = (x1*y1 + x1*z1 + y1*z1);

				if (pSAH <= bestSAHs[l])
				{
					bestSAHs[l] = pSAH;
					bestIDs[l] = j;
				}
				if (pSAH <= bestSAHs[j])
				{
					bestSAHs[j] = pSAH;
					bestIDs[j] = l;
				}
			}
		}

		num_splits++;
		still_inst = (nodes.objCount>1);
	}
	
	Logger::add("BVH created.. now optimizing");

	free(bestSAHs);
	free(bestIDs);

	//---------------------------------------------------------------------------------------

	#ifdef DEBUG_BVH
		bool bvhtreestable = checkBVHTree(nodes[0]);
		if (!bvhtreestable)
			Logger::add("bvh tree is not stable");
	#endif

	// ALLOC BUFFER FOR BVH
	unsigned int inst_size = sizeof(BVH::BVHInstance);
	unsigned int mb_size = sizeof(BVH::BVHMBInstance);
	unsigned int split_size = sizeof(BVH::BVHSplit);
	unsigned long long tree_size = num_splits * split_size + (numinst-nummbinst) * inst_size + mb_size * nummbinst;

	#ifndef _WIN64
		if (tree_size>2147483647)
		{
			char bbuf[256];
			sprintf_s(bbuf, 256, "Error, tree size in BVH is %lld, too much for 32 bit version.", tree_size);
			MessageBox(0, bbuf, "Error", 0);
			return false;
		}
	#endif

	root = (unsigned char *)_aligned_malloc((size_t)tree_size, 32);
	if (!root)
	{
		sprintf_s(buf, 256, "Error, can't allocate %lld bytes for BVH.", tree_size);
		MessageBox(0, buf, "Error", 0);
		return false;
	}

	sprintf_s(buf, 256, "Allocated %d bytes.", tree_size);
	Logger::add(buf);

	// MOVE DATA FROM DYNAMIC TREE TO BUFFER TREE
	BVH::Node * rnode = nodes[0];
	BVH::Node * cnode = rnode;
	BVH::Node * stacknodes[128];
	bool stackleftdone[128];
	bool stackrightdone[128];
	void * stackaddress[128];
	int stackdepth = 0;
	unsigned int nextaddress = 0;
	int clvl = -1;
	bool next_level_exists = true;
	char prefix[128];

	// DO IT LVL BY LVL
	while (next_level_exists)
	{
		clvl++;
		next_level_exists = false;
		cnode = rnode;
		for (int i=0; i<128; i++)
		{	//reset stuff
			stackleftdone[i] = false;
			stackrightdone[i] = false;
			prefix[i] = 0;
		}
		bool leftdone = false;
		bool rightdone = false;
		stackdepth = 0;
		BVH::BVHSplit * dst = (BVH::BVHSplit * )root;

		#ifdef DEBUG_BVH
			sprintf_s(buf, 256, "Trying lvl %d...", clvl);
			Logger::add(buf);
		#endif

		#ifdef DEBUG_BVH
			bool bvhtreestable = checkBVHTree(nodes[0]);
			if (!bvhtreestable)
				Logger::add("bvh tree is not stable");
		#endif

		bool still_on_tree = true;
		while (still_on_tree)
		{
			if (cnode==NULL)
			{
				Logger::add("BVH creation error. cNode is NULL.");
				MessageBox(0, "BVH creation error. cNode is NULL.", "Error", 0);
			}
			if (stackdepth>127)
				MessageBox(0, "BVH stack overload", "Error", 0);
			if (cnode->type == BVH::Node::TYPE_SPLIT)
			{
				if (stackdepth == clvl)
					next_level_exists = true;
				if (leftdone)
				{
					if (rightdone)
					{
						{
							unsigned int offsetright = cnode->right->offsettoroot - cnode->offsettoroot;
							unsigned int offsetleft = cnode->left->offsettoroot - cnode->offsettoroot;
							dst->offset_left = (0x3FFFFFFF & offsetleft);
							dst->offset_right = offsetright;
						}

						stackdepth--;
						if (stackdepth>=0)
						{
							// pop from stack
							prefix[stackdepth] = 0;
							cnode = stacknodes[stackdepth];
							leftdone = stackleftdone[stackdepth];
							rightdone = stackrightdone[stackdepth];
							dst = (BVH::BVHSplit * )stackaddress[stackdepth];
						}
						else
						{
							still_on_tree = false;
						}
					}
					else	// !rightdone
					{
						prefix[stackdepth] = 'R';
						stacknodes[stackdepth] = cnode;
						stackleftdone[stackdepth] = true;
						stackrightdone[stackdepth] = true;
						stackaddress[stackdepth] = dst;
						stackdepth++;
						cnode = cnode->right;
						leftdone = false;
						rightdone = false;
					}
				}
				else	// !leftdone
				{
					if (stackdepth == clvl)
					{
						#ifdef DEBUG_BVH
							Logger::add(prefix);
						#endif
						dst = (BVH::BVHSplit *)(root + nextaddress);
						*dst = *((BVH::BVHSplit *)cnode->ptr);
						cnode->offsettoroot = nextaddress;
						next_level_exists = true;
						nextaddress += split_size;
					}
					else
					{
						dst = (BVH::BVHSplit *)(root+cnode->offsettoroot);
					}

					// push on stack
					prefix[stackdepth] = 'L';
					stacknodes[stackdepth] = cnode;
					stackleftdone[stackdepth] = true;
					stackrightdone[stackdepth] = false;
					stackaddress[stackdepth] = dst;
					stackdepth++;

					// set to left child
					cnode = cnode->left;
					leftdone = false;
					rightdone = false;
				}
			}
			else	// instance
			{
				if (stackdepth == clvl)
				{
					#ifdef DEBUG_BVH
						prefix[stackdepth] = 'I';
						Logger::add(prefix);
						prefix[stackdepth] = 0;
					#endif

					if (cnode->type == BVH::Node::TYPE_INSTANCE)
					{
						BVH::BVHInstance * idst = (BVH::BVHInstance *)(root + nextaddress);
						*idst = *((BVH::BVHInstance *)cnode->ptr);
						idst->offset_left = ((BVH::BVHInstance *)cnode->ptr)->offset_left;//0x80000000;
						idst->offset_right = ((BVH::BVHInstance *)cnode->ptr)->offset_right;
						cnode->offsettoroot = nextaddress;
						nextaddress += inst_size;
					}
					else
					{
						BVH::BVHMBInstance * idst = (BVH::BVHMBInstance *)(root + nextaddress);
						*idst = *((BVH::BVHMBInstance *)cnode->ptr);
						idst->offset_left = ((BVH::BVHMBInstance *)cnode->ptr)->offset_left;//0x80000000;
						idst->offset_right = ((BVH::BVHMBInstance *)cnode->ptr)->offset_right;
						cnode->offsettoroot = nextaddress;
						nextaddress += mb_size;
					}

				}
				
				stackdepth--;
				if (stackdepth>=0)
				{
					// pop from stack
					prefix[stackdepth] = 0;
					cnode = stacknodes[stackdepth];
					leftdone = stackleftdone[stackdepth];
					rightdone = stackrightdone[stackdepth];
					dst = (BVH::BVHSplit *)stackaddress[stackdepth];
				}
				else
				{
					still_on_tree = false;
				}
			}
		}
	}

	sprintf_s(buf, 256, "BVH done. Next address: %lld. Tree size: %lld", nextaddress, tree_size);
	Logger::add(buf);
	Logger::add("Cleaning up BVH dynamic tree");

	//---------------------------------------------------------------------------------------

	// RELEASE DYNAMIC TREE
	stackdepth = 0;
	cnode = rnode;
	for (int i=0; i<128; i++)
		prefix[i] = 0;
	while (stackdepth>=0  &&  cnode)
	{
		if (cnode->type == BVH::Node::TYPE_SPLIT)
		{
			if (cnode->left)
			{
				BVH::Node * nextnode = cnode->left;
				cnode->left = NULL;
				stacknodes[stackdepth] = cnode;
				prefix[stackdepth] = 'L';
				cnode = nextnode;
				stackdepth++;
			}
			else
			{
				if (cnode->right)
				{
					BVH::Node * nextnode = cnode->right;
					cnode->right = NULL;
					stacknodes[stackdepth] = cnode;
					prefix[stackdepth] = 'R';
					cnode = nextnode;
					stackdepth++;

				}
				else
				{
					#ifdef DEBUG_BVH
						Logger::add(prefix);
					#endif
					BVH::BVHSplit * split = (BVH::BVHSplit *)cnode->ptr;
					if (split)
						delete split;
					cnode->ptr = NULL;
					delete cnode;
					stackdepth--;
					if (stackdepth>=0)
					{
						cnode = stacknodes[stackdepth];
						prefix[stackdepth] = 0;
					}
					else
					{
						cnode = NULL;
					}
				}
			}
		}
		else	// instance
		{
			#ifdef DEBUG_BVH
				prefix[stackdepth] = 'I';
				Logger::add(prefix);
				prefix[stackdepth] = 0;
			#endif

			delete cnode;

			stackdepth--;
			if (stackdepth>=0)
			{
				prefix[stackdepth] = 0;
				cnode = stacknodes[stackdepth];
			}
			else
			{
				cnode = NULL;
			}
		}
	}

	Logger::add("Deleting BVH instances.");
	if (instances)
		free(instances);
	instances = NULL;


	rnode = NULL;
	#ifdef DEBUG_BVH
		Logger::add("BVH dynamic tree deleted.");
	#endif

	return true;
}

//---------------------------------------------------------------------

float BVH::intersectForTrisNonSSE(const float sample_time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instID, int &tri, float &u, float &v)
{
	// sse still
	struct StackNear
	{
		float left;
		float right;
	};

	unsigned long long stackTricky[128];
	StackNear stackNear[128];

	float res = BIGFLOAT;
	int stackDepth = 0;
	unsigned long long curOffset = 0;
	Vector3d invDir;
	invDir.x = 1.0f / direction.x;
	invDir.y = 1.0f / direction.y;
	invDir.z = 1.0f / direction.z;

	__m128 sseOrigin = _mm_setr_ps(1.0f, origin.x, origin.y, origin.z);
	__m128 ssePerDir = _mm_setr_ps(1.0f, invDir.x, invDir.y, invDir.z);

	#define ACTION_ST_DO_NOTHING  0x100000000000000
	#define ACTION_ST_CHECK_LEFT  0x200000000000000
	#define ACTION_ST_CHECK_RIGHT 0x400000000000000
	#define ACTION_ST_CHECK_BOTH  0x800000000000000
	unsigned long long curActionT = ACTION_ST_CHECK_BOTH;

	while (stackDepth>=0)
	{
		unsigned char * addr = root + curOffset;
		BVH::BVHSplit * split = (BVH::BVHSplit *)addr;

		if (split->offset_left & 0x80000000)	// instance
		{
			BVH::BVHInstance * instance = (BVH::BVHInstance*)addr;
			BVH::BVHMBInstance * mbinstance = NULL;
			bool mb_now = false;
			if (split->offset_left & 0x40000000)	// motion blur
			{
				mbinstance = (BVH::BVHMBInstance*)addr;
				mb_now = true;
			}

			Matrix4d inst_matr_inv = instance->matr_inv.toFullMatrix();

			BSPTree* bsp = meshes[instance->offset_right];

			Matrix4d m_mb, m_mb_offset;
			m_mb.setIdentity();
			if (mb_now)	// motion blur
			{
				if (mbinstance->mb_angle_vel!=0.0f)
					m_mb.setRotation(Vector3d(mbinstance->mb_angle_x, mbinstance->mb_angle_y, mbinstance->mb_angle_z), mbinstance->mb_angle_vel*sample_time*-1);

				Vector3d tr = Vector3d(mbinstance->mb_vel_x, mbinstance->mb_vel_y, mbinstance->mb_vel_z); 
				tr *= sample_time * -1;
				tr = m_mb*tr;
				m_mb.M03 = tr.x;
				m_mb.M13 = tr.y;
				m_mb.M23 = tr.z;
			}

			#ifdef REVERSE_MATRIX_MULT
				Point3d origin1 = inst_matr_inv * origin;
				Vector3d direction1 = inst_matr_inv * direction; 
				if (mb_now)
				{
					origin1 = mbinstance->matr_offset_inv.toFullMatrix() * (m_mb * origin1);
					direction1 = mbinstance->matr_offset_inv.toFullMatrix() * (m_mb * direction1);
				}
			#else
				Point3d origin1 = instance->matr_inv.toFullMatrix() * (m_mb * origin);		// orig
				Vector3d direction1 = instance->matr_inv.toFullMatrix() * (m_mb * direction);		// orig
			#endif

			float maxDist1 = maxDist;
			int tri1;
			float u1,v1;

			float d = bsp->intersectForTris(origin1, direction1, maxDist1, tri1, u1, v1);

			if (d > 0  &&  res>d)
			{
				res = d;
				tri = tri1;
				u = u1;
				v = v1;

				if (mb_now)	// matrix for motion blur
				{
					if (mbinstance->mb_angle_vel!=0.0f)
						m_mb.setRotation(Vector3d(mbinstance->mb_angle_x, mbinstance->mb_angle_y, mbinstance->mb_angle_z), mbinstance->mb_angle_vel*sample_time);
					m_mb.M03 = mbinstance->mb_vel_x * sample_time;
					m_mb.M13 = mbinstance->mb_vel_y * sample_time;
					m_mb.M23 = mbinstance->mb_vel_z * sample_time;
				}

				#ifdef REVERSE_MATRIX_MULT
					instMatrix = instance->matr_tr.toFullMatrix() * m_mb;
					if (mb_now)
					{
						instMatrix = instMatrix * mbinstance->matr_offset.toFullMatrix();
					}
				#else
					instMatrix = m_mb * instance->matr_tr.toFullMatrix();	// orig
				#endif

				instID = split->offset_left & 0x3FFFFFFF;
			}

			stackDepth--;
			curOffset = stackTricky[stackDepth] & 0xFFFFFFFFFFFFFF;
			curActionT = ((stackTricky[stackDepth]) & 0xFF00000000000000);
		}
		else	// split
		{
			if (curActionT == ACTION_ST_CHECK_BOTH)
			{
				BVH::BVHSplit * boxLeft = (BVH::BVHSplit *)(addr+split->offset_left);
				BVH::BVHSplit * boxRight = (BVH::BVHSplit *)(addr+split->offset_right);

				 // SSE TEST
				__m128 lMinSSE = _mm_mul_ps(_mm_sub_ps(boxLeft->sseMin,  sseOrigin), ssePerDir);
				__m128 lMaxSSE = _mm_mul_ps(_mm_sub_ps(boxLeft->sseMax,  sseOrigin), ssePerDir);
				__m128 rMinSSE = _mm_mul_ps(_mm_sub_ps(boxRight->sseMin, sseOrigin), ssePerDir);
				__m128 rMaxSSE = _mm_mul_ps(_mm_sub_ps(boxRight->sseMax, sseOrigin), ssePerDir);
				__m128 lFar  = _mm_max_ps(lMinSSE, lMaxSSE);
				__m128 lNear = _mm_min_ps(lMinSSE, lMaxSSE);
				lFar = _mm_sub_ps(_mm_setzero_ps(), lFar);
				__m128 rFar  = _mm_max_ps(rMinSSE, rMaxSSE);
				__m128 rNear = _mm_min_ps(rMinSSE, rMaxSSE);
				rFar = _mm_sub_ps(_mm_setzero_ps(), rFar);

				TransposeSSE(lFar, rFar, lNear, rNear);
				__m128 maxSSE = _mm_max_ps(_mm_max_ps(lNear, rFar), rNear);
				__m128 tmpSSE = _mm_movelh_ps(maxSSE, maxSSE);
				__m128 mmaxSSE = _mm_add_ps(maxSSE, tmpSSE);		// -2lF, -2rF, lN-lF, rN-rF
				mmaxSSE = _mm_cmple_ps(mmaxSSE, _mm_setzero_ps());
				int mask = _mm_movemask_ps(mmaxSSE);
				mask = mask&(mask>>2);
				bool leftHit = mask&0x1;
				bool rightHit = mask&0x2;
				float leftNear  =  maxSSE.m128_f32[2];
				float rightNear =  maxSSE.m128_f32[3];

				if (leftHit)
				{
					if (rightHit)
					{
						stackNear[stackDepth].left = leftNear;
						stackNear[stackDepth].right = rightNear;
						if (leftNear < rightNear)
						{
							stackTricky[stackDepth] = (curOffset & 0xFFFFFFFFFFFFFF) | ACTION_ST_CHECK_RIGHT;
							stackDepth++;
							curOffset = curOffset + split->offset_left;
							curActionT = ACTION_ST_CHECK_BOTH;
						}
						else
						{
							stackTricky[stackDepth] = (curOffset & 0xFFFFFFFFFFFFFF) | ACTION_ST_CHECK_LEFT;
							stackDepth++;
							curOffset = curOffset + split->offset_right;
							curActionT = ACTION_ST_CHECK_BOTH;
						}
					}
					else
					{
						stackTricky[stackDepth] = (curOffset & 0xFFFFFFFFFFFFFF) | ACTION_ST_DO_NOTHING;
						stackDepth++;
						curOffset = curOffset + split->offset_left;
						curActionT = ACTION_ST_CHECK_BOTH;
					}
				}
				else
				{
					if (rightHit)
					{
						stackTricky[stackDepth] = (curOffset & 0xFFFFFFFFFFFFFF) | ACTION_ST_DO_NOTHING;
						stackDepth++;
						curOffset = curOffset + split->offset_right;
						curActionT = ACTION_ST_CHECK_BOTH;
					}
					else
					{
						stackDepth--;
						curOffset = stackTricky[stackDepth] & 0xFFFFFFFFFFFFFF;
						curActionT = ((stackTricky[stackDepth]) & 0xFF00000000000000);
				 	}
				}
				continue;
			} // end cur action check both


			if (curActionT == ACTION_ST_CHECK_LEFT)
			{
				if (res<stackNear[stackDepth].left)
				{
					stackDepth--;
					curOffset = stackTricky[stackDepth] & 0xFFFFFFFFFFFFFF;
					curActionT = ((stackTricky[stackDepth]) & 0xFF00000000000000);
					continue;
				}
				else
				{
					stackTricky[stackDepth] = (curOffset & 0xFFFFFFFFFFFFFF) | ACTION_ST_DO_NOTHING;
					stackDepth++;
					curOffset = curOffset + split->offset_left;
					curActionT = ACTION_ST_CHECK_BOTH;
					continue;
				}
			}	// end cur action check left

			if (curActionT == ACTION_ST_CHECK_RIGHT)
			{
				if (res<stackNear[stackDepth].right)
				{
					stackDepth--;
					curOffset = stackTricky[stackDepth] & 0xFFFFFFFFFFFFFF;
					curActionT = ((stackTricky[stackDepth]) & 0xFF00000000000000);
					continue;
				}
				else
				{
					stackTricky[stackDepth] = (curOffset & 0xFFFFFFFFFFFFFF) | ACTION_ST_DO_NOTHING;
					stackDepth++;
					curOffset = curOffset + split->offset_right;
					curActionT = ACTION_ST_CHECK_BOTH;
					continue;
				}
			}	// end cur action check right

			if (curActionT == ACTION_ST_DO_NOTHING)
			{
				stackDepth--;
				curOffset = stackTricky[stackDepth] & 0xFFFFFFFFFFFFFF;
				curActionT = ((stackTricky[stackDepth]) & 0xFF00000000000000);
				continue;
			}	// end cur action do nothing

		}	// end split
	}

	if (res==BIGFLOAT)
		res = -1;
	return res;
}

//---------------------------------------------------------------------

float BVH::intersectForTris(const float time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instID, int &tri, float &u, float &v)
{
	float res = BIGFLOAT;
	int stackDepth = 0;
	unsigned long long stackOffset[128];
	unsigned long long curOffset = 0;
	float stackNearLeft[128];
	float stackNearRight[128];

	char stackActions[128];
	#define ACTION_DO_NOTHING 1
	#define ACTION_CHECK_LEFT 2
	#define ACTION_CHECK_RIGHT 4
	#define ACTION_CHECK_BOTH 8
	int curAction = ACTION_CHECK_BOTH;

	__m128 sseOrigin, sseDir, sseInvDir, sseAllMinus, sseAllZero;
	sseOrigin.m128_f32[0] = 1.0f;
	sseOrigin.m128_f32[1] = origin.x;
	sseOrigin.m128_f32[2] = origin.y;
	sseOrigin.m128_f32[3] = origin.z;
	sseDir.m128_f32[0] = 1.0f;
	sseDir.m128_f32[1] = direction.x;
	sseDir.m128_f32[2] = direction.y;
	sseDir.m128_f32[3] = direction.z;
	sseInvDir.m128_f32[0] = 1.0f;
	sseInvDir.m128_f32[1] = 1.0f/direction.x;
	sseInvDir.m128_f32[2] = 1.0f/direction.y;
	sseInvDir.m128_f32[3] = 1.0f/direction.z;
	sseAllMinus = _mm_set1_ps(-1.0f);
	sseAllZero = _mm_set1_ps(0.0f);

	while (stackDepth>=0)
	{
		unsigned char * addr = root + curOffset;
		BVH::BVHSplit * split = (BVH::BVHSplit *)addr;

		if (split->offset_left & 0x80000000)	// instance
		{
			BVH::BVHInstance * instance = (BVH::BVHInstance*)addr;

			BSPTree* bsp = meshes[instance->offset_right];

			Point3d origin1 = origin;
			Vector3d direction1 = direction; 
			float maxDist1 = maxDist;
			int tri1;
			float u1,v1;

			float d = bsp->intersectForTris(origin1, direction1, maxDist1, tri1, u1, v1);
			if (d > 0  &&  res>d)
			{
				res = d;
				tri = tri1;
				u = u1;
				v = v1;
			}

			stackDepth--;
			curOffset = stackOffset[stackDepth];
			curAction = stackActions[stackDepth];
		}
		else	// split
		{
			if (curAction == ACTION_CHECK_BOTH)
			{
				BVH::BVHSplit * boxLeft = (BVH::BVHSplit *)(addr+split->offset_left);
				BVH::BVHSplit * boxRight = (BVH::BVHSplit *)(addr+split->offset_right);

				__m128 leftMins = boxLeft->sseMin;
				__m128 leftMaxs = boxLeft->sseMax;
				__m128 rightMins = boxRight->sseMin;
				__m128 rightMaxs = boxRight->sseMax;
				__m128 leftminminusorg  = _mm_sub_ps(leftMins,  sseOrigin);
				__m128 leftmaxminusorg  = _mm_sub_ps(leftMaxs,  sseOrigin);
				__m128 rightminminusorg = _mm_sub_ps(rightMins, sseOrigin);
				__m128 rightmaxminusorg = _mm_sub_ps(rightMaxs, sseOrigin);
				__m128 leftV1  = _mm_mul_ps(leftminminusorg,  sseInvDir);
				__m128 leftV2  = _mm_mul_ps(leftmaxminusorg,  sseInvDir);
				__m128 rightV1 = _mm_mul_ps(rightminminusorg, sseInvDir);
				__m128 rightV2 = _mm_mul_ps(rightmaxminusorg, sseInvDir);
				__m128 leftFars   = _mm_max_ps(leftV1,  leftV2);
				__m128 leftNears  = _mm_min_ps(leftV1,  leftV2);
				__m128 rightFars  = _mm_max_ps(rightV1, rightV2);
				__m128 rightNears = _mm_min_ps(rightV1, rightV2);
				__m128 leftNearsMinus  = _mm_mul_ps(leftNears,  sseAllMinus);
				__m128 rightNearsMinus = _mm_mul_ps(rightNears, sseAllMinus);
				TransposeSSE(leftFars, leftNearsMinus, rightFars, rightNearsMinus);
				__m128 nearFar = _mm_min_ps(_mm_min_ps(leftNearsMinus, rightFars), rightNearsMinus);
				__m128 sseA = _mm_unpacklo_ps(nearFar, nearFar);		// [ lF,  lF, -lN, -lN]
				__m128 sseB = _mm_unpackhi_ps(nearFar, nearFar);		// [ rF,  rF, -rN, -rN]
				__m128 sseC = _mm_movelh_ps(sseA, sseB);				// [ lF,  lF,  rF,  rF]
				__m128 sseD = _mm_movehl_ps(sseB, sseA);				// [-lN, -lN, -rN, -rN]
				__m128 sseE = _mm_add_ps(sseC, sseD);					// [lF-lN, lF-lN, rF-rN, rF-rN]
				__m128 sseZero = _mm_setzero_ps();						// [0 0 0 0]
				__m128 sseE2 = _mm_movehl_ps(sseE, sseE);				// [rF-rN, rF-rN, rF-rN, rF-rN]
				unsigned int leftHit = _mm_ucomilt_ss(sseZero, sseC) &  _mm_ucomilt_ss(sseZero, sseE);
				unsigned int rightHit = _mm_ucomilt_ss(sseZero, sseB) &  _mm_ucomilt_ss(sseZero, sseE2);

				__m128 sseG = _mm_cmpge_ps(sseC, sseZero);				// MASK: Far >= 0 
				__m128 sseH = _mm_cmpge_ps(sseE, sseZero);				// MASK: Far >= Near
				__m128 sseBoxHit = _mm_and_ps(sseG, sseH);				// AND above 2
				__m128 sseJ = _mm_mul_ps(sseD, sseAllMinus);			// [ lN,  lN,  rN,  rN]
				__m128 sseK = _mm_movelh_ps(sseJ, sseJ);				// [ lN,  lN,  lN,  lN]
				__m128 sseL = _mm_movehl_ps(sseJ, sseJ);				// [ rN,  rN,  rN,  rN]
				unsigned int leftNearer = _mm_ucomilt_ss(sseK, sseL);

				unsigned int mask1 = leftHit * rightHit * leftNearer;
				unsigned int mask2 = leftHit * rightHit * (1-leftNearer);
				unsigned int mask3 = leftHit * (1-rightHit);
				unsigned int mask4 = (1-leftHit) * rightHit;
				unsigned int mask5 = (1-leftHit) * (1-rightHit);
				if (mask5)
				{
					stackDepth--;
					curOffset = stackOffset[stackDepth];
					curAction = stackActions[stackDepth];
				}
				else
				{
					stackNearLeft[stackDepth] = -nearFar.m128_f32[1];
					stackNearRight[stackDepth] = -nearFar.m128_f32[3];

					stackOffset[stackDepth] = curOffset;
					stackActions[stackDepth] = ACTION_CHECK_RIGHT*mask1 + ACTION_CHECK_LEFT*mask2 + ACTION_DO_NOTHING*(mask3+mask4);
					stackDepth++;
					curOffset = curOffset + (split->offset_left*(mask1+mask3) + split->offset_right*(mask2+mask4));
					curAction = ACTION_CHECK_BOTH;
				}

				continue;
			} // end cur action check both

			if (curAction == ACTION_CHECK_LEFT)
			{
				if (res<stackNearLeft[stackDepth])
				{
					stackDepth--;
					curOffset = stackOffset[stackDepth];
					curAction = stackActions[stackDepth];
					continue;
				}
				else
				{
					stackOffset[stackDepth] = curOffset;
					stackActions[stackDepth] = ACTION_DO_NOTHING;
					stackDepth++;
					curOffset = curOffset + split->offset_left;
					curAction = ACTION_CHECK_BOTH;
					continue;
				}
			}	// end cur action check left

			if (curAction == ACTION_CHECK_RIGHT)
			{
				if (res<stackNearRight[stackDepth])
				{
					stackDepth--;
					curOffset = stackOffset[stackDepth];
					curAction = stackActions[stackDepth];
					continue;
				}
				else
				{
					stackOffset[stackDepth] = curOffset;
					stackActions[stackDepth] = ACTION_DO_NOTHING;
					stackDepth++;
					curOffset = curOffset + split->offset_right;
					curAction = ACTION_CHECK_BOTH;
					continue;
				}
			}	// end cur action check right

			if (curAction == ACTION_DO_NOTHING)
			{
				stackDepth--;
				curOffset = stackOffset[stackDepth];
				curAction = stackActions[stackDepth];
				continue;
			}	// end cur action do nothing

		}	// end split

	}

	if (res==BIGFLOAT)
		res = -1;
	return res;
}

//---------------------------------------------------------------------

inline void TransposeSSE(__m128 &A, __m128 &B, __m128 &C, __m128 &D)
{
	__m128 unp1 = _mm_unpacklo_ps(C, D);
	__m128 unp2 = _mm_unpackhi_ps(C, D);
	__m128 unp3 = _mm_unpacklo_ps(A, B);
	__m128 unp4 = _mm_unpackhi_ps(A, B);
	A = _mm_movelh_ps(unp3, unp1);
	B = _mm_movehl_ps(unp1, unp3);
	C = _mm_movelh_ps(unp4, unp2);
	D = _mm_movehl_ps(unp2, unp4);
}

//---------------------------------------------------------------------

bool checkBVHTree(BVH::Node * t)
{
	if (!t)
	{
		Logger::add("BVH split is null.");
		return false;
	}
	if (t->type == BVH::Node::TYPE_INSTANCE)
		return true;
	if (!t->left)
	{
		char buf[64];
		sprintf_s(buf, 64, "BVH split %llx has null left split.", (INT_PTR)t);
		Logger::add(buf);
		return false;
	}
	if (!t->right)
	{
		char buf[64];
		sprintf_s(buf, 64, "BVH split %llx has null right split.", (INT_PTR)t);
		Logger::add(buf);
		return false;
	}
	if (!checkBVHTree(t->left))
		return false;
	if (!checkBVHTree(t->right))
		return false;
	return true;
}

//---------------------------------------------------------------------

bool BVH::deleteAllStuff()
{
	for (int i=0; i<meshes.objCount; i++)
	{
		BSPTree * bsp = meshes[i];
		bsp->deleteAllStuff();
		delete(bsp);
	}
	meshes.trimLastObjects(meshes.objCount);
	meshes.createArray();

	_aligned_free(root);
	root = NULL;

	return true;
}

//---------------------------------------------------------------------

