#ifndef __METROPOLIS_LT_H
#define __METROPOLIS_LT_H

// old version of metropolis light transport - does not support may of nox features 

#include "raytracer.h"
#define MLT_MAX_VERTICES 20
#define MLT_MAX_REJ 1000

#define MLT_MUT_BIDIR 0
#define MLT_MUT_LENS 1
#define MLT_MUT_PERT_LENS 2
#define MLT_MUT_PERT_CAUSTIC 3
#define MLT_MUT_PERT_CHAIN 4

class MLTVertex;
class Metropolis;
class MltSeed;

//--------------------------------------------------------------------------------------------------------

class MLTVertex
{
public:
	Color4 atten;			// attenuation from last vertice due to opacity and fake glass between
	Point3d pos;			// position
	Vector3d normal;		// normal at pos
	Vector3d inDir;			// direction to previous vertex
	Vector3d outDir;		// direction to next vertex
	float inCosine;			// inDir dot normal
	float outCosine;		// outDir dot normal
	Triangle * tri;			// pointer to triangle
	float u,v;				// uv coordinates of pos on triangle
	Color4 inRadiance;		// radiance incoming from last vertex - for faster calculation

	MaterialNox * mat;		// random chosen material
	MatLayer * mlay;		// random chosen material layer
	float rough;			// roughness
	float pOut;				// probability of outgoing energy (since root)
	bool dirac;
	Color4 brdf;			// brdf for (in, pos, out)
	int instanceID;
	Matrix4d inst_mat;

	void fillHitData(HitData &hd, bool forward);
	bool fetchRoughness();
};

typedef MLTVertex *PMLTVertex;

//--------------------------------------------------------------------------------------------------------

class MltSeed 
{
	friend Metropolis;
public:
	MLTVertex ePath[MLT_MAX_VERTICES];
	MLTVertex lPath[MLT_MAX_VERTICES];
	int numL, numE;
	int lightTriID;
	int lightmeshID;
	Camera * cam;
	bool useSun;
	bool useSky;
	
private:
	bool validPath;
	int numTries;
	SceneStatic * ss;
	float rrprob;

public:
	static int errCntNonVisib;
	static int errCntPxBlack;
	static int errCntSpecAndNoInt;
	static int errCntNoIntNoMLay;
	static int errCntCamSrcInd;
	static int errCntPdf0;
	static int errCntPathBlack;
	static int errCntPathInfNaN;
	static int errCntRndLight;
	float weight;
	Color4 connAtt;
	float sample_time;


	bool createSeed(float camx, float camy, int numOfTries);
	bool tryToCreate(float cx, float cy);
	bool isValid()			{	return validPath;	}
	int getNumTries()		{	return numTries;	}
	bool sendNextVertices(bool isItLight);
	bool joinPaths(bool withVisibTest=true);
	bool randomNewLightPoint(Triangle ** tri, MaterialNox ** mat, MatLayer ** mlay, Matrix4d &instMat, int &instID, float &u, float &v, float &pdf, int &triID);
	float evalWeight();
	float evalWeightNoWeightContribution();
	float evalSunSkyWeight();

	MltSeed(SceneStatic * scst, Camera * tcam);
	MltSeed();
	~MltSeed();

};

//--------------------------------------------------------------------------------------------------------

class Metropolis
{
private:
	int numLP;
	int numEP;
	int numCLP;
	int numCEP;

	bool sourceSun;
	bool sourceSky;
	bool cSourceSun;
	bool cSourceSky;

	SceneStatic * ss;
	bool * running;
	Camera * cam;
	float pX, pY, pCX, pCY;					// x,y coords of rendered image
public:
	int threadIDNumber;
	static bool discardDirect;
	static float plR1, plR2;				// min and max dist of lens perturbation
	static float pcR1, pcR2;				// min and max theta change of caustic perturbation
	static float pch1R1, pch1R2;			// min and max dist of chain perturbation on lens
	static float pch2R1, pch2R2;			// min and max theta change of chain perturbation on diff reflection
private:
	float maxCausticRough;					// maximum roughness for caustic perturbation
	double sumweights;
	long int sumprobes;
	double lweights[MLT_MAX_VERTICES*2];
	long long lprobes[MLT_MAX_VERTICES*2];	


	MLTVertex ePath[MLT_MAX_VERTICES];
	MLTVertex lPath[MLT_MAX_VERTICES];

	MLTVertex eCandidate[MLT_MAX_VERTICES];
	MLTVertex lCandidate[MLT_MAX_VERTICES];
	int chainPos[MLT_MAX_VERTICES];
	int chainRefs;

	int kd, kde, kdl, ka, kae, kal;
	int lensMutNum;
	int causticPertStart, causticPertStop;
	int bm_start, bm_stop;
	int dm_del_eye, dm_del_light;
	float sample_time;
	float rndLightPdf;
	int sysRej;								// number of continuous rejections
	float weightBias;
	long long sumProbes;


	bool createNextCandidateVertices(const Vector3d *dir, const bool &isItLight, const int &steps, const bool &stopRandom);
	bool createNextVertices(const Point3d &orig, const Vector3d &dir, const bool &isItLight, const int &steps, const bool &stopRandom);
	void recalculateContributions(int lStart, int lEnd, int eStart, int eEnd);
	void recalculateWeights(int lStart, int lEnd, int eStart, int eEnd);
	float tentativeTransitionForBidir(bool forward=true);
	float tentativeTransitionForDiffuse(bool forward=true);
	float tentativeTransitionForLens(bool forward=true);
	float tentativeTransitionForLensSunsky(bool forward=true);
	float tentativeTransitionForPertLens(bool forward=true);
	float tentativeTransitionForPertLens2(bool forward=true);
	float tentativeTransitionForPertCaustic(bool forward=true);
	float tentativeTransitionForPertChain(bool forward=true);

	Color4 calculateContribution();
	bool mutateRandom();
	bool mutateBidir();
	bool mutateDiffuse();
	bool mutateLens();
	bool perturbateLens();
	bool perturbateLens2();
	bool perturbateCaustic();
	bool perturbateChain();
	int randomMutationType();



	bool visibilityTest();
	bool visibilityTest(MLTVertex * v1, MLTVertex * v2);
	Color4 visibilityTestFakeGlassAndOpacity(MLTVertex * v1, MLTVertex * v2);

	bool copySeedToCandidate(MltSeed * seed);

	float  w [MLT_MAX_VERTICES][MLT_MAX_VERTICES];			// weights						[l][e]
	Color4 uC[MLT_MAX_VERTICES][MLT_MAX_VERTICES];			// unweighted contribution		[l][e]
	Color4 C [MLT_MAX_VERTICES][MLT_MAX_VERTICES];			// contributions   [lightpath][eyepath]
	Color4 alphaL[MLT_MAX_VERTICES];
	Color4 alphaE[MLT_MAX_VERTICES];
	Color4 alphaCL[MLT_MAX_VERTICES];
	Color4 alphaCE[MLT_MAX_VERTICES];
	float pL[MLT_MAX_VERTICES];
	float pE[MLT_MAX_VERTICES];
	bool testMutateRandomWholeNewPaths();
	int lCnt[MLT_MAX_VERTICES];
	int eCnt[MLT_MAX_VERTICES];
	int ndel[MLT_MAX_VERTICES];
	int nadd[MLT_MAX_VERTICES];
	int nEdg[MLT_MAX_VERTICES*2];
	int KAAL[MLT_MAX_VERTICES];
	int KAAE[MLT_MAX_VERTICES];
	int KDDL[MLT_MAX_VERTICES];
	int KDDE[MLT_MAX_VERTICES];
	Color4 connectionAttenuation;
	Color4 connectionAttenuationCandidate;
	int accCnt;
	int rejCnt;
	int rejNonVis;
	int cInd;		// lens perturbation connection index
	int csInd;		// caustic perturbation start   ----> causticPertStart
	bool tempwasspecular;
	
	
	void logWholePathDebug(bool candidate=false);
	bool isPathValid(bool forward);
	void printHeckbertPath(bool forward);
	char * getHeckbertPath(bool forward);


	PMLTVertex mutPath[2*MLT_MAX_VERTICES];
	float  tG[2*MLT_MAX_VERTICES];
	float  pLE[2*MLT_MAX_VERTICES];
	float  pEL[2*MLT_MAX_VERTICES];
	Color4 brdf[2*MLT_MAX_VERTICES];
	int lTriID, clTriID;

	Color4 evalLocalIllumStandard(Point3d orig, Vector3d dir, float dx, float dy);


	bool randomNewLightPoint(Triangle ** tri, MaterialNox ** mat, MatLayer ** mlay, int &instID, Matrix4d &instMatr, float &u, float &v, float &pdf);



public:
	bool createInitialPaths();// not used
	bool initializePaths();

	Color4 runMutations();
	static int prMutBidir;
	static int prMutPertLens;
	static int prMutLens;
	static int prMutPertCaust;
	static int prMutPertChain;


	Metropolis(SceneStatic * st, Camera * camera, bool * working);
	~Metropolis();
};

//--------------------------------------------------------------------------------------------------------

#endif

