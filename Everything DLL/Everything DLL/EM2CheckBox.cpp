#include <windows.h>
#include <stdio.h>
#include "EM2Controls.h"

extern HMODULE hDllModule;

//----------------------------------------------------------------------

EM2CheckBox::EM2CheckBox(HWND hWnd)
{
	selected = false;
	mouseOver = false;
	useCursors = true;
	hwnd = hWnd;
	#ifdef GUI2_DRAW_TEXT_SHADOWS
		drawTextShadow = true;
	#else
		drawTextShadow = false;
	#endif

	colBackground  = NGCOL_TEXT_BACKGROUND;
	colText = NGCOL_LIGHT_GRAY;
	colTextSelected = NGCOL_LIGHT_GRAY;
	colTextDisabled = NGCOL_TEXT_DISABLED;
	colTextMouseOver = RGB(0xd5,0xa2,0x08);
	colCheckDisabled = RGB(0x40, 0x40, 0x40);
	colCheckSelected = RGB(0xd5,0xa2,0x08);
	colCheckUnselected = RGB(0x7f,0x7b,0x79);
	colCheckMouseOver = NGCOL_YELLOW;
	colTextShadow = RGB(15,15,15);
	colCheckShadow = RGB(25,25,25);

	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfont = true;
	bgImage = 0;
	bgShiftX = bgShiftY = 0;

	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
		captionSize = 0;

}

//----------------------------------------------------------------------

EM2CheckBox::~EM2CheckBox()
{
	if  (hFont  &&  autodelfont)
		DeleteObject(hFont);
	hFont = 0;
	if (caption)
		free(caption);
	caption = NULL;
}

//----------------------------------------------------------------------

void InitEM2CheckBox()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2CheckBox";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2CheckBoxProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2CheckBox *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2CheckBox * GetEM2CheckBoxInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2CheckBox * emcb = (EM2CheckBox *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2CheckBox * emcb = (EM2CheckBox *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcb;
}

//----------------------------------------------------------------------

void SetEM2CheckBoxInstance(HWND hwnd, EM2CheckBox *emcb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcb);
	#endif
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2CheckBoxProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2CheckBox * emc = GetEM2CheckBoxInstance(hwnd);

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2CheckBox * emcb1 = new EM2CheckBox(hwnd);
				SetEM2CheckBoxInstance(hwnd, (EM2CheckBox *)emcb1);			
			}
		break;
		case WM_NCDESTROY:
			{
				EM2CheckBox * emcb1;
				emcb1 = GetEM2CheckBoxInstance(hwnd);
				delete emcb1;
				SetEM2CheckBoxInstance(hwnd, 0);
			}
		break;
		case BM_GETCHECK:
			{
				if (emc->selected)
					return BST_CHECKED;
				else
					return BST_UNCHECKED;
			}
			break;
		case WM_PAINT:
			{
				if (!emc) 
					break;

				RECT rect;
				HDC hdc;
				PAINTSTRUCT ps;

				GetClientRect(hwnd, &rect);
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);

				if (emc->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emc->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emc->bgShiftX, emc->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emc->colBackground));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emc->colBackground));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, OPAQUE);
				}


				COLORREF checkCol = RGB(255,120,120);
				if (disabled)
					checkCol = emc->colCheckDisabled;
				else
					if (emc->mouseOver)
						checkCol = 	emc->colCheckMouseOver;
					else
						if (emc->selected)
							checkCol = emc->colCheckSelected;
						else
							checkCol = emc->colCheckUnselected;

				int vc = rect.bottom/2;

				HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, checkCol));
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, (HBRUSH)GetStockObject(HOLLOW_BRUSH));
				Rectangle(hdc, 1, vc-4, 9, vc+4);
				if (emc->drawTextShadow)
				{
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emc->colCheckShadow)));
					Rectangle(hdc, 0, vc-5, 10, vc+5);
					Rectangle(hdc, 2, vc-3, 8, vc+3);
				}

				DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, checkCol)));
				DeleteObject(SelectObject(hdc, CreateSolidBrush(checkCol)));
				if (emc->selected)
					Rectangle(hdc, 3, vc-2, 7, vc+2);
				DeleteObject(SelectObject(hdc, oldBrush));
				DeleteObject(SelectObject(hdc, hOldPen));
		

				HFONT hOldFont = (HFONT)SelectObject(hdc, emc->getFont());
				COLORREF txtcol = disabled ? emc->colTextDisabled : (emc->mouseOver ? emc->colTextMouseOver : (emc->selected ? emc->colTextSelected : emc->colText));
				SetBkColor(hdc, emc->colBackground);
	
				SIZE sz;
				GetTextExtentPoint32(hdc, emc->caption, emc->captionSize, &sz);
				RECT trect = {    16,    vc-(sz.cy+1)/2,    sz.cx+16,    vc+sz.cy-(sz.cy+1)/2};
			

				if (emc->drawTextShadow)
				{
					SetTextColor(hdc, emc->colTextShadow);
					ExtTextOut(hdc,   15,    vc-(sz.cy+1)/2,      ETO_CLIPPED, &trect, emc->caption, emc->captionSize, 0);
					ExtTextOut(hdc,   17,    vc-(sz.cy+1)/2,      ETO_CLIPPED, &trect, emc->caption, emc->captionSize, 0);
					ExtTextOut(hdc,   16,    vc-(sz.cy+1)/2+1,    ETO_CLIPPED, &trect, emc->caption, emc->captionSize, 0);
					ExtTextOut(hdc,   16,    vc-(sz.cy+1)/2-1,    ETO_CLIPPED, &trect, emc->caption, emc->captionSize, 0);
				}


				SetTextColor(hdc, txtcol);
				ExtTextOut(hdc,   16,    vc-(sz.cy+1)/2,    ETO_CLIPPED, &trect, emc->caption, emc->captionSize, 0);

				SelectObject(hdc, hOldFont);

				emc->clickRect.left = 0;
				emc->clickRect.top = rect.top;
				emc->clickRect.right = min(sz.cx+16, rect.right);
				emc->clickRect.bottom = rect.bottom;

				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				POINT pt = {(short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				if (PtInRect(&(emc->clickRect), pt))
				{
					emc->selected = !emc->selected;
					RECT rect;
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), BN_CLICKED), 
							(LPARAM)hwnd);
				}
			}
			break;
		case WM_MOUSEMOVE:
			{
				if (!GetCapture())
					SetCapture(hwnd);
				POINT p = { (short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				if (PtInRect(&(emc->clickRect), p))
				{
					if (!emc->mouseOver)
					{
						if (emc->useCursors)
							SetCursor(LoadCursor (NULL, IDC_HAND));
						InvalidateRect(hwnd, NULL, false);
					}
					emc->mouseOver = true;
				}
				else
				{
					if (emc->mouseOver)
					{
						if (emc->useCursors)
							SetCursor(LoadCursor (NULL, IDC_ARROW));
						InvalidateRect(hwnd, NULL, false);
					}
					emc->mouseOver = false;
				}

				RECT rect;
				GetClientRect(hwnd , &rect);
				if (!PtInRect(&rect, p))
					ReleaseCapture();
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emc->mouseOver = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

bool EM2CheckBox::setFont(HFONT hNewFont, bool autodel)
{
	if (autodelfont)
		DeleteObject(hFont);
	hFont = hNewFont;
	autodelfont = autodel;
	return true;
}

//----------------------------------------------------------------------

HFONT EM2CheckBox::getFont()
{
	return hFont;
}

//----------------------------------------------------------------------


