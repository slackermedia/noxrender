#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "valuesMinMax.h"
#include <math.h>
#include "dialogs.h"

extern HMODULE hDllModule;

void setPositionsOnStartEnvironmentTab(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcEnvironment(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnStartEnvironmentTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = NGCOL_LIGHT_GRAY;

				// GROUPBARS
				EM2GroupBar * emgr_longlat = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GR_LONG_LAT));
				emgr_longlat->bgImage = rMain->hBmpBg;
				emgr_longlat->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_datetime = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GR_DATE_AND_TIME));
				emgr_datetime->bgImage = rMain->hBmpBg;
				emgr_datetime->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_skysystem = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GR_SKY_SYSTEM));
				emgr_skysystem->bgImage = rMain->hBmpBg;
				emgr_skysystem->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_atm = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GR_ATMOSPHERE));
				emgr_atm->bgImage = rMain->hBmpBg;
				emgr_atm->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_sun = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GR_SUN));
				emgr_sun->bgImage = rMain->hBmpBg;
				emgr_sun->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_envmap = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GR_ENV_MAP));
				emgr_envmap->bgImage = rMain->hBmpBg;
				emgr_envmap->setFont(rMain->fonts->em2groupbar, false);


				// LONGITUDE LATITUDE
				EM2Text * emt_long = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_LONGITUDE));
				emt_long->bgImage = rMain->hBmpBg;
				emt_long->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_lat = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_LATITUDE));
				emt_lat->bgImage = rMain->hBmpBg;
				emt_lat->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_degrees1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_DEGREES1));
				emt_degrees1->bgImage = rMain->hBmpBg;
				emt_degrees1->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_degrees2 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_DEGREES2));
				emt_degrees2->bgImage = rMain->hBmpBg;
				emt_degrees2->setFont(rMain->fonts->em2text, false);
				emt_degrees2->align = EM2Text::ALIGN_RIGHT;					
				EM2EditSpin * emesLong = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE));
				emesLong->setFont(rMain->fonts->em2editspin, false);
				emesLong->bgImage = rMain->hBmpBg;
				EM2EditSpin * emesLat = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE));
				emesLat->setFont(rMain->fonts->em2editspin, false);
				emesLat->bgImage = rMain->hBmpBg;
				EM2Text * emt_TSunAlt = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_SUN_ALTITUDE));
				emt_TSunAlt->bgImage = rMain->hBmpBg;
				emt_TSunAlt->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_SunAlt = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_ALTITUDE));
				emt_SunAlt->bgImage = rMain->hBmpBg;
				emt_SunAlt->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_TSunAzi = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_SUN_AZIMUTH));
				emt_TSunAzi->bgImage = rMain->hBmpBg;
				emt_TSunAzi->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_SunAzi = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_AZIMUTH));
				emt_SunAzi->bgImage = rMain->hBmpBg;
				emt_SunAzi->setFont(rMain->fonts->em2text, false);

				emesLong->setValuesToInt(NOX_ENV_LONG_DEF, NOX_ENV_LONG_MIN, NOX_ENV_LONG_MAX, NOX_ENV_LONG_DEF, 1, 0.2f);
				emesLat->setValuesToInt(NOX_ENV_LAT_DEF, NOX_ENV_LAT_MIN, NOX_ENV_LAT_MAX, NOX_ENV_LAT_DEF, 1, 0.2f);

				emt_SunAlt->align = EM2Text::ALIGN_RIGHT;					
				emt_SunAzi->align = EM2Text::ALIGN_RIGHT;					
				SetWindowText(emt_SunAlt->hwnd, "0 �");
				SetWindowText(emt_SunAzi->hwnd, "0 �");


				// EARTH
				EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
				env->bmEarth = (HBITMAP)LoadImage(hDllModule, 
							MAKEINTRESOURCE(IDB_ENV_EARTH_IMG), IMAGE_BITMAP, 0, 0, 0);
				env->drawBorder = false;
				env->sunsky.setPosition(0,0);

				// try to get timezone shift
				TIME_ZONE_INFORMATION tzi;
				if (GetTimeZoneInformation(&tzi))
					env->sunsky.setStandardMeridianForTimeZone(tzi.Bias*PI/720);
				// set current date and time for sun sky initialization
				SYSTEMTIME st;
				GetLocalTime(&st);
				env->sunsky.setDate((int)st.wMonth, (int)st.wDay, (int)st.wHour, (float)(int)st.wMinute);
				env->evalSunPosition();
				env->evalShadow();

				// DATE TIME
				EM2Text * emt_date = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_DATE));
				emt_date->bgImage = rMain->hBmpBg;
				emt_date->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_time = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_TIME));
				emt_time->bgImage = rMain->hBmpBg;
				emt_time->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_gmt = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_GMT));
				emt_gmt->bgImage = rMain->hBmpBg;
				emt_gmt->setFont(rMain->fonts->em2text, false);

				EM2EditSpin * emesMonth = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MONTH));
				emesMonth->setFont(rMain->fonts->em2editspin, false);
				emesMonth->bgImage = rMain->hBmpBg;
				emesMonth->setValuesToInt((int)st.wMonth, 1, 12, 3, 1, 0.2f);
				EM2EditSpin * emesDay= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_DAY));
				emesDay->setFont(rMain->fonts->em2editspin, false);
				emesDay->bgImage = rMain->hBmpBg;
				emesDay->setValuesToInt((int)st.wDay, 1, 31, 21, 1, 0.2f);
				EM2EditSpin * emesHour = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_HOUR));
				emesHour->setFont(rMain->fonts->em2editspin, false);
				emesHour->bgImage = rMain->hBmpBg;
				emesHour->setValuesToInt((int)st.wHour, 0, 23, 12, 1, 0.2f);
				EM2EditSpin * emesMinute = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MINUTE));
				emesMinute->setFont(rMain->fonts->em2editspin, false);
				emesMinute->bgImage = rMain->hBmpBg;
				emesMinute->setValuesToInt((int)st.wMinute, 0, 59, 0, 1, 0.2f);
				EM2EditSpin * emesGmt= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GMT));
				emesGmt->setFont(rMain->fonts->em2editspin, false);
				emesGmt->bgImage = rMain->hBmpBg;
				emesGmt->setValuesToInt((int)tzi.Bias/-60, -12, 12, 0, 1, 0.2f);

				// SKY SYSTEM
				EM2CheckBox * emcSkyOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SKY_ENABLED));
				emcSkyOn->bgImage = rMain->hBmpBg;
				emcSkyOn->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emt_model = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_MODEL));
				emt_model->bgImage = rMain->hBmpBg;
				emt_model->setFont(rMain->fonts->em2text, false);
				emt_model->align = EM2Text::ALIGN_RIGHT;
				
				EM2ComboBox * emcSkyModel = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SKY_MODEL));
				emcSkyModel->bgImage = rMain->hBmpBg;
				emcSkyModel->setFontMain(rMain->fonts->em2combomain, false);
				emcSkyModel->setFontUnwrapped(rMain->fonts->em2combounwrap, false);
				RECT temprect;
				GetClientRect(emcSkyModel->hwnd, &temprect);
				emcSkyModel->rowWidth = temprect.right - 2;
				emcSkyModel->setNumItems(2);
				emcSkyModel->setItem(0, "PREETHAM", true);
				emcSkyModel->setItem(1, "HOSEK-WILKIE", true);

				// ATMOSPHERE
				EM2Text * emt_turb = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_TURBIDITY));
				emt_turb->bgImage = rMain->hBmpBg;
				emt_turb->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_albedo = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_ALBEDO));
				emt_albedo->bgImage = rMain->hBmpBg;
				emt_albedo->setFont(rMain->fonts->em2text, false);
				EM2EditTrackBar * emetTurb = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TURBIDITY));
				emetTurb->setFont(rMain->fonts->em2edittrack, false);
				emetTurb->bgImage = rMain->hBmpBg;
				emetTurb->setEditBoxWidth(38, 5);
				emetTurb->setValuesToInt(NOX_ENV_TURB_DEF, NOX_ENV_TURB_MIN, NOX_ENV_TURB_MAX, NOX_ENV_TURB_DEF, 5);
				EM2EditTrackBar * emetAlbedo = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_ALBEDO));
				emetAlbedo->setFont(rMain->fonts->em2edittrack, false);
				emetAlbedo->bgImage = rMain->hBmpBg;
				emetAlbedo->setEditBoxWidth(38, 5);
				emetAlbedo->setValuesToInt(NOX_ENV_ALBEDO_INT_DEF, NOX_ENV_ALBEDO_INT_MIN, NOX_ENV_ALBEDO_INT_MAX, NOX_ENV_ALBEDO_INT_DEF, 10);

				//SUN
				EM2CheckBox * emcSunOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_ENABLED));
				emcSunOn->bgImage = rMain->hBmpBg;
				emcSunOn->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcSunL2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_LAYER2));
				emcSunL2->bgImage = rMain->hBmpBg;
				emcSunL2->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emt_sunsize = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_SUN_SIZE));
				emt_sunsize->bgImage = rMain->hBmpBg;
				emt_sunsize->setFont(rMain->fonts->em2text, false);
				EM2EditTrackBar * emetSunsize = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_SIZE));
				emetSunsize->setFont(rMain->fonts->em2edittrack, false);
				emetSunsize->bgImage = rMain->hBmpBg;
				emetSunsize->setEditBoxWidth(38, 5);
				emetSunsize->floatAfterDot = 3;
				emetSunsize->setValuesToFloat(NOX_ENV_SUN_SIZE_DEF, NOX_ENV_SUN_SIZE_MIN, NOX_ENV_SUN_SIZE_MAX, NOX_ENV_SUN_SIZE_DEF, 10);

				// ENV MAP
				EM2CheckBox * emcEnvmapOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ENABLED));
				emcEnvmapOn->bgImage = rMain->hBmpBg;
				emcEnvmapOn->setFont(rMain->fonts->em2checkbox, false);

				EM2Text * emt_envlayer = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_BLENDLAYER));
				emt_envlayer->bgImage = rMain->hBmpBg;
				emt_envlayer->setFont(rMain->fonts->em2text, false);
				emt_envlayer->align = EM2Text::ALIGN_RIGHT;					
				EM2Text * emt_envpower = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_POWER));
				emt_envpower->bgImage = rMain->hBmpBg;
				emt_envpower->setFont(rMain->fonts->em2text, false);
				EM2Text * emt_envrot = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_ROTATION));
				emt_envrot->bgImage = rMain->hBmpBg;
				emt_envrot->setFont(rMain->fonts->em2text, false);
				emt_envrot->align = EM2Text::ALIGN_RIGHT;					

				EM2EditSpin * emesLayer = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_LAYER));
				emesLayer->setFont(rMain->fonts->em2editspin, false);
				emesLayer->bgImage = rMain->hBmpBg;
				emesLayer->setValuesToInt(NOX_ENV_MAP_BLEND_DEF, NOX_ENV_MAP_BLEND_MIN, NOX_ENV_MAP_BLEND_MAX, NOX_ENV_MAP_BLEND_DEF, 1, 0.1f);
				EM2EditSpin * emesPower = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_POWER));
				emesPower->setFont(rMain->fonts->em2editspin, false);
				emesPower->bgImage = rMain->hBmpBg;
				emesPower->setValuesToFloat(NOX_ENV_MAP_EV_DEF, NOX_ENV_MAP_EV_MIN, NOX_ENV_MAP_EV_MAX, NOX_ENV_MAP_EV_DEF, 0.5f, 0.1f);
				EM2EditSpin * emesRotation = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ROTATION));
				emesRotation->setFont(rMain->fonts->em2editspin, false);
				emesRotation->bgImage = rMain->hBmpBg;
				emesRotation->floatAfterDot = 1;
				emesRotation->setValuesToFloat(NOX_ENV_MAP_AZIMUTH_DEF, NOX_ENV_MAP_AZIMUTH_MIN, NOX_ENV_MAP_AZIMUTH_MAX, NOX_ENV_MAP_AZIMUTH_DEF, 5, 0.1f);

				EM2Button * embTex = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_TEXTURE));
				embTex->bgImage = rMain->hBmpBg;
				embTex->setFont(rMain->fonts->em2button, false);
				embTex->ellipsis = true;


			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_ENV_EARTH:
						{
							if (wmEvent != EN_POSITION_CHANGED)
								break;
							EM2EditSpin * emesLong = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE));
							EM2EditSpin * emesLat = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE));
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							CHECK(emesLong);
							CHECK(emesLat);
							CHECK(env);
							float longitude = env->sunsky.getLongitude();
							float latitude = env->sunsky.getLatitude();
							emesLong->setIntValue((int)((-longitude*180/PI)+0.0f));
							emesLat->setIntValue((int)((-latitude*180/PI)+0.0f));

							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							CHECK(sunsky);
							sunsky->setPosition(longitude, latitude);

							rMain->updateSunskyAzimuthAltitude();
						}
						break;
					case IDC_REND2_ENV_LONGITUDE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesLong = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE));
							EM2EditSpin * emesLat = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE));
							CHECK(emesLong);
							CHECK(emesLat);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							CHECK(sunsky);
							sunsky->setPosition((float)-emesLong->intValue*(PI)/180.0f, (float)(-emesLat->intValue)*(PI)/180.0f);
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							env->sunsky.copyFrom(sunsky);
							env->repaint();
						}
						break;
					case IDC_REND2_ENV_LATITUDE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesLong = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE));
							EM2EditSpin * emesLat = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE));
							CHECK(emesLong);
							CHECK(emesLat);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							CHECK(sunsky);
							sunsky->setPosition((float)-emesLong->intValue*(PI)/180.0f, (float)(-emesLat->intValue)*(PI)/180.0f);
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							env->sunsky.copyFrom(sunsky);
							env->repaint();
						}
						break;
					case IDC_REND2_ENV_MONTH:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesMonth = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MONTH));
							EM2EditSpin * emesDay= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_DAY));
							int maxday = 31;
							switch (emesMonth->intValue)
							{
								case 4:
								case 6:
								case 9:
								case 11:
									maxday = 30;
									break;
								case 2:
									maxday = 28;
									break;
							}
							emesDay->setValuesToInt(emesDay->intValue, 1, maxday, 21, 1, 0.1f);
							rMain->updateSunskyDateFromGui();
						}
						break;
					case IDC_REND2_ENV_DAY:
						{
							CHECK(wmEvent==WM_VSCROLL);
							rMain->updateSunskyDateFromGui();
						}
						break;
					case IDC_REND2_ENV_HOUR:
						{
							CHECK(wmEvent==WM_VSCROLL);
							rMain->updateSunskyDateFromGui();
						}
						break;
					case IDC_REND2_ENV_MINUTE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							rMain->updateSunskyDateFromGui();
						}
						break;
					case IDC_REND2_ENV_GMT:
						{
							CHECK(wmEvent==WM_VSCROLL);
							rMain->updateSunskyDateFromGui();
						}
						break;
					case IDC_REND2_ENV_SKY_ENABLED:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcSkyOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SKY_ENABLED));
							CHECK(emcSkyOn);
							Raytracer::getInstance()->curScenePtr->sscene.useSunSky = emcSkyOn->selected;
							rMain->updateLocksTabEnvironment(false);
						}
						break;
					case IDC_REND2_ENV_SKY_MODEL:
						{
							EM2ComboBox * emcSkyModel = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SKY_MODEL));
							CHECK(emcSkyModel);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							CHECK(sunsky);
							CHECK(env);
							sunsky->use_hosek = (emcSkyModel->selected==1);
							env->sunsky.use_hosek = (emcSkyModel->selected==1);
						}
						break;
					case IDC_REND2_ENV_TURBIDITY:
						{
							EM2EditTrackBar * emtTurb = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_TURBIDITY));
							CHECK(emtTurb);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							CHECK(sunsky);
							CHECK(env);
							sunsky->turbidity = emtTurb->intValue * 0.1f;
							env->sunsky.turbidity = emtTurb->intValue * 0.1f;
						}
						break;
					case IDC_REND2_ENV_ALBEDO:
						{
							EM2EditTrackBar * emtAlb = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_ALBEDO));
							CHECK(emtAlb);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							CHECK(sunsky);
							CHECK(env);
							sunsky->albedo = emtAlb->intValue * 0.01f;
							env->sunsky.albedo = emtAlb->intValue * 0.01f;
							sunsky->updateHosekData();
							env->sunsky.updateHosekData();
						}
						break;
					case IDC_REND2_ENV_SUN_ENABLED:
						{
							EM2CheckBox * emcSunOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_ENABLED));
							CHECK(emcSunOn);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							CHECK(sunsky);
							CHECK(env);
							sunsky->sunOn = emcSunOn->selected;
							env->sunsky.sunOn = emcSunOn->selected;
							rMain->updateLocksTabEnvironment(false);
						}
						break;
					case IDC_REND2_ENV_SUN_LAYER2:
						{
							EM2CheckBox * emcSunL2 = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_LAYER2));
							CHECK(emcSunL2);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							CHECK(sunsky);
							CHECK(env);
							sunsky->sunOtherBlendLayer = emcSunL2->selected;
							env->sunsky.sunOtherBlendLayer = emcSunL2->selected;
						}
						break;
					case IDC_REND2_ENV_SUN_SIZE:
						{
							EM2EditTrackBar * emtSunSize = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_SIZE));
							CHECK(emtSunSize);
							SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
							EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
							CHECK(sunsky);
							CHECK(env);
							sunsky->sunSize = emtSunSize->floatValue;
							env->sunsky.sunSize = emtSunSize->floatValue;
							float maxcos = cos(emtSunSize->floatValue/2*PI/180.0f);
							sunsky->maxCosSunSize = maxcos;
							env->sunsky.maxCosSunSize = maxcos;
						}
						break;
					case IDC_REND2_ENV_MAP_ENABLED:
						{
							EM2CheckBox * emcEnvOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ENABLED));
							CHECK(emcEnvOn);
							EnvSphere * envmap = &(Raytracer::getInstance()->curScenePtr->env);
							envmap->enabled = emcEnvOn->selected;
							rMain->updateLocksTabEnvironment(false);
						}
						break;
					case IDC_REND2_ENV_MAP_LAYER:
						{
							EM2EditSpin * emesEnvL = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_LAYER));
							CHECK(emesEnvL);
							EnvSphere * envmap = &(Raytracer::getInstance()->curScenePtr->env);
							envmap->blendlayer = emesEnvL->intValue;
						}
						break;
					case IDC_REND2_ENV_MAP_POWER:
						{
							EM2EditSpin * emesEnvPower = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_POWER));
							CHECK(emesEnvPower);
							EnvSphere * envmap = &(Raytracer::getInstance()->curScenePtr->env);
							envmap->powerEV = emesEnvPower->floatValue;
						}
						break;
					case IDC_REND2_ENV_MAP_ROTATION:
						{
							EM2EditSpin * emesEnvRot = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ROTATION));
							CHECK(emesEnvRot);
							EnvSphere * envmap = &(Raytracer::getInstance()->curScenePtr->env);
							envmap->azimuth_shift = emesEnvRot->floatValue;
							envmap->updateAzimuthDirections();
						}
						break;
					case IDC_REND2_ENV_MAP_TEXTURE:
						{
							CHECK(wmEvent==BN_CLICKED);
							Raytracer * rtr = Raytracer::getInstance();
							TextureInstance * t = &(rtr->curScenePtr->env.tex);
							Texture * orig = NULL;
							if (t->managerID>=0)
								orig = rtr->curScenePtr->texManager.textures[t->managerID];
							Texture * tcopy = new Texture();
							tcopy->copyFromOther(orig);
							tcopy->texMod = t->texMod;

							Texture * texres = (Texture *)DialogBoxParam(hDllModule, 
								MAKEINTRESOURCE(IDD_TEXTURE2_DIALOG), hWnd, Tex2DlgProc,(LPARAM)( tcopy ));
							if (texres)
							{
								if (t->filename)
									free(t->filename);
								if (t->fullfilename)
									free(t->fullfilename);
								t->filename = copyString(texres->filename);
								t->fullfilename = copyString(texres->fullfilename);

								rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

								int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, false);
								t->managerID = id;

								t->texMod = texres->texMod;
								rtr->curScenePtr->env.enabled = texres->bmp.isValid();

								texres->freeAllBuffers();
								delete texres;
							}
							
							rtr->curScenePtr->env.enabled = rtr->curScenePtr->env.tex.isValid();
							rMain->fillEnvTabAll();
						

						}
						break;
				}
			}		// end WM_COMMAND
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelEnv()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TITLE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	// GROUPS
	EM2GroupBar * emgLongLat = GetEM2GroupBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_LONG_LAT));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_LONG_LAT), bgShiftXRightPanel, 0, emgLongLat->bgShiftX, emgLongLat->bgShiftY);
	EM2GroupBar * emgDateTime = GetEM2GroupBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_DATE_AND_TIME));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_DATE_AND_TIME), bgShiftXRightPanel, 0, emgDateTime->bgShiftX, emgDateTime->bgShiftY);
	EM2GroupBar * emgSky = GetEM2GroupBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_SKY_SYSTEM));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_SKY_SYSTEM), bgShiftXRightPanel, 0, emgSky->bgShiftX, emgSky->bgShiftY);
	EM2GroupBar * emgAtm = GetEM2GroupBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_ATMOSPHERE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_ATMOSPHERE), bgShiftXRightPanel, 0, emgAtm->bgShiftX, emgAtm->bgShiftY);
	EM2GroupBar * emgSun = GetEM2GroupBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_SUN));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_SUN), bgShiftXRightPanel, 0, emgSun->bgShiftX, emgSun->bgShiftY);
	EM2GroupBar * emgEnvMap = GetEM2GroupBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_ENV_MAP));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_ENV_MAP), bgShiftXRightPanel, 0, emgEnvMap->bgShiftX, emgEnvMap->bgShiftY);

	// LONG/LAT
	EM2Text * emtLong = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_LONGITUDE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_LONGITUDE), bgShiftXRightPanel, 0, emtLong->bgShiftX, emtLong->bgShiftY);
	EM2Text * emtLat = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_LATITUDE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_LATITUDE), bgShiftXRightPanel, 0, emtLat->bgShiftX, emtLat->bgShiftY);
	EM2Text * emtDeg1 = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_DEGREES1));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_DEGREES1), bgShiftXRightPanel, 0, emtDeg1->bgShiftX, emtDeg1->bgShiftY);
	EM2Text * emtDeg2 = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_DEGREES2));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_DEGREES2), bgShiftXRightPanel, 0, emtDeg2->bgShiftX, emtDeg2->bgShiftY);
	EM2Text * emtTSunAzi = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_SUN_AZIMUTH));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_SUN_AZIMUTH), bgShiftXRightPanel, 0, emtTSunAzi->bgShiftX, emtTSunAzi->bgShiftY);
	EM2Text * emtSunAzi = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_AZIMUTH));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_AZIMUTH), bgShiftXRightPanel, 0, emtSunAzi->bgShiftX, emtSunAzi->bgShiftY);
	EM2Text * emtTSunAlt = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_SUN_ALTITUDE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_SUN_ALTITUDE), bgShiftXRightPanel, 0, emtTSunAlt->bgShiftX, emtTSunAlt->bgShiftY);
	EM2Text * emtSunAlt = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_ALTITUDE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_ALTITUDE), bgShiftXRightPanel, 0, emtSunAlt->bgShiftX, emtSunAlt->bgShiftY);
	EM2EditSpin * emesLong = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_LONGITUDE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_LONGITUDE), bgShiftXRightPanel, 0, emesLong->bgShiftX, emesLong->bgShiftY);
	EM2EditSpin * emesLat = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_LATITUDE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_LATITUDE), bgShiftXRightPanel, 0, emesLat->bgShiftX, emesLat->bgShiftY);

	// DATE/TIME
	EM2Text * emtDate = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_DATE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_DATE), bgShiftXRightPanel, 0, emtDate->bgShiftX, emtDate->bgShiftY);
	EM2Text * emtTime = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_TIME));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_TIME), bgShiftXRightPanel, 0, emtTime->bgShiftX, emtTime->bgShiftY);
	EM2Text * emtGmt = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_GMT));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_GMT), bgShiftXRightPanel, 0, emtGmt->bgShiftX, emtGmt->bgShiftY);
	EM2EditSpin * emesMonth = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MONTH));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_MONTH), bgShiftXRightPanel, 0, emesMonth->bgShiftX, emesMonth->bgShiftY);
	EM2EditSpin * emesDay = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_DAY));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_DAY), bgShiftXRightPanel, 0, emesDay->bgShiftX, emesDay->bgShiftY);
	EM2EditSpin * emesHour = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_HOUR));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_HOUR), bgShiftXRightPanel, 0, emesHour->bgShiftX, emesHour->bgShiftY);
	EM2EditSpin * emesMinute = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MINUTE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_MINUTE), bgShiftXRightPanel, 0, emesMinute->bgShiftX, emesMinute->bgShiftY);
	EM2EditSpin * emesGmt = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_GMT));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_GMT), bgShiftXRightPanel, 0, emesGmt->bgShiftX, emesGmt->bgShiftY);

	// SKY SYSTEM
	EM2CheckBox * emcSkyOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SKY_ENABLED));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_SKY_ENABLED), bgShiftXRightPanel, 0, emcSkyOn->bgShiftX, emcSkyOn->bgShiftY);
	EM2Text * emtModel = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_MODEL));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_MODEL), bgShiftXRightPanel, 0, emtModel->bgShiftX, emtModel->bgShiftY);
	EM2ComboBox * emcSkyModel = GetEM2ComboBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SKY_MODEL));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_SKY_MODEL), bgShiftXRightPanel, 0, emcSkyModel->bgShiftX, emcSkyModel->bgShiftY);

	// ATMOSPHERE
	EM2Text * emtTurb = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_TURBIDITY));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_TURBIDITY), bgShiftXRightPanel, 0, emtTurb->bgShiftX, emtTurb->bgShiftY);
	EM2Text * emtAlbedo = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_ALBEDO));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_ALBEDO), bgShiftXRightPanel, 0, emtAlbedo->bgShiftX, emtAlbedo->bgShiftY);
	EM2EditTrackBar * emetTurb = GetEM2EditTrackBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TURBIDITY));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TURBIDITY), bgShiftXRightPanel, 0, emetTurb->bgShiftX, emetTurb->bgShiftY);
	EM2EditTrackBar * emetAlbedo = GetEM2EditTrackBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_ALBEDO));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_ALBEDO), bgShiftXRightPanel, 0, emetAlbedo->bgShiftX, emetAlbedo->bgShiftY);

	// SUN
	EM2CheckBox * emcSunOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_ENABLED));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_ENABLED), bgShiftXRightPanel, 0, emcSunOn->bgShiftX, emcSunOn->bgShiftY);
	EM2CheckBox * emcSunL2 = GetEM2CheckBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_LAYER2));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_LAYER2), bgShiftXRightPanel, 0, emcSunL2->bgShiftX, emcSunL2->bgShiftY);
	EM2Text * emtSunSize = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_SUN_SIZE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_SUN_SIZE), bgShiftXRightPanel, 0, emtSunSize->bgShiftX, emtSunSize->bgShiftY);
	EM2EditTrackBar * emetSunSize = GetEM2EditTrackBarInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_SIZE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_SIZE), bgShiftXRightPanel, 0, emetSunSize->bgShiftX, emetSunSize->bgShiftY);

	// ENVIRONMENT MAP
	EM2CheckBox * emcEnvOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_ENABLED));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_ENABLED), bgShiftXRightPanel, 0, emcEnvOn->bgShiftX, emcEnvOn->bgShiftY);
	EM2Button * emcEnvTex = GetEM2ButtonInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_TEXTURE));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_TEXTURE), bgShiftXRightPanel, 0, emcEnvTex->bgShiftX, emcEnvTex->bgShiftY);
	EM2Text * emtEnvLayer = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_BLENDLAYER));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_BLENDLAYER), bgShiftXRightPanel, 0, emtEnvLayer->bgShiftX, emtEnvLayer->bgShiftY);
	EM2Text * emtEnvPower = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_POWER));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_POWER), bgShiftXRightPanel, 0, emtEnvPower->bgShiftX, emtEnvPower->bgShiftY);
	EM2Text * emtEnvRot = GetEM2TextInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_ROTATION));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_TEXT_ROTATION), bgShiftXRightPanel, 0, emtEnvRot->bgShiftX, emtEnvRot->bgShiftY);
	EM2EditSpin * emesLayer = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_LAYER));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_LAYER), bgShiftXRightPanel, 0, emesLayer->bgShiftX, emesLayer->bgShiftY);
	EM2EditSpin * emesPower = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_POWER));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_POWER), bgShiftXRightPanel, 0, emesPower->bgShiftX, emesPower->bgShiftY);
	EM2EditSpin * emesRot = GetEM2EditSpinInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_ROTATION));
	updateControlBgShift(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_ROTATION), bgShiftXRightPanel, 0, emesRot->bgShiftX, emesRot->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartEnvironmentTab(HWND hWnd)
{
	int x,y;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TITLE), HWND_TOP, 139,11, 110, 16, 0);

	x=8; y=39;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GR_LONG_LAT),		HWND_TOP,	x,		y,				360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH),				HWND_TOP,	x,		y+16,			360,	181,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_LONGITUDE),	HWND_TOP,	x,		y+210,			67,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE),			HWND_TOP,	x+67,	y+207,			52,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_DEGREES1),		HWND_TOP,	x+125,	y+210,			55,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_LATITUDE),		HWND_TOP,	x+197,	y+210,			58,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE),			HWND_TOP,	x+255,	y+207,			52,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_DEGREES2),		HWND_TOP,	x+305,	y+210,			55,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_SUN_ALTITUDE),	HWND_TOP,	x,		y+241,			82,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_ALTITUDE),		HWND_TOP,	x+82,	y+241,			28,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_SUN_AZIMUTH),	HWND_TOP,	x+254,	y+241,			78,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_AZIMUTH),		HWND_TOP,	x+332,	y+241,			28,		14,			0);

	x=8; y=308;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GR_DATE_AND_TIME),	HWND_TOP,	x,		y,				360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_DATE),			HWND_TOP,	x,		y+29,			33,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MONTH),				HWND_TOP,	x+35,	y+26,			44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_DAY),				HWND_TOP,	x+86,	y+26,			44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_TIME),			HWND_TOP,	x+145,	y+29,			32,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_HOUR),				HWND_TOP,	x+177,	y+26,			44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MINUTE),			HWND_TOP,	x+228,	y+26,			44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_GMT),			HWND_TOP,	x+287,	y+29,			27,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GMT),				HWND_TOP,	x+317,	y+26,			44,		20,			0);

	x=8; y=365;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GR_SKY_SYSTEM),		HWND_TOP,	x,		y,				360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SKY_ENABLED),		HWND_TOP,	x,		y+32,			80,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_MODEL),		HWND_TOP,	x+220,	y+30,			40,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SKY_MODEL),			HWND_TOP,	x+260,	y+27,			100,	20,			0);

	x=8; y=423;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GR_ATMOSPHERE),		HWND_TOP,	x,		y,				360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_TURBIDITY),	HWND_TOP,	x,		y+31,			65,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TURBIDITY),			HWND_TOP,	x+95,	y+28,			266,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_ALBEDO),		HWND_TOP,	x,		y+61,			55,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_ALBEDO),			HWND_TOP,	x+95,	y+58,			266,	20,			0);

	x=8; y=511;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GR_SUN),			HWND_TOP,	x,		y,				360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_ENABLED),		HWND_TOP,	x,		y+32,			85,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_LAYER2),		HWND_TOP,	x+264,	y+32,			97,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_SUN_SIZE),		HWND_TOP,	x,		y+60,			55,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_SIZE),			HWND_TOP,	x+95,	y+57,			266,	20,			0);

	x=8; y=599;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GR_ENV_MAP),		HWND_TOP,	x,		y,				360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ENABLED),		HWND_TOP,	x,		y+32,			85,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_TEXTURE),		HWND_TOP,	x+95,	y+26,			110,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_BLENDLAYER),	HWND_TOP,	x+216,	y+58,			80,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_LAYER),			HWND_TOP,	x+302,	y+55,			59,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_POWER),		HWND_TOP,	x,		y+58,			70,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_POWER),			HWND_TOP,	x+95,	y+55,			59,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TEXT_ROTATION),		HWND_TOP,	x+236,	y+30,			60,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ROTATION),		HWND_TOP,	x+302,	y+27,			59,		20,			0);

	InvalidateRect(hWnd, NULL, false);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE),		HWND_TOP,										0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE),		GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MONTH),			GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_DAY),			GetDlgItem(hWnd, IDC_REND2_ENV_MONTH),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_HOUR),			GetDlgItem(hWnd, IDC_REND2_ENV_DAY),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MINUTE),		GetDlgItem(hWnd, IDC_REND2_ENV_HOUR),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_GMT),			GetDlgItem(hWnd, IDC_REND2_ENV_MINUTE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_TURBIDITY),		GetDlgItem(hWnd, IDC_REND2_ENV_GMT),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_ALBEDO),		GetDlgItem(hWnd, IDC_REND2_ENV_TURBIDITY),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_SUN_SIZE),		GetDlgItem(hWnd, IDC_REND2_ENV_ALBEDO),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_POWER),		GetDlgItem(hWnd, IDC_REND2_ENV_SUN_SIZE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ROTATION),	GetDlgItem(hWnd, IDC_REND2_ENV_MAP_POWER),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_ENV_MAP_LAYER),		GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ROTATION),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillEnvTabAll()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);
	CHECK(sc->sscene.sunsky);
	SunSky * sunsky = sc->sscene.sunsky;
	HWND hWnd = hTabEnv;

	EM2EditSpin * emesLong =		GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_LONGITUDE));
	EM2EditSpin * emesLat =			GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_LATITUDE));
	EM2EditSpin * emesMonth =		GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_MONTH));
	EM2EditSpin * emesDay=			GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_DAY));
	EM2EditSpin * emesHour =		GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_HOUR));
	EM2EditSpin * emesMinute =		GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_MINUTE));
	EM2EditSpin * emesGmt=			GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_GMT));
	EM2CheckBox * emcSkyOn =		GetEM2CheckBoxInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_SKY_ENABLED));
	EM2ComboBox * emcSkyModel =		GetEM2ComboBoxInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_SKY_MODEL));
	EM2EditTrackBar * emetTurb =	GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_ENV_TURBIDITY));
	EM2EditTrackBar * emetAlbedo =	GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_ENV_ALBEDO));
	EM2CheckBox * emcSunOn =		GetEM2CheckBoxInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_SUN_ENABLED));
	EM2CheckBox * emcSunL2 =		GetEM2CheckBoxInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_SUN_LAYER2));
	EM2EditTrackBar * emetSunsize = GetEM2EditTrackBarInstance(	GetDlgItem(hWnd, IDC_REND2_ENV_SUN_SIZE));
	EM2CheckBox * emcEnvmapOn =		GetEM2CheckBoxInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ENABLED));
	EM2EditSpin * emesLayer =		GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_MAP_LAYER));
	EM2EditSpin * emesPower =		GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_MAP_POWER));
	EM2EditSpin * emesRotation =	GetEM2EditSpinInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_MAP_ROTATION));
	EM2Button * embTex =			GetEM2ButtonInstance(		GetDlgItem(hWnd, IDC_REND2_ENV_MAP_TEXTURE));

	if (emesLong)
		emesLong->setIntValue((int)(-sunsky->getLongitude()*180/PI));
	if (emesLat)
		emesLat->setIntValue((int)(-sunsky->getLatitude()*180/PI));
	if (emesMonth)
		emesMonth->setIntValue(sunsky->getMonth());
	if (emesDay)
	{
		int dmax = 31;
		int m = sunsky->getMonth();
		switch (m)
		{
			case 4:
			case 6:
			case 9:
			case 11:
				dmax = 30;
				break;
			case 2:
				dmax = 28;
				break;
		}
		emesDay->setValuesToInt(sunsky->getDay(), 1, dmax, 21, 1, 0.1f);
	}
	if (emesHour)
		emesHour->setIntValue(sunsky->getHour());
	if (emesMinute)
		emesMinute->setIntValue((int)sunsky->getMinute());
	if (emesGmt)
		emesGmt->setIntValue((int)(-sunsky->getMeridianForTimeZone()*12/PI));

	if (emcSkyOn)
	{
		emcSkyOn->selected = sc->sscene.useSunSky;
		InvalidateRect(emcSkyOn->hwnd, NULL, false);
	}
	if (emcSkyModel)
	{
		emcSkyModel->selected = sunsky->use_hosek ? 1 : 0;
		InvalidateRect(emcSkyModel->hwnd, NULL, false);
	}

	if (emetTurb)
		emetTurb->setIntValue((int)(sunsky->turbidity*10));
	if (emetAlbedo)
		emetAlbedo->setIntValue((int)(sunsky->albedo*100));

	if (emcSunOn)
	{
		emcSunOn->selected = sunsky->sunOn;
		InvalidateRect(emcSunOn->hwnd, NULL, false);
	}
	if (emcSunL2)
	{
		emcSunL2->selected = sunsky->sunOtherBlendLayer;
		InvalidateRect(emcSunL2->hwnd, NULL, false);
	}
	if (emetSunsize)
		emetSunsize->setFloatValue(sunsky->sunSize);

	if (emcEnvmapOn)
	{
		emcEnvmapOn->selected = sc->env.enabled;
		InvalidateRect(emcEnvmapOn->hwnd, NULL, false);
	}

	if (emesLayer)
		emesLayer->setIntValue(sc->env.blendlayer);
	if (emesPower)
		emesPower->setFloatValue(sc->env.powerEV);
	if (emesRotation)
		emesRotation->setFloatValue(sc->env.azimuth_shift);

	// tex button
	if (rtr->curScenePtr->env.tex.filename  &&  rtr->curScenePtr->env.tex.isValid())
		embTex->changeCaption(rtr->curScenePtr->env.tex.filename);
	else
		embTex->changeCaption("NO MAP LOADED");


	bool ok1 = updateSunskyAzimuthAltitude();

	bool ok2 = copySunskyToEnvControl();

	return ok1 && ok2;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::copySunskyToEnvControl()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);
	SunSky * sunsky = sc->sscene.sunsky;
	CHECK(sunsky);
	EMEnvironment * env =	GetEMEnvironmentInstance(	GetDlgItem(hTabEnv, IDC_REND2_ENV_EARTH));
	CHECK(env);

	env->sunsky.copyFrom(sunsky);
	env->evalSunPosition();
	env->evalShadow();
	InvalidateRect(env->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::updateSunskyAzimuthAltitude()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);
	SunSky * sunsky = sc->sscene.sunsky;
	CHECK(sunsky);
	int azimuth  = 180+(int)(sunsky->phiS*180.0f/PI);
	int altitude = 90-(int)(sunsky->thetaS*180.0f/PI);
	char buf[32];
	sprintf_s(buf, 32, "%d �", azimuth);
	SetWindowText(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_AZIMUTH), buf);
	sprintf_s(buf, 32, "%d �", altitude);
	SetWindowText(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_ALTITUDE), buf);
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::updateSunskyDateFromGui()
{
	HWND hWnd = hTabEnv;
	EMEnvironment * env = GetEMEnvironmentInstance(GetDlgItem(hWnd, IDC_REND2_ENV_EARTH));
	EM2EditSpin * emesMonth = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MONTH));
	EM2EditSpin * emesDay= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_DAY));
	EM2EditSpin * emesHour = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_HOUR));
	EM2EditSpin * emesMinute = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_MINUTE));
	EM2EditSpin * emesGmt= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_ENV_GMT));
	CHECK(env);
	CHECK(emesMonth);
	CHECK(emesDay);
	CHECK(emesMinute);
	CHECK(emesHour);
	CHECK(emesGmt);
	SunSky * sunsky = Raytracer::getInstance()->curScenePtr->sscene.sunsky;
	CHECK(sunsky);
	sunsky->setStandardMeridianForTimeZone(-emesGmt->intValue*PI/12);
	sunsky->setDate(emesMonth->intValue, emesDay->intValue, emesHour->intValue, (float)emesMinute->intValue);
	env->sunsky.copyFrom(sunsky);
	env->evalSunPosition();
	env->evalShadow();
	env->repaint();
	
	updateSunskyAzimuthAltitude();

	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabEnvironment(bool nowrendering)
{
	if (!nowrendering)
	{
		EM2CheckBox * emcSkyOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SKY_ENABLED));
		setControlEnabled(emcSkyOn->selected, hTabEnv, IDC_REND2_ENV_TEXT_MODEL);
		setControlEnabled(emcSkyOn->selected, hTabEnv, IDC_REND2_ENV_SKY_MODEL);
		EM2CheckBox * emcSunOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_SUN_ENABLED));
		setControlEnabled(emcSunOn->selected, hTabEnv, IDC_REND2_ENV_SUN_LAYER2);
		setControlEnabled(emcSunOn->selected, hTabEnv, IDC_REND2_ENV_SUN_SIZE);
		setControlEnabled(emcSunOn->selected, hTabEnv, IDC_REND2_ENV_TEXT_SUN_SIZE);
		EM2CheckBox * emcEnvOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEnv, IDC_REND2_ENV_MAP_ENABLED));
		setControlEnabled(emcEnvOn->selected, hTabEnv, IDC_REND2_ENV_MAP_TEXTURE);
		setControlEnabled(emcEnvOn->selected, hTabEnv, IDC_REND2_ENV_MAP_ROTATION);
		setControlEnabled(emcEnvOn->selected, hTabEnv, IDC_REND2_ENV_MAP_POWER);
		setControlEnabled(emcEnvOn->selected, hTabEnv, IDC_REND2_ENV_MAP_LAYER);

		setControlEnabled(emcEnvOn->selected, hTabEnv, IDC_REND2_ENV_TEXT_ROTATION);
		setControlEnabled(emcEnvOn->selected, hTabEnv, IDC_REND2_ENV_TEXT_POWER);
		setControlEnabled(emcEnvOn->selected, hTabEnv, IDC_REND2_ENV_TEXT_BLENDLAYER);

	}
}

//------------------------------------------------------------------------------------------------
