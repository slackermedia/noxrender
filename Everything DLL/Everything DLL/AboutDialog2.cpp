#include <Windows.h>
#include "dialogs.h"
#include "EM2Controls.h"
#include "resource.h"
#include "noxfonts.h"
#include <tchar.h>

// new gui version

extern HMODULE hDllModule;

HBITMAP loadPNGfromResource(WORD idRes);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK About2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hLogoBMP = 0;
	static HBITMAP hBgBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = 521 + wrect.right-wrect.left-crect.right;
				tmprect.bottom = 364 + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);

				GetClientRect(hWnd, &crect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hLogoBMP = loadPNGfromResource(IDB_NOX_LOGO_ABOUT);
				hBgBMP = generateBackgroundPattern(crect.right, crect.bottom, 0,0, NGCOL_BG_LIGHT, NGCOL_BG_DARK);
				NOXFontManager * fonts = NOXFontManager::getInstance();
				
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_CLOSE),		HWND_TOP,		439, 332,	72, 22,			SWP_NOZORDER);
				EM2Button * embclose = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_ABOUT2_CLOSE));
				embclose->bgImage = hBgBMP;
				embclose->setFont(fonts->em2button, false);
				updateControlBgShift(embclose->hwnd, 0, 0, embclose->bgShiftX, embclose->bgShiftY);

				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_LICENSE),		HWND_TOP,		357, 332,	72, 22,			SWP_NOZORDER);
				EM2Button * emblicense = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_ABOUT2_LICENSE));
				emblicense->bgImage = hBgBMP;
				emblicense->setFont(fonts->em2button, false);
				updateControlBgShift(emblicense->hwnd, 0, 0, emblicense->bgShiftX, emblicense->bgShiftY);

				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_AUTOUPDATE),		HWND_TOP,		10, 338,	200, 10,	SWP_NOZORDER);
				EM2CheckBox * emcbautoupdate = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_ABOUT2_AUTOUPDATE));
				emcbautoupdate->bgImage = hBgBMP;
				emcbautoupdate->setFont(fonts->em2checkbox, false);
				updateControlBgShift(emcbautoupdate->hwnd, 0, 0, emcbautoupdate->bgShiftX, emcbautoupdate->bgShiftY);


				int vMajor = Raytracer::getInstance()->getVersionMajor();
				int vMinor = Raytracer::getInstance()->getVersionMinor();
				int vBuild = Raytracer::getInstance()->getVersionBuild();
				char vertext[128];
				#ifdef _WIN64
					sprintf_s(vertext, 128, "VERSION %d.%.2d BETA 64-BIT", vMajor, vMinor);
				#else
					sprintf_s(vertext, 128, "VERSION %d.%.2d BETA 32-BIT", vMajor, vMinor);
				#endif
				SetWindowText(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_VERSION), vertext);

				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_NOX_RENDERER),	HWND_TOP,		20, 20,		200, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_VERSION),			HWND_TOP,		20, 43,		200, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_COPYRIGHT),		HWND_TOP,		20, 66,		200, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_LINK_EVERMOTION), HWND_TOP,		20, 89,		200, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_ALL_RIGHTS),		HWND_TOP,		20, 112,	200, 16,			SWP_NOZORDER);

				EM2Text * emtNox = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_NOX_RENDERER));
				emtNox->bgImage = hLogoBMP;
				emtNox->setFont(fonts->em2text, false);
				updateControlBgShift(emtNox->hwnd, -10, -10, emtNox->bgShiftX, emtNox->bgShiftY);
				EM2Text * emtVersion = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_VERSION));
				emtVersion->bgImage = hLogoBMP;
				emtVersion->setFont(fonts->em2text, false);
				updateControlBgShift(emtVersion->hwnd, -10, -10, emtVersion->bgShiftX, emtVersion->bgShiftY);
				EM2Text * emtCopyright = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_COPYRIGHT));
				emtCopyright->bgImage = hLogoBMP;
				emtCopyright->setFont(fonts->em2text, false);
				updateControlBgShift(emtCopyright->hwnd, -10, -10, emtCopyright->bgShiftX, emtCopyright->bgShiftY);
				EM2Text * emtLink = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_LINK_EVERMOTION));
				emtLink->bgImage = hLogoBMP;
				emtLink->setFont(fonts->em2text, false);
				updateControlBgShift(emtLink->hwnd, -10, -10, emtLink->bgShiftX, emtLink->bgShiftY);
				emtLink->setHyperLink("http://www.evermotion.org");
				EM2Text * emtAllRights = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_ALL_RIGHTS));
				emtAllRights->bgImage = hLogoBMP;
				emtAllRights->setFont(fonts->em2text, false);
				updateControlBgShift(emtAllRights->hwnd, -10, -10, emtAllRights->bgShiftX, emtAllRights->bgShiftY);



				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_CODE),		HWND_TOP,		11, 260,	195, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_GUI),			HWND_TOP,		11, 285,	130, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_TESTS),		HWND_TOP,		11, 310,	185, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_EMAIL1),		HWND_TOP,		205, 260,	210, 16,			SWP_NOZORDER);
				SetWindowPos(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_EMAIL3),		HWND_TOP,		195, 310,	200, 16,			SWP_NOZORDER);

				EM2Text * emtCode = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_CODE));
				emtCode->bgImage = hBgBMP;
				emtCode->setFont(fonts->em2text, false);
				updateControlBgShift(emtCode->hwnd, 0, 0, emtCode->bgShiftX, emtCode->bgShiftY);
				EM2Text * emtGui = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_GUI));
				emtGui->bgImage = hBgBMP;
				emtGui->setFont(fonts->em2text, false);
				updateControlBgShift(emtGui->hwnd, 0, 0, emtGui->bgShiftX, emtGui->bgShiftY);
				EM2Text * emtTests = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_TESTS));
				emtTests->bgImage = hBgBMP;
				emtTests->setFont(fonts->em2text, false);
				updateControlBgShift(emtTests->hwnd, 0, 0, emtTests->bgShiftX, emtTests->bgShiftY);

				EM2Text * emtEmail1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_EMAIL1));
				emtEmail1->bgImage = hBgBMP;
				emtEmail1->setFont(fonts->em2text, false);
				emtEmail1->setHyperLink("mailto:rozwin.skrzydla@gmail.com");
				updateControlBgShift(emtEmail1->hwnd, 0, 0, emtEmail1->bgShiftX, emtEmail1->bgShiftY);
				EM2Text * emtEmail3 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_ABOUT2_TEXT_EMAIL3));
				emtEmail3->bgImage = hBgBMP;
				emtEmail3->setFont(fonts->em2text, false);
				emtEmail3->setHyperLink("mailto:zed@pixelmustdie.com");
				updateControlBgShift(emtEmail3->hwnd, 0, 0, emtEmail3->bgShiftX, emtEmail3->bgShiftY);


				bool autoupdateOn = checkAutoupdateStatusFromRegistry();
				emcbautoupdate->selected = autoupdateOn;
				InvalidateRect(emcbautoupdate->hwnd, NULL, false);
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_DESTROY:
			{
				if (hLogoBMP)
					DeleteObject(hLogoBMP);
				hLogoBMP = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;

				GetClientRect(hWnd, &rect);
				hdc = BeginPaint(hWnd, &ps);

				// back buffer
				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				// copy background image
				HDC thdc = CreateCompatibleDC(hdc);			// bg image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, 0, 0, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				// copy background image
				HDC thdc2 = CreateCompatibleDC(hdc);			// logo image buffer hdc
				HBITMAP oldBitmap2 = (HBITMAP)SelectObject(thdc2, hLogoBMP);
				BitBlt(bhdc, 10, 10, rect.right-rect.left, rect.bottom-rect.top, thdc2, 0, 0, SRCCOPY);
				SelectObject(thdc2, oldBitmap2);
				DeleteDC(thdc2);

				// done
				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_ABOUT2_CLOSE:
						{
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;

					case IDC_ABOUT2_AUTOUPDATE:
						{
							EM2CheckBox * emcbautoupdate = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_ABOUT2_AUTOUPDATE));
							setAutoupdateStatusInRegistry(emcbautoupdate->selected);
						}
						break;
					case IDC_ABOUT2_LICENSE:
						{
							openLicense2Window(hWnd);
						}
						break;

					case IDC_ABOUT_TEST_TEXT:
						{
							if (wmEvent == STN_CLICKED)
							{
							   ShellExecute(hWnd, "open", "http://www.evermotion.org", NULL, NULL, SW_SHOWNORMAL);
							}
						}
						break;
				}
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------
