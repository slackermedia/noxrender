#include <windows.h>
#include <stdio.h>
#include "Colors.h"

#ifndef USE_OPENCL_POSTPRO
bool isOpenCLSupported()
{
	return false;
}
#endif

#ifdef USE_OPENCL_POSTPRO

#include <SHLWAPI.h>
#include <DelayImp.h>

#include <CL\opencl.h>
#include "log.h"
#include "ImageBufferOpenCLProgram.cl"

#ifndef PI
#define PI 3.1415926535897932384626433832795f
#endif

void kelvinToRGB(float temperature, float *rgb);
bool getCurveSettings(float * points, bool &useLuminance);

#pragma comment(lib, "OpenCL.lib")

//---------------------------------------------------------------------------------------------------------------

bool isOpenCLSupported()
{
	__try 
	{
		cl_int clOK;
		cl_uint platformIdCount = 0;
		clOK = clGetPlatformIDs (0, nullptr, &platformIdCount);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		return false;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

struct OpenclDevice
{
	cl_platform_id platform_id;
	cl_device_id dev_id;
	char * dev_name;
	cl_uint dev_compute_units;
	cl_ulong dev_max_alloc;
	char opencl_ver_major;
	char opencl_ver_minor;
	bool dev_is_gpu;

	OpenclDevice() { memset(this, 0, sizeof(OpenclDevice)); }
};

OpenclDevice * oclDevs = NULL;

//---------------------------------------------------------------------------------------------------------------

OpenclDevice * enumOpenClDevices(unsigned int & found)
{
	found = 0;
	cl_int clOK;

	isOpenCLSupported();

	cl_uint platformIdCount = 0;
	clOK = clGetPlatformIDs (0, nullptr, &platformIdCount);
	if (platformIdCount<1  ||  clOK!=CL_SUCCESS)
		return NULL;

	cl_platform_id * platforms = new cl_platform_id[platformIdCount];
	if (!platforms)
		return NULL;
	clOK = clGetPlatformIDs (platformIdCount, platforms, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		delete [] platforms;
		return NULL;
	}

	cl_uint deviceIdCountTotal = 0;
	cl_uint deviceIdCountSoFar = 0;
	for (unsigned int i=0; i<platformIdCount; i++)
	{
		cl_uint deviceIdCount = 0;
		clGetDeviceIDs (platforms[i], CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceIdCount);
		if (clOK==CL_SUCCESS)
			deviceIdCountTotal += deviceIdCount;
	}
	if (deviceIdCountTotal<1)
	{
		delete [] platforms;
		return NULL;
	}

	cl_device_id * devices = new cl_device_id[deviceIdCountTotal];
	if (!devices)
	{
		delete [] platforms;
		return NULL;
	}

	for (unsigned int i=0; i<platformIdCount; i++)
	{
		cl_uint deviceIdCount = 0;
		clGetDeviceIDs (platforms[i], CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceIdCount);
		if (clOK!=CL_SUCCESS)
			continue;
		clOK = clGetDeviceIDs (platforms[0], CL_DEVICE_TYPE_ALL, deviceIdCount, devices+deviceIdCountSoFar, nullptr);
		if (clOK!=CL_SUCCESS)
			continue;
		deviceIdCountSoFar += deviceIdCount;
	}
	deviceIdCountTotal = deviceIdCountSoFar;

	OpenclDevice * resDevs = new OpenclDevice[deviceIdCountTotal];
	if (!resDevs  ||  deviceIdCountTotal<1)
	{
		delete [] devices;
		delete [] platforms;
		return NULL;
	}

	for (unsigned int j=0; j<deviceIdCountTotal; j++)
	{
		resDevs[j].dev_id = devices[j];
		
		cl_platform_id platform;
		clOK = clGetDeviceInfo(devices[j], CL_DEVICE_PLATFORM, sizeof(cl_platform_id), &platform, NULL);
		if (clOK==CL_SUCCESS)
			resDevs[j].platform_id = platform;

		char devName[128];
		clOK = clGetDeviceInfo(devices[j], CL_DEVICE_NAME, 128, &devName, NULL);
		if (clOK==CL_SUCCESS)
			resDevs[j].dev_name = copyString(devName);

		char devOpenClCVer[128];
		clOK = clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, 128, &devOpenClCVer, NULL);
		if (clOK==CL_SUCCESS)
		{
			char * k = strchr(devOpenClCVer, '.');
			if (k)
			{
				resDevs[j].opencl_ver_major = *(k-1)-48;
				resDevs[j].opencl_ver_minor = *(k+1)-48;
			}
		}

		cl_device_type devType;
		clOK = clGetDeviceInfo(devices[j], CL_DEVICE_TYPE, sizeof(devType), &devType, NULL);
		if (clOK==CL_SUCCESS)
			resDevs[j].dev_is_gpu = ((devType&CL_DEVICE_TYPE_GPU)!=0);

		cl_uint maxCompUnits = 0;
		clOK = clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(maxCompUnits), &maxCompUnits, NULL);
		if (clOK==CL_SUCCESS)
			resDevs[j].dev_compute_units = maxCompUnits;

		cl_ulong maxMemAlloc = 0;
		clOK = clGetDeviceInfo(devices[j], CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(maxMemAlloc), &maxMemAlloc, NULL);
		if (clOK==CL_SUCCESS)
			resDevs[j].dev_max_alloc = maxMemAlloc;
	}

	delete [] devices;
	delete [] platforms;
	found = deviceIdCountTotal;
	return resDevs;
}

//---------------------------------------------------------------------------------------------------------------

void freeAllocatedBuffers(void * buf1 = NULL, void * buf2 = NULL, void * buf3 = NULL, void * buf4 = NULL)
{
	if (buf1)
		free(buf1);
	if (buf2)
		free(buf2);
	if (buf3)
		free(buf3);
	if (buf4)
		free(buf4);
}

//---------------------------------------------------------------------------------------------------------------

void releaseOpenCLobjects(cl_kernel * kernel, cl_command_queue * queue, cl_program * program, cl_context * context, cl_mem * mem1=NULL, cl_mem * mem2=NULL, cl_mem * mem3=NULL, cl_mem * mem4=NULL)
{
	if (kernel)
		clReleaseKernel(*kernel);
	if (queue)
		clReleaseCommandQueue(*queue);
	if (mem1)
		clReleaseMemObject(*mem1);
	if (mem2)
		clReleaseMemObject(*mem2);
	if (mem3)
		clReleaseMemObject(*mem3);
	if (mem4)
		clReleaseMemObject(*mem4);
	if (program)
		clReleaseProgram(*program);
	if (context)
 		clReleaseContext(*context);
}

//---------------------------------------------------------------------------------------------------------------

bool copyBufferFromDeviceToImageBuffer(ImageBuffer * img, cl_command_queue queue, cl_mem buf)
{
	if (!img  ||  !img->imgBuf  ||  img->width<1  ||  img->height<1)
		return false;
	int w = img->width;
	int h = img->height;
	int bsize = w*h*sizeof(Color4);
	char * seqBuf = (char *)malloc(bsize);
	if (!seqBuf)
		return false;

	cl_int clOK = clEnqueueReadBuffer (queue, buf, CL_TRUE, 0, bsize, seqBuf, 0, nullptr, nullptr);

	if (clOK==CL_SUCCESS)
		for (int y=0; y<h; y++)
		{
			void * s = ((char *)seqBuf) + y*w*sizeof(Color4);
			memcpy(img->imgBuf[y], s, w*sizeof(Color4));
		}
	free(seqBuf);

	return (clOK==CL_SUCCESS);
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLISOToneGamma(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &imgBuffer, float mpl, float tone, float gamma, int width, int height)
{
	cl_int clOK;
	const size_t globalWorkSize2 [] = { width*height, 0, 0 };

	cl_kernel kernelIsoToneGamma = clCreateKernel (program, "applyISOToneGamma", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelIsoToneGamma);
		return false;
	}

	clOK = clSetKernelArg(kernelIsoToneGamma, 0, sizeof (cl_mem), &imgBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelIsoToneGamma, 1, sizeof (float), &mpl);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelIsoToneGamma, 2, sizeof (float), &tone);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelIsoToneGamma, 3, sizeof (float), &gamma);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelIsoToneGamma);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelIsoToneGamma, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelIsoToneGamma);
		return false;
	}
	clReleaseKernel(kernelIsoToneGamma);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLCameraResponse(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &imgBuffer, int numberResp, int width, int height)
{
	float * responseRed   = getResponseFunctionRed(numberResp);
	float * responseGreen = getResponseFunctionGreen(numberResp);
	float * responseBlue  = getResponseFunctionBlue(numberResp);
	cl_int clOK;

	// ALLOC RESPONSES ------------------
	cl_mem respLookupR, respLookupG, respLookupB;
	respLookupR = clCreateBuffer (context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(float)*1024, responseRed, &clOK);
	if (clOK==CL_SUCCESS)
		respLookupG = clCreateBuffer (context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(float)*1024, responseGreen, &clOK);
	if (clOK==CL_SUCCESS)
		respLookupB = clCreateBuffer (context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(float)*1024, responseBlue, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL lookup table for response functions.");
		clReleaseMemObject(respLookupR);   clReleaseMemObject(respLookupG);   clReleaseMemObject(respLookupB);
		return false;
	}

	cl_kernel kernelResponse = clCreateKernel (program, "applyResponseFunction", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelResponse);
		clReleaseMemObject(respLookupR);   clReleaseMemObject(respLookupG);   clReleaseMemObject(respLookupB);
		Logger::add("Failed creating OpenCL kernel applyResponseFunction.");
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const size_t globalWorkSize2 [] = { width*height, 0, 0 };
	clOK = clSetKernelArg(kernelResponse, 0, sizeof (cl_mem), &imgBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResponse, 1, sizeof (cl_mem), &respLookupR);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResponse, 2, sizeof (cl_mem), &respLookupG);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResponse, 3, sizeof (cl_mem), &respLookupB);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResponse, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResponse, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelResponse);
		clReleaseMemObject(respLookupR);   clReleaseMemObject(respLookupG);   clReleaseMemObject(respLookupB);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelResponse, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelResponse);
		clReleaseMemObject(respLookupR);   clReleaseMemObject(respLookupG);   clReleaseMemObject(respLookupB);
		return false;
	}

	clReleaseKernel(kernelResponse);
	clReleaseMemObject(respLookupR);   clReleaseMemObject(respLookupG);   clReleaseMemObject(respLookupB);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool getFilterWeights(int filter, int aa, float &w1, float &w2, float &w3, float &w4)
{
	float rx = 0.5f*aa;
	rx = rx-(int)rx;
	float t;
	switch (filter)
	{
		case 4:	// mitchell-netravali
			{
				t = 2 - fabs(-2+rx);
				w1 = (5*t*t*t - 3*t*t)/18.0f;
				t = 1 - fabs(-1+rx);
				w2 = (-15*t*t*t + 18*t*t + 9*t + 2)/18.0f;
				t = 1 - fabs(rx);
				w3 = (-15*t*t*t + 18*t*t + 9*t + 2)/18.0f;
				t = 2 -  fabs(1+rx);
				w4 = (5*t*t*t - 3*t*t)/18.0f;
			}
			break;
		case 5:	// catmull-rom
			{
				t = 2 - fabs(-2+rx);
				w1 = (t*t*t - t*t)/2.0f;
				t = 1 - fabs(-1+rx);
				w2 = (-3*t*t*t + 4*t*t + t)/2.0f;
				t = 1 - fabs(rx);
				w3 = (-3*t*t*t + 4*t*t + t)/2.0f;
				t = 2 -  fabs(1+rx);
				w4 = (t*t*t - t*t)/2.0f;
			}
			break;
		case 6:	// b-spline
			{
				t = 2 - fabs(-2+rx);
				w1 = t*t*t/6.0f;
				t = 1 - fabs(-1+rx);
				w2 = (-3*t*t*t + 3*t*t + 3*t + 1)/6.0f;
				t = 1 - fabs(rx);
				w3 = (-3*t*t*t + 3*t*t + 3*t + 1)/6.0f;
				t = 2 -  fabs(1+rx);
				w4 = t*t*t/6.0f;
			}
			break;
		case 7:	// gauss
			{
				float p = 1.0f/sqrt(2.0f*PI);
				t = fabs(-2+rx);
				w1 = p * exp(-t*t/2);
				t = fabs(-1+rx);
				w2 = p * exp(-t*t/2);
				t = fabs(rx);
				w3 = p * exp(-t*t/2);
				t =  fabs(1+rx);
				w4 = p * exp(-t*t/2);
				float d = w1+w2+w3+w4;
				d = 1.0f/(d);
				w1 *= d;
				w2 *= d;
				w3 *= d;
				w4 *= d;
			}
			break;
		default: 
			return false;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLFilterWeightConst(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, cl_mem &dstBuffer, int filter, int aa, int width, int height)
{
	cl_float4 mv;
	bool ok = getFilterWeights(filter, aa, mv.s[0], mv.s[1], mv.s[2], mv.s[3]);
	if (!ok)
	{
		Logger::add("Error. Wrong filter.");
		return false;
	}

	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelResize = clCreateKernel (program, "applyFilter4W", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelResize);
		return false;
	}

	const cl_int dwidth = width/aa;
	const cl_int dheight = height/aa;
	const cl_int daa = aa;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelResize, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 1, sizeof (cl_mem), &dstBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 2, sizeof (cl_float4), &mv);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 3, sizeof (cl_int), &daa);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelResize);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelResize, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelResize);
		return false;
	}
	clReleaseKernel(kernelResize);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLFilterGaussExtreme(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, cl_mem &dstBuffer, int aa, int width, int height)
{
	cl_float8 mv;
	float rx = 0.5f*aa;
	rx = rx-(int)rx;
	float t,d;

	float sigma = 1.5f;
	float sigma2 = 0.5f/(sigma*sigma);
	float p = 1.0f/sqrt(2.0f*PI)/sigma;
	t = fabs(-3+rx);
	mv.s[0] = p * exp(-t*t*sigma2);
	t = fabs(-2+rx);
	mv.s[1] = p * exp(-t*t*sigma2);
	t = fabs(-1+rx);
	mv.s[2] = p * exp(-t*t*sigma2);
	t = fabs(rx);
	mv.s[3] = p * exp(-t*t*sigma2);
	t = fabs(1+rx);
	mv.s[4] = p * exp(-t*t*sigma2);
	t = fabs(2+rx);
	mv.s[5] = p * exp(-t*t*sigma2);

	d = mv.s[0] + mv.s[1] + mv.s[2] + mv.s[3] + mv.s[4] + mv.s[5];
	d = 1.0f/(d);
	mv.s[0] *= d;
	mv.s[1] *= d;
	mv.s[2] *= d;
	mv.s[3] *= d;
	mv.s[4] *= d;
	mv.s[5] *= d;

	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelResize = clCreateKernel (program, "applyFilterGaussExtreme", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelResize);
		return false;
	}

	const cl_int dwidth = width/aa;
	const cl_int dheight = height/aa;
	const cl_int daa = aa;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelResize, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 1, sizeof (cl_mem), &dstBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 2, sizeof (cl_float8), &mv);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 3, sizeof (cl_int), &daa);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelResize);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelResize, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelResize);
		return false;
	}
	clReleaseKernel(kernelResize);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLFilterNone(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, cl_mem &dstBuffer, int aa, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelResize = clCreateKernel (program, "applyFilterNone", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelResize);
		return false;
	}

	const cl_int dwidth = width/aa;
	const cl_int dheight = height/aa;
	const cl_int daa = aa;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelResize, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 1, sizeof (cl_mem), &dstBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 2, sizeof (cl_int), &daa);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 3, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 4, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelResize);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelResize, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelResize);
		return false;
	}
	clReleaseKernel(kernelResize);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLFilterBox1(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, cl_mem &dstBuffer, int aa, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelResize = clCreateKernel (program, "applyFilterBoxOld", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelResize);
		return false;
	}

	const cl_int dwidth = width/aa;
	const cl_int dheight = height/aa;
	const cl_int daa = aa;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelResize, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 1, sizeof (cl_mem), &dstBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 2, sizeof (cl_int), &daa);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 3, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 4, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelResize);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelResize, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelResize);
		return false;
	}
	clReleaseKernel(kernelResize);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLFilterBox2(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, cl_mem &dstBuffer, int aa, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelResize = clCreateKernel (program, aa==2 ? "applyFilterBoxAA2" : "applyFilterBoxAA3", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelResize);
		return false;
	}

	const cl_int dwidth = width/aa;
	const cl_int dheight = height/aa;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelResize, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 1, sizeof (cl_mem), &dstBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 2, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 3, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelResize);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelResize, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelResize);
		return false;
	}
	clReleaseKernel(kernelResize);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLFilterAAW(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, cl_mem &dstBuffer, float mpl, int aa, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelResize = clCreateKernel (program, "applyFilterAAW", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelResize);
		return false;
	}

	const cl_int dwidth = width/aa;
	const cl_int dheight = height/aa;
	const cl_float dmpl = mpl;
	const cl_int daa = aa;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelResize, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 1, sizeof (cl_mem), &dstBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 2, sizeof (cl_float), &dmpl);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 3, sizeof (cl_int), &daa);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelResize, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelResize);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelResize, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelResize);
		return false;
	}
	clReleaseKernel(kernelResize);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLBrightnessContrast(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, float bri, float con, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelBriCon = clCreateKernel (program, "applyBrightnessAndContrast", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelBriCon);
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_float cbri = bri;
	const cl_float ccon = con;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelBriCon, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelBriCon, 1, sizeof (cl_float), &cbri);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelBriCon, 2, sizeof (cl_float), &ccon);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelBriCon, 3, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelBriCon, 4, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelBriCon);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelBriCon, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelBriCon);
		return false;
	}
	clReleaseKernel(kernelBriCon);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLAddRGB(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, float addR, float addG, float addB, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelAddRGB = clCreateKernel (program, "applyAddRGB", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelAddRGB);
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_float caddR = addR;
	const cl_float caddG = addG;
	const cl_float caddB = addB;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelAddRGB, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelAddRGB, 1, sizeof (cl_float), &caddR);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelAddRGB, 2, sizeof (cl_float), &caddG);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelAddRGB, 3, sizeof (cl_float), &caddB);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelAddRGB, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelAddRGB, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelAddRGB);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelAddRGB, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelAddRGB);
		return false;
	}
	clReleaseKernel(kernelAddRGB);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLSaturation(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, float sat, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelSat = clCreateKernel (program, "applySaturation", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelSat);
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_float csat= sat;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelSat, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelSat, 1, sizeof (cl_float), &csat);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelSat, 2, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelSat, 3, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelSat);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelSat, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelSat);
		return false;
	}
	clReleaseKernel(kernelSat);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLTemperature(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, float temp, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelTemp = clCreateKernel (program, "applyTemperature", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelTemp);
		return false;
	}

	float tempSrcRGB[3];
	float tempDstRGB[3];
	kelvinToRGB(6000, tempSrcRGB);
	kelvinToRGB(temp, tempDstRGB);

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_float cTR = tempDstRGB[0] / tempSrcRGB[0];
	const cl_float cTG = tempDstRGB[1] / tempSrcRGB[1];
	const cl_float cTB = tempDstRGB[2] / tempSrcRGB[2];
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelTemp, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelTemp, 1, sizeof (cl_float), &cTR);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelTemp, 2, sizeof (cl_float), &cTG);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelTemp, 3, sizeof (cl_float), &cTB);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelTemp, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelTemp, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelTemp);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelTemp, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelTemp);
		return false;
	}
	clReleaseKernel(kernelTemp);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLVignette(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, int vignette, float aperture, int width, int height, int owidth, int oheight, int bposx, int bposy)
{
	cl_int clOK = CL_SUCCESS;
	cl_mem vLookup;
	vLookup = clCreateBuffer (context, CL_MEM_READ_WRITE, sizeof(float)*1501, NULL, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL lookup table for response functions.");
		clReleaseMemObject(vLookup);
		return false;
	}

	//------

	cl_kernel kernelLookup = clCreateKernel (program, "makeVignetteLookupTable", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseMemObject(vLookup);
		clReleaseKernel(kernelLookup);
		return false;
	}

	const cl_float dapaerture = aperture;
	const cl_float dvignette = (float)vignette;
	const size_t globalWorkSize3 [] = { 1501, 0, 0 };
	clOK = clSetKernelArg(kernelLookup, 0, sizeof (cl_mem), &vLookup);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelLookup, 1, sizeof (cl_float), &dapaerture);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelLookup, 2, sizeof (cl_float), &dvignette);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelLookup);
		clReleaseMemObject(vLookup);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelLookup, 1,
		nullptr,
		globalWorkSize3,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelLookup);
		clReleaseMemObject(vLookup);
		return false;
	}
	clReleaseKernel(kernelLookup);

	//-------

	cl_kernel kernelVign = clCreateKernel (program, "applyVignette", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelVign);
		clReleaseMemObject(vLookup);
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_int dowidth = owidth;
	const cl_int doheight = oheight;
	const cl_int dposx = bposx;
	const cl_int dposy = bposy;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelVign, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelVign, 1, sizeof (cl_mem), &vLookup);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelVign, 2, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelVign, 3, sizeof (cl_int), &dheight);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelVign, 4, sizeof (cl_int), &dowidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelVign, 5, sizeof (cl_int), &doheight);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelVign, 6, sizeof (cl_int), &dposx);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelVign, 7, sizeof (cl_int), &dposy);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelVign);
		clReleaseMemObject(vLookup);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelVign, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed kernel execution.");
		clReleaseKernel(kernelVign);
		clReleaseMemObject(vLookup);
		return false;
	}

	clReleaseKernel(kernelVign);
	clReleaseMemObject(vLookup);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLCurve(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, float * curvearray, bool useLumi, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_mem curveBuf = clCreateBuffer (context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
					2560, curvearray, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(curveBuf);
		Logger::add("Failed creating OpenCL array buffer.");
		return false;
	}

	cl_kernel kernelCurve;
	if (useLumi)
		kernelCurve = clCreateKernel (program, "applyCurveLumi", &clOK);
	else
		kernelCurve = clCreateKernel (program, "applyCurveRGB", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelCurve);
		clReleaseMemObject(curveBuf);
		Logger::add("Failed creating OpenCL kernel.");
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelCurve, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelCurve, 1, sizeof (cl_mem), &curveBuf);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelCurve, 2, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelCurve, 3, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelCurve);
		clReleaseMemObject(curveBuf);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelCurve, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(curveBuf);
		clReleaseKernel(kernelCurve);
		Logger::add("Failed kernel execution.");
		return false;
	}
	clReleaseKernel(kernelCurve);
	clReleaseMemObject(curveBuf);
	return true;
}


//---------------------------------------------------------------------------------------------------------------

bool putOpenCLZeroIntBuffer(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &intBuffer, int countInts)
{
	cl_int clOK = CL_SUCCESS;
	cl_kernel kernelZero = clCreateKernel (program, "zeroIntBuffer", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseKernel(kernelZero);
		return false;
	}

	const size_t globalWorkSize2 [] = {countInts, 0, 0 };
	clOK = clSetKernelArg(kernelZero, 0, sizeof (cl_mem), &intBuffer);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
		clReleaseKernel(kernelZero);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelZero, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelZero);
		Logger::add("Failed kernel execution.");
		return false;
	}
	clReleaseKernel(kernelZero);
	return true;
}


//---------------------------------------------------------------------------------------------------------------

bool putOpenCLRemoveDots(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, int radius, float thres, int maxposs, int width, int height)
{
	cl_int clOK = CL_SUCCESS;
	cl_mem neighbours = clCreateBuffer (context, CL_MEM_READ_WRITE,
					width*height*sizeof(cl_int), NULL, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(neighbours);
		Logger::add("Failed creating OpenCL array buffer.");
		return false;
	}

	cl_kernel kernelDotsCount = clCreateKernel (program, "dotsCountNeighbours", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(neighbours);
		clReleaseKernel(kernelDotsCount);
		Logger::add("Failed creating OpenCL kernel.");
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_int dradius = radius;
	const cl_int darea = (radius*2+1)*(radius*2+1);
	const cl_int dmaxpos = maxposs;
	const cl_float dthres = thres;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelDotsCount, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsCount, 1, sizeof (cl_mem), &neighbours);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsCount, 2, sizeof (cl_int), &dradius);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsCount, 3, sizeof (cl_float), &dthres);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsCount, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsCount, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(neighbours);
		clReleaseKernel(kernelDotsCount);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelDotsCount, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(neighbours);
		clReleaseKernel(kernelDotsCount);
		Logger::add("Failed kernel execution.");
		return false;
	}
	clReleaseKernel(kernelDotsCount);

	cl_kernel kernelDotsMap = clCreateKernel (program, "dotsMapBurned", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL kernel.");
		clReleaseMemObject(neighbours);
		clReleaseKernel(kernelDotsMap);
		return false;
	}

	clOK = clSetKernelArg(kernelDotsMap, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsMap, 1, sizeof (cl_mem), &neighbours);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsMap, 2, sizeof (cl_int), &darea);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsMap, 3, sizeof (cl_int), &dmaxpos);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsMap, 4, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelDotsMap, 5, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(neighbours);
		clReleaseKernel(kernelDotsMap);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelDotsMap, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(neighbours);
		clReleaseKernel(kernelDotsMap);
		Logger::add("Failed kernel execution.");
		return false;
	}

	clReleaseKernel(kernelDotsMap);
	clReleaseMemObject(neighbours);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLGrain(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, float grain, unsigned int s1, unsigned int s2, int width, int height)
{
	cl_int clOK = CL_SUCCESS;

	int wh = width*height;
	unsigned short * rndBuf = (unsigned short *)malloc(wh*sizeof(unsigned short));
	for (int i=0; i<wh; i++)
		rndBuf[i] = 2*rand();

	cl_mem clRandBuf = clCreateBuffer (context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
					wh*sizeof(cl_ushort), rndBuf, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(clRandBuf);
		free(rndBuf);
		Logger::add("Failed creating OpenCL rand buffer.");
		return false;
	}
	free(rndBuf);

	cl_kernel kernelGrain = clCreateKernel (program, "applyGrain", &clOK);
	if (clOK!=CL_SUCCESS)
	{
	clReleaseKernel(kernelGrain);
		clReleaseMemObject(clRandBuf);
		Logger::add("Failed creating OpenCL kernel.");
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_float damount = grain;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelGrain, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelGrain, 1, sizeof (cl_mem), &clRandBuf);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelGrain, 2, sizeof (cl_float), &damount);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelGrain, 3, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelGrain, 4, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed assigning OpenCL argument.");
	clReleaseKernel(kernelGrain);
		clReleaseMemObject(clRandBuf);
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelGrain, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelGrain);
		clReleaseMemObject(clRandBuf);
		Logger::add("Failed kernel execution.");
		return false;
	}

	clReleaseKernel(kernelGrain);
	clReleaseMemObject(clRandBuf);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLChromaticAberrationPlanar(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, float caRed, float caGreen, float caBlue, int width, int height)
{
	cl_int clOK = CL_SUCCESS;

	int bsize = width*height*4*sizeof(float);
	cl_mem dstBuffer = clCreateBuffer (context, CL_MEM_READ_WRITE,
					bsize, NULL, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(dstBuffer);
		Logger::add("Failed creating OpenCL new buffer for planar chromatic aberration.");
		return false;
	}

	cl_kernel kernelChrAbb = clCreateKernel (program, "applyChromaticAbbPlanar", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelChrAbb);
		clReleaseMemObject(dstBuffer);
		Logger::add("Failed creating OpenCL kernel.");
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_float dChrRed   = 1.0f - min(1.0f, max(0.0f, caRed  ))*0.01f;
	const cl_float dChrGreen = 1.0f - min(1.0f, max(0.0f, caGreen))*0.01f;
	const cl_float dChrBlue  = 1.0f - min(1.0f, max(0.0f, caBlue ))*0.01f;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelChrAbb, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelChrAbb, 1, sizeof (cl_mem), &dstBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelChrAbb, 2, sizeof (cl_float), &dChrRed);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelChrAbb, 3, sizeof (cl_float), &dChrGreen);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelChrAbb, 4, sizeof (cl_float), &dChrBlue);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelChrAbb, 5, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelChrAbb, 6, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelChrAbb);
		clReleaseMemObject(dstBuffer);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelChrAbb, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelChrAbb);
		clReleaseMemObject(dstBuffer);
		Logger::add("Failed kernel execution.");
		return false;
	}

	clReleaseKernel(kernelChrAbb);
	clReleaseMemObject(srcBuffer);
	srcBuffer = dstBuffer;

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool putOpenCLFog(cl_program &program, cl_command_queue &queue, cl_context &context, cl_mem &srcBuffer, Color4 &att, float dist, float speed, bool exclSky, int width, int height, float * depthcont)
{
	if (!depthcont)
	{
		Logger::add("Error, no depth buffer.");
		return true;
	}

	cl_int clOK = CL_SUCCESS;
	int bsize = width*height*sizeof(float);

	cl_mem depthBuffer = clCreateBuffer (context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
					bsize, depthcont, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseMemObject(depthBuffer);
		free(depthcont);
		Logger::add("Failed creating OpenCL depth buffer.");
		return false;
	}

	cl_kernel kernelFog = clCreateKernel (program, "applyFog", &clOK);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelFog);
		clReleaseMemObject(depthBuffer);
		Logger::add("Failed creating OpenCL kernel.");
		return false;
	}

	const cl_int dwidth = width;
	const cl_int dheight = height;
	const cl_int dexscSky = exclSky;
	const cl_float dspeed = 1.0f/speed;
	const cl_float4 datt = { att.r, att.g, att.b, 1.0f};
	const cl_float ddist = dist;
	const size_t globalWorkSize2 [] = { dwidth*dheight, 0, 0 };
	clOK = clSetKernelArg(kernelFog, 0, sizeof (cl_mem), &srcBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelFog, 1, sizeof (cl_mem), &depthBuffer);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelFog, 2, sizeof (cl_float), &ddist);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelFog, 3, sizeof (cl_float4), &datt);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelFog, 4, sizeof (cl_float), &dspeed);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelFog, 5, sizeof (cl_int), &dexscSky);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelFog, 6, sizeof (cl_int), &dwidth);
	if (clOK==CL_SUCCESS)
		clOK = clSetKernelArg(kernelFog, 7, sizeof (cl_int), &dheight);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelFog);
		clReleaseMemObject(depthBuffer);
		Logger::add("Failed assigning OpenCL argument.");
		return false;
	}

	clEnqueueNDRangeKernel (queue, kernelFog, 1,
		nullptr,
		globalWorkSize2,
		nullptr,
		0, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		clReleaseKernel(kernelFog);
		clReleaseMemObject(depthBuffer);
		Logger::add("Failed kernel execution.");
		return false;
	}
	clReleaseKernel(kernelFog);
	clReleaseMemObject(depthBuffer);

	return true;
}


//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------

//#define MAKETIMERLOG(info) 
#define MAKETIMERLOG(info) { tickCur=GetTickCount(); tickDiff=tickCur-tickLast; tickLast=tickCur; sprintf_s(buftick, 512, "%s - took %d ms", (info), tickDiff); Logger::add(buftick); }
	
ImageBuffer * makePostprocessOpenCL(ImageBuffer * src, int aa, ImageModifier * iMod, FinalModifier * fMod, FloatBuffer * depthBuf)
{
	if (!src || !src->imgBuf || src->width<8 || src->height<8)
	{
		Logger::add("Not valid buffer.");
		return NULL;
	}
	if (aa < 1   ||   aa > 3)
	{
		Logger::add("Wrong AA.");
		return NULL;
	}

	bool ok = true;
	DWORD tickCur, tickLast, tickDiff=0;
	char buftick[512];
	tickCur = tickLast = GetTickCount();
	MAKETIMERLOG("start")


	cl_int clOK = CL_SUCCESS;
	unsigned int numDevs = 0;
	if (!oclDevs)
	{
		oclDevs = enumOpenClDevices(numDevs);
		if (!oclDevs || numDevs<1)
		{
			Logger::add("No OpenCL devices found");
			return NULL;
		}
	}

	char buf[512];
	sprintf_s(buf, 512, "Trying OpenCL %d.%d device: %s", oclDevs[0].opencl_ver_major, oclDevs[0].opencl_ver_minor, oclDevs[0].dev_name);
	Logger::add(buf);

	MAKETIMERLOG("get device")

	const cl_context_properties contextProperties [] =
					{    CL_CONTEXT_PLATFORM,	reinterpret_cast<cl_context_properties>(oclDevs[0].platform_id),    0, 0	};
	cl_context context = clCreateContext (contextProperties, 1, &oclDevs[0].dev_id, NULL, NULL, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL context.");
		return NULL;
	}

	MAKETIMERLOG("get context")

	unsigned int w = src->width;
	unsigned int h = src->height;
	unsigned int bsize = w*h*sizeof(Color4);
	Color4 * seqBuf = (Color4 *)malloc(bsize);
	if(!seqBuf)
	{
		Logger::add("Continuous buffer for image not created.");
	 	clReleaseContext(context);
		return NULL;
	}
	for (unsigned int y=0; y<h; y++)
	{
		void * d = (char *)seqBuf + y*w*sizeof(Color4);
		memcpy(d, src->imgBuf[y], w*sizeof(Color4));
	}
	MAKETIMERLOG("prepare host buffer")

	// ALLOC MEM 1 ------------------
	cl_mem srcBuffer = clCreateBuffer (context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
				bsize, seqBuf, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL image buffer.");
		releaseOpenCLobjects(NULL, NULL, NULL, &context, &srcBuffer);
		free(seqBuf);
		return NULL;
	}
	cl_mem imgBuffer = srcBuffer;
	free(seqBuf);

	MAKETIMERLOG("prepare device buffer")
	

	cl_program program = clCreateProgramWithSource(context, 1, &ibPostOpenClProgram, NULL, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL program.");
		releaseOpenCLobjects(NULL, NULL, &program, &context, &imgBuffer);
		return NULL;
	}
	MAKETIMERLOG("create program")

	clOK = clBuildProgram(program, 1, &oclDevs[0].dev_id, nullptr, nullptr, nullptr);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed building OpenCL program.");

		size_t errc;
		clOK = clGetProgramBuildInfo(program, oclDevs[0].dev_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &errc);
		if (errc>0)
		{
			errc++;
			char * errtxt = (char*)malloc(errc);
			errtxt[0] = 0;
			clOK = clGetProgramBuildInfo(program, oclDevs[0].dev_id, CL_PROGRAM_BUILD_LOG, errc, errtxt, &errc);
			if (clOK!=CL_SUCCESS)
				sprintf_s(errtxt, errc, "clGetProgramBuildInfo returned %d", clOK);
			Logger::add(errtxt);
			free(errtxt);
		}
		releaseOpenCLobjects(NULL, NULL, &program, &context, &imgBuffer);
		return NULL;
	}
	MAKETIMERLOG("build program")

	cl_command_queue queue = clCreateCommandQueue (context, oclDevs[0].dev_id, 0, &clOK);
	if (clOK!=CL_SUCCESS)
	{
		Logger::add("Failed creating OpenCL queue.");
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		return NULL;
	}

	const size_t globalWorkSize [] = { w*h, 0, 0 };
	const size_t globalWorkSize2 [] = { w*h/4, 0, 0 };
	MAKETIMERLOG("create queue")

	float * depthCont = NULL;
	if (fMod  &&  depthBuf  &&  depthBuf->fbuf  &&  depthBuf->hbuf)
	{
		if (depthBuf->width==w  &&  depthBuf->height==h)
		{
			int bdsize = depthBuf->width*depthBuf->height*sizeof(float);
			depthCont = (float*)malloc(bdsize);
			if (!depthCont)
				Logger::add("Failed creating continuous depth buffer.");
			else
			{
				for (int i=0; i<depthBuf->height; i++)
					for (int j=0; j<depthBuf->width; j++)
						depthCont[i*depthBuf->width+j] = depthBuf->hbuf[i][j] ? depthBuf->fbuf[i][j]/(float)depthBuf->hbuf[i][j] : 0.0f;
			}
		}
	}

	// START HERE --------------------------------------------------------------------------------------------------

	// HOT PIXELS
	if (iMod->getDotsEnabled())
	{
		ok = putOpenCLRemoveDots(program, queue, context, imgBuffer, iMod->getDotsRadius(), iMod->getDotsThreshold(), iMod->getDotsDensity(), w, h);
		if (!ok)
		{
			releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
			freeAllocatedBuffers(depthCont);
			return NULL;
		}
	}
	MAKETIMERLOG("kernel hot pixels execution")

	// GRAIN
	if (fMod  &&  fMod->grain > 0)
	{
		ok = putOpenCLGrain(program, queue, context, imgBuffer, fMod->grain, 6347, 6734, w, h);
		if (!ok)
		{
			releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
			freeAllocatedBuffers(depthCont);
			return NULL;
		}
		MAKETIMERLOG("kernel grain execution")
	}

	// FOG
	if (fMod  &&  fMod->fogEnabled  &&  fMod->fogDist>0.0f)
	{
		float fogmpl = 1.0f/iMod->getMultiplier();
		Color4 c1;
		c1.r = pow(fMod->fogColor.r, 2.2f) * fogmpl;
		c1.g = pow(fMod->fogColor.g, 2.2f) * fogmpl;
		c1.b = pow(fMod->fogColor.b, 2.2f) * fogmpl;
		ok = putOpenCLFog(program, queue, context, imgBuffer, c1, fMod->fogDist, fMod->fogSpeed, fMod->fogExclSky, w, h, depthCont);
		if (!ok)
		{
			releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
			freeAllocatedBuffers(depthCont);
			return NULL;
		}
		MAKETIMERLOG("kernel fog execution")
	}

	// RESIZE AA - FILTER
	if (aa>1)
	{
		// ALLOC MEM 2 ------------------
		unsigned int bsize2 = w*h*sizeof(Color4)/(aa*aa);
		cl_mem dstBuffer = clCreateBuffer (context, CL_MEM_READ_WRITE,
					bsize2, NULL, &clOK);
		if (clOK!=CL_SUCCESS)
		{
			Logger::add("Failed creating OpenCL image dst buffer.");
			releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer, &dstBuffer);
			freeAllocatedBuffers(depthCont);
			return NULL;
		}
		int filter = iMod->getFilter();
		switch (filter)
		{
			case 0:
				ok = putOpenCLFilterNone(program, queue, context, imgBuffer, dstBuffer, aa, w, h);
				break;
			case 1:
				ok = putOpenCLFilterBox1(program, queue, context, imgBuffer, dstBuffer, aa, w, h);
				break;
			case 2:
				ok = putOpenCLFilterBox2(program, queue, context, imgBuffer, dstBuffer, aa, w, h);
				break;
			case 3:
				{
					float mpl = iMod->getMultiplier();
					ok = putOpenCLFilterAAW(program, queue, context, imgBuffer, dstBuffer, mpl, aa, w, h);
				}
				break;
			case 4:
			case 5:
			case 6:
			case 7:
				ok = putOpenCLFilterWeightConst(program, queue, context, imgBuffer, dstBuffer, filter, aa, w, h);
				break;
			case 8:
				ok = putOpenCLFilterGaussExtreme(program, queue, context, imgBuffer, dstBuffer, aa, w, h);
				break;
			default:
				ok = putOpenCLFilterBox2(program, queue, context, imgBuffer, dstBuffer, aa, w, h);
				break;
		}

		if (!ok)
		{
			releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer, &dstBuffer);
			freeAllocatedBuffers(depthCont);
			return NULL;
		}

		w = w/aa;
		h = h/aa;
		bsize = w*h*sizeof(Color4);

		clReleaseMemObject(imgBuffer);
		imgBuffer = dstBuffer;

		MAKETIMERLOG("kernel resize execution")
	}
	else
	{
	}

	// VIGNETTE
	int vign = iMod->getVignette();
	if (vign>0)
		ok = putOpenCLVignette(program, queue, context, imgBuffer, vign, 1.0f, w, h, src->vig_orig_width, src->vig_orig_height, src->vig_buck_pos_x, src->vig_buck_pos_y);
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel vignette execution")

	// CHROMATIC ABERRATION PLANAR
	if (fMod)
	{
		float chR = fMod->chromaticPlanarRed;
		float chG = fMod->chromaticPlanarGreen;
		float chB = fMod->chromaticPlanarBlue;
		if (chR>0.0f || chG>0.0f || chB>0.0f)
		{
			ok = putOpenCLChromaticAberrationPlanar(program, queue, context, imgBuffer, chR, chG, chB, w, h);
			if (!ok)
			{
				releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
				freeAllocatedBuffers(depthCont);
				return NULL;
			}
			MAKETIMERLOG("kernel chromatic aberration planar execution")
		}
	}

	// ISO / TONE / GAMMA
	int respNum = iMod->getResponseFunctionNumber();
	float gamma = respNum==0 ? 1.0f/max(0.1f, iMod->getGamma()) : 1.0f;
	ok = putOpenCLISOToneGamma(program, queue, context, imgBuffer, iMod->getMultiplier(), iMod->getToneMappingValue(), gamma, w, h);
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel isotonegamma execution")

	// CAMERA RESPONSE
	if (respNum>0)
		ok = putOpenCLCameraResponse(program, queue, context, imgBuffer, respNum, w, h);
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel response function execution")


	// BRIGHTNESS / CONTRAST
	float bri = iMod->getBrightness();
	float con = iMod->getContrast();
	if (bri!=0.0f  ||  con!=1.0f)
		ok = putOpenCLBrightnessContrast(program, queue, context, imgBuffer, bri, con, w, h);
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel brightness/contrast execution")

	// SATURATION
	float sat = iMod->getSaturation();
	if (sat!=0.0f)
		ok = putOpenCLSaturation(program, queue, context, imgBuffer, sat, w, h);
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel add saturation execution")

	// ADD RGB
	float addR = iMod->getR();
	float addG = iMod->getG();
	float addB = iMod->getB();
	if (addR!=0.0f  ||  addG!=0.0f  ||  addB!=0.0f)
		ok = putOpenCLAddRGB(program, queue, context, imgBuffer, addR, addG, addB, w, h);
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel add rgb execution")

	// CHANGE TEMPERATURE
	float temperature = iMod->getTemperature();
	if (addR!=6000.0f)
		ok = putOpenCLTemperature(program, queue, context, imgBuffer, temperature, w, h);
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel temperature execution")

	float curvearray[640];
	bool useLumi = false;
	bool ok2 = getCurveSettings(curvearray, useLumi);
	if (ok2)
	{
		Logger::add("using curve");
		ok = putOpenCLCurve(program, queue, context, imgBuffer, curvearray, useLumi, w, h);
	}
	if (!ok)
	{
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		return NULL;
	}
	MAKETIMERLOG("kernel curve execution")

	// GET BACK IMAGE
	ImageBuffer * result = new ImageBuffer();
	ok = result->allocBuffer(w,h);
	if (!ok)
	{
		Logger::add("Failed allocating memory for result.");
		releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
		freeAllocatedBuffers(depthCont);
		delete result;
		return NULL;
	}
	MAKETIMERLOG("prepare dst buf")

	copyBufferFromDeviceToImageBuffer(result, queue, imgBuffer);
	MAKETIMERLOG("copy res")

	freeAllocatedBuffers(depthCont);
	releaseOpenCLobjects(NULL, &queue, &program, &context, &imgBuffer);
	MAKETIMERLOG("release shit")

	return result;
}

//---------------------------------------------------------------------------------------------------------------

#endif
