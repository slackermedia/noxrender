#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::lockUnlockAfterStart()
{
	// RENDER
		disableControl(hOutputConf, IDC_REND_OUTPUT_WIDTH);
		disableControl(hOutputConf, IDC_REND_OUTPUT_HEIGHT);
		disableControl(hOutputConf, IDC_REND_OUTPUT_AA);
		disableControl(hOutputConf, IDC_REND_OUTPUT_ASPECT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_ON);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_HH);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_MM);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_SS);
		disableControl(hOutputConf, IDC_REND_OUTPUT_RANDOM_TYPE);
		disableControl(hOutputConf, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC);
		disableControl(hOutputConf, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH);
		disableControl(hOutputConf, IDC_REND_OUTPUT_FAKE_CAUSTIC);
		disableControl(hOutputConf, IDC_REND_OUTPUT_GI_SAMPLES);
		disableControl(hOutputConf, IDC_REND_OUTPUT_RIS_SAMPLES);
		disableControl(hOutputConf, IDC_REND_OUTPUT_CAMERA_CONNECT);
		disableControl(hOutputConf, IDC_REND_OUTPUT_EMITTER_HIT);
		disableControl(hOutputConf, IDC_REND_OUTPUT_BDPT_MAX_REFL);
	// REGIONS
		disableControl(hRegions, IDC_REND_REGIONS_HANDTOOL);
		disableControl(hRegions, IDC_REND_REGIONS_RECT);
		disableControl(hRegions, IDC_REND_REGIONS_AIRBRUSH);
		disableControl(hRegions, IDC_REND_REGIONS_FILL);
		disableControl(hRegions, IDC_REND_REGIONS_CLEAR);
		disableControl(hRegions, IDC_REND_REGIONS_INVERT);
		disableControl(hRegions, IDC_REND_REGIONS_AIRSIZE);
		disableControl(hRegions, IDC_REND_REGIONS_PRIORITY);
	// CAMERA
		disableControl(hCamera, IDC_REND_CAMERA_ISO);
		disableControl(hCamera, IDC_REND_CAMERA_APERTURE);
		disableControl(hCamera, IDC_REND_CAMERA_SHUTTER);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_LENGTH);
		disableControl(hCamera, IDC_REND_CAMERA_PREVIEW_BUTTON);
		disableControl(hCamera, IDC_REND_CAMERA_SETACTIVE);
		disableControl(hCamera, IDC_REND_CAMERA_DELETE_BUFFER);
		disableControl(hCamera, IDC_REND_CAMERA_MB_ENABLED);
		disableControl(hCamera, IDC_REND_CAMERA_FOCUS_DIST);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_15);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_20);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_24);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_28);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_35);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_50);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_85);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_135);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_BALANCE);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_SIZE);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_FLATTEN);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_VIGNETTE);
		disableControl(hCamera, IDC_REND_CAMERA_DOF_TEMP);
	// OBJECTS
		disableControl(hObjects, IDC_REND_OBJECTS_LISTVIEW);
	// MATERIALS
		disableControl(hMaterials, IDC_REND_MATERIALS_LISTVIEW);
		disableControl(hMaterials, IDC_REND_MATERIALS_EDIT_BUTTON);
	// ENVIRONMENT
		enableControl(hEnvironment, IDC_REND_ENV_MAP);
		enableControl(hEnvironment, IDC_REND_ENV_USE_SUNSKY);
		enableControl(hEnvironment, IDC_REND_ENV_LONGITUDE);
		enableControl(hEnvironment, IDC_REND_ENV_LATITUDE);
		enableControl(hEnvironment, IDC_REND_ENV_MONTH);
		enableControl(hEnvironment, IDC_REND_ENV_DAY);
		enableControl(hEnvironment, IDC_REND_ENV_HOUR);
		enableControl(hEnvironment, IDC_REND_ENV_MINUTE);
		enableControl(hEnvironment, IDC_REND_ENV_GMT);
		enableControl(hEnvironment, IDC_REND_ENV_TURBIDITY);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_ON);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_OTHER_LAYER);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_SIZE);
		enableControl(hEnvironment, IDC_REND_ENV_RESET_SUN_SIZE);
		disableControl(hEnvironment, IDC_REND_ENV_TEXTURE);
		disableControl(hEnvironment, IDC_REND_ENV_USE_MAP);
		disableControl(hEnvironment, IDC_REND_ENV_MAP_POWER);
		disableControl(hEnvironment, IDC_REND_ENV_MAP_LAYER);
		disableControl(hEnvironment, IDC_REND_ENV_MAP_AZIMUTH);
	// BLEND
		lockUnlockUsedBlendControls(false);
		disableControl(hBlend, IDC_REND_BLEND_IMPORT);
		disableControl(hBlend, IDC_REND_BLEND_EXPORT);
		disableControl(hBlend, IDC_REND_BLEND_REDRAW_FINAL);
	// POST
		disableControl(hPost, IDC_REND_POST_RESPONSE);
		disableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_GRAPH);
		disableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_PREVIEWS);
		disableControl(hPost, IDC_REND_POST_REDRAW_FINAL);
	// MAINBAR
		enableControl(hMainBar, IDC_REND_MAINBAR_LOADSCENE);
		disableControl(hMainBar, IDC_REND_MAINBAR_MERGE);
		disableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE);
		disableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE_WITH_TEX);
		disableControl(hMainBar, IDC_REND_MAINBAR_START);
		disableControl(hMainBar, IDC_REND_MAINBAR_RESUME);
		disableControl(hMainBar, IDC_REND_MAINBAR_STOP);
		disableControl(hMainBar, IDC_REND_MAINBAR_ZOOMIN);
		disableControl(hMainBar, IDC_REND_MAINBAR_ZOOMOUT);
		disableControl(hMainBar, IDC_REND_MAINBAR_ZOOM100);
		disableControl(hMainBar, IDC_REND_MAINBAR_ZOOMFIT);
		disableControl(hMainBar, IDC_REND_MAINBAR_SAVEIMAGE);
		disableControl(hMainBar, IDC_REND_MAINBAR_REFRESH);
		disableControl(hMainBar, IDC_REND_MAINBAR_SCENE_INFO);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::lockUnlockAfterLoadScene()
{
	// RENDER
		enableControl(hOutputConf, IDC_REND_OUTPUT_WIDTH);
		enableControl(hOutputConf, IDC_REND_OUTPUT_HEIGHT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_AA);
		enableControl(hOutputConf, IDC_REND_OUTPUT_ASPECT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_ON);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_HH);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_MM);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_SS);
		enableControl(hOutputConf, IDC_REND_OUTPUT_ENGINE);
		enableControl(hOutputConf, IDC_REND_OUTPUT_RANDOM_TYPE);
		enableControl(hOutputConf, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC);
		enableControl(hOutputConf, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH);
		enableControl(hOutputConf, IDC_REND_OUTPUT_FAKE_CAUSTIC);
		enableControl(hOutputConf, IDC_REND_OUTPUT_GI_SAMPLES);
		enableControl(hOutputConf, IDC_REND_OUTPUT_RIS_SAMPLES);
		enableControl(hOutputConf, IDC_REND_OUTPUT_CAMERA_CONNECT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_EMITTER_HIT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_BDPT_MAX_REFL);
	// REGIONS
		disableControl(hRegions, IDC_REND_REGIONS_HANDTOOL);
		disableControl(hRegions, IDC_REND_REGIONS_RECT);
		disableControl(hRegions, IDC_REND_REGIONS_AIRBRUSH);
		disableControl(hRegions, IDC_REND_REGIONS_FILL);
		disableControl(hRegions, IDC_REND_REGIONS_CLEAR);
		disableControl(hRegions, IDC_REND_REGIONS_INVERT);
		disableControl(hRegions, IDC_REND_REGIONS_AIRSIZE);
		disableControl(hRegions, IDC_REND_REGIONS_PRIORITY);
	// CAMERA
		enableControl(hCamera, IDC_REND_CAMERA_ISO);
		enableControl(hCamera, IDC_REND_CAMERA_APERTURE);
		enableControl(hCamera, IDC_REND_CAMERA_SHUTTER);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_LENGTH);
		enableControl(hCamera, IDC_REND_CAMERA_PREVIEW_BUTTON);
		enableControl(hCamera, IDC_REND_CAMERA_SETACTIVE);
		enableControl(hCamera, IDC_REND_CAMERA_DELETE_BUFFER);
		enableControl(hCamera, IDC_REND_CAMERA_MB_ENABLED);
		enableControl(hCamera, IDC_REND_CAMERA_FOCUS_DIST);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_15);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_20);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_24);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_28);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_35);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_50);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_85);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_135);
		enableControl(hCamera, IDC_REND_CAMERA_APERTURE_COPY);
		enableControl(hCamera, IDC_REND_CAMERA_BLADERADIUS);
		enableControl(hCamera, IDC_REND_CAMERA_BLADES_ROUND);
		enableControl(hCamera, IDC_REND_CAMERA_ANGLEBLADES);
		enableControl(hCamera, IDC_REND_CAMERA_NUMBLADES);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_BALANCE);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_SIZE);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_FLATTEN);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_VIGNETTE);
		enableControl(hCamera, IDC_REND_CAMERA_DOF_TEMP);
	// OBJECTS
		enableControl(hObjects, IDC_REND_OBJECTS_LISTVIEW);
	// MATERIALS
		enableControl(hMaterials, IDC_REND_MATERIALS_LISTVIEW);
		enableControl(hMaterials, IDC_REND_MATERIALS_EDIT_BUTTON);
	// ENVIRONMENT
		enableControl(hEnvironment, IDC_REND_ENV_MAP);
		enableControl(hEnvironment, IDC_REND_ENV_USE_SUNSKY);
		enableControl(hEnvironment, IDC_REND_ENV_LONGITUDE);
		enableControl(hEnvironment, IDC_REND_ENV_LATITUDE);
		enableControl(hEnvironment, IDC_REND_ENV_MONTH);
		enableControl(hEnvironment, IDC_REND_ENV_DAY);
		enableControl(hEnvironment, IDC_REND_ENV_HOUR);
		enableControl(hEnvironment, IDC_REND_ENV_MINUTE);
		enableControl(hEnvironment, IDC_REND_ENV_GMT);
		enableControl(hEnvironment, IDC_REND_ENV_TURBIDITY);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_ON);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_OTHER_LAYER);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_SIZE);
		enableControl(hEnvironment, IDC_REND_ENV_RESET_SUN_SIZE);
		enableControl(hEnvironment, IDC_REND_ENV_TEXTURE);
		enableControl(hEnvironment, IDC_REND_ENV_USE_MAP);
		enableControl(hEnvironment, IDC_REND_ENV_MAP_POWER);
		enableControl(hEnvironment, IDC_REND_ENV_MAP_LAYER);
		enableControl(hEnvironment, IDC_REND_ENV_MAP_AZIMUTH);
	// BLEND
		lockUnlockUsedBlendControls(true);
		enableControl(hBlend, IDC_REND_BLEND_IMPORT);
		enableControl(hBlend, IDC_REND_BLEND_EXPORT);
		enableControl(hBlend, IDC_REND_BLEND_REDRAW_FINAL);
		enableControl(hBlend, IDC_REND_BLEND_RESET_ALL);
	// POST
		enableControl(hPost, IDC_REND_POST_RESET_ISO);
		enableControl(hPost, IDC_REND_POST_RESET_TONE);
		enableControl(hPost, IDC_REND_POST_RESET_GAMMA);
		enableControl(hPost, IDC_REND_POST_RESET_BRIGHTNESS);
		enableControl(hPost, IDC_REND_POST_RESET_CONTRAST);
		enableControl(hPost, IDC_REND_POST_RESET_SATURATION);
		enableControl(hPost, IDC_REND_POST_RESET_RED);
		enableControl(hPost, IDC_REND_POST_RESET_GREEN);
		enableControl(hPost, IDC_REND_POST_RESET_BLUE);
		enableControl(hPost, IDC_REND_POST_RESET_TEMPERATURE);
		enableControl(hPost, IDC_REND_POST_RESET_VIGNETTE);
		enableControl(hPost, IDC_REND_POST_RESET_GI_COMP);
		enableControl(hPost, IDC_REND_TONE_ISO);
		enableControl(hPost, IDC_REND_TONE_TONE);
		enableControl(hPost, IDC_REND_TONE_GAMMA);
		enableControl(hPost, IDC_REND_POST_CURVE);
		enableControl(hPost, IDC_REND_POST_BRIGHTNESS);
		enableControl(hPost, IDC_REND_POST_CONTRAST);
		enableControl(hPost, IDC_REND_POST_SATURATION);
		enableControl(hPost, IDC_REND_POST_RED);
		enableControl(hPost, IDC_REND_POST_GREEN);
		enableControl(hPost, IDC_REND_POST_BLUE);
		enableControl(hPost, IDC_REND_POST_TEMPERATURE);
		enableControl(hPost, IDC_REND_POST_HOT_PIXELS_ON);
		enableControl(hPost, IDC_REND_POST_VIGNETTE);
		enableControl(hPost, IDC_REND_POST_GI_COMPENSATE);
		enableControl(hPost, IDC_REND_POST_CURVE_RESET);
		enableControl(hPost, IDC_REND_POST_CURVE_RED);
		enableControl(hPost, IDC_REND_POST_CURVE_GREEN);
		enableControl(hPost, IDC_REND_POST_CURVE_BLUE);
		enableControl(hPost, IDC_REND_POST_CURVE_LIGHTNESS);
		enableControl(hPost, IDC_REND_POST_TEMP_DIRECT);
		enableControl(hPost, IDC_REND_POST_TEMP_GI);
		enableControl(hPost, IDC_REND_POST_SHOW_TIMESTAMP);
		enableControl(hPost, IDC_REND_POST_SHOW_LOGO);
		enableControl(hPost, IDC_REND_POST_LOAD);
		enableControl(hPost, IDC_REND_POST_SAVE);
		enableControl(hPost, IDC_REND_POST_FILTER);
		enableControl(hPost, IDC_REND_POST_RESPONSE);
		enableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_GRAPH);
		enableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_PREVIEWS);
		enableControl(hPost, IDC_REND_POST_REDRAW_FINAL);
		enableControl(hPost, IDC_REND_POST_RESET_ALL);
		enableControl(hPost, IDC_REND_POST_SHOW_HISTOGRAM);

	// MAINBAR
		disableControl(hMainBar, IDC_REND_MAINBAR_LOADSCENE);
		enableControl(hMainBar, IDC_REND_MAINBAR_MERGE);
		enableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE);
		enableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE_WITH_TEX);
		enableControl(hMainBar, IDC_REND_MAINBAR_START);
		enableControl(hMainBar, IDC_REND_MAINBAR_RESUME);
		disableControl(hMainBar, IDC_REND_MAINBAR_STOP);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMIN);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMOUT);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOM100);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMFIT);
		enableControl(hMainBar, IDC_REND_MAINBAR_SAVEIMAGE);
		enableControl(hMainBar, IDC_REND_MAINBAR_REFRESH);
		enableControl(hMainBar, IDC_REND_MAINBAR_SCENE_INFO);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::lockUnlockAfterRenderStart()
{
	// RENDER
		disableControl(hOutputConf, IDC_REND_OUTPUT_WIDTH);
		disableControl(hOutputConf, IDC_REND_OUTPUT_HEIGHT);
		disableControl(hOutputConf, IDC_REND_OUTPUT_AA);
		disableControl(hOutputConf, IDC_REND_OUTPUT_ASPECT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_ON);
		disableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_HH);
		disableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_MM);
		disableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_SS);
		disableControl(hOutputConf, IDC_REND_OUTPUT_ENGINE);
		disableControl(hOutputConf, IDC_REND_OUTPUT_RANDOM_TYPE);
		disableControl(hOutputConf, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC);
		disableControl(hOutputConf, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH);
		disableControl(hOutputConf, IDC_REND_OUTPUT_FAKE_CAUSTIC);
		disableControl(hOutputConf, IDC_REND_OUTPUT_GI_SAMPLES);
		disableControl(hOutputConf, IDC_REND_OUTPUT_RIS_SAMPLES);
		disableControl(hOutputConf, IDC_REND_OUTPUT_SHADE);
		disableControl(hOutputConf, IDC_REND_OUTPUT_CAMERA_CONNECT);
		disableControl(hOutputConf, IDC_REND_OUTPUT_EMITTER_HIT);
		disableControl(hOutputConf, IDC_REND_OUTPUT_BDPT_MAX_REFL);
	// REGIONS
		enableControl(hRegions, IDC_REND_REGIONS_HANDTOOL);
		enableControl(hRegions, IDC_REND_REGIONS_RECT);
		enableControl(hRegions, IDC_REND_REGIONS_AIRBRUSH);
		enableControl(hRegions, IDC_REND_REGIONS_FILL);
		enableControl(hRegions, IDC_REND_REGIONS_CLEAR);
		enableControl(hRegions, IDC_REND_REGIONS_INVERT);
		enableControl(hRegions, IDC_REND_REGIONS_AIRSIZE);
		enableControl(hRegions, IDC_REND_REGIONS_PRIORITY);
	// CAMERA
		disableControl(hCamera, IDC_REND_CAMERA_ISO);
		disableControl(hCamera, IDC_REND_CAMERA_APERTURE);
		disableControl(hCamera, IDC_REND_CAMERA_SHUTTER);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_LENGTH);
		disableControl(hCamera, IDC_REND_CAMERA_PREVIEW_BUTTON);
		disableControl(hCamera, IDC_REND_CAMERA_SETACTIVE);
		disableControl(hCamera, IDC_REND_CAMERA_DELETE_BUFFER);
		disableControl(hCamera, IDC_REND_CAMERA_MB_ENABLED);
		disableControl(hCamera, IDC_REND_CAMERA_FOCUS_DIST);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_15);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_20);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_24);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_28);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_35);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_50);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_85);
		disableControl(hCamera, IDC_REND_CAMERA_FOCAL_135);
		disableControl(hCamera, IDC_REND_CAMERA_APERTURE_COPY);
		disableControl(hCamera, IDC_REND_CAMERA_BLADERADIUS);
		disableControl(hCamera, IDC_REND_CAMERA_BLADES_ROUND);
		disableControl(hCamera, IDC_REND_CAMERA_ANGLEBLADES);
		disableControl(hCamera, IDC_REND_CAMERA_NUMBLADES);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_BALANCE);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_SIZE);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_FLATTEN);
		disableControl(hCamera, IDC_REND_CAMERA_BOKEH_VIGNETTE);
		disableControl(hCamera, IDC_REND_CAMERA_DOF_TEMP);


	// OBJECTS
		enableControl(hObjects, IDC_REND_OBJECTS_LISTVIEW);
	// MATERIALS
		enableControl(hMaterials, IDC_REND_MATERIALS_LISTVIEW);
		disableControl(hMaterials, IDC_REND_MATERIALS_EDIT_BUTTON);
	// ENVIRONMENT
		disableControl(hEnvironment, IDC_REND_ENV_MAP);
		disableControl(hEnvironment, IDC_REND_ENV_USE_SUNSKY);
		disableControl(hEnvironment, IDC_REND_ENV_LONGITUDE);
		disableControl(hEnvironment, IDC_REND_ENV_LATITUDE);
		disableControl(hEnvironment, IDC_REND_ENV_MONTH);
		disableControl(hEnvironment, IDC_REND_ENV_DAY);
		disableControl(hEnvironment, IDC_REND_ENV_HOUR);
		disableControl(hEnvironment, IDC_REND_ENV_MINUTE);
		disableControl(hEnvironment, IDC_REND_ENV_GMT);
		disableControl(hEnvironment, IDC_REND_ENV_TURBIDITY);
		disableControl(hEnvironment, IDC_REND_ENV_ALBEDO);
		disableControl(hEnvironment, IDC_REND_ENV_MODEL);
		disableControl(hEnvironment, IDC_REND_ENV_SUN_ON);
		disableControl(hEnvironment, IDC_REND_ENV_SUN_OTHER_LAYER);
		disableControl(hEnvironment, IDC_REND_ENV_SUN_SIZE);
		disableControl(hEnvironment, IDC_REND_ENV_RESET_SUN_SIZE);
		disableControl(hEnvironment, IDC_REND_ENV_TEXTURE);
		disableControl(hEnvironment, IDC_REND_ENV_USE_MAP);
		disableControl(hEnvironment, IDC_REND_ENV_MAP_POWER);
		disableControl(hEnvironment, IDC_REND_ENV_MAP_LAYER);
		disableControl(hEnvironment, IDC_REND_ENV_MAP_AZIMUTH);
	// BLEND
		lockUnlockUsedBlendControls(true);
		enableControl(hBlend, IDC_REND_BLEND_IMPORT);
		enableControl(hBlend, IDC_REND_BLEND_EXPORT);
		disableControl(hBlend, IDC_REND_BLEND_REDRAW_FINAL);
		enableControl(hBlend, IDC_REND_BLEND_RESET_ALL);
	// POST
		enableControl(hPost, IDC_REND_POST_FILTER);
		enableControl(hPost, IDC_REND_POST_RESPONSE);
		enableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_GRAPH);
		enableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_PREVIEWS);
		disableControl(hPost, IDC_REND_POST_REDRAW_FINAL);
		enableControl(hPost, IDC_REND_POST_RESET_ALL);
	// MAINBAR
		disableControl(hMainBar, IDC_REND_MAINBAR_LOADSCENE);
		disableControl(hMainBar, IDC_REND_MAINBAR_MERGE);
		disableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE);
		disableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE_WITH_TEX);
		disableControl(hMainBar, IDC_REND_MAINBAR_START);
		enableControl(hMainBar, IDC_REND_MAINBAR_STOP);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMIN);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMOUT);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOM100);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMFIT);
		disableControl(hMainBar, IDC_REND_MAINBAR_SAVEIMAGE);
		enableControl(hMainBar, IDC_REND_MAINBAR_REFRESH);
		disableControl(hMainBar, IDC_REND_MAINBAR_RESUME);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::lockUnlockAfterRenderEnd()
{
	// RENDER
		enableControl(hOutputConf, IDC_REND_OUTPUT_WIDTH);
		enableControl(hOutputConf, IDC_REND_OUTPUT_HEIGHT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_AA);
		enableControl(hOutputConf, IDC_REND_OUTPUT_ASPECT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_ON);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_HH);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_MM);
		enableControl(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_SS);
		enableControl(hOutputConf, IDC_REND_OUTPUT_ENGINE);
		enableControl(hOutputConf, IDC_REND_OUTPUT_RANDOM_TYPE);
		enableControl(hOutputConf, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC);
		enableControl(hOutputConf, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH);
		enableControl(hOutputConf, IDC_REND_OUTPUT_FAKE_CAUSTIC);
		enableControl(hOutputConf, IDC_REND_OUTPUT_GI_SAMPLES);
		enableControl(hOutputConf, IDC_REND_OUTPUT_RIS_SAMPLES);
		enableControl(hOutputConf, IDC_REND_OUTPUT_SHADE);
		enableControl(hOutputConf, IDC_REND_OUTPUT_CAMERA_CONNECT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_EMITTER_HIT);
		enableControl(hOutputConf, IDC_REND_OUTPUT_BDPT_MAX_REFL);
	// REGIONS
		disableControl(hRegions, IDC_REND_REGIONS_HANDTOOL);
		disableControl(hRegions, IDC_REND_REGIONS_RECT);
		disableControl(hRegions, IDC_REND_REGIONS_AIRBRUSH);
		disableControl(hRegions, IDC_REND_REGIONS_FILL);
		disableControl(hRegions, IDC_REND_REGIONS_CLEAR);
		disableControl(hRegions, IDC_REND_REGIONS_INVERT);
		disableControl(hRegions, IDC_REND_REGIONS_AIRSIZE);
		disableControl(hRegions, IDC_REND_REGIONS_PRIORITY);
	// CAMERA
		enableControl(hCamera, IDC_REND_CAMERA_ISO);
		enableControl(hCamera, IDC_REND_CAMERA_APERTURE);
		enableControl(hCamera, IDC_REND_CAMERA_SHUTTER);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_LENGTH);
		enableControl(hCamera, IDC_REND_CAMERA_PREVIEW_BUTTON);
		enableControl(hCamera, IDC_REND_CAMERA_SETACTIVE);
		enableControl(hCamera, IDC_REND_CAMERA_DELETE_BUFFER);
		enableControl(hCamera, IDC_REND_CAMERA_MB_ENABLED);
		enableControl(hCamera, IDC_REND_CAMERA_FOCUS_DIST);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_15);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_20);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_24);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_28);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_35);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_50);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_85);
		enableControl(hCamera, IDC_REND_CAMERA_FOCAL_135);
		enableControl(hCamera, IDC_REND_CAMERA_APERTURE_COPY);
		enableControl(hCamera, IDC_REND_CAMERA_BLADERADIUS);
		enableControl(hCamera, IDC_REND_CAMERA_BLADES_ROUND);
		enableControl(hCamera, IDC_REND_CAMERA_ANGLEBLADES);
		enableControl(hCamera, IDC_REND_CAMERA_NUMBLADES);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_BALANCE);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_RING_SIZE);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_FLATTEN);
		enableControl(hCamera, IDC_REND_CAMERA_BOKEH_VIGNETTE);
		enableControl(hCamera, IDC_REND_CAMERA_DOF_TEMP);
	// OBJECTS
		enableControl(hObjects, IDC_REND_OBJECTS_LISTVIEW);
	// MATERIALS
		enableControl(hMaterials, IDC_REND_MATERIALS_LISTVIEW);
		enableControl(hMaterials, IDC_REND_MATERIALS_EDIT_BUTTON);
	// ENVIRONMENT
		enableControl(hEnvironment, IDC_REND_ENV_MAP);
		enableControl(hEnvironment, IDC_REND_ENV_USE_SUNSKY);
		enableControl(hEnvironment, IDC_REND_ENV_LONGITUDE);
		enableControl(hEnvironment, IDC_REND_ENV_LATITUDE);
		enableControl(hEnvironment, IDC_REND_ENV_MONTH);
		enableControl(hEnvironment, IDC_REND_ENV_DAY);
		enableControl(hEnvironment, IDC_REND_ENV_HOUR);
		enableControl(hEnvironment, IDC_REND_ENV_MINUTE);
		enableControl(hEnvironment, IDC_REND_ENV_GMT);
		enableControl(hEnvironment, IDC_REND_ENV_TURBIDITY);
		enableControl(hEnvironment, IDC_REND_ENV_ALBEDO);
		enableControl(hEnvironment, IDC_REND_ENV_MODEL);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_ON);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_OTHER_LAYER);
		enableControl(hEnvironment, IDC_REND_ENV_SUN_SIZE);
		enableControl(hEnvironment, IDC_REND_ENV_RESET_SUN_SIZE);
		enableControl(hEnvironment, IDC_REND_ENV_TEXTURE);
		enableControl(hEnvironment, IDC_REND_ENV_USE_MAP);
		enableControl(hEnvironment, IDC_REND_ENV_MAP_POWER);
		enableControl(hEnvironment, IDC_REND_ENV_MAP_LAYER);
		enableControl(hEnvironment, IDC_REND_ENV_MAP_AZIMUTH);
	// BLEND
		lockUnlockUsedBlendControls(true);
		enableControl(hBlend, IDC_REND_BLEND_IMPORT);
		enableControl(hBlend, IDC_REND_BLEND_EXPORT);
		enableControl(hBlend, IDC_REND_BLEND_REDRAW_FINAL);
		enableControl(hBlend, IDC_REND_BLEND_RESET_ALL);
	// POST
		enableControl(hPost, IDC_REND_POST_RESPONSE);
		enableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_GRAPH);
		enableControl(hPost, IDC_REND_POST_RESPONSE_SHOW_PREVIEWS);
		enableControl(hPost, IDC_REND_POST_REDRAW_FINAL);
		enableControl(hPost, IDC_REND_POST_RESET_ALL);
	// MAINBAR
		disableControl(hMainBar, IDC_REND_MAINBAR_LOADSCENE);
		enableControl(hMainBar, IDC_REND_MAINBAR_MERGE);
		enableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE);
		enableControl(hMainBar, IDC_REND_MAINBAR_SAVESCENE_WITH_TEX);
		enableControl(hMainBar, IDC_REND_MAINBAR_START);
		disableControl(hMainBar, IDC_REND_MAINBAR_STOP);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMIN);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMOUT);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOM100);
		enableControl(hMainBar, IDC_REND_MAINBAR_ZOOMFIT);
		enableControl(hMainBar, IDC_REND_MAINBAR_SAVEIMAGE);
		enableControl(hMainBar, IDC_REND_MAINBAR_REFRESH);
		enableControl(hMainBar, IDC_REND_MAINBAR_RESUME);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::lockUnlockAfterRenderPause()
{
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::lockUnlockAfterRenderUnpause()
{
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::lockUnlockUsedBlendControls(bool unlock)
{
	Raytracer * rtr = Raytracer::getInstance();
	int ac = rtr->curScenePtr->sscene.activeCamera;
	Camera * cam = NULL;
	if (ac>-1 && ac<rtr->curScenePtr->cameras.objCount)
		cam = rtr->curScenePtr->cameras[ac];
	int bits = 0;
	if (cam)
		bits = cam->blendBits;
	//1
	if (unlock  &&  (bits&(1<<0)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME1);
		enableControl(hBlend, IDC_REND_BLEND_RESET1);
		enableControl(hBlend, IDC_REND_BLEND_ON1);
		enableControl(hBlend, IDC_REND_BLEND_W1);
		enableControl(hBlend, IDC_REND_BLEND_R1);
		enableControl(hBlend, IDC_REND_BLEND_G1);
		enableControl(hBlend, IDC_REND_BLEND_B1);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME1);
		disableControl(hBlend, IDC_REND_BLEND_RESET1);
		disableControl(hBlend, IDC_REND_BLEND_ON1);
		disableControl(hBlend, IDC_REND_BLEND_W1);
		disableControl(hBlend, IDC_REND_BLEND_R1);
		disableControl(hBlend, IDC_REND_BLEND_G1);
		disableControl(hBlend, IDC_REND_BLEND_B1);
	}
	//2
	if (unlock  &&  (bits&(1<<1)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME2);
		enableControl(hBlend, IDC_REND_BLEND_RESET2);
		enableControl(hBlend, IDC_REND_BLEND_ON2);
		enableControl(hBlend, IDC_REND_BLEND_W2);
		enableControl(hBlend, IDC_REND_BLEND_R2);
		enableControl(hBlend, IDC_REND_BLEND_G2);
		enableControl(hBlend, IDC_REND_BLEND_B2);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME2);
		disableControl(hBlend, IDC_REND_BLEND_RESET2);
		disableControl(hBlend, IDC_REND_BLEND_ON2);
		disableControl(hBlend, IDC_REND_BLEND_W2);
		disableControl(hBlend, IDC_REND_BLEND_R2);
		disableControl(hBlend, IDC_REND_BLEND_G2);
		disableControl(hBlend, IDC_REND_BLEND_B2);
	}
	//3
	if (unlock  &&  (bits&(1<<2)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME3);
		enableControl(hBlend, IDC_REND_BLEND_RESET3);
		enableControl(hBlend, IDC_REND_BLEND_ON3);
		enableControl(hBlend, IDC_REND_BLEND_W3);
		enableControl(hBlend, IDC_REND_BLEND_R3);
		enableControl(hBlend, IDC_REND_BLEND_G3);
		enableControl(hBlend, IDC_REND_BLEND_B3);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME3);
		disableControl(hBlend, IDC_REND_BLEND_RESET3);
		disableControl(hBlend, IDC_REND_BLEND_ON3);
		disableControl(hBlend, IDC_REND_BLEND_W3);
		disableControl(hBlend, IDC_REND_BLEND_R3);
		disableControl(hBlend, IDC_REND_BLEND_G3);
		disableControl(hBlend, IDC_REND_BLEND_B3);
	}
	//4
	if (unlock  &&  (bits&(1<<3)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME4);
		enableControl(hBlend, IDC_REND_BLEND_RESET4);
		enableControl(hBlend, IDC_REND_BLEND_ON4);
		enableControl(hBlend, IDC_REND_BLEND_W4);
		enableControl(hBlend, IDC_REND_BLEND_R4);
		enableControl(hBlend, IDC_REND_BLEND_G4);
		enableControl(hBlend, IDC_REND_BLEND_B4);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME4);
		disableControl(hBlend, IDC_REND_BLEND_RESET4);
		disableControl(hBlend, IDC_REND_BLEND_ON4);
		disableControl(hBlend, IDC_REND_BLEND_W4);
		disableControl(hBlend, IDC_REND_BLEND_R4);
		disableControl(hBlend, IDC_REND_BLEND_G4);
		disableControl(hBlend, IDC_REND_BLEND_B4);
	}
	//5
	if (unlock  &&  (bits&(1<<4)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME5);
		enableControl(hBlend, IDC_REND_BLEND_RESET5);
		enableControl(hBlend, IDC_REND_BLEND_ON5);
		enableControl(hBlend, IDC_REND_BLEND_W5);
		enableControl(hBlend, IDC_REND_BLEND_R5);
		enableControl(hBlend, IDC_REND_BLEND_G5);
		enableControl(hBlend, IDC_REND_BLEND_B5);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME5);
		disableControl(hBlend, IDC_REND_BLEND_RESET5);
		disableControl(hBlend, IDC_REND_BLEND_ON5);
		disableControl(hBlend, IDC_REND_BLEND_W5);
		disableControl(hBlend, IDC_REND_BLEND_R5);
		disableControl(hBlend, IDC_REND_BLEND_G5);
		disableControl(hBlend, IDC_REND_BLEND_B5);
	}
	//6
	if (unlock  &&  (bits&(1<<5)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME6);
		enableControl(hBlend, IDC_REND_BLEND_RESET6);
		enableControl(hBlend, IDC_REND_BLEND_ON6);
		enableControl(hBlend, IDC_REND_BLEND_W6);
		enableControl(hBlend, IDC_REND_BLEND_R6);
		enableControl(hBlend, IDC_REND_BLEND_G6);
		enableControl(hBlend, IDC_REND_BLEND_B6);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME6);
		disableControl(hBlend, IDC_REND_BLEND_RESET6);
		disableControl(hBlend, IDC_REND_BLEND_ON6);
		disableControl(hBlend, IDC_REND_BLEND_W6);
		disableControl(hBlend, IDC_REND_BLEND_R6);
		disableControl(hBlend, IDC_REND_BLEND_G6);
		disableControl(hBlend, IDC_REND_BLEND_B6);
	}
	//7
	if (unlock  &&  (bits&(1<<6)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME7);
		enableControl(hBlend, IDC_REND_BLEND_RESET7);
		enableControl(hBlend, IDC_REND_BLEND_ON7);
		enableControl(hBlend, IDC_REND_BLEND_W7);
		enableControl(hBlend, IDC_REND_BLEND_R7);
		enableControl(hBlend, IDC_REND_BLEND_G7);
		enableControl(hBlend, IDC_REND_BLEND_B7);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME7);
		disableControl(hBlend, IDC_REND_BLEND_RESET7);
		disableControl(hBlend, IDC_REND_BLEND_ON7);
		disableControl(hBlend, IDC_REND_BLEND_W7);
		disableControl(hBlend, IDC_REND_BLEND_R7);
		disableControl(hBlend, IDC_REND_BLEND_G7);
		disableControl(hBlend, IDC_REND_BLEND_B7);
	}
	//8
	if (unlock  &&  (bits&(1<<7)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME8);
		enableControl(hBlend, IDC_REND_BLEND_RESET8);
		enableControl(hBlend, IDC_REND_BLEND_ON8);
		enableControl(hBlend, IDC_REND_BLEND_W8);
		enableControl(hBlend, IDC_REND_BLEND_R8);
		enableControl(hBlend, IDC_REND_BLEND_G8);
		enableControl(hBlend, IDC_REND_BLEND_B8);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME8);
		disableControl(hBlend, IDC_REND_BLEND_RESET8);
		disableControl(hBlend, IDC_REND_BLEND_ON8);
		disableControl(hBlend, IDC_REND_BLEND_W8);
		disableControl(hBlend, IDC_REND_BLEND_R8);
		disableControl(hBlend, IDC_REND_BLEND_G8);
		disableControl(hBlend, IDC_REND_BLEND_B8);
	}
	//9
	if (unlock  &&  (bits&(1<<8)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME9);
		enableControl(hBlend, IDC_REND_BLEND_RESET9);
		enableControl(hBlend, IDC_REND_BLEND_ON9);
		enableControl(hBlend, IDC_REND_BLEND_W9);
		enableControl(hBlend, IDC_REND_BLEND_R9);
		enableControl(hBlend, IDC_REND_BLEND_G9);
		enableControl(hBlend, IDC_REND_BLEND_B9);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME9);
		disableControl(hBlend, IDC_REND_BLEND_RESET9);
		disableControl(hBlend, IDC_REND_BLEND_ON9);
		disableControl(hBlend, IDC_REND_BLEND_W9);
		disableControl(hBlend, IDC_REND_BLEND_R9);
		disableControl(hBlend, IDC_REND_BLEND_G9);
		disableControl(hBlend, IDC_REND_BLEND_B9);
	}
	//10
	if (unlock  &&  (bits&(1<<9)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME10);
		enableControl(hBlend, IDC_REND_BLEND_RESET10);
		enableControl(hBlend, IDC_REND_BLEND_ON10);
		enableControl(hBlend, IDC_REND_BLEND_W10);
		enableControl(hBlend, IDC_REND_BLEND_R10);
		enableControl(hBlend, IDC_REND_BLEND_G10);
		enableControl(hBlend, IDC_REND_BLEND_B10);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME10);
		disableControl(hBlend, IDC_REND_BLEND_RESET10);
		disableControl(hBlend, IDC_REND_BLEND_ON10);
		disableControl(hBlend, IDC_REND_BLEND_W10);
		disableControl(hBlend, IDC_REND_BLEND_R10);
		disableControl(hBlend, IDC_REND_BLEND_G10);
		disableControl(hBlend, IDC_REND_BLEND_B10);
	}
	//11
	if (unlock  &&  (bits&(1<<10)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME11);
		enableControl(hBlend, IDC_REND_BLEND_RESET11);
		enableControl(hBlend, IDC_REND_BLEND_ON11);
		enableControl(hBlend, IDC_REND_BLEND_W11);
		enableControl(hBlend, IDC_REND_BLEND_R11);
		enableControl(hBlend, IDC_REND_BLEND_G11);
		enableControl(hBlend, IDC_REND_BLEND_B11);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME11);
		disableControl(hBlend, IDC_REND_BLEND_RESET11);
		disableControl(hBlend, IDC_REND_BLEND_ON11);
		disableControl(hBlend, IDC_REND_BLEND_W11);
		disableControl(hBlend, IDC_REND_BLEND_R11);
		disableControl(hBlend, IDC_REND_BLEND_G11);
		disableControl(hBlend, IDC_REND_BLEND_B11);
	}
	//12
	if (unlock  &&  (bits&(1<<11)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME12);
		enableControl(hBlend, IDC_REND_BLEND_RESET12);
		enableControl(hBlend, IDC_REND_BLEND_ON12);
		enableControl(hBlend, IDC_REND_BLEND_W12);
		enableControl(hBlend, IDC_REND_BLEND_R12);
		enableControl(hBlend, IDC_REND_BLEND_G12);
		enableControl(hBlend, IDC_REND_BLEND_B12);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME12);
		disableControl(hBlend, IDC_REND_BLEND_RESET12);
		disableControl(hBlend, IDC_REND_BLEND_ON12);
		disableControl(hBlend, IDC_REND_BLEND_W12);
		disableControl(hBlend, IDC_REND_BLEND_R12);
		disableControl(hBlend, IDC_REND_BLEND_G12);
		disableControl(hBlend, IDC_REND_BLEND_B12);
	}
	//13
	if (unlock  &&  (bits&(1<<12)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME13);
		enableControl(hBlend, IDC_REND_BLEND_RESET13);
		enableControl(hBlend, IDC_REND_BLEND_ON13);
		enableControl(hBlend, IDC_REND_BLEND_W13);
		enableControl(hBlend, IDC_REND_BLEND_R13);
		enableControl(hBlend, IDC_REND_BLEND_G13);
		enableControl(hBlend, IDC_REND_BLEND_B13);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME13);
		disableControl(hBlend, IDC_REND_BLEND_RESET13);
		disableControl(hBlend, IDC_REND_BLEND_ON13);
		disableControl(hBlend, IDC_REND_BLEND_W13);
		disableControl(hBlend, IDC_REND_BLEND_R13);
		disableControl(hBlend, IDC_REND_BLEND_G13);
		disableControl(hBlend, IDC_REND_BLEND_B13);
	}
	//14
	if (unlock  &&  (bits&(1<<13)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME14);
		enableControl(hBlend, IDC_REND_BLEND_RESET14);
		enableControl(hBlend, IDC_REND_BLEND_ON14);
		enableControl(hBlend, IDC_REND_BLEND_W14);
		enableControl(hBlend, IDC_REND_BLEND_R14);
		enableControl(hBlend, IDC_REND_BLEND_G14);
		enableControl(hBlend, IDC_REND_BLEND_B14);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME14);
		disableControl(hBlend, IDC_REND_BLEND_RESET14);
		disableControl(hBlend, IDC_REND_BLEND_ON14);
		disableControl(hBlend, IDC_REND_BLEND_W14);
		disableControl(hBlend, IDC_REND_BLEND_R14);
		disableControl(hBlend, IDC_REND_BLEND_G14);
		disableControl(hBlend, IDC_REND_BLEND_B14);
	}
	//15
	if (unlock  &&  (bits&(1<<14)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME15);
		enableControl(hBlend, IDC_REND_BLEND_RESET15);
		enableControl(hBlend, IDC_REND_BLEND_ON15);
		enableControl(hBlend, IDC_REND_BLEND_W15);
		enableControl(hBlend, IDC_REND_BLEND_R15);
		enableControl(hBlend, IDC_REND_BLEND_G15);
		enableControl(hBlend, IDC_REND_BLEND_B15);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME15);
		disableControl(hBlend, IDC_REND_BLEND_RESET15);
		disableControl(hBlend, IDC_REND_BLEND_ON15);
		disableControl(hBlend, IDC_REND_BLEND_W15);
		disableControl(hBlend, IDC_REND_BLEND_R15);
		disableControl(hBlend, IDC_REND_BLEND_G15);
		disableControl(hBlend, IDC_REND_BLEND_B15);
	}
	//16
	if (unlock  &&  (bits&(1<<15)))
	{
		enableControl(hBlend, IDC_REND_BLEND_NAME16);
		enableControl(hBlend, IDC_REND_BLEND_RESET16);
		enableControl(hBlend, IDC_REND_BLEND_ON16);
		enableControl(hBlend, IDC_REND_BLEND_W16);
		enableControl(hBlend, IDC_REND_BLEND_R16);
		enableControl(hBlend, IDC_REND_BLEND_G16);
		enableControl(hBlend, IDC_REND_BLEND_B16);
	}
	else
	{
		disableControl(hBlend, IDC_REND_BLEND_NAME16);
		disableControl(hBlend, IDC_REND_BLEND_RESET16);
		disableControl(hBlend, IDC_REND_BLEND_ON16);
		disableControl(hBlend, IDC_REND_BLEND_W16);
		disableControl(hBlend, IDC_REND_BLEND_R16);
		disableControl(hBlend, IDC_REND_BLEND_G16);
		disableControl(hBlend, IDC_REND_BLEND_B16);
	}

}

//----------------------------------------------------------------------------------------------------------------
