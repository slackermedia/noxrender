#define _CRT_RAND_S
#include "MatEditor.h"
#include "resource.h"
#include "XML_IO.h"
#include "log.h"
#include "MatDownload.h"
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include <iostream>
#include "log.h"

bool materialDownloaderProgressCallback(void * obj, float progress);
void jpgThreadDoneCallback(void * obj, bool OK);
void xmlThreadDoneCallback(void * obj, bool OK);
size_t write_data (void *ptr, size_t size, size_t nmemb, void *stream);	// <- check in NOXupdate.cpp

//------------------------------------------------------------------------------------------------------

OnlineMatLib::OnlineMatLib():
	toDel(0)
{
	int queryMode = MODE_CATEGORY;
	currentCategory = 1;
	currentPage = 0;
	tempDir[0] = 0;
	for (int i=0; i<7; i++)
		files[i][0] = 0;
	xmlFilename[0] = 0;
	totalResults = 0;
	username = NULL;
	passwordhash = NULL;
	remember = false;
	invalidUserPass = false;

}

//------------------------------------------------------------------------------------------------------

OnlineMatLib::OnlineMatInfo::OnlineMatInfo()
{
	filesize = 0;
	category = 0;
	layers = 0;
	textures = 0;
	id = -1;
	name = NULL;
	filename = NULL;
	imagename = NULL;
	author = NULL;
	description = NULL;
}

//------------------------------------------------------------------------------------------------------

OnlineMatLib::OnlineMatInfo::~OnlineMatInfo()
{
	clearAll();
}

//------------------------------------------------------------------------------------------------------

void OnlineMatLib::OnlineMatInfo::clearAll()
{
	filesize = 0;
	category = 0;
	layers = 0;
	textures = 0;
	id = -1;
	if (name)
		free(name);
	if (filename)
		free(filename);
	if (imagename)
		free(imagename);
	if (author)
		free(author);
	if (description)
		free(description);
	name = NULL;
	filename = NULL;
	imagename = NULL;
	author = NULL;
	description = NULL;
}

//------------------------------------------------------------------------------------------------------

bool OnlineMatLib::OnlineMatInfo::copyFromOther(OnlineMatLib::OnlineMatInfo * other)
{
	CHECK(other);
	char * newname			= copyString(other->name);
	char * newfilename		= copyString(other->filename);
	char * newimagename		= copyString(other->imagename);
	char * newauthor		= copyString(other->author);
	char * newdescription	= copyString(other->description);
	clearAll();
	name = newname;
	filename = newfilename;
	imagename = newimagename;
	author = newauthor;
	description = newdescription;
	filesize = other->filesize;
	category = other->category;
	layers = other->layers;
	textures = other->textures;
	id = other->id;
	return true;
}

//------------------------------------------------------------------------------------------------------

bool OnlineMatLib::createTempDir()
{
	char temppath[2048];
	int ll = GetTempPath(2048, temppath);
	if (ll > 2048)
		return false;
	if (ll < 3)
		return false;

	char temppathlong[2048];
	int ll2 = GetLongPathName(temppath, temppathlong, 2048);
	if (ll2 > 2048)
		return false;

	if (temppathlong[ll2-1] == '\\'  ||  temppathlong[ll2-1] == '/')
		temppathlong[ll2-1] = 0;

	if (!dirExists(temppathlong))
		return false;

	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(temppathlong, "nox", unique, tempDir);

	if (strlen(tempDir) < strlen(temppathlong))
		return false;

	CreateDirectory(tempDir, NULL);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool OnlineMatLib::generateTempFilename(int num)
{
	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(tempDir, "jpg", unique, files[num]);
	return true;
}

//------------------------------------------------------------------------------------------------------

void OnlineMatLib::stopAllDownloads()
{
	for (int i=0; i<7; i++)
		downloaders[i].breakDownloading = true;
}

//------------------------------------------------------------------------------------------------------

bool materialDownloaderProgressCallback(void * obj, float progress)
{
	if (!obj)
		return true;
	EMPView * empv = (EMPView *)obj;
	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();
	if (!empv->byteBuff->imgBuf)
		empv->byteBuff->allocBuffer(96,96);

	for (int y=0; y<96; y++)
		for (int x=0; x<96; x++)
			empv->byteBuff->imgBuf[y][x] = RGB(0,0,0);

	for (int x=6; x<90; x++)
		empv->byteBuff->imgBuf[43][x] = RGB(255,255,255);
	for (int x=6; x<90; x++)
		empv->byteBuff->imgBuf[52][x] = RGB(255,255,255);
	for (int y=44; y<52; y++)
	{
		empv->byteBuff->imgBuf[y][6] = RGB(255,255,255);
		empv->byteBuff->imgBuf[y][89] = RGB(255,255,255);
	}

	for (int y=45; y<51; y++)
		for (int x=8; x<progress*80+8; x++)
			empv->byteBuff->imgBuf[y][x] = RGB(255,255,255);
	InvalidateRect(empv->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------------

void jpgThreadDoneCallback(void * obj, bool OK)
{
	int i = (int)(INT_PTR)obj;
	HWND hP = GetDlgItem(MatEditorWindow::hMain, MatEditorWindow::libIDs[i]);
	EMPView * empv = GetEMPViewInstance(hP);
	if (!empv)
		return;

	if (!OK)
	{
		empv->byteBuff->clearBuffer();
		InvalidateRect(empv->hwnd, NULL, false);
		return;
	}

	ImageByteBuffer imb;
	imb.loadFromWindowsFormat(MatEditorWindow::onlinematlib.files[i]);
	if (imb.width<1 || imb.height<1)
	{
		empv->byteBuff->clearBuffer();
		InvalidateRect(empv->hwnd, NULL, false);
		return;
	}

	int ww = max(imb.width, imb.height);

	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();
	if (!empv->byteBuff->imgBuf)
		empv->byteBuff->allocBuffer(96,96);
	if (empv->byteBuff->width != 96  ||  empv->byteBuff->height != 96)
		empv->byteBuff->allocBuffer(96,96);

	ImageByteBuffer * aBuff = imb.copyWithZoom(96.0/ww);

	for (int y=0; y<96; y++)
		for (int x=0; x<96; x++)
		{
			if (y < aBuff->height  &&  x < aBuff->width)
				empv->byteBuff->imgBuf[y][x] = aBuff->imgBuf[y][x];
			else
				empv->byteBuff->imgBuf[y][x] = RGB(0,0,0);
		}

	imb.freeBuffer();
	aBuff->freeBuffer();
	delete aBuff;

	InvalidateRect(empv->hwnd, NULL, false);

}

//------------------------------------------------------------------------------------------------------

void xmlThreadDoneCallback(void * obj, bool OK)
{
	if (!OK)
	{
		Logger::add("Download materials info error (1)");
		return;
	}
	if (!obj)
	{
		Logger::add("Download materials info error (2)");
		return;
	}

	MatEditorWindow::onlinematlib.totalResults = 0;


	XMLonlineMatInfo xmlMat;
	bool ok = xmlMat.loadFile((char*)obj);

	if (ok)
	{
		for (int i=0; i<7; i++)
		{
			MatEditorWindow::onlinematlib.mat7Info[i].copyFromOther(&(xmlMat.infos[i]));
			EMPView * empv = GetEMPViewInstance(GetDlgItem(MatEditorWindow::hMain, MatEditorWindow::libIDs[i]));
			if (empv)
				empv->setToolTip("empty");
			MatEditorWindow::onlinematlib.downloaders[i].registerDownloadProgressCallback(NULL, NULL);
		}

		MatEditorWindow::onlinematlib.totalResults = xmlMat.totalResults;
		if (xmlMat.totalResults < 0)
		{								// count unknown - better unlock 
			enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_RIGHT);
			enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_LEFT);
		}
		else
		{
			if (xmlMat.totalResults > (MatEditorWindow::onlinematlib.currentPage+1)*7)			// check NEXT button
				enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_RIGHT);
			else
				disableControl(MatEditorWindow::hMain, IDC_MATEDIT_LIB_RIGHT);
			if (MatEditorWindow::onlinematlib.currentPage > 0)									// check PREV button
				enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_LEFT);
			else
				disableControl(MatEditorWindow::hMain, IDC_MATEDIT_LIB_LEFT);
		}

		MatEditorWindow::setPageControl(MatEditorWindow::onlinematlib.currentPage, max(0, MatEditorWindow::onlinematlib.totalResults-1)/7+1);


		for (int i=0; i<xmlMat.numMats; i++)
		{
			MatEditorWindow::onlinematlib.downloaders[i].breakDownloading = false;
			EMPView * empv = GetEMPViewInstance(GetDlgItem(MatEditorWindow::hMain, MatEditorWindow::libIDs[i]));
			if (empv)
			{
				char tempstring[2048];
				sprintf_s(tempstring, 2048, "Name: %s\nAuthor: %s\nCategory: %s\nLayers: %d\nTextures: %d\nFile size: %d bytes\nDescription:\n%s", 
							MatEditorWindow::onlinematlib.mat7Info[i].name,
							MatEditorWindow::onlinematlib.mat7Info[i].author,
							MatCategory::getCategoryName(MatEditorWindow::onlinematlib.mat7Info[i].category),
							MatEditorWindow::onlinematlib.mat7Info[i].layers,
							MatEditorWindow::onlinematlib.mat7Info[i].textures,
							MatEditorWindow::onlinematlib.mat7Info[i].filesize,
							MatEditorWindow::onlinematlib.mat7Info[i].description ? MatEditorWindow::onlinematlib.mat7Info[i].description : "");
				empv->setToolTip(tempstring);

				MatEditorWindow::onlinematlib.generateTempFilename(i);

				char * url = (char*)malloc(2048);
				sprintf_s(url, 2048, "www.evermotion.org/files/nox/materials/images/%s", MatEditorWindow::onlinematlib.mat7Info[i].imagename);

				MatEditorWindow::onlinematlib.downloaders[i].registerDownloadProgressCallback(materialDownloaderProgressCallback, empv);
				MatEditorWindow::onlinematlib.downloaders[i].registerThreadFinishedCallback(jpgThreadDoneCallback, (void*)(INT_PTR)i);
				MatEditorWindow::onlinematlib.downloaders[i].downloadFileThread(url, MatEditorWindow::onlinematlib.files[i]);

				char * tdel = copyString(MatEditorWindow::onlinematlib.files[i]);
				if (tdel)
					MatEditorWindow::onlinematlib.toDel.add(tdel);
			}
		}
	}
	else
	{
		Logger::add("XML containing material info not found or error in file...");
	}
}

//------------------------------------------------------------------------------------------------------

void OnlineMatLib::deleteAllTempFiles(bool delDir)
{
	char delinfobuf[2048];
	toDel.createArray();
	for (int i=0; i<toDel.objCount; i++)
	{
		if (toDel[i])
		{
			if (fileExists(toDel[i]))
			{
				sprintf_s(delinfobuf, 2048, "  Deleting %s", toDel[i]);
				Logger::add(delinfobuf);
				DeleteFile(toDel[i]);
			}
			free(toDel[i]);
		}
	}

	if (delDir  &&  dirExists(tempDir))
	{
		sprintf_s(delinfobuf, 2048, "  Deleting %s", tempDir);
		Logger::add(delinfobuf);
		RemoveDirectory(tempDir);
	}
	tempDir[0] = 0;

	toDel.freeList();
	toDel.destroyArray();
}

//------------------------------------------------------------------------------------------------------

void OnlineMatLib::clearPreviewControls(bool refresh)
{
	for (int i=0; i<7; i++)
	{
		EMPView * empv = GetEMPViewInstance(GetDlgItem(MatEditorWindow::hMain, MatEditorWindow::libIDs[i]));
		if (!empv)
			continue;
		empv->byteBuff->clearBuffer();
		empv->setToolTip("empty");
		empv->colBorder = GlobalWindowSettings::colorSchemes.EMPViewBorder;
		if (refresh)
			InvalidateRect(empv->hwnd, NULL, false);
	}
}

//------------------------------------------------------------------------------------------------------

bool OnlineMatLib::downloadXMLinfoByCategory(int category, int page)
{
	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(tempDir, "xml", unique, xmlFilename);
	if (strlen(xmlFilename) < strlen(tempDir))
		return false;

	totalResults = 0;

	currentCategory = category;
	currentPage = page;
	queryMode = MODE_CATEGORY;

	char * url = (char*)malloc(2048);
	sprintf_s(url, 2048, "www.evermotion.org/nox/noxGetMaterialsList/%d/%d/", page, category);

	char * tXMLfilename = copyString(xmlFilename);
	toDel.add(copyString(tXMLfilename));

	clearPreviewControls();

	MatDownloader * xmlDownload = new MatDownloader();
	xmlDownload->registerThreadFinishedCallback(xmlThreadDoneCallback, tXMLfilename);
	xmlDownload->downloadFileThread(url, tXMLfilename);

	// terminate preview threads
	for (int i=0; i<7; i++)
	{
		if (downloaders[i].threadHandle)
		{
			TerminateThread(downloaders[i].threadHandle, 1);
			CloseHandle(downloaders[i].threadHandle);
			downloaders[i].threadHandle = 0;
		}
	}

	return true;
}

//------------------------------------------------------------------------------------------------------

bool OnlineMatLib::downloadXMLinfoBySearch(char * text, int page)
{
	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(tempDir, "xml", unique, xmlFilename);
	if (strlen(xmlFilename) < strlen(tempDir))
		return false;

	totalResults = 0;

	CHECK(text);
	CHECK(strlen(text)>1);
	for (int i=0; i<(int)strlen(text); i++)
		if (text[i]==' ')
			text[i] = ';';

	char cpage[16];
	sprintf_s(cpage, 16, "%d", page);

	Logger::add(cpage);

	currentPage = page;
	queryMode = MODE_SEARCH;

	char * url = (char*)malloc(2048);
	sprintf_s(url, 2048, "www.evermotion.org/nox/noxSearch/%d",page);

	char * tXMLfilename = copyString(xmlFilename);
	toDel.add(copyString(tXMLfilename));

	clearPreviewControls();

	// terminate preview threads
	for (int i=0; i<7; i++)
	{
		if (downloaders[i].threadHandle)
		{
			TerminateThread(downloaders[i].threadHandle, 1);
			CloseHandle(downloaders[i].threadHandle);
			downloaders[i].threadHandle = 0;
		}
	}

	CURL *curl;
	CURLcode res;

	FILE * file = NULL;
	if (!fopen_s(&file, tXMLfilename, "wb"))
		return false;

	curl = curl_easy_init(); 

	curl_easy_setopt(curl, CURLOPT_POST, true); 

	struct curl_httppost *post=NULL;
	struct curl_httppost *last=NULL;
	curl_formadd(&post, &last,
			CURLFORM_COPYNAME, "keys",
			CURLFORM_COPYCONTENTS, text, CURLFORM_END);

	curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
	curl_easy_setopt(curl, CURLOPT_URL, url); 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); 

	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1); 
	curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, 0);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);

	res = curl_easy_perform(curl); 
	
	curl_easy_cleanup(curl); 
	fclose(file);

	xmlThreadDoneCallback(tXMLfilename, res==0);

	CHECK(res==0);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool OnlineMatLib::downloadXMLinfoByUsername(int page)
{
	if (!MatEditorWindow::onlinematlib.username)
	{
		bool res = MatEditorWindow::invokeUserPassDialog();
		if (!res)
			return false;
	}

	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(tempDir, "xml", unique, xmlFilename);
	if (strlen(xmlFilename) < strlen(tempDir))
		return false;

	totalResults = 0;

	currentPage = page;
	queryMode = MODE_MY_MATS;

	char * url = (char*)malloc(2048);
	sprintf_s(url, 2048, "www.evermotion.org/nox/noxGetMaterialsList/%d/-1/%s", page, MatEditorWindow::onlinematlib.username);

	char * tXMLfilename = copyString(xmlFilename);
	toDel.add(copyString(tXMLfilename));

	clearPreviewControls();

	// terminate preview threads
	for (int i=0; i<7; i++)
	{
		if (downloaders[i].threadHandle)
		{
			TerminateThread(downloaders[i].threadHandle, 1);
			CloseHandle(downloaders[i].threadHandle);
			downloaders[i].threadHandle = 0;
		}
	}

	MatDownloader * xmlDownload = new MatDownloader();
	xmlDownload->registerThreadFinishedCallback(xmlThreadDoneCallback, tXMLfilename);
	xmlDownload->downloadFileThread(url, tXMLfilename);

	return true;
}


