#include <math.h>
#include "raytracer.h"
#include <stdio.h>
#include "Metropolis.h"
#include "PathTracing.h"
#include "BDPT.h"
#include "log.h"
#include <psapi.h>

extern HMODULE hDllModule;
CRITICAL_SECTION ThreadManager::cs;

extern clock_t localticks;
extern clock_t renderticks;
extern clock_t bspticks;

ThreadManager::ThreadManager()
{
	threadsInitiated = false;
	numThreads = 0;
	threads = 0;
	cameraFifoThreadRunning = false;
	fifoWait = 1;
	notifyStoppedCallback = NULL;
}

ThreadManager::~ThreadManager()
{
	disposeThreads();
}

int ThreadManager::getNumThreads()
{
	return numThreads;
}

int ThreadManager::assignNumber()
{
	assignedNumbers++;
	return assignedNumbers-1;
}


bool ThreadManager::initThreads(int numberThreads, bool inMatEditor)
{
	if (threadsInitiated)
		disposeThreads();

	assignedNumbers = 0;

	if (numberThreads > 0)
	{
		threads = new RenderThread[numberThreads];
		if (!threads)
			return false;
		numThreads = numberThreads;
		threadsInitiated = true;
	}
	for (int i=0; i<numberThreads; i++)
	{
		threads[i].modeMatEditor = inMatEditor;
		threads[i].my_manager = this;
		threads[i].ss.perfCounter = 0;
	}
	return true;
}

bool ThreadManager::disposeThreads()
{
	stopThreads();
	threadsInitiated = false;
	bool allDead = false;
	int ntry = 0;
	while(!allDead)
	{
		ntry++;
		allDead = true;
		for (int i=0; i<numThreads; i++)
		{
			if (threads[i].isAlive())
			{
				allDead = false;
			}
			else
			{
			}
		}
		Sleep(100);
		showThreadsDlls(ntry);

		if (ntry%150==0)
		{
			int res = MessageBox(0, "Some rendering threads didn't stop in 15 seconds.\nDo \
you want to kill them?\nMemory will not be released properly, but \
you can save scene and not lose results.", "Warning", MB_YESNO | MB_ICONEXCLAMATION);
			if (res == IDYES)
			{
				Logger::add("Killing threads");

				for (int i=0; i<numThreads; i++)
				{
					TerminateThread(threads[i].hThread, 0);
					threads[i].resetAfterDie();
				}

			}

			int v1 = NOX_VER_MAJOR;
			int v2 = NOX_VER_MINOR;
			int v3 = NOX_VER_BUILD;
			int v4 = NOX_VER_COMP_NO;
			char vbuf[128];
			#ifdef _WIN64
				sprintf_s(vbuf, 128, "NOX Version %d.%d.%d.%d x64", v1, v2, v3, v4);
			#else
				sprintf_s(vbuf, 128, "NOX Version %d.%d.%d.%d x86", v1, v2, v3, v4);
			#endif
			Logger::add(vbuf);
		}
	}
	delete [] threads;
	threads = 0;
	numThreads = 0;
	threadsInitiated = false;
	cameraFifoThreadRunning = false;
	return true;
}

bool ThreadManager::runThreads()
{
	if (!threadsInitiated)
		return false;
	orderToStop = false;
	int i;
	for (i=0; i<numThreads; i++)
		threads[i].runThread();
	createCameraFifoThread();
	return true;
}

bool ThreadManager::pauseThreads()
{
	fifoWait = 10;
	if (!threadsInitiated)
		return false;
	int i;
	for (i=0; i<numThreads; i++)
		threads[i].pauseThread();
	return true;
}

bool ThreadManager::resumeThreads()
{
	fifoWait = 5;
	if (!threadsInitiated)
		return false;
	int i;
	for (i=0; i<numThreads; i++)
		threads[i].resumeThread();
	return true;
}

bool ThreadManager::stopThreads()
{
	if (!threadsInitiated)
		return false;
	orderToStop = true;
	int i;
	for (i=0; i<numThreads; i++)
		threads[i].stopThread();
	return true;
}

void ThreadManager::reportFinishedWork()
{
	bool all_done = true;
	for (int i=0; i<numThreads; i++)
	{
		if (threads[i].isAlive())
			all_done = false;
	}

	if (all_done)
	{
		if (!orderToStop)
		{
			if (notifyStoppedCallback)
				(*notifyStoppedCallback)();
		}

	}
}

void ThreadManager::registerStoppedCallback(void (*stopped_callback)(void))
{
	notifyStoppedCallback = stopped_callback;
}

void ThreadManager::showAllDlls()
{
	MODULEINFO modinfo;
	HANDLE hProcess = GetCurrentProcess();

	Logger::add("List of opened modules:");
    HMODULE hMods[1024];
    DWORD cbNeeded;
    if( EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
    {
        for (unsigned int i=0; i<(cbNeeded/sizeof(HMODULE)); i++)
        {
            char szModName[MAX_PATH];
			char line1[MAX_PATH+256];
			BOOL mOK1 = GetModuleInformation(hProcess, hMods[i], &modinfo, sizeof(modinfo));
            BOOL mOK2 = GetModuleFileNameEx( hProcess, hMods[i], szModName, sizeof(szModName));
			unsigned long long addr = getImageBase(szModName);
			sprintf_s(line1, MAX_PATH+256, "%2i.  %p - %p    -    %s   (local : %p)", (i+1), 
				modinfo.lpBaseOfDll, 
				(LPVOID)((long long)modinfo.lpBaseOfDll+modinfo.SizeOfImage), 
				szModName,
				addr);
			Logger::add(line1);
        }
    }

}

void ThreadManager::showThreadsDlls(int ntry)
{
	MODULEINFO modinfo;
	HANDLE hProcess = GetCurrentProcess();

    HMODULE hMods[1024];
    DWORD cbNeeded;
	BOOL enumOK = EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded);

	for (int j=0; j<numThreads; j++)
	{
		CONTEXT tContext;
		bool cOK = false;
		tContext.ContextFlags = CONTEXT_CONTROL;
		for (int i=0; i<20; i++)		// try a few times
			if (GetThreadContext(threads[j].hThread, &tContext))
			{
				cOK = true;
				break;
			}
		if (cOK)
		{
			INT_PTR instr_ptr;
			#ifdef _WIN64
				instr_ptr = tContext.Rip;
			#else
				instr_ptr = tContext.Eip;
			#endif

			bool gotModule = false;
			for (unsigned int i=0; i<(cbNeeded/sizeof(HMODULE)); i++)
			{
				BOOL mOK1 = GetModuleInformation(hProcess, hMods[i], &modinfo, sizeof(modinfo));
				if (instr_ptr >= (long long)modinfo.lpBaseOfDll   &&   instr_ptr < (long long)modinfo.lpBaseOfDll+modinfo.SizeOfImage)
				{
					char szModName[MAX_PATH];
					char line1[MAX_PATH+256];
					BOOL mOK2 = GetModuleFileNameEx( hProcess, hMods[i], szModName, sizeof(szModName));
					unsigned long long laddr = getImageBase(szModName);
					if (laddr>0)
					{
						INT_PTR naddr = instr_ptr - (INT_PTR)(modinfo.lpBaseOfDll) + (INT_PTR)laddr;
						if (ntry>0)
							sprintf_s(line1, MAX_PATH+256, "thread %d: (try %d)  cs=%p   in module %s  (local : %p)", (j+1), ntry, instr_ptr, szModName, naddr);
						else
							sprintf_s(line1, MAX_PATH+256, "thread %d: cs=%p   in module %s  (local : %p)", (j+1), instr_ptr, szModName, naddr);
					}
					else
					{
						if (ntry>0)
							sprintf_s(line1, MAX_PATH+256, "thread %d: (try %d)  cs=%p   in module %s", (j+1), ntry, instr_ptr, szModName);
						else
							sprintf_s(line1, MAX_PATH+256, "thread %d: cs=%p   in module %s", (j+1), instr_ptr, szModName);
					}
					Logger::add(line1);
					gotModule = true;
				}
			}
			if (!gotModule)
			{
				char line1[256];
				if (ntry>0)
					sprintf_s(line1, 256, "thread %d: (try %d)  cs=%p  not found in any module", (j+1), ntry, instr_ptr);
				else
					sprintf_s(line1, 256, "thread %d: cs=%p  not found in any module", (j+1), instr_ptr);
				Logger::add(line1);
			}
		}
		else
		{
			char line1[256];
			sprintf_s(line1, 256, "thread %d: (try %d)  GetThreadContext() failed", (j+1), ntry);
			Logger::add(line1);
		}
	}
}

INT_PTR ThreadManager::getImageBase(char * filename)
{
	HANDLE filehandle = CreateFile((const char*)filename, 
                        GENERIC_READ,FILE_SHARE_READ, 
                        NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,
                        NULL); 

	if (INVALID_HANDLE_VALUE == filehandle) 
		return 0;

	HANDLE mhand =  CreateFileMapping(filehandle,0,PAGE_READONLY,0,0,0);
	IMAGE_DOS_HEADER *mz = (IMAGE_DOS_HEADER *)MapViewOfFile(mhand,FILE_MAP_READ,0,0,0);
	if (mz->e_magic != IMAGE_DOS_SIGNATURE)
	{
		CloseHandle(filehandle);
		return 0;
	}
	IMAGE_DOS_HEADER *dos = (IMAGE_DOS_HEADER *)((ULONG_PTR) mz);
	IMAGE_NT_HEADERS *nt = (IMAGE_NT_HEADERS *)((ULONG_PTR) mz + (mz->e_lfanew));
	CloseHandle(filehandle);
	if (nt->Signature != IMAGE_NT_SIGNATURE)
		return 0;
	return nt->OptionalHeader.ImageBase;
}



void ThreadManager::getNextHaltonCoords(float &x, float &y)
{
	Raytracer * rtr = Raytracer::getInstance();

	EnterCriticalSection(&cs);

	rtr->curScenePtr->cameras.objArray[rtr->curScenePtr->sscene.activeCamera]->getHaltonCoords(x, y);

	LeaveCriticalSection(&cs);
}

void ThreadManager::addPixelColor(int x, int y, const Color4 &c, bool incrementHits)
{
	Raytracer * rtr = Raytracer::getInstance();

	EnterCriticalSection(&cs);

	EPixel ep(x,y,c);

	while (fifo.isFull())
		Sleep(2);

	fifo.putObject(ep);

	LeaveCriticalSection(&cs);
}

void ThreadManager::blockSemaphoreFromOutside()
{
	EnterCriticalSection(&cs);
}

void ThreadManager::releaseSemaphoreFromOutside()
{
	LeaveCriticalSection(&cs);
}

void ThreadManager::setThreadsPriority(int pri)
{
	int i;
	for (i=0; i<numThreads; i++)
		threads[i].setPriority(pri);
}

void ThreadManager::createCameraFifoThread()
{
	DWORD threadId;
	cameraFifoThreadRunning = true;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(ThreadManager::cameraFifoThreadProc), (LPVOID)this, 0, &threadId);
}

DWORD WINAPI ThreadManager::cameraFifoThreadProc(LPVOID lpParameter)
{
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->cameras[rtr->curScenePtr->sscene.activeCamera];

	while (rtr->curScenePtr->thrM->cameraFifoThreadRunning)
	{
		while (rtr->curScenePtr->thrM->fifo.isEmpty())
		{
			Sleep(rtr->curScenePtr->thrM->fifoWait);
			if (!rtr->curScenePtr->thrM->cameraFifoThreadRunning)
				return true;
		}

		EPixel ep = rtr->curScenePtr->thrM->fifo.getObject();
	
		cam->imgBuff->addPixelColor(ep.x, ep.y, ep.c, true);
	}
	return true;
}

int ThreadManager::getProcessorsNumber()
{
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	if (si.dwNumberOfProcessors > 0)
		return si.dwNumberOfProcessors;
	else
		return 1;
}

unsigned long long ThreadManager::collectPerfCounters()
{
	unsigned long long res = 0;
	for (int i=0; i<numThreads; i++)
		res += threads[i].getPerformanceCounter();
	return res;
}

unsigned long long ThreadManager::collectGISamplesCount()
{
	unsigned long long res = 0;
	for (int i=0; i<numThreads; i++)
		res += threads[i].ss.giSamplesCount;
	return res;
}

//------------------------------------------------------------------------------------------------

bool RenderThread::runThread()
{
	running = true;
	alive = true;

	Scene * sc =  Raytracer::getInstance()->curScenePtr;
	personalNumber = Raytracer::getInstance()->curScenePtr->thrM->assignNumber();
	Camera * cam = sc->cameras[sc->sscene.activeCamera];
	cam->setSeed(cam->maxHalton);

	hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(RenderThread::ThreadProc),
            (LPVOID)this,
            0,
            &threadId);

	SetThreadPriority(hThread, -15);
	SetThreadAffinityMask(hThread, 1<<personalNumber);
	Sleep(100);

	return true;
}

bool RenderThread::stopThread()
{
	running = false;
	return true;
}

bool RenderThread::pauseThread()
{
	paused = true;
	return true;
}

bool RenderThread::resumeThread()
{
	paused = false;
	return true;
}


RenderThread::RenderThread()
{
	hThread = 0;
	alive = false;
	running = false;
	paused = false;
	modeMatEditor = false;
	my_manager = NULL;
}

RenderThread::~RenderThread()
{
	resumeThread();
	stopThread();
	while (alive)
	{
		Sleep(10);
	}
	CloseHandle(hThread);
}

unsigned long long RenderThread::getPerformanceCounter()
{
	unsigned long long res = ss.perfCounter;
	ss.perfCounter = 0;
	return res;
}

void RenderThread::setPriority(int pri)
{
	if (pri > 0)
		pri = 0;
	if (pri < -15)
		pri = -15;
	SetThreadPriority(hThread, pri);
}

DWORD WINAPI RenderThread::ThreadProc(LPVOID lpParameter)
{
	clock_t ticksstart = 0;
	clock_t ticksstop = 0;

	RenderThread * tc;
	tc = (RenderThread *)lpParameter;
	bool matEditMode = tc->modeMatEditor;
	Raytracer * rtr = Raytracer::getInstance();
	Camera cam = *rtr->curScenePtr->cameras[rtr->curScenePtr->sscene.activeCamera];
	new (&(cam.staticRegion)) ImageRegion();
	cam.iReg = &(rtr->curScenePtr->cameras[rtr->curScenePtr->sscene.activeCamera]->staticRegion);
	cam.threadID = tc->personalNumber;

	cam.updateGlobalHalton();
	cam.cSampler.zeroEverything();

	cam.diaphSampler.zeroEverything();
	cam.copyDiaphFromShapeToSampler();
	if (cam.dofOnTemp)
	{
		if (matEditMode)
		{
			cam.diaphSampler.create(300, 50);
		}
		else
		{
			cam.diaphSampler.create(10000, 100);
		}
	}

	TlsSetValue(rtr->tlsIndexRandom, &(tc->rg));

	Vector3d dir;
	Point3d orig = cam.position;
	rtr->curScenePtr->copySceneStatic(&rtr->curScenePtr->sscene, &tc->ss);
	tc->ss.perfCounter = 0;
	#ifdef RAND_PER_THREAD
		tc->ss.marsaglia.setSeed(tc->rg.getRandomInt(1000000000), tc->rg.getRandomInt(1000000000));
		tc->rg.setSeed(time(0));
	#else
		tc->ss.marsaglia.setSeed((unsigned int)rand()*(unsigned int)rand(), (unsigned int)rand()*(unsigned int)rand());
	#endif
	tc->ss.giSamplesCount = 0;
	cam.marsagliaPtr = &tc->ss.marsaglia;
	cam.lattice.reset();
	cam.scanline.reset(cam.width*cam.aa, cam.height*cam.aa, tc->my_manager->getNumThreads(), tc->personalNumber);

	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_INTERSECTIONS)
	{
		ShadeTracer st(&tc->ss, &cam, &tc->running);
		st.threadID = tc->personalNumber;
		st.renderingLoop();
	}

	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_PATH_TRACING)
	{
		PathTracer3 pt(&tc->ss, &cam, &tc->running);
		pt.threadID = tc->personalNumber;
		pt.discard_secondary_caustic = tc->ss.disableSecondaryCaustic;
		pt.maxCausticRough = tc->ss.causticMaxRough * 0.01f;
		pt.fake_caustic = tc->ss.causticFake;
		pt.renderingLoop();
	}

	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_BIDIRECTIONAL_PATH_TRACING)
	{
		BDPT bidir;
		bidir.ss = &(tc->ss);
		bidir.cam = &cam;
		bidir.threadID = tc->personalNumber;
		bidir.working = &tc->running;
		bidir.renderingLoop();
	}

	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_METROPOLIS)
	{
		Metropolis mlt(&tc->ss, &cam, &tc->running);
		mlt.threadIDNumber =  tc->personalNumber;
		mlt.initializePaths();
		mlt.runMutations();
		rtr->curScenePtr->overall_pixels_count += tc->ss.overall_pixels_count;
	}

	TlsSetValue(rtr->tlsIndexRandom, NULL);
	cam.iReg->overallHits += tc->ss.giSamplesCount;
	rtr->curScenePtr->deleteSceneStaticPointers(&tc->ss);
	tc->alive = false;

	cam.diaphSampler.freeSamples();

	rtr->curScenePtr->thrM->reportFinishedWork();
	
	return 0;
}

bool RenderThread::isAlive()
{
	return alive;
}

bool RenderThread::resetAfterDie()
{
	running = false;
	paused = false;
	alive = false;
	return true;
}
