#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

//--------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::perturbateChain()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	CHECK(scene);

	// find path ES+DS+(L|D)
	bool checkShadow = false;
	chainRefs = 0;
	int t=0;
	bool pertPoss = false;
	bool sun_spec = false;
	bool sun_diff = false;

	bool specs[MLT_MAX_VERTICES];
	for (int i=0; i<MLT_MAX_VERTICES; i++)
		specs[i] = false;

	bool causticFromSunSky = false;

	for (int i=1; i<=numCEP; i++)
	{
		t++;
		MatLayer * mlay = eCandidate[i].mlay;
		CHECK(mlay);
		float tu=0,tv=0;
		if (eCandidate[i].tri)
			eCandidate[i].tri->evalTexUV(eCandidate[i].u, eCandidate[i].v, tu,tv);
		float rough = mlay->getRough(tu, tv);
		bool gSpec = (rough <= maxCausticRough);

		if (gSpec)
		{
			specs[chainRefs] = true;
		}
		else
		{
			if (!specs[chainRefs])
				return false;
			MatLayer * nlay = NULL;
			float tu=0, tv=0;
			float rough = 1;
			if (i==numCEP)
			{
				nlay = lCandidate[numCLP].mlay;
				if (lCandidate[numCLP].tri)
					lCandidate[numCLP].tri->evalTexUV(lCandidate[numCLP].u, lCandidate[numCLP].v, tu, tv);
				if (nlay)
					rough = nlay->getRough(tu, tv);
			}
			else
			{
				nlay = eCandidate[i+1].mlay;
				if (eCandidate[i+1].tri)
					eCandidate[i+1].tri->evalTexUV(eCandidate[i+1].u, eCandidate[i+1].v, tu, tv);
				if (nlay)
					rough = nlay->getRough(tu, tv);
			}

			chainPos[chainRefs] = t;
			
			if (!nlay)	// sun or sky
			{
				causticFromSunSky = true;
				if ( chainRefs < 1)
					return false;
				pertPoss = true;
				checkShadow = true;
				break;
			}

			if (nlay->type==MatLayer::TYPE_EMITTER   ||   rough > maxCausticRough)
			{
				if ( chainRefs < 1)
					return false;
				pertPoss = true;
				checkShadow = true;
				if (sourceSun)
					sun_diff = true;
				break;
			}
			chainRefs++;
		}
	}

	if (!pertPoss)
	{
		for (int i=numCLP; i>=0; i--)
		{
			t++;
			MatLayer * mlay = lCandidate[i].mlay;

			if (!mlay)	// sun or sky
			{
				if (i!=0)
					return false;
				causticFromSunSky = true;
				if (!specs[chainRefs])
					return false;
				if (chainRefs < 1)
					return false;	// LS*E
				pertPoss = true;
				chainPos[chainRefs] = t;
				if (sourceSun)
					sun_spec = true;
				break;
			}

			float tu=0,tv=0;
			if (lCandidate[i].tri)
				lCandidate[i].tri->evalTexUV(lCandidate[i].u, lCandidate[i].v, tu,tv);
			float rough = mlay->getRough(tu,tv);
			bool gSpec = (rough <= maxCausticRough);

			if (mlay->type == MatLayer::TYPE_EMITTER)
			{
				if (!specs[chainRefs])
					return false;
				if (chainRefs < 1)
					return false;	// LS*E
				pertPoss = true;
				chainPos[chainRefs] = t;
				break;
			}

			if (gSpec)
			{
				specs[chainRefs] = true;
			}
			else
			{

				if (!specs[chainRefs])
					return false;
				if (i==0)
					return false;
				MatLayer * nlay = lCandidate[i-1].mlay;
				chainPos[chainRefs] = t;
				if (nlay->type==MatLayer::TYPE_EMITTER   ||   rough > maxCausticRough)
				{
					if ( chainRefs < 1)
						return false;
					pertPoss = true;
					checkShadow = true;
					break;
				}
				chainRefs++;
			}
		}
	}

	if (!pertPoss)
		return false;
	
	float phi = Raytracer::getRandomGeneratorForThread()->getRandomFloat() * 2 * PI;			// 0 - 2PI
	float rnd = Raytracer::getRandomGeneratorForThread()->getRandomFloat();

	float temppch1R1 = pch1R1;
	if (sysRej > 0)
		temppch1R1 = pch1R1 / (float)sysRej;
	temppch1R1 = max(1.4f, temppch1R1);

	float dist = pch1R2 * exp(-log(pch1R2/temppch1R1)*rnd);
	int last = chainPos[chainRefs];

	// check if new pixel location is valid
	float sX = dist * sin(phi);
	float sY = dist * cos(phi);
	pCX += sX;
	pCY += sY;
	if (pCX<0  ||  pCY<0   ||   pCX>=cam->width*cam->aa   ||   pCY>=cam->height*cam->aa)
		return false;	// not in frustum

	Point3d sPos;
	Vector3d eyeDirection = cam->getDirection(pCX,pCY, sPos, 0);

	// fill dirs... rest unchanged
	eCandidate[0].inDir = eCandidate[0].normal = eCandidate[0].outDir = eyeDirection;
	eCandidate[0].pos = sPos;
	Vector3d cDir = eyeDirection;
	Point3d cPoint = sPos;
	MLTVertex * curPath = NULL;
	MLTVertex * origPath = NULL;

	//------------------------------------------------

	for (int i=1; i<=last; i++)
	{
		if (i<=numEP)
		{
			curPath = &(eCandidate[i]);
			origPath = &(ePath[i]);
		}
		else
		{
			curPath = &(lCandidate[numLP-(i-numEP-1)]);
			origPath = &(lPath[numLP-(i-numEP-1)]);
		}

		// send a ray
		float u=0, v=0;
		int chosen = -1, inst_id = -1;
		Matrix4d inst_matr;
		float bigf = BIGFLOAT;

		Color4 att = Color4(1,1,1);
		float d = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time, cPoint, cDir, bigf, inst_matr, inst_id, chosen, u, v, att, ss, true);
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.001f)
		{
			if (i==last  &&   !origPath->tri   &&   causticFromSunSky)
			{
				if (sourceSun)
				{
					Vector3d sdir;
					sdir = ss->sunsky->getSunDirection();
					sdir.normalize();
					if (sdir * cDir < ss->sunsky->maxCosSunSize)
						return false;
				}

				curPath->normal = -cDir;
				curPath->inDir = -cDir;
				curPath->outDir = -cDir;
				curPath->outCosine = 1;
				curPath->inCosine = 1;
				curPath->u = 0;
				curPath->v = 0;
				curPath->mat = NULL;
				curPath->mlay = NULL;
				curPath->pos = cPoint + cDir*1000000;
				curPath->tri = NULL;

				return true;
			}
			return false;
		}

		Triangle * tri  = &(scene->triangles[chosen]);
		MaterialNox * mat  = scene->mats[scene->instances[inst_id].materials[tri->matInInst]];
		MatLayer * mlay = NULL;

		if (mat == origPath->mat)
			mlay = origPath->mlay;
		else
			return false;

		if (i<=numEP)
			eCandidate[i].atten = att;
		if (i==numEP+1)
			connectionAttenuationCandidate = att;
		if (i>numEP+1)
			lCandidate[numLP-(i-numEP-2)].atten = att;

		// fill current vertex
		curPath->instanceID = inst_id;
		curPath->inst_mat = inst_matr;
		curPath->normal = (inst_matr.getMatrixForNormals() * tri->evalNormal(u,v)).getNormalized();
		curPath->u = u;
		curPath->v = v;
		curPath->mat = mat;
		curPath->mlay = mlay;
		curPath->pos = inst_matr * tri->getPointFromUV(u,v);
		curPath->tri = tri;
		curPath->rough = 1.0f;
		curPath->fetchRoughness();

		if (i<=numEP)
		{
			curPath->inDir = cDir * -1;
			curPath->inCosine = cDir*curPath->normal*-1;
		}
		else
		{
			curPath->outDir = cDir * -1;
			curPath->outCosine = cDir*curPath->normal*-1;
		}

		float tu,tv;
		tri->evalTexUV(u,v, tu,tv);

		float pdf;
		HitData hd;
		hd.in = cDir * -1;
		hd.normal_shade = curPath->normal;
		hd.tU = tu;
		hd.tV = tv;
		hd.normal_geom = (inst_matr.getMatrixForNormals() * tri->normal_geom).getNormalized();
		hd.adjustNormal();
		tri->getTangentSpaceSmooth(inst_matr, u,v, hd.dir_U, hd.dir_V);

		if (i!=last)
		{
			Vector3d oldDir = ( (i<=numEP) ? origPath->outDir : origPath->inDir );
			float oldCos = origPath->normal * oldDir;
			bool gotValidDir = false;
			int nTry = 0;
			while (!gotValidDir)
			{
				hd.clearFlagInvalidRandom();
				cDir = mlay->randomNewDirection(hd, pdf);
				float newCos = cDir * curPath->normal;
				if (newCos*oldCos > 0   &&   !hd.isFlagInvalidRandom())
					gotValidDir = true;
				if (nTry++ > 500)
					return false;
			}
		}
		else
		{
			if (mlay==NULL  &&  causticFromSunSky)
				return true;
			if (mlay && mlay->type == MatLayer::TYPE_EMITTER)
				return true;

			MLTVertex * nextPath;
			if (i+1 <= numEP)
				nextPath = &(eCandidate[i+1]);
			else
				if (numLP-(i-numEP) >= 0)
					nextPath = &(lCandidate[numLP-(i-numEP)]);
				else
					return false;

			bool vis = visibilityTest(curPath, nextPath);
			if (!vis)
				return false;

			Vector3d jDir = nextPath->pos - curPath->pos;
			jDir.normalize();
			hd.out = jDir;

			if (i<=numEP)
			{
				curPath->outDir = jDir;
				curPath->outCosine = jDir*curPath->normal;
			}
			else
			{
				curPath->inDir = jDir;
				curPath->inCosine = jDir*curPath->normal;
			}

			if (i+1<=numEP)
			{
				nextPath->inDir = -jDir;
				nextPath->inCosine = -jDir*nextPath->normal;
			}
			else
			{
				nextPath->outDir = -jDir;
				nextPath->outCosine = -jDir*nextPath->normal;
			}
		}

		float ccos = curPath->outDir * curPath->normal;
		bool nowPerturb = false;
		for (int j=0; j<chainRefs; j++)
			if (i==chainPos[j])
				nowPerturb = true;

		if (nowPerturb)
		{	// perturb direction on diffuse
			hd.out = cDir;
			ccos = curPath->normal * cDir;

			float phi = Raytracer::getRandomGeneratorForThread()->getRandomFloat() * 2 * PI;			// 0 - 2PI
			float rnd = Raytracer::getRandomGeneratorForThread()->getRandomFloat();


			float temppch2R1 = pch2R1;
			if (sysRej > 0)
				temppch2R1 = pch2R1 / (float)sysRej;
			temppch2R1 = max(0.001f, temppch2R1);

			float temppch2R2 = pch2R2;
			if (sysRej > 0)
				temppch2R2 = pch2R2 / (float)sysRej;
			temppch2R2 = max(0.001f, temppch2R2);
			float theta = temppch2R2 * exp(-log(temppch2R2/temppch2R1)*rnd);

			// eval dir
			Vector3d olddir;
			if (i<=numEP)
				olddir = origPath->outDir;
			else
				olddir = origPath->inDir;
			Vector3d aa = Vector3d::random();
			aa = aa ^ olddir;
			aa.normalize();
			Vector3d newdir = olddir*cos(theta) + aa*sin(theta);

			newdir.normalize();
			hd.out = newdir;
			Color4 tempcolor = mlay->getBRDF(hd);
			if (tempcolor.isBlack())
				return false;
			if (tempcolor.isInf()  ||  tempcolor.isNaN())
				return false;
			float tempfloat = mlay->getProbability(hd);
			if (tempfloat < 0.001f)
				return false;
			cDir = newdir;
			ccos = curPath->normal * cDir;
		}

		if (i!=last)
		{
			if (i<=numEP)
			{
				curPath->outDir = cDir;
				curPath->outCosine = curPath->normal * cDir;
			}
			else
			{
				curPath->inDir = cDir;
				curPath->inCosine = curPath->normal * cDir;
			}
		}

		if (fabs(curPath->inCosine) < 0.0001f   ||   fabs(curPath->outCosine) < 0.0001f)
			return false;

		if (ccos < 0)
			cPoint = curPath->pos + curPath->normal * -0.0001f;
		else
			cPoint = curPath->pos + curPath->normal * 0.0001f;
		
		curPath->brdf = mlay->getBRDF(hd);

		if (curPath->brdf.isBlack())
			return false;
		if (curPath->brdf.isInf()   ||   curPath->brdf.isNaN())
			return false;
	}

	isPathValid(true);

	return true;
}

//--------------------------------------------------------------------------------------------------------------------------------------

float Metropolis::tentativeTransitionForPertChain(bool forward)
{
	MLTVertex * lCur  = NULL;
	MLTVertex * eCur  = NULL;
	MLTVertex * lOrig = NULL;
	MLTVertex * eOrig = NULL;
	MLTVertex * cur   = NULL;
	MLTVertex * orig  = NULL;
	if (forward)
	{
		lCur = lCandidate;
		eCur = eCandidate;
		lOrig = lPath;
		eOrig = ePath;
	}
	else
	{
		lCur = lPath;
		eCur = ePath;
		lOrig = lCandidate;
		eOrig = eCandidate;
	}

	int last = chainPos[chainRefs];

	float wPdf = 1;
	Color4 wBrdf = Color4(1,1,1);

	// denominator -> pdfs
	for (int i=1; i<last; i++)
	{
		if (i<=numEP)
		{
			cur  = &(eCur[i]);
			orig = &(eOrig[i]);
		}
		else
		{
			cur  = &(lCur[numLP-(i-numEP-1)]);
			orig = &(lOrig[numLP-(i-numEP-1)]);
		}

		CHECK(cur->mlay);
		CHECK(cur->tri);

		bool nowPerturb = false;
		for (int j=0; j<chainRefs; j++)
			if (i==chainPos[j])
				nowPerturb = true;

		HitData hd;
		hd.normal_shade = cur->normal;
		if (i<=numEP)
		{
			hd.in = cur->inDir;
			hd.out = cur->outDir;
		}
		else
		{
			hd.in = cur->outDir;
			hd.out = cur->inDir;
		}

		cur->tri->evalTexUV(cur->u, cur->v, hd.tU, hd.tV);
		hd.normal_geom = (cur->inst_mat.getMatrixForNormals() * cur->tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		cur->tri->getTangentSpaceSmooth(cur->inst_mat, cur->u, cur->v, hd.dir_U, hd.dir_V);

		float ccos = fabs(hd.out*hd.normal_shade);

		float rough = cur->mlay->getRough(hd.tU, hd.tV);
		float pdf = cur->mlay->getProbability(hd);
		if (pdf < 0.001f)
			return 0;
		if (nowPerturb)
			pdf = 1;
		if (rough > 0)
		{
			if (ccos<0.001f)
				return 0;
			wPdf *= pdf/ccos;
		}
		else
			wPdf *= pdf;
	}
	if (wPdf<=0)
		return 0;

	// nominator - brdfs
	for (int i=1; i<=last; i++)
	{
		if (i<=numEP)
		{
			cur  = &(eCur[i]);
			orig = &(eOrig[i]);
		}
		else
		{
			cur  = &(lCur[numLP-(i-numEP-1)]);
			orig = &(lOrig[numLP-(i-numEP-1)]);
		}

		CHECK(cur->mlay);
		CHECK(cur->tri);

		HitData hd;
		hd.normal_shade = cur->normal;
		if (i<=numEP)
		{
			hd.in = cur->outDir;
			hd.out = cur->inDir;
		}
		else
		{
			hd.in = cur->inDir;
			hd.out = cur->outDir;
		}

		cur->tri->evalTexUV(cur->u, cur->v, hd.tU, hd.tV);
		hd.normal_geom = (cur->inst_mat.getMatrixForNormals() * cur->tri->normal_geom).getNormalized();
		hd.adjustNormal();	// done
		cur->tri->getTangentSpaceSmooth(cur->inst_mat, cur->u, cur->v, hd.dir_U, hd.dir_V);

		float ccos = fabs(hd.out*hd.normal_shade);
		Color4 brdf = cur->mlay->getBRDF(hd);
		if (brdf.isInf()  ||  brdf.isNaN())
			return 0;
		if (brdf.isBlack())
			return 0;
		wBrdf *= brdf;
	}

	//G
	int jj = numLP-(last-numEP-1);
	if (jj > 0)
	{
		MLTVertex * lastPath = (last<=numEP)    ?    &(eCur[last]) : &(lCur[jj]);
		MLTVertex * lastPath2 = (last+1<=numEP)    ?    &(eCur[last+1]) : &(lCur[jj-1]);
		CHECK(lastPath);
		CHECK(lastPath->mlay);
		CHECK(lastPath2);
		CHECK(lastPath2->mlay);
		if (lastPath->mlay->type == MatLayer::TYPE_SHADE)
		{
			HitData hd;
			lastPath2->fillHitData(hd, last+1>numEP);
			Color4 cc = lastPath2->mlay->getBRDF(hd);
			if (cc.isInf() || cc.isNaN())
				return 0;
			if (cc.isBlack())
				return 0;

			Vector3d lDir = lastPath2->pos - lastPath->pos;
			float d2 = lDir.normalize_return_old_length();
			d2 = d2*d2;
			if (d2 < 0.0001f)
				return 0;
			float ccos1 = fabs(lastPath->normal * lDir);
			float ccos2 = fabs(lastPath2->normal * lDir);	/// *-1
			if (ccos1 < 0.001f  ||  ccos2 < 0.001f)
				return 0;
			float G = ccos1 * ccos2 / d2;

			wBrdf *= cc * G;
		}
		else
			return 0;
	}

	Color4 attenwhole = Color4(1,1,1);
	if (forward)
	{
		for (int i=1; i<numCLP; i++)
			attenwhole *= lCandidate[i].atten;
		for (int i=1; i<numCEP; i++)
			attenwhole *= eCandidate[i].atten;
		attenwhole *= connectionAttenuationCandidate;
	}
	else
	{
		for (int i=1; i<numLP; i++)
			attenwhole *= lPath[i].atten;
		for (int i=1; i<numEP; i++)
			attenwhole *= ePath[i].atten;
		attenwhole *= connectionAttenuation;
	}

	wBrdf *= attenwhole;
			
	return (0.299f * wBrdf.r  +  0.587f * wBrdf.g  +  0.114f * wBrdf.b)/wPdf;
}

//--------------------------------------------------------------------------------------------------------------------------------------
