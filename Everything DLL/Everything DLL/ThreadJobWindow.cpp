#include "ThreadJobWindow.h"
#include "ColorSchemes.h"
#include "resource.h"

INT_PTR CALLBACK ThreadJobWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
static ThreadWindow * currentThreadWindow = NULL;

extern HMODULE hDllModule;

//--------------------------------------------------------------------------------------------------------------------

ThreadWindow::ThreadWindow()
{
	hTWindow = 0;
	opened = false;
	canAbort = true;
	aborted = false;
}

ThreadWindow::~ThreadWindow()
{
	currentThreadWindow = NULL;
}

//--------------------------------------------------------------------------------------------------------------------

bool ThreadWindow::createWindow(HWND hParent, HINSTANCE hInstance)
{
	EnableWindow(hParent, FALSE);
	currentThreadWindow = this;
	opened = true;
	hTWindow = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_THREAD_WINDOW), hParent, ThreadJobWindowProc);

	return true;
}

//--------------------------------------------------------------------------------------------------------------------

bool ThreadWindow::closeWindow()
{
	EnableWindow(GetParent(hTWindow), TRUE);
	EndDialog(hTWindow, 0);
	hTWindow = 0;
	opened = false;
	return true;
}

//--------------------------------------------------------------------------------------------------------------------

ThreadWindow * ThreadWindow::getCurrentInstance()
{
	return currentThreadWindow;
}

//--------------------------------------------------------------------------------------------------------------------

bool ThreadWindow::setWindowTitle(char * newText)
{
	SetWindowText(hTWindow, newText);
	return true;
}

//--------------------------------------------------------------------------------------------------------------------

bool ThreadWindow::setText(char * newText)
{
	HWND hText = GetDlgItem(hTWindow, IDC_THR_TEXT);
	EMText * emText = GetEMTextInstance(hText);
	if (!emText)
		return false;
	emText->changeCaption(newText);
	return true;
}

//--------------------------------------------------------------------------------------------------------------------

bool ThreadWindow::setProgress(float pos)
{
	HWND hProgress = GetDlgItem(hTWindow, IDC_THR_PROGRESS);
	EMProgressBar * emProg = GetEMProgressBarInstance(hProgress);
	if (!emProg)
		return false;
	emProg->setPos(pos);
	return true;
}

//--------------------------------------------------------------------------------------------------------------------

void ThreadWindow::notifyProgressBarCallback(char * message, float progress)
{
	if (currentThreadWindow)
	{
		currentThreadWindow->setText(message);
		currentThreadWindow->setProgress(progress);
	}
}

//--------------------------------------------------------------------------------------------------------------------

void ThreadWindow::allowAbort()
{
	canAbort = true;
	HWND hAbort = GetDlgItem(hTWindow, IDC_THR_ABORT);
	EnableWindow(hAbort, TRUE);
}

//--------------------------------------------------------------------------------------------------------------------

void ThreadWindow::denyAbort()
{
	canAbort = false;
	HWND hAbort = GetDlgItem(hTWindow, IDC_THR_ABORT);
	EnableWindow(hAbort, FALSE);
}

//--------------------------------------------------------------------------------------------------------------------

void ThreadWindow::abort()
{
	setText("Aborting, please wait...");
	aborted = true;
}

//--------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK ThreadJobWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (!bgBrush)
					bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

				HWND hText = GetDlgItem(hWnd, IDC_THR_TEXT);
				HWND hProgress = GetDlgItem(hWnd, IDC_THR_PROGRESS);
				HWND hAbort = GetDlgItem(hWnd, IDC_THR_ABORT);
				EMText * emText = GetEMTextInstance(hText);
				EMProgressBar * emProg = GetEMProgressBarInstance(hProgress);
				EMButton * emAbort = GetEMButtonInstance(hAbort);
				GlobalWindowSettings::colorSchemes.apply(emText, true);
				GlobalWindowSettings::colorSchemes.apply(emProg);
				GlobalWindowSettings::colorSchemes.apply(emAbort);
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_THR_ABORT:
						{
							if (wmEvent!=BN_CLICKED)
								break;
							if (currentThreadWindow)
								currentThreadWindow->abort();
							else
							{
								EnableWindow(GetParent(hWnd), TRUE);
								MessageBox(0, "whaaat?", "", 0);
								EndDialog(hWnd, 0);
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//--------------------------------------------------------------------------------------------------------------------
