#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "raytracer.h"
#include "log.h"

extern HMODULE hDllModule;
#define GRID_MATS 91
#define MATS_START 73

void setPositionsOnStartMaterialsTab(HWND hWnd);
bool addImageIDinCorner(ImageByteBuffer * imgbuf, int num, HFONT hFont, COLORREF txtCol, COLORREF bgCol, unsigned char alpha);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcMaterials(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));
				setPositionsOnStartMaterialsTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_MATS_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2GroupBar * emgrMats = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_MATS_GR_MATERIALS));
				emgrMats->bgImage = rMain->hBmpBg;
				emgrMats->setFont(rMain->fonts->em2groupbar, false);

				EM2ScrollBar * emsc1 = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_REND2_MATS_SCROLLBAR));
				emsc1->bgImage = rMain->hBmpBg;
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_SIZE:
			{
				rMain->matControlsUpdateMats();
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_MOUSEMOVE:
			{
				RECT rect;
				GetClientRect(hWnd, &rect);
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				if (PtInRect(&rect, pt))
					SetFocus(hWnd);
			}
			break;
		case WM_MOUSEWHEEL:
			{
				int zDelta = (int)wParam;
				EM2ScrollBar * emsc1 = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_REND2_MATS_SCROLLBAR));
				if (zDelta>0)	
					emsc1->workarea_pos = max(0, emsc1->workarea_pos-GRID_MATS); 
				else
					emsc1->workarea_pos = min(emsc1->size_work-emsc1->size_screen, emsc1->workarea_pos+GRID_MATS); 
				emsc1->updateSliderFromWorkarea();
				rMain->matControlsUpdateMats();
				InvalidateRect(emsc1->hwnd, NULL, false);
			}
			break;
		case 14001: // thread called this for mats update
			{
				rMain->matControlsCreateMatControls();
				rMain->matControlsUpdateMats();
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);

				for (int i=0; i<rMain->numMatControls; i++)
					if (wmId==i*20+40001)	// mat name 
					{
						CHECK(wmEvent==BN_CLICKED);
						rMain->runMaterialEditor(i);
					}

				switch (wmId)
				{
					case IDC_REND2_MATS_SCROLLBAR:
						{
							CHECK(wmEvent==SB2_POS_CHANGED);
							rMain->matControlsUpdateMats();
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelMats()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabMats, IDC_REND2_MATS_TITLE));
	updateControlBgShift(GetDlgItem(hTabMats, IDC_REND2_MATS_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgrMaterials = GetEM2GroupBarInstance(GetDlgItem(hTabMats, IDC_REND2_MATS_GR_MATERIALS));
	updateControlBgShift(GetDlgItem(hTabMats, IDC_REND2_MATS_GR_MATERIALS), bgShiftXRightPanel, 0, emgrMaterials->bgShiftX, emgrMaterials->bgShiftY);

	EM2ScrollBar * emsc1 = GetEM2ScrollBarInstance(GetDlgItem(hTabMats, IDC_REND2_MATS_SCROLLBAR));
	updateControlBgShift(GetDlgItem(hTabMats, IDC_REND2_MATS_SCROLLBAR), bgShiftXRightPanel, 0, emsc1->bgShiftX, emsc1->bgShiftY);

	matControlsUpdateMats();
}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartMaterialsTab(HWND hWnd)
{
	int x, y;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_MATS_TITLE), HWND_TOP, 108,11, 160, 16, 0);
	x=8; y=39;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_MATS_GR_MATERIALS),		HWND_TOP,			x,		y,			360,	16,			0);

}

//------------------------------------------------------------------------------------------------

bool RendererMain2::matControlsCreateMatControls()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	matControlsDeleteAll();

	int nmats = sc->mats.objCount;
	if (nmats<1)
		return true;

	matcontrol = new RM2MatControls[nmats];
	CHECK(matcontrol);
	numMatControls = nmats;

	int x = 8;
	int y = 85;
	for (int i=0; i<nmats; i++)
	{
		if (!sc->mats[i])
			continue;

		MaterialNox * mat = sc->mats[i];

		matcontrol[i].hPrev = CreateWindow("EMPView", "", WS_CHILD | WS_VISIBLE,
					x, y+i*GRID_MATS, 73, 73, hTabMats, (HMENU)(i*20+40000), hDllModule, 0);
		EMPView * emPrev = GetEMPViewInstance(matcontrol[i].hPrev);
		emPrev->showBorder = true;
		emPrev->colBorder = RGB(56,56,56);

		ImageByteBuffer * sprev = NULL;
		if (mat->preview)
		{
			int ow = max(1, mat->preview->width);
			int oh = max(1, mat->preview->height);
			sprev = mat->preview->copyWithZoom(max(73.0f/ow, 73.0f/oh));
			if (!emPrev->byteBuff  ||  emPrev->byteBuff->width!=73  ||  emPrev->byteBuff->height!=73)
				emPrev->byteBuff->allocBuffer(73, 73);
			if (sprev)
				emPrev->byteBuff->copyImageFrom(sprev);
		}
		bool ok = addImageIDinCorner(emPrev->byteBuff, i+1, fonts->em2text , RGB(255,255,255), RGB(0,0,0), 160);


		matcontrol[i].hName = CreateWindow("EM2Text", "MATERIAL NAME", WS_CHILD | WS_VISIBLE,
					x+82, y+i*GRID_MATS, 140, 73, hTabMats, (HMENU)(i*20+40001), hDllModule, 0);
		char * name = copyString(mat->name);
		if (name)
		{
			int l = (int)strlen(name);
			_strupr_s(name, l+1);
		}
		EM2Text * emtName = GetEM2TextInstance(matcontrol[i].hName);
		emtName->setFont(fonts->em2text, false);
		emtName->bgImage = hBmpBg;
		emtName->colText = NGCOL_LIGHT_GRAY;
		emtName->marginHori = 9;
		emtName->isItHyperLink = true;
		emtName->wholeAreaMouseOver = true;
		emtName->alphaBgMouseOver = 64;
		emtName->colBgMouseOver = RGB(128,128,128);
		SetWindowText(matcontrol[i].hName, name);

		matControlsCreateOneMatProps(i);

	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::matControlsUpdateMats()
{
	if (numMatControls<1)
		return false;

	int cut_margin_down = 10;

	RECT rect;
	GetClientRect(hTabMats, &rect);

	EM2ScrollBar * emsc = GetEM2ScrollBarInstance(GetDlgItem(hTabMats, IDC_REND2_MATS_SCROLLBAR));

	emsc->setWorkArea(  ((rect.bottom-MATS_START+cut_margin_down)/GRID_MATS)*GRID_MATS,      GRID_MATS*numMatControls);

	SetWindowPos(emsc->hwnd, HWND_TOP, 359, MATS_START, 9, rect.bottom-MATS_START-16, SWP_NOZORDER);

	InvalidateRect(emsc->hwnd, NULL, false);

	int movetxt = 0;
	if (emsc->size_work <= emsc->size_screen)
	{
		ShowWindow(emsc->hwnd, SW_HIDE);
		emsc->workarea_pos = 0;
		movetxt = 25;
	}
	else
	{
		emsc->workarea_pos = max(0, min(emsc->size_work-emsc->size_screen, emsc->workarea_pos));
		ShowWindow(emsc->hwnd, SW_SHOW);
	}


	int x = 8;
	int y = MATS_START;
	int fm = emsc->workarea_pos / GRID_MATS;
	int lastmat = (rect.bottom-MATS_START+cut_margin_down)/GRID_MATS + fm - 1;

	for (int i=0; i<numMatControls; i++)
	{
		int show_action = SW_SHOW;
		if (i<fm  ||  i>lastmat)
			show_action = SW_HIDE;
		ShowWindow(matcontrol[i].hPrev, show_action);
		ShowWindow(matcontrol[i].hName, show_action);
		for (int id=0; id<matcontrol[i].numProps; id++)
			ShowWindow(matcontrol[i].hProps[id], show_action);


		SetWindowPos(matcontrol[i].hPrev, HWND_TOP, x,y+(i-fm)*GRID_MATS, 73, 73, SWP_NOZORDER);
		int cnx = x+73;//82;
		int cny = y+(i-fm)*GRID_MATS;
		SetWindowPos(matcontrol[i].hName, HWND_TOP, cnx, cny, 168+movetxt, 73, SWP_NOZORDER | SWP_NOCOPYBITS);

		EM2Text * emtName = GetEM2TextInstance(matcontrol[i].hName);
		emtName->bgShiftX = cnx + bgShiftXRightPanel;
		emtName->bgShiftY = cny;
		InvalidateRect(matcontrol[i].hName, NULL, false);

		int ps = (73 - matcontrol[i].numProps*12) / 2;
		for (int id=0; id<matcontrol[i].numProps; id++)
		{
			int cx = x+250+movetxt;
			int cy = y+(i-fm)*GRID_MATS+ps+id*12+2;
			EM2Text * emtxt= GetEM2TextInstance(matcontrol[i].hProps[id]);
			emtxt->bgShiftX = cx + bgShiftXRightPanel;
			emtxt->bgShiftY = cy;
			SetWindowPos(matcontrol[i].hProps[id], HWND_TOP, cx, cy, 95, 11, SWP_NOZORDER | SWP_NOCOPYBITS);
		}
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::matControlsUpdateOneMat(int matnum)
{
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(matnum>=0);
	CHECK(matnum<rtr->curScenePtr->mats.objCount);
	CHECK(matnum<numMatControls);
	CHECK(matcontrol);
	MaterialNox * mat = rtr->curScenePtr->mats[matnum];
	CHECK(mat);

	EMPView * empv = GetEMPViewInstance(matcontrol[matnum].hPrev);
	if (empv)
	{
		ImageByteBuffer * sprev = NULL;
		if (mat->preview)
		{
			int ow = max(1, mat->preview->width);
			int oh = max(1, mat->preview->height);
			sprev = mat->preview->copyWithZoom(max(73.0f/ow, 73.0f/oh));
			if (!empv->byteBuff  ||  empv->byteBuff->width!=73  ||  empv->byteBuff->height!=73)
				empv->byteBuff->allocBuffer(73, 73);
			if (sprev)
				empv->byteBuff->copyImageFrom(sprev);
		}
		bool ok = addImageIDinCorner(empv->byteBuff, matnum+1, fonts->em2text , RGB(255,255,255), RGB(0,0,0), 160);
	}

	EM2Text * emname = GetEM2TextInstance(matcontrol[matnum].hName);
	if (emname)
	{
		char * name = copyString(mat->name);
		if (name)
		{
			int l = (int)strlen(name);
			_strupr_s(name, l+1);
		}
		emname->changeCaption(name);
		if (name)
			free(name);
	}

	InvalidateRect(matcontrol[matnum].hPrev, NULL, false);

	matControlsCreateOneMatProps(matnum);

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::matControlsCreateOneMatProps(int matnum)
{
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(matnum>=0);
	CHECK(matnum<rtr->curScenePtr->mats.objCount);
	CHECK(matnum<numMatControls);
	CHECK(matcontrol);
	MaterialNox * mat = rtr->curScenePtr->mats[matnum];
	CHECK(mat);

	// delete old prop text windows
	int nn = matcontrol[matnum].numProps;
	for (int i=0; i<nn; i++)
	{
		if (matcontrol[matnum].hProps[i])
			DestroyWindow(matcontrol[matnum].hProps[i]);
		matcontrol[matnum].hProps[i] = NULL;
	}
	matcontrol[matnum].numProps = 0;


	int id = 0;
	int i = matnum;
	char buf[256];
	int x = 8;
	int y = 85;

	if (mat->isSkyPortal)
	{
		// 0 - skyportal
		matcontrol[i].hProps[id] = CreateWindow("EM2Text", "SKYPORTAL", WS_CHILD | WS_VISIBLE,
					x+250, y+i*GRID_MATS-2+id*15, 95, 11, hTabMats, (HMENU)(i*20+40005), hDllModule, 0);
		EM2Text * emtLayers = GetEM2TextInstance(matcontrol[i].hProps[id]);
		emtLayers->setFont(fonts->em2matprops, false);
		emtLayers->bgImage = hBmpBg;
		id++;
		matcontrol[i].numProps = 1;
		return true;
	}
	else
	{
		// 0 - num layers
		if (mat->layers.objCount==1)
			sprintf_s(buf, 256, "1 LAYER");
		else
			sprintf_s(buf, 256, "%d LAYERS", mat->layers.objCount);
		matcontrol[i].hProps[id] = CreateWindow("EM2Text", buf, WS_CHILD | WS_VISIBLE,
					x+250, y+i*GRID_MATS-2+id*15, 95, 11, hTabMats, (HMENU)(i*20+40005), hDllModule, 0);
		EM2Text * emtLayers = GetEM2TextInstance(matcontrol[i].hProps[id]);
		emtLayers->setFont(fonts->em2matprops, false);
		emtLayers->bgImage = hBmpBg;
		id++;
	}


	bool use_normals, use_transmission, use_emission, use_sss, use_displacement;
	use_displacement = (mat->tex_displacement.isValid()  &&  mat->use_tex_displacement  &&  mat->displ_depth>0.0f);
	mat->checkProperties(use_normals, use_transmission, use_sss, use_emission);

	// 1 - displacement
	if (use_displacement)
	{
		matcontrol[i].hProps[id] = CreateWindow("EM2Text", "DISPLACEMENT", WS_CHILD | WS_VISIBLE,
					x+250, y+i*GRID_MATS-2+id*12, 95, 11, hTabMats, (HMENU)(i*20+40006), hDllModule, 0);
		EM2Text * emtDispl = GetEM2TextInstance(matcontrol[i].hProps[id]);
		emtDispl->setFont(fonts->em2matprops, false);
		emtDispl->bgImage = hBmpBg;
		id++;
	}

	// 2 - normal mapping
	if (use_normals)
	{
		matcontrol[i].hProps[id] = CreateWindow("EM2Text", "NORMAL MAPPING", WS_CHILD | WS_VISIBLE,
					x+250, y+i*GRID_MATS-2+id*12, 95, 11, hTabMats, (HMENU)(i*20+40007), hDllModule, 0);
		EM2Text * emtNormal = GetEM2TextInstance(matcontrol[i].hProps[id]);
		emtNormal->setFont(fonts->em2matprops, false);
		emtNormal->bgImage = hBmpBg;
		id++;
	}

	// 3 - transmission
	if (use_transmission)
	{
		matcontrol[i].hProps[id] = CreateWindow("EM2Text", "TRANSMISSION", WS_CHILD | WS_VISIBLE,
					x+250, y+i*GRID_MATS-2+id*12, 95, 11, hTabMats, (HMENU)(i*20+40008), hDllModule, 0);
		EM2Text * emtTransm = GetEM2TextInstance(matcontrol[i].hProps[id]);
		emtTransm->setFont(fonts->em2matprops, false);
		emtTransm->bgImage = hBmpBg;
		id++;
	}

	// 4 - sss
	if (use_sss)
	{
		matcontrol[i].hProps[id] = CreateWindow("EM2Text", "SSS", WS_CHILD | WS_VISIBLE,
					x+250, y+i*GRID_MATS-2+id*12, 95, 11, hTabMats, (HMENU)(i*20+40009), hDllModule, 0);
		EM2Text * emtSSS = GetEM2TextInstance(matcontrol[i].hProps[id]);
		emtSSS->setFont(fonts->em2matprops, false);
		emtSSS->bgImage = hBmpBg;
		id++;
	}

	// 5 - emission
	if (use_emission)
	{
		matcontrol[i].hProps[id] = CreateWindow("EM2Text", "EMISSION", WS_CHILD | WS_VISIBLE,
					x+250, y+i*GRID_MATS-2+id*12, 95, 11, hTabMats, (HMENU)(i*20+40010), hDllModule, 0);
		EM2Text * emtEmission = GetEM2TextInstance(matcontrol[i].hProps[id]);
		emtEmission->setFont(fonts->em2matprops, false);
		emtEmission->bgImage = hBmpBg;
		id++;
	}

	matcontrol[i].numProps = id;

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::matControlsDeleteAll()
{
	if (!matcontrol)
	{
		numMatControls = 0;
		return true;
	}

	for (int i=0; i<numMatControls; i++)
	{
		if (matcontrol[i].hName)
			DestroyWindow(matcontrol[i].hName);
		if (matcontrol[i].hPrev)
		{
			EMPView * empv = GetEMPViewInstance(matcontrol[i].hPrev);
			if (empv  &&  empv->byteBuff)
			{
				empv->byteBuff->freeBuffer();
				delete empv->byteBuff;
				empv->byteBuff = NULL;
			}
		}
		if (matcontrol[i].hProps)
			for (int j=0; j<matcontrol[i].numProps; j++)
			{
				if (matcontrol[i].hProps[j])
					DestroyWindow(matcontrol[i].hProps[j]);
				matcontrol[i].hProps[j] = 0;
			}
		matcontrol[i].numProps = 0;
	}

	delete [] matcontrol;
	matcontrol = NULL;
	numMatControls = 0;

	return true;
}

//------------------------------------------------------------------------------------------------

bool addImageIDinCorner(ImageByteBuffer * imgbuf, int num, HFONT hFont, COLORREF txtCol, COLORREF bgCol, unsigned char alpha)
{
	CHECK(imgbuf);
	CHECK(imgbuf->width>0);
	CHECK(imgbuf->height>0);

	HDC thdc = CreateDC("DISPLAY", NULL, NULL, NULL);
	HDC hdc = CreateCompatibleDC(thdc);
	CHECK(hdc);

	int rx = 256, ry = 128;
	HBITMAP hBMP = CreateCompatibleBitmap(hdc, rx, ry);
	HBITMAP hOldBitmap = (HBITMAP)SelectObject(hdc, hBMP);
	HFONT hOldFont = (HFONT)SelectObject(hdc, hFont ? hFont : GetStockObject(DEFAULT_GUI_FONT));

	char buf[64];
	sprintf_s(buf, 64, "%d", num);
	int ss = (int)strlen(buf);
	SIZE sz;
	GetTextExtentPoint32(hdc, buf, ss, &sz);
	sz.cx += 7;
	sz.cy += 4;
	RECT rect = { 0, 0 , sz.cx, sz.cy};
	SetBkMode(hdc, OPAQUE);
	SetBkColor(hdc, bgCol);
	SetTextColor(hdc, txtCol);
	ExtTextOut(hdc, 4, 2, ETO_OPAQUE, &rect, buf, ss, 0);

	SelectObject(hdc, hOldFont);
	SelectObject(hdc, hOldBitmap);

	BITMAPINFOHEADER bi = {0};
	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biHeight = ry;
	bi.biWidth = rx;
	bi.biPlanes = 1;
	bi.biBitCount =  32;
	bi.biCompression = BI_RGB;
	GetDIBits(hdc, hBMP, 0, ry, NULL, (BITMAPINFO*)&bi, DIB_RGB_COLORS);
	unsigned char * pBits = new unsigned char[bi.biSizeImage];
	if (pBits)
	{
		GetDIBits(hdc, hBMP, 0, ry, pBits, (BITMAPINFO*)&bi, DIB_RGB_COLORS);
		int a = alpha;
		int ma = 255-a;
		for (int y=0; y<sz.cy; y++)
		{
			for (int x=0; x<sz.cx; x++)
			{
				
				imgbuf->imgBuf[y][x] = RGB(
						(pBits[((ry-y-1)*rx+x)*4+0] * a + GetRValue(imgbuf->imgBuf[y][x]) * ma)/255,
						(pBits[((ry-y-1)*rx+x)*4+1] * a + GetGValue(imgbuf->imgBuf[y][x]) * ma)/255,
						(pBits[((ry-y-1)*rx+x)*4+2] * a + GetBValue(imgbuf->imgBuf[y][x]) * ma)/255 );
			}
		}
		delete [] pBits;
	}

	DeleteObject(hBMP);
	DeleteDC(hdc);
	DeleteDC(thdc);
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::matControlsLockUnlockAll(bool enabled)
{
	CHECK(matcontrol);
	CHECK(numMatControls>0);
	BOOL Enabled = enabled ? TRUE : FALSE;
	for (int i=0; i<numMatControls; i++)
	{
		EnableWindow(matcontrol[i].hName, Enabled);
		InvalidateRect(matcontrol[i].hName, NULL, false);
		EnableWindow(matcontrol[i].hPrev, Enabled);
		InvalidateRect(matcontrol[i].hPrev, NULL, false);

		for (int j=0; j<matcontrol[i].numProps; j++)
		{
			EnableWindow(matcontrol[i].hProps[j], Enabled);
			InvalidateRect(matcontrol[i].hProps[j], NULL, false);
		}
	}
	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabMaterials(bool nowrendering)
{
	GROUPBAR_ACTIVATE(GetDlgItem(hTabMats, IDC_REND2_MATS_GR_MATERIALS), !nowrendering);
	
}

//------------------------------------------------------------------------------------------------
