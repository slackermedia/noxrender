#include "textures.h"
#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <math.h>
#include "log.h"
#include "raytracer.h"
#include <mbstring.h>
#include "dialogs.h"

//EXR
#define OPENEXR_DLL
#include <ImathBox.h>
#include <half.h>
#include <ImfCRgbaFile.h>
#include <ImfRgbaFile.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfLut.h>
#include <ImfInputFile.h>
#include <ImfTiledInputFile.h>
#include <ImfPreviewImage.h>
#include <ImfChannelList.h>
#include <Iex.h>

//------------------------------------------------------------------------------------------------------------------------------------

#ifndef CHECK
	#define CHECK(a) if(!(a)) return false
#endif

//------------------------------------------------------------------------------------------------------------------------------------

float wrap_coord_float(float a)
{
	int bb = ((int&)a>>31)&0x1;
	return a-(int)a+bb;
}

int wrap_coord_int(int a, int resolution)
{
	int bb = a%resolution;
	bb += ((bb>>31)&0x1) * resolution;
	return bb;
}

//------------------------------------------------------------------------------------------------------------------------------------

BitmapImage::BitmapImage()
{
	imgOK = false;
	width = 0;
	height = 0;
	badColor = Color4(1,1,0);
	fdata = NULL;
	idata = NULL;
	isItFloat = false;
}

BitmapImage::~BitmapImage()
{
}

//------------------------------------------------------------------------------------------------------------------------------------

using namespace Imf;
#ifndef tmax
#define tmax(a, b)  (((a) > (b)) ? (a) : (b)) 
#endif
#ifndef tmin
#define tmin(a, b)  (((a) > (b)) ? (b) : (a)) 
#endif

//------------------------------------------------------------------------------------------------------------------------------------

bool BitmapImage::loadFromHDRFile(char * filename, TexModifer texmod, bool keepAsHDR)
{
	if (!filename)
		return false;
	if (strlen(filename)<1)
		return false;

	FILE * file = NULL;
	if (fopen_s( &file, filename, "rb"))
	{
		Logger::add("Failed to open file.");
		return false;
	}

	fseek(file, 0, SEEK_END);
	unsigned long long fsize = _ftelli64(file);
	if (fsize<1)
	{
		Logger::add("File is empty.");
		fclose(file);
		return false;
	}

	fseek(file, 0, SEEK_SET);
	char * fbuf = (char *)malloc((unsigned int)fsize);
	if (1 != fread(fbuf, (unsigned int)fsize, 1, file))
	{
		Logger::add("Failed to read from file");
		fclose(file);
		return false;
	}

	fclose(file);

	int cmpidx = _mbsnbicmp((unsigned char *)fbuf, (unsigned char *)"#?RADIANCE", 10);
	if (cmpidx!=0)
	{
		Logger::add("File header is invalid.");
		free(fbuf);
		return false;
	}

	char dl[] = {0xA, 0xD, 0x0};
	int curPos = 10;
	bool noRes = true;
	while (noRes)
	{
		if (curPos>=fsize)
		{
			free(fbuf);
			Logger::add("Resolution not found.");
			return false;
		}
		if (fbuf[curPos]==0xA)
		{
			curPos++;
			continue;
		}
		if (fbuf[curPos]==0xD)
		{
			curPos++;
			continue;
		}

		int left = (int)fsize - curPos;
		char * endLine = strpbrk(&(fbuf[curPos]), dl);
		if (!endLine)
		{
			Logger::add("Some weird error.");
			free(fbuf);
			return false;
		}

		int llen = (int)(endLine - &(fbuf[curPos]));
		if (llen<1)
		{
			curPos++;
			continue;
		}

		char * line = (char *)malloc(llen+1);
		memcpy(line, &(fbuf[curPos]), llen);
		line[llen] = 0;

		if (llen>8)	// RESOLUTION LINE
		{
			if ((line[0]=='-' || line[0]=='+')  &&  line[1]=='Y')
			{
				int i = 0;
				char * nexttoken = 0;
				char * token = strtok_s(line, " ", &nexttoken);
				while (token!=NULL)
				{
					i++;

					if (i==1)
					{
						if (!strcmp(token, "-Y"))
						{
						}
						else
							if (!strcmp(token, "+Y"))
							{
							}
							else
							{
							}
					}

					if (i==2)
					{
						char * addr;
						long long ival = _strtoi64(token, &addr, 10);
						if ((addr==token)  || ((unsigned int)(addr-token)<strlen(token)) )
						{	// conversion error (to int)
							Logger::add("Resolution is not a number.");
							return false;
						}
						height = (int)ival;
					}

					if (i==3)
					{
						if (!strcmp(token, "-X"))
						{
						}
						else
							if (!strcmp(token, "+X"))
							{
							}
							else
							{
							}
					}

					if (i==4)
					{
						char * addr;
						long long ival = _strtoi64(token, &addr, 10);
						if ((addr==token)  || ((unsigned int)(addr-token)<strlen(token)) )
						{	// conversion error (to int)
							Logger::add("Resolution is not a number.");
							return false;
						}
						width = (int)ival;
					}

					token = strtok_s(NULL, " ", &nexttoken);
				}

				noRes = false;
			}
		}

		curPos += llen;
		free(line);
	}

	int ww = width;
	int hh = height;
	char bbbuf[256];
	sprintf_s(bbbuf, 256, "Allocating hdr image %d x %d", ww, hh);
	Logger::add(bbbuf);
	bool aok = allocFloatBuffer(ww, hh);
	if (!aok)
	{
		free(fbuf);
		Logger::add("Failed to alloc memory for texture.");
		return false;
	}

	if (fbuf[curPos]==0xA)
	{
		curPos++;
	}

	/// PIXEL DATA STARTS HERE ------------

	unsigned char c1 = fbuf[curPos+0];
	unsigned char c2 = fbuf[curPos+1];
	if (c1!=2 || c2!=2)
	{
		// BITMAP LIKE
		int wh=ww*hh;
		if (wh*4+curPos != fsize)
		{
			free(fbuf);
			Logger::add("Bitmap encoding size wrong.");
			return false;
		}
		for (int i=0; i<wh; i++)
		{
			float r = ((unsigned char*)fbuf)[curPos+0]/255.0f;
			float g = ((unsigned char*)fbuf)[curPos+1]/255.0f;
			float b = ((unsigned char*)fbuf)[curPos+2]/255.0f;
			unsigned int val = (unsigned int)((unsigned char)(fbuf[curPos+3]));
			float pp = pow(2.0f, (float)((int)val-128));
			fdata[i] = Color4(r*pp, g*pp, b*pp);
			curPos += 4;
		}
	}
	else
	{
		// RUN LENGTH ENCODING
		int px = 0;
		int scn_no = 0;
		bool done = false;
		while (!done)
		{
			unsigned char c1 = fbuf[curPos+0];
			unsigned char c2 = fbuf[curPos+1];
			unsigned char c3 = fbuf[curPos+2];
			unsigned char c4 = fbuf[curPos+3];
		
			if (c1==2 && c2==2)	// new run-length encoded
			{
				scn_no++;
				int scanlinesize = c3*256+c4; 
				curPos+=4;

				for (int i=0; i<4; i++)
				{
					int pixels_left = scanlinesize;
					while (pixels_left>0)
					{
						unsigned char n = (unsigned char)fbuf[curPos++];
						int px2 = px + scanlinesize - pixels_left;
						if (n>128)
						{	// n pixels from one value
							n -= 128;
							pixels_left -= n;
							unsigned int val = (unsigned int)((unsigned char)(fbuf[curPos++]));	// read one;)
							float fval = val/255.0f;
							switch (i)
							{
								case 0: for (int j=0; j<n; j++) fdata[px2+j].r = fval; break;
								case 1: for (int j=0; j<n; j++) fdata[px2+j].g = fval; break;
								case 2: for (int j=0; j<n; j++) fdata[px2+j].b = fval; break;
								case 3: for (int j=0; j<n; j++)
										{
											float pp = pow(2.0f, (float)((int)val-128));
											fdata[px2+j].r *= pp;
											fdata[px2+j].g *= pp;
											fdata[px2+j].b *= pp;
										}
										break;
							}

						}
						else
						{	// n pixels from different values
							pixels_left -= n;
							for (int j=0; j<n; j++)
							{
								unsigned char val = (unsigned int)((unsigned char)(fbuf[curPos++]));
								float fval = val/255.0f;
								switch (i)
								{
									case 0: fdata[px2+j].r = fval; break;
									case 1: fdata[px2+j].g = fval; break;
									case 2: fdata[px2+j].b = fval; break;
									case 3:
										{
											float pp = pow(2.0f, (float)((int)val-128));
											fdata[px2+j].r *= pp;
											fdata[px2+j].g *= pp;
											fdata[px2+j].b *= pp;
										}
										break;

								}
							}
						}
					}
				}

				px += scanlinesize;
				if (px>=width*height)
					done = true;
			}
			else
			{
				char bbuf[256];
				sprintf_s(bbuf, 256, "Wrong start line: %d %d %d %d   ... hex: %x %x %x %x", c1, c2, c3, c4, c1, c2, c3, c4);
				Logger::add(bbuf);
				freeFloatBuffer();
				free(fbuf);
				return false;
			}
		}
	}

	free(fbuf);
	imgOK = true;
	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool BitmapImage::loadFromExrFile(char * filename, TexModifer texmod, bool keepAsHDR)
{
	Imf::Rgba * iBuff;
	int ewidth, eheight;
	
	if (!fileExists(filename))
		return false;

	try
	{
		RgbaInputFile file(filename);
		Imath::Box2i dw = file.dataWindow();

		ewidth  = dw.max.x - dw.min.x + 1;
		eheight = dw.max.y - dw.min.y + 1;

		//alloc buffer
		iBuff = (Imf::Rgba *)malloc (sizeof(Imf::Rgba)*ewidth*eheight);
		if (!iBuff)
			return false;

		file.setFrameBuffer(iBuff, 1, ewidth);
		file.readPixels(dw.min.y, dw.max.y);
	}
	catch (const std::exception &exc)
	{
		free(iBuff);
		MessageBox(NULL, exc.what(), "Error",  0);
		return false;
	}


	if (keepAsHDR)
		allocFloatBuffer(ewidth, eheight);
	else
		allocByteBuffer(ewidth, eheight);

	int x, y;
	if (keepAsHDR)
	{
		for (y=0; y<eheight; y++)
		{
			for (x=0; x<ewidth; x++)
			{
				fdata[y*ewidth+x].r = (float)(iBuff[y*ewidth+x].r);
				fdata[y*ewidth+x].g = (float)(iBuff[y*ewidth+x].g);
				fdata[y*ewidth+x].b = (float)(iBuff[y*ewidth+x].b);
				fdata[y*ewidth+x].a = 1.0f;//iBuff[y*ewidth+x].a;
				if (0>fdata[y*ewidth+x].r)
					fdata[y*ewidth+x].r = 0.0f;
				if (0>fdata[y*ewidth+x].g)
					fdata[y*ewidth+x].g = 0.0f;
				if (0>fdata[y*ewidth+x].b)
					fdata[y*ewidth+x].b = 0.0f;
				if (0>fdata[y*ewidth+x].a)
					fdata[y*ewidth+x].a = 1.0f;

				if (fdata[y*ewidth+x].isNaN())
				{
					fdata[y*ewidth+x] = Color4(0,0,0);
					Logger::add("NaN in exr");
				}
			}
		}
	}
	else
	{
		for (y=0; y<eheight; y++)
		{
			for (x=0; x<ewidth; x++)
			{
				idata[4*(y*ewidth+x)+0] = (unsigned char)tmin(255, tmax(0, iBuff[y*ewidth+x].r * 255));
				idata[4*(y*ewidth+x)+1] = (unsigned char)tmin(255, tmax(0, iBuff[y*ewidth+x].g * 255));
				idata[4*(y*ewidth+x)+2] = (unsigned char)tmin(255, tmax(0, iBuff[y*ewidth+x].b * 255));
				idata[4*(y*ewidth+x)+3] = (unsigned char)tmin(255, tmax(0, iBuff[y*ewidth+x].a * 255));
			}
		}
	}

	free(iBuff);

	width = ewidth;
	height = eheight;

	isItFloat = keepAsHDR;

	imgOK = true;

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

//#define LFF_LOG(a) Logger::add(a)
#ifndef LFF_LOG
	#define LFF_LOG(a)
#endif

bool BitmapImage::loadFromFile(char * filename, TexModifer texmod, bool keepAsHDR)
{
	ULONG_PTR gdiplusToken;
	ULONG_PTR gdiplusBGThreadToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartupOutput gdiplusStartupOutput;
	
	
	gdiplusStartupInput.SuppressBackgroundThread = TRUE;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, &gdiplusStartupOutput);
	
	Gdiplus::Status stat = gdiplusStartupOutput.NotificationHook(&gdiplusBGThreadToken); 
	if (stat != Gdiplus::Ok)
		return false;

	LFF_LOG("lf: loading file:");
	LFF_LOG(filename);

	LFF_LOG("lf: gdi+ ok");
	
	size_t l = strlen(filename)+2;
	WCHAR * wch = (WCHAR *)malloc(sizeof(WCHAR)*(l));
	MultiByteToWideChar(CP_ACP, 0, filename, -1, wch, (unsigned int)l);
	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(wch);

	if (!bm)
	{
		LFF_LOG("lf: file not loaded");
		imgOK = false;
		gdiplusStartupOutput.NotificationUnhook(gdiplusBGThreadToken);
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	int rx, ry, x, y;
	rx = bm->GetWidth();
	ry = bm->GetHeight();

	if (rx<1 || ry<1)
	{
		LFF_LOG("lf: resolution 0x0 or file not loaded");
		imgOK = false;
		delete bm;
		gdiplusStartupOutput.NotificationUnhook(gdiplusBGThreadToken);
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	Gdiplus::Color c;
	if (keepAsHDR)
	{
		LFF_LOG("lf: allocating float buffer");
		allocFloatBuffer(rx, ry);
	}
	else
	{
		LFF_LOG("lf: allocating byte buffer");
		allocByteBuffer(rx, ry);
	}

	// check allocation
	if ( (keepAsHDR && fdata==0) || (!keepAsHDR && idata==0) )
	{
		LFF_LOG("lf: allocation failed");
		imgOK = false;
		delete bm;
		gdiplusStartupOutput.NotificationUnhook(gdiplusBGThreadToken);
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	//===========
	// try to get it manually through GDIPLUS => HBITMAP => BITMAP => buffer - it's faster
	LFF_LOG("lf: trying to get through GDIPLUS => HBITMAP => BITMAP => buffer");
	bool gotFromHBMP = true;
	HBITMAP hbmp;
	Gdiplus::Color col;
	if (!bm->GetHBITMAP(col, &hbmp))
	{
		BITMAP bmp1;
		GetObject(hbmp,sizeof(bmp1), &bmp1);
		if (bmp1.bmBits   &&   bmp1.bmBitsPixel==32)
		{
			if (keepAsHDR)
				for (y=0; y<ry; y++)
				{
					for (x=0; x<rx; x++)
					{
						fdata[y*rx+x] = Color4(
									((unsigned char*)(bmp1.bmBits))[((ry-y-1)*rx+x)*4+0]/255.0f,
									((unsigned char*)(bmp1.bmBits))[((ry-y-1)*rx+x)*4+1]/255.0f,
									((unsigned char*)(bmp1.bmBits))[((ry-y-1)*rx+x)*4+2]/255.0f   );
					}
				}
			else
				for (y=0; y<ry; y++)
				{
					for (x=0; x<rx; x++)
					{
						idata[(y*rx+x)*4+2] = ((unsigned char*)(bmp1.bmBits))[((ry-y-1)*rx+x)*4+0];
						idata[(y*rx+x)*4+1] = ((unsigned char*)(bmp1.bmBits))[((ry-y-1)*rx+x)*4+1];
						idata[(y*rx+x)*4+0] = ((unsigned char*)(bmp1.bmBits))[((ry-y-1)*rx+x)*4+2];
					}
				}
		}
		else
			gotFromHBMP = false;
	}
	else
		gotFromHBMP = false;

	DeleteObject(hbmp);
	//===========
	
	bool failed_BMP_to_buf = false;
	if (!gotFromHBMP)
	{
		LFF_LOG("lf: failed... now trying GDIPLUS => buffer");
		if (keepAsHDR)
			for (y=0; y<ry; y++)
			{
				for (x=0; x<rx; x++)
				{
					if (bm->GetPixel(x, y, &c) != 0)
						failed_BMP_to_buf = true;
					fdata[y*rx + x] = Color4( c.GetRed()/255.0f,  c.GetGreen()/255.0f, c.GetBlue()/255.0f );
				}
				if (failed_BMP_to_buf)
					break;
			}
		else
			for (y=0; y<ry; y++)
			{
				for (x=0; x<rx; x++)
				{
					if (bm->GetPixel(x, y, &c) != 0)
						failed_BMP_to_buf = true;
					idata[(y*rx+x)*4+0] = c.GetRed();
					idata[(y*rx+x)*4+1] = c.GetGreen();
					idata[(y*rx+x)*4+2] = c.GetBlue();
					idata[(y*rx+x)*4+3] = c.GetAlpha();
				}
				if (failed_BMP_to_buf)
					break;
			}
	}
	if (failed_BMP_to_buf)
	{
		LFF_LOG("lf: also failed... bye");
		imgOK = false;
		freeFloatBuffer();
		freeByteBuffer();
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	width = rx;
	height = ry;

	isItFloat = keepAsHDR;

	delete bm;
	imgOK = true;
	gdiplusStartupOutput.NotificationUnhook(gdiplusBGThreadToken);
	Gdiplus::GdiplusShutdown(gdiplusToken);
	free(wch);

	LFF_LOG("lf: GDI+ released");

	return imgOK;
}

//------------------------------------------------------------------------------------------------------------------------------------

Color4 BitmapImage::getColor(int x, int y)
{
	Color4 res;

	if (!imgOK)
		return badColor;

	// wrap coords
	x = x % width;
	y = y % height;
	x += ((x>>31)&0x1) * width;
	y += ((y>>31)&0x1) * height;

	if (isItFloat)
		return fdata[y*width + x];
	else
	{
		float mply = 1/255.0f;
		res = Color4(idata[(y*width+x)*4+0]*mply,  idata[(y*width+x)*4+1]*mply,  idata[(y*width+x)*4+2]*mply);
		return res;
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

Color4 BitmapImage::getColorUnprocessed(int x, int y)
{
	Color4 res;

	if (!imgOK)
		return badColor;

	// wrap coords
	x = x % width;
	y = y % height;
	x += ((x>>31)&0x1) * width;
	y += ((y>>31)&0x1) * height;

	if (isItFloat)
	{
		return fdata[y*width + x];
	}
	else
	{
		float ff = 1.0f/255.0f;
		return Color4(idata[(y*width+x)*4+0]*ff, idata[(y*width+x)*4+1]*ff, idata[(y*width+x)*4+2]*ff);
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

bool BitmapImage::isValid()
{
	return imgOK;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool BitmapImage::allocByteBuffer(int w, int h)
{
	imgOK = false;
	if (idata)
		delete [] idata;

	idata = new unsigned char[w*h*4];
	if (!idata)
		return false;

	width = w;
	height = h;
	imgOK = true;
	isItFloat = false;

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

void BitmapImage::freeByteBuffer()
{
	if (idata)
		delete [] idata;

	idata = NULL;
	width = 0;
	height = 0;
	imgOK = false;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool BitmapImage::allocFloatBuffer(int w, int h)
{
	imgOK = false;
	if (fdata)
		_aligned_free(fdata);

	fdata = (Color4*)_aligned_malloc(w*h*sizeof(Color4), 16);
	if (!fdata)
		return false;

	width = w;
	height = h;
	imgOK = true;
	isItFloat = true;

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

void BitmapImage::freeFloatBuffer()
{
	if (fdata)
		_aligned_free(fdata);
	fdata = NULL;
	width = 0;
	height = 0;
	imgOK = false;
}

//------------------------------------------------------------------------------------------------------------------------------------

void BitmapImage::freeEverything()
{
	freeFloatBuffer();
	freeByteBuffer();
}

//------------------------------------------------------------------------------------------------------------------------------------

bool BitmapImage::copyFrom(BitmapImage * other)
{
	if (other->fdata)
	{
		if (fdata!=other->fdata)
		{
			freeFloatBuffer();
			if (allocFloatBuffer(other->width, other->height))
			{
				int l = width * height * sizeof(Color4);
				memcpy(fdata, other->fdata, l);
			}
			else
				return false;
		}
	}

	if (other->idata)
	{
		if (idata!=other->idata)
		{
			freeByteBuffer();
			if (allocByteBuffer(other->width, other->height))
			{
				int l = width * height * 4;
				memcpy(idata, other->idata, l);
			}
			else
				return false;
		}
	}

	isItFloat = other->isItFloat;
	imgOK = other->imgOK;
	badColor = other->badColor;

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------

Texture::Texture()
{
	imgOK = false;
	reserved = 0;
	gammaDown = 2.2f;
	filename = NULL;
	fullfilename = NULL;
	exportedFilename = NULL;
	exportedArrayID = 0;
	is_it_converted_normal = false;
	fileModTime[0] = fileModTime[1] = 0;
	fileModTimeDontAsk[0] = fileModTimeDontAsk[1] = 0;
}

Texture::~Texture()
{	
}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::allocByteBuffer(int w, int h)
{
	imgOK = bmp.allocByteBuffer(w, h);
	return imgOK;
}

void Texture::freeByteBuffer()
{
	bmp.freeByteBuffer();
	imgOK = false;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::allocFloatBuffer(int w, int h)
{
	imgOK = bmp.allocFloatBuffer(w, h);
	return imgOK;
}

void Texture::freeFloatBuffer()
{
	bmp.freeFloatBuffer();
	imgOK = false;
}

//------------------------------------------------------------------------------------------------------------------------------------

void Texture::freeAllBuffers()
{
	bmp.freeByteBuffer();
	bmp.freeFloatBuffer();
	if (filename)
		free(filename);
	if (fullfilename)
		free(fullfilename);
	filename = NULL;
	fullfilename = NULL;
	imgOK = false;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool Texture::copyFromOther(Texture * other)
{
	CHECK(other);
	CHECK(other->isValid());

	freeAllBuffers();

	bool cpok = bmp.copyFrom(&(other->bmp));
	if (!cpok)
		return false;

	if (filename)
		free(filename);
	if (fullfilename)
		free(fullfilename);
	filename = copyString(other->filename);
	fullfilename = copyString(other->fullfilename);

	imgOK = other->imgOK;
	gammaDown = other->gammaDown;
	reserved = other->reserved;
	texMod = other->texMod;
	normMod = other->normMod;
	is_it_converted_normal = other->is_it_converted_normal;

	fileModTime[0] = other->fileModTime[0];
	fileModTime[1] = other->fileModTime[1];
	fileModTimeDontAsk[0] = other->fileModTimeDontAsk[0];
	fileModTimeDontAsk[1] = other->fileModTimeDontAsk[1];

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::loadFromFile(char * fileName)
{
	CHECK(fileName);
	CHECK(strlen(fileName)>4);

	int l = (int)strlen(fileName);
	int l1 = l-4;
	if (	((fileName[l1+0]=='.')) &&
			((fileName[l1+1]=='e') || (fileName[l1+1]=='E')) &&
			((fileName[l1+2]=='x') || (fileName[l1+2]=='X')) &&
			((fileName[l1+3]=='r') || (fileName[l1+3]=='R'))	)
		imgOK = bmp.loadFromExrFile(fileName, texMod, true);
	else
		if (	((fileName[l1+0]=='.')) &&
				((fileName[l1+1]=='h') || (fileName[l1+1]=='H')) &&
				((fileName[l1+2]=='d') || (fileName[l1+2]=='D')) &&
				((fileName[l1+3]=='r') || (fileName[l1+3]=='R'))	)
			imgOK = bmp.loadFromHDRFile(fileName, texMod, true);
		else
			imgOK = bmp.loadFromFile(fileName, texMod, false);
	
	if (imgOK)
	{
		int l = (int)strlen(fileName)+2;
		char * tbuf = (char*)malloc(l);
		if (tbuf)
			sprintf_s(tbuf, l, "%s", fileName);
		fullfilename = tbuf;

		size_t s = strlen(fileName);
		char * b = (char*)malloc(s+1);
		if (b)
		{
			char * tpos;
			tpos = fileName;
			for (unsigned int i=0; i<s; i++)
			{	// cut directory prefix
				if (fileName[i] == 92     ||     fileName[i] == 47)
					tpos = &(fileName[i+1]);
			}
			sprintf_s(b, s+1, "%s", tpos);
			filename = b;
		}

		HANDLE hFile = CreateFile(fullfilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile != INVALID_HANDLE_VALUE)
		{
			FILETIME fileTime;
			if (GetFileTime(hFile, NULL, NULL, &fileTime))
			{
				fileModTime[0] = fileTime.dwLowDateTime;
				fileModTime[1] = fileTime.dwHighDateTime;
			}
			CloseHandle(hFile);
		}
	}

	return imgOK;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::loadFromFileTryOtherFolders(char * fileName)
{
	bool loaded = loadFromFile(fileName);
	if (loaded)
		return true;

	bool abort = false;

	char tfname[2048];
	char buf[4096];
	char * justFile = getOnlyFile(fileName);
	char * srcDir = getDirectory(fileName);

	sprintf_s(buf, 4096, "      Texture file %s does not exist.", fileName);
	Logger::add(buf);

	Raytracer * rtr = Raytracer::getInstance();
	while (!loaded  &&  !abort)
	{
		for (int i=0; i<rtr->texDirs.objCount; i++)
		{
			sprintf_s(tfname, 2048, "%s\\%s", rtr->texDirs[i], justFile);

			sprintf_s(buf, "        Checking for file %s", tfname);
			Logger::add(buf);

			if (fileExists(tfname))
			{
				Logger::add("        File exists... trying to open...");
				loaded = loadFromFile(tfname);
				if (loaded)
					Logger::add("        Loaded successfully.");
				else
					Logger::add("        Failed to load, this may be not an image file.");
			}
			else
			{
				Logger::add("        File does not exist");
			}
		}

		if (!loaded)
		{
			if (rtr->showTexNotFoundDlg)
			{
				Logger::add("        Texture file not found in any previous folders, calling MessageBox...");
				sprintf_s(buf, 4096, "Texture file\n%s\nwas not found.\nDo you want to specify other folder for texture searching?\n\n(Cancel for no tex load and not showing this message again)", fileName);
				int res = MessageBox(0, buf, "Warning", MB_YESNOCANCEL);
				switch (res)
				{
					case IDYES:
						{
							char * newdir = openDirectoryDialog(0, "Pick textures folder", srcDir);
							if (newdir)
							{
								rtr->texDirs.add(newdir);
								rtr->texDirs.createArray();
								sprintf_s(buf, 4096, "        Folder %s was chosen.", newdir);
								Logger::add(buf);
							}
							else
							{
								Logger::add("        Folder was not chosen...");
							}
						}
						break;
					case IDNO:
						Logger::add("        NO...");
						abort = true;
						break;
					case IDCANCEL:
						rtr->showTexNotFoundDlg = false;
						Logger::add("        CANCEL...");
						abort = true;
						break;
				}
			}
			else
			{
				Logger::add("        Texture file not found in any previous folders, can't call MessageBox...");
				abort = true;
			}
		}
	}

	if (justFile)
		free(justFile);
	if (srcDir)
		free(srcDir);

	return loaded;
}

//------------------------------------------------------------------------------------------------------------------------------------

Color4 Texture::getColor(float x, float y, TexModifer * tMod)
{
	Color4 res;

	if (!imgOK)
		return bmp.badColor;

	y = 1 - y;

	if (!tMod)
		tMod = &texMod;

	int ix,iy;
	float rx, ry;
	rx = x * bmp.getWidth();
	ry = y * bmp.getHeight();
	ix = (int)( rx );
	iy = (int)( ry );

	// original buffer + process realtime
	if (tMod->interpolateProbe)
	{
		int brx = (ix>>31)&0x1;
		int bry = (iy>>31)&0x1;
		rx -= ix-brx;
		ry -= iy-bry;

		res = bmp.getColorUnprocessed(ix  , iy  ) * (1-rx)*(1-ry)
			+ bmp.getColorUnprocessed(ix+1, iy  ) * (rx  )*(1-ry)
			+ bmp.getColorUnprocessed(ix  , iy+1) * (1-rx)*(ry  )
			+ bmp.getColorUnprocessed(ix+1, iy+1) * (rx  )*(ry  );
	}
	else
		res = bmp.getColorUnprocessed(ix, iy);
	res = processColor(res, tMod);


	return res;
}


//------------------------------------------------------------------------------------------------------------------------------------

#define PROCCOLORSSE
inline void chooseSSE(const __m128 &mask, const __m128 &a, const __m128 &b, __m128 &result)
{	// if mask return a else return b
	__m128 am, bnm;
	am = _mm_and_ps(a, mask);
	bnm = _mm_andnot_ps(mask, b);
	result = _mm_or_ps(am, bnm);
}

Color4 Texture::processColor(const Color4 &srccol, TexModifer * texModifier)
{
	if (!texModifier)
		return srccol;

	Color4 c = srccol;
	float g = texModifier->gamma;
	unsigned int invmask = texModifier->invert ? 0xFFFFFFFF : 0;

#ifdef PROCCOLORSSE
	__m128 sseBrightness = _mm_set_ps(0.0f, texModifier->brightness, texModifier->brightness, texModifier->brightness);
	__m128 sseZero = _mm_setzero_ps();
	__m128 sseOne = _mm_set1_ps(1.0f);
	c.sse = _mm_min_ps(sseOne, _mm_max_ps(sseZero, _mm_add_ps(sseBrightness, c.sse)));
	__m128 sseContrast = _mm_set_ps(0.0f, texModifier->contrast, texModifier->contrast, texModifier->contrast);
	__m128 sseHalf = _mm_set1_ps(0.5f);
	c.sse = _mm_min_ps(sseOne, _mm_max_ps(sseZero, _mm_add_ps(_mm_mul_ps(_mm_sub_ps(c.sse, sseHalf), sseContrast), sseHalf)));

	ColorHSL hsl = c.toColorHSL();
	hsl.s = tmin(1, tmax(0, hsl.s+texModifier->saturation*sqrt(hsl.s)));
	hsl.h += texModifier->hue;
	hsl.h = hsl.h - (int)hsl.h;
	c = hsl.toColor4();

	__m128 sseColorAdd = _mm_set_ps(0.0f, texModifier->blue, texModifier->green, texModifier->red);
	sseZero = _mm_setzero_ps();
	sseOne = _mm_set1_ps(1.0f);
	__m128 sseInvert = _mm_set1_ps((float&)invmask);
	c.sse = _mm_min_ps(sseOne, _mm_max_ps(sseZero, _mm_add_ps(sseColorAdd, c.sse)));
	__m128 sseInvertedValue = _mm_sub_ps(sseOne, c.sse);
	chooseSSE(sseInvert, sseInvertedValue, c.sse, c.sse);

	int ir = (int)(c.r*(GAMMAARRAYSIZE-1));
	int ig = (int)(c.g*(GAMMAARRAYSIZE-1));
	int ib = (int)(c.b*(GAMMAARRAYSIZE-1));
	c.r = texModifier->gammaLookup[ir];
	c.g = texModifier->gammaLookup[ig];
	c.b = texModifier->gammaLookup[ib];

#else

	float invert = texModifier->invert ? 1.0f : 0.0f;

	c.r = tmin(1.0f, tmax(0.0f, texModifier->brightness+c.r));
	c.g = tmin(1.0f, tmax(0.0f, texModifier->brightness+c.g));
	c.b = tmin(1.0f, tmax(0.0f, texModifier->brightness+c.b));
	
	float h2 = 0.5f;
	c.r = tmin(1.0f, tmax(0.0f, (c.r-h2) * texModifier->contrast + h2 ));
	c.g = tmin(1.0f, tmax(0.0f, (c.g-h2) * texModifier->contrast + h2 ));
	c.b = tmin(1.0f, tmax(0.0f, (c.b-h2) * texModifier->contrast + h2 ));

	ColorHSL hsl = c.toColorHSL();
	hsl.s = tmin(1, tmax(0, hsl.s+texModifier->saturation*sqrt(hsl.s)));
	hsl.h += texModifier->hue;
	hsl.h = hsl.h - (int)hsl.h;
	c = hsl.toColor4();

	c.r = tmin(1.0f, tmax(0.0f, texModifier->red+c.r));
	c.g = tmin(1.0f, tmax(0.0f, texModifier->green+c.g));
	c.b = tmin(1.0f, tmax(0.0f, texModifier->blue+c.b));

	c.r = invert * (1-c.r) + (1-invert) * c.r;
	c.g = invert * (1-c.g) + (1-invert) * c.g;
	c.b = invert * (1-c.b) + (1-invert) * c.b;

	int ir = (int)(c.r*(GAMMAARRAYSIZE-1));
	int ig = (int)(c.g*(GAMMAARRAYSIZE-1));
	int ib = (int)(c.b*(GAMMAARRAYSIZE-1));
	c.r = texModifier->gammaLookup[ir];
	c.g = texModifier->gammaLookup[ig];
	c.b = texModifier->gammaLookup[ib];
#endif	
	return c;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::isValid()
{
	return imgOK;
}

//------------------------------------------------------------------------------------------------------------------------------------

void TexModifer::clipValues()
{
	brightness	= min(max(brightness,	TEXMOD_MIN_BRIGHTNESS), TEXMOD_MAX_BRIGHTNESS);
	contrast	= min(max(contrast,		TEXMOD_MIN_CONTRAST),	TEXMOD_MAX_CONTRAST);
	saturation	= min(max(saturation,	TEXMOD_MIN_SATURATION), TEXMOD_MAX_SATURATION);
	hue			= min(max(hue,			TEXMOD_MIN_HUE),		TEXMOD_MAX_HUE);
	red			= min(max(red,			TEXMOD_MIN_RED),		TEXMOD_MAX_RED);
	green		= min(max(green,		TEXMOD_MIN_GREEN),		TEXMOD_MAX_GREEN);
	blue		= min(max(blue,			TEXMOD_MIN_BLUE),		TEXMOD_MAX_BLUE);
	gamma		= min(max(gamma,		TEXMOD_MIN_GAMMA),		TEXMOD_MAX_GAMMA);
	updateGamma(gamma);
}

//------------------------------------------------------------------------------------------------------------------------------------

void TexModifer::updateGamma(float newgamma)
{
	gamma = newgamma;
	for (int i=0; i<GAMMAARRAYSIZE; i++)
	{
		gammaLookup[i] = pow(i/(float)(GAMMAARRAYSIZE-1), gamma);
	}
}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::isItNormalMap()	// false on empty, HDR and non-blue one
{
	if (!isValid())
		return false;
	if (bmp.isItFloat)
		return false;

	int w = bmp.getWidth();
	int h = bmp.getHeight();

	int bad = 0;
	float p2 = 2.0f/255.0f;
	float maxval = 1.1f;
	float minval = 1.0f/maxval;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			unsigned char cr = bmp.idata[(y*w+x)*4+0];
			unsigned char cg = bmp.idata[(y*w+x)*4+1];
			unsigned char cb = bmp.idata[(y*w+x)*4+2];
			float fr = cr * p2 -1;
			float fg = cg * p2 -1;
			float fb = cb * p2 -1;

			float d2 = fr*fr + fg*fg + fb*fb;
			if (d2>maxval || d2<minval)
				bad++;
		}
	}

	if (bad>w*h/20)
		return false;
	
	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::convertMeToNormalMap()
{
	if (!isValid())
		return false;
	if (bmp.isItFloat)
		return false;

	int w = bmp.getWidth();
	int h = bmp.getHeight();

	int zz = max(w,h);
	int dz = zz/8;

	unsigned char * imgbuf = (unsigned char *)malloc(w*h*4);
	CHECK(imgbuf);

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			int rc = (y*w+x)*4;
			int rx1 = (y*w+(x+1)%w)*4;
			int ry1 = (((y+1)%h)*w+x)*4;
			int rx2 = (y*w+(x-1+w)%w)*4;
			int ry2 = (((y-1+h)%h)*w+x)*4;
			int vc = (int)(bmp.idata[rc]) + (int)(bmp.idata[rc+1]) + (int)(bmp.idata[rc+2]);
			int vx1 = (int)(bmp.idata[rx1]) + (int)(bmp.idata[rx2+1]) + (int)(bmp.idata[rx1+2]);
			int vx2 = (int)(bmp.idata[rx2]) + (int)(bmp.idata[rx1+1]) + (int)(bmp.idata[rx2+2]);
			int vy1 = (int)(bmp.idata[ry1]) + (int)(bmp.idata[ry1+1]) + (int)(bmp.idata[ry1+2]);
			int vy2 = (int)(bmp.idata[ry2]) + (int)(bmp.idata[ry2+1]) + (int)(bmp.idata[ry2+2]);
			int dx = (vx2-vx1)/3;
			int dy = (vy1-vx2)/3;
			Vector3d vec = Vector3d((float)dx,(float)dy,(float)dz);
			vec.normalize();
			imgbuf[(y*w+x)*4+0] = (unsigned char)(vec.x*127+128);
			imgbuf[(y*w+x)*4+1] = (unsigned char)(vec.y*127+128);
			imgbuf[(y*w+x)*4+2] = (unsigned char)(vec.z*127+128);
			imgbuf[(y*w+x)*4+3] = 255;
		}
	}


	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			bmp.idata[(y*w+x)*4+0] = imgbuf[(y*w+x)*4+0];
			bmp.idata[(y*w+x)*4+1] = imgbuf[(y*w+x)*4+1];
			bmp.idata[(y*w+x)*4+2] = imgbuf[(y*w+x)*4+2];
			bmp.idata[(y*w+x)*4+3] = imgbuf[(y*w+x)*4+3];
		}
	}

	free(imgbuf);
	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

void Texture::getNormalVector(float x, float y, float v[3], NormalModifier *nMod)
{
	v[0]=0.0f; v[1]=0.0f; v[2]=1.0f; 
	if (!isValid())
		return;
	if (bmp.isItFloat)
		return;
	float invx = 1.0f;
	float invy = 1.0f;
	float perpower = 1.0f;
	if (nMod)
	{
		invx = nMod->invertX ? -1.0f : 1.0f;
		invy = nMod->invertY ? -1.0f : 1.0f;
		perpower = 1.0f/nMod->power;
	}

	int w = bmp.getWidth();
	int h = bmp.getHeight();

	float y2 = 1 - y;
	float rx = x * w;
	float ry = y2 * h;
	int ix = (int)( rx );
	int iy = (int)( ry );
	ix = ix % w;
	iy = iy % h;
	ix += ((ix>>31)&0x1) * w;
	iy += ((iy>>31)&0x1) * h;

	int cx = bmp.idata[(iy*w+ix)*4+0];
	int cy = bmp.idata[(iy*w+ix)*4+1];
	int cz = bmp.idata[(iy*w+ix)*4+2];
	float vx = (cx-128)/128.0f;
	float vy = (cy-128)/128.0f;
	float vz = (cz-128)/128.0f;
	Vector3d vecnormal = Vector3d(vx*invx, vy*invy, vz*perpower);
	vecnormal.normalize();
	v[0] = vecnormal.x;
	v[1] = vecnormal.y;
	v[2] = vecnormal.z;

}

//------------------------------------------------------------------------------------------------------------------------------------

bool Texture::updateModifiedTexFile(HWND hWnd, bool &reload, bool &all)
{
	HANDLE hFile = CreateFile(fullfilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
		return false;
	FILETIME fileTime;
	if (!GetFileTime(hFile, NULL, NULL, &fileTime))
	{
		CloseHandle(hFile);
		return false;
	}
	CloseHandle(hFile);

	if (fileTime.dwLowDateTime==fileModTime[0]  &&  fileTime.dwHighDateTime==fileModTime[1])
		return true;
	bool dontasktime = (fileTime.dwLowDateTime==fileModTimeDontAsk[0]  &&  fileTime.dwHighDateTime==fileModTimeDontAsk[1]);

	// ---- different modification dates - need to reload texture

	Logger::add("This texture should be reloaded:");
	Logger::add(fullfilename);

	bool willreload = false;
	if (all)
	{
		willreload = reload;
		if (willreload)
		{
			fileModTime[0] = fileTime.dwLowDateTime;
			fileModTime[1] = fileTime.dwHighDateTime;
		}
		else
		{
			fileModTimeDontAsk[0] = fileTime.dwLowDateTime;
			fileModTimeDontAsk[1] = fileTime.dwHighDateTime;
		}
	}
	else
	{
		if (!dontasktime)
		{
			char * cpfname = copyString(filename);
			_strupr(cpfname);
			char txt[2048];
			sprintf_s(txt, 2048, "TEXTURE %s HAS CHANGED. RELOAD?", cpfname);
			int res = runAskTexReloadDialog(hWnd, txt);

			all = (res>1);
		
			if (res==1)
			{
				willreload = true;
			}
			else
			{
				fileModTimeDontAsk[0] = fileTime.dwLowDateTime;
				fileModTimeDontAsk[1] = fileTime.dwHighDateTime;
			}

			if (all)
				reload = willreload;
		}
	}
	if (willreload)
	{
		bool ok = loadFromFile(fullfilename);
		if (is_it_converted_normal)
			convertMeToNormalMap();
		Logger::add(ok ? "OK" : "Failed");
	}

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------

