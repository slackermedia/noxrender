#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include "EM2Controls.h"
#include "log.h"

#define _USE_UNICODE

extern HMODULE hDllModule;

//----------------------------------------------------------------------

EM2Text::EM2Text(HWND hWnd) 
{
	colText = NGCOL_DARK_GRAY;
	colLink = NGCOL_LIGHT_GRAY;
	colLinkMouseOver = NGCOL_YELLOW; RGB(255,160,110);
	colBackGnd = NGCOL_TEXT_BACKGROUND;
	colDisabledText = RGB(64,64,64);
	colDisabledText = NGCOL_TEXT_DISABLED;
	colTextShadow = RGB(15,15,15);

	colBgMouseOver = RGB(0,0,0);
	alphaBgMouseOver = 0;

	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfont = true;
	hwnd = hWnd;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;
	marginTop = 1;
	marginHori = 0;

	DWORD style;
	style = GetWindowLong(hwnd, GWL_STYLE);
	if (style & ES_RIGHT)
		align = ALIGN_RIGHT;
	else
		align = ALIGN_LEFT;


	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
	{
		captionSize = 0;
		caption[captionSize] = 0;
	}	

	linkAddress = NULL;
	isItHyperLink = false;
	isMouseOverNow = false;
	useCursors = true;

	wholeAreaMouseOver = false;
	forwardMouseScroll = false;
	#ifdef GUI2_DRAW_TEXT_SHADOWS
		drawTextShadow = true;
	#else
		drawTextShadow = false;
	#endif

	GetClientRect(hwnd, &textRect);
}

//----------------------------------------------------------------------

EM2Text::~EM2Text() 
{
	if (caption)
		free(caption);
	caption = NULL;
	if  (hFont  &&  autodelfont)
		DeleteObject(hFont);
	hFont = 0;
}

//----------------------------------------------------------------------

void InitEM2Text()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2Text";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2TextProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2Text *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2Text * GetEM2TextInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2Text * emt = (EM2Text *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2Text * emt = (EM2Text *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emt;
}

//----------------------------------------------------------------------

void SetEM2TextInstance(HWND hwnd, EM2Text *emt)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emt);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emt);
	#endif
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2TextProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2Text * emt;
	emt = GetEM2TextInstance(hwnd);

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2Text * emt1 = new EM2Text(hwnd);
				SetEM2TextInstance(hwnd, (EM2Text *)emt1);			
			}
			break;
		case WM_DESTROY:
			{
				EM2Text * emt1;
				emt1 = GetEM2TextInstance(hwnd);
				delete emt1;
				SetEM2TextInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
			{
				if (!emt)
					break;

				RECT rect;
				HDC hdc, thdc;
				PAINTSTRUCT ps;
				SIZE sz;
				HANDLE hOldFont;

				DWORD style;
				style = GetWindowLong(hwnd, GWL_STYLE);
				bool disabled = ((style & WS_DISABLED) > 0);
				GetClientRect(hwnd, &rect);

				thdc = BeginPaint(hwnd, &ps);

				hdc = CreateCompatibleDC(thdc);
				HBITMAP backBMP = CreateCompatibleBitmap(thdc, rect.right, rect.bottom);
				HBITMAP oldBMP = (HBITMAP)SelectObject(hdc, backBMP);

				if (emt->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP * curBitmap = &emt->bgImage;
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, *curBitmap);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emt->bgShiftX, emt->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emt->colBackGnd));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emt->colBackGnd));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, TRANSPARENT);
					SetBkColor(hdc, emt->colBackGnd);
				}

				if (emt->alphaBgMouseOver>0  &&  emt->isMouseOverNow)
				{
					#ifdef GUI2_USE_GDIPLUS
					{
						ULONG_PTR gdiplusToken;		// activate gdi+
						Gdiplus::GdiplusStartupInput gdiplusStartupInput;
						Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
						{
							Gdiplus::Graphics graphics(hdc);

							Gdiplus::SolidBrush brushNormal(Gdiplus::Color(emt->alphaBgMouseOver,	GetRValue(emt->colBgMouseOver),	GetGValue(emt->colBgMouseOver),	GetBValue(emt->colBgMouseOver)	));
							graphics.FillRectangle(&brushNormal, 0,0, rect.right, rect.bottom);

						}	// gdi+ variables destructors here
						Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
					}
					#else
					#endif
				}

				SetBkColor(hdc, emt->colBackGnd);
				hOldFont = SelectObject(hdc, emt->getFont());
				GetClientRect(hwnd, &rect);

				#ifdef _USE_UNICODE
					wchar_t tttxt[2048];
					GetWindowTextW(hwnd, tttxt, 2048);
					int ulen = (int)wcslen(tttxt);
					GetTextExtentPoint32W(hdc, tttxt, ulen, &sz);
				#else
					GetTextExtentPoint32(hdc, emt->caption, emt->captionSize, &sz);
				#endif


				emt->evalTextRect(hdc, &(emt->textRect));
		
				int posx, posy;
				switch (emt->align)
				{
					case EM2Text::ALIGN_RIGHT:
						posx = (rect.right-rect.left-sz.cx-emt->marginHori);
						posy = (rect.bottom-rect.top-sz.cy+1)/2;
						break;
					case EM2Text::ALIGN_CENTER:
						posx = (rect.right-rect.left-sz.cx+1)/2;
						posy = (rect.bottom-rect.top-sz.cy+1)/2;
						break;
					default:
						posx = emt->marginHori;
						posy = (rect.bottom-rect.top-sz.cy+1)/2;
						break;
				}

				posy = rect.bottom/2 - (sz.cy+1)/2;

				if (emt->drawTextShadow)
				{
					SetTextColor(hdc, emt->colTextShadow);
					#ifdef _USE_UNICODE
						ExtTextOutW(hdc, posx-1, posy, ETO_CLIPPED, &rect, tttxt, ulen, 0);
						ExtTextOutW(hdc, posx+1, posy, ETO_CLIPPED, &rect, tttxt, ulen, 0);
						ExtTextOutW(hdc, posx, posy-1, ETO_CLIPPED, &rect, tttxt, ulen, 0);
						ExtTextOutW(hdc, posx, posy+1, ETO_CLIPPED, &rect, tttxt, ulen, 0);
					#else
						ExtTextOut(hdc, posx-1, posy, ETO_CLIPPED, &rect, emt->caption, emt->captionSize, 0);
						ExtTextOut(hdc, posx+1, posy, ETO_CLIPPED, &rect, emt->caption, emt->captionSize, 0);
						ExtTextOut(hdc, posx, posy-1, ETO_CLIPPED, &rect, emt->caption, emt->captionSize, 0);
						ExtTextOut(hdc, posx, posy+1, ETO_CLIPPED, &rect, emt->caption, emt->captionSize, 0);
					#endif
				}

				if (disabled)
					SetTextColor(hdc, emt->colDisabledText);
				else
				{
					if (emt->isItHyperLink)
					{
						if (emt->isMouseOverNow)
							SetTextColor(hdc, emt->colLinkMouseOver);
						else
							SetTextColor(hdc, emt->colLink);
					}
					else
						SetTextColor(hdc, emt->colText);
				}
				#ifdef _USE_UNICODE
					ExtTextOutW(hdc, posx, posy, ETO_CLIPPED, &rect, tttxt, ulen, 0);
				#else
					ExtTextOut(hdc, posx, posy, ETO_CLIPPED, &rect, emt->caption, emt->captionSize, 0);
				#endif
			
				SelectObject(hdc, hOldFont);

				BitBlt(thdc, 0, 0,  rect.right, rect.bottom,  hdc, 0, 0, SRCCOPY);
				SelectObject(hdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(hdc); 

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_SETTEXT:
			{
				char * b = (char *)lParam;

				int ss;
				if (b)
					ss = (int)strlen(b);
				else
					ss = 0;
				char * newBuff = (char *)malloc(ss+2);
				if (!newBuff)
					return false;
				if (b)
					sprintf_s(newBuff, ss+2, "%s", b);
				else
					sprintf_s(newBuff, ss+2, "");
				if (emt->caption)
					free(emt->caption);
				emt->caption = newBuff;
				emt->captionSize = ss;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_MOUSEWHEEL:
			{
				if (emt->forwardMouseScroll)
					SendMessage(GetParent(hwnd), WM_MOUSEWHEEL, wParam, lParam);
			}
			break;
		case WM_MOUSEMOVE:
			{
				if (emt->forwardMouseScroll)
					SetFocus(hwnd);
				if (emt->isItHyperLink)
				{
					POINT pt = { LOWORD(lParam), HIWORD(lParam) };
					if (GetCapture() != hwnd)
					{
						SetCapture(hwnd);
					}
					else
					{
						RECT rect;
						GetClientRect(hwnd, &rect);
						if (!PtInRect(&rect, pt))
						{
							ReleaseCapture();
						}
					}

					RECT rect;
					GetClientRect(hwnd, &rect);
					if (!PtInRect(emt->wholeAreaMouseOver ? &rect : &emt->textRect, pt))
					{
						if (emt->isMouseOverNow)
							InvalidateRect(hwnd, NULL, FALSE);
						SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW)) );
						emt->isMouseOverNow = false;
					}
					else
					{
						if (!emt->isMouseOverNow)
							InvalidateRect(hwnd, NULL, FALSE);
						if (emt->linkAddress  ||  emt->useCursors)
							SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_HAND)) );
						emt->isMouseOverNow = true;
					}
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emt->isMouseOverNow = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				RECT rect;
				GetClientRect(hwnd, &rect);
				if (PtInRect(emt->wholeAreaMouseOver ? &rect : &emt->textRect, pt))
				{
					if (emt->isItHyperLink)
					{
						if (emt->linkAddress)
							ShellExecute(hwnd, "open", emt->linkAddress, NULL, NULL, SW_SHOWNORMAL);
						else
							SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLongPtr(hwnd, GWL_ID), BN_CLICKED), (LPARAM)hwnd);
						emt->isMouseOverNow = false;
						InvalidateRect(hwnd, NULL, FALSE);
					}
				}
			}
			break;
		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

bool EM2Text::changeCaption(char *newCaption)
{
	SetWindowText(hwnd, newCaption);
	return true;
}

//----------------------------------------------------------------------

bool EM2Text::setHyperLink(char * newAddress)
{
	char * oldAddress = linkAddress;
	if (newAddress)
	{
		if (strlen(newAddress)>0)
		{
			char * cpAddress = copyString(newAddress);
			if (!cpAddress)
				return false;
			linkAddress = cpAddress;
			isItHyperLink = true;
		}
		else
		{
			linkAddress = NULL;
			isItHyperLink = false;
		}
	}
	else
	{
		linkAddress = NULL;
		isItHyperLink = false;
	}

	if (oldAddress)
		free(oldAddress);

	InvalidateRect(hwnd, NULL, false);
	return true;
}

//----------------------------------------------------------------------

bool EM2Text::evalTextRect(HDC &hdc, RECT * rect)
{
	if (!rect)
		return false;

	RECT crect;
	GetClientRect(hwnd, &crect);
	SIZE sz;
	#ifdef _USE_UNICODE
		wchar_t tttxt[2048];
		GetWindowTextW(hwnd, tttxt, 2048);
		int ulen = (int)wcslen(tttxt);
		GetTextExtentPoint32W(hdc, tttxt, ulen, &sz);
	#else
		GetTextExtentPoint32(hdc, caption, captionSize, &sz);
	#endif
	int posx, posy;
	switch (align)
	{
		case ALIGN_RIGHT:
			posx = (crect.right-crect.left-sz.cx-marginHori);
			posy = (crect.bottom-crect.top-sz.cy)/2;
			break;
		case ALIGN_CENTER:
			posx = (crect.right-crect.left-sz.cx)/2;
			posy = (crect.bottom-crect.top-sz.cy)/2;
			break;
		default:
			posx = marginHori;
			posy = (crect.bottom-crect.top-sz.cy)/2;
			break;
	}

	rect->left = posx;
	rect->top = posy;
	rect->right = posx+sz.cx;
	rect->bottom = posy+sz.cy;
	return true;
}

//----------------------------------------------------------------------

bool EM2Text::setFont(HFONT hNewFont, bool autodel)
{
	if (autodelfont)
		DeleteObject(hFont);
	hFont = hNewFont;
	autodelfont = autodel;
	return true;
}

//----------------------------------------------------------------------

HFONT EM2Text::getFont()
{
	return hFont;
}

//----------------------------------------------------------------------
