#define _CRT_RAND_S
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "log.h"
#include "MatEditor.h"


INT_PTR CALLBACK UserPassDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				GlobalWindowSettings::colorSchemes.apply(GetEMGroupBarInstance(		GetDlgItem(hWnd, IDC_USERPASS_GB1)));
				GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(		GetDlgItem(hWnd, IDC_USERPASS_CANCEL)));
				GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(		GetDlgItem(hWnd, IDC_USERPASS_OK)));
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_USERPASS_TEXT1)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_USERPASS_TEXT2)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_USERPASS_TEXT3)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMCheckBoxInstance(		GetDlgItem(hWnd, IDC_USERPASS_REMEMBER)));
				GlobalWindowSettings::colorSchemes.apply(GetEMEditSimpleInstance(	GetDlgItem(hWnd, IDC_USERPASS_LOGIN)));
				GlobalWindowSettings::colorSchemes.apply(GetEMEditSimpleInstance(	GetDlgItem(hWnd, IDC_USERPASS_PASSWORD)));

				EMCheckBox * emremember = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_USERPASS_REMEMBER));
				EMEditSimple * emlogin = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_USERPASS_LOGIN));
				EMEditSimple * empass = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_USERPASS_PASSWORD));

				emremember->selected = MatEditorWindow::onlinematlib.remember;
				InvalidateRect(emremember->hwnd, NULL, false);
				emlogin->setText(MatEditorWindow::onlinematlib.username);
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_USERPASS_OK:
						{
							CHECK(wmEvent==BN_CLICKED);

							EMCheckBox * emremember = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_USERPASS_REMEMBER));
							EMEditSimple * emlogin = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_USERPASS_LOGIN));
							EMEditSimple * empass = GetEMEditSimpleInstance(GetDlgItem(hWnd, IDC_USERPASS_PASSWORD));

							MatEditorWindow::onlinematlib.remember = emremember->selected;
							MatEditorWindow::onlinematlib.username = emlogin->getText();
							char * hash = createMD5(empass->getText());
							MatEditorWindow::onlinematlib.passwordhash = hash;

							if (emremember->selected)
								saveUserPassToRegistry(MatEditorWindow::onlinematlib.username, hash, 13);
							else
								delUserPassFromRegistry();

							MatEditorWindow::onlinematlib.invalidUserPass = false;

							EndDialog(hWnd, (INT_PTR)1);
						}
						break;
					case IDC_USERPASS_CANCEL:
						{
							CHECK(wmEvent==BN_CLICKED);
							EndDialog(hWnd, (INT_PTR)0);
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}


