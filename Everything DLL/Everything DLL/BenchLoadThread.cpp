#define _CRT_RAND_S
#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <wingdi.h>
#include "BenchmarkWindow.h"
#include "resource.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include "log.h"

//----------------------------------------------------------------------------------------------------------------

DWORD WINAPI BenchmarkWindow::LoadSceneThreadProc(LPVOID lpParameter)
{
	Raytracer * rtr = Raytracer::getInstance();
	Logger::add("load scene (thread)");
	rtr->loadBenchmarkScenes(&BenchmarkWindow::notifyProgressBarCallback);
	Scene * sc = rtr->curScenePtr;
	Camera * ccam = rtr->curScenePtr->getActiveCamera();
	EMPView * empvimg = GetEMPViewInstance(hImage);
	empvimg->imgMod->copyDataFromAnother(&ccam->iMod);

	sc->registerProgressCallback(NULL, 0);

	setMode(NBMODE_NORMAL);

	checkAndAddLicense();

	return 0;
}

//----------------------------------------------------------------------------------------------------------------

bool BenchmarkWindow::startLoadingThread()
{
	//showLoading();
	setMode(NBMODE_LOADING);

	DWORD threadId;
	HANDLE hThread = CreateThread( 
			NULL,
			0,
			(LPTHREAD_START_ROUTINE)(LoadSceneThreadProc),
			(LPVOID)0,
			0,
			&threadId);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

void BenchmarkWindow::notifyProgressBarCallback(char * message, float progress)
{
	EMProgressBar * empb = GetEMProgressBarInstance(hProgress);
	if (empb)
		empb->setPos(progress);
	InvalidateRect(hProgress, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------
