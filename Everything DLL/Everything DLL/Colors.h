#ifndef __COLORS_AND_BUFFER_H
#define __COLORS_AND_BUFFER_H

#include <windows.h>
#include <xmmintrin.h>
#include <intrin.h>

#include "DLL.h"
#include <time.h>
#include <vector>
#include <math.h>
#include "CameraResponse.h"
using namespace std;

class DiaphragmSampler;

class ImageBuffer;
class ImageByteBuffer;
class FloatBuffer;

#define BBUF_SSE

#define USE_OPENCL_POSTPRO

class Camera;
class SunSky;

#include "Color4.h"

//---------------------------------------------------------------------

struct RedrawRegion
{
	HWND hwnd;
	int top, bottom, left, right;
};

//---------------------------------------------------------------------

class NoxEXIFdata
{
public:
	short iso;
	unsigned int shutterNom;
	unsigned int shutterDenom;
	unsigned int fNumberNom;
	unsigned int fNumberDenom;
	unsigned int focalNom;
	unsigned int focalDenom;
	NoxEXIFdata()  {iso=0; shutterNom = 0; shutterDenom = 0; fNumberNom = 0; fNumberDenom = 0; focalNom = 0; focalDenom = 0; }
	~NoxEXIFdata() {}
};

//---------------------------------------------------------------------

class FinalModifier
{
public:
	float aperture;
	float focal;	// in meters
	float focusDistance;
	int quality;
	int numThreads;
	int algorithm;


	int bokehRingBalance;
	int bokehRingSize;
	int bokehFlatten;
	int bokehVignette;

	int bladesNum;
	float bladesAngle;
	float bladesRadius;


	bool roundBlades;
	bool dofOn;
	bool dotsEnabled;
	int dotsRadius;
	float dotsThreshold;
	int dotsDensity;
	float grain;
	int vignette;

	bool bloomEnabled;
	float bloomPower;
	unsigned int bloomArea;
	float bloomThreshold;
	bool bloomAttenuationOn;
	Color4 bloomAttenuationColor;
	bool glareEnabled;
	float glarePower;
	float glareDispersion;
	float glareThreshold;
	int glareArea;
	bool glareTextureApertureOn;
	bool glareTextureObstacleOn;

	Color4 fogColor;
	float  fogDist;
	float  fogSpeed;
	bool   fogExclSky;
	bool   fogEnabled;

	bool aerialEnabled;
	float aerialScale;

	float chromaticShiftLens;
	float chromaticShiftDepth;
	float chromaticAchromatic;

	float chromaticPlanarRed;
	float chromaticPlanarGreen;
	float chromaticPlanarBlue;

	void copyDataFromOther(FinalModifier * fMod, bool dofTab, bool effectsTab);

	FinalModifier() 
			{ 
				aperture = 4.0f; focal=0.035f; focusDistance=1.0f; quality=6; 
				bokehRingBalance = 0; bokehRingSize = 20; bokehFlatten = 0; bokehVignette = 0;
				bladesNum = 5; bladesAngle = 0.0f; bladesRadius = 1.0f; 
				roundBlades = false; dofOn = false;
				chromaticShiftLens = 0.0f; chromaticShiftDepth = 0.0f; chromaticAchromatic = 0.0f;
				numThreads = 2; algorithm = 1;	grain = 0.0f;  vignette = 20;
				dotsEnabled = false; dotsRadius = 2; dotsThreshold = 10.0f; dotsDensity = 1;
				bloomEnabled = false;  bloomPower = 0.0f;  bloomArea = 192;  bloomThreshold = 2.0f;
				bloomAttenuationOn = false;  bloomAttenuationColor = Color4(1.0f,1.0f,1.0f);
				glareEnabled = false;  glarePower = 0.0f;  glareDispersion = 0.0f;  glareThreshold = 2.0f;  glareArea = 128;
				glareTextureApertureOn = false;  glareTextureObstacleOn = false;
				chromaticPlanarRed = 0.0f; chromaticPlanarGreen = 0.0f; chromaticPlanarBlue = 0.0f;
				fogColor = Color4(0.75f, 0.75f, 0.75f); fogSpeed = 1.0f; fogExclSky = false; fogDist = 0.0f; fogEnabled = false;
				aerialEnabled = false; aerialScale = 1.0f;
			}
};

//---------------------------------------------------------------------

class ImageModifier
{
private:
	float gammaValue;
	float toneMappingValue;
	float multiplier;
	float iso_ev_compensation;
	float iso_camera;
	float gi_compensation;
	int filter;
	float brightness;
	float contrast;
	float saturation;
	float temperature;
	float addR;
	float addG;
	float addB;
	int vignette;
	int response_function;
	bool dotsEnabled;
	int dotsRadius;
	float dotsThreshold;
	int dotsDensity;
	bool cbCRed;
	bool cbCGreen;
	bool cbCBlue;
	bool cbCLuminance;
	vector<int> pointsRx;
	vector<int> pointsRy;
	vector<int> pointsGx;
	vector<int> pointsGy;
	vector<int> pointsBx;
	vector<int> pointsBy;
	vector<int> pointsLx;
	vector<int> pointsLy;
public:
	static const float GAMMA_MIN_VALUE;
	static const float GAMMA_MAX_VALUE;
	static const float TONE_MAPPING_MIN_VALUE;
	static const float TONE_MAPPING_MAX_VALUE;
	static const float MULTIPLY_MIN_VALUE;
	static const float MULTIPLY_MAX_VALUE;

	void setGamma(float gamma)					{ gammaValue = min(GAMMA_MAX_VALUE, max(GAMMA_MIN_VALUE, gamma));    }
	void setToneMappingValue(float tone)		{ toneMappingValue = min(TONE_MAPPING_MAX_VALUE, max(TONE_MAPPING_MIN_VALUE, tone));    }
	void setMultiplier(float multiply)			{ multiplier = min(MULTIPLY_MAX_VALUE, max(MULTIPLY_MIN_VALUE, multiply));    }
	void setBrightness(float br)				{ brightness = br; }
	void setContrast(float c)					{ contrast = c; }
	void setSaturation(float s)					{ saturation = s; }
	void setRGBAdd(float r, float g, float b)	{ addR = r;  addG = g;  addB = b; }
	void setTemperature(float s)				{ temperature = s; }
	void setDotsEnabled(bool on)				{ dotsEnabled = on; }
	void setDotsRadius(int i)					{ dotsRadius = i; }
	void setDotsThreshold(float f)				{ dotsThreshold = f; }
	void setDotsDensity(int i)					{ dotsDensity = i; }
	void setFilter(int type)					{ filter = type; }
	void setVignette(int v)						{ vignette = v; }
	void setResponseFunctionNumber(int r)		{ response_function = max(0, min(r, NOX_NUM_RESPONSE_FILTERS)); }
	void setISO_EV_compensation(float f)		{ iso_ev_compensation = f; }
	void setISO_camera(float f) 				{ iso_camera = f; }
	void setGICompensation(float f)				{ gi_compensation = f; }
	void evalMultiplierFromISO_and_EV()			{ multiplier = iso_camera * 0.01f * pow(2.0f, iso_ev_compensation); }



	void setCurveCheckboxes(bool r, bool g, bool b, bool l)    { cbCRed = r;  cbCGreen = g;  cbCBlue = b;  cbCLuminance = l; }

	int getCurveRedPointsCount() const { return (int)pointsRx.size(); }
	int getCurveGreenPointsCount() const  { return (int)pointsGx.size(); }
	int getCurveBluePointsCount() const  { return (int)pointsBx.size(); }
	int getCurveLuminancePointsCount() const  { return (int)pointsLx.size(); }
	void getCurveRedPoint(int index, int &x, int &y)  const    { x = pointsRx[index];  y = pointsRy[index]; }
	void getCurveGreenPoint(int index, int &x, int &y)  const  { x = pointsGx[index];  y = pointsGy[index]; }
	void getCurveBluePoint(int index, int &x, int &y)  const   { x = pointsBx[index];  y = pointsBy[index]; }
	void getCurveLuminancePoint(int index, int &x, int &y)  const  { x = pointsLx[index];  y = pointsLy[index]; }

	void delCurveRedPoints()     { pointsRx.clear(); pointsRy.clear(); }
	void delCurveGreenPoints()   { pointsGx.clear(); pointsGy.clear(); }
	void delCurveBluePoints()    { pointsBx.clear(); pointsBy.clear(); }
	void delCurveLuminancePoints()   { pointsLx.clear(); pointsLy.clear(); }
	void delCurveAllPoints()    { delCurveRedPoints();  delCurveGreenPoints();  delCurveBluePoints();  delCurveLuminancePoints(); }

	void addRedPoint(int x, int y)    { pointsRx.push_back(x); pointsRy.push_back(y); }
	void addGreenPoint(int x, int y)  { pointsGx.push_back(x); pointsGy.push_back(y); }
	void addBluePoint(int x, int y)   { pointsBx.push_back(x); pointsBy.push_back(y); }
	void addLuminancePoint(int x, int y)  { pointsLx.push_back(x); pointsLy.push_back(y); }

	float getBrightness() const				{ return brightness;}
	float getGamma() const					{ return gammaValue;}
	float getToneMappingValue() const		{ return toneMappingValue; }
	float getMultiplier() const				{ return multiplier; }
	float getContrast() const				{ return contrast; }
	float getSaturation() const				{ return saturation; }
	float getR() const						{ return addR; }
	float getG() const						{ return addG; }
	float getB() const						{ return addB; }
	float getTemperature() const			{ return temperature; }
	bool getDotsEnabled() const				{ return dotsEnabled; }
	int getDotsRadius() const				{ return dotsRadius; }
	float getDotsThreshold() const			{ return dotsThreshold; }
	int getDotsDensity() const				{ return dotsDensity; }
	int getFilter() const					{ return filter; }
	int getVignette() const					{ return vignette; }
	int getResponseFunctionNumber() const	{ return response_function; }
	float getISO_EV_compensation() const	{ return iso_ev_compensation; }
	float getISO_camera() const				{ return iso_camera; }
	float getGICompensation() const			{ return gi_compensation; }


	void getCurveCheckboxes(bool &r, bool &g, bool &b, bool &l) const   { r = cbCRed;  g = cbCGreen;  b = cbCBlue;  l = cbCLuminance; }

	ImageBuffer * modifyImage(ImageBuffer * src);
	void copyDataFromAnother(const ImageModifier * src, bool postTab, bool corrTab)
	{
		if (postTab)
		{
			gammaValue = src->getGamma();
			toneMappingValue = src->getToneMappingValue();
			multiplier = src->getMultiplier();
			filter = src->getFilter();
			vignette = src->getVignette();
			response_function = src->getResponseFunctionNumber();
			iso_ev_compensation = src->getISO_EV_compensation();
			iso_camera = src->getISO_camera();
			gi_compensation = src->getGICompensation();
			dotsEnabled = src->getDotsEnabled();
			dotsRadius = src->getDotsRadius();
			dotsThreshold = src->getDotsThreshold();
			dotsDensity = src->getDotsDensity();
		}

		if (corrTab)
		{
			brightness = src->getBrightness();
			contrast = src->getContrast();
			saturation = src->getSaturation();
			temperature = src->getTemperature();
			addR = src->getR();
			addG = src->getG();
			addB = src->getB();

			src->getCurveCheckboxes(cbCRed, cbCGreen, cbCBlue, cbCLuminance);
			pointsRx.clear();
			pointsRy.clear();
			pointsGx.clear();
			pointsGy.clear();
			pointsBx.clear();
			pointsBy.clear();
			pointsLx.clear();
			pointsLy.clear();
			for (int i=0; i<src->getCurveRedPointsCount(); i++)
			{
				int x,y;
				src->getCurveRedPoint(i, x, y);
				addRedPoint(x,y);
			}
			for (int i=0; i<src->getCurveGreenPointsCount(); i++)
			{
				int x,y;
				src->getCurveGreenPoint(i, x, y);
				addGreenPoint(x,y);
			}
			for (int i=0; i<src->getCurveBluePointsCount(); i++)
			{
				int x,y;
				src->getCurveBluePoint(i, x, y);
				addBluePoint(x,y);
			}
			for (int i=0; i<src->getCurveLuminancePointsCount(); i++)
			{
				int x,y;
				src->getCurveLuminancePoint(i, x, y);
				addLuminancePoint(x,y);
			}
		}

	}

	void copyDataFromAnother(const ImageModifier * src)
	{
		copyDataFromAnother(src, true,  true);
	}

	ImageModifier(float gamma, float tone, float multiply);
	ImageModifier();
	~ImageModifier();
};

//---------------------------------------------------------------------

class ImageByteBuffer
{
public:
	int width;
	int height;
	int w4;
	bool drawingBufferLock;

	char * timestamp;
	void applyTimeStamp();
	void applyLogoStamp(ImageBuffer * logo);

	COLORREF ** imgBuf;
	DECLDIR bool allocBuffer(int w, int h);
	DECLDIR void freeBuffer();
	bool clearBuffer();
	void lockBuffer();
	void unlockBuffer();
	void copyToClipboard(HWND hWnd);

	ImageByteBuffer * copy();
	ImageByteBuffer * copyWithZoom(double zoom);
	bool copyImageFrom(ImageByteBuffer * other);
	bool loadFromResource(HMODULE hMod, WORD idRes);

	bool loadFromWindowsFormat(char * filename);
	bool saveAsWindowsFormat(char * filename, int format, NoxEXIFdata * exif);

	void freeTimeStamp();
	DECLDIR ImageByteBuffer();
	DECLDIR ~ImageByteBuffer();
};

//---------------------------------------------------------------------

class ImageBuffer
{
public:
	int width;
	int height;

	clock_t cTime, lTime;

	float refreshTime;
	bool refresh;
	HWND refreshHWND;
	bool dontDeleteMeInDestructor;

	Color4 ** imgBuf;
	int ** hitsBuf;
	bool allocBuffer(int w, int h);
	void freeBuffer();
	bool clearBuffer();
	bool setHitsValues(int val);
	
	int vig_buck_pos_x;
	int vig_buck_pos_y;
	int vig_orig_width;
	int vig_orig_height;

	void addPixelColor(int x, int y, const Color4 &c, bool incrementHits);

	bool saveAsEXR(char * filename, const ImageModifier &im, int aa);
	bool saveAsWindowsFormat(char * filename, const ImageModifier &im, int format, char * timestamp, ImageBuffer * logo, int aa, NoxEXIFdata * exif);

	bool applyBrightness(float val);
	bool applyContrast(float val);
	bool applySaturation(float val);
	bool applyRGBCorrection(float r, float g, float b);
	bool applyCurve(void * emc);
	bool applyGrain(float grain);
	bool applyTemperature(float t);
	bool applyISOToneGamma(float mpl, float tone, float gamma);
	bool applySimpleVignette(int val, float aperture);
	bool removeBurnedDots(int radius, float thres, int maxposs);
	bool applyFog(FloatBuffer * depthmap, float dist1, float dist2, Color4 &att1, Color4 &att2, int func, float speed, bool exclSky);
	bool applyAerialPerspective(FloatBuffer * depthmap, float scale, float enMpl, Camera * cam, SunSky * sunsky, bool exclSky);
	ImageBuffer * applyFilter(int type, int w, int h);
	ImageBuffer * applyBoxFilter(int w, int h);
	ImageBuffer * applyBox2Filter(int w, int h);
	ImageBuffer * applyAAWeightedFilter(int w, int h);
	ImageBuffer * applyMitchellNetravaliFilter(int w, int h);
	ImageBuffer * applyCatmullRomFilter(int w, int h);
	ImageBuffer * applyBSplineFilter(int w, int h);
	ImageBuffer * applyGaussFilter(int w, int h);
	ImageBuffer * applyGaussExtremeFilter(int w, int h);
	ImageBuffer * applyNoAAFilter(int w, int h);
	bool loadFromResource(WORD idRes);
	bool loadFromEXR(char * filename);
	ImageByteBuffer * toImageByteBuffer();
	bool copyToImageByteBuffer(ImageByteBuffer * img, COLORREF badColor);
	bool copyPartToImageByteBuffer(ImageByteBuffer * img, int px, int py);
	ImageBuffer * applyGauss(int numPixels);
	ImageBuffer * applyGaussAdaptive();
	ImageBuffer * applyBloomAndGlare(ImageModifier * iMod, bool nowRendering);//float bloom);	//deprecated
	ImageBuffer * doPostProcess(int aa, ImageModifier * iMod, FinalModifier * fMod, FloatBuffer * depthBuf, char * message=NULL);

	ImageBuffer * evalSingleBloom(int type, float ratio, FinalModifier * fMod);
	ImageBuffer * applyFakeBloomFast(FinalModifier * fMod);
	ImageBuffer * applyGlare(ImageBuffer * dstBuf, FinalModifier * fMod);
	ImageBuffer * evalChromaticAberrationPlanar(float valR, float valG, float valB);
	ImageBuffer * copyResize(int nw, int nh);
	ImageBuffer * copyBuffer();
	ImageBuffer * applyCameraResponseFunction(int numberResp);
	ImageBuffer * evalSimpleDOFImage(FloatBuffer * depth, FinalModifier * fMod);
	float findGaussianBlurRoForNumPixels(int numPixels);

	ImageBuffer();
	~ImageBuffer();
};

//---------------------------------------------------------------------

class FloatBuffer
{
	bool sseAlign;
public:
	int width, height;
	float ** fbuf;
	unsigned int ** hbuf;

	bool allocBuffer(int w, int h, bool sseAligned, bool allocHits);
	bool clearBuffer();
	void freeBuffer();
	bool copyFromOther(FloatBuffer * other);
	FloatBuffer * copyToNew();
	bool isSSEaligned();
	bool copyAsDepthToByteBuffer(ImageByteBuffer * img, int aa);
	bool copyAsDOFTemperatureToByteBuffer(ImageByteBuffer * img, float dofDist, int aa, float focal, float aperture);

	FloatBuffer();
	~FloatBuffer();
};

//---------------------------------------------------------------------

class FakeDOFThread
{
public:
	HANDLE hThread;
	DWORD threadId;
	int numAllThreads;
	int curThreadNum;
	int algorithmChosen;
	float progress;	// 0 - 100
	bool jobDone;
	bool abort;

	ImageBuffer * imgSrc;
	ImageBuffer * imgDst;
	FloatBuffer * depth;
	FinalModifier * fMod;
	DiaphragmSampler * diaph;
	float * a1sRadius;
	float * a1dRadius;
	bool a1Phase1Done;
	bool a1Phase1DoneInAll;
	bool a1Phase2Done;
	bool a1Phase2DoneInAll;

	bool runThread();
	static DWORD WINAPI FakeDofThreadProc(LPVOID lpParameter);

	bool simpleDOFAlgorithm1();
	bool simpleDOFAlgorithm2();
	bool simpleDOFAlgorithm3();

	FakeDOFThread();
	~FakeDOFThread();
};

//---------------------------------------------------------------------

bool loadImageData(char * filename, ImageBuffer * imgbuf);	// sth is wrong, don't use
bool saveImageData(char * filename, ImageBuffer * imgbuf);
bool loadFloatData(char * filename, FloatBuffer * fbuf);
bool saveFloatData(char * filename, FloatBuffer * fbuf);
int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
HBITMAP loadHBitmapFromBinaryResource(HMODULE hMod, WORD idRes);
char * formatTime(int sec);
char * createTimeStamp();


#endif

