#define _CRT_RAND_S
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "BenchmarkWindow.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include "log.h"

char resultFilename2[2048];
bool sendBenchResultAsThread(BenchResult * result);
int checkResSendError();
int checkResSendError2();
void closeBenchResDialog(bool ok);

HWND hUploadBenchResWindow;
bool abortSendingResult = false;

size_t write_data (void *ptr, size_t size, size_t nmemb, void *stream);	
int MessageBoxNox(HWND hParent, char *messageText, char *titleText, HFONT hFontText, HFONT hFontButton);

//-----------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK UploadBenchResDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static BenchResult * bres = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (lParam==NULL)
				{
					MessageBoxNox(hWnd, "Internal error. Result won't be send.", "Error", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
					EndDialog(hWnd, 1);
				}
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				bres = (BenchResult *)lParam;

				EMButton * embAbort = GetEMButtonInstance(GetDlgItem(hWnd, IDC_UPLRESBENCH_ABORT));
				GlobalWindowSettings::colorSchemes.apply(embAbort);
				embAbort->setFont(BenchmarkWindow::hFontMessageButton);

				HWND hText = GetDlgItem(hWnd, IDC_UPLRESBENCH_STATUS);
				SendMessage(hText, WM_SETFONT, (WPARAM)BenchmarkWindow::hFontMessageText, 1);


				hUploadBenchResWindow = hWnd;
				sendBenchResultAsThread(bres);
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, 1);
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_UPLRESBENCH_ABORT:
						{
							CHECK(wmEvent==BN_CLICKED);
							abortSendingResult = true;
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:	
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------------------------

bool randomTempFullFilename(char * fname, int blen)
{
	CHECK(fname);
	CHECK(blen>0);

	char temppath[2048];
	int ll = GetTempPath(2048, temppath);
	if (ll > 2048)
		return false;

	if (ll < 3)
		return false;

	char temppathlong[2048];
	int ll2 = GetLongPathName(temppath, temppathlong, 2048);
	if (ll2 > 2048)
		return false;

	if (temppathlong[ll2-1] == '\\'  ||  temppathlong[ll2-1] == '/')
		temppathlong[ll2-1] = 0;

	if (!dirExists(temppathlong))
		return false;

	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(temppathlong, "res_", unique, fname);

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

int noxbenchUploadProgressCallback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
	Logger::add(".");
	if (abortSendingResult)
		return 1;
	return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

DWORD WINAPI UploadResThreadProc(LPVOID lpParameter)
{

	CHECK(lpParameter);
	abortSendingResult = false;
	BenchResult * result = (BenchResult *)lpParameter;

	if (!randomTempFullFilename(resultFilename2, 2048))
	{
		closeBenchResDialog(false);
		return 0;
	}

	FILE * file = NULL;
	if (fopen_s(&file, resultFilename2, "wb"))
	{
		closeBenchResDialog(false);
		return 0;
	}
	if (!file)
	{
		closeBenchResDialog(false);
		return 0;
	}

	CURL *curl;
	CURLcode res;

	char * strURL = "www.evermotion.org/nox/noxUploadBenchmark";
	char sc_threadsCount[64], sc_coresCount[64], sc_cpusCount[64], sc_resultF[64], sc_engineversion[64], sc_sessionID[64], hashInput[2048];
	sprintf_s(sc_threadsCount, 64, "%d", result->num_threads);
	sprintf_s(sc_coresCount, 64, "%d", result->num_cores);
	sprintf_s(sc_cpusCount, 64, "%d", result->num_cpus);
	sprintf_s(sc_engineversion, 64, "%d.%d.%d", result->nox_ver_major, result->nox_ver_minor, result->nox_ver_build);
	_sprintf_s_l(sc_resultF, 64, "%.2f", noxLocale, result->result);
	sprintf_s(sc_sessionID, 64, "%u", result->session_id);
	_sprintf_s_l(hashInput, 2048, "%.2f/%d/%d/%d/%s/%d.%d.%d/%u/NOX", noxLocale, 
		result->result, result->num_cpus, result->num_cores, result->num_threads, result->cpuname, result->nox_ver_major, result->nox_ver_minor, result->nox_ver_build, result->session_id);
	char * hashVer = createMD5(hashInput);
	Logger::add("Sending result...");
	Logger::add("Result:");
	Logger::add(sc_resultF);
	Logger::add("CPU:");
	Logger::add(result->cpuname);
	Logger::add("CPUs:");
	Logger::add(sc_cpusCount);
	Logger::add("Cores:");
	Logger::add(sc_coresCount);
	Logger::add("Threads:");
	Logger::add(sc_threadsCount);
	Logger::add("Result ID:");
	Logger::add(sc_sessionID);
	Logger::add(hashVer);


	curl_global_init(CURL_GLOBAL_ALL); 
	curl = curl_easy_init(); 

	curl_easy_setopt(curl, CURLOPT_POST, 1); 
	
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0); 
	curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, noxbenchUploadProgressCallback); 

	struct curl_httppost *post=NULL;
	struct curl_httppost *last=NULL;
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pResult",
			   CURLFORM_COPYCONTENTS, sc_resultF, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pNumCpus",
			   CURLFORM_COPYCONTENTS, sc_cpusCount, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pNumCores",
			   CURLFORM_COPYCONTENTS, sc_coresCount, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pNumThreads",
			   CURLFORM_COPYCONTENTS, sc_threadsCount, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pCpuName",
			   CURLFORM_COPYCONTENTS, result->cpuname, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pNoxVersion",
			   CURLFORM_COPYCONTENTS, sc_engineversion, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pSessionID",
			   CURLFORM_COPYCONTENTS, sc_sessionID, CURLFORM_END);
	curl_formadd(&post, &last,
               CURLFORM_COPYNAME, "pSum",
			   CURLFORM_COPYCONTENTS, hashVer, CURLFORM_END);

	curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
	curl_easy_setopt(curl, CURLOPT_URL, strURL); 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); 
	
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

	res = curl_easy_perform(curl); 
	if (res)
	{
		switch (res)
		{
			case CURLE_ABORTED_BY_CALLBACK:
				MessageBoxNox(BenchmarkWindow::hMain, "Aborted by user.", "Error", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
				break;
			default:
				MessageBoxNox(BenchmarkWindow::hMain, "Connection error.", "Error", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
				break;
		}
		closeBenchResDialog(false);
		return 1;
	}

	// always cleanup 
	curl_easy_cleanup(curl);
	curl_global_cleanup();
	fclose(file);

	int s = checkResSendError2();
	if (fileExists(resultFilename2)  &&  1002!=s)
	{
		DeleteFile(resultFilename2);
	}
	
	switch (s)
	{
		case 0:
			MessageBoxNox(BenchmarkWindow::hMain, "Result successfully sent.\nThanks for your time.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
		case 1:
			MessageBoxNox(BenchmarkWindow::hMain, "Result rejected. Error 1: Request query invalid.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
		case 2:
			MessageBoxNox(BenchmarkWindow::hMain, "Result rejected. Error 2: Request query invalid.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
		case 3:
			MessageBoxNox(BenchmarkWindow::hMain, "Result rejected. Result ID already exists.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
		case 4:
			MessageBoxNox(BenchmarkWindow::hMain, "Result rejected. Other error.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
		case 1001:
			MessageBoxNox(BenchmarkWindow::hMain, "Could not open result file. Upload not verified.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
		case 1002:
			MessageBoxNox(BenchmarkWindow::hMain, "Result file is not valid. Upload not verified.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
		case 1003:
			MessageBoxNox(BenchmarkWindow::hMain, "Upload unknown error code.", "Info", BenchmarkWindow::hFontMessageText, BenchmarkWindow::hFontMessageButton);
			break;
	}

	closeBenchResDialog(s==0);
	return 1;
}

//-----------------------------------------------------------------------------------------------------------------------

void closeBenchResDialog(bool ok)
{
	EndDialog(hUploadBenchResWindow, ok?0:1);
}

//-----------------------------------------------------------------------------------------------------------------------

bool sendBenchResultAsThread(BenchResult * result)
{
	CHECK(result);

	DWORD threadId;
	HANDLE hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(UploadResThreadProc), (LPVOID)result, 0, &threadId);

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

int checkResSendError()
{
	FILE * file;
	if (fopen_s(&file, resultFilename2, "rb"))
		return 1001;

	char buf[64];
	int r = (int)fread(buf, 1, 64, file);
	if (r!=1)
	{
		fclose(file);
		return 1002;
	}

	fclose(file);

	switch (buf[0])
	{
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		default: return 1003;
	}
}

//-----------------------------------------------------------------------------------------------------------------------

int checkResSendError2()
{
	FILE * file;
	if (fopen_s(&file, resultFilename2, "rb"))
		return 1001;

	_fseeki64(file, 0, SEEK_END);
	long long realFileSize = _ftelli64(file);
	_fseeki64(file, 0, SEEK_SET);

	char * buf = (char *)malloc(realFileSize);
	int r = (int)fread(buf, 1, realFileSize, file);
	if (r!=realFileSize)
	{
		fclose(file);
		return 1002;
	}

	fclose(file);

	int i=0;
	while (buf[i]<33  &&  i<realFileSize-1)
		i++;

	switch (buf[i])
	{
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		default: return 1003;
	}
}

//-----------------------------------------------------------------------------------------------------------------------
