#ifndef __mat_download
#define __mat_download

#include <windows.h>

typedef bool (MatDownloaderProgressCallback)(void * obj, float progress);

typedef void (ThreadDoneCallback)(void * obj, bool OK);


class MatDownloader
{
public:
	MatDownloaderProgressCallback * noxProgressCallback;
	ThreadDoneCallback * tDoneCallback;
	void * prObj;
	void * tdObj;
	char * threadURL;
	char * threadFilename;
	bool breakDownloading;
	HANDLE threadHandle;

	void registerDownloadProgressCallback(MatDownloaderProgressCallback * cBack, void * pObject)	{ noxProgressCallback = cBack; prObj = pObject; }
	void registerThreadFinishedCallback(ThreadDoneCallback * td, void * pObject)	{ tDoneCallback = td; tdObj = pObject; }

	bool downloadFile(char * address, char * filename);
	bool downloadFileThread(char * address, char * filename);

	MatDownloader();

};

#endif
