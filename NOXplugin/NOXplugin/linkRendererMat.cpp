#include <windows.h>
#include <math.h>
#include <gdiplus.h>
#include "linkRenderer.h"
#include "noxdebug.h"

#ifdef PORTABLE_COMPILATION
	#include "noxinclude/MatEditor.h"
	#include "noxinclude/Dialogs.h"
#else
	#include "../../Everything DLL/Everything DLL/MatEditor.h"
	#include "../../Everything DLL/Everything DLL/MatEditor2.h"
	#include "../../Everything DLL/Everything DLL/Dialogs.h"
#endif

#include "resource.h"
#include <tchar.h>

HWND hMatDlg = 0;
void * TrMaterial::blendsettings = NULL;

//------------------------------------------------------------------------------------------------

TrMaterial::TrMaterial()
{
	ADD_LOG_CONSTR("TrMaterial constructor");
	address = NULL;
	curLay = NULL;
	llay = -1;
	createSimpleMaterial();
}

TrMaterial::~TrMaterial()
{
	deleteNOXmaterial();
}

//------------------------------------------------------------------------------------------------

bool TrMaterial::createSimpleMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	address = rtr->callNewMaterial();
	MatLayer lay;
	lay.refl0  = Color4(0.75f ,0.75f ,0.75f, 1);
	lay.refl90 = Color4(1,1,1,1);
	((MaterialNox*)address)->addMatLayer(lay);
	return true;
}

//------------------------------------------------------------------------------------------------

void TrMaterial::deleteNOXmaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	if (address)
	{
		MaterialNox * mat = (MaterialNox*)address;
		mat->deleteBuffers(true, false, false);
		mat->deleteBuffers(false, true, false);
		mat->deleteBuffers(false, false, true);
		mat->deleteLayers();
		rtr->callDeleteMaterial(mat);
	}
	address = NULL;
}

//------------------------------------------------------------------------------------------------

void TrMaterial::SetAmbient(float r, float g, float b)
{
}

void TrMaterial::SetDiffuse(float r, float g, float b)
{
}

void TrMaterial::SetSpecular(float r, float g, float b)
{
}

void TrMaterial::SetShininess(float v)
{
}

bool TrMaterial::GetAmbient(float &r, float &g, float &b)
{
	float a;
	return getLayerColor0(0, r,g,b,a);
	r = 0;
	g = 1;
	b = 1;
	return true;
}

bool TrMaterial::GetDiffuse(float &r, float &g, float &b)
{
	float a;
	return getLayerColor0(0, r,g,b,a);
}

bool TrMaterial::GetSpecular(float &r, float &g, float &b)
{
	float a;
	return getLayerColor90(0, r,g,b,a);
}

float TrMaterial::GetXParency()
{
	float whole = 0;
	float transp = 0;
	for (int i=0; i<getLayersNumber(); i++)
	{
		float contr = (float)getLayerContribution(i);
		whole += contr;
		if (getLayerTransmOn(i))
			transp += contr;
	}
	if (whole > 0)
		return min (0.9f , transp / whole);
	return 0.0f;
}


float TrMaterial::GetShininess()
{
	return 1-getLayerRoughness(0);
}

float TrMaterial::GetShinStr()
{
	return 1-getLayerRoughness(0);
}

//------------------------------------------------------------------------------------------------

void notifyBack(bool acceptChanges, void * resAddr)
{	
	// callback called when material editor is closed
	Raytracer * rtr = Raytracer::getInstance();
	TrMaterial * mat = (TrMaterial*)resAddr;

	MaterialNox * oldMat = (MaterialNox*)(mat->address);
	mat->address = rtr->editedMaterial->copy();

	rtr->editedMaterial->deleteBuffers(true, true, false);
	rtr->editedMaterial->deleteLayers();
	delete rtr->editedMaterial;
	rtr->editedMaterial = NULL;
	
	if (oldMat)
	{
		oldMat->deleteBuffers(true, true, false);
		oldMat->deleteLayers();
		delete oldMat;
	}

	if (hMatDlg)
	{
		SendMessage(hMatDlg, WM_COMMAND, MAKEWPARAM(IDC_NOX_MAT_RUN_EDITOR, 6666), (LPARAM)0);
		HWND hRootParent = GetAncestor(hMatDlg, GA_ROOT);
		EnableWindow(hRootParent, true);
		ShowWindow(hMatDlg, SW_SHOW);
		SetFocus(hMatDlg);
	}
	hMatDlg = 0;

}

//------------------------------------------------------------------------------------------------

void TrMaterial::backFromEditor(bool accept)
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr = rtr->scenes[0];

	ADD_LOG_PARTS_MAIN("backFromEditor");

	if (accept)
	{
		MaterialNox * oldMat = (MaterialNox*)(address);
		address = rtr->editedMaterial->copy();

		if (oldMat)
		{
			oldMat->deleteBuffers(true, true, true);
			rtr->editedMaterial->deleteLayers();
			rtr->callDeleteMaterial(oldMat);
			oldMat = NULL;
		}
	}

	if (rtr->editedMaterial)
	{
			rtr->editedMaterial->deleteBuffers(true, true, true);
			rtr->editedMaterial->deleteLayers();
			rtr->callDeleteEditedMaterial();
			rtr->editedMaterial = NULL;
	}

	if (hMatDlg)
	{
		SendMessage(hMatDlg, WM_COMMAND, MAKEWPARAM(IDC_NOX_MAT_RUN_EDITOR, 6666), (LPARAM)0);
		HWND hRootParent = GetAncestor(hMatDlg, GA_ROOT);
		EnableWindow(hRootParent, true);
		ShowWindow(hMatDlg, SW_SHOW);
		SetFocus(hMatDlg);
	}
	hMatDlg = 0;
}

//------------------------------------------------------------------------------------------------

bool TrMaterial::runEditor(HINSTANCE hInst, HWND hParent)
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * oldedmat = rtr->editedMaterial;

	rtr->curScenePtr->texManager.logTextures();

	rtr->editedMaterial = ((MaterialNox*)address)->copy();

	rtr->editedMaterial->updateSceneTexturePointers(rtr->curScenePtr);

	if (oldedmat)
	{
		oldedmat->deleteBuffers(true, true, true);
		oldedmat->deleteLayers();
		rtr->callDeleteMaterial(oldedmat);
		oldedmat = NULL;
	}

	hMatDlg = hParent;
	MatEditorWindow matEdit(hInst);
	if (blendsettings)
		matEdit.copyBlendNamesFrom((BlendSettings*)blendsettings);

	if (!hParent)
		MessageBox(0, _T("no parent"), _T(""), 0);

	bool accept = matEdit.createWindow(hParent, MatEditorWindow::MODE_3DSMAX, NULL);
	return accept;
}

//------------------------------------------------------------------------------------------------

bool TrMaterial::runEditor2(HINSTANCE hInst, HWND hParent)
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * oldedmat = rtr->editedMaterial;

	rtr->curScenePtr->texManager.logTextures();

	rtr->editedMaterial = ((MaterialNox*)address)->copy();

	rtr->editedMaterial->updateSceneTexturePointers(rtr->curScenePtr);

	if (oldedmat)
	{
		oldedmat->deleteBuffers(true, true, true);
		oldedmat->deleteLayers();
		rtr->callDeleteMaterial(oldedmat);
		oldedmat = NULL;
	}

	if (!hParent)
		MessageBox(0, _T("no parent"), _T(""), 0);
	hMatDlg = hParent;

	MatEditor2 * matEdit = MatEditor2::getInstance();
	bool accept = matEdit->createWindow(hInst, hParent, MatEditor2::MODE_3DSMAX, NULL);


	return accept;
}

//------------------------------------------------------------------------------------------------

void TrMaterial::setBlendLayerName(int num, char * newname)
{
	if (blendsettings)
		((BlendSettings*)blendsettings)->setName(num, newname);
}

//------------------------------------------------------------------------------------------------

char * TrMaterial::getBlendLayerName(int num)
{
	if (num<0 || num > 15)
		return NULL;
	if (blendsettings)
		return ((BlendSettings*)blendsettings)->names[num];
	return NULL;
}

//------------------------------------------------------------------------------------------------

void TrMaterial::runBlendLayersDialog(HWND hWnd)
{	
	runBlendNamesDialog(hWnd, (BlendSettings*)blendsettings);

}

//------------------------------------------------------------------------------------------------

void TrMaterial::runNOXTestDialog(HWND hWnd)
{	
}

//------------------------------------------------------------------------------------------------

void TrMaterial::createBlendSettings()
{
	blendsettings = new BlendSettings();
}

//------------------------------------------------------------------------------------------------

int TrMaterial::getLayersNumber()
{
	if (!address)
		return 0;
	return ((MaterialNox*)address)->layers.objCount;
}

char * TrMaterial::getName()
{
	if (!address)
		return 0;
	return ((MaterialNox*)address)->name;
}

bool TrMaterial::getSkyportal()
{
	if (!address)
		return 0;
	return ((MaterialNox*)address)->isSkyPortal;
}

bool TrMaterial::getMatteShadow()
{
	if (!address)
		return 0;
	return ((MaterialNox*)address)->isMatteShadow;
}

int	TrMaterial::getBlendLayerNumber()
{
	if (!address)
		return 0;
	return ((MaterialNox*)address)->blendIndex;
}


int TrMaterial::getLayerType(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 0;
	return ((MaterialNox*)address)->layers[lNum].type;
}

int TrMaterial::getLayerContribution(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 0;
	return ((MaterialNox*)address)->layers[lNum].contribution;
}

float TrMaterial::getLayerRoughness(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 1.0f;
	return ((MaterialNox*)address)->layers[lNum].roughness;
}

bool TrMaterial::getLayerColor0 (int lNum, float &r, float &g, float &b, float &a)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	r = ((MaterialNox*)address)->layers[lNum].refl0.r;
	g = ((MaterialNox*)address)->layers[lNum].refl0.g;
	b = ((MaterialNox*)address)->layers[lNum].refl0.b;
	a = ((MaterialNox*)address)->layers[lNum].refl0.a;
	return true;
}

bool TrMaterial::getLayerColor90(int lNum, float &r, float &g, float &b, float &a)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	r = ((MaterialNox*)address)->layers[lNum].refl90.r;
	g = ((MaterialNox*)address)->layers[lNum].refl90.g;
	b = ((MaterialNox*)address)->layers[lNum].refl90.b;
	a = ((MaterialNox*)address)->layers[lNum].refl90.a;
	return true;
}

bool TrMaterial::getLayerConnect90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return true;
	return ((MaterialNox*)address)->layers[lNum].connect90;
}

float TrMaterial::getLayerAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 1.0f;
	return ((MaterialNox*)address)->layers[lNum].anisotropy;
}

float TrMaterial::getLayerAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 1.0f;
	return ((MaterialNox*)address)->layers[lNum].aniso_angle;
}

int TrMaterial::getLayerBrdfType(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return MatLayer::BRDF_TYPE_GGX;
	return ((MaterialNox*)address)->layers[lNum].brdf_type;
}

bool TrMaterial::getLayerTransmOn(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].transmOn;
}

bool TrMaterial::getLayerTransmColor(int lNum, float &r, float &g, float &b, float &a)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	r = ((MaterialNox*)address)->layers[lNum].transmColor.r;
	g = ((MaterialNox*)address)->layers[lNum].transmColor.g;
	b = ((MaterialNox*)address)->layers[lNum].transmColor.b;
	a = ((MaterialNox*)address)->layers[lNum].transmColor.a;
	return true;
}

bool TrMaterial::getLayerFakeGlass(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].fakeGlass;
}

bool TrMaterial::getLayerAbsOn(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].absOn;
}

bool TrMaterial::getLayerAbsColor(int lNum, float &r, float &g, float &b, float &a)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	r = ((MaterialNox*)address)->layers[lNum].absColor.r;
	g = ((MaterialNox*)address)->layers[lNum].absColor.g;
	b = ((MaterialNox*)address)->layers[lNum].absColor.b;
	a = ((MaterialNox*)address)->layers[lNum].absColor.a;
	return true;
}

float TrMaterial::getLayerAbsDist(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 100.0f;
	return ((MaterialNox*)address)->layers[lNum].absDist;
}

bool TrMaterial::getLayerEmissionColor(int lNum, float &r, float &g, float &b, float &a)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	r = ((MaterialNox*)address)->layers[lNum].color.r;
	g = ((MaterialNox*)address)->layers[lNum].color.g;
	b = ((MaterialNox*)address)->layers[lNum].color.b;
	a = ((MaterialNox*)address)->layers[lNum].color.a;
	return true;
}

float TrMaterial::getLayerEmissionPower(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 0.0f;
	return ((MaterialNox*)address)->layers[lNum].power;
}

int TrMaterial::getLayerEmissionUnit(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 1;
	return ((MaterialNox*)address)->layers[lNum].unit;
}

int TrMaterial::getLayerEmissionTemperature(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 6500;
	return ((MaterialNox*)address)->layers[lNum].emitterTemperature;
}

char* TrMaterial::getLayerEmissionIes(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	if ( !((MaterialNox*)address)->layers[lNum].ies )
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].ies->filename;
}

float TrMaterial::getLayerFresnelIOR(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 1.3f;
	return ((MaterialNox*)address)->layers[lNum].fresnel.IOR;
}

float TrMaterial::getLayerDispersionValue(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 0.0f;
	return ((MaterialNox*)address)->layers[lNum].dispersionValue;
}

bool  TrMaterial::getLayerDispersionOn(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].dispersionOn;
}

bool  TrMaterial::getLayerSSSOn(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].sssON;
}

float TrMaterial::getLayerSSSDensity(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 0.0f;
	return ((MaterialNox*)address)->layers[lNum].sssDens;
}

float TrMaterial::getLayerSSSCollDirection(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 0.0f;
	return ((MaterialNox*)address)->layers[lNum].sssCollDir;
}

float TrMaterial::getOpacity()
{
	if (!address)
		return 1.0f;
	return ((MaterialNox*)address)->opacity;
}

float TrMaterial::getDisplacementDepth()
{
	if (!address)
		return 0.0f;
	return ((MaterialNox*)address)->displ_depth;
}

int   TrMaterial::getDisplacementSubdivs()
{
	if (!address)
		return 1;
	return ((MaterialNox*)address)->displ_subdivs_lvl;
}

int   TrMaterial::getDisplacementNormalMode()
{
	if (!address)
		return 0;
	return ((MaterialNox*)address)->displ_normal_mode;
}

float TrMaterial::getDisplacementShift()
{
	if (!address)
		return 0.0f;
	return ((MaterialNox*)address)->displ_shift;
}

bool  TrMaterial::getDisplacementContinuity()
{
	if (!address)
		return false;
	return ((MaterialNox*)address)->displ_continuity;
}

bool  TrMaterial::getDisplacementIgnoreScale()
{
	if (!address)
		return false;
	return ((MaterialNox*)address)->displ_ignore_scale;
}

bool  TrMaterial::getLayerUseTexRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_col0;
}

char* TrMaterial::getLayerTexRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.fullfilename;
}

bool  TrMaterial::getLayerUseTexRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_col90;
}

char* TrMaterial::getLayerTexRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.fullfilename;
}

bool  TrMaterial::getLayerUseTexTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_transm;
}

char* TrMaterial::getLayerTexTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.fullfilename;
}

bool  TrMaterial::getLayerUseTexAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_aniso;
}

char* TrMaterial::getLayerTexAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.fullfilename;
}

bool  TrMaterial::getLayerUseTexAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_aniso_angle;
}

char* TrMaterial::getLayerTexAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.fullfilename;
}

bool  TrMaterial::getLayerUseTexRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_rough;
}

char* TrMaterial::getLayerTexRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.fullfilename;
}

bool  TrMaterial::getLayerUseTexWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_weight;
}

char* TrMaterial::getLayerTexWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.fullfilename;
}

bool  TrMaterial::getLayerUseTexLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_light;
}

char* TrMaterial::getLayerTexLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_light.fullfilename;
}

bool  TrMaterial::getLayerUseTexNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].use_tex_normal;
}

char* TrMaterial::getLayerTexNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.fullfilename;
}

bool  TrMaterial::getUseTexOpacity()
{
	if (!address)
		return false;
	return ((MaterialNox*)address)->use_tex_opacity;
}

char* TrMaterial::getTexOpacity()
{
	if (!address)
		return NULL;
	return ((MaterialNox*)address)->tex_opacity.fullfilename;
}

char* TrMaterial::getTexDisplacement()
{
	if (!address)
		return NULL;
	return ((MaterialNox*)address)->tex_displacement.fullfilename;
}

bool  TrMaterial::getUseTexDisplacement()
{
	if (!address)
		return false;
	return ((MaterialNox*)address)->use_tex_displacement;
}


bool TrMaterial::getLayerTexPostFilterRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPostFilterAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.interpolateProbe;
}

bool TrMaterial::getTexPostFilterOpacity()
{
	if (!address)
		return NULL;
	return ((MaterialNox*)address)->tex_opacity.texMod.interpolateProbe;
}

bool TrMaterial::getTexPostFilterDisplacement()
{
	if (!address)
		return NULL;
	return ((MaterialNox*)address)->tex_displacement.texMod.interpolateProbe;
}

bool TrMaterial::getLayerTexPrecalculateRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getLayerTexPrecalculateAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return NULL;
	return false;
}

bool TrMaterial::getTexPrecalculateOpacity()
{
	if (!address)
		return NULL;
	return false;
}

float TrMaterial::getLayerTexPostBrightnessRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.brightness;
}

float TrMaterial::getLayerTexPostBrightnessAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.brightness;
}

float TrMaterial::getTexPostBrightnessOpacity()
{
	if (!address)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->tex_opacity.texMod.brightness;
}

float TrMaterial::getTexPostBrightnessDisplacement()
{
	if (!address)
		return TEXMOD_DEF_BRIGHTNESS;
	return ((MaterialNox*)address)->tex_displacement.texMod.brightness;
}

float TrMaterial::getLayerTexPostContrastRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.contrast;
}

float TrMaterial::getLayerTexPostContrastAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.contrast;
}

float TrMaterial::getTexPostContrastOpacity()
{
	if (!address)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->tex_opacity.texMod.contrast;
}

float TrMaterial::getTexPostContrastDisplacement()
{
	if (!address)
		return TEXMOD_DEF_CONTRAST;
	return ((MaterialNox*)address)->tex_displacement.texMod.contrast;
}

float TrMaterial::getLayerTexPostSaturationRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.saturation;
}

float TrMaterial::getLayerTexPostSaturationAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.saturation;
}

float TrMaterial::getTexPostSaturationOpacity()
{
	if (!address)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->tex_opacity.texMod.saturation;
}

float TrMaterial::getTexPostSaturationDisplacement()
{
	if (!address)
		return TEXMOD_DEF_SATURATION;
	return ((MaterialNox*)address)->tex_displacement.texMod.saturation;
}

float TrMaterial::getLayerTexPostHueRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.hue;
}

float TrMaterial::getLayerTexPostHueRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.hue;
}

float TrMaterial::getLayerTexPostHueRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.hue;
}

float TrMaterial::getLayerTexPostHueWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.hue;
}

float TrMaterial::getLayerTexPostHueLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.hue;
}

float TrMaterial::getLayerTexPostHueTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.hue;
}

float TrMaterial::getLayerTexPostHueNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.hue;
}

float TrMaterial::getLayerTexPostHueAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.hue;
}

float TrMaterial::getLayerTexPostHueAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.hue;
}

float TrMaterial::getTexPostHueOpacity()
{
	if (!address)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->tex_opacity.texMod.hue;
}

float TrMaterial::getTexPostHueDisplacement()
{
	if (!address)
		return TEXMOD_DEF_HUE;
	return ((MaterialNox*)address)->tex_displacement.texMod.hue;
}

float TrMaterial::getLayerTexPostRedRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.red;
}

float TrMaterial::getLayerTexPostRedRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.red;
}

float TrMaterial::getLayerTexPostRedRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.red;
}

float TrMaterial::getLayerTexPostRedWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.red;
}

float TrMaterial::getLayerTexPostRedLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.red;
}

float TrMaterial::getLayerTexPostRedTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.red;
}

float TrMaterial::getLayerTexPostRedNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.red;
}

float TrMaterial::getLayerTexPostRedAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.red;
}

float TrMaterial::getLayerTexPostRedAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.red;
}

float TrMaterial::getTexPostRedOpacity()
{
	if (!address)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->tex_opacity.texMod.red;
}

float TrMaterial::getTexPostRedDisplacement()
{
	if (!address)
		return TEXMOD_DEF_RED;
	return ((MaterialNox*)address)->tex_displacement.texMod.red;
}

float TrMaterial::getLayerTexPostGreenRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.green;
}

float TrMaterial::getLayerTexPostGreenRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.green;
}

float TrMaterial::getLayerTexPostGreenRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.green;
}

float TrMaterial::getLayerTexPostGreenWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.green;
}

float TrMaterial::getLayerTexPostGreenLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.green;
}

float TrMaterial::getLayerTexPostGreenTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.green;
}

float TrMaterial::getLayerTexPostGreenNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.green;
}

float TrMaterial::getLayerTexPostGreenAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.green;
}

float TrMaterial::getLayerTexPostGreenAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.green;
}

float TrMaterial::getTexPostGreenOpacity()
{
	if (!address)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->tex_opacity.texMod.green;
}

float TrMaterial::getTexPostGreenDisplacement()
{
	if (!address)
		return TEXMOD_DEF_GREEN;
	return ((MaterialNox*)address)->tex_displacement.texMod.green;
}

float TrMaterial::getLayerTexPostBlueRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.blue;
}

float TrMaterial::getLayerTexPostBlueAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.blue;
}

float TrMaterial::getTexPostBlueOpacity()
{
	if (!address)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->tex_opacity.texMod.blue;
}

float TrMaterial::getTexPostBlueDisplacement()
{
	if (!address)
		return TEXMOD_DEF_BLUE;
	return ((MaterialNox*)address)->tex_displacement.texMod.blue;
}

float TrMaterial::getLayerTexPostGammaRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.gamma;
}

float TrMaterial::getLayerTexPostGammaAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.gamma;
}

float TrMaterial::getTexPostGammaOpacity()
{
	if (!address)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->tex_opacity.texMod.gamma;
}

float TrMaterial::getTexPostGammaDisplacement()
{
	if (!address)
		return TEXMOD_DEF_GAMMA;
	return ((MaterialNox*)address)->tex_displacement.texMod.gamma;
}

bool TrMaterial::getLayerTexPostInvertRefl0(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_col0.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertRefl90(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_col90.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertRough(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_rough.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertWeight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_weight.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertLight(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_light.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertTransm(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_transm.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertNormal(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertAniso(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso.texMod.invert;
}

bool TrMaterial::getLayerTexPostInvertAnisoAngle(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->layers[lNum].tex_aniso_angle.texMod.invert;
}

bool TrMaterial::getTexPostInvertOpacity()
{
	if (!address)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->tex_opacity.texMod.invert;
}

bool TrMaterial::getTexPostInvertDisplacement()
{
	if (!address)
		return TEXMOD_DEF_INVERT;
	return ((MaterialNox*)address)->tex_displacement.texMod.invert;
}

float TrMaterial::getLayerTexPostNormalPower(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return 0.0f;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.normMod.powerEV;
}

bool TrMaterial::getLayerTexPostNormalInvertX(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.normMod.invertX;
}

bool TrMaterial::getLayerTexPostNormalInvertY(int lNum)
{
	if (!address   ||   lNum >= ((MaterialNox*)address)->layers.objCount)
		return false;
	return ((MaterialNox*)address)->layers[lNum].tex_normal.normMod.invertY;
}


//------------------------------------------------------------------------------------------------

bool TrMaterial::addLayer()
{
	if (llay >= 15   ||   !address)
		return false;
	llay++;

	while (llay >= ((MaterialNox*)address)->layers.objCount)
	{
		MatLayer newlay;
		((MaterialNox*)address)->addMatLayer(newlay);
	}
	curLay = (void*)&(((MaterialNox*)address)->layers[llay]);
	return true;
}

bool TrMaterial::setCurLayer(int nID)
{
	if (!address)
		return false;
	if (nID<0 || nID>=((MaterialNox*)address)->layers.objCount)
		return false;

	llay = nID;
	curLay = (void*)&(((MaterialNox*)address)->layers[llay]);

	return true;
}

bool TrMaterial::setName(char * newname)
{
	if (!address)
		return false;

	if (newname   &&   strlen(newname)>0)
		((MaterialNox*)address)->setName(newname);
	
	return true;
}

bool TrMaterial::setSkyportal(bool b)
{
	if (!address)
		return false;
	((MaterialNox*)address)->isSkyPortal = b;
	return true;
}

bool TrMaterial::setMatteShadow(bool b)
{
	if (!address)
		return false;
	((MaterialNox*)address)->isMatteShadow = b;
	return true;
}

bool TrMaterial::setBlendLayerNumber(int num)
{
	if (!address)
		return false;
	((MaterialNox*)address)->blendIndex = num;
	return true;
}

bool TrMaterial::setLayerType(int newtype)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->type = newtype;
	return true;
}

bool TrMaterial::setLayerContribution(int newcontr)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->contribution = newcontr;
	return true;
}

bool TrMaterial::setLayerRoughness(float newrough)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->roughness = newrough;
	return true;
}

bool TrMaterial::setLayerColor0 (float r, float g, float b, float a)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->refl0.r = r;
	((MatLayer*)curLay)->refl0.g = g;
	((MatLayer*)curLay)->refl0.b = b;
	((MatLayer*)curLay)->refl0.a = a;
	return true;
}

bool TrMaterial::setLayerColor90(float r, float g, float b, float a)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->refl90.r = r;
	((MatLayer*)curLay)->refl90.g = g;
	((MatLayer*)curLay)->refl90.b = b;
	((MatLayer*)curLay)->refl90.a = a;
	return true;
}

bool TrMaterial::setLayerConnect90(int newconn)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->connect90 = (newconn != 0);
	return true;
}

bool TrMaterial::setLayerAniso(float newaniso)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->anisotropy = newaniso;
	return true;
}

bool TrMaterial::setLayerAnisoAngle(float newanisoangle)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->aniso_angle = newanisoangle;
	return true;
}

bool TrMaterial::setLayerBrdfType(int newbrdf)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->brdf_type = newbrdf;
	return true;
}

bool TrMaterial::setLayerFakeGlass(int newfakeglass)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->fakeGlass = (newfakeglass != 0);
	return true;
}

bool TrMaterial::setLayerTransmOn(int newtransmon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->transmOn = (newtransmon != 0);
	return true;
}

bool TrMaterial::setLayerTransmColor(float r, float g, float b, float a)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->transmColor.r = r;
	((MatLayer*)curLay)->transmColor.g = g;
	((MatLayer*)curLay)->transmColor.b = b;
	((MatLayer*)curLay)->transmColor.a = a;
	return true;
}

bool TrMaterial::setLayerAbsOn(int newabson)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->absOn = (newabson != 0);
	return true;
}

bool TrMaterial::setLayerAbsColor(float r, float g, float b, float a)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->absColor.r = r;
	((MatLayer*)curLay)->absColor.g = g;
	((MatLayer*)curLay)->absColor.b = b;
	((MatLayer*)curLay)->absColor.a = a;
	return true;
}

bool TrMaterial::setLayerAbsDist(float newdist)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->absDist = newdist;
	return true;
}

bool TrMaterial::setLayerEmissionColor(float r, float g, float b, float a)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->color.r = r;
	((MatLayer*)curLay)->color.g = g;
	((MatLayer*)curLay)->color.b = b;
	((MatLayer*)curLay)->color.a = a;
	return true;
}

bool TrMaterial::setLayerEmissionPower(float newpower)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->power = newpower;
	return true;
}

bool TrMaterial::setLayerEmissionUnit(int newunit)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->unit = newunit;
	return true;
}

bool TrMaterial::setLayerEmissionTemperature(int newtemp)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->emitterTemperature = newtemp;
	return true;
}

bool TrMaterial::setLayerEmissionIes(char * fname)
{
	if (!curLay)
		return false;
	if (fname   &&   strlen(fname)>0)
	{
		if (!((MatLayer*)curLay)->addIes())
			return false;
		if ( !((MatLayer*)curLay)->ies->parseFile(fname) )
			return false;
	}
	else
	{
		((MatLayer*)curLay)->removeIes();
	}
	return true;
}

bool TrMaterial::setLayerFresnelIOR(float newIOR)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->fresnel.IOR = newIOR;
	return true;
}

bool TrMaterial::setLayerDispersionValue(float newDisp)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->dispersionValue = newDisp;
	return true;
}

bool TrMaterial::setLayerDispersionOn(int newdispOn)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->dispersionOn = (newdispOn != 0);
	return true;
}

bool TrMaterial::setLayerSSSOn(int newSSSOn)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->sssON = (newSSSOn != 0);
	return true;
}

bool TrMaterial::setLayerSSSDensity(float newSSSDens)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->sssDens = newSSSDens;
	return true;
}

bool TrMaterial::setLayerSSSCollDirection(float newSSSCDir)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->sssCollDir = newSSSCDir;
	return true;
}

bool TrMaterial::setOpacity(float newopacity)
{
	if (!address)
		return false;
	((MaterialNox*)address)->opacity = newopacity;
	return true;
}
bool TrMaterial::setDisplacementDepth(float newdepth)
{
	if (!address)
		return false;
	((MaterialNox*)address)->displ_depth = newdepth;
	return true;
}

bool TrMaterial::setDisplacementSubdivs(int newsubdivs)
{
	if (!address)
		return false;
	((MaterialNox*)address)->displ_subdivs_lvl = newsubdivs;
	return true;
}

bool TrMaterial::setDisplacementNormalMode(int newnormalmode)
{
	if (!address)
		return false;
	((MaterialNox*)address)->displ_normal_mode = newnormalmode;
	return true;
}

bool TrMaterial::setDisplacementShift(float newshift)
{
	if (!address)
		return false;
	((MaterialNox*)address)->displ_shift = newshift;
	return true;
}

bool TrMaterial::setDisplacementContinuity(bool on)
{
	if (!address)
		return false;
	((MaterialNox*)address)->displ_continuity = on;
	return true;
}

bool TrMaterial::setDisplacementIgnoreScale(bool on)
{
	if (!address)
		return false;
	((MaterialNox*)address)->displ_ignore_scale = on;
	return true;
}

bool TrMaterial::setLayerUseTexRefl0(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_col0 = b;
	return true;
}

bool TrMaterial::setLayerTexRefl0(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_col0);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_col0.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexRefl90(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_col90 = b;
	return true;
}

bool TrMaterial::setLayerTexRefl90(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_col90);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_col90.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexTransm(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_transm = b;
	return true;
}

bool TrMaterial::setLayerTexTransm(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_transm);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_transm.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexAniso(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_aniso = b;
	return true;
}

bool TrMaterial::setLayerTexAniso(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_aniso);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_aniso.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexAnisoAngle(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_aniso_angle = b;
	return true;
}

bool TrMaterial::setLayerTexAnisoAngle(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_aniso_angle);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_aniso_angle.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexRough(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_rough = b;
	return true;
}

bool TrMaterial::setLayerTexRough(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_rough);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_rough.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexWeight(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_weight= b;
	return true;
}

bool TrMaterial::setLayerTexWeight(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_weight);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_weight.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexLight(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_light = b;
	return true;
}

bool TrMaterial::setLayerTexLight(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_light);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_light.filename = NULL;
	return true;
}

bool TrMaterial::setLayerUseTexNormal(bool b)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->use_tex_normal = b;
	return true;
}

bool TrMaterial::setLayerTexNormal(char * newname)
{
	if (!curLay)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MatLayer*)curLay)->tex_normal);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, true);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MatLayer*)curLay)->tex_normal.filename = NULL;
	return true;
}

bool TrMaterial::setUseTexOpacity(bool b)
{
	if (!address)
		return false;
	((MaterialNox*)address)->use_tex_opacity = b;
	return true;
}

bool TrMaterial::setTexOpacity(char * newname)
{
	if (!address)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MaterialNox*)address)->tex_opacity);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MaterialNox*)address)->tex_opacity.filename = NULL;
	return true;
}

bool TrMaterial::setUseTexDisplacement(bool b)
{
	if (!address)
		return false;
	((MaterialNox*)address)->use_tex_displacement = b;
	return true;
}

bool TrMaterial::setTexDisplacement(char * newname)
{
	if (!address)
		return false;
	if (newname  &&   strlen(newname)>0)
	{
		Raytracer * rtr = Raytracer::getInstance();
		TextureInstance * tex_ptr = &(((MaterialNox*)address)->tex_displacement);
		int id = rtr->curScenePtr->texManager.addTexture(newname, false, NULL, false);
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}
	else
		((MaterialNox*)address)->tex_opacity.filename = NULL;
	return true;
}

bool TrMaterial::setLayerTexFilterRefl0(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexFilterRefl90(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexFilterRough(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexFilterWeight(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexFilterLight(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexFilterTransm(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexFilterAniso(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexFilterAnisoAngle(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setTexFilterOpacity(bool newON)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setTexFilterDisplacement(bool newON)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.interpolateProbe = newON;
	return true;
}

bool TrMaterial::setLayerTexPrecalculateRefl0(bool newON)
{
	if (!curLay)
		return false;
	return true;
}

bool TrMaterial::setLayerTexPrecalculateRefl90(bool newON)
{
	if (!curLay)
		return false;
	return true;
}

bool TrMaterial::setLayerTexPrecalculateRough(bool newON)
{
	if (!curLay)
		return false;
	return true;
}

bool TrMaterial::setLayerTexPrecalculateWeight(bool newON)
{
	if (!curLay)
		return false;
	return true;
}

bool TrMaterial::setLayerTexPrecalculateLight(bool newON)
{
	if (!curLay)
		return false;
	return true;
}

bool TrMaterial::setLayerTexPrecalculateTransm(bool newON)
{
	if (!curLay)
		return false;
	return true;
}

bool TrMaterial::setTexPrecalculateOpacity(bool newON)
{
	if (!address)
		return false;
	return true;
}

bool TrMaterial::setTexPrecalculateDisplacement(bool newON)
{
	if (!address)
		return false;
	return true;
}



bool TrMaterial::setLayerTexPostBrightnessRefl0(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessRefl90(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessRough(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessWeight(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessLight(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessTransm(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessNormal(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessAniso(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostBrightnessAnisoAngle(float newBri)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setTexPostBrightnessOpacity(float newBri)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setTexPostBrightnessDisplacement(float newBri)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.brightness = min(TEXMOD_MAX_BRIGHTNESS, max(TEXMOD_MIN_BRIGHTNESS, newBri));
	return true;
}

bool TrMaterial::setLayerTexPostContrastRefl0(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastRefl90(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastRough(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastWeight(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastLight(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastTransm(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastNormal(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastAniso(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostContrastAnisoAngle(float newCon)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setTexPostContrastOpacity(float newCon)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setTexPostContrastDisplacement(float newCon)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.contrast = min(TEXMOD_MAX_CONTRAST, max(TEXMOD_MIN_CONTRAST, newCon));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationRefl0(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationRefl90(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationRough(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationWeight(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationLight(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationTransm(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationNormal(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationAniso(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostSaturationAnisoAngle(float newSat)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setTexPostSaturationOpacity(float newSat)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setTexPostSaturationDisplacement(float newSat)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.saturation = min(TEXMOD_MAX_SATURATION, max(TEXMOD_MIN_SATURATION, newSat));
	return true;
}

bool TrMaterial::setLayerTexPostHueRefl0(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueRefl90(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueRough(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueWeight(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueLight(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueTransm(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueNormal(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueAniso(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostHueAnisoAngle(float newHue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setTexPostHueOpacity(float newHue)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setTexPostHueDisplacement(float newHue)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.hue = min(TEXMOD_MAX_HUE, max(TEXMOD_MIN_HUE, newHue));
	return true;
}

bool TrMaterial::setLayerTexPostRedRefl0(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedRefl90(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedRough(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedWeight(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedLight(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedTransm(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedNormal(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedAniso(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostRedAnisoAngle(float newRed)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setTexPostRedOpacity(float newRed)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setTexPostRedDisplacement(float newRed)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.red = min(TEXMOD_MAX_RED, max(TEXMOD_MIN_RED, newRed));
	return true;
}

bool TrMaterial::setLayerTexPostGreenRefl0(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenRefl90(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenRough(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenWeight(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenLight(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenTransm(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenNormal(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenAniso(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostGreenAnisoAngle(float newGreen)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setTexPostGreenOpacity(float newGreen)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setTexPostGreenDisplacement(float newGreen)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.green = min(TEXMOD_MAX_GREEN, max(TEXMOD_MIN_GREEN, newGreen));
	return true;
}

bool TrMaterial::setLayerTexPostBlueRefl0(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueRefl90(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueRough(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueWeight(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueLight(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueTransm(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueNormal(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueAniso(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostBlueAnisoAngle(float newBlue)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setTexPostBlueOpacity(float newBlue)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setTexPostBlueDisplacement(float newBlue)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.blue = min(TEXMOD_MAX_BLUE, max(TEXMOD_MIN_BLUE, newBlue));
	return true;
}

bool TrMaterial::setLayerTexPostGammaRefl0(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaRefl90(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaRough(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaWeight(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaLight(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaTransm(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaNormal(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaAniso(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostGammaAnisoAngle(float newGamma)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setTexPostGammaOpacity(float newGamma)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setTexPostGammaDisplacement(float newGamma)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.gamma = min(TEXMOD_MAX_GAMMA, max(TEXMOD_MIN_GAMMA, newGamma));
	return true;
}

bool TrMaterial::setLayerTexPostInvertRefl0(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col0.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertRefl90(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_col90.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertRough(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_rough.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertWeight(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_weight.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertLight(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_light.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertTransm(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_transm.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertNormal(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertAniso(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso.texMod.invert = newON;
	return true;
}

bool TrMaterial::setLayerTexPostInvertAnisoAngle(bool newON)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_aniso_angle.texMod.invert = newON;
	return true;
}

bool TrMaterial::setTexPostInvertOpacity(bool newON)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_opacity.texMod.invert = newON;
	return true;
}

bool TrMaterial::setTexPostInvertDisplacement(bool newON)
{
	if (!address)
		return false;
	((MaterialNox*)address)->tex_displacement.texMod.invert = newON;
	return true;
}


bool TrMaterial::setLayerTexPostNormalPower(float newpower)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.normMod.powerEV = newpower;
	((MatLayer*)curLay)->tex_normal.normMod.power = pow(2.0f, newpower);
	return true;
}

bool TrMaterial::setLayerTexPostNormalInvertX(bool newinv)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.normMod.invertX = newinv;
	return true;
}

bool TrMaterial::setLayerTexPostNormalInvertY(bool newinv)
{
	if (!curLay)
		return false;
	((MatLayer*)curLay)->tex_normal.normMod.invertY = newinv;
	return true;
}


//----------------------------------------------------------------------------------------------------------

float * TrMaterial::createPreview(int w, int h)
{
	if (w<1 || w>4096  ||  h<1 || h>4096)
		return NULL;
	if (!address)
		return NULL;
	
	MaterialNox * mat = (MaterialNox *)address;
	if (!mat->preview)
		return NULL;
	if (!mat->preview->imgBuf)
		return NULL;

	float * img = (float*)malloc(w*h*4*sizeof(float));
	if (!img)
		return NULL;

	// gdi from here
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	Gdiplus::Bitmap * bmsrc = new Gdiplus::Bitmap(mat->preview->width, mat->preview->height, PixelFormat32bppARGB);
	if (!bmsrc)
	{
		free(img);
		Gdiplus::GdiplusShutdown(gdiplusToken);
		return NULL;
	}

	Gdiplus::Bitmap * bmdst= new Gdiplus::Bitmap(w, h, PixelFormat32bppARGB);
	if (!bmdst)
	{
		delete bmsrc;
		free(img);
		Gdiplus::GdiplusShutdown(gdiplusToken);
		return NULL;
	}
	
	Gdiplus::Color color;
	COLORREF rgb;
	for (int y=0; y<mat->preview->height; y++)
		for (int x=0; x<mat->preview->width; x++)
		{
			rgb = mat->preview->imgBuf[y][x];
			color.SetFromCOLORREF(rgb);
			bmsrc->SetPixel(x,y,color);
		}

	Gdiplus::Graphics * g = new Gdiplus::Graphics(bmdst);
	g->DrawImage(bmsrc, (float)0,(float)0,(float)w,(float)h);

	float m = 1.0f/255.0f;
	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			bmdst->GetPixel(x,y, &color);
			img[((y*w+x)*4+0)] = color.GetR()*m;
			img[((y*w+x)*4+1)] = color.GetG()*m;
			img[((y*w+x)*4+2)] = color.GetB()*m;
			img[((y*w+x)*4+3)] = 1.0f;
		}

	delete g;
	delete bmdst;
	delete bmsrc;
	Gdiplus::GdiplusShutdown(gdiplusToken);

	return img;
}

void * TrMaterial::cloneAddress()
{
	return (void *)((MaterialNox *)address)->copy();
}

bool  TrMaterial::getPreviewExists()
{
	MaterialNox * mat = (MaterialNox *)address;
	if (!mat)
		return false;
	if (!mat->preview)
		return false;
	if (!mat->preview->imgBuf)
		return false;
	if (mat->preview->width < 1   ||   mat->preview->height < 1)
		return false;
	return true;
}

int   TrMaterial::getPreviewWidth()
{
	MaterialNox * mat = (MaterialNox *)address;
	if (!mat)
		return 0;
	if (!mat->preview)
		return 0;
	return mat->preview->width;
}

int   TrMaterial::getPreviewHeight()
{
	MaterialNox * mat = (MaterialNox *)address;
	if (!mat)
		return 0;
	if (!mat->preview)
		return 0;
	return mat->preview->height;
}

void* TrMaterial::getPreviewBuffer()
{
	MaterialNox * mat = (MaterialNox *)address;
	if (!mat)
		return NULL;
	if (!mat->preview)
		return NULL;

	unsigned char * buf = (unsigned char *)malloc(sizeof(COLORREF) * mat->preview->width * mat->preview->height);
	if (!buf)
		return NULL;

	int ls = sizeof(COLORREF)*mat->preview->width;
	for (int i=0; i<mat->preview->height; i++)
	{
		void * src = mat->preview->imgBuf[i];
		void * dst = buf+(i*ls);
		memcpy(dst,src, ls);
	}

	return buf;
}

int   TrMaterial::getPreviewBufSize()
{
	MaterialNox * mat = (MaterialNox *)address;
	if (!mat)
		return 0;
	if (!mat->preview)
		return 0;
	if (!mat->preview->imgBuf)
		return 0;
	return mat->preview->width * mat->preview->height * sizeof(COLORREF);
}

bool TrMaterial::setNewPreview(int w1, int h1, unsigned char * buf)
{
	MaterialNox * mat = (MaterialNox *)address;
	if (!mat)
		return false;
	if (!mat->preview)
		mat->createEmptyPreview();
	if (!mat->preview->allocBuffer(w1,h1))
		return false;

	int ls = sizeof(COLORREF)*mat->preview->width;
	for (int i=0; i<mat->preview->height; i++)
	{
		void * dst = mat->preview->imgBuf[i];
		void * src = &(buf[i*ls]);
		memcpy(dst,src, ls);
	}

	return true;
}

bool TrMaterial::isTexLoaded(int which)
{
	switch (which)
	{
		case NOX_TEX_ROUGHNESS:
			break;
		case NOX_TEX_REFL0:
			break;
		case NOX_TEX_REFL90:
			break;
		case NOX_TEX_WEIGHT:
			break;
	}
	return true;
}

