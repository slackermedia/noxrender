#include "max2013predef.h"
#include "NOXSunLight.h"
#include <decomp.h> 


//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	if (!hParams) 
	{
		hParams = ip->AddRollupPage(
					hInstance, 
					MAKEINTRESOURCE(IDD_NOX_SUNSKY_TARGET),
					NOXSunLightTargetDlgProc, 
					GetString(IDS_NOX_SUN_TARGET_DLG_TITLE), 
					(LPARAM)this);    
		ip->RegisterDlgWnd(hParams);
	}
	else 
	{
		DLSetWindowLongPtr(hParams, this);
	}
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	if (flags&END_EDIT_REMOVEUI) 
	{
		ip->UnRegisterDlgWnd(hParams);
		ip->DeleteRollupPage(hParams);
		hParams = NULL;
	} 
	else 
	{
		DLSetWindowLongPtr(hParams, NULL);
	}
	ip = NULL;
}

//------------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK NOXSunLightTargetDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	NOXSunTarget * nst = DLGetWindowLongPtr<NOXSunTarget *>(hWnd);
	if (!nst && message!=WM_INITDIALOG) return FALSE;

	switch (message) 
	{
		case WM_INITDIALOG: 
			{
				nst = (NOXSunTarget*)lParam;
				DLSetWindowLongPtr(hWnd, lParam);
				nst->hParams = hWnd;
			}
			break;
		case WM_COMMAND:
			{
				switch(LOWORD(wParam)) 
				{
					case IDC_NOX_SUNSKY_TARGET_RESET_ROTATION:
					{
						if (HIWORD(wParam) == BN_CLICKED)
							nst->resetRotation();
					}
					break;
					case IDC_NOX_SUNSKY_TARGET_SELECT_SUN:
					{
						if (HIWORD(wParam) == BN_CLICKED)
							nst->selectSun();
					}
					break;
				}
			}
			break;
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MOUSEMOVE:
			{
				return FALSE;
			}
			break;
		default:
			return FALSE;
	}
	return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------
