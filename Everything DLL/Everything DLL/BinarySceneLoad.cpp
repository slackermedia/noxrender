#include "BinaryScene.h"
#include "raytracer.h"
#include "log.h"
#include <math.h>
#include "valuesMinMax.h"

#define FREAD_ULONG8(a)			fread((&a), 8, 1, file)
#define FREAD_UINT(a)			fread((&a), 4, 1, file)
#define FREAD_FLOAT(a)			fread((&a), 4, 1, file)
#define FREAD_USHORT(a) 		fread((&a), 2, 1, file)
#define FREAD_UCHAR(a)			fread((&a), 1, 1, file)
#define FREAD_STRING(a, size)	fread((a), (size), 1, file)

#define LOG_NOX_PARSING

//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------

BinaryScene::BinaryScene()
{
	file = NULL;
	cTri = 0;
	triCounter = 0;
	curMeshNum = 0;	
	directory = NULL;
	showMessages = true;
	addCamBuffs = true;
	singleTriSize = 0;
}

BinaryScene::~BinaryScene()
{
	if (file)
		fclose(file);
	file = NULL;
	directory = NULL;
}

//----------------------------------------------------------------------------------------

char * BinaryScene::getValidTexPath(char * fname)
{
	if (!fname)
		return NULL;

	if (strlen(fname) < 2)
		return copyString(fname);

	if (fname[1] == ':')
		return copyString(fname);

	if (!directory)
		return copyString(fname);

	int s = (int)strlen(directory) + (int)strlen(fname) + 16 ;
	char * res = (char *)malloc(s);
	sprintf_s(res, s, "%s\\%s", directory, fname);

	return res;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::checkHeader(char * filename)
{
	FILE * tfile = NULL;
	if (fopen_s(&tfile, filename, "rb"))
		return false;

	bool isBinary = false;
	unsigned int header;
	if (1 == fread(&header, 4, 1, file))
	{
		if (header == NOX_BIN_HEADER)
			isBinary = true;
	}

	fclose(file);
	return isBinary;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::LoadScene(char * filename, bool merge)
{
	file = NULL;
	if (fopen_s(&file, filename, "rb"))
	{
		MessageBox(0, "Error. Can't open file.", "Error", 0);
		return false;
	}

	directory = getDirectory(filename);

	#ifdef LOG_NOX_PARSING
		Logger::add("------------------------------------------------------------------------");
		Logger::add("Loading file:");
		Logger::add(filename);
	#endif


	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->sscene.activeCamera = 0;
	cTri = 0;
	triCounter = 0;
	curMeshNum = 0;	

	rtr->curScenePtr->notifyProgress("Loading scene...", 0.0f);
	rtr->texDirs.add(copyString(directory));
	rtr->texDirs.createArray();



	unsigned long long fileSize;
	unsigned long long realFileSize;
	char errorInfo[256];

	_fseeki64(file, 0, SEEK_END);
	realFileSize = _ftelli64(file);
	_fseeki64(file, 0, SEEK_SET);
	if (realFileSize < 40)
	{
		MessageBox(0, "Error. File is too small. Can't be a NOX scene.", "Error", 0);
		return false;
	}

	unsigned int header;
	if (1 != FREAD_UINT(header))
	{
		MessageBox(0, "Error reading file header", "Error", 0);
		return false;
	}

	if (header != NOX_BIN_HEADER)
	{
		MessageBox(0, "Error. File header is not NOX.", "Error", 0);
		return false;
	}

	if (1 != FREAD_ULONG8(fileSize))
	{
		MessageBox(0, "Error reading file size.", "Error", 0);
		return false;
	}

	if (realFileSize != fileSize)
	{
		MessageBox(0, "Error. Real file size and written file size are different.", "Error", 0);
		return false;
	}

	unsigned int fileversion;
	if (1 != FREAD_UINT(fileversion))
	{
		MessageBox(0, "Error reading file version.", "Error", 0);
		return false;
	}

	#ifdef LOG_NOX_PARSING
		unsigned char vZero  = ((char*)(&fileversion))[0];
		unsigned char vMajor = ((char*)(&fileversion))[1];
		unsigned char vMinor = ((char*)(&fileversion))[2];
		unsigned char vBuild = ((char*)(&fileversion))[3];
		if (vMajor != NOX_VER_MAJOR   ||   vMinor != NOX_VER_MINOR  ||  vBuild != NOX_VER_BUILD)
		{
			int a = NOX_VER_MAJOR;
			int b = NOX_VER_MINOR;
			int c = NOX_VER_BUILD;
			char wText[256];
			sprintf_s(wText, 256, "Warning. Engine version is %d.%d.%d while scene file version is %d.%d.%d", a,b,c, vMajor, vMinor, vBuild);
			Logger::add(wText);
		}
	#endif


	unsigned short nextChunk;
	unsigned long long tPos = _ftelli64(file);
	if (1 != FREAD_USHORT(nextChunk))
	{
		sprintf_s(errorInfo, 256, "Error reading first chunk ID at %lld byte.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	if (nextChunk != NOX_BIN_SCENE)
	{
		sprintf_s(errorInfo, 256, "Error. First chunk ID at %lld is supposed to be scene chunk instead of %x.", tPos, nextChunk);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	if (!parseSceneChunk(merge))
	{
		return false;
	}

	if (rtr->curScenePtr->cameras.objCount < 1)
	{
		MessageBox(0, "No camera found in scene.", "Error", MB_ICONERROR);
		return false;
	}

	#ifdef LOG_NOX_PARSING
		Logger::add("File loaded and parsed successfully!");
		Logger::add("------------------------------------------------------------------------");
	#endif

	fclose(file);
	file = NULL;
	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseSceneChunk(bool merge)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	char errorInfo[256];
	unsigned long long tPos;

	unsigned long long sceneSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(sceneSize))
	{
		sprintf_s(errorInfo, 256, "Error reading scene size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	mrgCamID = 0;

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + sceneSize - 10;

	#ifdef LOG_NOX_PARSING
		Logger::add("Parsing scene");
	#endif

	while (currentPos < borderPos)
	{
		unsigned short nextChunk;
		tPos = _ftelli64(file);
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error reading chunk ID at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		if (merge)
		{
			if (nextChunk == NOX_BIN_CAMERA)
			{
				if (!parseCameraChunk(true))
				{
					return false;
				}
				mrgCamID++;
			}
			else
			{
				ignoreChunk();
			}
		}
		else
		{
			switch (nextChunk)
			{
				case NOX_BIN_GEOMETRY:
					{
						if (!parseGeometryChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_CAMERA:
					{
						if (!parseCameraChunk(false))
						{
							return false;
						}
					}
					break;
				case NOX_BIN_SUNSKY:
					{
						if (!parseSunskyChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_BLEND_LAYERS:
					{
						if (!parseBlendsChunk(&Raytracer::getInstance()->curScenePtr->blendSettings))
						{
							return false;
						}
					}
					break;
				case NOX_BIN_USERCOLORS:
					{
						if (!parseUsercolorsChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_RENDERER_SETTINGS:
					{
						if (!parseRendererSettingsChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_ENVIRONMENT_MAP:
					{
						if (!parseEnvironmentMapChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_SCENE_INFO:
					{
						if (!parseSceneInfoChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_SCENE_SETTINGS:
					{
						if (!parseSceneSettingsChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_MATERIALS:
					{
						if (!parseMaterialsChunk())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_USER_PRESETS:
					{
						if (!parseUserPresets())
						{
							return false;
						}
					}
					break;
				case NOX_BIN_PRELOAD_POST:
					{
						unsigned int nameSize = 0;
						tPos = _ftelli64(file);
						if (1!=FREAD_UINT(nameSize))
						{
							sprintf_s(errorInfo, 256, "Error reading post file name size at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						nameSize -= 6;
						
						char * pName = (char *)malloc(nameSize+1);
						if (!pName)
						{
							sprintf_s(errorInfo, 256, "Error allocating %d bytes for post file name. Size taken from %lld.", (nameSize+1), tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}

						tPos = _ftelli64(file);
						if (1!=FREAD_STRING(pName, nameSize))
						{
							sprintf_s(errorInfo, 256, "Error reading post file name at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							free(pName);
							return false;
						}

						pName[nameSize] = 0;
						#ifdef LOG_NOX_PARSING
							char logtext[512];
							sprintf_s(logtext, 512, "Should load post file: %s", pName);
							Logger::add(logtext);
						#endif

						Raytracer * rtr = Raytracer::getInstance();
						Scene * sc = rtr->curScenePtr;
						sc->postLoadPostFile = pName;
					}
					break;
				case NOX_BIN_PRELOAD_BLEND:
					{
						unsigned int nameSize = 0;
						tPos = _ftelli64(file);
						if (1!=FREAD_UINT(nameSize))
						{
							sprintf_s(errorInfo, 256, "Error reading blend file name size at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						nameSize -= 6;
						
						char * pName = (char *)malloc(nameSize+1);
						if (!pName)
						{
							sprintf_s(errorInfo, 256, "Error allocating %d bytes for blend file name. Size taken from %lld.", (nameSize+1), tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}

						tPos = _ftelli64(file);
						if (1!=FREAD_STRING(pName, nameSize))
						{
							sprintf_s(errorInfo, 256, "Error reading blend file name at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							free(pName);
							return false;
						}

						pName[nameSize] = 0;
						#ifdef LOG_NOX_PARSING
							char logtext[512];
							sprintf_s(logtext, 512, "Should load blend file: %s", pName);
							Logger::add(logtext);
						#endif

						Raytracer * rtr = Raytracer::getInstance();
						Scene * sc = rtr->curScenePtr;
						sc->postLoadBlendFile = pName;
					}
					break;
				case NOX_BIN_PRELOAD_FINAL:
					{
						unsigned int nameSize = 0;
						tPos = _ftelli64(file);
						if (1!=FREAD_UINT(nameSize))
						{
							sprintf_s(errorInfo, 256, "Error reading final file name size at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						nameSize -= 6;
						
						char * pName = (char *)malloc(nameSize+1);
						if (!pName)
						{
							sprintf_s(errorInfo, 256, "Error allocating %d bytes for final file name. Size taken from %lld.", (nameSize+1), tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}

						tPos = _ftelli64(file);
						if (1!=FREAD_STRING(pName, nameSize))
						{
							sprintf_s(errorInfo, 256, "Error reading final file name at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							free(pName);
							return false;
						}

						pName[nameSize] = 0;
						#ifdef LOG_NOX_PARSING
							char logtext[512];
							sprintf_s(logtext, 512, "Should load final file: %s", pName);
							Logger::add(logtext);
						#endif

						Raytracer * rtr = Raytracer::getInstance();
						Scene * sc = rtr->curScenePtr;
						sc->postLoadFinalFile = pName;
					}
					break;

				default:
					{
						if (!parseUnknownChunk(nextChunk, 4))
						{
							return false;
						}
					}
					break;
			}	// end while
		}		// end if

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Scene part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::ignoreChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	char errorInfo[256];
	unsigned long long tPos;
	unsigned long long chunkSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(chunkSize))
	{
		sprintf_s(errorInfo, 256, "Error reading ignored chunk size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + chunkSize - 2 - 8;

	if (_fseeki64(file, borderPos, SEEK_SET))
	{
		sprintf_s(errorInfo, 256, "Error while reading or file has ended before ignored chunk end.");
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseUnknownChunk(unsigned short chunkID, unsigned int sizesize)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	char errorInfo[256];
	unsigned long long tPos;

	tPos = _ftelli64(file);
	sprintf_s(errorInfo, 256, "Unknown chunk (0x%x) at %lld\nScene file is probably corrupted.\nRendering may crash.", chunkID, tPos);
	MessageBox(0, errorInfo, "Warning", 0);

	if (sizesize!=4 && sizesize!=8)
	{
		sprintf_s(errorInfo, 256, "Internal error. Size should be written as 4 or 8 bytes - not %d bytes", sizesize);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long chunkSize = 0;
	tPos = _ftelli64(file);
	if (sizesize==8)
	{
		if (1!=FREAD_ULONG8(chunkSize))
		{
			sprintf_s(errorInfo, 256, "Error reading unknown chunk (%x) size at %lld.", chunkID, tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}
	}
	if (sizesize==4)
	{
		unsigned int sss4;
		if (1!=FREAD_UINT(sss4))
		{
			sprintf_s(errorInfo, 256, "Error reading unknown chunk (%x) size at %lld.", chunkID, tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}
		chunkSize = sss4;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + chunkSize - 2 - sizesize;

	if (_fseeki64(file, borderPos, SEEK_SET))
	{
		sprintf_s(errorInfo, 256, "Error while reading or file has ended before unknown chunk (%x) end.", chunkID);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return false;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseGeometryChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}


	unsigned long long geomStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long geomSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(geomSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading Geometry chunk size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + geomSize - 10;

	unsigned int numMeshes = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(numMeshes))
	{
		sprintf_s(errorInfo, 256, "Error while reading number of meshes in geometry at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	unsigned int triSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(triSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading single triangle size in file at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	if (triSize != 102  &&  triSize != 106)
	{
		sprintf_s(errorInfo, 256, "Error. Single triangle size in file should be 102 or 106 instead of %d at %lld.", triSize, tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	singleTriSize = triSize;
	
	unsigned int gnumTris = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(gnumTris))
	{
		sprintf_s(errorInfo, 256, "Error while reading number of triangles in geometry at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	

	#ifdef LOG_NOX_PARSING
		char logtext[512];
		Logger::add("  Parsing geometry");
		sprintf_s(logtext, 512, "    total number of meshes: %u", numMeshes);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    total number of triangles: %u", gnumTris);
		Logger::add(logtext);
	#endif

	totalTris = gnumTris;
	soFarTris = 0;

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Geometry part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MESH:
				{
					static int singleID = 1000000;
					singleID++;
					if (!parseMeshChunk())
					{
						return false;
					}

					Raytracer * rtr = Raytracer::getInstance();
					MeshInstance inst;
					inst.meshID = rtr->curScenePtr->meshes.objCount-1;
					inst.instSrcID = singleID;
					rtr->curScenePtr->meshes[inst.meshID].instSrcID = singleID;
					rtr->curScenePtr->instances.add(inst);
					rtr->curScenePtr->instances.createArray();
				}
				break;
			case NOX_BIN_INSTANCE_SOURCE:
				{
					if (!parseInstanceSourceChunk())
					{
						return false;
					}
				}
				break;
			case NOX_BIN_INSTANCE:
				{
					if (!parseInstanceChunk())
					{
						return false;
					}
				}
				break;
			case NOX_BIN_SCALE_SCENE:
				{
					tPos = _ftelli64(file);
					float scale;
					if (1!=FREAD_FLOAT(scale))
					{
						sprintf_s(errorInfo, 256, "Error reading geometry scale at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Scene * sc = Raytracer::getInstance()->curScenePtr;
					if (sc)
					{
						scale = min(NOX_SCALE_GEOM_MAX, max(NOX_SCALE_GEOM_MIN, scale));
						sc->sscene.scaleScene = scale;
						sc->sscene.perScaleScene = 1.0f/scale;
					}
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Geometry part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	return true;
}

//----------------------------------------------------------------------------------------

void initMeshInst(void * addr);

bool BinaryScene::parseInstanceChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	#ifdef LOG_NOX_PARSING
		Logger::add("    Parsing instance");
	#endif

	unsigned long long instanceStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long instanceSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(instanceSize))
	{
		sprintf_s(errorInfo, 256, "Error reading instance size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + instanceSize - 10;

	unsigned int meshid=0;
	if (1!=FREAD_UINT(meshid))
	{
		sprintf_s(errorInfo, 256, "Error reading instance id at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	MeshInstance * t_inst = new MeshInstance();
	t_inst->meshID = -1;
	t_inst->instSrcID = meshid;
	rtr->curScenePtr->instances.add(*t_inst, initMeshInst);
	free(t_inst);
	t_inst = NULL;
	rtr->curScenePtr->instances.createArray();
	MeshInstance * inst = &(rtr->curScenePtr->instances[rtr->curScenePtr->instances.objCount-1]);
	inst->meshID = -1;
	inst->instSrcID = meshid;

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Instance part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_INSTANCE_NAME:
				{
					unsigned int nameSize = 0;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(nameSize))
					{
						sprintf_s(errorInfo, 256, "Error reading instance name size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					nameSize -= 6;
					
					char * pName = (char *)malloc(nameSize+1);
					if (!pName)
					{
						sprintf_s(errorInfo, 256, "Error allocating %d bytes for instance name. Size taken from %lld.", (nameSize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1!=FREAD_STRING(pName, nameSize))
					{
						sprintf_s(errorInfo, 256, "Error reading instance name at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(pName);
						return false;
					}

					pName[nameSize] = 0;
					#ifdef LOG_NOX_PARSING
						char logtext[512];
						sprintf_s(logtext, 512, "      instance name: %s", pName);
						Logger::add(logtext);
					#endif

					if (inst->name)
						free(inst->name);
					inst->name = pName;
				}
				break;
			case NOX_BIN_INSTANCE_MATRIX:
				{
					Matrix4d matrix;
					for (int i=0; i<4; i++)
						for (int j=0; j<4; j++)
						{
							float val;
							tPos = _ftelli64(file);
							if (1 != FREAD_FLOAT(val))
							{
								sprintf_s(errorInfo, 256, "Error while reading Instance matrix chunk at %lld.", tPos);
								MessageBox(0, errorInfo, "Error", 0);
								return false;
							}
							matrix.M[i][j] = val;
						}

					Logger::add("Instance matrix:");
					char buf[256];
					for (int i=0; i<4; i++)
					{
						sprintf_s(buf, 256, "%f  %f  %f  %f", matrix.M[i][0], matrix.M[i][1], matrix.M[i][2], matrix.M[i][3]);
						Logger::add(buf);
					}

					inst->matrix_transform = matrix;
					matrix.getInverted(inst->matrix_inv);
				}
				break;
			case NOX_BIN_INSTANCE_MATERIALS:
				{
					#ifdef LOG_NOX_PARSING
						Logger::add("      instance materials...");
					#endif
					unsigned short nummats;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(nummats))
					{
						sprintf_s(errorInfo, 256, "Error while reading Instance materials count at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					inst->matsFromFile.freeList();
					for (int i=0; i<nummats;  i++)
					{
						unsigned int matID;
						tPos = _ftelli64(file);
						if (1 != FREAD_UINT(matID))
						{
							sprintf_s(errorInfo, 256, "Error while reading Instance material ID at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						inst->matsFromFile.add(matID);
						#ifdef LOG_NOX_PARSING
							char logtext[512];
							sprintf_s(logtext, 512, "      instance material id: %d", matID);
							Logger::add(logtext);
						#endif

					}
					inst->matsFromFile.createArray();
				}
				break;
			case NOX_BIN_INSTANCE_MOTION_BLUR:
				{
					unsigned short mbon;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mbon))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur on at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					inst->mb_on = (mbon!=0);
					#ifdef LOG_NOX_PARSING
						if (inst->mb_on)
							Logger::add("      instance motion blur on");
						else
							Logger::add("      instance motion blur off");
					#endif
				}
				break;
			case NOX_BIN_INSTANCE_MB_POS:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(inst->mb_velocity.x))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur pos at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(inst->mb_velocity.y))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur pos at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(inst->mb_velocity.z))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur pos at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					#ifdef LOG_NOX_PARSING
						char logtext[512];
						sprintf_s(logtext, 512, "      instance motion blur move: %.4f  %.4f  %.4f", inst->mb_velocity.x, inst->mb_velocity.y, inst->mb_velocity.z);
						Logger::add(logtext);
					#endif
				}
				break;
			case NOX_BIN_INSTANCE_MB_ANGLE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(inst->mb_rot_vel))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur rotation angle at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(inst->mb_rot_axis.x))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur rotation axis at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(inst->mb_rot_axis.y))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur rotation axis at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(inst->mb_rot_axis.z))
					{
						sprintf_s(errorInfo, 256, "Error while reading motion blur rotation axis at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					#ifdef LOG_NOX_PARSING
						char logtext[512];
						sprintf_s(logtext, 512, "      instance motion blur rotate: %.4f around %.4f  %.4f  %.4f", inst->mb_rot_vel, inst->mb_rot_axis.x, inst->mb_rot_axis.y, inst->mb_rot_axis.z);
						Logger::add(logtext);
					#endif
				}
				break;
			case NOX_BIN_INSTANCE_OFFSET_MATRIX:
				{
					Matrix4d matrix;
					for (int i=0; i<4; i++)
						for (int j=0; j<4; j++)
						{
							float val;
							tPos = _ftelli64(file);
							if (1 != FREAD_FLOAT(val))
							{
								sprintf_s(errorInfo, 256, "Error while reading Instance offset matrix chunk at %lld.", tPos);
								MessageBox(0, errorInfo, "Error", 0);
								return false;
							}
							matrix.M[i][j] = val;
						}
					inst->matrix_offset = matrix;
					matrix.getInverted(inst->matrix_offset_inv);
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (inst->mb_on)
	{
		float tempfps=24.0f;
		Matrix4d m_mb;
		m_mb.setIdentity();
		m_mb.setRotation(inst->mb_rot_axis, inst->mb_rot_vel/tempfps);
		m_mb.M03 = inst->mb_velocity.x/tempfps;
		m_mb.M13 = inst->mb_velocity.y/tempfps;
		m_mb.M23 = inst->mb_velocity.z/tempfps;
		logMatrixMax("MB Matrix", m_mb);

		m_mb.setIdentity();
		m_mb.setRotation(inst->mb_rot_axis, inst->mb_rot_vel*-1);
		Vector3d tr = inst->mb_velocity * -1;
		tr = m_mb*tr;
		m_mb.M03 = tr.x;
		m_mb.M13 = tr.y;
		m_mb.M23 = tr.z;
		logMatrixMax("MB Matrix Inverted", m_mb);

		logMatrixMax("Offset Matrix", inst->matrix_offset);
		logMatrixMax("Offset Matrix Inverted", inst->matrix_offset_inv);

		logMatrixMax("Transform matrix", inst->matrix_transform);

		Vector3d pp = inst->matrix_transform * inst->mb_rot_axis;
		Vector3d pp2 = inst->matrix_transform * inst->mb_velocity;
		Matrix4d mver = inst->matrix_transform * m_mb;
		logMatrixMax("Verify matrix", mver);
		char buf[64];
		sprintf_s(buf, 64, "transf * rotvec = %f  %f  %f", pp.x,  pp.y,  pp.z);
		Logger::add(buf);
		sprintf_s(buf, 64, "transf * move = %f  %f  %f", pp2.x,  pp2.y,  pp2.z);
		Logger::add(buf);

	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Instance part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseInstanceSourceChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	unsigned long long instanceSourceStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long instanceSourceSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(instanceSourceSize))
	{
		sprintf_s(errorInfo, 256, "Error reading instance source size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + instanceSourceSize - 10;

	unsigned int meshid=0;
	if (1!=FREAD_UINT(meshid))
	{
		sprintf_s(errorInfo, 256, "Error reading instance id at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}


	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Instance source part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MESH:
				{
					if (!parseMeshChunk())
					{
						return false;
					}

					Raytracer * rtr = Raytracer::getInstance();
					MeshInstance inst;
					int mID = rtr->curScenePtr->meshes.objCount-1;
					rtr->curScenePtr->meshes[mID].instSrcID = meshid;
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Instance source part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseMeshChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	unsigned long long meshStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long meshSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(meshSize))
	{
		sprintf_s(errorInfo, 256, "Error reading mesh size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + meshSize - 10;


	unsigned int mnumTris = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(mnumTris))
	{
		sprintf_s(errorInfo, 256, "Error reading number of tris in mesh at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	rtr->curScenePtr->notifyProgress("Loading geometry...", (float)(soFarTris/(double)totalTris*100));

	triCounter = 0;
	cbMin = Point3d( BIGFLOAT,  BIGFLOAT,  BIGFLOAT);
	cbMax = Point3d(-BIGFLOAT, -BIGFLOAT, -BIGFLOAT);
	Mesh em;
	rtr->curScenePtr->meshes.add(em);
	rtr->curScenePtr->meshes.createArray();
	Mesh * mesh = &(rtr->curScenePtr->meshes[rtr->curScenePtr->meshes.objCount-1]);
	bool firstTri = true;


	#ifdef LOG_NOX_PARSING
		char logtext[512];
		Logger::add("    Parsing mesh");
		sprintf_s(logtext, 512, "      triangles in mesh: %u", mnumTris);
		Logger::add(logtext);
	#endif

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		unsigned short nextChunk;
		tPos = _ftelli64(file);
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Mesh part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_TRIANGLE:
				{
					if (firstTri)
					{
						mesh->firstTri = cTri;
						firstTri = false;
					}
					mesh->lastTri = cTri;

					if (!parseTriangleFastChunk())
					{
						return false;
					}
				}
				break;
			case NOX_BIN_MESHNAME:
				{
					if (!parseMeshNameChunk(mesh))
					{
						return false;
					}
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Mesh part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	curMeshNum++;

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseMeshNameChunk(Mesh * mesh)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long meshnameStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned int meshNameSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(meshNameSize))
	{
		sprintf_s(errorInfo, 256, "Error reading mesh name size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	meshNameSize -= 6;
	
	char * meshName = (char *)malloc(meshNameSize+1);
	if (!meshName)
	{
		sprintf_s(errorInfo, 256, "Error allocating %d bytes for mesh name. Size taken from %lld.", (meshNameSize+1), tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	tPos = _ftelli64(file);
	if (1!=FREAD_STRING(meshName, meshNameSize))
	{
		sprintf_s(errorInfo, 256, "Error reading mesh name at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		free(meshName);
		return false;
	}

	meshName[meshNameSize] = 0;
	mesh->assignName(meshName);

	#ifdef LOG_NOX_PARSING
		char logtext[512];
		sprintf_s(logtext, 512, "      mesh name: %s", meshName);
		Logger::add(logtext);
	#endif

	free(meshName);

	return true;
}


//----------------------------------------------------------------------------------------

bool BinaryScene::parseTriangleFastChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	cTri++;
	soFarTris++;

	unsigned long long triStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	char wholePart[200];
	tPos = _ftelli64(file);
	if (singleTriSize == 102)
	{
		if (1!=fread(wholePart, 100, 1, file))
		{
			sprintf_s(errorInfo, 256, "Error reading triangle at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}
	}

	if (singleTriSize == 106)
	{
		if (1!=fread(wholePart, 104, 1, file))
		{
			sprintf_s(errorInfo, 256, "Error reading triangle at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}
	}

	Point3d v1,v2,v3;
	Vector3d n1,n2,n3;
	Point2d uv1, uv2, uv3;
	unsigned int matID;
	unsigned int matInInstSlot = 0;

	if (singleTriSize == 102)
	{
		matID = *((unsigned int *)&(wholePart[0]));
		v1.x = *((float *)&(wholePart[4]));
		v1.y = *((float *)&(wholePart[8]));
		v1.z = *((float *)&(wholePart[12]));
		v2.x = *((float *)&(wholePart[16]));
		v2.y = *((float *)&(wholePart[20]));
		v2.z = *((float *)&(wholePart[24]));
		v3.x = *((float *)&(wholePart[28]));
		v3.y = *((float *)&(wholePart[32]));
		v3.z = *((float *)&(wholePart[36]));
		n1.x = *((float *)&(wholePart[40]));
		n1.y = *((float *)&(wholePart[44]));
		n1.z = *((float *)&(wholePart[48]));
		n2.x = *((float *)&(wholePart[52]));
		n2.y = *((float *)&(wholePart[56]));
		n2.z = *((float *)&(wholePart[60]));
		n3.x = *((float *)&(wholePart[64]));
		n3.y = *((float *)&(wholePart[68]));
		n3.z = *((float *)&(wholePart[72]));
		uv1.x = *((float *)&(wholePart[76]));
		uv1.y = *((float *)&(wholePart[80]));
		uv2.x = *((float *)&(wholePart[84]));
		uv2.y = *((float *)&(wholePart[88]));
		uv3.x = *((float *)&(wholePart[92]));
		uv3.y = *((float *)&(wholePart[96]));
	}

	if (singleTriSize == 106)
	{
		matInInstSlot = *((unsigned int *)&(wholePart[0]));
		matID = *((unsigned int *)&(wholePart[4]));
		v1.x = *((float *)&(wholePart[8]));
		v1.y = *((float *)&(wholePart[12]));
		v1.z = *((float *)&(wholePart[16]));
		v2.x = *((float *)&(wholePart[20]));
		v2.y = *((float *)&(wholePart[24]));
		v2.z = *((float *)&(wholePart[28]));
		v3.x = *((float *)&(wholePart[32]));
		v3.y = *((float *)&(wholePart[36]));
		v3.z = *((float *)&(wholePart[40]));
		n1.x = *((float *)&(wholePart[44]));
		n1.y = *((float *)&(wholePart[48]));
		n1.z = *((float *)&(wholePart[52]));
		n2.x = *((float *)&(wholePart[56]));
		n2.y = *((float *)&(wholePart[60]));
		n2.z = *((float *)&(wholePart[64]));
		n3.x = *((float *)&(wholePart[68]));
		n3.y = *((float *)&(wholePart[72]));
		n3.z = *((float *)&(wholePart[76]));
		uv1.x = *((float *)&(wholePart[80]));
		uv1.y = *((float *)&(wholePart[84]));
		uv2.x = *((float *)&(wholePart[88]));
		uv2.y = *((float *)&(wholePart[92]));
		uv3.x = *((float *)&(wholePart[96]));
		uv3.y = *((float *)&(wholePart[100]));
	}

	Triangle * tri = new Triangle(v1,v2,v3, n1,n2,n3, uv1,uv2,uv3, curMeshNum);

	tri->matInInst = matInInstSlot;
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->triangles.add(*tri);
	delete tri;

	float maxx, maxy, maxz, minx, miny, minz;
	maxx = max(max(v1.x, v2.x), v3.x);
	maxy = max(max(v1.y, v2.y), v3.y);
	maxz = max(max(v1.z, v2.z), v3.z);
	minx = min(min(v1.x, v2.x), v3.x);
	miny = min(min(v1.y, v2.y), v3.y);
	minz = min(min(v1.z, v2.z), v3.z);
	if (cbMin.x > minx)
		cbMin.x = minx;
	if (cbMin.y > miny)
		cbMin.y = miny;
	if (cbMin.z > minz)
		cbMin.z = minz;
	if (cbMax.x < maxx)
		cbMax.x = maxx;
	if (cbMax.y < maxy)
		cbMax.y = maxy;
	if (cbMax.z < maxz)
		cbMax.z = maxz;

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseTriangleChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	cTri++;
	soFarTris++;

	unsigned long long triStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned int matID = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(matID))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle material ID at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	float v1x, v1y, v1z, v2x, v2y, v2z, v3x, v3y, v3z;
	float n1x, n1y, n1z, n2x, n2y, n2z, n3x, n3y, n3z;
	float uv1x, uv1y, uv2x, uv2y, uv3x, uv3y;

	// vertices
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v1x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v1x) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v1y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v1y) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v1z))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v1z) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v2x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v2x) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v2y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v2y) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v2z))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v2z) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v3x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v3x) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v3y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v3y) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(v3z))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (v3z) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	// normals
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n1x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n1x) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n1y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n1y) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n1z))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n1z) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n2x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n2x) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n2y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n2y) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n2z))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n2z) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n3x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n3x) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n3y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n3y) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(n3z))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (n3z) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	// uv
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(uv1x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (uv1u) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(uv1y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (uv1v) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(uv2x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (uv2u) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(uv2y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (uv2v) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(uv3x))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (uv3u) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	tPos = _ftelli64(file);
	if (1!=FREAD_FLOAT(uv3y))
	{
		sprintf_s(errorInfo, 256, "Error reading triangle data (uv3v) at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	// PARSED OK - now add

	Triangle * tri = new Triangle( Point3d(v1x, v1y, v1z), Point3d(v2x, v2y, v2z), Point3d(v3x, v3y, v3z), 
						Vector3d(n1x, n1y, n1z), Vector3d(n2x, n2y, n2z), Vector3d(n3x, n3y, n3z), 
						Point2d(uv1x, uv1y), Point2d(uv2x, uv2y), Point2d(uv3x, uv3y), curMeshNum);
		
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->triangles.add(*tri);
	delete tri;

	float maxx, maxy, maxz, minx, miny, minz;
	maxx = max(max(v1x, v2x), v3x);
	maxy = max(max(v1y, v2y), v3y);
	maxz = max(max(v1z, v2z), v3z);
	minx = min(min(v1x, v2x), v3x);
	miny = min(min(v1y, v2y), v3y);
	minz = min(min(v1z, v2z), v3z);
	if (cbMin.x > minx)
		cbMin.x = minx;
	if (cbMin.y > miny)
		cbMin.y = miny;
	if (cbMin.z > minz)
		cbMin.z = minz;
	if (cbMax.x < maxx)
		cbMax.x = maxx;
	if (cbMax.y < maxy)
		cbMax.y = maxy;
	if (cbMax.z < maxz)
		cbMax.z = maxz;

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseCameraChunk(bool merge)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	unsigned long long cameraStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long camSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(camSize))
	{
		sprintf_s(errorInfo, 256, "Error reading camera size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + camSize - 10;


	Camera * cam = new Camera();

	unsigned short cam_active = 0;
	unsigned int cam_width = 640;
	unsigned int cam_height = 480;
	float cam_posx=0, cam_posy=0, cam_posz=0;
	float cam_dirx=0, cam_diry=0, cam_dirz=0;
	float  cam_upx=0, cam_upy=0,  cam_upz=0;
	float  cam_rx=0,  cam_ry=0,   cam_rz=0;
	float cam_angle = 360.0f/PI*atan(18.0f/35);
	float cam_iso = 100;
	float cam_aperture = 8;
	float cam_shutter = 250;
	unsigned short cam_autoexp_on = 0;
	unsigned short cam_autoexp_type = 0;
	unsigned short cam_autofocus_on = 0;
	float cam_focusdist = 2;
	unsigned short cam_blades_number = 5;
	float cam_blades_angle = 0;
	unsigned short cam_blades_round = 0;
	float cam_blades_radius = 3;
	unsigned short cam_antialias = 2;
	unsigned short ring_size = 20;
	short ring_balance = 0;
	short bokeh_vignette = 0;
	unsigned short bokeh_flatten = 0;
	unsigned short dof_on = true;
	bool wasFinal = false;
	unsigned short mb_enabled;
	Vector3d mb_pos;
	Vector3d mb_rot;
	float mb_rot_angle;
	bool there_was_right_dir = false;


	#ifdef LOG_NOX_PARSING
		char logtext[512];
		Logger::add("  Parsing camera");
	#endif


	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		unsigned short nextChunk;
		tPos = _ftelli64(file);
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Camera part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_CAM_NAME:
				{
					if (!parseCameraNameChunk(cam))
					{
						return false;
					}
				}
				break;

			case NOX_BIN_CAM_ACTIVE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_active))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera active chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_WIDTH:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(cam_width))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera width chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					cam->width = cam_width;
				}
				break;
			case NOX_BIN_CAM_HEIGHT:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(cam_height))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera height chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					cam->height = cam_height;
				}
				break;
			case NOX_BIN_CAM_AA:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_antialias))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera aa chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					cam->aa = cam_antialias;
				}
				break;
			case NOX_BIN_CAM_POS:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_posx))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera position x chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_posy))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera position y chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_posz))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera position z chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_DIR:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_dirx))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera direction x chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_diry))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera direction y chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_dirz))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera direction z chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_UPDIR:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_upx))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera up direction x chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_upy))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera up direction y chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_upz))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera up direction z chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_RIGHTDIR:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_rx))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera up direction x chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_ry))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera up direction y chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_rz))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera up direction z chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					there_was_right_dir = true;
				}
				break;
			case NOX_BIN_CAM_ANGLE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_angle))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera angle chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_ISO:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_iso))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera ISO chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_APERTURE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_aperture))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera aperture chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_SHUTTER:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_shutter))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera shutter chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_AUTOEXPOSURE_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_autoexp_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera autoexposure on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_AUTOEXPOSURE_TYPE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_autoexp_type))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera autoexposure type chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_AUTOFOCUS:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_autofocus_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera autofocus chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_FOCUSDIST:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_focusdist))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera focus distance chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BLADESNUMBER:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_blades_number))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera blades number chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BLADESANGLE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_blades_angle))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera blades angle chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BLADESROUND:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_blades_round))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera blades round chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BLADESRADIUS:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cam_blades_radius))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera blades radius chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_DOF_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(dof_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera dof on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BOKEH_RING_SIZE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ring_size))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera bokeh ring size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BOKEH_RING_BALANCE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ring_balance))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera bokeh ring balance chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BOKEH_FLATTEN:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(bokeh_flatten))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera bokeh flatten chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_BOKEH_VIGNETTE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(bokeh_vignette))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera bokeh vignette chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_POST:
				{
					if (!parseCameraPostChunk(&cam->iMod))
					{
						return false;
					}
				}
				break;
			case NOX_BIN_FINAL_POST:
				{
					char * texDiaph = NULL;
					char * texObst = NULL;
					if (!parseCameraFinalPostChunk(&cam->fMod, &texDiaph, &texObst))
					{
						return false;
					}

					if (texObst)
					{
						cam->texObstacle.loadFromFile(texObst);
						free(texObst);
					}
					if (texDiaph)
					{
						cam->texDiaphragm.loadFromFile(texDiaph);
						free(texDiaph);
					}
					cam->fMod.focal = 0.5f * 36 / tan(cam->angle * PI / 360.0f) * 0.001f;
					wasFinal = true;
				}
				break;
			case NOX_BIN_CAM_BUFFERS:
				{
					if (!parseCameraBuffersChunk(cam))
					{
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_DEPTH_BUF:
				{
					if (!parseCameraDepthBufChunk(cam))
					{
						return false;
					}
				}
				break;

			case NOX_BIN_CAM_MB_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mb_enabled))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_MB_POS:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mb_pos.x))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur pos chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mb_pos.y))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur pos chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mb_pos.z))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur pos chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_CAM_MB_ANGLE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mb_rot_angle))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur rotation angle chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (1 != FREAD_FLOAT(mb_rot.x))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur rotation axis chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mb_rot.y))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur rotation axis chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mb_rot.z))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera motion blur rotation axis chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;

			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Camera part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	#ifdef LOG_NOX_PARSING
		sprintf_s(logtext, 512, "    camera resolution: %d x %d", cam_width, cam_height);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera aa: %d", cam_antialias);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera is active: %s", (cam_active!=0) ? "yes" : "no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera position: %f %f %f", cam_posx, cam_posy, cam_posz);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera direction: %f %f %f", cam_dirx, cam_diry, cam_dirz);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera up direction: %f %f %f", cam_upx, cam_upy, cam_upz);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera angle: %f", cam_angle);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera ISO: %f", cam_iso);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera aperture: f/%f", cam_aperture);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera shutter: 1/%f sec", cam_shutter );
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera autoexposure: %s", (cam_autoexp_on!=0) ? "yes" : "no" );
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera autoexposure type: %d", cam_autoexp_type);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera autofocus: %s", (cam_autofocus_on!=0) ? "yes" : "no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera focus distance: %f", cam_focusdist);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera aperture blades number: %d", cam_blades_number);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera aperture blades angle: %f", cam_blades_angle);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera aperture blades round: %s", (cam_blades_round!=0) ? "yes" : "no" );
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera aperture blades radius: %f", cam_blades_radius);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera motion blur on: %s", mb_enabled ? "yes" : "no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera motion blur pos: %f  %f  %f", mb_pos.x, mb_pos.y, mb_pos.z);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    camera motion blur rotation: %f around %f  %f  %f", mb_rot_angle, mb_rot.x, mb_rot.y, mb_rot.z);
		Logger::add(logtext);
	#endif

	cam->width = cam_width;
	cam->height = cam_height;
	cam->position.x = cam_posx;
	cam->position.y = cam_posy;
	cam->position.z = cam_posz;
	cam->direction.x = cam_dirx;
	cam->direction.y = cam_diry;
	cam->direction.z = cam_dirz;
	cam->upDir.x = cam_upx;
	cam->upDir.y = cam_upy;
	cam->upDir.z = cam_upz;
	cam->direction.normalize();
	cam->upDir.normalize();
	if (!there_was_right_dir)
		cam->rightDir = (cam->direction ^ cam->upDir).getNormalized();
	else
		cam->rightDir = Vector3d(cam_rx, cam_ry, cam_rz);
	cam->rightDir.normalize();
	cam->angle = cam_angle;
	cam->ISO = cam_iso;
	cam->iMod.setISO_camera(cam_iso);
	cam->iMod.evalMultiplierFromISO_and_EV();
	cam->aperture = cam_aperture;
	cam->shutter = cam_shutter;
	cam->autoexposure = (cam_autoexp_on!=0);
	cam->autoexposureType = cam_autoexp_type;
	cam->autoFocus = (cam_autofocus_on!=0);
	cam->focusDist = cam_focusdist;
	cam->apBladesNum = cam_blades_number;
	cam->apBladesAngle = cam_blades_angle;
	cam->apBladesRound = (cam_blades_round!=0);
	cam->apBladesRadius = cam_blades_radius;
	cam->aa = cam_antialias;
	cam->mplApSh = 1.0f/cam_shutter/cam_aperture/cam_aperture/2.5f;
	cam->diaphSampler.intBokehRingBalance = ring_balance;
		if (ring_balance>=0)
		cam->diaphSampler.bokehRingBalance = ring_balance+1.0f;
	else
		cam->diaphSampler.bokehRingBalance = 1.0f + ring_balance/21.0f;
	cam->diaphSampler.intBokehRingSize = ring_size;
	cam->diaphSampler.bokehRingSize = ring_size*0.01f;
	cam->diaphSampler.intBokehFlatten = bokeh_flatten;
	cam->diaphSampler.bokehFlatten = bokeh_flatten*0.01f;
	cam->diaphSampler.intBokehVignette = bokeh_vignette;
	cam->diaphSampler.bokehVignette = bokeh_vignette*0.01f;
	cam->dofOnTemp = (dof_on!=0);

	cam->mb_on = (mb_enabled != 0);
	cam->mb_velocity = mb_pos;
	cam->mb_rot_vel = mb_rot;
	cam->mb_rot_angle = mb_rot_angle;


	float ctime = 1.0f/24.0f;
	Matrix4d mb_mat;
	mb_mat.setIdentity();
	mb_mat.setRotation(mb_rot, mb_rot_angle * ctime);
	mb_mat.M03 = mb_pos.x * ctime;
	mb_mat.M13 = mb_pos.y * ctime;
	mb_mat.M23 = mb_pos.z * ctime;
	logMatrix("mb", mb_mat);

	if (!merge)
	{
		rtr->curScenePtr->cameras.add(cam);
		rtr->curScenePtr->cameras.createArray();

		if (cam_active!=0)
		{
			rtr->curScenePtr->sscene.activeCamera = rtr->curScenePtr->cameras.objCount-1;
		}
	}

	if (!wasFinal)
	{
		cam->fMod.aperture = cam->aperture;
		cam->fMod.bladesNum = cam->apBladesNum;
		cam->fMod.bladesAngle = cam->apBladesAngle;
		cam->fMod.bladesRadius = cam->apBladesRadius;
		cam->fMod.roundBlades = cam->apBladesRound;
		cam->fMod.bokehRingBalance = cam->diaphSampler.intBokehRingBalance;
		cam->fMod.bokehRingSize = cam->diaphSampler.intBokehRingSize;
		cam->fMod.bokehFlatten = cam->diaphSampler.intBokehFlatten;
		cam->fMod.bokehVignette = cam->diaphSampler.intBokehVignette;
		cam->fMod.focusDistance = cam->focusDist;
	}

	if (merge)
	{
		if (mrgCamID >= rtr->curScenePtr->cameras.objCount)
		{
			sprintf_s(errorInfo, 256, "Error. Merged file contains more cameras than base file.");
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		float pos = mrgCamID/(float)rtr->curScenePtr->cameras.objCount;
		rtr->curScenePtr->notifyProgress("Merging scene buffers...", 0.0f);


		Camera * origcam = rtr->curScenePtr->cameras[mrgCamID];
		if (!origcam)
		{
			sprintf_s(errorInfo, 256, "Error. Base camera is NULL.");
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		if (!origcam->mergeDataFromOtherCameraFile(cam))
		{
			cam->deleteStuffAfterMerge();
			return false;
		}

		// all clean up
		cam->deleteStuffAfterMerge();
		delete cam;
		cam = NULL;
	}

	rtr->curScenePtr->notifyProgress("Merging done.", 100.0f);

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseCameraPostChunk(ImageModifier * iMod)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}
	if (!iMod)
	{
		MessageBox(0, "Error. Tried to parse post for NULL object.", "Error", 0);
		return false;
	}

	ImageModifier * im = iMod;

	unsigned long long postStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long postSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(postSize))
	{
		sprintf_s(errorInfo, 256, "Error reading camera post size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + postSize - 10;

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		unsigned short nextChunk;
		tPos = _ftelli64(file);
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Camera Post part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_CAM_POST_ISO:
				{
					float iso;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(iso))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post iso chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setMultiplier(iso);	// value is ignored
				}
				break;
			case NOX_BIN_CAM_POST_EV:
				{
					float ev;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(ev))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post iso ev chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setISO_EV_compensation(ev);
					im->evalMultiplierFromISO_and_EV();
				}
				break;
			case NOX_BIN_CAM_POST_GI_EV:
				{
					float ev;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(ev))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post gi ev chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setGICompensation(ev);
				}
				break;
			case NOX_BIN_CAM_POST_TONE:
				{
					float tone;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(tone))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post tone chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setToneMappingValue(tone);
				}
				break;
			case NOX_BIN_CAM_POST_GAMMA:
				{
					float gamma;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(gamma))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post gamma chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setGamma(gamma);
				}
				break;
			case NOX_BIN_CAM_POST_BRIGHTNESS:
				{
					float bri;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(bri))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post brightness chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setBrightness(bri);
				}
				break;
			case NOX_BIN_CAM_POST_CONTRAST:
				{
					float contr;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(contr))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post contrast chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setContrast(contr);
				}
				break;
			case NOX_BIN_CAM_POST_SATURATION:
				{
					float sat;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(sat))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post saturation chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setSaturation(sat);
				}
				break;
			case NOX_BIN_CAM_POST_ADD_RED:
				{
					float addR;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(addR))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post add red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					float r,g,b;
					r = addR;
					g = im->getG();
					b = im->getB();
					im->setRGBAdd(r,g,b);
				}
				break;
			case NOX_BIN_CAM_POST_ADD_GREEN:
				{
					float addG;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(addG))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post add green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					float r,g,b;
					r = im->getR();
					g = addG;
					b = im->getB();
					im->setRGBAdd(r,g,b);
				}
				break;
			case NOX_BIN_CAM_POST_ADD_BLUE:
				{
					float addB;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(addB))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post add blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					float r,g,b;
					r = im->getR();
					g = im->getG();
					b = addB;
					im->setRGBAdd(r,g,b);
				}
				break;
			case NOX_BIN_CAM_POST_TEMPERATURE:
				{
					float temperature;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(temperature))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post temperature chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setTemperature(temperature);
				}
				break;
			case NOX_BIN_CAM_POST_VIGNETTE:
				{
					unsigned short vignette;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(vignette))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post vignette chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setVignette(vignette);
				}
				break;
			case NOX_BIN_CAM_POST_RESPONSE_FUNCTION:
				{
					unsigned short response;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(response))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post response function chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setResponseFunctionNumber(response);
				}
				break;
			case NOX_BIN_CAM_POST_FILTER:
				{
					unsigned short filter;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(filter))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post aa filter chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setFilter(filter);
				}
				break;
			case NOX_BIN_CAM_POST_DOTS_ENABLED:
				{
					unsigned short dots;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(dots))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post hot pixels remover chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setDotsEnabled(dots!=0);
				}
				break;
			case NOX_BIN_CAM_POST_DOTS_RADIUS:
				{
					int radius;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(radius))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post hot pixels radius chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setDotsRadius(radius);
				}
				break;
			case NOX_BIN_CAM_POST_DOTS_THRESHOLD:
				{
					float thr;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(thr))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post hot pixels threshold chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setDotsThreshold(thr);
				}
				break;
			case NOX_BIN_CAM_POST_DOTS_DENSITY:
				{
					int dens;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(dens))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post hot pixels density chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					im->setDotsDensity(dens);
				}
				break;
			case NOX_BIN_CAM_POST_CURVE:
				{
					unsigned int numPoints = 0;
					unsigned short ckR, ckG, ckB, ckL;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(numPoints))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post curve number points chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ckR))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post curve red checkbox chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ckG))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post curve green checkbox chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ckB))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post curve blue checkbox chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ckL))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post curve lightness checkbox chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (ckL!=0)
						im->setCurveCheckboxes( false, false, false, (ckL!=0) );
					else
						if ((ckR!=0) || (ckG!=0) || (ckB!=0))
							im->setCurveCheckboxes( (ckR!=0), (ckG!=0), (ckB!=0), (ckL!=0) );
						else
							im->setCurveCheckboxes( false, false, false, true);

					for (unsigned int i=0; i<numPoints; i++)
					{
						unsigned short id, sx, sy;
						tPos = _ftelli64(file);
						if (1 != FREAD_USHORT(id))
						{
							sprintf_s(errorInfo, 256, "Error while reading camera post curve point tag at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						tPos = _ftelli64(file);
						if (1 != FREAD_USHORT(sx))
						{
							sprintf_s(errorInfo, 256, "Error while reading camera post curve point x at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						tPos = _ftelli64(file);
						if (1 != FREAD_USHORT(sy))
						{
							sprintf_s(errorInfo, 256, "Error while reading camera post curve point y at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						switch (id)
						{
							case NOX_BIN_CAM_POST_CURVE_PR:
								im->addRedPoint(sx,sy);
								break;
							case NOX_BIN_CAM_POST_CURVE_PG:
								im->addGreenPoint(sx,sy);
								break;
							case NOX_BIN_CAM_POST_CURVE_PB:
								im->addBluePoint(sx,sy);
								break;
							case NOX_BIN_CAM_POST_CURVE_PL:
								im->addLuminancePoint(sx,sy);
								break;
							default:
								{
									sprintf_s(errorInfo, 256, "Error. Invalid post curve point ID (0x%x) at %lld.", id, tPos);
									MessageBox(0, errorInfo, "Error", 0);
									return false;
								}
								break;
						}

					}
				}		// curve ends here
				break;
			case NOX_BIN_CAM_POST_BLOOM:
				{
					float bloom;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(bloom))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setBloom(bloom);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_BLOOM_THRESHOLD:
				{
					float threshold;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(threshold))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom threshold chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setThreshold(threshold);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_BLOOM_ON:
				{
					unsigned short bloom_on;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(bloom_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setOnBloom(bloom_on!=0);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_BLOOM_AREA:
				{
					unsigned int bloom_area;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(bloom_area))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom area chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setBloomArea(bloom_area);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_BLOOM_ATTENUATION_ON:
				{
					unsigned short bloom_att_on;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(bloom_att_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom attenuation on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setBloomIgnoreColor(bloom_att_on!=0);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_BLOOM_ATTENUATION_COLOR:
				{
					Color4 acolor;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(acolor.r))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom attenuation color R chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(acolor.g))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom attenuation color G chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(acolor.b))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom attenuation color B chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(acolor.a))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post bloom attenuation color A chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setBloomColor(acolor);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_GLARE:
				{
					float glare;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(glare))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post glare chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setGlare(glare);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_GLARE_ON:
				{
					unsigned short glare_on;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(glare_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post glare on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setOnGlare(glare_on!=0);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_GLARE_DISPERSION:
				{
					float glare_disp;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(glare_disp))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post glare dispersion chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setGlareDispersion(glare_disp);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_GLARE_TEX_DIAPHRAGM_ON:
				{
					unsigned short glare_tex_di_on;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(glare_tex_di_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post glare diaphragm texture on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setOnDiaphragmShape(glare_tex_di_on!=0);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_GLARE_TEX_OBSTACLE_ON:
				{
					unsigned short glare_tex_ob_on;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(glare_tex_ob_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post glare obstacle texture on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					//im->setOnDiaphragmShape(glare_tex_ob_on!=0);	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_GLARE_TEX_DIAPHRAGM:
				{
					unsigned int lsize;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(lsize))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post glare diaphragm texture chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					lsize -= 6;
					if (lsize<0)
					{
						sprintf_s(errorInfo, 256, "Error. Camera post glare diaphragm texture name size is negative at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (lsize==0)
						break;


					char * tex_di_name = (char *)malloc(lsize+1);
					if (!tex_di_name)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for diaphragm texture name.", (lsize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(tex_di_name, lsize))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of diaphragm texture name chunk at %lld.", lsize, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(tex_di_name);
						return false;
					}
					tex_di_name[lsize] = 0;

					//if (tex_di_name)
					//	cam->texDiaphragm.loadFromFile(getValidTexPath(tex_di_name));	<- value ignored
				}
				break;
			case NOX_BIN_CAM_POST_GLARE_TEX_OBSTACLE:
				{
					unsigned int lsize;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(lsize))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera post glare obstacle texture chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					lsize -= 6;
					if (lsize<0)
					{
						sprintf_s(errorInfo, 256, "Error. Camera post glare obstacle texture name size is negative at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (lsize==0)
						break;


					char * tex_ob_name = (char *)malloc(lsize+1);
					if (!tex_ob_name)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for obstacle texture name.", (lsize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(tex_ob_name, lsize))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of obstacle texture name chunk at %lld.", lsize, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(tex_ob_name);
						return false;
					}
					tex_ob_name[lsize] = 0;

					//if (tex_ob_name)
					//	cam->texDiaphragm.loadFromFile(getValidTexPath(tex_ob_name));	<- value ignored
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Camera Post part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseCameraFinalPostChunk(FinalModifier * fMod, char ** texDiaph, char ** texObst)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	if (!fMod)
	{
		MessageBox(0, "Error. Tried to parse final post for NULL object.", "Error", 0);
		return false;
	}
	FinalModifier * fm = fMod;
	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long postStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long postSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(postSize))
	{
		sprintf_s(errorInfo, 256, "Error reading camera final post size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + postSize - 10;

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		unsigned short nextChunk;
		tPos = _ftelli64(file);
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Camera Final Post part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_FINAL_QUALITY:
				{
					unsigned int quality;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(quality))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post quality chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->quality = quality;
				}
				break;
			case NOX_BIN_FINAL_THREADS:
				{
					unsigned short nThreads;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(nThreads))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post threads number chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->numThreads = nThreads;
				}
				break;
			case NOX_BIN_FINAL_ALGORITHM:
				{
					unsigned short algorithm;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(algorithm))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post dof algorithm chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->algorithm = algorithm;
				}
				break;
			case NOX_BIN_FINAL_FAKE_DOF_ON:
				{
					unsigned short fakedofon;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(fakedofon))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post fake dof on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->dofOn = (fakedofon!=0);
				}
				break;
			case NOX_BIN_FINAL_FOCUS_DISTANCE:
				{
					float focusDist;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(focusDist))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post focus distance chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->focusDistance = focusDist;
				}
				break;
			case NOX_BIN_FINAL_APERTURE:
				{
					float aperture;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(aperture))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post aperture chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->aperture = aperture;
				}
				break;
			case NOX_BIN_FINAL_DIAPH_NUM_BLADES:
				{
					unsigned short numBlades;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(numBlades))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post diaphragm blades number chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bladesNum = numBlades;
				}
				break;
			case NOX_BIN_FINAL_DIAPH_ANGLE:
				{
					float angle;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(angle))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post diaphragm angle chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bladesAngle = angle;
				}
				break;
			case NOX_BIN_FINAL_DIAPH_RADIUS:
				{
					float radius;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(radius))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post diaphragm blade radius chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bladesRadius = radius;
				}
				break;
			case NOX_BIN_FINAL_DIAPH_BLADES_ROUND:
				{
					unsigned short bladesround;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(bladesround))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post diaphragm blades round chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->roundBlades = (bladesround!=0);
				}
				break;
			case NOX_BIN_FINAL_BOKEH_RING_BALANCE:
				{
					unsigned short ringBalance;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ringBalance))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bokeh ring balance chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bokehRingBalance = ringBalance;
				}
				break;
			case NOX_BIN_FINAL_BOKEH_RING_SIZE:
				{
					unsigned short ringSize;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(ringSize))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bokeh ring size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bokehRingSize = ringSize;
				}
				break;
			case NOX_BIN_FINAL_BOKEH_FLATTEN:
				{
					unsigned short flatten;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(flatten))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bokeh flatten chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bokehFlatten = flatten;
				}
				break;
			case NOX_BIN_FINAL_BOKEH_VIGNETTE:
				{
					unsigned short vignette;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(vignette))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bokeh vignette chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bokehVignette = vignette;
				}
				break;
			case NOX_BIN_FINAL_ABERRATION_SHIFT_LENS:
				{
					float shiftLens;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(shiftLens))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post chromatic aberration shift on lens chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->chromaticShiftLens = shiftLens;
				}
				break;
			case NOX_BIN_FINAL_ABERRATION_SHIFT_DEPTH:
				{
					float shiftDepth;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(shiftDepth))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post chromatic aberration shift on depth chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->chromaticShiftDepth = shiftDepth;
				}
				break;
			case NOX_BIN_FINAL_ABERRATION_ACHROMATIC:
				{
					float achromatic;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(achromatic))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post chromatic aberration achromatic chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->chromaticAchromatic = achromatic;
				}
				break;

			case NOX_BIN_FINAL_ABERRATION_RED:
				{
					float red;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(red))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post chromatic aberration red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->chromaticPlanarRed = red;
				}
				break;

			case NOX_BIN_FINAL_ABERRATION_GREEN:
				{
					float green;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(green))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post chromatic aberration green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->chromaticPlanarGreen = green;
				}
				break;

			case NOX_BIN_FINAL_ABERRATION_BLUE:
				{
					float blue;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(blue))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post chromatic aberration blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->chromaticPlanarBlue = blue;
				}
				break;



			case NOX_BIN_FINAL_BLOOM_ON:
				{
					unsigned short bloomon;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(bloomon))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bloomEnabled = (bloomon!=0);
				}
				break;
			case NOX_BIN_FINAL_BLOOM_POWER:
				{
					float bloompower;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(bloompower))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom power chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bloomPower = bloompower;
				}
				break;
			case NOX_BIN_FINAL_BLOOM_AREA:
				{
					unsigned int bloomarea;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(bloomarea))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom area chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bloomArea = bloomarea;
				}
				break;
			case NOX_BIN_FINAL_BLOOM_THRESHOLD:
				{
					float bloomthres;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(bloomthres))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom threshold chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bloomThreshold = bloomthres;
				}
				break;
			case NOX_BIN_FINAL_BLOOM_ATTENUATION_ON:
				{
					unsigned short bloomatton;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(bloomatton))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom attenuation enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bloomAttenuationOn = (bloomatton!=0);
				}
				break;
			case NOX_BIN_FINAL_BLOOM_ATTENUATION_COLOR:
				{
					float bloomr, bloomg, bloomb;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(bloomr))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom attenuation color R chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (1 != FREAD_FLOAT(bloomg))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom attenuation color G chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (1 != FREAD_FLOAT(bloomb))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post bloom attenuation color B chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->bloomAttenuationColor = Color4(bloomr, bloomg, bloomb);
				}
				break;
			case NOX_BIN_FINAL_GLARE_ON:
				{
					unsigned short glareon;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(glareon))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post glare enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->glareEnabled = (glareon!=0);
				}
				break;
			case NOX_BIN_FINAL_GLARE_POWER:
				{
					float glarepower;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(glarepower))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post glare power chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->glarePower = glarepower;
				}
				break;
			case NOX_BIN_FINAL_GLARE_DISPERSION:
				{
					float glaredisp;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(glaredisp))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post glare dispersion chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->glareDispersion = glaredisp;
				}
				break;
			case NOX_BIN_FINAL_GLARE_THRESHOLD:
				{
					float glarethres;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(glarethres))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post glare threshold chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->glareThreshold = glarethres;
				}
				break;
			case NOX_BIN_FINAL_GLARE_AREA:
				{
					unsigned int glarearea;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(glarearea))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post glare area chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->glareArea = glarearea;
				}
				break;
			case NOX_BIN_FINAL_GLARE_SHAPE_ON:
				{
					unsigned short glareshapeon;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(glareshapeon))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post glare tex shape enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->glareTextureApertureOn = (glareshapeon!=0);
				}
				break;
			case NOX_BIN_FINAL_GLARE_OBSTACLE_ON:
				{
					unsigned short glareobson;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(glareobson))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post glare tex obstacle enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->glareTextureObstacleOn = (glareobson!=0);
				}
				break;
			case NOX_BIN_FINAL_GLARE_SHAPE_TEX:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading glare shape texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					char * tex_name = (char *)malloc(tLen+1);
					if (!tex_name)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(tex_name, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(tex_name);
						return false;
					}
					tex_name[tLen] = 0;
					if (texDiaph)
						*texDiaph = tex_name;
				}
				break;
			case NOX_BIN_FINAL_GLARE_OBSTACLE_TEX:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading glare obstacle texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					char * tex_name = (char *)malloc(tLen+1);
					if (!tex_name)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(tex_name, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(tex_name);
						return false;
					}
					tex_name[tLen] = 0;
					if (texObst)
						*texObst = tex_name;
				}
				break;
			case NOX_BIN_FINAL_DOTS_ON:
				{
					unsigned short dotson;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(dotson))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post hot pixels remover enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->dotsEnabled = (dotson!=0);
				}
				break;
			case NOX_BIN_FINAL_DOTS_RADIUS:
				{
					unsigned int dotsrad;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(dotsrad))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post hot pixels remover radius chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->dotsRadius = dotsrad;
				}
				break;
			case NOX_BIN_FINAL_DOTS_THRESHOLD:
				{
					float dotsthres;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(dotsthres))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post hot pixels remover threshold chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->dotsThreshold = dotsthres;
				}
				break;
			case NOX_BIN_FINAL_DOTS_DENSITY:
				{
					unsigned int dotsdens;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(dotsdens))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post hot pixels remover density chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->dotsDensity = dotsdens;
				}
				break;
			case NOX_BIN_FINAL_GRAIN:
				{
					float grain;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(grain))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post grain chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->grain = grain;
				}
				break;
			case NOX_BIN_FINAL_VIGNETTE:
				{
					unsigned int vignette;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(vignette))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post vignette chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->vignette = vignette;
				}
				break;
			case NOX_BIN_FINAL_FOG_ENABLED:
				{
					unsigned short fogon;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(fogon))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final fog enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->fogEnabled = (fogon!=0);
				}
				break;
			case NOX_BIN_FINAL_FOG_EXCLUDE_BG:
				{
					unsigned short fogexcl;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(fogexcl))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final fog exclude bg chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->fogExclSky = (fogexcl!=0);
				}
				break;
			case NOX_BIN_FINAL_FOG_DISTANCE:
				{
					float val;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(val))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post fog distance chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->fogDist = val;
				}
				break;
			case NOX_BIN_FINAL_FOG_DENSITY:
				{
					float val;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(val))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final post fog density chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->fogSpeed = val;
				}
				break;
			case NOX_BIN_FINAL_FOG_COLOR:
				{
					float cr, cg, cb;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(cr))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final fog color R chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (1 != FREAD_FLOAT(cg))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final fog color G chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (1 != FREAD_FLOAT(cb))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera final fog color B chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					fm->fogColor = Color4(cr,cg,cb);
				}
				break;

			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Camera Final Post part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;


}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseCameraBuffersChunk(Camera * cam)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	if (!cam)
	{
		MessageBox(0, "Error. Tried to parse buffers for NULL camera.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->notifyProgress("Loading rendered image...", 0.0f);

	unsigned long long cameraBufsStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long camBufsSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(camBufsSize))
	{
		sprintf_s(errorInfo, 256, "Error reading camera buffers size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + camBufsSize - 10;

	unsigned int bwidth = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(bwidth))
	{
		sprintf_s(errorInfo, 256, "Error reading camera buffers width at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	unsigned int bheight = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(bheight))
	{
		sprintf_s(errorInfo, 256, "Error reading camera buffers height at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	unsigned long long gi_hits = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(gi_hits))
	{
		sprintf_s(errorInfo, 256, "Error reading camera buffers GI hits at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	char test[64];
	sprintf_s(test, 64, "GI hits: %lld", gi_hits);
	Logger::add(test);

	cam->staticRegion.overallHits = gi_hits;

	unsigned int rend_time = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(rend_time))
	{
		sprintf_s(errorInfo, 256, "Error reading camera rendering time at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	unsigned int halton = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(halton))
	{
		sprintf_s(errorInfo, 256, "Error reading camera Halton seed at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	cam->maxHalton = halton;
	cam->rendTime = rend_time;

	int bbcounter = 0;
	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		unsigned short nextChunk;
		tPos = _ftelli64(file);
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Camera Buffers part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_CAM_BUFFERS_GI_TYPE:
				{
					unsigned int uival = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(uival))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera gi buffers type at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					cam->gi_type = uival;
				}
				break;
			case NOX_BIN_CAM_BUFFER:
				{
					rtr->curScenePtr->notifyProgress("Loading rendered image...", (bbcounter++)*100.0f/32.0f);

					unsigned long long cam_buf_size = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_ULONG8(cam_buf_size))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera buffer size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					unsigned short cam_buf_type = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_buf_type))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera buffer type at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (cam_buf_type > 1)
					{
						sprintf_s(errorInfo, 256, "Error. Camera buffer type at %lld has wrong value (%d).", tPos, cam_buf_type);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					unsigned short cam_buf_layer = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(cam_buf_layer))
					{
						sprintf_s(errorInfo, 256, "Error while reading camera buffer layer at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (cam_buf_layer > 15)
					{
						sprintf_s(errorInfo, 256, "Error. Camera buffer layer at %lld has wrong value (%d).", tPos, cam_buf_layer);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					ImageBuffer * imgbuf;
					if (cam_buf_type == 0)
						imgbuf  = &(cam->blendBuffDirect[cam_buf_layer]); 
					else
						imgbuf  = &(cam->blendBuffGI[cam_buf_layer]); 

					bool buf_ok = imgbuf->allocBuffer(bwidth, bheight);
					if (!buf_ok)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate memory for camera buffer (%d x %d).", bwidth, bheight);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					#define READ_CONST_CHUNKS
					#ifdef READ_CONST_CHUNKS
						unsigned int filechunksize = 524288;
						unsigned long long bcsize = bwidth * bheight * sizeof(Color4);
						unsigned char * buf = (unsigned char *)malloc(bcsize);
						DWORD t1 = GetTickCount();
						bool ok = loadBuffer(buf, bcsize, filechunksize);
						DWORD t2 = GetTickCount();
						if (!ok)
						{
							free(buf);
							sprintf_s(errorInfo, 256, "Error. Can't allocate temporary memory for camera buffer (%d x %d).", bwidth, bheight);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						unsigned int bwsize = bwidth * sizeof(Color4);
						for (unsigned int y=0; y<bheight; y++)
						{
							tPos = _ftelli64(file);
							memcpy(imgbuf->imgBuf[y], buf+(bwsize*y), bwsize);
						}
						free(buf);

						unsigned long long bhsize = bwidth * bheight * sizeof(int);
						unsigned char * buf2 = (unsigned char *)malloc(bhsize);
						DWORD t3 = GetTickCount();
						bool ok2 = loadBuffer(buf2, bhsize, filechunksize);
						DWORD t4 = GetTickCount();
						if (!ok2)
						{
							free(buf2);
							sprintf_s(errorInfo, 256, "Error. Can't allocate temporary memory for camera hits buffer (%d x %d).", bwidth, bheight);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
						unsigned int bwhsize = bwidth * sizeof(int);
						for (unsigned int y=0; y<bheight; y++)
						{
							tPos = _ftelli64(file);
							memcpy(imgbuf->hitsBuf[y], buf2+(bwhsize*y), bwhsize);
						}
						free(buf2);

						char tbuf[512];
						sprintf_s(tbuf, 512, "Reading buffer %lld MB in chunks of %d KB took %d ms, avg speed %d MB/s",
							(bcsize+bhsize)/1024/1024, filechunksize/1024, (t4-t3+t2-t1), (bcsize+bhsize)/1024/1024*1000/(t4-t3+t2-t1)	);
						Logger::add(tbuf);
					#else
						// OLD WAY, LINE BY LINE
						DWORD t1 = GetTickCount();
						unsigned int bwsize = bwidth * sizeof(Color4);
						for (unsigned int y=0; y<bheight; y++)
						{
							tPos = _ftelli64(file);
							if (1 != fread(imgbuf->imgBuf[y], bwsize, 1, file))
							{
								sprintf_s(errorInfo, 256, "Error while reading camera buffer at %lld.", tPos);
								MessageBox(0, errorInfo, "Error", 0);
								return false;
							}
						}
					
						unsigned int bwhsize = bwidth * sizeof(int);
						for (unsigned int y=0; y<bheight; y++)
						{
							tPos = _ftelli64(file);
							if (1 != fread(imgbuf->hitsBuf[y], bwhsize, 1, file))
							{
								sprintf_s(errorInfo, 256, "Error while reading camera hits buffer at %lld.", tPos);
								MessageBox(0, errorInfo, "Error", 0);
								return false;
							}
						}
						
						DWORD t2 = GetTickCount();
						char tbuf[512];
						sprintf_s(tbuf, 512, "Reading buffer %lld MB as lines (%d and %d bytes) took %d ms, avg speed %d MB/s",
							(bwsize+bwhsize)*bheight/1024/1024, bwsize, bwhsize, (t2-t1), (bwsize+bwhsize)*bheight/1024/1024*1000/(t2-t1)	);
						Logger::add(tbuf);

					#endif

					#ifdef LOG_NOX_PARSING
						char logtext[512];
						sprintf_s(logtext, 512, "    parsing camera %s buffer at layer %d", (cam_buf_type==0 ? "direct" : "GI"), (cam_buf_layer+1));
						Logger::add(logtext);
					#endif

					cam->blendBits |= (1<<cam_buf_layer);
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Camera buffers part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	#ifdef LOG_NOX_PARSING
		char logtext[512];
		sprintf_s(logtext, 512, "    parsing camera buffers OK");
		Logger::add(logtext);
	#endif
	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::loadBuffer(void * buf, unsigned long long bsize, unsigned int chunksize)
{
	if (!file)
		return false;
	if (!buf)
		return false;
	if (bsize<1)
		return false;
	if (chunksize<1)
		return false;

	unsigned long long rest = bsize;
	unsigned char * buf2 = (unsigned char *)buf;
	while (rest>0)
	{
		unsigned int ch = min(chunksize, (unsigned int)(min((unsigned long long)0xffffffff, rest)));
		if (1 != fread(buf2, ch, 1, file))
			return false;
		buf2 += ch;
		rest -= ch;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseCameraDepthBufChunk(Camera * cam)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	if (!cam)
	{
		MessageBox(0, "Error. Tried to parse depth buffer for NULL camera.", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->notifyProgress("Loading depth buffer...", 0.0f);

	unsigned long long cameraDBufStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long camDBufSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(camDBufSize))
	{
		sprintf_s(errorInfo, 256, "Error reading camera depth buffer size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + camDBufSize - 10;

	unsigned int bwidth = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(bwidth))
	{
		sprintf_s(errorInfo, 256, "Error reading camera depth buffer width at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	unsigned int bheight = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(bheight))
	{
		sprintf_s(errorInfo, 256, "Error reading camera depth buffer height at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long ssize = bwidth*bheight*(sizeof(float)+sizeof(unsigned int))+18;
	if (ssize!=camDBufSize)
	{
		sprintf_s(errorInfo, 256, "Error reading camera depth buffer. Chunk size %lld instead of %lld.", camDBufSize, ssize);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	if (cam->depthBuf)
	{
		cam->depthBuf->freeBuffer();
		delete cam->depthBuf;
		cam->depthBuf = NULL;
	}

	cam->depthBuf = new FloatBuffer();
	if (!cam->depthBuf)
	{
		MessageBox(0, "Error allocating memory.", "Error", 0);
		return false;
	}

	bool allOK = cam->depthBuf->allocBuffer(bwidth, bheight, true, true);
	if (!allOK)
	{
		MessageBox(0, "Error allocating memory for depth buffer.", "Error", 0);
		return false;
	}

	unsigned int bwsize = bwidth * sizeof(float);
	for (unsigned int y=0; y<bheight; y++)
	{
		tPos = _ftelli64(file);
		if (1 != fread(cam->depthBuf->fbuf[y], bwsize, 1, file))
		{
			sprintf_s(errorInfo, 256, "Error while reading camera depth buffer at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}
	}

	bwsize = bwidth * sizeof(unsigned int);
	for (unsigned int y=0; y<bheight; y++)
	{
		tPos = _ftelli64(file);
		if (1 != fread(cam->depthBuf->hbuf[y], bwsize, 1, file))
		{
			sprintf_s(errorInfo, 256, "Error while reading camera depth buffer at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}
	}

	currentPos = _ftelli64(file);
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Camera depth buffer part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseCameraNameChunk(Camera * cam)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long camnameStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned int camNameSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(camNameSize))
	{
		sprintf_s(errorInfo, 256, "Error reading camera name size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	camNameSize -= 6;
	
	char * camName = (char *)malloc(camNameSize+1);
	if (!camName)
	{
		sprintf_s(errorInfo, 256, "Error allocating %d bytes for camera name. Size taken from %lld.", (camNameSize+1), tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	tPos = _ftelli64(file);
	if (1!=FREAD_STRING(camName, camNameSize))
	{
		sprintf_s(errorInfo, 256, "Error reading camera name at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		free(camName);
		return false;
	}

	camName[camNameSize] = 0;

	cam->assignName(camName);

	#ifdef LOG_NOX_PARSING
		char logtext[512];
		sprintf_s(logtext, 512, "    camera name: %s", camName);
		Logger::add(logtext);
	#endif

	free(camName);

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseMaterialsChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long matsStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long matsSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(matsSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Materials chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	Logger::add("  Parsing materials");

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + matsSize - 10;

	unsigned int numMats = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(numMats))
	{
		sprintf_s(errorInfo, 256, "Error while reading number of materials at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	unsigned int cmatnumber = 0;
	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Materials part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MATERIAL:
				{
					rtr->curScenePtr->notifyProgress("Loading materials...", (cmatnumber++/(float)numMats*100));
					if (!parseMaterialChunk())
					{
						return false;
					}
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Materials part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	Raytracer::getInstance()->curScenePtr->mats.createArray();

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseMaterialChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	unsigned long long matStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long matSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(matSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Material chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + matSize - 10;

	unsigned int matID = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(matID))
	{
		sprintf_s(errorInfo, 256, "Error while reading material ID at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	unsigned int blendLayer = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(blendLayer))
	{
		sprintf_s(errorInfo, 256, "Error while reading material blend layer at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned int numLayers = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(numLayers))
	{
		sprintf_s(errorInfo, 256, "Error while reading number of material layers at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	MaterialNox * mat = new MaterialNox();
	mat->id = matID;
	mat->hasEmitters = false;
	mat->hasShade = false;
	mat->hasFakeGlass = false;
	mat->hasInvisibleEmitter = false;
	mat->hasWeightTexShade = false;
	mat->isSkyPortal = false;
	mat->isMatteShadow = false;
	unsigned int nLayers = 0;
	mat->blendIndex = blendLayer;
	char * tex_opacity = NULL;


	#ifdef LOG_NOX_PARSING
		Logger::add("    Parsing new material");
		char logtext[512];
		sprintf_s(logtext, 512, "      mat ID: %d", matID);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      blend layer: %d", (blendLayer+1));
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      num layers: %d", numLayers);
		Logger::add(logtext);
	#endif


	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Materials part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MATLAYER:
				{
					nLayers++;
					if (!parseMatLayerChunk(mat))
					{
						return false;
					}
				}
				break;
			case NOX_BIN_MATNAME:
				{
					if (!parseMaterialNameChunk(mat))
					{
						return false;
					}
				}
				break;
			case NOX_BIN_MAT_PREVIEW:
				{
					if (!parseMaterialPreviewChunk(mat))
					{
						return false;
					}
				}
				break;
			case NOX_BIN_MAT_SKYPORTAL:
				{
					unsigned short skyportal = 0;
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(skyportal))
					{
						sprintf_s(errorInfo, 256, "Error while reading material skyportal at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->isSkyPortal = (skyportal!=0);
				}
				break;
			case NOX_BIN_MAT_MATTE_SHADOW:
				{
					unsigned short matte = 0;
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(matte))
					{
						sprintf_s(errorInfo, 256, "Error while reading material matte shadow at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->isMatteShadow = (matte!=0);
				}
				break;
			case NOX_BIN_MAT_DISPLACEMENT_DEPTH:
				{
					float fff = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(fff))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material displacement depth at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->displ_depth = min(NOX_MAT_DISP_DEPTH_MAX, max(NOX_MAT_DISP_DEPTH_MIN, fff));
				}
				break;
			case NOX_BIN_MAT_DISPLACEMENT_SHIFT:
				{
					float fff = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(fff))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material displacement shift at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->displ_shift = min(NOX_MAT_DISP_SHIFT_MAX, max(NOX_MAT_DISP_SHIFT_MIN, fff));
				}
				break;
			case NOX_BIN_MAT_DISPLACEMENT_SUBDIVS:
				{
					unsigned short usd = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(usd))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material displacement subdivs at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->displ_subdivs_lvl = usd;
				}
				break;
			case NOX_BIN_MAT_DISPLACEMENT_NORMAL_MODE:
				{
					unsigned int usd = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(usd))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material displacement normal mode at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->displ_normal_mode = usd;
				}
				break;
			case NOX_BIN_MAT_DISPLACEMENT_CONTINUITY:
				{
					unsigned short cnt = 0;
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(cnt))
					{
						sprintf_s(errorInfo, 256, "Error while reading material displacement continuity at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->displ_continuity = (cnt!=0);
				}
				break;
			case NOX_BIN_MAT_DISPLACEMENT_IGNORE_SCALE:
				{
					unsigned short cnt = 0;
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(cnt))
					{
						sprintf_s(errorInfo, 256, "Error while reading material displacement ignore scale at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->displ_ignore_scale = (cnt!=0);
				}
				break;

			case NOX_BIN_MAT_OPACITY:
				{
					float fff = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(fff))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material opacity at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->opacity = min(1.0f, max(0.0f, fff));
				}
				break;
			case NOX_BIN_MLAY_TEXTURE:
				{
					if (!parseTextureChunk(mat, NULL))
					{
						return false;
					}
				}
				break;
			case NOX_BIN_MAT_USE_TEX_OPACITY:
				{
					unsigned short usnextChunk = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(usnextChunk))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material opacity use texture at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					mat->use_tex_opacity = (usnextChunk != 0);
				}
				break;
			case NOX_BIN_MAT_TEX_OPACITY:
				{
					unsigned int uinextChunk = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(uinextChunk))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material opacity texture at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					int tLen = uinextChunk-6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					tex_opacity = (char *)malloc(tLen+1);
					if (!tex_opacity)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for material opacity texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(tex_opacity, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of material opacity texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(tex_opacity);
						return false;
					}
					tex_opacity[tLen] = 0;
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Material part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	if (tex_opacity)
	{
		char txt[512];
		sprintf_s(txt, 512, "      Deprecated texture info in loaded file");
		Logger::add(txt);
		sprintf_s(txt, 512, "        Material (%s) should load texture for opacity:", mat->name);
		Logger::add(txt);
		sprintf_s(txt, 512, "        %s", tex_opacity);
		Logger::add(txt);
		sprintf_s(txt, 512, "        Texture was turned %s.", (mat->use_tex_opacity ? "on" : "off"));
		Logger::add(txt);
	}

	#ifdef LOG_NOX_PARSING
		//char logtext[512];
		sprintf_s(logtext, 512, "      opacity: %f", mat->opacity);
		Logger::add(logtext);
	#endif
		
	if (nLayers > 0)
	{
		mat->layers.createArray();
		mat->updateFlags();
		Raytracer::getInstance()->curScenePtr->mats.add(mat);
		Raytracer::getInstance()->curScenePtr->mats.createArray();
	}
	else
	{
		delete mat;
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseMaterialPreviewChunk(MaterialNox * mat)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	unsigned long long matPrevStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long matPrevSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(matPrevSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Material Preview chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	if (matPrevSize < 20)
	{
		sprintf_s(errorInfo, 256, "Error. Material Preview chunk has size %lld (taken from %lld). Not big enough.", matPrevSize, tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + matPrevSize - 10;

	unsigned int pwidth = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(pwidth))
	{
		sprintf_s(errorInfo, 256, "Error while reading material preview width at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned int pheight = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(pheight))
	{
		sprintf_s(errorInfo, 256, "Error while reading material preview height at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned int bufsize = (unsigned int)matPrevSize - 18;
	if (bufsize != (pwidth*pheight*sizeof(COLORREF)))
	{
		sprintf_s(errorInfo, 256, "Error. Data for matierial preview is incorrect.\nBuffer size %d for %dx%d preview.", bufsize, pwidth, pheight);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	COLORREF * bufPrev = (COLORREF *)malloc(bufsize);
	if (!bufPrev)
	{
		sprintf_s(errorInfo, 256, "Error while allocating buffer (size %d) for material preview %dx%d.", bufsize, pwidth, pheight);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	if (1!=fread(bufPrev, bufsize, 1, file))
	{
		free(bufPrev);
		sprintf_s(errorInfo, 256, "Error while reading material preview at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	currentPos = _ftelli64(file);
	if (currentPos != borderPos)
	{
		free(bufPrev);
		sprintf_s(errorInfo, 256, "Error. Material preview part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	if (!mat->preview)
		mat->preview = new ImageByteBuffer();

	bool isBufOK = mat->preview->allocBuffer(pwidth,pheight);
	if (!isBufOK)
	{
		free(bufPrev);
		Logger::add("Loaded material preview, but can't allocate image buffer for it.");
		return true;
	}

	mat->preview->clearBuffer();

	int i = 0;
	for (unsigned int y=0; y<pheight; y++)
	{
		for (unsigned int x=0; x<pwidth; x++)
		{
			mat->preview->imgBuf[y][x] = bufPrev[i];
			i++;
		}
	}

	free(bufPrev);

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseMaterialNameChunk(MaterialNox * mat)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long matnameStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned int matNameSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(matNameSize))
	{
		sprintf_s(errorInfo, 256, "Error reading material name size at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	matNameSize -= 6;
	
	char * matName = (char *)malloc(matNameSize+1);
	if (!matName)
	{
		sprintf_s(errorInfo, 256, "Error allocating %d bytes for material name. Size taken from %lld.", (matNameSize+1), tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	tPos = _ftelli64(file);
	if (1!=FREAD_STRING(matName, matNameSize))
	{
		sprintf_s(errorInfo, 256, "Error reading material name at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		free(matName);
		return false;
	}

	matName[matNameSize] = 0;
	mat->name = matName;

	#ifdef LOG_NOX_PARSING
		char logtext[512];
		sprintf_s(logtext, 512, "      parsed material name: %s", matName);
		Logger::add(logtext);
	#endif


	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseMatLayerChunk(MaterialNox * mat)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long mlayStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	MatLayer tmLayer;
	mat->layers.add(tmLayer, &(MatLayer::runConstructor));
	mat->layers.createArray();
	MatLayer * mLayer = &(mat->layers[mat->layers.objCount-1]);

	unsigned long long mlaySize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(mlaySize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Material Layer chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + mlaySize - 10;

	unsigned short mlay_type = MatLayer::TYPE_SHADE;
	unsigned int mlay_contribution = 100;
	float mlay_ior = 1.56f;
	float mlay_refl0r = 1.0f;
	float mlay_refl0g = 1.0f;
	float mlay_refl0b = 1.0f;
	float mlay_refl0a = 1.0f;
	float mlay_refl90r = 1.0f;
	float mlay_refl90g = 1.0f;
	float mlay_refl90b = 1.0f;
	float mlay_refl90a = 1.0f;
	float mlay_transm_colr = 1.0f;
	float mlay_transm_colg = 1.0f;
	float mlay_transm_colb = 1.0f;
	float mlay_transm_cola = 1.0f;
	float mlay_rough = 1.0f;
	unsigned short mlay_transm_on = 0;
	unsigned short mlay_fake_glass = 0;
	unsigned short mlay_abs_on = 0;
	unsigned short mlay_disp_on = 0;
	unsigned short mlay_conn90 = 1;
	float mlay_abs_colr = 1.0f;
	float mlay_abs_colg = 1.0f;
	float mlay_abs_colb = 1.0f;
	float mlay_abs_cola = 1.0f;
	float mlay_abs_dist = 1.0f;
	float mlay_emiss_colr = 1.0f;
	float mlay_emiss_colg = 1.0f;
	float mlay_emiss_colb = 1.0f;
	float mlay_emiss_cola = 1.0f;
	unsigned short mlay_emiss_invisible = 0;
	float mlay_disp_val = 1.0f;
	unsigned short mlay_sss_on = 0;
	float mlay_sss_density = 0.0f;
	float mlay_sss_dir = 0.0f;
	float mlay_anisotropy = 0.0f;
	float mlay_aniso_angle = 0.0f;
	unsigned int mlay_emiss_temp = 6000;
	float mlay_emiss_power = 100;
	unsigned int mlay_emiss_unit = 1;
	unsigned short mlay_tex_on_refl0  = 0;
	unsigned short mlay_tex_on_refl90 = 0;
	unsigned short mlay_tex_on_rough  = 0;
	unsigned short mlay_tex_on_weight = 0;
	unsigned short mlay_tex_on_light  = 0;
	char * mlay_tex_refl0  = NULL;
	char * mlay_tex_refl90 = NULL;
	char * mlay_tex_rough  = NULL;
	char * mlay_tex_weight = NULL;
	char * mlay_tex_light  = NULL;
	char * ies_file_name = NULL;

	tPos = _ftelli64(file);
	if (1!=FREAD_USHORT(mlay_type))
	{
		sprintf_s(errorInfo, 256, "Error while reading material layer type at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(mlay_contribution))
	{
		sprintf_s(errorInfo, 256, "Error while reading material layer contribution at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Material Layer part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MLAY_FRESNEL:
				{
					unsigned int frsize = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(frsize))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer fresnel size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					unsigned short fnextChunk = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(fnextChunk))
					{
						sprintf_s(errorInfo, 256, "Error while reading next chunk in material layer fresnel at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					if (fnextChunk != NOX_BIN_MLAY_FRES_IOR)
					{
						sprintf_s(errorInfo, 256, "Error. Fresnel IOR instead of %x expected at %lld.", fnextChunk, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_ior))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer IOR chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_REFL0:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl0r))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 0 red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl0g))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 0 green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl0b))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 0 blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl0a))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 0 alpha chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_REFL90:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl90r))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 90 red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl90g))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 90 green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl90b))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 90 blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_refl90a))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 90 alpha chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_CONNECT90:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_conn90))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer refl 0/90 connect chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_ROUGH:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_rough))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer roughness chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TRANSM_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_transm_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer transmission on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TRANSM_COLOR:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_transm_colr))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer transmission red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_transm_colg))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer transmission green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_transm_colb))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer transmission blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_transm_cola))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer transmission alpha chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_FAKEGLASS_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_fake_glass))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer fake glass on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_ABS_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_abs_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer absorption on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_ABS_COLOR:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_abs_colr))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer absorption color red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_abs_colg))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer absorption color green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_abs_colb))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer absorption color blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_abs_cola))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer absorption color alpha chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_ABS_DIST:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_abs_dist))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer absorption distance chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_DISP_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_disp_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer dispersion on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_DISP_VALUE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_disp_val))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer dispersion value chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_EMISS_COLOR:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_emiss_colr))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission color red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_emiss_colg))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission color green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_emiss_colb))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission color blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_emiss_cola))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission color alpha chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_EMISS_TEMP:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(mlay_emiss_temp))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission black body temperature chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_EMISS_POWER:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_emiss_power))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission power chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_EMISS_UNIT:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(mlay_emiss_unit))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission power unit chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_EMISS_INVISIBLE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_emiss_invisible))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer emission invisible chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_EMISS_IES:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading IES size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					ies_file_name = (char *)malloc(tLen+1);
					if (!ies_file_name)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for IES filename.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(ies_file_name, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of IES filename chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(ies_file_name);
						return false;
					}
					ies_file_name[tLen] = 0;
				}
				break;
			case NOX_BIN_MLAY_SSS_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_sss_on))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer SSS on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_SSS_DENSITY:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_sss_density))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer SSS density value chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_SSS_DIRECTION:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_sss_dir))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer SSS collision direction value chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_ANISOTROPY:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_anisotropy))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer anisotropy value chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_ANISOTROPY_ANGLE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(mlay_aniso_angle))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer anisotropy angle value chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_REFL0_TEX_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_tex_on_refl0))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 0 texture on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_REFL90_TEX_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_tex_on_refl90))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 90 texture on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_ROUGH_TEX_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_tex_on_rough))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer roughness texture on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_WEIGHT_TEX_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_tex_on_weight))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer contribution texture on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_LIGHT_TEX_ON:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(mlay_tex_on_light))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer light texture on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_REFL0_TEX:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 0 texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					mlay_tex_refl0 = (char *)malloc(tLen+1);
					if (!mlay_tex_refl0)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for reflection 0 texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(mlay_tex_refl0, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of material layer reflection 0 texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(mlay_tex_refl0);
						return false;
					}
					mlay_tex_refl0[tLen] = 0;
				}
				break;
			case NOX_BIN_MLAY_REFL90_TEX:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer reflection 90 texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					mlay_tex_refl90 = (char *)malloc(tLen+1);
					if (!mlay_tex_refl90)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for reflection 90 texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(mlay_tex_refl90, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of material layer reflection 90 texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(mlay_tex_refl90);
						return false;
					}
					mlay_tex_refl90[tLen] = 0;
				}
				break;
			case NOX_BIN_MLAY_ROUGH_TEX:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer roughness texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					mlay_tex_rough = (char *)malloc(tLen+1);
					if (!mlay_tex_rough)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for roughness texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(mlay_tex_rough, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of material layer roughness texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(mlay_tex_rough);
						return false;
					}
					mlay_tex_rough[tLen] = 0;
				}
				break;
			case NOX_BIN_MLAY_WEIGHT_TEX:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer contribution texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					mlay_tex_weight = (char *)malloc(tLen+1);
					if (!mlay_tex_weight)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for contribution texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(mlay_tex_weight, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of material layer contribution texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(mlay_tex_weight);
						return false;
					}
					mlay_tex_weight[tLen] = 0;
				}
				break;
			case NOX_BIN_MLAY_LIGHT_TEX:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading material layer light texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					mlay_tex_light = (char *)malloc(tLen+1);
					if (!mlay_tex_light)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for light texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(mlay_tex_light, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of material layer light texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(mlay_tex_light);
						return false;
					}
					mlay_tex_light[tLen] = 0;
				}
				break;
			case NOX_BIN_MLAY_TEXTURE:
				{
					if (!parseTextureChunk(mat, mLayer))
					{
						return false;
					}
				}
				break;

			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Material layer part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	mLayer->type = mlay_type;
	mLayer->contribution = mlay_contribution;
	mLayer->fresnel.IOR = mlay_ior;
	mLayer->refl0 =  Color4(mlay_refl0r,  mlay_refl0g,  mlay_refl0b,  mlay_refl0a);
	mLayer->refl90 = Color4(mlay_refl90r, mlay_refl90g, mlay_refl90b, mlay_refl90a);
	mLayer->roughness = mlay_rough;
	mLayer->transmOn = (mlay_transm_on!=0);
	mLayer->transmColor = Color4(mlay_transm_colr, mlay_transm_colg, mlay_transm_colb, mlay_transm_cola);
	mLayer->fakeGlass = (mlay_fake_glass!=0);
	mLayer->absOn = (mlay_abs_on!=0);
	mLayer->absColor = Color4(mlay_abs_colr, mlay_abs_colg, mlay_abs_colb, mlay_abs_cola);
	mLayer->absDist = mlay_abs_dist;
	mLayer->color = Color4(mlay_emiss_colr, mlay_emiss_colg, mlay_emiss_colb, mlay_emiss_cola);
	mLayer->emitterTemperature = mlay_emiss_temp;
	mLayer->power = mlay_emiss_power;
	mLayer->unit = mlay_emiss_unit;
	mLayer->dispersionValue = mlay_disp_val;
	mLayer->dispersionOn = (mlay_disp_on!=0);
	mLayer->sssCollDir = mlay_sss_dir;
	mLayer->sssDens = mlay_sss_density;
	mLayer->sssON = (mlay_sss_on!=0);
	mLayer->connect90 = (mlay_conn90!=0);
	mLayer->anisotropy = mlay_anisotropy;
	mLayer->aniso_angle = mlay_aniso_angle;
	mLayer->emissInvisible = (mlay_emiss_invisible!=0);

	if (ies_file_name)
	{
		if (!mLayer->ies)
			mLayer->ies = new IesNOX();
		mLayer->ies->parseFile(getValidTexPath(ies_file_name));
	}

	if (mlay_tex_refl0)
	{
		char txt[512];
		sprintf_s(txt, 512, "      Deprecated texture info in loaded file");
		Logger::add(txt);
		sprintf_s(txt, 512, "        Material (%s) in layer %d should load texture for reflection 0:", mat->name, (mat->layers.objCount-1));
		Logger::add(txt);
		sprintf_s(txt, 512, "        %s", mlay_tex_refl0);
		Logger::add(txt);
		sprintf_s(txt, 512, "        Texture was turned %s.", (mlay_tex_on_refl0 ? "on" : "off"));
		Logger::add(txt);
	}
	if (mlay_tex_refl90)
	{
		char txt[512];
		sprintf_s(txt, 512, "      Deprecated texture info in loaded file");
		Logger::add(txt);
		sprintf_s(txt, 512, "        Material (%s) in layer %d should load texture for reflection 90:", mat->name, (mat->layers.objCount-1));
		Logger::add(txt);
		sprintf_s(txt, 512, "        %s", mlay_tex_refl90);
		Logger::add(txt);
		sprintf_s(txt, 512, "        Texture was turned %s.", (mlay_tex_on_refl90 ? "on" : "off"));
		Logger::add(txt);
	}
	if (mlay_tex_rough)
	{
		char txt[512];
		sprintf_s(txt, 512, "      Deprecated texture info in loaded file");
		Logger::add(txt);
		sprintf_s(txt, 512, "        Material (%s) in layer %d should load texture for roughness:", mat->name, (mat->layers.objCount-1));
		Logger::add(txt);
		sprintf_s(txt, 512, "        %s", mlay_tex_rough);
		Logger::add(txt);
		sprintf_s(txt, 512, "        Texture was turned %s.", (mlay_tex_on_rough ? "on" : "off"));
		Logger::add(txt);
	}
	if (mlay_tex_weight)
	{
		char txt[512];
		sprintf_s(txt, 512, "      Deprecated texture info in loaded file");
		Logger::add(txt);
		sprintf_s(txt, 512, "        Material (%s) in layer %d should load texture for weight:", mat->name, (mat->layers.objCount-1));
		Logger::add(txt);
		sprintf_s(txt, 512, "        %s", mlay_tex_weight);
		Logger::add(txt);
		sprintf_s(txt, 512, "        Texture was turned %s.", (mlay_tex_on_weight ? "on" : "off"));
		Logger::add(txt);
	}
	if (mlay_tex_light)
	{
		char txt[512];
		sprintf_s(txt, 512, "      Deprecated texture info in loaded file");
		Logger::add(txt);
		sprintf_s(txt, 512, "        Material (%s) in layer %d should load texture for light:", mat->name, (mat->layers.objCount-1));
		Logger::add(txt);
		sprintf_s(txt, 512, "        %s", mlay_tex_light);
		Logger::add(txt);
		sprintf_s(txt, 512, "        Texture was turned %s.", (mlay_tex_on_light ? "on" : "off"));
		Logger::add(txt);
	}

	if (mlay_type == MatLayer::TYPE_SHADE)
		mat->hasShade = true;
	if (mlay_type == MatLayer::TYPE_EMITTER)
		mat->hasEmitters = true;
	if (mlay_type == MatLayer::TYPE_EMITTER  &&  mlay_emiss_invisible)
		mat->hasInvisibleEmitter = true;
	if (mlay_type == MatLayer::TYPE_SHADE   &&   mlay_fake_glass)
		mat->hasFakeGlass = true;
	if (mLayer->use_tex_weight  &&  mLayer->tex_weight.isValid())
		mat->hasWeightTexShade = true;

	#ifdef LOG_NOX_PARSING
		Logger::add("      Parsing new layer");
		char logtext[512];
		sprintf_s(logtext, 512, "        type: %s", 
			((mlay_type==MatLayer::TYPE_EMITTER) ? "emitter" : ((mlay_type==MatLayer::TYPE_SHADE) ? "shade" : "UNKNOWN") ) );
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        contribution: %d", mlay_contribution);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        ior: %f", mlay_ior);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        refl0: %f %f %f %f",  mlay_refl0r,  mlay_refl0g,  mlay_refl0b,  mlay_refl0a);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        refl90: %f %f %f %f", mlay_refl90r, mlay_refl90g, mlay_refl90b, mlay_refl90a );
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        roughness: %f", mlay_rough*100);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        transmission: %s", (mlay_transm_on!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        fake glass: %s", (mlay_fake_glass!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        absorption: %s", (mlay_abs_on!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        absorption color: %f %f %f %f", mlay_abs_colr, mlay_abs_colg, mlay_abs_colb, mlay_abs_cola);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        absorption distance: %f", mlay_abs_dist);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        emission color: %f %f %f %f", mlay_emiss_colr, mlay_emiss_colg, mlay_emiss_colb, mlay_emiss_cola);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        emission temperature: %d", mlay_emiss_temp);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        emission power: %f", mlay_emiss_power);
		Logger::add(logtext);
		char * unit = NULL;
		switch (mlay_emiss_unit)
		{
			case MatLayer::EMISSION_LUMEN:					unit = "lumen"; break;
			case MatLayer::EMISSION_LUX:					unit = "lux"; break;
			case MatLayer::EMISSION_WATT:					unit = "watt"; break;
			case MatLayer::EMISSION_WATT_PER_SQR_METER:		unit = "watt/m2"; break;
			default:										unit = "unknown"; break;
		}
		sprintf_s(logtext, 512, "      emission unit: %s", unit);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        SSS: %s, density %f, direction %f", (mlay_sss_on!=0)?"yes":"no", mlay_sss_density, mlay_sss_dir);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "        anisotropy: %f,   angle: %f", mlay_anisotropy, mlay_aniso_angle);
		Logger::add(logtext);

		sprintf_s(logtext, 512, "      tex refl0 on: %s", (mlay_tex_on_refl0!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex refl0 file: %s", (mlay_tex_refl0?mlay_tex_refl0:"none"));
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex refl90 on: %s", (mlay_tex_on_refl90!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex refl90 file: %s", (mlay_tex_refl90?mlay_tex_refl90:"none"));
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex rough on: %s", (mlay_tex_on_rough!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex rough file: %s", (mlay_tex_rough?mlay_tex_rough:"none"));
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex contribution on: %s", (mlay_tex_on_weight!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex contribution file: %s", (mlay_tex_weight?mlay_tex_weight:"none"));
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex light on: %s", (mlay_tex_on_light!=0)?"yes":"no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "      tex light file: %s", (mlay_tex_light?mlay_tex_light:"none"));
		Logger::add(logtext);
	#endif

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseTextureChunk(MaterialNox * mat, MatLayer * mlay)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long texStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long texSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(texSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Texture chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + texSize - 10;

	unsigned short tex_type = 0;
	unsigned short tex_mlay_slot = 0;

	char * tex_name = NULL;
	unsigned short tex_enabled = 0;
	unsigned short tex_filter = 0;
	unsigned short tex_precalc = 0;

	tPos = _ftelli64(file);
	if (1!=FREAD_USHORT(tex_type))
	{
		sprintf_s(errorInfo, 256, "Error while reading texture type at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	tPos = _ftelli64(file);
	if (1!=FREAD_USHORT(tex_mlay_slot))
	{
		sprintf_s(errorInfo, 256, "Error while reading texture slot in layer at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	switch (tex_type)
	{
		case 0:
			{
				_fseeki64(file, texStart+2, SEEK_SET);
				if (!parseTextureFileChunk(mat, mlay))
				{
					return false;
				}
			}
			break;
		default:
			{
				sprintf_s(errorInfo, 256, "Texture type %d at %lld is not supported.", tex_type, tPos);
				MessageBox(0, errorInfo, "Error", 0);
				return false;
			}
			break;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseTextureFileChunk(MaterialNox * mat, MatLayer * mlay)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long texStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long texSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(texSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Texture chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + texSize - 10;

	unsigned short tex_type = 0;
	unsigned short tex_mlay_slot = 0;

	char * tex_name = NULL;
	unsigned short tex_enabled = 0;
	unsigned short tex_filter = 0;
	unsigned short tex_precalc = 0;


	tPos = _ftelli64(file);
	if (1!=FREAD_USHORT(tex_type))
	{
		sprintf_s(errorInfo, 256, "Error while reading texture type at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
	
	tPos = _ftelli64(file);
	if (1!=FREAD_USHORT(tex_mlay_slot))
	{
		sprintf_s(errorInfo, 256, "Error while reading texture slot in layer at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	TextureInstance * tex_ptr = NULL;
	switch (tex_mlay_slot)
	{
		case LAYER_TEX_SLOT_COLOR0:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_col0);
			tex_ptr->texMod.gamma = 2.2f;
			break;
		case LAYER_TEX_SLOT_COLOR90:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_col90);
			tex_ptr->texMod.gamma = 2.2f;
			break;
		case LAYER_TEX_SLOT_ROUGHNESS:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_rough);
			tex_ptr->texMod.gamma = 1.0f;
			break;
		case LAYER_TEX_SLOT_WEIGHT:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_weight);
			tex_ptr->texMod.gamma = 1.0f;
			break;
		case LAYER_TEX_SLOT_LIGHT:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_light);
			tex_ptr->texMod.gamma = 2.2f;
			break;
		case LAYER_TEX_SLOT_TRANSM:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_transm);
			tex_ptr->texMod.gamma = 2.2f;
			break;
		case LAYER_TEX_SLOT_ANISOTROPY:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_aniso);
			tex_ptr->texMod.gamma = 1.0f;
			break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_aniso_angle);
			tex_ptr->texMod.gamma = 1.0f;
			break;
		case LAYER_TEX_SLOT_NORMAL:
			if (!mlay)
			{
				MessageBox(0, "Material Layer NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mlay->tex_normal);
			tex_ptr->texMod.gamma = 1.0f;
			break;
		case MAT_TEX_SLOT_OPACITY:
			if (!mat)
			{
				MessageBox(0, "Material NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mat->tex_opacity);
			tex_ptr->texMod.gamma = 1.0f;
			break;
		case MAT_TEX_SLOT_DISPLACEMENT:
			if (!mat)
			{
				MessageBox(0, "Material NULL pointer received. Internal error", "Error", 0);
				return false;
			}
			tex_ptr = &(mat->tex_displacement);
			tex_ptr->texMod.gamma = 1.0f;
			break;
		case TEX_SLOT_ENV:
			tex_ptr = &(rtr->curScenePtr->env.tex);
			tex_ptr->texMod.gamma = 2.2f;
			break;
	}

	tex_ptr->texMod.updateGamma(tex_ptr->texMod.gamma);

	if (!tex_ptr)
	{
		sprintf_s(errorInfo, 256, "Texture slot %d taken from %lld is not supported.", tex_mlay_slot, tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk id in Texture part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MLAY_TEX_ENABLED:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(tex_enabled))
					{
						sprintf_s(errorInfo, 256, "Error reading texture enabled chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_FILTER:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(tex_filter))
					{
						sprintf_s(errorInfo, 256, "Error reading texture filter chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_PRECALCULATE:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(tex_precalc))
					{
						sprintf_s(errorInfo, 256, "Error reading texture precalculate chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_FILENAME:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture name size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					tex_name = (char *)malloc(tLen+1);
					if (!tex_name)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for texture name.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(tex_name, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of texture name chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(tex_name);
						return false;
					}
					tex_name[tLen] = 0;
				}
				break;
			case NOX_BIN_MLAY_TEX_POST:
				{
					if (!parseTextPostChunk(&(tex_ptr->texMod)))
					{
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_NORM_POST:
				{
					if (!parseNormPostChunk(&(tex_ptr->normMod)))
					{
						return false;
					}
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Texture part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	switch (tex_mlay_slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			mlay->use_tex_col0				= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_COLOR90:		mlay->use_tex_col90				= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_ROUGHNESS:		mlay->use_tex_rough				= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_WEIGHT:			mlay->use_tex_weight			= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_LIGHT:			mlay->use_tex_light				= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_NORMAL:			mlay->use_tex_normal			= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_TRANSM:			mlay->use_tex_transm			= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_ANISOTROPY:		mlay->use_tex_aniso				= (tex_enabled != 0); break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:	mlay->use_tex_aniso_angle		= (tex_enabled != 0); break;
		case MAT_TEX_SLOT_OPACITY:			mat->use_tex_opacity			= (tex_enabled != 0); break;
		case MAT_TEX_SLOT_DISPLACEMENT:		mat->use_tex_displacement		= (tex_enabled != 0); break;
		case TEX_SLOT_ENV:					rtr->curScenePtr->env.enabled	= (tex_enabled != 0); break;
	}

	if (tex_name)
	{
		char * texpath = getValidTexPath(tex_name);
		Raytracer * rtr = Raytracer::getInstance();
		int id = rtr->curScenePtr->texManager.addTexture(texpath, true, NULL, (LAYER_TEX_SLOT_NORMAL==tex_mlay_slot));
		tex_ptr->managerID = id;
		if (id>-1)
		{
			Texture * tex = rtr->curScenePtr->texManager.textures[id];
			tex_ptr->filename = copyString(tex->filename);
			tex_ptr->fullfilename = copyString(tex->fullfilename);
			tex_ptr->texScPtr = tex;
		}
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseTextPostChunk(TexModifer * texmod)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	if (!texmod)
	{
		MessageBox(0, "Texture Modifier NULL pointer received. Internal error", "Error", 0);
		return false;
	}


	unsigned long long postStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long postSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(postSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Texture Modifier chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + postSize - 6;
	
	float post_brightness = TEXMOD_DEF_BRIGHTNESS;
	float post_contrast = TEXMOD_DEF_CONTRAST;
	float post_saturation = TEXMOD_DEF_SATURATION;
	float post_hue = TEXMOD_DEF_HUE;
	float post_red = TEXMOD_DEF_RED;
	float post_green = TEXMOD_DEF_GREEN;
	float post_blue = TEXMOD_DEF_BLUE;
	float post_gamma = TEXMOD_DEF_GAMMA;
	unsigned short post_invert = TEXMOD_DEF_INVERT ? 1 : 0;
	unsigned short post_interpolate = TEXMOD_DEF_INTERPOLATE ? 1 : 0;


	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk id in Texture Modifier part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MLAY_TEX_POST_BRIGHTNESS:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_brightness))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post brightness chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_CONTRAST:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_contrast))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post contrast chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_SATURATION:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_saturation))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post saturation chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_HUE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_hue))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post hue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_RED:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_red))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post red chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_GREEN:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_green))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post green chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_BLUE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_blue))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post blue chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_GAMMA:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_gamma))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post gamma chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_INVERT:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(post_invert))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post invert chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_TEX_POST_INTERPOLATE:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(post_interpolate))
					{
						sprintf_s(errorInfo, 256, "Error while reading texture post interpolate chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Texture Modifier part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	texmod->brightness = post_brightness;
	texmod->contrast = post_contrast;
	texmod->saturation = post_saturation;
	texmod->hue = post_hue;
	texmod->red = post_red;
	texmod->green = post_green;
	texmod->blue = post_blue;
	texmod->gamma = post_gamma;
	texmod->updateGamma(post_gamma);
	texmod->invert = (post_invert!=0);
	texmod->interpolateProbe = (post_interpolate!=0);

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseNormPostChunk(NormalModifier * normmod)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	if (!normmod)
	{
		MessageBox(0, "Normal Modifier NULL pointer received. Internal error", "Error", 0);
		return false;
	}


	unsigned long long postStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long postSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_UINT(postSize))
	{
		sprintf_s(errorInfo, 256, "Error while reading size of Normal Modifier chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + postSize - 6;
	
	unsigned short post_inv_x = false;
	unsigned short post_inv_y = false;
	float post_power = 0.0f;

	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk id in Normal Modifier part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_MLAY_NORM_POST_POWER_EV:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_FLOAT(post_power))
					{
						sprintf_s(errorInfo, 256, "Error while reading normal post power chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_NORM_POST_INVERT_X:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(post_inv_x))
					{
						sprintf_s(errorInfo, 256, "Error while reading normal post invert x chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_MLAY_NORM_POST_INVERT_Y:
				{
					tPos = _ftelli64(file);
					if (1 != FREAD_USHORT(post_inv_y))
					{
						sprintf_s(errorInfo, 256, "Error while reading normal post invert y chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;

			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Normal Modifier part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	normmod->powerEV = post_power;
	normmod->power = pow(2.0f, post_power);
	normmod->invertX = (post_inv_x!=0);
	normmod->invertY = (post_inv_y!=0);

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseSunskyChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long sskyStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long sskySize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(sskySize))
	{
		sprintf_s(errorInfo, 256, "Error reading Sunsky size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + sskySize - 10;

	unsigned short nss_on = 0;
	unsigned short nss_month = 1;
	unsigned short nss_day = 1;
	unsigned short nss_hour = 12;
	unsigned short nss_minute = 0;
	unsigned short nss_model = 0;
	short nss_gmt = 0;
	unsigned short nss_sun_on = 1;
	unsigned short nss_sun_other_layer = 1;
	float nss_longitude = 0;
	float nss_latitude = 0;
	float nss_turbidity = 2;
	float nss_aerosol = 1;
	float nss_sunsize = 0.5333f;
	float nss_dirNx = 0;
	float nss_dirNy = 0;
	float nss_dirNz = -1;
	float nss_dirEx = 1;
	float nss_dirEy = 0;
	float nss_dirEz = 0;
	float nss_albedo = 0.5f;
	
	currentPos = _ftelli64(file);
	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Sunsky part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_SS_ON:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_on))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_MONTH:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_month))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky month chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_DAY:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_day))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky day chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_HOUR:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_hour))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky hour chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_MINUTE:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_minute))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky minute chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_GMT:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT((unsigned short &)nss_gmt))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky gmt chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_SUN_ON:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_sun_on))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky sun on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_SUN_OTHER_LAYER:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_sun_other_layer))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky sun on second blend layer on chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_LONGITUDE:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_longitude))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky longitude chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_LATITUDE:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_latitude))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky latitude chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_TURBIDITY:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_turbidity))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky turbidity chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_AEROSOL:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_aerosol))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky aerosol chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_SUN_SIZE:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_sunsize))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky sun size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_DIRECTIONS:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_dirNx))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky direction north x chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_dirNy))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky direction north y chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_dirNz))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky direction north z chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_dirEx))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky direction east x chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_dirEy))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky direction east y chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_dirEz))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky direction east z chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_MODEL:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(nss_model))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky model chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			case NOX_BIN_SS_ALBEDO:
				{
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(nss_albedo))
					{
						sprintf_s(errorInfo, 256, "Error reading sunsky albedo chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}
	
	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Sunsky part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}
		
	Raytracer * rtr = Raytracer::getInstance();

	Vector3d dirN = Vector3d(nss_dirNx, nss_dirNy, nss_dirNz);
	Vector3d dirE = Vector3d(nss_dirEx, nss_dirEy, nss_dirEz);
	Vector3d dirU = dirE ^ dirN;
	dirE = dirN ^ dirU;
	rtr->curScenePtr->sscene.sunsky->dirN =  dirN;
	rtr->curScenePtr->sscene.sunsky->dirS = -dirN;
	rtr->curScenePtr->sscene.sunsky->dirE =  dirE;
	rtr->curScenePtr->sscene.sunsky->dirW = -dirE;
	rtr->curScenePtr->sscene.sunsky->dirU =  dirU;

	rtr->curScenePtr->sscene.useSunSky = (nss_on!=0);
	rtr->curScenePtr->sscene.sunsky->aerosol = nss_aerosol;
	rtr->curScenePtr->sscene.sunsky->turbidity = nss_turbidity;
	rtr->curScenePtr->sscene.sunsky->setStandardMeridianForTimeZone(-nss_gmt*PI/12);
	rtr->curScenePtr->sscene.sunsky->setPosition(nss_longitude, nss_latitude);
	rtr->curScenePtr->sscene.sunsky->setDate(nss_month, nss_day, nss_hour, (float)nss_minute);
	
	rtr->curScenePtr->sscene.sunsky->sunOn = (nss_sun_on!=0);
	rtr->curScenePtr->sscene.sunsky->sunSize = nss_sunsize;
	rtr->curScenePtr->sscene.sunsky->sunOtherBlendLayer = (nss_sun_other_layer!=0);


	rtr->curScenePtr->sscene.sunsky->albedo = nss_albedo;
	rtr->curScenePtr->sscene.sunsky->use_hosek = (nss_model!=0);
	rtr->curScenePtr->sscene.sunsky->updateHosekData();

	#ifdef LOG_NOX_PARSING
		Logger::add("  Parsing sunsky");
		char logtext[512];
		sprintf_s(logtext, 512, "    sunsky on: %s", (nss_on!=0) ? "yes" : "no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    model: %s", nss_model ? "Hosek-Wilkie" : "Preetham");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    turbidity: %f", nss_turbidity);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    aerosol: %f", nss_aerosol);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    albedo: %f", nss_albedo);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    gmt: %d", nss_gmt);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    date: %d-%d %d:%d", nss_month, nss_day, nss_hour, nss_minute);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    longitude: %f   (%f)", -180.0f/PI*nss_longitude, nss_longitude);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    latitude:  %f   (%f)", -180.0f/PI*nss_latitude, nss_latitude);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    sun on: %s", (nss_sun_on!=0) ? "yes" : "no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    sun other blend layer: %s", (nss_sun_other_layer!=0) ? "yes" : "no");
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    sun size: %f", nss_sunsize);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    dir N: %f %f %f", nss_dirNx, nss_dirNy, nss_dirNz);
		Logger::add(logtext);
		sprintf_s(logtext, 512, "    dir E: %f %f %f", nss_dirEx, nss_dirEy, nss_dirEz);
		Logger::add(logtext);
	
	#endif

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseBlendsChunk(BlendSettings * blend)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}
	if (!blend)
	{
		MessageBox(0, "Blend layers object is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long blendsStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long blendsSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(blendsSize))
	{
		sprintf_s(errorInfo, 256, "Error reading Blend layers size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + blendsSize - 10;

	unsigned int current_id = 0;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in User Colors part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_BLEND_LAYER:
				{
					unsigned long blsize;
					tPos = _ftelli64(file);
					if (1!=FREAD_ULONG8(blsize))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer chunk size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					unsigned int id = 0;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(id))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer id at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					current_id = min(15, max(0, id));
				}
				break;
			case NOX_BIN_BLEND_LAYER_ON:
				{
					unsigned short isON = 1;
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(isON))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer on at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					blend->blendOn[current_id] = (isON!=0);
				}
				break;
			case NOX_BIN_BLEND_LAYER_WEIGHT:
				{
					float weight = 100.0;
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(weight))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer weight at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					blend->weights[current_id] = weight/100.0f;
				}
				break;
			case NOX_BIN_BLEND_LAYER_RED:
				{
					float red = 100.0;
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(red))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer red at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					blend->red[current_id] = red/100.0f;
				}
				break;
			case NOX_BIN_BLEND_LAYER_GREEN:
				{
					float green = 100.0;
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(green))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer green at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					blend->green[current_id] = green/100.0f;
				}
				break;
			case NOX_BIN_BLEND_LAYER_BLUE:
				{
					float blue = 100.0;
					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(blue))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer blue at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					blend->blue[current_id] = blue/100.0f;
				}
				break;
			case NOX_BIN_BLEND_LAYER_NAME:
				{
					unsigned int nsize;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(nsize))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer name size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					if (nsize<7)
					{
						sprintf_s(errorInfo, 256, "Blend layer name size at %lld is too small (%d).", tPos, (nsize-6));
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					char * bname = (char*)malloc(nsize-5);
					if (1!=FREAD_STRING(bname, nsize-6))
					{
						sprintf_s(errorInfo, 256, "Error reading blend layer name at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					bname[nsize-6] = 0;

					blend->setName(current_id, bname);
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Blend layers part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseUsercolorsChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long usercolorsStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long userColorsSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(userColorsSize))
	{
		sprintf_s(errorInfo, 256, "Error reading User Colors size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + userColorsSize - 10;

	unsigned short current_id = 0;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in User Colors part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_USER_SINGLE_COLOR:
				{
					float r=0.0f, g=0.0f, b=0.0f, a=0.0f;
					tPos = _ftelli64(file);
					if (1!=FREAD_USHORT(current_id))
					{
						sprintf_s(errorInfo, 256, "Error reading User Color ID at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					current_id = min(17, max(0, current_id));

					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(r))
					{
						sprintf_s(errorInfo, 256, "Error reading User Color R at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					UserColors::colors[current_id].r = r;

					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(g))
					{
						sprintf_s(errorInfo, 256, "Error reading User Color G at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					UserColors::colors[current_id].g = g;

					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(b))
					{
						sprintf_s(errorInfo, 256, "Error reading User Color B at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					UserColors::colors[current_id].b = b;

					tPos = _ftelli64(file);
					if (1!=FREAD_FLOAT(a))
					{
						sprintf_s(errorInfo, 256, "Error reading User Color A at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					UserColors::colors[current_id].a = a;

				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. User Colors part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseRendererSettingsChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long settingsStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long settingsSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(settingsSize))
	{
		sprintf_s(errorInfo, 256, "Error reading Renderer Settings size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + settingsSize - 10;

	unsigned short current_id = 0;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Renderer Settings part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_RSETT_TIMER_ON_STOPAFTER:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Stop Timer On at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.stoppingTimerOn = (val!=0);
				}
				break;
			case NOX_BIN_RSETT_TIMER_ON_REFRESH:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Refresh Timer On at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.refreshTimeOn = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_TIMER_ON_AUTOSAVE:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Autosave Timer On at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.autosaveTimerOn = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_TIMER_STOPAFTER_HOURS:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Stop Timer Hours at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.stopTimerHours = val;
				}
				break;

			case NOX_BIN_RSETT_TIMER_STOPAFTER_MINUTES:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading  Stop Timer Minutes at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.stopTimerMinutes = val;
				}
				break;

			case NOX_BIN_RSETT_TIMER_STOPAFTER_SECONDS:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading  Stop Timer Seconds at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.stopTimerSeconds = val;
				}
				break;

			case NOX_BIN_RSETT_TIMER_REFRESH_SECONDS:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Refresh Timer at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.refreshTime = val;
				}
				break;

			case NOX_BIN_RSETT_TIMER_AUTOSAVE_MINUTES:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Autosave Timer at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.autosaveTimerMinutes = val;
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_ACTIVE:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Active at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_active = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_CAMERA_CHANGE:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Camera Change at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_nextCam = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_CAMERA_DESCENNDING:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Camera Descending at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_nCamDesc = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_COPY_POST:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Copy Post at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_copyPost = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_DELETE_BUFFERS:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Delete Buffers at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_delBuffs = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_SAVE_IMAGE:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Save Image at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_saveImg = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_SUNSKY_CHANGE:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Sunsky Change at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_changeSunsky = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_SUNSKY_MINUTES:
				{
					unsigned int val;
					if (1!=FREAD_UINT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Run Next Sunsky Minutes at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->gui_timers.rn_sunskyMins = val;
				}
				break;

			case NOX_BIN_RSETT_RUNNEXT_SAVE_IMAGE_FOLDER:
				{
					unsigned int tLen = 0;
					tPos = _ftelli64(file);
					if (1 != FREAD_UINT(tLen))
					{
						sprintf_s(errorInfo, 256, "Error while Run Next Save Image Folder size chunk at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					tLen -=6;
					if (tLen<1)	// zero length string - no tex, but no error
						break;

					char * dir = (char *)malloc(tLen+1);
					if (!dir)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for Next Save Image Folder.", (tLen+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(dir, tLen))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of Next Save Image Folder chunk at %lld.", tLen, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(dir);
						return false;
					}
					dir[tLen] = 0;

					if (rtr->gui_timers.rn_saveFolder)
						free(rtr->gui_timers.rn_saveFolder);
					rtr->gui_timers.rn_saveFolder = dir;
				}
				break;

			case NOX_BIN_RSETT_ENGINE:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Engine at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					switch (val)
					{
						case 1:
							sc->sscene.giMethod = 1;	break;	//pt	
						case 2:
							sc->sscene.giMethod = 2;	break;	//bdpt
						case 3:
							sc->sscene.giMethod = 3;	break;	//mlt
						case 10:
							sc->sscene.giMethod = 10;	break;	//inters
					}
				}
				break;

			case NOX_BIN_RSETT_FILL_PATTERN:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Pixel Fill Pattern at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					sc->sscene.random_type = val;
				}
				break;

			case NOX_BIN_RSETT_BUCKET_SIZE:
				{
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Bucket Size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					sc->sscene.bucket_size_x = val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Bucket Size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					sc->sscene.bucket_size_y = val;
				}
				break;

			case NOX_BIN_RSETT_STOP_AFTER_PASS:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Stop After Pass at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					sc->sscene.stop_after_pass = (val!=0);
				}
				break;

			case NOX_BIN_RSETT_SAMPLES_PER_PIXEL:
				{
					unsigned int val;
					if (1!=FREAD_UINT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Samples Per Pixel at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					sc->sscene.samples_per_pixel_in_pass = val;
				}
				break;

			case NOX_BIN_RSETT_PT_GI_SAMPLES:
				{
					unsigned int val;
					if (1!=FREAD_UINT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading PT GI Samples at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					sc->sscene.pt_gi_samples = val;
				}
				break;
			case NOX_BIN_RSETT_PT_USE_FAKE_CAUSTIC:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading PT Fake Caustic at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					sc->sscene.causticFake = (val!=0);
				}
				break;
			case NOX_BIN_RSETT_PT_GI_DISABLE_SECOND_CAUSTIC:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading PT Disable Secondary Caustic at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					sc->sscene.disableSecondaryCaustic = (val!=0);
				}
				break;
			case NOX_BIN_RSETT_PT_MAX_CAUSTIC_ROUGH:
				{
					float val;
					if (1!=FREAD_FLOAT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading PT Max Caustic Rough at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					Raytracer * rtr = Raytracer::getInstance();
					Scene * sc = rtr->curScenePtr;
					sc->sscene.causticMaxRough = val;
				}
				break;

			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Renderer Settings part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseSceneInfoChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long siStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long sceneInfoSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(sceneInfoSize))
	{
		sprintf_s(errorInfo, 256, "Error reading Scene Info size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + sceneInfoSize - 10;

	Scene * sc = rtr->curScenePtr;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Scene Info part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_SCENEINFO_SCENENAME:
				{
					unsigned int ssize;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(ssize))
					{
						sprintf_s(errorInfo, 256, "Error reading Scene Name length at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					ssize -=6;
					if (ssize<1)
						break;

					char * buf = (char *)malloc(ssize+1);
					if (!buf)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for scene name.", (ssize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(buf, ssize))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of scene name chunk at %lld.", ssize, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(buf);
						return false;
					}
					buf[ssize] = 0;

					if (sc->scene_name)
						free(sc->scene_name);
					sc->scene_name = buf;
					sc->le_sc_name = ssize;
				}
				break;
			case NOX_BIN_SCENEINFO_AUTHOR:
				{
					unsigned int ssize;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(ssize))
					{
						sprintf_s(errorInfo, 256, "Error reading Scene Author length at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					ssize -=6;
					if (ssize<1)
						break;

					char * buf = (char *)malloc(ssize+1);
					if (!buf)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for scene author.", (ssize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(buf, ssize))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of scene author chunk at %lld.", ssize, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(buf);
						return false;
					}
					buf[ssize] = 0;

					if (sc->scene_author_name)
						free(sc->scene_author_name);
					sc->scene_author_name = buf;
					sc->le_sc_author_name = ssize;
				}
				break;
			case NOX_BIN_SCENEINFO_EMAIL:
				{
					unsigned int ssize;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(ssize))
					{
						sprintf_s(errorInfo, 256, "Error reading Scene Author Email length at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					ssize -=6;
					if (ssize<1)
						break;

					char * buf = (char *)malloc(ssize+1);
					if (!buf)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for scene author email.", (ssize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(buf, ssize))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of scene author email chunk at %lld.", ssize, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(buf);
						return false;
					}
					buf[ssize] = 0;

					if (sc->scene_author_email)
						free(sc->scene_author_email);
					sc->scene_author_email = buf;
					sc->le_sc_author_email = ssize;
				}
				break;
			case NOX_BIN_SCENEINFO_WWW:
				{
					unsigned int ssize;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(ssize))
					{
						sprintf_s(errorInfo, 256, "Error reading Scene Author webpage length at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					ssize -=6;
					if (ssize<1)
						break;

					char * buf = (char *)malloc(ssize+1);
					if (!buf)
					{
						sprintf_s(errorInfo, 256, "Error. Can't allocate %d bytes (taken from %lld) for scene author webpage.", (ssize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1 != FREAD_STRING(buf, ssize))
					{
						sprintf_s(errorInfo, 256, "Error while reading %d bytes of scene author webpage chunk at %lld.", ssize, tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(buf);
						return false;
					}
					buf[ssize] = 0;

					if (sc->scene_author_www)
						free(sc->scene_author_www);
					sc->scene_author_www = buf;
					sc->le_sc_author_www = ssize;
				}
				break;

			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}
		currentPos = _ftelli64(file);
	}


	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Scene Info part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseSceneSettingsChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long siStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long sceneSettingsSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(sceneSettingsSize))
	{
		sprintf_s(errorInfo, 256, "Error reading Scene Settings size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + sceneSettingsSize - 10;

	Scene * sc = rtr->curScenePtr;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Scene Info part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_SCENESETT_MOTION_BLUR_ON:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Motion Blur ON at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->curScenePtr->sscene.motion_blur_on = (val!=0);
					if (rtr->curScenePtr->sscene.motion_blur_on)
						Logger::add("    motion blur enabled");
					else
						Logger::add("    motion blur disabled");
				}
				break;
			case NOX_BIN_SCENESETT_MOTION_BLUR_TIME:
				{
					float val;
					if (1!=FREAD_FLOAT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Motion Blur Time at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->curScenePtr->sscene.motion_blur_time = val;
					char logtext[512];
					sprintf_s(logtext, 512, "    motion blur time: %.3f", val);
					Logger::add(logtext);
				}
				break;
			case NOX_BIN_SCENESETT_MOTION_BLUR_MAX_TIME:
				{
					float val;
					if (1!=FREAD_FLOAT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Motion Blur Max Time at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->curScenePtr->sscene.motion_blur_max_time = val;
					char logtext[512];
					sprintf_s(logtext, 512, "    motion blur max time: %.3f", val);
					Logger::add(logtext);
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}
		currentPos = _ftelli64(file);
	}

	if (rtr->curScenePtr->sscene.motion_blur_time > rtr->curScenePtr->sscene.motion_blur_max_time)
		rtr->curScenePtr->sscene.motion_blur_time = rtr->curScenePtr->sscene.motion_blur_max_time;

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Scene Settings part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseEnvironmentMapChunk()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long envStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long envSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(envSize))
	{
		sprintf_s(errorInfo, 256, "Error reading Environment Map size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + envSize - 10;

	unsigned short current_id = 0;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in Environment Map part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_ENV_ENABLED:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Environment Map Enabled at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->curScenePtr->env.enabled = (val!=0);
				}
				break;
			case NOX_BIN_ENV_BLEND_LAYER:
				{
					unsigned short val;
					if (1!=FREAD_USHORT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Environment Map Blend Layer at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->curScenePtr->env.blendlayer = val;
				}
				break;
			case NOX_BIN_ENV_POWER_EV:
				{
					float val;
					if (1!=FREAD_FLOAT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Environment Map Power EV at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->curScenePtr->env.powerEV = val;
				}
				break;
			case NOX_BIN_ENV_AZIMUTH:
				{
					float val;
					if (1!=FREAD_FLOAT(val))
					{
						sprintf_s(errorInfo, 256, "Error reading Environment Map Azimuth at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					rtr->curScenePtr->env.azimuth_shift = val;
				}
				break;
			case NOX_BIN_ENV_DIRECTIONS:
				{
					float val[6];
					for (int i=0; i<6; i++)
					{
						if (1!=FREAD_FLOAT(val[i]))
						{
							sprintf_s(errorInfo, 256, "Error reading Environment Map Directions at %lld.", tPos);
							MessageBox(0, errorInfo, "Error", 0);
							return false;
						}
					}
					Vector3d dirN = Vector3d(val[0], val[1], val[2]);
					Vector3d dirE = Vector3d(val[3], val[4], val[5]);
					dirN.normalize();
					dirE.normalize();
					Vector3d dirU = dirE ^ dirN;
					dirU.normalize();
					dirE = dirN ^ dirU;
					dirE.normalize();
					rtr->curScenePtr->env.dirN = dirN;
					rtr->curScenePtr->env.dirE = dirE;
					rtr->curScenePtr->env.dirU = dirU;
					rtr->curScenePtr->env.updateAzimuthDirections();
				}
				break;
			case NOX_BIN_MLAY_TEXTURE:
				{
					if (!parseTextureChunk(NULL, NULL))
					{
						return false;
					}
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}

		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. Environment Map part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseUserPresets()
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	unsigned long long upStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long userPresetsSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(userPresetsSize))
	{
		sprintf_s(errorInfo, 256, "Error reading User Presets size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + userPresetsSize - 10;

	Scene * sc = rtr->curScenePtr;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in User Presets part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_USER_PRESET:
				{
					PresetPost * preset = new PresetPost();
					if (!parseUserPreset(preset))
						return false;
					sc->presets.add(preset);
					sc->presets.createArray();
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}
		currentPos = _ftelli64(file);
	}


	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. User presets part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

bool BinaryScene::parseUserPreset(PresetPost * preset)
{
	if (!file)
	{
		MessageBox(0, "File is NULL. Internal error", "Error", 0);
		return false;
	}
	if (!preset)
	{
		MessageBox(0, "User Preset is NULL. Internal error", "Error", 0);
		return false;
	}

	unsigned long long upStart = _ftelli64(file) - 2;
	unsigned long long tPos;
	char errorInfo[256];

	unsigned long long userPresetSize = 0;
	tPos = _ftelli64(file);
	if (1!=FREAD_ULONG8(userPresetSize))
	{
		sprintf_s(errorInfo, 256, "Error reading User Preset size chunk at %lld.", tPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	unsigned long long currentPos = _ftelli64(file);
	unsigned long long borderPos = currentPos + userPresetSize - 10;

	while (currentPos < borderPos)
	{
		tPos = _ftelli64(file);
		unsigned short nextChunk;
		if (1 != FREAD_USHORT(nextChunk))
		{
			sprintf_s(errorInfo, 256, "Error while reading next chunk in User Preset part at %lld.", tPos);
			MessageBox(0, errorInfo, "Error", 0);
			return false;
		}

		switch (nextChunk)
		{
			case NOX_BIN_CAM_POST:
				{
					if (!parseCameraPostChunk(&preset->iMod))
						return false;
				}
				break;
			case NOX_BIN_FINAL_POST:
				{
					if (!parseCameraFinalPostChunk(&preset->fMod, &preset->texGlareDiaphragm, &preset->texGlareObstacle))
						return false;
				}
				break;
			case NOX_BIN_BLEND_LAYERS:
				{
					if (!parseBlendsChunk(&preset->blend))
						return false;
				}
				break;
			case NOX_BIN_USER_PRESET_NAME:
				{
					unsigned int nameSize = 0;
					tPos = _ftelli64(file);
					if (1!=FREAD_UINT(nameSize))
					{
						sprintf_s(errorInfo, 256, "Error reading user preset name size at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}
					nameSize -= 6;
						
					char * pName = (char *)malloc(nameSize+1);
					if (!pName)
					{
						sprintf_s(errorInfo, 256, "Error allocating %d bytes for user preset name. Size taken from %lld.", (nameSize+1), tPos);
						MessageBox(0, errorInfo, "Error", 0);
						return false;
					}

					tPos = _ftelli64(file);
					if (1!=FREAD_STRING(pName, nameSize))
					{
						sprintf_s(errorInfo, 256, "Error reading user preset name at %lld.", tPos);
						MessageBox(0, errorInfo, "Error", 0);
						free(pName);
						return false;
					}

					pName[nameSize] = 0;
					preset->name = pName;
				}
				break;
			default:
				{
					if (!parseUnknownChunk(nextChunk, 4))
					{
						return false;
					}
				}
				break;
		}
		currentPos = _ftelli64(file);
	}

	if (currentPos != borderPos)
	{
		sprintf_s(errorInfo, 256, "Error. User preset part should end at %lld instead of %lld.", borderPos, currentPos);
		MessageBox(0, errorInfo, "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

