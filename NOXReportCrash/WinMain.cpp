#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include <shlobj.h>

FILE * crashfilerespond = NULL;

//#define DEBUG_C_AAA_TXT

LRESULT CALLBACK NoxCrashReportWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
bool doesFileExist(char * filename);
bool doesDirExist(char * dirName);
char * getRendererDirectory();
char * getUserDirectory();
char * getUserDirectoryFromReg();
bool sendReport(char * crfilename);

char reportfilename[2048];

//-------------------------------------------------------------------------------------------------------------

ATOM MyRegisterReportCrashWndClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	HICON sIcon = 0;//LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= NoxCrashReportWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= sIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "nox_crash_report";
	wcex.hIconSm		= sIcon; //LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	return RegisterClassEx(&wcex);
}

//-------------------------------------------------------------------------------------------------------------

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	//MessageBox(0, lpCmdLine, "Error", 0);
	//for (int i=0; i <__argc; i++)
	//	MessageBox(0, __argv[i], "Error", 0);

	bool abort = false;
	if (__argc<2)
		abort = true;
	else
		if (__argv[1]  &&  strcmp(__argv[1], "-sendreport"))
			abort = true;

	if (abort)
	{
		MessageBox(0, "Sorry, crash report cannot be invoked manually.", "Error", 0);
		return 1;
	}

	MyRegisterReportCrashWndClass(hInstance);


	char * noxDir = getUserDirectoryFromReg();
	if (!noxDir)
		noxDir = getUserDirectory();
	if (!noxDir)
		noxDir = getRendererDirectory();
	sprintf_s(reportfilename, 2048, "%s\\logs\\noxcrash.txt", noxDir);
	if (!doesFileExist(reportfilename))
	{
		MessageBox(0, "NOX has crashed.\nUnfortunately can't find crash log.", "Error", 0);
		return 1;
	}

	int w = 355;
	int h = 115;
	srand((unsigned int)time(0));
	HWND hMain = CreateWindow("nox_crash_report", "Report crash.", WS_CAPTION | WS_SYSMENU | WS_CLIPCHILDREN ,
		(GetSystemMetrics(SM_CXSCREEN)-w)/2, (GetSystemMetrics(SM_CYSCREEN)-h)/2,    w, h,     0, NULL, hInstance, NULL);
	if (hMain)
		ShowWindow(hMain, SW_SHOW);

	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	HWND hText1 = CreateWindow("STATIC", "Unfortunately NOX has crashed.\nWould you like to send us crash report?", WS_CHILD | WS_VISIBLE,
		  10,10,   420,30,   hMain, (HMENU)200, hInstance, NULL);
	SendMessage(hText1, WM_SETFONT, (WPARAM)hFont, TRUE);
	HWND hShow = CreateWindow("BUTTON", "Show report", WS_CHILD | WS_VISIBLE,
		  10,50,   100,20,   hMain, (HMENU)201, hInstance, NULL);
	SendMessage(hShow, WM_SETFONT, (WPARAM)hFont, TRUE);
	HWND hSend = CreateWindow("BUTTON", "Send report", WS_CHILD | WS_VISIBLE,
		  120,50,   100,20,   hMain, (HMENU)202, hInstance, NULL);
	SendMessage(hSend, WM_SETFONT, (WPARAM)hFont, TRUE);
	HWND hClose = CreateWindow("BUTTON", "Close window", WS_CHILD | WS_VISIBLE,
		  230,50,   100,20,   hMain, (HMENU)203, hInstance, NULL);
	SendMessage(hClose, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Main message loop:
	MSG msg;
	HACCEL hAccelTable;
	hAccelTable = 0;//LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MATERIALEDITORUSEDLL));
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

//-------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK NoxCrashReportWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		{
		}	// end WM_CREATE
		break;
	case WM_SIZE:
		{
		}
		break;
	case WM_CLOSE:
		{
			DestroyWindow(hWnd);
		}
		break;
	case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
	//case WM_SIZING:
	//	{
	//		RECT * rect = (RECT *)lParam;
	//		rect->right = max(rect->right, max(rect->right-rect->left,400)+rect->left);
	//		rect->bottom = rect->top+415;
	//	}
	//	break;
	case 26120:	// send
		{
			sendReport(reportfilename);
			EnableWindow(GetDlgItem(hWnd, 201), TRUE);
			EnableWindow(GetDlgItem(hWnd, 202), TRUE);
			EnableWindow(GetDlgItem(hWnd, 203), TRUE);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case 201:	// show report
					{
						if (reportfilename)
							ShellExecute(NULL, NULL, reportfilename, NULL, NULL, SW_SHOWNORMAL);
					}
					break;
				case 202:	// send report
					{
						//bool ok = sendReport(reportfilename);
						EnableWindow(GetDlgItem(hWnd, 201), FALSE);
						EnableWindow(GetDlgItem(hWnd, 202), FALSE);
						EnableWindow(GetDlgItem(hWnd, 203), FALSE);
						SendMessage(hWnd, 26120, 0, 0);
					}
					break;
				case 203:	// close
					{
						DestroyWindow(hWnd);
					}
					break;
			}
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	return 0;
}

//-------------------------------------------------------------------------------------------------------------

char * getUserDirectory()
{
	TCHAR szPath[MAX_PATH];
	BOOL ok = SHGetSpecialFolderPath(0, szPath, CSIDL_LOCAL_APPDATA, FALSE);
	if (!ok)
		return NULL;
	int l = MAX_PATH+64;
	char * res = (char*)malloc(l);
	sprintf_s(res, l, "%s\\Evermotion\\NOX", szPath);
	return res;
}

//-------------------------------------------------------------------------------------------------------------

char * getUserDirectoryFromReg()
{
	// get default directory
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres = RegQueryValueEx(key3, "userdirectory", NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
	{	// if OK
	}
	else
	{
		return NULL;
	}

	size_t ss = strlen(buffer)+1;
	char * res = (char *)malloc(ss);
	if (res)
		sprintf_s(res, ss, "%s", buffer);

	for (int i=(int)ss-2; i>2; i--)
	{
		if (res[i]=='\\'  ||  res[i]=='/')
			res[i] = 0;
		else
			break;
	}

	return res;
}

//-------------------------------------------------------------------------------------------------------------

char * getRendererDirectory()
{
	// get default directory
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres = RegQueryValueEx(key3, "directory", NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
	{	// if OK
	}
	else
	{
		return NULL;
	}

	size_t ss = strlen(buffer)+1;
	char * res = (char *)malloc(ss);
	if (res)
		sprintf_s(res, ss, "%s", buffer);

	for (int i=(int)ss-2; i>2; i--)
	{
		if (res[i]=='\\'  ||  res[i]=='/')
			res[i] = 0;
		else
			break;
	}

	return res;
}

//-------------------------------------------------------------------------------------------------------------

unsigned int getUserId()
{
	// get default directory
	DWORD rsize = 2048;
	char buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, "Evermotion", 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, "Renderer", 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048;
		regres = RegQueryValueEx(key3, "report_random_id", NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
	{	// if OK
	}
	else
	{
		return 0;
	}

	long long id = 0;
	char * addr;
	int a = (int)_strtoi64(buffer, &addr, 10);
	if (addr == buffer)
	{	// conversion error (to int)
		id = 0;
	}
	else
	{
		id = max(0, a);
	}

	return (unsigned int)id;
}

//-------------------------------------------------------------------------------------------------------------

bool setUserId(unsigned int id)
{	LONG regres;
	HKEY key1, key2, key3;
	char buf[64];
	sprintf_s(buf, 64, "%u", id);

	regres = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, KEY_WRITE, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegCreateKey(key1, "Evermotion", &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegCreateKey(key2, "Renderer", &key3);

	if (regres == ERROR_SUCCESS)
		regres = RegSetValueEx(key3, "report_random_id", 0, REG_SZ, (BYTE*)buf, 64);

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool doesDirExist(char * dirName)
{
	bool res;
	HANDLE hDir;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hDir = FindFirstFile(dirName, &fd);
	
	if (hDir == INVALID_HANDLE_VALUE   ||   !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hDir != INVALID_HANDLE_VALUE)
		FindClose(hDir);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

bool doesFileExist(char * filename)		// will return false on dir
{
	if (!filename)
		return false;

	bool res;
	HANDLE hFile;
	WIN32_FIND_DATA fd;

	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hFile = FindFirstFile(filename, &fd);
	
	if (hFile == INVALID_HANDLE_VALUE   ||   (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		res = false;
	else
		res = true;

	if (hFile != INVALID_HANDLE_VALUE)
		FindClose(hFile);

	return res;
}

//-------------------------------------------------------------------------------------------------------------

size_t write_data (void *ptr, size_t size, size_t nmemb, void *stream) 
{ 
	#ifdef DEBUG_C_AAA_TXT
		return fwrite(ptr, size, nmemb, (FILE*)stream);
	#else
		return nmemb;
	#endif
}

//-------------------------------------------------------------------------------------------------------------

bool sendReport(char * crfilename)
{
	if (!crfilename)
		return false;
	if (!doesFileExist(crfilename))
		return false;

	//Sleep(3000);
	FILE * file = NULL;
	FILE * file2 = NULL;
	
	#ifdef DEBUG_C_AAA_TXT
		if (fopen_s(&crashfilerespond, "C:\\aaa.txt", "wb"))
			return false;
		file2 = crashfilerespond;
	#endif

	if (fopen_s(&file, crfilename, "rb"))
		return false;
	if (!file)
	{
		MessageBox(0, "Report file opening failed. Report was not sent.", "Error", 0);
		return false;
	}
	_fseeki64(file, 0, SEEK_END);
	unsigned long long fileSize = _ftelli64(file);
	_fseeki64(file, 0, SEEK_SET);
	if (fileSize<1)
	{
		fclose(file);
		MessageBox(0, "Report file is empty. Report was not sent.", "Error", 0);
		return false;
	}

	char * txt = (char*)malloc((size_t)fileSize+1);
	if (1!=fread(txt, (size_t)fileSize, 1, file))
	{
		fclose(file);
		MessageBox(0, "Reading from report file failed. Report was not sent.", "Error", 0);
		return false;
	}
	txt[fileSize]=0;
	fclose(file);

	unsigned int id = getUserId();
	if (id==0)
	{
		id = rand()*rand();
		setUserId(id);
	}
	char idtxt[64];
	sprintf_s(idtxt, 64, "%u", id);

	CURL *curl;
	CURLcode res;

	char * strURL = "http://evermotion.org/nox/sendCrashReport";

	curl_global_init(CURL_GLOBAL_ALL); 
	curl = curl_easy_init(); 

	curl_easy_setopt(curl, CURLOPT_POST, 1); 

	struct curl_httppost * post = NULL;
	struct curl_httppost * last = NULL;

	curl_formadd(&post, &last,
				CURLFORM_COPYNAME, "report",
				CURLFORM_COPYCONTENTS, txt, CURLFORM_END);

	curl_formadd(&post, &last,
				CURLFORM_COPYNAME, "user_id",
				CURLFORM_COPYCONTENTS, idtxt, CURLFORM_END);

	//curl_formadd(&post, &last,
	//			CURLFORM_COPYNAME, "report",
	//			CURLFORM_COPYCONTENTS, "Report_text", CURLFORM_END);
	//curl_formadd(&post, &last,
	//			CURLFORM_COPYNAME, "user_id",
	//			CURLFORM_COPYCONTENTS, "121314", CURLFORM_END);

	curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
	curl_easy_setopt(curl, CURLOPT_URL, strURL); 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); 
	curl_easy_setopt(curl, CURLOPT_POSTREDIR, CURL_REDIR_POST_301); 

	
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file2);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

	res = curl_easy_perform(curl); 
	 
	curl_formfree(post);

	if (res)
	{
		MessageBox(0, "Connection error. Report not sent.\nThanks anyway :)", "Error", 0);
		// error shit, not sent
	}
	else
	{
		MessageBox(0, "Report has been sent.\nThank you :)", "Report", 0);
		//ShellExecute(NULL, NULL, "C:\\aaa.txt", NULL, NULL, SW_SHOWNORMAL);
		PostQuitMessage(0);
	}
	curl_easy_cleanup(curl); 
	curl_global_cleanup();

	free(txt);

	#ifdef DEBUG_C_AAA_TXT
		fclose(crashfilerespond);
	#endif

	return true;
}

//-------------------------------------------------------------------------------------------------------------

