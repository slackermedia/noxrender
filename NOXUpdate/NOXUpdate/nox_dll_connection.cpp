#define _WIN32_WINNT 0x0502
#include <windows.h>
#include "nox_update.h"
#include "../../Everything DLL/Everything DLL/RendererMainWindow.h"
#include "../../Everything DLL/Everything DLL/raytracer.h"


bool getNoxVersion(int &vMajor, int &vMinor, int &vBuild)
{
	HINSTANCE noxInstance;
	#ifdef _WIN64
		noxInstance = LoadLibrary("EverythingDLL64.dll");
	#else
		noxInstance = LoadLibrary("EverythingDLL32.dll");
	#endif

	if (noxInstance == 0)
		return false;

	typedef int (*getVersionMajorPtr)();
	typedef int (*getVersionMinorPtr)();
	typedef int (*getVersionBuildPtr)();
	getVersionMajorPtr getVersionMajor =  (getVersionMajorPtr)GetProcAddress(noxInstance,"?getVersionMajor@Raytracer@@SAHXZ");
	getVersionMinorPtr getVersionMinor =  (getVersionMajorPtr)GetProcAddress(noxInstance,"?getVersionMinor@Raytracer@@SAHXZ");
	getVersionBuildPtr getVersionBuild =  (getVersionMajorPtr)GetProcAddress(noxInstance,"?getVersionBuild@Raytracer@@SAHXZ");

	if (getVersionMajor==NULL   ||   getVersionMinor==NULL   ||   getVersionBuild==NULL)
	{
		FreeLibrary(noxInstance);
		return false;
	}

	vMajor = getVersionMajor();
	vMinor = getVersionMinor();
	vBuild = getVersionBuild();

	FreeLibrary(noxInstance);
	return true;
}
