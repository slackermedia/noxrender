#include "max2013predef.h"
#include "NOXExporter.h"
#include <Object.h>
#include "NOXSunLight.h"
#include "NOXRenderer.h"

#include "noxdebug.h"

//#define DEBUG_COUNT_DELETE_COPIES

static NOXExportClassDesc noxExportDesc;
ClassDesc2* GetNOXExportDesc() { return &noxExportDesc; }

NOXExport::NOXExport()
{
	ADD_LOG_CONSTR("NOXExport Constructor - empty");
}

NOXExport::~NOXExport() 
{
	ADD_LOG_CONSTR("NOXExport Destructor - empty");
}

int NOXExport::ExtCount()
{
	return 2;
}

const TCHAR *NOXExport::Ext(int n)
{
	if (n==0)
		return _T("nox");
	else
		return _T("nxs");
}

const TCHAR *NOXExport::LongDesc()
{
	return _T("NOX scene");
}
	
const TCHAR *NOXExport::ShortDesc() 
{			
	return _T("NOX scene");
}

const TCHAR *NOXExport::AuthorName()
{			
	return _T("");
}

const TCHAR *NOXExport::CopyrightMessage() 
{	
	return _T("");
}

const TCHAR *NOXExport::OtherMessage1() 
{		
	return _T("");
}

const TCHAR *NOXExport::OtherMessage2() 
{		
	return _T("");
}

unsigned int NOXExport::Version()
{				
	return 100;		// version number * 100 (i.e. v3.01 = 301)
}

void NOXExport::ShowAbout(HWND hWnd)
{			
}

BOOL NOXExport::SupportsOptions(int ext, DWORD options)
{
	return FALSE;	//At present, the only export option is SCENE_EXPORT_SELECTED, but this may change in the future
}

//-------------------------------------------------------------------------------------------------------------------------------------------	


int	NOXExport::DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts, DWORD options)
{
	ADD_LOG_PARTS_MAIN("NOXExport::DoExport");

	bool ok = true;
	if (!name)
		return FALSE;
	char * name_ansi = getAnsiCopy1(name);
	int l = (int)strlen(name_ansi);
	bool doItBinary = false;
	if (	(name_ansi[l-3]=='n'  ||  name_ansi[l-3]=='N')  &&    
			(name_ansi[l-2]=='o'  ||  name_ansi[l-2]=='O')  &&    
			(name_ansi[l-1]=='x'  ||  name_ansi[l-1]=='X'))
		doItBinary = true;

	ADD_LOG_PARTS_MAIN(name_ansi);
	free(name_ansi);

	if (doItBinary)
	{
		BinaryScenePlugin bScene(false);
		bScene.hInfoWindow = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_NOX_EXPORT_DIALOG), i->GetMAXHWnd(), NOXExportInfoDlgProc);
		bScene.setProgInfo(_T("Enumerating scene nodes..."));
		SceneEnum sEnum(ei, &bScene);
		ei->theScene->EnumTree(&sEnum);
		ok = bScene.saveScene(name);
		DestroyWindow(bScene.hInfoWindow);
	}
	else
	{
		XMLScene xScene(false);
		xScene.hInfoWindow = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_NOX_EXPORT_DIALOG), i->GetMAXHWnd(), NOXExportInfoDlgProc);
		xScene.setProgInfo(_T("Enumerating scene nodes..."));
		SceneEnum sEnum(ei, &xScene);
		ei->theScene->EnumTree(&sEnum);
		ok = xScene.saveScene(name);
		DestroyWindow(xScene.hInfoWindow);
	}

	showDeletedCopiesStats();

	if (ok)
	{
		MessageBox(0, _T("Export complete."), _T(""), 0);
		ADD_LOG_PARTS_MAIN("NOXExport::DoExport done");
		return TRUE;
	}

	MessageBox(0, _T("Export error."), _T(""), 0);
	ADD_LOG_PARTS_MAIN("NOXExport::DoExport failed");

	return FALSE;
}




int SceneEnum::callback(INode *  node)
{
	int i;
	for (i=0; i<node->NumberOfChildren(); i++)
	{
		callback(node->GetChildNode(i));
	}
	
	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return TREE_ABORT;

	if (objSt.obj->SuperClassID() == CAMERA_CLASS_ID)
	{
		if (xscene)
			xscene->cameras.push_back(node);
		if (bscene)
			bscene->cameras.push_back(node);
	}

	if (objSt.obj->SuperClassID() == LIGHT_CLASS_ID)
		if (objSt.obj->ClassID() == NOXSunLight_CLASS_ID)
		{
			if (xscene)
				xscene->sunsky = node;
			if (bscene)
				bscene->sunsky = node;
		}

	if (	objSt.obj->CanConvertToType(Class_ID(EDITTRIOBJ_CLASS_ID,0))   ||   
			//objSt.obj->CanConvertToType(Class_ID(NURBSOBJ_CLASS_ID,0))   ||   
			objSt.obj->CanConvertToType(Class_ID(PATCHOBJ_CLASS_ID,0))   ||   
			objSt.obj->CanConvertToType(Class_ID(POLYOBJ_CLASS_ID,0))   )
	{
		if (objSt.obj && objSt.obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
		{
			if (!node->Renderable())
				return TREE_CONTINUE;
			if (node->IsNodeHidden())
				return TREE_CONTINUE;

			SClass_ID scID = node->SuperClassID();
			if (scID == REF_TARGET_CLASS_ID)
			{
				const TCHAR * name = node->GetName();
				if (name   &&   !_tcscmp(name, _T("MNode")))
					return TREE_CONTINUE;
			}
			
			TriObject * tobj = (TriObject *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), triObjectClassID);
			if (tobj)
			{
				Object * robj = node->GetObjectRef();

				Mesh mesh = tobj->GetMesh();
				if (xscene)
				{
					int sID = -1;
					for (int i=0; i<(int)xscene->meshsources.size(); i++)
					{
						if (xscene->meshsources[i].objRef == robj)
						{	sID = i;  break;	}
					}
					if (sID<0)
					{
						xscene->meshsources.push_back(MeshInstSrc(robj, node));
						sID = (int)xscene->meshsources.size()-1;
					}
					xscene->meshinstances.push_back(MeshInst(sID, node));

					xscene->meshes.push_back(node);
					xscene->numTris += mesh.getNumFaces();
				}
				if (bscene)
				{
					int sID = -1;
					for (int i=0; i<(int)bscene->meshsources.size(); i++)
					{
						if (bscene->meshsources[i].objRef == robj)
						{	sID = i;  break;	}
					}
					if (sID<0)
					{
						bscene->meshsources.push_back(MeshInstSrc(robj, node));
						sID = (int)bscene->meshsources.size()-1;
						bscene->numTris += mesh.getNumFaces();
					}
					bscene->meshinstances.push_back(MeshInst(sID, node));

					bscene->meshes.push_back(node);
				}
				deleteSecondIfCopied(objSt.obj, tobj);
			}
		}

	}

	return TREE_CONTINUE;
}


static int deletedCopies = 0;

void countDeletedCopies()
{
	#ifdef DEBUG_COUNT_DELETE_COPIES
		deletedCopies++;
	#endif
}

void showDeletedCopiesStats()
{
	#ifdef DEBUG_COUNT_DELETE_COPIES
		char buf[64];
		sprintf_s(buf, 64, "Deleted copies: %d", deletedCopies);
		MessageBox(0, buf, "Info", 0);
	#endif
}
