#ifndef __RAYTRACER__H
#define __RAYTRACER__H

#define LOG_CONSTRUCTORS
#define RAND_PER_THREAD

#include "nox_version.h"

#define OWN_COUNTERS
#define OWN_SSE
//#define OWN_SSE3
//#define FORCE_SSE

#include "DLL.h"
#include "Colors.h"
#include "textures.h"
#include "myList.h"
#include <windows.h>
#include <xmmintrin.h>
//#include <pmmintrin.h>
#include <intrin.h>
#include "regions.h"
#include "QuasiRandom.h"
#include "ies.h"
#include "MersenneTwister.h"

//#define TDECLDIR
#define TDECLDIR DECLDIR

#define CHECK(a) if(!(a)) return false

#define SFLOAT 0.0000000000001f
#define BIGFLOAT (float(1.0e30))
#define PI		3.1415926535897932384626433832795f
#define PER_PI	0.31830988618379067153776752674503f

extern _locale_t noxLocale;
extern _locale_t noxLocaleComa;

#define IS_DISPLACEMENT_PRODUCT(f) ((f)&(1<<0))
#define SET_DISPLACEMENT_PRODUCT(f) ((f)|=(1<<0))
#define SET_NO_DISPLACEMENT_PRODUCT(f) ((f)&=(~(1<<0)))

class EMCurve;	// pointer in scene
#define lmesh_pow 0.3f

//---------------------------------------------------------------------

class Marsaglia;
class Lattice;
class Point3d;
class Vector3d;
class Matrix3d;
class Matrix4d;
class Triangle;
class Mesh;
class MeshInstance;
class Triangle4;
class Ray4;
class BVH;
class BSPTree;
class BlendSettings;
class SceneStatic;
class Scene;
class Raytracer;
class BBox;
class LightMesh;
class SkyPortalMesh;
class MatLayer;
class MaterialNox;
class HitData;
class DiaphragmShape;
class Camera;
class EPixel;
class ThreadManager;
class RenderThread;
class DepthMapThread;
class Fresnel;
class SunSky;
class TemperatureColor;
class UserColors;
class PresetPost;
class TextureInstance;
class TextureManager;
class EnvSphere;
class Bucket;
class RandomGenerator;


//---------------------------------------------------------------------    FLOAT4

#ifdef OWN_SSE
class _CRT_ALIGN(16) Float4 
#else
class Float4
#endif
{
public:
	union
	{
		#ifdef OWN_SSE
			__m128 sse;
		#endif
		struct
		{
			float v1,v2,v3,v4;
		};
		struct
		{
			float x,y,z,w;
		};
		float V[4];
		unsigned int bitmask[4];
		int s_int[4];
	};


	Float4(const Float4 &r);
	Float4();

	Float4& operator=(const Float4 &f);
};

//---------------------------------------------------------------------    POINT2D

class Point2d
{
public:
	union
	{
		float V[2];
		struct
		{
			float x,y;
		};
	};

	Point2d() {}
	Point2d(float X, float Y) {x=X; y=Y;}
};

//---------------------------------------------------------------------    POINT3D

#ifdef FORCE_SSE
class _CRT_ALIGN(16) Point3d
#else
class Point3d
#endif
{
public:
	union
	{
		#ifdef FORCE_SSE
			__m128 sse;
			float V[4];
		#else
			float V[3];
		#endif
		struct
		{
			float x,y,z;
			#ifdef FORCE_SSE
				float w;
			#endif
		};
	};

	Point3d();
	Point3d(const float &X, const float &Y, const float &Z);
	Vector3d operator-(const Point3d &another) const;
	Point3d  operator+(const Vector3d &dir) const;
	Point3d  operator*(const float &f) const;
	Vector3d toVec();
};

//---------------------------------------------------------------------    VECTOR3D

#ifdef FORCE_SSE
class _CRT_ALIGN(16) Vector3d
#else
class Vector3d
#endif
{
public:
	union
	{
		#ifdef FORCE_SSE
			__m128 sse;
			float V[4];
		#else
			float V[3];
		#endif
		struct
		{
			float x,y,z;
			#ifdef FORCE_SSE
				float w;
			#endif
		};
	};

	Vector3d();
	Vector3d(const float &X, const float &Y, const float &Z);

	bool normalize();
	float normalize_return_old_length();	// -1 for error
	Vector3d getNormalized();
	void invert();
	float length();
	float lengthSqr();
	static Vector3d random();
	Vector3d reflect(const Vector3d &normal) const;	// vector *this points out of surface
	Vector3d refractSnell(const Vector3d &normal, const float &ior, bool &innerReflected);

	void toAngles(float &theta, float &phi);
	void getFromAngles(const float &theta, const float &phi);

	Vector3d operator+(const Vector3d &another) const;
	Vector3d operator+=(const Vector3d &another);
	Vector3d operator-();
	Vector3d operator*(const float &s) const;
	Vector3d operator*=(const float &s);
	Vector3d operator^(const Vector3d &other) const;		// cross product
	float    operator*(const Vector3d &other) const;		// dot3
	Point3d toPoint() {return Point3d(x,y,z);}
};

//---------------------------------------------------------------------    TRIANGLE

class Matrix3d
{
public:
	static const int BY_ROWS = 1;
	static const int BY_COLS = 2;
	union
	{
		float M[3][3];
		struct
		{
			float M00, M01, M02;
			float M10, M11, M12;
			float M20, M21, M22;
		};
	};
	Matrix3d() {M00=M11=M22=1.0f; M01=M02=M10=M12=M20=M21=0.0f;}
	Matrix3d(const Point3d &v1, const Point3d &v2, const Point3d &v3, const int &type);
	Matrix3d(const Vector3d &v1, const Vector3d &v2, const Vector3d &v3, const int &type);
	Matrix3d operator* (const Matrix3d &m) const;

	bool invert();
};

Vector3d operator*(const Matrix3d &m, const Vector3d &v);

//---------------------------------------------------------------------    MATRIX4D

class _CRT_ALIGN(16) Matrix4d
{
public:
	union
	{
		__m128 sse[4];
		float M[4][4];
		struct
		{
			float M00, M01, M02, M03;
			float M10, M11, M12, M13;
			float M20, M21, M22, M23;
			float M30, M31, M32, M33;
		};
	};

	bool setRotation(Vector3d dir, float angle);
	void setIdentity();
	float getDeterminant();
	float getMinor(int y, int x);
	bool getInverted(Matrix4d &m);
	Matrix4d getTransposed();
	Vector3d getScaleComponent() const;
	Matrix4d getMatrixForNormals() const;
	Matrix4d operator*(const Matrix4d &m) const;
	Vector3d operator* (const Vector3d &v) const;
	Point3d operator* (const Point3d &v) const;
};

void logMatrix(char * name, Matrix4d &m);
void logMatrixMax(char * name, Matrix4d &m);

//---------------------------------------------------------------------    MATRIX4D

class Matrix4dSmall
{
public:
	union
	{
		float M[3][4];
		struct
		{
			float M00, M01, M02, M03;
			float M10, M11, M12, M13;
			float M20, M21, M22, M23;
		};
	};
	Matrix4dSmall(const Matrix4d & m4);
	Matrix4dSmall(const Matrix4dSmall & m4);
	Matrix4d toFullMatrix();
};

//---------------------------------------------------------------------    BLEND

class Triangle
{
public:
	Point3d V1, V2, V3;			// 36 bytes
	Vector3d N1, N2, N3;		// 36 bytes
	Vector3d normal_geom;		// 12 bytes
	Point2d UV1, UV2, UV3;		// 24 bytes
	Vector3d dirU1, dirV1, dirU2, dirV2, dirU3, dirV3; // 72 bytes

	unsigned int flags;			// 4 bytes
	int matNum;					// 4 bytes
	float area;					// 4 bytes
	unsigned int meshOwn;		// 4 bytes
	unsigned int matInInst;		// 4 bytes
								// 200 bytes total

	float intersection(const Point3d &origin, const Vector3d &direction, float &u, float &v);
	float intersection(const Point3d &origin, const Vector3d &direction, const float &maxDist);
	bool getUV(const Point3d &q, float &u, float &v);
	Triangle() { flags = 0; }
	Triangle(	const Point3d &v1, const Point3d &v2, const Point3d &v3, 
			const Vector3d &n1, const Vector3d &n2, const Vector3d &n3, 
			const Point2d &uv1, const Point2d &uv2, const Point2d &uv3, 
			const unsigned int &owner);
	~Triangle();

	float getArea();
	float evaluateArea();

	static float getArea(const Point3d &p1, const Point3d &p2, const Point3d &p3);
	Point3d getPointFromUV(const float &u, const float &v);
	void randomPoint(float &u, float &v, Point3d &point);
	bool evalTexUV(const float &u, const float &v, float &tU, float &tV) const;
	Vector3d evalNormal(const float &u, const float &v) const;
	bool getTangentSpaceSmooth(const Matrix4d &matr, const float &u, const float &v, Vector3d &dirU, Vector3d &dirV);
	bool evalTangentSpace(Matrix4d &matr, const float &u, const float &v, Vector3d &U, Vector3d &V);
	void getBadUVdir(Matrix4d &matr, Vector3d &uDir, Vector3d &vDir);	// based on global axis dirs
	void getBadUVdirSmooth(Vector3d normal_shade, Vector3d &uDir, Vector3d &vDir);	// based on global axis dirs
	bool evalUVvectors(Vector3d &TU1, Vector3d &TU2, Vector3d &TU3, Vector3d &TV1, Vector3d &TV2, Vector3d &TV3) const;

	bool addDisplacedTris(MaterialNox * mat, MeshInstance * mInst);

};

//---------------------------------------------------------------------    MESH

class Mesh
{
public:
	int firstTri;
	int lastTri;
	int instSrcID;
	char * name;

	int displacement_instance_id;

	bool evalUVdirs();
	bool assignName(char * nname);
	Mesh()  {  instSrcID=-1;   firstTri = -1;   lastTri = -2;   name = NULL;  displacement_instance_id = -1;  }
	~Mesh() {  if (name)   { free(name); }  name = NULL;  }
};

//---------------------------------------------------------------------    INSTANCE

class MeshInstance
{
public:
	int meshID;
	int instSrcID;
	Matrix4d matrix_transform;
	Matrix4d matrix_inv;
	bool has_displacement;
	int displace_orig_ID;
	bool mb_on;
	Vector3d mb_velocity;
	Vector3d mb_rot_axis;
	float mb_rot_vel;
	Matrix4d matrix_offset;
	Matrix4d matrix_offset_inv;
	char * name;
	bList<int, 8> matsFromFile;
	bList<int, 8> materials;

	MeshInstance();
	MeshInstance(const MeshInstance &mInst);
	MeshInstance& operator= (const MeshInstance& mInst);

};

//---------------------------------------------------------------------    TRIANGLE4

class Triangle4
{
public:
	Float4 v0x;
	Float4 v0y;
	Float4 v0z;
	Float4 e1x;
	Float4 e1y;
	Float4 e1z;
	Float4 e2x;
	Float4 e2y;
	Float4 e2z;

	Float4 numbers;

	Triangle4();
	Triangle4(const Triangle4 & t);
	Triangle4(const Triangle &t1, const Triangle &t2, const Triangle &t3, const Triangle &t4, int &n1, int &n2, int &n3, int &n4);

	Triangle4& operator=(const Triangle4 &t);

	void intersection(const Ray4 &rays, Float4 &dist, Float4 &U, Float4 &V, Float4 &maxd, Float4 &num) const;
	void intersection2(const Ray4 &rays, Float4 &dist, Float4 &UU, Float4 &VV, Float4 &maxd, Float4 &num) const;
};

//---------------------------------------------------------------------    RAY4

class Ray4
{
public:
	Float4 ox;
	Float4 oy;
	Float4 oz;
	Float4 dx;
	Float4 dy;
	Float4 dz;

	Ray4();
	Ray4(const Ray4 &ray4);
	Ray4(const float &Ox, const float &Oy, const float &Oz, const float &Dx, const float &Dy, const float &Dz);
	Ray4(const Float4 &or, const Float4 &dr);

	Ray4& operator=(const Ray4 &ray4);
};

//---------------------------------------------------------------------    RANDOM GENERATOR

class RandomGenerator
{
	MersenneTwister mt;
public:
	float getRandomFloat();
	double getRandomDouble();
	unsigned int getRandomInt(unsigned int maxval);
	void setSeed(int seed) { mt.initialize(seed); }


	RandomGenerator() { mt.initialize((int)time(0)); }
};

//---------------------------------------------------------------------    MARSAGLIA

class Marsaglia
{
	#define aMarsaglia 18000
	#define bMarsaglia 36969
	unsigned int X;
	unsigned int Y;
public:
	unsigned int getRandom();
	void setSeed(unsigned int seed1, unsigned int seed2);
	Marsaglia() { X = 6745745; Y = 43634; }
};

//---------------------------------------------------------------------    SKYLINE

class ScanLine
{
	int X, Y, num_threads, thr_ID;
public:
	int width, height;

	void reset(int width1, int height1, int numthreads, int threadID);
	void getNextPoint(float &x, float &y);
};

//---------------------------------------------------------------------    LATTICE

class Lattice
{
	int ax;
	int ay;
	int m;
	int i;
	double addx;
	double addy;
public:
	bool randomPoint(float &x, float &y);
	bool reset();

	Lattice();
	~Lattice() {}
};

//---------------------------------------------------------------------    BUCKET

class Bucket
{
public:
	int res_x, res_y;
	int tile_size_x, tile_size_y;
	int tx_min, tx_max, ty_min, ty_max;
	int s0x, s0y;	// coord of left-upper corner of center bucket
	int * coordsThreads;
	int nThr;

	static const int DIR_UP = 0;
	static const int DIR_LEFT = 1;
	static const int DIR_DOWN = 2;
	static const int DIR_RIGHT = 3;
	int bdir;
	int max_dir_up, max_dir_down, max_dir_left, max_dir_right;
	int cbx, cby;
	int totalBucketsOnImage;
	int curTaken;
	bool stopOnLast;


	bool getCoordinates(int tx, int ty, int &x1, int &y1, int &x2, int &y2);
	bool randomPixel(int tx, int ty, float &x, float &y);
	bool getNewTile(int &tx, int &ty);
	bool reset(int iw, int ih, int bw, int bh);

	Bucket();
	~Bucket() {}

};

//---------------------------------------------------------------------    MATRIX3D

class BlendSettings
{
public:
	bool blendOn[16];
	float weights[16];
	float red[16];
	float green[16];
	float blue[16];
	(char *) names[16];

	void setDefaultNames();
	DECLDIR void setName(int num, char * newname);
	bool copyFrom(BlendSettings * src);
	bool copyNamesFrom(BlendSettings * src);
	DECLDIR BlendSettings();
	~BlendSettings();
};

//---------------------------------------------------------------------    BVH

class BVH
{
public:
	struct Node
	{
		void * ptr;
		Node * left;
		Node * right;
		unsigned int offsettoroot;
		int type;
		static const int TYPE_SPLIT = 0;
		static const int TYPE_INSTANCE = 1;
		static const int TYPE_INSTANCE_MB = 2;
	};

	struct BVHSplit
	{
		union 
		{
			struct
			{
				unsigned int offset_left;	// last bit 0
				float minx;
				float miny;
				float minz;
				unsigned int offset_right;
				float maxx;
				float maxy;
				float maxz;
			};
			struct
			{
				__m128 sseMin;
				__m128 sseMax;
			};
		};
	};

	struct BVHInstance
	{
		unsigned int offset_left;	// last bit 1, pre last bit 0 for non motion blur
		float minx;
		float miny;
		float minz;
		unsigned int offset_right;	// mesh id here
		float maxx;
		float maxy;
		float maxz;

		Matrix4dSmall matr_tr;
		Matrix4dSmall matr_inv;
		
		float mb_angle_x, mb_angle_y, mb_angle_z, mb_angle_vel;
		float mb_vel_x, mb_vel_y, mb_vel_z;
		float reserved;	// for align - 192 bytes total
	};

	struct BVHMBInstance
	{
		// 
		unsigned int offset_left;	// last bit 1, pre last bit 1 for motion blur
		float minx;
		float miny;
		float minz;
		unsigned int offset_right;	// mesh id here
		float maxx;
		float maxy;
		float maxz;

		Matrix4dSmall matr_tr;
		Matrix4dSmall matr_inv;
		Matrix4dSmall matr_offset;
		Matrix4dSmall matr_offset_inv;
		
		float mb_angle_x, mb_angle_y, mb_angle_z, mb_angle_vel;
		float mb_vel_x, mb_vel_y, mb_vel_z;
		float reserved;	// for align - 192 bytes total
	};

	bList<BSPTree *, 16> meshes;
	unsigned char * root;

	bool addMesh(Mesh * mesh);
	bool createTree();
	bool deleteAllStuff();

	float intersectForTris(const float sample_time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instID, int &tri, float &u, float &v);
	float intersectForTrisNonSSE(const float sample_time, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instID, int &tri, float &u, float &v);

	BVH();
	~BVH();
};

//---------------------------------------------------------------------    BSP TREE

#define BSP_ISLEAF(n)		((n.offset&0x1) & ((n.offset&0x2)>>1))
#define BSP_DIMENSION(n)	(n.offset & 0x3)
#define BSP_OFFSET(n)		(((size_t)(n.offset & (0xFFFFFFFC)))<<1)
#define BSP_NON_SSE

class BSPTree
{
public:
	struct BSPSplit
	{
		unsigned int offset;
		float split;
	};
	struct BSPLeaf
	{
		unsigned int offset;
		unsigned int numTris;
	};
	union BSPNode
	{
		BSPSplit splitNode;
		BSPLeaf leafNode;
	};

	struct nodeStack
	{
		unsigned long long offset;
		float tnear;
		float tfar;
	};

	struct naiveNode
	{
		static float intervalSAH;
		static int intervalNumSAH;
		static unsigned long long nextAddr;
		static int maxLevel;
		static unsigned long long rOffset;
		naiveNode * left;
		naiveNode * right;

		float minX, minY, minZ, maxX, maxY, maxZ;
		float split;
		unsigned long long offset;
		unsigned long long tOffset;
		BSPTree * bspOwner;

		bList<int, 32> * tris;
		char dimension;
		char level;
		bool isLeaf;
		bool amILeft;

		naiveNode();
		naiveNode(const naiveNode & node);
		~naiveNode();
		void copyFrom(const naiveNode & node);
		void createOffsets(int lvl);
		void addToBSPBuffer(void * addr);
		void divideSAH();
		float areaSAH();
		bool triInside(Triangle * tri);
		bool intersectionEdge(const Point3d &p1, const Point3d &p2);
		void makeTris4(void * addr);
	};

	void addTrisRoot();
	void addTrisMesh(Mesh * mesh);
	void addTriangle(int index);
	void makeBSP();
	void deleteAllStuff();

	float intersectTris(const Point3d &origin, const Vector3d &direction, const Vector3d &invdirection, int &tri, float &u, float &v, float maxDist, size_t offset, unsigned int num4);
	bool intersection(const Point3d &origin, const Vector3d &direction, const Vector3d &invdir, const float &maxDist, float &near, float &far);
	float intersectForTris(const Point3d &origin, const Vector3d &direction, float &maxDist, int &tri, float &u, float &v);

	float minx, miny, minz, maxx, maxy, maxz;


	static int counterTris;
	static int counterNodes;
	static int counter5;
	char * name;

	BSPNode * root;
	naiveNode * nRoot;

	static int maxDivs;
	static int maxTris;
	static unsigned long long kdtree_buf_size;

	BSPTree();
	~BSPTree();
};

//---------------------------------------------------------------------    TEXTURE INSTANCE

class TextureInstance
{
public:
	TexModifer texMod;
	NormalModifier normMod;
	char * filename;
	char * fullfilename;
	int managerID;
	Texture * texScPtr;
	unsigned int exportedArrayID;		// used to find fast rel path
	char * exportedFilename;			// relative path for export

	DECLDIR bool isValid();
	Color4 getColor(float x, float y);
	Vector3d TextureInstance::getNormal(float x, float y);

	DECLDIR void setEmptyTex();
	bool updateSceneTexturePointer(Scene * sc, bool normal_slot);

	DECLDIR TextureInstance & TextureInstance::operator= (const TextureInstance & texInst);

	TextureInstance(const TextureInstance & texinst);
	TextureInstance();
	~TextureInstance();
};

//---------------------------------------------------------------------    TEXTURE MANAGER

class TextureManager
{
public:
	myList<Texture*> textures;

	DECLDIR int addTexture(char * fullfilename, bool tryOtherFolders, Texture * src, bool asNormal);
	DECLDIR bool updateTextures(Scene * sc);
	bool checkForUsed(Scene * sc, char * fullfilename);
	int  findID(char * fullfilename, bool normal_slot);
	DECLDIR bool updateInstance(TextureInstance * texInst, bool normal_slot);
	bool checkReloadModifiedTextures(HWND hWnd, bool forceReload);

	DECLDIR bool logTextures();

	TextureManager();
	~TextureManager();
};

//---------------------------------------------------------------------    ENVIRONMENT SPHERE

class EnvSphere
{
public:
	TextureInstance tex;
	Vector3d dirU, dirN, dirE, rDirN, rDirE;
	float powerEV;
	float azimuth_shift;
	int blendlayer;
	bool enabled;

	Color4 getColor(Vector3d dir);
	void evalAngles(Vector3d dir, float &phi, float &theta, float &x, float &y);
	void updateAzimuthDirections();
	
	float getProbability(Vector3d dir);
	Vector3d randomDir(float &pdf);

	float getProbabilityUniform(Vector3d dir);
	Vector3d randomDirUniform(float &pdf);

	DECLDIR EnvSphere();
	DECLDIR ~EnvSphere();
};

//---------------------------------------------------------------------    SCENE SETTINGS

class SceneStatic
{
public:
	float scaleScene;
	float perScaleScene;
	int bounces;
	int maxTrisPerBox;
	int maxBoxDivs;
	int numProbesFirstGI;
	int giSamplesCount;	// for bdpt, also it has to be per thread
	Point3d sMax, sMin;

	float * lightsProb;
	float * lightMeshPowers;
	float * lightMeshSumPowers;
	float * lightMeshPowersPow;
	float * lightMeshSumPowersPow;
	float lightsTotalArea;
	float allLightsPower;
	float allLightsPowerPow;
	float skyPortalsTotalArea;

	MaterialNox *				defaultMat;
	myList<MaterialNox*> *		mats;
	myList<LightMesh *> *	lightMeshes;

	bool drawBufDirect;
	bool drawBufGI;
	bool drawStampLogo;
	bool drawStampTime;
	int activeCamera;
	int giMethod;
	int random_type;
	int tone_mapping;
	int editableMaterial;
	bool useSkyPortals;
	bool disableSecondaryCaustic;
	bool causticFake;
	float causticMaxRough;

	int bucket_size_x;
	int bucket_size_y;
	int samples_per_pixel_in_pass;
	int pt_gi_samples;
	int pt_ris_samples;
	bool stop_after_pass;
	

	bool motion_blur_on;
	float motion_blur_time;
	float motion_blur_max_time;
	bool motion_blur_still;

	unsigned long long perfCounter;

	BSPTree	bsp;
	BVH bvh;

	Marsaglia marsaglia;
	SunSky * sunsky;
	bool useSunSky;

	int pixels_count;
	int overall_pixels_count;

	int randomLightMesh(float &prob);
	void logLightMeshStats();
	float evalPDF(int tri);
	float evalPDF(int instID, int tri);
	int findLightMesh(int nTri);
	int findLightMeshByInstanceAndTri(int instanceID, int nTri);
	float bb_minx, bb_miny, bb_minz;
	float bb_maxx, bb_maxy, bb_maxz;


};

//---------------------------------------------------------------------    SCENE

class Scene
{
private:
	void (*notifyProgressCallback)(char * message, float progress);
public:

	SceneStatic sscene;

	bList<Triangle, 128>	triangles;
	myList<MaterialNox*>	mats;
	myList<Camera *>		cameras;
	myList<LightMesh *>		lightMeshes;
	myList<SkyPortalMesh *> skyportalMeshes;
	bool * 					usedMaterials;

	EnvSphere				env;
	bList<Mesh, 64>			meshes;
	bList<MeshInstance, 64>	instances;
	BlendSettings			blendSettings;
	int	blendBits;
	myList<PresetPost *>	presets;

	TextureManager texManager;
	
	int numThreads;
	ThreadManager * thrM;
	HWND pps;
	int pixels_per_second;
	int overall_pixels_count;
	int progressRefreshTime;
	bool nowRendering;
	bool startAfterLoad;
	bool * abortThreadPtr;
	char * sceneFilename;
	char * scene_name;
	char * scene_author_name;
	char * scene_author_email;
	char * scene_author_www;
	unsigned int le_sc_name;
	unsigned int le_sc_author_name;
	unsigned int le_sc_author_email;
	unsigned int le_sc_author_www;
	char * postLoadPostFile;
	char * postLoadBlendFile;
	char * postLoadFinalFile;

	bool decodeSceneStrings();
	bool changeTexturePaths(char * mainPath, char * subDir);
	bool releaseRelativePaths();
	EMCurve * curve;
	void * embree_scene;

	int evalBlendBits();

	float intersectBVHforNonOpacFakeGlass(const float sample_time, const Point3d &origin, const Vector3d &direction, float &maxDist, Matrix4d & inst_mat, int &inst_id, int &tri, float &u, float &v, Color4 &attenuation, SceneStatic * customSS, bool allowRandomStop);
	float intersectBSPforNonOpacFakeGlass(const Point3d &origin, const Vector3d &direction, float &maxDist, int &tri, float &u, float &v, Color4 &attenuation, SceneStatic * customSS, bool allowRandomStop);
	float intersectBruteForce(const Point3d &origin, const Vector3d &direction, int &tri, const float &maxDist, float &u, float &v);

	int loadSceneFromXML(char * filename);
	int saveSceneToXML(char * filename, bool addCamBuffers);
	bool disposeEverything();

	bool presetAddCurrent(char * presetname);
	bool presetApply(int id);
	bool presetRemove(int id);


	bool mergeSceneFromBinary(char * filename);
	int loadSceneFromBinary(char * filename);
	int saveSceneToBinary(char * filename, bool addCamBuffers, bool silentMode=false);

	bool postLoadInitialize(bool toMatEditor);
	bool preRenderInitialize();
	bool postRenderReleaseAll();

	bool copyFromEditedMaterial(int matnum);
	bool copyToEditedMaterial(int matnum);
	bool deleteEditedMaterial();

	bool updateUsedMaterials();
	bool updateSkyportals();
	bool updateSkyportalsOld1();
	bool updateLightMeshes();
	bool assignMaterials();
	bool assignMeshIDs();
	bool removeDisplacementMeshes();
	bool createDisplacementMeshes();
	int createDisplacedMesh(int instID);
	int fillDisplacementHoles(int instID);
	void preEvalFresnels();
	void createLightMeshes();
	void copySceneStatic(SceneStatic * src, SceneStatic * dst);
	void deleteSceneStaticPointers(SceneStatic * ss);
	void copyImageRegionsSelected();
	Camera * getActiveCamera();

	int randomSkyPortal(float &prob);

	void registerProgressCallback(void (*print_progress_callback)(char * message, float progress), int miliSec);
	void releaseProgressCallback();
	bool notifyProgress(char * message, float progress);
	void refreshPixelsPerSecond();

	bool evalDepthMap(Camera * cam);
	bool evalSceneBoundingBox();

	Scene();
	~Scene();
};

//---------------------------------------------------------------------    RAYTRACER

class Raytracer
{
private:
	TDECLDIR Raytracer();
	TDECLDIR ~Raytracer();
	static Raytracer * instance;
public:
	static const int GI_METHOD_DEFAULT = 0;
	static const int GI_METHOD_PATH_TRACING = 1;
	static const int GI_METHOD_BIDIRECTIONAL_PATH_TRACING = 2;
	static const int GI_METHOD_METROPOLIS = 3;
	static const int GI_METHOD_INTERSECTIONS = 10;
	static const int GI_NO_GI = 21;
	static const int GI_NO_GI_SHOWNORMALS = 20;
	static const int GI_NO_GI_KD_TREE_NODES = 32;
	static const int GI_NO_GI_OCTREE_NODES = 33;
	static const int TONE_MAPPING_LINEAR = 1;
	static const int TONE_MAPPING_HSV = 2;

	DECLDIR static Raytracer * getInstance();
	DECLDIR static int getVersionMajor();
	DECLDIR static int getVersionMinor();
	DECLDIR static int getVersionBuild();

	DECLDIR int addNewScene();

	myList<Scene *> scenes;
	myList<char *> materialEditorSceneFilenames;
	myList<char *> materialEditorSceneNames;
	myList<int> matScenesIndices;
	myList<char*>texDirs;
	DECLDIR bool loadMatEditorScenes(void (*print_progress_callback)(char * message, float progress));
	DECLDIR bool loadBenchmarkScenes(void (*print_progress_callback)(char * message, float progress));
	int firstMatScene;
	MaterialNox * editedMaterial;
	Scene* curScenePtr;
	ImageBuffer logoStamp;
	int curSceneInd;
	bool showTexNotFoundDlg;
	int rayReflectionProbability;
	bool runDialogsUnicode;
	bool use_embree;
	void updateOpenCLStateOnGui();
	struct {
		int texAutoReloadMode;
		bool useOpenCLonPost;
		bool openCLsupported;
	} options;
	DECLDIR bool callDeleteEditedMaterial();
	DECLDIR bool callDeleteMaterial(MaterialNox * mat);
	DECLDIR MaterialNox * callNewMaterial();
	static DWORD tlsIndexRandom;
	int pluginMaxSceneId;
	float getRandomFloatThread();
	unsigned int getRandomIntThread(unsigned int maxval);
	static RandomGenerator * getRandomGeneratorForThread();
	RandomGenerator rgMain;
	struct {
		bool refreshTimeOn;
		bool stoppingTimerOn;
		bool autosaveTimerOn;
		int stopTimerHours;
		int stopTimerMinutes;
		int stopTimerSeconds;
		int refreshTime;
		int autosaveTimerMinutes;
		bool rn_active;
		bool rn_nextCam;
		bool rn_nCamDesc;
		bool rn_copyPost;
		bool rn_delBuffs;
		bool rn_saveImg;
		bool rn_changeSunsky;
		int rn_sunskyMins;
		int rn_fileFormat;
		char * rn_saveFolder;
	} gui_timers;

	DECLDIR bool setUserColor(const int &ucnum, const Color4 &c);
	DECLDIR bool getUserColor(const int &ucnum, Color4 &c);
};

//---------------------------------------------------------------------    EPIXEL

class EPixel
{
public:
	Color4 c;
	int x;
	int y;

	EPixel() {x=y=0; c.r=c.g=c.b=0; c.a=1;}
	EPixel(const int &X, const int &Y, const Color4 &C) {x=X; y=Y; c=C;}
};

//---------------------------------------------------------------------    THREAD MANAGER

class ThreadManager
{
private:
	bool threadsInitiated;
	int numThreads;
	int assignedNumbers;


	int fifoWait;
	bool cameraFifoThreadRunning;
	myFifo<EPixel, 1024> fifo;
	void createCameraFifoThread();
	bool orderToStop;
	void (*notifyStoppedCallback)(void);


public:
	RenderThread * threads;
	static CRITICAL_SECTION cs;
	bool initThreads(int numberThreads, bool inMatEditor);
	bool disposeThreads();
	bool runThreads();
	bool stopThreads();
	bool pauseThreads();
	bool resumeThreads();
	int getNumThreads();
	void setThreadsPriority(int pri);
	int assignNumber();
	static int getProcessorsNumber();
	static void showAllDlls();
	void showThreadsDlls(int ntry=-1);
	static INT_PTR getImageBase(char * filename);
	void reportFinishedWork();

	unsigned long long collectPerfCounters();
	unsigned long long collectGISamplesCount();

	static void getNextHaltonCoords(float &x, float &y);
	void addPixelColor(int x, int y, const Color4 &c, bool incrementHits=true);
	static void blockSemaphoreFromOutside();		/// be very careful !!!!!
	static void releaseSemaphoreFromOutside();		/// be very careful !!!!!
	static DWORD WINAPI cameraFifoThreadProc(LPVOID lpParameter);
	void registerStoppedCallback(void (*stopped_callback)(void));

	ThreadManager();
	~ThreadManager();
};

//---------------------------------------------------------------------    RENDER THREAD

class RenderThread
{
private:
	static DWORD WINAPI ThreadProc(LPVOID lpParameter);
public:
	HANDLE hThread;
	bool modeMatEditor;
private:
	bool running;
	bool paused;
	bool alive;
	DWORD threadId;
	int personalNumber;
public:
	myFifo<EPixel, 32> fifo;
	ThreadManager * my_manager;
	SceneStatic ss;
	unsigned long long getPerformanceCounter();
	RandomGenerator rg;

	bool isAlive();
	bool resetAfterDie();
	bool runThread();
	bool stopThread();
	bool pauseThread();
	bool resumeThread();
	void setPriority(int pri);

	RenderThread();
	~RenderThread();
};

//---------------------------------------------------------------------    THREAD DEPTH MAP 

class DepthMapThread
{
public:
	HANDLE hThread;
	DWORD threadId;
	int numAllThreads;
	int curThreadNum;
	float progress;	// 0 - 100
	bool jobDone;
	bool abort;
	Camera * cam;
	Scene * sc;

	FloatBuffer * depth;

	bool runThread();

	DepthMapThread();
	~DepthMapThread();
};

//---------------------------------------------------------------------    BOUNDING BOX

__declspec(align(16)) class BBox
{
public:
	Float4 bmin;
	Float4 bmax;

	BBox * children;
	Triangle4 * tris4;
	myList<int> tris;

	int divLeft;
	int triStart;
	int triNum;
	static int soFarIn;

	int num4; 
	int last4;

	unsigned char vchildren;
	bool isLeaf;

	BBox();
	BBox(const Point3d &Bmin, const Point3d &Bmax, const int &num, const int &divRecLeft);
	~BBox();

	bool intersectionSSE(const Float4 &origin, const Float4 &invdirection, const Float4 &maxDist);
	bool intersectionSSE(const Float4 &origin, const Float4 &invdirection);
	bool intersection(const Point3d &origin, const Vector3d &direction);
	bool intersection(const Point3d &origin, const Vector3d &direction, const float &maxDist);
	void intersectTrisPureSSE(const Float4 &origin, const Float4 &direction, const Float4 &invdirection, Float4 &isect);
	float intersectTrisSSE(const Float4 &origin, const Float4 &direction, const Float4 &invdirection, int &tri, float &u, float &v);
	float intersectTris(const Point3d &origin, const Vector3d &direction, int &tri, float &u, float &v);
	float intersectTris(const Point3d &origin, const Vector3d &direction, int &tri, const float &maxDist);
	bool divideVolume();
	bool tryToAdd(int tri);
	void addTrisRoot();
	void releaseRoot();
	void evalCenter();
	void copyTriangles();
	void releaseNonLeaf();
};

//---------------------------------------------------------------------    EMITTER

class LightMesh
{
public:
	float totalArea;		// total area of emissing triangles in mesh
	float totalPower;		// total power of mesh emission - the same as power in material
	int numTris;			// number of emissing tris in mesh
	int instNum;			// number/id of instance owning triangles with one material
	
	bList<int, 64> tris;	// list of tris (by id)
	float * triAreas;		// list of each tri's area
	float * triAreasSum;	// sum of areas from 1st tri

	void freeArrays();
	int randomTriangle(float &prob);
	LightMesh * copy();
	void deleteTris();		// deprecated
	LightMesh();
	~LightMesh();
};

//---------------------------------------------------------------------    SKYPORTAL

class SkyPortalMesh
{
public:
	int numTris;			// number of portal tris in mesh
	int instNum;			// number/id of instance owning triangles with one material
	float totalArea;		// total area of portal triangles in mesh
	float * triAreas;		// list of each tri's area
	float * triAreasSum;	// sum of areas from 1st tri

	bList<int, 64> tris;	// list of tris (by id)

	void freeArrays();
	int randomTriangle(float &prob, int &meshTriID);
	SkyPortalMesh * copy();
	SkyPortalMesh();
	~SkyPortalMesh();
};

//---------------------------------------------------------------------    FRESNEL

class Fresnel
{
private:
	float getValueCustom(float angle);				// f: [0 , 1] => [0 , 1]
	float getValueFresnel(float angle);				// f: [0 , 1] => [0 , 1]
public:
	float getValueFresnelFromCos(float kos);		// f: [0 , 1] => [0 , 1]
	static const int MODE_FRESNEL = 1;
	static const int MODE_CUSTOM = 2;
	int mode;

	float IOR;

	float X0, X1, X2, X3;
	float Y0, Y1, Y2, Y3;
	bool customPolynomial;	

	float getValue(float angle);	// f: [0 , 1] => [0 , 1]
	float getValueFromCos(float kos);

	float preEvaledValues[102];
	void preEval();
	float getPreEvaled(float kos);

	Fresnel();
	~Fresnel();

};

//---------------------------------------------------------------------    MATLAYER

//#define VERIFY_MATLAYER_BRDF_PDFS

class MatLayer
{
public:
	// sse on the beginning
	Color4 refl0;
	Color4 refl90;
	Color4 absColor;
	Color4 color;
	Color4 transmColor;
	Color4 gRefl0;
	Color4 gRefl90;
	Color4 gAbsColor;
	Color4 gColor;
	Color4 gTransmColor;

	static const int TYPE_EMITTER = 1;
	static const int TYPE_SHADE = 2;

	static const int EMISSION_WATT = 1;
	static const int EMISSION_WATT_PER_SQR_METER = 2;
	static const int EMISSION_LUMEN = 3;
	static const int EMISSION_LUX = 4;

	static const int BRDF_TYPE_SCHLICK = 1;
	static const int BRDF_TYPE_GGX = 2;
	static const int BRDF_TYPE_BLINN = 3;

	int type;
	int contribution;

	bool connect90;

	bool use_tex_weight;
	bool use_tex_col0;
	bool use_tex_col90;
	bool use_tex_rough;
	bool use_tex_light;
	bool use_tex_normal;
	bool use_tex_transm;
	bool use_tex_aniso;
	bool use_tex_aniso_angle;

	TextureInstance tex_weight;
	TextureInstance tex_col0;
	TextureInstance tex_col90;
	TextureInstance tex_rough;
	TextureInstance tex_light;
	TextureInstance tex_normal;
	TextureInstance tex_transm;
	TextureInstance tex_aniso;
	TextureInstance tex_aniso_angle;

	// shade type values:
	float roughness;
	Fresnel fresnel;
	int brdf_type;
	float anisotropy;
	float aniso_angle;

	// transmission stuff
	float absDist;		// absorption unit distance
	float dispersionValue;	// 0 - 1
	bool absOn;			// absorption enabled
	bool transmOn;		// transmission enabled
	bool fakeGlass;		// fake glass enabled
	bool dispersionOn;	// dispersion enabled
	float transluencyWeight;

	bool sssON;
	float sssDens;
	float sssCollDir;

	// emitter type values:
	float power;
	int emitterTemperature;
	int unit;
	IesNOX * ies;
	bool emissInvisible;

	DECLDIR bool addIes();
	DECLDIR void removeIes();

	Color4 getColor0(float tu, float tv);
	Color4 getColor90(float tu, float tv);
	float getRough(float tu, float tv);
	float getWeight(float tu, float tv);
	Color4 getLightColor(float tu, float tv);
	Vector3d getNormalMap(float tu, float tv);
	Color4 getTransmissionColor(float tu, float tv);
	float getAnisotropy(float tu, float tv);
	float getAnisoAngle(float tu, float tv);

	float getIlluminance(SceneStatic * ss, int ntri);
	float getIlluminance(SceneStatic * ss, int instanceID, int ntri);


	bool resetValues(bool delTex);
	void deleteAllTextures();
	bool copyFromOtherWithoutTextures(MatLayer * src);


	void preRenderProcess();

	bool isDeltaDistribution();	// don't use it
	
	Vector3d randomNewDirection(HitData & hd, float &probability);
	Color4 getBRDF(HitData & hd);
	float getProbability(HitData & hd);
	bool getBRDFandPDFs(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b);

	Vector3d randomNewDirectionHomogenous(HitData & hd, float &probability);
	float getProbabilityHomogenous(HitData & hd);

	Vector3d randomNewDirectionAshikhminShirley(HitData & hd, float &probability);
	Color4 getBRDFAshikhminShirley(HitData & hd);
	float getProbabilityAshikhminShirley(HitData & hd);

	Vector3d randomNewDirectionSchlick(HitData & hd, float &probability);
	Color4 getBRDFSchlick(HitData & hd);
	float getProbabilitySchlick(HitData & hd);

	Vector3d randomNewDirectionWalterMicrofacetGGX(HitData & hd, float &probability);
	Color4 getBRDFWalterMicrofacetGGX(HitData & hd);
	float getProbabilityWalterMicrofacetGGX(HitData & hd);
	float getProbabilityWalterMicrofacetGGX2(HitData & hd);
	bool getBRDFandPDFsGGX(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b);

	Vector3d randomNewDirectionWalterMicrofacetBeckmann(HitData & hd, float &probability);
	Color4 getBRDFWalterMicrofacetBeckmann(HitData & hd);
	float getProbabilityWalterMicrofacetBeckmann(HitData & hd);
	bool getBRDFandPDFsBeckmann(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b);

	Vector3d randomNewDirectionMicrofacetAniso(HitData & hd, float &probability);
	Color4 getBRDFMicrofacetAniso(HitData & hd);
	float getProbabilityMicrofacetAniso(HitData & hd);
	bool getBRDFandPDFsMicrofacetAniso(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b);

	Vector3d randomNewDirectionWalterMicrofacetBlinn(HitData & hd, float &probability);
	Color4 getBRDFWalterMicrofacetBlinn(HitData & hd);
	float getProbabilityWalterMicrofacetBlinn(HitData & hd);

	Vector3d randomNewDirectionLambertTransmission(HitData & hd, float &probability);
	Color4 getBRDFLambertTransmission(HitData & hd);
	float getProbabilityLambertTransmission(HitData & hd);
	bool getBRDFandPDFsLambertTransmission(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b);

	Vector3d randomNewDirectionLambert2(HitData & hd, float &probability);
	Vector3d randomNewDirectionLambert1(HitData & hd, float &probability);
	Vector3d randomNewDirectionLambert(HitData & hd, float &probability);
	Color4 getBRDFLambert(HitData & hd);
	float getProbabilityLambert(HitData & hd);
	bool getBRDFandPDFsLambert(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b);

	Vector3d randomNewDirectionPureSpecular(HitData & hd, float &probability);
	Color4 getBRDFPureSpecular(HitData & hd);
	float getProbabilitySpecular(HitData & hd);

	Vector3d randomNewDirectionIdealReflectionTransmission(HitData & hd, float &probability);
	Color4 getBRDFIdealReflectionTransmission(HitData & hd);
	float getProbabilityIdealReflectionTransmission(HitData & hd);
	bool getBRDFandPDFsIdealReflectionTransmission(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b);

	Vector3d randomNewDirectionDispersionGlass(HitData & hd, float &probability);
	Color4 getBRDFDispersionGlass(HitData & hd);
	float getProbabilityDispersionGlass(HitData & hd);

	MatLayer & operator= (const MatLayer & mlay);
	MatLayer(const MatLayer & mlay);

	static void runConstructor(void * addr);
	DECLDIR MatLayer();
	DECLDIR ~MatLayer();
};

//---------------------------------------------------------------------    MATERIAL

class MaterialNox
{
public:
	const static int NM_SMOOTH = 0;
	const static int NM_FACE = 1;
	const static int NM_SOURCE = 2;

	bList<MatLayer, 16> layers;
	
	unsigned int id;
	bool hasEmitters;
	bool hasShade;
	bool hasWeightTexShade;
	bool hasFakeGlass;
	bool hasInvisibleEmitter;
	bool isMatteShadow;
	bool isSkyPortal;
	char * name;
	float weightsSumEmit;
	float weightsSumShade;
	int prSceneIndex;
	int blendIndex;
	float opacity;
	bool use_tex_opacity;
	bool use_tex_displacement;
	bool displ_continuity;
	bool displ_ignore_scale;
	float displ_shift;
	int displ_normal_mode;
	TextureInstance tex_opacity;
	TextureInstance tex_displacement;
	float displ_depth;
	unsigned int displ_subdivs_lvl;

	ImageByteBuffer * preview;

	DECLDIR bool setName(char * newname);
	DECLDIR bool createEmptyPreview();
	DECLDIR bool addMatLayer(MatLayer &mlay);

	void logMaterial();

	float evalSumWeights(float tu, float tv);
	float evalSumWeightsShade(float tu, float tv);
	float evalSumWeightsEmitter(float tu, float tv);
	float getOpacity(float tu, float tv);
	float getDisplacement(float tu, float tv);
	bool savePreview(char * filename, int format);
	void prepareToRender();
	void updateFlags();
	void updateWeights();
	void checkProperties(bool &use_normals, bool &use_transmission, bool &use_sss, bool &use_emission);
	DECLDIR bool updateSceneTexturePointers(Scene * sc);
	DECLDIR MaterialNox * copy();
	DECLDIR bool deleteBuffers(bool delName=true, bool delPreview=true, bool delTextures=true, bool delIES=true);
	DECLDIR void deleteLayers();
	Color4 getRandomBRDF(const Vector3d &in, const Vector3d &out, const Vector3d &normal, float &probability, const Point2d &tUV);
	Color4 getAllLayersBRDF(HitData &hd);
	MatLayer * getRandomShadeLayer(float tu, float tv, float &probability);
	MatLayer * getRandomEmissionLayer(float tu, float tv, float &probability);
	MatLayer * getRandomFakeGlassLayer(float tu, float tv, float &probability);
	MatLayer * getRandomAnyLayer(float tu, float tv, float &probability);
	MatLayer * getRandomGlassLayer(float tu, float tv, float &probability, float maxRough);

	Color4 evalAllLayersFakeGlassAndOpacityAttenuation(HitData &hd, float &opac);
	DECLDIR MaterialNox();
	DECLDIR ~MaterialNox();
};

//---------------------------------------------------------------------    HITDATA

class HitData
{
public:
	Vector3d in;
	Vector3d out;
	Vector3d normal_shade;
	Vector3d normal_geom;
	Vector3d dir_U;
	Vector3d dir_V;
	float lastDist;
	float spectr_pos;

	float tU, tV;
	unsigned int flags;
	static const char FLAG_DELTA = (1<<0);
	static const char FLAG_INTERNAL_REFLECTION = (1<<1);
	static const char FLAG_INVALID_RANDOM = (1<<2);
	static const char FLAG_GHOST_REFLECTION = (1<<3);
	static const char FLAG_SSS = (1<<4);

	inline void setFlagDelta() { flags |= FLAG_DELTA; }
	inline void clearFlagDelta() { flags &= ~FLAG_DELTA; }
	inline void setFlagInternalReflection() { flags |= FLAG_INTERNAL_REFLECTION; }
	inline void clearFlagInternalReflection() { flags &= ~FLAG_INTERNAL_REFLECTION; }
	inline void setFlagInvalidRandom() { flags |= FLAG_INVALID_RANDOM; }
	inline void clearFlagInvalidRandom() { flags &= ~FLAG_INVALID_RANDOM; }
	inline void setFlagGhostReflection() { flags |= FLAG_GHOST_REFLECTION; }
	inline void clearFlagGhostReflection() { flags &= ~FLAG_GHOST_REFLECTION; }
	inline void setFlagSSS() { flags |= FLAG_SSS; }
	inline void clearFlagSSS() { flags &= ~FLAG_SSS; }
	inline void clearFlagsAll() { flags = 0; }
	inline bool isFlagDelta() { return ((flags & FLAG_DELTA)!=0); }
	inline bool isFlagInternalReflection() { return ((flags & FLAG_INTERNAL_REFLECTION)!=0); }
	inline bool isFlagInvalidRandom() { return ((flags & FLAG_INVALID_RANDOM)!=0); }
	inline bool isFlagGhostReflection() { return ((flags & FLAG_GHOST_REFLECTION)!=0); }
	inline bool isFlagSSS() { return ((flags & FLAG_SSS)!=0); }


	void adjustNormal();
	bool randomSpectrumPosFromColor(const Color4 &c);
	bool randomSpectrumPosSimple();
	inline void swapDirections() { Vector3d t=in; in=out; out=t; }


	HitData(Vector3d vIn, Vector3d vOut, Vector3d vNormShade, Vector3d vNormGeom, Vector3d vDirU, Vector3d vDirV, float txU, float txV, float lastDistance, float spectral_pos=-1.0f)
			{ in=vIn; out=vOut; normal_shade=vNormShade; normal_geom=vNormGeom; dir_U=vDirU; dir_V=vDirV; tU=txU; tV=txV; lastDist=lastDistance; spectr_pos=spectral_pos; flags=0; }
	HitData() {in=out=normal_shade=normal_geom=dir_U=dir_V=Vector3d(0,0,0); lastDist=BIGFLOAT; spectr_pos=-1.0f; tU=tV=0.0f; flags=0;}
	~HitData() {}
};

//---------------------------------------------------------------------    DIAPHRAGM SHAPE

class DiaphragmShape
{
public:
	float vert[32];
	float vec[32];
	float cx[16];
	float cy[16];
	float angles[16];

	int   numBlades;
	float aperture;
	float angle;
	float radius;
	bool  roundBlades;


	bool initializeDiaphragm(int NumBlades, float Aperture, float Angle, bool RoundBlades, float Radius);
	bool isPointInDiaphragm(float u, float v);
};

//---------------------------------------------------------------------    CAMERA

class Camera
{
public:
	ImageModifier iMod;
	FinalModifier fMod;
	Point3d position;
	Vector3d direction;
	Vector3d upDir;
	Vector3d rightDir;
	Matrix3d iMat;

private:
	float X2, Y2;
	unsigned int halton;
	
public:
	static unsigned int maxHalton;
	unsigned int threadID;
	Marsaglia * marsagliaPtr;
	Lattice lattice;
	ScanLine scanline;
	Bucket * buckets;
	int bucket_samples;
	int bucket_samples_cur;
	HWND bucket_hwnd_image;
	int cur_bucket_sample;
	int cur_bucket_i, cur_bucket_j;
	float cur_bucket_x, cur_bucket_y, cur_bucket_w, cur_bucket_h;
	bool runRefreshRegionThread(RedrawRegion rgn);

	int width;
	int height;
	int aa;
	float apertureField;
	float aperture;
	float ISO;
	float shutter;
	float angle;
	float cotg;
	float clipNear;
	float clipFar;
	float coneSize;	// in radians
	float coneCosSize;
	float conePix;	// in radians
	unsigned int rendTime;
	unsigned int startTick;
	unsigned int lastRendTime;
	bool mb_on;
	Vector3d mb_velocity;
	Vector3d mb_rot_vel;
	float mb_rot_angle;


	bool matrixOK;
	bool nowRendering;
	bool glareOK;
	bool depthOK;
	
	bool autoexposure;
	int autoexposureType;
	bool autoFocus;
	float focusDist;
	int afMode;

	bool dofOnTemp;

	int apBladesNum;
	float apBladesAngle;
	bool apBladesRound;
	float apBladesRadius;
	Random2DCircleSampler cSampler;
	DiaphragmShape diaphShape;
	DiaphragmSampler diaphSampler;
	bool pinhole;
	float scaleAperture;
	float mplApSh;	// multiplier for energy (time/aperture/aperture/2.5)


	ImageBuffer   blendBuffDirect[16];
	ImageBuffer   blendBuffGI[16];
	float blendWeights[16];
	int blendBits;
	int gi_type;

	FloatBuffer * depthBuf;
	ImageBuffer * imgBuff;
	ImageRegion * iReg;
	ImageRegion staticRegion;

	char * name;

	Texture texDiaphragm;
	Texture texObstacle;
	Texture texDiapObst;
	Texture texFourier;

	bool	copyDiaphFromShapeToSampler();
	int		getRendTime(DWORD tickCount, bool whole=true);
	Vector3d	getDirection(float x, float y, Point3d &sPos, float sample_time);
	bool	initializeCamera(bool withErase);
	void	randomNewCoords(float &x, float &y, int type, bool &noMore, bool isDirect=false);
	void	getHaltonCoords(float &x, float &y, bool isDirect=false);
	bool	getCoordsFromDir(const Vector3d &dir, const Point3d &dofPoint, float &x, float &y);
	bool	getCoordsFromDirReturnInFrustum(const Vector3d &dir, const Point3d &dofPoint, float &x, float &y);
	float	evalDistMult(float x, float y);
	bool	prepareInvertedMatrix();
	bool	setLength(float leng);
	bool	setLengthHori(float leng);
	DECLDIR bool	setLengthfromFovHori36(float fovHori);
	float	fovDiagToLength(float fov);
	float	lengthToFovDiag(float leng);
	float	lengthToFovHori(float leng);
	bool	createApertureShapePreview(ImageByteBuffer * img);	// img must be allocated
	void	createDiaphragmAndObstacleImage(int widthheight);
	bool	createGlarePattern(int widthheight);
	void	updateGlobalHalton() { maxHalton = halton; }
	void	randomAperturePoint(float &x, float &y);	// [-1..1][-1..1]
	void	tempShowBufferAddress();
	void	resetFinalValues();
	void	evalConeCosSize();

	bool	mergeDataFromOtherCameraFile(Camera * cam);
	bool	deleteStuffAfterMerge();
	bool	releaseAllBuffers();

	ImageBuffer *	blendBuffers(bool addDirect=true, bool addGI=true, bool giHitsPerPixel=true, RedrawRegion * rgn = NULL, ImageModifier * imod=NULL, bool smartGI=false);

	bool	assignName(char * nname);
	void	setSeed(unsigned int seed) {halton = seed; }
	unsigned int	getSeed() {return halton;}
	Camera *	copy();

	static const int RND_TYPE_SCANLINE = 0;
	static const int RND_TYPE_HALTON = 1;
	static const int RND_TYPE_MARSAGLIA = 2;
	static const int RND_TYPE_RAND = 3;
	static const int RND_TYPE_RAND_S = 4;
	static const int RND_TYPE_LATTICE = 5;
	static const int RND_TYPE_BUCKETS = 6;
	static const int GI_TYPE_PT = 0;
	static const int GI_TYPE_BDPT = 1;
	static const int GI_TYPE_MLT = 2;

	DECLDIR Camera();
	DECLDIR ~Camera();
};


//---------------------------------------------------------------------    SUNSKY

typedef float ABCDEFGHIJ[9];

class _CRT_ALIGN(16) SunSky
{
private:
public:
	Vector3d sunDir;
	Vector3d dirN, dirS, dirW, dirE, dirU;
private:
	Color4 getSunOutsideColor();
	float Ax,Ay,AY;
	float Bx,By,BY;
	float Cx,Cy,CY;
	float Dx,Dy,DY;
	float Ex,Ey,EY;

	float getAx() {return -0.0193f * turbidity - 0.2592f;}
	float getBx() {return -0.0665f * turbidity + 0.0008f; }
	float getCx() {return -0.0004f * turbidity + 0.2125f; }
	float getDx() {return -0.0641f * turbidity - 0.8989f; }
	float getEx() {return -0.0033f * turbidity + 0.0452f; }

	float getAy() {return -0.0167f * turbidity - 0.2608f; }
	float getBy() {return -0.0950f * turbidity + 0.0092f; }
	float getCy() {return -0.0079f * turbidity + 0.2102f; }
	float getDy() {return -0.0441f * turbidity - 1.6537f; }
	float getEy() {return -0.0109f * turbidity + 0.0529f; }

	float getAY() {return  0.1787f * turbidity - 1.4630f; }
	float getBY() {return -0.3554f * turbidity + 0.4275f; }
	float getCY() {return -0.0227f * turbidity + 5.3251f; }
	float getDY() {return  0.1206f * turbidity - 2.5771f; }
	float getEY() {return -0.0670f * turbidity + 0.3703f; }

	float getZenithx();
	float getZenithy();
	float getZenithY();
	float getx(float costheta, float gamma);
	float gety(float costheta, float gamma);
	float getY(float costheta, float gamma);

	float zx,zy,zY;

	float cosThetaS;
	float Fx(float costheta, float gamma);
	float Fx(float costheta, float gamma, float cosGamma);
	float Fy(float costheta, float gamma);
	float Fy(float costheta, float gamma, float cosGamma);
	float FY(float costheta, float gamma);
	float FY(float costheta, float gamma, float cosGamma);

	int month, day, hour;
	float minute;
	float longitude, latitude;
	float sinlat, sinlong, coslat, coslong;

	float solarTimeInHours;
	float solarDeclination;
	float solarGlobalTimeInHours;
	float realPhiS;
	float realThetaS;
	Vector3d realSunDir;
	Color4 sunColor;
	
	ABCDEFGHIJ hosek_abc[3];
	float hosek_radiances[3];

	void evalEarthTimeInHours();
	void calcSunPosition();
	Color4 getHosekSkyColor(Vector3d dir);

public:
	float turbidity;
	float albedo;
	float thetaS, phiS;
	float aerosol;
	bool sunOn;
	bool sunOtherBlendLayer;
	bool use_hosek;
	float sunSize;
	float sunSizeSteradians;
	float maxCosSunSize;
	DECLDIR bool copyFrom(SunSky * other);
	DECLDIR bool updateHosekData();

	DECLDIR void setStandardMeridianForTimeZone(float inRadians);
	void setSunDir(const Vector3d &sDir, const float &turb);
	DECLDIR void setDate(int Month, int Day, int Hour, float Minute);
	DECLDIR void setPosition(float Longitude, float Latitude);

	bool isEnlighted(float ttheta, float tphi);
	unsigned char getEnlightedAlpha(float ttheta, float tphi);


	DECLDIR int getDayOfYear();
	float earthTimeInHours;
	float standardMeridianForTimeZone;


	int getMonth() {return month;}
	int getDay() {return day;}
	int getHour() {return hour;}
	float getMinute() {return minute;}
	float getLongitude() {return longitude;}
	float getLatitude() {return latitude; }


	float getSolarTimeInHours() {return solarTimeInHours;}
	float getSolarGlobalTimeInHours() {return solarGlobalTimeInHours;}
	float getSolarDeclination() {return solarDeclination;}
	float getMeridianForTimeZone() {return standardMeridianForTimeZone;}

	Vector3d getSunDirection();
	Vector3d getRandomPerturbedSunDirection();
	float getSunSizeSteradians();


	Color4 getColor(const Vector3d &direction);
	Color4 getSunColor();
	void evalSunColor();
	Vector3d randomDirOnHemisphere();

	void makeLog();

	bool evalAerialPerspective(Vector3d dir, float dist, Color4 &atten, Color4 &skyIn, Color4 &sunIn);

	DECLDIR SunSky();
	DECLDIR ~SunSky();
};

//---------------------------------------------------------------------    VALIDATOR

class Validator
{
private:
	Raytracer * rtr;
public:
	int error;
	static const int SCENE_OK = 0;
	static const int SCENE_ERROR_NO_CAMERA = 1;
	static const int SCENE_ERROR_NO_ACTIVE_CAMERA = 1;
	static const int SCENE_ERROR_NO_EMISSION = 2;
	static const int SCENE_ERROR_NO_TRIANGLES = 3;
	static const int SCENE_ERROR_NO_BOUNDING_BOXES = 4;

	bool checkScene();
	Validator(Raytracer * raytr);
	~Validator();
};

//---------------------------------------------------------------------    TEMPERATURE COLOR

class TemperatureColor
{
public:
	static Color4 getEnergyColor(int temperature);
	static Color4 getGammaColor(int temperature);
};

//---------------------------------------------------------------------

class UserColors
{
public:
	static Color4 colors[18];
};

//---------------------------------------------------------------------

class PresetPost
{
public:
	BlendSettings blend;
	ImageModifier iMod;
	FinalModifier fMod;
	char * name;
	char * texGlareObstacle;
	char * texGlareDiaphragm;

	PresetPost();
	~PresetPost();
};

//---------------------------------------------------------------------

#endif

