#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>


#define PMAX 8
float pdfDiff[] = { 0.15f,  0.3f,  0.125f,  0.0625f,  0.03125f,  0.015625f,  0.0078125f,  0.00390625f,  0.001953125f,  0.0009765625f,  0.00048828125f,  0.000244140625f };

//--------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::mutateDiffuse()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	
	int contDiff = 0;
	bool gotDiff = false;
	int mStart = -1;
	int mStop = -1;
	int cntr = 0;
	bool mutPossible = false;
	for (int i=1; i<=numEP; i++)
	{
		cntr++;
		bool rOK = (ePath[i].rough > 0);
		if (rOK)
		{
			if (mStart < 0)
				mStart = cntr;
		}
		else
		{
			if (mStart > 0)
			{
				mStop = cntr-1;
				if (mStop - mStart > 1)
					mutPossible = true;
				else
					return false;	// 0-2 vertices can't be mutated
			}
		}
	}

	for (int i=numLP; i>0; i--)
	{
		cntr++;
		bool rOK = (lPath[i].rough > 0);
		if (rOK)
		{
			if (mStart < 0)
				mStart = cntr;
		}
		else
		{
			if (mStart > 0)
			{
				mStop = cntr-1;
				if (mStop - mStart > 1)
					mutPossible = true;
				else
					return false;	// 0-2 vertices can't be mutated
			}
		}
	}

	if (mStart>0  && mStop < 0)
		mStop = cntr;

	int maxMut = mStop - mStart - 1;
	if (maxMut < 1)
		return false;
	maxMut = min(maxMut, PMAX);
	
	float psum = 0;
	for (int i=maxMut; i>=1; i--)
		psum += pdfDiff[i-1];
	float r1 = rtr->getRandomFloatThread();
	r1 *= psum;
	float pn = 0;
	int ndel = 1;
	for (int i=maxMut; i>=1; i--)
	{
		pn += pdfDiff[i-1];
		if (pn > r1)
		{
			ndel = i;
			break;
		}
	}
	int nd = rtr->getRandomIntThread(10000) % (ndel+1);
	dm_del_eye = nd;
	dm_del_light = ndel-nd;
	bm_start = mStart;
	bm_stop = mStart + ndel + 1;

	// --------------------------------------    MUTATE EYE SIDE PATH
	for (int i=bm_start; i<bm_start+dm_del_eye; i++)
	{
		MLTVertex * curPath = NULL;
		MLTVertex * origPath = NULL;
		MLTVertex * nextPath = NULL;
		MLTVertex * nextOrigPath = NULL;
		bool eyeSide = true;
		bool nextEyeSide;
		if (i<=numEP)
		{
			curPath = &(eCandidate[i]);
			origPath = &(ePath[i]);
			eyeSide = true;
		}
		else
		{
			curPath = &(lCandidate[numLP-(i-numEP-1)]);
			origPath = &(lPath[numLP-(i-numEP-1)]);
			eyeSide = false;
		}
		if (i+1 <= numEP)
		{
			nextPath = &(eCandidate[i+1]);
			nextOrigPath = &(ePath[i+1]);
			nextEyeSide = true;
		}
		else
		{
			nextPath = &(lCandidate[numLP-(i-numEP)]);
			nextOrigPath = &(lPath[numLP-(i-numEP)]);
			nextEyeSide = false;
		}

		CHECK(curPath->mlay);
		HitData hd;
		float ndpdf;
		curPath->fillHitData(hd, eyeSide);
		hd.clearFlagInvalidRandom();
		Vector3d newDir = curPath->mlay->randomNewDirection(hd, ndpdf);
		if (hd.isFlagInvalidRandom())
			return false;
		if (ndpdf < 0.001f)
			return false;
		if (eyeSide)
			curPath->outDir = newDir;
		else
			curPath->inDir = newDir;

		Point3d cPoint = curPath->pos + curPath->normal * (newDir*curPath->normal>0 ? 0.0001f : -0.0001f);

		// send a ray
		float u=0, v=0;
		int chosen = -1, inst_id = -1;
		float bigf = BIGFLOAT;
		Color4 att = Color4(1,1,1);
		Matrix4d inst_matr;

		float d = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time, cPoint, newDir, bigf, inst_matr, inst_id, chosen, u, v, att, ss, false);
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.0001f)
			return false;	// nothing intersected

		Triangle * tri = &(scene->triangles[chosen]);

		MaterialNox * mat = scene->mats[scene->instances[inst_id].materials[tri->matInInst]];

		MatLayer * mlay = NULL;
		float tu, tv;
		tri->evalTexUV(u,v, tu, tv);

		if (mat == nextOrigPath->mat)
		{
			mlay = nextOrigPath->mlay;
		}
		else
		{
			float rlpdf;
			mlay = mat->getRandomShadeLayer(tu, tv, rlpdf);
		}
		CHECK(mlay);

		nextPath->instanceID = inst_id;
		nextPath->inst_mat = inst_matr;
		nextPath->atten = att;
		nextPath->tri = tri;
		nextPath->mat = mat;
		nextPath->mlay = mlay;
		nextPath->u = u;
		nextPath->v = v;
		nextPath->fetchRoughness();
		if (nextPath->rough == 0)
			return false;
		nextPath->pos = inst_matr * tri->getPointFromUV(u,v);
		nextPath->normal = (inst_matr.getMatrixForNormals() * tri->evalNormal(u, v)).getNormalized();
		if (nextEyeSide)
			nextPath->inDir = newDir * -1;
		else
			nextPath->outDir = newDir * -1;
	}

	// --------------------------------------    MUTATE LIGHT SIDE PATH
	for (int i=bm_stop; i>bm_stop-dm_del_light; i--)
	{
		MLTVertex * curPath = NULL;
		MLTVertex * origPath = NULL;
		MLTVertex * nextPath = NULL;
		MLTVertex * nextOrigPath = NULL;
		bool eyeSide = true;
		bool nextEyeSide;
		if (i<=numEP)
		{
			curPath = &(eCandidate[i]);
			origPath = &(ePath[i]);
			eyeSide = true;
		}
		else
		{
			curPath = &(lCandidate[numLP-(i-numEP-1)]);
			origPath = &(lPath[numLP-(i-numEP-1)]);
			eyeSide = false;
		}
		if (i-1 <= numEP)
		{
			nextPath = &(eCandidate[i-1]);
			nextOrigPath = &(ePath[i-1]);
			nextEyeSide = true;
		}
		else
		{
			nextPath = &(lCandidate[numLP-(i-2-numEP)]);
			nextOrigPath = &(lPath[numLP-(i-2-numEP)]);
			nextEyeSide = false;
		}

		CHECK(curPath->mlay);
		HitData hd;
		float ndpdf;
		curPath->fillHitData(hd, !eyeSide);
		Vector3d newDir = curPath->mlay->randomNewDirection(hd, ndpdf);
		if (hd.isFlagInvalidRandom())
			return false;
		if (ndpdf < 0.001f)
			return false;
		if (eyeSide)
			curPath->inDir = newDir;
		else
			curPath->outDir = newDir;

		Point3d cPoint = curPath->pos + curPath->normal * (newDir*curPath->normal>0 ? 0.0001f : -0.0001f);

		// send a ray
		float u=0, v=0;
		int chosen = -1, inst_id = -1;
		float bigf = BIGFLOAT;
		Color4 att = Color4(1,1,1);
		Matrix4d inst_matr;
		float d = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time, cPoint, newDir, bigf, inst_matr, inst_id, chosen, u, v, att, ss, false);
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.0001f)		// not less than 0.1mm
			return false;	// nothing intersected

		Triangle * tri = &(scene->triangles[chosen]);
		MaterialNox * mat = scene->mats[scene->instances[inst_id].materials[tri->matInInst]];
		MatLayer * mlay = NULL;
		float tu, tv;
		tri->evalTexUV(u,v, tu, tv);

		if (mat == nextOrigPath->mat)
		{
			mlay = nextOrigPath->mlay;
		}
		else
		{
			float rlpdf;
			mlay = mat->getRandomShadeLayer(tu, tv, rlpdf);
		}
		CHECK(mlay);

		nextPath->instanceID = inst_id;
		nextPath->inst_mat = inst_matr;
		nextPath->atten = att;
		nextPath->tri = tri;
		nextPath->mat = mat;
		nextPath->mlay = mlay;
		nextPath->u = u;
		nextPath->v = v;
		nextPath->fetchRoughness();
		if (nextPath->rough == 0)
			return false;
		nextPath->pos = inst_matr * tri->getPointFromUV(u,v);
		nextPath->normal = (inst_matr.getMatrixForNormals() * tri->evalNormal(u, v)).getNormalized();
		if (nextEyeSide)
			nextPath->outDir = newDir * -1;
		else
			nextPath->inDir = newDir * -1;
	}

	// CONNECTION
	int lnc = bm_stop-dm_del_light;
	int enc = bm_start+dm_del_eye;

	MLTVertex * lConn = (lnc<=numCEP ? &(eCandidate[lnc]) : &(lCandidate[numCLP-(lnc-1-numCEP)]));
	MLTVertex * eConn = (enc<=numCEP ? &(eCandidate[enc]) : &(lCandidate[numCLP-(enc-1-numCEP)]));
	Vector3d leDir = eConn->pos - lConn->pos;
	leDir.normalize();
	if (lnc<=numCEP)
		lConn->inDir = leDir;
	else
		lConn->outDir = leDir;
	if (enc<=numCEP)
		eConn->outDir = -leDir;
	else
		eConn->inDir = -leDir;

	// CHECK BRDFs
	HitData lhd, ehd;
	lConn->fillHitData(lhd, (lnc>numCEP) );
	eConn->fillHitData(ehd, (enc<=numCEP) );
	lConn->brdf = lConn->mlay->getBRDF(lhd);
	eConn->brdf = eConn->mlay->getBRDF(ehd);
	if (eConn->brdf.isBlack())
		return false;
	if (lConn->brdf.isBlack())
		return false;

	// CHECK FOR SHADOW
	Color4 connColor = visibilityTestFakeGlassAndOpacity(lConn, eConn);
	
	if (connColor.isBlack())
		return false;

	if (enc<numCEP)
		eCandidate[enc+1].atten = connColor;
	if (enc==numCEP)
		connectionAttenuationCandidate = connColor;
	if (enc>numCEP)
		lCandidate[numCLP-(enc-numCEP)].atten = connColor;

	return true;
}

//--------------------------------------------------------------------------------------------------------------------------------------

float Metropolis::tentativeTransitionForDiffuse(bool forward)
{
	int nL, nE;
	MLTVertex * ePtr;
	MLTVertex * lPtr;
	if (forward)
	{
		ePtr = eCandidate;
		lPtr = lCandidate;
		nL = numCLP;
		nE = numCEP;
	}
	else
	{
		ePtr = ePath;
		lPtr = lPath;
		nL = numLP;
		nE = numEP;
	}

	PMLTVertex mPtr[MLT_MAX_VERTICES*2];
	float G[MLT_MAX_VERTICES*2];
	float pLE[MLT_MAX_VERTICES*2];
	float pEL[MLT_MAX_VERTICES*2];
	Color4 brdf[MLT_MAX_VERTICES*2];
	double pST[MLT_MAX_VERTICES*2];
	float weight[MLT_MAX_VERTICES*2];

	int ndel = dm_del_eye + dm_del_light;

	for (int i=0; i<ndel+2; i++)
		mPtr[i] = (i+bm_start <= nE) ? &(ePtr[i+bm_start]) : &(lPtr[nL-(i+bm_start-nE-1)]);
	int nC = nE-bm_start;


	// eval G
	for (int i=0; i<ndel+1; i++)
	{
		Vector3d v1 = mPtr[i+1]->pos - mPtr[i]->pos;
		float d2 = v1.lengthSqr();
		if (d2 < 0.00000001f)
			return 0;
		float k1,k2;
		if (i<=nC)
			k1 = mPtr[i]->outDir  * mPtr[i]->normal;
		else
			k1 = mPtr[i]->inDir  * mPtr[i]->normal;
		if (i<nC)
			k2 = mPtr[i+1]->inDir * mPtr[i+1]->normal;
		else
			k2 = mPtr[i+1]->outDir * mPtr[i+1]->normal;
		if (d2 > 0.00000001f)
			G[i] = fabs(k1*k2)/d2;
		else
			return 0;
	}

	// eval pLE & pEL & brdf
	for (int i=0; i<ndel+2; i++)
	{
		HitData hd;
		mPtr[i]->fillHitData(hd, (i<=nC));
		hd.lastDist = 1;
		if (i>0)
			hd.lastDist = (mPtr[i]->pos - mPtr[i-1]->pos).length();
		pLE[i] = 0.0f;
		pEL[i] = 0.0f;	
		brdf[i] = Color4(1,1,1);

		if (mPtr[i]->mlay)
		{
			float tcos = fabs(hd.out*hd.normal_shade);
			if (tcos > 0.001f)
				pEL[i] = mPtr[i]->mlay->getProbability(hd);
			else
				pEL[i] = 0;

			Vector3d tvec = hd.in;
			hd.in = hd.out;
			hd.out = tvec;

			tcos = fabs(hd.out*hd.normal_shade);
			if (tcos > 0.001f)
				pLE[i] = mPtr[i]->mlay->getProbability(hd);
			else
				pLE[i] = 0;

			brdf[i] = mPtr[i]->mlay->getBRDF(hd);		// L->x->E
		}
	}

	// eval probabilities for whole paths
	for (int i=0; i<ndel+1; i++)
	{
		pST[i] = 1.0f;
		for (int j=0; j<i; j++)
			pST[i] *= pEL[j] / fabs(j<=nC?mPtr[j]->outCosine:mPtr[j]->inCosine) * G[j];		// projected solid angle pdf * G
		for (int j=ndel+1; j>i+1; j--)
			pST[i] *= pLE[j] / fabs(j<=nC?mPtr[j]->inCosine:mPtr[j]->outCosine) * G[j-1];	// projected solid angle pdf * G
	}

	double sumPdf = 0.0;
	for (int i=0; i<ndel+1; i++)
		sumPdf += pST[i]*pST[i];

	if (sumPdf <= 0.0f)
		return 0.0f;

	for (int i=0; i<ndel+1; i++)
		weight[i] = (float)(pST[i]*pST[i] / sumPdf);

	Color4 cBrdf = Color4(0,0,0);
	for (int i=0; i<ndel+1; i++)
	{
		Color4 pBrdf = Color4(1,1,1);
		for (int j=0; j<i; j++)
		{
			if (pEL[j] > 0.00001f)
				pBrdf *= brdf[j] * fabs(j<=nC?mPtr[j]->outCosine:mPtr[j]->inCosine) * (1.0f/pEL[j]);
			else
				pBrdf = Color4(0,0,0);
		}
		
		for (int j=ndel+1; j>i+1; j--)
		{
			if (pLE[j] > 0.00001f)
			{
				pBrdf *= brdf[j] * fabs(j<=nC?mPtr[j]->inCosine:mPtr[j]->outCosine) * (1.0f/pLE[j]);
			}
			else
				pBrdf = Color4(0,0,0);
		}
		
		pBrdf *= brdf[i] * brdf[i+1] * G[i];
		cBrdf += pBrdf * weight[i];
	}

	if (cBrdf.isInf()  ||  cBrdf.isInf())
		return 0;
	if (cBrdf.isBlack())
		return 0;

	Color4 fakeAndOpacityAttenuation = Color4(1,1,1);
	if (forward)
	{
		fakeAndOpacityAttenuation = connectionAttenuationCandidate;
		for (int i=1; i<=numCEP; i++)
			fakeAndOpacityAttenuation *= eCandidate[i].atten;
		for (int i=1; i<=numCLP; i++)
			fakeAndOpacityAttenuation *= lCandidate[i].atten;
	}
	else
	{
		fakeAndOpacityAttenuation = connectionAttenuation;
		for (int i=1; i<=numEP; i++)
			fakeAndOpacityAttenuation *= ePath[i].atten;
		for (int i=1; i<=numLP; i++)
			fakeAndOpacityAttenuation *= lPath[i].atten;
	}
	cBrdf *= fakeAndOpacityAttenuation;

	float R = ( 0.299f * cBrdf.r  +  0.587f * cBrdf.g  +  0.114f * cBrdf.b );
	return R;
}

//--------------------------------------------------------------------------------------------------------------------------------------

