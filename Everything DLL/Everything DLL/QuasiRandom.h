#ifndef __QUASI_RANDOM_H
#define __QUASI_RANDOM_H

#include "Colors.h"

#include <vector>
using namespace std;


double getHalton(unsigned int number, int base);
void getDoubleHalton(int number, float &x, float &y);

unsigned int getMarsaglia(unsigned int seed1 = 0, unsigned int seed2 = 0);
#define MAXMARS 0xFFFFFFFF
#define getMarsagliaFloat() ((float)((double)getMarsaglia()/(double)MAXMARS))


class Random2DCircleSampler
{
private:
	static const int GRID_SIZE = 32;
	vector<int> grid[GRID_SIZE][GRID_SIZE];

	float *s;
	int numSamples;
	int cur;
public:
	void create(int num, int triesPerPoint);
	void getNext(float &x, float &y);
	void getThat(int n, float &x, float &y);
	int  getNumPoints() { return numSamples; }
	void freeSamples();
	void zeroEverything();	// be very careful, memory may leak

	Random2DCircleSampler();
	~Random2DCircleSampler();
};




class DiaphragmSampler
{
protected:
	static const int GRID_SIZE = 32;
	vector<int> grid[GRID_SIZE][GRID_SIZE];

	float *s;
	float *chrS;
	int numSamples;
	int cur;
public:
	float vert[32];
	float vec[32];
	float cx[16];
	float cy[16];
	float angles[16];
	float ABCM[64];

	int   numBlades;
	float aperture;
	float angle;
	float radius;
	bool  roundBlades;
	int intBokehRingSize;
	int intBokehRingBalance;
	int intBokehFlatten;
	int intBokehVignette;
	float bokehRingSize;
	float bokehRingBalance;
	float bokehFlatten;
	float bokehVignette;
	
	float chromaticShiftLens;
	float chromaticShiftDepth;
	float chromaticAchromatic;

	bool initializeDiaphragm(int NumBlades, float Aperture, float Angle, bool RoundBlades, float Radius);
	bool isPointInDiaphragm(float u, float v, float &w, float &d, float other_aperture = -1.0f);
	float getDistAbsPreevaled(int n, float px, float py);

	void create(int num, int triesPerPoint, bool updateProgress=false);
	void copyPointsFromOther(DiaphragmSampler * other);
	void getNext(float &x, float &y);
	void getThat(int n, float &x, float &y);
	int  getNumPoints() { return numSamples; }
	void freeSamples();
	void zeroEverything();	// be very careful, memory may leak
	bool createApertureShapePreview(ImageByteBuffer * img);	// img must be allocated
	bool createApertureShapePreviewChromaticBokeh(ImageByteBuffer * img, float depth);	// img must be allocated
	HBITMAP createApertureShapePreviewAlpha(HDC hdc, int size);
	HBITMAP createApertureShapePreviewChromaticBokehAlpha(HDC hdc, int size, float depth);

	bool allocSpaceChromaticSampleColors();
	bool freeSpaceChromaticSampleColors();
	bool evalChromaticSampleColors(float depth, int numSampl);
	bool getChrColorSample(unsigned int id, float &r, float &g, float &b);


	DiaphragmSampler();
	~DiaphragmSampler();
};





#endif

