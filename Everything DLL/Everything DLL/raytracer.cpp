#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "XML_IO.h"
#include "raytracer.h"
#include "QuasiRandom.h"
#include "PathTracing.h"
#include "Metropolis.h"
#include "log.h"
#include <locale.h>
#include "BinaryScene.h"
#include "embree.h"
extern float blackbody[];
DWORD Raytracer::tlsIndexRandom;

extern clock_t bspticks;
clock_t renderticks = 0;

extern clock_t localticks;
extern char  * defaultDirectory;

_locale_t noxLocale;
_locale_t noxLocaleComa;

bool isOpenCLSupported();

Raytracer * Raytracer::instance = 0;

Raytracer * Raytracer::getInstance()
{
	if (!instance)
		instance = new Raytracer();
	return instance;
}

bool xorString(char * data, char * pswd, unsigned int ldata, unsigned int lpswd)
{
	CHECK(data);
	CHECK(pswd);
	CHECK(ldata);
	CHECK(lpswd);

	for (unsigned int i=0; i<ldata; i++)
	{
		data[i] = data[i] ^ pswd[i%lpswd];
	}

	return true;
}

Scene::Scene():
	triangles(0),
	meshes(0),
	instances(0)
{
	sceneFilename = NULL;
	sscene.maxTrisPerBox = 80;
	sscene.maxBoxDivs = 6;
	sscene.bounces = 10;
	sscene.numProbesFirstGI = 10;
	sscene.sMin = Point3d(BIGFLOAT, BIGFLOAT, BIGFLOAT);
	sscene.sMax = Point3d(-BIGFLOAT, -BIGFLOAT, -BIGFLOAT);
	sscene.activeCamera = 0;
	sscene.scaleScene = 1.0f;
	sscene.perScaleScene = 1.0f;

	numThreads = 2;
	thrM = new ThreadManager();
	sscene.giMethod = Raytracer::GI_METHOD_PATH_TRACING;
	pixels_per_second = 0;
	overall_pixels_count = 0;
	pps = 0;
	sscene.tone_mapping = Raytracer::TONE_MAPPING_HSV;
	#ifdef FORCE_SSE
		sunsky = (SunSky *)_aligned_malloc(sizeof(SunSky), 16);
		*sunsky = SunSky();
	#else
		sscene.sunsky = new SunSky();
	#endif
	sscene.useSunSky = true;
	sscene.useSunSky = false;
	sscene.lightMeshPowers = NULL;
	sscene.lightMeshSumPowers = NULL;
	sscene.useSkyPortals = false;

	sscene.perfCounter = 0;

	sscene.defaultMat = new MaterialNox();
	MatLayer mLay;
	mLay.type = MatLayer::TYPE_SHADE;
	mLay.contribution = 100;
	mLay.refl0  = Color4(1,1,1);
	mLay.refl90 = Color4(1,1,1);
	mLay.transmColor = Color4(1,1,1);
	sscene.defaultMat->layers.add(mLay, &(MatLayer::runConstructor));
	sscene.defaultMat->layers.createArray();
	sscene.editableMaterial = 0;
	sscene.mats = &mats;
	sscene.lightMeshes = &lightMeshes;
	notifyProgressCallback = 0;
	progressRefreshTime = 200;
	sscene.sunsky->setPosition(0, 0);
	sscene.sunsky->setStandardMeridianForTimeZone(0);
	nowRendering = false;
	abortThreadPtr = NULL;
	startAfterLoad = false;
	usedMaterials = NULL;
	sscene.skyPortalsTotalArea = 0.0f;
	sscene.random_type = Camera::RND_TYPE_LATTICE;
	sscene.random_type = Camera::RND_TYPE_SCANLINE;
	sscene.disableSecondaryCaustic = false;
	sscene.causticMaxRough = 0.0f;
	sscene.causticFake = false;
	sscene.motion_blur_on = false;
	sscene.motion_blur_time = 0.0f;
	sscene.motion_blur_max_time = 0.0f;
	sscene.motion_blur_still = false;

	scene_name = NULL;
	scene_author_name = NULL;
	scene_author_email = NULL;
	scene_author_www = NULL;
	le_sc_name = 0;
	le_sc_author_name = 0;
	le_sc_author_email = 0;
	le_sc_author_www = 0;

	postLoadPostFile = NULL;
	postLoadBlendFile = NULL;
	postLoadFinalFile = NULL;

	sscene.bucket_size_x = 20;
	sscene.bucket_size_y = 20;
	sscene.samples_per_pixel_in_pass = 5;
	sscene.stop_after_pass = false;
	sscene.pt_gi_samples = 1;
	sscene.pt_ris_samples = 1;

	sscene.drawBufDirect = true;
	sscene.drawBufGI = true;
	sscene.drawStampLogo = false;
	sscene.drawStampTime = false;
	sscene.bb_minx = sscene.bb_miny = sscene.bb_minz = 0.0f;
	sscene.bb_maxx = sscene.bb_maxy = sscene.bb_maxz = 0.0f;

	curve = NULL;
	embree_scene = NULL;
}

Scene::~Scene()
{
	if (sscene.defaultMat)
		delete sscene.defaultMat;
	sscene.defaultMat = NULL;
	if (thrM)
		delete thrM;
	DeleteCriticalSection(&ThreadManager::cs);
}

Raytracer::Raytracer() 
{
	noxLocale = _create_locale(LC_NUMERIC, "");
	noxLocale->locinfo->lconv->decimal_point[0]='.';
	noxLocaleComa = _create_locale(LC_NUMERIC, "");
	noxLocaleComa->locinfo->lconv->decimal_point[0]=',';

	use_embree = false;
	#ifdef USE_EMBREE
		use_embree = embreeSystemMinimum();
	#endif

	InitializeCriticalSection(&ThreadManager::cs);
	editedMaterial = NULL;
	curScenePtr = NULL;
	materialEditorSceneFilenames.add("mat editor\\scene01\\scene.nxs");
	materialEditorSceneFilenames.add("mat editor\\scene02\\scene.nxs");
	materialEditorSceneFilenames.add("mat editor\\scene03\\scene.nxs");
	materialEditorSceneFilenames.add("mat editor\\scene04\\scene.nxs");
	materialEditorSceneFilenames.add("mat editor\\scene05\\scene.nxs");
	materialEditorSceneNames.add("Reflection scene");
	materialEditorSceneNames.add("Emitter scene");
	materialEditorSceneNames.add("IES Emitter scene");
	materialEditorSceneNames.add("SSS scene");
	materialEditorSceneNames.add("Displacement scene");
	materialEditorSceneFilenames.createArray();
	materialEditorSceneNames.createArray();
	firstMatScene = -1;
	for (int i=0; i<18; i++)
		UserColors::colors[i] = Color4(0,0,0);

	pluginMaxSceneId = -1;
	showTexNotFoundDlg = true;
	rayReflectionProbability = 80;
	runDialogsUnicode = false;
	tlsIndexRandom = TlsAlloc();
	TlsSetValue(tlsIndexRandom, NULL);

	gui_timers.refreshTimeOn = true;
	gui_timers.stoppingTimerOn = false;
	gui_timers.autosaveTimerOn = false;
	gui_timers.stopTimerHours = 0;
	gui_timers.stopTimerMinutes = 0;
	gui_timers.stopTimerSeconds = 0;
	gui_timers.refreshTime = 10;
	gui_timers.autosaveTimerMinutes = 60;
	gui_timers.rn_active = false;
	gui_timers.rn_nextCam = true;
	gui_timers.rn_nCamDesc = false;
	gui_timers.rn_copyPost = true;
	gui_timers.rn_delBuffs = true;
	gui_timers.rn_saveImg = false;
	gui_timers.rn_changeSunsky = false;
	gui_timers.rn_sunskyMins = 0;
	gui_timers.rn_fileFormat = 0;
	gui_timers.rn_saveFolder = NULL;

	options.useOpenCLonPost = false;
	options.texAutoReloadMode = 2;
	options.openCLsupported = isOpenCLSupported();

}

Raytracer::~Raytracer() 
{
}

int Raytracer::addNewScene()
{
	Scene * scene = new Scene();
	scenes.add(scene);
	scenes.createArray();
	return scenes.objCount-1;
}

bool Scene::disposeEverything()
{
	triangles.freeList();

	// materials
	for (int i=0; i<mats.objCount; i++)
	{
		MaterialNox * mat = mats[i];
		if (mat->preview)
			delete mat->preview;
		mat->preview = NULL;
		if (mat->name)
			free(mat->name);
		mat->name = NULL;

		mat->deleteLayers();

		delete mats[i];
	}
	mats.freeList();

	// cameras
	for (int i=0; i<cameras.objCount; i++)
	{
		Camera * cam = cameras[i];
		if (cam->imgBuff)
			delete cam->imgBuff;
		cam->imgBuff = 0;
		delete cam;
	}
	cameras.freeList();

	// lights
	for (int i=0; i<lightMeshes.objCount; i++)
	{
		LightMesh * lm = lightMeshes[i];
		lm->freeArrays();
		delete lm;
	}
	lightMeshes.freeList();

	// meshes
	meshes.freeList();

	// thread manager
	if (thrM)
	{
		thrM->stopThreads();
		thrM->disposeThreads();
		delete thrM;
		thrM = NULL;
	}

	return true;
}

bool Raytracer::loadBenchmarkScenes(void (*print_progress_callback)(char * message, float progress))
{
	scenes.add(new Scene);
	scenes.createArray();
	curSceneInd = scenes.objCount-1;
	matScenesIndices.add(curSceneInd);
	matScenesIndices.createArray();
	curScenePtr = scenes[curSceneInd];
	
	curScenePtr->registerProgressCallback(print_progress_callback, 100);

	int err = curScenePtr->loadSceneFromBinary("scene1\\scene.nox");
	if (err)
	{
		MessageBox(0, "Error loading scene", "Error", 0);
		return false;
	}

	curScenePtr->postLoadInitialize(false);

	return true;
}

bool Raytracer::loadMatEditorScenes(void (*print_progress_callback)(char * message, float progress))
{
	char scenepath[4096];
	char statusText[256];
	Raytracer * rtr = Raytracer::getInstance();

	firstMatScene = rtr->scenes.objCount;
	rtr->matScenesIndices.freeList();

	for (int isc=0; isc<rtr->materialEditorSceneFilenames.objCount; isc++)
	{
		char * partfilename = rtr->materialEditorSceneFilenames[isc];
		sprintf_s(scenepath, 4096, "%s\\%s", defaultDirectory, partfilename);

		rtr->scenes.add(new Scene);
		rtr->scenes.createArray();
		rtr->curSceneInd = rtr->scenes.objCount-1;
		rtr->matScenesIndices.add(rtr->curSceneInd);
		rtr->matScenesIndices.createArray();
		rtr->curScenePtr = rtr->scenes[rtr->curSceneInd];
		rtr->curScenePtr->sscene.useSunSky = false;

		rtr->curScenePtr->registerProgressCallback(print_progress_callback, 100);
		sprintf_s(statusText, 256, "Loading material scenes (%d of %d)", isc, materialEditorSceneFilenames.objCount);
		rtr->curScenePtr->notifyProgress(statusText, (float)isc*100/materialEditorSceneFilenames.objCount);

		int err = rtr->curScenePtr->loadSceneFromXML(scenepath);
		if (err)
		{
			char bbuf[4096];
			if (err<0)
				sprintf_s(bbuf, 4096, "Error while loading scene\n%s\n\nProbably file does not exist or XML header corrupted.", scenepath);
			else
				sprintf_s(bbuf, 4096, "Error while loading scene\n%s\n\nWatch line %d.", scenepath, err);
			MessageBox(0, bbuf, "Error", 0);
			return false;
		}

		rtr->curScenePtr->postLoadInitialize(true);
	}

	rtr->curScenePtr->notifyProgress("Done", 0);

	return true;
}




int Scene::saveSceneToXML(char * filename, bool addCamBuffers)
{
	XMLScene xmls;
	if (!xmls.saveScene(filename, this, addCamBuffers))
		return 1;
	else
	{
		int l = (int)strlen(filename)+1;		
		char * t = (char*)malloc(l);
		sprintf_s(t, l, "%s", filename);
		if (sceneFilename)
			free(sceneFilename);
		sceneFilename = t;
		return 0;
	}
}

int Scene::saveSceneToBinary(char * filename, bool addCamBuffers, bool silentMode)
{
	BinaryScene bScene;
	if (!bScene.SaveScene(filename, addCamBuffers, silentMode))
		return 1;
	else
		return 0;
}


int Scene::loadSceneFromXML(char * filename)
{
	XMLScene xmls;
	if (!xmls.LoadScene(filename))
		return xmls.errLine;
	else
	{
		int l = (int)strlen(filename)+1;		
		char * t = (char*)malloc(l);
		sprintf_s(t, l, "%s", filename);
		if (sceneFilename)
			free(sceneFilename);
		sceneFilename = t;
		return 0;
	}
}

bool Scene::mergeSceneFromBinary(char * filename)
{
	BinaryScene bScene;
	if (!bScene.LoadScene(filename, true))
		return false;

	return true;
}

int Scene::loadSceneFromBinary(char * filename)
{
	BinaryScene bScene;
	if (!bScene.LoadScene(filename))
		return 1;
	else
	{
		int l = (int)strlen(filename)+1;		
		char * t = (char*)malloc(l);
		sprintf_s(t, l, "%s", filename);
		if (sceneFilename)
			free(sceneFilename);
		sceneFilename = t;
		return 0;
	}
}


Validator::Validator(Raytracer * raytr)
{
	rtr = raytr;
}

Validator::~Validator()
{
}

bool Validator::checkScene()
{
	error = SCENE_OK;
	Scene * scene = Raytracer::getInstance()->curScenePtr;

	// check camera
	if (scene->cameras.objCount < 1)
	{
		error = SCENE_ERROR_NO_CAMERA;
		return false;
	}

	// check active camera
	if ((scene->sscene.activeCamera >= scene->cameras.objCount) || (scene->sscene.activeCamera < 0))
	{
		error = SCENE_ERROR_NO_ACTIVE_CAMERA;
		return false;
	}

	// check triangles
	if (scene->triangles.objCount < 1)
	{
		error = SCENE_ERROR_NO_TRIANGLES;
		return false;
	}

	// so everything OK
	return true;
}


void Scene::refreshPixelsPerSecond()
{
	static char ptemp[64];
	sprintf_s(ptemp, 64, "%d", pixels_per_second);
	SetWindowText(pps, ptemp);
}

void Scene::copyImageRegionsSelected()
{
	return;
}

Camera * Scene::getActiveCamera()
{
	if (cameras.objCount == 0)
		return NULL;
	return cameras[sscene.activeCamera];
}

void Scene::registerProgressCallback(void (*print_progress_callback)(char * message, float progress), int miliSec)
{
	notifyProgressCallback = print_progress_callback;
	progressRefreshTime = miliSec;
}

void Scene::releaseProgressCallback()
{
	notifyProgressCallback = 0;
}

bool Scene::notifyProgress(char * message, float progress)
{
	if (notifyProgressCallback)
	{
		(*notifyProgressCallback)(message, progress);
		return true;
	}
	return false;
}

bool Mesh::assignName(char * nname)
{
	if (!nname)
		return false;

	int nsize = (int)strlen(nname);
	if (nsize < 1)
		return false;
	
	char * newName = (char*)malloc(nsize+1);
	if (!newName)
		return false;

	sprintf_s(newName, nsize+1, "%s", nname);
	if (name)
		free(name);
	name  = newName;

	return true;
}

bool Scene::decodeSceneStrings()
{
	unsigned int numtris = (unsigned int)triangles.objCount;
	char pswd[64]; 
	sprintf_s(pswd, 64, "%d", numtris);
	unsigned int l = (unsigned int)strlen(pswd);

	if (scene_name)
	{
		if (!xorString(scene_name, pswd, le_sc_name, l))
		{
			free(scene_name);
			scene_name = copyString("Unnamed");
			char tmp[256];
			sprintf_s(tmp, 256, "Failed to decode scene name. Str size: %d  Psw size: %d", le_sc_name, l);
			Logger::add(tmp);
		}
	}

	if (scene_author_name)
	{
		if (!xorString(scene_author_name, pswd, le_sc_author_name, l))
		{
			free(scene_author_name);
			scene_author_name = NULL;
			char tmp[256];
			sprintf_s(tmp, 256, "Failed to decode scene author. Str size: %d  Psw size: %d", le_sc_author_name, l);
			Logger::add(tmp);
		}
	}

	if (scene_author_email)
	{
		if (!xorString(scene_author_email, pswd, le_sc_author_email, l))
		{
			free(scene_author_email);
			scene_author_email = NULL;
			char tmp[256];
			sprintf_s(tmp, 256, "Failed to decode scene author email. Str size: %d  Psw size: %d", le_sc_author_email, l);
			Logger::add(tmp);
		}
	}

	if (scene_author_www)
	{
		if (!xorString(scene_author_www, pswd, le_sc_author_www, l))
		{
			free(scene_author_www);
			scene_author_www = NULL;
			char tmp[256];
			sprintf_s(tmp, 256, "Failed to decode scene author webpage. Str size: %d  Psw size: %d", le_sc_author_www, l);
			Logger::add(tmp);
		}
	}

	Logger::add("scene name:");
	if (scene_name)
		Logger::add(scene_name);
	Logger::add("scene author:");
	if (scene_author_name)
		Logger::add(scene_author_name);
	Logger::add("scene author email:");
	if (scene_author_email)
		Logger::add(scene_author_email);
	Logger::add("scene author www:");
	if (scene_author_www)
		Logger::add(scene_author_www);

	return true;
}

bool Scene::changeTexturePaths(char * mainPath, char * subDir)
{
	if (!mainPath)
		return false;

	char * goodMainPath = copyDirPathWithoutLastBackslash(mainPath, false);
	if (!goodMainPath)
		return false;

	char * texDstPath = NULL;
	if (subDir)
	{
		int l1 = (int)strlen(goodMainPath);
		int l2 = (int)strlen(subDir);
		if (l2<1)
			texDstPath = copyString(goodMainPath);
		else
		{
			texDstPath = (char*)malloc(l1+l2+5);
			if (texDstPath)
				sprintf_s(texDstPath, (l1+l2+5), "%s\\%s", goodMainPath, subDir);
		}
	}
	else
	{
		texDstPath = copyString(goodMainPath);
	}

	if (!texDstPath)
	{
		free(goodMainPath);
		return false;
	}

	// we have valid dst path
	bList<char *, 16> textures(0);
	bList<char *, 16> dst_textures(0);
	bList<bool, 16> isItCopy(0);

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);

	// make array of all texture names
	for (int i=0; i<sc->mats.objCount; i++)
	{
		MaterialNox * mat = sc->mats[i];
		if (!mat)
			continue;

		for (int j=0; j<mat->layers.objCount; j++)
		{
			MatLayer * mlay = &(mat->layers[j]);
			if (mlay->tex_col0.fullfilename)
			{
				mlay->tex_col0.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_col0.fullfilename));
			}
			if (mlay->tex_col90.fullfilename)
			{
				mlay->tex_col90.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_col90.fullfilename));
			}
			if (mlay->tex_rough.fullfilename)
			{
				mlay->tex_rough.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_rough.fullfilename));
			}
			if (mlay->tex_weight.fullfilename)
			{
				mlay->tex_weight.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_weight.fullfilename));
			}
			if (mlay->tex_light.fullfilename)
			{
				mlay->tex_light.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_light.fullfilename));
			}
			if (mlay->tex_normal.fullfilename)
			{
				mlay->tex_normal.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_normal.fullfilename));
			}
			if (mlay->tex_transm.fullfilename)
			{
				mlay->tex_transm.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_transm.fullfilename));
			}
			if (mlay->tex_aniso.fullfilename)
			{
				mlay->tex_aniso.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_aniso.fullfilename));
			}
			if (mlay->tex_aniso_angle.fullfilename)
			{
				mlay->tex_aniso_angle.exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->tex_aniso_angle.fullfilename));
			}
			if (mlay->ies  &&  mlay->ies->filename)	// ies
			{
				mlay->ies->exportedArrayID = textures.objCount;
				textures.add(copyString(mlay->ies->filename));
			}
		}

		if (mat->tex_opacity.fullfilename)
		{
			mat->tex_opacity.exportedArrayID = textures.objCount;
			textures.add(copyString(mat->tex_opacity.fullfilename));
		}

		if (mat->tex_displacement.fullfilename)
		{
			mat->tex_displacement.exportedArrayID = textures.objCount;
			textures.add(copyString(mat->tex_displacement.fullfilename));
		}
	}

	if (sc->env.tex.fullfilename)
		{
			sc->env.tex.exportedArrayID = textures.objCount;
			textures.add(copyString(sc->env.tex.fullfilename));
		}

	for (int i=0; i<sc->cameras.objCount; i++)
	{
		Camera * cam = sc->cameras[i];
		if (!cam)
			continue;
		if (cam->texDiaphragm.fullfilename)
		{
			cam->texDiaphragm.exportedArrayID = textures.objCount;
			textures.add(copyString(cam->texDiaphragm.fullfilename));
		}
		if (cam->texObstacle.fullfilename)
		{
			cam->texObstacle.exportedArrayID = textures.objCount;
			textures.add(copyString(cam->texObstacle.fullfilename));
		}
	}

	textures.createArray();
	int numtex = textures.objCount;

	// get only filenames - watch for duplicates
	for (int i=0; i<numtex; i++)
	{
		char * srcFFname = textures[i];
		char * filename = getOnlyFile(srcFFname);
		if (!filename)
		{
			return false;
		}

		bool tryAgain = true;
		bool alreadyGotOne = false;
		while (tryAgain)
		{
			tryAgain = false;
			for (int j=0; j<i; j++)
			{
				if (!strcmp(dst_textures[j], filename))
				{
					if (!strcmp(textures[j], srcFFname))
					{	// exactly the same tex used
						tryAgain = false;
						alreadyGotOne = true;
						break;
					}
					else
					{	// only the same name... generate other name
						tryAgain = true;
						char * nextName = addOneToFilename(filename);
						if (!nextName)
							return false;
						free(filename);
						filename = nextName;
						break;
					}
				}
				else
				{	// different filenames.. go on
				}
			}
		}

		dst_textures.add(filename);
		dst_textures.createArray();
		isItCopy.add(alreadyGotOne);
		isItCopy.createArray();
	}

	unsigned int mdst = dst_textures.objCount;
	dst_textures.add(copyString(""));

	// add temporary relative paths to nox textures
	for (int i=0; i<sc->mats.objCount; i++)
	{
		MaterialNox * mat = sc->mats[i];
		if (!mat)
			continue;

		for (int j=0; j<mat->layers.objCount; j++)
		{
			MatLayer * mlay = &(mat->layers[j]);
			if (mlay->tex_col0.fullfilename)
				mlay->tex_col0.exportedFilename = copyString(dst_textures[min(mlay->tex_col0.exportedArrayID, mdst)]);
			if (mlay->tex_col90.fullfilename)
				mlay->tex_col90.exportedFilename = copyString(dst_textures[min(mlay->tex_col90.exportedArrayID, mdst)]);
			if (mlay->tex_rough.fullfilename)
				mlay->tex_rough.exportedFilename = copyString(dst_textures[min(mlay->tex_rough.exportedArrayID, mdst)]);
			if (mlay->tex_weight.fullfilename)
				mlay->tex_weight.exportedFilename = copyString(dst_textures[min(mlay->tex_weight.exportedArrayID, mdst)]);
			if (mlay->tex_light.fullfilename)
				mlay->tex_light.exportedFilename = copyString(dst_textures[min(mlay->tex_light.exportedArrayID, mdst)]);
			if (mlay->tex_normal.fullfilename)
				mlay->tex_normal.exportedFilename = copyString(dst_textures[min(mlay->tex_normal.exportedArrayID, mdst)]);
			if (mlay->tex_transm.fullfilename)
				mlay->tex_transm.exportedFilename = copyString(dst_textures[min(mlay->tex_transm.exportedArrayID, mdst)]);
			if (mlay->tex_aniso.fullfilename)
				mlay->tex_aniso.exportedFilename = copyString(dst_textures[min(mlay->tex_aniso.exportedArrayID, mdst)]);
			if (mlay->tex_aniso_angle.fullfilename)
				mlay->tex_aniso_angle.exportedFilename = copyString(dst_textures[min(mlay->tex_aniso_angle.exportedArrayID, mdst)]);
			if (mlay->ies  &&  mlay->ies->filename)	// ies
				mlay->ies->exportedFilename = copyString(dst_textures[min(mlay->ies->exportedArrayID, mdst)]);
		}

		if (mat->tex_opacity.fullfilename)
			mat->tex_opacity.exportedFilename = copyString(dst_textures[min(mat->tex_opacity.exportedArrayID, mdst)]);
		if (mat->tex_displacement.fullfilename)
			mat->tex_displacement.exportedFilename = copyString(dst_textures[min(mat->tex_displacement.exportedArrayID, mdst)]);
	}
	if (sc->env.tex.fullfilename)
		sc->env.tex.exportedFilename = copyString(dst_textures[min(sc->env.tex.exportedArrayID, mdst)]);

	for (int i=0; i<sc->cameras.objCount; i++)
	{
		Camera * cam = sc->cameras[i];
		if (!cam)
			continue;
		if (cam->texDiaphragm.fullfilename)
			cam->texDiaphragm.exportedFilename = copyString(dst_textures[min(cam->texDiaphragm.exportedArrayID, mdst)]);
		if (cam->texObstacle.fullfilename)
			cam->texObstacle.exportedFilename = copyString(dst_textures[min(cam->texObstacle.exportedArrayID, mdst)]);
	}

	// copy texture files
	for (unsigned int i=0; i<mdst; i++)
	{
		if (isItCopy[i])
		{
			Logger::add("Ignoring already copied:");
			Logger::add(dst_textures[i]);
			continue;
		}

		int lgmp = (int)strlen(goodMainPath);
		int lfn  = (int)strlen(dst_textures[i]);
		char * dst_fullfilename = (char *)malloc(lgmp+lfn+10);
		sprintf_s(dst_fullfilename, lgmp+lfn+10, "%s\\%s", goodMainPath, dst_textures[i]);


		Logger::add("Copying tex file (from to):");
		Logger::add(textures[i]);
		Logger::add(dst_fullfilename);

		if (fileExists(textures[i]))
		{
			//BOOL res = TRUE;
			BOOL res = CopyFile(textures[i], dst_fullfilename, false);
			if (res)
				Logger::add("Copying successful");
			else
				Logger::add("Copying failed.");
		}
		else
			Logger::add("Copying failed - source file doesn't exist.");

		free(dst_fullfilename);
	}


	textures.createArray();
	dst_textures.createArray();
	isItCopy.createArray();

	// release arrays
	for (int i=0; i<textures.objCount; i++)
	{
		Logger::add("Releasing from array:");
		Logger::add(textures[i]);
		if (textures[i])
			free(textures[i]);
	}
	for (int i=0; i<dst_textures.objCount; i++)
	{
		Logger::add("Releasing from array:");
		Logger::add(dst_textures[i]);
		if (dst_textures[i])
			free(dst_textures[i]);
	}
	textures.freeList();
	textures.destroyArray();
	dst_textures.freeList();
	dst_textures.destroyArray();
	isItCopy.freeList();
	isItCopy.destroyArray();

	return true;
}

bool Scene::releaseRelativePaths()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);

	// make array of all texture names
	for (int i=0; i<sc->mats.objCount; i++)
	{
		MaterialNox * mat = sc->mats[i];
		if (!mat)
			continue;

		for (int j=0; j<mat->layers.objCount; j++)
		{
			MatLayer * mlay = &(mat->layers[j]);

			if (mlay->tex_col0.exportedFilename)
				free(mlay->tex_col0.exportedFilename);
			mlay->tex_col0.exportedFilename = NULL;
			mlay->tex_col0.exportedArrayID = 0;
			
			if (mlay->tex_col90.exportedFilename)
				free(mlay->tex_col90.exportedFilename);
			mlay->tex_col90.exportedFilename = NULL;
			mlay->tex_col90.exportedArrayID = 0;
			
			if (mlay->tex_rough.exportedFilename)
				free(mlay->tex_rough.exportedFilename);
			mlay->tex_rough.exportedFilename = NULL;
			mlay->tex_rough.exportedArrayID = 0;
			
			if (mlay->tex_weight.exportedFilename)
				free(mlay->tex_weight.exportedFilename);
			mlay->tex_weight.exportedFilename = NULL;
			mlay->tex_weight.exportedArrayID = 0;
			
			if (mlay->tex_light.exportedFilename)
				free(mlay->tex_light.exportedFilename);
			mlay->tex_light.exportedFilename = NULL;
			mlay->tex_light.exportedArrayID = 0;

			if (mlay->tex_normal.exportedFilename)
				free(mlay->tex_normal.exportedFilename);
			mlay->tex_normal.exportedFilename = NULL;
			mlay->tex_normal.exportedArrayID = 0;

			if (mlay->tex_transm.exportedFilename)
				free(mlay->tex_transm.exportedFilename);
			mlay->tex_transm.exportedFilename = NULL;
			mlay->tex_transm.exportedArrayID = 0;

			if (mlay->tex_aniso.exportedFilename)
				free(mlay->tex_aniso.exportedFilename);
			mlay->tex_aniso.exportedFilename = NULL;
			mlay->tex_aniso.exportedArrayID = 0;

			if (mlay->tex_aniso_angle.exportedFilename)
				free(mlay->tex_aniso_angle.exportedFilename);
			mlay->tex_aniso_angle.exportedFilename = NULL;
			mlay->tex_aniso_angle.exportedArrayID = 0;

			if (mlay->ies)
			{
				if (mlay->ies->exportedFilename)
					free(mlay->ies->exportedFilename);
				mlay->ies->exportedFilename = NULL;
				mlay->ies->exportedArrayID = 0;
			}
		}

		if (mat->tex_opacity.exportedFilename)
			free(mat->tex_opacity.exportedFilename);
		mat->tex_opacity.exportedFilename = NULL;
		mat->tex_opacity.exportedArrayID = 0;

		if (mat->tex_displacement.exportedFilename)
			free(mat->tex_displacement.exportedFilename);
		mat->tex_displacement.exportedFilename = NULL;
		mat->tex_displacement.exportedArrayID = 0;
	}

	if (sc->env.tex.exportedFilename)
		free(sc->env.tex.exportedFilename);
	sc->env.tex.exportedFilename = NULL;
	sc->env.tex.exportedArrayID = 0;

	for (int i=0; i<sc->cameras.objCount; i++)
	{
		Camera * cam = sc->cameras[i];
		if (!cam)
			continue;

		if (cam->texDiaphragm.exportedFilename)
			free(cam->texDiaphragm.exportedFilename);
		cam->texDiaphragm.exportedFilename = NULL;
		cam->texDiaphragm.exportedArrayID = 0;

		if (cam->texObstacle.exportedFilename)
			free(cam->texObstacle.exportedFilename);
		cam->texObstacle.exportedFilename = NULL;
		cam->texObstacle.exportedArrayID = 0;
	}

	return true;
}


float Scene::intersectBruteForce(const Point3d &origin, const Vector3d &direction, int &tri, const float &maxDist, float &u, float &v)
{
	float tu,tv;
	float nearest = BIGFLOAT;
	int ntri = -1;
	float nu, nv;
	float d = -1;
	for (int i=0; i<triangles.objCount; i++)
	{
		d = triangles[i].intersection(origin, direction, tu,tv);
		if (d > 0.0f)
		{
			if (d < nearest)
			{
				nearest = d;
				ntri = i;
				nu = tu;
				nv = tv;
			}
		}
	}
	if (nearest > 0.0f)
	{
		tri = ntri;
		u = nu;
		v = nv;
		return nearest;
	}

	return -1;
}

BlendSettings::BlendSettings()
{
	for (int i=0; i<16; i++)
	{
		blendOn[i] = true;
		weights[i] = 1;
		red[i] = 1;
		green[i] = 1;
		blue[i] = 1;
		names[i] = NULL;
	}
	setDefaultNames();
}

BlendSettings::~BlendSettings()
{
	for (int i=0; i<16; i++)
	{
		if (names[i])
			free(names[i]);
		names[i] = NULL;
	}
}

bool BlendSettings::copyFrom(BlendSettings * src)
{
	CHECK(src);
	for (int i=0; i<16; i++)
	{
		blendOn[i] = src->blendOn[i];
		weights[i] = src->weights[i];
		red[i] = src->red[i];
		green[i] = src->green[i];
		blue[i] = src->blue[i];
		char * oldname = names[i];
		names[i] = copyString(src->names[i]);
		if (oldname)
			free(oldname);
	}
	return true;
}

bool BlendSettings::copyNamesFrom(BlendSettings * src)
{
	CHECK(src);
	for (int i=0; i<16; i++)
	{
		char * oldname = names[i];
		names[i] = copyString(src->names[i]);
		if (oldname)
			free(oldname);
	}
	return true;
}

void BlendSettings::setDefaultNames()
{
	char nname[64];
	for (int i=0; i<16; i++)
	{
		sprintf_s(nname, 64, "Layer %d", (i+1));
		setName(i, nname);
	}
}

void BlendSettings::setName(int num, char * newname)
{
	char * oldname = names[num];
	names[num] = copyString(newname);
	if (oldname)
		free(oldname);
}


Color4 TemperatureColor::getEnergyColor(int temperature)
{
	Color4 res;
	int t = min(10000, max(1000, temperature))-1000;
	int s = t/100;
	float p = t/100.0f - s;

	res.r = (1-p)*blackbody[s*3  ] + p*blackbody[s*3+3];
	res.g = (1-p)*blackbody[s*3+1] + p*blackbody[s*3+4];
	res.b = (1-p)*blackbody[s*3+2] + p*blackbody[s*3+5];

	return res;
}

Color4 TemperatureColor::getGammaColor(int temperature)
{
	Color4 res = getEnergyColor(temperature);
	res.r = pow(res.r,(1/2.2f));
	res.g = pow(res.g,(1/2.2f));
	res.b = pow(res.b,(1/2.2f));
	return res;
}

bool Raytracer::setUserColor(const int &ucnum, const Color4 &c)
{
	if (ucnum<0 || ucnum>17)
		return false;
	UserColors::colors[ucnum] = c;
	return true;
}

bool Raytracer::getUserColor(const int &ucnum, Color4 &c)
{
	if (ucnum<0 || ucnum>17)
		return false;
	c = UserColors::colors[ucnum];
	return true;
}

int Scene::evalBlendBits()
{
	int resbits = 0;
	for (int i=0; i<mats.objCount; i++)
	{
		MaterialNox * mat = mats[i];
		if (!mat->hasEmitters)
			continue;
		resbits |= (1<<mat->blendIndex);
	}

	if (sscene.useSunSky)
	{
		resbits |= 1;		// at blend layer 0
	}
	if (sscene.sunsky->sunOn) 
		if (sscene.sunsky->sunOtherBlendLayer)
			resbits |= (1<<1);	// at blend layer 1
		else
			resbits |= (1<<0);	// at blend layer 1


	if (env.enabled && env.tex.isValid())
		resbits |= (1<<(env.blendlayer-1));
	
	return resbits;
}

float Scene::intersectBVHforNonOpacFakeGlass(const float sample_time, const Point3d &origin, const Vector3d &direction, float &maxDist, Matrix4d & inst_mat, int &inst_id, int &tri, float &u, float &v, Color4 &attenuation, SceneStatic * customSS, bool allowRandomStop)
{
	attenuation = Color4(1,1,1);
	bool shadow = false;
	bool asShadow = (maxDist < 1000000.0f);
	Point3d shMax = origin + direction * maxDist;
	Vector3d dir = direction;
	Point3d sPoint = origin;
	int maxtri = triangles.objCount;
	float tmaxDist = maxDist;
	RandomGenerator * rg = Raytracer::getInstance()->getRandomGeneratorForThread();

	while (!shadow)
	{
		float uu,vv;
		int ttri = -1;
		Matrix4d matr;
		int instID = -1;
		float d = customSS->bvh.intersectForTrisNonSSE(sample_time, sPoint, dir, matr, tmaxDist, instID, ttri, uu, vv);
		if (d<0  ||  ttri<0  ||  ttri>=maxtri)
			return -1;

		Triangle * triangle = &(triangles[ttri]);
		HitData hd;
		hd.in = -dir;
		hd.out = dir;
		hd.normal_shade = (matr.getMatrixForNormals() * triangle->evalNormal(uu,vv)).getNormalized();
		hd.normal_geom = (matr.getMatrixForNormals() * triangle->normal_geom).getNormalized();
		hd.adjustNormal();
		triangle->getTangentSpaceSmooth(matr, uu, vv, hd.dir_U, hd.dir_V);
		triangle->evalTexUV(uu,vv, hd.tU,hd.tV);
		MaterialNox * mat = mats[instances[instID].materials[triangle->matInInst]];
		Point3d p = matr * triangle->getPointFromUV(uu,vv);

		float opac;
		Color4 att = mat->evalAllLayersFakeGlassAndOpacityAttenuation(hd, opac);
		float maxrgb = max(att.r, max(att.g, att.b));
		bool stop_now = false;
		if (maxrgb > 0)
		{
			float rand_att = 0;
			if (allowRandomStop)
				#ifdef RAND_PER_THREAD
					rand_att = rg->getRandomFloat();
				#else
					rand_att = rand()/(float)RAND_MAX;
				#endif
			if (rand_att < maxrgb)
			{
				stop_now = false;
				att *= 1.0f/maxrgb;
			}
			else
				stop_now = true;
		}
		else
			stop_now = true;

		if (stop_now)
		{
			u = uu;
			v = vv;
			tri = ttri;
			inst_mat = matr;
			inst_id = instID;
			float dist = (p-origin).length();
			return dist;
		}

		attenuation *= att;
		sPoint = p + hd.normal_shade * (hd.normal_shade*hd.out > 0 ? 0.0001f : -0.0001f);
		if (asShadow)
		{
			dir = shMax - sPoint;
			tmaxDist = dir.normalize_return_old_length();
		}
	}
	return -1;

}

float Scene::intersectBSPforNonOpacFakeGlass(const Point3d &origin, const Vector3d &direction, float &maxDist, int &tri, float &u, float &v, Color4 &attenuation, SceneStatic * customSS, bool allowRandomStop)
{	// incorrect - no instances implemented!!!!!!!!!
	attenuation = Color4(1,1,1);
	bool shadow = false;
	bool asShadow = (maxDist < 1000000.0f);
	Point3d shMax = origin + direction * maxDist;
	Vector3d dir = direction;
	Point3d sPoint = origin;
	int maxtri = triangles.objCount;
	float tmaxDist = maxDist;
	RandomGenerator * rg = Raytracer::getInstance()->getRandomGeneratorForThread();

	while (!shadow)
	{
		float uu,vv;
		int ttri = -1;
		float d = customSS->	bsp.intersectForTris(sPoint, dir, tmaxDist, ttri, uu, vv);
		if (d<0  ||  ttri<0  ||  ttri>=maxtri)
			return -1;

		Triangle * triangle = &(triangles[ttri]);
		HitData hd;
		hd.in = -dir;
		hd.out = dir;
		hd.normal_shade = triangle->evalNormal(uu,vv);
		hd.normal_geom = triangle->normal_geom;
		hd.adjustNormal();
		triangle->evalTexUV(uu,vv, hd.tU,hd.tV);
		MaterialNox * mat = mats[triangle->matNum];
		Point3d p = triangle->getPointFromUV(uu,vv);

		float opac;
		Color4 att = mat->evalAllLayersFakeGlassAndOpacityAttenuation(hd, opac);
		float maxrgb = max(att.r, max(att.g, att.b));
		bool stop_now = false;
		if (maxrgb > 0)
		{
			float rand_att = 0;
			if (allowRandomStop)
				#ifdef RAND_PER_THREAD
					rand_att = rg->getRandomFloat();
				#else
					rand_att = rand()/(float)RAND_MAX;
				#endif
			if (rand_att < maxrgb)
			{
				stop_now = false;
				att *= 1.0f/maxrgb;
			}
			else
				stop_now = true;
		}
		else
			stop_now = true;

		if (stop_now)
		{
			u = uu;
			v = vv;
			tri = ttri;
			float dist = (p-origin).length();
			return dist;
		}

		attenuation *= att;
		sPoint = p + hd.normal_shade * (hd.normal_shade*hd.out > 0 ? 0.0001f : -0.0001f);
		if (asShadow)
		{
			dir = shMax - sPoint;
			tmaxDist = dir.normalize_return_old_length();
		}
	}
	return -1;
}

bool Raytracer::callDeleteEditedMaterial()
{
	delete editedMaterial;
	editedMaterial = NULL;
	return true;
}

bool Raytracer::callDeleteMaterial(MaterialNox * mat)
{
	CHECK(mat);
	delete mat;
	return true;
}

MaterialNox * Raytracer::callNewMaterial()
{
	MaterialNox * mat = new MaterialNox;
	return mat;
}

unsigned int Marsaglia::getRandom()
{
	X = aMarsaglia*(X&0xFFFF)+(X>>16);
	Y = bMarsaglia*(Y&0xFFFF)+(Y>>16);
	return (X<<16)+(Y&0xFFFF);
}

void Marsaglia::setSeed(unsigned int seed1, unsigned int seed2)
{
	if (seed1 && seed2)
	{
		X = seed1;
		Y = seed2;
	}
}





Lattice::Lattice() 
{ 
	ax=2341; 
	ay=3433; 
	m=50000; 
	i=0; 

	#ifdef RAND_PER_THREAD
		RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
		addx = rg->getRandomDouble();
		addy = rg->getRandomDouble();
	#else
		addx = rand()/(double)RAND_MAX;
		addy = rand()/(double)RAND_MAX;
	#endif
}

bool Lattice::reset()
{
	i = 0;
	#ifdef RAND_PER_THREAD
		RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
		addx = rg->getRandomDouble();
		addy = rg->getRandomDouble();
	#else
		addx = rand()/(double)RAND_MAX;
		addy = rand()/(double)RAND_MAX;
	#endif
	return true;
}

bool Lattice::randomPoint(float &x, float &y)
{
	if (i++ >= m)
	{
		i = 0;
		#ifdef RAND_PER_THREAD
			RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
			addx = rg->getRandomDouble();
			addy = rg->getRandomDouble();
		#else
			addx = rand()/(double)RAND_MAX;
			addy = rand()/(double)RAND_MAX;
		#endif
	}

	double fx = (ax*i)/(double)m;
	double fy = (ay*i)/(double)m;
	fx = fx - floor(fx);
	fy = fy - floor(fy);
	fx += addx;
	fy += addy;
	fx = fx - floor(fx);
	fy = fy - floor(fy);
	x = (float)fx;
	y = (float)fy;

	return true;
}


void ScanLine::reset(int width1, int height1, int numthreads, int threadID)
{
	num_threads = numthreads;
	width = width1;
	height = height1;
	X = Y = 0;
	if (threadID>-1)
		Y = thr_ID = threadID;
	else
		#ifdef RAND_PER_THREAD
		{
			RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
			Y = rg->getRandomInt(height);
		}
		#else
			Y = rand()%height;
		#endif
}

void ScanLine::getNextPoint(float &x, float &y)
{
	X++;
	if (X>=width)
	{
		X = 0;
		Y += num_threads;
		if (Y >= height)
		{
			Y -= height;
		}
	}
	#ifdef RAND_PER_THREAD
		RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
		x = (float)X + rg->getRandomFloat();//-0.5f;
		rg->getRandomFloat();
		y = (float)Y + rg->getRandomFloat();//-0.5f;
		rg->getRandomFloat();
	#else
		x = (float)X + rand()/(float)RAND_MAX;
		y = (float)Y + rand()/(float)RAND_MAX;
	#endif
}

//------------------------------------------------------------------------------------------------

bool Scene::copyToEditedMaterial(int matnum)
{
	CHECK(matnum>=0);
	CHECK(matnum<mats.objCount);

	Raytracer * rtr = Raytracer::getInstance();
	CHECK(mats[matnum]);

	MaterialNox * mat = mats[matnum]->copy();
	CHECK(mat);

	deleteEditedMaterial();

	rtr->editedMaterial = mat;
	
	rtr->editedMaterial->updateSceneTexturePointers(this);

	return true;
}

//------------------------------------------------------------------------------------------------

bool Scene::copyFromEditedMaterial(int matnum)
{
	CHECK(matnum>=0);
	CHECK(matnum<mats.objCount);
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(rtr->editedMaterial);

	// make copy and put in array
	MaterialNox * oldmat = rtr->curScenePtr->mats[matnum];
	MaterialNox * newmat = rtr->editedMaterial->copy();
	newmat->id = oldmat->id;
	rtr->curScenePtr->mats.setElement(matnum, newmat);
	rtr->curScenePtr->mats.createArray();

	// udpate tex
	newmat->tex_opacity.managerID		= rtr->curScenePtr->texManager.addTexture(newmat->tex_opacity.fullfilename,			false, newmat->tex_opacity.texScPtr,		false);
	newmat->tex_displacement.managerID	= rtr->curScenePtr->texManager.addTexture(newmat->tex_displacement.fullfilename,	false, newmat->tex_displacement.texScPtr,	false);
	for (int i=0; i<newmat->layers.objCount; i++)
	{
		MatLayer * mlay = &(newmat->layers[i]);
		mlay->tex_weight.managerID      = rtr->curScenePtr->texManager.addTexture(mlay->tex_weight.fullfilename,		false, mlay->tex_weight.texScPtr,		false);
		mlay->tex_col0.managerID        = rtr->curScenePtr->texManager.addTexture(mlay->tex_col0.fullfilename,			false, mlay->tex_col0.texScPtr,			false);
		mlay->tex_col90.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_col90.fullfilename,			false, mlay->tex_col90.texScPtr,		false);
		mlay->tex_light.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_light.fullfilename,			false, mlay->tex_light.texScPtr,		false);
		mlay->tex_normal.managerID      = rtr->curScenePtr->texManager.addTexture(mlay->tex_normal.fullfilename,		false, mlay->tex_normal.texScPtr,		true);
		mlay->tex_transm.managerID      = rtr->curScenePtr->texManager.addTexture(mlay->tex_transm.fullfilename,		false, mlay->tex_transm.texScPtr,		false);
		mlay->tex_rough.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_rough.fullfilename,			false, mlay->tex_rough.texScPtr,		false);
		mlay->tex_aniso.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso.fullfilename,			false, mlay->tex_aniso.texScPtr,		false);
		mlay->tex_aniso_angle.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso_angle.fullfilename,	false, mlay->tex_aniso_angle.texScPtr,	false);
	}
	rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);
	newmat->updateSceneTexturePointers(rtr->curScenePtr);
	newmat->updateFlags();

	// del old
	if (oldmat)
	{
		oldmat->deleteBuffers(true, true, true);
		oldmat->layers.destroyArray();
		oldmat->layers.freeList();
		delete oldmat;
		oldmat = NULL;
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool Scene::deleteEditedMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->editedMaterial)
	{
		rtr->editedMaterial->deleteBuffers(true, true, true);
		rtr->editedMaterial->layers.destroyArray();
		rtr->editedMaterial->layers.freeList();
		delete rtr->editedMaterial;
		rtr->editedMaterial = NULL;
	}
	return true;
}

//------------------------------------------------------------------------------------------------

bool Scene::evalSceneBoundingBox()
{
	BVH::BVHSplit * sroot = (BVH::BVHSplit*)sscene.bvh.root;
	if (sroot)
	{
		sscene.bb_minx = sroot->minx;	sscene.bb_maxx = sroot->maxx;
		sscene.bb_miny = sroot->miny;	sscene.bb_maxy = sroot->maxy;
		sscene.bb_minz = sroot->minz;	sscene.bb_maxz = sroot->maxz;
		return true;
	}

	float minx, miny, minz, maxx, maxy, maxz;
	minx = miny = minz = BIGFLOAT;
	maxx = maxy = maxz = -BIGFLOAT;
	char buf[512];
	DWORD ltick = GetTickCount()-26;
	for (int i=0; i<instances.objCount; i++)
	{
		MeshInstance * inst = &instances[i];

		DWORD ctick = GetTickCount();
		if (ctick-ltick>25)
		{
			sprintf_s(buf, 512, "Evaluating bounding box. Checking instance %d of %d - %s", (i+1), instances.objCount, inst->name);
			notifyProgress(buf, 100.0f*i/instances.objCount);
			ltick = ctick;
		}

		if (abortThreadPtr  &&  *abortThreadPtr)
			return false;
		Matrix4d matr = inst->matrix_transform;
		Mesh * mesh = &meshes[inst->meshID];
		for (int j=mesh->firstTri; j<=mesh->lastTri; j++)
		{
			Triangle &tri = triangles[j];
			Point3d v1 = matr * tri.V1;
			Point3d v2 = matr * tri.V2;
			Point3d v3 = matr * tri.V3;
			minx = min(min(minx, v1.x), min(v2.x, v3.x));
			miny = min(min(miny, v1.y), min(v2.y, v3.y));
			minz = min(min(minz, v1.z), min(v2.z, v3.z));
			maxx = max(max(maxx, v1.x), max(v2.x, v3.x));
			maxy = max(max(maxy, v1.y), max(v2.y, v3.y));
			maxz = max(max(maxz, v1.z), max(v2.z, v3.z));
		}
	}

	sscene.bb_minx = minx;	sscene.bb_maxx = maxx;
	sscene.bb_miny = miny;	sscene.bb_maxy = maxy;
	sscene.bb_minz = minz;	sscene.bb_maxz = maxz;

	return true;
}

//------------------------------------------------------------------------------------------------
