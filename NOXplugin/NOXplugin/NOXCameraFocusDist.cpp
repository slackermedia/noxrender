#include "max2013predef.h"
#include "NOXCamera.h"
#include <macrorec.h>
#include <decomp.h>
#include "NOXExporter.h"


NOXCamFocusDistMouseCallback mcb;

Point3 getNodeCenterPosition(INode * node)
{
	Matrix3 nmat = node->GetNodeTM(GetCOREInterface()->GetTime());
	Point3 pos = nmat.GetRow(3);
	return pos;
	return Point3(0,0,0);
}


int NOXCamFocusDistMouseCallback::proc( HWND hwnd, int msg, int point, int flag, IPoint2 m )
{
	int res = TRUE;	
	#ifdef ISMAX2013
		ViewExp * vpt = &(GetCOREInterface()->GetActiveViewExp());
	#else
		ViewExp * vpt = GetCOREInterface()->GetActiveViewport();
	#endif
	TimeValue t = GetCOREInterface()->GetTime();

	switch ( msg ) 
	{
		case MOUSE_POINT:
			switch ( point ) 
			{
				case 0: 
				{
					GraphicsWindow * gw = vpt->getGW();
					gw->clearHitCode();

					INode * node = GetCOREInterface()->PickNode(hwnd, m, NULL);
					if (node)
					{
						Point3 pos = getNodeCenterPosition(node);

						ULONG handle;
						camObject->NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
						INode * camnode = GetCOREInterface()->GetINodeByHandle( handle );
						if ( camnode ) 
						{

							Matrix3 cammat = (camnode->GetObjTMAfterWSM(t));
							
							Point3 campos = cammat.GetRow(3);
							Point3 toPoint = pos - campos;
							Point3 nToPoint = toPoint;
							float ndist = toPoint.FLength();
							nToPoint = nToPoint.Normalize();

							Point3 camDir = cammat.GetRow(2);
							camDir = camDir.Normalize();
							camDir *= -1;
							
							float ccos = camDir % nToPoint;

							if (ccos < 0.001f)
								ndist = 5;
							else 
								ndist = ndist * ccos;
							if (ndist < 5)
								ndist = 5;


							camObject->focusLength = ndist;

							if (camObject->focusConnectedToTarget)
							{
								Matrix3 targtm = cammat;
								targtm.PreTranslate(Point3(0.0f,0.0f,-ndist));
								INode *targNode = camnode->GetTarget();
								if (targNode)
									targNode->SetNodeTM(0,targtm);

								camObject->SetTDist(t, ndist);
							}
						}
					}

					GetCOREInterface()->DeleteMode(&NOXCamFocusDistCommandMode::GetInstance());
				}
				break;
			}
			break;
		case MOUSE_FREEMOVE:
		{
		}
		break;
		case MOUSE_PROPCLICK:
		{
			GetCOREInterface()->DeleteMode(&NOXCamFocusDistCommandMode::GetInstance());
			res = FALSE;						
		}
		break;
		case MOUSE_ABORT:
		{
			GetCOREInterface()->DeleteMode(&NOXCamFocusDistCommandMode::GetInstance());
			res = FALSE;						
		}
		break;
	}

	#ifndef ISMAX2013
		GetCOREInterface()->ReleaseViewport( vpt );
	#endif
	return res;
}

MouseCallBack * NOXCamFocusDistCommandMode::MouseProc (int *numPoints)
{
	if (numPoints)
		*numPoints = 10;
	mcb.camObject = cam;
	return &mcb;
}

ChangeForegroundCallback * NOXCamFocusDistCommandMode::ChangeFGProc ()
{
	return NULL;
	return CHANGE_FG_SELECTED;
}

BOOL NOXCamFocusDistCommandMode::ChangeFG (CommandMode *oldMode)
{
	return FALSE;
	return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED);
}

NOXCamFocusDistCommandMode& NOXCamFocusDistCommandMode::GetInstance()
{
	static NOXCamFocusDistCommandMode ncfdcMode;
	return ncfdcMode;
}

void NOXCamFocusDistCommandMode::EnterMode()
{
	SendMessage(hButton, BM_SETSTATE, 1, 0);
}

void NOXCamFocusDistCommandMode::ExitMode()
{
	SendMessage(hButton, BM_SETSTATE, 0, 0);
}

