#include <stdio.h>
#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "raytracer.h"
#include "valuesMinMax.h"
#include "log.h"

void setPositionsOnStartCameraTab(HWND hWnd);
void updateCameraPanelFOVfromCamera(Camera * cam, HWND hCamPanel);
bool setCameraAndPanelFocal(HWND hCamPanel, float focal);
bool updateDiaphragmImage(HWND hWnd, Camera * cam);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcCamera(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnStartCameraTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 

				// GROUP BARS
				EM2GroupBar * emgSelCam = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_CAM_GR_SELECT_CAMERA));
				emgSelCam->bgImage = rMain->hBmpBg;
				emgSelCam->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgMB = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_CAM_GR_MOTION_BLUR));
				emgMB->bgImage = rMain->hBmpBg;
				emgMB->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgExp = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_CAM_GR_EXPOSURE));
				emgExp->bgImage = rMain->hBmpBg;
				emgExp->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgFO = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_CAM_GR_FOCUS_OPTICS));
				emgFO->bgImage = rMain->hBmpBg;
				emgFO->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgAB = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_CAM_GR_APERTURE_BOKEH));
				emgAB->bgImage = rMain->hBmpBg;
				emgAB->setFont(rMain->fonts->em2groupbar, false);

				// CAMERAS LIST
				EM2ListView * emlvc = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_REND2_CAM_LIST_CAMS));
				emlvc->setColNumAndClearData(3, 200);
				emlvc->prepareWindow();
				emlvc->setHeightData(26);
				emlvc->setHeightHeader(26);
				emlvc->colWidths[0] = 60;
				emlvc->colWidths[1] = 197;
				emlvc->colWidths[2] = 87;
				emlvc->align[0] = EM2ListView::ALIGN_RIGHT;
				emlvc->align[1] = EM2ListView::ALIGN_LEFT;
				emlvc->align[2] = EM2ListView::ALIGN_CENTER;
				emlvc->alignHeader[0] = EM2ListView::ALIGN_RIGHT;
				emlvc->alignHeader[1] = EM2ListView::ALIGN_LEFT;
				emlvc->alignHeader[2] = EM2ListView::ALIGN_CENTER;
				emlvc->margins[0] = 43;
				emlvc->margins[1] = 0;
				emlvc->margins[2] = 0;
				emlvc->marginsHeader[0] = 40;
				emlvc->marginsHeader[1] = 0;
				emlvc->marginsHeader[2] = 0;
				emlvc->setHeaderName(0, "ID");
				emlvc->setHeaderName(1, "NAME");
				emlvc->setHeaderName(2, "RENDERED");
				emlvc->addEmptyRow();
				emlvc->setData(0,0, "1");
				emlvc->setData(1,0, "CAMERA 01");
				emlvc->setData(2,0, "NOOOOO");
				emlvc->addEmptyRow();
				emlvc->setData(0,1, "2");
				emlvc->setData(1,1, "CAMERA 02");
				emlvc->setData(2,1, "NOOOOO");
				emlvc->setFontHeader(rMain->fonts->em2listheader, false);
				emlvc->setFontData(rMain->fonts->em2listentry, false);
				emlvc->bgImage = rMain->hBmpBg;
				for (int i=2; i<10; i++)
				{
					char buf[256];
					emlvc->addEmptyRow();
					sprintf_s(buf, 256, "%d", i+1);
					emlvc->setData(0,i, buf);
					sprintf_s(buf, 256, "CAMERA %d", i+1);
					emlvc->setData(1,i, buf);
					emlvc->setData(2,i, "NOOOOO");
				}

				// MOTION BLUR
				EM2CheckBox * emcMBenabled = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_MB_ENABLED));
				emcMBenabled->bgImage = rMain->hBmpBg;
				emcMBenabled->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emtmbdur = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_MB_DURATION));
				emtmbdur->bgImage = rMain->hBmpBg;
				emtmbdur->setFont(rMain->fonts->em2text, false);
				emtmbdur->align = EM2Text::ALIGN_RIGHT;
				EM2EditSpin * emesMBdur = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_MB_DURATION));
				emesMBdur->setFont(rMain->fonts->em2editspin, false);
				emesMBdur->bgImage = rMain->hBmpBg;
				emesMBdur->setValuesToFloat(0.0f, 0.0f, 0.0f, 0.0f, 0.1f, 0.01f);
				EM2CheckBox * emcMBstill = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_MB_STILL));
				emcMBstill->bgImage = rMain->hBmpBg;
				emcMBstill->setFont(rMain->fonts->em2checkbox, false);

				// EXPOSURE
				EM2Text * emtiso = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_ISO));
				emtiso->bgImage = rMain->hBmpBg;
				emtiso->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emesISO = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_ISO));
				emesISO->setFont(rMain->fonts->em2editspin, false);
				emesISO->bgImage = rMain->hBmpBg;
				emesISO->setValuesToInt(NOX_CAM_ISO_DEF, NOX_CAM_ISO_MIN, NOX_CAM_ISO_MAX, NOX_CAM_ISO_DEF, 10, 0.25f);

				EM2Text * emtshutter = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_SHUTTER));
				emtshutter->bgImage = rMain->hBmpBg;
				emtshutter->setFont(rMain->fonts->em2text, false);
				emtshutter->align = EM2Text::ALIGN_RIGHT;

				EM2EditSpin * emesshutter = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_SHUTTER));
				emesshutter->setFont(rMain->fonts->em2editspin, false);
				emesshutter->bgImage = rMain->hBmpBg;
				emesshutter->setValuesToFloat(NOX_CAM_SHUTTER_DEF, NOX_CAM_SHUTTER_MIN, NOX_CAM_SHUTTER_MAX, NOX_CAM_SHUTTER_DEF, 10, 0.2f);

				// FOCUS AND OPTICS
				EM2Text * emtdist = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_DISTANCE));
				emtdist->bgImage = rMain->hBmpBg;
				emtdist->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emesfdist = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FOCUS_DIST));
				emesfdist->setFont(rMain->fonts->em2editspin, false);
				emesfdist->bgImage = rMain->hBmpBg;
				emesfdist->setValuesToFloat(NOX_CAM_FOCUS_DEF, NOX_CAM_FOCUS_MIN, NOX_CAM_FOCUS_MAX, NOX_CAM_FOCUS_DEF, 10, 0.1f);

				EM2Text * emtmeters = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_METERS));
				emtmeters->bgImage = rMain->hBmpBg;
				emtmeters->setFont(rMain->fonts->em2text, false);
				EM2Text * emtfocal = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOCAL));
				emtfocal->bgImage = rMain->hBmpBg;
				emtfocal->setFont(rMain->fonts->em2text, false);
				emtfocal->align = EM2Text::ALIGN_RIGHT;
				EM2EditSpin * emesfocal = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FOCAL));
				emesfocal->setFont(rMain->fonts->em2editspin, false);
				emesfocal->bgImage = rMain->hBmpBg;
				emesfocal->setValuesToFloat(NOX_CAM_FOCAL_DEF, NOX_CAM_FOCAL_MIN, NOX_CAM_FOCAL_MAX, NOX_CAM_FOCAL_DEF, 5, 0.2f);

				// FOCAL BUTTONS
				EM2Button * embF15 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F15MM));
				embF15->bgImage = rMain->hBmpBg;
				embF15->setFont(rMain->fonts->em2button, false);
				EM2Button * embF20 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F20MM));
				embF20->bgImage = rMain->hBmpBg;
				embF20->setFont(rMain->fonts->em2button, false);
				EM2Button * embF24 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F24MM));
				embF24->bgImage = rMain->hBmpBg;
				embF24->setFont(rMain->fonts->em2button, false);
				EM2Button * embF28 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F28MM));
				embF28->bgImage = rMain->hBmpBg;
				embF28->setFont(rMain->fonts->em2button, false);
				EM2Button * embF35 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F35MM));
				embF35->bgImage = rMain->hBmpBg;
				embF35->setFont(rMain->fonts->em2button, false);
				EM2Button * embF50 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F50MM));
				embF50->bgImage = rMain->hBmpBg;
				embF50->setFont(rMain->fonts->em2button, false);
				EM2Button * embF85 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F85MM));
				embF85->bgImage = rMain->hBmpBg;
				embF85->setFont(rMain->fonts->em2button, false);
				EM2Button * embF135 = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_CAM_F135MM));
				embF135->bgImage = rMain->hBmpBg;
				embF135->setFont(rMain->fonts->em2button, false);

				// FOV
				EM2Text * emttextfovhori = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOV_HORI));
				emttextfovhori->bgImage = rMain->hBmpBg;
				emttextfovhori->setFont(rMain->fonts->em2text, false);
				EM2Text * emttextfovvert = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOV_VERT));
				emttextfovvert->bgImage = rMain->hBmpBg;
				emttextfovvert->setFont(rMain->fonts->em2text, false);
				EM2Text * emttextfovdiag = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOV_DIAG));
				emttextfovdiag->bgImage = rMain->hBmpBg;
				emttextfovdiag->setFont(rMain->fonts->em2text, false);

				EM2Text * emtfovhori = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FOV_HORI));
				emtfovhori->bgImage = rMain->hBmpBg;
				emtfovhori->setFont(rMain->fonts->em2text, false);
				EM2Text * emtfovvert = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FOV_VERT));
				emtfovvert->bgImage = rMain->hBmpBg;
				emtfovvert->setFont(rMain->fonts->em2text, false);
				EM2Text * emtfovdiag = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FOV_DIAG));
				emtfovdiag->bgImage = rMain->hBmpBg;
				emtfovdiag->setFont(rMain->fonts->em2text, false);
				emttextfovdiag->align = EM2Text::ALIGN_RIGHT;
				emtfovdiag->align = EM2Text::ALIGN_RIGHT;
				emtfovhori->align = EM2Text::ALIGN_RIGHT;
				emtfovvert->align = EM2Text::ALIGN_RIGHT;
				SetWindowText(emtfovhori->hwnd, "5 �");
				SetWindowText(emtfovvert->hwnd, "5 �");
				SetWindowText(emtfovdiag->hwnd, "5 �");


				// APERTURE AND BOKEH
				EM2CheckBox * emcRoundBlades = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_ROUND_BLADES));
				emcRoundBlades->bgImage = rMain->hBmpBg;
				emcRoundBlades->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emtblades= GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_BLADES));
				emtblades->bgImage = rMain->hBmpBg;
				emtblades->setFont(rMain->fonts->em2text, false);
				EM2Text * emtaperture = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_APERTURE));
				emtaperture->bgImage = rMain->hBmpBg;
				emtaperture->setFont(rMain->fonts->em2text, false);
				EM2Text * emtangle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_ANGLE));
				emtangle->bgImage = rMain->hBmpBg;
				emtangle->setFont(rMain->fonts->em2text, false);
				EM2Text * emtradius = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_RADIUS));
				emtradius->bgImage = rMain->hBmpBg;
				emtradius->setFont(rMain->fonts->em2text, false);

				EM2CheckBox * emcDOFon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_DOF_ON));
				emcDOFon->bgImage = rMain->hBmpBg;
				emcDOFon->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emtringbalance= GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_RING_BALANCE));
				emtringbalance->bgImage = rMain->hBmpBg;
				emtringbalance->setFont(rMain->fonts->em2text, false);
				EM2Text * emtringsize = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_RING_SIZE));
				emtringsize->bgImage = rMain->hBmpBg;
				emtringsize->setFont(rMain->fonts->em2text, false);
				EM2Text * emtflatten = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FLATTEN));
				emtflatten->bgImage = rMain->hBmpBg;
				emtflatten->setFont(rMain->fonts->em2text, false);
				EM2Text * emtvignette = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_VIGNETTE));
				emtvignette->bgImage = rMain->hBmpBg;
				emtvignette->setFont(rMain->fonts->em2text, false);

				EM2EditSpin * emetAperture = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_APERTURE));
				emetAperture->setFont(rMain->fonts->em2editspin, false);
				emetAperture->bgImage = rMain->hBmpBg;
				emetAperture->setValuesToFloat(NOX_CAM_APERTURE_DEF, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, NOX_CAM_APERTURE_DEF, 0.4f, 0.1f);
				EM2EditSpin * emetNumBlades = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_NUM_BLADES));
				emetNumBlades->setFont(rMain->fonts->em2editspin, false);
				emetNumBlades->bgImage = rMain->hBmpBg;
				emetNumBlades->setValuesToInt(NOX_CAM_BLADES_NUM_DEF, NOX_CAM_BLADES_NUM_MIN, NOX_CAM_BLADES_NUM_MAX, NOX_CAM_BLADES_NUM_DEF, 1, 0.1f);
				EM2EditSpin * emetAngle = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_ANGLE));
				emetAngle->setFont(rMain->fonts->em2editspin, false);
				emetAngle->bgImage = rMain->hBmpBg;
				emetAngle->setValuesToFloat(NOX_CAM_BLADES_ANGLE_DEF, NOX_CAM_BLADES_ANGLE_MIN, NOX_CAM_BLADES_ANGLE_MAX, NOX_CAM_BLADES_ANGLE_DEF, 5.0f, 0.5f);
				EM2EditSpin * emetRadius = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_RADIUS));
				emetRadius->setFont(rMain->fonts->em2editspin, false);
				emetRadius->bgImage = rMain->hBmpBg;
				emetRadius->setValuesToFloat(NOX_CAM_BLADES_RADIUS_DEF, NOX_CAM_BLADES_RADIUS_MIN, NOX_CAM_BLADES_RADIUS_MAX, NOX_CAM_BLADES_RADIUS_DEF, 0.5f, 0.01f);
				EM2EditSpin * emetRingBalance = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_RING_BALANCE));
				emetRingBalance->setFont(rMain->fonts->em2editspin, false);
				emetRingBalance->bgImage = rMain->hBmpBg;
				emetRingBalance->setValuesToInt(NOX_CAM_BOKEH_BALANCE_DEF, NOX_CAM_BOKEH_BALANCE_MIN, NOX_CAM_BOKEH_BALANCE_MAX, NOX_CAM_BOKEH_BALANCE_DEF, 1, 0.1f);
				EM2EditSpin * emetRingSize = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_RING_SIZE));
				emetRingSize->setFont(rMain->fonts->em2editspin, false);
				emetRingSize->bgImage = rMain->hBmpBg;
				emetRingSize->setValuesToInt(NOX_CAM_BOKEH_RING_SIZE_DEF, NOX_CAM_BOKEH_RING_SIZE_MIN, NOX_CAM_BOKEH_RING_SIZE_MAX, NOX_CAM_BOKEH_RING_SIZE_DEF, 5, 0.2f);
				EM2EditSpin * emetFlatten = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FLATTEN));
				emetFlatten->setFont(rMain->fonts->em2editspin, false);
				emetFlatten->bgImage = rMain->hBmpBg;
				emetFlatten->setValuesToInt(NOX_CAM_BOKEH_FLATTEN_DEF, NOX_CAM_BOKEH_FLATTEN_MIN, NOX_CAM_BOKEH_FLATTEN_MAX, NOX_CAM_BOKEH_FLATTEN_DEF, 5, 0.2f);
				EM2EditSpin * emetVignette = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_VIGNETTE));
				emetVignette->setFont(rMain->fonts->em2editspin, false);
				emetVignette->bgImage = rMain->hBmpBg;
				emetVignette->setValuesToInt(NOX_CAM_BOKEH_VIGNETTE_DEF, NOX_CAM_BOKEH_VIGNETTE_MIN, NOX_CAM_BOKEH_VIGNETTE_MAX, NOX_CAM_BOKEH_VIGNETTE_DEF, 5, 0.2f);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case EMT_TAB_OPENED:
			{
				Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
				CHECK(cam);
				updateDiaphragmImage(rMain->getHWND(), cam);
				InvalidateRect(rMain->getHWND(), NULL, false);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_CAM_LIST_CAMS:
						{
							EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hWnd, IDC_REND2_CAM_LIST_CAMS));
							CHECK(emlv);
							if (wmEvent==LV_SELECTION_CHANGED)
							{
								Raytracer * rtr = Raytracer::getInstance();
								if (rtr->curScenePtr->sscene.activeCamera!=emlv->selected)
								{
									rMain->changeCameraOnThread();
								}
							}

						}
						break;
					case IDC_REND2_CAM_MB_ENABLED:
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_MB_ENABLED));
							CHECK(cam);
							CHECK(emc);
							cam->mb_on = emc->selected;
							rMain->updateLocksTabCamera(false);
						}
						break;
					case IDC_REND2_CAM_MB_STILL:
						{
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_MB_STILL));
							CHECK(emc);
							Raytracer::getInstance()->curScenePtr->sscene.motion_blur_still = emc->selected;
						}
						break;
					case IDC_REND2_CAM_MB_DURATION:
						{
							EM2EditSpin* emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_MB_DURATION));
							CHECK(emes);
							Raytracer::getInstance()->curScenePtr->sscene.motion_blur_time = emes->floatValue;
						}
						break;
					case IDC_REND2_CAM_ISO:
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin* emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_ISO));
							CHECK(cam);
							CHECK(emes);
							float f = (float)emes->intValue;
							cam->ISO = f;
							cam->iMod.setISO_camera(f);
							cam->iMod.evalMultiplierFromISO_and_EV();
						}
						break;
					case IDC_REND2_CAM_SHUTTER:
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin* emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_SHUTTER));
							CHECK(cam);
							CHECK(emes);
							cam->shutter = emes->floatValue;
						}
						break;
					case IDC_REND2_CAM_FOCUS_DIST:
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin* emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FOCUS_DIST));
							CHECK(cam);
							CHECK(emes);
							cam->focusDist = emes->floatValue;
						}
						break;
					case IDC_REND2_CAM_FOCAL:
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin* emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FOCAL));
							CHECK(cam);
							CHECK(emes);
							cam->setLengthHori(emes->floatValue);
							updateCameraPanelFOVfromCamera(cam, hWnd);
						}
						break;
					case IDC_REND2_CAM_F15MM:	setCameraAndPanelFocal(hWnd, 15.0f);	break;
					case IDC_REND2_CAM_F20MM:	setCameraAndPanelFocal(hWnd, 20.0f);	break;
					case IDC_REND2_CAM_F24MM:	setCameraAndPanelFocal(hWnd, 24.0f);	break;
					case IDC_REND2_CAM_F28MM:	setCameraAndPanelFocal(hWnd, 28.0f);	break;
					case IDC_REND2_CAM_F35MM:	setCameraAndPanelFocal(hWnd, 35.0f);	break;
					case IDC_REND2_CAM_F50MM:	setCameraAndPanelFocal(hWnd, 50.0f);	break;
					case IDC_REND2_CAM_F85MM:	setCameraAndPanelFocal(hWnd, 85.0f);	break;
					case IDC_REND2_CAM_F135MM:	setCameraAndPanelFocal(hWnd, 135.0f);	break;

					case IDC_REND2_CAM_APERTURE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_APERTURE));
							CHECK(cam);
							CHECK(emes);
							cam->aperture = emes->floatValue;

							updateDiaphragmImage(rMain->getHWND(), cam);
							InvalidateRect(rMain->getHWND(), NULL, false);

						}
						break;
					case IDC_REND2_CAM_DOF_ON:
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_DOF_ON));
							CHECK(cam);
							CHECK(emc);
							cam->dofOnTemp = emc->selected;

							rMain->updateLocksTabCamera(false);
						}
						break;
					case IDC_REND2_CAM_NUM_BLADES:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_NUM_BLADES));
							CHECK(cam);
							CHECK(emes);
							cam->apBladesNum = emes->intValue;

							updateDiaphragmImage(rMain->getHWND(), cam);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_CAM_BLADES_ANGLE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin* emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_ANGLE));
							CHECK(cam);
							CHECK(emes);
							cam->apBladesAngle = emes->floatValue;

							updateDiaphragmImage(rMain->getHWND(), cam);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_CAM_BLADES_RADIUS:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_RADIUS));
							CHECK(cam);
							CHECK(emes);
							cam->apBladesRadius = emes->floatValue;

							updateDiaphragmImage(rMain->getHWND(), cam);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_CAM_ROUND_BLADES:
						{
							CHECK(wmEvent==BN_CLICKED);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_CAM_ROUND_BLADES));
							CHECK(cam);
							CHECK(emc);
							cam->apBladesRound = emc->selected;

							rMain->updateLocksTabCamera(false);

							updateDiaphragmImage(rMain->getHWND(), cam);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_CAM_RING_BALANCE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_RING_BALANCE));
							CHECK(cam);
							CHECK(emes);
							int val = emes->intValue;
							cam->diaphSampler.intBokehRingBalance = val;
							if (val>=0)
								cam->diaphSampler.bokehRingBalance = val+1.0f;
							else
								cam->diaphSampler.bokehRingBalance = 1.0f + val/21.0f;

							updateDiaphragmImage(rMain->getHWND(), cam);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_CAM_RING_SIZE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_RING_SIZE));
							CHECK(cam);
							CHECK(emes);

							cam->diaphSampler.intBokehRingSize = emes->intValue;
							cam->diaphSampler.bokehRingSize = emes->intValue*0.01f;

							updateDiaphragmImage(rMain->getHWND(), cam);
							InvalidateRect(rMain->getHWND(), NULL, false);
						}
						break;
					case IDC_REND2_CAM_FLATTEN:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_FLATTEN));
							CHECK(cam);
							CHECK(emes);
							cam->diaphSampler.intBokehFlatten = emes->intValue;
							cam->diaphSampler.bokehFlatten = emes->intValue*0.01f;
						}
						break;
					case IDC_REND2_CAM_VIGNETTE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2EditSpin * emes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_CAM_VIGNETTE));
							CHECK(cam);
							CHECK(emes);
							cam->diaphSampler.intBokehVignette = emes->intValue;
							cam->diaphSampler.bokehVignette = emes->intValue*0.01f;
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelCamera()
{
	// TITLE
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TITLE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	// CAMERAS LIST 
	EM2ListView * emlvc = GetEM2ListViewInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_LIST_CAMS));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_LIST_CAMS), bgShiftXRightPanel, 0, emlvc->bgShiftX, emlvc->bgShiftY);

	// GROUPS
	EM2GroupBar * emgSelCam = GetEM2GroupBarInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_SELECT_CAMERA));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_SELECT_CAMERA), bgShiftXRightPanel, 0, emgSelCam->bgShiftX, emgSelCam->bgShiftY);
	EM2GroupBar * emgMB = GetEM2GroupBarInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_MOTION_BLUR));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_MOTION_BLUR), bgShiftXRightPanel, 0, emgMB->bgShiftX, emgMB->bgShiftY);
	EM2GroupBar * emgExp = GetEM2GroupBarInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_EXPOSURE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_EXPOSURE), bgShiftXRightPanel, 0, emgExp->bgShiftX, emgExp->bgShiftY);
	EM2GroupBar * emgFO = GetEM2GroupBarInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_FOCUS_OPTICS));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_FOCUS_OPTICS), bgShiftXRightPanel, 0, emgFO->bgShiftX, emgFO->bgShiftY);
	EM2GroupBar * emgAB = GetEM2GroupBarInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_APERTURE_BOKEH));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_APERTURE_BOKEH), bgShiftXRightPanel, 0, emgAB->bgShiftX, emgAB->bgShiftY);

	// MOTION BLUR
	EM2CheckBox * emcMBen = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_ENABLED));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_ENABLED), bgShiftXRightPanel, 0, emcMBen->bgShiftX, emcMBen->bgShiftY);
	EM2Text * emtduration = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_MB_DURATION));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_MB_DURATION), bgShiftXRightPanel, 0, emtduration->bgShiftX, emtduration->bgShiftY);
	EM2EditSpin * emsduration = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_DURATION));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_DURATION), bgShiftXRightPanel, 0, emsduration->bgShiftX, emsduration->bgShiftY);
	EM2CheckBox * emcMBstill = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_STILL));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_STILL), bgShiftXRightPanel, 0, emcMBstill->bgShiftX, emcMBstill->bgShiftY);

	// EXPOSURE
	EM2Text * emtiso = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_ISO));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_ISO), bgShiftXRightPanel, 0, emtiso->bgShiftX, emtiso->bgShiftY);
	EM2EditSpin * emsiso = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_ISO));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_ISO), bgShiftXRightPanel, 0, emsiso->bgShiftX, emsiso->bgShiftY);
	EM2Text * emtshutter = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_SHUTTER));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_SHUTTER), bgShiftXRightPanel, 0, emtshutter->bgShiftX, emtshutter->bgShiftY);
	EM2EditSpin * emsshutter = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_SHUTTER));	
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_SHUTTER), bgShiftXRightPanel, 0, emsshutter->bgShiftX, emsshutter->bgShiftY);

	// FOCUS AND OPTICS
	EM2Text * emtdist = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_DISTANCE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_DISTANCE), bgShiftXRightPanel, 0, emtdist->bgShiftX, emtdist->bgShiftY);
	EM2EditSpin * emsfdist = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOCUS_DIST));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOCUS_DIST), bgShiftXRightPanel, 0, emsfdist->bgShiftX, emsfdist->bgShiftY);
	EM2Text * emtmeters = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_METERS));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_METERS), bgShiftXRightPanel, 0, emtmeters->bgShiftX, emtmeters->bgShiftY);
	EM2Text * emtfocal = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOCAL));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOCAL), bgShiftXRightPanel, 0, emtfocal->bgShiftX, emtfocal->bgShiftY);
	EM2EditSpin * emsfocal = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOCAL));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOCAL), bgShiftXRightPanel, 0, emsfocal->bgShiftX, emsfocal->bgShiftY);

	// FOCAL BUTTONS
	EM2Button * embF15mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F15MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F15MM), bgShiftXRightPanel, 0, embF15mm->bgShiftX, embF15mm->bgShiftY);
	EM2Button * embF20mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F20MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F20MM), bgShiftXRightPanel, 0, embF20mm->bgShiftX, embF20mm->bgShiftY);
	EM2Button * embF24mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F24MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F24MM), bgShiftXRightPanel, 0, embF24mm->bgShiftX, embF24mm->bgShiftY);
	EM2Button * embF28mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F28MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F28MM), bgShiftXRightPanel, 0, embF28mm->bgShiftX, embF28mm->bgShiftY);
	EM2Button * embF35mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F35MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F35MM), bgShiftXRightPanel, 0, embF35mm->bgShiftX, embF35mm->bgShiftY);
	EM2Button * embF50mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F50MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F50MM), bgShiftXRightPanel, 0, embF50mm->bgShiftX, embF50mm->bgShiftY);
	EM2Button * embF85mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F85MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F85MM), bgShiftXRightPanel, 0, embF85mm->bgShiftX, embF85mm->bgShiftY);
	EM2Button * embF135mm = GetEM2ButtonInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_F135MM));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_F135MM), bgShiftXRightPanel, 0, embF135mm->bgShiftX, embF135mm->bgShiftY);

	// FOV
	EM2Text * emttextfovhori = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOV_HORI));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOV_HORI), bgShiftXRightPanel, 0, emttextfovhori->bgShiftX, emttextfovhori->bgShiftY);
	EM2Text * emttextfovvert = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOV_VERT));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOV_VERT), bgShiftXRightPanel, 0, emttextfovvert->bgShiftX, emttextfovvert->bgShiftY);
	EM2Text * emttextfovdiag = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOV_DIAG));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FOV_DIAG), bgShiftXRightPanel, 0, emttextfovdiag->bgShiftX, emttextfovdiag->bgShiftY);
	EM2Text * emtfovhori = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_HORI));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_HORI), bgShiftXRightPanel, 0, emtfovhori->bgShiftX, emtfovhori->bgShiftY);
	EM2Text * emtfovvert = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_VERT));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_VERT), bgShiftXRightPanel, 0, emtfovvert->bgShiftX, emtfovvert->bgShiftY);
	EM2Text * emtfovdiag = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_DIAG));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_DIAG), bgShiftXRightPanel, 0, emtfovdiag->bgShiftX, emtfovdiag->bgShiftY);

	// APERTURE
	EM2CheckBox * emcroundblades = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_ROUND_BLADES));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_ROUND_BLADES), bgShiftXRightPanel, 0, emcroundblades->bgShiftX, emcroundblades->bgShiftY);
	EM2Text * emtblades = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_BLADES));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_BLADES), bgShiftXRightPanel, 0, emtblades->bgShiftX, emtblades->bgShiftY);
	EM2EditSpin * emtnumbl = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_NUM_BLADES));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_NUM_BLADES), bgShiftXRightPanel, 0, emtnumbl->bgShiftX, emtnumbl->bgShiftY);
	EM2Text * emtaperture = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_APERTURE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_APERTURE), bgShiftXRightPanel, 0, emtaperture->bgShiftX, emtaperture->bgShiftY);
	EM2EditSpin * emtapert = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_APERTURE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_APERTURE), bgShiftXRightPanel, 0, emtapert->bgShiftX, emtapert->bgShiftY);
	EM2Text * emtangle = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_ANGLE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_ANGLE), bgShiftXRightPanel, 0, emtangle->bgShiftX, emtangle->bgShiftY);
	EM2EditSpin * emtblangle = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_BLADES_ANGLE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_BLADES_ANGLE), bgShiftXRightPanel, 0, emtblangle->bgShiftX, emtblangle->bgShiftY);
	EM2Text * emtradius = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_RADIUS));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_RADIUS), bgShiftXRightPanel, 0, emtradius->bgShiftX, emtradius->bgShiftY);
	EM2EditSpin * emtblradius = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_BLADES_RADIUS));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_BLADES_RADIUS), bgShiftXRightPanel, 0, emtblradius->bgShiftX, emtblradius->bgShiftY);

	// BOKEH
	EM2CheckBox * emcdofon = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_DOF_ON));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_DOF_ON), bgShiftXRightPanel, 0, emcdofon->bgShiftX, emcdofon->bgShiftY);
	EM2Text * emtringbalance = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_RING_BALANCE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_RING_BALANCE), bgShiftXRightPanel, 0, emtringbalance->bgShiftX, emtringbalance->bgShiftY);
	EM2EditSpin * emtbringbalance = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_RING_BALANCE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_RING_BALANCE), bgShiftXRightPanel, 0, emtbringbalance->bgShiftX, emtbringbalance->bgShiftY);
	EM2Text * emtringsize = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_RING_SIZE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_RING_SIZE), bgShiftXRightPanel, 0, emtringsize->bgShiftX, emtringsize->bgShiftY);
	EM2EditSpin * emtbringsize = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_RING_SIZE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_RING_SIZE), bgShiftXRightPanel, 0, emtbringsize->bgShiftX, emtbringsize->bgShiftY);
	EM2Text * emtflatten = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FLATTEN));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_FLATTEN), bgShiftXRightPanel, 0, emtflatten->bgShiftX, emtflatten->bgShiftY);
	EM2EditSpin * emtbflatten = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FLATTEN));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_FLATTEN), bgShiftXRightPanel, 0, emtbflatten->bgShiftX, emtbflatten->bgShiftY);
	EM2Text * emtvignette = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_VIGNETTE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_TEXT_VIGNETTE), bgShiftXRightPanel, 0, emtvignette->bgShiftX, emtvignette->bgShiftY);
	EM2EditSpin * emtbvignette = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_VIGNETTE));
	updateControlBgShift(GetDlgItem(hTabCamera, IDC_REND2_CAM_VIGNETTE), bgShiftXRightPanel, 0, emtbvignette->bgShiftX, emtbvignette->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartCameraTab(HWND hWnd)
{
	// srodek panela x=188
	int x, y;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TITLE), HWND_TOP, 159,11, 70, 16, 0);

	x=8; y=39;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_GR_SELECT_CAMERA),	HWND_TOP,			x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_LIST_CAMS),			HWND_TOP,			x,		y+17,		360,	132,		0);

	x=8; y=191;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_GR_MOTION_BLUR),	HWND_TOP,			x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_MB_ENABLED),		HWND_TOP,			x,		y+32,		70,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_MB_DURATION),	HWND_TOP,			x+99,	y+30,		70,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_MB_DURATION),		HWND_TOP,			x+175,	y+27,		55,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_MB_STILL),			HWND_TOP,			x+316,	y+32,		70,		10,			0);

	x=8; y=250;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_GR_EXPOSURE),		HWND_TOP,			x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_ISO),			HWND_TOP,			x-1,	y+35,		24,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_ISO),				HWND_TOP,			x+25,	y+32,		56,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_SHUTTER),		HWND_TOP,			x+211,	y+35,		80,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_SHUTTER),			HWND_TOP,			x+297,	y+32,		64,		20,			0);

	x=8; y=318;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_GR_FOCUS_OPTICS),	HWND_TOP,			x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_DISTANCE),		HWND_TOP,			x-1,	y+30,		56,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FOCUS_DIST),		HWND_TOP,			x+57,	y+27,		71,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_METERS),		HWND_TOP,			x+133,	y+30,		45,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOCAL),		HWND_TOP,			x+206,	y+30,		85,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FOCAL),				HWND_TOP,			x+297,	y+27,		64,		20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F15MM),				HWND_TOP,			x,		y+60,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F20MM),				HWND_TOP,			x+97,	y+60,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F24MM),				HWND_TOP,			x+194,	y+60,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F28MM),				HWND_TOP,			x+291,	y+60,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F35MM),				HWND_TOP,			x,		y+90,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F50MM),				HWND_TOP,			x+97,	y+90,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F85MM),				HWND_TOP,			x+194,	y+90,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_F135MM),			HWND_TOP,			x+291,	y+90,		69,		22,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOV_HORI),		HWND_TOP,			x,		y+126,		55,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOV_VERT),		HWND_TOP,			x+138,	y+126,		55,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FOV_DIAG),		HWND_TOP,			x+273,	y+126,		55,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FOV_HORI),			HWND_TOP,			x+55,	y+126,		32,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FOV_VERT),			HWND_TOP,			x+193,	y+126,		32,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FOV_DIAG),			HWND_TOP,			x+328,	y+126,		32,		14,			0);

	x=8; y=472;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_GR_APERTURE_BOKEH),	HWND_TOP,			x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_BLADES),		HWND_TOP,		x,		y+90,			80,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_RING_BALANCE),	HWND_TOP,		x,		y+120,			80,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_FLATTEN),		HWND_TOP,		x,		y+150,			80,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_APERTURE),		HWND_TOP,		x+240,	y+30,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_RADIUS),		HWND_TOP,		x+240,	y+60,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_ANGLE),		HWND_TOP,		x+240,	y+90,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_RING_SIZE),	HWND_TOP,		x+240,	y+120,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_TEXT_VIGNETTE),		HWND_TOP,		x+240,	y+150,			60,		16,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_NUM_BLADES),		HWND_TOP,		x+85,	y+ 88,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_RING_BALANCE),		HWND_TOP,		x+85,	y+118,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FLATTEN),			HWND_TOP,		x+85,	y+148,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_APERTURE),			HWND_TOP,		x+305,	y+ 28,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_RADIUS),		HWND_TOP,		x+305,	y+ 58,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_ANGLE),		HWND_TOP,		x+305,	y+ 88,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_RING_SIZE),			HWND_TOP,		x+305,	y+118,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_VIGNETTE),			HWND_TOP,		x+305,	y+148,			55,		20,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_DOF_ON),			HWND_TOP,		x+0,	y+33,			100,	10,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_ROUND_BLADES),		HWND_TOP,		x+0,	y+63,			100,	10,		0);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_MB_DURATION),	HWND_TOP,										0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_ISO),			GetDlgItem(hWnd, IDC_REND2_CAM_MB_DURATION),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_SHUTTER),		GetDlgItem(hWnd, IDC_REND2_CAM_ISO),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FOCUS_DIST),	GetDlgItem(hWnd, IDC_REND2_CAM_SHUTTER),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FOCAL),			GetDlgItem(hWnd, IDC_REND2_CAM_FOCUS_DIST),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_APERTURE),		GetDlgItem(hWnd, IDC_REND2_CAM_FOCAL),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_RADIUS),	GetDlgItem(hWnd, IDC_REND2_CAM_APERTURE),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_NUM_BLADES),	GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_RADIUS),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_ANGLE),	GetDlgItem(hWnd, IDC_REND2_CAM_NUM_BLADES),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_RING_BALANCE),	GetDlgItem(hWnd, IDC_REND2_CAM_BLADES_ANGLE),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_RING_SIZE),		GetDlgItem(hWnd, IDC_REND2_CAM_RING_BALANCE),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_FLATTEN),		GetDlgItem(hWnd, IDC_REND2_CAM_RING_SIZE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_CAM_VIGNETTE),		GetDlgItem(hWnd, IDC_REND2_CAM_FLATTEN),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillCameraTabAll()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	EM2ListView * em2list = GetEM2ListViewInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_LIST_CAMS));
	CHECK(em2list);
	em2list->freeEntries();

	char buf64[64];
	int numcams = sc->cameras.objCount;
	for (int i=0; i<numcams; i++)
	{
		em2list->addEmptyRow();
		sprintf_s(buf64, 64, "%d", i+1);
		em2list->setData(0, i, buf64);

		Camera * cam = sc->cameras[i];
		if (cam)
		{
			em2list->setData(1, i, cam->name);
		}
		else
		{
			em2list->setData(1, i, "Error - empty");
		}
	}

	if (numcams>0)
	{
		em2list->selected = sc->sscene.activeCamera;
		if (em2list->selected>=sc->cameras.objCount  ||  em2list->selected<0)
			em2list->selected = 0;
		Camera * cam = sc->cameras[em2list->selected];
		fillCameraData(cam);
	}
	else
	{
		em2list->selected = -1;
	}
	InvalidateRect(em2list->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::updateCamerasStateOnList()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	EM2ListView * em2list = GetEM2ListViewInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_LIST_CAMS));
	CHECK(em2list);
	if (em2list->getNumRows()!=sc->cameras.objCount)
		return fillCameraTabAll();

	for (int i=0; i<sc->cameras.objCount; i++)
	{
		Camera * cam = sc->cameras[i];
		bool empty = true;
		if (cam)
		{
			for (int j=0; j<16; j++)
				if (cam->blendBuffDirect[j].imgBuf)
					empty = false;
		}
		if (empty)
			em2list->setData(2, i, NULL);
		else
		{
			char buf[64];
			sprintf_s(buf, 64, "%d X %d", cam->width, cam->height);
			em2list->setData(2, i, buf);
		}
	}
	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillCameraData(Camera * cam)
{
	if (!cam)
		return false;

	EM2CheckBox * emcMBon = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_ENABLED));
	EM2CheckBox * emcMBstill = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_STILL));
	EM2EditSpin * emsMBdur = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_DURATION));
	EM2EditSpin * emsISO = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_ISO));
	EM2EditSpin * emsShutter = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_SHUTTER));
	EM2EditSpin * emsDist = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOCUS_DIST));
	EM2EditSpin * emsFocal = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOCAL));
	EM2Text * emtFOVhori = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_HORI));
	EM2Text * emtFOVvert = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_VERT));
	EM2Text * emtFOVdiag = GetEM2TextInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FOV_DIAG));
	EM2EditSpin * emtAperture = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_APERTURE));
	EM2CheckBox * emcDOFon = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_DOF_ON));
	EM2EditSpin * emtBlades = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_NUM_BLADES));
	EM2EditSpin * emtAngle = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_BLADES_ANGLE));
	EM2EditSpin * emtRadius = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_BLADES_RADIUS));
	EM2CheckBox * emcRound = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_ROUND_BLADES));
	EM2EditSpin * emtBalance = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_RING_BALANCE));
	EM2EditSpin * emtRingSize = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_RING_SIZE));
	EM2EditSpin * emtFlatten = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_FLATTEN));
	EM2EditSpin * emtVignette = GetEM2EditSpinInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_VIGNETTE));

	emcMBon->selected = cam->mb_on;
	InvalidateRect(emcMBon->hwnd, NULL, false);
	emcMBstill->selected = Raytracer::getInstance()->curScenePtr->sscene.motion_blur_still;
	InvalidateRect(emcMBstill->hwnd, NULL, false);
	emsMBdur->setFloatValue(Raytracer::getInstance()->curScenePtr->sscene.motion_blur_time);
	emsISO->setIntValue((int)cam->ISO);
	emsShutter->setFloatValue(cam->shutter);
	emsDist->setFloatValue(cam->focusDist);
	float f = 0.5f * 36 / tan(cam->angle * PI / 360.0f);
	emsFocal->setFloatValue(f);
	emtAperture->setFloatValue(cam->aperture);
	emcDOFon->selected = cam->dofOnTemp;
	InvalidateRect(emcDOFon->hwnd, NULL, false);
	emtBlades->setIntValue(cam->apBladesNum);
	emtAngle->setFloatValue(cam->apBladesAngle);
	emtRadius->setFloatValue(cam->apBladesRadius);
	emcRound->selected = cam->apBladesRound;
	InvalidateRect(emcRound->hwnd, NULL, false);

	emtBalance->setIntValue(cam->diaphSampler.intBokehRingBalance);
	emtRingSize->setIntValue(cam->diaphSampler.intBokehRingSize);
	emtFlatten->setIntValue(cam->diaphSampler.intBokehFlatten);
	emtVignette->setIntValue(cam->diaphSampler.intBokehVignette);

	updateCameraPanelFOVfromCamera(cam, hTabCamera);

	return true;
}

//------------------------------------------------------------------------------------------------

void updateCameraPanelFOVfromCamera(Camera * cam, HWND hCamPanel)
{
	if (!cam)
		return;

	float f = 0.5f * 36 / tan(cam->angle * PI / 360.0f);

	int ww = max(1, cam->width);
	int hh = max(1, cam->height);
	float ssh = 18.0f*hh/ww;
	float ssd = sqrt(ssh*ssh + 18.0f*18.0f);
	char buf[64];
	sprintf_s(buf, 64, "%.1f�", (360.0f/PI*atan(18.0f / f)));
	SetWindowText(GetDlgItem(hCamPanel, IDC_REND2_CAM_FOV_HORI), buf);
	sprintf_s(buf, 64, "%.1f�", (360.0f/PI*atan(ssh / f)));
	SetWindowText(GetDlgItem(hCamPanel, IDC_REND2_CAM_FOV_VERT), buf);
	sprintf_s(buf, 64, "%.1f�", (360.0f/PI*atan(ssd / f)));
	SetWindowText(GetDlgItem(hCamPanel, IDC_REND2_CAM_FOV_DIAG), buf);
}

//------------------------------------------------------------------------------------------------

bool setCameraAndPanelFocal(HWND hCamPanel, float focal)
{
	Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
	EM2EditSpin* emes = GetEM2EditSpinInstance(GetDlgItem(hCamPanel, IDC_REND2_CAM_FOCAL));
	CHECK(cam);
	CHECK(emes);
	emes->setFloatValue(focal);
	cam->setLengthHori(emes->floatValue);
	updateCameraPanelFOVfromCamera(cam, hCamPanel);
	return true;
}

//------------------------------------------------------------------------------------------------

bool updateDiaphragmImage(HWND hWnd, Camera * cam)
{
	CHECK(hWnd);
	CHECK(cam);

	cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
	HDC thdc = GetDC(hWnd);
	HBITMAP hbmp = cam->diaphSampler.createApertureShapePreviewAlpha(thdc , 64);
	ReleaseDC(hWnd, thdc);
	if (!hbmp)
		return false;
	RendererMain2 * rMain = RendererMain2::getInstance();
	if (rMain->hDiaph)
		DeleteObject(rMain->hDiaph);
	rMain->hDiaph = hbmp;

	return true;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::changeCamera(int id, bool updListPos)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (id<0  ||  id>=sc->cameras.objCount)
		return false;
	sc->sscene.activeCamera = id;
	Camera * cam = sc->getActiveCamera();
	CHECK(cam);

	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_LIST_CAMS));
	if (emlv  &&  updListPos)
	{
		emlv->selected = id;
		InvalidateRect(emlv->hwnd, NULL, false);
	}
	
	EMPView * empv = GetEMPViewInstance(hViewport);
	empv->imgMod->copyDataFromAnother(&cam->iMod);

	fillCameraData(cam);
	fillPostTabAll(&cam->iMod);
	fillCorrectionTabAll(&cam->iMod);
	fillFakeDOFTabAll(&cam->fMod);
	fillEffectsTabAll(cam, &cam->fMod);

	if (!cam->imgBuff)
		cam->imgBuff = new ImageBuffer();
	if (cam->imgBuff->width<1 || cam->imgBuff->height<1 || !cam->imgBuff->imgBuf || !cam->imgBuff->hitsBuf)
		cam->imgBuff->allocBuffer(cam->width*cam->aa, cam->height*cam->aa);

	empv->otherSource = cam->imgBuff;
	empv->byteBuff->allocBuffer(cam->width, cam->height);

	refreshRender(NULL);	// this function isis called from thread already, no need to create another thread

	return true;
}

//------------------------------------------------------------------------------------------------

DWORD WINAPI changeCameraStuffThreadProc(LPVOID lpParameter)
{
	RendererMain2 * rMain = RendererMain2::getInstance();

	EnableWindow(rMain->hTabCamera, FALSE);
	UpdateWindow(rMain->hTabCamera);
	EnableWindow(GetDlgItem(rMain->hTabCamera, IDC_REND2_CAM_LIST_CAMS), FALSE);
	Sleep(2);

	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(rMain->hTabCamera, IDC_REND2_CAM_LIST_CAMS));
	int id = emlv->selected;
	bool ok = rMain->changeCamera(id, true);
	circleActive = false;


	EnableWindow(GetDlgItem(rMain->hTabCamera, IDC_REND2_CAM_LIST_CAMS), TRUE);
	EnableWindow(rMain->hTabCamera, TRUE);
	UpdateWindow(rMain->hTabCamera);

	rMain->updateLocksTabCamera(false);

	Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
	CHECK(cam);
	updateDiaphragmImage(rMain->getHWND(), cam);
	InvalidateRect(rMain->getHWND(), NULL, false);
	rMain->changeRenderResFromCam(cam);

	return 0;
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::changeCameraOnThread()
{
	RECT crect;
	GetClientRect(hViewport, &crect);
	openAnimCircleDialog(getHWND(), (crect.right)/2, (crect.bottom)/2);

	DWORD threadID;
	HANDLE hThread = CreateThread( 
            NULL,
            0,
            (LPTHREAD_START_ROUTINE)(changeCameraStuffThreadProc),
            (LPVOID)0,
            0,
            &threadID);
	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabCamera(bool nowrendering)
{
	if (!nowrendering)
	{
		EM2CheckBox * emcMBon = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_MB_ENABLED));
		setControlEnabled(emcMBon->selected, hTabCamera, IDC_REND2_CAM_MB_DURATION);
		setControlEnabled(emcMBon->selected, hTabCamera, IDC_REND2_CAM_MB_STILL);
		setControlEnabled(emcMBon->selected, hTabCamera, IDC_REND2_CAM_TEXT_MB_DURATION);

		EM2CheckBox * emcDOFon = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_DOF_ON));
		EM2CheckBox * emcRound = GetEM2CheckBoxInstance(GetDlgItem(hTabCamera, IDC_REND2_CAM_ROUND_BLADES));
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_ROUND_BLADES);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_NUM_BLADES);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_RING_BALANCE);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_FLATTEN);
		setControlEnabled(emcDOFon->selected && emcRound->selected, hTabCamera, IDC_REND2_CAM_BLADES_RADIUS);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_BLADES_ANGLE);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_RING_SIZE);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_VIGNETTE);

		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_TEXT_BLADES);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_TEXT_RING_BALANCE);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_TEXT_FLATTEN);
		setControlEnabled(emcDOFon->selected && emcRound->selected, hTabCamera, IDC_REND2_CAM_TEXT_RADIUS);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_TEXT_ANGLE);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_TEXT_RING_SIZE);
		setControlEnabled(emcDOFon->selected, hTabCamera, IDC_REND2_CAM_TEXT_VIGNETTE);
	}
}

//------------------------------------------------------------------------------------------------
