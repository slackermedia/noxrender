#ifndef _nox_exporter_h_
#define _nox_exporter_h_

#ifndef ID_NOX_EXP
#define ID_NOX_EXP 1030912
#endif

Bool RegisterNOXexporter(void);

#include <tchar.h>
#include "c4d_misc.h"

struct MeshID
{
	unsigned long long c4ID;
	unsigned int noxID;
};

struct TexModNox
{
	float brightness;
	float contrast;
	float saturation;
	float hue;
	float red;
	float green;
	float blue;
	float gamma;
	bool invert;
	bool interpolateProbe;
	float normalPowerEV;
	bool normalInvertX;
	bool normalInvertY;
	TexModNox()	{	brightness = 0.0f; contrast = 1.0f; saturation = 0.0f; hue = 0.0f; 
					red = 0.0f; green = 0.0f; blue = 0.0f; gamma = 2.2f; invert = false; 
					interpolateProbe = true; 
					normalPowerEV = 0.0f; normalInvertX = false; normalInvertY = false; }
	~TexModNox()	{}
};

class NOXexporter: public SceneSaverData
{
public:
	virtual FILEERROR Save(BaseSceneSaver* node, const Filename& name, BaseDocument* gdoc, SCENEFILTER filterflags);
	bool runNox(TCHAR * filename);

	BaseDocument * doc;
	BaseObject * sunsky;
	BaseObject * envsky;
	FILE * file;
	bool bShowError;
	bool bShowInfo;
	bool bShowWarning;
	bool bShowInfoDetailed;
	unsigned long long geometrySize;
	unsigned int trisCount;
	unsigned int meshCount;
	unsigned int matsCount;
	unsigned long long camerasSize;
	unsigned int numCameras;
	unsigned int uniqueInstanceID;
	unsigned int uniqueMatID;
	float scale;

	c4d_misc::BaseArray<MeshID> instanceIDs;

	void evalSizeGeometry();
	void evalSizeCameras();
	unsigned long long evalSizeScene();
	unsigned int evalSizeTriangle();

	bool showErrorReturnFalse(char * error);
	void showInfo(char * info);
	void showDetailedInfo(char * info);
	void showWarning(char * warning);

	void enumObjectsEvalGeomSize(BaseObject * obj);
	void enumObjectsEvalCamerasSize(BaseObject * obj);
	bool enumObjectsAddCameras(BaseObject * obj);
	bool enumObjectsAddMeshes(BaseObject * obj);
	bool enumObjectsAddInstances(BaseObject * obj);
	unsigned long long evalMeshSize(BaseObject * obj);
	unsigned long long evalInstanceSourceSize(BaseObject * obj);
	unsigned long long evalInstanceSize(BaseObject * obj);
	unsigned long long evalCameraSize(CameraObject * obj);
	unsigned long long evalMaterialsSize();
	unsigned long long evalDefaultMaterialSize();
	unsigned long long evalMaterialSize(BaseMaterial * mat);
	unsigned long long evalTextureSize(BaseMaterial * mat, unsigned  int texC4Did);
	unsigned long long evalSunskySize();
	unsigned long long evalEnvmapSize();

	BaseObject * findInstanceObjectSource(BaseObject * obj);
	int getInstanceIDfromGUID(unsigned long long id);

	BaseObject * findSunsky(BaseObject * obj);
	BaseObject * findSunskyEnv(BaseObject * obj);
	BaseTag * findSelectionByName(BaseObject * obj, String name);
	unsigned int findMaterialID(BaseMaterial * mat);
	bool fillMeshMaterialsArray(BaseObject * obj, unsigned int * mats, unsigned int mcount);

	bool insertScenePart();
	bool insertCameraPart(CameraObject * cam);
	bool insertGeometryPart();
	bool insertInstancePart(BaseObject * obj);
	bool insertInstanceSourcePart(BaseObject * obj);
	bool insertMeshPart(BaseObject * obj);
	bool insertTriangle(PolygonObject *pobj, unsigned int v1, unsigned int v2, unsigned int v3, unsigned int matID, Vector uv1, Vector uv2, Vector uv3, SVector n1, SVector n2, SVector n3);
	bool insertMaterials();
	bool insertDefaultMaterial();
	bool insertMaterial(BaseMaterial * mat);
	bool insertColor(unsigned short tag, Vector col, char * errStr);
	bool insertSunsky();
	bool insertEnvmap();

	void tempDoShitMaterial(BaseMaterial * mat);
	void tempDoShitCamera(CameraObject * cam);
	char * getTexFilename(BaseMaterial * mat, unsigned int chID);
	TexModNox getTexMod(BaseMaterial * mat, unsigned int channelID);

	bool insertTexturePart(BaseMaterial * mat, int texNoxID, unsigned int texC4Did, TexModNox tmod);

	static NodeData *Alloc(void) { return gNew NOXexporter; }
	NOXexporter() { doc=NULL; file=NULL; }

};

#endif
