#include <windows.h>
#include <stdio.h>
#include "EMControls.h"

extern HMODULE hDllModule;

void InitEMEditSpin()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMEditSpin";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMEditSpinProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMEditSpin *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMEditSpin::EMEditSpin(HWND hWnd)
{
	colBackground = RGB(96,192,255);
	colButtonBorderUpLeft = RGB(128,192,255);
	colButtonBorderDownRight = RGB(32,96,208);
	colButtonBackground = RGB(64,128,255);
	colButtonBackgroundClick = RGB(96,160,255);
	colArrow = RGB(0,0,0);
	colArrowClicked = RGB(255,0,0);
	colEditBorderUpLeft = RGB(64,160,224);
	colEditBorderDownRight = RGB(128,224,255);
	colEditBackground = RGB(160,240,255);
	colEditText = RGB(0,0,0);
	colDisabledBorderLight = RGB(224,224,224);
	colDisabledBorderDark = RGB(160,160,160);
	colDisabledBackground = RGB(192,192,192);
	colDisabledEditBackground = RGB(192,192,192);
	colDisabledEditText = RGB(96,96,96);
	colDisabledArrow = RGB(128,128,128);

	hEditBrush = CreateSolidBrush(colEditBackground);
	hEditDisabledBrush = CreateSolidBrush(colDisabledEditBackground);
	hwnd = hWnd;
	hEdit = 0;

	nowScrolling = false;
	clicked1 = false;
	clicked2 = false;
	overButton1 = false;
	overButton2 = false;

	allowFloat = true;

	height = 18;	// even val is better
	buttonHeight = height/2;
	buttonWidth = 12;

	RECT rect;
	GetClientRect(hWnd, &rect);

	rEdit = rect;
	editRectWidth = rect.right - buttonWidth;
	rEdit.right = editRectWidth;

	rUpper = rect;
	rUpper.left = editRectWidth;
	rUpper.bottom = buttonHeight;

	rLower = rect;
	rLower.left = editRectWidth;
	rLower.top = buttonHeight;

	minIntValue = 0;
	maxIntValue = 100;
	minFloatValue = 0;
	maxFloatValue = 100;
	intValue = minIntValue;
	floatValue = minFloatValue;
	intChangeValue = 1;
	floatChangeValue = 1;
	intMouseChangeValue = 0.1f;
	floatMouseChangeValue = 0.1f;
	floatAfterDot = 2;
}

EMEditSpin::~EMEditSpin()
{
	if (hEditBrush)
		DeleteObject(hEditBrush);
}

EMEditSpin * GetEMEditSpinInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMEditSpin * emes = (EMEditSpin *)GetWindowLongPtr(hwnd, 0);
	#else
		EMEditSpin * emes = (EMEditSpin *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emes;
}

void SetEMEditSpinInstance(HWND hwnd, EMEditSpin *emes)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emes);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emes);
	#endif
}

typedef LRESULT (WINAPI * EDITPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EDITPROC OldEditSpinProc;

void verifyEditSpinControlData(HWND hwnd, int &lastIntValue, float &lastFloatValue)
{
	char * addr;
	char * addr1;
	char * addr2;
	char buff[512];
	memset(buff, 0, 512);
	((WORD*)buff)[0] = 512;
	int cpied = (int)SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
	if (cpied>=0)
		buff[cpied]=0;
	HWND hFazia = GetParent(hwnd);
	EMEditSpin * emes;
	emes = GetEMEditSpinInstance(hFazia);
	bool notify  = false;
	if (emes->allowFloat)
	{
		float a = (float)_strtod_l(buff, &addr1, noxLocale);
		float b = (float)_strtod_l(buff, &addr2, noxLocaleComa);
		if (addr1 == buff   &&   addr2 == buff)
		{	// conversion error (to float)
			a = lastFloatValue;
			emes->floatToEditbox(a);
		}
		else
		{
			if (addr2>addr1)
				a = b;
			if (a > emes->maxFloatValue)
				a = emes->maxFloatValue;
			if (a < emes->minFloatValue)
				a = emes->minFloatValue;
			if (lastFloatValue != a)
				if (fabs(lastFloatValue-a) > pow(0.1f, emes->floatAfterDot))
					notify = true;
			emes->floatValue = a;
			emes->floatToEditbox(a);
			lastFloatValue = a;
		}
	}
	else
	{
		int a = (int)_strtoi64(buff, &addr, 10);
		if (addr == buff)
		{	// conversion error (to int)
			a = lastIntValue;
			emes->intToEditbox(a);
		}
		else
		{
			if (a > emes->maxIntValue)
				a = emes->maxIntValue;
			if (a < emes->minIntValue)
				a = emes->minIntValue;
			if (lastIntValue != a)
				notify = true;
			emes->intValue = a;
			emes->intToEditbox(a);
			lastIntValue = a;
		}
	}
	if (notify)
		emes->notifyScrolling();
	InvalidateRect(hFazia, NULL, FALSE);
}

LRESULT CALLBACK EMEditSpinEditProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static int lastIntValue = 0;
	static float lastFloatValue = 0;

	switch (msg)
	{
	case 65001:
		{
			lastIntValue = (int&)wParam;
			lastFloatValue = (float&)lParam;
		}
		break;
	case WM_CHAR:
	//case WM_KEYDOWN:
		{
			if (wParam == VK_RETURN)
			{
				verifyEditSpinControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_RETURN), (LPARAM)hwnd);
				SendMessage(hwnd, EM_SETSEL, 0, -1);
				return TRUE;
			}
			if (wParam == VK_ESCAPE)
			{
				HWND hFazia = GetParent(hwnd);
				EMEditSpin * emes = GetEMEditSpinInstance(hFazia);
				if (emes->allowFloat)
				{
					emes->floatValue = lastFloatValue;
					emes->floatToEditbox(lastFloatValue);
				}
				else
				{
					emes->intValue = lastIntValue;
					emes->intToEditbox(lastIntValue);
				}
				return TRUE;
			}
			if (wParam == VK_TAB)
			{
				verifyEditSpinControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				if (GetKeyState(VK_SHIFT)&0x8000)
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB_SHIFT), (LPARAM)hwnd);
				else
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB), (LPARAM)hwnd);

				return TRUE;
			}
		}
		break;
	case WM_KILLFOCUS:
		{
			verifyEditSpinControlData(hwnd, lastIntValue, lastFloatValue);
		}
		break;
	case WM_SETFOCUS:
		{
			char * addr;
			char buff[512];
			((WORD*)buff)[0] = 512;
			SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
			HWND hFazia = GetParent(hwnd);
			EMEditSpin * emes;
			emes = GetEMEditSpinInstance(hFazia);
			if (emes->allowFloat)
			{
				float a = (float)strtod(buff, &addr);
				if (addr == buff)
				{	// conversion error (to float)
				}
				else
				{
					if (a > emes->maxFloatValue)
						a = emes->maxFloatValue;
					if (a < emes->minFloatValue)
						a = emes->minFloatValue;
					lastFloatValue = a;
				}
			}
			else
			{
				int a = (int)_strtoi64(buff, &addr, 10);
				if (addr == buff)
				{	// conversion error (to int)
				}
				else
				{
					if (a > emes->maxIntValue)
						a = emes->maxIntValue;
					if (a < emes->minIntValue)
						a = emes->minIntValue;
					lastIntValue = a;
				}
			}
			SendMessage(hwnd, EM_SETSEL, 0, -1);
		}
		break;
	default:
		return CallWindowProc(OldEditSpinProc, hwnd, msg, wParam, lParam);
	}

	return CallWindowProc(OldEditSpinProc, hwnd, msg, wParam, lParam);
}


LRESULT CALLBACK EMEditSpinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMEditSpin * emes;
	emes = GetEMEditSpinInstance(hwnd);
	static int lastPosY;
	static int lastIntValue;
	static float lastFloatValue;
	static HRGN rgn = 0;

    switch(msg)
    {
	case WM_CREATE:
		{
			EMEditSpin * emes1 = new EMEditSpin(hwnd);
			SetEMEditSpinInstance(hwnd, (EMEditSpin *)emes1);
			emes1->hEdit = CreateWindow("EDIT", "", 
				WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VISIBLE | ES_RIGHT 
				| ES_MULTILINE | ES_WANTRETURN,
				1, 1, emes1->editRectWidth-2, emes1->height-2,
				hwnd, (HMENU)0, NULL, 0);
			SendMessage(emes1->hEdit, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

			#ifdef _WIN64
				OldEditSpinProc = (EDITPROC) SetWindowLongPtr(emes1->hEdit, GWLP_WNDPROC, (LONG_PTR)EMEditSpinEditProc) ;
			#else
				OldEditSpinProc = (EDITPROC) (HANDLE)(LONG_PTR)SetWindowLong(emes1->hEdit, GWL_WNDPROC, (LONG)(LONG_PTR)EMEditSpinEditProc) ;
			#endif

			if (emes1->allowFloat)
				emes1->floatToEditbox(emes1->floatValue);
			else
				emes1->intToEditbox(emes1->intValue);

			RECT rect;
			GetClientRect(hwnd, &rect);

			POINT rp[] = {
				{0,0},
				{rect.right,0},
				{rect.right, emes1->height},
				{0, emes1->height},
			};

			rgn = CreatePolygonRgn(rp, 4, WINDING);
			SetWindowRgn(hwnd, rgn, false);

		}
		break;
	case WM_SIZE:
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			rect.right = LOWORD(lParam);
			rect.bottom = HIWORD(lParam);

			emes->rEdit = rect;
			emes->editRectWidth = rect.right - emes->buttonWidth;
			emes->rEdit.right = emes->editRectWidth;

			emes->rUpper = rect;
			emes->rUpper.left = emes->editRectWidth;
			emes->rUpper.bottom = emes->buttonHeight;

			emes->rLower = rect;
			emes->rLower.left = emes->editRectWidth;
			emes->rLower.top = emes->buttonHeight;

			SetWindowPos(emes->hEdit, HWND_TOP, 1, 1, emes->editRectWidth-2, emes->height-2, SWP_NOZORDER);

			POINT rp[] = {
				{0,0},
				{rect.right,0},
				{rect.right, emes->height},
				{0, emes->height},
			};
			rgn = CreatePolygonRgn(rp, 4, WINDING);
			SetWindowRgn(hwnd, rgn, false);
		}
		break;
	case WM_CLOSE:
		{
			return TRUE;
		}
		break;
	case WM_NCDESTROY:
		{
			EMEditSpin * emes1;
			emes1 = GetEMEditSpinInstance(hwnd);
			delete emes1;
			SetEMEditSpinInstance(hwnd, 0);
			DeleteObject(rgn);
		}
		break;
	case WM_CTLCOLOREDIT:
	{
		HDC hdc1 = (HDC)wParam;
		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);
		if (!disabled)
		{
			SetTextColor(hdc1, emes->colEditText);
			SetBkColor(hdc1, emes->colEditBackground);
		}
		else
		{
			SetTextColor(hdc1, emes->colDisabledEditText);
			SetBkColor(hdc1, emes->colDisabledEditBackground);
		}
		if (disabled)
			return (INT_PTR)(emes->hEditDisabledBrush);
		else
			return (INT_PTR)(emes->hEditBrush);
	}
		break;
	case WM_PAINT:
	{
		if (!emes) 
			break;

		RECT rect, sRect;
		HDC hdc, thdc;
		PAINTSTRUCT ps;
		HPEN hOldPen;

		bool drawButton1Clicked = emes->clicked1;
		bool drawButton2Clicked = emes->clicked2;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);

		// get DC and create backbuffer DC
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rect);
		thdc = CreateCompatibleDC(hdc); 
		HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
		HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

		// get spinner part rect
		sRect = rect;
		sRect.left = emes->editRectWidth;

		// paint whole control
		POINT cBorder[] = {	{0, 0}, {rect.right,0}, 
							{rect.right,rect.bottom}, 
							{0,rect.bottom} };	// 4

		HRGN crg1 = CreatePolygonRgn(cBorder, 4, WINDING);
		HBRUSH cfbr = CreateSolidBrush(emes->colBackground);
		FillRgn(thdc, crg1, cfbr);
		DeleteObject(cfbr);
		DeleteObject(crg1);

		// draw editbox border
		POINT eBorder1[] = {
			{0,   emes->height-1 },
			{0,   0},
			{emes->editRectWidth,   0}
		};

		POINT eBorder2[] = {
			{emes->editRectWidth-1,   1},
			{emes->editRectWidth-1,   emes->height-1},
			{0, emes->height-1}
		};

		hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colEditBorderUpLeft));
		Polyline(thdc, eBorder1, 3);
		DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colEditBorderDownRight)));
		Polyline(thdc, eBorder2, 3);
		DeleteObject(SelectObject(thdc, hOldPen));

		// fill upper button
		POINT uBorder[] = {
			{emes->editRectWidth, 0},
			{emes->editRectWidth + emes->buttonWidth, 0},
			{emes->editRectWidth + emes->buttonWidth,   emes->buttonHeight},
			{emes->editRectWidth,  emes->buttonHeight}   };	// 4

		HRGN urg1 = CreatePolygonRgn(uBorder, 4, WINDING);
		HBRUSH ufbr1;
		if (disabled)
			ufbr1 = CreateSolidBrush(emes->colDisabledBackground);
		else
			if (drawButton1Clicked)
				ufbr1 = CreateSolidBrush(emes->colButtonBackgroundClick);
			else
				ufbr1 = CreateSolidBrush(emes->colButtonBackground);
		FillRgn(thdc, urg1, ufbr1);
		DeleteObject(ufbr1);
		DeleteObject(urg1);

		// draw upper button border
		POINT b1Border1[] = {
			{emes->editRectWidth, emes->buttonHeight-1},
			{emes->editRectWidth, 0},
			{emes->editRectWidth + emes->buttonWidth, 0}
		};
		POINT b1Border2[] = {
			{emes->editRectWidth + emes->buttonWidth-1, 1},
			{emes->editRectWidth + emes->buttonWidth-1, emes->buttonHeight-1},
			{emes->editRectWidth, emes->buttonHeight-1}
		};

		if (disabled)
		{
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colDisabledBorderLight));
			Polyline(thdc, b1Border1, 3);
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colDisabledBorderDark)));
			Polyline(thdc, b1Border2, 3);
		}
		else
		{
			if (drawButton1Clicked)
			{
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderDownRight));
				Polyline(thdc, b1Border1, 3);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderUpLeft)));
				Polyline(thdc, b1Border2, 3);
			}
			else
			{
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderUpLeft));
				Polyline(thdc, b1Border1, 3);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderDownRight)));
				Polyline(thdc, b1Border2, 3);
			}
		}
		DeleteObject(SelectObject(thdc, hOldPen));

		// draw upper arrow
		POINT uaBorder[] = {
			{emes->editRectWidth + emes->buttonWidth/2-4, emes->buttonHeight/2+3},
			{emes->editRectWidth + emes->buttonWidth/2+4, emes->buttonHeight/2+3},
			{emes->editRectWidth + emes->buttonWidth/2, emes->buttonHeight/2-2} };	// 3

		HRGN uarg1 = CreatePolygonRgn(uaBorder, 3, WINDING);
		HBRUSH uafbr1;
		if (disabled)
			uafbr1 = CreateSolidBrush(emes->colDisabledArrow);
		else
			if (drawButton1Clicked)
				uafbr1 = CreateSolidBrush(emes->colArrowClicked);
			else
				uafbr1 = CreateSolidBrush(emes->colArrow);
		FillRgn(thdc, uarg1, uafbr1);
		DeleteObject(uafbr1);
		DeleteObject(uarg1);


		// fill lower button
		POINT dBorder[] = {
			{emes->editRectWidth, emes->buttonHeight},
			{emes->editRectWidth + emes->buttonWidth, emes->buttonHeight},
			{emes->editRectWidth + emes->buttonWidth, emes->buttonHeight*2},
			{emes->editRectWidth, emes->buttonHeight*2} };	// 4

		HRGN drg1 = CreatePolygonRgn(dBorder, 4, WINDING);
		HBRUSH dfbr1;
		if (disabled)
			dfbr1 = CreateSolidBrush(emes->colDisabledBackground);
		else
			if (drawButton2Clicked)
				dfbr1 = CreateSolidBrush(emes->colButtonBackgroundClick);
			else
				dfbr1 = CreateSolidBrush(emes->colButtonBackground);

		FillRgn(thdc, drg1, dfbr1);
		DeleteObject(dfbr1);
		DeleteObject(drg1);

		// draw lower button border
		POINT b2Border1[] = {
			{emes->editRectWidth, emes->buttonHeight-1 + emes->buttonHeight},
			{emes->editRectWidth, emes->buttonHeight},
			{emes->editRectWidth + emes->buttonWidth, emes->buttonHeight}
		};
		POINT b2Border2[] = {
			{emes->editRectWidth + emes->buttonWidth-1, 1 + emes->buttonHeight},
			{emes->editRectWidth + emes->buttonWidth-1, emes->buttonHeight-1 + emes->buttonHeight},
			{emes->editRectWidth, emes->buttonHeight-1 + emes->buttonHeight}
		};

		if (disabled)
		{
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colDisabledBorderLight));
			Polyline(thdc, b2Border1, 3);
			DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colDisabledBorderDark)));
			Polyline(thdc, b2Border2, 3);
		}
		else
		{
			if (drawButton2Clicked)
			{
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderDownRight));
				Polyline(thdc, b2Border1, 3);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderUpLeft)));
				Polyline(thdc, b2Border2, 3);
			}
			else
			{
				hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderUpLeft));
				Polyline(thdc, b2Border1, 3);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colButtonBorderDownRight)));
				Polyline(thdc, b2Border2, 3);
			}
		}
		DeleteObject(SelectObject(thdc, hOldPen));

		// draw lower arrow
		POINT laBorder[] = {
			{emes->editRectWidth + emes->buttonWidth/2-3, emes->buttonHeight+emes->buttonHeight/2-2},
			{emes->editRectWidth + emes->buttonWidth/2+4, emes->buttonHeight+emes->buttonHeight/2-2},
			{emes->editRectWidth + emes->buttonWidth/2,	  emes->buttonHeight+emes->buttonHeight/2+2} };	// 3

		HRGN larg1 = CreatePolygonRgn(laBorder, 3, WINDING);
		HBRUSH lafbr1;
		if (disabled)
			lafbr1 = CreateSolidBrush(emes->colDisabledArrow);
		else
			if (drawButton2Clicked)
				lafbr1 = CreateSolidBrush(emes->colArrowClicked);
			else
				lafbr1 = CreateSolidBrush(emes->colArrow);
		FillRgn(thdc, larg1, lafbr1);
		DeleteObject(lafbr1);
		DeleteObject(larg1);

		// copy from back buffer and free resources
		BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
		SelectObject(thdc, oldBMP);
		DeleteObject(hbmp);
		DeleteDC(thdc);
		EndPaint(hwnd, &ps);

		InvalidateRect(emes->hEdit, NULL, false);
	}
	break;

	case WM_MOUSEMOVE:
	{
		if (!GetCapture())
			SetCapture(hwnd);
		if (GetCapture() == hwnd)
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			rect.left = emes->editRectWidth;
			POINT pt;
			pt.x = (short)LOWORD(lParam);
			pt.y = (short)HIWORD(lParam);
			if (PtInRect(&rect, pt) || emes->nowScrolling)
			{
				if (PtInRect(&(emes->rUpper), pt))
					emes->overButton1 = true;
				else
					emes->overButton1 = false;

				if (PtInRect(&(emes->rLower), pt))
					emes->overButton2 = true;
				else
					emes->overButton2 = false;

				if (emes->clicked1 || emes->clicked2)
				{

					int yRes = GetSystemMetrics(SM_CYSCREEN);
					POINT pt1 = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
					ClientToScreen(hwnd, &pt1);
					if (pt1.y > yRes-2)
					{
						SetCursorPos(pt1.x, 1);
						lastPosY -= yRes;
						break;
					}
					if (pt1.y < 1)
					{
						SetCursorPos(pt1.x, yRes-2);
						lastPosY += yRes;
						break;
					}

					if (emes->allowFloat)
					{
						emes->floatValue = lastFloatValue + (lastPosY - (short)HIWORD(lParam))*emes->floatMouseChangeValue;
					}
					else
					{
						emes->intValue = lastIntValue + (int)((lastPosY - (short)HIWORD(lParam))*emes->intMouseChangeValue);
					}
					emes->updateChangedValue();
					emes->notifyScrolling();
				}
			}
			else
			{
				ReleaseCapture();
			}
			InvalidateRect(hwnd, &rect, FALSE);
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		RECT rect;
		GetClientRect(hwnd, &rect);
		rect.left = emes->editRectWidth;
		POINT pt = { LOWORD(lParam), HIWORD(lParam) };
		if (PtInRect(&rect, pt))
		{
			SetFocus(hwnd);
			if (PtInRect(&(emes->rUpper), pt))
			{	// clicked upper button
				emes->clicked1 = true;
				emes->overButton1 = true;
				emes->clicked2 = false;
				emes->overButton2 = false;
				emes->nowScrolling = true;
				if (emes->allowFloat)
				{
					emes->floatValue += emes->floatChangeValue;
					lastFloatValue = emes->floatValue;

				}
				else
				{
					lastIntValue = emes->intValue;
					emes->intValue += emes->intChangeValue;
				}
				lastPosY = pt.y;
				emes->updateChangedValue();
				emes->notifyScrolling();
			}
			if (PtInRect(&(emes->rLower), pt))
			{	// clicked lower button
				emes->clicked1 = false;
				emes->overButton1 = false;
				emes->clicked2 = true;
				emes->overButton2 = true;
				emes->nowScrolling = true;
				if (emes->allowFloat)
				{
					emes->floatValue -= emes->floatChangeValue;
					lastFloatValue = emes->floatValue;

				}
				else
				{
					lastIntValue = emes->intValue;
					emes->intValue -= emes->intChangeValue;
				}
				lastPosY = pt.y;
				emes->updateChangedValue();
				emes->notifyScrolling();
			}
			RECT rect;
			GetClientRect(hwnd, &rect);
			InvalidateRect(hwnd, &rect, FALSE);
		}

	}
	break;
	case WM_LBUTTONUP:
	{
		emes->clicked1 = false;
		emes->clicked2 = false;
		emes->nowScrolling = false;
		ReleaseCapture();
		RECT rect;
		GetClientRect(hwnd, &rect);
		InvalidateRect(hwnd, &rect, FALSE);
		emes->notifyScrolling();
	}
	break;
	case WM_SETFOCUS:
		{
			SetFocus(emes->hEdit);
		}
		break;
	case WM_COMMAND:
	{
		int wmId, wmEvent;
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		
		LONG editID = GetWindowLong(emes->hEdit, GWL_ID);
		if (wmId == editID  &&   wmEvent == ES_RETURN)
		{
		}
		if (wmId == editID  &&   wmEvent == ES_TAB)
		{
			HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, false);
			if (hNext != hwnd)
				SetFocus(hNext);
		}
		if (wmId == editID  &&   wmEvent == ES_TAB_SHIFT)
		{
			HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, true);
			if (hNext != hwnd)
				SetFocus(hNext);
		}
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}



void EMEditSpin::floatToEditbox(float value)
{
	char eValue[64];
	char temp[64];
	sprintf_s(temp, 64, "%%.%df", floatAfterDot);
	sprintf_s(eValue, 64, temp, value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}

void EMEditSpin::intToEditbox(int value)
{
	char eValue[64];
	sprintf_s(eValue, 64, "%d", value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}


void EMEditSpin::setValuesToInt(int value, int minVal, int maxVal, int changeValue, float mouseChangeValue)
{
	minIntValue = minVal;
	maxIntValue = maxVal;
	if (value > maxIntValue)
		value = maxIntValue;
	if (value < minIntValue)
		value = minIntValue;
	intValue = value;

	allowFloat = false;
	intToEditbox(value);
	intChangeValue = changeValue;
	intMouseChangeValue = mouseChangeValue;
}

void EMEditSpin::setValuesToFloat(float value, float minVal, float maxVal, float changeValue, float mouseChangeValue)
{
	minFloatValue = minVal;
	maxFloatValue = maxVal;
	if (value > maxFloatValue)
		value = maxFloatValue;
	if (value < minFloatValue)
		value = minFloatValue;
	floatValue = value;

	allowFloat = true;
	floatToEditbox(value);
	floatChangeValue = changeValue;
	floatMouseChangeValue = mouseChangeValue;
}

void EMEditSpin::updateChangedValue()
{
	if (allowFloat)
	{
		if (floatValue > maxFloatValue)
			floatValue = maxFloatValue;
		if (floatValue < minFloatValue)
			floatValue = minFloatValue;
		floatToEditbox(floatValue);
	}
	else
	{
		if (intValue > maxIntValue)
			intValue = maxIntValue;
		if (intValue < minIntValue)
			intValue = minIntValue;
		intToEditbox(intValue);
	}
}

void EMEditSpin::notifyScrolling()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_VSCROLL), (LPARAM)hwnd);
}

