#ifndef __max__2013__predef__
#define __max__2013__predef__

#include <tchar.h>

char * getAnsiCopy1(const TCHAR * src, int * rlen=NULL);
char * copyStringMax1(const char * src, unsigned int addSpace=0);
TCHAR * copyAnsiToTchar1(char * src);


#endif
