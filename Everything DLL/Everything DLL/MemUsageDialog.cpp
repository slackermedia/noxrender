#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

int evalFontHeight(HWND hWnd);
int getControlLines(HWND hWnd);
int getTextLines(HWND hWnd);
bool analyzeSceneAndFillText(HWND hTxt);

//---------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK MemUsageDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBRUSH editBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			if (!editBrush)
				editBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			
			EMButton * embclose = GetEMButtonInstance(GetDlgItem(hWnd, IDC_MEM_CLOSE));
			GlobalWindowSettings::colorSchemes.apply(embclose);
			EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_MEM_SCROLL));
			GlobalWindowSettings::colorSchemes.apply(emsb);

			HWND hText = GetDlgItem(hWnd, IDC_MEM_TEXT);
			analyzeSceneAndFillText(hText);

			int wl = getTextLines(hText);
			emsb->size_screen = getControlLines(hText);
			emsb->size_work = wl;
			emsb->workarea_pos = 0;
			emsb->updateSize();
			emsb->updateRegions();
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_MEM_CLOSE:
					{
						EndDialog(hWnd, (INT_PTR)NULL);
					}
					break;
				case IDC_MEM_SCROLL:
				{
					HWND hLic = GetDlgItem(hWnd, IDC_MEM_TEXT);
					EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_MEM_SCROLL));
					if (!emsb)
						break;
					if (!hLic)
						break;
					if (wmEvent != SB_POS_CHANGED)
						break;

					int first = (int)SendMessage(hLic, EM_GETFIRSTVISIBLELINE, 0, 0);
					SendMessage(hLic, EM_LINESCROLL, 0, emsb->workarea_pos-first);
					InvalidateRect(hLic, NULL, true);
				}
				break;
				case IDC_MEM_TEXT:
				{
					if (wmEvent == EN_VSCROLL)
					{
						HWND hText = GetDlgItem(hWnd, IDC_MEM_TEXT);
						EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hWnd, IDC_MEM_SCROLL));
						int first = (int)SendMessage(hText, EM_GETFIRSTVISIBLELINE, 0, 0);
						emsb->workarea_pos = first;
						emsb->updateSize();
						emsb->updateRegions();
						InvalidateRect(emsb->hwnd, NULL, false);
					}
				}
				break;
			}
		}
		break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(200,200,200));
				SetBkColor(hdc1, GlobalWindowSettings::colorSchemes.DialogBackground);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

bool addTextureInfo(Scene * sc, Texture * tex, unsigned long long &total_size, char * chname, int txtsize, int &left, char * txtbuf, char * &txtptr, int num_tabs)
{
	total_size = 0;
	CHECK(sc);
	CHECK(tex);
	CHECK(txtbuf);

	int w = tex->bmp.getWidth();
	int h = tex->bmp.getHeight();

	
	num_tabs = min(10, max(0, num_tabs));
	char tabs_str[64];
	tabs_str[0] = 0;
	for (int i=0; i<num_tabs; i++)
	{
		int l = (int)strlen(tabs_str);
		sprintf_s(tabs_str+l, 64-l, "	");
	}

	if (tex->bmp.idata)
	{
		unsigned long long i_size = w * h * 4;
		sprintf_s(txtptr, left, "%s%s texture 32-bit buffer (%d x %d) size: %.2f MB\r\n", tabs_str, chname, w, h, ((float)i_size/1024.0f/1024.0f) );
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
		total_size += i_size;
	}

	if (tex->bmp.fdata)
	{
		unsigned long long f_size = w * h * sizeof(Color4);
		sprintf_s(txtptr, left, "%s%s texture HDR buffer (%d x %d) size: %.2f MB\r\n", tabs_str, chname, w, h, ((float)f_size/1024.0f/1024.0f) );
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
		total_size += f_size;
	}

	return true;
}

bool analyzeSceneAndFillText(HWND hTxt)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);
	int txtsize = 512000;
	int left;
	char * txtbuf = (char*)malloc(txtsize);
	CHECK(txtbuf);
	char * txtptr = txtbuf;

	// ----- CAMERAS --------

	unsigned long long whole_scene_size = 0;
	unsigned long long cameras_size = 0;

	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
	sprintf_s(txtptr, left, "Cameras found: %d.\r\n", sc->cameras.objCount);
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	for (int i=0; i<sc->cameras.objCount; i++)
	{
		Camera * cam = sc->cameras[i];
		unsigned long long cam_size = 0;
		if (!cam)
		{
			sprintf_s(txtptr, left, "	Camera %d is NULL\r\n", (i+1));
			txtptr = strlen(txtbuf)+txtbuf;
			left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			continue;
		}

		sprintf_s(txtptr, left, "	Camera %d : %s\r\n", (i+1), cam->name);
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

		sprintf_s(txtptr, left, "		Res: %d x %d   AA x%d\r\n", cam->width, cam->height, cam->aa);
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

		for (int j=0; j<16; j++)
		{
			unsigned long long direct_size = 0;
			unsigned long long gi_size = 0;
			if (cam->blendBuffDirect[j].imgBuf)
			{
				direct_size = (sizeof(Color4)+sizeof(int)) * cam->blendBuffDirect[j].width * cam->blendBuffDirect[j].height;
				sprintf_s(txtptr, left, "		Blend layer %d direct: (%d x %d): %.2f MB\r\n", 
								(j+1), cam->blendBuffDirect[j].width, cam->blendBuffDirect[j].height, ((float)direct_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}

			if (cam->blendBuffGI[j].imgBuf)
			{
				gi_size = (sizeof(Color4)+sizeof(int)) * cam->blendBuffGI[j].width * cam->blendBuffGI[j].height;
				sprintf_s(txtptr, left, "		Blend layer %d GI: (%d x %d): %.2f MB\r\n", 
								(j+1), cam->blendBuffGI[j].width, cam->blendBuffGI[j].height, ((float)gi_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}

			cam_size += direct_size;
			cam_size += gi_size;
		}

		if (cam->imgBuff)
		{
			unsigned long long img_size = 0;
			if (cam->imgBuff->imgBuf)
			{
				img_size = (sizeof(Color4)+sizeof(int)) * cam->imgBuff->width * cam->imgBuff->height;
				sprintf_s(txtptr, left, "		Accumulation buffer: (%d x %d): %.2f MB\r\n", 
								cam->imgBuff->width, cam->imgBuff->height, ((float)img_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}
			cam_size += img_size;
		}

		if (cam->depthBuf)
		{
			unsigned long long depth_size = 0;
			if (cam->depthBuf->fbuf)
			{
				depth_size = (sizeof(float)+sizeof(int)) * cam->depthBuf->width * cam->depthBuf->height;
				sprintf_s(txtptr, left, "		Depth buffer: (%d x %d): %.2f MB\r\n", 
								cam->depthBuf->width, cam->depthBuf->height, ((float)depth_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}
			cam_size += depth_size;
		}

		Texture * tex = &(cam->texDiaphragm);
		char chname[64];
		sprintf_s(chname, 64, "Camera diaphragm");
		unsigned long long diaph_size = 0;
		addTextureInfo(sc, tex, diaph_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += diaph_size;

		tex = &(cam->texObstacle);
		sprintf_s(chname, 64, "Camera obstacle");
		unsigned long long obst_size = 0;
		addTextureInfo(sc, tex, obst_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += obst_size;

		tex = &(cam->texDiapObst);
		sprintf_s(chname, 64, "Camera diaphragm and obstacle");
		unsigned long long diaphobst_size = 0;
		addTextureInfo(sc, tex, diaphobst_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += diaphobst_size;

		tex = &(cam->texFourier);
		sprintf_s(chname, 64, "Camera glare");
		unsigned long long glare_size = 0;
		addTextureInfo(sc, tex, glare_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += glare_size;

		sprintf_s(txtptr, left, "		Total camera size : %.2f MB\r\n", ((float)cam_size/1024.0f/1024.0f));
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

		cameras_size += cam_size;
	}

	sprintf_s(txtptr, left, "	All cameras total size : %.2f MB\r\n\r\n", ((float)cameras_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	// ----- GEOMETRY --------

	unsigned int num_tris = sc->triangles.objCount;
	unsigned long long geom_size = num_tris * sizeof(Triangle);

	sprintf_s(txtptr, left, "Triangles: %d   Size : %.2f MB\r\n", num_tris, ((float)geom_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	unsigned long long kdtree_size = sc->sscene.bsp.kdtree_buf_size;
	sprintf_s(txtptr, left, "kd-tree size : %.2f MB\r\n\r\n", ((float)kdtree_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	// ----- TEXTURE MANAGER ------
	sprintf_s(txtptr, left, "Textures. Found : %d\r\n", sc->texManager.textures.objCount);
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	unsigned long long textures_size = 0;
	for (int i=0; i<sc->texManager.textures.objCount; i++)
	{
		if (!sc->texManager.textures[i])
			continue;
		if (!sc->texManager.textures[i]->isValid())
			continue;

		Texture * tex = sc->texManager.textures[i];
		char chname[512];
		sprintf_s(chname, 64, "Texture no. %d - %s - ", (i+1), tex->filename);
		unsigned long long tex_size = 0;
		addTextureInfo(sc, tex, tex_size, chname, txtsize, left, txtbuf, txtptr, 1);
		textures_size += tex_size;
	}

	sprintf_s(txtptr, left, "	All textures size: %.2f MB\r\n\r\n", ((float)textures_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);


	// ----- SUMMARY ------

	unsigned long long total_scene_buffers = 0;
	total_scene_buffers += cameras_size;
	total_scene_buffers += geom_size;
	total_scene_buffers += kdtree_size;
	total_scene_buffers += textures_size;

	sprintf_s(txtptr, left, "Total scene size: %.2f MB\r\n", ((float)total_scene_buffers/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	int ok = SetWindowText(hTxt, txtbuf);

	free(txtbuf);
	if (!ok)
		return false;

	return true;
}

