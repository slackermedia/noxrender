#include "raytracer.h"

//---------------------------------------------------------------------

EnvSphere::EnvSphere()
{
	dirU = Vector3d(0,1,0);
	dirN = Vector3d(0,0,-1);
	dirE = Vector3d(1,0,0);
	enabled = false;
	blendlayer = 3;
	powerEV = 0.0f;
	azimuth_shift = 0.0f;
}

//---------------------------------------------------------------------

EnvSphere::~EnvSphere()
{
}

//---------------------------------------------------------------------

void EnvSphere::updateAzimuthDirections()
{
	dirN.normalize();
	dirU = dirE ^ dirN;
	dirU.normalize();
	dirE = dirN ^ dirU;
	dirE.normalize();
	float cos_a = cos(azimuth_shift*PI/180.0f);
	float sin_a = sin(azimuth_shift*PI/180.0f);
	rDirN = dirN * cos_a +  dirE * sin_a;
	rDirE = dirN * -sin_a +  dirE * cos_a;
	rDirN.normalize();
	rDirE.normalize();
}

//---------------------------------------------------------------------

void EnvSphere::evalAngles(Vector3d dir, float &phi, float &theta, float &x, float &y)
{
	float costheta = min(1.0f, max(-1.0f, dirU * dir));
	theta = acos(costheta);

	float cosN = min(1.0f, max(-1.0f, rDirN * dir));
	float cosE = min(1.0f, max(-1.0f, rDirE * dir));
	phi = atan2(cosE, cosN);

	x = min(1.0f, max(0.0f, (phi+PI)/(2*PI)));
	y = 1.0f - min(1.0f, max(0.0f, theta/PI));
}

//---------------------------------------------------------------------

Color4 EnvSphere::getColor(Vector3d dir)
{
	if (!tex.isValid())
		return Color4(0,0,0);
	
	float phi, theta, x, y;
	evalAngles(dir, phi, theta, x, y);

	return tex.getColor(x,y) * 10000.0f;
}

//---------------------------------------------------------------------

float EnvSphere::getProbability(Vector3d dir)
{
	return getProbabilityUniform(dir);
}

//---------------------------------------------------------------------

Vector3d EnvSphere::randomDir(float &pdf)
{
	return randomDirUniform(pdf);
}

//---------------------------------------------------------------------

float EnvSphere::getProbabilityUniform(Vector3d dir)
{
	return 1;
}

//---------------------------------------------------------------------

Vector3d EnvSphere::randomDirUniform(float &pdf)
{
	return Vector3d(1,0,0);
}

//---------------------------------------------------------------------

