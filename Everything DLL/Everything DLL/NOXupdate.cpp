#define _CRT_RAND_S
#include <windows.h>
#include "resource.h"
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include "log.h"

#define FREAD_UCHAR(a)			fread((&a), 1, 1, file)

extern char  * defaultDirectory;
char * getTempDirectory();
char * randTempFilename(char * prefix);


bool isVersionHigher(int vLocalMajor, int vLocalMinor, int vLocalBuild, int vServerMajor, int vServerMinor, int vServerBuild)
{
	if (vLocalMajor < vServerMajor)
		return true;
	if (vLocalMajor > vServerMajor)
		return false;

	if (vLocalMinor < vServerMinor)
		return true;
	if (vLocalMinor > vServerMinor)
		return false;

	if (vLocalBuild < vServerBuild)
		return true;
	if (vLocalBuild > vServerBuild)
		return false;

	return false; // the same
}

size_t write_data (void *ptr, size_t size, size_t nmemb, void *stream) 
{ 
	return fwrite(ptr, size, nmemb, (FILE*)stream);
}

bool updateDownloadFile(char * address, char * filename)
{
	CHECK(address);
	CHECK(filename);

	CURL *curl;
	CURLcode res;


	FILE * file;
	if (fopen_s(&file, filename, "wb"))
		return false;
	CHECK(file);

	curl = curl_easy_init(); 

	curl_easy_setopt(curl, CURLOPT_POST, true); 

	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
	curl_easy_setopt(curl, CURLOPT_URL, address); 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); 

	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1); 
	curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, 0);

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
	
	res = curl_easy_perform(curl); 
	
	long http_code = 0;
	curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

	curl_easy_cleanup(curl); 
	fclose(file);

	CHECK(res==0);
	if (http_code>=400)
		return false;

	return true;
}


bool checkForNOXUpdate()
{
	char * tempFilename = randTempFilename("nox");
	if (!tempFilename)
		return false;

	char * address = (char*)malloc(512);
	if (!address)
		return false;

	bool alpha = checkBetaTesterStatusFromRegistry();

	if (alpha)
		sprintf_s(address, 512, "http://www.evermotion.org/files/noxRenderer/update/alpha.ver");
	else
		sprintf_s(address, 512, "http://www.evermotion.org/files/noxRenderer/update/nox.ver");

	bool dlOK = updateDownloadFile(address, tempFilename);


	FILE * file;
	if (fopen_s(&file, tempFilename, "rb")  ||  !file)
		return false;

	_fseeki64(file, 0, SEEK_END);
	long long int filesize = _ftelli64(file);
	_fseeki64(file, 0, SEEK_SET);
	
	if (filesize != 3)
	{
		fclose(file);
		if (fileExists(tempFilename))
			DeleteFile(tempFilename);
		return false;
	}

	unsigned char v1, v2, v3;
	bool fOK = true;
	if (!FREAD_UCHAR(v1))
		fOK = false;
	if (!FREAD_UCHAR(v2))
		fOK = false;
	if (!FREAD_UCHAR(v3))
		fOK = false;

	fclose(file);
	if (fileExists(tempFilename))
		DeleteFile(tempFilename);

	if (!fOK)
		return false;

	int sMajor = v1;
	int sMinor = v2;
	int sBuild = v3;
	int lMajor = NOX_VER_MAJOR;
	int lMinor = NOX_VER_MINOR;
	int lBuild = NOX_VER_BUILD;

	bool higher = isVersionHigher(lMajor, lMinor, lBuild, sMajor, sMinor, sBuild);

	return higher;
}

DWORD WINAPI checkUpdateThreadProc(LPVOID lpParameter)
{
	bool updOK = checkForNOXUpdate();

	if (updOK)
	{
		int resp = MessageBox(0, "New version of NOX found.\nDo you want to run updater?", "NOX Update", MB_YESNO);
		if (resp == IDYES)
		{
			runUpdater(false);
		}
	}

	return 0;
}

bool checkForNOXUpdateAsThread()
{
	bool autoupdate = checkAutoupdateStatusFromRegistry();
	if (!autoupdate)
		return false;

	DWORD threadId;
	HANDLE threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(checkUpdateThreadProc), (LPVOID)0, 0, &threadId);
	if (!threadHandle)
		return false;
	return true;
}

bool runUpdater(bool silentMode)
{
	bool autoupdate = checkAutoupdateStatusFromRegistry();
	if (!autoupdate)
		return false;

	if (!defaultDirectory)
		return false;
	if (!dirExists(defaultDirectory))
		return false;

	char * tempdirectory = getTempDirectory();
	if (!tempdirectory  ||  !dirExists(tempdirectory))
		return false;

	char * tDir = (char *)malloc(2048);
	sprintf_s(tDir, 2048, "%s\\noxUpdate", tempdirectory);
	free(tempdirectory);
	if (!tDir)
		return false;

	if (!dirExists(tDir))
		CreateDirectory(tDir, NULL);
	if (!dirExists(tDir))
		return false;

	char updSrcFilename[2048];
	char libSrcFilename[2048];
	char updDstFilename[2048];
	char libDstFilename[2048];

	sprintf_s(updSrcFilename, 2048, "%s\\bin\\32\\NOXUpdate.exe", defaultDirectory);
	sprintf_s(libSrcFilename, 2048, "%s\\dll\\32\\libcurl.dll", defaultDirectory);

	sprintf_s(updDstFilename, 2048, "%s\\NOXUpdate.exe", tDir);
	sprintf_s(libDstFilename, 2048, "%s\\libcurl.dll", tDir);

	if (!CopyFile(updSrcFilename, updDstFilename, FALSE))
		return false;
	if (!CopyFile(libSrcFilename, libDstFilename, FALSE))
		return false;


	char args[2048];
	if (silentMode)
		sprintf_s(args, 2048, "%s -silent", updDstFilename);
	else
		sprintf_s(args, 2048, "%s", updDstFilename);


	OSVERSIONINFO ovi;
	GetVersionEx(&ovi);
	bool ok1 = false;
	if (ovi.dwMajorVersion > 5)
	{
		int rr = (int)(long long)ShellExecute(0, "runas", args, 0, 0, SW_SHOWNORMAL);
		ok1 = (rr>32);
	}
	else
	{
		PROCESS_INFORMATION pi;
		STARTUPINFO si = {	sizeof(STARTUPINFO),	NULL,	"",	NULL, 0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0 };
		ok1 = (CreateProcess(NULL, args, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS , NULL, NULL, &si, &pi) == TRUE);
	}

	if (!ok1)
		return false;

	return true;
}

