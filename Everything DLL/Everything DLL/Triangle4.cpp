#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "raytracer.h"
#include <emmintrin.h>

inline void choose(const Float4 &mask, const Float4 &a, const Float4 &b, Float4 &result)
{	// if mask return a else return b
	#ifdef OWN_SSE
	Float4 am, bnm;
	am.sse = _mm_and_ps(a.sse, mask.sse);
	bnm.sse = _mm_andnot_ps(mask.sse, b.sse);
	result.sse = _mm_or_ps(am.sse, bnm.sse);
	#endif
}
void cross(const float & v1x, const float & v1y, const float & v1z, 
		   const float & v2x, const float & v2y, const float & v2z, 
		   float & rx, float & ry, float & rz)
{
	rx = v1y*v2z - v1z*v2y;
	ry = v1z*v2x - v1x*v2z;
	rz = v1x*v2y - v1y*v2x;
}

//=====================================================================================================

Float4::Float4()
{
	#ifdef OWN_SSE
		sse = _mm_setzero_ps();
	#else
		v1 = v2 = v3 = v4 = 0.0f;
	#endif
}

Float4::Float4(const Float4 &r)
{
	#ifdef OWN_SSE
		sse = r.sse;
	#else
		v1 = r.v1;
		v2 = r.v2;
		v3 = r.v3;
		v4 = r.v4;
	#endif
}

Float4& Float4::operator=(const Float4 &f)
{
	#ifdef OWN_SSE
	sse = f.sse;
	#else
		v1 = f.v1;
		v2 = f.v2;
		v3 = f.v3;
		v4 = f.v4;
	#endif
	return *this;
}

//=====================================================================================================

Ray4::Ray4()
{
	#ifdef OWN_SSE
		ox.sse = _mm_setzero_ps();
		oy.sse = _mm_setzero_ps();
		oz.sse = _mm_setzero_ps();
		dx.sse = _mm_setzero_ps();
		dy.sse = _mm_setzero_ps();
		dz.sse = _mm_setzero_ps();
	#else
		ox.v1 = ox.v2 = ox.v3 = ox.v4 = 0.0f;
		oy.v1 = oy.v2 = oy.v3 = oy.v4 = 0.0f;
		oz.v1 = oz.v2 = oz.v3 = oz.v4 = 0.0f;
		dx.v1 = dx.v2 = dx.v3 = dx.v4 = 0.0f;
		dy.v1 = dy.v2 = dy.v3 = dy.v4 = 0.0f;
		dz.v1 = dz.v2 = dz.v3 = dz.v4 = 0.0f;
	#endif
}

Ray4::Ray4(const Ray4 &ray4)
{
	#ifdef OWN_SSE
		ox.sse = ray4.ox.sse;
		oy.sse = ray4.oy.sse;
		oz.sse = ray4.oz.sse;
		dx.sse = ray4.dx.sse;
		dy.sse = ray4.dy.sse;
		dz.sse = ray4.dz.sse;
	#else
		ox.v1 = ray4.ox.v1;   ox.v2 = ray4.ox.v2;   ox.v3 = ray4.ox.v3;   ox.v4 = ray4.ox.v4;
		oy.v1 = ray4.oy.v1;   oy.v2 = ray4.oy.v2;   oy.v3 = ray4.oy.v3;   oy.v4 = ray4.oy.v4;
		oz.v1 = ray4.oz.v1;   oz.v2 = ray4.oz.v2;   oz.v3 = ray4.oz.v3;   oz.v4 = ray4.oz.v4;
		dx.v1 = ray4.dx.v1;   dx.v2 = ray4.dx.v2;   dx.v3 = ray4.dx.v3;   dx.v4 = ray4.dx.v4;
		dy.v1 = ray4.dy.v1;   dy.v2 = ray4.dy.v2;   dy.v3 = ray4.dy.v3;   dy.v4 = ray4.dy.v4;
		dz.v1 = ray4.dz.v1;   dz.v2 = ray4.dz.v2;   dz.v3 = ray4.dz.v3;   dz.v4 = ray4.dz.v4;
	#endif
}

Ray4::Ray4(const float &Ox, const float &Oy, const float &Oz, const float &Dx, const float &Dy, const float &Dz)
{
	#ifdef OWN_SSE
		ox.sse = _mm_set1_ps(Ox);
		oy.sse = _mm_set1_ps(Oy);
		oz.sse = _mm_set1_ps(Oz);
		dx.sse = _mm_set1_ps(Dx);
		dy.sse = _mm_set1_ps(Dy);
		dz.sse = _mm_set1_ps(Dz);
	#else
		ox.v1 = ox.v2 = ox.v3 = ox.v4 = Ox;
		oy.v1 = oy.v2 = oy.v3 = oy.v4 = Oy;
		oz.v1 = oz.v2 = oz.v3 = oz.v4 = Oz;
		dx.v1 = dx.v2 = dx.v3 = dx.v4 = Dx;
		dy.v1 = dy.v2 = dy.v3 = dy.v4 = Dy;
		dz.v1 = dz.v2 = dz.v3 = dz.v4 = Dz;
	#endif
}

Ray4::Ray4(const Float4 &or, const Float4 &dr)
{
	#ifdef OWN_SSE

		ox.sse = _mm_unpacklo_ps(or.sse, or.sse);
		ox.sse = _mm_unpacklo_ps(ox.sse, ox.sse);
		oy.sse = _mm_unpacklo_ps(or.sse, or.sse);
		oy.sse = _mm_unpackhi_ps(oy.sse, oy.sse);
		oz.sse = _mm_unpackhi_ps(or.sse, or.sse);
		oz.sse = _mm_unpacklo_ps(oz.sse, oz.sse);
		dx.sse = _mm_unpacklo_ps(dr.sse, dr.sse);
		dx.sse = _mm_unpacklo_ps(dx.sse, dx.sse);
		dy.sse = _mm_unpacklo_ps(dr.sse, dr.sse);
		dy.sse = _mm_unpackhi_ps(dy.sse, dy.sse);
		dz.sse = _mm_unpackhi_ps(dr.sse, dr.sse);
		dz.sse = _mm_unpacklo_ps(dz.sse, dz.sse);
	#else
		ox.v1 = ox.v2 = ox.v3 = ox.v4 = or.v1;
		oy.v1 = oy.v2 = oy.v3 = oy.v4 = or.v2;
		oz.v1 = oz.v2 = oz.v3 = oz.v4 = or.v3;
		dx.v1 = dx.v2 = dx.v3 = dx.v4 = dr.v1;
		dy.v1 = dy.v2 = dy.v3 = dy.v4 = dr.v2;
		dz.v1 = dz.v2 = dz.v3 = dz.v4 = dr.v3;
	#endif
}

Ray4& Ray4::operator=(const Ray4 &ray4)
{
	#ifdef OWN_SSE
		ox.sse = ray4.ox.sse;
		oy.sse = ray4.oy.sse;
		oz.sse = ray4.oz.sse;
		dx.sse = ray4.dx.sse;
		dy.sse = ray4.dy.sse;
		dz.sse = ray4.dz.sse;
	#else
		ox.v1 = ray4.ox.v1;   ox.v2 = ray4.ox.v2;   ox.v3 = ray4.ox.v3;   ox.v4 = ray4.ox.v4;
		oy.v1 = ray4.oy.v1;   oy.v2 = ray4.oy.v2;   oy.v3 = ray4.oy.v3;   oy.v4 = ray4.oy.v4;
		oz.v1 = ray4.oz.v1;   oz.v2 = ray4.oz.v2;   oz.v3 = ray4.oz.v3;   oz.v4 = ray4.oz.v4;
		dx.v1 = ray4.dx.v1;   dx.v2 = ray4.dx.v2;   dx.v3 = ray4.dx.v3;   dx.v4 = ray4.dx.v4;
		dy.v1 = ray4.dy.v1;   dy.v2 = ray4.dy.v2;   dy.v3 = ray4.dy.v3;   dy.v4 = ray4.dy.v4;
		dz.v1 = ray4.dz.v1;   dz.v2 = ray4.dz.v2;   dz.v3 = ray4.dz.v3;   dz.v4 = ray4.dz.v4;
	#endif
	return *this;
}


//=====================================================================================================

Triangle4::Triangle4()
{
	#ifdef OWN_SSE
		v0x.sse = _mm_setzero_ps();
		v0y.sse = _mm_setzero_ps();
		v0z.sse = _mm_setzero_ps();
		e1x.sse = _mm_setzero_ps();
		e1y.sse = _mm_setzero_ps();
		e1z.sse = _mm_setzero_ps();
		e2x.sse = _mm_setzero_ps();
		e2y.sse = _mm_setzero_ps();
		e2z.sse = _mm_setzero_ps();

		int ii = -1;
		numbers.sse = _mm_set1_ps(*(float*)(&ii));
	#else
		v0x.v1 = v0x.v2 = v0x.v3 = v0x.v4 = 0;
		v0y.v1 = v0y.v2 = v0y.v3 = v0y.v4 = 0;
		v0z.v1 = v0z.v2 = v0z.v3 = v0z.v4 = 0;
		e1x.v1 = e1x.v2 = e1x.v3 = e1x.v4 = 0;
		e1y.v1 = e1y.v2 = e1y.v3 = e1y.v4 = 0;
		e1z.v1 = e1z.v2 = e1z.v3 = e1z.v4 = 0;
		e2x.v1 = e2x.v2 = e2x.v3 = e2x.v4 = 0;
		e2y.v1 = e2y.v2 = e2y.v3 = e2y.v4 = 0;
		e2z.v1 = e2z.v2 = e2z.v3 = e2z.v4 = 0;

		numbers.s_int[0] = numbers.s_int[1] = numbers.s_int[2] = numbers.s_int[3] = -1;
	#endif
}

Triangle4::Triangle4(const Triangle4 & t)
{
	#ifdef OWN_SSE
		v0x.sse = t.v0x.sse;
		v0y.sse = t.v0y.sse;
		v0z.sse = t.v0z.sse;
		e1x.sse = t.e1x.sse;
		e1y.sse = t.e1y.sse;
		e1z.sse = t.e1z.sse;
		e2x.sse = t.e2x.sse;
		e2y.sse = t.e2y.sse;
		e2z.sse = t.e2z.sse;
		numbers.sse = t.numbers.sse;

	#else
		v0x.v1 = t.v0x.v1;   v0x.v2 = t.v0x.v2;   v0x.v3 = t.v0x.v3;   v0x.v4 = t.v0x.v4;
		v0y.v1 = t.v0y.v1;   v0y.v2 = t.v0y.v2;   v0y.v3 = t.v0y.v3;   v0y.v4 = t.v0y.v4;
		v0z.v1 = t.v0z.v1;   v0z.v2 = t.v0z.v2;   v0z.v3 = t.v0z.v3;   v0z.v4 = t.v0z.v4;
		e1x.v1 = t.e1x.v1;   e1x.v2 = t.e1x.v2;   e1x.v3 = t.e1x.v3;   e1x.v4 = t.e1x.v4;
		e1y.v1 = t.e1y.v1;   e1y.v2 = t.e1y.v2;   e1y.v3 = t.e1y.v3;   e1y.v4 = t.e1y.v4;
		e1z.v1 = t.e1z.v1;   e1z.v2 = t.e1z.v2;   e1z.v3 = t.e1z.v3;   e1z.v4 = t.e1z.v4;
		e2x.v1 = t.e2x.v1;   e2x.v2 = t.e2x.v2;   e2x.v3 = t.e2x.v3;   e2x.v4 = t.e2x.v4;
		e2y.v1 = t.e2y.v1;   e2y.v2 = t.e2y.v2;   e2y.v3 = t.e2y.v3;   e2y.v4 = t.e2y.v4;
		e2z.v1 = t.e2z.v1;   e2z.v2 = t.e2z.v2;   e2z.v3 = t.e2z.v3;   e2z.v4 = t.e2z.v4;
		numbers.s_int[0] = t.numbers.s_int[0];
		numbers.s_int[1] = t.numbers.s_int[1];
		numbers.s_int[2] = t.numbers.s_int[2];
		numbers.s_int[3] = t.numbers.s_int[3];
	#endif
}

Triangle4::Triangle4(const Triangle &t1, const Triangle &t2, const Triangle &t3, const Triangle &t4, int &n1, int &n2, int &n3, int &n4)
{
	#ifdef OWN_SSE
		v0x.sse = _mm_set_ps(t1.V1.x,  t2.V1.x,  t3.V1.x,  t4.V1.x);
		v0y.sse = _mm_set_ps(t1.V1.y,  t2.V1.y,  t3.V1.y,  t4.V1.y);
		v0z.sse = _mm_set_ps(t1.V1.z,  t2.V1.z,  t3.V1.z,  t4.V1.z);
		e1x.sse = _mm_set_ps(t1.V2.x,  t2.V2.x,  t3.V2.x,  t4.V2.x);
		e1y.sse = _mm_set_ps(t1.V2.y,  t2.V2.y,  t3.V2.y,  t4.V2.y);
		e1z.sse = _mm_set_ps(t1.V2.z,  t2.V2.z,  t3.V2.z,  t4.V2.z);
		e2x.sse = _mm_set_ps(t1.V3.x,  t2.V3.x,  t3.V3.x,  t4.V3.x);
		e2y.sse = _mm_set_ps(t1.V3.y,  t2.V3.y,  t3.V3.y,  t4.V3.y);
		e2z.sse = _mm_set_ps(t1.V3.z,  t2.V3.z,  t3.V3.z,  t4.V3.z);
		e1x.sse = _mm_sub_ps(e1x.sse, v0x.sse);
		e1y.sse = _mm_sub_ps(e1y.sse, v0y.sse);
		e1z.sse = _mm_sub_ps(e1z.sse, v0z.sse);
		e2x.sse = _mm_sub_ps(e2x.sse, v0x.sse);
		e2y.sse = _mm_sub_ps(e2y.sse, v0y.sse);
		e2z.sse = _mm_sub_ps(e2z.sse, v0z.sse);
		numbers.sse = _mm_set_ps(*(float *)(&n1), *(float *)(&n2), *(float *)(&n3), *(float *)(&n4));
	#else
		v0x.v1 = t1.V1.x;   v0x.v2 = t2.V1.x;   v0x.v3 = t3.V1.x;   v0x.v4 = t4.V1.x;
		v0y.v1 = t1.V1.y;   v0y.v2 = t2.V1.y;   v0y.v3 = t3.V1.y;   v0y.v4 = t4.V1.y;
		v0z.v1 = t1.V1.z;   v0z.v2 = t2.V1.z;   v0z.v3 = t3.V1.z;   v0z.v4 = t4.V1.z;
		numbers.s_int[0] = n1;
		numbers.s_int[1] = n2;
		numbers.s_int[2] = n3;
		numbers.s_int[3] = n4;
	#endif
}


Triangle4& Triangle4::operator=(const Triangle4 &t)
{
	#ifdef OWN_SSE
		v0x.sse = t.v0x.sse;
		v0y.sse = t.v0y.sse;
		v0z.sse = t.v0z.sse;
		e1x.sse = t.e1x.sse;
		e1y.sse = t.e1y.sse;
		e1z.sse = t.e1z.sse;
		e2x.sse = t.e2x.sse;
		e2y.sse = t.e2y.sse;
		e2z.sse = t.e2z.sse;

		numbers.sse = t.numbers.sse;
	#else
		v0x.v1 = t.v0x.v1;   v0x.v2 = t.v0x.v2;   v0x.v3 = t.v0x.v3;   v0x.v4 = t.v0x.v4;
		v0y.v1 = t.v0y.v1;   v0y.v2 = t.v0y.v2;   v0y.v3 = t.v0y.v3;   v0y.v4 = t.v0y.v4;
		v0z.v1 = t.v0z.v1;   v0z.v2 = t.v0z.v2;   v0z.v3 = t.v0z.v3;   v0z.v4 = t.v0z.v4;
		e1x.v1 = t.e1x.v1;   e1x.v2 = t.e1x.v2;   e1x.v3 = t.e1x.v3;   e1x.v4 = t.e1x.v4;
		e1y.v1 = t.e1y.v1;   e1y.v2 = t.e1y.v2;   e1y.v3 = t.e1y.v3;   e1y.v4 = t.e1y.v4;
		e1z.v1 = t.e1z.v1;   e1z.v2 = t.e1z.v2;   e1z.v3 = t.e1z.v3;   e1z.v4 = t.e1z.v4;
		e2x.v1 = t.e2x.v1;   e2x.v2 = t.e2x.v2;   e2x.v3 = t.e2x.v3;   e2x.v4 = t.e2x.v4;
		e2y.v1 = t.e2y.v1;   e2y.v2 = t.e2y.v2;   e2y.v3 = t.e2y.v3;   e2y.v4 = t.e2y.v4;
		e2z.v1 = t.e2z.v1;   e2z.v2 = t.e2z.v2;   e2z.v3 = t.e2z.v3;   e2z.v4 = t.e2z.v4;

		numbers.s_int[0] = t.numbers.s_int[0];
		numbers.s_int[1] = t.numbers.s_int[1];
		numbers.s_int[2] = t.numbers.s_int[2];
		numbers.s_int[3] = t.numbers.s_int[3];
	#endif
	return *this;
}



void Triangle4::intersection(const Ray4 &rays, Float4 &dist, Float4 &U, Float4 &V, Float4 &maxd, Float4 &num) const
{
	#ifdef OWN_SSE
		Float4 px,py,pz;
		Float4 tx,ty,tz;
		Float4 qx,qy,qz;
		Float4 det,ddet,u,v, uv;
		Float4 zero, one, tmpOK, OK;

		// set zeros and ones vectors
		zero.sse = _mm_setzero_ps();
		one.sse  = _mm_set1_ps(1.0f);

		// P = D * E2
 		px.sse = _mm_sub_ps(_mm_mul_ps(rays.dy.sse, e2z.sse), _mm_mul_ps(rays.dz.sse, e2y.sse));
		py.sse = _mm_sub_ps(_mm_mul_ps(rays.dz.sse, e2x.sse), _mm_mul_ps(rays.dx.sse, e2z.sse));
		pz.sse = _mm_sub_ps(_mm_mul_ps(rays.dx.sse, e2y.sse), _mm_mul_ps(rays.dy.sse, e2x.sse));

		// det = P dot E1
		det.sse = _mm_add_ps(_mm_add_ps(_mm_mul_ps(e1x.sse, px.sse), _mm_mul_ps(e1y.sse, py.sse)), _mm_mul_ps(e1z.sse, pz.sse));

		// check if dot is non zero
		OK.sse  = _mm_cmpneq_ps(det.sse, zero.sse);	// det non zero
		
		ddet.sse = _mm_div_ps(one.sse, det.sse);

		// T = O - V0
		tx.sse = _mm_sub_ps(rays.ox.sse, v0x.sse);
		ty.sse = _mm_sub_ps(rays.oy.sse, v0y.sse);
		tz.sse = _mm_sub_ps(rays.oz.sse, v0z.sse);

		// U = (P dot T) / det
		u.sse = _mm_mul_ps(_mm_add_ps(_mm_add_ps(_mm_mul_ps(tx.sse, px.sse), _mm_mul_ps(ty.sse,py.sse)), _mm_mul_ps(tz.sse,pz.sse)), ddet.sse);
		
		// check if u is between (0,1)
		tmpOK.sse = _mm_cmpge_ps(u.sse, zero.sse);	// u >= 0
		OK.sse    = _mm_and_ps(OK.sse, tmpOK.sse);
		tmpOK.sse = _mm_cmple_ps(u.sse, one.sse);	// u <= 1
		OK.sse    = _mm_and_ps(OK.sse, tmpOK.sse);

		// Q = T * E1
		qx.sse = _mm_sub_ps(_mm_mul_ps(ty.sse, e1z.sse), _mm_mul_ps(tz.sse, e1y.sse));
		qy.sse = _mm_sub_ps(_mm_mul_ps(tz.sse, e1x.sse), _mm_mul_ps(tx.sse, e1z.sse));
		qz.sse = _mm_sub_ps(_mm_mul_ps(tx.sse, e1y.sse), _mm_mul_ps(ty.sse, e1x.sse));

		// V = (Q dot D) / det
		v.sse = _mm_mul_ps(_mm_add_ps(_mm_add_ps(_mm_mul_ps(rays.dx.sse, qx.sse), _mm_mul_ps(rays.dy.sse,qy.sse)), _mm_mul_ps(rays.dz.sse,qz.sse)), ddet.sse);

		// check if u,v is in triangle
		tmpOK.sse = _mm_cmpge_ps(v.sse, zero.sse);	// v >= 0
		OK.sse    = _mm_and_ps(OK.sse, tmpOK.sse);
		tmpOK.sse = _mm_cmple_ps(_mm_add_ps(u.sse,v.sse), one.sse);	// u+v <= 1
		OK.sse    = _mm_and_ps(OK.sse, tmpOK.sse);

		// dist = (Q dot E2) / det
		dist.sse  = _mm_mul_ps(_mm_add_ps(_mm_add_ps(_mm_mul_ps(e2x.sse, qx.sse), _mm_mul_ps(e2y.sse,qy.sse)), _mm_mul_ps(e2z.sse,qz.sse)), ddet.sse);
		tmpOK.sse = _mm_cmpgt_ps(dist.sse, zero.sse);
		OK.sse    = _mm_and_ps(OK.sse, tmpOK.sse);
		dist.sse  = _mm_and_ps(dist.sse, OK.sse);

		Float4 gr,ls;
		gr.sse = _mm_cmpgt_ps(dist.sse, zero.sse);	// dist positive
		ls.sse = _mm_cmple_ps(dist.sse, maxd.sse);	// dist less than max
		gr.sse = _mm_and_ps(gr.sse,ls.sse);	// dist is between 0 and max
		
		choose(gr, dist, maxd, maxd);
		choose(gr, numbers, num, num);
		choose(gr, u, U, U);
		choose(gr, v, V, V);
	#else
		float px,py,pz;
		float tx,ty,tz;
		float qx,qy,qz;
		float det,u,v;
		int i;
		Float4 OK;

		for (i=0; i<4; i++)
		{
			OK.bitmask[i] = 0x00000000;
			cross(rays.dx.V[i], rays.dy.V[i], rays.dz.V[i],   e2x.V[i], e2y.V[i], e2z.V[i],   px,py,pz);
			det = e1x.V[i]*px + e1y.V[i]*py + e1z.V[i]*pz;
			if (fabs(det) > 0.0000001f)
			{
				det = 1.0f/det;
				tx = rays.ox.V[i] - v0x.V[i];
				ty = rays.oy.V[i] - v0y.V[i];
				tz = rays.oz.V[i] - v0z.V[i];
				u = tx*px + ty*py + tz*pz;
				u *= det;
				if (u<=1 && u>=0)
				{
					cross(tx,ty,tz, e1x.V[i],e1y.V[i],e1z.V[i], qx,qy,qz);
					v = rays.dx.V[i]*qx + rays.dy.V[i]*qy + rays.dz.V[i]*qz;
					v *= det;
					if (v>=0 && u+v<=1)
					{
						dist.V[i] = (e2x.V[i]*qx + e2y.V[i]*qy + e2z.V[i]*qz) * det;
						OK.bitmask[i] = 0xFFFFFFFF;
						if (dist.V[i] > 0.0001f   &&   dist.V[i] < maxd.V[i])
						{
							U.V[i] = u;
							V.V[i] = v;
							maxd.V[i] = dist.V[i];
							num.s_int[i] = numbers.s_int[i];
						}
					}
					else
						dist.V[i] = 0;
				}
				else
					dist.V[i] = 0;
			}
			else
				dist.V[i] = 0;
		}
	#endif

}

void Triangle4::intersection2(const Ray4 &rays, Float4 &dist, Float4 &UU, Float4 &VV, Float4 &maxd, Float4 &num) const
{
	// only tests
}