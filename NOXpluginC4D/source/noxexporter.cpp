#include "c4d.h"
#include "noxexporter.h"
#include "xnoxscene.h"
#include "c4d_symbols.h"
#include "customgui_datetime.h"
#include <tchar.h>
#include "C:\Users\Marchewa\Documents\Visual Studio 2012\Projects\Everything DLL\Everything DLL\BinaryValues.h"
#include "C:\Users\Marchewa\Documents\Visual Studio 2012\Projects\Everything DLL\Everything DLL\nox_version.h"

#define FWRITE_ULONG8(a)		fwrite((&a), 8, 1, file)
#define FWRITE_UINT(a)			fwrite((&a), 4, 1, file)
#define FWRITE_FLOAT(a)			fwrite((&a), 4, 1, file)
#define FWRITE_USHORT(a) 		fwrite((&a), 2, 1, file)
#define FWRITE_UCHAR(a)			fwrite((&a), 1, 1, file)
#define FWRITE_STRING(a, size)	fwrite((a), (size), 1, file)

void enumNodes(GeListNode * node);
void enumObjects(BaseObject * obj);
int MakeObjectEditable( BaseDocument *doc, BaseObject *obj );
void doSomeShit(BaseObject * obj);
void doSomeShit2(BaseObject * obj);
void doSomeShit3(BaseObject * obj);
BaseObject * findDeformedMesh(BaseObject * obj);

TexModNox texModDefault;

Bool RegisterNOXexporter(void)
{
	String name;
	name = GeLoadString(IDS_NOX_EXPORTER);

	return RegisterSceneSaverPlugin(ID_NOX_EXP, name, 0, NOXexporter::Alloc, "Xnoxscene", "nox");
}

FILEERROR NOXexporter::Save(BaseSceneSaver* node, const Filename& name, BaseDocument* gdoc, SCENEFILTER filterflags)
{
	bShowError = true;
	bShowInfo = true;
	bShowInfoDetailed = false;
	bShowWarning = true;
	bool bRunNox = false;

	GeData data;
	Bool pOK;
	pOK = node->GetParameter(DescID(NOX_SHOW_ERROR), data, DESCFLAGS_GET_0);
	if (pOK  &&  data.GetType()==DA_LONG)
		bShowError = (data.GetBool()!=0);
	pOK = node->GetParameter(DescID(NOX_SHOW_INFO), data, DESCFLAGS_GET_0);
	if (pOK  &&  data.GetType()==DA_LONG)
		bShowInfo = (data.GetBool()!=0);
	pOK = node->GetParameter(DescID(NOX_SHOW_INFO_DETAIL), data, DESCFLAGS_GET_0);
	if (pOK  &&  data.GetType()==DA_LONG)
		bShowInfoDetailed = (data.GetBool()!=0);
	pOK = node->GetParameter(DescID(NOX_SHOW_WARNING), data, DESCFLAGS_GET_0);
	if (pOK  &&  data.GetType()==DA_LONG)
		bShowWarning = (data.GetBool()!=0);
	pOK = node->GetParameter(DescID(NOX_RUN_NOX), data, DESCFLAGS_GET_0);
	if (pOK  &&  data.GetType()==DA_LONG)
		bRunNox = (data.GetBool()!=0);


	file = NULL;
	doc = gdoc;
	sunsky = NULL;
	envsky = NULL;
	uniqueInstanceID = 0;
	uniqueMatID = 1;
	instanceIDs.Reset();

	AutoAlloc<UnitScaleData> internalScaleData; 
	internalScaleData->SetUnitScale(1.0, DOCUMENT_UNIT_M);
	scale = CalculateTranslationScale((const UnitScaleData*)doc->GetDataInstance()->GetCustomDataType(DOCUMENT_DOCUNIT,CUSTOMDATATYPE_UNITSCALE), internalScaleData);
	char buf[256];
	sprintf_s(buf, 256, "scale = %f",scale);
	showDetailedInfo(buf);

	char * filename = name.GetString().GetCStringCopy();

	if (fopen_s(&file, filename, "wb"))
	{
		showErrorReturnFalse("Error. Can't open file.");
		return FILEERROR_OPEN;
	}

	sunsky = findSunsky(doc->GetFirstObject());
	if (sunsky)
		showDetailedInfo("physical sky found");

	envsky = findSunskyEnv(doc->GetFirstObject());
	if (envsky)
		showDetailedInfo("env sky found");


	unsigned long long sceneSize = evalSizeScene();


	unsigned int nox_start_header = NOX_BIN_HEADER;
	if (1!=FWRITE_UINT(nox_start_header))
	{
		showErrorReturnFalse("Writing header error.");
		fclose(file);
		return FILEERROR_WRITE;
	}

	unsigned long long fileSize = sceneSize + 16;
	if (1!=FWRITE_ULONG8(fileSize))
	{
		showErrorReturnFalse("Writing file size error.");
		fclose(file);
		return FILEERROR_WRITE;
	}

	unsigned int noxversion;
	((char*)(&noxversion))[0] = (unsigned char)NOX_VER_MAJOR;
	((char*)(&noxversion))[1] = (unsigned char)NOX_VER_MINOR;
	((char*)(&noxversion))[2] = (unsigned char)NOX_VER_BUILD;
	((char*)(&noxversion))[3] = (unsigned char)NOX_VER_COMP_NO;
	if (1!=FWRITE_UINT(noxversion))
	{
		showErrorReturnFalse("Writing NOX version error.");
		fclose(file);
		return FILEERROR_WRITE;
	}

	if (!insertScenePart())
	{
		fclose(file);
		return FILEERROR_WRITE;
	}

	fclose(file);

	showInfo("Export complete.");
	
	if (bRunNox)
	{
		showInfo("Starting NOX...");
		#ifdef _UNICODE
			TCHAR ufname[2048];
			name.GetString().GetUcBlockNull(ufname, 2048);
			bool run_ok = runNox(ufname);
		#else
			bool run_ok = runNox(filename);
		#endif
		if (run_ok)
			showInfo("OK, rendering");
		else
			showInfo("NOX run failed.");
	}

	return FILEERROR_NONE;
}

//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::showErrorReturnFalse(char * error)
{
	if (bShowError)
		GePrint(error);
	return false;
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXexporter::showInfo(char * info)
{
	if (bShowInfo)
		GePrint(info);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXexporter::showDetailedInfo(char * info)
{
	if (bShowInfoDetailed)
		GePrint(info);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXexporter::showWarning(char * warning)
{
	if (bShowWarning)
		GePrint(warning);
}

//========== SCENE ======================================================================================================

unsigned long long NOXexporter::evalSizeScene()
{
	unsigned long long sceneSize = 0;

	evalSizeGeometry();
	evalSizeCameras();
	unsigned long long sunskySize = evalSunskySize();
	unsigned long long envSize = evalEnvmapSize();
	unsigned long long matsSize = evalMaterialsSize();

	sceneSize = 10 + geometrySize + camerasSize + matsSize + sunskySize + envSize;

	return sceneSize;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertScenePart()
{
	BaseObject * startObj = doc->GetFirstObject();

	unsigned long long sceneSize = evalSizeScene();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short tag_scene = NOX_BIN_SCENE;
	if (1!=FWRITE_USHORT(tag_scene))
		return showErrorReturnFalse("Writing scene tag error.");
	if (1!=FWRITE_ULONG8(sceneSize))
		return showErrorReturnFalse("Writing scene tag size error.");



	if (!enumObjectsAddCameras(startObj))
		return false;
	
	if (!insertGeometryPart())
		return false;

	if (!insertMaterials())
		return false;

	if (!insertSunsky())
		return false;

	if (!insertEnvmap())
		return false;

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sceneSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Scene chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sceneSize, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//========== CAMERAS ====================================================================================================

void NOXexporter::evalSizeCameras()
{
	camerasSize = 0;
	BaseObject * startObj = doc->GetFirstObject();
	enumObjectsEvalCamerasSize(startObj);
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::enumObjectsAddCameras(BaseObject * obj)
{
	while (obj)
	{
		BaseObject *child = obj->GetDown();
		if (child)
		{
			bool ok = enumObjectsAddCameras(child);
			if (!ok)
				return false;
		}

		if (obj->GetType() == Ocamera)
		{
			bool ok = insertCameraPart((CameraObject *)obj);
			if (!ok)
				return false;
		}

		obj = obj->GetNext();
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXexporter::enumObjectsEvalCamerasSize(BaseObject * obj)
{
	while (obj)
	{
		BaseObject *child = obj->GetDown();
		
		if (child)
		{
			enumObjectsEvalCamerasSize(child);
		}

		if (obj->GetType() == Ocamera)
		{
			unsigned long long camSize = evalCameraSize((CameraObject *)obj);
			camerasSize += camSize;
		}

		obj = obj->GetNext();
	}

}

//========== CAMERA =====================================================================================================

unsigned long long NOXexporter::evalCameraSize(CameraObject * obj)
{
	CameraObject * cam = obj;
	// todo: sprawdzic cam
	char * cname = cam->GetName().GetCStringCopy();
	int sname = 13;
	if (cname) 
		sname = (int)strlen(cname)+6;
	unsigned long long res = 10 + 102 + sname;

	// get DIAPH SHAPE enabled
	GeData dd;
	Bool pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_SHAPE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_LONG  &&  dd.GetBool()!=0)
		res += 18;

	numCameras++;
	return res;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertCameraPart(CameraObject * cam)
{
	if (!cam)
		return true;

	float cam_angle = 54.43f, cam_angle_vert = 54.43f;
	unsigned short cam_dof_on = 0;
	bool doffront=false, dofback=false;
	unsigned int cam_width = 800, cam_height = 600;
	float cam_focusdist = 2;
	float cam_iso = 100.0f;
	float cam_shutter = 250.0f;
	float cam_aperture = 8.0f;
	float cam_shift_x = 0.0f, cam_shift_y = 0.0f;

	bool cam_diaph_shape = false;
	unsigned short cam_diaph_blades = 5;
	float cam_diaph_angle = 0.0f;
	float cam_diaph_bias = 0.0f;
	float cam_diaph_aniso = 0.0f;

	Bool pOK;
	GeData dd;

	// get ANGLE
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_FOV), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_angle = (float)dd.GetReal() * 180.0f / 3.1415926535897932384626433832795f;
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_FOV_VERTICAL), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_angle_vert = (float)dd.GetReal() * 180.0f / 3.1415926535897932384626433832795f;
	float tan_vert = tan(cam_angle_vert * 3.1415926535897932384626433832795f / 360.0f);
	float tan_hori = tan(cam_angle * 3.1415926535897932384626433832795f / 360.0f);

	// get DOF ON
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_FRONTBLUR), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_LONG)
		doffront = (dd.GetBool()!=0);
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_REARBLUR), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_LONG)
		dofback= (dd.GetBool()!=0);
	if (doffront && dofback)
		cam_dof_on = 1;

	// get WIDTH and HEIGHT
	RenderData * rd = doc->GetActiveRenderData();
	pOK = rd->GetParameter(RDATA_XRES_VIRTUAL, dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_width = (unsigned int)dd.GetReal();
	pOK = rd->GetParameter(RDATA_YRES_VIRTUAL, dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_height = (unsigned int)dd.GetReal();

	// get FOCUS DIST
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_TARGETDISTANCE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_focusdist = (float)dd.GetReal() * scale;

	// get ISO
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_ISO_VALUE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_iso = (float)dd.GetReal();

	// get SHUTTER
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_SHUTTER_SPEED_VALUE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_shutter = 1.0f / (float)dd.GetReal();
	if (cam_shutter > 10000.0f)
		cam_shutter = 10000.0f;
	if (cam_shutter < 0.02f)
		cam_shutter = 0.02f;

	// get APERTURE
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_FNUMBER_VALUE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_aperture = (float)dd.GetReal();
	if (cam_aperture > 36.0f)
		cam_aperture = 36.0f;
	if (cam_aperture < 1.0f)
		cam_aperture = 1.0f;

	// get SHIFT
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_FILM_OFFSET_X), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_shift_x = (float)dd.GetReal();
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_FILM_OFFSET_Y), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_shift_y = (float)dd.GetReal();


	// get DIAPH SHAPE enabled
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_SHAPE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_LONG)
		cam_diaph_shape = (dd.GetBool()!=0);
	// get DIAPH BLADES
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_BLADES), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_LONG)
		cam_diaph_blades = dd.GetLong();
	if (cam_diaph_blades<5)
		cam_diaph_blades = 5;
	if (cam_diaph_blades>15)
		cam_diaph_blades = 15;
	// get DIAPH ANGLE
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_ANGLE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_diaph_angle = (float)dd.GetReal() * 180.0f / 3.1415926535897932384626433832795f;
	// get DIAPH BIAS
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_BIAS), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_diaph_bias = (float)dd.GetReal();
	// get DIAPH ANISO
	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_ANISOTROPY), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
		cam_diaph_aniso = (float)dd.GetReal();

	//tempDoShitCamera(cam);

	unsigned long long sizeCam = evalCameraSize(cam);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short tag_camera = NOX_BIN_CAMERA;
	if (1!=FWRITE_USHORT(tag_camera))
		return showErrorReturnFalse("Writing camera tag error.");

	if (1!=FWRITE_ULONG8(sizeCam))
		return showErrorReturnFalse("Writing camera tag size error.");

	
	char * cname = cam->GetName().GetCStringCopy();
	if (!cname)
		cname = "Unnamed";
	int sname = (int)strlen(cname)+6;
	unsigned short tag_cam_name = NOX_BIN_CAM_NAME;
	if (1!=FWRITE_USHORT(tag_cam_name))			// 2
		return showErrorReturnFalse("Writing camera name tag error.");
	if (1!=FWRITE_UINT(sname))					// 4
		return showErrorReturnFalse("Writing camera name size error.");
	if (1!=FWRITE_STRING(cname, sname-6))		// X
		return showErrorReturnFalse("Writing camera name error.");

	unsigned short tag_cam_width = NOX_BIN_CAM_WIDTH;
	if (1!=FWRITE_USHORT(tag_cam_width))		// 2
		return showErrorReturnFalse("Writing camera width tag error.");
	if (1!=FWRITE_UINT(cam_width))				// 4
		return showErrorReturnFalse("Writing camera width error.");
	unsigned short tag_cam_height = NOX_BIN_CAM_HEIGHT;
	if (1!=FWRITE_USHORT(tag_cam_height))		// 2
		return showErrorReturnFalse("Writing camera height tag error.");
	if (1!=FWRITE_UINT(cam_height))				// 4
		return showErrorReturnFalse("Writing camera height error.");


	SMatrix matr = cam->GetMg().ToSM();
	float cposx =  matr.off.x * scale;
	float cposy =  matr.off.y * scale;
	float cposz = -matr.off.z * scale;
	unsigned short tag_cam_pos = NOX_BIN_CAM_POS;
	if (1!=FWRITE_USHORT(tag_cam_pos))			// 2
		return showErrorReturnFalse("Writing camera position tag error.");
	if (1!=FWRITE_FLOAT(cposx))					// 4
		return showErrorReturnFalse("Writing camera position error.");
	if (1!=FWRITE_FLOAT(cposy))					// 4
		return showErrorReturnFalse("Writing camera position error.");
	if (1!=FWRITE_FLOAT(cposz))					// 4
		return showErrorReturnFalse("Writing camera position error.");

	SVector cam_dir = matr.v3 + matr.v1*cam_shift_x*tan_hori*2 + matr.v2*cam_shift_y*tan_vert*-2;
	unsigned short tag_cam_dirf = NOX_BIN_CAM_DIR;
	if (1!=FWRITE_USHORT(tag_cam_dirf))			// 2
		return showErrorReturnFalse("Writing camera dir tag error.");
	if (1!=FWRITE_FLOAT(cam_dir.x))				// 4
		return showErrorReturnFalse("Writing camera dir error.");
	if (1!=FWRITE_FLOAT(cam_dir.y))				// 4
		return showErrorReturnFalse("Writing camera dir error.");
	float v3z = -cam_dir.z;
	if (1!=FWRITE_FLOAT(v3z))					// 4
		return showErrorReturnFalse("Writing camera dir error.");

	unsigned short tag_cam_diru = NOX_BIN_CAM_UPDIR;
	if (1!=FWRITE_USHORT(tag_cam_diru))			// 2
		return showErrorReturnFalse("Writing camera dir tag error.");
	if (1!=FWRITE_FLOAT(matr.v2.x))				// 4
		return showErrorReturnFalse("Writing camera dir error.");
	if (1!=FWRITE_FLOAT(matr.v2.y))				// 4
		return showErrorReturnFalse("Writing camera dir error.");
	float v2z = -matr.v2.z;
	if (1!=FWRITE_FLOAT(v2z))					// 4
		return showErrorReturnFalse("Writing camera dir error.");

	unsigned short tag_cam_dirr = NOX_BIN_CAM_RIGHTDIR;
	if (1!=FWRITE_USHORT(tag_cam_dirr))			// 2
		return showErrorReturnFalse("Writing camera dir tag error.");
	if (1!=FWRITE_FLOAT(matr.v1.x))				// 4
		return showErrorReturnFalse("Writing camera dir error.");
	if (1!=FWRITE_FLOAT(matr.v1.y))				// 4
		return showErrorReturnFalse("Writing camera dir error.");
	float v1z = -matr.v1.z;
	if (1!=FWRITE_FLOAT(v1z))					// 4
		return showErrorReturnFalse("Writing camera dir error.");


	unsigned short tag_cam_angle = NOX_BIN_CAM_ANGLE;
	if (1!=FWRITE_USHORT(tag_cam_angle))		// 2
		return showErrorReturnFalse("Writing camera angle tag error.");
	if (1!=FWRITE_FLOAT(cam_angle))				// 4
		return showErrorReturnFalse("Writing camera angle error.");

	unsigned short tag_cam_focusdist = NOX_BIN_CAM_FOCUSDIST;
	if (1!=FWRITE_USHORT(tag_cam_focusdist))	// 2
		return showErrorReturnFalse("Writing camera focus distance tag error.");
	if (1!=FWRITE_FLOAT(cam_focusdist))			// 4
		return showErrorReturnFalse("Writing camera focus distance error.");

	unsigned short tag_cam_iso = NOX_BIN_CAM_ISO;
	if (1!=FWRITE_USHORT(tag_cam_iso))			// 2
		return showErrorReturnFalse("Writing camera ISO tag error.");
	if (1!=FWRITE_FLOAT(cam_iso))				// 4
		return showErrorReturnFalse("Writing camera ISO error.");

	unsigned short tag_cam_dof_on = NOX_BIN_CAM_DOF_ON;
	if (1!=FWRITE_USHORT(tag_cam_dof_on))		// 2
		return showErrorReturnFalse("Writing camera dof on tag error.");
	if (1!=FWRITE_USHORT(cam_dof_on))			// 2
		return showErrorReturnFalse("Writing camera dof on error.");

	unsigned short tag_cam_shutter = NOX_BIN_CAM_SHUTTER;
	if (1!=FWRITE_USHORT(tag_cam_shutter))		// 2
		return showErrorReturnFalse("Writing camera shutter tag error.");
	if (1!=FWRITE_FLOAT(cam_shutter))			// 4
		return showErrorReturnFalse("Writing camera shutter error.");
	
	unsigned short tag_cam_aperture = NOX_BIN_CAM_APERTURE;
	if (1!=FWRITE_USHORT(tag_cam_aperture))		// 2
		return showErrorReturnFalse("Writing camera aperture tag error.");
	if (1!=FWRITE_FLOAT(cam_aperture))			// 4
		return showErrorReturnFalse("Writing camera aperture error.");

	if (cam_diaph_shape)		// total 18 here 
	{
		unsigned short tag_cam_diaph_blades = NOX_BIN_CAM_BLADESNUMBER;
		if (1!=FWRITE_USHORT(tag_cam_diaph_blades))			// 2
			return showErrorReturnFalse("Writing camera diaph blades count tag error.");
		if (1!=FWRITE_USHORT(cam_diaph_blades))				// 2
			return showErrorReturnFalse("Writing camera diaph blades count error.");
		unsigned short tag_cam_diaph_angle = NOX_BIN_CAM_BLADESANGLE;
		if (1!=FWRITE_USHORT(tag_cam_diaph_angle))			// 2
			return showErrorReturnFalse("Writing camera diaph angle tag error.");
		if (1!=FWRITE_FLOAT(cam_diaph_angle))				// 4
			return showErrorReturnFalse("Writing camera diaph angle error.");
		unsigned short tag_cam_diaph_balance = NOX_BIN_CAM_BOKEH_RING_BALANCE;
		if (1!=FWRITE_USHORT(tag_cam_diaph_balance))		// 2
			return showErrorReturnFalse("Writing camera diaph bokeh balance tag error.");
		unsigned short cam_diaph_balance = cam_diaph_bias>0 ? -1 : (unsigned short)(cam_diaph_bias*-20);
		if (1!=FWRITE_USHORT(cam_diaph_balance))			// 2
			return showErrorReturnFalse("Writing camera diaph bokeh balance error.");
		unsigned short tag_cam_diaph_flatten = NOX_BIN_CAM_BOKEH_FLATTEN;
		if (1!=FWRITE_USHORT(tag_cam_diaph_flatten))		// 2
			return showErrorReturnFalse("Writing camera diaph bokeh flatten tag error.");
		unsigned short cam_diaph_flatten = (unsigned short)(fabs(cam_diaph_aniso)*100.0f);
		if (1!=FWRITE_USHORT(cam_diaph_flatten))			// 2
			return showErrorReturnFalse("Writing camera diaph bokeh flatten  error.");
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sizeCam)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Camera chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sizeCam, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXexporter::tempDoShitCamera(CameraObject * cam)
{
	if (!cam)
		return;

	char buf[256];
	Bool pOK;
	GeData dd;

	pOK = cam->GetParameter(DescID(CAMERA_FOCUS), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
	{
		sprintf_s(buf, 256, "focal: %.4f", (float)dd.GetReal());
		GePrint(buf);
	}

	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
	{
		sprintf_s(buf, 256, "sensor: %.4f", (float)dd.GetReal());
		GePrint(buf);
	}

	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_35), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
	{
		sprintf_s(buf, 256, "sensor35: %.4f", (float)dd.GetReal());
		GePrint(buf);
	}

	pOK = cam->GetParameter(DescID(CAMERAOBJECT_APERTURE_ANGLE), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
	{
		sprintf_s(buf, 256, "sensor angle: %.4f", (float)dd.GetReal());
		GePrint(buf);
	}

	pOK = cam->GetParameter(DescID(CAMERAOBJECT_FOV), dd, DESCFLAGS_GET_0);
	if (pOK  &&  dd.GetType()==DA_REAL)
	{
		sprintf_s(buf, 256, "sensor angle: %.4f = %.4f", (float)dd.GetReal(), (float)dd.GetReal()*180.0f/3.1415926535897932384626433832795f);
		GePrint(buf);
	}




}


//========= GEOMETRY ====================================================================================================

void NOXexporter::enumObjectsEvalGeomSize(BaseObject * obj)
{
	while (obj)
	{
		BaseObject *child = obj->GetDown();
		
		if (child)
		{
			enumObjectsEvalGeomSize(child);
		}

		unsigned long long instmeshSize = evalInstanceSourceSize(obj);
		unsigned long long instSize = evalInstanceSize(obj);
		geometrySize += instmeshSize;
		geometrySize += instSize;

		obj = obj->GetNext();
	}
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXexporter::evalSizeGeometry()
{
	geometrySize = 22;
	trisCount = 0;
	meshCount = 0;

	BaseObject * startObj = doc->GetFirstObject();
	enumObjectsEvalGeomSize(startObj);
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::enumObjectsAddMeshes(BaseObject * obj)
{
	while (obj)
	{
		BaseObject *child = obj->GetDown();
		if (child)
		{
			bool ok = enumObjectsAddMeshes(child);
			if (!ok)
				return false;
		}

		if (!insertInstanceSourcePart(obj))
			return false;

		obj = obj->GetNext();
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::enumObjectsAddInstances(BaseObject * obj)
{
	while (obj)
	{
		BaseObject *child = obj->GetDown();
		if (child)
		{
			bool ok = enumObjectsAddInstances(child);
			if (!ok)
				return false;
		}

		if (!insertInstancePart(obj))
			return false;

		obj = obj->GetNext();
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertGeometryPart()
{
	//unsigned long long sizeGeom = evalSizeGeometry();	// or already have one
	unsigned long long chunkStart = _ftelli64(file);
	unsigned int triSize = evalSizeTriangle();

	char buf[256];
	sprintf_s(buf, 256, "Adding geometry, size = %lld bytes", geometrySize);
	showDetailedInfo(buf);

	unsigned short tag_geometry = NOX_BIN_GEOMETRY;
	if (1!=FWRITE_USHORT(tag_geometry))	// 2
		return showErrorReturnFalse("Writing geometry tag error.");
	if (1!=FWRITE_ULONG8(geometrySize))	// 8
		return showErrorReturnFalse("Writing geometry tag size error.");
	if (1!=FWRITE_UINT(meshCount))		// 4
		return showErrorReturnFalse("Writing geometry mesh count error.");
	if (1!=FWRITE_UINT(triSize))		// 4
		return showErrorReturnFalse("Writing geometry triangle size error.");
	if (1!=FWRITE_UINT(trisCount))		// 4
		return showErrorReturnFalse("Writing geometry triangles count error.");

	BaseObject * startObj = doc->GetFirstObject();
	bool ok1 = enumObjectsAddMeshes(startObj);
	if (!ok1)
		return false;
	bool ok2 = enumObjectsAddInstances(startObj);
	if (!ok2)
		return false;


	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != geometrySize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Geometry chunk size is incorrect.\nShould be %llu instead of %llu bytes.", geometrySize, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//========== MESH ======================================================================================================= MESH 

unsigned long long NOXexporter::evalMeshSize(BaseObject * obj)
{
	if (obj->GetType() == Oinstance)
		return 0;

	unsigned int numtris = 0;
	//BaseObject * obj2 = obj->GetCache(NULL);
	BaseObject * obj2 = findDeformedMesh(obj);
	PolygonObject * pobj = ToPoly(obj2?obj2:obj);
	if (!pobj)
		return 0;

	const CPolygon * padr = pobj->GetPolygonR();
	if (!padr)
		return 0;

	LONG pcnt = pobj->GetPolygonCount();

	for (int i=0; i<pcnt; i++)
	{
		numtris++;
		if (!padr[i].IsTriangle())
			numtris++;
	}

	char * name = obj->GetName().GetCStringCopy();
	int lname = name ? (int)strlen(name)+6 : 13;
	unsigned long long triSize = evalSizeTriangle();
	unsigned long long res = triSize * numtris + lname + 14;
	trisCount += numtris;

	return res;
}

//-----------------------------------------------------------------------------------------------------------------------

BaseObject * findDeformedMesh(BaseObject * obj)
{
	if (!obj)
	return NULL;
	BaseObject * tp = obj->GetDeformCache();
	if (tp)
	{
		BaseObject * res = findDeformedMesh(tp);
		if (res)
			return res;
	}
	else
	{
		tp = obj->GetCache(NULL);
		if (tp)
		{
			//GePrint("going down");
			BaseObject * res = findDeformedMesh(tp);
			if (res)
				return res;
			//GePrint("back up");
		}
		else
		{
			if (!obj->GetBit(BIT_CONTROLOBJECT))
			{
				if (obj->IsInstanceOf(Opolygon))
				{
					//GePrint("POLYGON ......");
					//GePrint(obj->GetName());
					return obj;
				}
				//else
				//{
				//	GePrint("not a polygon");
				//	GePrint(obj->GetName());
				//}
			}
			//else 
			//{
			//	GePrint("BIT_CONTROLOBJECT false");
			//	GePrint(obj->GetName());
			//}
		}
	}
	for (tp = obj->GetDown(); tp; tp=tp->GetNext())
	{
		//GePrint("going next");
		BaseObject * res = findDeformedMesh(tp);
		if (res)
			return res;
	}

	return NULL;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertMeshPart(BaseObject * obj)
{
	unsigned long long sizeMesh = evalMeshSize(obj);
	//if (sizeMesh==0)
	//	return true;

	unsigned int numtris = 0;
	BaseObject * obj2 = findDeformedMesh(obj);
	PolygonObject * pobj = ToPoly(obj2?obj2:obj);
	if (!pobj)
		return false;

	const CPolygon * padr = pobj->GetPolygonR();
	if (!padr)
		return false;

	LONG pcnt = pobj->GetPolygonCount();

	for (int i=0; i<pcnt; i++)
	{
		numtris++;
		if (!padr[i].IsTriangle())
			numtris++;
	}

	char buf[512];
	sprintf_s(buf, 512, "Adding mesh %s", obj->GetName().GetCStringCopy());
	showInfo(buf);

	BaseTag * ctag = obj->GetFirstTag();
	int ii = 0;
	while (ctag)
	{
		ii++;
		sprintf_s(buf, 512, "  tag: %s", ctag->GetName().GetCStringCopy());
		showDetailedInfo(buf);
		ctag = ctag->GetNext();
	}

	unsigned int * matIDs = (unsigned int *)malloc(sizeof(unsigned int) * pcnt);
	if (!fillMeshMaterialsArray(obj, matIDs, pcnt))
	{
		for (int i=0; i<pcnt; i++)
			matIDs[i] = 0;
		showWarning("Materials array not created.");
	}

	UVWTag * uvtag = (UVWTag *)pobj->GetTag(Tuvw);
	SVector * normals = pobj->CreatePhongNormals();

	unsigned long long chunkStart = _ftelli64(file);

	unsigned short tag_mesh = NOX_BIN_MESH;
	if (1!=FWRITE_USHORT(tag_mesh))		// 2
		return showErrorReturnFalse("Writing mesh tag error.");
	if (1!=FWRITE_ULONG8(sizeMesh))		// 8
		return showErrorReturnFalse("Writing mesh tag size error.");
	if (1!=FWRITE_UINT(numtris))		// 8
		return showErrorReturnFalse("Writing mesh triangles count error.");

	// name
	char * mname = obj->GetName().GetCStringCopy();
	if (!mname)
		mname = "Unnamed";
	int sname = (int)strlen(mname)+6;
	unsigned short tag_mesh_name = NOX_BIN_MESHNAME;
	if (1!=FWRITE_USHORT(tag_mesh_name))	// 2
		return showErrorReturnFalse("Writing mesh name tag error.");
	if (1!=FWRITE_UINT(sname))				// 4
		return showErrorReturnFalse("Writing mesh name size error.");
	if (1!=FWRITE_STRING(mname, sname-6))	// x
		return showErrorReturnFalse("Writing mesh name error.");

	Vector uv1, uv2, uv3, uv4;
	SVector n1, n2, n3, n4;
	const Vector * vadr = pobj->GetPointR();
	for (int i=0; i<pcnt; i++)
	{
		if (uvtag)
		{
			UVWStruct uvstruct = uvtag->GetSlow(i);
			uv1 = uvstruct.a;
			uv2 = uvstruct.b;
			uv3 = uvstruct.c;
			uv4 = uvstruct.d;
		}
		else
			uv1=uv2=uv3=uv4=Vector(0.0, 0.0, 0.0);

		if (normals)
		{
			n1 = normals[i*4+0];
			n2 = normals[i*4+1];
			n3 = normals[i*4+2];
			n4 = normals[i*4+3];
		}
		else
		{
			n1 = ((vadr[padr[i].b]-vadr[padr[i].a])%(vadr[padr[i].d]-vadr[padr[i].a])).ToSV();
			n2 = ((vadr[padr[i].c]-vadr[padr[i].b])%(vadr[padr[i].a]-vadr[padr[i].b])).ToSV();
			n3 = ((vadr[padr[i].d]-vadr[padr[i].c])%(vadr[padr[i].b]-vadr[padr[i].c])).ToSV();
			n4 = ((vadr[padr[i].a]-vadr[padr[i].d])%(vadr[padr[i].c]-vadr[padr[i].d])).ToSV();
			n1.Normalize();
			n2.Normalize();
			n3.Normalize();
			n4.Normalize();
		}

		
		if (!insertTriangle(pobj, padr[i].a, padr[i].b, padr[i].c, matIDs[i], uv1, uv2, uv3, n1, n2, n3))
			return false;

		if (!padr[i].IsTriangle())
		{
			if (!insertTriangle(pobj, padr[i].a, padr[i].c, padr[i].d, matIDs[i], uv1, uv3, uv4, n1, n3, n4))
				return false;
		}
	}

	free(matIDs);
	if (normals)
		GeFree(normals);

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sizeMesh)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Mesh chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sizeMesh, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::fillMeshMaterialsArray(BaseObject * obj, unsigned int * mats, unsigned int mcount)
{
	if (!mats)
		return false;
	if (mcount<1)
		return false;

	for (unsigned int i=0; i<mcount; i++)
		mats[i] = 0;

	if (!obj)
		return true;
	BaseTag * ctag = NULL;

	int numsels = 0;
	int nummats = 0;
	ctag = obj->GetFirstTag();
	while (ctag)
	{
		if (ctag->GetType() == Tpolygonselection)
			numsels++;
		if (ctag->GetType() == Ttexture)
			nummats++;
		ctag = ctag->GetNext();
	}

	int curMatID = 1;
	ctag = obj->GetFirstTag();
	while (ctag)
	{
		if (ctag->GetType() == Ttexture)
		{
			TextureTag * ttag = (TextureTag *)ctag;

			SelectionTag * selTag = NULL;
			BaseContainer * bc = ttag->GetDataInstance();
			if (bc)
			{
				String selname = bc->GetString(TEXTURETAG_RESTRICTION);
				selTag = (SelectionTag *)findSelectionByName(obj, selname);
			}
			if (selTag)
			{
				BaseSelect * bs = selTag->GetBaseSelect();
				if (bs)
				{
					for (unsigned int i=0; i<mcount; i++)
						if (bs->IsSelected(i))
							mats[i] = curMatID;
				}
			}
			else
			{
				for (unsigned int i=0; i<mcount; i++)
					mats[i] = curMatID;
			}
			curMatID++;
		}
		ctag = ctag->GetNext();
	}

	return true;
}

//========== TRIANGLE ===================================================================================================

unsigned int NOXexporter::evalSizeTriangle()
{
	return 106;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertTriangle(PolygonObject *pobj, unsigned int v1, unsigned int v2, unsigned int v3, unsigned int matID, Vector uv1, Vector uv2, Vector uv3, SVector n1, SVector n2, SVector n3)
{
	if (!pobj)
		return false;
	unsigned int pcnt = pobj->GetPointCount();
	if (v1>=pcnt  ||  v2>=pcnt  ||  v3>=pcnt)
		return showErrorReturnFalse("Triangle vertex id out of bound");

	unsigned int triSize = evalSizeTriangle();
	unsigned long long chunkStart = _ftelli64(file);


	const Vector * vadr = pobj->GetPointR();
	//Vector norm = ((vadr[v2]-vadr[v1])%(vadr[v3]-vadr[v1]));
	//norm.Normalize();

	char tbuf[107];
	*((unsigned short*)&(tbuf[0])) = NOX_BIN_TRIANGLE;
	*((unsigned int *)&(tbuf[2])) = matID;
	*((unsigned int *)&(tbuf[6 ])) = 0;//depr
	*((float *)&(tbuf[10])) = vadr[v1].x * scale;
	*((float *)&(tbuf[14])) = vadr[v1].y * scale;
	*((float *)&(tbuf[18])) = vadr[v1].z * scale;
	*((float *)&(tbuf[22])) = vadr[v2].x * scale;
	*((float *)&(tbuf[26])) = vadr[v2].y * scale;
	*((float *)&(tbuf[30])) = vadr[v2].z * scale;
	*((float *)&(tbuf[34])) = vadr[v3].x * scale;
	*((float *)&(tbuf[38])) = vadr[v3].y * scale;
	*((float *)&(tbuf[42])) = vadr[v3].z * scale;
	*((float *)&(tbuf[46])) = n1.x;
	*((float *)&(tbuf[50])) = n1.y;
	*((float *)&(tbuf[54])) = n1.z;
	*((float *)&(tbuf[58])) = n2.x;
	*((float *)&(tbuf[62])) = n2.y;
	*((float *)&(tbuf[66])) = n2.z;
	*((float *)&(tbuf[70])) = n3.x;
	*((float *)&(tbuf[74])) = n3.y;
	*((float *)&(tbuf[78])) = n3.z;
	*((float *)&(tbuf[82])) = uv1.x;
	*((float *)&(tbuf[86])) = 1.0f-uv1.y;
	*((float *)&(tbuf[90])) = uv2.x;
	*((float *)&(tbuf[94])) = 1.0f-uv2.y;
	*((float *)&(tbuf[98])) = uv3.x;
	*((float *)&(tbuf[102]))= 1.0f-uv3.y;


	if (1!=FWRITE_STRING(tbuf, triSize))		// 2
		return showErrorReturnFalse("Writing triangle tag error.");

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != triSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Triangle chunk size is incorrect.\nShould be %llu instead of %llu bytes.", triSize, realSize);
		return showErrorReturnFalse(errmsg);
	}
	return true;
}

//========= INSTANCE SOURCE =============================================================================================

unsigned long long NOXexporter::evalInstanceSourceSize(BaseObject * obj)
{
	unsigned long long meshSize = evalMeshSize(obj);
	if (meshSize>0)
		return meshSize + 14;
	return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertInstanceSourcePart(BaseObject * obj)
{
	unsigned long long sizeInstSrc = evalInstanceSourceSize(obj);
	if (sizeInstSrc==0)
		return true;

	// remember id
	MeshID mID;
	mID.c4ID = obj->GetGUID();
	mID.noxID = uniqueInstanceID;
	uniqueInstanceID++;
	instanceIDs.Append(mID);

	unsigned long long chunkStart = _ftelli64(file);

	unsigned short tag_instsrc = NOX_BIN_INSTANCE_SOURCE;
	if (1!=FWRITE_USHORT(tag_instsrc))		// 2
		return showErrorReturnFalse("Writing instance source tag error.");
	if (1!=FWRITE_ULONG8(sizeInstSrc))		// 8
		return showErrorReturnFalse("Writing instance source tag size error.");
	if (1!=FWRITE_UINT(mID.noxID))			// 4
		return showErrorReturnFalse("Writing instance source id error.");
	//uniqueInstanceID++;	// todo: inaczej rozwiazac, bo dla instancingu nie da rady go wyciagnac
	
	if (!insertMeshPart(obj))
		return false;

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sizeInstSrc)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Instance Source chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sizeInstSrc, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//============= INSTANCE ================================================================================================

BaseObject * NOXexporter::findInstanceObjectSource(BaseObject * obj)
{
	BaseContainer *cnt = obj->GetDataInstance();
	if (cnt)
	{
		BaseObject * res = cnt->GetObjectLink(1001 , doc);
		return res;
	}
	return NULL;
}

//-----------------------------------------------------------------------------------------------------------------------

int NOXexporter::getInstanceIDfromGUID(unsigned long long id)
{
	int num = instanceIDs.GetCount();
	for (int i=0; i<num; i++)
	{
		if (id==instanceIDs[i].c4ID)
			return instanceIDs[i].noxID;
	}
	return -1;
}

//-----------------------------------------------------------------------------------------------------------------------

unsigned long long NOXexporter::evalInstanceSize(BaseObject * obj)
{
	bool isInstance = (obj->GetType()==Oinstance);
	//BaseObject * obj2 = obj->GetCache(NULL);
	BaseObject * obj2 = findDeformedMesh(obj);
	PolygonObject * pobj = ToPoly((BaseObject*)(obj2?obj2:obj));
	if (!pobj  &&  !isInstance)	// todo: also check reference object
		return 0;

	const CPolygon * padr = pobj ? pobj->GetPolygonR() : NULL;
	if (!padr)
		return 0;

	// count selections (materials)
	int nummats = 1;
	BaseObject * obj3 = obj;
	if (isInstance)
		obj3 = findInstanceObjectSource(obj);
	BaseTag * ctag = obj3->GetFirstTag();
	while (ctag)
	{
		if (ctag->GetType() == Ttexture)
			nummats++;
		ctag = ctag->GetNext();
	}

	char * name = obj->GetName().GetCStringCopy();
	int lname = name ? (int)strlen(name)+6 : 13;
	unsigned int cmatssize = 4+4*nummats;

	unsigned long long res = lname + 14 + 66 + cmatssize;

	return res;
}

//-----------------------------------------------------------------------------------------------------------------------

BaseTag * NOXexporter::findSelectionByName(BaseObject * obj, String name)
{
	if (!obj)
		return NULL;
	if (!name.Content())
		return NULL;

	BaseTag * ctag = obj->GetFirstTag();
	while (ctag)
	{
		if (ctag->GetType() == Tpolygonselection)
			if (ctag->GetName() == name)
				return ctag;
		ctag = ctag->GetNext();
	}
	return NULL;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertInstancePart(BaseObject * obj)
{
	unsigned long long sizeInst = evalInstanceSize(obj);
	if (sizeInst==0)
		return true;

	BaseObject * obj2 = obj;
	if (obj->GetType() == Oinstance)
		obj2 = findInstanceObjectSource(obj);
	if (!obj2)
		return true;
	int instanceID = getInstanceIDfromGUID(obj2->GetGUID());
	if (instanceID<0)
		return true;

	char * name = obj->GetName().GetCStringCopy();
	int lname = name ? (int)strlen(name)+6 : 13;

	// count selections/materials
	unsigned short nummats = 1;
	int numsels = 0;
	BaseObject * obj3 = obj;
	if (obj->GetType() == Oinstance)
		obj3 = findInstanceObjectSource(obj);
	BaseTag * ctag = obj3->GetFirstTag();
	while (ctag)
	{
		if (ctag->GetType() == Tpolygonselection)
			numsels++;
		if (ctag->GetType() == Ttexture)
			nummats++;
		ctag = ctag->GetNext();
	}

	char buf[512];
	sprintf_s(buf, 512, "Adding instance %s", obj->GetName().GetCStringCopy());
	showInfo(buf);

	unsigned long long chunkStart = _ftelli64(file);

	unsigned short tag_instance = NOX_BIN_INSTANCE;
	if (1!=FWRITE_USHORT(tag_instance))		// 2
		return showErrorReturnFalse("Writing instance tag error.");
	if (1!=FWRITE_ULONG8(sizeInst))			// 8
		return showErrorReturnFalse("Writing instance tag size error.");
	if (1!=FWRITE_UINT(instanceID))			// 4
		return showErrorReturnFalse("Writing instance id error.");
	
	// --- name ---
	char * mname = obj->GetName().GetCStringCopy();
	if (!mname)
		mname = "Unnamed";
	int sname = (int)strlen(mname)+6;
	unsigned short tag_inst_name = NOX_BIN_INSTANCE_NAME;
	if (1!=FWRITE_USHORT(tag_inst_name))	// 2
		return showErrorReturnFalse("Writing instance name tag error.");
	if (1!=FWRITE_UINT(sname))				// 4
		return showErrorReturnFalse("Writing instance name size error.");
	if (1!=FWRITE_STRING(mname, sname-6))	// x
		return showErrorReturnFalse("Writing instance name error.");

	// --- materials ---
	unsigned short tag_inst_materials = NOX_BIN_INSTANCE_MATERIALS;
	if (1!=FWRITE_USHORT(tag_inst_materials))	// 2
		return showErrorReturnFalse("Writing instance materials tag error.");
	//unsigned short num_mats = 1;
	if (1!=FWRITE_USHORT(nummats))				// 2
		return showErrorReturnFalse("Writing instance materials count error.");
	unsigned int mat_id = 0;
	if (1!=FWRITE_UINT(mat_id))					// 4
		return showErrorReturnFalse("Writing instance material id error.");
	ctag = obj3->GetFirstTag();
	while (ctag)
	{
		if (ctag->GetType() == Ttexture)
		{
			TextureTag * ttag = (TextureTag *)ctag;
			BaseMaterial * mat = ttag->GetMaterial();
			unsigned int matID = findMaterialID(mat);
			if (1!=FWRITE_UINT(matID))					// 4
				return showErrorReturnFalse("Writing instance material id error.");
		}
		ctag = ctag->GetNext();
	}

	// --- matrix ---
	Matrix matr = Matrix();
	matr = obj->GetMg();
	matr.off = matr.off * scale;
	unsigned short tag_inst_matrix = NOX_BIN_INSTANCE_MATRIX;
	if (1!=FWRITE_USHORT(tag_inst_matrix))		// 2
		return showErrorReturnFalse("Writing instance matrix tag error.");
	for (int i=0; i<3; i++)
	{
		float mpl = i==2 ? -1.0f : 1.0f;
		for (int j=0; j<4; j++)
		{
			float a = (float)matr[(j+1)%4][i];
			a *= mpl;
			if (1!=FWRITE_FLOAT(a))	// 4
				return showErrorReturnFalse("Writing instance matrix tag error.");
		}
	}
	for (int i=0; i<4; i++)
	{
		float t = i==3 ? 1.0f : 0.0f;
		if (1!=FWRITE_FLOAT(t))	// 4
			return showErrorReturnFalse("Writing instance matrix tag error.");
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sizeInst)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Instance chunk size is incorrect.\nShould be %llu instead of %llu bytes.", sizeInst, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//=========== MATERIALS =================================================================================================

unsigned long long NOXexporter::evalMaterialsSize()
{
	unsigned long long defMatSize = evalDefaultMaterialSize();

	unsigned long long matsSize = 0;
	matsCount = 1;
	BaseMaterial * cmat = doc->GetFirstMaterial();
	while (cmat)
	{
		unsigned long long cmatsize = evalMaterialSize(cmat);
		matsSize += cmatsize;
		matsCount++;
		cmat = cmat->GetNext();
	}

	return defMatSize + matsSize + 14;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertMaterials()
{
	unsigned long long chunkStart = _ftelli64(file);
	unsigned long long matsSize = evalMaterialsSize();

	unsigned short tag_materials = NOX_BIN_MATERIALS;
	if (1!=FWRITE_USHORT(tag_materials))	// 2
		return showErrorReturnFalse("Writing materials tag error.");
	if (1!=FWRITE_ULONG8(matsSize))			// 8
		return showErrorReturnFalse("Writing materials tag size error.");
	//unsigned int numMats = 1;		// ZLEEEEEEEEEEE
	if (1!=FWRITE_UINT(matsCount))			// 4
		return showErrorReturnFalse("Writing materials count error.");

	if (!insertDefaultMaterial())
		return false;

	BaseMaterial * cmat = doc->GetFirstMaterial();
	while (cmat)
	{
		//tempDoShitMaterial(cmat);
		if (!insertMaterial(cmat))
			return false;
		cmat = cmat->GetNext();
		uniqueMatID++;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matsSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Materials chunk size is incorrect.\nShould be %llu instead of %llu bytes.", matsSize, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

unsigned int NOXexporter::findMaterialID(BaseMaterial * mat)
{
	if (!mat)
		return 0;
	unsigned int result = 0;
	BaseMaterial * cmat = doc->GetFirstMaterial();
	while (cmat)
	{
		result++;
		if (mat==cmat)
			return result;
		cmat = cmat->GetNext();
	}
	return result;
}

//=========== MATERIAL ==================================================================================================

unsigned long long NOXexporter::evalMaterialSize(BaseMaterial * mat)
{
	if (!mat)
		return 0;

	GeData dd;
	bool enabled_reflection = false;
	bool enabled_color = false;
	bool enabled_transparency = false;
	bool enabled_luminance = false;
	bool enabled_normal = false;
	bool enabled_bump = false;
	bool enabled_displacement = false;
	bool enabled_displacement_sub = false;
	bool enabled_alpha = false;
	bool enabled_bump_in_refl = false;
	bool refl_transp_on_same = false;
	if (mat->GetParameter(DescID(MATERIAL_USE_REFLECTION), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_reflection = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_COLOR), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_color = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_TRANSPARENCY), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_transparency = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_LUMINANCE), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_luminance = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_NORMAL), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_normal = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_BUMP), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_bump = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_DISPLACEMENT), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_displacement = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_ALPHA), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_alpha = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_DISPLACEMENT_SUBPOLY), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_displacement_sub = (dd.GetBool()!=0);
	enabled_displacement &= enabled_displacement_sub;
	if (mat->GetParameter(DescID(MATERIAL_REFLECTION_USE_BUMP), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_bump_in_refl = (dd.GetBool()!=0);
	if (enabled_reflection  &&  enabled_transparency)
	{
		float bri_refl=1.0f, bri_transp=1.0f;
		float blur_refl=0.0f, blur_transp=0.0f;
		if (mat->GetParameter(DescID(MATERIAL_REFLECTION_BRIGHTNESS), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				bri_refl = dd.GetReal();
		if (mat->GetParameter(DescID(MATERIAL_TRANSPARENCY_BRIGHTNESS), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				bri_transp = dd.GetReal();
		if (mat->GetParameter(DescID(MATERIAL_REFLECTION_DISPERSION), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				blur_refl = dd.GetReal();
		if (mat->GetParameter(DescID(MATERIAL_TRANSPARENCY_DISPERSION), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				blur_transp = dd.GetReal();
		if (blur_refl==blur_transp  &&  bri_refl==bri_transp)
			refl_transp_on_same = true;
	}

	unsigned long long texSizeColor =			!enabled_color			? 0 : evalTextureSize(mat, MATERIAL_COLOR_SHADER);
	unsigned long long texSizeLuminance =		!enabled_luminance		? 0 : evalTextureSize(mat, MATERIAL_LUMINANCE_SHADER);
	unsigned long long texSizeReflection =		!enabled_reflection		? 0 : evalTextureSize(mat, MATERIAL_REFLECTION_SHADER);
	unsigned long long texSizeNormal =			!enabled_normal			? 0 : evalTextureSize(mat, MATERIAL_NORMAL_SHADER);
	unsigned long long texSizeBump =			!enabled_bump			? 0 : evalTextureSize(mat, MATERIAL_BUMP_SHADER);
	unsigned long long texSizeDisplacement =	!enabled_displacement	? 0 : evalTextureSize(mat, MATERIAL_DISPLACEMENT_SHADER);
	unsigned long long texSizeTransparency =	!enabled_transparency	? 0 : evalTextureSize(mat, MATERIAL_TRANSPARENCY_SHADER);
	unsigned long long texSizeAlpha =			!enabled_alpha			? 0 : evalTextureSize(mat, MATERIAL_ALPHA_SHADER);
	if (texSizeNormal>0  &&  texSizeBump>0)
	{
		texSizeNormal = 0;
		enabled_normal = false;
	}


	unsigned long long layers_size = 0;
	if (!enabled_reflection && !enabled_color && !enabled_transparency && !enabled_luminance)
		layers_size = 34;

	if (enabled_color)
	{
		unsigned long long color_size = 0;
		color_size = 34 + texSizeColor + texSizeNormal + texSizeBump;
		layers_size += color_size;
	}

	if (enabled_reflection  &&  !refl_transp_on_same)
	{
		unsigned long long refl_size = 0;
		refl_size = 52 + texSizeReflection;
		if (enabled_bump_in_refl)
			refl_size += texSizeBump + texSizeNormal;
		layers_size += refl_size;
	}

	if (enabled_transparency)
	{
		unsigned long long transp_size = 0;
		transp_size = 92 + texSizeTransparency + texSizeNormal + texSizeBump;
		if (refl_transp_on_same)
			transp_size += texSizeReflection;
		layers_size += transp_size;
	}

	if (enabled_luminance)
	{
		unsigned long long lumi_size = 0;
		lumi_size = 52 + texSizeLuminance;
		layers_size += lumi_size;
	}

	char * mname = mat->GetName().GetCStringCopy();
	int cname = mname ? (int)strlen(mname)+6 : 13;

	return 62 + cname + layers_size + texSizeAlpha + texSizeDisplacement;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertMaterial(BaseMaterial * mat)
{
	if (!mat)
		return true;

	unsigned long long chunkStart = _ftelli64(file);
	unsigned long long matSize = evalMaterialSize(mat);

	unsigned short tag_material = NOX_BIN_MATERIAL;
	if (1!=FWRITE_USHORT(tag_material))		// 2
		return showErrorReturnFalse("Writing material tag error.");
	if (1!=FWRITE_ULONG8(matSize))			// 8
		return showErrorReturnFalse("Writing material tag size error.");
	if (1!=FWRITE_UINT(uniqueMatID))		// 4
		return showErrorReturnFalse("Writing material ID error.");
	unsigned int matBlend = 0;
	if (1!=FWRITE_UINT(matBlend))			// 4
		return showErrorReturnFalse("Writing material blend channel error.");
	unsigned int numLayers = 0;
	if (1!=FWRITE_UINT(numLayers))			// 4
		return showErrorReturnFalse("Writing material layers count error.");

	// name
	char * mname = mat->GetName().GetCStringCopy();
	int cname = mname ? (int)strlen(mname)+6 : 13;
	unsigned short tag_mat_name = NOX_BIN_MATNAME;
	if (1!=FWRITE_USHORT(tag_mat_name))		// 2
		return showErrorReturnFalse("Writing material name tag error.");
	if (1!=FWRITE_UINT(cname))				// 4
		return showErrorReturnFalse("Writing material name size error.");
	if (1!=FWRITE_STRING(mname, cname-6))	// x
		return showErrorReturnFalse("Writing material name error.");
	
	// get enabled channels
	GeData dd;
	bool enabled_reflection = false;
	bool enabled_color = false;
	bool enabled_transparency = false;
	bool enabled_luminance = false;
	bool enabled_normal = false;
	bool enabled_bump = false;
	bool enabled_bump_in_refl = false;
	bool enabled_displacement = false;
	bool enabled_displacement_sub = false;
	bool enabled_alpha = false;
	bool enabled_glow = false;
	bool use_default_layer = false;
	bool refl_transp_on_same = false;
	if (mat->GetParameter(DescID(MATERIAL_USE_REFLECTION), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_reflection = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_COLOR), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_color = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_TRANSPARENCY), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_transparency = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_LUMINANCE), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_luminance = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_NORMAL), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_normal = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_BUMP), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_bump = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_DISPLACEMENT), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_displacement = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_USE_GLOW), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_glow = (dd.GetBool()!=0);
	if (mat->GetParameter(DescID(MATERIAL_DISPLACEMENT_SUBPOLY), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_displacement_sub = (dd.GetBool()!=0);
	enabled_displacement &= enabled_displacement_sub;
	if (mat->GetParameter(DescID(MATERIAL_REFLECTION_USE_BUMP), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_bump_in_refl = (dd.GetBool()!=0);
	if (enabled_reflection  &&  enabled_transparency)
	{
		float bri_refl=1.0f, bri_transp=1.0f;
		float blur_refl=0.0f, blur_transp=0.0f;

		if (mat->GetParameter(DescID(MATERIAL_REFLECTION_BRIGHTNESS), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				bri_refl = dd.GetReal();
		if (mat->GetParameter(DescID(MATERIAL_TRANSPARENCY_BRIGHTNESS), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				bri_transp = dd.GetReal();
		// bluriness
		if (mat->GetParameter(DescID(MATERIAL_REFLECTION_DISPERSION), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				blur_refl = dd.GetReal();
		if (mat->GetParameter(DescID(MATERIAL_TRANSPARENCY_DISPERSION), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				blur_transp = dd.GetReal();
		if (blur_refl==blur_transp  &&  bri_refl==bri_transp)
			refl_transp_on_same = true;
	}


	if (mat->GetParameter(DescID(MATERIAL_USE_ALPHA), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled_alpha = (dd.GetBool()!=0);
	if (!enabled_reflection && !enabled_color && !enabled_transparency && !enabled_luminance)
		use_default_layer = true;

	unsigned long long texSizeColor =			!enabled_color			? 0 : evalTextureSize(mat, MATERIAL_COLOR_SHADER);
	unsigned long long texSizeLuminance =		!enabled_luminance		? 0 : evalTextureSize(mat, MATERIAL_LUMINANCE_SHADER);
	unsigned long long texSizeReflection =		!enabled_reflection		? 0 : evalTextureSize(mat, MATERIAL_REFLECTION_SHADER);
	unsigned long long texSizeNormal =			!enabled_normal			? 0 : evalTextureSize(mat, MATERIAL_NORMAL_SHADER);
	unsigned long long texSizeBump =			!enabled_bump			? 0 : evalTextureSize(mat, MATERIAL_BUMP_SHADER);
	unsigned long long texSizeDisplacement =	!enabled_displacement	? 0 : evalTextureSize(mat, MATERIAL_DISPLACEMENT_SHADER);
	unsigned long long texSizeTransparency =	!enabled_transparency	? 0 : evalTextureSize(mat, MATERIAL_TRANSPARENCY_SHADER);
	unsigned long long texSizeAlpha =			!enabled_alpha			? 0 : evalTextureSize(mat, MATERIAL_ALPHA_SHADER);
	if (texSizeNormal>0  &&  texSizeBump>0)
	{
		texSizeNormal = 0;
		enabled_normal = false;
	}


	// LAMBERT LAYER ---- 34 bytes + tex
	if (enabled_color)
	{
		unsigned short tag_mlayer = NOX_BIN_MATLAYER;
		if (1!=FWRITE_USHORT(tag_mlayer))		// 2
			return showErrorReturnFalse("Writing material layer tag error.");
		unsigned long long layerSize = 34 + texSizeColor + texSizeNormal + texSizeBump;
		if (1!=FWRITE_ULONG8(layerSize))		// 8
			return showErrorReturnFalse("Writing material layer tag size error.");
		unsigned short mlay_type = 2;
		if (1!=FWRITE_USHORT(mlay_type))		// 2
			return showErrorReturnFalse("Writing material layer type error.");
		unsigned int mlay_contr = 100;
		if (1!=FWRITE_UINT(mlay_contr))			// 4
			return showErrorReturnFalse("Writing material layer contribution error.");

		Vector col_diff = Vector(0.8f, 0.8f, 0.8f);
		if (mat->GetParameter(DescID(MATERIAL_COLOR_COLOR), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_VECTOR)
				col_diff = dd.GetVector();
		if (!insertColor(NOX_BIN_MLAY_REFL0, col_diff, "Writing material layer color0 error."))
			return false;						// 18
		

		if (!insertTexturePart(mat, LAYER_TEX_SLOT_COLOR0, MATERIAL_COLOR_SHADER, getTexMod(mat, MATERIAL_COLOR_SHADER)))
			return false;

		if (enabled_normal)
			if (!insertTexturePart(mat, LAYER_TEX_SLOT_NORMAL, MATERIAL_NORMAL_SHADER, getTexMod(mat, MATERIAL_NORMAL_SHADER)))
				return false;
		if (enabled_bump)
			if (!insertTexturePart(mat, LAYER_TEX_SLOT_NORMAL, MATERIAL_BUMP_SHADER, getTexMod(mat, MATERIAL_BUMP_SHADER)))
				return false;
		//getTexMod(mat, MATERIAL_COLOR_SHADER);
	}	// LAMBERT LAYER END

	// ONLY REFLECTION LAYER ---- 52 bytes + tex
	if (enabled_reflection  &&  !refl_transp_on_same)
	{
		unsigned short tag_mlayer = NOX_BIN_MATLAYER;
		if (1!=FWRITE_USHORT(tag_mlayer))			// 2
			return showErrorReturnFalse("Writing material layer tag error.");
		unsigned long long layerSize = 52 + texSizeReflection;
		if (enabled_bump_in_refl)
			layerSize += texSizeNormal + texSizeBump;
		if (1!=FWRITE_ULONG8(layerSize))			// 8
			return showErrorReturnFalse("Writing material layer tag size error.");
		unsigned short mlay_type = 2;
		if (1!=FWRITE_USHORT(mlay_type))			// 2
			return showErrorReturnFalse("Writing material layer type error.");
		unsigned int mlay_contr = 100;
		if (1!=FWRITE_UINT(mlay_contr))				// 4
			return showErrorReturnFalse("Writing material layer contribution error.");

		Vector col_refl = Vector(0.8f, 0.8f, 0.8f);
		if (mat->GetParameter(DescID(MATERIAL_REFLECTION_COLOR), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_VECTOR)
				col_refl = dd.GetVector();
		if (!insertColor(NOX_BIN_MLAY_REFL0, col_refl, "Writing material layer color0 error."))
			return false;							// 18

		float lay_rough = 0.4f;
		if (mat->GetParameter(DescID(MATERIAL_REFLECTION_DISPERSION), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				lay_rough = dd.GetReal();
		if (lay_rough > 0.99f)
			lay_rough = 0.99f;

		float ior = 10.0f;

		unsigned short tag_mlay_rough = NOX_BIN_MLAY_ROUGH;
		if (1!=FWRITE_USHORT(tag_mlay_rough))		// 2
			return showErrorReturnFalse("Writing material layer roughness tag error.");
		if (1!=FWRITE_FLOAT(lay_rough))				// 4
			return showErrorReturnFalse("Writing material layer roughness error.");

		unsigned short tag_mlay_fresnel = NOX_BIN_MLAY_FRESNEL;
		if (1!=FWRITE_USHORT(tag_mlay_fresnel))		// 2
			return showErrorReturnFalse("Writing material layer fresnel tag error.");
		unsigned int freslen = 12;
		if (1!=FWRITE_UINT(freslen))				// 4
			return showErrorReturnFalse("Writing material layer fresnel size error.");
		unsigned short tag_mlay_fresnel_ior = NOX_BIN_MLAY_FRES_IOR;
		if (1!=FWRITE_USHORT(tag_mlay_fresnel_ior))	// 2
			return showErrorReturnFalse("Writing material layer ior tag error.");
		if (1!=FWRITE_FLOAT(ior))					// 4
			return showErrorReturnFalse("Writing material layer ior error.");

		if (!insertTexturePart(mat, LAYER_TEX_SLOT_COLOR0, MATERIAL_REFLECTION_SHADER, getTexMod(mat, MATERIAL_REFLECTION_SHADER)))
			return false;

		if (enabled_bump_in_refl)
		{
			if (enabled_normal)
				if (!insertTexturePart(mat, LAYER_TEX_SLOT_NORMAL, MATERIAL_NORMAL_SHADER, getTexMod(mat, MATERIAL_NORMAL_SHADER)))
					return false;
			if (enabled_bump)
				if (!insertTexturePart(mat, LAYER_TEX_SLOT_NORMAL, MATERIAL_BUMP_SHADER, getTexMod(mat, MATERIAL_BUMP_SHADER)))
					return false;
		}
	}	// ONLY REFLECTION LAYER END


	// ONLY TRANSMISSION LAYER ---- 92 bytes + tex
	if (enabled_transparency)//  &&  !enabled_reflection)
	{
		unsigned short tag_mlayer = NOX_BIN_MATLAYER;
		if (1!=FWRITE_USHORT(tag_mlayer))			// 2
			return showErrorReturnFalse("Writing material layer tag error.");
		unsigned long long layerSize = 92 + texSizeTransparency + texSizeNormal + texSizeBump;
		if (refl_transp_on_same)
			layerSize += texSizeReflection;
		if (1!=FWRITE_ULONG8(layerSize))			// 8
			return showErrorReturnFalse("Writing material layer tag size error.");
		unsigned short mlay_type = 2;
		if (1!=FWRITE_USHORT(mlay_type))			// 2
			return showErrorReturnFalse("Writing material layer type error.");
		unsigned int mlay_contr = 100;
		if (1!=FWRITE_UINT(mlay_contr))				// 4
			return showErrorReturnFalse("Writing material layer contribution error.");

		Vector col0 = Vector(0,0,0);
		Vector col90 = Vector(0,0,0);
		if (refl_transp_on_same)
		{
			Vector col90 = Vector(1,1,1);
			if (mat->GetParameter(DescID(MATERIAL_REFLECTION_COLOR), dd, DESCFLAGS_GET_0))
				if (dd.GetType()==DA_VECTOR)
					col0 = dd.GetVector();

		}

		Vector col_transm = Vector(0.8f, 0.8f, 0.8f);
		if (mat->GetParameter(DescID(MATERIAL_TRANSPARENCY_COLOR), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_VECTOR)
				col_transm = dd.GetVector();
		if (!insertColor(NOX_BIN_MLAY_TRANSM_COLOR, col_transm, "Writing material layer transmission color error."))
			return false;							// 18
		if (!insertColor(NOX_BIN_MLAY_REFL0,  col0,  "Writing material layer refl0 color error."))
			return false;							// 18
		if (!insertColor(NOX_BIN_MLAY_REFL90, col90, "Writing material layer refl90 color error."))
			return false;							// 18
		
		unsigned short tag_mlay_transm_on = NOX_BIN_MLAY_TRANSM_ON;
		if (1!=FWRITE_USHORT(tag_mlay_transm_on))	// 2
			return showErrorReturnFalse("Writing material layer transmission on tag error.");
		unsigned short transm_on = 1;
		if (1!=FWRITE_USHORT(transm_on))			// 2
			return showErrorReturnFalse("Writing material layer transmission on error.");


		float lay_rough = 0.4f;
		if (mat->GetParameter(DescID(MATERIAL_TRANSPARENCY_DISPERSION), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				lay_rough = dd.GetReal();
		if (lay_rough > 0.99f)
			lay_rough = 0.99f;
		unsigned short tag_mlay_rough = NOX_BIN_MLAY_ROUGH;
		if (1!=FWRITE_USHORT(tag_mlay_rough))		// 2
			return showErrorReturnFalse("Writing material layer roughness tag error.");
		if (1!=FWRITE_FLOAT(lay_rough))				// 4
			return showErrorReturnFalse("Writing material layer roughness error.");

		float ior = 10.0f;
		if (mat->GetParameter(DescID(MATERIAL_TRANSPARENCY_REFRACTION), dd, DESCFLAGS_GET_0))
			if (dd.GetType()==DA_REAL)
				ior = dd.GetReal();
		unsigned short tag_mlay_fresnel = NOX_BIN_MLAY_FRESNEL;
		if (1!=FWRITE_USHORT(tag_mlay_fresnel))		// 2
			return showErrorReturnFalse("Writing material layer fresnel tag error.");
		unsigned int freslen = 12;
		if (1!=FWRITE_UINT(freslen))				// 4
			return showErrorReturnFalse("Writing material layer fresnel size error.");
		unsigned short tag_mlay_fresnel_ior = NOX_BIN_MLAY_FRES_IOR;
		if (1!=FWRITE_USHORT(tag_mlay_fresnel_ior))	// 2
			return showErrorReturnFalse("Writing material layer ior tag error.");
		if (1!=FWRITE_FLOAT(ior))					// 4
			return showErrorReturnFalse("Writing material layer ior error.");





		if (!insertTexturePart(mat, LAYER_TEX_SLOT_COLOR0, MATERIAL_TRANSPARENCY_SHADER, getTexMod(mat, MATERIAL_TRANSPARENCY_SHADER)))
			return false;
		if (refl_transp_on_same)
			if (!insertTexturePart(mat, LAYER_TEX_SLOT_COLOR0, MATERIAL_REFLECTION_SHADER, getTexMod(mat, MATERIAL_REFLECTION_SHADER)))
				return false;

		if (enabled_normal)
			if (!insertTexturePart(mat, LAYER_TEX_SLOT_NORMAL, MATERIAL_NORMAL_SHADER, getTexMod(mat, MATERIAL_NORMAL_SHADER)))
				return false;
		if (enabled_bump)
			if (!insertTexturePart(mat, LAYER_TEX_SLOT_NORMAL, MATERIAL_BUMP_SHADER, getTexMod(mat, MATERIAL_BUMP_SHADER)))
				return false;
	}	// TRANSMISSION ONLY LAYER END


	// EMISSION LAYER ---- 52 bytes + tex 
	if (enabled_luminance)
	{
		unsigned short tag_mlayer = NOX_BIN_MATLAYER;
		if (1!=FWRITE_USHORT(tag_mlayer))		// 2
			return showErrorReturnFalse("Writing material layer tag error.");
		unsigned long long layerSize = 52 + texSizeLuminance;
		if (1!=FWRITE_ULONG8(layerSize))		// 8
			return showErrorReturnFalse("Writing material layer tag size error.");
		unsigned short mlay_type = 1;
		if (1!=FWRITE_USHORT(mlay_type))		// 2
			return showErrorReturnFalse("Writing material layer type error.");
		unsigned int mlay_contr = 100;
		if (1!=FWRITE_UINT(mlay_contr))			// 4
			return showErrorReturnFalse("Writing material layer contribution error.");

		unsigned short tag_em_color =	NOX_BIN_MLAY_EMISS_COLOR;
		unsigned short tag_em_temp =	NOX_BIN_MLAY_EMISS_TEMP;
		unsigned short tag_em_pow =		NOX_BIN_MLAY_EMISS_POWER;
		unsigned short tag_em_unit =	NOX_BIN_MLAY_EMISS_UNIT;
		Vector em_color = Vector(1.0, 1.0, 1.0);
		unsigned int em_temp = 6000;
		float em_power = 100.0f;
		unsigned int em_unit = 1;

		if (!insertColor(tag_em_color, em_color, "Writing material layer emission color error."))
			return false;						// 18
		if (1!=FWRITE_USHORT(tag_em_temp))		// 2
			return showErrorReturnFalse("Writing material layer emission temperature tag error.");
		if (1!=FWRITE_UINT(em_temp))			// 4
			return showErrorReturnFalse("Writing material layer emission temperature error.");
		if (1!=FWRITE_USHORT(tag_em_pow))		// 2
			return showErrorReturnFalse("Writing material layer emission power tag error.");
		if (1!=FWRITE_FLOAT(em_power))			// 4
			return showErrorReturnFalse("Writing material layer emission power error.");
		if (1!=FWRITE_USHORT(tag_em_unit))		// 2
			return showErrorReturnFalse("Writing material layer emission unit tag error.");
		if (1!=FWRITE_UINT(em_unit))			// 4
			return showErrorReturnFalse("Writing material layer emission unit error.");

		if (!insertTexturePart(mat, LAYER_TEX_SLOT_LIGHT, MATERIAL_LUMINANCE_SHADER, getTexMod(mat, MATERIAL_LUMINANCE_SHADER)))
			return false;						// X
	}	// EMISSION LAYER END

	// DEFAULT LAYER - when no usable channel enabled - 34 bytes
	if (use_default_layer)
	{
		unsigned short tag_mlayer = NOX_BIN_MATLAYER;
		if (1!=FWRITE_USHORT(tag_mlayer))		// 2
			return showErrorReturnFalse("Writing material layer tag error.");
		unsigned long long layerSize = 34;
		if (1!=FWRITE_ULONG8(layerSize))		// 8
			return showErrorReturnFalse("Writing material layer tag size error.");
		unsigned short mlay_type = 2;
		if (1!=FWRITE_USHORT(mlay_type))		// 2
			return showErrorReturnFalse("Writing material layer type error.");
		unsigned int mlay_contr = 100;
		if (1!=FWRITE_UINT(mlay_contr))			// 4
			return showErrorReturnFalse("Writing material layer contribution error.");

		if (!insertColor(NOX_BIN_MLAY_REFL0, Vector(0.8, 0.8, 0.8), "Writing material layer color0 error."))
			return false;						// 18
	}	// DEFAULT LAYER END


	unsigned short tag_mat_skyportal = NOX_BIN_MAT_SKYPORTAL;
	if (1!=FWRITE_USHORT(tag_mat_skyportal))	// 2
		return showErrorReturnFalse("Writing material skyportal tag error.");
	unsigned short portal_on = enabled_glow ? 1 : 0;
	if (1!=FWRITE_USHORT(portal_on))			// 2
		return showErrorReturnFalse("Writing material skyportal error.");


	float opacity = 1.0f;
	unsigned short tag_mat_opacity = NOX_BIN_MAT_OPACITY;
	if (1!=FWRITE_USHORT(tag_mat_opacity))		// 2
		return showErrorReturnFalse("Writing material opacity tag error.");
	if (1!=FWRITE_FLOAT(opacity))				// 4
		return showErrorReturnFalse("Writing material opacity tag error.");

	if (!insertTexturePart(mat, MAT_TEX_SLOT_OPACITY, MATERIAL_ALPHA_SHADER, getTexMod(mat, MATERIAL_ALPHA_SHADER)))
		return false;							// X

	// displacement shit
	unsigned short tag_disp_depth = NOX_BIN_MAT_DISPLACEMENT_DEPTH;
	unsigned short tag_disp_subdivs = NOX_BIN_MAT_DISPLACEMENT_SUBDIVS;
	unsigned short tag_disp_continuity = NOX_BIN_MAT_DISPLACEMENT_CONTINUITY;
	unsigned short tag_disp_ignscale = NOX_BIN_MAT_DISPLACEMENT_IGNORE_SCALE;
	unsigned short tag_disp_shift = NOX_BIN_MAT_DISPLACEMENT_SHIFT;
	unsigned short tag_disp_normalmode = NOX_BIN_MAT_DISPLACEMENT_NORMAL_MODE;
	float disp_depth = 0.0f;
	float disp_shift = 0.0f;
	unsigned short disp_subdivs = 1;	// range 1-8
	unsigned short disp_continuity = 1;
	unsigned short disp_ignorescale = 0;
	unsigned int disp_normalmode = 0;	// 0-smooth, 1-face, 2-source
	if (mat->GetParameter(DescID(MATERIAL_DISPLACEMENT_SUBPOLY_SUBDIVISION), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			disp_subdivs = dd.GetLong();
	if (disp_subdivs > 8)
		disp_subdivs = 8;
	if (mat->GetParameter(DescID(MATERIAL_DISPLACEMENT_HEIGHT), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_REAL)
			disp_depth = dd.GetReal() * scale;

	if (1!=FWRITE_USHORT(tag_disp_depth))		// 2
		return showErrorReturnFalse("Writing material displacement depth tag error.");
	if (1!=FWRITE_FLOAT(disp_depth))			// 4
		return showErrorReturnFalse("Writing material displacement depth error.");
	if (1!=FWRITE_USHORT(tag_disp_subdivs))		// 2
		return showErrorReturnFalse("Writing material displacement subdivs tag error.");
	if (1!=FWRITE_USHORT(disp_subdivs))			// 2
		return showErrorReturnFalse("Writing material displacement subdivs error.");
	if (1!=FWRITE_USHORT(tag_disp_continuity))	// 2
		return showErrorReturnFalse("Writing material displacement continuity tag error.");
	if (1!=FWRITE_USHORT(disp_continuity))		// 2
		return showErrorReturnFalse("Writing material displacement continuity error.");
	if (1!=FWRITE_USHORT(tag_disp_ignscale))	// 2
		return showErrorReturnFalse("Writing material displacement ignore scale tag error.");
	if (1!=FWRITE_USHORT(disp_ignorescale))		// 2
		return showErrorReturnFalse("Writing material displacement ignore scale error.");
	if (1!=FWRITE_USHORT(tag_disp_shift))		// 2
		return showErrorReturnFalse("Writing material displacement shift tag error.");
	if (1!=FWRITE_FLOAT(disp_shift))			// 4
		return showErrorReturnFalse("Writing material displacement shift error.");
	if (1!=FWRITE_USHORT(tag_disp_normalmode))	// 2
		return showErrorReturnFalse("Writing material displacement normalmode tag error.");
	if (1!=FWRITE_UINT(disp_normalmode))		// 4
		return showErrorReturnFalse("Writing material displacement normalmode error.");

	if (enabled_displacement)
		if (!insertTexturePart(mat, MAT_TEX_SLOT_DISPLACEMENT, MATERIAL_DISPLACEMENT_SHADER, getTexMod(mat, MATERIAL_DISPLACEMENT_SHADER)))
			return false;							// X




	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Material chunk size is incorrect.\nShould be %llu instead of %llu bytes.", matSize, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//======= DEFAULT MATERIAL ==============================================================================================

unsigned long long NOXexporter::evalDefaultMaterialSize()
{
	return 38+17;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertDefaultMaterial()
{
	unsigned long long chunkStart = _ftelli64(file);
	unsigned long long matSize = evalDefaultMaterialSize();

	unsigned short tag_material = NOX_BIN_MATERIAL;
	if (1!=FWRITE_USHORT(tag_material))		// 2
		return showErrorReturnFalse("Writing default material tag error.");
	if (1!=FWRITE_ULONG8(matSize))			// 8
		return showErrorReturnFalse("Writing default material tag size error.");
	unsigned int matID = 0;
	if (1!=FWRITE_UINT(matID))				// 4
		return showErrorReturnFalse("Writing default material ID error.");
	unsigned int matBlend = 0;
	if (1!=FWRITE_UINT(matBlend))			// 4
		return showErrorReturnFalse("Writing default material blend channel error.");
	unsigned int numLayers = 0;
	if (1!=FWRITE_UINT(numLayers))			// 4
		return showErrorReturnFalse("Writing default material layers count error.");

	// name
	char * mname = "Default mat";
	int sname = 17;
	unsigned short tag_mat_name = NOX_BIN_MATNAME;
	if (1!=FWRITE_USHORT(tag_mat_name))		// 2
		return showErrorReturnFalse("Writing default material name tag error.");
	if (1!=FWRITE_UINT(sname))				// 4
		return showErrorReturnFalse("Writing default material name size error.");
	if (1!=FWRITE_STRING(mname, sname-6))	// x
		return showErrorReturnFalse("Writing default material name error.");

	// layer
	unsigned short tag_mlayer = NOX_BIN_MATLAYER;
	if (1!=FWRITE_USHORT(tag_mlayer))		// 2
		return showErrorReturnFalse("Writing default material layer tag error.");
	unsigned long long layerSize = 16;
	if (1!=FWRITE_ULONG8(layerSize))		// 8
		return showErrorReturnFalse("Writing default material layer tag size error.");
	unsigned short mlay_type = 2;
	if (1!=FWRITE_USHORT(mlay_type))		// 2
		return showErrorReturnFalse("Writing default material layer type error.");
	unsigned int mlay_contr = 100;
	if (1!=FWRITE_UINT(mlay_contr))			// 4
		return showErrorReturnFalse("Writing default material layer contribution error.");

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Default material chunk size is incorrect.\nShould be %llu instead of %llu bytes.", matSize, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}
	
//=======================================================================================================================

void NOXexporter::tempDoShitMaterial(BaseMaterial * mat)
{
	if (!mat)
		return;

	GePrint("Looking material ...");
	GePrint(mat->GetName());
	char buf[256];


	BaseChannel * ch = mat->GetChannel(CHANNEL_COLOR);
	if (ch)
	{
		BaseContainer bc = ch->GetData();
		int typeee = bc.GetType(BASECHANNEL_COLOR_EX);
		Bool on = bc.GetBool(MATERIAL_USE_COLOR);
		String texname = bc.GetString(BASECHANNEL_TEXTURE);
		Vector col = bc.GetVector(BASECHANNEL_COLOR_EX);
		sprintf_s(buf, 256, "material color color type: %d, enabled: %s  color: %.3f, %.3f, %.3f", typeee, (on==TRUE?"yes":"no"), col.x, col.y, col.z);
		GePrint(buf);
		GePrint(texname);
	}
	else
	{
		GePrint("no color channel");
	}
	//BaseContainer bc = mat->GetData();

	//--- spec

	GeData dd;
	
	mat->GetParameter(DescID(MATERIAL_SPECULAR_WIDTH), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material specular mode type: %d, value: %f", dd.GetType(), dd.GetReal());
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_COLOR), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use color type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_LUMINANCE), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use luminance type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_REFLECTION), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use reflection type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_FOG), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use fog type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_NORMAL), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use normal type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_SPECULAR), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use specular type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_DIFFUSION), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use diffusion type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_TRANSPARENCY), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use transparency type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);


	mat->GetParameter(DescID(MATERIAL_USE_ENVIRONMENT), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use environment type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_BUMP), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use bump type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_ALPHA), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use alpha type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_USE_GLOW), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material use glow type: %d, value: %s", dd.GetType(), dd.GetBool()?"yes":"no");
	GePrint(buf);

	mat->GetParameter(DescID(MATERIAL_COLOR_COLOR), dd, DESCFLAGS_GET_0);
	sprintf_s(buf, 256, "material color color type: %d, value: %f  %f  %f", dd.GetType(), dd.GetVector().ToSV().x, dd.GetVector().ToSV().y, dd.GetVector().ToSV().z);
	GePrint(buf);

	char * texColor = getTexFilename(mat, MATERIAL_COLOR_SHADER);
	if (texColor)
	{
		GePrint("Tex COLOR");
		GePrint(texColor);
	}

	char * texSpec = getTexFilename(mat, MATERIAL_SPECULAR_SHADER);
	if (texSpec)
	{
		GePrint("Tex SPECULAR");
		GePrint(texSpec);
	}

}

//=======================================================================================================================

char * NOXexporter::getTexFilename(BaseMaterial * mat, unsigned int chID)
{
	GeData ddf;
	BaseList2D * bFilter = NULL;
	BaseList2D * bTexture = NULL;
	mat->GetParameter(DescID(chID), ddf, DESCFLAGS_GET_0);
	if (ddf.GetType()==DA_ALIASLINK)
	{
		BaseLink * bl = ddf.GetBaseLink();
		if (bl)
		{
			BaseList2D * bl2d = bl->GetLink(doc);
			if (bl2d)
			{
				if (bl2d->GetType() == Xbitmap)
					bTexture = bl2d;
				if (bl2d->GetType() == Xfilter)
					bFilter = bl2d;
			}
		}
	}

	if (bFilter)
	{
		GeData dd2;
		bFilter->GetParameter(DescID(1001), dd2, DESCFLAGS_GET_0);
		if (dd2.GetType()==DA_ALIASLINK)
		{
			BaseLink * bl = dd2.GetBaseLink();
			if (bl)
			{
				BaseList2D * bl2d = bl->GetLink(doc);
				if (bl2d)
				{
					if (bl2d->GetType() == Xbitmap)
						bTexture = bl2d;
				}
			}
		}
	}

	if (bTexture)
	{
		GeData dd3;
		bTexture->GetParameter(DescID(BITMAPSHADER_FILENAME), dd3, DESCFLAGS_GET_0);
		if (dd3.GetType()==DA_FILENAME)
			return dd3.GetFilename().GetString().GetCStringCopy();
	}

	return NULL;
}

//-----------------------------------------------------------------------------------------------------------------------

TexModNox NOXexporter::getTexMod(BaseMaterial * mat, unsigned int channelID)
{
	TexModNox res;

	GeData ddf;
	BaseList2D * bFilter = NULL;
	BaseList2D * bTexture = NULL;
	mat->GetParameter(DescID(channelID), ddf, DESCFLAGS_GET_0);
	if (ddf.GetType()==DA_ALIASLINK)
	{
		BaseLink * bl = ddf.GetBaseLink();
		if (bl)
		{
			BaseList2D * bl2d = bl->GetLink(doc);
			if (bl2d)
			{
				if (bl2d->GetType() == Xbitmap)
					bTexture = bl2d;
				if (bl2d->GetType() == Xfilter)
					bFilter = bl2d;
			}
		}
	}
	//char buf[256];
	if (bFilter)
	{
		GeData dd2;
		Bool pOK;

		pOK = bFilter->GetParameter(DescID(1002), dd2, DESCFLAGS_GET_0);	// hue
		if (pOK  &&  dd2.GetType()==DA_REAL)
			res.hue = (float)dd2.GetReal()/3.1415926535897932384626433832795;

		pOK = bFilter->GetParameter(DescID(1003), dd2, DESCFLAGS_GET_0);	// saturation
		if (pOK  &&  dd2.GetType()==DA_REAL)
			res.saturation = (float)dd2.GetReal();

		pOK = bFilter->GetParameter(DescID(1006), dd2, DESCFLAGS_GET_0);	// brightness
		if (pOK  &&  dd2.GetType()==DA_REAL)
			res.brightness = (float)dd2.GetReal();

		pOK = bFilter->GetParameter(DescID(1007), dd2, DESCFLAGS_GET_0);	// contrast
		if (pOK  &&  dd2.GetType()==DA_REAL)
		{
			float c = (float)dd2.GetReal();
			res.contrast = c < 0.0f ? c+1.0f : c*3.0f+1.0f;
		}

		// gamma = 1011
	}


	if (channelID == MATERIAL_BUMP_SHADER)
	{
		GeData dd;
		Bool pOK = mat->GetParameter(DescID(MATERIAL_BUMP_STRENGTH), dd, DESCFLAGS_GET_0);
		if (pOK  &&  dd.GetType()==DA_REAL)
		{
			float strg = (float)dd.GetReal();
			if (strg<0.0f)
				res.normalInvertX = res.normalInvertY = true;
			res.normalPowerEV = fabs(strg) * 10.0f - 5.0f;
		}
	}
	
	if (channelID == MATERIAL_NORMAL_SHADER)
	{
		GeData dd;
		Bool pOK = mat->GetParameter(DescID(MATERIAL_NORMAL_STRENGTH), dd, DESCFLAGS_GET_0);
		if (pOK  &&  dd.GetType()==DA_REAL)
		{
			float strg = (float)dd.GetReal();
			if (strg>2.0f)
				strg = 2.0f;
			res.normalPowerEV = strg * 5.0f - 5.0f;
		}
		pOK = mat->GetParameter(DescID(MATERIAL_NORMAL_REVERSEX), dd, DESCFLAGS_GET_0);
		if (pOK  &&  dd.GetType()==DA_LONG)
		{
			bool invx = (dd.GetBool()!=0);
			res.normalInvertX = invx;
		}
		pOK = mat->GetParameter(DescID(MATERIAL_NORMAL_REVERSEY), dd, DESCFLAGS_GET_0);
		if (pOK  &&  dd.GetType()==DA_LONG)
		{
			bool invy = (dd.GetBool()!=0);
			res.normalInvertY = invy;
		}
	}

	if (channelID == MATERIAL_ALPHA_SHADER)
	{
		GeData dd;
		Bool pOK = mat->GetParameter(DescID(MATERIAL_ALPHA_INVERT), dd, DESCFLAGS_GET_0);
		if (pOK  &&  dd.GetType()==DA_LONG)
		{
			bool inv = (dd.GetBool()!=0);
			res.invert = inv;
		}
	}

	switch (channelID)
	{
		case MATERIAL_COLOR_SHADER:
		case MATERIAL_LUMINANCE_SHADER:
		case MATERIAL_REFLECTION_SHADER:
		case MATERIAL_TRANSPARENCY_SHADER:
			res.gamma = 2.2f;
			break;
		case MATERIAL_ALPHA_SHADER:
		case MATERIAL_NORMAL_SHADER:
		case MATERIAL_BUMP_SHADER:
		case MATERIAL_DISPLACEMENT_SHADER:
			res.gamma = 1.0f;
			break;
		default:
			res.gamma = 2.0f;
	}

	return res;
}


//=======================================================================================================================

unsigned long long NOXexporter::evalTextureSize(BaseMaterial * mat, unsigned int texC4Did)
{
	if (!mat)
		return 0;

	char * filename = getTexFilename(mat, texC4Did);
	if (!filename)
		return 0;

	int cfname = (int)strlen(filename);
	if (cfname<1)
		return 0;
	cfname += 6;

	unsigned long long texpost = 62;
	unsigned long long normpost = 20;

	return cfname + 18  + normpost + texpost;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertTexturePart(BaseMaterial * mat, int texNoxID, unsigned int texC4Did, TexModNox tmod)
{
	if (!mat)
		return false;

	char * filename = getTexFilename(mat, texC4Did);
	if (!filename)
		return true;

	int cfname = (int)strlen(filename);
	if (cfname<1)
		return true;
	cfname += 6;


	unsigned long long chunkStart = _ftelli64(file);
	unsigned long long texSize = evalTextureSize(mat, texC4Did);

	unsigned short tag_texture = NOX_BIN_MLAY_TEXTURE;
	if (1!=FWRITE_USHORT(tag_texture))		// 2
		return showErrorReturnFalse("Writing texture tag error.");
	if (1!=FWRITE_ULONG8(texSize))			// 8
		return showErrorReturnFalse("Writing texture size error.");

	unsigned short tex_type = 0;
	if (1!=FWRITE_USHORT(tex_type))			// 2
		return showErrorReturnFalse("Writing texture type error.");
	unsigned short tex_slot = texNoxID;
	if (1!=FWRITE_USHORT(tex_slot))			// 2
		return showErrorReturnFalse("Writing texture slot error.");

	unsigned short tag_tex_filename = NOX_BIN_MLAY_TEX_FILENAME;
	if (1!=FWRITE_USHORT(tag_tex_filename))	// 2
		return showErrorReturnFalse("Writing texture filename tag error.");
	if (1!=FWRITE_UINT(cfname))				// 4
		return showErrorReturnFalse("Writing texture filename size error.");
	if (1!=FWRITE_STRING(filename, (cfname-6)))	// X
		return showErrorReturnFalse("Writing texture filename error.");

	unsigned short tag_tex_enabled = NOX_BIN_MLAY_TEX_ENABLED;
	if (1!=FWRITE_USHORT(tag_tex_enabled))	// 2
		return showErrorReturnFalse("Writing texture enabled tag error.");
	unsigned short texenabled = 1;
	if (1!=FWRITE_USHORT(texenabled))		// 2
		return showErrorReturnFalse("Writing texture enabled error.");

	// POSTPROCESS
	unsigned short tag_tex_post = NOX_BIN_MLAY_TEX_POST;
	if (1!=FWRITE_USHORT(tag_tex_post))		// 2
		return showErrorReturnFalse("Writing texture post tag error.");
	unsigned int texpostsize = 62;
	if (1!=FWRITE_UINT(texpostsize))		// 4
		return showErrorReturnFalse("Writing texture post size error.");

	unsigned short tag_post_bri = NOX_BIN_MLAY_TEX_POST_BRIGHTNESS;
	if (1!=FWRITE_USHORT(tag_post_bri))		// 2
		return showErrorReturnFalse("Writing texture post brightness tag error.");
	if (1!=FWRITE_FLOAT(tmod.brightness))	// 4
		return showErrorReturnFalse("Writing texture post brightness error.");

	unsigned short tag_post_con = NOX_BIN_MLAY_TEX_POST_CONTRAST;
	if (1!=FWRITE_USHORT(tag_post_con))		// 2
		return showErrorReturnFalse("Writing texture post contrast tag error.");
	if (1!=FWRITE_FLOAT(tmod.contrast))	// 4
		return showErrorReturnFalse("Writing texture post contrast error.");

	unsigned short tag_post_sat = NOX_BIN_MLAY_TEX_POST_SATURATION;
	if (1!=FWRITE_USHORT(tag_post_sat))		// 2
		return showErrorReturnFalse("Writing texture post saturation tag error.");
	if (1!=FWRITE_FLOAT(tmod.saturation))	// 4
		return showErrorReturnFalse("Writing texture post saturation error.");

	unsigned short tag_post_hue = NOX_BIN_MLAY_TEX_POST_HUE;
	if (1!=FWRITE_USHORT(tag_post_hue))		// 2
		return showErrorReturnFalse("Writing texture post hue tag error.");
	if (1!=FWRITE_FLOAT(tmod.hue))			// 4
		return showErrorReturnFalse("Writing texture post hue error.");

	unsigned short tag_post_red = NOX_BIN_MLAY_TEX_POST_RED;
	if (1!=FWRITE_USHORT(tag_post_red))		// 2
		return showErrorReturnFalse("Writing texture post red tag error.");
	if (1!=FWRITE_FLOAT(tmod.red))			// 4
		return showErrorReturnFalse("Writing texture post red error.");

	unsigned short tag_post_gre = NOX_BIN_MLAY_TEX_POST_GREEN;
	if (1!=FWRITE_USHORT(tag_post_gre))		// 2
		return showErrorReturnFalse("Writing texture post green tag error.");
	if (1!=FWRITE_FLOAT(tmod.green))		// 4
		return showErrorReturnFalse("Writing texture post green error.");

	unsigned short tag_post_blu = NOX_BIN_MLAY_TEX_POST_BLUE;
	if (1!=FWRITE_USHORT(tag_post_blu))		// 2
		return showErrorReturnFalse("Writing texture post blue tag error.");
	if (1!=FWRITE_FLOAT(tmod.blue))		// 4
		return showErrorReturnFalse("Writing texture post blue error.");

	unsigned short tag_post_gam = NOX_BIN_MLAY_TEX_POST_GAMMA;
	if (1!=FWRITE_USHORT(tag_post_gam))		// 2
		return showErrorReturnFalse("Writing texture post gamma tag error.");
	if (1!=FWRITE_FLOAT(tmod.gamma))		// 4
		return showErrorReturnFalse("Writing texture post gamma error.");

	unsigned short tag_post_inv = NOX_BIN_MLAY_TEX_POST_INVERT;
	if (1!=FWRITE_USHORT(tag_post_inv))		// 2
		return showErrorReturnFalse("Writing texture post invert tag error.");
	unsigned short tinv = tmod.invert ? 1 : 0;
	if (1!=FWRITE_USHORT(tinv))				// 2
		return showErrorReturnFalse("Writing texture post invert error.");

	unsigned short tag_post_intpl = NOX_BIN_MLAY_TEX_POST_INTERPOLATE;
	if (1!=FWRITE_USHORT(tag_post_intpl))	// 2
		return showErrorReturnFalse("Writing texture post interpolate tag error.");
	unsigned short tintpl = tmod.interpolateProbe ? 1 : 0;
	if (1!=FWRITE_USHORT(tintpl))			// 2
		return showErrorReturnFalse("Writing texture post interpolate error.");

	// NORMAL POSTPROCESS
	unsigned short tag_norm_post = NOX_BIN_MLAY_NORM_POST;
	if (1!=FWRITE_USHORT(tag_norm_post))	// 2
		return showErrorReturnFalse("Writing texture normal post tag error.");
	unsigned int normpostsize = 20;
	if (1!=FWRITE_UINT(normpostsize))		// 4
		return showErrorReturnFalse("Writing texture normal post size error.");

	unsigned short tag_npost_inv_x = NOX_BIN_MLAY_NORM_POST_INVERT_X;
	if (1!=FWRITE_USHORT(tag_npost_inv_x))	// 2
		return showErrorReturnFalse("Writing texture normal post invert x tag error.");
	unsigned short ninvx = tmod.normalInvertX ? 1 : 0;
	if (1!=FWRITE_USHORT(ninvx))			// 2
		return showErrorReturnFalse("Writing texture normal post invert x error.");

	unsigned short tag_npost_inv_y = NOX_BIN_MLAY_NORM_POST_INVERT_Y;
	if (1!=FWRITE_USHORT(tag_npost_inv_y))	// 2
		return showErrorReturnFalse("Writing texture normal post invert y tag error.");
	unsigned short ninvy = tmod.normalInvertY ? 1 : 0;
	if (1!=FWRITE_USHORT(ninvy))			// 2
		return showErrorReturnFalse("Writing texture normal post invert y error.");

	unsigned short tag_npost_power = NOX_BIN_MLAY_NORM_POST_POWER_EV;
	if (1!=FWRITE_USHORT(tag_npost_power))		// 2
		return showErrorReturnFalse("Writing texture normal post power tag error.");
	if (1!=FWRITE_FLOAT(tmod.normalPowerEV))	// 4
		return showErrorReturnFalse("Writing texture normal post power error.");

	// CHECK POS
	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != texSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Texture chunk size is incorrect.\nShould be %llu instead of %llu bytes.", texSize, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//=======================================================================================================================

bool NOXexporter::insertColor(unsigned short tag, Vector col, char * errStr)
{
	float c0r = col.x;
	float c0g = col.y;
	float c0b = col.z;
	float c0a = 1.0f;
	if (1!=FWRITE_USHORT(tag))				// 2
		return showErrorReturnFalse(errStr);
	if (1!=FWRITE_FLOAT(c0r))				// 4
		return showErrorReturnFalse(errStr);
	if (1!=FWRITE_FLOAT(c0g))				// 4
		return showErrorReturnFalse(errStr);
	if (1!=FWRITE_FLOAT(c0b))				// 4
		return showErrorReturnFalse(errStr);
	if (1!=FWRITE_FLOAT(c0a))				// 4
		return showErrorReturnFalse(errStr);
	return true;
}

//=======================================================================================================================

BaseObject * NOXexporter::findSunsky(BaseObject * obj)
{
	BaseObject * res = NULL;
	while (obj)
	{
		if (obj->GetType()==1011146)	// physical sky
			return obj;
		BaseObject *child = obj->GetDown();
		if (child)
		{
			BaseObject * found = findSunsky(child);
			if (found)
				return found;
		}
		obj = obj->GetNext();
	}

	return res;
}

//-----------------------------------------------------------------------------------------------------------------------

unsigned long long NOXexporter::evalSunskySize()
{
	if (!sunsky)
		return 0;
	return 90;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertSunsky()
{
	if (!sunsky)
		return true;
	bool sky_enabled = false;
	bool sun_enabled = false;
	float pos_lat=0, pos_long=0;
	float pos_gmt = 0;
	float turbidity = 2.0f;
	unsigned short dt_month=3, dt_day=21, dt_hour=12, dt_minute=0;
	unsigned short ss_model = 1;
	unsigned short ss_sun_other = 1;

	Bool pOK;
	GeData dd;
	char buf[256];

	pOK = sunsky->GetParameter(DescID(27007), dd, DESCFLAGS_GET_0);	// sky enabled
	if (pOK  &&  dd.GetType()==DA_LONG)
		sky_enabled = (dd.GetBool()!=0);
	pOK = sunsky->GetParameter(DescID(25000), dd, DESCFLAGS_GET_0);	// sun enabled
	if (pOK  &&  dd.GetType()==DA_LONG)
		sun_enabled = (dd.GetBool()!=0);
	pOK = sunsky->GetParameter(DescID(10200), dd, DESCFLAGS_GET_0);	// latitude
	if (pOK  &&  dd.GetType()==DA_REAL)
		pos_lat = dd.GetReal();
	pOK = sunsky->GetParameter(DescID(10201), dd, DESCFLAGS_GET_0);	// longitude
	if (pOK  &&  dd.GetType()==DA_REAL)
		pos_long = dd.GetReal() * -1;
	pOK = sunsky->GetParameter(DescID(10208), dd, DESCFLAGS_GET_0);	// gmt
	if (pOK  &&  dd.GetType()==DA_REAL)
		pos_gmt = dd.GetReal();
	pOK = sunsky->GetParameter(DescID(24020), dd, DESCFLAGS_GET_0);	// turbidity
	if (pOK  &&  dd.GetType()==DA_REAL)
		turbidity = dd.GetReal();

	showInfo("Adding sunsky");
	if (sky_enabled)
		showDetailedInfo("Sky enabled");
	if (sun_enabled)
		showDetailedInfo("Sun enabled");

	sprintf_s(buf, 256, "pos: %.2f x %.2f  +%.1f gmt", pos_long, pos_lat, pos_gmt);
	showDetailedInfo(buf);

	pOK = sunsky->GetParameter(DescID(10000), dd, DESCFLAGS_GET_0);	// date/time - 1009398
	if (pOK  &&  dd.GetType()==1009398)
	{
		DateTimeData * dtd = (DateTimeData *)dd.GetCustomDataType(dd.GetType());
		if (dtd)
		{
			DateTime dt = dtd->GetDateTime();
			dt_month = dt.month;
			dt_day = dt.day;
			dt_hour = dt.hour;
			dt_minute = dt.minute;
			sprintf_s(buf, 256, "date: %d-%d  %d:%d", dt.month, dt.day, dt.hour, dt.minute );
			showDetailedInfo(buf);
		}
	}

	Vector vE = Vector(1.0f, 0.0f, 0.0f);
	Vector vN = Vector(0.0f, 0.0f, 1.0f);
	Matrix matr = sunsky->GetMg();
	matr.off = Vector(0,0,0);
	Vector vMN = matr * vN;
	Vector vME = matr * vE;

	unsigned long long chunkStart = _ftelli64(file);
	unsigned long long ssSize = evalSunskySize();

	unsigned short tag_sunsky = NOX_BIN_SUNSKY;
	if (1!=FWRITE_USHORT(tag_sunsky))		// 2
		return showErrorReturnFalse("Writing sunsky tag error.");
	if (1!=FWRITE_ULONG8(ssSize))			// 8
		return showErrorReturnFalse("Writing sunsky size error.");

	unsigned short tag_ss_sky_on = NOX_BIN_SS_ON;
	if (1!=FWRITE_USHORT(tag_ss_sky_on))	// 2
		return showErrorReturnFalse("Writing sunsky sky on tag error.");
	unsigned short sky_on = sky_enabled ? 1 : 0;
	if (1!=FWRITE_USHORT(sky_on))			// 2
		return showErrorReturnFalse("Writing sunsky sky on error.");

	unsigned short tag_ss_sun_on = NOX_BIN_SS_SUN_ON;
	if (1!=FWRITE_USHORT(tag_ss_sun_on))	// 2
		return showErrorReturnFalse("Writing sunsky sun on tag error.");
	unsigned short sun_on = sun_enabled ? 1 : 0;
	if (1!=FWRITE_USHORT(sun_on))			// 2
		return showErrorReturnFalse("Writing sunsky sun on error.");

	unsigned short tag_ss_month = NOX_BIN_SS_MONTH;
	if (1!=FWRITE_USHORT(tag_ss_month))		// 2
		return showErrorReturnFalse("Writing sunsky month tag error.");
	if (1!=FWRITE_USHORT(dt_month))			// 2
		return showErrorReturnFalse("Writing sunsky month error.");

	unsigned short tag_ss_day = NOX_BIN_SS_DAY;
	if (1!=FWRITE_USHORT(tag_ss_day))		// 2
		return showErrorReturnFalse("Writing sunsky day tag error.");
	if (1!=FWRITE_USHORT(dt_day))			// 2
		return showErrorReturnFalse("Writing sunsky day error.");

	unsigned short tag_ss_hour = NOX_BIN_SS_HOUR;
	if (1!=FWRITE_USHORT(tag_ss_hour))		// 2
		return showErrorReturnFalse("Writing sunsky hour tag error.");
	if (1!=FWRITE_USHORT(dt_hour))			// 2
		return showErrorReturnFalse("Writing sunsky hour error.");

	unsigned short tag_ss_minute = NOX_BIN_SS_MINUTE;
	if (1!=FWRITE_USHORT(tag_ss_minute))	// 2
		return showErrorReturnFalse("Writing sunsky minute tag error.");
	if (1!=FWRITE_USHORT(dt_minute))		// 2
		return showErrorReturnFalse("Writing sunsky minute error.");

	unsigned short tag_ss_gmt = NOX_BIN_SS_GMT;
	if (1!=FWRITE_USHORT(tag_ss_gmt))		// 2
		return showErrorReturnFalse("Writing sunsky gmt tag error.");
	unsigned short us_gmt = (unsigned short)(int)pos_gmt;
	if (1!=FWRITE_USHORT(us_gmt))			// 2
		return showErrorReturnFalse("Writing sunsky gmt error.");

	unsigned short tag_ss_long = NOX_BIN_SS_LONGITUDE;
	if (1!=FWRITE_USHORT(tag_ss_long))		// 2
		return showErrorReturnFalse("Writing sunsky longitude tag error.");
	if (1!=FWRITE_FLOAT(pos_long))			// 4
		return showErrorReturnFalse("Writing sunsky longitude error.");

	unsigned short tag_ss_lat = NOX_BIN_SS_LATITUDE;
	if (1!=FWRITE_USHORT(tag_ss_lat))		// 2
		return showErrorReturnFalse("Writing sunsky latitude tag error.");
	if (1!=FWRITE_FLOAT(pos_lat))			// 4
		return showErrorReturnFalse("Writing sunsky latitude error.");

	unsigned short tag_ss_turb= NOX_BIN_SS_TURBIDITY;
	if (1!=FWRITE_USHORT(tag_ss_turb))		// 2
		return showErrorReturnFalse("Writing sunsky turbidity tag error.");
	if (1!=FWRITE_FLOAT(turbidity))			// 4
		return showErrorReturnFalse("Writing sunsky turbidity error.");

	unsigned short tag_ss_dirs = NOX_BIN_SS_DIRECTIONS;
	if (1!=FWRITE_USHORT(tag_ss_dirs))		// 2
		return showErrorReturnFalse("Writing sunsky directions tag error.");
	//float f_one = 1.0f, f_zero = 0.0f, f_minusone = -1.0f;
	float dex=vME.x,  dey=vME.y,  dez=-vME.z;
	float dnx=vMN.x,  dny=vMN.y,  dnz=-vMN.z;
	// north dir
	if (1!=FWRITE_FLOAT(dnx))			// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dny))			// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dnz))		// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	// east dir
	if (1!=FWRITE_FLOAT(dex))				// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dey))			// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dez))			// 4
		return showErrorReturnFalse("Writing sunsky directions error.");

	unsigned short tag_ss_model = NOX_BIN_SS_MODEL;
	if (1!=FWRITE_USHORT(tag_ss_model))		// 2
		return showErrorReturnFalse("Writing sunsky model tag error.");
	if (1!=FWRITE_USHORT(ss_model))			// 2
		return showErrorReturnFalse("Writing sunsky model error.");

	unsigned short tag_ss_sun_other = NOX_BIN_SS_SUN_OTHER_LAYER;
	if (1!=FWRITE_USHORT(tag_ss_sun_other))	// 2
		return showErrorReturnFalse("Writing sunsky sun on other layer tag error.");
	if (1!=FWRITE_USHORT(ss_sun_other))		// 2
		return showErrorReturnFalse("Writing sunsky sun on other layer error.");


	// CHECK POS
	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != ssSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Sunsky chunk size is incorrect.\nShould be %llu instead of %llu bytes.", ssSize, realSize);
		return showErrorReturnFalse(errmsg);
	}


	return true;
}

//=======================================================================================================================

BaseObject * NOXexporter::findSunskyEnv(BaseObject * obj)
{
	BaseObject * res = NULL;
	while (obj)
	{
		if (obj->GetType()==5105)	// normal sky
			return obj;
		BaseObject *child = obj->GetDown();
		if (child)
		{
			BaseObject * found = findSunskyEnv(child);
			if (found)
				return found;
		}
		obj = obj->GetNext();
	}

	return res;
}

//-----------------------------------------------------------------------------------------------------------------------

unsigned long long NOXexporter::evalEnvmapSize()
{
	if (!envsky)
		return 0;

	BaseMaterial * mat = NULL;
	BaseTag * ctag = envsky->GetFirstTag();
	while (ctag)
	{
		if (ctag->GetType() == Ttexture)
			mat = ((TextureTag *)ctag)->GetMaterial();
		ctag = ctag->GetNext();
	}
	if (!mat)
		return 0;
	char * texEnv = getTexFilename(mat, MATERIAL_LUMINANCE_SHADER);
	if (!texEnv)
		return 0;

	unsigned long long envtexsize = evalTextureSize(mat, MATERIAL_LUMINANCE_SHADER);

	return 56 + envtexsize;
}

//-----------------------------------------------------------------------------------------------------------------------

bool NOXexporter::insertEnvmap()
{
	if (!envsky)
		return true;

	BaseMaterial * mat = NULL;
	BaseTag * ctag = envsky->GetFirstTag();
	Matrix tmatr;
	while (ctag)
	{
		if (ctag->GetType() == Ttexture)
		{
			TextureTag * ttag = (TextureTag *)ctag;
			tmatr = ttag->GetMl();
			mat = ttag->GetMaterial();
		}
		ctag = ctag->GetNext();
	}

	if (!mat)
		return true;

	char * texEnv = getTexFilename(mat, MATERIAL_LUMINANCE_SHADER);
	if (!texEnv)
		return true;

	float powerEV = 0.0f;
	unsigned short blendLayer = 3;
	unsigned short enabled = 1;
	float azimuth = 0.0f;

	Vector vE = Vector(0.0f, 0.0f, 1.0f);
	Vector vN = Vector(-1.0f, 0.0f, 0.0f);
	Matrix matr = envsky->GetMg();
	Matrix matr2 = matr * tmatr;
	matr2.off = Vector(0,0,0);
	Vector vMN = matr2 * vN;
	Vector vME = matr2 * vE;
	vMN.Normalize();
	vME.Normalize();

	GeData dd;
	if (mat->GetParameter(DescID(MATERIAL_USE_LUMINANCE), dd, DESCFLAGS_GET_0))
		if (dd.GetType()==DA_LONG)
			enabled = (dd.GetBool()!=0) ? 1 : 0;
	

	unsigned long long chunkStart = _ftelli64(file);
	unsigned long long envSize = evalEnvmapSize();

	unsigned short tag_envmap = NOX_BIN_ENVIRONMENT_MAP;
	if (1!=FWRITE_USHORT(tag_envmap))		// 2
		return showErrorReturnFalse("Writing environment tag error.");
	if (1!=FWRITE_ULONG8(envSize))			// 8
		return showErrorReturnFalse("Writing environment size error.");


	if (!insertTexturePart(mat, TEX_SLOT_ENV, MATERIAL_LUMINANCE_SHADER, getTexMod(mat, MATERIAL_LUMINANCE_SHADER)))
		return false;

	unsigned short tag_enabled = NOX_BIN_ENV_ENABLED;
	if (1!=FWRITE_USHORT(tag_enabled))		// 2
		return showErrorReturnFalse("Writing env enabled tag error.");
	if (1!=FWRITE_USHORT(enabled))			// 2
		return showErrorReturnFalse("Writing env enabled error.");

	unsigned short tag_blend = NOX_BIN_ENV_BLEND_LAYER;
	if (1!=FWRITE_USHORT(tag_blend))		// 2
		return showErrorReturnFalse("Writing env blend layer tag error.");
	if (1!=FWRITE_USHORT(blendLayer))		// 2
		return showErrorReturnFalse("Writing env blend layer error.");

	unsigned short tag_power = NOX_BIN_ENV_POWER_EV;
	if (1!=FWRITE_USHORT(tag_power))		// 2
		return showErrorReturnFalse("Writing env power tag error.");
	if (1!=FWRITE_FLOAT(powerEV))			// 4
		return showErrorReturnFalse("Writing env power error.");

	unsigned short tag_azimuth = NOX_BIN_ENV_AZIMUTH;
	if (1!=FWRITE_USHORT(tag_azimuth))		// 2
		return showErrorReturnFalse("Writing env azimuth tag error.");
	if (1!=FWRITE_FLOAT(azimuth))			// 4
		return showErrorReturnFalse("Writing env azimuth error.");

	unsigned short tag_ss_dirs = NOX_BIN_ENV_DIRECTIONS;
	if (1!=FWRITE_USHORT(tag_ss_dirs))		// 2
		return showErrorReturnFalse("Writing sunsky directions tag error.");
	float dex=vME.x,  dey=vME.y,  dez=-vME.z;
	float dnx=vMN.x,  dny=vMN.y,  dnz=-vMN.z;
	// north dir
	if (1!=FWRITE_FLOAT(dnx))				// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dny))				// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dnz))				// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	// east dir
	if (1!=FWRITE_FLOAT(dex))				// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dey))				// 4
		return showErrorReturnFalse("Writing sunsky directions error.");
	if (1!=FWRITE_FLOAT(dez))				// 4
		return showErrorReturnFalse("Writing sunsky directions error.");



	// CHECK POS
	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != envSize)
	{
		char errmsg[128];
		sprintf_s(errmsg, 128, "Export error. Environment chunk size is incorrect.\nShould be %llu instead of %llu bytes.", envSize, realSize);
		return showErrorReturnFalse(errmsg);
	}

	return true;
}

//=======================================================================================================================
