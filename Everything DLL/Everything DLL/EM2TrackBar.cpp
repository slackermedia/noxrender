#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <math.h>
#include "log.h"
#include "EM2Controls.h"
#include "raytracer.h"

extern HMODULE hDllModule;

//------------------------------------------------------------------------------------------------

EM2TrackBar::EM2TrackBar(HWND hWnd) 
{
	hwnd = hWnd;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;
	invert = false;
	
	bgGradient = 0;

	isHorizontal = true;
	allowFloat = false;

	minIntValue = 0;
	maxIntValue = 1000;
	intValue = 0;
	intDefaultValue = 0;

	minFloatValue = 0.0f;
	maxFloatValue = 1.0f;
	floatValue = 0.0f;
	floatDefaultValue = 0.0f;

	colSlU		= RGB(92,92,92);
	colSlUR		= RGB(44,44,44);
	colSlR		= RGB(25,25,25);
	colSlRD		= RGB(19,19,19);
	colSlD		= RGB(20,20,20);
	colSlDL		= RGB(29,29,29);
	colSlL		= RGB(63,63,63);
	colSlLU		= RGB(97,97,97);
	colSlC		= RGB(35,35,35);

	colBorder = RGB(0,0,0);
	alphaBorder = 56;


}

//------------------------------------------------------------------------------------------------

EM2TrackBar::~EM2TrackBar() 
{
	if (bgGradient)
		DeleteObject(bgGradient);
	bgGradient = 0;
}

//------------------------------------------------------------------------------------------------

void InitEM2TrackBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2TrackBar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2TrackBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2TrackBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//------------------------------------------------------------------------------------------------

EM2TrackBar * GetEM2TrackBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2TrackBar * emetb = (EM2TrackBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2TrackBar * emetb = (EM2TrackBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emetb;
}

//------------------------------------------------------------------------------------------------

void SetEM2TrackBarInstance(HWND hwnd, EM2TrackBar *emetb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emetb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emetb);
	#endif
}

//------------------------------------------------------------------------------------------------

LRESULT CALLBACK EM2TrackBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2TrackBar * emtb;
	emtb = GetEM2TrackBarInstance(hwnd);
	static int lastPosX;
	static int lastBarPos;

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2TrackBar * emtb1 = new EM2TrackBar(hwnd);
				SetEM2TrackBarInstance(hwnd, (EM2TrackBar *)emtb1);
			}
		break;
		case WM_SIZE:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				rect.left = 0;
				rect.top = 0;
				rect.right = (int)(short)LOWORD(lParam);
				rect.bottom = (int)(short)HIWORD(lParam);

				emtb->resize(&rect);
			}
			break;
		case WM_CLOSE:
			{
				return TRUE;
			}
			break;
		case WM_DESTROY:
			{
				EM2TrackBar * emtb1;
				emtb1 = GetEM2TrackBarInstance(hwnd);
				delete emtb1;
				SetEM2TrackBarInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
			{
				if (!emtb)
					break;

				RECT rect;
				PAINTSTRUCT ps;
				GetClientRect(hwnd, &rect);

				bool disabled = (((GetWindowLong(hwnd, GWL_STYLE)) & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);

				// draw background
				if (emtb->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer thdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emtb->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emtb->bgShiftX, emtb->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, OPAQUE);
				}


				if (emtb->bgGradient)
				{
					HDC thdc = CreateCompatibleDC(hdc);
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emtb->bgGradient);
					BitBlt(hdc, 1, 1, rect.right-2, rect.bottom-2, thdc, 0, 0, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}

				// slider pos
				int mpos, ppos;
				if (emtb->allowFloat)
				{
					mpos = (int)(emtb->maxFloatValue-emtb->minFloatValue*1000);
					ppos = (int)(emtb->floatValue-emtb->minFloatValue*1000);
				}
				else
				{
					ppos = emtb->intValue-emtb->minIntValue;
					mpos = emtb->maxIntValue-emtb->minIntValue;
				}
				int pos = 0;
				if (emtb->isHorizontal)
					pos = (int)((rect.right-5)*ppos/mpos) + 2;
				else
					pos = (int)((rect.bottom-5)*ppos/mpos) + 2;

				if (emtb->invert)
					if (emtb->isHorizontal)
						pos = (rect.right-5-(pos-2))+2;
					else
						pos = (rect.bottom-5-(pos-2))+2;

				// draw slider
				if (emtb->isHorizontal)
				{
					int yy = rect.bottom;
					HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlC));
					MoveToEx(hdc, pos, 2, NULL);
					LineTo(hdc, pos, yy-2);
					MoveToEx(hdc, pos, 1, NULL);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlU)));
					LineTo(hdc, pos+1, 1);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlUR)));
					LineTo(hdc, pos+1, 2);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlR)));
					LineTo(hdc, pos+1, yy-2);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlRD)));
					LineTo(hdc, pos, yy-2);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlD)));
					LineTo(hdc, pos-1, yy-2);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlDL)));
					LineTo(hdc, pos-1, yy-3);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlL)));
					LineTo(hdc, pos-1, 1);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlLU)));
					LineTo(hdc, pos, 1);
					DeleteObject(SelectObject(hdc, oldPen));
				}
				else
				{
					int xx = rect.right;
					HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlC));
					MoveToEx(hdc, 2, pos, NULL);
					LineTo(hdc, xx-2, pos);
					MoveToEx(hdc, 2, pos-1, NULL);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlU)));
					LineTo(hdc, xx-2, pos-1);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlUR)));
					LineTo(hdc, xx-2, pos);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlR)));
					LineTo(hdc, xx-2, pos+1);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlRD)));
					LineTo(hdc, xx-3, pos+1);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlD)));
					LineTo(hdc, 1, pos+1);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlDL)));
					LineTo(hdc, 1, pos);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlL)));
					LineTo(hdc, 1, pos-1);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emtb->colSlLU)));
					LineTo(hdc, 2, pos-1);
					DeleteObject(SelectObject(hdc, oldPen));
				}

				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);
						Gdiplus::Color cBorder = Gdiplus::Color(emtb->alphaBorder, GetRValue(emtb->colBorder), GetGValue(emtb->colBorder), GetBValue(emtb->colBorder));
						Gdiplus::Pen bpen(cBorder);
						graphics.DrawRectangle(&bpen, 0,0, rect.right-1, rect.bottom-1);
					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
					COLORREF colborder = getMixedColor(NGCOL_BG_MEDIUM, emtb->alphaBorder, emtb->colBorder);
					HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, colborder));
					Rectangle(hdc, 0,0, rect.right, rect.bottom);
					SelectObject(hdc, oldPen);
					SelectObject(hdc, oldBrush);
				}
				#endif


				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);

			}
			break;
		case WM_LBUTTONDOWN:
			{
				if (!GetCapture())
					SetCapture(hwnd);
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				emtb->posFromMouse(pt.x, pt.y);
				emtb->notifyParent();
			}
			break;
		case WM_LBUTTONUP:
			{
				RECT crect;
				GetClientRect(hwnd, &crect);
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				ReleaseCapture();
			}
			break;
		case WM_MOUSEMOVE:
			{
				RECT crect;
				GetClientRect(hwnd, &crect);
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				if (wParam&MK_LBUTTON)
				{
					emtb->posFromMouse(pt.x, pt.y);
					emtb->notifyParent();
				}

			}
			break;
		case WM_RBUTTONUP:
			{
				bool notify = false;
				if (emtb->allowFloat)
				{
					notify = (emtb->floatDefaultValue != emtb->floatValue);
					emtb->setFloatValue(emtb->floatDefaultValue);
				}
				else
				{
					notify = (emtb->intDefaultValue != emtb->intValue);
					emtb->setIntValue(emtb->intDefaultValue);
				}
				if (notify)
					emtb->notifyParent();
			}
			break;
		default:
			break;
		}

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//------------------------------------------------------------------------------------------------

void EM2TrackBar::notifyParent()
{
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), WM_HSCROLL), (LPARAM)hwnd);
}

//------------------------------------------------------------------------------------------------

void EM2TrackBar::resize(RECT * nrect)
{
}

//------------------------------------------------------------------------------------------------

void EM2TrackBar::setValuesToInt(int value, int minVal, int maxVal, int defaultValue)
{
	intDefaultValue = min(maxVal, max(minVal, defaultValue));
	minIntValue = minVal;
	maxIntValue = maxVal;
	setIntValue(value);
}

//------------------------------------------------------------------------------------------------

void EM2TrackBar::setValuesToFloat(float value, float minVal, float maxVal, float defaultValue)
{
	floatDefaultValue = min(maxVal, max(minVal, defaultValue));
	minFloatValue = minVal;
	maxFloatValue = maxVal;
	setFloatValue(value);
}

//------------------------------------------------------------------------------------------------

void EM2TrackBar::setIntValue(int value)
{
	intValue = min(maxIntValue, max(minIntValue, value));
	InvalidateRect(hwnd, NULL, false);
}

//------------------------------------------------------------------------------------------------

void EM2TrackBar::setFloatValue(float value)
{
	floatValue = min(maxFloatValue, max(minFloatValue, value));
	InvalidateRect(hwnd, NULL, false);
}

//------------------------------------------------------------------------------------------------

bool EM2TrackBar::createGradient(COLORREF col1, COLORREF col2)
{
	if (!hwnd)
		return false;

	RECT crect;
	GetClientRect(hwnd, &crect);
	int w = crect.right-2;
	int h = crect.bottom-2;

	unsigned char * buf = (unsigned char *)malloc(w*h*4);
	if (!buf)
		return false;

	int r1 = GetRValue(invert ? col2 : col1);
	int g1 = GetGValue(invert ? col2 : col1);
	int b1 = GetBValue(invert ? col2 : col1);
	int r2 = GetRValue(invert ? col1 : col2);
	int g2 = GetGValue(invert ? col1 : col2);
	int b2 = GetBValue(invert ? col1 : col2);

	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			unsigned char blend = (unsigned char)min(255,(isHorizontal ? max(0, x-1)*255/(w-3) : max(0,y-1)*255/(h-3)));
			unsigned char rblend = 255-blend;
			unsigned char r = r1 * rblend / 255 + r2 * blend / 255;
			unsigned char g = g1 * rblend / 255 + g2 * blend / 255;
			unsigned char b = b1 * rblend / 255 + b2 * blend / 255;
			buf[(y*w+x)*4+0] = b;
			buf[(y*w+x)*4+1] = g;
			buf[(y*w+x)*4+2] = r;
			buf[(y*w+x)*4+3] = 255;
		}

	HBITMAP hbmp = CreateBitmap(w,h, 1, 32, buf);
	if (hbmp)
	{
		if (bgGradient)
			DeleteObject(bgGradient);
		bgGradient = hbmp;
	}

	free(buf);

	return (hbmp!=0);
}

//------------------------------------------------------------------------------------------------

bool EM2TrackBar::createHueRainbowGradient()
{
	if (!hwnd)
		return false;

	RECT crect;
	GetClientRect(hwnd, &crect);
	int w = crect.right-2;
	int h = crect.bottom-2;

	unsigned char * buf = (unsigned char *)malloc(w*h*4);
	if (!buf)
		return false;

	ColorHSV hsv = ColorHSV(0, 1, 1);

	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			hsv.h = min(1.0f,(isHorizontal ? max(0, x-1)/(float)(w-3) : max(0,y-1)/(float)(h-3)));
			if (invert)
				hsv.h = 1.0f - hsv.h;
			Color4 col = hsv.toColor4();
			unsigned char r = (unsigned char )min(255.0f, max(0.0f, col.r*255.0f));
			unsigned char g = (unsigned char )min(255.0f, max(0.0f, col.g*255.0f));
			unsigned char b = (unsigned char )min(255.0f, max(0.0f, col.b*255.0f));
			buf[(y*w+x)*4+0] = b;
			buf[(y*w+x)*4+1] = g;
			buf[(y*w+x)*4+2] = r;
			buf[(y*w+x)*4+3] = 255;
		}

	HBITMAP hbmp = CreateBitmap(w,h, 1, 32, buf);
	if (hbmp)
	{
		if (bgGradient)
			DeleteObject(bgGradient);
		bgGradient = hbmp;
	}

	free(buf);

	return (hbmp!=0);
}

//------------------------------------------------------------------------------------------------

bool EM2TrackBar::createTemperatureGradient(int tempstart, int tempstop)
{
	if (!hwnd)
		return false;

	RECT crect;
	GetClientRect(hwnd, &crect);
	int w = crect.right-2;
	int h = crect.bottom-2;

	unsigned char * buf = (unsigned char *)malloc(w*h*4);
	if (!buf)
		return false;

	if (w<4  &&  isHorizontal)
		return false;
	if (h<4  &&  !isHorizontal)
		return false;

	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			int temperature = 1000;
			if (isHorizontal)
				temperature = tempstart + (tempstop-tempstart) * min(max(0, x-1), w-3) / (w-3);
			else
				temperature = tempstart + (tempstop-tempstart) * min(max(0, y-1), h-3) / (h-3);

			Color4 col = TemperatureColor::getGammaColor(temperature);
			unsigned char r = (unsigned char )min(255.0f, max(0.0f, col.r*255.0f));
			unsigned char g = (unsigned char )min(255.0f, max(0.0f, col.g*255.0f));
			unsigned char b = (unsigned char )min(255.0f, max(0.0f, col.b*255.0f));
			buf[(y*w+x)*4+0] = b;
			buf[(y*w+x)*4+1] = g;
			buf[(y*w+x)*4+2] = r;
			buf[(y*w+x)*4+3] = 255;
		}

	HBITMAP hbmp = CreateBitmap(w,h, 1, 32, buf);
	if (hbmp)
	{
		if (bgGradient)
			DeleteObject(bgGradient);
		bgGradient = hbmp;
	}

	free(buf);

	return (hbmp!=0);
}

//------------------------------------------------------------------------------------------------

bool EM2TrackBar::posFromMouse(int mx, int my)
{
	RECT crect;
	GetClientRect(hwnd, &crect);
	int m = 1;
	int p = 0;
	if (isHorizontal)
	{
		m = max(1,crect.right-5); 
		p = min(m, max(0, mx-2));
	}
	else
	{
		m = max(1,crect.bottom-5); 
		p = min(m, max(0, my-2));
	}
	if (invert)
		p = m-p;

	if (allowFloat)
		setFloatValue((float)p/(float)m*(maxFloatValue-minFloatValue)+minFloatValue);
	else
		setIntValue(((maxIntValue-minIntValue)*p-1+m)/m+minIntValue);
	InvalidateRect(hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------
