#include "resource.h"
#include "RendererMainWindow.h"
//#include "MaterialEditorMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>

extern HMODULE hDllModule;

//----------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK RendererMainWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		{
			RendererMainWindow::backgroundBrush = CreateSolidBrush(RendererMainWindow::theme.WindowBackground);
			#ifdef _WIN64
				SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG_PTR)(RendererMainWindow::backgroundBrush));
			#else
				SetClassLong(hWnd, GCL_HBRBACKGROUND, (LONG)(LONG_PTR)(RendererMainWindow::backgroundBrush));
			#endif

		}	// end WM_CREATE
		break;
	case WM_SIZE:
		{
			resizeMainBar();
			resizePictureView();
			resizeTabs();
			resizeFinal();
			resizeRegions();
			resizeMaterials();
			resizeOutputConf();
			resizeStatusBar();
			resizeCamera();
			resizeObjects();
			resizeBlend();
			resizePost();
			resizeEnvironment();
		}
		break;
	case WM_CLOSE:
		{
			Raytracer * rtr = Raytracer::getInstance();
			if (rtr->curScenePtr->nowRendering)
			{
				int rres = MessageBox(hMain, "NOX is still rendering.\nClose NOX anyway?", "Warning", MB_YESNO|MB_ICONSTOP);
				if (rres==IDYES)
					DestroyWindow(hMain);
			}
			else
			{
				DestroyWindow(hMain);
			}
			return 0;
		}
		break;
	case WM_DESTROY:
		{
			Logger::add("Closing renderer main window. Bye :)");
			Logger::closeFile();
			PostQuitMessage(0);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case 345:	// tabs
				{
					tabsChanged();
				}
				break;
				case 32771:		// F5 accelerator
				{
					refreshRenderedImage();
				}
				break;
				case 32773:		// Ctrl+S accelerator
				{
					Logger::add("Ctrl+Shift+S pressed. Dialog opened.");
					char * filename = saveFileDialog(hMain, 
						"EXR high dynamic range image (*.exr)\0*.exr\0BMP image (*.bmp)\0*.bmp\0PNG image (*.png)\0*.png\0JPEG image (*.jpg)\0*.jpg\0TIFF image (*.tiff)\0*.tiff\0", 
						"Save image", "png", 3);
					saveImage(filename);
				}
				break;
				
				case IDC_FINAL_IMAGE:	// PVIEW
				{
					if (wmEvent == PV_CTRL_C)
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						HBITMAP hBMP = empv->getRenderHBitmap();
						if (hBMP == 0)
							break;

						OpenClipboard(hImage);
						EmptyClipboard();
						SetClipboardData(CF_BITMAP, hBMP);
						CloseClipboard();

						DeleteObject(hBMP);
					}
					if (wmEvent == PV_MIDDLECLICKED)
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						empv->zoom100();
					}
					if (wmEvent == PV_PIXEL_PICKED)
					{
						EMTabs * emt = GetEMTabsInstance(hTabs);
						if (emt->selected == POSFINAL)
						{
							EMPView * empv = GetEMPViewInstance(hImage);
							float px, py;
							empv->evalControlPosToImagePos(empv->ppx, empv->ppy, px, py);

							SendMessage(hFinal, PV_PIXEL_PICKED, (WPARAM&)px, (LPARAM&)py);
						}
					}
					if (wmEvent == PV_DROPPED_FILE)
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						if (empv->lastDroppedFilename)
						{
							if (loadSceneAsync(empv->lastDroppedFilename, false))
							{
							}
							else
							{
								MessageBox(hMain, "Failed opening scene. Probably not a nox scene file.", "Error", 0);
							}
						}
					}
				}
				break;
			}
		}
		break;
	case WM_MOUSEMOVE:
		{
			EMStatusBar * ems = GetEMStatusBarInstance(hStatus2);
			POINT pos;
			pos.x = (short)LOWORD(lParam); 
			pos.y = (short)HIWORD(lParam); 
			ClientToScreen(hWnd, &pos);
			ScreenToClient(hImage, &pos);
			if (pos.x < 0 || pos.y < 0)
			{
				ems->setCaption("");
				break;
			}
			EMPView * empv = GetEMPViewInstance(hImage);
			Color4 c;
			Camera * cam = NULL;
			Scene * sc = Raytracer::getInstance()->curScenePtr;
			if (sc)
				if (sc->cameras.objCount > 0)
					cam = sc->getActiveCamera();
			int aa = 1;
			if (cam)
				aa = cam->aa;
			int ix, iy;
			if (!empv->getColor(pos.x, pos.y, false, c, ix, iy, aa))
			{
				ems->setCaption("");
				break;
			}
			char buff[128];
			sprintf_s(buff, 128, "%d x %d - RGB: %.4f  %.4f  %.4f", ix, iy, c.r, c.g, c.b);
			ems->setCaption(buff);
		}
		break;
	case WM_COPYDATA:
		{
			if (!lParam)
				break;
			COPYDATASTRUCT * cds = (COPYDATASTRUCT*)lParam;
			switch (cds->dwData)
			{
				case WN_LOADSCENE:
				case WN_LOADSCENE_AND_RUN:
					{
						char * tfname = NULL;
						char * ptr = (char*)cds->lpData;
						tfname = (char*)malloc(cds->cbData);
						sprintf_s(tfname, cds->cbData, "%s", ptr);
						if (!PathFileExists(tfname))
							break;
						loadSceneAsync(tfname, (cds->dwData==WN_LOADSCENE_AND_RUN) );
					}
					break;
				case WN_LOAD_POST:
					{
						Logger::add("load post message");
						Raytracer * rtr = Raytracer::getInstance();
						Scene * sc = rtr->curScenePtr;
						sc->postLoadPostFile = (char*)malloc(cds->cbData);
						sprintf_s(sc->postLoadPostFile, cds->cbData, "%s", (char*)cds->lpData);
						Logger::add(sc->postLoadPostFile);
					}
					break;
				case WN_LOAD_BLEND:
					{
						Logger::add("load blend message");
						Raytracer * rtr = Raytracer::getInstance();
						Scene * sc = rtr->curScenePtr;
						sc->postLoadBlendFile = (char*)malloc(cds->cbData);
						sprintf_s(sc->postLoadBlendFile, cds->cbData, "%s", (char*)cds->lpData);
						Logger::add(sc->postLoadBlendFile);
					}
					break;
				case WN_LOAD_FINAL:
					{
						Logger::add("load final message");
						Raytracer * rtr = Raytracer::getInstance();
						Scene * sc = rtr->curScenePtr;
						sc->postLoadFinalFile = (char*)malloc(cds->cbData);
						sprintf_s(sc->postLoadFinalFile, cds->cbData, "%s", (char*)cds->lpData);
						Logger::add(sc->postLoadFinalFile);
					}
					break;
			}
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	return 0;
}



//----------------------------------------------------------------------------------------------------------------

