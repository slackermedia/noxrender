#include "raytracer.h"
#include "Colors.h"

//---------------------------------------------------------------------

PresetPost::PresetPost()
{
	name = NULL;
	texGlareObstacle = NULL;
	texGlareDiaphragm = NULL;
}

//---------------------------------------------------------------------

PresetPost::~PresetPost()
{
	if (name)
		free(name);
	if (texGlareObstacle)
		free(texGlareObstacle);
	if (texGlareDiaphragm)
		free(texGlareDiaphragm);
	name = NULL;
	texGlareObstacle = NULL;
	texGlareDiaphragm = NULL;
}

//---------------------------------------------------------------------

bool Scene::presetAddCurrent(char * presetname)
{
	PresetPost * pr = new PresetPost();
	Camera * cam = getActiveCamera();
	CHECK(cam);

	if (presetname  &&  strlen(presetname)>0)
		pr->name = copyString(presetname);
	else
		pr->name = copyString("Unnamed");

	pr->blend.copyFrom(&blendSettings);
	pr->iMod.copyDataFromAnother(&cam->iMod);
	pr->fMod.copyDataFromOther(&cam->fMod, true, true);

	pr->texGlareDiaphragm = copyString(cam->texDiaphragm.fullfilename);
	pr->texGlareObstacle  = copyString(cam->texObstacle.fullfilename);
	
	presets.add(pr);
	presets.createArray();
	return true;
}

//---------------------------------------------------------------------

bool Scene::presetApply(int id)
{
	if (id<0  ||  id>=presets.objCount)
		return false;
	Camera * cam = getActiveCamera();
	CHECK(cam);

	PresetPost * pr = presets[id];
	if (!pr)
		return false;

	blendSettings.copyFrom(&pr->blend);
	cam->fMod.copyDataFromOther(&pr->fMod, true, true);
	cam->iMod.copyDataFromAnother(&pr->iMod);

	cam->texDiaphragm.freeAllBuffers();
	cam->texObstacle.freeAllBuffers();
	if (pr->texGlareDiaphragm)
		cam->texDiaphragm.loadFromFile(pr->texGlareDiaphragm);
	if (pr->texGlareObstacle)
		cam->texObstacle.loadFromFile(pr->texGlareObstacle);

	return true;
}

//---------------------------------------------------------------------

bool Scene::presetRemove(int id)
{
	if (id<0  ||  id>=presets.objCount)
		return false;
	PresetPost * pr = presets[id];
	if (pr)
		delete pr;
	presets.removeElement(id);

	return true;
}

//---------------------------------------------------------------------

