#ifndef __embree_h__
#define __embree_h__

#ifdef _WIN64
	#define USE_EMBREE
#endif


#include "raytracer.h"

//---------------------------------------------------------------------

void initEmbree();
void releaseEmbree();
bool embreeSystemMinimum();

float intersectEmbree(Scene * scene, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instID, int &tri, float &u, float &v);

bool createEmbreeScene(Scene * sc);
bool createEmbreeSceneInstances(Scene * sc);


//---------------------------------------------------------------------

#endif
