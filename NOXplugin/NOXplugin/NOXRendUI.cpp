#include "max2013predef.h"
#include "NOXRenderer.h"
#include "NOXplugin.h"
#include "3dsmaxport.h"
#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"

static INT_PTR CALLBACK NOXRendParamsDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
char * chooseFileToExportDialog(HWND hwnd, char * filename);
char * chooseFilePostDialog(HWND hwnd, char * filename);
char * chooseFileBlendDialog(HWND hwnd, char * filename);
char * chooseFileFinalDialog(HWND hwnd, char * filename);
HWND NOXRendParamDlg::hPanel = 0;
HWND NOXRendParamDlg::hRPanel = 0;

NOXRendParamDlg::NOXRendParamDlg(NOXRenderer *r,IRendParams *i,BOOL prog, HINSTANCE hInstance)
{
	rend = r;
	ir = i;
	this->prog = prog;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	if (prog) 
	{
		hRPanel = ir->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_NOX_REND_DIALOG),
			NOXRendParamsDlgProc,
			_T("NOX Renderer"),
			(LPARAM)this);
	} 
	else 
	{
		hPanel = ir->AddTabRollupPage(
			NOXRend_Tab_Class_ID,
			hInstance, 
			MAKEINTRESOURCE(IDD_NOX_PREREND_DIALOG),
			NOXRendParamsDlgProc,
			_T("NOX Renderer"),
			(LPARAM)this);
	}
}

NOXRendParamDlg::~NOXRendParamDlg()
{
	DeleteObject(hFont);
	if (hPanel)
		ir->DeleteRollupPage(hPanel);
	if (hRPanel)
		ir->DeleteRollupPage(hRPanel);
}

void NOXRendParamDlg::AcceptParams()
{
}

void NOXRendParamDlg::RejectParams()
{
}

void NOXRendParamDlg::InitParamDialog(HWND hWnd)
{
	HWND hFilename = GetDlgItem(hWnd, IDC_NOX_REND_FILENAME);
	HWND hSceneName = GetDlgItem(hWnd, IDC_NOX_REND_SCENE_NAME);
	HWND hAuthorName = GetDlgItem(hWnd, IDC_NOX_REND_AUTHOR);
	HWND hEmail = GetDlgItem(hWnd, IDC_NOX_REND_EMAIL);
	HWND hWWW = GetDlgItem(hWnd, IDC_NOX_REND_WWW);
	TCHAR * temptchar = NULL;

	// scene name
	temptchar = copyAnsiToTchar1(rend->scene_name ? rend->scene_name : "");
	SetWindowText(hSceneName, temptchar);
	free(temptchar);

	// author
	temptchar = copyAnsiToTchar1(rend->scene_author_name ? rend->scene_author_name : "");
	SetWindowText(hAuthorName, temptchar);
	free(temptchar);

	// email
	temptchar = copyAnsiToTchar1(rend->scene_author_email ? rend->scene_author_email : "");
	SetWindowText(hEmail, temptchar);
	free(temptchar);

	// www
	temptchar = copyAnsiToTchar1(rend->scene_author_www ? rend->scene_author_www : "");
	SetWindowText(hWWW, temptchar);
	free(temptchar);

	// filename
	if (!rend->filename)
		rend->generateFilename();
	temptchar = copyAnsiToTchar1(rend->filename ? rend->filename : "");
	SetWindowText(hFilename, temptchar);
	free(temptchar);

	HWND matButton = GetDlgItem(hWnd, IDC_NOX_REND_OVERRIDE_MATERIAL_BUTTON);
	HWND matOverEnabled = GetDlgItem(hWnd, IDC_NOX_REND_OVERRIDE_MATERIAL_ENABLED);
	if (rend->overrideMaterial)
	{
		const TCHAR * name = rend->overrideMaterial->GetName();
		SetWindowText(matButton, name);
	}
	else
		SetWindowText(matButton, _T("None"));
	if (rend->overrideMatEnabled)
	{
		EnableWindow(matButton, TRUE);
		SendMessage(matOverEnabled, BM_SETCHECK, BST_CHECKED,   0);
	}
	else
	{
		EnableWindow(matButton, FALSE);
		SendMessage(matOverEnabled, BM_SETCHECK, BST_UNCHECKED,   0);
	}


	HWND hPostFilename = GetDlgItem(hWnd, IDC_NOX_REND_POST_FILENAME);
	HWND hUsePost = GetDlgItem(hWnd, IDC_NOX_REND_POST_ENABLE);
	HWND hBlendFilename = GetDlgItem(hWnd, IDC_NOX_REND_BLEND_FILENAME);
	HWND hUseBlend = GetDlgItem(hWnd, IDC_NOX_REND_BLEND_ENABLE);
	HWND hFinalFilename = GetDlgItem(hWnd, IDC_NOX_REND_FINAL_FILENAME);
	HWND hUseFinal = GetDlgItem(hWnd, IDC_NOX_REND_FINAL_ENABLE);

	SendMessage(hUsePost, BM_SETCHECK, rend->use_file_post ? BST_CHECKED : BST_UNCHECKED,   0);
	if (rend->filename_post)
	{
		temptchar = copyAnsiToTchar1(rend->filename_post);
		SetWindowText(hPostFilename, temptchar);
		free(temptchar);
	}
	else
		SetWindowText(hPostFilename, _T("none"));

	SendMessage(hUseBlend, BM_SETCHECK, rend->use_file_blend ? BST_CHECKED : BST_UNCHECKED,   0);
	if (rend->filename_blend)
	{
		temptchar = copyAnsiToTchar1(rend->filename_blend);
		SetWindowText(hBlendFilename, temptchar);
		free(temptchar);
	}
	else
		SetWindowText(hBlendFilename, _T("none"));

	SendMessage(hUseFinal, BM_SETCHECK, rend->use_file_final ? BST_CHECKED : BST_UNCHECKED,   0);
	if (rend->filename_final)
	{
		temptchar = copyAnsiToTchar1(rend->filename_final);
		SetWindowText(hFinalFilename, temptchar);
		free(temptchar);
	}
	else
		SetWindowText(hFinalFilename, _T("none"));

	HWND hMotionBlurOn = GetDlgItem(hWnd, IDC_NOX_REND_MB_ON);
	SendMessage(hMotionBlurOn, BM_SETCHECK, rend->mb_on ? BST_CHECKED : BST_UNCHECKED,   0);
	TCHAR buf[64];
	_stprintf_s(buf, 64, _T("%.3f"), rend->mb_duration);
	HWND hMotionBlurDuration = GetDlgItem(hWnd, IDC_NOX_REND_MB_NUM_FRAMES);
	SetWindowText(hMotionBlurDuration, buf);
	EnableWindow(hMotionBlurDuration, rend->mb_on ? TRUE : FALSE);


	HWND hComboEngine = GetDlgItem(hWnd, IDC_NOX_REND_ENGINE);
	SendMessage(hComboEngine, CB_RESETCONTENT, 0,0);
	SendMessage(hComboEngine, CB_ADDSTRING,0,(LPARAM)_T("Path Tracing"));
	SendMessage(hComboEngine, CB_ADDSTRING,0,(LPARAM)_T("Bidirectional Path Tracing"));
	SendMessage(hComboEngine, CB_ADDSTRING,0,(LPARAM)_T("Intersections"));
	SendMessage(hComboEngine, CB_SETCURSEL, (WPARAM)rend->engine, 0);

}

void NOXRendParamDlg::InitProgDialog(HWND hWnd)
{
}


static INT_PTR CALLBACK NOXRendParamsDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	NOXRendParamDlg *dlg = DLGetWindowLongPtr<NOXRendParamDlg*>(hWnd);
	switch (msg) 
	{
	case WM_INITDIALOG:
		dlg = (NOXRendParamDlg*)lParam;
		DLSetWindowLongPtr(hWnd, lParam);
		if (dlg) 
		{
			if (dlg->prog)
			   dlg->InitProgDialog(hWnd);
			else
			   dlg->InitParamDialog(hWnd);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_NOX_REND_CHFILENAME:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						char * fname = chooseFileToExportDialog(hWnd, dlg->rend->filename);
						if (fname)
						{
							char * nfname = (char*)malloc(strlen(fname)+1);
							sprintf_s(nfname, strlen(fname)+1, "%s", fname);
							if (dlg->rend->filename)
								free(dlg->rend->filename);
							dlg->rend->filename = nfname;
							TCHAR * temptchar = copyAnsiToTchar1(dlg->rend->filename);
							SetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_FILENAME), temptchar);
							free(temptchar);
						}

					}
				break;
				case IDC_NOX_REND_BLEND_NAMES:
					if (wmEvent==BN_CLICKED)
					{
						TrMaterial tr;
						tr.runBlendLayersDialog(hWnd);
					}
				break;
				case IDC_NOX_REND_SCENE_NAME:
					{
						if (wmEvent==EN_SETFOCUS)
						{
							DisableAccelerators();
							break;
						}
						if (wmEvent==EN_KILLFOCUS)
						{
							EnableAccelerators();

							TCHAR tempstr[2048];
							int l = GetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_SCENE_NAME), tempstr, 2048);
							char * newstr = NULL;
							if (l>0)
								newstr = getAnsiCopy1(tempstr);
							if (dlg->rend->scene_name)
								free(dlg->rend->scene_name);
							dlg->rend->scene_name = newstr;
							break;
						}
					}
				break;
				case IDC_NOX_REND_AUTHOR:
					{
						if (wmEvent==EN_SETFOCUS)
						{
							DisableAccelerators();
							break;
						}
						if (wmEvent==EN_KILLFOCUS)
						{
							EnableAccelerators();

							TCHAR tempstr[2048];
							int l = GetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_AUTHOR), tempstr, 2048);
							char * newstr = NULL;
							if (l>0)
								newstr = getAnsiCopy1(tempstr);
							if (dlg->rend->scene_author_name)
								free(dlg->rend->scene_author_name);
							dlg->rend->scene_author_name = newstr;
							break;
						}
					}
				break;
				case IDC_NOX_REND_EMAIL:
					{
						if (wmEvent==EN_SETFOCUS)
						{
							DisableAccelerators();
							break;
						}
						if (wmEvent==EN_KILLFOCUS)
						{
							EnableAccelerators();

							TCHAR tempstr[2048];
							int l = GetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_EMAIL), tempstr, 2048);
							char * newstr = NULL;
							if (l>0)
								newstr = getAnsiCopy1(tempstr);
							if (dlg->rend->scene_author_email)
								free(dlg->rend->scene_author_email);
							dlg->rend->scene_author_email = newstr;
							break;
						}
					}
				break;
				case IDC_NOX_REND_WWW:
					{
						if (wmEvent==EN_SETFOCUS)
						{
							DisableAccelerators();
							break;
						}
						if (wmEvent==EN_KILLFOCUS)
						{
							EnableAccelerators();

							TCHAR tempstr[2048];
							int l = GetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_WWW), tempstr, 2048);
							char * newstr = NULL;
							if (l>0)
								newstr = getAnsiCopy1(tempstr);
							if (dlg->rend->scene_author_www)
								free(dlg->rend->scene_author_www);
							dlg->rend->scene_author_www = newstr;
							break;
						}
					}
				break;
				case IDC_NOX_REND_OVERRIDE_MATERIAL_BUTTON:
					{
						BOOL newMat = FALSE;
						BOOL cancel = FALSE;
						MtlBase * mat = GetCOREInterface()->DoMaterialBrowseDlg(hWnd, BROWSE_MATSONLY | BROWSE_INCNONE, newMat, cancel);

						HWND matButton = GetDlgItem(hWnd, IDC_NOX_REND_OVERRIDE_MATERIAL_BUTTON);
						if (cancel == FALSE)
						{
							Mtl * mtl = dynamic_cast<Mtl*>(mat);
							dlg->rend->overrideMaterial = mtl;
							if (dlg->rend->overrideMaterial)
							{
								const TCHAR * name = dlg->rend->overrideMaterial->GetName();
								SetWindowText(matButton, name);
							}
							else
								SetWindowText(matButton, _T("None"));
						}
					}
					break;
				case IDC_NOX_REND_OVERRIDE_MATERIAL_ENABLED:
					{
						HWND matButton = GetDlgItem(hWnd, IDC_NOX_REND_OVERRIDE_MATERIAL_BUTTON);
						HWND matOverEnabled = GetDlgItem(hWnd, IDC_NOX_REND_OVERRIDE_MATERIAL_ENABLED);
						dlg->rend->overrideMatEnabled = (BST_CHECKED == SendMessage(matOverEnabled, BM_GETCHECK, 0, 0));
						if (dlg->rend->overrideMatEnabled)
							EnableWindow(matButton, TRUE);
						else
							EnableWindow(matButton, FALSE);
					}
					break;
				case IDC_NOX_REND_POST_CHFILENAME:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						char * fname = chooseFilePostDialog(hWnd, dlg->rend->filename_post);
						if (fname)
						{
							char * nfname = (char*)malloc(strlen(fname)+1);
							sprintf_s(nfname, strlen(fname)+1, "%s", fname);
							if (dlg->rend->filename_post)
								free(dlg->rend->filename_post);
							dlg->rend->filename_post = nfname;
							TCHAR * temptchar = copyAnsiToTchar1(dlg->rend->filename_post);
							SetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_POST_FILENAME), temptchar);
							free(temptchar);
							SendMessage(GetDlgItem(hWnd, IDC_NOX_REND_POST_ENABLE), BM_SETCHECK, BST_CHECKED,   0);
							dlg->rend->use_file_post = true;
						}
					}
				break;
				case IDC_NOX_REND_BLEND_CHFILENAME:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						char * fname = chooseFileBlendDialog(hWnd, dlg->rend->filename_blend);
						if (fname)
						{
							char * nfname = (char*)malloc(strlen(fname)+1);
							sprintf_s(nfname, strlen(fname)+1, "%s", fname);
							if (dlg->rend->filename_blend)
								free(dlg->rend->filename_blend);
							dlg->rend->filename_blend = nfname;
							TCHAR * temptchar = copyAnsiToTchar1(dlg->rend->filename_blend);
							SetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_BLEND_FILENAME), temptchar);
							free(temptchar);
							SendMessage(GetDlgItem(hWnd, IDC_NOX_REND_BLEND_ENABLE), BM_SETCHECK, BST_CHECKED,   0);
							dlg->rend->use_file_blend = true;
						}
					}
				break;
				case IDC_NOX_REND_FINAL_CHFILENAME:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						char * fname = chooseFileFinalDialog(hWnd, dlg->rend->filename_final);
						if (fname)
						{
							char * nfname = (char*)malloc(strlen(fname)+1);
							sprintf_s(nfname, strlen(fname)+1, "%s", fname);
							if (dlg->rend->filename_final)
								free(dlg->rend->filename_final);
							dlg->rend->filename_final = nfname;
							TCHAR * temptchar = copyAnsiToTchar1(dlg->rend->filename_final);
							SetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_FINAL_FILENAME), temptchar);
							free(temptchar);
							SendMessage(GetDlgItem(hWnd, IDC_NOX_REND_FINAL_ENABLE), BM_SETCHECK, BST_CHECKED,   0);
							dlg->rend->use_file_final = true;
						}
					}
				break;
				case IDC_NOX_REND_POST_ENABLE:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						dlg->rend->use_file_post = (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_NOX_REND_POST_ENABLE), BM_GETCHECK, 0, 0));
					}
				break;
				case IDC_NOX_REND_BLEND_ENABLE:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						dlg->rend->use_file_blend = (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_NOX_REND_BLEND_ENABLE), BM_GETCHECK, 0, 0));
					}
				break;
				case IDC_NOX_REND_FINAL_ENABLE:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						dlg->rend->use_file_final = (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_NOX_REND_FINAL_ENABLE), BM_GETCHECK, 0, 0));
					}
				break;
				case IDC_NOX_REND_MB_ON:
					if (wmEvent==BN_CLICKED  &&  !dlg->prog)
					{
						dlg->rend->mb_on = (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_NOX_REND_MB_ON), BM_GETCHECK, 0, 0));
						HWND hMotionBlurDuration = GetDlgItem(hWnd, IDC_NOX_REND_MB_NUM_FRAMES);
						EnableWindow(hMotionBlurDuration, dlg->rend->mb_on ? TRUE : FALSE);
					}
				break;
				case IDC_NOX_REND_MB_NUM_FRAMES:
					{
						if (wmEvent==EN_SETFOCUS)
						{
							DisableAccelerators();
							break;
						}
						if (wmEvent==EN_KILLFOCUS)
						{
							EnableAccelerators();

							TCHAR tempstr[2048];
							int l = GetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_MB_NUM_FRAMES), tempstr, 2048);
							char * newstr = NULL;
							if (l>0)
							{
								newstr = getAnsiCopy1(tempstr);
							}
							float minshutter = 0.0f;
							float maxshutter = 100.0f;

							char * addr;
							float a = (float)strtod(newstr, &addr);
							if (addr == newstr)
							{	// conversion error (to float)
							}
							else
							{
								dlg->rend->mb_duration = min(maxshutter, max(minshutter, a));
							}

							TCHAR buf[64];
							_stprintf_s(buf, 64, _T("%.3f"), dlg->rend->mb_duration);
							SetWindowText(GetDlgItem(hWnd, IDC_NOX_REND_MB_NUM_FRAMES), buf);
						}
					}
				break;
				case IDC_NOX_REND_ENGINE:
					{
						if (wmEvent==CBN_SELCHANGE)
						{
							HWND hCombo = GetDlgItem(hWnd, IDC_NOX_REND_ENGINE);
							dlg->rend->engine = (int)SendMessage(hCombo, CB_GETCURSEL, 0, 0);
						}
					}
				break;

			}
		}	// END WM_COMMAND
		break;
	case WM_DESTROY:
		break;      
	default:
		return FALSE;
	}  
	return TRUE;
}


char * chooseFileToExportDialog(HWND hwnd, char * filename)
{
	TCHAR fname[1024];
	_stprintf_s(fname, 1024, _T(""));
	if (filename)
	{
		TCHAR * cpfn = copyAnsiToTchar1(filename);
		_stprintf_s(fname, 1024, _T("%s"), cpfn);
		free(cpfn);
	}

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hInstance;
	ofn.lpstrFilter = _T("Scene file (*.nox)\0*.nox\0All files\0*.*\0\0");
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = fname;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = _T("");
	ofn.lpstrTitle = _T("Choose scene export file name");
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = _T("nox");
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST;

	bool res = (GetSaveFileName(&ofn) == TRUE);
	if (res)
		return getAnsiCopy1(ofn.lpstrFile);
	
	return NULL;
}

char * chooseFilePostDialog(HWND hwnd, char * filename)
{
	TCHAR fname[1024];
	_stprintf_s(fname, 1024, _T(""));
	if (filename)
	{
		TCHAR * cpfn = copyAnsiToTchar1(filename);
		_stprintf_s(fname, 1024, _T("%s"), cpfn);
		free(cpfn);
	}

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hInstance;
	ofn.lpstrFilter = _T("Post settings file (*.nxp)\0*.nxp\0All files\0*.*\0\0");
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = fname;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = _T("");
	ofn.lpstrTitle = _T("Choose post settings file");
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = _T("nxp");
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST;

	bool res = (GetSaveFileName(&ofn) == TRUE);
	if (res)
		return getAnsiCopy1(ofn.lpstrFile);
	
	return NULL;
}

char * chooseFileBlendDialog(HWND hwnd, char * filename)
{
	TCHAR fname[1024];
	_stprintf_s(fname, 1024, _T(""));
	if (filename)
	{
		TCHAR * cpfn = copyAnsiToTchar1(filename);
		_stprintf_s(fname, 1024, _T("%s"), cpfn);
		free(cpfn);
	}

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hInstance;
	ofn.lpstrFilter = _T("Blend settings file (*.nxb)\0*.nxb\0All files\0*.*\0\0");
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = fname;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = _T("");
	ofn.lpstrTitle = _T("Choose blend settings file");
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = _T("nxb");
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST;

	bool res = (GetSaveFileName(&ofn) == TRUE);
	if (res)
		return getAnsiCopy1(ofn.lpstrFile);

	return NULL;
}

char * chooseFileFinalDialog(HWND hwnd, char * filename)
{
	TCHAR fname[1024];
	_stprintf_s(fname, 1024, _T(""));
	if (filename)
	{
		TCHAR * cpfn = copyAnsiToTchar1(filename);
		_stprintf_s(fname, 1024, _T("%s"), cpfn);
		free(cpfn);
	}

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.hInstance = hInstance;
	ofn.lpstrFilter = _T("Final settings file (*.nxf)\0*.nxf\0All files\0*.*\0\0");
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = fname;
	ofn.nMaxFile = 1024;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = _T("");
	ofn.lpstrTitle = _T("Choose final settings file");
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = _T("nxf");
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST;

	bool res = (GetSaveFileName(&ofn) == TRUE);
	if (res)
		return getAnsiCopy1(ofn.lpstrFile);
	
	return NULL;
}

void NOXRenderer::generateFilename(bool isXML)
{
	#ifdef ISMAX2013
		const TCHAR * tpname = GetCOREInterface()->GetCurFileName().ToMCHAR();
	#else
		const TCHAR * tpname = GetCOREInterface()->GetCurFileName();
	#endif
	char * pname = getAnsiCopy1(tpname);
	if (pname)
	{
		int l = (int)strlen(pname);
		if (l>4)
		{
			if (	(pname[l-4]=='.'                 ) &&  (pname[l-3]=='m'||pname[l-3]=='M') &&  
					(pname[l-2]=='a'||pname[l-2]=='A') &&  (pname[l-1]=='x'||pname[l-1]=='X') )
				pname[l-4] = 0;
		}
		else
			pname = copyStringMax1("default");
	}
	else
	{
		pname = copyStringMax1("default");
	}

	TCHAR * rdir = getNoxUserDirectoryFromReg();
	if (!rdir)
		rdir = getNoxUserDirectory1();

	if (!rdir)
		return;

	char * rdir_ansi = getAnsiCopy1(rdir);

	int l = (int)strlen(rdir_ansi) + (int)strlen(pname) + 128;
	char * res = (char *)malloc(l);
	SYSTEMTIME st;
	GetLocalTime(&st);

	if (isXML)
		sprintf_s(res, l, "%s\\exported scenes\\%s_%d_%d_%d__%d.%d.%d.xml", rdir_ansi, pname,
					st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	else
		sprintf_s(res, l, "%s\\exported scenes\\%s_%d_%d_%d__%d.%d.%d.nox", rdir_ansi, pname,
					st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	free(rdir);
	free(rdir_ansi);

	if (filename)
		free(filename);
	filename = res;
}