#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

// old gui version

extern HMODULE hDllModule;

INT_PTR CALLBACK SceneInfoDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hLogoBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (!bgBrush)
					bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				
				EMButton * embOK  = GetEMButtonInstance(GetDlgItem(hWnd, IDC_SI_OK));
				EMButton * embMem = GetEMButtonInstance(GetDlgItem(hWnd, IDC_SI_MEM));

				GlobalWindowSettings::colorSchemes.apply(embOK);
				GlobalWindowSettings::colorSchemes.apply(embMem);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TEXT1)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TEXT2)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TEXT3)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TEXT4)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TEXT5)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TEXT6)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TEXT7)), true);

				EMText * emtSceneName	= GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_SCENE_NAME));
				EMText * emtObjects		= GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_OBJECTS));
				EMText * emtTriangles	= GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_TRIANGLES));
				EMText * emtMaterials	= GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_MATERIALS));
				EMText * emtAuthor		= GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_AUTHOR));
				EMText * emtEmail		= GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_EMAIL));
				EMText * emtWWW			= GetEMTextInstance(GetDlgItem(hWnd, IDC_SI_WWW));

				GlobalWindowSettings::colorSchemes.apply(emtSceneName	, true);
				GlobalWindowSettings::colorSchemes.apply(emtObjects		, true);
				GlobalWindowSettings::colorSchemes.apply(emtTriangles	, true);
				GlobalWindowSettings::colorSchemes.apply(emtMaterials	, true);
				GlobalWindowSettings::colorSchemes.apply(emtAuthor		, true);
				GlobalWindowSettings::colorSchemes.apply(emtEmail		, true);
				GlobalWindowSettings::colorSchemes.apply(emtWWW			, true);

				Raytracer * rtr = Raytracer::getInstance();
				Scene * sc = rtr->curScenePtr;
				if (!sc)
					break;

				char buf[512];
				emtSceneName->changeCaption(sc->scene_name			? sc->scene_name			: "Unnamed");
				emtAuthor->changeCaption(	sc->scene_author_name	? sc->scene_author_name		: "");
				emtEmail->changeCaption(	sc->scene_author_email	? sc->scene_author_email	: "");
				emtWWW->changeCaption(		sc->scene_author_www	? sc->scene_author_www		: "");

				sprintf_s(buf, 512, "%d", sc->meshes.objCount);
				emtObjects->changeCaption(buf);
				sprintf_s(buf, 512, "%d", sc->triangles.objCount);
				emtTriangles->changeCaption(buf);
				sprintf_s(buf, 512, "%d", sc->mats.objCount);
				emtMaterials->changeCaption(buf);

				if (sc->scene_author_www)
				{
					int ll = (int)strlen(sc->scene_author_www);
					if (ll>0)
					{
						bool thereisHttp = false;
						if (ll > 6)
						{
							char * w = sc->scene_author_www;
							if ( (w[0]=='h' || w[0]=='H')	&&
								 (w[1]=='t' || w[1]=='T')	&&
								 (w[2]=='t' || w[2]=='T')	&&
								 (w[3]=='p' || w[3]=='P')	&&
								 (w[4]==':')	&&
								 (w[5]=='/')	&&
								 (w[6]=='/'))
								 thereisHttp = true;
						}
						if (!thereisHttp)
						{
							char * tmpstr = (char*)malloc(ll+10);
							if (tmpstr)
							{
								sprintf_s(tmpstr, ll+10, "http://%s", sc->scene_author_www);
								emtWWW->setHyperLink(tmpstr);
								free(tmpstr);
							}
						}
						else
							emtWWW->setHyperLink(sc->scene_author_www);
					}
				}

				if (sc->scene_author_email)
				{
					int ll = (int)strlen(sc->scene_author_email);
					if (ll>0)
					{
						bool thereisMailto = false;
						if (ll > 6)
						{
							char * w = sc->scene_author_email;
							if ( (w[0]=='m' || w[0]=='M')	&&
								 (w[1]=='a' || w[1]=='A')	&&
								 (w[2]=='i' || w[2]=='I')	&&
								 (w[3]=='l' || w[3]=='L')	&&
								 (w[4]=='t' || w[4]=='T')	&&
								 (w[5]=='o' || w[5]=='O')	&&
								 (w[6]==':'))
								 thereisMailto = true;
						}
						if (!thereisMailto)
						{
							char * tmpstr = (char*)malloc(ll+10);
							if (tmpstr)
							{
								sprintf_s(tmpstr, ll+10, "mailto:%s", sc->scene_author_email);
								emtEmail->setHyperLink(tmpstr);
								free(tmpstr);
							}
						}
						else
							emtEmail->setHyperLink(sc->scene_author_email);
					}
				}
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_SI_OK:
						{
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;
					case IDC_SI_MEM:
						{
							int res = (int)DialogBoxParam(hDllModule,
									MAKEINTRESOURCE(IDD_MEMORY_USAGE_DIALOG), hWnd, MemUsageDlgProc,(LPARAM)(0));
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				if ((HWND)lParam == GetDlgItem(hWnd, IDC_ABOUT_TEST_TEXT))
					SetTextColor(hdc1, RGB(0,160,210));
				else
					SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

