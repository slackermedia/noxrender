#include <windows.h>
#include <math.h>
#include <gdiplus.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "QuasiRandom.h"
#include "resource.h"
#include "RendererMainWindow.h"
#include "log.h"

#define OPENEXR_DLL
#include <ImathBox.h>
#include <half.h>
#include <ImfCRgbaFile.h>
#include <ImfRgbaFile.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfLut.h>
#include <ImfInputFile.h>
#include <ImfTiledInputFile.h>
#include <ImfPreviewImage.h>
#include <ImfChannelList.h>
#include <Iex.h>

#ifndef max
#define max(a, b)  (((a) > (b)) ? (a) : (b)) 
#endif
#ifndef min
#define min(a, b)  (((a) > (b)) ? (b) : (a)) 
#endif

extern HMODULE hDllModule;

//----------------------------------------------------------------------------------------------------------------------------------

const float ImageModifier::GAMMA_MIN_VALUE = 1.0f;
const float ImageModifier::GAMMA_MAX_VALUE = 4.0f;
const float ImageModifier::TONE_MAPPING_MIN_VALUE = 0.0f;
const float ImageModifier::TONE_MAPPING_MAX_VALUE = 1.0f;
const float ImageModifier::MULTIPLY_MIN_VALUE = 0.000001f;
const float ImageModifier::MULTIPLY_MAX_VALUE = 1000000.0f;

ImageModifier::ImageModifier()
{
	multiplier = 1.0f;
	gammaValue = 2.2f;
	toneMappingValue = 0.85f;
	brightness = 0.0f;
	contrast = 1.0f;
	saturation = 0.0f;
	addR = addG = addB = 0.0f;
	temperature = 6000;
	cbCRed = true;
	cbCGreen = true;
	cbCBlue = true;
	cbCLuminance = false;
	filter = 2;
	vignette = 0;
	response_function = RESPONSE_NUM_DSCS315_1;
	iso_ev_compensation = 0;
	iso_camera = 100;
	gi_compensation = 0.0f;
	dotsEnabled = false;
	dotsRadius = 1;
	dotsThreshold = 5.0f;
	dotsDensity = 1;
}

ImageModifier::ImageModifier(float gamma, float tone, float multiply)
{
	multiplier = 1.0f;
	gammaValue = 1.0f;
	toneMappingValue = 0.0f;
	brightness = 0.0f;
	contrast = 1.0f;
	saturation = 0.0f;
	addR = addG = addB = 0.0f;
	temperature = 6000;
	cbCRed = true;
	cbCGreen = true;
	cbCBlue = true;
	cbCLuminance = false;
	filter = 2;
	vignette = 0;
	response_function = RESPONSE_NUM_DSCS315_1;
	iso_ev_compensation = 0;
	iso_camera = 100;
	gi_compensation = 0.0;
	dotsEnabled = false;
	dotsRadius = 1;
	dotsThreshold = 5.0f;
	dotsDensity = 1;

	setToneMappingValue(tone);
	setGamma(gamma);
	setMultiplier(multiply);
}

ImageModifier::~ImageModifier()
{
}

ImageBuffer * ImageModifier::modifyImage(ImageBuffer * src)
{
	if (!src)
		return NULL;
	if (!src->imgBuf)
		return NULL;

	int w = src->width;
	int h = src->height;
	if (w < 1   ||   h < 1)
		return NULL;

	ImageBuffer * newbuff = new ImageBuffer();
	if (!newbuff)
		return NULL;

	bool OK = newbuff->allocBuffer(w,h);
	if (!OK)
	{
		delete newbuff;
		return NULL;
	}
	int x,y;
	Color4 s,d;
	int h1;
	float g = 1.0f/gammaValue;
	bool useGamma = gammaValue!=1.0f;
	bool useTone = toneMappingValue!=0.0f;

	for (y=0; y<h; y++)
	{
		for (x=0; x<w; x++)
		{
			s = src->imgBuf[y][x];
			h1 = src->hitsBuf[y][x];
			if (h1 < 1)
			{
				newbuff->imgBuf[y][x] = Color4(0,0,0);
				newbuff->hitsBuf[y][x] = 0;
				continue;
			}
			d = s * (1.0f/h1);

			d *= multiplier;

			if (useTone)
			{
				d.r = d.r / pow(1 + d.r, toneMappingValue);
				d.g = d.g / pow(1 + d.g, toneMappingValue);
				d.b = d.b / pow(1 + d.b, toneMappingValue);
			}

			if (useGamma)
			{
				d.r = pow(d.r, g);
				d.g = pow(d.g, g);
				d.b = pow(d.b, g);
			}

			newbuff->imgBuf[y][x] = d*(float)h1;
			newbuff->hitsBuf[y][x] = h1;
		}
	}

	return newbuff;
}


//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

using namespace Imf;

ImageBuffer::ImageBuffer()
{
	imgBuf = NULL;
	hitsBuf = NULL;

	width = 0;
	height = 0;

	refreshTime = 1;
	refreshHWND = 0;
	lTime = clock();
	dontDeleteMeInDestructor = false;

	vig_buck_pos_x = 0;
	vig_buck_pos_y = 0;
	vig_orig_width = 0;
	vig_orig_height = 0;
}

ImageBuffer::~ImageBuffer()
{
	if (!dontDeleteMeInDestructor)
		freeBuffer();
}


bool ImageBuffer::allocBuffer(int w, int h)
{
	freeBuffer();

	if (w<1 || h<1)
		return false;

	int i;
	width = w;
	height = h;

	hitsBuf = (int **)malloc(sizeof(int*)*height);
	if (!hitsBuf)
		return false;

	for (i=0; i<height; i++)
		hitsBuf[i] = 0;

	for (i=0; i<height; i++)
	{
		hitsBuf[i] = (int *)malloc(sizeof(int)*width);
		if (!hitsBuf[i])
		{
			freeBuffer();
			return false;
		}
	}

	imgBuf = (Color4 **)malloc(sizeof(Color4*)*height);
	if (!imgBuf)
	{
		freeBuffer();
		return false;
	}

	for (i=0; i<height; i++)
		imgBuf[i] = 0;

	for (i=0; i<height; i++)
	{
		#ifdef COLOR_SSE
			imgBuf[i] = (Color4 *)_aligned_malloc(sizeof(Color4)*width, 16);
		#else
			imgBuf[i] = (Color4 *)malloc(sizeof(Color4)*width);
		#endif
		if (!imgBuf[i])
		{
			freeBuffer();
			return false;
		}
	}
	return true;
}

void ImageBuffer::freeBuffer()
{
	if (imgBuf)
	{
		int i;
		for (i=0; i<height; i++)
		{
			if (imgBuf[i])
				#ifdef COLOR_SSE
					_aligned_free(imgBuf[i]);
				#else
					free(imgBuf[i]);
				#endif
		}
		free(imgBuf);
	}

	if (hitsBuf)
	{
		int i;
		for (i=0; i<height; i++)
		{
			if (hitsBuf[i])
				free(hitsBuf[i]);
		}
		free(hitsBuf);
	}
	hitsBuf = NULL;
	imgBuf = NULL;
	width = 0;
	height = 0;
}

bool ImageBuffer::clearBuffer()
{
	Color4 b = Color4(0,0,0);
	int i,j;
	for (i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			#ifdef COLOR_SSE
				imgBuf[i][j].sse = b.sse;
			#else
				imgBuf[i][j] = b;
			#endif
			hitsBuf[i][j] = 0;
		}
	}
	return true;
}

bool ImageBuffer::setHitsValues(int val)
{
	for (int i=0; i<height; i++)
		for(int j=0; j<width; j++)
			hitsBuf[i][j] = val;
	return true;
}

void ImageBuffer::addPixelColor(int x, int y, const Color4 &c, bool incrementHits)
{
	int x1 = x;
	int y1 = y;

	if (x1 < 0) 
		x1 = 0;
	if (x1 > width - 1)
		x1 = width - 1;
	if (y1 < 0) 
		y1 = 0;
	if (y1 > height - 1)
		y1 = height - 1;

	if (incrementHits)
		hitsBuf[y1][x1]++;
	imgBuf[y1][x1] += c;
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	unsigned int num = 0;
	unsigned int size = 0;
  
	Gdiplus::GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;
  
	Gdiplus::ImageCodecInfo* imageCodecInfo = new Gdiplus::ImageCodecInfo[size];
	Gdiplus::GetImageEncoders(num, size, imageCodecInfo);
  
	for(unsigned int i = 0; i < num; ++i)
	{
		if( wcscmp(imageCodecInfo[i].MimeType, format) == 0 )
		{
			*pClsid = imageCodecInfo[i].Clsid;
			delete[] imageCodecInfo;
			return i;
		}    
	}
	delete[] imageCodecInfo;
	return -1;
}

bool ImageBuffer::saveAsWindowsFormat(char * filename, const ImageModifier &im, int format, char * timestamp, ImageBuffer * logo, int aa, NoxEXIFdata * exif)
{
	ImageModifier ciMod;
	ciMod.copyDataFromAnother(&im);

	ImageBuffer * img = NULL;
	img = doPostProcess(aa, &ciMod, NULL, NULL, "Processing image...");
	if (!img)
		return false;

	// activate gdi+
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	size_t l = strlen(filename)+2;
	WCHAR * wch = (WCHAR *)malloc(sizeof(WCHAR)*(l));
	MultiByteToWideChar(CP_ACP, 0, filename, -1, wch, (int)l);

	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(img->width, img->height, PixelFormat32bppARGB);
	if (!bm)
	{
		delete img;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	Gdiplus::Color c;
	int x, y;
	for (y=0; y<img->height; y++)
	{
		for (x=0; x<img->width; x++)
		{
			{
				c = Gdiplus::Color(Gdiplus::Color::MakeARGB(
					255,
					(BYTE)(int)(min(255, max(0, (int)(img->imgBuf[y][x].r * 255.0f)))),
					(BYTE)(int)(min(255, max(0, (int)(img->imgBuf[y][x].g * 255.0f)))),
					(BYTE)(int)(min(255, max(0, (int)(img->imgBuf[y][x].b * 255.0f)))) ) );
			}
			bm->SetPixel(x,y, c);
		}
	}

	Gdiplus::PropertyItem pItemSoftName;
	pItemSoftName.id = PropertyTagSoftwareUsed;
	pItemSoftName.type = PropertyTagTypeASCII;
	char softName[64];
	int v1 = NOX_VER_MAJOR;
	int v2 = NOX_VER_MINOR;
	int v3 = NOX_VER_BUILD;
	sprintf_s(softName, 64, "NOX Renderer %d.%.2d", v1, v2);
	pItemSoftName.value = softName;
	pItemSoftName.length = (unsigned int)strlen(softName)+1;
	bm->SetPropertyItem(&pItemSoftName);

	if (exif)
	{
		unsigned int shutter[2];
		shutter[0] = exif->shutterNom;
		shutter[1] = exif->shutterDenom;
		Gdiplus::PropertyItem pItemShutter;
		pItemShutter.id = PropertyTagExifExposureTime;
		pItemShutter.type = PropertyTagTypeRational;
		pItemShutter.value = shutter;
		pItemShutter.length = 2*(unsigned int)sizeof(unsigned int);
		bm->SetPropertyItem(&pItemShutter);

		unsigned int fnumber[2];
		fnumber[0] = exif->fNumberNom;
		fnumber[1] = exif->fNumberDenom;
		Gdiplus::PropertyItem pItemFnumber;
		pItemFnumber.id = PropertyTagExifFNumber;
		pItemFnumber.type = PropertyTagTypeRational;
		pItemFnumber.value = fnumber;
		pItemFnumber.length = 2*(unsigned int)sizeof(unsigned int);
		bm->SetPropertyItem(&pItemFnumber);

		unsigned int focal[2];
		focal[0] = exif->focalNom;
		focal[1] = exif->focalDenom;
		Gdiplus::PropertyItem pItemFocal;
		pItemFocal.id = PropertyTagExifFocalLength;
		pItemFocal.type = PropertyTagTypeRational;
		pItemFocal.value = focal;
		pItemFocal.length = 2*(unsigned int)sizeof(unsigned int);
		bm->SetPropertyItem(&pItemFocal);

		short iso = exif->iso;
		Gdiplus::PropertyItem pItemISO;
		pItemISO.id = PropertyTagExifISOSpeed;
		pItemISO.type = PropertyTagTypeShort;
		pItemISO.value = &iso;
		pItemISO.length = 2;
		bm->SetPropertyItem(&pItemISO);
	}


	if (timestamp  &&  strlen(timestamp)>0)
	{
		size_t l2 = strlen(timestamp)+2;
		WCHAR * wtstamp = (WCHAR *)malloc(sizeof(WCHAR)*(l2));
		MultiByteToWideChar(CP_ACP, 0, timestamp, -1, wtstamp, (int)l2);

		HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
		LOGFONT lf;
		GetObject(hFont, sizeof(LOGFONT), &lf);
		float hh1 = 12;//-lf.lfHeight;
		size_t l1 = strlen(lf.lfFaceName)+2;
		WCHAR * wface = (WCHAR *)malloc(sizeof(WCHAR)*(l1));
		MultiByteToWideChar(CP_ACP, 0, lf.lfFaceName, -1, wface, (int)l1);
		Gdiplus::FontFamily  * fontFamily = new Gdiplus::FontFamily(wface);
		Gdiplus::Font        * font = new Gdiplus::Font(fontFamily, hh1, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		Gdiplus::PointF        pointF = Gdiplus::PointF(3.0f, 2.0f + img->height-35);
		Gdiplus::SolidBrush  * solidBrush = new Gdiplus::SolidBrush(Gdiplus::Color(255, 255, 255, 255));
		Gdiplus::SolidBrush  * halfBrush = new Gdiplus::SolidBrush(Gdiplus::Color(128, 0, 0, 0));

		Gdiplus::Graphics * g = new Gdiplus::Graphics(bm);
		Gdiplus::RectF rect;
		g->MeasureString(wtstamp, -1, font, pointF, &rect);
		g->FillRectangle(halfBrush, rect);

		g->DrawString(wtstamp, -1, font, pointF, solidBrush);
		
		delete g;
		delete solidBrush;
		delete halfBrush;
		delete font;
		delete fontFamily;
		free(wface);
		free(wtstamp);
	}

	if (logo   &&   logo->width>0   &&   logo->height>0)
	{
		int xp,yp;
		xp = img->width - logo->width-5;
		yp = 8;
		float a = 0.15f;
		float r,g,b;
		for (int y=0; y<logo->height; y++)
			for (int x=0; x<logo->width; x++)
			{
				if (x + xp < 0     ||     x + xp > (int)bm->GetWidth()     ||     y + yp <= 0     ||     y + yp >= (int)bm->GetHeight())
					continue;
				Color4 l = logo->imgBuf[y][x];
				Gdiplus::Color c;
				bm->GetPixel(x+xp, y+yp, &c);
				r = l.r * l.a*a * 255.0f + c.GetR()*(1.0f-l.a*a);
				g = l.g * l.a*a * 255.0f + c.GetG()*(1.0f-l.a*a);
				b = l.b * l.a*a * 255.0f + c.GetB()*(1.0f-l.a*a);
				bm->SetPixel(x+xp, y+yp, Gdiplus::Color((BYTE)r,(BYTE)g,(BYTE)b));
			}
	}
		


	ULONG quality = 100;
	Gdiplus::EncoderParameters encoderParameters;
	encoderParameters.Count = 1;
	encoderParameters.Parameter[0].Guid = Gdiplus::EncoderQuality;
	encoderParameters.Parameter[0].Type = Gdiplus::EncoderParameterValueTypeLong;
	encoderParameters.Parameter[0].NumberOfValues = 1;
	encoderParameters.Parameter[0].Value = &quality;

	Gdiplus::EncoderParameters * enparams = NULL;
	if (format == 4)
		enparams = &encoderParameters;


	CLSID pngClsid;
	WCHAR * ff;
	switch (format)
	{
		case 1: ff = L"image/bmp"; break;
		case 2: ff = L"image/png"; break;
		case 3: ff = L"image/tiff"; break;
		case 4: ff = L"image/jpeg"; break;
		default: ff = L"image/bmp"; break;
	}

	if (GetEncoderClsid(ff, &pngClsid) < 0)
	{
		delete img;
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	if (bm->Save(wch, &pngClsid, enparams) != 0)
	{
		delete img;
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	delete img;
	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	free(wch);
	return true;
}

bool ImageBuffer::saveAsEXR(char * filename, const ImageModifier &im, int aa)
{
	Rgba * rc;
	int i,j;
	
	aa = max(1, aa);
	ImageBuffer * ib = applyFilter(im.getFilter(), width/aa, height/aa);
	if (!ib)
		return false;

	Imf::Rgba * iBuff = (Imf::Rgba *)malloc (sizeof(Imf::Rgba)*ib->width*ib->height);
	if (!iBuff)
	{
		delete ib;
		return false;
	}

	for (j=0; j<ib->height; j++)
		for (i=0; i<ib->width; i++)
		{
			rc = &(iBuff[j*ib->width+i]);
			{
				rc->r = ib->imgBuf[j][i].r;
				rc->g = ib->imgBuf[j][i].g;
				rc->b = ib->imgBuf[j][i].b;
				rc->a = 1;
			}
		}
	
	try
	{
		Header header(ib->width, ib->height);
		RgbaOutputFile iFile(filename, header, WRITE_RGBA);
		iFile.setFrameBuffer(iBuff, 1, ib->width);
		iFile.writePixels(ib->height);
	}
	catch (const std::exception &exc)
	{
		delete ib;
		free(iBuff);
		MessageBox(NULL, exc.what(), "Error",  0);
		return false;
	}

	delete ib;
	free(iBuff);

	return true;
}

//---------------------------------------------------------------------------------------------------------

bool ImageBuffer::loadFromEXR(char * filename)
{
	Imf::Rgba * iBuff;
	int ewidth, eheight;

	try
	{
		RgbaInputFile file(filename);
		Imath::Box2i dw = file.dataWindow();

		ewidth  = dw.max.x - dw.min.x + 1;
		eheight = dw.max.y - dw.min.y + 1;

		iBuff = (Imf::Rgba *)malloc (sizeof(Imf::Rgba)*ewidth*eheight);
		if (!iBuff)
			return false;

		file.setFrameBuffer(iBuff, 1, ewidth);
		file.readPixels(dw.min.y, dw.max.y);
	}
	catch (const std::exception &exc)
	{
		free(iBuff);
		MessageBox(NULL, exc.what(), "Error",  0);
		return false;
	}

	allocBuffer(ewidth, eheight);

	int x, y;
	for (y=0; y<height; y++)
	{
		for (x=0; x<width; x++)
		{
			imgBuf[y][x].r = iBuff[y*width+x].r;
			imgBuf[y][x].g = iBuff[y*width+x].g;
			imgBuf[y][x].b = iBuff[y*width+x].b;
			imgBuf[y][x].a = 1;
			hitsBuf[y][x] = 1;
		}
	}

	free(iBuff);

	return true;
}


ImageByteBuffer * ImageBuffer::toImageByteBuffer()
{
	ImageByteBuffer * res = new ImageByteBuffer;
	res->allocBuffer(width, height);

	int i,j;
	BYTE r,g,b;
	for (j=0; j<height; j++)
		for (i=0; i<width; i++)
		{
			r = min(255, max(0, (BYTE)(imgBuf[j][i].r*255.0f)));
			g = min(255, max(0, (BYTE)(imgBuf[j][i].g*255.0f)));
			b = min(255, max(0, (BYTE)(imgBuf[j][i].b*255.0f)));
			res->imgBuf[j][i] = RGB(r,g,b);
		}

	return res;
}

bool ImageBuffer::copyToImageByteBuffer(ImageByteBuffer * img, COLORREF badColor)
{
	if (!img)
		return false;

	int i,j;
	BYTE r,g,b;

	for (j=0; j<img->height; j++)
	{
		for (i=0; i<img->width; i++)
		{
			if (j >= height   ||   i >= width) 
				img->imgBuf[j][i] = badColor;
			else
			{
				r = (BYTE)min(255, max(0, (int)(imgBuf[j][i].r*255.0f)));
				g = (BYTE)min(255, max(0, (int)(imgBuf[j][i].g*255.0f)));
				b = (BYTE)min(255, max(0, (int)(imgBuf[j][i].b*255.0f)));
				img->imgBuf[j][i] = RGB(r,g,b);
			}
		}
	}

	return true;
}

bool ImageBuffer::copyPartToImageByteBuffer(ImageByteBuffer * img, int px, int py)
{
	if (!img)
		return false;

	int x1, x2, y1, y2;
	x1 = max(0, px);
	y1 = max(0, py);
	x2 = min(px + width, img->width);
	y2 = min(py + height, img->height);

	int i,j;
	BYTE r,g,b;
	for (j=y1; j<y2; j++)
	{
		for (i=x1; i<x2; i++)
		{
			r = (BYTE)min(255, max(0, (int)(imgBuf[j-py][i-px].r*255.0f)));
			g = (BYTE)min(255, max(0, (int)(imgBuf[j-py][i-px].g*255.0f)));
			b = (BYTE)min(255, max(0, (int)(imgBuf[j-py][i-px].b*255.0f)));
			img->imgBuf[j][i] = RGB(r,g,b);
		}
	}
	return true;
}

ImageBuffer * ImageBuffer::copyBuffer()
{
	CHECK(imgBuf);
	CHECK(hitsBuf);
	CHECK(width>0);
	CHECK(height>0);

	ImageBuffer * res = new ImageBuffer();
	CHECK(res);
	bool ok = res->allocBuffer(width, height);
	if (!ok)
	{
		delete res;
		return NULL;
	}

	for (int y=0; y<height; y++)
		memcpy(res->imgBuf[y], imgBuf[y], sizeof(Color4)*width);

	for (int y=0; y<height; y++)
		memcpy(res->hitsBuf[y], hitsBuf[y], sizeof(int)*width);

	return res;
}

ImageBuffer * ImageBuffer::copyResize(int nw, int nh)
{
	if (!imgBuf)
		return NULL;
	if (nw<1 || nh<1)
		return NULL;

	ImageBuffer * res = new ImageBuffer();
	CHECK(res);
	bool ok = res->allocBuffer(nw, nh);
	if (!ok)
	{
		delete res;
		return NULL;
	}

	float ratio_x = nw/(float)width;
	float ratio_y = nh/(float)height;
	bool higher_x = (nw>width);
	bool higher_y = (nh>height);


	if (higher_y)
	{
		if (higher_x)
		{
			for (int y=0; y<nh; y++)
			{
				float ny = (y+0.5f)/nh*height;
				int iny = (int)(ny-0.5f);
				float wy2 = (float)ny - (iny+0.5f);
				float wy1 = (iny+1.5f) - (float)ny;
				int y1 = max(0, min(height-1, iny));
				int y2 = max(0, min(height-1, iny+1));

				for (int x=0; x<nw; x++)
				{
					float nx = (x+0.5f)/nw*width;
					int inx = (int)(nx-0.5f);
					float wx2 = (float)nx - (inx+0.5f);
					float wx1 = (inx+1.5f) - (float)nx;
					int x1 = max(0, min(width-1, inx));
					int x2 = max(0, min(width-1, inx+1));
					Color4 c = 
							imgBuf[y1][x1] * wx1*wy1 + 
							imgBuf[y1][x2] * wx2*wy1 + 
							imgBuf[y2][x1] * wx1*wy2 + 
							imgBuf[y2][x2] * wx2*wy2;

					res->imgBuf[y][x] = c;
				}
			}
		} //                            --------------------------------
		else
		{
			for (int y=0; y<nh; y++)
			{
				float ny = (y+0.5f)/nh*height;
				int iny = (int)(ny-0.5f);
				float wy2 = (float)ny - (iny+0.5f);
				float wy1 = (iny+1.5f) - (float)ny;
				int y1 = max(0, min(height-1, iny));
				int y2 = max(0, min(height-1, iny+1));

				for (int x=0; x<nw; x++)
				{
					float xstart = x/(float)nw*width;
					float xstop  = (x+1)/(float)nw*width;
					int ix1 = (int)xstart;
					int ix2 = (int)xstop;
					ix2 = min(ix2, width-1);
					Color4 c1 = Color4(0,0,0);
					Color4 c2 = Color4(0,0,0);
					for (int tx=ix1; tx<=ix2; tx++)
					{
						float wx1 = min(1, tx+1-xstart);
						float wx2 = min(1, xstop - tx);
						float wx  = min(wx1, wx2);
						int xx = min(tx, width-1);
						c1 += imgBuf[y1][xx] * wx;
						c2 += imgBuf[y2][xx] * wx;
					}
					res->imgBuf[y][x] = (c1*wy1 + c2*wy2) * (1.0f/(xstop-xstart)) ;
				}
			}
		} //                            --------------------------------
	}
	else
	{
		if (higher_x)
		{
			for (int y=0; y<nh; y++)
			{
				float ystart = y/(float)nh*height;
				float ystop  = (y+1)/(float)nh*height;
				int iy1 = (int)ystart;
				int iy2 = (int)ystop;
				iy2 = min(iy2, height-1);

				for (int x=0; x<nw; x++)
				{
					Color4 csum = Color4(0,0,0);
					for (int ty=iy1; ty<=iy2; ty++)
					{
						float wy1 = min(1, ty+1-ystart);
						float wy2 = min(1, ystop - ty);
						float wy  = min(wy1, wy2);
						int yy = min(ty, height-1);

						float nx = (x+0.5f)/nw*width;
						int inx = (int)(nx-0.5f);
						float wx2 = nx - (inx+0.5f);
						float wx1 = (inx+1.5f) - nx;
						int x1 = max(0, min(width-1, inx));
						int x2 = max(0, min(width-1, inx+1));
						Color4 c = 
								imgBuf[yy][x1] * wx1 + 
								imgBuf[yy][x2] * wx2;
						csum += c * wy;
					}
					res->imgBuf[y][x] = csum * (1.0f/(ystop-ystart)) ;
					}
			}
		} //                            --------------------------------
		else
		{
			for (int y=0; y<nh; y++)
			{
				float ystart = y/(float)nh*height;
				float ystop  = (y+1)/(float)nh*height;
				int iy1 = (int)ystart;
				int iy2 = (int)ystop;
				iy2 = min(iy2, height-1);

				for (int x=0; x<nw; x++)
				{
					float xstart = x/(float)nw*width;
					float xstop  = (x+1)/(float)nw*width;
					int ix1 = (int)xstart;
					int ix2 = (int)xstop;
					ix2 = min(ix2, width-1);

					Color4 cy = Color4(0,0,0);
					for (int ty=iy1; ty<=iy2; ty++)
					{
						float wy1 = min(1, ty+1-ystart);
						float wy2 = min(1, ystop - ty);
						float wy  = min(wy1, wy2);
						int yy = min(ty, height-1);


						Color4 cx = Color4(0,0,0);
						for (int tx=ix1; tx<=ix2; tx++)
						{
							float wx1 = min(1, tx+1-xstart);
							float wx2 = min(1, xstop - tx);
							float wx  = min(wx1, wx2);
							int xx = min(tx, width-1);
							cx += imgBuf[yy][xx] * wx;
						}
						cy += cx * wy;
					}

					res->imgBuf[y][x] = cy * (1.0f/(ystop-ystart)) * (1.0f/(xstop-xstart)) ;
				}
			}
		} //                            --------------------------------
	}

	for (int y=0; y<nh; y++)
		for (int x=0; x<nw; x++)
			res->hitsBuf[y][x] = 1;

	return res;
}


ImageBuffer * ImageBuffer::evalSingleBloom(int type, float ratio, FinalModifier * fMod)
{
	ImageBuffer * res = new ImageBuffer();
	CHECK(res);
	bool allocOK = res->allocBuffer(width, height);
	CHECK(allocOK);
	res->clearBuffer();

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();

	float threshold = fMod->bloomThreshold;
	float minBr = threshold / cam->iMod.getMultiplier();
	float bloom_exp = fMod->bloomPower;
	float bloom = (float)pow(2, bloom_exp);
	float wMn = 1.0f * bloom;
	bool useBloomOtherColor = fMod->bloomAttenuationOn;
	Color4 bloomColor = fMod->bloomAttenuationColor;
	int bloomarea = fMod->bloomArea;
	bloomarea = max(1, (int)(bloomarea * ratio));
	int rad = bloomarea;

	if (type == 0)	// BOTH
	{
		float cbloom = bloomarea * 0.03f;
		int wym = rad*2 + 1;
		float * arr = (float*)malloc(wym*sizeof(float));

		float pcbloom22 = -1.0f/(2.0f*cbloom*cbloom);
		float f = 1 / sqrt(2*PI*cbloom*cbloom);
		for (int i=0; i<wym; i++)
		{
			int x = i-rad;
			float r2 = (float)(x*x);
			arr[i] = pow ( (rad-fabs((float)x))/(float)rad , 8.0f) * 45.0f/rad/rad/PI;
		}

		cbloom = bloomarea * 0.03f;
		pcbloom22 = -1.0f/(2.0f*cbloom*cbloom);
		f = 1 / sqrt(2*PI*cbloom*cbloom);
		for (int i=0; i<wym; i++)
		{
			int x = i-rad;
			float r2 = (float)(x*x);
			arr[i] = arr[i]*0.5f + 0.5f * pow (f * exp(r2*pcbloom22) , 2);
		}

		for (int j=0; j<height; j++)
		{
			for (int i=0; i<width; i++)
			{
				Color4 c = imgBuf[j][i];
				if (c.r < minBr     &&     c.g < minBr     &&     c.b < minBr)
					continue;

				Color4 c_bl = c * wMn;
				if (useBloomOtherColor)
					c_bl *= bloomColor;

				{
					int min_y, max_y, min_x, max_x;
					min_y = max(0, j-rad);
					max_y = min(res->height-1, j+rad);
					min_x = max(0, i-rad);
					max_x = min(res->width-1, i+rad);
					int radMinusI = rad-i;
					int radMinusJ = rad-j;


					for (int y=min_y; y<=max_y; y++)
					{
						Color4 c_bl_gy = c_bl;
						Color4 * resYline = res->imgBuf[y];
						float y_2 = (float)(y-j)*(y-j);
						for (int x=min_x; x<=max_x; x++)
						{
							float d_2 = (x-i)*(x-i) + y_2;
							float d = fabs(rad+sqrt(d_2));
							int id = (int)d;
							float g_x = arr[min(2*rad, id)] * (id+1-d) + arr[min(2*rad, id+1)]*(d-id);
							resYline[x] += c_bl_gy * g_x;
						}
					}
				}
			}
		}

		if (arr)
			free(arr);

	}

	if (type == 1)	// ONLY GAUSS
	{
		float cbloom = bloomarea * 0.02f;
		int rad = bloomarea;
		int wym = rad*2 + 1;
		float * arr = (float*)malloc(wym*sizeof(float));

		float pcbloom22 = -1.0f/(2.0f*cbloom*cbloom);
		float f = 1 / sqrt(2*PI*cbloom*cbloom);
		for (int i=0; i<wym; i++)
		{
			int x = i-rad;
			float r2 = (float)(x*x);
			arr[i] = f * exp(r2*pcbloom22) * 0.5f;
		}

		for (int j=0; j<height; j++)
		{
			for (int i=0; i<width; i++)
			{
				Color4 c = imgBuf[j][i];
				if (c.r < minBr     &&     c.g < minBr     &&     c.b < minBr)
					continue;

				Color4 c_bl = c * wMn;
				if (useBloomOtherColor)
					c_bl *= bloomColor;

				{
					int min_y, max_y, min_x, max_x;
					min_y = max(0, j-rad/4);
					max_y = min(res->height-1, j+rad/4);
					min_x = max(0, i-rad/4);
					max_x = min(res->width-1, i+rad/4);
					int radMinusI = rad-i;
					int radMinusJ = rad-j;


					for (int y=min_y; y<=max_y; y++)
					{
						float g_y = arr[y+radMinusJ];
						Color4 c_bl_gy = c_bl*g_y;
						Color4 * resYline = res->imgBuf[y];
						for (int x=min_x; x<=max_x; x++)
						{
							float g_x = arr[x+radMinusI];
							resYline[x] += c_bl_gy * g_x;
						}
					}
				}
			}
		}
		if (arr)
			free(arr);


	}

	if (type == 2)	// ONLY POW8
	{
		for (int j=0; j<height; j++)
		{
			for (int i=0; i<width; i++)
			{
				Color4 c = imgBuf[j][i];
				if (c.r < minBr     &&     c.g < minBr     &&     c.b < minBr)
					continue;

				Color4 c_bl = c * wMn;
				if (useBloomOtherColor)
					c_bl *= bloomColor;

				{
					int min_y, max_y, min_x, max_x;
					min_y = max(0, j-rad);
					max_y = min(res->height-1, j+rad);
					min_x = max(0, i-rad);
					max_x = min(res->width-1, i+rad);
					int radMinusI = rad-i;
					int radMinusJ = rad-j;


					for (int y=min_y; y<=max_y; y++)
					{
						float y_2 = (float)(y+radMinusJ-rad)*(y+radMinusJ-rad);
						Color4 c_bl_gy = c_bl;
						Color4 * resYline = res->imgBuf[y];
						for (int x=min_x; x<=max_x; x++)
						{
							float d_2 = (x+radMinusI-rad)*(x+radMinusI-rad) + y_2;
							float d = sqrt(d_2);
							float f = 1-d/rad;
							f = max(0, f);

							float f_8 = f*f*f*f*f*f*f*f;
							f_8 *= 45.0f/rad/rad/PI * 0.5f;

							resYline[x] += c_bl_gy * f_8;
						}
					}
				}
			}
		}

	}

	return res;
}


ImageBuffer * ImageBuffer::applyFakeBloomFast(FinalModifier * fMod)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res)
		return NULL;
	if (!res->allocBuffer(width, height))
		return NULL;

	for (int j=0; j<height; j++)
	{
		memcpy(res->imgBuf[j],  imgBuf[j],  width*sizeof(Color4));
		memcpy(res->hitsBuf[j], hitsBuf[j], width*sizeof(int));
	}

	Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();

	float threshold = fMod->bloomThreshold;
	bool useBloom = fMod->bloomEnabled;
	if (!useBloom)
		return res;

	int smW=120, smH=240;
	float t = pow((float)height, 0.85f);
	float ratio = t/height;
	smH = (int)t;
	smW = (int)(width * t / height);


	ImageBuffer * smallImg = copyResize(smW, smH);
	ImageBuffer * smallDstGauss = smallImg->evalSingleBloom(1, ratio, fMod);
	ImageBuffer * bigbloomGauss = smallDstGauss->copyResize(width, height);
	smallDstGauss->freeBuffer();
	for (int y=0; y<height; y++)
		for (int x=0; x<width; x++)
			res->imgBuf[y][x] += bigbloomGauss->imgBuf[y][x];
	bigbloomGauss->freeBuffer();
	delete smallDstGauss;
	delete bigbloomGauss;

	ImageBuffer * smallDstPow = smallImg->evalSingleBloom(2, ratio, fMod);
	ImageBuffer * bigbloomPow = smallDstPow->copyResize(width, height);
	smallDstPow->freeBuffer();
	for (int y=0; y<height; y++)
		for (int x=0; x<width; x++)
			res->imgBuf[y][x] += bigbloomPow->imgBuf[y][x];
	bigbloomPow->freeBuffer();
	delete smallDstPow;
	delete bigbloomPow;

	smallImg->freeBuffer();
	delete smallImg;
	
	return res;
}

ImageBuffer * ImageBuffer::applyGlare(ImageBuffer * dstBuf, FinalModifier * fMod)
{
	CHECK(imgBuf);
	CHECK(dstBuf);
	CHECK(dstBuf->imgBuf);
	CHECK(dstBuf->width==width);
	CHECK(dstBuf->height==height);
	ImageBuffer * res = dstBuf;

	Raytracer * rtr = Raytracer::getInstance();

	Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
	float threshold = fMod->glareThreshold;
	float minBr = threshold / cam->iMod.getMultiplier();
	float glare_exp = fMod->glarePower;
	float glare = (float)pow(2, glare_exp);
	bool useGlare = fMod->glareEnabled;

	if (!cam->glareOK   &&   useGlare   &&   glare>0)
	{
		cam->createDiaphragmAndObstacleImage(fMod->glareArea);
		cam->createGlarePattern(fMod->glareArea);
	}

	int glarecntr = 0;
	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			Color4 c = imgBuf[y][x];
			if (c.r < minBr     &&     c.g < minBr     &&     c.b < minBr)
				continue;
			glarecntr++;
		}
	}

	int * coords = (int *)malloc(sizeof(int)*glarecntr*2);
	CHECK(coords);

	int glidx = 0;
	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			Color4 c = imgBuf[y][x];
			if (c.r < minBr     &&     c.g < minBr     &&     c.b < minBr)
				continue;
			coords[2*glidx+0] = x;
			coords[2*glidx+1] = y;
			glidx++;
		}
	}

	for (int i=0; i<glarecntr; i++)
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Applying glare on image ...", i*100.0f/glarecntr);
		if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
		{
			free(coords);
			return res;
		}
	
		int x = coords[2*i+0];
		int y = coords[2*i+1];
		Color4 c = imgBuf[y][x];

		Color4 c_gl = c * (glare * 0.001f);
		int h = cam->texFourier.bmp.getHeight();
		int w = cam->texFourier.bmp.getWidth();
		int min_y, max_y, min_x, max_x;
		min_y = max(0, y-h/2);
		max_y = min(height-1, y+h/2-1);
		min_x = max(0, x-w/2);
		max_x = min(width-1, x+w/2-1);


		for (int y1=min_y; y1<=max_y; y1++)
		{
			int yy = y1 - y + h/2;
			Color4 * resYline = res->imgBuf[y1];

			for (int x1=min_x; x1<=max_x; x1++)
			{
				int xx = x1 - x + w/2;
				resYline[x1] += c_gl * cam->texFourier.bmp.fdata[yy*w+xx];
			}
		}
	}

	free(coords);
	return res;
}

ImageBuffer * ImageBuffer::applyBloomAndGlare(ImageModifier * iMod, bool nowRendering)	// deprecated
{
	return NULL;
}


ImageBuffer * ImageBuffer::applyGauss(int numPixels)
{
	ImageBuffer * res = new ImageBuffer();
	if (!res)
		return NULL;
	if (!res->allocBuffer(width, height))
		return NULL;

	int n = max(1 ,min(5, numPixels));
	float ro = findGaussianBlurRoForNumPixels(numPixels);

	float m[121];
	int i,j,k;
	k = 0;
	for (i=-n; i<=n; i++)
		for (j=-n; j<=n; j++)
		{
			m[k] = 1.0f/2.0f/PI/ro/ro * exp(-(i*i+j*j)/2.0f/ro/ro);
			k++;
		}

	int x,y,xx,yy;
	Color4 c;

	for (x=0; x<width; x++)
	{
		for (y=0; y<height; y++)
		{
			c = Color4(0,0,0);
			k = 0;
			for (i=-n; i<=n; i++)
			{
				for (j=-n; j<=n; j++)
				{
					xx = x + i;
					yy = y + j;
					xx = min(width-1 , max(xx, 0));
					yy = min(height-1, max(yy, 0));
					if (hitsBuf[yy][xx] > 0)
						c += imgBuf[yy][xx] * (m[k] / hitsBuf[yy][xx]);
					k++;
				}
			}

			res->hitsBuf[y][x] = max(1, hitsBuf[y][x]);
			res->imgBuf[y][x] = c * (float)res->hitsBuf[y][x];

		}
	}

	return res;
}


ImageBuffer * ImageBuffer::applyGaussAdaptive()
{
	ImageBuffer * res = new ImageBuffer();
	if (!res)
		return NULL;
	if (!res->allocBuffer(width, height))
		return NULL;

	int i,j,k;
	int n;
	float ro1 = findGaussianBlurRoForNumPixels(1);
	float ro2 = findGaussianBlurRoForNumPixels(2);
	float ro3 = findGaussianBlurRoForNumPixels(3);
	float ro4 = findGaussianBlurRoForNumPixels(4);
	float ro5 = findGaussianBlurRoForNumPixels(5);
	float m1[9];
	float m2[25];
	float m3[49];
	float m4[81];
	float m5[121];

	k = 0;
	n = 1;
	for (i=-n; i<=n; i++)
		for (j=-n; j<=n; j++)
		{
			m1[k] = 1.0f/2.0f/PI/ro1/ro1 * exp(-(i*i+j*j)/2.0f/ro1/ro1);
			k++;
		}
 	k = 0;
	n = 2;
	for (i=-n; i<=n; i++)
		for (j=-n; j<=n; j++)
		{
			m2[k] = 1.0f/2.0f/PI/ro2/ro2 * exp(-(i*i+j*j)/2.0f/ro2/ro2);
			k++;
		}
	k = 0;
	n = 3;
	for (i=-n; i<=n; i++)
		for (j=-n; j<=n; j++)
		{
			m3[k] = 1.0f/2.0f/PI/ro3/ro3 * exp(-(i*i+j*j)/2.0f/ro3/ro3);
			k++;
		}
	k = 0;
	n = 4;
	for (i=-n; i<=n; i++)
		for (j=-n; j<=n; j++)
		{
			m4[k] = 1.0f/2.0f/PI/ro4/ro4 * exp(-(i*i+j*j)/2.0f/ro4/ro4);
			k++;
		}
	k = 0;
	n = 5;
	for (i=-n; i<=n; i++)
		for (j=-n; j<=n; j++)
		{
			m5[k] = 1.0f/2.0f/PI/ro5/ro5 * exp(-(i*i+j*j)/2.0f/ro5/ro5);
			k++;
		}

	int x,y,xx,yy,h;
	Color4 c;

	for (x=0; x<width; x++)
	{
		for (y=0; y<height; y++)
		{
			c = Color4(0,0,0);
			k = 0;
			h = hitsBuf[y][x];
			n = max((5-h/10), 0);
			//n = 5;
			for (i=-n; i<=n; i++)
			{
				for (j=-n; j<=n; j++)
				{
					xx = x + i;
					yy = y + j;
					xx = min(width-1 , max(xx, 0));
					yy = min(height-1, max(yy, 0));
					if (hitsBuf[yy][xx] > 0)
					{
						switch (n)
						{
							case 0: c += imgBuf[y][x] * (1.0f / hitsBuf[y][x]);
								break;
							case 1: c += imgBuf[yy][xx] * (m1[k] / hitsBuf[yy][xx]);
								break;
							case 2: c += imgBuf[yy][xx] * (m2[k] / hitsBuf[yy][xx]);
								break;
							case 3: c += imgBuf[yy][xx] * (m3[k] / hitsBuf[yy][xx]);
								break;
							case 4: c += imgBuf[yy][xx] * (m4[k] / hitsBuf[yy][xx]);
								break;
							case 5: c += imgBuf[yy][xx] * (m5[k] / hitsBuf[yy][xx]);
								break;
						}
					}
					k++;
				}
			}

			res->hitsBuf[y][x] = max(1, hitsBuf[y][x]);
			res->imgBuf[y][x] = c * (float)res->hitsBuf[y][x];

		}
	}

	return res;
}


float ImageBuffer::findGaussianBlurRoForNumPixels(int numPixels)
{
	float res = 0.1f;
	float sum = 2;
	int n = numPixels;
	float c;

	int i,j;

	do 
	{
		res += 0.001f;
		sum = 0;
		for (i=-n; i<=n; i++)
			for (j=-n; j<=n; j++)
			{
				c = 1.0f/2.0f/PI/res/res * exp(-(i*i+j*j)/2.0f/res/res);
				sum += c;
			}
	}
	while (sum > 0.999f);

	return res;
}


//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------

ImageByteBuffer::ImageByteBuffer()
{
	timestamp = NULL;
	width = 0;
	height = 0;
	imgBuf = NULL;
	drawingBufferLock = false;
}

ImageByteBuffer::~ImageByteBuffer()
{
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool ImageByteBuffer::allocBuffer(int w, int h)
{
	freeBuffer();

	int i;
	width = w;
	height = h;
	w4 = (width/4) * 4;
	if (w4<width)
		w4 += 4;

	imgBuf = (COLORREF **)malloc(sizeof(COLORREF*)*height);
	if (!imgBuf)
	{
		freeBuffer();
		return false;
	}

	for (i=0; i<height; i++)
		imgBuf[i] = NULL;

	for (i=0; i<height; i++)
	{
		#ifdef BBUF_SSE
			imgBuf[i] = (COLORREF *)_aligned_malloc(sizeof(COLORREF)*w4, 16);
		#else
			imgBuf[i] = (COLORREF *)malloc(sizeof(COLORREF)*width);
		#endif
		if (!imgBuf[i])
		{
			freeBuffer();
			return false;
		}
	}
	return true;
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool ImageByteBuffer::clearBuffer()
{
	if (!imgBuf)
		return false;
	COLORREF b = RGB(0,0,0);
	int i,j;
	for (i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			imgBuf[i][j] = b;
		}
	}
	return true;
}

//-------------------------------------------------------------------------------------------------------------------------------------

void ImageByteBuffer::freeBuffer()
{
	while (drawingBufferLock)
		Sleep(10);

	if (imgBuf)
	{
		int i;
		for (i=0; i<height; i++)
		{
			if (imgBuf[i])
				#ifdef BBUF_SSE
					_aligned_free(imgBuf[i]);
				#else
					free(imgBuf[i]);
				#endif
		}
		free(imgBuf);
	}
	imgBuf = NULL;
}

void ImageByteBuffer::lockBuffer()
{
	drawingBufferLock = true;
}

void ImageByteBuffer::unlockBuffer()
{
	drawingBufferLock = false;
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool ImageByteBuffer::loadFromWindowsFormat(char * filename)
{
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	size_t l = strlen(filename)+2;
	WCHAR * wch = (WCHAR *)malloc(sizeof(WCHAR)*(l));
	MultiByteToWideChar(CP_ACP, 0, filename, -1, wch, (int)l);

	Gdiplus::Bitmap * bm = Gdiplus::Bitmap::FromFile(wch);
	if (!bm)
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	//bm->GetHBITMAP()
	int w1 = bm->GetWidth();
	int h1 = bm->GetHeight();

	if (!allocBuffer(w1,h1))
	if (!bm)
	{
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}


	Gdiplus::Color c;
	int x, y;
	for (y=0; y<h1; y++)
	{
		for (x=0; x<w1; x++)
		{
			bm->GetPixel(x,y,&c);
			imgBuf[y][x] = c.ToCOLORREF();
		}
	}

	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	free(wch);
	return true;
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool ImageByteBuffer::saveAsWindowsFormat(char * filename, int format, NoxEXIFdata * exif)
{
	CHECK(filename);

	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	size_t l = strlen(filename)+2;
	WCHAR * wch = (WCHAR *)malloc(sizeof(WCHAR)*(l));
	MultiByteToWideChar(CP_ACP, 0, filename, -1, wch, (int)l);

	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(width, height, PixelFormat32bppARGB);
	if (!bm)
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	Gdiplus::Color c;
	int x, y;
	for (y=0; y<height; y++)
	{
		for (x=0; x<width; x++)
		{
			{
				COLORREF sc = imgBuf[y][x];
				c = Gdiplus::Color(Gdiplus::Color::MakeARGB(
					255,
					(BYTE)GetRValue(sc),
					(BYTE)GetGValue(sc),
					(BYTE)GetBValue(sc) ));
			}
			bm->SetPixel(x,y, c);
		}
	}

	Gdiplus::PropertyItem pItemSoftName;
	pItemSoftName.id = PropertyTagSoftwareUsed;
	pItemSoftName.type = PropertyTagTypeASCII;
	char softName[64];
	int v1 = NOX_VER_MAJOR;
	int v2 = NOX_VER_MINOR;
	int v3 = NOX_VER_BUILD;
	sprintf_s(softName, 64, "NOX Renderer %d.%.2d", v1, v2);
	pItemSoftName.value = softName;
	pItemSoftName.length = (unsigned int)strlen(softName)+1;
	bm->SetPropertyItem(&pItemSoftName);

	if (exif)
	{
		unsigned int shutter[2];
		shutter[0] = exif->shutterNom;
		shutter[1] = exif->shutterDenom;
		Gdiplus::PropertyItem pItemShutter;
		pItemShutter.id = PropertyTagExifExposureTime;
		pItemShutter.type = PropertyTagTypeRational;
		pItemShutter.value = shutter;
		pItemShutter.length = 2*(unsigned int)sizeof(unsigned int);
		bm->SetPropertyItem(&pItemShutter);

		unsigned int fnumber[2];
		fnumber[0] = exif->fNumberNom;
		fnumber[1] = exif->fNumberDenom;
		Gdiplus::PropertyItem pItemFnumber;
		pItemFnumber.id = PropertyTagExifFNumber;
		pItemFnumber.type = PropertyTagTypeRational;
		pItemFnumber.value = fnumber;
		pItemFnumber.length = 2*(unsigned int)sizeof(unsigned int);
		bm->SetPropertyItem(&pItemFnumber);

		unsigned int focal[2];
		focal[0] = exif->focalNom;
		focal[1] = exif->focalDenom;
		Gdiplus::PropertyItem pItemFocal;
		pItemFocal.id = PropertyTagExifFocalLength;
		pItemFocal.type = PropertyTagTypeRational;
		pItemFocal.value = focal;
		pItemFocal.length = 2*(unsigned int)sizeof(unsigned int);
		bm->SetPropertyItem(&pItemFocal);

		short iso = exif->iso;
		Gdiplus::PropertyItem pItemISO;
		pItemISO.id = PropertyTagExifISOSpeed;
		pItemISO.type = PropertyTagTypeShort;
		pItemISO.value = &iso;
		pItemISO.length = 2;
		bm->SetPropertyItem(&pItemISO);
	}


	ULONG quality = 100;
	Gdiplus::EncoderParameters encoderParameters;
	encoderParameters.Count = 1;
	encoderParameters.Parameter[0].Guid = Gdiplus::EncoderQuality;
	encoderParameters.Parameter[0].Type = Gdiplus::EncoderParameterValueTypeLong;
	encoderParameters.Parameter[0].NumberOfValues = 1;
	encoderParameters.Parameter[0].Value = &quality;

	Gdiplus::EncoderParameters * enparams = NULL;
	if (format == 4)
		enparams = &encoderParameters;


	CLSID pngClsid;
	WCHAR * ff;
	switch (format)
	{
		case 1: ff = L"image/bmp"; break;
		case 2: ff = L"image/png"; break;
		case 3: ff = L"image/tiff"; break;
		case 4: ff = L"image/jpeg"; break;
		default: ff = L"image/bmp"; break;
	}

	if (GetEncoderClsid(ff, &pngClsid) < 0)
	{
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	if (bm->Save(wch, &pngClsid, enparams) != 0)
	{
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return false;
	}

	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	free(wch);
	return true;
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool ImageBuffer::loadFromResource(WORD idRes)
{
	HRSRC res = FindResource(hDllModule, MAKEINTRESOURCE(idRes), "BINARY");
	if (!res)
		return false;

	HGLOBAL mem = LoadResource(hDllModule, res);
	void *data = LockResource(mem);
	if (!data)
		return false;
	size_t sz = SizeofResource(hDllModule, res);
	if (sz < 1)
		return false;

	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, sz);
	if (!hGlobal)
		return false;
	LPVOID pvData = GlobalLock(hGlobal);
	CopyMemory(pvData,data, sz);
	LPSTREAM pStream = NULL;
	HRESULT hr = CreateStreamOnHGlobal( hGlobal, false, &pStream);
	if (hr != S_OK   ||  !pStream)
	{
		if (pStream)
			pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		return false;
	}

	// activate gdi+
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// make bitmap
	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(pStream);
	if (!bm  ||  bm->GetLastStatus() != Gdiplus::Ok  ||  bm->GetWidth()<1  ||  bm->GetHeight()<1)
	{
		pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		if (bm)
			delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		return false;
	}

	bool a1 = allocBuffer(bm->GetWidth(), bm->GetHeight());
	if (!a1)
	{
		pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
	}

	for (unsigned int y=0; y<bm->GetHeight(); y++)
		for (unsigned int x=0; x<bm->GetWidth(); x++)
		{
			Gdiplus::Color color;
			bm->GetPixel(x,y, &color);
			Color4 c;
			c.r = color.GetR() / 255.0f;
			c.g = color.GetG() / 255.0f;
			c.b = color.GetB() / 255.0f;
			c.a = color.GetA() / 255.0f;
			imgBuf[y][x] = c;
		}

	pStream->Release();
	GlobalUnlock(hGlobal);
	GlobalFree(hGlobal);
	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	return true;
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool ImageByteBuffer::loadFromResource(HMODULE hMod, WORD idRes)
{
	HRSRC res = FindResource(hMod, MAKEINTRESOURCE(idRes), "BINARY");
	if (!res)
		return false;

	HGLOBAL mem = LoadResource(hMod, res);
	void *data = LockResource(mem);
	if (!data)
		return false;
	size_t sz = SizeofResource(hMod, res);
	if (sz < 1)
		return false;

	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, sz);
	if (!hGlobal)
		return false;
	LPVOID pvData = GlobalLock(hGlobal);
	CopyMemory(pvData,data, sz);
	LPSTREAM pStream = NULL;
	HRESULT hr = CreateStreamOnHGlobal( hGlobal, false, &pStream);
	if (hr != S_OK   ||  !pStream)
	{
		if (pStream)
			pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		return false;
	}

	// activate gdi+
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// make bitmap
	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(pStream);
	if (!bm  ||  bm->GetLastStatus() != Gdiplus::Ok  ||  bm->GetWidth()<1  ||  bm->GetHeight()<1)
	{
		pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		if (bm)
			delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		return false;
	}

	bool a1 = allocBuffer(bm->GetWidth(), bm->GetHeight());
	if (!a1)
	{
		pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
	}

	for (unsigned int y=0; y<bm->GetHeight(); y++)
		for (unsigned int x=0; x<bm->GetWidth(); x++)
		{
			Gdiplus::Color color;
			bm->GetPixel(x,y, &color);
			unsigned char r = color.GetR();
			unsigned char g = color.GetG();
			unsigned char b = color.GetB();
			unsigned char a = color.GetA();
			COLORREF c = r | (g<<8) | (b<<16) | (a<<24); 
			imgBuf[y][x] = c;
		}

	pStream->Release();
	GlobalUnlock(hGlobal);
	GlobalFree(hGlobal);
	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	return true;
}

//-------------------------------------------------------------------------------------------------------------------------------------

HBITMAP loadHBitmapFromBinaryResource(HMODULE hMod, WORD idRes)
{
	HRSRC res = FindResource(hMod, MAKEINTRESOURCE(idRes), "BINARY");
	if (!res)
		return 0;

	HGLOBAL mem = LoadResource(hMod, res);
	void *data = LockResource(mem);
	if (!data)
		return 0;
	size_t sz = SizeofResource(hMod, res);
	if (sz < 1)
		return 0;

	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, sz);
	if (!hGlobal)
		return 0;
	LPVOID pvData = GlobalLock(hGlobal);
	CopyMemory(pvData,data, sz);
	LPSTREAM pStream = NULL;
	HRESULT hr = CreateStreamOnHGlobal( hGlobal, false, &pStream);
	if (hr != S_OK   ||  !pStream)
	{
		if (pStream)
			pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		return 0;
	}

	// activate gdi+
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// make bitmap
	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(pStream);
	if (!bm  ||  bm->GetLastStatus() != Gdiplus::Ok  ||  bm->GetWidth()<1  ||  bm->GetHeight()<1)
	{
		pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		if (bm)
			delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		return 0;
	}

	// get hbitmap
	HBITMAP result;
	Gdiplus::Color c(0,0,0);
	Gdiplus::Status s = bm->GetHBITMAP(c, &result);
	if (s!=Gdiplus::Ok)
	{
		pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
	}

	pStream->Release();
	GlobalUnlock(hGlobal);
	GlobalFree(hGlobal);
	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	
	return result;
}

//-------------------------------------------------------------------------------------------------------------------------------------

void ImageByteBuffer::applyLogoStamp(ImageBuffer * logo)
{
	if (!logo   ||   logo->width<1   ||   logo->height<1)
		return;

	int xp,yp;
	xp = width - logo->width-5;
	yp = 8;
	float a = 0.15f;
	float r,g,b;
	for (int y=0; y<logo->height; y++)
		for (int x=0; x<logo->width; x++)
		{
			if (x + xp < 0     ||     x + xp > width     ||     y + yp <= 0     ||     y + yp >= height)
				continue;
			Color4 l = logo->imgBuf[y][x];
			r = l.r * l.a*a * 255.0f + GetRValue(imgBuf[y+yp][x+xp])*(1.0f-l.a*a);
			g = l.g * l.a*a * 255.0f + GetGValue(imgBuf[y+yp][x+xp])*(1.0f-l.a*a);
			b = l.b * l.a*a * 255.0f + GetBValue(imgBuf[y+yp][x+xp])*(1.0f-l.a*a);
			imgBuf[y+yp][x+xp] = RGB(r,g,b);
		}
}

//-------------------------------------------------------------------------------------------------------------------------------------

void ImageByteBuffer::freeTimeStamp()
{
	if (timestamp)
		free(timestamp);
	timestamp = NULL;	
}

//-------------------------------------------------------------------------------------------------------------------------------------

void ImageByteBuffer::applyTimeStamp()
{
	if (!timestamp)
		return;

	// activate gdi+
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	int hhh = 35;

	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(width, hhh, PixelFormat32bppARGB);
	if (!bm)
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
		return ;
	}

	Gdiplus::Color c;
	int x, y;
	for (y=0; y<hhh; y++)
	{
		for (x=0; x<width; x++)
		{
			int y1 = y+height-hhh;
			c = Gdiplus::Color(Gdiplus::Color::MakeARGB(
				255,
				GetRValue(imgBuf[y1][x]),
				GetGValue(imgBuf[y1][x]),
				GetBValue(imgBuf[y1][x])));
			bm->SetPixel(x,y, c);
		}
	}

	size_t l2 = strlen(timestamp)+2;
	WCHAR * wtstamp = (WCHAR *)malloc(sizeof(WCHAR)*(l2));
	MultiByteToWideChar(CP_ACP, 0, timestamp, -1, wtstamp, (int)l2);

	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	LOGFONT lf;
	GetObject(hFont, sizeof(LOGFONT), &lf);
	float hh1 = 12;
	size_t l1 = strlen(lf.lfFaceName)+2;
	WCHAR * wface = (WCHAR *)malloc(sizeof(WCHAR)*(l1));
	MultiByteToWideChar(CP_ACP, 0, lf.lfFaceName, -1, wface, (int)l1);
	Gdiplus::FontFamily  * fontFamily = new Gdiplus::FontFamily(wface);
	Gdiplus::Font        * font = new Gdiplus::Font(fontFamily, hh1, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	Gdiplus::PointF        pointF = Gdiplus::PointF(3.0f, 2.0f);
	Gdiplus::SolidBrush  * solidBrush = new Gdiplus::SolidBrush(Gdiplus::Color(255, 255, 255, 255));
	Gdiplus::SolidBrush  * halfBrush = new Gdiplus::SolidBrush(Gdiplus::Color(128, 0, 0, 0));

	Gdiplus::Graphics * g = new Gdiplus::Graphics(bm);
	Gdiplus::RectF rect;
	g->MeasureString(wtstamp, -1, font, pointF, &rect);
	g->FillRectangle(halfBrush, rect);

	g->DrawString(wtstamp, -1, font, pointF, solidBrush);
	
	delete g;
	delete solidBrush;
	delete halfBrush;
	delete font;
	delete fontFamily;
	free(wface);
	free(wtstamp);

	for (y=0; y<hhh; y++)
	{
		for (x=0; x<width; x++)
		{
			int y1 = y+height-hhh;
			bm->GetPixel(x,y,&c);
			imgBuf[y1][x] = RGB(c.GetR(), c.GetG(), c.GetB());
		}
	}

	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	return;
}

//-------------------------------------------------------------------------------------------------------------------------------------

void ImageByteBuffer::copyToClipboard(HWND hWnd)
{
	if (!imgBuf)
		return;

	HBITMAP hBMP;
	int w = width;
	int h = height;
	unsigned char * buf = (unsigned char *)malloc(w*h*4);
	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			buf[4*(y*w+x)+0] = GetBValue(imgBuf[y][x]);
			buf[4*(y*w+x)+1] = GetGValue(imgBuf[y][x]);
			buf[4*(y*w+x)+2] = GetRValue(imgBuf[y][x]);
			buf[4*(y*w+x)+3] = 255;
		}
	hBMP = CreateBitmap(w,h, 1, 32, buf);
	free(buf);

	OpenClipboard(hWnd);
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, hBMP);
	CloseClipboard();

	DeleteObject(hBMP);
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool ImageByteBuffer::copyImageFrom(ImageByteBuffer * other)
{
	if (!other)
		return false;
	if (!imgBuf  ||  width!=other->width   ||   height!=other->height)
	{
		freeBuffer();
		bool aOK = allocBuffer(other->width, other->height);
		if (!aOK)
			return false;
	}

	for (int i=0; i<height; i++)
		memcpy(imgBuf[i], other->imgBuf[i], sizeof(COLORREF)*width);

	return true;
}

//-------------------------------------------------------------------------------------------------------------------------------------

ImageByteBuffer * ImageByteBuffer::copy()
{
	ImageByteBuffer * ibb = new ImageByteBuffer();
	if (!ibb)
		return NULL;

	if (!ibb->allocBuffer(width, height))
	{
		delete ibb;
		return NULL;
	}

	for (int i=0; i<height; i++)
	{
		if (!ibb->imgBuf[i]   ||   !imgBuf[i])
		{
			delete ibb;
			return NULL;
		}
		memcpy(ibb->imgBuf[i], imgBuf[i], sizeof(COLORREF)*width);
	}

	return ibb;
}

//-------------------------------------------------------------------------------------------------------------------------------------

ImageByteBuffer * ImageByteBuffer::copyWithZoom(double zoom)
{
	if (zoom < 0.001f)
		return NULL;
	double pzoom = 1.0 / zoom;
	double pmul = zoom * zoom;

	ImageByteBuffer * ibb = new ImageByteBuffer();
	if (!ibb)
		return NULL;

	int w,h;
	w = max(1, (int)(zoom * width));
	h = max(1, (int)(zoom * height));

	if (!ibb->allocBuffer(w,h))
	{
		delete ibb;
		return NULL;
	}

	float r,g,b;
	float px1, px2, py1, py2;
	int   ix1, ix2, iy1, iy2;
	float wx, wy, wg;
	for (int i=0; i<h; i++)
	{
		if (!ibb->imgBuf[i]   ||   !imgBuf[i])
		{
			delete ibb;
			return NULL;
		}

		py1 = (float)(i*pzoom);
		py2 = (float)((i+1)*pzoom);
		py1 = min(max(py1,0), height-1);
		py2 = min(max(py2,0), height-1);
		iy1 = (int)floor(py1);
		iy2 = (int)ceil (py2);
		for (int j=0; j<w; j++)
		{
			px1 = (float)(j*pzoom);
			px2 = (float)((j+1)*pzoom);
			px1 = min(max(px1,0), width-1);
			px2 = min(max(px2,0), width-1);
			ix1 = (int)floor(px1);
			ix2 = (int)ceil (px2);
			r = g = b = 0;
			for (int x=ix1; x<ix2; x++)
			{
				wx = 1.0f;
				if (x==ix1)
					wx = ix1 - px1 + 1;
				if (x==ix2-1)
					wx = px2 - ix2 + 1;

				for (int y=iy1; y<iy2; y++)
				{
					wy = 1.0f;
					if (y==iy1)
						wy = iy1 - py1 + 1;
					if (y==iy2-1)
						wy = py2 - iy2 + 1;

					wg = wx * wy;
					r += GetRValue(imgBuf[y][x])*wg;
					g += GetGValue(imgBuf[y][x])*wg;
					b += GetBValue(imgBuf[y][x])*wg;
				}
			}

			r = (float)(r*pmul);//*0.5;
			g = (float)(g*pmul);//*0.5;
			b = (float)(b*pmul);//*0.5;
			ibb->imgBuf[i][j] = RGB((int)r,(int)g,(int)b);
		}
	}

	return ibb;
}

//-------------------------------------------------------------------------------------------------------------------------------------

bool loadImageData(char * filename, ImageBuffer * imgbuf)
{
	if (!filename   ||   !imgbuf)
		return false;
	if (!imgbuf->imgBuf   ||   !imgbuf->hitsBuf)
		return false;
	if (imgbuf->width < 1   ||   imgbuf->height < 1)
		return false;
	for (int j=0; j<imgbuf->height; j++)
		if (imgbuf->imgBuf[j] == NULL   ||   imgbuf->hitsBuf[j] == NULL)
			return false;

	
	HANDLE hFile;
	hFile = CreateFile(filename,  GENERIC_READ,  0,  NULL,  OPEN_EXISTING,  FILE_ATTRIBUTE_NORMAL,  NULL);
	if (INVALID_HANDLE_VALUE == hFile)
		return false;

	int toRead = imgbuf->height * (sizeof(Color4)*imgbuf->width + sizeof(int)*imgbuf->width);
	int realsize;
	realsize = GetFileSize(hFile, NULL);

	if (realsize != toRead)
		return false;


	toRead = sizeof(Color4)*imgbuf->width;
	int read = 0;
	for (int j=0; j<imgbuf->height; j++)
	{
		BOOL ok = ReadFile(hFile, imgbuf->imgBuf[j], toRead, (LPDWORD)&read, NULL);
		if (!ok    ||    toRead != read)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	toRead = sizeof(int)*imgbuf->width;
	for (int j=0; j<imgbuf->height; j++)
	{
		BOOL ok = ReadFile(hFile, imgbuf->hitsBuf[j], toRead, (LPDWORD)&read, NULL);
		if (!ok    ||    toRead != read)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	CloseHandle(hFile);
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool loadFloatData(char * filename, FloatBuffer * fbuf)
{
	if (!filename   ||   !fbuf)
		return false;
	if (!fbuf->fbuf   ||   !fbuf->hbuf)
		return false;
	if (fbuf->width < 1   ||   fbuf->height < 1)
		return false;
	for (int j=0; j<fbuf->height; j++)
		if (fbuf->fbuf[j] == NULL   ||   fbuf->hbuf[j] == NULL)
			return false;

	
	HANDLE hFile;
	hFile = CreateFile(filename,  GENERIC_READ,  0,  NULL,  OPEN_EXISTING,  FILE_ATTRIBUTE_NORMAL,  NULL);
	if (INVALID_HANDLE_VALUE == hFile)
		return false;

	int toRead = fbuf->height * (sizeof(float)*fbuf->width + sizeof(int)*fbuf->width);
	int realsize;
	realsize = GetFileSize(hFile, NULL);

	if (realsize != toRead)
		return false;


	toRead = sizeof(float)*fbuf->width;
	int read = 0;
	for (int j=0; j<fbuf->height; j++)
	{
		BOOL ok = ReadFile(hFile, fbuf->fbuf[j], toRead, (LPDWORD)&read, NULL);
		if (!ok    ||    toRead != read)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	toRead = sizeof(int)*fbuf->width;
	for (int j=0; j<fbuf->height; j++)
	{
		BOOL ok = ReadFile(hFile, fbuf->hbuf[j], toRead, (LPDWORD)&read, NULL);
		if (!ok    ||    toRead != read)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	CloseHandle(hFile);
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool saveImageData(char * filename, ImageBuffer * imgbuf)
{
	if (!filename   ||   !imgbuf)
		return false;
	if (!imgbuf->imgBuf   ||   !imgbuf->hitsBuf)
		return false;
	if (imgbuf->width < 1   ||   imgbuf->height < 1)
		return false;
	for (int j=0; j<imgbuf->height; j++)
		if (imgbuf->imgBuf[j] == NULL   ||   imgbuf->hitsBuf[j] == NULL)
			return false;

	HANDLE hFile;
	hFile = CreateFile(filename,  GENERIC_WRITE,  0,  NULL,  CREATE_ALWAYS,  FILE_ATTRIBUTE_NORMAL,  NULL);
	if (INVALID_HANDLE_VALUE == hFile)
		return false;

	int toWrite = sizeof(Color4)*imgbuf->width;
	int written = 0;
	for (int j=0; j<imgbuf->height; j++)
	{
		BOOL ok = WriteFile(hFile, imgbuf->imgBuf[j], toWrite, (LPDWORD)&written, NULL);
		if (!ok    ||    toWrite != written)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	toWrite = sizeof(int)*imgbuf->width;
	for (int j=0; j<imgbuf->height; j++)
	{
		BOOL ok = WriteFile(hFile, imgbuf->hitsBuf[j], toWrite, (LPDWORD)&written, NULL);
		if (!ok    ||    toWrite != written)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	CloseHandle(hFile);
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool saveFloatData(char * filename, FloatBuffer * fbuf)
{
	if (!filename   ||   !fbuf)
		return false;
	if (!fbuf->fbuf  ||   !fbuf->hbuf)
		return false;
	if (fbuf->width < 1   ||   fbuf->height < 1)
		return false;
	for (int j=0; j<fbuf->height; j++)
		if (fbuf->fbuf[j] == NULL   ||   fbuf->hbuf[j] == NULL)
			return false;

	HANDLE hFile;
	hFile = CreateFile(filename,  GENERIC_WRITE,  0,  NULL,  CREATE_ALWAYS,  FILE_ATTRIBUTE_NORMAL,  NULL);
	if (INVALID_HANDLE_VALUE == hFile)
		return false;

	int toWrite = sizeof(float)*fbuf->width;
	int written = 0;
	for (int j=0; j<fbuf->height; j++)
	{
		BOOL ok = WriteFile(hFile, fbuf->fbuf[j], toWrite, (LPDWORD)&written, NULL);
		if (!ok    ||    toWrite != written)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	toWrite = sizeof(int)*fbuf->width;
	for (int j=0; j<fbuf->height; j++)
	{
		BOOL ok = WriteFile(hFile, fbuf->hbuf[j], toWrite, (LPDWORD)&written, NULL);
		if (!ok    ||    toWrite != written)
		{
			CloseHandle(hFile);
			return false;
		}
	}

	CloseHandle(hFile);
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool getCurveSettings(float * points, bool &useLuminance)
{
	//points size: 4*4*32 + 4*32 = 640 floats = 2560 bytes
	Raytracer * rtr = Raytracer::getInstance();
	if (!rtr->curScenePtr->curve)
		return false;
	if (!rtr->curScenePtr->curve->hasAnyPoint())
		return false;
	if (!points)
		return false;
	EMCurve * emc = rtr->curScenePtr->curve;

	useLuminance = emc->isLuminocityMode;

	for (int i=0; i<32; i++)
		points[0+i] = (float)emc->sortedPtsR[i].x;
	for (int i=0; i<32; i++)
		points[32+i] = (float)emc->sortedPtsG[i].x;
	for (int i=0; i<32; i++)
		points[64+i] = (float)emc->sortedPtsB[i].x;
	for (int i=0; i<32; i++)
		points[96+i] = (float)emc->sortedPtsL[i].x;

	for (int i=0; i<32; i++)
		points[128+0+i] = emc->fra[i];
	for (int i=0; i<32; i++)
		points[128+32+i] = emc->frb[i];
	for (int i=0; i<32; i++)
		points[128+64+i] = emc->frc[i];
	for (int i=0; i<32; i++)
		points[128+96+i] = emc->frd[i];

	for (int i=0; i<32; i++)
		points[256+0+i] = emc->fga[i];
	for (int i=0; i<32; i++)
		points[256+32+i] = emc->fgb[i];
	for (int i=0; i<32; i++)
		points[256+64+i] = emc->fgc[i];
	for (int i=0; i<32; i++)
		points[256+96+i] = emc->fgd[i];

	for (int i=0; i<32; i++)
		points[384+0+i] = emc->fba[i];
	for (int i=0; i<32; i++)
		points[384+32+i] = emc->fbb[i];
	for (int i=0; i<32; i++)
		points[384+64+i] = emc->fbc[i];
	for (int i=0; i<32; i++)
		points[384+96+i] = emc->fbd[i];

	for (int i=0; i<32; i++)
		points[512+0+i] = emc->fla[i];
	for (int i=0; i<32; i++)
		points[512+32+i] = emc->flb[i];
	for (int i=0; i<32; i++)
		points[512+64+i] = emc->flc[i];
	for (int i=0; i<32; i++)
		points[512+96+i] = emc->fld[i];

	return true;
}

//----------------------------------------------------------------------------------------------------------------

#ifdef USE_OPENCL_POSTPRO
ImageBuffer * makePostprocessOpenCL(ImageBuffer * src, int aa, ImageModifier * iMod, FinalModifier * fMod, FloatBuffer * depthBuf);
#endif

#define GRAIN_BEFORE_FILTER

//----------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::doPostProcess(int aa, ImageModifier * iMod, FinalModifier * fMod, FloatBuffer * depthBuf, char * message)
{
	if (aa < 1   ||   aa > 3)
		return NULL;
	Raytracer * rtr = Raytracer::getInstance();
	#ifdef USE_OPENCL_POSTPRO
	if (rtr->options.useOpenCLonPost  &&  rtr->options.openCLsupported)
	{
		ImageBuffer * iiibuf = makePostprocessOpenCL(this, aa, iMod, fMod, depthBuf);
		if (iiibuf)
			return iiibuf;
		Raytracer::getInstance()->options.useOpenCLonPost = false;
		Raytracer::getInstance()->updateOpenCLStateOnGui();
	}
	#endif

	float numSteps = 10;
	float cstep = 0;
	char * msg = NULL;
	if (message)
		msg = message;
	else 
		msg = "";

	int w = width/aa;
	int h = height/aa;
	ImageBuffer * iBuf1 = NULL;


	if (iMod->getDotsEnabled())
		removeBurnedDots(iMod->getDotsRadius(), iMod->getDotsThreshold(), iMod->getDotsDensity());

	if (fMod  &&  fMod->aerialEnabled)
	{
		applyAerialPerspective(depthBuf, fMod->aerialScale, iMod->getMultiplier(), rtr->curScenePtr->getActiveCamera(), rtr->curScenePtr->sscene.sunsky, true);
	}

	if (fMod  &&  fMod->fogEnabled  &&  fMod->fogDist>0.0f)	
	{
		float fogmpl = 1.0f/iMod->getMultiplier();
		Color4 c1;
		c1.r = pow(fMod->fogColor.r, 2.2f) * fogmpl;
		c1.g = pow(fMod->fogColor.g, 2.2f) * fogmpl;
		c1.b = pow(fMod->fogColor.b, 2.2f) * fogmpl;
		Color4 c2 = Color4(fogmpl,fogmpl,fogmpl);
		applyFog(depthBuf, fMod->fogDist, 100, c1, c2, 0, 1.0f/fMod->fogSpeed, fMod->fogExclSky);
	}


	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	ImageBuffer * iFBuf = NULL;
	if (fMod  &&  fMod->dofOn)
	{
		iFBuf = evalSimpleDOFImage(depthBuf, fMod);
	}

	if (!iFBuf)
		iFBuf = this;

	#ifdef GRAIN_BEFORE_FILTER
		if (fMod)
		{
			iFBuf->applyGrain(fMod->grain);
		}
	#endif


	if (aa==1)
	{
		iBuf1 = new ImageBuffer();
		bool ok = iBuf1->allocBuffer(w,h);
		if (!ok)
		{
			delete iBuf1;
			if (iFBuf != this)
			{
				iFBuf->freeBuffer();
				delete iFBuf;
			}
			return NULL;
		}

		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++)
			{
				int h = iFBuf->hitsBuf[y][x];
				if (h>0)
					iBuf1->imgBuf[y][x] = iFBuf->imgBuf[y][x] * (1.0f/h);
				else
					iBuf1->imgBuf[y][x] = Color4(0,0,0);
				iBuf1->hitsBuf[y][x] = 1;
			}
	}
	else
	{
		iBuf1 = iFBuf->applyFilter(iMod->getFilter(), w, h);
		if (!iBuf1)
		{
			if (iFBuf != this)
			{
				iFBuf->freeBuffer();
				delete iFBuf;
			}
			return NULL;
		}
	}

	if (iFBuf != this)
	{
		iFBuf->freeBuffer();
		delete iFBuf;
	}
	iFBuf = NULL;

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	iBuf1->vig_buck_pos_x = vig_buck_pos_x;
	iBuf1->vig_buck_pos_y = vig_buck_pos_y;
	iBuf1->vig_orig_width = vig_orig_width;
	iBuf1->vig_orig_height = vig_orig_height;
	iBuf1->applySimpleVignette(iMod->getVignette(), 1.0f);

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	int responsenumber = iMod->getResponseFunctionNumber();

	ImageBuffer * iBuf2 = NULL;
	if (fMod   &&   fMod->bloomEnabled)	
	{
		iBuf2 = iBuf1->applyFakeBloomFast(fMod);
	}
	else
	{
		iBuf2 = iBuf1->copyBuffer();
	}

	if (fMod   &&   fMod->glareEnabled)	
	{
		iBuf1->applyGlare(iBuf2, fMod);
	}

	iBuf1->freeBuffer();
	delete iBuf1;
	iBuf1 = NULL;


	if (fMod)
	{
		ImageBuffer * imgchrabb = iBuf2->evalChromaticAberrationPlanar(fMod->chromaticPlanarRed, fMod->chromaticPlanarGreen, fMod->chromaticPlanarBlue);
		if (imgchrabb)
		{
			iBuf2->freeBuffer();
			delete iBuf2;
			iBuf2 = imgchrabb;
		}
	}

	if (!iBuf2)
		return NULL;

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	float gamma = iMod->getGamma();
	if (responsenumber != 0)
		gamma = 1.0f;
	iBuf2->applyISOToneGamma(iMod->getMultiplier(), iMod->getToneMappingValue(), gamma);
	
	ImageBuffer * iBuf3 = iBuf2->applyCameraResponseFunction(responsenumber);
	iBuf2->freeBuffer();
	delete iBuf2;
	iBuf2 = iBuf3;

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	float br = iMod->getBrightness();
	if (br != 0)
		iBuf2->applyBrightness(br);

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	float con = iMod->getContrast();
	if (con != 1)
		iBuf2->applyContrast(con);

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	float sat = iMod->getSaturation();
	if (sat != 0)
		iBuf2->applySaturation(sat);

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);
	
	float addR = iMod->getR();
	float addG = iMod->getG();
	float addB = iMod->getB();
	if (addR != 0   ||   addG != 0   ||   addB != 0)
		iBuf2->applyRGBCorrection(addR, addG, addB);

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	float tmp = iMod->getTemperature();
	if (tmp != 6000   &&   iBuf2)
		iBuf2->applyTemperature(tmp);

	if (message)
		rtr->curScenePtr->notifyProgress(msg, cstep++/numSteps*100.0f);

	EMCurve * emc = GetEMCurveInstance(GetDlgItem(RendererMainWindow::hPost, IDC_REND_POST_CURVE));
	if (emc  &&  emc->hasAnyPoint())
		iBuf2->applyCurve(emc);

	if (rtr->curScenePtr->curve)
		if (rtr->curScenePtr->curve->hasAnyPoint())
			iBuf2->applyCurve(rtr->curScenePtr->curve);

	#ifndef GRAIN_BEFORE_FILTER
		if (fMod)
		{
			iBuf2->applyGrain(fMod->grain);
		}
	#endif

	if (message)
		rtr->curScenePtr->notifyProgress("", 0);

	return iBuf2;
}


//----------------------------------------------------------------------------------------------------------------

bool FloatBuffer::allocBuffer(int w, int h, bool sseAligned, bool allocHits)
{	
	freeBuffer();

	if (w<1 || h<1)
		return false;

	int i;
	width = w;
	height = h;
	sseAlign = sseAligned;

	int w4 = width;
	while (w4%4)
		w4++;

	if (allocHits)
	{
		hbuf = (unsigned int **)malloc(sizeof(unsigned int *) * height);
		if (!hbuf)
		{
			freeBuffer();
			return false;
		}

		for (i=0; i<height; i++)
			hbuf[i] = 0;


		for (i=0; i<height; i++)
		{
			if (sseAligned)
				hbuf[i] = (unsigned int *)_aligned_malloc(sizeof(unsigned int)*w4, 16);
			else
				hbuf[i] = (unsigned int *)malloc(sizeof(unsigned int)*width);

			if (!hbuf[i])
			{
				freeBuffer();
				return false;
			}
		}

	}

	fbuf = (float **)malloc(sizeof(float *) * height);
	if (!fbuf)
	{
		freeBuffer();
		return false;
	}

	for (i=0; i<height; i++)
		fbuf[i] = 0;


	for (i=0; i<height; i++)
	{
		if (sseAligned)
			fbuf[i] = (float *)_aligned_malloc(sizeof(float)*w4, 16);
		else
			fbuf[i] = (float *)malloc(sizeof(float)*width);

		if (!fbuf[i])
		{
			freeBuffer();
			return false;
		}
	}

	return true;
}

bool FloatBuffer::clearBuffer()
{
	if (!fbuf)
		return false;

	for (int y=0; y<height; y++)
		for (int x=0; x<width; x++)
			fbuf[y][x] = BIGFLOAT;

	for (int y=0; y<height; y++)
		for (int x=0; x<width; x++)
			hbuf[y][x] = 0;

	return true;
}

void FloatBuffer::freeBuffer()
{
	if (fbuf)
	{
		if (sseAlign)
		{
			for (int y=0; y<height; y++)
			{
				_aligned_free(fbuf[y]);
				fbuf[y] = NULL;
				_aligned_free(hbuf[y]);
				hbuf[y] = NULL;
			}
		}
		else
		{
			for (int y=0; y<height; y++)
			{
				free(fbuf[y]);
				fbuf[y] = NULL;
				free(hbuf[y]);
				hbuf[y] = NULL;
			}
		}
		free(fbuf);
		free(hbuf);
	}

	fbuf = NULL;
	hbuf = NULL;
	sseAlign = false;
	width = 0;
	height = 0;
}

bool FloatBuffer::copyFromOther(FloatBuffer * other)
{
	if (!other)
		return false;
	if (!other->fbuf)
		return false;
	if (other->width<1 || other->height<1)
		return false;

	bool algn = other->isSSEaligned();
	bool allochits = (other->hbuf!=NULL);



	bool ok = allocBuffer(other->width, other->height, algn, allochits);
	
	if (!ok)
		return false;

	for (int y=0; y<height; y++)
		memcpy(fbuf[y], other->fbuf[y], width * sizeof(float));

	if (allochits)
		for (int y=0; y<height; y++)
			memcpy(hbuf[y], other->hbuf[y], width * sizeof(unsigned int));

	return true;
}

FloatBuffer * FloatBuffer::copyToNew()
{
	FloatBuffer * res = new FloatBuffer();
	bool ok = res->copyFromOther(this);
	if (!ok)
	{
		delete res;
		return NULL;
	}

	return res;
}

bool FloatBuffer::isSSEaligned()
{
	return sseAlign;
}

bool FloatBuffer::copyAsDOFTemperatureToByteBuffer(ImageByteBuffer * img, float dofDist, int aa, float focal, float aperture)
{
	CHECK(img);
	CHECK(fbuf);
	CHECK(hbuf);
	CHECK(width>0  &&  height>0);
	CHECK(img->imgBuf);
	CHECK(img->width>0  &&  img->height>0);

	aa = min(max(aa, 1), 3);

	int ww = min(img->width, width/aa);
	int hh = min(img->height, height/aa);

	img->clearBuffer();

	float gamma = 1.0f / 2.2f;

	if (dofDist < 0.01f)
		dofDist = 0.01f;

	float maxdiff = 0.2f;
	float perdofdist = 1.0f/dofDist;
	float permaxdiff = 1.0f/maxdiff;
	float dofmaxdiff = dofDist * maxdiff;
	float perdofmaxdiff = 1.0f/dofmaxdiff;


	if (1)	// sse
	{
		while (ww%4)
			ww++;
		int wwp4 = ww/4;

		if (aa==1)
		{
			unsigned int mabs = 0x7FFFFFFF;
			__m128 sseOne = _mm_set1_ps(1.0f);
			__m128 sseZero = _mm_set1_ps(0.0f);
			__m128 sse255 = _mm_set1_ps(255.0f);
			__m128 sseAbsMask = _mm_set1_ps((float&)mabs);
			__m128 ssedofmaxdiff = _mm_set1_ps(dofmaxdiff);
			__m128 ssedofDist = _mm_set1_ps(dofDist);
			__m128 sseperdofmaxdiff = _mm_set1_ps(perdofmaxdiff);

			float rl = ww*focal*focal   /   (2*dofDist*aperture*0.036f);
			__m128 sserl = _mm_set1_ps(rl);
			__m128 sseperrl = _mm_set1_ps(1.0f/rl);

			for (int y=0; y<hh; y++)
			{
				__m128  * ssefbuf = (__m128  *)fbuf[y];
				__m128i * ssehbuf = (__m128i *)hbuf[y];
				for (int x=0; x<wwp4; x++)
				{
					__m128 sVal = ssefbuf[x];								// vector from array	(dist sum)
					__m128i hVal = ssehbuf[x];								// vector from array	(hits)
					__m128 fhVal = _mm_cvtepi32_ps(hVal);					// convert int to float

					fhVal = _mm_max_ps(fhVal, sseOne);						// make sure it is not zero
					__m128 sseDepth = _mm_div_ps(sVal, fhVal);				// dist = dist sum / hits

					__m128 sseDiff = _mm_sub_ps(ssedofDist, sseDepth);		// dist - depth
					sseDiff = _mm_and_ps(sseDiff, sseAbsMask);				// abs (dist - depth)
					sseDiff = _mm_rcp_ps(sseDiff);							// approx 1.0f / abs(dist - depth)
					__m128 ssetemp = _mm_mul_ps(sseDepth, sseperrl);		// depth / rl
					sseDiff = _mm_mul_ps(ssetemp, sseDiff);					// mul both above

					sseDiff = _mm_max_ps(sseDiff, sseZero);					// not less than 0
					sseDiff = _mm_min_ps(sseDiff, sseOne);					// not higher than 1	
					sseDiff = _mm_mul_ps(sseDiff, sse255);					// multiply by 255

					__m128i sseInt = _mm_cvtps_epi32(sseDiff);				// convert to int
					__m128i sseInt1 = _mm_slli_epi32(sseInt, 8);			// propagate R to G and B
					__m128i sseInt2 = _mm_slli_epi32(sseInt1, 8);
					sseInt = _mm_or_si128(sseInt, sseInt1);
					sseInt = _mm_or_si128(sseInt, sseInt2);
					(__m128i&)(img->imgBuf[y][x*4]) = sseInt;				// finito
				}
			}
		}

		if (aa==2)
		{
			unsigned int mabs = 0x7FFFFFFF;
			__m128 sseOne = _mm_set1_ps(1.0f);
			__m128 sseZero = _mm_set1_ps(0.0f);
			__m128 sse255 = _mm_set1_ps(255.0f);
			__m128 sseHalf = _mm_set1_ps(0.5f);
			__m128 sseAbsMask = _mm_set1_ps((float&)mabs);
			__m128 ssedofmaxdiff = _mm_set1_ps(dofmaxdiff);
			__m128 ssedofDist = _mm_set1_ps(dofDist);
			__m128 sseperdofmaxdiff = _mm_set1_ps(perdofmaxdiff);

			float rl = ww*focal*focal   /   (2*dofDist*aperture*0.036f);
			__m128 sserl = _mm_set1_ps(rl);
			__m128 sseperrl = _mm_set1_ps(1.0f/rl);


			for (int y=0; y<hh; y++)
			{
				int y2 = y*2;
				__m128  * ssefbuf = (__m128  *)fbuf[y2];
				__m128i * ssehbuf = (__m128i *)hbuf[y2];
				for (int x=0; x<wwp4; x++)
				{
					int x2 = x*2;
					__m128 sVal1  = ssefbuf[x2];								// vector from array	(dist sum)
					__m128i hVal1 = ssehbuf[x2];								// vector from array	(hits)
					__m128 sVal2  = ssefbuf[x2+1];								// vector from array	(dist sum)
					__m128i hVal2 = ssehbuf[x2+1];								// vector from array	(hits)
					__m128 fhVal1 = _mm_cvtepi32_ps(hVal1);						// convert int to float
					__m128 fhVal2 = _mm_cvtepi32_ps(hVal2);						// convert int to float

					fhVal1 = _mm_max_ps(fhVal1, sseOne);						// make sure it is not zero
					fhVal2 = _mm_max_ps(fhVal2, sseOne);						// make sure it is not zero
					__m128 sseDepth1 = _mm_div_ps(sVal1, fhVal1);				// dist = dist sum / hits
					__m128 sseDepth2 = _mm_div_ps(sVal2, fhVal2);				// dist = dist sum / hits

					__m128 sseUnpack1LO = _mm_unpacklo_ps(sseDepth1, sseDepth2);
					__m128 sseUnpack1HI = _mm_unpackhi_ps(sseDepth1, sseDepth2);
					__m128 sseUnpack2LO = _mm_unpacklo_ps(sseUnpack1LO, sseUnpack1HI);
					__m128 sseUnpack2HI = _mm_unpackhi_ps(sseUnpack1LO, sseUnpack1HI);

					__m128 sseDepth = _mm_min_ps(sseUnpack2LO, sseUnpack2HI);

					__m128 sseDiff = _mm_sub_ps(ssedofDist, sseDepth);		// dist - depth
					sseDiff = _mm_and_ps(sseDiff, sseAbsMask);				// abs (dist - depth)
					sseDiff = _mm_rcp_ps(sseDiff);							// approx 1.0f / abs(dist - depth)
					__m128 ssetemp = _mm_mul_ps(sseDepth, sseperrl);		// depth / rl
					sseDiff = _mm_mul_ps(ssetemp, sseDiff);					// mul both above


					sseDiff = _mm_max_ps(sseDiff, sseZero);					// not less than 0
					sseDiff = _mm_min_ps(sseDiff, sseOne);					// not higher than 1	
					sseDiff = _mm_mul_ps(sseDiff, sse255);					// multiply by 255


					__m128i sseInt = _mm_cvtps_epi32(sseDiff);				// convert to int
					__m128i sseInt1 = _mm_slli_epi32(sseInt, 8);			// propagate R to G and B
					__m128i sseInt2 = _mm_slli_epi32(sseInt1, 8);
					sseInt = _mm_or_si128(sseInt, sseInt1);
					sseInt = _mm_or_si128(sseInt, sseInt2);
					(__m128i&)(img->imgBuf[y][x*4]) = sseInt;				// finito
				}
			}
		}

		if (aa==3)
		{
			float rl = ww*focal*focal   /   (2*dofDist*aperture*0.036f);
			for (int y=0; y<hh; y++)
			{
				for (int x=0; x<ww; x++)
				{
					float val = fbuf[y*3+1][x*3+1];
					unsigned int hits = hbuf[y*3+1][x*3+1];
					float perhits = hits ? 1.0f/hits : 0.0f;
					val *= perhits;
					val = min(1.0f, val/fabs(rl*(dofDist-val)));
					float gval = val;
					unsigned char c = (unsigned char)min(max(255.0f*gval, 0.0f), 255.0f);
					img->imgBuf[y][x] = RGB(c,c,c);
				}
			}
		}
	}

	return true;
}

bool FloatBuffer::copyAsDepthToByteBuffer(ImageByteBuffer * img, int aa)
{
	CHECK(img);
	CHECK(fbuf);
	CHECK(hbuf);
	CHECK(width>0  &&  height>0);
	CHECK(img->imgBuf);
	CHECK(img->width>0  &&  img->height>0);

	float minval = BIGFLOAT;
	float maxval = -BIGFLOAT;

	aa = min(max(aa, 1), 3);

	img->clearBuffer();

	int w = min(width/aa, img->width);
	int h = min(height/aa, img->height);

	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			float val = fbuf[y*aa][x*aa];
			int hits = hbuf[y*aa][x*aa];
			if (hits>0  &&  val<1000.0f*hits)
			{
				minval = min(minval, val/hits);
				maxval = max(maxval, val/hits);
			}
		}

	float diff = maxval-minval;
	if (fabs(diff)<0.00001f  ||   fabs(diff)>10000   ||   minval>1000)
		return true;

	float perdiff = fabs(1.0f/diff);
	float gamma = 1.0f / 2.2f;

	for (int y=0; y<h; y++)
	{
		for (int x=0; x<w; x++)
		{
			float val = fbuf[y*aa][x*aa];
			unsigned int hits = hbuf[y*aa][x*aa];
			float perhits = hits ? 1.0f/hits : 0.0f;
			val *= perhits;
			val -= minval;
			val *= perdiff;
			val = max(0, 1.0f-val);
			float gval = val;
			unsigned char c = (unsigned char)min(max(255.0f*gval, 0.0f), 255.0f);
			img->imgBuf[y][x] = RGB(c,c,c);
		}
	}

	return true;
}

FloatBuffer::FloatBuffer()
{
	sseAlign = false;
	width = height = 0;
	fbuf = NULL;
	hbuf = NULL;
}

FloatBuffer::~FloatBuffer()
{
	freeBuffer();
}

//----------------------------------------------------------------------------------------------------------------

HBITMAP loadWindowsImage(char * filename)
{
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	size_t l = strlen(filename)+2;
	WCHAR * wch = (WCHAR *)malloc(sizeof(WCHAR)*(l));
	MultiByteToWideChar(CP_ACP, 0, filename, -1, wch, (int)l);

	Gdiplus::Bitmap * bm = Gdiplus::Bitmap::FromFile(wch);
	if (!bm)
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
		free(wch);
		return 0;
	}
	free(wch);

	HBITMAP res;
	Gdiplus::Color c;
	if (Gdiplus::Ok != bm->GetHBITMAP(c, &res))
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
		delete bm;
		return 0;
	}

	Gdiplus::GdiplusShutdown(gdiplusToken);

	return res;
}

//----------------------------------------------------------------------------------------------------------------

void FinalModifier::copyDataFromOther(FinalModifier * fMod, bool dofTab, bool effectsTab)
{
	if (!fMod)
		return;

	if (dofTab)
	{
		chromaticShiftLens = fMod->chromaticShiftLens;
		chromaticShiftDepth = fMod->chromaticShiftDepth;
		chromaticAchromatic = fMod->chromaticAchromatic;

		chromaticPlanarRed = fMod->chromaticPlanarRed;
		chromaticPlanarGreen = fMod->chromaticPlanarGreen;
		chromaticPlanarBlue = fMod->chromaticPlanarBlue;

		focal = fMod->focal;
		aperture = fMod->aperture;
		focusDistance = fMod->focusDistance;
		quality = fMod->quality;
		numThreads = fMod->numThreads;
		algorithm = fMod->algorithm;

		bokehRingBalance = fMod->bokehRingBalance;
		bokehRingSize = fMod->bokehRingSize;
		bokehFlatten = fMod->bokehFlatten;
		bokehVignette = fMod->bokehVignette;

		bladesNum = fMod->bladesNum;
		bladesAngle = fMod->bladesAngle;
		bladesRadius = fMod->bladesRadius;
		roundBlades = fMod->roundBlades;
		dofOn = fMod->dofOn;
	}

	if (effectsTab)
	{
		grain = fMod->grain;

		bloomEnabled = fMod->bloomEnabled;
		bloomPower = fMod->bloomPower;
		bloomArea = fMod->bloomArea;
		bloomThreshold = fMod->bloomThreshold;
		bloomAttenuationOn = fMod->bloomAttenuationOn;
		bloomAttenuationColor = fMod->bloomAttenuationColor;
		glareEnabled = fMod->glareEnabled;
		glarePower = fMod->glarePower;
		glareDispersion = fMod->glareDispersion;
		glareThreshold = fMod->glareThreshold;
		glareArea = fMod->glareArea;
		glareTextureApertureOn = fMod->glareTextureApertureOn;
		glareTextureObstacleOn = fMod->glareTextureObstacleOn;

		fogColor = fMod->fogColor;
		fogDist = fMod->fogDist;
		fogSpeed = fMod->fogSpeed;
		fogExclSky = fMod->fogExclSky;
		fogEnabled = fMod->fogEnabled;

		aerialEnabled = fMod->aerialEnabled;
		aerialScale = fMod->aerialScale;
	}

	vignette = fMod->vignette;

	dotsEnabled = fMod->dotsEnabled;
	dotsRadius = fMod->dotsRadius;
	dotsThreshold = fMod->dotsThreshold;
	dotsDensity = fMod->dotsDensity;
}

//----------------------------------------------------------------------------------------------------------------
