#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EM2Controls.h"
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "noxfonts.h"
#include "log.h"

#define TIMER_REDRAW_TEX2_ID 151

//------------------------------------------------------------------------------------------------

extern HMODULE hDllModule;
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
void texDlg2UpdateBgShifts(HWND hWnd);
void texDlg2SetPositionsOnStart(HWND hWnd);
void texDlg2LockAll(HWND hWnd, bool lock);
bool texDlg2UpdateTexData(HWND hWnd, Texture * tex);
bool texDlg2UpdateGUItoTexMod(HWND hWnd, Texture * tex);
bool texDlg2UpdateTexModToGui(HWND hWnd, Texture * tex);
bool texDlg2RunRefreshThread(HWND hWnd, Texture * tex);
char * openTexDialog(HWND hwnd);


//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK Tex2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBmp = 0;
	static Texture * tex1 = NULL;
	static Texture * tex2 = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBmp = generateBackgroundPattern(1920, 1200, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_OK));
				embOK->bgImage = hBgBmp;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_CANCEL));
				embCancel->bgImage = hBgBmp;
				embCancel->setFont(fonts->em2button, false);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TITLE));
				emtitle->bgImage = hBgBmp;
				emtitle->setFont(fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				//  TEXTURE GROUP
				EM2GroupBar * emgrTex = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_TEX2_GR_TEXTURE));
				emgrTex->bgImage = hBgBmp;
				emgrTex->setFont(fonts->em2groupbar, false);

				EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_TEX2_PREVIEW));

				EM2Text * emtRes= GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_RES));
				emtRes->bgImage = hBgBmp;
				emtRes->setFont(fonts->em2text, false);

				EM2Text * emtType = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TYPE));
				emtType->bgImage = hBgBmp;
				emtType->setFont(fonts->em2text, false);

				EM2Button * embLoad = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_LOAD));
				embLoad->bgImage = hBgBmp;
				embLoad->setFont(fonts->em2button, false);

				EM2Button * embUnload = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_UNLOAD));
				embUnload->bgImage = hBgBmp;
				embUnload->setFont(fonts->em2button, false);

				EM2Button * embPick = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_PICK));
				embPick->bgImage = hBgBmp;
				embPick->setFont(fonts->em2button, false);

				EM2Text * emtName = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_NAME));
				emtName->bgImage = hBgBmp;
				emtName->setFont(fonts->em2text, false);

				//  COLOR CORRECTION GROUP
				EM2GroupBar * emgrColCorr = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_TEX2_GR_COLOR_CORRECTION));
				emgrColCorr->bgImage = hBgBmp;
				emgrColCorr->setFont(fonts->em2groupbar, false);

				EM2Text * emtBri = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_BRIGHTNESS));
				emtBri->bgImage = hBgBmp;
				emtBri->setFont(fonts->em2text, false);
				EM2Text * emtCon = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_CONTRAST));
				emtCon->bgImage = hBgBmp;
				emtCon->setFont(fonts->em2text, false);
				EM2Text * emtSat = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_SATURATION));
				emtSat->bgImage = hBgBmp;
				emtSat->setFont(fonts->em2text, false);
				EM2Text * emtHue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_HUE));
				emtHue->bgImage = hBgBmp;
				emtHue->setFont(fonts->em2text, false);
				EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_RED));
				emtRed->bgImage = hBgBmp;
				emtRed->setFont(fonts->em2text, false);
				EM2Text * emtGre = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_GREEN));
				emtGre->bgImage = hBgBmp;
				emtGre->setFont(fonts->em2text, false);
				EM2Text * emtBlu = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_BLUE));
				emtBlu->bgImage = hBgBmp;
				emtBlu->setFont(fonts->em2text, false);
				EM2Text * emtGam = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_GAMMA));
				emtGam->bgImage = hBgBmp;
				emtGam->setFont(fonts->em2text, false);

				int editwidth = 38;
				EM2EditTrackBar * emetBri = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS));
				emetBri->setFont(fonts->em2edittrack, false);
				emetBri->bgImage = hBgBmp;
				emetBri->setEditBoxWidth(editwidth, 5);
				emetBri->setValuesToFloat(TEXMOD_DEF_BRIGHTNESS, TEXMOD_MIN_BRIGHTNESS, TEXMOD_MAX_BRIGHTNESS, TEXMOD_DEF_BRIGHTNESS, 0.05f);
				EM2EditTrackBar * emetCon = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_CONTRAST));
				emetCon->setFont(fonts->em2edittrack, false);
				emetCon->bgImage = hBgBmp;
				emetCon->setEditBoxWidth(editwidth, 5);
				emetCon->setValuesToFloat(TEXMOD_DEF_CONTRAST, TEXMOD_MIN_CONTRAST, TEXMOD_MAX_CONTRAST, TEXMOD_DEF_CONTRAST, 0.05f);
				EM2EditTrackBar * emetSat = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_SATURATION));
				emetSat->setFont(fonts->em2edittrack, false);
				emetSat->bgImage = hBgBmp;
				emetSat->setEditBoxWidth(editwidth, 5);
				emetSat->setValuesToFloat(TEXMOD_DEF_SATURATION, TEXMOD_MIN_SATURATION, TEXMOD_MAX_SATURATION, TEXMOD_DEF_SATURATION, 0.05f);
				EM2EditTrackBar * emetHue = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_HUE));
				emetHue->setFont(fonts->em2edittrack, false);
				emetHue->bgImage = hBgBmp;
				emetHue->setEditBoxWidth(editwidth, 5);
				emetHue->setValuesToFloat(TEXMOD_DEF_HUE, TEXMOD_MIN_HUE, TEXMOD_MAX_HUE, TEXMOD_DEF_HUE, 0.05f);
				EM2EditTrackBar * emetRed = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_RED));
				emetRed->setFont(fonts->em2edittrack, false);
				emetRed->bgImage = hBgBmp;
				emetRed->setEditBoxWidth(editwidth, 5);
				emetRed->setValuesToFloat(TEXMOD_DEF_RED, TEXMOD_MIN_RED, TEXMOD_MAX_RED, TEXMOD_DEF_RED, 0.05f);
				EM2EditTrackBar * emetGre = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GREEN));
				emetGre->setFont(fonts->em2edittrack, false);
				emetGre->bgImage = hBgBmp;
				emetGre->setEditBoxWidth(editwidth, 5);
				emetGre->setValuesToFloat(TEXMOD_DEF_GREEN, TEXMOD_MIN_GREEN, TEXMOD_MAX_GREEN, TEXMOD_DEF_GREEN, 0.05f);
				EM2EditTrackBar * emetBlu = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BLUE));
				emetBlu->setFont(fonts->em2edittrack, false);
				emetBlu->bgImage = hBgBmp;
				emetBlu->setEditBoxWidth(editwidth, 5);
				emetBlu->setValuesToFloat(TEXMOD_DEF_BLUE, TEXMOD_MIN_BLUE, TEXMOD_MAX_BLUE, TEXMOD_DEF_BLUE, 0.05f);
				EM2EditTrackBar * emetGam = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GAMMA));
				emetGam->setFont(fonts->em2edittrack, false);
				emetGam->bgImage = hBgBmp;
				emetGam->setEditBoxWidth(editwidth, 5);
				emetGam->setValuesToFloat(TEXMOD_DEF_GAMMA, TEXMOD_MIN_GAMMA, TEXMOD_MAX_GAMMA, TEXMOD_DEF_GAMMA, 0.1f);

				EM2CheckBox * emcBilinear = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INTERPOLATION));
				emcBilinear->bgImage = hBgBmp;
				emcBilinear->setFont(fonts->em2button, false);
				emcBilinear->selected = TEXMOD_DEF_INTERPOLATE;

				EM2CheckBox * emcInvert = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INVERT));
				emcInvert->bgImage = hBgBmp;
				emcInvert->setFont(fonts->em2button, false);
				emcInvert->selected = TEXMOD_DEF_INVERT;

				EM2Button * embReset = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_RESET_ALL));
				embReset->bgImage = hBgBmp;
				embReset->setFont(fonts->em2button, false);

				texDlg2SetPositionsOnStart(hWnd);
				texDlg2UpdateBgShifts(hWnd);

				tex1 = (Texture *)lParam;
				tex2 = new Texture();
				if (tex1)
				{
					tex2->copyFromOther(tex1);
				}
				
				texDlg2UpdateTexData(hWnd, tex2);
				texDlg2UpdateTexModToGui(hWnd, tex2);
				texDlg2RunRefreshThread(hWnd, tex2);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBmp)
					DeleteObject(hBgBmp);
				hBgBmp = 0;
			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBmp)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBmp);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}
				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_TIMER:
			{
				if (wParam == TIMER_REDRAW_TEX2_ID)
				{
					KillTimer(hWnd, TIMER_REDRAW_TEX2_ID);
					texDlg2RunRefreshThread(hWnd, tex2);
				}
			}
			break;
		case WM_CLOSE:
			{
				SendMessage(hWnd, WM_COMMAND, MAKELONG(IDC_TEX2_CANCEL, BN_CLICKED), (LPARAM)GetDlgItem(hWnd, IDC_TEX2_CANCEL));
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_TEX2_OK:
						{
							CHECK(wmEvent==BN_CLICKED);

							if (tex1)
							{
								tex1->freeAllBuffers();
								delete tex1;
								tex1 = NULL;
							}

							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_TEX2_PREVIEW));
							if (empv  &&  empv->byteBuff)
							{
								empv->byteBuff->freeBuffer();
								delete empv->byteBuff;
								empv->byteBuff = NULL;
							}

							EndDialog(hWnd, (INT_PTR)(tex2));
						}
						break;
					case IDC_TEX2_CANCEL:
						{
							CHECK(wmEvent==BN_CLICKED);

							if (tex1)
							{
								tex1->freeAllBuffers();
								delete tex1;
								tex1 = NULL;
							}
							if (tex2)
							{
								tex2->freeAllBuffers();
								delete tex2;
								tex2 = NULL;
							}

							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_TEX2_PREVIEW));
							if (empv  &&  empv->byteBuff)
							{
								empv->byteBuff->freeBuffer();
								delete empv->byteBuff;
								empv->byteBuff = NULL;
							}

							EndDialog(hWnd, (INT_PTR)0);
						}
						break;
					case IDC_TEX2_LOAD:
						{
							CHECK(wmEvent==BN_CLICKED);
							KillTimer(hWnd, TIMER_REDRAW_TEX2_ID);
							Logger::add("Texture load clicked.");

							char * fname = openTexDialog(hWnd);
							if (!fname)
							{
								Logger::add("No file selected.");
								break;
							}
							Logger::add("Loading file:");
							Logger::add(fname);

							texDlg2LockAll(hWnd, true);

							Texture * newtex = new Texture();
							if (newtex->loadFromFile(fname)  &&   newtex->isValid())
							{
								if (tex2)
								{
									tex2->freeAllBuffers();
									delete tex2;
								}
								tex2 = newtex;

								texDlg2UpdateTexData(hWnd, tex2);

								texDlg2RunRefreshThread(hWnd, tex2);

								// update zoom

								//texDlg2LockAll(hWnd, false);	// temporary here
							}
							else
								texDlg2LockAll(hWnd, false);
							if (fname)
								free(fname);
						}
						break;
					case IDC_TEX2_UNLOAD:
						{
							CHECK(wmEvent==BN_CLICKED);
							KillTimer(hWnd, TIMER_REDRAW_TEX2_ID);
							Texture * newtex = new Texture();

							if (tex2)
							{
								tex2->freeAllBuffers();
								delete tex2;
							}
							tex2 = newtex;

							texDlg2UpdateTexData(hWnd, tex2);
							Logger::add("relased texture");

							texDlg2RunRefreshThread(hWnd, tex2);
						}
						break;
					case IDC_TEX2_PICK:
						{
							char * dstfname = (char *)DialogBoxParamW(hDllModule, 
											MAKEINTRESOURCEW(IDD_TEXTURE_PICKER2), hWnd, TexPicker2DlgProc,(LPARAM)(tex2 ? tex2->fullfilename : NULL));

							if (dstfname)
							{
								if (tex2  &&  tex2->fullfilename  &&  !strcmp(tex2->fullfilename, dstfname))
									break;
								Logger::add("Loading file:");
								Logger::add(dstfname);

								texDlg2LockAll(hWnd, true);

								Texture * newtex = new Texture();
								if (newtex->loadFromFile(dstfname)  &&   newtex->isValid())
								{
									if (tex2)
									{
										tex2->freeAllBuffers();
										delete tex2;
									}
									tex2 = newtex;

									texDlg2UpdateTexData(hWnd, tex2);

									texDlg2RunRefreshThread(hWnd, tex2);

									// update zoom
								}
								else
									texDlg2LockAll(hWnd, false);
								if (dstfname)
									free(dstfname);
							}
							else
							{
							}
						}
						break;
					case IDC_TEX2_BRIGHTNESS:
					case IDC_TEX2_CONTRAST:
					case IDC_TEX2_SATURATION:
					case IDC_TEX2_HUE:
					case IDC_TEX2_RED:
					case IDC_TEX2_GREEN:
					case IDC_TEX2_BLUE:
					case IDC_TEX2_GAMMA:
					case IDC_TEX2_INVERT:
					case IDC_TEX2_INTERPOLATION:
						{
							CHECK(wmEvent==WM_HSCROLL || wmEvent==BN_CLICKED);
							texDlg2UpdateGUItoTexMod(hWnd, tex2);
							if (IDC_TEX2_GAMMA==wmId)
								tex2->texMod.updateGamma(tex2->texMod.gamma);
							SetTimer(hWnd, TIMER_REDRAW_TEX2_ID, 500, NULL);
						}
						break;
					case IDC_TEX2_RESET_ALL:
						{
							tex2->texMod.reset();
							texDlg2UpdateTexModToGui(hWnd, tex2);
							texDlg2RunRefreshThread(hWnd, tex2);
						}
						break;
				}
			}	// end WM_COMMAND
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

void texDlg2UpdateBgShifts(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgrTexture = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_TEX2_GR_TEXTURE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_GR_TEXTURE), 0, 0, emgrTexture->bgShiftX, emgrTexture->bgShiftY);
	EM2GroupBar * emgrColCorr = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_TEX2_GR_COLOR_CORRECTION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_GR_COLOR_CORRECTION), 0, 0, emgrColCorr->bgShiftX, emgrColCorr->bgShiftY);

	EM2Text * emtRes = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_RES));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_RES), 0, 0, emtRes->bgShiftX, emtRes->bgShiftY);
	EM2Text * emtType = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TYPE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TYPE), 0, 0, emtType->bgShiftX, emtType->bgShiftY);
	EM2Text * emtName = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_NAME));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_NAME), 0, 0, emtName->bgShiftX, emtName->bgShiftY);

	EM2Button * embLoad = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_LOAD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_LOAD), 0, 0, embLoad->bgShiftX, embLoad->bgShiftY);
	EM2Button * embUnload = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_UNLOAD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_UNLOAD), 0, 0, embUnload->bgShiftX, embUnload->bgShiftY);
	EM2Button * embPick = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_PICK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_PICK), 0, 0, embPick->bgShiftX, embPick->bgShiftY);

	EM2Text * emtBri = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_BRIGHTNESS));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_BRIGHTNESS), 0, 0, emtBri->bgShiftX, emtBri->bgShiftY);
	EM2Text * emtCon = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_CONTRAST));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_CONTRAST), 0, 0, emtCon->bgShiftX, emtCon->bgShiftY);
	EM2Text * emtSat = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_SATURATION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_SATURATION), 0, 0, emtSat->bgShiftX, emtSat->bgShiftY);
	EM2Text * emtHue = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_HUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_HUE), 0, 0, emtHue->bgShiftX, emtHue->bgShiftY);
	EM2Text * emtRed = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_RED));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_RED), 0, 0, emtRed->bgShiftX, emtRed->bgShiftY);
	EM2Text * emtGre = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_GREEN));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_GREEN), 0, 0, emtGre->bgShiftX, emtGre->bgShiftY);
	EM2Text * emtBlu = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_BLUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_BLUE), 0, 0, emtBlu->bgShiftX, emtBlu->bgShiftY);
	EM2Text * emtGam = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEX2_TEXT_GAMMA));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_TEXT_GAMMA), 0, 0, emtGam->bgShiftX, emtGam->bgShiftY);
	
	EM2EditTrackBar * emetBri = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS), 0, 0, emetBri->bgShiftX, emetBri->bgShiftY);
	EM2EditTrackBar * emetCon = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_CONTRAST));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_CONTRAST), 0, 0, emetCon->bgShiftX, emetCon->bgShiftY);
	EM2EditTrackBar * emetSat = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_SATURATION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_SATURATION), 0, 0, emetSat->bgShiftX, emetSat->bgShiftY);
	EM2EditTrackBar * emetHue = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_HUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_HUE), 0, 0, emetHue->bgShiftX, emetHue->bgShiftY);
	EM2EditTrackBar * emetRed = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_RED));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_RED), 0, 0, emetRed->bgShiftX, emetRed->bgShiftY);
	EM2EditTrackBar * emetGre = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GREEN));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_GREEN), 0, 0, emetGre->bgShiftX, emetGre->bgShiftY);
	EM2EditTrackBar * emetBlu = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BLUE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_BLUE), 0, 0, emetBlu->bgShiftX, emetBlu->bgShiftY);
	EM2EditTrackBar * emetGam = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GAMMA));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_GAMMA), 0, 0, emetGam->bgShiftX, emetGam->bgShiftY);

	EM2CheckBox * emcInterp = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INTERPOLATION));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_INTERPOLATION), 0, 0, emcInterp->bgShiftX, emcInterp->bgShiftY);
	EM2CheckBox * emcInvert = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INVERT));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_INVERT), 0, 0, emcInvert->bgShiftX, emcInvert->bgShiftY);

	EM2Button * embReset = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_RESET_ALL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_RESET_ALL), 0, 0, embReset->bgShiftX, embReset->bgShiftY);

	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);
	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEX2_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEX2_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void texDlg2SetPositionsOnStart(HWND hWnd)
{
	RECT tmprect, wrect,crect;
	GetWindowRect(hWnd, &wrect);
	GetClientRect(hWnd, &crect);
	tmprect.left = 0;		tmprect.top = 0;
	tmprect.right = 758 + wrect.right-wrect.left-crect.right;
	tmprect.bottom = 381 + wrect.bottom-wrect.top-crect.bottom;
	SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);

	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TITLE), HWND_TOP, 299,6, 160, 17, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_OK),					HWND_TOP,			294,	341,		75,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_CANCEL),				HWND_TOP,			389,	341,		75,		22,			0);


	int x,y;
	x=9; y=34;
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_GR_TEXTURE),				HWND_TOP,			x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_PREVIEW),				HWND_TOP,			x,		y+28,		231,	231,		0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_RES),					HWND_TOP,			x+240,	y+30,		120,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TYPE),					HWND_TOP,			x+240,	y+60,		120,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_LOAD),					HWND_TOP,			x+241,	y+177,		119,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_UNLOAD),					HWND_TOP,			x+241,	y+207,		119,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_PICK),					HWND_TOP,			x+241,	y+237,		119,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_NAME),					HWND_TOP,			x+1,	y+270,		360,	16,			0);


	x=389; y=34;
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_GR_COLOR_CORRECTION),	HWND_TOP,			x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_BRIGHTNESS),		HWND_TOP,			x+0,	y+30,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_CONTRAST),			HWND_TOP,			x+0,	y+60,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_SATURATION),		HWND_TOP,			x+0,	y+90,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_HUE),				HWND_TOP,			x+0,	y+120,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_RED),				HWND_TOP,			x+0,	y+150,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_GREEN),				HWND_TOP,			x+0,	y+180,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_BLUE),				HWND_TOP,			x+0,	y+210,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_TEXT_GAMMA),				HWND_TOP,			x+0,	y+240,		80,		16,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS),				HWND_TOP,			x+84,	y+28,		276,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_CONTRAST),				HWND_TOP,			x+84,	y+58,		276,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_SATURATION),				HWND_TOP,			x+84,	y+88,		276,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_HUE),					HWND_TOP,			x+84,	y+118,		276,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_RED),					HWND_TOP,			x+84,	y+148,		276,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_GREEN),					HWND_TOP,			x+84,	y+178,		276,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_BLUE),					HWND_TOP,			x+84,	y+208,		276,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_GAMMA),					HWND_TOP,			x+84,	y+238,		276,	20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_INTERPOLATION),			HWND_TOP,			x+0,	y+273,		160,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_INVERT),					HWND_TOP,			x+179,	y+273,		60,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_RESET_ALL),				HWND_TOP,			x+280,	y+267,		80,		22,			0);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS),		HWND_TOP,								0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_CONTRAST),		GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_SATURATION),		GetDlgItem(hWnd, IDC_TEX2_CONTRAST),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_HUE),			GetDlgItem(hWnd, IDC_TEX2_SATURATION),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_RED),			GetDlgItem(hWnd, IDC_TEX2_HUE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_GREEN),			GetDlgItem(hWnd, IDC_TEX2_RED),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_BLUE),			GetDlgItem(hWnd, IDC_TEX2_GREEN),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEX2_GAMMA),			GetDlgItem(hWnd, IDC_TEX2_BLUE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);


	// OK i Cancel za blisko?
}

//------------------------------------------------------------------------------------------------

void texDlg2LockAll(HWND hWnd, bool lock)
{
	setControlEnabled(!lock, hWnd, IDC_TEX2_OK);
	setControlEnabled(!lock, hWnd, IDC_TEX2_CANCEL);

	setControlEnabled(!lock, hWnd, IDC_TEX2_PREVIEW);
	setControlEnabled(!lock, hWnd, IDC_TEX2_LOAD);
	setControlEnabled(!lock, hWnd, IDC_TEX2_UNLOAD);
	setControlEnabled(!lock, hWnd, IDC_TEX2_PICK);

	setControlEnabled(!lock, hWnd, IDC_TEX2_BRIGHTNESS);
	setControlEnabled(!lock, hWnd, IDC_TEX2_CONTRAST);
	setControlEnabled(!lock, hWnd, IDC_TEX2_SATURATION);
	setControlEnabled(!lock, hWnd, IDC_TEX2_HUE);
	setControlEnabled(!lock, hWnd, IDC_TEX2_RED);
	setControlEnabled(!lock, hWnd, IDC_TEX2_GREEN);
	setControlEnabled(!lock, hWnd, IDC_TEX2_BLUE);
	setControlEnabled(!lock, hWnd, IDC_TEX2_GAMMA);

	setControlEnabled(!lock, hWnd, IDC_TEX2_INTERPOLATION);
	setControlEnabled(!lock, hWnd, IDC_TEX2_INVERT);
	setControlEnabled(!lock, hWnd, IDC_TEX2_RESET_ALL);
}

//------------------------------------------------------------------------------------------------

bool texDlg2UpdateGUItoTexMod(HWND hWnd, Texture * tex)
{
	CHECK(hWnd);
	CHECK(tex);

	EM2EditTrackBar * emtb_bri = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS));
	EM2EditTrackBar * emtb_con = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_CONTRAST));
	EM2EditTrackBar * emtb_sat = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_SATURATION));
	EM2EditTrackBar * emtb_hue = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_HUE));
	EM2EditTrackBar * emtb_red = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_RED));
	EM2EditTrackBar * emtb_gre = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GREEN));
	EM2EditTrackBar * emtb_blu = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BLUE));
	EM2EditTrackBar * emtb_gam = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GAMMA));
	EM2CheckBox * emc_inv = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INVERT));
	EM2CheckBox * emc_int = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INTERPOLATION));

	tex->texMod.brightness = emtb_bri->floatValue;
	tex->texMod.contrast   = emtb_con->floatValue;
	tex->texMod.saturation = emtb_sat->floatValue;
	tex->texMod.hue        = emtb_hue->floatValue;
	tex->texMod.red        = emtb_red->floatValue;
	tex->texMod.green      = emtb_gre->floatValue;
	tex->texMod.blue       = emtb_blu->floatValue;
	tex->texMod.gamma      = emtb_gam->floatValue;
	tex->texMod.updateGamma(tex->texMod.gamma);
	tex->texMod.invert     = emc_inv->selected;
	tex->texMod.interpolateProbe = emc_int->selected;

	return true;
}

//------------------------------------------------------------------------------------------------

bool texDlg2UpdateTexModToGui(HWND hWnd, Texture * tex)
{
	CHECK(hWnd);
	CHECK(tex);

	EM2EditTrackBar * emtb_bri = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BRIGHTNESS));
	EM2EditTrackBar * emtb_con = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_CONTRAST));
	EM2EditTrackBar * emtb_sat = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_SATURATION));
	EM2EditTrackBar * emtb_hue = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_HUE));
	EM2EditTrackBar * emtb_red = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_RED));
	EM2EditTrackBar * emtb_gre = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GREEN));
	EM2EditTrackBar * emtb_blu = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_BLUE));
	EM2EditTrackBar * emtb_gam = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_TEX2_GAMMA));
	EM2CheckBox * emc_inv      = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INVERT));
	EM2CheckBox * emc_int = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEX2_INTERPOLATION));

	emtb_bri->setFloatValue(tex->texMod.brightness);
	emtb_con->setFloatValue(tex->texMod.contrast);
	emtb_sat->setFloatValue(tex->texMod.saturation);
	emtb_hue->setFloatValue(tex->texMod.hue);
	emtb_red->setFloatValue(tex->texMod.red);
	emtb_gre->setFloatValue(tex->texMod.green);
	emtb_blu->setFloatValue(tex->texMod.blue);
	emtb_gam->setFloatValue(tex->texMod.gamma);
	emc_inv->selected = tex->texMod.invert;
	InvalidateRect(emc_inv->hwnd, NULL, false);
	emc_int->selected = tex->texMod.interpolateProbe;
	InvalidateRect(emc_int->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

bool texDlg2UpdateTexData(HWND hWnd, Texture * tex)
{
	CHECK(hWnd);
	if (!tex  ||  !tex->isValid()  ||  !tex->filename)
	{
		SetWindowText(GetDlgItem(hWnd, IDC_TEX2_RES), "RES: 0 x 0 PX");
		SetWindowText(GetDlgItem(hWnd, IDC_TEX2_TYPE), "TYPE: EMPTY");
		SetWindowText(GetDlgItem(hWnd, IDC_TEX2_NAME), "NO FILE LOADED");
		return true;
	}

	char buf[256];
	sprintf_s(buf, 256, "RES: %d x %d PX", tex->bmp.getWidth(), tex->bmp.getHeight());
	SetWindowText(GetDlgItem(hWnd, IDC_TEX2_RES), buf);
	if (tex->bmp.isItFloat)
		SetWindowText(GetDlgItem(hWnd, IDC_TEX2_TYPE), "128 BIT HDR");
	else
		SetWindowText(GetDlgItem(hWnd, IDC_TEX2_TYPE), "32 BIT BITMAP");
	SetWindowText(GetDlgItem(hWnd, IDC_TEX2_NAME), tex->filename);

	return true;
}

//------------------------------------------------------------------------------------------------

bool texDlg2makeTexPreview(HWND hPrev, Texture * tex)
{
	EMPView * empv = GetEMPViewInstance(hPrev);
	CHECK(empv);
	RECT rect;
	GetClientRect(hPrev, &rect);
	if (!tex || !tex->isValid())
	{
		ImageByteBuffer * ibuf = new ImageByteBuffer();
		ibuf->allocBuffer(rect.right, rect.bottom);
		ibuf->clearBuffer();
		if (empv->byteBuff)
		{
			empv->byteBuff->freeBuffer();
			delete empv->byteBuff;
		}
		empv->byteBuff = ibuf;
		InvalidateRect(hPrev, NULL, false);
		return true;
	}

	ImageByteBuffer * ibuf = NULL;
	if (empv->byteBuff->width!=tex->bmp.getWidth()  ||  empv->byteBuff->height!=tex->bmp.getHeight())
	{
		ibuf = new ImageByteBuffer();
		ibuf->allocBuffer(tex->bmp.getWidth(), tex->bmp.getHeight());
	}
	else
		ibuf = empv->byteBuff;


	int w = tex->bmp.getWidth();
	int h = tex->bmp.getHeight();
	if (w<=0  ||  h<=0)
		return false;

	TexModifer tm = tex->texMod;
	float invert = tm.invert ? 1.0f : 0.0f;

	time_t time_start = clock();

	if (tex->bmp.isItFloat)
	{
		for (int y=0; y<h; y++)
		{
			for (int x=0; x<w; x++)
			{
				Color4 c = tex->bmp.fdata[y*w+x];

				c.r = min(1.0f, max(0.0f, tm.brightness+c.r));
				c.g = min(1.0f, max(0.0f, tm.brightness+c.g));
				c.b = min(1.0f, max(0.0f, tm.brightness+c.b));
				
				float h2 = 0.5f;
				c.r = min(1.0f, max(0.0f, (c.r-h2) * tm.contrast + h2 ));
				c.g = min(1.0f, max(0.0f, (c.g-h2) * tm.contrast + h2 ));
				c.b = min(1.0f, max(0.0f, (c.b-h2) * tm.contrast + h2 ));

				ColorHSL hsl = c.toColorHSL();
				hsl.s = min(1, max(0, hsl.s+tm.saturation*sqrt(hsl.s)));
				hsl.h += tm.hue;
				hsl.h = hsl.h - (int)hsl.h;
				c = hsl.toColor4();

				c.r = min(1.0f, max(0.0f, tm.red+c.r));
				c.g = min(1.0f, max(0.0f, tm.green+c.g));
				c.b = min(1.0f, max(0.0f, tm.blue+c.b));

				c.r = invert * (1-c.r) + (1-invert) * c.r;
				c.g = invert * (1-c.g) + (1-invert) * c.g;
				c.b = invert * (1-c.b) + (1-invert) * c.b;
				
				ibuf->imgBuf[y][x] = RGB(min(1, c.r)*255,  min(1, c.g)*255,  min(1, c.b)*255);
			}
		}
	}
	else
	{
		for (int y=0; y<h; y++)
		{
			for (int x=0; x<w; x++)
			{
				Color4 c;
				c.r = tex->bmp.idata[(y*w+x)*4+0] / 255.0f;
				c.g = tex->bmp.idata[(y*w+x)*4+1] / 255.0f;
				c.b = tex->bmp.idata[(y*w+x)*4+2] / 255.0f;

				c.r = min(1.0f, max(0.0f, tm.brightness+c.r));
				c.g = min(1.0f, max(0.0f, tm.brightness+c.g));
				c.b = min(1.0f, max(0.0f, tm.brightness+c.b));
				
				float h2 = 0.5f;
				c.r = min(1.0f, max(0.0f, (c.r-h2) * tm.contrast + h2 ));
				c.g = min(1.0f, max(0.0f, (c.g-h2) * tm.contrast + h2 ));
				c.b = min(1.0f, max(0.0f, (c.b-h2) * tm.contrast + h2 ));

				ColorHSL hsl = c.toColorHSL();
				hsl.s = min(1, max(0, hsl.s+tm.saturation*sqrt(hsl.s)));
				hsl.h += tm.hue;
				hsl.h = hsl.h - (int)hsl.h;
				c = hsl.toColor4();

				c.r = min(1.0f, max(0.0f, tm.red+c.r));
				c.g = min(1.0f, max(0.0f, tm.green+c.g));
				c.b = min(1.0f, max(0.0f, tm.blue+c.b));

				c.r = invert * (1-c.r) + (1-invert) * c.r;
				c.g = invert * (1-c.g) + (1-invert) * c.g;
				c.b = invert * (1-c.b) + (1-invert) * c.b;
				
				ibuf->imgBuf[y][x] = RGB(min(1, c.r)*255,  min(1, c.g)*255,  min(1, c.b)*255);
			}
		}
	}


	if (empv->byteBuff!=ibuf)
	{
		if (empv->byteBuff)
		{
			empv->byteBuff->freeBuffer();
			delete empv->byteBuff;
		}
		empv->byteBuff = ibuf;
	}
	
	InvalidateRect(hPrev, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------

struct __prev_param
{
	HWND hWnd;
	Texture * tex;
};

DWORD WINAPI texDlg2refreshPostThreadProc(LPVOID lpParameter)
{
	CHECK(lpParameter);
	__prev_param * param = (__prev_param*)lpParameter;
	HWND hWnd = param->hWnd;
	Texture * tex = param->tex;
	CHECK(hWnd);
	CHECK(tex);
	HWND hPrev = GetDlgItem(hWnd, IDC_TEX2_PREVIEW);
	CHECK(hPrev);

	texDlg2makeTexPreview(hPrev, tex);

	texDlg2LockAll(hWnd, false);

	return 0;
}

//------------------------------------------------------------------------------------------------

bool texDlg2RunRefreshThread(HWND hWnd, Texture * tex)
{
	static __prev_param param;
	param.hWnd = hWnd;
	param.tex = tex;

	texDlg2LockAll(hWnd, true);
	DWORD threadId = 0;
	HANDLE hThread = CreateThread( NULL, 0, 
		(LPTHREAD_START_ROUTINE)(texDlg2refreshPostThreadProc), (LPVOID)&param, 0, &threadId);
	if (!hThread)
	{
		texDlg2LockAll(hWnd, false);
		return false;
	}
	return true;
}

//------------------------------------------------------------------------------------------------
