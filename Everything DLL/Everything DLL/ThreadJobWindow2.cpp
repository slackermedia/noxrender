#include <windows.h>
#include <GdiPlus.h>
#include "ThreadJobWindow.h"
#include "ColorSchemes.h"
#include "resource.h"
#include "EM2Controls.h"
#include "noxfonts.h"

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
INT_PTR CALLBACK ThreadJobWindow2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
static ThreadWindow2 * currentThreadWindow2 = NULL;

extern HMODULE hDllModule;

//------------------------------------------------------------------------------------------------

ThreadWindow2::ThreadWindow2()
{
	hTWindow = 0;
	opened = false;
	canAbort = true;
	aborted = false;
}

ThreadWindow2::~ThreadWindow2()
{
	currentThreadWindow2 = NULL;
}

//------------------------------------------------------------------------------------------------

bool ThreadWindow2::createWindow(HWND hParent, HINSTANCE hInstance)
{
	EnableWindow(hParent, FALSE);
	currentThreadWindow2 = this;
	opened = true;
	hTWindow = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_THREAD_WINDOW2), hParent, ThreadJobWindow2Proc);

	return true;
}

//------------------------------------------------------------------------------------------------

bool ThreadWindow2::closeWindow()
{
	HWND hPar = GetParent(hTWindow);
	EnableWindow(GetParent(hTWindow), TRUE);
	EndDialog(hTWindow, 0);
	opened = false;
	SendMessage(hPar, WM_ACTIVATE, 1, 0);
	hTWindow = 0;
	return true;
}

//------------------------------------------------------------------------------------------------

ThreadWindow2 * ThreadWindow2::getCurrentInstance()
{
	return currentThreadWindow2;
}

//------------------------------------------------------------------------------------------------

bool ThreadWindow2::setWindowTitle(char * newText)
{
	SetWindowText(hTWindow, newText);
	return true;
}

//------------------------------------------------------------------------------------------------

bool ThreadWindow2::setText(char * newText)
{
	HWND hText = GetDlgItem(hTWindow, IDC_THR2_TEXT);
	EM2Text * emText = GetEM2TextInstance(hText);
	if (!emText)
		return false;
	emText->changeCaption(newText);
	return true;
}

//------------------------------------------------------------------------------------------------

bool ThreadWindow2::setProgress(float pos)
{
	HWND hProgress = GetDlgItem(hTWindow, IDC_THR2_PROGRESS);
	EMProgressBar * emProg = GetEMProgressBarInstance(hProgress);
	if (!emProg)
		return false;
	emProg->setPos(pos);
	return true;
}

//------------------------------------------------------------------------------------------------

void ThreadWindow2::notifyProgressBarCallback(char * message, float progress)
{
	if (currentThreadWindow2)
	{
		currentThreadWindow2->setText(message);
		currentThreadWindow2->setProgress(progress);
	}
}

//------------------------------------------------------------------------------------------------

void ThreadWindow2::allowAbort()
{
	canAbort = true;
	HWND hAbort = GetDlgItem(hTWindow, IDC_THR2_ABORT);
	EnableWindow(hAbort, TRUE);
}

//------------------------------------------------------------------------------------------------

void ThreadWindow2::denyAbort()
{
	canAbort = false;
	HWND hAbort = GetDlgItem(hTWindow, IDC_THR2_ABORT);
	EnableWindow(hAbort, FALSE);
}

//------------------------------------------------------------------------------------------------

void ThreadWindow2::abort()
{
	setText("Aborting, please wait...");
	aborted = true;
}

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK ThreadJobWindow2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = wrect.right-wrect.left;
				tmprect.bottom = 121 + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);

				RECT rect;
				GetClientRect(hWnd, &rect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(rect.right, 121, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();
				int mlr = 10; 
				int bw = 70;

				HWND hText = GetDlgItem(hWnd, IDC_THR2_TEXT);
				HWND hProgress = GetDlgItem(hWnd, IDC_THR2_PROGRESS);
				HWND hAbort = GetDlgItem(hWnd, IDC_THR2_ABORT);

				SetWindowPos(hProgress,		HWND_TOP, mlr, 18, rect.right-mlr*2, 19, SWP_NOZORDER);
				SetWindowPos(hText,			HWND_TOP, mlr, 51, rect.right-mlr*2, 16, SWP_NOZORDER);
				SetWindowPos(hAbort,		HWND_TOP, (rect.right-bw)/2, 81, bw, 22, SWP_NOZORDER);

				EMProgressBar * emProg = GetEMProgressBarInstance(hProgress);
				GlobalWindowSettings::colorSchemes.apply(emProg);
				emProg->useNewStyle = true;
				emProg->align = EMProgressBar::ALIGN_CENTER_CONTROL;

				EM2Button * emAbort = GetEM2ButtonInstance(hAbort);
				emAbort->bgImage = hBgBMP;
				emAbort->setFont(fonts->em2button, false);
				emAbort->bgShiftX = (rect.right-bw)/2;
				emAbort->bgShiftY = 81;

				EM2Text * emText = GetEM2TextInstance(hText);
				emText->bgImage = hBgBMP;
				emText->setFont(fonts->em2text, false);
				emText->bgShiftX = mlr;
				emText->bgShiftY = 51;

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;

			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);
						Gdiplus::Pen s2pen(Gdiplus::Color(64, 0x15,0x15,0x15));
						graphics.DrawRectangle(&s2pen, 9,17,rect.right-9*2-1, 20);
					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(32,32,32)));
					HBRUSH oldBr = (HBRUSH)SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
					Rectangle(hdc, 9,17, rect.right-9, 17+21);
					SelectObject(hdc, oldBr);
					DeleteObject(SelectObject(hdc, oldPen));
				#endif

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_THR2_ABORT:
						{
							if (wmEvent!=BN_CLICKED)
								break;
							if (currentThreadWindow2)
								currentThreadWindow2->abort();
							else
							{
								EnableWindow(GetParent(hWnd), TRUE);
								EndDialog(hWnd, 0);
							}
						}
						break;
				}
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------
