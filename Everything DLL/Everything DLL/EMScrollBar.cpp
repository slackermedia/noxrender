#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;
#define CHANGE_PEN_DELOLD(hhdc, ppen)		DeleteObject((HPEN)SelectObject(hhdc, ppen))
#define CHANGE_BRUSH_DELOLD(hhdc, bbr)		DeleteObject((HBRUSH)SelectObject(hhdc, bbr))

void drawScrollbar(HWND hwnd, HDC hdc, EMScrollBar * emsb);
void drawScrollbarNewStyle(HWND hwnd, HDC hdc, EMScrollBar * emsb);
void drawArrowUpDown(HDC hdc, bool dirUp, COLORREF cLeft, COLORREF cRight, int px, int py, int w, int h);
void drawArrowLeftRight(HDC hdc, bool dirLeft, COLORREF cTop, COLORREF cBottom, int px, int py, int w, int h);

//------------------------------------------------------------------------------------------------------------------

void InitEMScrollBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMScrollbar";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMScrollBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMScrollBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//------------------------------------------------------------------------------------------------------------------

EMScrollBar * GetEMScrollBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMScrollBar * emsb = (EMScrollBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EMScrollBar * emsb = (EMScrollBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emsb;
}

//------------------------------------------------------------------------------------------------------------------

void SetEMScrollBarInstance(HWND hwnd, EMScrollBar *emsb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emsb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emsb);
	#endif
}

//------------------------------------------------------------------------------------------------------------------

EMScrollBar::EMScrollBar(HWND hWnd)
{
	hwnd = hWnd;
	RECT crect;
	GetClientRect(hWnd, &crect);
	control_arrangement = (crect.right>crect.bottom) ? ARR_HORIZONTAL : ARR_VERTICAL;

	buttonLeftClicked = false;
	buttonRightClicked = false;
	buttonTopClicked = false;
	buttonBottomClicked = false;
	sliderClicked = false;
	buttonLeftMouseOver = false;
	buttonRightMouseOver = false;
	buttonTopMouseOver = false;
	buttonBottomMouseOver = false;
	sliderMouseOver = false;

	rgnButtonLeft = 0;
	rgnButtonRight = 0;
	rgnButtonTop = 0;
	rgnButtonBottom = 0;
	rgnTrack = 0;
	rgnSlider = 0;

	slider_pos = 50;
	size_work = 1000;		// whole used area
	size_screen = 500;		// visible area
	grid = 1;

	updateSize();
	updateRegions();
	
	colBg = RGB(0,0,0);
	colTrack = RGB(64,64,64);
	colTrackLight = RGB(96,96,96);
	colTrackDark = RGB(44,44,44);
	colSlider = RGB(64,64,64);
	colSliderMouseOver = RGB(74,74,74);
	colSliderClicked = RGB(88,88,88);
	colSliderLight = RGB(100,100,100);
	colSliderDark = RGB(44,44,44);
	colButton = RGB(64,64,64);
	colButtonMouseOver = RGB(74,74,74);
	colButtonClicked = RGB(88,88,88);
	colButtonLight = RGB(100,100,100);
	colButtonDark = RGB(44,44,44);
	colButtonArrow = RGB(160,160,160);
	colButtonArrowMouseOver = RGB(192,192,192);
	colButtonArrowClicked = RGB(224,224,224);

	useNewStyle = false;

	colNButtonLeft = RGB(60,60,60);
	colNButtonRight = RGB(44,44,44);
	colNButtonBevelLeft = RGB(100,100,100);
	colNButtonBevelTop = RGB(68,68,68);
	colNButtonBevelRight = RGB(40,40,40);
	colNButtonBevelBottom = RGB(53,53,53);
	colNArrowLeft = RGB(133,133,133);
	colNArrowRight = RGB(177,177,177);
	colNPathLeft = RGB(37,37,37);
	colNPathRight = RGB(47,47,47);
	colNDisabledPathLeft = RGB(37,37,37);
	colNDisabledPathRight = RGB(47,47,47);
	colNMOverButtonLeft = RGB(67,77,92);
	colNMOverButtonRight = RGB(38,49,64);
	colNMOverButtonBevelLeft = RGB(108,116,128);
	colNMOverButtonBevelTop = RGB(84,93,105);
	colNMOverButtonBevelRight = RGB(36,45,60);
	colNMOverButtonBevelBottom = RGB(31,39,50);
	colNMOverArrowLeft = RGB(133,133,133);
	colNMOverArrowRight = RGB(177,177,177);
	colNMClickButtonLeft = RGB(46,67,99);
	colNMClickButtonRight = RGB(13,33,64);
	colNMClickButtonBevelLeft = RGB(102,117,141);
	colNMClickButtonBevelTop = RGB(37,58,88);
	colNMClickButtonBevelRight = RGB(13,33,64);
	colNMClickButtonBevelBottom = RGB(32,40,52);
	colNMClickArrowLeft = RGB(133,133,133);
	colNMClickArrowRight = RGB(177,177,177);
	colDisabledBg = RGB(96,96,96);
	colDisabledTrack = RGB(64,64,64);
	colDisabledTrackLight = RGB(72,72,72);
	colDisabledTrackDark = RGB(56,56,56);
	colDisabledSlider = RGB(64,64,64);
	colDisabledSliderLight = RGB(72,72,72);
	colDisabledSliderDark = RGB(56,56,56);
	colDisabledButton = RGB(64,64,64);
	colDisabledButtonLight = RGB(72,72,72);
	colDisabledButtonDark = RGB(56,56,56);
	colDisabledButtonArrow = RGB(48,48,48);

}

//------------------------------------------------------------------------------------------------------------------

EMScrollBar::~EMScrollBar()
{
}

//------------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK EMScrollBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMScrollBar * emsb;
	emsb = GetEMScrollBarInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT crect;
	static int cmx, cmy, cpos;

	switch (msg)
	{
		case WM_CREATE:
			{
				emsb = new EMScrollBar(hwnd);
				SetEMScrollBarInstance(hwnd, emsb);
			}
			break;
		case WM_DESTROY:
			{
				delete emsb;
				SetEMScrollBarInstance(hwnd, 0);
			}
			break;
		case WM_SIZE:
			{
				emsb->updateSize();
				emsb->updateRegions();
			}
			break;
		case WM_PAINT:
			{
				hdc = BeginPaint(hwnd, &ps);
				if (emsb->useNewStyle)
					drawScrollbarNewStyle(hwnd, hdc, emsb);
				else
					drawScrollbar(hwnd, hdc, emsb);
				EndPaint(hwnd, &ps);
			}
			break;
		case WM_MOUSEMOVE:
			{
				SetCapture(emsb->hwnd);
				bool need_redraw = false;
				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);
				GetClientRect(hwnd, &crect);

				if (emsb->sliderClicked  ||  emsb->buttonBottomClicked  ||  emsb->buttonTopClicked  ||  emsb->buttonLeftClicked  ||  emsb->buttonRightClicked)
				{
					if (emsb->sliderClicked)
					{
						int dx = px.x - cmx;
						int dy = px.y - cmy;
						if (emsb->control_arrangement == EMScrollBar::ARR_HORIZONTAL)
							emsb->slider_pos = cpos + dx;
						if (emsb->control_arrangement == EMScrollBar::ARR_VERTICAL)
							emsb->slider_pos = cpos + dy;

						int lastwpos = emsb->workarea_pos;
						emsb->slider_pos = max(0, min(emsb->control_track_length-emsb->control_slider_length , emsb->slider_pos));
						emsb->updateRegions();
						emsb->updateWorkareaPositionFromSlider();

						InvalidateRect(hwnd, NULL, false);
						if (lastwpos != emsb->workarea_pos)
							emsb->notifyParentChange();
						break;
					}
				}

				// check mouse over slider
				if (PtInRegion(emsb->rgnSlider, px.x, px.y))
				{
					if (emsb->sliderMouseOver == false)
						need_redraw = true;
					emsb->sliderMouseOver = true;
				}
				else
				{
					if (emsb->sliderMouseOver == true)
						need_redraw = true;
					emsb->sliderMouseOver = false;
				}

				if (emsb->control_arrangement == EMScrollBar::ARR_VERTICAL)
				{
					// check mouse over button up
					if (PtInRegion(emsb->rgnButtonTop, px.x, px.y))
					{
						if (emsb->buttonTopMouseOver== false)
							need_redraw = true;
						emsb->buttonTopMouseOver = true;
					}
					else
					{
						if (emsb->buttonTopMouseOver == true)
							need_redraw = true;
						emsb->buttonTopMouseOver = false;
					}

					// check mouse over button down
					if (PtInRegion(emsb->rgnButtonBottom, px.x, px.y))
					{
						if (emsb->buttonBottomMouseOver== false)
							need_redraw = true;
						emsb->buttonBottomMouseOver = true;
					}
					else
					{
						if (emsb->buttonBottomMouseOver == true)
							need_redraw = true;
						emsb->buttonBottomMouseOver = false;
					}
				}
				if (emsb->control_arrangement == EMScrollBar::ARR_HORIZONTAL)
				{
					// check mouse over button left
					if (PtInRegion(emsb->rgnButtonLeft, px.x, px.y))
					{
						if (emsb->buttonLeftMouseOver== false)
							need_redraw = true;
						emsb->buttonLeftMouseOver = true;
					}
					else
					{
						if (emsb->buttonLeftMouseOver == true)
							need_redraw = true;
						emsb->buttonLeftMouseOver = false;
					}

					// check mouse over button right
					if (PtInRegion(emsb->rgnButtonRight, px.x, px.y))
					{
						if (emsb->buttonRightMouseOver== false)
							need_redraw = true;
						emsb->buttonRightMouseOver = true;
					}
					else
					{
						if (emsb->buttonRightMouseOver == true)
							need_redraw = true;
						emsb->buttonRightMouseOver = false;
					}
				}



				if (!PtInRect(&crect, px))
				{
					if (!emsb->sliderClicked  &&  !emsb->buttonBottomClicked  &&  !emsb->buttonTopClicked  &&  !emsb->buttonLeftClicked  &&  !emsb->buttonRightClicked)

						ReleaseCapture();
				}

				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);

				if (emsb->control_arrangement == EMScrollBar::ARR_HORIZONTAL)
				{
					if (PtInRegion(emsb->rgnButtonLeft, px.x, px.y))
					{
						emsb->workarea_pos -= max(10, emsb->grid);
						emsb->workarea_pos = max(0, emsb->workarea_pos);
						emsb->slider_pos  = emsb->workarea_pos * (emsb->control_track_length-emsb->control_slider_length) / max(1, (emsb->size_work-emsb->size_screen));

						emsb->updateRegions();
						emsb->buttonLeftClicked = true;
						InvalidateRect(hwnd, NULL, false);
						emsb->notifyParentChange();
						break;
					}
					if (PtInRegion(emsb->rgnButtonRight, px.x, px.y))
					{
						emsb->workarea_pos += max(10, emsb->grid);
						emsb->workarea_pos = min(emsb->size_work-emsb->size_screen, emsb->workarea_pos);
						emsb->slider_pos  = emsb->workarea_pos * (emsb->control_track_length-emsb->control_slider_length) / max(1, (emsb->size_work-emsb->size_screen));

						emsb->updateRegions();
						emsb->buttonRightClicked = true;
						InvalidateRect(hwnd, NULL, false);
						emsb->notifyParentChange();
						break;
					}
				}
				if (emsb->control_arrangement == EMScrollBar::ARR_VERTICAL)
				{
					if (PtInRegion(emsb->rgnButtonTop, px.x, px.y))
					{
						emsb->workarea_pos -= max(10, emsb->grid);
						emsb->workarea_pos = max(0, emsb->workarea_pos);
						emsb->slider_pos  = emsb->workarea_pos * (emsb->control_track_length-emsb->control_slider_length) / max(1, (emsb->size_work-emsb->size_screen));

						emsb->updateRegions();
						emsb->buttonTopClicked = true;
						InvalidateRect(hwnd, NULL, false);
						emsb->notifyParentChange();
						break;
					}
					if (PtInRegion(emsb->rgnButtonBottom, px.x, px.y))
					{
						emsb->workarea_pos += max(10, emsb->grid);
						emsb->workarea_pos = min(emsb->size_work-emsb->size_screen, emsb->workarea_pos);
						emsb->slider_pos  = emsb->workarea_pos * (emsb->control_track_length-emsb->control_slider_length) / max(1, (emsb->size_work-emsb->size_screen));

						emsb->updateRegions();
						emsb->buttonBottomClicked = true;
						InvalidateRect(hwnd, NULL, false);
						emsb->notifyParentChange();
						break;
					}
				}

				if (PtInRegion(emsb->rgnTrackBefore, px.x, px.y))
				{
					int c = (emsb->size_screen / emsb->grid) * emsb->grid;
					emsb->workarea_pos -= c;
					emsb->workarea_pos = max(0, emsb->workarea_pos);
					emsb->slider_pos  = emsb->workarea_pos * (emsb->control_track_length-emsb->control_slider_length) / max(1, (emsb->size_work-emsb->size_screen));

					emsb->updateRegions();
					InvalidateRect(hwnd, NULL, false);
					emsb->notifyParentChange();
					break;
				}

				if (PtInRegion(emsb->rgnTrackAfter, px.x, px.y))
				{
					int c = (emsb->size_screen / emsb->grid) * emsb->grid;
					emsb->workarea_pos += c;
					emsb->workarea_pos = min(emsb->size_work-emsb->size_screen, emsb->workarea_pos);
					emsb->slider_pos  = emsb->workarea_pos * (emsb->control_track_length-emsb->control_slider_length) / max(1, (emsb->size_work-emsb->size_screen));

					emsb->updateRegions();
					InvalidateRect(hwnd, NULL, false);
					emsb->notifyParentChange();
					break;
				}

				if (PtInRegion(emsb->rgnSlider, px.x, px.y))
				{
					emsb->sliderClicked = true;
					cmx = px.x;
					cmy = px.y;
					cpos = emsb->slider_pos;
					InvalidateRect(hwnd, NULL, false);
					break;
				}
			}
			break;
		case WM_LBUTTONUP:
			{
				POINT px;
				px.x = (short)LOWORD(lParam);
				px.y = (short)HIWORD(lParam);
				emsb->sliderClicked = false;
				emsb->buttonBottomClicked = false;
				emsb->buttonTopClicked = false;
				emsb->buttonLeftClicked = false;
				emsb->buttonRightClicked = false;
				SendMessage(hwnd, WM_MOUSEMOVE, wParam, lParam);
				InvalidateRect(hwnd, NULL, false);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//------------------------------------------------------------------------------------------------------------------

bool EMScrollBar::updateRegions(bool onlySlider)
{
	RECT crect;
	GetClientRect(hwnd, &crect);
	control_arrangement = (crect.right>crect.bottom) ? ARR_HORIZONTAL : ARR_VERTICAL;



	if (control_arrangement == ARR_HORIZONTAL)
	{
		POINT pLeft[] = { {0,0} ,    {control_button_length, 0} ,    {control_button_length, control_thickness} ,  {0, control_thickness}  };
		POINT pRight[] = { {crect.right-control_button_length,0} ,    {crect.right, 0} ,    {crect.right, control_thickness} ,  {crect.right-control_button_length, control_thickness}  };
		POINT pSlider[] = {	{control_button_length+slider_pos, 0}, 
							{control_button_length+slider_pos+control_slider_length , 0},
							{control_button_length+slider_pos+control_slider_length, crect.bottom},
							{control_button_length+slider_pos, crect.bottom} };
		POINT pTrkBef[] = { {control_button_length, 0},
							{control_button_length+slider_pos, 0},
							{control_button_length+slider_pos, crect.bottom},
							{control_button_length, crect.bottom} };
		POINT pTrkAft[] = {	{control_button_length+slider_pos+control_slider_length, 0},
							{crect.right-control_button_length, 0},
							{crect.right-control_button_length, crect.bottom},
							{control_button_length+slider_pos+control_slider_length, crect.bottom} };

		HRGN newRgnBtnLeft = CreatePolygonRgn(pLeft, 4, WINDING);
		HRGN newRgnBtnRight = CreatePolygonRgn(pRight, 4, WINDING);
		HRGN newRgnSlider = CreatePolygonRgn(pSlider, 4, WINDING);
		HRGN newRgnTrackBefore = CreatePolygonRgn(pTrkBef, 4, WINDING);
		HRGN newRgnTrackAfter = CreatePolygonRgn(pTrkAft, 4, WINDING);

		if (rgnButtonLeft)
			DeleteObject(rgnButtonLeft);
		rgnButtonLeft = newRgnBtnLeft;
		if (rgnButtonRight)
			DeleteObject(rgnButtonRight);
		rgnButtonRight = newRgnBtnRight;
		if (rgnSlider)
			DeleteObject(rgnSlider);
		rgnSlider = newRgnSlider;
		if (rgnTrackBefore)
			DeleteObject(rgnTrackBefore);
		rgnTrackBefore = newRgnTrackBefore;
		if (rgnTrackAfter)
			DeleteObject(rgnTrackAfter);
		rgnTrackAfter = newRgnTrackAfter;

	}
	else
	{
		POINT pTop[] = {	{0,0} ,  {control_thickness, 0} ,  {control_thickness, control_button_length} ,  {0, control_button_length} };
		POINT pBottom[] = { {0,crect.bottom-control_button_length} ,  {control_thickness, crect.bottom-control_button_length} ,  
							{control_thickness, crect.bottom} ,  {0, crect.bottom} };
		POINT pSlider[] = {	{0,           control_button_length+slider_pos},
							{crect.right, control_button_length+slider_pos},
							{crect.right, control_button_length+slider_pos+control_slider_length},
							{0,	          control_button_length+slider_pos+control_slider_length} };
		POINT pTrkBef[] = {	{0,           control_button_length}, 
							{crect.right, control_button_length}, 
							{crect.right, control_button_length+slider_pos}, 
							{0,           control_button_length+slider_pos} };
		POINT pTrkAft[] = {	{0,           control_button_length+slider_pos+control_slider_length}, 
							{crect.right, control_button_length+slider_pos+control_slider_length}, 
							{crect.right, crect.bottom-control_button_length}, 
							{0,           crect.bottom-control_button_length} };
			

		HRGN newRgnBtnTop = CreatePolygonRgn(pTop, 4, WINDING);
		HRGN newRgnBtnBottom = CreatePolygonRgn(pBottom, 4, WINDING);
		HRGN newRgnSlider = CreatePolygonRgn(pSlider, 4, WINDING);
		HRGN newRgnTrackBefore = CreatePolygonRgn(pTrkBef, 4, WINDING);
		HRGN newRgnTrackAfter = CreatePolygonRgn(pTrkAft, 4, WINDING);


		if (rgnButtonTop)
			DeleteObject(rgnButtonTop);
		rgnButtonTop = newRgnBtnTop;
		if (rgnButtonBottom)
			DeleteObject(rgnButtonBottom);
		rgnButtonBottom = newRgnBtnBottom;
		if (rgnSlider)
			DeleteObject(rgnSlider);
		rgnSlider = newRgnSlider;
		if (rgnTrackBefore)
			DeleteObject(rgnTrackBefore);
		rgnTrackBefore = newRgnTrackBefore;
		if (rgnTrackAfter)
			DeleteObject(rgnTrackAfter);
		rgnTrackAfter = newRgnTrackAfter;
	}

	return true;
}

//------------------------------------------------------------------------------------------------------------------

bool EMScrollBar::updateSize()
{
	RECT crect;
	GetClientRect(hwnd, &crect);
	control_arrangement = (crect.right>crect.bottom) ? ARR_HORIZONTAL : ARR_VERTICAL;

	control_thickness = (control_arrangement==ARR_HORIZONTAL) ? crect.bottom : crect.right;
	control_length = (control_arrangement==ARR_HORIZONTAL) ? crect.right : crect.bottom;
	control_button_length = control_thickness;
	control_track_length = control_length - 2*control_button_length;
	control_slider_length = max(20, control_track_length * size_screen / max(1,size_work));
		
	slider_pos  = workarea_pos * (control_track_length-control_slider_length) / max(1, (size_work-size_screen));

	return true;
}

//------------------------------------------------------------------------------------------------------------------

bool EMScrollBar::updateWorkareaPositionFromSlider()
{
	workarea_pos = slider_pos * (size_work-size_screen) / (max(1, control_track_length-control_slider_length));
	if (grid > 1)
	{
		int a = workarea_pos/grid;
		int b = workarea_pos%grid;
		if (b>grid/2)
			workarea_pos = (a+1)*grid;
		else
			workarea_pos = a*grid;
	}

	return true;
}

//------------------------------------------------------------------------------------------------------------------

bool EMScrollBar::notifyParentChange()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, SB_POS_CHANGED), 
			(LPARAM)hwnd);

	return true;
}

//------------------------------------------------------------------------------------------------------------------

void drawScrollbarNewStyle(HWND hwnd, HDC hdc, EMScrollBar * emsb)
{
	HDC thdc;
	RECT crect;
	GetClientRect(hwnd, &crect);
	HBRUSH oldBrush;
	HPEN oldPen;

	DWORD style;
	style = GetWindowLong(hwnd, GWL_STYLE);
	bool disabled = ((style & WS_DISABLED) > 0);
	bool horizontal = (emsb->control_arrangement == EMScrollBar::ARR_HORIZONTAL);
	bool vertical = (emsb->control_arrangement == EMScrollBar::ARR_VERTICAL);
	bool dontneed = (emsb->size_work <= emsb->size_screen);

	// CREATE TEMP HDC
	thdc = CreateCompatibleDC(hdc); 
	HBITMAP hbmp = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
	HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);
	HBRUSH tmpBrsh = CreateSolidBrush(RGB(0,0,0));
	HPEN tmpPen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
	oldBrush = (HBRUSH)SelectObject(thdc, tmpBrsh);
	oldPen = (HPEN)SelectObject(thdc, tmpPen);

	int butLength = emsb->control_button_length;
	int butThick = emsb->control_thickness;
	int cLen = emsb->control_length;

	// VERTICAL
	if (emsb->control_arrangement == EMScrollBar::ARR_VERTICAL)
	{
		COLORREF cPathLeft  = emsb->colNPathLeft;
		COLORREF cPathRight = emsb->colNPathRight;
		COLORREF cButtonULeft  = emsb->colNButtonLeft;
		COLORREF cButtonURight = emsb->colNButtonRight;
		COLORREF cButtonDLeft  = emsb->colNButtonLeft;
		COLORREF cButtonDRight = emsb->colNButtonRight;
		COLORREF cButtonSLeft  = emsb->colNButtonLeft;
		COLORREF cButtonSRight = emsb->colNButtonRight;
		COLORREF cButtonUBevelLeft   = emsb->colNButtonBevelLeft;
		COLORREF cButtonUBevelTop    = emsb->colNButtonBevelTop;
		COLORREF cButtonUBevelRight  = emsb->colNButtonBevelRight;
		COLORREF cButtonUBevelBottom = emsb->colNButtonBevelBottom;
		COLORREF cButtonDBevelLeft   = emsb->colNButtonBevelLeft;
		COLORREF cButtonDBevelTop    = emsb->colNButtonBevelTop;
		COLORREF cButtonDBevelRight  = emsb->colNButtonBevelRight;
		COLORREF cButtonDBevelBottom = emsb->colNButtonBevelBottom;
		COLORREF cButtonSBevelLeft   = emsb->colNButtonBevelLeft;
		COLORREF cButtonSBevelTop    = emsb->colNButtonBevelTop;
		COLORREF cButtonSBevelRight  = emsb->colNButtonBevelRight;
		COLORREF cButtonSBevelBottom = emsb->colNButtonBevelBottom;
		COLORREF cArrowULeft  = emsb->colNArrowLeft;
		COLORREF cArrowURight = emsb->colNArrowRight;
		COLORREF cArrowDLeft  = emsb->colNArrowLeft;
		COLORREF cArrowDRight = emsb->colNArrowRight;

		if (emsb->buttonBottomMouseOver)
		{
			cButtonDLeft = emsb->colNMOverButtonLeft;
			cButtonDRight = emsb->colNMOverButtonRight;
			cButtonDBevelLeft = emsb->colNMOverButtonBevelLeft;
			cButtonDBevelTop = emsb->colNMOverButtonBevelTop;
			cButtonDBevelRight = emsb->colNMOverButtonBevelRight;
			cButtonDBevelBottom = emsb->colNMOverButtonBevelBottom;
			cArrowDLeft  = emsb->colNMOverArrowLeft;
			cArrowDRight = emsb->colNMOverArrowRight;
		}
		if (emsb->buttonBottomClicked)
		{
			cButtonDLeft = emsb->colNMClickButtonLeft;
			cButtonDRight = emsb->colNMClickButtonRight;
			cButtonDBevelLeft = emsb->colNMClickButtonBevelLeft;
			cButtonDBevelTop = emsb->colNMClickButtonBevelTop;
			cButtonDBevelRight = emsb->colNMClickButtonBevelRight;
			cButtonDBevelBottom = emsb->colNMClickButtonBevelBottom;
			cArrowDLeft  = emsb->colNMClickArrowLeft;
			cArrowDRight = emsb->colNMClickArrowRight;
		}
		if (emsb->buttonTopMouseOver)
		{
			cButtonULeft = emsb->colNMOverButtonLeft;
			cButtonURight = emsb->colNMOverButtonRight;
			cButtonUBevelLeft = emsb->colNMOverButtonBevelLeft;
			cButtonUBevelTop = emsb->colNMOverButtonBevelTop;
			cButtonUBevelRight = emsb->colNMOverButtonBevelRight;
			cButtonUBevelBottom = emsb->colNMOverButtonBevelBottom;
			cArrowULeft  = emsb->colNMOverArrowLeft;
			cArrowURight = emsb->colNMOverArrowRight;
		}
		if (emsb->buttonTopClicked)
		{
			cButtonULeft = emsb->colNMClickButtonLeft;
			cButtonURight = emsb->colNMClickButtonRight;
			cButtonUBevelLeft = emsb->colNMClickButtonBevelLeft;
			cButtonUBevelTop = emsb->colNMClickButtonBevelTop;
			cButtonUBevelRight = emsb->colNMClickButtonBevelRight;
			cButtonUBevelBottom = emsb->colNMClickButtonBevelBottom;
			cArrowULeft  = emsb->colNMClickArrowLeft;
			cArrowURight = emsb->colNMClickArrowRight;
		}
		if (emsb->sliderMouseOver)
		{
			cButtonSLeft = emsb->colNMOverButtonLeft;
			cButtonSRight = emsb->colNMOverButtonRight;
			cButtonSBevelLeft = emsb->colNMOverButtonBevelLeft;
			cButtonSBevelTop = emsb->colNMOverButtonBevelTop;
			cButtonSBevelRight = emsb->colNMOverButtonBevelRight;
			cButtonSBevelBottom = emsb->colNMOverButtonBevelBottom;
		}
		if (emsb->sliderClicked)
		{
			cButtonSLeft = emsb->colNMClickButtonLeft;
			cButtonSRight = emsb->colNMClickButtonRight;
			cButtonSBevelLeft = emsb->colNMClickButtonBevelLeft;
			cButtonSBevelTop = emsb->colNMClickButtonBevelTop;
			cButtonSBevelRight = emsb->colNMClickButtonBevelRight;
			cButtonSBevelBottom = emsb->colNMClickButtonBevelBottom;
		}
		if (dontneed  ||  disabled)
		{
			cPathLeft = emsb->colNDisabledPathLeft;
			cPathRight = emsb->colNDisabledPathRight;
		}


		// PATH
		drawGradientHorizontal(thdc, cPathLeft, cPathRight, 0, 0, butThick, cLen);

		if (!dontneed  &&  !disabled)
		{
			// UPPER BUTTON
			drawGradientHorizontal(thdc, cButtonULeft, cButtonURight,  1, 1,   butThick-2, butLength-2);
			drawBevel(thdc, cButtonUBevelLeft, cButtonUBevelTop, cButtonUBevelRight, cButtonUBevelBottom,
						0,0, butThick-1, butLength-1);

			// LOWER BUTTON
			drawGradientHorizontal(thdc, cButtonDLeft, cButtonDRight, 1, cLen-butLength+1,   butThick-2, butLength-2);
			drawBevel(thdc, cButtonDBevelLeft, cButtonDBevelTop, cButtonDBevelRight, cButtonDBevelBottom,
						0,cLen-butLength, butThick-1, cLen-1);

			// ARROWS
			drawArrowUpDown(thdc, true,  cArrowULeft, cArrowURight, 0,0, butThick, butLength);
			drawArrowUpDown(thdc, false, cArrowDLeft, cArrowDRight, 0,cLen-butLength, butThick, butLength);

			// SLIDER
			drawGradientHorizontal(thdc, cButtonSLeft, cButtonSRight, 1, butLength+emsb->slider_pos+1, butThick-2, emsb->control_slider_length-2);
			drawBevel(thdc, cButtonSBevelLeft, cButtonSBevelTop, cButtonSBevelRight, cButtonSBevelBottom,
					0,butLength+emsb->slider_pos, butThick-1, butLength+emsb->slider_pos+emsb->control_slider_length-1);
		}
	}

	// HORIZONTAL
	if (emsb->control_arrangement == EMScrollBar::ARR_HORIZONTAL)
	{
		COLORREF cPathTop  = emsb->colNPathLeft;
		COLORREF cPathBottom = emsb->colNPathRight;
		COLORREF cButtonLTop  = emsb->colNButtonLeft;
		COLORREF cButtonLBottom = emsb->colNButtonRight;
		COLORREF cButtonRTop  = emsb->colNButtonLeft;
		COLORREF cButtonRBottom = emsb->colNButtonRight;
		COLORREF cButtonSTop  = emsb->colNButtonLeft;
		COLORREF cButtonSBottom = emsb->colNButtonRight;
		COLORREF cButtonLBevelLeft   = emsb->colNButtonBevelLeft;
		COLORREF cButtonLBevelTop    = emsb->colNButtonBevelTop;
		COLORREF cButtonLBevelRight  = emsb->colNButtonBevelRight;
		COLORREF cButtonLBevelBottom = emsb->colNButtonBevelBottom;
		COLORREF cButtonRBevelLeft   = emsb->colNButtonBevelLeft;
		COLORREF cButtonRBevelTop    = emsb->colNButtonBevelTop;
		COLORREF cButtonRBevelRight  = emsb->colNButtonBevelRight;
		COLORREF cButtonRBevelBottom = emsb->colNButtonBevelBottom;
		COLORREF cButtonSBevelLeft   = emsb->colNButtonBevelLeft;
		COLORREF cButtonSBevelTop    = emsb->colNButtonBevelTop;
		COLORREF cButtonSBevelRight  = emsb->colNButtonBevelRight;
		COLORREF cButtonSBevelBottom = emsb->colNButtonBevelBottom;
		COLORREF cArrowLTop  = emsb->colNArrowLeft;
		COLORREF cArrowLBottom = emsb->colNArrowRight;
		COLORREF cArrowRTop  = emsb->colNArrowLeft;
		COLORREF cArrowRBottom = emsb->colNArrowRight;

		if (emsb->buttonRightMouseOver)
		{
			cButtonRTop = emsb->colNMOverButtonLeft;
			cButtonRBottom = emsb->colNMOverButtonRight;
			cButtonRBevelLeft = emsb->colNMOverButtonBevelLeft;
			cButtonRBevelTop = emsb->colNMOverButtonBevelTop;
			cButtonRBevelRight = emsb->colNMOverButtonBevelRight;
			cButtonRBevelBottom = emsb->colNMOverButtonBevelBottom;
			cArrowRTop  = emsb->colNMOverArrowLeft;
			cArrowRBottom = emsb->colNMOverArrowRight;
		}
		if (emsb->buttonRightClicked)
		{
			cButtonRTop = emsb->colNMClickButtonLeft;
			cButtonRBottom = emsb->colNMClickButtonRight;
			cButtonRBevelLeft = emsb->colNMClickButtonBevelLeft;
			cButtonRBevelTop = emsb->colNMClickButtonBevelTop;
			cButtonRBevelRight = emsb->colNMClickButtonBevelRight;
			cButtonRBevelBottom = emsb->colNMClickButtonBevelBottom;
			cArrowRTop  = emsb->colNMClickArrowLeft;
			cArrowRBottom = emsb->colNMClickArrowRight;
		}
		if (emsb->buttonLeftMouseOver)
		{
			cButtonLTop = emsb->colNMOverButtonLeft;
			cButtonLBottom = emsb->colNMOverButtonRight;
			cButtonLBevelLeft = emsb->colNMOverButtonBevelLeft;
			cButtonLBevelTop = emsb->colNMOverButtonBevelTop;
			cButtonLBevelRight = emsb->colNMOverButtonBevelRight;
			cButtonLBevelBottom = emsb->colNMOverButtonBevelBottom;
			cArrowLTop  = emsb->colNMOverArrowLeft;
			cArrowLBottom = emsb->colNMOverArrowRight;
		}
		if (emsb->buttonLeftClicked)
		{
			cButtonLTop = emsb->colNMClickButtonLeft;
			cButtonLBottom = emsb->colNMClickButtonRight;
			cButtonLBevelLeft = emsb->colNMClickButtonBevelLeft;
			cButtonLBevelTop = emsb->colNMClickButtonBevelTop;
			cButtonLBevelRight = emsb->colNMClickButtonBevelRight;
			cButtonLBevelBottom = emsb->colNMClickButtonBevelBottom;
			cArrowLTop  = emsb->colNMClickArrowLeft;
			cArrowLBottom = emsb->colNMClickArrowRight;
		}
		if (emsb->sliderMouseOver)
		{
			cButtonSTop = emsb->colNMOverButtonLeft;
			cButtonSBottom = emsb->colNMOverButtonRight;
			cButtonSBevelLeft = emsb->colNMOverButtonBevelLeft;
			cButtonSBevelTop = emsb->colNMOverButtonBevelTop;
			cButtonSBevelRight = emsb->colNMOverButtonBevelRight;
			cButtonSBevelBottom = emsb->colNMOverButtonBevelBottom;
		}
		if (emsb->sliderClicked)
		{
			cButtonSTop = emsb->colNMClickButtonLeft;
			cButtonSBottom = emsb->colNMClickButtonRight;
			cButtonSBevelLeft = emsb->colNMClickButtonBevelLeft;
			cButtonSBevelTop = emsb->colNMClickButtonBevelTop;
			cButtonSBevelRight = emsb->colNMClickButtonBevelRight;
			cButtonSBevelBottom = emsb->colNMClickButtonBevelBottom;
		}
		if (dontneed  ||  disabled)
		{
			cPathTop = emsb->colNDisabledPathLeft;
			cPathBottom = emsb->colNDisabledPathRight;
		}


		// PATH
		drawGradientVertical(thdc, cPathTop, cPathBottom, 0, 0, cLen, butThick);

		if (!dontneed  &&  !disabled)
		{
			// LEFT BUTTON
			drawGradientVertical(thdc, cButtonLTop, cButtonLBottom,  1, 1,    butLength-2, butThick-2);
			drawBevel(thdc, cButtonLBevelLeft, cButtonLBevelTop, cButtonLBevelRight, cButtonLBevelBottom,
						0,0, butLength-1, butThick-1);

			// RIGHT BUTTON
			drawGradientVertical(thdc, cButtonRTop, cButtonRBottom, cLen-butLength+1, 1,   butLength-2, butThick-2);
			drawBevel(thdc, cButtonRBevelLeft, cButtonRBevelTop, cButtonRBevelRight, cButtonRBevelBottom,
						cLen-butLength, 0,  cLen-1, butThick-1);

			// ARROWS
			drawArrowLeftRight(thdc, true,  cArrowLTop, cArrowLBottom, 0,0,  butLength, butThick);
			drawArrowLeftRight(thdc, false, cArrowRTop, cArrowRBottom, cLen-butLength,0,  butLength, butThick);

			// SLIDER
			drawGradientVertical(thdc, cButtonSTop, cButtonSBottom, butLength+emsb->slider_pos+1, 1, emsb->control_slider_length-2, butThick-2);
			drawBevel(thdc, cButtonSBevelLeft, cButtonSBevelTop, cButtonSBevelRight, cButtonSBevelBottom,
						butLength+emsb->slider_pos, 0,  butLength+emsb->slider_pos+emsb->control_slider_length-1, butThick-1);
		}
	}

	DeleteObject((HBRUSH)SelectObject(thdc, oldBrush));
	DeleteObject((HPEN)SelectObject(thdc, oldPen));

	// COPY TO ORIGINAL HDC
	GetClientRect(hwnd, &crect);
	BitBlt(hdc, 0, 0, crect.right, crect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
	SelectObject(thdc, oldBMP);
	DeleteObject(hbmp);
	DeleteDC(thdc);
}

void drawScrollbar(HWND hwnd, HDC hdc, EMScrollBar * emsb)
{
	HDC thdc;
	RECT crect;
	GetClientRect(hwnd, &crect);
	HBRUSH oldBrush;
	HPEN oldPen;

	DWORD style;
	style = GetWindowLong(hwnd, GWL_STYLE);
	bool disabled = ((style & WS_DISABLED) > 0);
	bool horizontal = (emsb->control_arrangement == EMScrollBar::ARR_HORIZONTAL);
	bool vertical = (emsb->control_arrangement == EMScrollBar::ARR_VERTICAL);
	bool dontneed = (emsb->size_work <= emsb->size_screen);

	// CREATE TEMP HDC
	thdc = CreateCompatibleDC(hdc); 
	HBITMAP hbmp = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
	HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);
	HBRUSH tmpBrsh = CreateSolidBrush(RGB(0,0,0));
	HPEN tmpPen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
	oldBrush = (HBRUSH)SelectObject(thdc, tmpBrsh);
	oldPen = (HPEN)SelectObject(thdc, tmpPen);

	// fill everything
	POINT cBorder[] = {	{0, 0}, {crect.right,0}, 
						{crect.right,crect.bottom}, 
						{0,crect.bottom} };	// 4
	HRGN cfhrg = CreatePolygonRgn(cBorder, 4, WINDING);
	HBRUSH cfbr = CreateSolidBrush(emsb->colBg);//emetb->colBackground);
	FillRgn(thdc, cfhrg, cfbr);
	DeleteObject(cfbr);
	DeleteObject(cfhrg);

	int butLength = emsb->control_button_length;
	int butThick = emsb->control_thickness;

	// HORIZONTAL
	if (emsb->control_arrangement == EMScrollBar::ARR_HORIZONTAL)
	{
		POINT pButton1LeftUp[]		= { {0,butThick-2} ,  {0,0} ,  {butLength-1,0} };
		POINT pButton1RightDown[]	= { {butLength-1, 0} , {butLength-1, butThick-1} ,  {-1, butThick-1} };
		POINT pButton2LeftUp[]		= { {crect.right-butLength,butThick-2} ,  {crect.right-butLength,0} ,  {crect.right-1,0} };
		POINT pButton2RightDown[]	= { {crect.right-1, 0} , {crect.right-1, butThick-1} ,  {crect.right-butLength-1, butThick-1} };
		POINT pTrackLeftUp[]		= { {butLength, crect.bottom-1} ,  {butLength, 0} ,  {crect.right-butLength,0} };
		POINT pTrackRightDown[]		= { {crect.right-butLength-1,1} ,  {crect.right-butLength-1,crect.bottom-1} ,   {butLength,crect.bottom-1} };
		POINT pSliderLeftUp[]		= { {butLength+emsb->slider_pos, crect.bottom-2},
										{butLength+emsb->slider_pos, 0}, {butLength+emsb->slider_pos+emsb->control_slider_length-1, 0} };
		POINT pSliderRightDown[]	= {	{butLength+emsb->slider_pos+emsb->control_slider_length-1, 0},
										{butLength+emsb->slider_pos+emsb->control_slider_length-1, crect.bottom-1}, {butLength+emsb->slider_pos-1, crect.bottom-1} };

		int cntr = butLength+emsb->slider_pos + emsb->control_slider_length/2;
		POINT pSliderT1LeftUp[]		= { {cntr-5, crect.bottom-5}, {cntr-5, 3}, {cntr-3, 3} };
		POINT pSliderT1RightDown[]	= { {cntr-4, 4}, {cntr-4, crect.bottom-4}, {cntr-6, crect.bottom-4} };
		POINT pSliderT2LeftUp[]		= { {cntr-0, crect.bottom-5}, {cntr-0, 3}, {cntr+2, 3} };
		POINT pSliderT2RightDown[]	= { {cntr+1, 4}, {cntr+1, crect.bottom-4}, {cntr-1, crect.bottom-4} };
		POINT pSliderT3LeftUp[]		= { {cntr+5, crect.bottom-5}, {cntr+5, 3}, {cntr+7, 3} };
		POINT pSliderT3RightDown[]	= { {cntr+6, 4}, {cntr+6, crect.bottom-4}, {cntr+4, crect.bottom-4} };
			
		int tp2 = butThick/2;
		int tp4 = butThick/4;
		int tp8 = butThick/8;
		int add = 1 - butThick%2;

		HBRUSH btnbr1 = 0;
		HBRUSH btnbr2 = 0;
		HBRUSH trkbr = 0;
		HBRUSH sldbr = 0;
		HPEN trkpen  = 0;
		HPEN trkpenL  = 0;
		HPEN trkpenD  = 0;
		HPEN btnpen1 = 0;
		HPEN btnpenL1 = 0;
		HPEN btnpenD1 = 0;
		HPEN btnpen2  = 0;
		HPEN btnpenL2 = 0;
		HPEN btnpenD2 = 0;
		HPEN sldpen  = 0;
		HPEN sldpenL = 0;
		HPEN sldpenD = 0;
		COLORREF colArrow1 = 0;
		COLORREF colArrow2 = 0;

		if (disabled || dontneed)
		{
			btnbr1 = CreateSolidBrush(emsb->colDisabledButton);
			btnpen1  = CreatePen(PS_SOLID,1,emsb->colDisabledButton);
			btnpenL1 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonLight);
			btnpenD1 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonDark);
			btnbr2 = CreateSolidBrush(emsb->colDisabledButton);
			btnpen2  = CreatePen(PS_SOLID,1,emsb->colDisabledButton);
			btnpenL2 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonLight);
			btnpenD2 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonDark);
			trkbr = CreateSolidBrush(emsb->colDisabledTrack);
			trkpen  = CreatePen(PS_SOLID,1,emsb->colDisabledTrack);
			trkpenL  = CreatePen(PS_SOLID,1, emsb->colDisabledTrackLight);
			trkpenD  = CreatePen(PS_SOLID,1, emsb->colDisabledTrackDark);
			colArrow1 = emsb->colDisabledButtonArrow;
			colArrow2 = emsb->colDisabledButtonArrow;
			sldpen  = CreatePen(PS_SOLID,1, emsb->colDisabledSlider);
			sldpenL = CreatePen(PS_SOLID,1, emsb->colDisabledSliderLight);
			sldpenD = CreatePen(PS_SOLID,1, emsb->colDisabledSliderDark);
			sldbr = CreateSolidBrush(emsb->colDisabledSlider);
		}
		else
		{
			if (emsb->buttonLeftClicked)
			{
				btnbr1 = CreateSolidBrush(emsb->colButtonClicked);
				btnpen1  = CreatePen(PS_SOLID,1,emsb->colButtonClicked);
				btnpenL1 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
				btnpenD1 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
				colArrow1 = emsb->colButtonArrowClicked;
			}
			else
			{
				if (emsb->buttonLeftMouseOver)
				{
					btnbr1 = CreateSolidBrush(emsb->colButtonMouseOver);
					btnpen1  = CreatePen(PS_SOLID,1,emsb->colButtonMouseOver);
					btnpenL1 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD1 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow1 = emsb->colButtonArrowMouseOver;
				}
				else
				{
					btnbr1 = CreateSolidBrush(emsb->colButton);
					btnpen1  = CreatePen(PS_SOLID,1,emsb->colButton);
					btnpenL1 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD1 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow1 = emsb->colButtonArrow;
				}
			}
			if (emsb->buttonRightClicked)
			{
				btnbr2 = CreateSolidBrush(emsb->colButtonClicked);
				btnpen2  = CreatePen(PS_SOLID,1,emsb->colButtonClicked);
				btnpenL2 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
				btnpenD2 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
				colArrow2 = emsb->colButtonArrowClicked;
			}
			else
			{
				if (emsb->buttonRightMouseOver)
				{
					btnbr2 = CreateSolidBrush(emsb->colButtonMouseOver);
					btnpen2  = CreatePen(PS_SOLID,1,emsb->colButtonMouseOver);
					btnpenL2 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD2 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow2 = emsb->colButtonArrowMouseOver;
				}
				else
				{
					btnbr2 = CreateSolidBrush(emsb->colButton);
					btnpen2  = CreatePen(PS_SOLID,1,emsb->colButton);
					btnpenL2 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD2 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow2 = emsb->colButtonArrow;
				}
			}
			trkbr = CreateSolidBrush(emsb->colTrack);
			trkpen  = CreatePen(PS_SOLID,1,emsb->colTrack);
			trkpenL  = CreatePen(PS_SOLID,1,emsb->colTrackLight);
			trkpenD  = CreatePen(PS_SOLID,1,emsb->colTrackDark);
			sldpen  = CreatePen(PS_SOLID,1, emsb->colSlider);
			sldpenL = CreatePen(PS_SOLID,1, emsb->colSliderLight);
			sldpenD = CreatePen(PS_SOLID,1, emsb->colSliderDark);
			if (emsb->sliderClicked)
				sldbr = CreateSolidBrush(emsb->colSliderClicked);
			else
				if (emsb->sliderMouseOver)
					sldbr = CreateSolidBrush(emsb->colSliderMouseOver);
				else
					sldbr = CreateSolidBrush(emsb->colSlider);

		}

		// fill buttons
		CHANGE_BRUSH_DELOLD(thdc, btnbr1);
		CHANGE_PEN_DELOLD(thdc, btnpen1);
		Rectangle(thdc, 0, 0, butLength, crect.bottom);		// left
		CHANGE_BRUSH_DELOLD(thdc, btnbr2);
		CHANGE_PEN_DELOLD(thdc, btnpen2);
		Rectangle(thdc, crect.right-butLength, 0, crect.right, crect.bottom);		// right

		// draw buttons upper left lines
		CHANGE_PEN_DELOLD(thdc, btnpenL1);
		Polyline(thdc, pButton1LeftUp, 3);
		CHANGE_PEN_DELOLD(thdc, btnpenL2);
		Polyline(thdc, pButton2LeftUp, 3);

		// draw buttons lower right lines
		CHANGE_PEN_DELOLD(thdc, btnpenD1);
		Polyline(thdc, pButton1RightDown, 3);
		CHANGE_PEN_DELOLD(thdc, btnpenD2);
		Polyline(thdc, pButton2RightDown, 3);

		// draw arrows
		for (int i=0; i<tp4; i++)
			for (int j=-i; j<=i-add; j++)
				SetPixel(thdc, i+butLength/2-tp8, j+tp2, colArrow1);
		for (int i=0; i<tp4; i++)
			for (int j=-i; j<=i-add; j++)
				SetPixel(thdc, crect.right-(i+butLength/2-tp8), j+tp2, colArrow2);

		// fill track
		CHANGE_BRUSH_DELOLD(thdc, trkbr);
		CHANGE_PEN_DELOLD(thdc, trkpen);
		Rectangle(thdc, butLength, 0, crect.right-butLength, crect.bottom);

		// draw track border lines
		CHANGE_PEN_DELOLD(thdc, trkpenD);
		Polyline(thdc, pTrackLeftUp, 3);
		CHANGE_PEN_DELOLD(thdc, trkpenL);
		Polyline(thdc, pTrackRightDown, 3);

		if (!dontneed)
		{
			// fill slider
			CHANGE_BRUSH_DELOLD(thdc, sldbr);
			CHANGE_PEN_DELOLD(thdc, sldpen);
			//Rectangle(thdc, 0, butLength+emsb->slider_pos, crect.right, butLength+emsb->slider_pos+emsb->control_slider_length);
			Rectangle(thdc, butLength+emsb->slider_pos, 0, butLength+emsb->slider_pos+emsb->control_slider_length, crect.bottom);

			// draw track border lines
			CHANGE_PEN_DELOLD(thdc, sldpenL);
			Polyline(thdc, pSliderLeftUp, 3);
			Polyline(thdc, pSliderT1LeftUp, 3);
			Polyline(thdc, pSliderT2LeftUp, 3);
			Polyline(thdc, pSliderT3LeftUp, 3);
			CHANGE_PEN_DELOLD(thdc, sldpenD);
			Polyline(thdc, pSliderRightDown, 3);
			Polyline(thdc, pSliderT1RightDown, 3);
			Polyline(thdc, pSliderT2RightDown, 3);
			Polyline(thdc, pSliderT3RightDown, 3);


		}
		else
		{
			DeleteObject(sldpen);
			DeleteObject(sldpenL);
			DeleteObject(sldpenD);
			DeleteObject(sldbr);
		}
	}

	// VERTICAL
	if (emsb->control_arrangement == EMScrollBar::ARR_VERTICAL)
	{
		POINT pButton1LeftUp[]		= { {0,butLength-2} ,  {0,0} ,  {butThick-1,0} };
		POINT pButton1RightDown[]	= { {butThick-1, 0} , {butThick-1, butLength-1} ,  {-1, butLength-1} };
		POINT pButton2LeftUp[]		= { {0,crect.bottom-2} ,  {0,crect.bottom-butLength} ,  {butThick-1,crect.bottom-butLength} };
		POINT pButton2RightDown[]	= { {butThick-1,crect.bottom-butLength} ,  {butThick-1, crect.bottom-1} ,  {-1,crect.bottom-1} };
		POINT pTrackLeftUp[]		= { {0, crect.bottom-butLength-1} ,  {0, butLength} ,  {crect.right, butLength} };
		POINT pTrackRightDown[]		= { {crect.right-1, butLength+1} ,  {crect.right-1, crect.bottom-butLength-1} ,  {0, crect.bottom-butLength-1} };
		POINT pSliderLeftUp[]		= { {0,  butLength+emsb->slider_pos+emsb->control_slider_length-2} ,  {0, butLength+emsb->slider_pos} ,  
										{crect.right-1, butLength+emsb->slider_pos} };
		POINT pSliderRightDown[]	= { {crect.right-1, butLength+emsb->slider_pos} ,  {crect.right-1, butLength+emsb->slider_pos+emsb->control_slider_length-1} ,  
										{-1, butLength+emsb->slider_pos+emsb->control_slider_length-1} };

		int cntr = butLength+emsb->slider_pos + emsb->control_slider_length/2;
		POINT pSliderT1LeftUp[]		= { {3, cntr-4}, {3, cntr-5}, {crect.right-4, cntr-5} };
		POINT pSliderT1RightDown[]	= { {crect.right-4, cntr-5}, {crect.right-4, cntr-4}, {3, cntr-4} };
		POINT pSliderT2LeftUp[]		= { {3, cntr+1}, {3, cntr}, {crect.right-4, cntr} };
		POINT pSliderT2RightDown[]	= { {crect.right-4, cntr}, {crect.right-4, cntr+1}, {3, cntr+1} };
		POINT pSliderT3LeftUp[]		= { {3, cntr+6}, {3, cntr+5}, {crect.right-4, cntr+5} };
		POINT pSliderT3RightDown[]	= { {crect.right-4, cntr+5}, {crect.right-4, cntr+6}, {3, cntr+6} };
			

		int tp2 = butThick/2;
		int tp4 = butThick/4;
		int tp8 = butThick/8;
		int add = 1 - butThick%2;

		HBRUSH btnbr1 = 0;
		HBRUSH btnbr2 = 0;
		HBRUSH trkbr = 0;
		HBRUSH sldbr = 0;
		HPEN trkpen  = 0;
		HPEN trkpenL  = 0;
		HPEN trkpenD  = 0;
		HPEN btnpen1  = 0;
		HPEN btnpenL1 = 0;
		HPEN btnpenD1 = 0;
		HPEN btnpen2  = 0;
		HPEN btnpenL2 = 0;
		HPEN btnpenD2 = 0;
		HPEN sldpen  = 0;
		HPEN sldpenL = 0;
		HPEN sldpenD = 0;
		COLORREF colArrow1 = 0;
		COLORREF colArrow2 = 0;

		if (disabled || dontneed)
		{
			btnbr1 = CreateSolidBrush(emsb->colDisabledButton);
			btnpen1  = CreatePen(PS_SOLID,1,emsb->colDisabledButton);
			btnpenL1 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonLight);
			btnpenD1 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonDark);
			btnbr2 = CreateSolidBrush(emsb->colDisabledButton);
			btnpen2  = CreatePen(PS_SOLID,1,emsb->colDisabledButton);
			btnpenL2 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonLight);
			btnpenD2 = CreatePen(PS_SOLID,1, emsb->colDisabledButtonDark);
			trkbr = CreateSolidBrush(emsb->colDisabledTrack);
			trkpen  = CreatePen(PS_SOLID,1,emsb->colDisabledTrack);
			trkpenL  = CreatePen(PS_SOLID,1, emsb->colDisabledTrackLight);
			trkpenD  = CreatePen(PS_SOLID,1, emsb->colDisabledTrackDark);
			colArrow1 = emsb->colDisabledButtonArrow;
			colArrow2 = emsb->colDisabledButtonArrow;
			sldpen  = CreatePen(PS_SOLID,1, emsb->colDisabledSlider);
			sldpenL = CreatePen(PS_SOLID,1, emsb->colDisabledSliderLight);
			sldpenD = CreatePen(PS_SOLID,1, emsb->colDisabledSliderDark);
			sldbr = CreateSolidBrush(emsb->colDisabledSlider);
		}
		else
		{
			if (emsb->buttonTopClicked)
			{
				btnbr1 = CreateSolidBrush(emsb->colButtonClicked);
				btnpen1  = CreatePen(PS_SOLID,1,emsb->colButtonClicked);
				btnpenL1 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
				btnpenD1 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
				colArrow1 = emsb->colButtonArrowClicked;
			}
			else
			{
				if (emsb->buttonTopMouseOver)
				{
					btnbr1 = CreateSolidBrush(emsb->colButtonMouseOver);
					btnpen1  = CreatePen(PS_SOLID,1,emsb->colButtonMouseOver);
					btnpenL1 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD1 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow1 = emsb->colButtonArrowMouseOver;
				}
				else
				{
					btnbr1 = CreateSolidBrush(emsb->colButton);
					btnpen1  = CreatePen(PS_SOLID,1,emsb->colButton);
					btnpenL1 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD1 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow1 = emsb->colButtonArrow;
				}
			}
			if (emsb->buttonBottomClicked)
			{
				btnbr2 = CreateSolidBrush(emsb->colButtonClicked);
				btnpen2  = CreatePen(PS_SOLID,1,emsb->colButtonClicked);
				btnpenL2 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
				btnpenD2 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
				colArrow2 = emsb->colButtonArrowClicked;
			}
			else
			{
				if (emsb->buttonBottomMouseOver)
				{
					btnbr2 = CreateSolidBrush(emsb->colButtonMouseOver);
					btnpen2  = CreatePen(PS_SOLID,1,emsb->colButtonMouseOver);
					btnpenL2 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD2 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow2 = emsb->colButtonArrowMouseOver;
				}
				else
				{
					btnbr2 = CreateSolidBrush(emsb->colButton);
					btnpen2  = CreatePen(PS_SOLID,1,emsb->colButton);
					btnpenL2 = CreatePen(PS_SOLID,1, emsb->colButtonLight);
					btnpenD2 = CreatePen(PS_SOLID,1, emsb->colButtonDark);
					colArrow2 = emsb->colButtonArrow;
				}
			}
			trkbr = CreateSolidBrush(emsb->colTrack);
			trkpen  = CreatePen(PS_SOLID,1,emsb->colTrack);
			trkpenL  = CreatePen(PS_SOLID,1,emsb->colTrackLight);
			trkpenD  = CreatePen(PS_SOLID,1,emsb->colTrackDark);
			sldpen  = CreatePen(PS_SOLID,1, emsb->colSlider);
			sldpenL = CreatePen(PS_SOLID,1, emsb->colSliderLight);
			sldpenD = CreatePen(PS_SOLID,1, emsb->colSliderDark);
			if (emsb->sliderClicked)
				sldbr = CreateSolidBrush(emsb->colSliderClicked);
			else
				if (emsb->sliderMouseOver)
					sldbr = CreateSolidBrush(emsb->colSliderMouseOver);
				else
					sldbr = CreateSolidBrush(emsb->colSlider);
		}

		// fill buttons
		CHANGE_BRUSH_DELOLD(thdc, btnbr1);
		CHANGE_PEN_DELOLD(thdc, btnpen1);
		Rectangle(thdc, 0, 0, crect.right, butLength);		// top
		CHANGE_BRUSH_DELOLD(thdc, btnbr2);
		CHANGE_PEN_DELOLD(thdc, btnpen2);
		Rectangle(thdc, 0, crect.bottom-butLength, crect.right, crect.bottom);		// bottom

		// draw buttons upper left lines
		CHANGE_PEN_DELOLD(thdc, btnpenL1);
		Polyline(thdc, pButton1LeftUp, 3);
		CHANGE_PEN_DELOLD(thdc, btnpenL2);
		Polyline(thdc, pButton2LeftUp, 3);

		// draw buttons lower right lines
		CHANGE_PEN_DELOLD(thdc, btnpenD1);
		Polyline(thdc, pButton1RightDown, 3);
		CHANGE_PEN_DELOLD(thdc, btnpenD2);
		Polyline(thdc, pButton2RightDown, 3);

		// draw arrows
		for (int i=0; i<tp4; i++)
			for (int j=-i; j<=i-add; j++)
				SetPixel(thdc, j+tp2,i+butLength/2-tp8, colArrow1);
		for (int i=0; i<tp4; i++)
			for (int j=-i; j<=i-add; j++)
				SetPixel(thdc, j+tp2,crect.bottom-(i+butLength/2-tp8), colArrow2);


		// fill track
		CHANGE_BRUSH_DELOLD(thdc, trkbr);
		CHANGE_PEN_DELOLD(thdc, trkpen);
		Rectangle(thdc, 0, butLength, crect.right, crect.bottom-butLength);

		// draw track border lines
		CHANGE_PEN_DELOLD(thdc, trkpenD);
		Polyline(thdc, pTrackLeftUp, 3);
		CHANGE_PEN_DELOLD(thdc, trkpenL);
		Polyline(thdc, pTrackRightDown, 3);


		if (!dontneed)
		{
			// fill slider
			CHANGE_BRUSH_DELOLD(thdc, sldbr);
			CHANGE_PEN_DELOLD(thdc, sldpen);
			Rectangle(thdc, 0, butLength+emsb->slider_pos, crect.right, butLength+emsb->slider_pos+emsb->control_slider_length);

			// draw track border lines and that thing
			CHANGE_PEN_DELOLD(thdc, sldpenL);
			Polyline(thdc, pSliderLeftUp, 3);
			Polyline(thdc, pSliderT1LeftUp, 3);
			Polyline(thdc, pSliderT2LeftUp, 3);
			Polyline(thdc, pSliderT3LeftUp, 3);
			CHANGE_PEN_DELOLD(thdc, sldpenD);
			Polyline(thdc, pSliderRightDown, 3);
			Polyline(thdc, pSliderT1RightDown, 3);
			Polyline(thdc, pSliderT2RightDown, 3);
			Polyline(thdc, pSliderT3RightDown, 3);

		}
		else
		{
			DeleteObject(sldpen);
			DeleteObject(sldpenL);
			DeleteObject(sldpenD);
			DeleteObject(sldbr);
		}

	}

	DeleteObject((HBRUSH)SelectObject(thdc, oldBrush));
	DeleteObject((HPEN)SelectObject(thdc, oldPen));

	// COPY TO ORIGINAL HDC
	GetClientRect(hwnd, &crect);
	BitBlt(hdc, 0, 0, crect.right, crect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
	SelectObject(thdc, oldBMP);
	DeleteObject(hbmp);
	DeleteDC(thdc);
}

//------------------------------------------------------------------------------------------------------------------

void drawArrowUpDown(HDC hdc, bool dirUp, COLORREF cLeft, COLORREF cRight, int px, int py, int w, int h)
{
	int bgr1 = GetRValue(cLeft);
	int bgg1 = GetGValue(cLeft);
	int bgb1 = GetBValue(cLeft);
	int bgr2 = GetRValue(cRight);
	int bgg2 = GetGValue(cRight);
	int bgb2 = GetBValue(cRight);

	int np = w%2;
	int adsize = w/4-1;
	int start = w/2-adsize-1;
	int stop = w/2+adsize+1+np;
	int ystart = dirUp ? h/2+w/8 : h/2-w/8+1;
	int ymul = dirUp ? -1 : 1;
	int ss = stop - start;

	for (int i=0; i<ss; i++)
	{
		unsigned char r = (bgr2*(i) + bgr1*(ss-i-1))/(ss-1);
		unsigned char g = (bgg2*(i) + bgg1*(ss-i-1))/(ss-1);
		unsigned char b = (bgb2*(i) + bgb1*(ss-i-1))/(ss-1);
		COLORREF c = RGB(r,g,b);
		int hh = (i<=ss/2-1+np) ? i : ss-i-1;
		for (int j=0; j<=hh; j++)
			SetPixel(hdc, px+i+start, py+j*ymul+ystart, c);
	}
}

//------------------------------------------------------------------------------------------------------------------

void drawArrowLeftRight(HDC hdc, bool dirLeft, COLORREF cTop, COLORREF cBottom, int px, int py, int w, int h)
{
	int bgr1 = GetRValue(cTop);
	int bgg1 = GetGValue(cTop);
	int bgb1 = GetBValue(cTop);
	int bgr2 = GetRValue(cBottom);
	int bgg2 = GetGValue(cBottom);
	int bgb2 = GetBValue(cBottom);

	int np = h%2;
	int adsize = h/4-1;
	int start = h/2-adsize-1;
	int stop = h/2+adsize+1+np;
	int xstart = dirLeft ? w/2+h/8 : w/2-h/8+1;
	int xmul = dirLeft ? -1 : 1;
	int ss = stop - start;

	for (int i=0; i<ss; i++)
	{
		unsigned char r = (bgr2*(i) + bgr1*(ss-i-1))/(ss-1);
		unsigned char g = (bgg2*(i) + bgg1*(ss-i-1))/(ss-1);
		unsigned char b = (bgb2*(i) + bgb1*(ss-i-1))/(ss-1);
		COLORREF c = RGB(r,g,b);
		int ww = (i<=ss/2-1+np) ? i : ss-i-1;
		for (int j=0; j<=ww; j++)
			SetPixel(hdc, px+j*xmul+xstart, py+i+start, c);
	}
}

//------------------------------------------------------------------------------------------------------------------
