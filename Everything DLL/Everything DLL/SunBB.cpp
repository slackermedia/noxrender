#include "BDPT.h"
#include "log.h"

//----------------------------------------------------------------

Point3d SunBoundingBox::randomStartPoint(float &pdf)
{
	#ifdef RAND_PER_THREAD
		float rnd1 = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float rnd1 = ((float)rand()/(float)RAND_MAX);
	#endif
	rnd1 *= totalarea;
	int i=0;
	while (rnd1>sumareas[i])
		i++;

	Triangle * tri = tris+i;
	Point3d p;
	float u,v;
	tri->randomPoint(u,v, p);
	pdf = 1.0f/totalarea;

	return p;
}

//----------------------------------------------------------------

bool evalSBBTri(Triangle &tri, float A, float B, float C, float D, Vector3d sdir, Point3d o1, Point3d o2, Point3d o3)
{
	float m = A*sdir.x + B*sdir.y + C*sdir.z;
	if (m==0.0f)
		return false;
	float per_m = -1.0f/m;

	float d1 = (A*o1.x + B*o1.y + C*o1.z + D) * per_m;
	float d2 = (A*o2.x + B*o2.y + C*o2.z + D) * per_m;
	float d3 = (A*o3.x + B*o3.y + C*o3.z + D) * per_m;
	tri.V1 = o1 + sdir*d1;
	tri.V2 = o2 + sdir*d2;
	tri.V3 = o3 + sdir*d3;

	tri.area = tri.evaluateArea();

	Vector3d vv1, vv2, vv3;
	vv1 = tri.V2-tri.V1;
	vv1.normalize();
	vv2 = tri.V3-tri.V1;
	vv2.normalize();
	vv3 = vv1^vv2;
	vv3.normalize();
	tri.normal_geom = vv3;

	return true;
}

//----------------------------------------------------------------

bool SunBoundingBox::createProjectedPlane(Vector3d sundir, SceneStatic * ss)
{
	CHECK(ss);
	float x1 = ss->bb_minx;	float x2 = ss->bb_maxx;
	float y1 = ss->bb_miny;	float y2 = ss->bb_maxy;
	float z1 = ss->bb_minz;	float z2 = ss->bb_maxz;
	Point3d center;
	center.x = (x1+x2) * 0.5f;
	center.y = (y1+y2) * 0.5f;
	center.z = (z1+z2) * 0.5f;
	float dx = fabs(x2-x1);
	float dy = fabs(y2-y1);
	float dz = fabs(z2-z1);
	float d = max(max(dx, dy), dz) * 2;
	Point3d p = center + sundir * d;

	A = sundir.x;
	B = sundir.y;
	C = sundir.z;
	D = -(A*p.x + B*p.y + C*p.z);
	
	int i = 0;

	if (sundir.x>0)
	{
		Point3d p1, p2, p3, p4;
		p1.x = x2;	p1.y = y1;	p1.z = z1;
		p2.x = x2;	p2.y = y1;	p2.z = z2;
		p3.x = x2;	p3.y = y2;	p3.z = z2;
		p4.x = x2;	p4.y = y2;	p4.z = z1;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p2, p3);
		i++;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p3, p4);
		i++;
	}
	else
	{
		Point3d p1, p2, p3, p4;
		p1.x = x1;	p1.y = y1;	p1.z = z1;
		p2.x = x1;	p2.y = y1;	p2.z = z2;
		p3.x = x1;	p3.y = y2;	p3.z = z2;
		p4.x = x1;	p4.y = y2;	p4.z = z1;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p2, p3);
		i++;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p3, p4);
		i++;
	}

	if (sundir.y>0)
	{
		Point3d p1, p2, p3, p4;
		p1.x = x2;	p1.y = y2;	p1.z = z1;
		p2.x = x2;	p2.y = y2;	p2.z = z2;
		p3.x = x1;	p3.y = y2;	p3.z = z2;
		p4.x = x1;	p4.y = y2;	p4.z = z1;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p2, p3);
		i++;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p3, p4);
		i++;
	}
	else
	{
		Point3d p1, p2, p3, p4;
		p1.x = x2;	p1.y = y1;	p1.z = z1;
		p2.x = x2;	p2.y = y1;	p2.z = z2;
		p3.x = x1;	p3.y = y1;	p3.z = z2;
		p4.x = x1;	p4.y = y1;	p4.z = z1;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p2, p3);
		i++;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p3, p4);
		i++;
	}

	if (sundir.z>0)
	{
		Point3d p1, p2, p3, p4;
		p1.x = x2;	p1.y = y1;	p1.z = z2;
		p2.x = x1;	p2.y = y1;	p2.z = z2;
		p3.x = x1;	p3.y = y2;	p3.z = z2;
		p4.x = x2;	p4.y = y2;	p4.z = z2;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p2, p3);
		i++;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p3, p4);
		i++;
	}
	else
	{
		Point3d p1, p2, p3, p4;
		p1.x = x2;	p1.y = y1;	p1.z = z1;
		p2.x = x1;	p2.y = y1;	p2.z = z1;
		p3.x = x1;	p3.y = y2;	p3.z = z1;
		p4.x = x2;	p4.y = y2;	p4.z = z1;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p2, p3);
		i++;
		evalSBBTri(tris[i], A, B, C, D, sundir, p1, p3, p4);
		i++;
	}

	totalarea = 0.0f;
	for (int i=0; i<6; i++)
	{
		areas[i] = tris[i].area;
		totalarea += areas[i];
		sumareas[i] = totalarea;
	}

	return true;
}

//----------------------------------------------------------------

float SunBoundingBox::evalDistance(Point3d origin, Vector3d sdir)
{

	float m = A*sdir.x + B*sdir.y + C*sdir.z;
	if (m==0.0f)
		return 0.0f;
	float per_m = -1.0f/m;

	float d1 = (A*origin.x + B*origin.y + C*origin.z + D) * per_m;
	return d1;
}

//----------------------------------------------------------------
