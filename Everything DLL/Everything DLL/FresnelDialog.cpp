#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "valuesMinMax.h"


INT_PTR CALLBACK FresnelDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

			HWND hX1,hX2,hY0,hY1,hY2,hY3, hIOR, hLines, hFr;
			hX1    = GetDlgItem(hWnd, IDC_FR_SPIN_X1);
			hX2    = GetDlgItem(hWnd, IDC_FR_SPIN_X2);
			hY0    = GetDlgItem(hWnd, IDC_FR_SPIN_Y0);
			hY1    = GetDlgItem(hWnd, IDC_FR_SPIN_Y1);
			hY2    = GetDlgItem(hWnd, IDC_FR_SPIN_Y2);
			hY3    = GetDlgItem(hWnd, IDC_FR_SPIN_Y3);
			hIOR   = GetDlgItem(hWnd, IDC_FR_SPIN_IOR);
			hLines = GetDlgItem(hWnd, IDC_FR_LINES);
			hFr    = GetDlgItem(hWnd, IDC_FR_FRESNEL_BIG);
			EMFresnel * emf = GetEMFresnelInstance(hFr);
			GlobalWindowSettings::colorSchemes.apply(emf);

			emf->dialogFresnelPtr = (Fresnel*)lParam;
			if (!emf->dialogFresnelPtr)
				emf->dialogFresnelPtr = new Fresnel();
			Fresnel * fres = emf->dialogFresnelPtr;

			EMButton * bOK = GetEMButtonInstance(GetDlgItem(hWnd, IDC_FR_OK));
			EMButton * bCancel = GetEMButtonInstance(GetDlgItem(hWnd, IDC_FR_CANCEL));
			bOK->colBackGnd = RGB(112,176,255);
			bOK->colBackGndClicked = RGB(160,224,255);
			bOK->colBorderDark = RGB(96,160,224);
			bOK->colBorderLight = RGB(128,192,255);
			bOK->colText = RGB(0,0,0);
			bCancel->colBackGnd = RGB(112,176,255);
			bCancel->colBackGndClicked = RGB(160,224,255);
			bCancel->colBorderDark = RGB(96,160,224);
			bCancel->colBorderLight = RGB(128,192,255);
			bCancel->colText = RGB(0,0,0);
			GlobalWindowSettings::colorSchemes.apply(bOK);
			GlobalWindowSettings::colorSchemes.apply(bCancel);

			EMEditSpin * emesx0  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_X0));
			EMEditSpin * emesx1  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_X1));
			EMEditSpin * emesx2  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_X2));
			EMEditSpin * emesx3  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_X3));
			EMEditSpin * emesy0  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_Y0));
			EMEditSpin * emesy1  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_Y1));
			EMEditSpin * emesy2  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_Y2));
			EMEditSpin * emesy3  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_Y3));
			EMEditSpin * emesior = GetEMEditSpinInstance(hIOR);
			emesx0->setValuesToFloat(0 , 0, 1, 0.1f, 0.01f);
			emesx3->setValuesToFloat(1 , 0, 1, 0.1f, 0.01f);
			emesx2->setValuesToFloat(fres->X1 , 0.01f, 0.99f, 0.1f, 0.01f);
			emesx2->setValuesToFloat(fres->X2 , 0.01f, 0.99f, 0.1f, 0.01f);
			emesy0->setValuesToFloat(fres->Y0 , 0, 1, 0.1f, 0.01f);
			emesy1->setValuesToFloat(fres->Y1 , 0, 1, 0.1f, 0.01f);
			emesy2->setValuesToFloat(fres->Y2 , 0, 1, 0.1f, 0.01f);
			emesy3->setValuesToFloat(fres->Y3 , 0, 1, 0.1f, 0.01f);
			emesior->setValuesToFloat(fres->IOR , NOX_MAT_IOR_MIN, NOX_MAT_IOR_MAX, 0.1f, 0.01f);
			EMCheckBox * emcbl = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FR_LINES));
			EMCheckBox * emcbc = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FR_CUSTOM));
			emcbl->selected = !fres->customPolynomial;
			GlobalWindowSettings::colorSchemes.apply(emesx0);
			GlobalWindowSettings::colorSchemes.apply(emesx1);
			GlobalWindowSettings::colorSchemes.apply(emesx2);
			GlobalWindowSettings::colorSchemes.apply(emesx3);
			GlobalWindowSettings::colorSchemes.apply(emesy0);
			GlobalWindowSettings::colorSchemes.apply(emesy1);
			GlobalWindowSettings::colorSchemes.apply(emesy2);
			GlobalWindowSettings::colorSchemes.apply(emesy3);
			GlobalWindowSettings::colorSchemes.apply(emesior);
			GlobalWindowSettings::colorSchemes.apply(emcbl);
			GlobalWindowSettings::colorSchemes.apply(emcbc);

			SetWindowPos(hFr, HWND_TOP, 0,0, 400, 400, SWP_NOMOVE|SWP_NOOWNERZORDER);
			emf->IOR = fres->IOR;
			emf->X1 = fres->X1;
			emf->X2 = fres->X2;
			emf->Y0 = fres->Y0;
			emf->Y1 = fres->Y1;
			emf->Y2 = fres->Y2;
			emf->Y3 = fres->Y3;
			emf->drawSimpleLines = !fres->customPolynomial;

			switch (fres->mode)
			{
				case Fresnel::MODE_CUSTOM:
					{
						emf->mode = EMFresnel::MODE_CUSTOM;
						GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FR_CUSTOM))->selected = true;
						EnableWindow(hIOR,   false);
						EnableWindow(hLines, true);
						EnableWindow(hX1,    true);
						EnableWindow(hX2,    true);
						EnableWindow(hY0,    true);
						EnableWindow(hY1,    true);
						EnableWindow(hY2,    true);
						EnableWindow(hY3,    true);
					}
					break;
				case Fresnel::MODE_FRESNEL:
					{
						GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FR_CUSTOM))->selected = false;
						emf->mode = EMFresnel::MODE_FRESNEL;
						EnableWindow(hIOR,   true);
						EnableWindow(hLines, false);
						EnableWindow(hX1,    false);
						EnableWindow(hX2,    false);
						EnableWindow(hY0,    false);
						EnableWindow(hY1,    false);
						EnableWindow(hY2,    false);
						EnableWindow(hY3,    false);
					}
					break;
			}

		}
		break;
	case WM_DESTROY:
		{
			if (bgBrush)
				DeleteObject(bgBrush);
			bgBrush = 0;
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
			case IDC_FR_OK:
				{
					if (wmEvent == BN_CLICKED)
					{
						HWND hFr = GetDlgItem(hWnd, IDC_FR_FRESNEL_BIG);
						EMFresnel * emf = GetEMFresnelInstance(hFr);
						(*emf->dialogFresnelPtr) = emf->getFresnelClassInstance();
						EndDialog(hWnd, (INT_PTR)emf->dialogFresnelPtr);
					}
				}
				break;
			case IDC_FR_CANCEL:
				{
					if (wmEvent == BN_CLICKED)
					{
						EndDialog(hWnd, (INT_PTR)NULL);
					}
				}
				break;
			case IDC_FR_CUSTOM:
				{
					if (wmEvent != BN_CLICKED)
						break;
					bool isSet = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FR_CUSTOM))->selected;
					HWND hIOR, hLines, hX0,hX1,hX2,hX3,hY0,hY1,hY2,hY3;
					hIOR   = GetDlgItem(hWnd, IDC_FR_SPIN_IOR);
					hLines = GetDlgItem(hWnd, IDC_FR_LINES);
					hX0    = GetDlgItem(hWnd, IDC_FR_SPIN_X0);
					hX1    = GetDlgItem(hWnd, IDC_FR_SPIN_X1);
					hX2    = GetDlgItem(hWnd, IDC_FR_SPIN_X2);
					hX3    = GetDlgItem(hWnd, IDC_FR_SPIN_X3);
					hY0    = GetDlgItem(hWnd, IDC_FR_SPIN_Y0);
					hY1    = GetDlgItem(hWnd, IDC_FR_SPIN_Y1);
					hY2    = GetDlgItem(hWnd, IDC_FR_SPIN_Y2);
					hY3    = GetDlgItem(hWnd, IDC_FR_SPIN_Y3);
					HWND hFr = GetDlgItem(hWnd, IDC_FR_FRESNEL_BIG);
					EMFresnel * emf = GetEMFresnelInstance(hFr);
					if (isSet)
					{
						EnableWindow(hIOR,   false);
						EnableWindow(hLines, true);
						EnableWindow(hX1,    true);
						EnableWindow(hX2,    true);
						EnableWindow(hY0,    true);
						EnableWindow(hY1,    true);
						EnableWindow(hY2,    true);
						EnableWindow(hY3,    true);
						emf->mode = EMFresnel::MODE_CUSTOM;
						InvalidateRect(hIOR, NULL, false);
						InvalidateRect(hLines, NULL, false);
						InvalidateRect(hX0, NULL, false);
						InvalidateRect(hX1, NULL, false);
						InvalidateRect(hX2, NULL, false);
						InvalidateRect(hX3, NULL, false);
						InvalidateRect(hY0, NULL, false);
						InvalidateRect(hY1, NULL, false);
						InvalidateRect(hY2, NULL, false);
						InvalidateRect(hY3, NULL, false);
					}
					else
					{
						EnableWindow(hIOR,   true);
						EnableWindow(hLines, false);
						EnableWindow(hX1,    false);
						EnableWindow(hX2,    false);
						EnableWindow(hY0,    false);
						EnableWindow(hY1,    false);
						EnableWindow(hY2,    false);
						EnableWindow(hY3,    false);
						emf->mode = EMFresnel::MODE_FRESNEL;
						InvalidateRect(hIOR, NULL, false);
						InvalidateRect(hLines, NULL, false);
						InvalidateRect(hX0, NULL, false);
						InvalidateRect(hX1, NULL, false);
						InvalidateRect(hX2, NULL, false);
						InvalidateRect(hX3, NULL, false);
						InvalidateRect(hY0, NULL, false);
						InvalidateRect(hY1, NULL, false);
						InvalidateRect(hY2, NULL, false);
						InvalidateRect(hY3, NULL, false);
					}
					RECT rect;
					GetClientRect(hFr, &rect);
					InvalidateRect(hFr, &rect, false);
				}
				break;
			case IDC_FR_LINES:
				{
					if (wmEvent != BN_CLICKED)
						break;
					bool isSet = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_FR_LINES))->selected;
					HWND hFr = GetDlgItem(hWnd, IDC_FR_FRESNEL_BIG);
					EMFresnel * emf = GetEMFresnelInstance(hFr);
					if (isSet)
						emf->drawSimpleLines = true;
					else
						emf->drawSimpleLines = false;
					RECT rect;
					GetClientRect(hFr, &rect);
					InvalidateRect(hFr, &rect, false);
				}
				break;
			case IDC_FR_SPIN_X1:
			case IDC_FR_SPIN_X2:
			case IDC_FR_SPIN_Y0:
			case IDC_FR_SPIN_Y1:
			case IDC_FR_SPIN_Y2:
			case IDC_FR_SPIN_Y3:
				{
					if (wmEvent != WM_VSCROLL)
						break;

					HWND hX1,hX2,hY0,hY1,hY2,hY3, hFr;
					hX1    = GetDlgItem(hWnd, IDC_FR_SPIN_X1);
					hX2    = GetDlgItem(hWnd, IDC_FR_SPIN_X2);
					hY0    = GetDlgItem(hWnd, IDC_FR_SPIN_Y0);
					hY1    = GetDlgItem(hWnd, IDC_FR_SPIN_Y1);
					hY2    = GetDlgItem(hWnd, IDC_FR_SPIN_Y2);
					hY3    = GetDlgItem(hWnd, IDC_FR_SPIN_Y3);
					hFr = GetDlgItem(hWnd, IDC_FR_FRESNEL_BIG);
					EMFresnel * emf = GetEMFresnelInstance(hFr);

					emf->X1 = GetEMEditSpinInstance(hX1)->floatValue;
					emf->X2 = GetEMEditSpinInstance(hX2)->floatValue;
					emf->Y0 = GetEMEditSpinInstance(hY0)->floatValue;
					emf->Y1 = GetEMEditSpinInstance(hY1)->floatValue;
					emf->Y2 = GetEMEditSpinInstance(hY2)->floatValue;
					emf->Y3 = GetEMEditSpinInstance(hY3)->floatValue;

					RECT rect;
					GetClientRect(hFr, &rect);
					InvalidateRect(hFr, &rect, false);
				}
				break;
			case IDC_FR_SPIN_IOR:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					HWND hFr = GetDlgItem(hWnd, IDC_FR_FRESNEL_BIG);
					EMFresnel * emf = GetEMFresnelInstance(hFr);
					emf->IOR = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_FR_SPIN_IOR))->floatValue;
					RECT rect;
					GetClientRect(hFr, &rect);
					InvalidateRect(hFr, &rect, false);
				}
				break;
			case IDC_FR_FRESNEL_BIG:
				{
					if (wmEvent == WM_VSCROLL)
					{
						HWND hFr = GetDlgItem(hWnd, IDC_FR_FRESNEL_BIG);
						EMFresnel * emf = GetEMFresnelInstance(hFr);
						switch (emf->mode)
						{
							case EMFresnel::MODE_CUSTOM:
							{
								HWND hX1,hX2,hY0,hY1,hY2,hY3;
								hX1    = GetDlgItem(hWnd, IDC_FR_SPIN_X1);
								hX2    = GetDlgItem(hWnd, IDC_FR_SPIN_X2);
								hY0    = GetDlgItem(hWnd, IDC_FR_SPIN_Y0);
								hY1    = GetDlgItem(hWnd, IDC_FR_SPIN_Y1);
								hY2    = GetDlgItem(hWnd, IDC_FR_SPIN_Y2);
								hY3    = GetDlgItem(hWnd, IDC_FR_SPIN_Y3);
								GetEMEditSpinInstance(hX1)->setValuesToFloat(emf->X1, 0.01f, 0.99f, 0.1f, 0.01f);
								GetEMEditSpinInstance(hX2)->setValuesToFloat(emf->X2, 0.01f, 0.99f, 0.1f, 0.01f);
								GetEMEditSpinInstance(hY0)->setValuesToFloat(emf->Y0, 0, 1,  0.1f, 0.01f);
								GetEMEditSpinInstance(hY1)->setValuesToFloat(emf->Y1, 0, 1,  0.1f, 0.01f);
								GetEMEditSpinInstance(hY2)->setValuesToFloat(emf->Y2, 0, 1,  0.1f, 0.01f);
								GetEMEditSpinInstance(hY3)->setValuesToFloat(emf->Y3, 0, 1,  0.1f, 0.01f);

							}
							break;
							case EMFresnel::MODE_FRESNEL:
							{
								HWND hIOR = GetDlgItem(hWnd, IDC_FR_SPIN_IOR);
								GetEMEditSpinInstance(hIOR)->setValuesToFloat(emf->IOR, NOX_MAT_IOR_MIN, NOX_MAT_IOR_MAX, 0.1f, 0.01f);
							}
							break;
						}
					}

				}
				break;
			}


		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)bgBrush;
		}
	case WM_CTLCOLORDLG:
		{
			return (INT_PTR)bgBrush;
		}
		break;
	}
	return false;
}




