#include "BDPT.h"
#include "log.h"
#include <float.h>
#include <math.h>
#include "BDPT_Counters.h"
#include "embree.h"

#define LOG_NAN_BDPT
#define NON_RR_COUNT 2
#define BRDF_ALL_LAYERS
//#define BRDF_ALL_LAYERS_CORRECT
#define COS_IN_BRDF
#define PORTALS_APDF_DIST
#define SUN_PORTAL_CONNECTION_REPAIR

bool bdpt_connect_to_cam = true;
bool bdpt_can_hit_emitter = true;
int bdpt_max_refl = 29;

//#define ONLY_CONNECT_TO_CAMERA
//#define ONLY_PATH_TRACING
#define SHADOW_RUSSIAN_ROULETTE

bool findSSSPoint(const float sample_time, const Point3d &origin, const HitData &hd_in, HitData &hd_out, MaterialNox * mat, MatLayer * mlay, 
				  Matrix4d &instMatrix, int &instanceID, int &iitri, float &fu, float &fv, Color4 &attenuation);

#define START_CAMERA 0
#define START_EMITTER 1
#define START_SUN 2
#define START_PORTAL_SUN 4
#define START_PORTAL_SKY 5
#define START_PORTAL_ENV 6

//----------------------------------------------------------------

BDPT::BDPT()
{
	ss = NULL;
	cam = NULL;
	sample_time = 0;
	working = NULL;
	threadID = 0;
	max_refl = bdpt_max_refl;
	connect_to_cam = bdpt_connect_to_cam;
	can_hit_emitter = bdpt_can_hit_emitter;
	etl_size = 0;
}

//----------------------------------------------------------------

BDPT::~BDPT()
{
}

//----------------------------------------------------------------

void BDPT::renderingLoop()
{
	finished = false;
	if (!ss  ||  !cam  ||  !working)
		return;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	bool use_embree = rtr->use_embree;

	int eyestart = connect_to_cam ? 0 : 1;
	max_refl = max(0, min(max_refl, BDPT_MAX_NODES-1));
	Color4 toAddBuffer[16];
	Color4 toAddBufferGI[16];
	for (int i=0; i<16; i++)
		toAddBuffer[i] = Color4(0,0,0);
	for (int i=0; i<16; i++)
		toAddBufferGI[i] = Color4(0,0,0);
	bool blendLayersActive[16];
	for (int i=0; i<16; i++)
		blendLayersActive[i] = ((cam->blendBits & (1<<i)) != 0);
	float mb_time = scene->sscene.motion_blur_time;

	bool usesun = ss->sunsky->sunOn  &&  !ss->useSkyPortals;
	bool usesky = ss->useSunSky  &&  !ss->useSkyPortals;
	bool useskyportals = ss->useSkyPortals;
	bool useportalsky = ss->useSkyPortals && ss->useSunSky;
	bool useportalsun  = ss->useSkyPortals && ss->sunsky->sunOn;
	int sunchannel = ss->sunsky->sunOtherBlendLayer ? 1 : 0;
	bool useenvmap = scene->env.enabled && scene->env.tex.isValid()  &&  !ss->useSkyPortals;
	bool useportalenv = scene->env.enabled && scene->env.tex.isValid()  &&  ss->useSkyPortals;
	float envmpl = pow(2.0f, scene->env.powerEV);
	int envchannel = scene->env.blendlayer-1;

	float per_cone = 1.0f/cam->coneSize;
	sbb.createProjectedPlane(ss->sunsky->getSunDirection(), ss);

	etl_size = 0;
	int emitter_type_list[10];
	if (usesun)
		emitter_type_list[etl_size++] = START_SUN;
	if (ss->lightMeshes->objCount>0)
		emitter_type_list[etl_size++] = START_EMITTER;
	if (useportalsun)
		emitter_type_list[etl_size++] = START_PORTAL_SUN;
	if (useportalsky)
		emitter_type_list[etl_size++] = START_PORTAL_SKY;
	if (useportalenv)
		emitter_type_list[etl_size++] = START_PORTAL_ENV;
	if (etl_size<1)
		emitter_type_list[etl_size++] = -1;
	int ietl = 0;
	double avg_contr_sum = 0;
	unsigned long long avg_contr_cntr = 1;

	portalmlay.roughness = 1.0f;
	RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();

	initBDPTCounters();

	// MAIN LOOP
	while (*working  &&  !finished)
	{
		if (scene->sscene.motion_blur_still)
			sample_time = scene->sscene.motion_blur_time;
		else
			#ifdef RAND_PER_THREAD
				sample_time = rg->getRandomFloat() * mb_time;
			#else
				sample_time = rand()/(float)RAND_MAX * mb_time;
			#endif
		spectr_pos_cam = -1;
		spectr_pos_emitter = -1;
		ray_in_sky = false;


		cur_emitter_type = emitter_type_list[ietl];
		bool okEye = true;

		CNT_EYE_CREATE_START;
		if (ietl==0)
			okEye = createPath(START_CAMERA);
		CNT_EYE_CREATE_STOP;
		if (finished)
		{
			logBDPTCounters();
			return;
		}
		CNT_EMITTER_CREATE_START;
		bool okLight = createPath(cur_emitter_type);
		CNT_EMITTER_CREATE_STOP;
		
		#ifdef ONLY_CONNECT_TO_CAMERA
			eSize = 0;
		#endif
		#ifdef ONLY_PATH_TRACING
			//lSize = 0;
			lSize = min(0, lSize);
		#endif
		

		if (okEye)
		{
			int eyestop = eSize;
			bool checksky = usesky  &&  ray_in_sky;
			bool checksun = usesun  &&  ray_in_sky;
			bool checkenv = useenvmap &&  ray_in_sky;
			#ifdef ONLY_CONNECT_TO_CAMERA
				checksky = false;
				checksun = false;
			#endif

			// EMITTER HIT
			if (can_hit_emitter && ePath[eSize].mlay && ePath[eSize].mlay->type==MatLayer::TYPE_EMITTER)
			{
				checksky = false;
				eyestop--;
				BDPTNode * cPath = ePath + eSize;
				int backup_emitter_type = cur_emitter_type;
				cur_emitter_type = START_EMITTER;
				float weight = evalWeight(eSize, -1);
				cur_emitter_type = backup_emitter_type;

				float ies = 1.0f; 
				if (cPath->mlay->ies  &&  cPath->mlay->ies->isValid())
				{
					float ncos = min(1.0f, max(-1.0f, cPath->hd.in * cPath->hd.normal_shade));
					float langle = acos(ncos);
					ies = cPath->mlay->ies->getEmission(0.0f, langle);
					ies *= 1.0f / fabs(ncos);
				}
				Color4 emc = cPath->mlay->getLightColor(cPath->hd.tU, cPath->hd.tV) * cPath->mlay->getIlluminance(ss, instanceIDHitEmitter, triIDHitEmitter);
				Color4 inc = cPath->incoming;
				float faceside = (cPath->hd.in * cPath->hd.normal_shade > 0) ? 1.0f : 0.0f;
				Color4 ccc = emc * weight * inc * faceside * ies;
				if (ePath[eSize-1].hd.spectr_pos>=0)
					ccc *= 4;
				if (!ccc.isNaN()  &&  !ccc.isInf())
					toAddBuffer[cPath->mat->blendIndex] += ccc;
					#ifdef LOG_NAN_BDPT
						else
						{
							char buf[256];
							sprintf_s(buf, 256, "bdpt nan/inf  on emitter hit eSize=%d  w=%f emc: %f %f %f  inc: %f %f %f ", 
												eSize, weight, emc.r, emc.g, emc.b, inc.r, inc.g, inc.b);
							Logger::add(buf);
						}
					#endif
			}

			// SKYPORTAL HIT
			if (can_hit_emitter && ePath[eSize].mat && ePath[eSize].mat->isSkyPortal  &&  (useportalsky  ||  useportalenv))
			{
				BDPTNode * eNode = &(ePath[eSize]);
				Vector3d tosky = eNode->hd.in * -1;

				if (useportalenv)
				{
					Color4 envcolor = scene->env.getColor(tosky);
					Color4 inc = eNode->incoming;
					HitData hd2 = eNode->hd;
					hd2.swapDirections();
					eNode->pdfBackAngle = portalmlay.getProbabilityLambert(hd2);

					int backup_emitter_type = cur_emitter_type;
					cur_emitter_type = START_PORTAL_ENV;
					float weight = evalWeight(eSize, -1);
					cur_emitter_type = backup_emitter_type;

					Color4 cntr = inc*envcolor*envmpl*weight;
					cntr *= 1.0f/etl_size;	// will add it for every emitter type
					if (eNode->hd.spectr_pos >= 0)
						cntr *= 4;
					if (!cntr.isNaN()  &&  !cntr.isInf())
						toAddBuffer[envchannel] += cntr;
					#ifdef LOG_NAN_BDPT
						else
						{
							char buf[256];
							sprintf_s(buf, 256, "bdpt nan/inf on skyportal hit (env)  eSize=%d   w=%f  inc: %f %f %f  env: %f %f %f  p_env=%f", 
											eSize, weight, inc.r, inc.g, inc.b, envcolor.r, envcolor.g, envcolor.b, envmpl);
							Logger::add(buf);
						}
					#endif
				}
				if (useportalsky)
				{
					Color4 skycolor = ss->sunsky->getColor(tosky);
					Color4 inc = eNode->incoming;
					HitData hd2 = eNode->hd;
					hd2.swapDirections();
					eNode->pdfBackAngle = portalmlay.getProbabilityLambert(hd2);

					int backup_emitter_type = cur_emitter_type;
					cur_emitter_type = START_PORTAL_SKY;
					float weight = evalWeight(eSize, -1);
					cur_emitter_type = backup_emitter_type;

					Color4 cntr = inc*skycolor*weight;
					cntr *= 1.0f/etl_size;	// will add it for every emitter type
					if (eNode->hd.spectr_pos >= 0)
						cntr *= 4;
					if (!cntr.isNaN()  &&  !cntr.isInf())
						toAddBuffer[0] += cntr;
					#ifdef LOG_NAN_BDPT
						else
						{
							char buf[256];
							sprintf_s(buf, 256, "bdpt nan/inf on skyportal hit (sky)  eSize=%d   w=%f  inc: %f %f %f  sky: %f %f %f", 
											eSize, weight, inc.r, inc.g, inc.b, skycolor.r, skycolor.g, skycolor.b);
							Logger::add(buf);
						}
					#endif
				}
				if (useportalsun)
				{
					Vector3d sundir = ss->sunsky->getSunDirection();
					if (tosky * sundir >= ss->sunsky->maxCosSunSize)
					{
						Color4 suncolor = ss->sunsky->getSunColor();
						Color4 inc = eNode->incoming;
						eNode->pdfBackAngle = 1.0f / ss->sunsky->getSunSizeSteradians();

						int backup_emitter_type = cur_emitter_type;
						cur_emitter_type = START_PORTAL_SUN;
						float weight = evalWeight(eSize, -1);
						cur_emitter_type = backup_emitter_type;

						Color4 cntr = inc*suncolor*weight;
						cntr *= 1.0f/etl_size;	// will add it for every emitter type
						if (eNode->hd.spectr_pos >= 0)
							cntr *= 4;
						if (!cntr.isNaN()  &&  !cntr.isInf())
							toAddBuffer[sunchannel] += cntr;
						#ifdef LOG_NAN_BDPT
							else
							{
								char buf[256];
								sprintf_s(buf, 256, "bdpt nan/inf on skyportal hit (sun)  eSize=%d  w=%f  inc: %f %f %f  sun: %f %f %f", 
											eSize, weight, inc.r, inc.g, inc.b,  suncolor.r, suncolor.g, suncolor.b);
								Logger::add(buf);
							}
						#endif
					}

				}
				eyestop--;
			}

			// NOTHING HIT - CHECK ENV MAP
			if (checkenv)
			{
				BDPTNode * eNode = &(ePath[eSize]);
				Vector3d toenv = eNode->hd.out;
				Color4 envcolor = scene->env.getColor(toenv);

				float ccos = eNode->rough>0.0f ? fabs(eNode->hd.normal_shade * eNode->hd.out) : 1.0f;
				Color4 inc = eNode->incoming;
				Color4 brdf = eNode->brdf;
				float perpdf = 1.0f/eNode->pdfAngle;
				float perRR = 1.0f/eNode->rr;
				if (eSize<1)
					perpdf = perRR = 1.0f;
				#ifdef COS_IN_BRDF
					Color4 cntr = inc*brdf*envcolor*envmpl*perpdf*perRR*att_to_sky;
				#else
					Color4 cntr = inc*brdf*ccos*envcolor*envmpl*perpdf*perRR*att_to_sky;
				#endif

				if (eNode->hd.spectr_pos >= 0)
					cntr *= 4;

				if (!cntr.isNaN()  &&  !cntr.isInf())
					toAddBuffer[envchannel] += cntr;
				else
				{
					char buf[256];
					sprintf_s(buf, 256, "env nan ... numE: %d  inc: %f %f %f   brdf: %f %f %f   pdf: %f  rr: %f", eSize, inc.r, inc.g, inc.b, brdf.r, brdf.g, brdf.b, 
										eNode->pdfAngle, eNode->rr);
					Logger::add(buf);
					sprintf_s(buf, 256, "envcol: %f %f %f   cos: %f", envcolor.r, envcolor.g, envcolor.b, ccos);
					Logger::add(buf);
				}
			}

			// NOTHING HIT - CHECK SKY
			if (checksky)
			{
				BDPTNode * eNode = &(ePath[eSize]);
				Vector3d tosky = eNode->hd.out;
				Color4 skycolor = ss->sunsky->getColor(tosky);

				float ccos = eNode->rough>0.0f ? fabs(eNode->hd.normal_shade * eNode->hd.out) : 1.0f;
				Color4 inc = eNode->incoming;
				Color4 brdf = eNode->brdf;
				float perpdf = 1.0f/eNode->pdfAngle;
				float perRR = 1.0f/eNode->rr;
				if (eSize<1)
					perpdf = perRR = 1.0f;
				#ifdef COS_IN_BRDF
					Color4 cntr = inc*brdf*skycolor*perpdf*perRR*att_to_sky;
				#else
					Color4 cntr = inc*brdf*ccos*skycolor*perpdf*perRR*att_to_sky;
				#endif
				if (eNode->hd.spectr_pos >= 0)
					cntr *= 4;

				if (!cntr.isNaN()  &&  !cntr.isInf())
					toAddBuffer[0] += cntr;
				else
				{
					char buf[256];
					sprintf_s(buf, 256, "sky nan ... numE: %d  inc: %f %f %f   brdf: %f %f %f   pdf: %f  rr: %f", eSize, inc.r, inc.g, inc.b, brdf.r, brdf.g, brdf.b, 
										eNode->pdfAngle, eNode->rr);
					Logger::add(buf);
					sprintf_s(buf, 256, "skycol: %f %f %f   cos: %f", skycolor.r, skycolor.g, skycolor.b, ccos);
					Logger::add(buf);
				}
			}

			// NOTHING HIT - CHECK SUN
			if (checksun)
			{
				BDPTNode * eNode = &(ePath[eSize]);
				Vector3d tosun = eNode->hd.out;
				Vector3d sundir = ss->sunsky->getSunDirection();
				if (tosun * sundir >= ss->sunsky->maxCosSunSize  &&  eSize<BDPT_MAX_NODES-1)
				{
					Color4 suncolor = ss->sunsky->getSunColor();
					float ccos = eNode->rough>0.0f ? fabs(eNode->hd.normal_shade * eNode->hd.out) : 1.0f;
					Color4 inc = eNode->incoming;
					Color4 brdf = eNode->brdf;
					float perpdf = 1.0f/eNode->pdfAngle;
					float perRR = 1.0f/eNode->rr;
					#ifdef COS_IN_BRDF
						Color4 cntr = inc*brdf*suncolor*perpdf*perRR*att_to_sky;
					#else
						Color4 cntr = inc*brdf*ccos*suncolor*perpdf*perRR*att_to_sky;
					#endif
					if (eNode->hd.spectr_pos >= 0)
						cntr *= 4;

					BDPTNode * nNode = &(ePath[eSize+1]);
					#ifdef PORTALS_APDF_DIST
						float dp = sbb.evalDistance(eNode->int_point, tosun);
						nNode->pdfArea = eNode->pdfAngle / (dp*dp);
					#else
						nNode->pdfArea = eNode->pdfAngle;
					#endif
					nNode->pdfBackAngle =  1.0f / ss->sunsky->getSunSizeSteradians();
					nNode->rr = nNode->rrBack = 1.0f;
					pdfBackAreaHitEmitter = 1.0f/sbb.totalarea;

					int backup_emitter_type = cur_emitter_type;
					cur_emitter_type = START_SUN;
					float weight = evalWeight(eSize+1, -1);
					cur_emitter_type = backup_emitter_type;

					if (!cntr.isNaN()  &&  !cntr.isInf())
						toAddBuffer[sunchannel] += cntr * weight;
					else
					{
						char buf[256];
						sprintf_s(buf, 256, "sun nan ... numE: %d  inc: %f %f %f   brdf: %f %f %f   pdf: %f  rr: %f", eSize, inc.r, inc.g, inc.b, brdf.r, brdf.g, brdf.b, 
											eNode->pdfAngle, eNode->rr);
						Logger::add(buf);
						sprintf_s(buf, 256, "suncol: %f %f %f   cos: %f", suncolor.r, suncolor.g, suncolor.b, ccos);
						Logger::add(buf);
					}
				}
			}

			int lbufid = 0;
			if (!okLight)
				lSize = -1;
			else
			{
				switch (cur_emitter_type)
				{
					case START_SUN:
					case START_PORTAL_SUN:
						lbufid = sunchannel;	break;
					case START_PORTAL_SKY:
						lbufid = 0;				break;
					case START_PORTAL_ENV:
						lbufid = envchannel;	break;
					default:
						lbufid = lPath[0].mat->blendIndex;	break;
				}
			}

			// ---- CONNECTIONS ----
			Color4 ccc = Color4(0,0,0);
			for (int i=eyestart; i<=eyestop; i++)
				for (int j=0; j<=lSize; j++)
				{
					// make backup of connecting vertices
					BDPTNode backupEye = ePath[i];
					BDPTNode backupLight = lPath[j];
					bool ok = true;
					
					// connect (dirs, pdfs)
					BDPTNode * eNode = ePath + i;
					BDPTNode * lNode = lPath + j;
					Vector3d jdir = eNode->int_point - lNode->int_point;
					float dist = jdir.normalize_return_old_length();
					eNode->hd.out = jdir * -1;
					lNode->hd.out = jdir;
					if (j==0    &&   (cur_emitter_type == START_SUN  ||  cur_emitter_type == START_PORTAL_SUN))
					{
						jdir = ss->sunsky->getRandomPerturbedSunDirection() * -1;
						eNode->hd.out = jdir * -1;
						lNode->hd.out = jdir;
						dist = 1.0f;

						if (cur_emitter_type == START_SUN)
						{
							#ifdef SUN_PORTAL_CONNECTION_REPAIR
								float dd = sbb.evalDistance(eNode->int_point, eNode->hd.out);
								lNode->int_point = eNode->int_point + eNode->hd.out * dd;
							#endif
							lNode->hd.out = lNode->hd.in = lNode->hd.normal_geom = lNode->hd.normal_shade = jdir;
						}
						if (cur_emitter_type == START_PORTAL_SUN)
						{	
							#ifdef SUN_PORTAL_CONNECTION_REPAIR
								// ray has to be sent to find point on portal
								int instanceID = -1, itri = -1;
								float u=0.0f, v=0.0f, bigf = BIGFLOAT;
								Matrix4d inst_mat;
								Vector3d dirr = eNode->hd.out;
								Color4 att;
								Point3d pointr = eNode->int_point + eNode->hd.normal_geom * (eNode->hd.normal_geom*dirr>0 ? 0.0001f : -0.0001f) ;
								float d = findNonOpacFakeTri(pointr, dirr, inst_mat, bigf, instanceID, itri, u,v, att, true);
								if (d>0)
								{
									lNode->tri = &(scene->triangles[itri]);
									lNode->mat = scene->mats[scene->instances[instanceID].materials[lNode->tri->matInInst]];
									if (!lNode->mat->isSkyPortal)
										ok = false;
									lNode->int_point = inst_mat * lNode->tri->getPointFromUV(u,v);
								}
								lNode->hd.in = lNode->hd.normal_shade = lNode->hd.normal_geom;
								lNode->mlay = NULL;
								lNode->u = u, lNode->v = v;
							#endif
						}
					}

					// ----- PDFs -----
					if (i==0)	// ---- camera PDF
					{
						eNode->pdfAngle = 1.0f;
						eNode->pdfAngle = 1.0f / cam->coneSize;
						eNode->pdfAngle *= 1.0f / (cam->direction*eNode->hd.out);
						eNode->pdfBackAngle = 1.0f;
					}
					else		// camera side PDF
					{
						#ifdef BRDF_ALL_LAYERS
							eNode->updatePdfsBrdfMultilayered(true, false);
						#else
							eNode->pdfAngle = eNode->mlay->getProbability(eNode->hd);
							eNode->hd.swapDirections();
							eNode->pdfBackAngle = eNode->mlay->getProbability(eNode->hd);
							eNode->hd.swapDirections();
						#endif
					}
					if (j==0)	// emitter PDF
					{
						if (cur_emitter_type == START_PORTAL_SKY)
						{
							lNode->pdfAngle = portalmlay.getProbabilityLambert(lNode->hd);
						}
						if (cur_emitter_type == START_PORTAL_ENV)
						{
							lNode->pdfAngle = portalmlay.getProbabilityLambert(lNode->hd);
						}
						if (cur_emitter_type == START_PORTAL_SUN)
						{
							lNode->pdfAngle = 1.0f / ss->sunsky->getSunSizeSteradians();
						}
						if (cur_emitter_type == START_SUN)
						{
							lNode->pdfAngle = 1.0f / ss->sunsky->getSunSizeSteradians();
						}
						if (cur_emitter_type == START_EMITTER)
						{
							if (lNode->mlay->ies  &&  lNode->mlay->ies->isValid())
							{
								float langle = acos(min(1.0f, max(-1.0f, jdir * lNode->hd.normal_shade)));
								lNode->pdfAngle = lNode->mlay->ies->getProbability(langle);
							}
							else
								lNode->pdfAngle = lNode->mlay->getProbabilityLambert(lNode->hd);
						}
						lNode->pdfBackAngle = 1.0f;
					}
					else	// emitter side PDF
					{
						#ifdef BRDF_ALL_LAYERS
							lNode->updatePdfsBrdfMultilayered(false, false);
						#else
							lNode->pdfAngle = lNode->mlay->getProbability(lNode->hd);
							lNode->hd.swapDirections();
							lNode->pdfBackAngle = lNode->mlay->getProbability(lNode->hd);
							lNode->hd.swapDirections();
						#endif
					}

					// ---- BRDFs ----
					Color4 brdfEye = Color4(1.0f, 1.0f, 1.0f);
					Color4 brdfLight = Color4(1.0f, 1.0f, 1.0f);
					if (i>0)	// eye side brdf
					{
						#ifdef BRDF_ALL_LAYERS
						#else
							#ifdef COS_IN_BRDF
								eNode->brdf = eNode->mlay->getBRDF(eNode->hd) * (1.0f/eNode->pdfMatLayer) * fabs(eNode->hd.normal_shade * jdir);	// dir to light should be smoothed
							#else
								eNode->brdf = eNode->mlay->getBRDF(eNode->hd) * (1.0f/eNode->pdfMatLayer);
							#endif
						#endif
						#ifdef COS_IN_BRDF
							brdfEye = eNode->brdf;
						#else
							brdfEye = eNode->brdf * fabs(eNode->hd.normal_shade * jdir);	// dir to light should be smoothed
						#endif
						//RR
						float maxc = max(brdfEye.r, max(brdfEye.g, brdfEye.b));
						eNode->rr = (i<=NON_RR_COUNT  ||  eNode->rough==0.0f) ? 1.0f : min(1.0f, maxc/eNode->pdfAngle);
						#ifdef COS_IN_BRDF
							float rback = min(1.0f, max(eNode->brdf.r, max(eNode->brdf.g, eNode->brdf.b)) * fabs(eNode->hd.in * eNode->hd.normal_geom) / fabs(eNode->hd.normal_shade * jdir));
						#else
							float rback = min(1.0f, max(eNode->brdf.r, max(eNode->brdf.g, eNode->brdf.b)) * fabs(eNode->hd.in * eNode->hd.normal_shade));
						#endif
						eNode->rrBack = (j<NON_RR_COUNT  ||  eNode->rough==0.0f) ? 1.0f : min(1.0f, rback/eNode->pdfBackAngle);

					}
					if (j>0)	// emitter side brdf
					{
						#ifdef BRDF_ALL_LAYERS
						#else
							#ifdef COS_IN_BRDF
								lNode->brdf = lNode->mlay->getBRDF(lNode->hd) * (1.0f/eNode->pdfMatLayer) * fabs(lNode->hd.normal_geom * jdir);	// dir to eye can't be smooth
							#else
								lNode->brdf = lNode->mlay->getBRDF(lNode->hd) * (1.0f/eNode->pdfMatLayer);
							#endif
						#endif
						#ifdef COS_IN_BRDF
							brdfLight = lNode->brdf;
						#else
							brdfLight = lNode->brdf * fabs(lNode->hd.normal_geom * jdir);	// dir to eye can't be smooth
						#endif
						float shade_corr = fabs((lNode->hd.in * lNode->hd.normal_shade) / (lNode->hd.in * lNode->hd.normal_geom));
						if (_isnan(shade_corr))
							Logger::add("shade corr L nan");
						brdfLight *= shade_corr;	// dir to light is always non-smooth, need to correct
						//RR
						float maxc = max(brdfLight.r, max(brdfLight.g, brdfLight.b));
						lNode->rr = (j<=NON_RR_COUNT  ||  lNode->rough==0.0f) ? 1.0f : min(1.0f, maxc/lNode->pdfAngle);
						#ifdef COS_IN_BRDF
							float rback = min(1.0f, max(lNode->brdf.r, max(lNode->brdf.g, lNode->brdf.b)) * fabs(lNode->hd.in * lNode->hd.normal_shade) / fabs(lNode->hd.normal_geom * jdir));
						#else
							float rback = min(1.0f, max(lNode->brdf.r, max(lNode->brdf.g, lNode->brdf.b)) * fabs(lNode->hd.in * lNode->hd.normal_shade));
						#endif
						lNode->rrBack = (i<NON_RR_COUNT  ||  lNode->rough==0.0f) ? 1.0f : min(1.0f, rback/lNode->pdfBackAngle);
					}
					else	// emitter itself brdf
					{
						if (cur_emitter_type == START_PORTAL_SKY)
						{
							lNode->incoming = ss->sunsky->getColor(jdir*-1) * (1.0f/lNode->pdfArea);
							brdfLight = Color4(1,1,1) * fabs(jdir*lNode->hd.normal_geom);
						}
						if (cur_emitter_type == START_PORTAL_ENV)
						{
							lNode->incoming = scene->env.getColor(jdir*-1) * (1.0f/lNode->pdfArea);
							brdfLight = Color4(1,1,1) * fabs(jdir*lNode->hd.normal_geom);
						}
						if (cur_emitter_type == START_SUN)
						{
							brdfLight = Color4(1,1,1) * ss->sunsky->getSunSizeSteradians();
						}
						if (cur_emitter_type == START_PORTAL_SUN)
						{
							brdfLight = Color4(1,1,1) * ss->sunsky->getSunSizeSteradians();
						}
						if (cur_emitter_type == START_EMITTER)
						{
							if (lNode->mlay->ies  &&  lNode->mlay->ies->isValid())
							{
								float ncos = min(1.0f, max(-1.0f, jdir * lNode->hd.normal_shade));
								float langle = acos(ncos);
								brdfLight = Color4(1,1,1) * lNode->mlay->ies->getEmission(0.0f, langle);
							}
							else
							{
								if (lNode->hd.normal_geom*jdir<0)
									brdfLight = Color4(0,0,0);
								else
									brdfLight *= Color4(1,1,1)*fabs(lNode->hd.normal_shade * jdir);
							}
							lNode->brdf = brdfLight;
						}
					}


					// --- in cam frustum
					bool in_frustum = false;
					float fr_x, fr_y;
					if (i==0)
					{
						Vector3d camdir = jdir * -1;
						Point3d dofpoint = ePath[0].int_point;
						in_frustum = cam->getCoordsFromDirReturnInFrustum(camdir, dofpoint, fr_x, fr_y);
						if (!in_frustum)
							ok = false;
					}

					if (brdfLight.isBlack()  ||  brdfEye.isBlack())
						ok = false;
					if (brdfLight.isInf()  ||  brdfLight.isNaN()  ||  brdfEye.isInf()  ||  brdfEye.isNaN())
					{
						Logger::add("brdf nan/inf");
						ok = false;
					}
					if (lNode->hd.isFlagDelta()   ||   eNode->hd.isFlagDelta())
						ok = false;

					// weight
					float weight = 0;
					if (ok)
						weight = evalWeight(i,j);
					if (_isnan(weight))
					{
						Logger::add("weight nan");
						weight=0.0f;
					}
	
					if (i==0)
					{
						weight /= cam->conePix;
						weight /= fabs(cam->direction * jdir);
						weight /= fabs(cam->direction * jdir);
						weight /= fabs(cam->direction * jdir);
						if (_isnan(weight))
						{
							Logger::add("weight nan");
							weight=0.0f;
						}
 					}

					Color4 uwContr = eNode->incoming * brdfEye * lNode->incoming * brdfLight * (1/(dist*dist)) * weight;
					float contr_pdf = 1.0f;
					#ifdef SHADOW_RUSSIAN_ROULETTE
					if (i>0)
					{
						if (_isnan(avg_contr_sum)  ||  avg_contr_sum > 1.0e24  ||  avg_contr_cntr>0xffffffffffffff)
						{
							Logger::add("reset cntr");
							avg_contr_sum = 0.0f;
							avg_contr_cntr = 0;
						}
						float curcontr = uwContr.r + uwContr.g + uwContr.b;
						avg_contr_sum += curcontr;
						avg_contr_cntr++;
						float avg_contr = (float)(avg_contr_sum / (double)avg_contr_cntr);
						contr_pdf = min(1.0f, curcontr / avg_contr );
						float rndrr = rg->getRandomFloat();
						if (rndrr>contr_pdf)
							ok = false;
						else
							uwContr *= 1.0f/contr_pdf;
					}
					#endif

					// visibility
					Color4 att = Color4(1,1,1);
					bool visTest = false;
					CNT_SHADOW_RAYS_START;
					if (ok)
						visTest = visibilityTest(i, j, att);
					CNT_SHADOW_RAYS_STOP;

					if (i>28 && j>28)
					{
						char buf[512];
						sprintf_s(buf, 512, "warning i = %d of %d    j = %d of %d  at %d x %d", i, eSize, j, lSize, (int)(cX/cam->aa), (int)(cY/cam->aa));
						Logger::add(buf);
					}

					Color4 contr = uwContr * att ;

					if (contr.isNaN()  ||  contr.isInf())
					{
						#ifdef LOG_NAN_BDPT

							#ifdef SHADOW_RUSSIAN_ROULETTE
								char buf[512];
								sprintf_s(buf, 512, "bdpt nan/inf on connection srr i=%d  j=%d  einc: %f %f %f  linc: %f %f %f  ebrdf: %f %f %f  lbrdf: %f %f %f  d=%f  w=%f  att: %f %f %f   uc: %f %f %f  cpdf=%f  avgcntrb=%f",
											i, j, eNode->incoming.r, eNode->incoming.g, eNode->incoming.b,  lNode->incoming.r, lNode->incoming.g, lNode->incoming.b,
											brdfEye.r, brdfEye.g, brdfEye.b,  brdfLight.r, brdfLight.g, brdfLight.b, dist, weight, att.r, att.g, att.b, uwContr.r, uwContr.g, uwContr.b, contr_pdf, avg_contr_sum);
								Logger::add(buf);
							#else
								char buf[512];
								sprintf_s(buf, 512, "bdpt nan/inf on connection i=%d  j=%d  einc: %f %f %f  linc: %f %f %f  ebrdf: %f %f %f  lbrdf: %f %f %f  d=%f  w=%f  att: %f %f %f",
											i, j, eNode->incoming.r, eNode->incoming.g, eNode->incoming.b,  lNode->incoming.r, lNode->incoming.g, lNode->incoming.b,
											brdfEye.r, brdfEye.g, brdfEye.b,  brdfLight.r, brdfLight.g, brdfLight.b, dist, weight, att.r, att.g, att.b);
								Logger::add(buf);
							#endif
						#endif
						contr = Color4(0,0,0);
					}

					if (eNode->hd.spectr_pos>=0)
						contr *= 4;
					if (lNode->hd.spectr_pos>=0)
						contr *= 4;

					if (visTest)
					{
						if (i>0)
							ccc += contr;
						else
						{
							if (in_frustum  &&  connect_to_cam)
								cam->blendBuffGI[lbufid].addPixelColor((int)(fr_x+0.5f), (int)(fr_y+0.5f), contr, false);
						}

					}

					// get vertices from backup
					ePath[i] = backupEye;
					lPath[j] = backupLight;

				}
			toAddBuffer[lbufid] += ccc;
		}

		if (ietl == etl_size-1)
		{
			for (int i=0; i<16; i++)
			{
				if (blendLayersActive[i])
				{
					cam->blendBuffDirect[i].addPixelColor((int)(cX+0.5f), (int)(cY+0.5f), toAddBuffer[i] , true);
					toAddBuffer[i] = Color4(0.0f, 0.0f, 0.0f);
				}
			}
			ss->giSamplesCount++;
		}
		ietl = (ietl+1)%etl_size;
		ss->perfCounter++;
	}

	logBDPTCounters();

}

//----------------------------------------------------------------

bool BDPT::createPath(int start_type)
{
	if (start_type<0)
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
	Color4 outgoing = Color4(1.0f, 1.0f, 1.0f);

	bool isCameraSide = (START_CAMERA==start_type);
	BDPTNode * chPath = isCameraSide ? ePath : lPath;
	if (isCameraSide)
	{
		eSize = 0;
		float dX, dY;
		Point3d origin = cam->position;
		Vector3d direction;
		cam->randomNewCoords(dX, dY, ss->random_type, finished, true);
		direction = cam->getDirection(dX,dY, origin, sample_time);
		cX = dX;   cY = dY;
		ePath[0].clearNode();
		ePath[0].int_point = origin;
		ePath[0].hd.normal_geom = ePath[0].hd.normal_shade = ePath[0].hd.in = cam->direction;
		ePath[0].hd.out = direction;
		ePath[0].sssIn = false;
		ePath[0].pdfAngle = 1.0f;
		ePath[0].pdfArea = 1.0f;
		ePath[0].incoming = Color4(1.0f, 1.0f, 1.0f);
		ePath[0].brdf = Color4(1.0f, 1.0f, 1.0f);
		ePath[0].rr = 1.0f;
		ePath[0].rrBack = 1.0f;
		ePath[0].pdfMatLayer = 1.0f;
		eSize = 0;
		ePath[0].pdfAngle = 1.0f / cam->coneSize;
		ePath[0].pdfAngle *= 1.0f / (cam->direction*ePath[0].hd.out);
		ePath[0].rough = 1;
		ePath[0].fakeRefl = ePath[0].fakeTransm = false;
	}
	else
	{
		lSize = -1;

		// GEOM EMITTER
		if (start_type == START_EMITTER)
		{
			float lpdf1, lpdf2, lpdf3, llayerpdf;
			int ilm, ilt;
			ilm = ss->randomLightMesh(lpdf1);
			if (ilm<0)
				return false;
			LightMesh *lm = (*ss->lightMeshes)[ilm];
			if (ilm >= 0 && lm)
				ilt = lm->randomTriangle(lpdf2);
			else
				return false;

			lSize = 0;

			MeshInstance * mInst1 = &(scene->instances[lm->instNum]);
			BDPTNode * lpc = &(lPath[0]);
			lpc->inst_mat = mInst1->matrix_transform;
			lpc->tri = &(scene->triangles[ilt]);
			lpc->mlay = NULL;
			lpc->sssIn = false;
			lpc->mat = scene->mats[mInst1->materials[lpc->tri->matInInst]];
			lpc->tri->randomPoint(lpc->u, lpc->v, lpc->int_point);
			lpc->int_point = lpc->inst_mat * lpc->int_point;
			lpc->mlay = lpc->mat->getRandomEmissionLayer(lpc->u, lpc->v, llayerpdf);
			if (!lpc->mlay)
				return false;
			lpc->rough = 1.0f;

			lpc->hd.normal_geom = lpc->inst_mat.getMatrixForNormals() * lpc->tri->normal_geom;
			lpc->hd.normal_geom.normalize();
			lpc->hd.in = lpc->hd.normal_geom;
			lpc->hd.normal_shade = lpc->inst_mat.getMatrixForNormals() * lpc->tri->evalNormal(lpc->u, lpc->v);
			lpc->hd.normal_shade.normalize();
			lpc->tri->evalTexUV(lpc->u, lpc->v, lpc->hd.tU, lpc->hd.tV);
			lpc->hd.spectr_pos = -1;
			lpc->hd.lastDist = 0;
			lpc->hd.flags = 0;
			lpc->fakeRefl = lpc->fakeTransm = false;
			float opacity = lpc->mat->getOpacity(lpc->hd.tU, lpc->hd.tV);

			float cosout = 1.0f;
			float ies = 1.0f; 
			if (lpc->mlay->ies  &&  lpc->mlay->ies->isValid())
			{
				float angle = lpc->mlay->ies->randomDirectionAngle(lpdf3);
				Vector3d rdir = lpc->hd.normal_geom;
				while (rdir*lpc->hd.normal_geom > 0.95)
					rdir = Vector3d::random();
				Vector3d pdir = lpc->hd.normal_geom ^ rdir;
				pdir.normalize();
				lpc->hd.out = lpc->hd.normal_geom * cos(angle) + pdir * sin(angle);
				ies = lpc->mlay->ies->getEmission(0.0f, angle);
			}
			else
			{
				lpc->hd.out = lpc->mlay->randomNewDirectionLambert2(lpc->hd, lpdf3);
				cosout = fabs(lpc->hd.normal_shade * lpc->hd.out);
			}
			lSize = 0;
			lpc->pdfAngle = lpdf3;
			lpc->pdfArea = ss->lightMeshPowersPow[ilm]/ss->allLightsPowerPow * 1/(*ss->lightMeshes)[ilm]->totalArea;
			lpc->rr = 1.0f;
			lpc->rrBack = 1.0f;
			lpc->pdfMatLayer = 1.0f;

			outgoing = lpc->mlay->getLightColor(lpc->hd.tU, lpc->hd.tV) * ies * (ss->lightMeshPowers[ilm] * (ss->allLightsPowerPow/ss->lightMeshPowersPow[ilm]) * opacity * cosout);
			lpc->incoming = lpc->mlay->getLightColor(lpc->hd.tU, lpc->hd.tV) * (ss->lightMeshPowers[ilm] * (ss->allLightsPowerPow/ss->lightMeshPowersPow[ilm]) * opacity );

			lpc->brdf = Color4(1.0f, 1.0f, 1.0f);
			outgoing *= 1.0f/lpdf3;
		}	// end GEOM EMITTER

		// SUN
		if (start_type == START_SUN)
		{
			float sApdf;
			Point3d sp = sbb.randomStartPoint(sApdf);

			BDPTNode * lpc = &(lPath[0]);
			lpc->mat = NULL;
			lpc->mlay = NULL;
			lpc->tri = NULL;
			lpc->u = lpc->v = 0.0f;
			lpc->pdfMatLayer = 1.0f;
			lpc->inst_mat.setIdentity();
			lpc->rough = 0.0f;
			lpc->rr = 1.0f;
			lpc->rrBack = 1.0f;
			lpc->sssIn = false;

			lpc->int_point = sp;
			lpc->pdfArea = sApdf;
			lpc->hd.out = ss->sunsky->getRandomPerturbedSunDirection() * -1;
			lpc->hd.in = lpc->hd.normal_shade = lpc->hd.normal_geom = lpc->hd.out;
			lpc->hd.tU = lpc->hd.tV = 0.0f;
			lpc->pdfAngle = 1.0f / ss->sunsky->getSunSizeSteradians();
			lpc->pdfBackAngle = 1.0f;
			lpc->fakeRefl = lpc->fakeTransm = false;

			lpc->brdf = Color4(1.0f, 1.0f, 1.0f);
			lpc->incoming = ss->sunsky->getSunColor();
			if (lpc->incoming.isNaN())
			{
				lSize = -1;
				outgoing = lpc->incoming = Color4(0,0,0);
				return false;
			}
			outgoing = lpc->incoming * ss->sunsky->getSunSizeSteradians();
			outgoing *= (1.0f/sApdf);
			lSize = 0;
		}

		// PORTAL SUN
		if (start_type == START_PORTAL_SUN)
		{
			float pdfRandSkyPortal = 1.0f;
			int skpId = scene->randomSkyPortal(pdfRandSkyPortal);
			SkyPortalMesh * skpMesh = scene->skyportalMeshes[skpId];
			if (!skpMesh)
				return false;

			MeshInstance * mInst = &(scene->instances[skpMesh->instNum]);
			Matrix4d instMatPortal = mInst->matrix_transform; 
			float pdfRandSkpTri;
			int triInMesh = 0;
			int iskptri = skpMesh->randomTriangle(pdfRandSkpTri, triInMesh);
			if (iskptri < 0  ||  iskptri>=scene->triangles.objCount)
				return false;

			Triangle * skptri = &(scene->triangles[iskptri]);
			float skpu, skpv;
			Point3d skpPoint;
			skptri->randomPoint(skpu, skpv, skpPoint);
			skpPoint = instMatPortal * skpPoint;

			BDPTNode * lpc = &(lPath[0]);
			lpc->mat = NULL;
			lpc->mlay = NULL;
			lpc->tri = skptri;
			lpc->u = skpu; lpc->v = skpv;
			lpc->pdfMatLayer = 1.0f;
			lpc->inst_mat = instMatPortal;
			lpc->rough = 0.0f;
			lpc->rr = 1.0f;
			lpc->rrBack = 1.0f;
			lpc->sssIn = false;
			lpc->fakeRefl = lpc->fakeTransm = false;

			lpc->int_point = skpPoint;
			lpc->pdfArea = pdfRandSkyPortal * pdfRandSkpTri;
			lpc->pdfArea = 1.0f / ss->skyPortalsTotalArea;
			lpc->hd.out = ss->sunsky->getRandomPerturbedSunDirection() * -1;
			lpc->hd.normal_geom = instMatPortal * skptri->normal_geom;
			lpc->hd.normal_geom.normalize();
			lpc->hd.in = lpc->hd.normal_shade = lpc->hd.normal_geom;
			lpc->hd.tU = lpc->hd.tV = 0.0f;
			lpc->pdfAngle = 1.0f / ss->sunsky->getSunSizeSteradians();
			lpc->pdfBackAngle = 1.0f;

			lpc->brdf = Color4(1.0f, 1.0f, 1.0f);
			lpc->incoming = ss->sunsky->getSunColor();
			outgoing = lpc->incoming * ss->sunsky->getSunSizeSteradians() * max(0.0f, lpc->hd.out * lpc->hd.normal_geom);
			outgoing *= (1.0f/lpc->pdfArea);
			lSize = 0;
		}

		// PORTAL SKY
		if (start_type == START_PORTAL_SKY)
		{
			float pdfRandSkyPortal = 1.0f;
			int skpId = scene->randomSkyPortal(pdfRandSkyPortal);
			SkyPortalMesh * skpMesh = scene->skyportalMeshes[skpId];
			if (!skpMesh)
				return false;

			MeshInstance * mInst = &(scene->instances[skpMesh->instNum]);
			Matrix4d instMatPortal = mInst->matrix_transform;
			float pdfRandSkpTri;
			int triInMesh = 0;
			int iskptri = skpMesh->randomTriangle(pdfRandSkpTri, triInMesh);
			if (iskptri < 0  ||  iskptri>=scene->triangles.objCount)
				return false;

			Triangle * skptri = &(scene->triangles[iskptri]);
			float skpu, skpv;
			Point3d skpPoint;
			skptri->randomPoint(skpu, skpv, skpPoint);
			skpPoint = instMatPortal * skpPoint;

			BDPTNode * lpc = &(lPath[0]);
			lpc->mat = NULL;
			lpc->mlay = NULL;
			lpc->tri = skptri;
			lpc->u = skpu; lpc->v = skpv;
			lpc->pdfMatLayer = 1.0f;
			lpc->inst_mat = instMatPortal;
			lpc->rough = 1.0f;
			lpc->rr = 1.0f;
			lpc->rrBack = 1.0f;
			lpc->sssIn = false;
			lpc->fakeRefl = lpc->fakeTransm = false;

			lpc->int_point = skpPoint;
			lpc->pdfArea = 1.0f/ss->skyPortalsTotalArea;
			lpc->hd.normal_geom = instMatPortal.getMatrixForNormals() * lpc->tri->normal_geom;
			lpc->hd.normal_geom.normalize();
			lpc->hd.in = lpc->hd.normal_shade = lpc->hd.normal_geom;
			lpc->hd.tU = lpc->hd.tV = 0.0f;
			lpc->hd.out = portalmlay.randomNewDirectionLambert2(lpc->hd, lpc->pdfAngle);
			lpc->pdfBackAngle = 1.0f;

			lpc->brdf = Color4(1.0f, 1.0f, 1.0f);
			lpc->incoming = ss->sunsky->getColor(lpc->hd.out*-1) * (1.0f/lpc->pdfArea);
			outgoing = lpc->incoming * (1.0f / lpc->pdfAngle) * fabs(lpc->hd.normal_geom*lpc->hd.out);
			lSize = 0;
		}

		// PORTAL ENV
		if (start_type == START_PORTAL_ENV)
		{
			float pdfRandSkyPortal = 1.0f;
			int skpId = scene->randomSkyPortal(pdfRandSkyPortal);
			SkyPortalMesh * skpMesh = scene->skyportalMeshes[skpId];
			if (!skpMesh)
				return false;

			MeshInstance * mInst = &(scene->instances[skpMesh->instNum]);
			Matrix4d instMatPortal = mInst->matrix_transform;
			float pdfRandSkpTri;
			int triInMesh = 0;
			int iskptri = skpMesh->randomTriangle(pdfRandSkpTri, triInMesh);
			if (iskptri < 0  ||  iskptri>=scene->triangles.objCount)
				return false;

			Triangle * skptri = &(scene->triangles[iskptri]);
			float skpu, skpv;
			Point3d skpPoint;
			skptri->randomPoint(skpu, skpv, skpPoint);
			skpPoint = instMatPortal * skpPoint;

			BDPTNode * lpc = &(lPath[0]);
			lpc->mat = NULL;
			lpc->mlay = NULL;
			lpc->tri = skptri;
			lpc->u = skpu; lpc->v = skpv;
			lpc->pdfMatLayer = 1.0f;
			lpc->inst_mat = instMatPortal;
			lpc->rough = 1.0f;
			lpc->rr = 1.0f;
			lpc->rrBack = 1.0f;
			lpc->sssIn = false;
			lpc->fakeRefl = lpc->fakeTransm = false;

			lpc->int_point = skpPoint;
			lpc->pdfArea = 1.0f/ss->skyPortalsTotalArea;
			lpc->hd.normal_geom = instMatPortal.getMatrixForNormals() * lpc->tri->normal_geom;
			lpc->hd.normal_geom.normalize();
			lpc->hd.in = lpc->hd.normal_shade = lpc->hd.normal_geom;
			lpc->hd.tU = lpc->hd.tV = 0.0f;
			lpc->hd.out = portalmlay.randomNewDirectionLambert2(lpc->hd, lpc->pdfAngle);
			lpc->pdfBackAngle = 1.0f;

			lpc->brdf = Color4(1.0f, 1.0f, 1.0f);
			lpc->incoming = scene->env.getColor(lpc->hd.out*-1) * (1.0f/lpc->pdfArea);
			outgoing = lpc->incoming * (1.0f / lpc->pdfAngle) * fabs(lpc->hd.normal_geom*lpc->hd.out);
			lSize = 0;
		}
	}

	#ifdef ONLY_PATH_TRACING
		if (!isCameraSide)
			return true;
	#endif
	#ifdef ONLY_CONNECT_TO_CAMERA
		if (isCameraSide)
			return true;
	#endif

	for (int i=1; i<=max_refl; i++)
	{
		Vector3d dirr = chPath[i-1].hd.out;
		Vector3d tng = chPath[i-1].hd.normal_geom;
		Point3d pointr = chPath[i-1].int_point;
		pointr = pointr + tng * (tng*dirr>0.0f ? 0.0001f : -0.0001f);
		float bigf = BIGFLOAT;
		int instanceID = -1;
		int itri = -1;
		float u=0.0f, v=0.0f;
		Matrix4d inst_mat;
		Color4 att1 = Color4(1,1,1);
		bool stopOnPortal = false;
		bool fakeRefl = false, fakeTransm = false;
		if (isCameraSide  &&  can_hit_emitter)
			stopOnPortal = true;
		float invWgt = 1.0f;
		if (isCameraSide  &&  can_hit_emitter)
			invWgt = evalAttenuationForInvisibleLight(i-1, 0);

		float d = findTri(pointr, dirr, inst_mat, bigf, instanceID, itri, u,v, att1, stopOnPortal, invWgt, fakeRefl, fakeTransm);

		if (d<0.0f)
		{
			if (isCameraSide)
			{
				ray_in_sky = true;
				att_to_sky = att1;
			}
			return true;
		}
		BDPTNode * cp = chPath+i;

		cp->clearNode();
		
		cp->tri = &(scene->triangles[itri]);
		cp->inst_mat = inst_mat;
		cp->u = u;
		cp->v = v;
		cp->int_point = inst_mat * cp->tri->getPointFromUV(u,v);
		cp->lastDistance = d;
		cp->fakeRefl = fakeRefl;
		cp->fakeTransm = fakeTransm;

		cp->hd.normal_shade = inst_mat.getMatrixForNormals() * cp->tri->evalNormal(u, v);
		cp->hd.normal_shade.normalize();
		cp->hd.normal_geom = inst_mat.getMatrixForNormals() * cp->tri->normal_geom;
		cp->hd.normal_geom.normalize();

		cp->tri->evalTexUV(u,v, cp->hd.tU, cp->hd.tV);
		cp->hd.in = dirr * -1;
		cp->hd.lastDist = d;
		cp->hd.spectr_pos = -1;
		cp->hd.flags = 0;

		cp->tri->getTangentSpaceSmooth(inst_mat, u,v, cp->hd.dir_U, cp->hd.dir_V);

		cp->mat = scene->mats[scene->instances[instanceID].materials[cp->tri->matInInst]];
		cp->pdfAngle = 1.0f;
		BDPTNode * cPrev = chPath+(i-1);
		cp->pdfArea = cPrev->pdfAngle * cPrev->rr * fabs(cp->hd.normal_geom*cp->hd.in) / (d*d);
		outgoing *= att1;
		cp->incoming = outgoing;

		if ((START_SUN==start_type  ||  START_PORTAL_SUN==start_type)  &&  i==1)
		{
			#ifdef PORTALS_APDF_DIST
			#else
				cp->pdfArea = cPrev->pdfAngle * fabs(cp->hd.normal_geom*cp->hd.in);
			#endif
		}

		if (cp->mat->isSkyPortal  &&  isCameraSide  &&  can_hit_emitter)
		{
			cp->mlay = NULL;
			pdfBackAreaHitEmitter = 1.0f / ss->skyPortalsTotalArea;
			cp->rr = cp->rrBack = 1.0f;
			cp->rough = 1.0f;
			cp->hd.out = cp->hd.normal_shade = cp->hd.normal_geom;
			HitData hd2 = cp->hd;
			hd2.swapDirections();
			cp->pdfBackAngle = portalmlay.getProbabilityLambert(hd2);
			eSize = i;
			return true;
		}

		float rlpdf = 1.0f;
		cp->mlay = cp->mat->getRandomAnyLayer(cp->hd.tU, cp->hd.tV, rlpdf);
		if (!cp->mlay)
			return false;
		cp->pdfMatLayer = rlpdf;

		if (cp->mlay->type == MatLayer::TYPE_EMITTER)
		{
			if (isCameraSide  &&  can_hit_emitter)
			{	// camera to light
				eSize = i;
				cp->hd.out = cp->hd.in;
				if (cp->mlay->ies  &&  cp->mlay->ies->isValid())
				{
					float angle = acos(min(1.0f, max(-1.0f, cp->hd.in * cp->hd.normal_geom)));
					cp->pdfBackAngle = cp->mlay->ies->getProbability(angle);
				}
				else
					cp->pdfBackAngle = cp->mlay->getProbabilityLambert(cp->hd);
				pdfBackAreaHitEmitter = ss->evalPDF(instanceID, itri);
				triIDHitEmitter = itri;
				instanceIDHitEmitter = instanceID;
				cp->rr = 1.0f;
				cp->rrBack = 1.0f;

				return true;
			}
			else
				return true;
		}

		if (isCameraSide)
			eSize = i;
		else 
			lSize = i;

		// ----- SSS -----
		if (cp->mlay->sssON)
		{
			if (i>=max_refl)
				return true;
			cp->evalNormalFromMap();
			cp->rough = cp->mlay->getRough(cp->hd.tU, cp->hd.tV);

			Matrix4d sssInstMat;
			Color4 sssAtt = Color4(1,1,1);
			float sssU=0.0f, sssV=0.0f;
			int sssItri=-1, sssInstID = instanceID;
			MaterialNox * sssmat = cp->mat;
			MatLayer * sssmlay = cp->mlay;
			HitData sssHD;
			bool sssOK = findSSSPoint(sample_time, cp->int_point, cp->hd, sssHD, sssmat, sssmlay, sssInstMat, sssInstID, sssItri, sssU, sssV, sssAtt);	// from pt
			if (!sssOK)
				return true;
			cp->sssIn = true;
			cp->sssOut = false;

			BDPTNode * np = chPath+(i+1);
			np->u=sssU;		np->v=sssV;
			np->sssIn = false;
			np->sssOut = true;

			np->rough = cp->rough;
			np->inst_mat = cp->inst_mat;
			np->mat = cp->mat;
			np->mlay = cp->mlay;
			np->tri = &(scene->triangles[sssItri]);

			np->int_point = sssInstMat * np->tri->getPointFromUV(sssU, sssV);
			np->pdfMatLayer = cp->pdfMatLayer;

			np->hd = sssHD;

			np->brdf = Color4(1.0f, 1.0f, 1.0f);
			np->pdfArea = 1.0f;
			np->pdfAngle = 1.0f;
			np->pdfBackAngle = 1.0f;
			np->rr = 1.0f;
			np->rrBack = 1.0f;

			np->incoming = cp->incoming * sssAtt;
			outgoing *= sssAtt;

			cp->hd.out = cp->hd.in * -1;
			cp->pdfAngle = 1.0f;
			cp->pdfBackAngle = 1.0f;
			HitData hd2 = cp->hd;
			hd2.swapDirections();
			cp->pdfBackAngle = cp->mlay->getProbability(hd2);
			cp->rr = 1.0f;
			cp->rrBack = 1.0f;
			cp->brdf = Color4(1,1,1);

			cp = np;

			i++;
			if (isCameraSide)
				eSize = i;
			else 
				lSize = i;
		}

		cp->evalNormalFromMap();
		cp->hd.adjustNormal();
		cp->rough = cp->mlay->getRough(cp->hd.tU, cp->hd.tV);

		cp->hd.spectr_pos = isCameraSide ? spectr_pos_cam : spectr_pos_emitter;
		float rndpdf=1.0f;
		cp->hd.clearFlagInvalidRandom();
		cp->hd.out = cp->mlay->randomNewDirection(cp->hd, rndpdf);
		bool valid_rand = !cp->hd.isFlagInvalidRandom();
		if (isCameraSide)
			spectr_pos_cam = cp->hd.spectr_pos;
		else
			spectr_pos_emitter = cp->hd.spectr_pos;

		if (rndpdf<0.001f)
			rndpdf = 0.001f;
		float fcossout = cp->hd.isFlagDelta() ? 1.0f : fabs(cp->hd.out * cp->hd.normal_shade);	// all layers brdf moze miec wplyw
		float fcossin = cp->hd.isFlagDelta() ? 1.0f : (isCameraSide ? fabs(cp->hd.in * cp->hd.normal_geom) : fabs(cp->hd.in * cp->hd.normal_shade));	// all layers brdf moze miec wplyw

		#ifdef BRDF_ALL_LAYERS
			cp->updatePdfsBrdfMultilayered(isCameraSide, true);
		#else
			HitData hd2 = cp->hd;
			hd2.swapDirections();
			cp->pdfAngle = rndpdf;
			#ifdef COS_IN_BRDF
				cp->brdf = cp->mlay->getBRDF(cp->hd) * (1.0f/rlpdf) * fcossout;
			#else
				cp->brdf = cp->mlay->getBRDF(cp->hd) * (1.0f/rlpdf);
			#endif
			cp->pdfBackAngle = cp->mlay->getProbability(hd2);
		#endif
		#ifdef COS_IN_BRDF
			Color4 reflpower = cp->brdf;
		#else
			Color4 reflpower = cp->brdf * fcoss;
		#endif
		reflpower *= (1.0f/cp->pdfAngle);

		float shade_corr = 1.0f;
		if (!isCameraSide)
			float shade_corr = fabs((cp->hd.in * cp->hd.normal_shade) / (cp->hd.in * cp->hd.normal_geom));

		float scos = cp->hd.isFlagDelta() ? 1.0f : fabs(cp->hd.out * cp->hd.normal_shade);	// all layers brdf moze miec wplyw

		// RUSSIAN ROULETTE
		float rrPdf = shade_corr*max(reflpower.r, max(reflpower.g, reflpower.b));
		rrPdf = min(1.0f, rrPdf);
		if (i<=NON_RR_COUNT)
			rrPdf = 1.0f;
		#ifdef RAND_PER_THREAD
			float rtemp = rg->getRandomFloat();
		#else
			float rtemp = rand()/(float)RAND_MAX;
		#endif
		if (cp->rough == 0.0f)
			rrPdf = 1.0f;
		if (!valid_rand)
			return true;
		if (rtemp > rrPdf)
			return true;
		cp->rr = rrPdf;
		#ifdef COS_IN_BRDF
			float rback = max(cp->brdf.r, max(cp->brdf.g, cp->brdf.b)) * fcossin / fcossout;
		#else
			float rback = max(cp->brdf.r, max(cp->brdf.g, cp->brdf.b)) * fabs(cp->hd.in * cp->hd.normal_shade);
		#endif
		cp->rrBack = min(1.0f, rback/cp->pdfBackAngle);

		reflpower *= shade_corr;	// going from emitter is always non-smooth, need to correct

		if (reflpower.isInf()  ||  reflpower.isNaN())
			return true;
		if (reflpower.isBlack())
			return true;
		outgoing *= reflpower * (1.0f/rrPdf);

	}

	return true;
}

//----------------------------------------------------------------

bool BDPT::isShadowOpacFakeGlass(const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, Color4 &attenuation, bool stopOnPortal)
{
	attenuation = Color4(1.0f, 1.0f, 1.0f);
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	
	Point3d stPoint = origin;
	float md = maxDist;
	iitri = -1;
	float sum_d = 0.0f;

	Point3d pEnd;
	Vector3d dir = direction;
	bool evaldir = false;
	if (maxDist<BIGFLOAT)
	{
		evaldir = true;
		pEnd = origin + direction * maxDist;
	}
	Vector3d geom_normal = direction;

	bool checked = false;
	int ii = 0;
	while (!checked)
	{
		if (ii++>1000)
		{
			Logger::add("isShadowOpacFakeGlass stucked");
			return false;
		}

		float u,v, d;
		int itri;
		
		if (evaldir)
		{
			dir = pEnd - stPoint;
			md = dir.normalize_return_old_length();
			md -= 0.0001f;
			float dw = dir * geom_normal;
			dir = dir * dw + direction * (1.0f-dw);
			dir.normalize();
		}

		Matrix4d inst_mat;
		inst_mat.setIdentity();
		int instID = 0;
		if (rtr->use_embree)
			d = intersectEmbree(scene, stPoint, dir, inst_mat, md, instID, itri, u,v);
		else
			d = rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(sample_time, stPoint, dir, inst_mat, md, instID, itri, u,v);
		if (d<0 || itri<0)
			return false;

		sum_d += d;

		Triangle tri = (scene->triangles[itri]);
		geom_normal = inst_mat.getMatrixForNormals() * tri.normal_geom;
		geom_normal.normalize();

		MaterialNox * fmat = scene->mats[scene->instances[instID].materials[tri.matInInst]];
		float tU, tV;
		tri.evalTexUV(u, v, tU, tV);
		Point3d inters = tri.getPointFromUV(u,v);
		inters = inst_mat * inters;
		Vector3d normal = inst_mat.getMatrixForNormals() * tri.evalNormal(u, v);
		normal.normalize();

		if (fmat->isSkyPortal)
		{
			if (stopOnPortal)
			{
				iitri = itri;
				instMatrix = inst_mat; 
				instanceID = instID;
				maxDist = sum_d;

				return true;
			}

			stPoint = inters + geom_normal * ((dir*geom_normal>0) ? 0.0001f : -0.0001f);
			continue;
		}

		if (fmat->hasInvisibleEmitter)
		{
			stPoint = inters + geom_normal * ((direction*geom_normal>0) ? 0.0001f : -0.0001f);
			continue;
		}

		float opacity = fmat->getOpacity(tU, tV);
		if (!fmat->hasFakeGlass   &&   opacity>=1.0f)
		{
			iitri = itri;
			instMatrix = inst_mat; 
			instanceID = instID;
			maxDist = sum_d;
			return true;
		}


		Vector3d uvdirU, uvdirV;
		tri.getTangentSpaceSmooth(inst_mat, u,v, uvdirU, uvdirV);
		HitData hd(direction*-1.0f, direction, normal, geom_normal, uvdirU, uvdirV, tU, tV, 0.0f, -1.0f);
		hd.adjustNormal();
		hd.lastDist = d;

		float fpdf = 1;
		MatLayer * flay = NULL;
		flay = fmat->getRandomFakeGlassLayer(tU, tV, fpdf);
		if (flay  &&  fpdf>0.0001f)
		{
			Color4 fbrdf = flay->getBRDF(hd);
			fbrdf = fbrdf * opacity + Color4(1,1,1) * (1.0f-opacity);

			if (fbrdf.isBlack()  ||   fbrdf.isInf()   ||   fbrdf.isNaN())
			{
				iitri = itri;
				instMatrix = inst_mat; 
				instanceID = instID;
				maxDist = sum_d;

				return true;
			}
			attenuation *= fbrdf;
		}
		else
		{
			attenuation *= 1.0f-opacity;
		}

		stPoint = inters + geom_normal * ((direction*geom_normal>0) ? 0.0001f : -0.0001f);
	}

	return false;
}

//----------------------------------------------------------------

float BDPT::findNonOpacFakeTri(const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, float &uu, float &vv, Color4 &attenuation, bool stopOnPortal)
{
	attenuation = Color4(1.0f, 1.0f, 1.0f);
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();

	Point3d stPoint = origin;
	float md = maxDist;

	Point3d pEnd;
	Vector3d dir = direction;
	bool evaldir = false;
	if (maxDist<BIGFLOAT)
	{
		evaldir = true;
		pEnd = origin + direction * maxDist;
	}

	float sum_d = 0.0f;

	int ii = 0;
	bool checked = false;
	while (!checked)
	{
		if (ii++>1000)
		{
			Logger::add("findNonOpacFakeTri stucked");
			return -1;
		}

		float u,v, d;
		int itri;
		
		if (evaldir)
		{
			dir = pEnd - stPoint;
			md = dir.normalize_return_old_length();
		}

		Matrix4d inst_mat;
		inst_mat.setIdentity();
		int instID = 0;

		if (rtr->use_embree)
			d = intersectEmbree(scene, stPoint, dir, inst_mat, md, instID, itri, u, v);
		else
			d = rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(sample_time, stPoint, dir, inst_mat, md, instID, itri, u, v);
		if (d<0 || itri<0)
		{
			return -1;
		}

		sum_d += d;

		Triangle tri = (scene->triangles[itri]);
		MaterialNox * fmat = scene->mats[scene->instances[instID].materials[tri.matInInst]];
		float tU, tV;
		tri.evalTexUV(u, v, tU, tV);
		Point3d inters = tri.getPointFromUV(u,v);
		inters = inst_mat * inters;
		Vector3d tri_normal_geom = inst_mat.getMatrixForNormals() * tri.normal_geom;
		tri_normal_geom.normalize();

		if (fmat->isSkyPortal)
		{
			if (stopOnPortal)
			{
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat; 
				instanceID = instID;
				return sum_d;
			}

			stPoint = inters + tri_normal_geom * ((direction*tri_normal_geom>0) ? 0.0001f : -0.0001f);
			continue;
		}
		
		if (fmat->hasInvisibleEmitter)
		{
			attenuation *= 2.0f;
			float r = rg->getRandomFloat();
			if (r<0.5f)
			{
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat; 
				instanceID = instID;
				return sum_d;
			}
			stPoint = inters + tri_normal_geom * ((direction*tri_normal_geom>0) ? 0.0001f : -0.0001f);
			continue;
		}

		float opacity = fmat->getOpacity(tU, tV);
		bool emitterstop = false;
		if (fmat->hasEmitters)
		{
			#ifdef RAND_PER_THREAD
				emitterstop = (rg->getRandomFloat() < opacity);
			#else
				emitterstop = (rand()/(float)RAND_MAX < opacity);
			#endif
		}
		if ((!fmat->hasFakeGlass   &&   opacity>=1.0f)  ||  emitterstop)
		{
			iitri = itri;
			uu = u;
			vv = v;
			instMatrix = inst_mat; 
			instanceID = instID;
			return sum_d;
		}
			
		Vector3d normal = tri.evalNormal(u, v);
		normal = inst_mat.getMatrixForNormals() * normal;
		normal.normalize();

		Vector3d uvdirU, uvdirV;
		tri.getTangentSpaceSmooth(inst_mat, u,v, uvdirU, uvdirV);

		HitData hd(direction*-1.0f, direction, normal, tri_normal_geom, uvdirU, uvdirV, tU, tV, 0.0f, -1.0f);
		hd.adjustNormal();
		hd.lastDist = d;

		float fpdf = 1;
		MatLayer * flay = fmat->getRandomFakeGlassLayer(tU, tV, fpdf);
		if (flay  &&  fpdf>0.0001f)
		{
			Color4 fbrdf = flay->getBRDF(hd);
			fbrdf = fbrdf * (1.0f/fpdf) * opacity + Color4(1,1,1) * (1.0f-opacity);
			if (fbrdf.isBlack()  ||   fbrdf.isInf()   ||   fbrdf.isNaN())
			{
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat; 
				instanceID = instID;
				return sum_d;
			}
			attenuation *= fbrdf;
		}
		else
		{
			attenuation *= 1.0f-opacity;
		}

		stPoint = inters + normal * ((direction*normal>0) ? 0.0001f : -0.0001f);
	}

	return -1;
}

//----------------------------------------------------------------

float BDPT::findTri(const Point3d &origin, Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, float &uu, float &vv, Color4 &attenuation, 
					bool stopOnPortal, float invWgt, bool &fakeRefl, bool &fakeTransm)
{
	attenuation = Color4(1.0f, 1.0f, 1.0f);
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
	fakeRefl = false;
	fakeTransm = false;

	Point3d stPoint = origin;
	float md = maxDist;

	Point3d pEnd;
	Vector3d dir = direction;
	bool evaldir = false;
	if (maxDist<BIGFLOAT)
	{
		evaldir = true;
		pEnd = origin + direction * maxDist;
	}

	float sum_d = 0.0f;

	int ii = 0;
	bool checked = false;
	while (!checked)
	{
		if (ii++>1000)
		{
			Logger::add("findTri stucked");
			return -1;
		}

		float u,v, d;
		int itri;
		
		if (evaldir)
		{
			dir = pEnd - stPoint;
			md = dir.normalize_return_old_length();
		}

		Matrix4d inst_mat;
		inst_mat.setIdentity();
		int instID = 0;

		if (rtr->use_embree)
			d = intersectEmbree(scene, stPoint, dir, inst_mat, md, instID, itri, u, v);
		else
			d = rtr->curScenePtr->sscene.bvh.intersectForTrisNonSSE(sample_time, stPoint, dir, inst_mat, md, instID, itri, u, v);
		if (d<0 || itri<0)
		{
			return -1;
		}

		sum_d += d;

		Triangle tri = (scene->triangles[itri]);
		MaterialNox * fmat = scene->mats[scene->instances[instID].materials[tri.matInInst]];
		float tU, tV;
		tri.evalTexUV(u, v, tU, tV);
		Point3d inters = tri.getPointFromUV(u,v);
		inters = inst_mat * inters;
		Vector3d tri_normal_geom = inst_mat.getMatrixForNormals() * tri.normal_geom;
		tri_normal_geom.normalize();

		if (fmat->isSkyPortal)
		{
			if (stopOnPortal)
			{
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat; 
				instanceID = instID;
				return sum_d;
			}

			stPoint = inters + tri_normal_geom * ((direction*tri_normal_geom>0) ? 0.0001f : -0.0001f);
			continue;
		}
		
		if (fmat->hasInvisibleEmitter)
		{
			if (direction*tri_normal_geom>0  ||  invWgt<=0.0f)
			{
				stPoint = inters + tri_normal_geom * ((direction*tri_normal_geom>0) ? 0.0001f : -0.0001f);
				continue;
			}
			invWgt = min(invWgt, 0.5f);
			float r = rg->getRandomFloat();
			if (r<invWgt)
			{
				attenuation *= 1.0f/invWgt;
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat;
				instanceID = instID;
				return sum_d;
			}
			attenuation *= 1.0f/(1.0f-invWgt);
			stPoint = inters + tri_normal_geom * ((direction*tri_normal_geom>0) ? 0.0001f : -0.0001f);
			continue;
		}

		float opacity = fmat->getOpacity(tU, tV);
		bool opacstop = true;
		if (opacity<1)
		{
			#ifdef RAND_PER_THREAD
				opacstop = (rg->getRandomFloat() < opacity);
			#else
				opacstop = (rand()/(float)RAND_MAX < opacity);
			#endif
		}
		if (opacstop  &&  !fmat->hasFakeGlass) 
		{
			iitri = itri;
			uu = u;
			vv = v;
			instMatrix = inst_mat; 
			instanceID = instID;
			return sum_d;
		}
			
		Vector3d normal = tri.evalNormal(u, v);
		normal = inst_mat.getMatrixForNormals() * normal;
		normal.normalize();

		Vector3d uvdirU, uvdirV;
		tri.getTangentSpaceSmooth(inst_mat, u,v, uvdirU, uvdirV);

		HitData hd(direction*-1.0f, direction, normal, tri_normal_geom, uvdirU, uvdirV, tU, tV, 0.0f, -1.0f);
		hd.adjustNormal();
		hd.lastDist = d;
	
		opacity = 1;
		float fpdf = 1;
		MatLayer * flay = fmat->getRandomFakeGlassLayer(tU, tV, fpdf);
		if (flay  &&  fpdf>0.0001f)
		{
			float dpdf;
			Vector3d newdirection = flay->randomNewDirection(hd, dpdf);
			if (direction*newdirection < 0.99f)
				fakeRefl = true;
			else
				fakeTransm = true;
			dir = hd.out = direction = newdirection;
			Color4 fbrdf = flay->getBRDF(hd);
			fbrdf = fbrdf * (1.0f/dpdf) * opacity + Color4(1,1,1) * (1.0f-opacity);
			if (fbrdf.isBlack()  ||   fbrdf.isInf()   ||   fbrdf.isNaN())
			{
				iitri = itri;
				uu = u;
				vv = v;
				instMatrix = inst_mat; 
				instanceID = instID;
				return sum_d;
			}
			attenuation *= fbrdf;
		}

		stPoint = inters + normal * ((direction*normal>0) ? 0.0001f : -0.0001f);
	}

	return -1;
}

//----------------------------------------------------------------

bool BDPT::visibilityTest(int eI, int lI, Color4 &attenuation)
{
	Raytracer * rtr = Raytracer::getInstance();
	attenuation = Color4(1.0f, 1.0f, 1.0f);
	BDPTNode * eNode = ePath+eI;
	BDPTNode * lNode = lPath+lI;
	bool sunconn = (lI==0   &&   (cur_emitter_type==START_SUN  ||  cur_emitter_type==START_PORTAL_SUN));

	Vector3d tdir = lNode->int_point - eNode->int_point;
	if (sunconn)
		tdir = lNode->hd.out * -1;
	Point3d ePoint = eNode->int_point + eNode->hd.normal_geom * (tdir*eNode->hd.normal_geom > 0 ? 0.0001f : -0.0001f);
	Point3d lPoint = lNode->int_point + lNode->hd.normal_geom * (tdir*lNode->hd.normal_geom < 0 ? 0.0001f : -0.0001f);
	Vector3d elDir = lPoint - ePoint;
	float maxdist = elDir.normalize_return_old_length();

	if (sunconn)
	{
		maxdist = BIGFLOAT;
		elDir = lNode->hd.out * -1;
	}

	int instanceID = -1;
	int itri = -1;
	float u=0.0f, v=0.0f;
	Matrix4d inst_mat;

	bool stopOnPortal = false;
	bool shadow = isShadowOpacFakeGlass(ePoint, elDir, inst_mat, maxdist, instanceID, itri, attenuation, stopOnPortal);
	return !shadow;
}

//----------------------------------------------------------------

Color4 BDPT::evalPathContributionUnweighted(int eI, int lI)
{
	Color4 emission = lPath[0].incoming  * (1.0f / lPath[0].pdfArea);
	Color4 cntr = Color4(1,1,1);
	for (int i=1; i<=eI; i++)
	{
		float cosprev = fabs(ePath[i-1].hd.out * ePath[i-1].hd.normal_shade);
		if (i==1)
			cosprev = 1.0f;
		float coscur = fabs(ePath[i].hd.in * ePath[i].hd.normal_shade);
		float d2 = (ePath[i-1].int_point - ePath[i].int_point).lengthSqr();
		float pA = ePath[i].pdfArea;
		#ifdef COS_IN_BRDF
			cntr *= ePath[i].brdf * coscur * (1.0f / d2 / pA);
		#else
			cntr *= ePath[i].brdf * cosprev * coscur * (1.0f / d2 / pA);
		#endif
	}
	for (int i=1; i<=lI; i++)
	{
		float cosprev = fabs(lPath[i-1].hd.out * lPath[i-1].hd.normal_shade);
		if (i==1)
			cosprev = max(0.0f, lPath[i-1].hd.out * lPath[i-1].hd.normal_shade);
		float coscur = fabs(lPath[i].hd.in * lPath[i].hd.normal_shade);
		float d2 = (lPath[i-1].int_point - lPath[i].int_point).lengthSqr();
		float pA = lPath[i].pdfArea;
		#ifdef COS_IN_BRDF
			cntr *= lPath[i].brdf * coscur * (1.0f / d2 / pA);
		#else
			cntr *= lPath[i].brdf * cosprev * coscur * (1.0f / d2 / pA);
		#endif
		if (i==1)
			cntr *=d2;
	}

	float cosL = fabs(lPath[lI].hd.out * lPath[lI].hd.normal_shade);
	float cosE = fabs(ePath[eI].hd.out * ePath[eI].hd.normal_shade);
	float d2 = (lPath[lI].int_point - ePath[eI].int_point).lengthSqr();
	if (lI==0)
		cosL = max(0.0f, lPath[0].hd.out * lPath[0].hd.normal_shade);
	if (eI==0)
		cosE = 1.0f;
	#ifdef COS_IN_BRDF
		cntr *= 1.0f / d2;
	#else
		cntr *= cosL * cosE / d2;
	#endif

	cntr *= emission;

	return cntr;
}

//----------------------------------------------------------------

float BDPT::evalWeight	(int eI, int lI, bool make_log)
{
	CNT_WEIGHT_EVAL_START;
	#define float_wprec double
	float_wprec ps[BDPT_MAX_NODES*2+2];
	float_wprec pAF[BDPT_MAX_NODES*2+2];
	float_wprec pAB[BDPT_MAX_NODES*2+2];
	bool speculars[BDPT_MAX_NODES*2+2];
	bool SSSs[BDPT_MAX_NODES*2+2];
	bool fakeReflections[BDPT_MAX_NODES*2+2];
	bool fakeTransmissions[BDPT_MAX_NODES*2+2];
	int s = eI+lI+2;

	float invatt = 1.0f;
	if (cur_emitter_type==START_EMITTER  &&  lPath[0].mlay  &&  lPath[0].mlay->type==MatLayer::TYPE_EMITTER  &&  lPath[0].mlay->emissInvisible)
		invatt = evalAttenuationForInvisibleLight(eI, lI);

	if (eI==0  &&  lI==0)
		#ifdef ONLY_CONNECT_TO_CAMERA
			return invatt;
		#else
			return 0.0f;
		#endif
	if (eI==1  &&  lI==-1)	// direct light on camera hit
		return invatt;

	float p_etl_size = 1.0f/etl_size;
	bool sun_conn = (cur_emitter_type==START_SUN   ||   cur_emitter_type==START_PORTAL_SUN);
	for (int i=0; i<BDPT_MAX_NODES*2+2; i++)
		SSSs[i] = false;
	for (int i=0; i<BDPT_MAX_NODES*2+2; i++)
		fakeReflections[i] = fakeTransmissions[i] = false;

	for (int i=0; i<s-1; i++)
	{
		bool e1 = (i<=eI);
		bool e2 = (i<eI);
		BDPTNode * v1 = e1 ? &(ePath[i]) : &(lPath[lI-(i-eI-1)]);
		BDPTNode * v2 = e2 ? &(ePath[i+1]) : &(lPath[lI-(i-eI)]);
		float d2 = (v1->int_point-v2->int_point).lengthSqr();
		speculars[i] = v1->hd.isFlagDelta();
		#ifdef PORTALS_APDF_DIST
		#else
			if (sun_conn  &&  i==s-2)
				d2 = 1;
		#endif

		if (e1)
		{
			if (e2)
			{	// both on eye path
				d2 = v2->lastDistance * v2->lastDistance;
				fakeReflections[i+1] = v2->fakeRefl;
				fakeTransmissions[i+1] = v2->fakeTransm;
				float rrback = (v2->hd.isFlagDelta() || (i<s-2 && i>s-NON_RR_COUNT-2)) ? 1.0f : v2->rrBack;
				pAF[i+1] = v2->pdfArea;
				pAB[i] = v2->pdfBackAngle * rrback / d2 * fabs(v1->hd.out*v1->hd.normal_geom);
				if (v1->sssIn && v2->sssOut)
				{
					SSSs[i+1] = true;
					pAB[i] = 1;
				}
			}
			else
			{	// 1 on eye, 2 on light
				fakeReflections[i+1] = false;
				fakeTransmissions[i+1] = false;
				float rr1 = (v1->hd.isFlagDelta() || (i<=NON_RR_COUNT)) ? 1.0f : v1->rr;
				float rr2 = (v2->hd.isFlagDelta() || (i<s-2 && i>s-NON_RR_COUNT-2)) ? 1.0f : v2->rr;
				pAF[i+1] = v1->pdfAngle * rr1 / d2 * fabs(v2->hd.out*v2->hd.normal_geom);
				pAB[i] = v2->pdfAngle * rr2 / d2 * fabs(v1->hd.out*v1->hd.normal_geom);
				if (v1->sssIn && v2->sssIn)
					SSSs[i+1] = true;
			}
		}
		else
		{	// !e1 -> !e2	// both on light
			d2 = v1->lastDistance * v1->lastDistance;
			fakeReflections[i+1] = v1->fakeRefl;
			fakeTransmissions[i+1] = v1->fakeTransm;
			float rrback = (v1->hd.isFlagDelta() || (i<=NON_RR_COUNT)) ? 1.0f : v1->rrBack;
			pAF[i+1] = v1->pdfBackAngle * rrback / d2 * fabs(v2->hd.out*v2->hd.normal_geom);
			pAB[i] = v1->pdfArea;
			if (v2->sssIn && v1->sssOut)
			{
				SSSs[i+1] = true;
				pAF[i+1] = 1;
			}
		}
	}

	speculars[s-1] = false;
	pAF[0] = 1.0f;	// point on camera
	pAB[0] = 0.0f;	// can't hit camera

	if (lI<0)	// emitter/portal was hit, pArea was remembered in other var
		pAB[s-1] = pdfBackAreaHitEmitter;
	else		// just a connection
		pAB[s-1] = lPath[0].pdfArea;

	if (!can_hit_emitter)
		pAF[s-1] = 0.0f;	// can't hit emitter
	if (!connect_to_cam)
		pAB[1] = 0.0f;	// can't connect to camera

	#ifdef ONLY_CONNECT_TO_CAMERA
		pAF[1]=0.0f;
	#endif
	#ifdef ONLY_PATH_TRACING
		pAB[max(0, s-2)] = 0.0f;
	#endif

	float pdfRndSunDir = (float)pAB[s-1];
	#ifdef SUN_PORTAL_CONNECTION_REPAIR
		if (sun_conn)
		{
			float d2 = 1.0f;
			float lcos = 1.0f;
			if (lI<0)	// portal was hit
			{
				d2 = (ePath[eI].int_point - ePath[eI-1].int_point).lengthSqr();
				lcos = fabs(ePath[eI].hd.in * ePath[eI].hd.normal_geom);
			}
			else		// portal connected
			{
				if (lI>0)
					d2 = (lPath[0].int_point - lPath[1].int_point).lengthSqr();
				else
					d2 = (lPath[0].int_point - ePath[eI].int_point).lengthSqr();
				lcos = fabs(lPath[0].hd.out * lPath[0].hd.normal_geom);
			}
			pdfRndSunDir = 1.0f*lcos/ss->sunsky->getSunSizeSteradians() / d2;
		}
	#endif

	if (cur_emitter_type==START_SUN || cur_emitter_type==START_EMITTER || cur_emitter_type==START_PORTAL_SUN || cur_emitter_type==START_PORTAL_SKY || cur_emitter_type==START_PORTAL_ENV)
		pAB[s-1] *= p_etl_size;

	double psum = 0;
	for (int i=0; i<s+1; i++)
	{
		double pp = 1.0;
		for ( int ee=0; ee<i; ee++)
			pp *= pAF[ee];
		for (int ll=i; ll<s; ll++)
			pp *= pAB[ll];
		#ifdef SUN_PORTAL_CONNECTION_REPAIR
			if (sun_conn  &&  i==s-1)	// sun connection correction
				pp *= pdfRndSunDir/pAB[s-1] ;
		#endif

		if (SSSs[i])
			pp = 0;
		if (i>0 && speculars[i-1])	// spec on eye conn
			pp = 0;
		if (i<s && speculars[i])	// spec on light conn
			pp = 0;
		if (fakeReflections[i])
			pp = 0;
		ps[i] = pp;
		psum += pp*pp;
	}

	float_wprec res = ps[eI+1]*ps[eI+1]/psum;
	res *= invatt;

	CNT_WEIGHT_EVAL_STOP;

	if (make_log || _isnan(res) )
	{
		if (make_log)
			Logger::add("log forced");
		char buf[512];
		Logger::add("weight is nan");
		if (psum==0.0f)
			Logger::add("sum=0");
		if (res<0.999f)
			Logger::add("weight<1");
		if (lI<0)
			Logger::add("to light");

		sprintf_s(buf, 512, "e=%d  l=%d  s=%d  w=%f", eI, lI, s, res); 
		Logger::add(buf);
		sprintf_s(buf, 512, "w = %f =  %f / %f ", res, (ps[eI+1]*ps[eI+1]), psum); 
		Logger::add(buf);
		for (int i=0; i<s; i++)
		{
			sprintf_s(buf, 512, "pAF[%d] = %f", i, pAF[i]); 
			Logger::add(buf);
		}
		for (int i=0; i<s; i++)
		{
			sprintf_s(buf, 512, "pAB[%d] = %f", i, pAB[i]); 
			Logger::add(buf);
		}
		if (sun_conn)
		{
			sprintf_s(buf, 512, "pAB[%d] = %f sun portal corrected", s-1, pdfRndSunDir); 
			Logger::add(buf);
		}
		for (int i=0; i<s+1; i++)
		{
			sprintf_s(buf, 512, "ps[%d] = %f", i, ps[i]); 
			Logger::add(buf);
		}
		for (int i=0; i<s+1; i++)
		{
			sprintf_s(buf, 512, "fake reflection at %d", i); 
			if (fakeReflections[i])
				Logger::add(buf);
			sprintf_s(buf, 512, "fake transmission at %d", i); 
			if (fakeTransmissions[i])
				Logger::add(buf);
		}
		for (int i=0; i<=eI; i++)
		{
			sprintf_s(buf, 512, "Eye pDir[%d] = %f     rrF=%.6f  rrB=%.6f  pDirBack[%d] = %f    dist=%f", i, ePath[i].pdfAngle, ePath[i].rr, ePath[i].rrBack, i, ePath[i].pdfBackAngle, ePath[i].lastDistance); 
			Logger::add(buf);
		}
		for (int i=0; i<=lI; i++)
		{
			sprintf_s(buf, 512, "Light pDir[%d] = %f     rrF=%.6f  rrB=%.6f   pDirBack[%d] = %f    dist=%f", i, lPath[i].pdfAngle, lPath[i].rr, lPath[i].rrBack, i, lPath[i].pdfBackAngle, lPath[i].lastDistance); 
			Logger::add(buf);
		}


		if (lSize>-1)
		{
			sprintf_s(buf, 512, "LA = %f", lPath[0].pdfArea); 
			Logger::add(buf);
		}
		if (lI>-1)
		{
			sprintf_s(buf, 512, "dist = %f", (lPath[lI].int_point-ePath[eI].int_point).length()); 
			Logger::add(buf);
		}


	}

	return (float)res;
	return 1.0f;
}

//----------------------------------------------------------------

float BDPT::evalAttenuationForInvisibleLight(int eI, int lI)
{
	if (eI==0  &&  lI==0)
		return 0.0f;
	if (eI==1  &&  lI==-1)
		return 0.0f;

	float res = 1.0f;
	int s = eI+lI+2;
	for (int i=1; i<s-1; i++)
	{
		BDPTNode * v1 = (i<=eI) ? &(ePath[i]) : &(lPath[lI-(i-eI-1)]);
		if ((v1->hd.in*v1->hd.normal_geom) * (v1->hd.out*v1->hd.normal_geom) >= 0.0f)
			return 1.0f;	// illuminate reflection
		if (v1->mlay->type == MatLayer::TYPE_SHADE)
			res *= v1->rough;
			//res *= v1->mlay->roughness;
		else
			res = 0;
	}
	return res;
}

//----------------------------------------------------------------

void BDPT::makeLog(int iE, int iL)
{
	char buf[512];
	sprintf_s(buf, 512, "numE=%d   numL=%d   conE=%d   conL=%d", eSize, lSize, iE, iL);
	Logger::add(buf);

	for (int i=0; i<=iE; i++)
	{
		sprintf_s(buf, 512, "E %2d  r=%.3f  pA=%.6f  pDir=%.6f  incoming=%f  %f  %f  cos_in=%.4f  cos_out=%.4f", i, ePath[i].rough, ePath[i].pdfArea, ePath[i].pdfAngle, 
					ePath[i].incoming.r, ePath[i].incoming.g, ePath[i].incoming.b, (ePath[i].hd.in*ePath[i].hd.normal_geom), (ePath[i].hd.out*ePath[i].hd.normal_geom));
		Logger::add(buf);
	}
	for (int i=0; i<=iL; i++)
	{
		sprintf_s(buf, 512, "L %2d  r=%.3f  pA=%.6f  pDir=%.6f  incoming=%f  %f  %f  cos_in=%.4f  cos_out=%.4f", i, lPath[i].rough, lPath[i].pdfArea, lPath[i].pdfAngle, 
					lPath[i].incoming.r, lPath[i].incoming.g, lPath[i].incoming.b, (lPath[i].hd.in*lPath[i].hd.normal_geom), (lPath[i].hd.out*lPath[i].hd.normal_geom));
		Logger::add(buf);
	}
}

//-----------------------------------------------------------------------------------------------------

void BDPTNode::evalNormalFromMap()
{
	if (!tri  ||  !mlay)
		return;
	if (!mlay->tex_normal.isValid())
		return;

	Vector3d vecU, vecV, vecU1, vecV1;
	tri->getTangentSpaceSmooth(inst_mat, u,v, vecU, vecV);

	vecU1 = hd.normal_shade ^ vecU;
	vecU = vecU1 ^ hd.normal_shade;
	vecU.normalize();

	vecV1 = hd.normal_shade ^ vecV;
	vecV = vecV1 ^ hd.normal_shade;
	vecV.normalize();

	Vector3d nMap = mlay->getNormalMap(hd.tU, hd.tV);
	Vector3d newnormal = vecU * nMap.x + vecV * nMap.y + hd.normal_shade * nMap.z;
	newnormal.normalize();

	hd.normal_shade = newnormal;
	hd.adjustNormal();
}

//-----------------------------------------------------------------------------------------------------

bool BDPTNode::isDeltaDistr(float tu, float tv)
{
	if (!mat)
		return false;
	for (int i=0; i<mat->layers.objCount; i++)
		if (mat->layers[i].getRough(tu, tv)>0)
			return false;
	return true;
}

//-----------------------------------------------------------------------------------------------------

bool BDPTNode::isDeltaDistrCurLayer()
{
	if (!mlay)
		return false;
	return (mlay->roughness==0.0f);
}

//-----------------------------------------------------------------------------------------------------

bool BDPTNode::updatePdfsBrdfMultilayered(bool fromCam, bool correctPdf)
{	
	// correctPdf only on reflection, false on connection
	if (!mat)
		return false;

	CNT_MULTILAYERS_START;

	bool sssHere = sssIn || sssOut;
	brdf = Color4(0,0,0);
	float pdf = 0.0f;
	float backpdf = 0.0f;
	float sumw = 0.0f;
	float tcos = fromCam ? fabs(hd.out * hd.normal_shade) : fabs(hd.out * hd.normal_geom);
	for (int j=0; j<mat->layers.objCount; j++)
	{
		hd.clearFlagDelta();
		hd.clearFlagSSS();
		MatLayer * tmlay = &(mat->layers[j]);

		Color4 tbrdf;
		float tpdf, tbpdf;
		tmlay->getBRDFandPDFs(hd, tbrdf, tpdf, tbpdf);
		#ifdef COS_IN_BRDF
			if (!hd.isFlagDelta())
				tbrdf *= tcos;
		#endif

		if (hd.isFlagSSS() && sssHere && tpdf > 0)
		{
			pdfAngle = tpdf;
			pdfBackAngle = tbpdf;
			brdf = tbrdf;
			float w2 = 0;
			for (int k=0; k<mat->layers.objCount; k++)
				w2 += mat->layers[k].getWeight(hd.tU, hd.tV);
			brdf *= w2;
			CNT_MULTILAYERS_STOP;
			return true;
		}

		if (hd.isFlagDelta() && tpdf > 0)
		{
			pdfAngle = tpdf;
			pdfBackAngle = tbpdf;
			brdf = tbrdf;
			CNT_MULTILAYERS_STOP;
			return true;
		}

		float w = tmlay->getWeight(hd.tU, hd.tV);
		sumw += w;
		if (tpdf<=0  &&  correctPdf)
			continue;

		#ifdef BRDF_ALL_LAYERS_CORRECT
			if (correctPdf)
				brdf += tbrdf * w * (1.0f/tpdf) ;
			else
				brdf += tbrdf * w;
		#else
			brdf += tbrdf * w;
		#endif
		pdf += tpdf * w;
		backpdf += tbpdf * w;
	}
	pdfAngle = pdf / sumw;
	pdfBackAngle = backpdf / sumw;
	sssIn = sssOut = false;
	#ifdef BRDF_ALL_LAYERS_CORRECT
		if (correctPdf)
			brdf *= pdfAngle;
	#endif
	CNT_MULTILAYERS_STOP;
	return true;
}

//-----------------------------------------------------------------------------------------------------
