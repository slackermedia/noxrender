#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>
#define clamp(x,a,b)   max((a),min((x),(b)))

//----------		LAMBERT TRANSMISSION BRDF

Color4 MatLayer::getBRDFLambertTransmission(HitData & hd)
{
	float cos_in =  hd.in*hd.normal_shade;
	float cos_out = hd.out*hd.normal_shade;
	if ((cos_in)*(cos_out) < 0)
		return getTransmissionColor(hd.tU, hd.tV) * PER_PI;
	else
	{
		if (cos_in>0)
			return getColor0(hd.tU, hd.tV) * PER_PI;//  * 0.5f;
		else
			return getColor90(hd.tU, hd.tV) * PER_PI;//  * 0.5f;
	}
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityLambertTransmission(HitData & hd)
{
	float cosTheta = hd.out*hd.normal_shade;
	return fabs(cosTheta*PER_PI *0.5f);
}

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionLambertTransmission(HitData &hd, float &probability)
{
	#ifdef RAND_PER_THREAD
		RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
		float a = rg->getRandomFloat();
		float b = rg->getRandomFloat();
		bool useTransl = (rg->getRandomInt(10000)%2==1);
	#else
		bool useTransl = (rand()%2==1);
		float a = rand()/(float)RAND_MAX;
		float b = rand()/(float)RAND_MAX;
	#endif

	float sx = a*2-1;
	float sy = b*2-1;
	float r;

	if (sx==0.0f && sy==0.0f)
	{
		probability = PER_PI/2;
		Vector3d res = hd.normal_shade;
		if (hd.in*hd.normal_shade < 0)
			res.invert();
		if (useTransl)
			res.invert();
		return res;
	}

	if (sx >= -sy) 
	{
		if (sx > sy) 
			r = sx;
		else 
			r = sy;
	}
	else 
	{
		if (sx <= sy) 
			r = -sx;
		else 
			r = -sy;
	}

	Vector3d u,v,res;
	u = Vector3d::random();
	while (fabs(u*hd.normal_shade) > 0.95)
		u = Vector3d::random();
	v = hd.normal_shade^u;
	v.normalize();

	float costheta = sqrt(1.0f-r*r);
	probability = max(0.001f, costheta*PER_PI/2);

	res = hd.normal_shade*costheta + v*r;
	if (hd.in*hd.normal_shade < 0)
		res.invert();
	if (useTransl)
		res.invert();

	hd.out = res;
	return res;
}

//-------------------------------------------------------------------------------------------

bool MatLayer::getBRDFandPDFsLambertTransmission(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b)
{
	float cosThetaOut = hd.out*hd.normal_shade;
	float cosThetaIn= hd.in*hd.normal_shade;

	pdf_f = fabs(cosThetaOut*PER_PI *0.5f);
	pdf_b = fabs(cosThetaIn*PER_PI *0.5f);

	if ((cosThetaIn)*(cosThetaOut) < 0)
		brdf = getTransmissionColor(hd.tU, hd.tV) * PER_PI;
	else
	{
		if (cosThetaIn>0)
			brdf =  getColor0(hd.tU, hd.tV) * PER_PI;
		else
			brdf = getColor90(hd.tU, hd.tV) * PER_PI;
	}

	#ifdef VERIFY_MATLAYER_BRDF_PDFS
		Color4 vbrdf = getBRDFLambertTransmission(hd);
		float vpdf_f = getProbabilityLambertTransmission(hd);
		HitData hdback = hd;
		hdback.swapDirections();
		float vpdf_b = getProbabilityLambertTransmission(hdback);
		if (vbrdf.r!=brdf.r  ||  vbrdf.g!=brdf.g  ||  vbrdf.b!=brdf.b)
			Logger::add("brdf verify failed on lambert");
		if (vpdf_f!=pdf_f)
			Logger::add("pdf forward verify failed on lambert");
		if (vpdf_b!=pdf_b)
			Logger::add("pdf backward verify failed on lambert");
	#endif

	return true;
}

//-------------------------------------------------------------------------------------------
