#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "log.h"
#include "valuesMinMax.h"
#include "raytracer.h"
#include "PathTracing.h"
#include "dialogs.h"

void setPositionsOnStartRenderTab(HWND hWnd);
void updatePositionsVisibilityRenderTab(HWND hWnd, int fill, int engine);
bool formatStringFromUnsignedInteger(char * buf, int bufSize, int numDigits, unsigned int number);

extern bool bdpt_connect_to_cam;
extern bool bdpt_can_hit_emitter;
extern int bdpt_max_refl;

extern HMODULE hDllModule;

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcRender(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	static HBRUSH bgBrush = 0;
	static float ratio = 16.0f/9.0f;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				rMain->hTabRender = hWnd;
				setPositionsOnStartRenderTab(hWnd);


				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 

				// BASIC SETTINGS
				EM2GroupBar * emgRes = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_REND_GR_RESOLUTION));
				emgRes->bgImage = rMain->hBmpBg;
				emgRes->setFont(rMain->fonts->em2groupbar, false);

				EM2EditSpin * emesWidth = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_WIDTH));
				emesWidth->bgImage = rMain->hBmpBg;
				emesWidth->setFont(rMain->fonts->em2editspin, false);
				emesWidth->setValuesToInt(NOX_IMAGE_WIDTH_DEF, NOX_IMAGE_WIDTH_MIN, NOX_IMAGE_WIDTH_MAX, NOX_IMAGE_WIDTH_DEF, 1, 0.25f);

				EM2EditSpin * emesHeight = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_HEIGHT));
				emesHeight->bgImage = rMain->hBmpBg;
				emesHeight->setFont(rMain->fonts->em2editspin, false);
				emesHeight->setValuesToInt(NOX_IMAGE_HEIGHT_DEF, NOX_IMAGE_HEIGHT_MIN, NOX_IMAGE_HEIGHT_MAX, NOX_IMAGE_HEIGHT_DEF, 1, 0.25f);

				EM2CheckBox * emcLink = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_LINK_RES));
				emcLink->bgImage = rMain->hBmpBg;
				emcLink->setFont(rMain->fonts->em2checkbox, false);
				emcLink->selected = true;
				ratio = (float)NOX_IMAGE_WIDTH_DEF / (float)NOX_IMAGE_HEIGHT_DEF;

				EM2EditSpin * emesAA = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_AA));
				emesAA->bgImage = rMain->hBmpBg;
				emesAA->setFont(rMain->fonts->em2editspin, false);
				emesAA->setValuesToInt(NOX_IMAGE_AA_DEF, NOX_IMAGE_AA_MIN, NOX_IMAGE_AA_MAX, NOX_IMAGE_AA_DEF, 1, 0.25f);

				EM2Text * emtWidth = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_WIDTH));
				emtWidth->bgImage = rMain->hBmpBg;
				emtWidth->setFont(rMain->fonts->em2text, false);
				EM2Text * emtHeight= GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_HEIGHT));
				emtHeight->bgImage = rMain->hBmpBg;
				emtHeight->setFont(rMain->fonts->em2text, false);
				EM2Text * emtAA = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_ANTIALIASING));
				emtAA->bgImage = rMain->hBmpBg;
				emtAA->setFont(rMain->fonts->em2text, false);
				emtAA->align = EM2Text::ALIGN_RIGHT;

				// FILL
				EM2Text * emtBWidth = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_WIDTH));
				emtBWidth->bgImage = rMain->hBmpBg;
				emtBWidth->setFont(rMain->fonts->em2text, false);
				emtBWidth->align = EM2Text::ALIGN_RIGHT;
				EM2Text * emtBHeight= GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_HEIGHT));
				emtBHeight->bgImage = rMain->hBmpBg;
				emtBHeight->setFont(rMain->fonts->em2text, false);
				emtBHeight->align = EM2Text::ALIGN_RIGHT;
				EM2Text * emtBSamples = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_SAMPLES));
				emtBSamples->bgImage = rMain->hBmpBg;
				emtBSamples->setFont(rMain->fonts->em2text, false);
				EM2Text * emtFill = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_FILL));
				emtFill->bgImage = rMain->hBmpBg;
				emtFill->setFont(rMain->fonts->em2text, false);

				EM2EditSpin * emesBWidth = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_WIDTH));
				emesBWidth->bgImage = rMain->hBmpBg;
				emesBWidth->setFont(rMain->fonts->em2editspin, false);
				emesBWidth->setValuesToInt(20, 10, 256, 20, 1, 0.25f);
				EM2EditSpin * emesBHeight = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_HEIGHT));
				emesBHeight->bgImage = rMain->hBmpBg;
				emesBHeight->setFont(rMain->fonts->em2editspin, false);
				emesBHeight->setValuesToInt(20, 10, 256, 20, 1, 0.25f);
				EM2EditSpin * emesBSamples = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_SAMPLES));
				emesBSamples->bgImage = rMain->hBmpBg;
				emesBSamples->setFont(rMain->fonts->em2editspin, false);
				emesBSamples->setValuesToInt(30, 20, 5000, 30, 1, 0.25f);


				EM2ComboBox * emcFill = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_FILL));
				emcFill->bgImage = rMain->hBmpBg;
				emcFill->setFontMain(rMain->fonts->em2combomain, false);
				emcFill->setFontUnwrapped(rMain->fonts->em2combounwrap, false);
				emcFill->align = EM2ComboBox::ALIGN_LEFT;
				RECT temprect;
				GetClientRect(emcFill->hwnd, &temprect);
				emcFill->rowWidth = temprect.right - 2;
				emcFill->maxShownRows = 7;
				emcFill->setNumItems(7);
				emcFill->setItem(0, "SCAN LINE", true);
				emcFill->setItem(1, "HALTON", true);
				emcFill->setItem(2, "MARSAGLIA", true);
				emcFill->setItem(3, "RAND", true);
				emcFill->setItem(4, "RAND_S", true);
				emcFill->setItem(5, "LATTICE", true);
				emcFill->setItem(6, "BUCKETS", true);


				// TIMERS
				EM2GroupBar * emgTimers = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_REND_GR_TIMERS));
				emgTimers->bgImage = rMain->hBmpBg;
				emgTimers->setFont(rMain->fonts->em2groupbar, false);
				EM2CheckBox * emcStopAfter = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_AFTER));
				emcStopAfter->bgImage = rMain->hBmpBg;
				emcStopAfter->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emtHours1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_HOURS1));
				emtHours1->bgImage = rMain->hBmpBg;
				emtHours1->setFont(rMain->fonts->em2text, false);
				EM2Text * emtMinutes1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_MINUTES1));
				emtMinutes1->bgImage = rMain->hBmpBg;
				emtMinutes1->setFont(rMain->fonts->em2text, false);
				EM2Text * emtSeconds1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_SECONDS1));
				emtSeconds1->bgImage = rMain->hBmpBg;
				emtSeconds1->setFont(rMain->fonts->em2text, false);
				emtSeconds1->align = EM2Text::ALIGN_RIGHT;

				EM2EditSpin * emesStopHours = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_HOURS));
				emesStopHours->bgImage = rMain->hBmpBg;
				emesStopHours->setFont(rMain->fonts->em2editspin, false);
				emesStopHours->setValuesToInt(0, 0, 50, 0, 1, 0.1f);
				EM2EditSpin * emesStopMinutes = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_MINUTES));
				emesStopMinutes->bgImage = rMain->hBmpBg;
				emesStopMinutes->setFont(rMain->fonts->em2editspin, false);
				emesStopMinutes->setValuesToInt(0, 0, 59, 0, 1, 0.1f);
				EM2EditSpin * emesStopSeconds = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_SECONDS));
				emesStopSeconds->bgImage = rMain->hBmpBg;
				emesStopSeconds->setFont(rMain->fonts->em2editspin, false);
				emesStopSeconds->setValuesToInt(0, 0, 59, 0, 1, 0.1f);

				EM2CheckBox * emtStupidAfterStop = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_STUPID_AFTER_STOP));
				emtStupidAfterStop->bgImage = rMain->hBmpBg;
				emtStupidAfterStop->setFont(rMain->fonts->em2checkbox, false);
				EM2Button * embASSettings = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_REND_AFTER_STOP_SETTINGS));
				embASSettings->bgImage = rMain->hBmpBg;
				embASSettings->setFont(rMain->fonts->em2checkbox, false);

				EM2CheckBox * emcAutosave = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE));
				emcAutosave->bgImage = rMain->hBmpBg;
				emcAutosave->setFont(rMain->fonts->em2checkbox, false);
				EM2EditSpin * emesAutosaveMin = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE_MINUTES));
				emesAutosaveMin->bgImage = rMain->hBmpBg;
				emesAutosaveMin->setFont(rMain->fonts->em2editspin, false);
				emesAutosaveMin->setValuesToInt(60, 5, 90, 60, 1, 0.1f);
				EM2Text * emtMinutes2 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_MINUTES2));
				emtMinutes2->bgImage = rMain->hBmpBg;
				emtMinutes2->setFont(rMain->fonts->em2text, false);

				// ENGINE
				EM2GroupBar * emgEngine = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_REND_GR_ENGINE));
				emgEngine->bgImage = rMain->hBmpBg;
				emgEngine->setFont(rMain->fonts->em2groupbar, false);

				EM2Text * emtEngine = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_ENGINE));
				emtEngine->bgImage = rMain->hBmpBg;
				emtEngine->setFont(rMain->fonts->em2text, false);

				EM2ComboBox * emcEngine = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_ENGINE));
				emcEngine->bgImage = rMain->hBmpBg;
				emcEngine->setFontMain(rMain->fonts->em2combomain, false);
				emcEngine->setFontUnwrapped(rMain->fonts->em2combounwrap, false);
				emcEngine->align = EM2ComboBox::ALIGN_LEFT;
				emcEngine->rowWidth = 130;
				emcEngine->maxShownRows = 3;
				emcEngine->selected = 0;
				emcEngine->setNumItems(3);
				emcEngine->setItem(0, "PATH TRACING", true);
				emcEngine->setItem(1, "BIDIR. PATH TRACING", true);
				emcEngine->setItem(2, "INTERSECTIONS", true);

				// INTERSECTIONS
				EM2Text * emtShade = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_SHADE));
				emtShade->bgImage = rMain->hBmpBg;
				emtShade->setFont(rMain->fonts->em2text, false);
				emtShade->align = EM2Text::ALIGN_RIGHT;

				EM2ComboBox * emcShade = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_INT_SHADE));
				emcShade->bgImage = rMain->hBmpBg;
				emcShade->setFontMain(rMain->fonts->em2combomain, false);
				emcShade->setFontUnwrapped(rMain->fonts->em2combounwrap, false);
				emcShade->rowWidth = 150;
				emcShade->maxShownRows = 4;
				emcShade->selected = 0;
				emcShade->setNumItems(4);
				emcShade->setItem(0, "SHADE SMOOTH", true);
				emcShade->setItem(1, "SHADE GEOM", true);
				emcShade->setItem(2, "NORMALS SHADE", true);
				emcShade->setItem(3, "NORMALS GEOM", true);

				// BDPT
				EM2Text * emtMaxRefl = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_MAX_REFL));
				emtMaxRefl->bgImage = rMain->hBmpBg;
				emtMaxRefl->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emesMaxRefl = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_MAX_REFL));
				emesMaxRefl->bgImage = rMain->hBmpBg;
				emesMaxRefl->setFont(rMain->fonts->em2editspin, false);
				emesMaxRefl->setValuesToInt(29, 0, 29, 29, 1, 0.1f);
				EM2CheckBox * emcEmHit = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_EMITTER_HIT));
				emcEmHit->bgImage = rMain->hBmpBg;
				emcEmHit->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcCamConn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_CAMERA_CONNECT));
				emcCamConn->bgImage = rMain->hBmpBg;
				emcCamConn->setFont(rMain->fonts->em2checkbox, false);

				// PT
				EM2Text * emtGi = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_GI_SAMPLES));
				emtGi->bgImage = rMain->hBmpBg;
				emtGi->setFont(rMain->fonts->em2text, false);
				emtGi->align = EM2Text::ALIGN_RIGHT;
				EM2Text * emtRIS = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_RESAMPLE_IS));
				emtRIS->bgImage = rMain->hBmpBg;
				emtRIS->setFont(rMain->fonts->em2text, false);
				emtRIS->align = EM2Text::ALIGN_RIGHT;
				EM2Text * emtCaustRough = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_CAUSTICS_ROUGH));
				emtCaustRough->bgImage = rMain->hBmpBg;
				emtCaustRough->setFont(rMain->fonts->em2text, false);
				emtCaustRough->align = EM2Text::ALIGN_RIGHT;
				EM2CheckBox * emcFakeCaust = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_FAKE_CAUSTICS));
				emcFakeCaust->bgImage = rMain->hBmpBg;
				emcFakeCaust->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcNo2nd= GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC));
				emcNo2nd->bgImage = rMain->hBmpBg;
				emcNo2nd->setFont(rMain->fonts->em2checkbox, false);
				EM2EditSpin * emesGI = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_GI_SAMPLES));
				emesGI->bgImage = rMain->hBmpBg;
				emesGI->setFont(rMain->fonts->em2editspin, false);
				emesGI->setValuesToInt(1, 1, 100, 1, 1, 0.1f);
				EM2EditSpin * emesRIS = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_RIS));
				emesRIS->bgImage = rMain->hBmpBg;
				emesRIS->setFont(rMain->fonts->em2editspin, false);
				emesRIS->setValuesToInt(1, 1, 16, 1, 1, 0.1f);
				EM2EditSpin * emesCaustRough = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_CAUSTIC_ROUGH));
				emesCaustRough->bgImage = rMain->hBmpBg;
				emesCaustRough->setFont(rMain->fonts->em2editspin, false);
				emesCaustRough->setValuesToFloat(0.0f, 0.0f, 10.0f, 0.0f, 0.5f, 0.1f);

				// MISC
				EM2GroupBar * emgMisc = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_REND_GR_MISC));
				emgMisc->bgImage = rMain->hBmpBg;
				emgMisc->setFont(rMain->fonts->em2groupbar, false);
				EM2CheckBox * emcTimeStamp = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_TIMESTAMP));
				emcTimeStamp->bgImage = rMain->hBmpBg;
				emcTimeStamp->setFont(rMain->fonts->em2checkbox, false);
				emcTimeStamp->selected = false;
				EM2CheckBox * emcNoxStamp = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_NOX_STAMP));
				emcNoxStamp->bgImage = rMain->hBmpBg;
				emcNoxStamp->setFont(rMain->fonts->em2checkbox, false);
				emcNoxStamp->selected = false;

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				int rl = rect.left;
				int rt = rect.top;
				HPEN hOldPen = (HPEN)SelectObject(bhdc, CreatePen(PS_SOLID, 1, RGB(62,62,62)));
				MoveToEx(bhdc, 124-rl, 77 -rt, NULL);
				LineTo(  bhdc, 133-rl, 77 -rt);
				LineTo(  bhdc, 133-rl, 81 -rt);
				MoveToEx(bhdc, 124-rl, 106-rt, NULL);
				LineTo(  bhdc, 133-rl, 106-rt);
				LineTo(  bhdc, 133-rl, 102-rt);

				DeleteObject(SelectObject(bhdc, hOldPen));

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_TIMER:
			{
				if (wParam == TIMER_AUTOSAVE_ID)
				{
					if (rMain->timers.autosaveOn)
					{
						Raytracer * rtr = Raytracer::getInstance();
						if (!rtr  ||  !rtr->curScenePtr)
							break;
						char * fname = rtr->curScenePtr->sceneFilename;
						if (fname  &&  rtr->curScenePtr->nowRendering)
						{
							rMain->saveScene(fname, false, true);
						}
					}
				}
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_REND_WIDTH:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesWidth		= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_WIDTH));
							EM2EditSpin * emesHeight	= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_HEIGHT));
							EM2CheckBox * emcLink		= GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_LINK_RES));
							if (emcLink->selected)
								emesHeight->setIntValue((int)(emesWidth->intValue/ratio));
							// todo: update cam angles
						}
						break;
					case IDC_REND2_REND_HEIGHT:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesWidth		= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_WIDTH));
							EM2EditSpin * emesHeight	= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_HEIGHT));
							EM2CheckBox * emcLink		= GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_LINK_RES));
							if (emcLink->selected)
								emesWidth->setIntValue((int)(emesHeight->intValue*ratio));
						}
						break;
					case IDC_REND2_REND_LINK_RES:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2EditSpin * emesWidth		= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_WIDTH));
							EM2EditSpin * emesHeight	= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_HEIGHT));
							EM2CheckBox * emcLink		= GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_LINK_RES));
							if (emcLink->selected)
								ratio = (float)emesWidth->intValue/(float)emesHeight->intValue;
						}
						break;
					case IDC_REND2_REND_AA:
						{
							Camera * cam = sc->getActiveCamera();
							CHECK(cam);
							EM2EditSpin * emesAA = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_AA));
							cam->aa = emesAA->intValue;
						}
						break;
					case IDC_REND2_REND_FILL:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EM2ComboBox * emcFill = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_FILL));
							EM2ComboBox * emcEngine = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_ENGINE));
							updatePositionsVisibilityRenderTab(hWnd, emcFill->selected, emcEngine->selected);
							Raytracer::getInstance()->curScenePtr->sscene.random_type = emcFill->selected;
						}
						break;
					case IDC_REND2_REND_BUCKET_WIDTH:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesBWidth = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_WIDTH));
							sc->sscene.bucket_size_x = emesBWidth->intValue;
						}
						break;
					case IDC_REND2_REND_BUCKET_HEIGHT:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesBHeight = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_HEIGHT));
							sc->sscene.bucket_size_y = emesBHeight->intValue;
						}
						break;
					case IDC_REND2_REND_BUCKET_SAMPLES:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesBSamples = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_SAMPLES));
							sc->sscene.samples_per_pixel_in_pass = emesBSamples->intValue;
						}
						break;
					case IDC_REND2_REND_STOP_AFTER:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcStopAfter = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_AFTER));
							rMain->timers.stopOn = emcStopAfter->selected;
						}
						break;
					case IDC_REND2_REND_AUTOSAVE:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcAutosave = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE));
							rMain->timers.autosaveOn = emcAutosave->selected;
						}
						break;
					case IDC_REND2_REND_TEXT_STUPID_AFTER_STOP:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcRunAfter = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_STUPID_AFTER_STOP));
							rMain->runNext.active = emcRunAfter->selected;
						}
						break;
					case IDC_REND2_REND_AFTER_STOP_SETTINGS:
						{
							CHECK(wmEvent==BN_CLICKED);

							RunNextSettings * rnc = rMain->runNext.getCopy();
							RunNextSettings * res = (RunNextSettings *)DialogBoxParam(hDllModule,
								MAKEINTRESOURCE(IDD_AFTER_STOP_DIALOG2), hWnd, AfterStop2DlgProc,(LPARAM)(rnc));
							if (res)
							{
								rMain->runNext.copyFrom(res);
							}
						}
						break;
					case IDC_REND2_REND_STOP_HOURS:
					case IDC_REND2_REND_STOP_MINUTES:
					case IDC_REND2_REND_STOP_SECONDS:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesStopHours		= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_HOURS));
							EM2EditSpin * emesStopMinutes	= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_MINUTES));
							EM2EditSpin * emesStopSeconds	= GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_SECONDS));
							int t = (emesStopHours->intValue * 60 + emesStopMinutes->intValue) * 60 + emesStopSeconds->intValue;
							rMain->timers.stopSec = t;
							rMain->timers.stopOn = true;
							EM2CheckBox * emcStopAfter = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_STOP_AFTER));
							emcStopAfter->selected = true;
							InvalidateRect(emcStopAfter->hwnd, NULL, false);
						}
						break;
					case IDC_REND2_REND_AUTOSAVE_MINUTES:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesAutosaveMin = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE_MINUTES));
							rMain->timers.stopSec = emesAutosaveMin->intValue;
						}
						break;
					case IDC_REND2_REND_ENGINE:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EM2ComboBox * emcFill = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_FILL));
							EM2ComboBox * emcEngine = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_ENGINE));

							Raytracer * rtr = Raytracer::getInstance();
							Scene * sc = rtr->curScenePtr;
							switch (emcEngine->selected)
							{
								case 0:		sc->sscene.giMethod = Raytracer::GI_METHOD_PATH_TRACING;					break;
								case 1:		sc->sscene.giMethod = Raytracer::GI_METHOD_BIDIRECTIONAL_PATH_TRACING;		break;
								//case 2:		sc->sscene.giMethod = Raytracer::GI_METHOD_METROPOLIS;						break;
								case 2:		sc->sscene.giMethod = Raytracer::GI_METHOD_INTERSECTIONS;					break;
							}

							updatePositionsVisibilityRenderTab(hWnd, emcFill->selected, emcEngine->selected);
						}
						break;
					case IDC_REND2_REND_INT_SHADE:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EM2ComboBox * emcShade = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_INT_SHADE));
							ShadeTracer::shade_type = emcShade->selected;
						}
						break;
					case IDC_REND2_REND_BDPT_MAX_REFL:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesMaxRefl = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_MAX_REFL));
							bdpt_max_refl = emesMaxRefl->intValue;
						}
						break;
					case IDC_REND2_REND_BDPT_EMITTER_HIT:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcEmHit = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_EMITTER_HIT));
							bdpt_can_hit_emitter = emcEmHit->selected;
						}
						break;
					case IDC_REND2_REND_BDPT_CAMERA_CONNECT:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcCamConn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_CAMERA_CONNECT));
							bdpt_connect_to_cam = emcCamConn->selected;
						}
						break;

					case IDC_REND2_REND_PT_FAKE_CAUSTICS:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcFakeCaust = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_FAKE_CAUSTICS));
							sc->sscene.causticFake = emcFakeCaust->selected;
						}
						break;
					case IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcNo2nd = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC));
							sc->sscene.disableSecondaryCaustic = emcNo2nd->selected;
						}
						break;
					case IDC_REND2_REND_PT_GI_SAMPLES:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesGI = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_GI_SAMPLES));
							sc->sscene.pt_gi_samples = emesGI->intValue;
						}
						break;
					case IDC_REND2_REND_PT_RIS:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesRIS = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_RIS));
							sc->sscene.pt_ris_samples = emesRIS->intValue;
						}
						break;
					case IDC_REND2_REND_PT_CAUSTIC_ROUGH:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesCaustRough = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_REND_PT_CAUSTIC_ROUGH));
							sc->sscene.causticMaxRough = emesCaustRough->floatValue;
						}
						break;
					case IDC_REND2_REND_TIMESTAMP:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcTimeStamp = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_TIMESTAMP));
							sc->sscene.drawStampTime = emcTimeStamp->selected;
							rMain->refreshRenderOnThread();
						}
						break;
					case IDC_REND2_REND_NOX_STAMP:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcNoxStamp = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_REND_NOX_STAMP));
							sc->sscene.drawStampLogo = emcNoxStamp->selected;
							rMain->refreshRenderOnThread();
						}
						break;

				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelRender()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TITLE));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	// RESOLUTION / BASIC SETTINGS
	EM2GroupBar * emgRes = GetEM2GroupBarInstance(GetDlgItem(hTabRender, IDC_REND2_REND_GR_RESOLUTION));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_GR_RESOLUTION), bgShiftXRightPanel, 0, emgRes->bgShiftX, emgRes->bgShiftY);
	EM2EditSpin * emesWidth = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_WIDTH));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_WIDTH), bgShiftXRightPanel, 0, emesWidth->bgShiftX, emesWidth->bgShiftY);
	EM2EditSpin * emesHeight = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_HEIGHT));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_HEIGHT), bgShiftXRightPanel, 0, emesHeight->bgShiftX, emesHeight->bgShiftY);
	EM2EditSpin * emesAA = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AA));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_AA), bgShiftXRightPanel, 0, emesAA->bgShiftX, emesAA->bgShiftY);
	EM2CheckBox * emtLink = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_LINK_RES));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_LINK_RES), bgShiftXRightPanel, 0, emtLink->bgShiftX, emtLink->bgShiftY);
	EM2Text * emtWidth = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_WIDTH));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_WIDTH), bgShiftXRightPanel, 0, emtWidth->bgShiftX, emtWidth->bgShiftY);
	EM2Text * emtHeight= GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_HEIGHT));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_HEIGHT), bgShiftXRightPanel, 0, emtHeight->bgShiftX, emtHeight->bgShiftY);
	EM2Text * emtAA = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_ANTIALIASING));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_ANTIALIASING), bgShiftXRightPanel, 0, emtAA->bgShiftX, emtAA->bgShiftY);
	
	// FILL
	EM2Text * emtFill = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_FILL));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_FILL), bgShiftXRightPanel, 0, emtFill->bgShiftX, emtFill->bgShiftY);
	EM2Text * emtSamples = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_BUCKET_SAMPLES));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_BUCKET_SAMPLES), bgShiftXRightPanel, 0, emtSamples->bgShiftX, emtSamples->bgShiftY);
	EM2Text * emtBWidth = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_BUCKET_WIDTH));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_BUCKET_WIDTH), bgShiftXRightPanel, 0, emtBWidth->bgShiftX, emtBWidth->bgShiftY);
	EM2Text * emtBHeight = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_BUCKET_HEIGHT));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_BUCKET_HEIGHT), bgShiftXRightPanel, 0, emtBHeight->bgShiftX, emtBHeight->bgShiftY);
	EM2ComboBox * emcFill = GetEM2ComboBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_FILL));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_FILL), bgShiftXRightPanel, 0, emcFill->bgShiftX, emcFill->bgShiftY);
	EM2EditSpin * emesSamples = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_SAMPLES));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_SAMPLES), bgShiftXRightPanel, 0, emesSamples->bgShiftX, emesSamples->bgShiftY);
	EM2EditSpin * emesBWidth = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_WIDTH));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_WIDTH), bgShiftXRightPanel, 0, emesBWidth->bgShiftX, emesBWidth->bgShiftY);
	EM2EditSpin * emesBHeight = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_HEIGHT));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_HEIGHT), bgShiftXRightPanel, 0, emesBHeight->bgShiftX, emesBHeight->bgShiftY);
		
	// TIMERS
	EM2GroupBar * emgTimers = GetEM2GroupBarInstance(GetDlgItem(hTabRender, IDC_REND2_REND_GR_TIMERS));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_GR_TIMERS), bgShiftXRightPanel, 0, emgTimers->bgShiftX, emgTimers->bgShiftY);
	EM2CheckBox * emcStopAfter = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_AFTER));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_AFTER), bgShiftXRightPanel, 0, emcStopAfter->bgShiftX, emcStopAfter->bgShiftY);
	EM2EditSpin * emesStopH = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_HOURS));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_HOURS), bgShiftXRightPanel, 0, emesStopH->bgShiftX, emesStopH->bgShiftY);
	EM2EditSpin * emesStopM = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_MINUTES));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_MINUTES), bgShiftXRightPanel, 0, emesStopM->bgShiftX, emesStopM->bgShiftY);
	EM2EditSpin * emesStopS = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_SECONDS));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_SECONDS), bgShiftXRightPanel, 0, emesStopS->bgShiftX, emesStopS->bgShiftY);
	EM2EditSpin * emesASmin = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AUTOSAVE_MINUTES));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_AUTOSAVE_MINUTES), bgShiftXRightPanel, 0, emesASmin->bgShiftX, emesASmin->bgShiftY);
	EM2Text * emtHours = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_HOURS1));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_HOURS1), bgShiftXRightPanel, 0, emtHours->bgShiftX, emtHours->bgShiftY);
	EM2Text * emtMinutes = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_MINUTES1));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_MINUTES1), bgShiftXRightPanel, 0, emtMinutes->bgShiftX, emtMinutes->bgShiftY);
	EM2Text * emtSeconds = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_SECONDS1));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_SECONDS1), bgShiftXRightPanel, 0, emtSeconds->bgShiftX, emtSeconds->bgShiftY);
	EM2Text * emtMinutes2 = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_MINUTES2));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_MINUTES2), bgShiftXRightPanel, 0, emtMinutes2->bgShiftX, emtMinutes2->bgShiftY);
	EM2Text * emtAfterBlabla = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_STUPID_AFTER_STOP));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_STUPID_AFTER_STOP), bgShiftXRightPanel, 0, emtAfterBlabla->bgShiftX, emtAfterBlabla->bgShiftY);
	EM2CheckBox * emcAutosave = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AUTOSAVE));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_AUTOSAVE), bgShiftXRightPanel, 0, emcAutosave->bgShiftX, emcAutosave->bgShiftY);
	EM2Button * embASSettings = GetEM2ButtonInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AFTER_STOP_SETTINGS));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_AFTER_STOP_SETTINGS), bgShiftXRightPanel, 0, embASSettings->bgShiftX, embASSettings->bgShiftY);

	// ENGINE
	EM2GroupBar * emgEngine = GetEM2GroupBarInstance(GetDlgItem(hTabRender, IDC_REND2_REND_GR_ENGINE));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_GR_ENGINE), bgShiftXRightPanel, 0, emgEngine->bgShiftX, emgEngine->bgShiftY);
	EM2Text * emtEngine = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_ENGINE));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_ENGINE), bgShiftXRightPanel, 0, emtEngine->bgShiftX, emtEngine->bgShiftY);
	EM2ComboBox * emcEngine = GetEM2ComboBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_ENGINE));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_ENGINE), bgShiftXRightPanel, 0, emcEngine->bgShiftX, emcEngine->bgShiftY);

	// SHADE
	EM2Text * emtShade = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_SHADE));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_SHADE), bgShiftXRightPanel, 0, emtShade->bgShiftX, emtShade->bgShiftY);
	EM2ComboBox * emcShade = GetEM2ComboBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_INT_SHADE));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_INT_SHADE), bgShiftXRightPanel, 0, emcShade->bgShiftX, emcShade->bgShiftY);

	// BDPT
	EM2Text * emtMaxRefl = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_MAX_REFL));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_MAX_REFL), bgShiftXRightPanel, 0, emtMaxRefl->bgShiftX, emtMaxRefl->bgShiftY);
	EM2CheckBox * emcCamCon = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_CAMERA_CONNECT));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_CAMERA_CONNECT), bgShiftXRightPanel, 0, emcCamCon->bgShiftX, emcCamCon->bgShiftY);
	EM2CheckBox * emcEmHit = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_EMITTER_HIT));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_EMITTER_HIT), bgShiftXRightPanel, 0, emcEmHit->bgShiftX, emcEmHit->bgShiftY);
	EM2EditSpin * emesMaxRefl = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_MAX_REFL));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_MAX_REFL), bgShiftXRightPanel, 0, emesMaxRefl->bgShiftX, emesMaxRefl->bgShiftY);

	// PT
	EM2CheckBox * emcFakeCaust = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_FAKE_CAUSTICS));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_PT_FAKE_CAUSTICS), bgShiftXRightPanel, 0, emcFakeCaust->bgShiftX, emcFakeCaust->bgShiftY);
	EM2CheckBox * emcNo2nd = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC), bgShiftXRightPanel, 0, emcNo2nd->bgShiftX, emcNo2nd->bgShiftY);
	EM2Text * emtGI = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_GI_SAMPLES));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_GI_SAMPLES), bgShiftXRightPanel, 0, emtGI->bgShiftX, emtGI->bgShiftY);
	EM2Text * emtRIS = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_RESAMPLE_IS));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_RESAMPLE_IS), bgShiftXRightPanel, 0, emtRIS->bgShiftX, emtRIS->bgShiftY);
	EM2Text * emtCaustRough = GetEM2TextInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_CAUSTICS_ROUGH));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_CAUSTICS_ROUGH), bgShiftXRightPanel, 0, emtCaustRough->bgShiftX, emtCaustRough->bgShiftY);
	EM2EditSpin * emesGI = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_GI_SAMPLES));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_PT_GI_SAMPLES), bgShiftXRightPanel, 0, emesGI->bgShiftX, emesGI->bgShiftY);
	EM2EditSpin * emesRIS = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_RIS));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_PT_RIS), bgShiftXRightPanel, 0, emesRIS->bgShiftX, emesRIS->bgShiftY);
	EM2EditSpin * emesCaustRough = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_CAUSTIC_ROUGH));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_PT_CAUSTIC_ROUGH), bgShiftXRightPanel, 0, emesCaustRough->bgShiftX, emesCaustRough->bgShiftY);

	// MISC
	EM2GroupBar * emgMisc = GetEM2GroupBarInstance(GetDlgItem(hTabRender, IDC_REND2_REND_GR_MISC));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_GR_MISC), bgShiftXRightPanel, 0, emgMisc->bgShiftX, emgMisc->bgShiftY);
	EM2CheckBox * emcTimestamp = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TIMESTAMP));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_TIMESTAMP), bgShiftXRightPanel, 0, emcTimestamp->bgShiftX, emcTimestamp->bgShiftY);
	EM2CheckBox * emcNoxstamp = GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_NOX_STAMP));
	updateControlBgShift(GetDlgItem(hTabRender, IDC_REND2_REND_NOX_STAMP), bgShiftXRightPanel, 0, emcNoxstamp->bgShiftX, emcNoxstamp->bgShiftY);



}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartRenderTab(HWND hWnd)
{
	// txt y-4
	int x,y;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TITLE), HWND_TOP, 159,11, 70, 16, 0);

	x=8; y=39;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_GR_RESOLUTION),		HWND_TOP,		x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_WIDTH),			HWND_TOP,		x,		y+30,		57,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_HEIGHT),			HWND_TOP,		x,		y+60,		57,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_WIDTH),				HWND_TOP,		x+57,	y+28,		55,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_HEIGHT),				HWND_TOP,		x+57,	y+58,		55,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_LINK_RES),				HWND_TOP,		x+120,	y+48,		44,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_ANTIALIASING),	HWND_TOP,		x+228,	y+45,		80,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_AA),					HWND_TOP,		x+317,	y+43,		44,		20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_FILL),			HWND_TOP,		x,		y+90,		25,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_FILL),					HWND_TOP,		x+26,	y+88,		85,		20,			0);


	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_SAMPLES),		HWND_TOP,		x+57,	y+118,		55,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_WIDTH),			HWND_TOP,		x+306,	y+88,		55,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_HEIGHT),		HWND_TOP,		x+306,	y+118,		55,		20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_SAMPLES),	HWND_TOP,		x,		y+120,		57,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_WIDTH),	HWND_TOP,		x+210,	y+90,		89,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_HEIGHT),	HWND_TOP,		x+210,	y+120,		89,		16,			0);


	updatePositionsVisibilityRenderTab(hWnd, 0, 0);
}

//------------------------------------------------------------------------------------------------

void updatePositionsVisibilityRenderTab(HWND hWnd, int fill, int engine)
{
	int addFillY = 0;
	int addEngineY = 0;
	int showFillBuckets = SW_HIDE;
	int showEnginePT = SW_HIDE;
	int showEngineBDPT = SW_HIDE;
	int showEngineINT = SW_HIDE;
	int x,y;

	if (fill==6)
	{
		addFillY = 30;
		showFillBuckets = SW_SHOW;
	}
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_SAMPLES),			showFillBuckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_WIDTH),			showFillBuckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_HEIGHT),			showFillBuckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_SAMPLES),	showFillBuckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_WIDTH),		showFillBuckets);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_BUCKET_HEIGHT),		showFillBuckets);

	x = 8; y = 159+addFillY;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_GR_TIMERS),				HWND_TOP,		x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_STOP_AFTER),				HWND_TOP,		x,		y+33,		85,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_STOP_HOURS),				HWND_TOP,		x+88,	y+28,		44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_HOURS1),				HWND_TOP,		x+137,	y+30,		25,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_STOP_MINUTES),				HWND_TOP,		x+192,	y+28,		44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_MINUTES1),			HWND_TOP,		x+242,	y+30,		25,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_STOP_SECONDS),				HWND_TOP,		x+292,	y+28,		44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_SECONDS1),			HWND_TOP,		x+336,	y+30,		25,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_STUPID_AFTER_STOP ),	HWND_TOP,		x,		y+60,		290,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_AFTER_STOP_SETTINGS),		HWND_TOP,		x+291,	y+57,		69,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE),					HWND_TOP,		x,		y+93,		85,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE_MINUTES),			HWND_TOP,		x+88,	y+88,		44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_MINUTES2),			HWND_TOP,		x+137,	y+90,		25,		16,			0);

	//engine = 1;
	switch (engine)
	{
		case 1: 
			showEngineBDPT = SW_SHOW;	addEngineY = 90;	break;
		case 2: 
			showEngineINT= SW_SHOW;		addEngineY = 60;	break;
		case 0: 
		default:
			showEnginePT= SW_SHOW;		addEngineY = 120;	break;
	}
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_GI_SAMPLES),		showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_RESAMPLE_IS),		showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_CAUSTICS_ROUGH),	showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_PT_GI_SAMPLES),			showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_PT_RIS),					showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_PT_CAUSTIC_ROUGH),		showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_PT_FAKE_CAUSTICS),		showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC),	showEnginePT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_MAX_REFL),			showEngineBDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_MAX_REFL),			showEngineBDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_CAMERA_CONNECT),	showEngineBDPT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_EMITTER_HIT),		showEngineBDPT);
	//ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_CAMERA_CONNECT),	SW_HIDE);
	//ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_EMITTER_HIT),		SW_HIDE);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_SHADE),				showEngineINT);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_REND_INT_SHADE),				showEngineINT);

	x = 8; y = 279 + addFillY;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_GR_ENGINE),				HWND_TOP,		x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_ENGINE),				HWND_TOP,		x,		y+30,		43,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_ENGINE),					HWND_TOP,		x+43,	y+28,		126,	20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_SHADE),				HWND_TOP,		x+205,	y+30,		43,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_INT_SHADE),				HWND_TOP,		x+250,	y+28,		110,	20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_MAX_REFL),			HWND_TOP,		x,		y+60,		103,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_MAX_REFL),			HWND_TOP,		x+103,	y+58,		44,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_EMITTER_HIT),			HWND_TOP,		x+284,	y+33,		75,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_CAMERA_CONNECT),		HWND_TOP,		x+248,	y+63,		111,	10,			0);


	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_FAKE_CAUSTICS),			HWND_TOP,		x,		y+63,		105,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC),	HWND_TOP,		x,		y+93,		145,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_GI_SAMPLES),			HWND_TOP,		x+226,	y+30,		70,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_RESAMPLE_IS),			HWND_TOP,		x+226,	y+60,		70,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TEXT_CAUSTICS_ROUGH),		HWND_TOP,		x+200,	y+90,		96,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_GI_SAMPLES),			HWND_TOP,		x+305,	y+28,		55,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_RIS),					HWND_TOP,		x+305,	y+58,		55,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_CAUSTIC_ROUGH),			HWND_TOP,		x+305,	y+88,		55,		20,			0);
	

	x = 8; y = 279 + addFillY + addEngineY;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_GR_MISC),					HWND_TOP,		x,		y,			360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_TIMESTAMP),				HWND_TOP,		x,		y+33,		90,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_NOX_STAMP),				HWND_TOP,		x+104,	y+33,		90,		10,			0);
	
	// tabs order
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_WIDTH),			HWND_TOP,											0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_HEIGHT),			GetDlgItem(hWnd, IDC_REND2_REND_WIDTH),				0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_AA),				GetDlgItem(hWnd, IDC_REND2_REND_HEIGHT),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_SAMPLES),	GetDlgItem(hWnd, IDC_REND2_REND_AA),				0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_WIDTH),		GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_SAMPLES),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_HEIGHT),	GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_WIDTH),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_STOP_HOURS),		GetDlgItem(hWnd, IDC_REND2_REND_BUCKET_HEIGHT),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_STOP_MINUTES),		GetDlgItem(hWnd, IDC_REND2_REND_STOP_HOURS),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_STOP_SECONDS),		GetDlgItem(hWnd, IDC_REND2_REND_STOP_MINUTES),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE_MINUTES),	GetDlgItem(hWnd, IDC_REND2_REND_STOP_SECONDS),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_GI_SAMPLES),	GetDlgItem(hWnd, IDC_REND2_REND_AUTOSAVE_MINUTES),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_RIS),			GetDlgItem(hWnd, IDC_REND2_REND_PT_GI_SAMPLES),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_PT_CAUSTIC_ROUGH),	GetDlgItem(hWnd, IDC_REND2_REND_PT_RIS),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_REND_BDPT_MAX_REFL),	GetDlgItem(hWnd, IDC_REND2_REND_PT_CAUSTIC_ROUGH),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);

	RendererMain2::getInstance()->updateBgShiftsPanelRender();

	RedrawWindow(hWnd, 0, 0, RDW_INVALIDATE | RDW_ALLCHILDREN );
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillRenderTab(Camera * cam)
{
	EM2EditSpin * emesWidth			= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_WIDTH));
	EM2EditSpin * emesHeight		= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_HEIGHT));
	EM2CheckBox * emcLink			= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_LINK_RES));
	EM2EditSpin * emesAA			= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AA));
	EM2EditSpin * emesBWidth		= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_WIDTH));
	EM2EditSpin * emesBHeight		= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_HEIGHT));
	EM2EditSpin * emesBSamples		= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BUCKET_SAMPLES));
	EM2ComboBox * emcFill			= GetEM2ComboBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_FILL));
	EM2CheckBox * emcActionAfter	= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TEXT_STUPID_AFTER_STOP));
	EM2CheckBox * emcStopAfter		= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_AFTER));
	EM2EditSpin * emesStopHours		= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_HOURS));
	EM2EditSpin * emesStopMinutes	= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_MINUTES));
	EM2EditSpin * emesStopSeconds	= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_STOP_SECONDS));
	EM2CheckBox * emcAutosave		= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AUTOSAVE));
	EM2EditSpin * emesAutosaveMin	= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AUTOSAVE_MINUTES));
	EM2ComboBox * emcEngine			= GetEM2ComboBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_ENGINE));
	EM2ComboBox * emcShade			= GetEM2ComboBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_INT_SHADE));
	EM2EditSpin * emesMaxRefl		= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_MAX_REFL));
	EM2CheckBox * emcEmHit			= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_EMITTER_HIT));
	EM2CheckBox * emcCamConn		= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_BDPT_CAMERA_CONNECT));
	EM2CheckBox * emcFakeCaust		= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_FAKE_CAUSTICS));
	EM2CheckBox * emcNo2nd			= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC));
	EM2EditSpin * emesGI			= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_GI_SAMPLES));
	EM2EditSpin * emesRIS			= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_RIS));
	EM2EditSpin * emesCaustRough	= GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_PT_CAUSTIC_ROUGH));
	EM2CheckBox * emcTimeStamp		= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_TIMESTAMP));
	EM2CheckBox * emcNoxStamp		= GetEM2CheckBoxInstance(GetDlgItem(hTabRender, IDC_REND2_REND_NOX_STAMP));

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (cam)
	{
		emesWidth->setIntValue(cam->width);
		emesHeight->setIntValue(cam->height);
		emesAA->setIntValue(cam->aa);
		// update ratio
		SendMessage(hTabRender, WM_COMMAND, MAKEWPARAM(IDC_REND2_REND_LINK_RES, BN_CLICKED), (LPARAM)emcLink->hwnd);
	}
	emesBWidth->setIntValue(sc->sscene.bucket_size_x);
	emesBHeight->setIntValue(sc->sscene.bucket_size_y);
	emesBSamples->setIntValue(sc->sscene.samples_per_pixel_in_pass);

	switch (sc->sscene.giMethod)
	{
		case Raytracer::GI_METHOD_PATH_TRACING: 				emcEngine->selected = 0;		break;
		case Raytracer::GI_METHOD_BIDIRECTIONAL_PATH_TRACING:	emcEngine->selected = 1;		break;
		case Raytracer::GI_METHOD_INTERSECTIONS: 				emcEngine->selected = 2;		break;
		default:					 							emcEngine->selected = 0;		break;
	}
	switch (sc->sscene.random_type)
	{
		case Camera::RND_TYPE_SCANLINE:		emcFill->selected = 0;		break;
		case Camera::RND_TYPE_HALTON:		emcFill->selected = 1;		break;
		case Camera::RND_TYPE_MARSAGLIA:	emcFill->selected = 2;		break;
		case Camera::RND_TYPE_RAND:			emcFill->selected = 3;		break;
		case Camera::RND_TYPE_RAND_S:		emcFill->selected = 4;		break;
		case Camera::RND_TYPE_LATTICE:		emcFill->selected = 5;		break;
		case Camera::RND_TYPE_BUCKETS: 		emcFill->selected = 6;		break;
		default:					 		emcFill->selected = 0;		break;
	}
	InvalidateRect(emcFill->hwnd, NULL, false);
	InvalidateRect(emcFill->hwnd, NULL, false);

	// TIMERS
	emcStopAfter->selected = timers.stopOn;
	emcAutosave->selected = timers.autosaveOn;
	InvalidateRect(emcStopAfter->hwnd, NULL, false);
	InvalidateRect(emcAutosave->hwnd, NULL, false);
	int stsec = timers.stopSec%60;
	int stmin = (timers.stopSec/60)%60;
	int sthrs = timers.stopSec/3600;
	emesStopHours->setIntValue(sthrs);
	emesStopMinutes->setIntValue(stmin);
	emesStopSeconds->setIntValue(stsec);
	emesAutosaveMin->setIntValue(timers.autosaveMin);
	emcActionAfter->selected = runNext.active;
	InvalidateRect(emcActionAfter->hwnd, NULL, false);


	// PT
	emcFakeCaust->selected = sc->sscene.causticFake;
	emcNo2nd->selected = sc->sscene.disableSecondaryCaustic;
	InvalidateRect(emcFakeCaust->hwnd, NULL, false);
	InvalidateRect(emcNo2nd->hwnd, NULL, false);
	emesGI->setIntValue(sc->sscene.pt_gi_samples);
	emesRIS->setIntValue(sc->sscene.pt_ris_samples);
	emesCaustRough->setFloatValue(sc->sscene.causticMaxRough);

	// BDPT
	emesMaxRefl->setIntValue(bdpt_max_refl);
	emcEmHit->selected = bdpt_can_hit_emitter;
	emcCamConn->selected = bdpt_connect_to_cam;
	InvalidateRect(emcEmHit->hwnd, NULL, false);
	InvalidateRect(emcCamConn->hwnd, NULL, false);

	// INT
	emcShade->selected = ShadeTracer::shade_type;

	// STAMPS 
	emcTimeStamp->selected = sc->sscene.drawStampTime;
	emcNoxStamp->selected = sc->sscene.drawStampLogo;
	InvalidateRect(emcTimeStamp->hwnd, NULL, false);
	InvalidateRect(emcNoxStamp->hwnd, NULL, false);

	updatePositionsVisibilityRenderTab(hTabRender, emcFill->selected, emcEngine->selected);

	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateTimersFromGUItoRtr()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->gui_timers.stoppingTimerOn			= timers.stopOn;
	rtr->gui_timers.autosaveTimerOn			= timers.autosaveOn;
	rtr->gui_timers.refreshTimeOn			= timers.refreshOn;
	rtr->gui_timers.refreshTime				= timers.refreshSec;
	rtr->gui_timers.stopTimerHours			= timers.stopSec/3600;
	rtr->gui_timers.stopTimerMinutes		= (timers.stopSec/60)%60;
	rtr->gui_timers.stopTimerSeconds		= timers.stopSec%60;
	rtr->gui_timers.autosaveTimerMinutes	= timers.autosaveMin;

	rtr->gui_timers.rn_active			= runNext.active;
	rtr->gui_timers.rn_nextCam			= runNext.nextCam;
	rtr->gui_timers.rn_nCamDesc			= runNext.nCamDesc;
	rtr->gui_timers.rn_copyPost			= runNext.copyPost;
	rtr->gui_timers.rn_delBuffs			= runNext.delBuffs;
	rtr->gui_timers.rn_saveImg			= runNext.saveImg;
	rtr->gui_timers.rn_changeSunsky		= runNext.changeSunsky;
	rtr->gui_timers.rn_sunskyMins		= runNext.sunskyMins;
	rtr->gui_timers.rn_fileFormat		= runNext.fileFormat;
	if (rtr->gui_timers.rn_saveFolder)
		free(rtr->gui_timers.rn_saveFolder);
	rtr->gui_timers.rn_saveFolder = copyString(runNext.saveFolder);
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateTimersFromRtrToGUI()
{
	Raytracer * rtr = Raytracer::getInstance();
	timers.stopOn			= rtr->gui_timers.stoppingTimerOn;
	timers.autosaveOn		= rtr->gui_timers.autosaveTimerOn;
	timers.refreshOn		= rtr->gui_timers.refreshTimeOn;
	timers.refreshSec		= rtr->gui_timers.refreshTime;
	timers.stopSec			= (rtr->gui_timers.stopTimerHours*60 + rtr->gui_timers.stopTimerMinutes)*60+rtr->gui_timers.stopTimerSeconds;
	timers.autosaveMin		= rtr->gui_timers.autosaveTimerMinutes;

	runNext.active			= rtr->gui_timers.rn_active;
	runNext.nextCam			= rtr->gui_timers.rn_nextCam;
	runNext.nCamDesc		= rtr->gui_timers.rn_nCamDesc;
	runNext.copyPost		= rtr->gui_timers.rn_copyPost;
	runNext.delBuffs		= rtr->gui_timers.rn_delBuffs;
	runNext.saveImg			= rtr->gui_timers.rn_saveImg;
	runNext.changeSunsky	= rtr->gui_timers.rn_changeSunsky;
	runNext.sunskyMins		= rtr->gui_timers.rn_sunskyMins;
	runNext.fileFormat		= rtr->gui_timers.rn_fileFormat;
	if (runNext.saveFolder)
		free(runNext.saveFolder);
	runNext.saveFolder = copyString(rtr->gui_timers.rn_saveFolder);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::doAfterRenderingStuff()
{
	static int fcounter = 0;
	// SAVE IMAGE
	Logger::add("Rendering stopped automatically");
	if (!runNext.saveFolder)
	{
		Logger::add("Images folder not set - aborting sequence");
		MessageBox(0, "Images folder not set - sequence aborted.", "Error", 0);
		return false;
	}
	if (!dirExists(runNext.saveFolder))
	{
		char buf[2048];
		sprintf_s(buf, 2048, "Images folder \"%s\" does not exist - sequence aborted.", runNext.saveFolder);
		Logger::add(buf);
		MessageBox(0, buf, "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	
	int cc = sc->sscene.activeCamera+1;
	char nbuf[64], imgname[512], fcbuf[64];
	char * ebuf = NULL;
	if (runNext.fileFormat==1)
		ebuf = "jpg";
	else
		if (runNext.fileFormat==2)
			ebuf = "exr";
		else
			ebuf = "png";
	if (runNext.nextCam)
	{
		bool fok = formatStringFromUnsignedInteger(nbuf, 64, 4, cc);
		if (fok)
			sprintf_s(imgname, 512, "img_camera_%s.%s", nbuf, ebuf);
		else
			sprintf_s(imgname, 512, "img_camera_%d.%s", cc, ebuf);
	}
	else
	{
		bool fok = formatStringFromUnsignedInteger(fcbuf, 64, 5, fcounter);
		if (fok)
			sprintf_s(imgname, 512, "img_%s.%s", fcbuf, ebuf);
		else
			sprintf_s(imgname, 512, "img_%d.%s", fcounter, ebuf);
		fcounter++;
	}
	char fullfilename[2048];
	sprintf_s(fullfilename, "%s\\%s", runNext.saveFolder, imgname);
	Logger::add("Saving image to:");
	Logger::add(fullfilename);

	bool ok = saveImage(fullfilename);
	if (ok)
		Logger::add("Saved successfully");
	else
		Logger::add("Saving failed");

	// NEXT CAMERA
	if (runNext.nextCam)
	{
		if (runNext.nCamDesc)
		{
			if (sc->sscene.activeCamera<1)
				return false;
		}
		else
		{
			if (sc->sscene.activeCamera>=sc->cameras.objCount-1)
				return false;
		}

		Logger::add("Releasing camera buffers...");
		Camera * cam_old = sc->getActiveCamera();
		if (!cam_old)
			return false;
		if (runNext.delBuffs)
			cam_old->releaseAllBuffers();
		EMPView * empv = GetEMPViewInstance(hViewport);
		if (empv)
			empv->otherSource = NULL;

		Logger::add("Changing camera...");
		if (runNext.nCamDesc)
			sc->sscene.activeCamera--;
		else
			sc->sscene.activeCamera++;
		if (sc->sscene.activeCamera < 0   ||   sc->sscene.activeCamera >= sc->cameras.objCount)
			return false;
		Camera * cam_new = sc->getActiveCamera();
		if (!cam_new)
			return false;

		// COPY POST
		if (runNext.copyPost)
		{
			Logger::add("Copying settings from old camera...");
			cam_new->fMod = cam_old->fMod;
			cam_new->iMod.copyDataFromAnother(&(cam_old->iMod));

			cam_new->width = cam_old->width;
			cam_new->height = cam_old->height;
			cam_new->aa = cam_old->aa;
		}

		Logger::add("Updating gui stuff...");
		//changecamer
		changeCamera(sc->sscene.activeCamera, true);
	}

	// SUNSKY
	if (runNext.changeSunsky  &&   runNext.sunskyMins!=0)
	{
		Logger::add("Changing sunsky...");
		HWND hEnv = GetDlgItem(hTabEnv, IDC_REND2_ENV_EARTH);
		EMEnvironment * emen = GetEMEnvironmentInstance(hEnv);
		int month = emen->sunsky.getMonth();
		int day = emen->sunsky.getDay();
		int hour = emen->sunsky.getHour();
		float minute = emen->sunsky.getMinute();

		minute += runNext.sunskyMins;
		while (minute>59.0f)
		{
			minute-=60.0f;
			hour++;
		}
		while (minute<0.0f)
		{
			minute+=60.0f;
			hour--;
		}
		while (hour>23)
		{
			hour -= 24;
			day++;
		}
		while (hour<0)
		{
			hour += 24;
			day--;
		}
		int maxday = 31;
		if (month==4 || month==6 || month==9 || month==11)
			maxday=30;
		if (month==2)
			maxday=28;
		if (day>maxday)
		{
			day=1;
			month++;
			if (month>12)
				month = 1;
		}
		if (day<1)
		{
			month--;
			if (month<1)
				month = 12;
			switch (month)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					day = 31;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					day = 30;
					break;
				case 2:
					day = 28;
					break;
			}
		}

		emen->sunsky.setDate(month, day, hour, minute);
		emen->evalSunPosition();
		fillEnvTabAll();

	}

	Logger::add("Start rendering");
	startRender(false);

	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabRender(bool nowrendering)
{
	//Raytracer * rtr = Raytracer::getInstance();
	//bool nowrendering = rtr->curScenePtr->nowRendering;
	setControlEnabled(true, hTabRender, IDC_REND2_REND_NOX_STAMP);
	setControlEnabled(true, hTabRender, IDC_REND2_REND_TIMESTAMP);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabRender, IDC_REND2_REND_GR_MISC), true);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::changeRenderResFromCam(Camera * cam)
{
	CHECK(cam);
	EM2EditSpin * emesWidth = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_WIDTH));
	emesWidth->setIntValue(cam->width);
	EM2EditSpin * emesHeight = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_HEIGHT));
	emesHeight->setIntValue(cam->height);
	EM2EditSpin * emesAA = GetEM2EditSpinInstance(GetDlgItem(hTabRender, IDC_REND2_REND_AA));
	emesAA->setIntValue(cam->aa);
	return true;
}

//------------------------------------------------------------------------------------------------
