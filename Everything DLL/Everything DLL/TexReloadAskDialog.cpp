#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "EM2Controls.h"
#include "noxfonts.h"

extern HMODULE hDllModule;

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
bool initReloadAskControlPositions(HWND hWnd);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
bool updateReloadAskControlBackgrounds(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK TexReloadAskDlg2Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = 450 + wrect.right-wrect.left-crect.right;
				tmprect.bottom = 82 + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);
	
				RECT rect;
				GetClientRect(hWnd, &rect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(rect.right, rect.bottom, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();

				EM2Text * emt = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEXASK_TEXT));
				char * text = lParam ? (char*)lParam : "NOX FOUND THAT TEXTURE WAS CHANGED. DO YOU WANT TO RELOAD IT?";
				emt->changeCaption(text);

				EM2Text * emttext = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEXASK_TEXT));
				emttext->bgImage = hBgBMP;
				emttext->setFont(fonts->em2text, false);

				EM2CheckBox * emcall = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEXASK_ALLFILES));
				emcall->bgImage = hBgBMP;
				emcall->setFont(fonts->em2checkbox, false);

				EM2Button * embyes = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEXASK_YES));
				embyes->bgImage = hBgBMP;
				embyes->setFont(fonts->em2button, false);

				EM2Button * embno = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEXASK_NO));
				embno->bgImage = hBgBMP;
				embno->setFont(fonts->em2button, false);

				initReloadAskControlPositions(hWnd);
				updateReloadAskControlBackgrounds(hWnd);

			}	// end WM_INITDIALOG
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;

			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_TEXASK_YES:
						{
							if (wmEvent == BN_CLICKED)
							{
								EM2CheckBox * emcall = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEXASK_ALLFILES));
								int res = emcall->selected ? 3 : 1;
								EndDialog(hWnd, (INT_PTR)(res)); 
							}
						}
						break;
					case IDC_TEXASK_NO:
						{
							if (wmEvent == BN_CLICKED)
							{
								EM2CheckBox * emcall = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEXASK_ALLFILES));
								int res = emcall->selected ? 2 : 0;
								EndDialog(hWnd, (INT_PTR)(res)); 
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool initReloadAskControlPositions(HWND hWnd)
{
	SetWindowPos(GetDlgItem(hWnd, IDC_TEXASK_TEXT),			HWND_TOP,	10, 18,		430, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEXASK_ALLFILES),		HWND_TOP,	10, 48,		160, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEXASK_YES),			HWND_TOP,	270, 45,	80, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_TEXASK_NO),			HWND_TOP,	360, 45,	80, 22, 0);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateReloadAskControlBackgrounds(HWND hWnd)
{
	EM2Text * emttext = GetEM2TextInstance(GetDlgItem(hWnd, IDC_TEXASK_TEXT));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEXASK_TEXT), 0, 0, emttext->bgShiftX, emttext->bgShiftY);

	EM2CheckBox * emcall = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_TEXASK_ALLFILES));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEXASK_ALLFILES), 0, 0, emcall->bgShiftX, emcall->bgShiftY);

	EM2Button * embyes = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEXASK_YES));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEXASK_YES), 0, 0, embyes->bgShiftX, embyes->bgShiftY);

	EM2Button * embno = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_TEXASK_NO));
	updateControlBgShift(GetDlgItem(hWnd, IDC_TEXASK_NO), 0, 0, embno->bgShiftX, embno->bgShiftY);

	return true;
}

//------------------------------------------------------------------------------------------------

int runAskTexReloadDialog(HWND hWnd, char * txtparam)
{
	int res = (int)DialogBoxParam(hDllModule,
							MAKEINTRESOURCE(IDD_TEX_RELOAD_ASK_DIALOG), hWnd, TexReloadAskDlg2Proc,(LPARAM)( txtparam ));
	return res;
}

//------------------------------------------------------------------------------------------------
