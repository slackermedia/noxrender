#include "resource.h"
#include "RendererMainWindow.h"
#include "MatEditor.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include "XML_IO.h"
#include "ThreadJobWindow.h"

extern char  * defaultDirectory;
extern HMODULE hDllModule;

HWND hRendererMain;

//#define DEBUG_ONE_THREAD
//#define DEBUG_DISABLE_STAMP

HINSTANCE		RendererMainWindow::hInstance1;
HMODULE			RendererMainWindow::hModule = 0;
HWND			RendererMainWindow::hMain  = 0;
HWND			RendererMainWindow::hImage = 0;
HWND 			RendererMainWindow::hOutputConf = 0;
HWND 			RendererMainWindow::hMaterials = 0;
HWND 			RendererMainWindow::hLogs = 0;
HWND 			RendererMainWindow::hFinal = 0;
HWND 			RendererMainWindow::hRegions = 0;
HWND 			RendererMainWindow::hCamera = 0;
HWND 			RendererMainWindow::hBlend = 0;
HWND 			RendererMainWindow::hObjects = 0;
HWND 			RendererMainWindow::hPost = 0;
HWND 			RendererMainWindow::hEnvironment = 0;
HWND 			RendererMainWindow::hMainBar = 0;
HWND 			RendererMainWindow::hTabs = 0;
HWND 			RendererMainWindow::hStatus1 = 0;
HWND 			RendererMainWindow::hStatus2 = 0;
HWND 			RendererMainWindow::hStatus3 = 0;
HWND 			RendererMainWindow::hStatus4 = 0;
HWND 			RendererMainWindow::hProgress1 = 0;
ThreadWindow *	RendererMainWindow::thrWindow = 0;

bool			RendererMainWindow::rendererWindowClassRegistered = false;
bool			RendererMainWindow::showTimeStamp = false;
bool			RendererMainWindow::showLogoStamp = true;
bool			RendererMainWindow::stoppingTimerOn = false;
bool			RendererMainWindow::autosaveTimerOn = false;
bool			RendererMainWindow::refreshTimeOn = true;
bool			RendererMainWindow::stoppingPassOn = false;

RunNextSettings RendererMainWindow::runNext;

bool			RendererMainWindow::refreshThrRunning = false;
int				RendererMainWindow::refreshTime = 10;
int 			RendererMainWindow::stopTimerHours = 0;
int 			RendererMainWindow::stopTimerMinutes = 0;
int 			RendererMainWindow::stopTimerSeconds = 0;
int 			RendererMainWindow::autosaveTimerMinutes = 0;
HBRUSH			RendererMainWindow::backgroundBrush = 0;
ThemeManager 	RendererMainWindow::theme;
float			RendererMainWindow::statusPart = 0.75f;
int				RendererMainWindow::sceneNumber = -1;

int				RendererMainWindow::mainBarHeight = 36;
int				RendererMainWindow::tabsHeight = 24;
int				RendererMainWindow::outputConfHeight = 0;
int				RendererMainWindow::materialsHeight = 0;
int				RendererMainWindow::statusHeight = 18;

int				RendererMainWindow::iposx = 150;
int				RendererMainWindow::iposy = 150;
bool			RendererMainWindow::keepAspect = true;
float			RendererMainWindow::aspectRatio = 4.0f/3.0f;

//----------------------------------------------------------------------------------------------------------------

RendererMainWindow::RendererMainWindow(HINSTANCE hInst)
{
	hInstance1 = hInst;
	if (!rendererWindowClassRegistered)
	{
		hModule = hDllModule;
		MyRegisterClass(hInst);
	}
	hMain = 0;
}

//----------------------------------------------------------------------------------------------------------------

RendererMainWindow::~RendererMainWindow()
{
}

//----------------------------------------------------------------------------------------------------------------

ATOM RendererMainWindow::MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	HICON sIcon = LoadIcon(hDllModule, MAKEINTRESOURCE(IDI_ICON1));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= RendererMainWindow::WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;	//??
	wcex.hIcon			= sIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)CreateSolidBrush(COLOR_WINDOW_BACKGROUND);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "RendererMainWindow";
	wcex.hIconSm		= sIcon;
	return RegisterClassEx(&wcex);
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::create(bool asPlugin, HWND hParent)
{
	int wx = 1280;
	int wy = 800;
	int cx = GetSystemMetrics(SM_CXSCREEN);
	int cy = GetSystemMetrics(SM_CYSCREEN);

	Raytracer * rtr = Raytracer::getInstance();
	rtr->scenes.add(new Scene);
	rtr->scenes.createArray();
	rtr->curSceneInd = rtr->scenes.objCount-1;
	rtr->curScenePtr = rtr->scenes.objArray[rtr->curSceneInd];

	hMain = CreateWindow("RendererMainWindow", "Renderer", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN ,
		  (cx-wx)/2, (cy-wy)/2,   wx,wy,    hParent, NULL, hInstance1, NULL);

	hRendererMain = hMain;

	RECT crect;
	GetClientRect(hMain, &crect);

	hOutputConf  = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_OUTPUT),      hMain, RendererMainWindow::OutputWndProc);
	hMaterials   = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_MATERIALS),   hMain, RendererMainWindow::MaterialsWndProc);
	hFinal		 = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_FINAL),       hMain, RendererMainWindow::FinalWndProc);
	hRegions     = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_REGIONS),     hMain, RendererMainWindow::RegionsWndProc);
	hCamera      = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_CAMERA),      hMain, RendererMainWindow::CameraWndProc);
	hObjects     = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_OBJECTS),     hMain, RendererMainWindow::ObjectsWndProc);
	hBlend		 = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_BLEND),       hMain, RendererMainWindow::BlendWndProc);
	hPost        = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_POST),        hMain, RendererMainWindow::PostWndProc);
	hEnvironment = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_ENVIRONMENT), hMain, RendererMainWindow::EnvironmentWndProc);
	hMainBar     = CreateDialog(hModule, MAKEINTRESOURCE(IDD_RENDERER_MAINBAR),     hMain, RendererMainWindow::MainBarWndProc);

	hImage		= CreateWindow("EMPView", "", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						150,150,  640,480,   hMain, (HMENU)IDC_FINAL_IMAGE, hModule, NULL);

	GetEMPViewInstance(hImage)->allowDragFiles();

	int sp = (int) ( (crect.right-crect.left) * statusPart );
	int sp2 = 120;
	int sp3 = 120;
	int sr = crect.right-crect.left - sp - 5;

	hStatus1	= CreateWindow("EMStatusBar", "aa", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						2,    crect.bottom-crect.top-statusHeight,   sp, statusHeight,   hMain, (HMENU)456, hModule, NULL);
	hStatus2	= CreateWindow("EMStatusBar", "", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						2+sp,    crect.bottom-crect.top-statusHeight,   sp+sp2, statusHeight,   hMain, (HMENU)458, hModule, NULL);
	hStatus3	= CreateWindow("EMStatusBar", "", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						2+sp,    crect.bottom-crect.top-statusHeight,   sp+sp2+sp3, statusHeight,   hMain, (HMENU)459, hModule, NULL);
	hStatus4	= CreateWindow("EMStatusBar", "", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						2+sp,    crect.bottom-crect.top-statusHeight,   sp+sp2+sp3, statusHeight,   hMain, (HMENU)460, hModule, NULL);
	hProgress1  = CreateWindow("EMProgressBar", "", WS_CHILD | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						sp+sp2+3, crect.bottom-crect.top-statusHeight,   sr, statusHeight,   hMain, (HMENU)457, hModule, NULL);
	hTabs		= CreateWindow("EMTabs", "", WS_CHILD  | WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS ,
						150, mainBarHeight, 600, 30, hMain, (HMENU)345, hModule, NULL);


	EMStatusBar * emsb = GetEMStatusBarInstance(hStatus1);
	EMStatusBar * emsb2 = GetEMStatusBarInstance(hStatus2);
	EMStatusBar * emsb3 = GetEMStatusBarInstance(hStatus3);
	EMStatusBar * emsb4 = GetEMStatusBarInstance(hStatus4);
	EMProgressBar * empb = GetEMProgressBarInstance(hProgress1);
	theme.apply(emsb);
	theme.apply(emsb2);
	theme.apply(emsb3);
	theme.apply(emsb4);
	theme.apply(empb);
	emsb->setCaption("");
	EMTabs * emt = GetEMTabsInstance(hTabs);
	emt->caps.add("Render");
	emt->caps.add("Regions");
	emt->caps.add("Camera");
	emt->caps.add("Objects");
	emt->caps.add("Materials");
	emt->caps.add("Environment");
	emt->caps.add("Blend");
	emt->caps.add("Post");
	emt->caps.add("Final");
	emt->updateLenghts();
	emt->hRegion = 0;
	tabsHeight = emt->heightTabSel;
	theme.apply(emt);
	
	EMPView * empv = GetEMPViewInstance(hImage);
	theme.apply(empv);
	empv->createRegions();
	empv->mode = EMPView::MODE_SHOW;
	if (empv->rgnPtr)
		empv->rgnPtr->airbrush_size = 14;

	#ifdef _WIN64
		SetClassLongPtr(hImage, GCLP_HBRBACKGROUND, (LONG_PTR)0);
	#else
		SetClassLong(hImage, GCL_HBRBACKGROUND, (LONG)(LONG_PTR)0);
	#endif

	if (!hMain)
		return false;

	ShowWindow(hMain, SW_SHOW);
	UpdateWindow(hMain);

	tabsChanged();	
	lockUnlockAfterStart();

	Raytracer::getInstance()->curScenePtr->registerProgressCallback(notifyProgressBarCallback, 100);
	Raytracer::getInstance()->logoStamp.loadFromResource(IDB_NOX_LOGO_STAMP);

	InvalidateRect(hMain, NULL, false);
	UpdateWindow(hMain);

	Sleep(100);
	bool loadOK = Raytracer::getInstance()->loadMatEditorScenes(notifyProgressBarCallback);

	Raytracer::getInstance()->curSceneInd = 0;
	Raytracer::getInstance()->curScenePtr = Raytracer::getInstance()->scenes[0];

	checkForNOXUpdateAsThread();

	int numThreads, numCores, numProcessors;
	if (getProcessorInfoAdvanced(numProcessors, numCores, numThreads))
	{
		char buf[256];
		sprintf_s(buf, 256, "Found %d processors with %d cores and %d threads.", numProcessors, numCores, numThreads);
		Logger::add(buf);
		char * cpuname = getProcessorInfo(false);
		if (cpuname)
			Logger::add(cpuname);
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

HWND RendererMainWindow::getHWND()
{
	return hMain;
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::tabsChanged()
{
	EMTabs * emt = GetEMTabsInstance(hTabs);

	ShowWindow(hOutputConf,   SW_HIDE);
	ShowWindow(hMaterials,    SW_HIDE);
	ShowWindow(hFinal,        SW_HIDE);
	ShowWindow(hRegions,      SW_HIDE);
	ShowWindow(hEnvironment,  SW_HIDE);
	ShowWindow(hCamera,       SW_HIDE);
	ShowWindow(hObjects,      SW_HIDE);
	ShowWindow(hPost,         SW_HIDE);
	ShowWindow(hBlend,        SW_HIDE);

	if (emt->lastSelected == POSFINAL)
	{
		SendMessage(hFinal, EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);
	}
	if (emt->lastSelected == POSREGIONS)
	{
		SendMessage(hRegions, EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);
	}

	switch (emt->selected)
	{
		case POSRENDER:			ShowWindow(hOutputConf  , SW_SHOW);	break;
		case POSREGIONS:		ShowWindow(hRegions     , SW_SHOW);	break;
		case POSCAMERA:			ShowWindow(hCamera      , SW_SHOW);	break;
		case POSOBJECTS:		ShowWindow(hObjects     , SW_SHOW);	break;
		case POSMATERIALS:		ShowWindow(hMaterials   , SW_SHOW);	break;
		case POSENVIRONMENT:	ShowWindow(hEnvironment , SW_SHOW);	break;
		case POSBLEND:			ShowWindow(hBlend		, SW_SHOW);	break;
		case POSPOST:			ShowWindow(hPost        , SW_SHOW);	break;
		case POSFINAL:			ShowWindow(hFinal       , SW_SHOW);	break;
	}

	resizePictureView();
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::loadPostprocess()
{
	char * filename;
	filename = openFileDialog(hMain, "XML postprocess settings (*.nxp)\0*.nxp\0", "Load postprocess", "nxp", 1);
	if (!filename)
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = NULL;
	if (rtr->curScenePtr)
		cam = rtr->curScenePtr->getActiveCamera();
	if (!cam)
	{
		MessageBox(0, "No camera found.", "", 0);
		return false;
	}

	XMLScene xscene;
	UpdateWindow(hMain);

	int ok = xscene.loadPost(filename, cam);
	if (ok)
	{
		fillCameraPostData(cam);
		EMPView * empv = GetEMPViewInstance(hImage);
		empv->imgMod->copyDataFromAnother(&cam->iMod);
		refreshRenderedImage();
	}
	else
	{
		int err = xscene.errLine;
		char buf[256];
		if (err>0)
			sprintf_s(buf, 256, "Something is wrong with loading postprocess settings.\nWatch line %d", err); 
		else
			sprintf_s(buf, 256, "Something is wrong with XML while loading postprocess settings.\nError code: %d", err); 
		MessageBox(0, buf, "", 0);
	}


	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::loadFinalPost()
{
	char * filename;
	filename = openFileDialog(hMain, "XML final postprocess settings (*.nxf)\0*.nxf\0", "Load final postprocess", "nxf", 1);
	if (!filename)
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = NULL;
	if (rtr->curScenePtr)
		cam = rtr->curScenePtr->getActiveCamera();
	if (!cam)
	{
		MessageBox(0, "No camera found.", "", 0);
		return false;
	}

	XMLScene xscene;
	UpdateWindow(hMain);

	int ok = xscene.loadFinal(filename, cam);
	if (ok)
	{
		fillFinalSettings(cam);
	}
	else
	{
		int err = xscene.errLine;
		char buf[256];
		if (err>0)
			sprintf_s(buf, 256, "Something is wrong with loading final postprocess settings.\nWatch line %d", err); 
		else
			sprintf_s(buf, 256, "Something is wrong with XML while loading final postprocess settings.\nError code: %d", err); 
		MessageBox(0, buf, "", 0);
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::savePostprocess()
{
	char * filename;
	filename = saveFileDialog(hMain, "XML postprocess settings (*.nxp)\0*.nxp\0", "Save postprocess", "nxp", 1);
	if (!filename)
		return false;

	XMLScene xscene;
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = NULL;
	if (rtr->curScenePtr)
		cam = rtr->curScenePtr->getActiveCamera();
		
	bool saved = xscene.savePost(filename, cam);
	
	if (saved)
		MessageBox(hMain, "Saved", "", 0);
	else
		MessageBox(hMain, "Not saved", "", 0);

	return saved;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::saveFinalPost()
{
	char * filename;
	filename = saveFileDialog(hMain, "XML final postprocess settings (*.nxf)\0*.nxf\0", "Save final postprocess", "nxf", 1);
	if (!filename)
		return false;

	XMLScene xscene;
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = NULL;
	if (rtr->curScenePtr)
		cam = rtr->curScenePtr->getActiveCamera();
		
	bool saved = xscene.saveFinal(filename, cam);
	
	if (saved)
		MessageBox(hMain, "Saved", "", 0);
	else
		MessageBox(hMain, "Not saved", "", 0);

	return saved;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::saveScene(char * filename, bool withTextures, bool silentMode)
{
	Raytracer * rtr = Raytracer::getInstance();

	rtr->curScenePtr->sscene.sunsky->copyFrom(
		&(GetEMEnvironmentInstance(GetDlgItem(hEnvironment, IDC_REND_ENV_MAP))->sunsky));

	UpdateWindow(hMain);
	if (!filename)
	{
		Logger::add("File was not selected.");
		return false;
	}
	if (strlen(filename) < 5)
	{
		Logger::add("Something wrong with file name.");
		return false;
	}

	unsigned int fnlen = (unsigned int)strlen(filename);
	bool isBinaryExt = false;
	if (	(filename[fnlen-3]=='n'  ||  filename[fnlen-3]=='N')  &&    
			(filename[fnlen-2]=='o'  ||  filename[fnlen-2]=='O')  &&    
			(filename[fnlen-1]=='x'  ||  filename[fnlen-1]=='X'))
		isBinaryExt = true;

	updateTimersFromGUI();

	bool addCamBuffs = true;
	if (withTextures)
	{
		char * fdir = getDirectory(filename);
		char * tsdir = getOnlyFile(filename);
		char * sdir = NULL;
		if (tsdir  &&  strlen(tsdir)>0)
		{
			int sts = (int)strlen(tsdir);
			sdir = (char*)malloc(sts+32);
			if (sdir)
				sprintf_s(sdir, sts+32, "%s textures", tsdir);
		}

		bool changed = rtr->curScenePtr->changeTexturePaths(fdir, sdir);

		if (fdir)
			free(fdir);
		if (tsdir)
			free(tsdir);
		if (sdir)
			free(sdir);

		int addbb = MessageBox(hMain, "Add rendered buffers to saved file?\nThis may greatly increase file size.", "Exporting", MB_YESNO|MB_ICONQUESTION);
		if (IDYES==addbb)
			addCamBuffs = true;
		else
			addCamBuffs = false;
	}


	int err;
	if (isBinaryExt)
		err = rtr->curScenePtr->saveSceneToBinary(filename, addCamBuffs, silentMode);
	else
		err = rtr->curScenePtr->saveSceneToXML(filename, addCamBuffs);
	if (err == 0)
	{
	}

	if (withTextures)
	{
		// release relative paths in texture file names
		rtr->curScenePtr->releaseRelativePaths();
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMainWindow::LoadSceneThreadProc(LPVOID lpParameter)
{
	if (!lpParameter)
		return 0;	// scene name null

	Raytracer * rtr = Raytracer::getInstance();
	char * filename = (char *)lpParameter;

	loadScene(filename);

	ThreadWindow * tw = RendererMainWindow::thrWindow;
	if (tw)
	{
		rtr->curScenePtr->abortThreadPtr = NULL;
		tw->closeWindow();
		delete tw;
		RendererMainWindow::thrWindow = NULL;
	}

	Raytracer::getInstance()->curScenePtr->registerProgressCallback(notifyProgressBarCallback, 100);

	return 0;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::tryLoadSceneDroppedAsync(char * filename, bool andStartRendering)
{
	return loadSceneAsync(filename, andStartRendering);
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::loadSceneAsync(char * filename, bool andStartRendering)
{
	if (!filename)
		return false;
	Raytracer * rtr = Raytracer::getInstance();
	ThreadWindow * tw = new ThreadWindow(); 
	RendererMainWindow::thrWindow = tw;
	tw->createWindow(hMain, hDllModule);
	tw->setWindowTitle("Loading scene");
	rtr->curScenePtr->startAfterLoad = andStartRendering;
	rtr->curScenePtr->abortThreadPtr = &(tw->aborted);
	rtr->curScenePtr->registerProgressCallback(ThreadWindow::notifyProgressBarCallback ,50);
						
	DWORD threadId;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMainWindow::LoadSceneThreadProc), (LPVOID)filename, 0, &threadId);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

bool RendererMainWindow::loadScene(char *filename)
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->sscene.useSunSky = false;

	UpdateWindow(hMain);
	if (!filename)
	{
		Logger::add("File was not selected.");
		return false;
	}
	unsigned int fnlen = (unsigned int)strlen(filename);
	if (fnlen < 5)
	{
		Logger::add("Something wrong with file name.");
		return false;
	}

	bool isBinaryExt = false;
	if (	(filename[fnlen-3]=='n'  ||  filename[fnlen-3]=='N')  &&    
			(filename[fnlen-2]=='o'  ||  filename[fnlen-2]=='O')  &&    
			(filename[fnlen-1]=='x'  ||  filename[fnlen-1]=='X'))
		isBinaryExt = true;

	disableControl(hMainBar, IDC_REND_MAINBAR_LOADSCENE);
	GetEMPViewInstance(hImage)->denyDragFiles();

	int err = 0;
	if (isBinaryExt)
	{

		err = rtr->curScenePtr->loadSceneFromBinary(filename);
		if (err)
			MessageBox(0, "Binary NOX scene load failed", "", 0);
	}
	else
		err = rtr->curScenePtr->loadSceneFromXML(filename);

	if (err == 0)
	{
		Logger::add("Scene loaded.");

		if (!runNext.saveFolder)
		{
			runNext.saveFolder = getDirectory(filename);
		}

		Logger::add("Initializing");
		rtr->curScenePtr->postLoadInitialize(false);

		Camera * ccam = rtr->curScenePtr->getActiveCamera();

		if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
		{
			Logger::add("Aborted by user!!!");
			return false;
		}


		rtr->curScenePtr->decodeSceneStrings();

		char bb[512];
		sprintf_s(bb, "Triangles: %d", rtr->curScenePtr->triangles.objCount);
		Logger::add(bb);
		sprintf_s(bb, "Materials: %d", rtr->curScenePtr->mats.objCount);
		Logger::add(bb);

		if (ccam)
		{
			EMEditSpin * emesw = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_WIDTH));
			EMEditSpin * emesh = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_HEIGHT));
			EMEditSpin * emesa = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_AA));
			emesw->setValuesToInt(ccam->width,  NOX_IMAGE_WIDTH_MIN, NOX_IMAGE_WIDTH_MAX, 1, 1);
			emesh->setValuesToInt(ccam->height, NOX_IMAGE_HEIGHT_MIN, NOX_IMAGE_HEIGHT_MAX, 1, 1);
			emesa->setValuesToInt(ccam->aa,     NOX_IMAGE_AA_MIN, NOX_IMAGE_AA_MAX, 1, 1);
			aspectRatio = ccam->width/(float)ccam->height;
		}
		fillMaterialsList();
		fillMaterialsImageList();
		fillObjectsList();
		fillInstancesList();
		fillCameraList();
		fillBlendLayersData();
		fillEnvironmentMapData();

		fillCameraData();
		fillCameraPostData();
		GetEMEnvironmentInstance(GetDlgItem(hEnvironment, IDC_REND_ENV_MAP))->sunsky.copyFrom(rtr->curScenePtr->sscene.sunsky);
		GetEMEnvironmentInstance(GetDlgItem(hEnvironment, IDC_REND_ENV_MAP))->evalSunPosition();
		GetEMEnvironmentInstance(GetDlgItem(hEnvironment, IDC_REND_ENV_MAP))->evalShadow();
		InvalidateRect(GetDlgItem(hEnvironment, IDC_REND_ENV_MAP), NULL, false);
		fillSunSkyControls();
		updateTimersAndRunNext();
		fillTimersData();
		fillRenderSettings();
		fillFinalSettings(ccam);

		lockUnlockAfterLoadScene();

		char bbb[64];
		sprintf_s(bbb, 64, "cam addr: %d", (int)(INT_PTR)ccam);
		Logger::add(bbb);
		sprintf_s(bbb, 64, "cam buff addr: %d", (int)(INT_PTR)ccam->imgBuff);
		Logger::add(bbb);

		if (ccam)
		{
			if (!ccam->imgBuff)
				ccam->imgBuff = new ImageBuffer();
			if (ccam->aa < 1)
				ccam->aa = 1; 
			if (ccam->aa > 3)
				ccam->aa = 3; 
			if (!ccam->imgBuff->imgBuf   ||   !ccam->imgBuff->hitsBuf)
				ccam->imgBuff->allocBuffer(ccam->width*ccam->aa , ccam->height*ccam->aa);

		}

		if (ccam   &&   ccam->imgBuff   &&   ccam->imgBuff->imgBuf   &&   ccam->imgBuff->hitsBuf   &&   ccam->aa>0)
		{
			int cw = ccam->imgBuff->width/ccam->aa;
			int ch = ccam->imgBuff->height/ccam->aa;
			EMEditSpin * emesw = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_WIDTH));
			EMEditSpin * emesh = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_HEIGHT));
			emesw->setValuesToInt(cw, NOX_IMAGE_WIDTH_MIN , NOX_IMAGE_WIDTH_MAX,  1, 1);
			emesh->setValuesToInt(ch, NOX_IMAGE_HEIGHT_MIN, NOX_IMAGE_HEIGHT_MAX, 1, 1);

			EMPView * empv = GetEMPViewInstance(RendererMainWindow::hImage);
			empv->otherSource = ccam->imgBuff;

			empv->byteBuff->allocBuffer(cw, ch);

			{
				ImageBuffer * ib;
				if (empv->otherSource)
					ib = empv->otherSource;
				else
					ib = empv->imgBuff;
				RECT rect;
				GetClientRect(hImage, &rect);
				if (empv->mx > ib->width/(float)rect.right-1.0f/empv->zoom)
					empv->mx = ib->width/(float)rect.right-1.0f/empv->zoom;
				if (empv->my > ib->height/(float)rect.bottom-1.0f/empv->zoom)
					empv->my = ib->height/(float)rect.bottom-1.0f/empv->zoom;
				if (empv->mx < 0.0f)
					empv->mx = 0.0f;
				if (empv->my < 0.0f)
					empv->my = 0.0f;
			}

			empv->imgMod->copyDataFromAnother(&ccam->iMod);
			updateRenderedImage(empv, true, false, NULL, false);

			char * tst = createTimeStamp();
			if (tst)
			{
				if (empv->byteBuff)  
				{
					if (empv->byteBuff->timestamp)
						free(empv->byteBuff->timestamp);
					empv->byteBuff->timestamp = tst;
				}
			}
			#ifndef DEBUG_DISABLE_STAMP
				if (showTimeStamp)
					empv->byteBuff->applyTimeStamp();
				if (showLogoStamp)
					empv->byteBuff->applyLogoStamp(&rtr->logoStamp);
			#endif

			refreshTimeInStatus();

		}

		InvalidateRect(hImage, NULL, false);

		rtr->curScenePtr->notifyProgress("Scene loaded", 0);

		sceneNumber = rtr->curSceneInd;

		// LOAD POST FILE
		if (rtr->curScenePtr->postLoadPostFile)
		{
			Logger::add("Loading post file...");
			XMLScene xscene;
			int ok = xscene.loadPost(rtr->curScenePtr->postLoadPostFile, ccam);
			if (ok)
			{
				Logger::add("Load OK");
				fillCameraPostData(ccam);
			}
			else
				Logger::add("Load failed");
		}

		// LOAD BLEND FILE
		if (rtr->curScenePtr->postLoadBlendFile)
		{
			Logger::add("Loading blend file...");
			XMLScene xscene;
			int ok = xscene.loadBlendSettings(rtr->curScenePtr->postLoadBlendFile, rtr->curScenePtr->blendSettings);
			if (ok)
			{
				Logger::add("Load OK");
				fillBlendLayersData();
			}
			else
				Logger::add("Load failed");
		}

		// LOAD FINAL FILE
		if (rtr->curScenePtr->postLoadFinalFile)
		{
			Logger::add("Loading final file...");
			XMLScene xscene;
			int ok = xscene.loadFinal(rtr->curScenePtr->postLoadFinalFile, ccam);
			if (ok)
			{
				Logger::add("Load OK");
				fillFinalSettings(ccam);
			}
			else
				Logger::add("Load failed");
		}

		if (rtr->curScenePtr->startAfterLoad)
		{
			updateEngineSettingsBeforeRender(); // does nothing
			startStoppingTimer();
			startRendering();
			runRefreshThread();
		}

	}
	else
	{
		if (isBinaryExt)
		{
		}
		else
		{
			char xbuf[256];
			if (err > 0)
			{
				sprintf_s(xbuf, 256, "Scene not loaded! Watch line %d.", err);
				Logger::add(xbuf);
				rtr->curScenePtr->notifyProgress("Scene not loaded", 0);
			}
			else
			{
				Logger::add("XML structure inconsistent.");
				rtr->curScenePtr->notifyProgress("Scene not loaded", 0);
			}
		}
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMainWindow::MergeSceneThreadProc(LPVOID lpParameter)
{
	if (!lpParameter)
		return 0;	// scene name null

	Raytracer * rtr = Raytracer::getInstance();
	char * filename = (char *)lpParameter;
	CHECK(filename);

	mergeScene(filename);

	ThreadWindow * tw = RendererMainWindow::thrWindow;
	if (tw)
	{
		rtr->curScenePtr->abortThreadPtr = NULL;
		tw->closeWindow();
		delete tw;
		RendererMainWindow::thrWindow = NULL;
	}

	Raytracer::getInstance()->curScenePtr->registerProgressCallback(notifyProgressBarCallback, 100);

	refreshRenderedImage();

	return 0;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::mergeSceneAsync(char * filename)
{
	if (!filename)
		return false;
	Raytracer * rtr = Raytracer::getInstance();
	ThreadWindow * tw = new ThreadWindow(); 
	RendererMainWindow::thrWindow = tw;
	tw->createWindow(hMain, hDllModule);
	tw->setWindowTitle("Loading scene for merge");
	rtr->curScenePtr->abortThreadPtr = &(tw->aborted);
	rtr->curScenePtr->registerProgressCallback(ThreadWindow::notifyProgressBarCallback ,50);
						
	DWORD threadId;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RendererMainWindow::MergeSceneThreadProc), (LPVOID)filename, 0, &threadId);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::mergeScene(char * filename)
{
	if (!filename)
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	if (!rtr->curScenePtr->mergeSceneFromBinary(filename))
		return false;

	InvalidateRect(hImage, NULL, false);

	return true;	
}

//----------------------------------------------------------------------------------------------------------------

int stTime = 0;

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::startRendering(bool withErase)
{
	Raytracer * rtr = Raytracer::getInstance();

	Camera * cam = rtr->curScenePtr->cameras[rtr->curScenePtr->sscene.activeCamera];
	cam->blendBits = rtr->curScenePtr->evalBlendBits();
	cam->bucket_hwnd_image = hImage;
	lockUnlockAfterRenderStart();
	GetEMPViewInstance(hImage)->denyDragFiles();

	int w,h,aa;
	EMPView * empv = GetEMPViewInstance(hImage);
	w = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_WIDTH))->intValue;
	h = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_HEIGHT))->intValue;
	aa = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_AA))->intValue;
	rtr->curScenePtr->blendBits = rtr->curScenePtr->evalBlendBits();
	cam->width = w;
	cam->height = h;
	cam->aa = aa;

	empv->rgnPtr = &(cam->staticRegion);
	bool initOK = cam->initializeCamera(withErase);
	if (!initOK)
	{
		rtr->curScenePtr->notifyProgress("Stop forced.", 0);
		lockUnlockAfterRenderEnd();
		return;
	}
	lockUnlockAfterRenderStart();
	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();
	empv->byteBuff->allocBuffer(w,h);
	empv->byteBuff->clearBuffer();
	empv->otherSource = cam->imgBuff;
	empv->rgnPtr->createRegions(cam->width, cam->height);
	if (withErase)
	{
		cam->staticRegion.overallHits = 0;
	}

	rtr->curScenePtr->notifyProgress("Rendering", 0);

	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	int prn = rtr->curScenePtr->thrM->getProcessorsNumber();
	if (prn < 1)
		prn = 1;

	#ifdef DEBUG_ONE_THREAD
		prn = 1;
	#endif

	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		MaterialNox * m = rtr->curScenePtr->mats[i];
		if (m)
			m->prepareToRender();
	}

	if (withErase)
	{
		cam->rendTime = 0;
		for (int cc=0; cc<16; cc++)
		{
			if (!(cam->blendBits&(1<<cc)))
				continue;
			for (int y=0; y<cam->blendBuffGI[cc].height; y++)
				for (int x=0; x<cam->blendBuffGI[cc].width; x++)
					cam->blendBuffGI[cc].hitsBuf[y][x] = 0;
		}

	}
	cam->startTick = GetTickCount();
	cam->nowRendering = true;
	rtr->curScenePtr->sscene.sunsky->copyFrom(
		&(GetEMEnvironmentInstance(GetDlgItem(hEnvironment, IDC_REND_ENV_MAP))->sunsky));


	stTime = cam->rendTime;

	if (cam->buckets)
	{
		empv->showBuckets = true;
		cam->buckets->nThr = prn;
		cam->buckets->coordsThreads = (int *)malloc(cam->buckets->nThr*sizeof(int)*4);
		for (int i=0; i<cam->buckets->nThr*4; i++)
			cam->buckets->coordsThreads[i] = -1;
		empv->bucketsCoords = cam->buckets->coordsThreads;
		empv->nBuckets = cam->buckets->nThr;
	}
	stoppingPassOn = false;

	rtr->curScenePtr->thrM->registerStoppedCallback(stoppedRenderingCallback);
	rtr->curScenePtr->thrM->initThreads(prn, false);
	rtr->curScenePtr->thrM->runThreads();
	rtr->curScenePtr->nowRendering = true;

}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::stopRendering()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->thrM->showThreadsDlls();

	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->resumeThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	lockUnlockAfterRenderEnd();
	rtr->curScenePtr->nowRendering = false;
	Camera * cam = rtr->curScenePtr->cameras[rtr->curScenePtr->sscene.activeCamera];
	cam->nowRendering = false;
	int ss = (GetTickCount() - cam->startTick)/1000;
	if (ss < 0)
	{
	}
	cam->rendTime += ss;
	if (cam->buckets)
	{
		cam->buckets->nThr = 0;
		if (cam->buckets->coordsThreads)
			free(cam->buckets->coordsThreads);
		cam->buckets->coordsThreads = NULL;
	}

	EMPView * empv = GetEMPViewInstance(hImage);
	empv->bucketsCoords = NULL;
	empv->nBuckets = 0;
	empv->showBuckets = false;
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::stoppedRenderingCallback(void)
{
	stoppingPassOn = true;
	SendMessage(hOutputConf, WM_TIMER, TIMER_STOP_ID, 0);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::refreshTimeInStatus()
{
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (!cam)
		return;

	int sec = cam->getRendTime(GetTickCount());
	char * timeinfo = formatTime(sec);
	char temp[128];
	sprintf_s(temp, 128, "Render time: %s", timeinfo);

	EMStatusBar * emsb = GetEMStatusBarInstance(hStatus3);
	emsb->setCaption(temp);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::refreshCounterInStatus()
{
	Raytracer * rtr = Raytracer::getInstance();
	if (!rtr->curScenePtr->nowRendering)
		return;
		
	unsigned long long cnt = rtr->curScenePtr->thrM->collectPerfCounters();

	char txt[128];
	if (cnt>10000)
		if (cnt>10000000)
			sprintf_s(txt, 128, "Samples/sec: %dM", cnt/1000000);
		else
			sprintf_s(txt, 128, "Samples/sec: %dK", cnt/1000);
	else
		sprintf_s(txt, 128, "Samples/sec: %d", cnt);

	EMStatusBar * emsb = GetEMStatusBarInstance(hStatus4);
	emsb->setCaption(txt);

}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::saveImage(char * filename)
{
	if (!filename)
	{
		Logger::add("No file selected.");
		return false;
	}

	int l = (int)strlen(filename);
	if (l < 5)
	{
		char buf[64];
		sprintf_s(buf, 64, "Wrong filename: \"%s\"", filename);
		Logger::add(buf);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (!cam)
		return false;
	int aa = cam->aa;
	ImageModifier * iMod = &cam->iMod;
	ImageBuffer * iBuff;
	EMPView * empv = GetEMPViewInstance(hImage);
	if (!empv->otherSource)
		iBuff = empv->imgBuff;
	else
		iBuff = empv->otherSource;

	ImageByteBuffer * ibbuf = empv->byteBuff;
	if (!ibbuf)
		return false;

	char * tst = NULL;
	#ifndef DEBUG_DISABLE_STAMP
		if (showTimeStamp)
			tst = createTimeStamp();
	#endif

	char l0,l1,l2,l3,l4;
	l0 = filename[l-4];
	l1 = filename[l-3];
	l2 = filename[l-2];
	l3 = filename[l-1];
	l4 = filename[l-5];

	int filetype = 0;	// unspecified
	if ( (l0 == '.') && (l1 == 'e' || l1 == 'E')  &&  (l2 == 'x' || l2 == 'X')  &&  (l3 == 'r' || l3 == 'R'))
		filetype = 1;	// exr
	if ( (l0 == '.') && (l1 == 'b' || l1 == 'B')  &&  (l2 == 'm' || l2 == 'M')  &&  (l3 == 'p' || l3 == 'P'))
		filetype = 2;	// bmp
	if ( (l0 == '.') && (l1 == 'p' || l1 == 'P')  &&  (l2 == 'n' || l2 == 'N')  &&  (l3 == 'g' || l3 == 'G'))
		filetype = 3;	// png
	if ( (l0 == '.') && (l1 == 'j' || l1 == 'J')  &&  (l2 == 'p' || l2 == 'P')  &&  (l3 == 'g' || l3 == 'G'))
		filetype = 4;	// jpg
	if ( (l4 == '.') && (l0 == 't' || l0 == 'T')  &&  (l1 == 'i' || l1 == 'I')  &&  (l2 == 'f' || l2 == 'F')  &&  (l3 == 'f' || l3 == 'F'))
		filetype = 5;	// tiff

	bool saveOK = false;

	Logger::add("Saving:");
	Logger::add(filename);
	rtr->curScenePtr->notifyProgress("Saving image", 0);

	ImageBuffer * logo = &rtr->logoStamp;
	#ifdef DEBUG_DISABLE_STAMP
		logo = NULL;
	#endif
	if (!showLogoStamp)
		logo = NULL;

	NoxEXIFdata exif;
	exif.iso = (short)(int)(cam->ISO*1.00001f);
	exif.focalNom = (unsigned int)(0.5f * 36 / tan(cam->angle * PI / 360.0f));
	exif.focalDenom = 1;
	exif.shutterNom = 10000;
	exif.shutterDenom = (unsigned int)(cam->shutter*10000);
	exif.fNumberDenom = 100;
	exif.fNumberNom = (unsigned int)(cam->aperture*100);

	switch (filetype)
	{
		case 1:
			saveOK = iBuff->saveAsEXR(filename, *iMod, aa);
			break;
		case 2:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 1, &exif);
			break;
		case 3:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 2, &exif);
			break;
		case 4:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 4, &exif);
			break;
		case 5:
			saveOK = ibbuf->saveAsWindowsFormat(filename, 3, &exif);
			break;
		default:
			saveOK = false;
			break;
	}

	if (tst)
		free(tst);
	tst = NULL;

	if (!saveOK)
	{
		Logger::add("Not saved!");
		MessageBox(NULL, "Image not saved!", "Error", 0);
		return false;
	}
	else
	{
		rtr->curScenePtr->notifyProgress("Image saved", 0);
		Logger::add("Saved");
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::editMaterial(int matnum)
{
	Raytracer * rtr = Raytracer::getInstance();

	bool accept = false;

	if (matnum >= 0   &&   matnum < rtr->curScenePtr->mats.objCount)
	{
		MaterialNox * mat = rtr->curScenePtr->mats[matnum]->copy();
		
		if (rtr->editedMaterial)
		{
			rtr->editedMaterial->deleteBuffers(true, true, true);
			rtr->editedMaterial->layers.destroyArray();
			rtr->editedMaterial->layers.freeList();
			delete rtr->editedMaterial;
			rtr->editedMaterial = NULL;
		}

		rtr->editedMaterial = mat;
	
		rtr->editedMaterial->updateSceneTexturePointers(rtr->curScenePtr);

		MatEditorWindow matEdit(hDllModule);
		matEdit.blendNames.copyNamesFrom(&(rtr->curScenePtr->blendSettings));
		accept = matEdit.createWindow(hMain, MatEditorWindow::MODE_RENDERER, NULL);

		Logger::add("i'm back");
	}
	else
	{
		MessageBox(hMaterials, "No material selected!", "Error", 0);
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::notifyBackFromMaterialEditor(bool AcceptChanges)	// not used
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curSceneInd = sceneNumber;
	rtr->curScenePtr = rtr->scenes[sceneNumber];
	EMImageList * emil = GetEMImageListInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_IMAGE_LIST));
	int m = emil->imageSelected;

	if (AcceptChanges)
	{
		MaterialNox * oldmat = rtr->curScenePtr->mats[m];
		MaterialNox * newmat = rtr->editedMaterial->copy();
		newmat->id = oldmat->id;
		rtr->curScenePtr->mats.setElement(m, newmat);
		rtr->curScenePtr->mats.createArray();
		delete oldmat;
		delete rtr->editedMaterial;
		rtr->editedMaterial = NULL;
	}

	fillMaterialsImageList();
	SetFocus(hMain);
	ShowWindow(hMain, SW_RESTORE);
	updateMaterialPreviewFullSize(m);

	Logger::add("RENDERER MODE");
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::afterMatEdit(bool accepted, int matnum)
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curSceneInd = sceneNumber;
	rtr->curScenePtr = rtr->scenes[sceneNumber];

	if (accepted)
	{
		int m = matnum;
		MaterialNox * oldmat = rtr->curScenePtr->mats[m];
		MaterialNox * newmat = rtr->editedMaterial->copy();
		newmat->id = oldmat->id;
		rtr->curScenePtr->mats.setElement(m, newmat);
		rtr->curScenePtr->mats.createArray();

		newmat->tex_opacity.managerID = rtr->curScenePtr->texManager.addTexture(newmat->tex_opacity.fullfilename, false, newmat->tex_opacity.texScPtr, false);
		newmat->tex_displacement.managerID = rtr->curScenePtr->texManager.addTexture(newmat->tex_displacement.fullfilename, false, newmat->tex_displacement.texScPtr, false);
		for (int i=0; i<newmat->layers.objCount; i++)
		{
			MatLayer * mlay = &(newmat->layers[i]);
			mlay->tex_weight.managerID      = rtr->curScenePtr->texManager.addTexture(mlay->tex_weight.fullfilename, false, mlay->tex_weight.texScPtr, false);
			mlay->tex_col0.managerID        = rtr->curScenePtr->texManager.addTexture(mlay->tex_col0.fullfilename,   false, mlay->tex_col0.texScPtr, false);
			mlay->tex_col90.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_col90.fullfilename,  false, mlay->tex_col90.texScPtr, false);
			mlay->tex_light.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_light.fullfilename,  false, mlay->tex_light.texScPtr, false);
			mlay->tex_normal.managerID      = rtr->curScenePtr->texManager.addTexture(mlay->tex_normal.fullfilename, false, mlay->tex_normal.texScPtr, true);
			mlay->tex_transm.managerID      = rtr->curScenePtr->texManager.addTexture(mlay->tex_transm.fullfilename, false, mlay->tex_transm.texScPtr, false);
			mlay->tex_rough.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_rough.fullfilename,  false, mlay->tex_rough.texScPtr, false);
			mlay->tex_aniso.managerID       = rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso.fullfilename, false, mlay->tex_aniso.texScPtr, false);
			mlay->tex_aniso_angle.managerID = rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso_angle.fullfilename, false, mlay->tex_aniso_angle.texScPtr, false);
		}
		rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);
		newmat->updateSceneTexturePointers(rtr->curScenePtr);

		if (oldmat)
		{
			oldmat->deleteBuffers(true, true, true);
			oldmat->layers.destroyArray();
			oldmat->layers.freeList();
			delete oldmat;
			oldmat = NULL;
		}
	}
	

	if (rtr->editedMaterial)
	{
		rtr->editedMaterial->deleteBuffers(true, true, true);
		rtr->editedMaterial->layers.destroyArray();
		rtr->editedMaterial->layers.freeList();
		delete rtr->editedMaterial;
		rtr->editedMaterial = NULL;
	}

	rtr->curScenePtr->texManager.logTextures();

	rtr->curScenePtr->mats[matnum]->logMaterial();

	rtr->curScenePtr->updateSkyportals();
	if (!rtr->curScenePtr->updateLightMeshes())
	{
		if (!rtr->curScenePtr->sscene.useSunSky)
			MessageBox(hMain, "Warning!\nNo emitter nor sun/sky on scene!", "Warning", 0);
	}

	fillMaterialsImageList();
	updateMaterialPreviewFullSize(matnum);
	fillSingleMaterialData();

	Logger::add("RENDERER MODE");
}

//----------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::updateLightMeshes()	// never used ?????  deeeel
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->createLightMeshes();

	return true;
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillMaterialsList()
{
	Raytracer * rtr = Raytracer::getInstance();
	HWND hList = GetDlgItem(hMaterials, IDC_REND_MATERIALS_LISTVIEW);
	EMListView * emlv = GetEMListViewInstance(hList);
	if (!emlv)
		return;

	char buf[64];

	for (int i=0; i<emlv->data.objCount; i++)
		if (emlv->data[i])
			for (int j=0; j<emlv->numCols; j++)
				if (emlv->data[i][j])
				{
					free(emlv->data[i][j]);
					emlv->data[i][j] = NULL;
				}
	emlv->data.freeList();
	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		emlv->addEmptyRow();

		sprintf_s(buf, 64, "%d", rtr->curScenePtr->mats[i]->id);
		emlv->setData(0, i, buf);

		if (rtr->curScenePtr->mats[i]->name)
			emlv->setData(1, i, rtr->curScenePtr->mats[i]->name);
		else
		{
			sprintf_s(buf, 64, "Unnamed %d", i+1);
			emlv->setData(1, i, buf);
		}

		sprintf_s(buf, 64, "%d", rtr->curScenePtr->mats[i]->layers.objCount);
		emlv->setData(2, i, buf);

		MaterialNox * mat = rtr->curScenePtr->mats[i];
		bool emiss = false;
		for (int j=0; j<mat->layers.objCount; j++)
			if (mat->layers[j].type == MatLayer::TYPE_EMITTER)
				emiss = true;
		if (emiss)
			emlv->setData(3, i, "yes");
		else
			emlv->setData(3, i, "no");

	}

	InvalidateRect(hList, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillMaterialsImageList()
{
	Raytracer * rtr = Raytracer::getInstance();
	int r = MAT_PREV_LIST_IMG_RES;

	HWND hImList = GetDlgItem(hMaterials, IDC_REND_MATERIALS_IMAGE_LIST);
	EMImageList * emil = GetEMImageListInstance(hImList);
	if (!emil)
		return;

	// del old stuff
	for (int i=0; i<emil->imgNames1.objCount; i++)
	{
		if (emil->imgNames1[i])
			free(emil->imgNames1[i]);
		emil->imgNames1[i] = NULL;
	}
	for (int i=0; i<emil->imgNames2.objCount; i++)
	{
		if (emil->imgNames2[i])
			free(emil->imgNames2[i]);
		emil->imgNames2[i] = NULL;
	}
	for (int i=0; i<emil->imgBuffers.objCount; i++)
	{
		if (emil->imgBuffers[i])
			free(emil->imgBuffers[i]);
		emil->imgBuffers[i] = NULL;
	}
	emil->imgBuffers.freeList();
	emil->imgBuffers.createArray();
	emil->imgNames1.freeList();
	emil->imgNames1.createArray();
	emil->imgNames2.freeList();
	emil->imgNames2.createArray();


	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		MaterialNox * mat = rtr->curScenePtr->mats[i];
		char * name = mat->name;
		if (name)
			emil->imgNames1.add(copyString(name));
		else
		{
			char buf[64];
			sprintf_s(buf, 64, "Unnamed %d", i+1);
			emil->imgNames1.add(copyString(buf));
		}

		emil->imgNames2.add(NULL);

		if (mat->preview  &&  mat->preview->imgBuf  &&  mat->preview->width>0  &&  mat->preview->height>0)
		{
			double zoom = min((double)emil->img_width/(double)mat->preview->width, (double)emil->img_height/(double)mat->preview->height);
			ImageByteBuffer * tempBuf = mat->preview->copyWithZoom(zoom);
			if (tempBuf  &&  tempBuf->imgBuf)
			{
				COLORREF * newbuf = (COLORREF*)malloc(sizeof(COLORREF)*emil->img_width*emil->img_height);
				if (newbuf)
				{
					int xx = min(tempBuf->width, emil->img_width);
					int yy = min(tempBuf->height, emil->img_height);
					for (int y=0; y<yy; y++)
						for (int x=0; x<xx; x++)
						{
							COLORREF srccol = tempBuf->imgBuf[tempBuf->height-y-1][x];
							newbuf[emil->img_width*y+x] =  ((srccol&0xff0000)>>16) | ((srccol&0x00ff00)) | ((srccol&0x0000ff)<<16);		// swap red and blue
						}
					emil->imgBuffers.add(newbuf);
				}
				else
					emil->imgBuffers.add(NULL);
			}
			else
				emil->imgBuffers.add(NULL);
			if (tempBuf)
			{
				tempBuf->freeBuffer();
				delete tempBuf;
			}
		}
		else
			emil->imgBuffers.add(NULL);
	}

	emil->imgNames1.createArray();
	emil->imgNames2.createArray();
	emil->imgBuffers.createArray();

	InvalidateRect(hImList, NULL, false);

	RECT erect;
	GetClientRect(emil->hwnd, &erect);
	int swork = emil->getWorkspaceLength();
	int sscreen = erect.bottom;

	EMScrollBar * emsb = GetEMScrollBarInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_PREVIEW_IMLIST_SCROLL));
	emsb->size_work = swork;
	emsb->size_screen = sscreen;
	emsb->updateSize();
	emsb->updateRegions();
	InvalidateRect(emsb->hwnd, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillSingleMaterialData()
{
	int id = -1;
	EMImageList * emil = GetEMImageListInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_IMAGE_LIST));
	if (emil)
		id = emil->imageSelected;

	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * mat = NULL;
	if (id > -1)
		mat = rtr->curScenePtr->mats[id];

	if(!hMaterials)
		return;

	EMText * emName =		GetEMTextInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_NAME));
	EMText * emLayers =		GetEMTextInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_LAYERS_COUNT));
	EMText * emEmission =	GetEMTextInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_EMISSION));
	EMText * emBlend =		GetEMTextInstance(GetDlgItem(hMaterials, IDC_REND_MATERIALS_BLEND));

	if (mat)
	{
		char buf[64];
		emName->changeCaption(mat->name);
		sprintf_s(buf, 64, "%d", mat->layers.objCount);
		emLayers->changeCaption(buf);
		bool emm = false;
		for (int i=0; i<mat->layers.objCount; i++)
			if (mat->layers[i].type == MatLayer::TYPE_EMITTER)
				emm = true;
		sprintf_s(buf, 64, "%s", emm ? "yes" : "no");
		emEmission->changeCaption(buf);

		if (mat->blendIndex>=0  &&  mat->blendIndex<16)
			emBlend->changeCaption(	rtr->curScenePtr->blendSettings.names[mat->blendIndex] );
		else
		{
			sprintf_s(buf, 64, "%d", mat->blendIndex+1);
			emBlend->changeCaption(buf);
		}
	}
	else
	{
		emName->changeCaption("");
		emLayers->changeCaption("");
		emEmission->changeCaption("");
		emBlend->changeCaption("");
	}

}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::updateMaterialPreviewFullSize(int matnum)
{
	HWND hPr = GetDlgItem(hMaterials, IDC_REND_MATERIALS_PREVIEW);
	EMPView * empv = GetEMPViewInstance(hPr);
	Raytracer * rtr = Raytracer::getInstance();

	if (empv->byteBuff)
	{
		empv->byteBuff->freeBuffer();
		delete empv->byteBuff;
		empv->byteBuff = NULL;
	}

	if (matnum<0   ||   matnum>=rtr->curScenePtr->mats.objCount)
	{
		InvalidateRect(hPr, NULL, false);
		return;
	}

	MaterialNox * mat = rtr->curScenePtr->mats[matnum];
	if (!mat   ||   !mat->preview   ||   !mat->preview->imgBuf)
	{
		InvalidateRect(hPr, NULL, false);
		return;
	}

	empv->byteBuff = mat->preview->copy();
	InvalidateRect(hPr, NULL, false);

}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::updateMaterialPreview(int matnum)
{
	HWND hPr = GetDlgItem(hMaterials, IDC_REND_MATERIALS_PREVIEW);
	EMPView * empv = GetEMPViewInstance(hPr);
	Raytracer * rtr = Raytracer::getInstance();

	if (matnum < 0  ||  matnum>rtr->curScenePtr->mats.objCount)
	{
		if (empv->byteBuff)
		{
			empv->byteBuff->freeBuffer();
			delete empv->byteBuff;
		}
		empv->byteBuff = NULL;
		InvalidateRect(hPr, NULL, false);
		return;
	}

	MaterialNox * mat = rtr->curScenePtr->mats[matnum];

	if (!mat   ||   !mat->preview   ||   !mat->preview->imgBuf)
	{
		if (empv->byteBuff)
		{
			empv->byteBuff->freeBuffer();
			delete empv->byteBuff;
		}
		empv->byteBuff = NULL;
		InvalidateRect(hPr, NULL, false);
		return;
	}

	{
		RECT rect;
		GetClientRect(hPr, &rect);
		int w = (rect.right-rect.left);
		int h = (rect.bottom-rect.top);
		if (w<1 || h<1)
			return;
		float z1,z2;
		z1 = (float) w /  mat->preview->width;
		z2 = (float) h /  mat->preview->height;
		z1 = min(z1,z2);
		if (empv->byteBuff)
		{
			empv->byteBuff->freeBuffer();
			delete empv->byteBuff;
		}
		empv->byteBuff = mat->preview->copyWithZoom(z1);
		InvalidateRect(hPr, NULL, false);
	}
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillObjectsList()
{
	Raytracer * rtr = Raytracer::getInstance();
	HWND hList = GetDlgItem(hObjects, IDC_REND_OBJECTS_LISTVIEW);
	EMListView * emlv = GetEMListViewInstance(hList);
	if (!emlv)
	{
		return;
	}

	char buf[64];

	emlv->data.freeList();
	for (int i=0; i<rtr->curScenePtr->meshes.objCount; i++)
	{
		emlv->addEmptyRow();

		sprintf_s(buf, 64, "%d", i);
		emlv->setData(0, i, buf);

		if (rtr->curScenePtr->meshes[i].name)
		{
			emlv->setData(1, i, rtr->curScenePtr->meshes[i].name);
		}
		else
		{
			sprintf_s(buf, 64, "Unnamed %d", i+1);
			emlv->setData(1, i, buf);
		}

		sprintf_s(buf, 64, "%d", rtr->curScenePtr->meshes[i].lastTri - rtr->curScenePtr->meshes[i].firstTri + 1);
		emlv->setData(2, i, buf);
	}

	InvalidateRect(hList, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillInstancesList()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	HWND hList = GetDlgItem(hObjects, IDC_REND_OBJECTS_INSTANCES_LIST);
	EMListView * emlv = GetEMListViewInstance(hList);
	if (!emlv)
	{
		return;
	}

	char buf[64];

	emlv->data.freeList();
	for (int i=0; i<sc->instances.objCount; i++)
	{
		emlv->addEmptyRow();

		if (sc->instances[i].name)
			emlv->setData(0, i, sc->instances[i].name);
		else
		{
			sprintf_s(buf, 64, "Unnamed %d", i+1);
			emlv->setData(0, i, buf);
		}

		int j = sc->instances[i].meshID;
		if (j<sc->meshes.objCount && sc->meshes[j].name)
			emlv->setData(1, i, sc->meshes[j].name);
		else
		{
			sprintf_s(buf, 64, "Unnamed %d", i+1);
			emlv->setData(1, i, buf);
		}
	}

	InvalidateRect(hList, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillCameraList()
{
	Raytracer * rtr = Raytracer::getInstance();
	HWND hList = GetDlgItem(hCamera, IDC_REND_CAMERA_LISTVIEW);
	EMListView * emlv = GetEMListViewInstance(hList);
	if (!emlv)
		return;

	char buf[64];

	emlv->data.freeList();
	for (int i=0; i<rtr->curScenePtr->cameras.objCount; i++)
	{
		emlv->addEmptyRow();

		sprintf_s(buf, 64, "%d", i);
		emlv->setData(0, i, buf);

		if (rtr->curScenePtr->cameras[i]->name)
			emlv->setData(1, i, rtr->curScenePtr->cameras[i]->name);
		else
		{
			sprintf_s(buf, 64, "Unnamed %d", i+1);
			emlv->setData(1, i, buf);
		}

		if (rtr->curScenePtr->sscene.activeCamera == i)
			emlv->setData(2, i, "yes");
		else
			emlv->setData(2, i, "no");
	}

	InvalidateRect(hList, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillCameraPostData(Camera * cam)
{
	if (!cam)
		cam = Raytracer::getInstance()->curScenePtr->cameras[ Raytracer::getInstance()->curScenePtr->sscene.activeCamera ];
	if (!cam)
		return;

	HWND hISO, hGIEV, hTone, hGamma, hBri, hCon, hSat, hRed, hBlu, hGre, hTem, hVgn, hRsp, hCRed, hCGre, hCBlu, hCLum, hCurve, hFltr, hHot;
	hISO		= GetDlgItem(hPost, IDC_REND_TONE_ISO);
	hGIEV		= GetDlgItem(hPost, IDC_REND_POST_GI_COMPENSATE);
	hTone		= GetDlgItem(hPost, IDC_REND_TONE_TONE);
	hGamma		= GetDlgItem(hPost, IDC_REND_TONE_GAMMA);
	hBri		= GetDlgItem(hPost, IDC_REND_POST_BRIGHTNESS);
	hCon		= GetDlgItem(hPost, IDC_REND_POST_CONTRAST);
	hSat		= GetDlgItem(hPost, IDC_REND_POST_SATURATION);
	hRed		= GetDlgItem(hPost, IDC_REND_POST_RED);
	hGre		= GetDlgItem(hPost, IDC_REND_POST_GREEN);
	hBlu		= GetDlgItem(hPost, IDC_REND_POST_BLUE);
	hTem		= GetDlgItem(hPost, IDC_REND_POST_TEMPERATURE);
	hVgn		= GetDlgItem(hPost, IDC_REND_POST_VIGNETTE);
	hRsp		= GetDlgItem(hPost, IDC_REND_POST_RESPONSE);
	hCRed		= GetDlgItem(hPost, IDC_REND_POST_CURVE_RED);
	hCGre		= GetDlgItem(hPost, IDC_REND_POST_CURVE_GREEN);
	hCBlu		= GetDlgItem(hPost, IDC_REND_POST_CURVE_BLUE);
	hCLum		= GetDlgItem(hPost, IDC_REND_POST_CURVE_LIGHTNESS);
	hCurve		= GetDlgItem(hPost, IDC_REND_POST_CURVE);
	hFltr		= GetDlgItem(hPost, IDC_REND_POST_FILTER);
	hHot		= GetDlgItem(hPost, IDC_REND_POST_HOT_PIXELS_ON);

	EMEditTrackBar *	emesISO		= GetEMEditTrackBarInstance(hISO);
	EMEditTrackBar *	emesGIEV	= GetEMEditTrackBarInstance(hGIEV);
	EMEditTrackBar *	emesTone	= GetEMEditTrackBarInstance(hTone);
	EMEditTrackBar *	emesGamma	= GetEMEditTrackBarInstance(hGamma);
	EMEditTrackBar *	emesBri		= GetEMEditTrackBarInstance(hBri);
	EMEditTrackBar *	emesCon		= GetEMEditTrackBarInstance(hCon);
	EMEditTrackBar *	emesSat		= GetEMEditTrackBarInstance(hSat);
	EMEditTrackBar *	emesRed		= GetEMEditTrackBarInstance(hRed);
	EMEditTrackBar *	emesGre		= GetEMEditTrackBarInstance(hGre);
	EMEditTrackBar *	emesBlu		= GetEMEditTrackBarInstance(hBlu);
	EMEditTrackBar *	emesTem		= GetEMEditTrackBarInstance(hTem);
	EMEditTrackBar *	emesVgn		= GetEMEditTrackBarInstance(hVgn);
	EMCheckBox *		emesCRed	= GetEMCheckBoxInstance(hCRed);
	EMCheckBox *		emesCGre	= GetEMCheckBoxInstance(hCGre);
	EMCheckBox *		emesCBlu	= GetEMCheckBoxInstance(hCBlu);
	EMCheckBox *		emesCLum	= GetEMCheckBoxInstance(hCLum);
	EMCurve *			emCurve		= GetEMCurveInstance(hCurve);
	EMComboBox *		emFilter	= GetEMComboBoxInstance(hFltr);
	EMComboBox *		emRsp		= GetEMComboBoxInstance(hRsp);
	EMCheckBox *		emcbHot		= GetEMCheckBoxInstance(hHot);

	emRsp->selected = cam->iMod.getResponseFunctionNumber();
	InvalidateRect(emRsp->hwnd, NULL, false);
	emcbHot->selected = cam->iMod.getDotsEnabled();
	InvalidateRect(emcbHot->hwnd, NULL, false);


	emesISO->setValuesToFloat(cam->iMod.getISO_EV_compensation(), NOX_POST_EV_MIN, NOX_POST_EV_MAX, 0.5f);
	emesTone->setValuesToFloat(cam->iMod.getToneMappingValue(), NOX_POST_TONE_MIN, NOX_POST_TONE_MAX, 0.01f);
	emesGamma->setValuesToFloat(cam->iMod.getGamma(), NOX_POST_GAMMA_MIN, NOX_POST_GAMMA_MAX, 0.1f);
	emesBri->setValuesToFloat(cam->iMod.getBrightness(), -1, 1, 0.05f);
	emesCon->setValuesToFloat(cam->iMod.getContrast(), 0, 2, 0.05f);
	emesSat->setValuesToFloat(cam->iMod.getSaturation(), -1, 1, 0.05f);
	emesRed->setValuesToFloat(cam->iMod.getR(), -1, 1, 0.05f);
	emesGre->setValuesToFloat(cam->iMod.getG(), -1, 1, 0.05f);
	emesBlu->setValuesToFloat(cam->iMod.getB(), -1, 1, 0.05f);
	emesTem->setValuesToInt((int)cam->iMod.getTemperature(), 1000, 11000, 10);
	emesVgn->setValuesToInt(cam->iMod.getVignette(), NOX_POST_VIGNETTE_MIN, NOX_POST_VIGNETTE_MAX, 10);
	emesGIEV->setValuesToFloat(cam->iMod.getGICompensation(), NOX_POST_EV_MIN, NOX_POST_EV_MAX, 0.5f);

	emFilter->selected = cam->iMod.getFilter();
	InvalidateRect(hFltr, NULL, false);

	bool cr,cg,cb,cl;
	cam->iMod.getCurveCheckboxes(cr,cg,cb,cl);
	emesCRed->selected = cr;
	emesCGre->selected = cg;
	emesCBlu->selected = cb;
	emesCLum->selected = cl;
	InvalidateRect(hCRed, NULL, false);
	InvalidateRect(hCGre, NULL, false);
	InvalidateRect(hCBlu, NULL, false);
	InvalidateRect(hCLum, NULL, false);

	emCurve->reset();
	for (int i=0; i<cam->iMod.getCurveRedPointsCount(); i++)
	{
		EMCurve::P2D p;
		cam->iMod.getCurveRedPoint(i, p.x, p.y);
		emCurve->pointsR.push_back(p);
	}

	for (int i=0; i<cam->iMod.getCurveGreenPointsCount(); i++)
	{
		EMCurve::P2D p;
		cam->iMod.getCurveGreenPoint(i, p.x, p.y);
		emCurve->pointsG.push_back(p);
	}

	for (int i=0; i<cam->iMod.getCurveBluePointsCount(); i++)
	{
		EMCurve::P2D p;
		cam->iMod.getCurveBluePoint(i, p.x, p.y);
		emCurve->pointsB.push_back(p);
	}

	for (int i=0; i<cam->iMod.getCurveLuminancePointsCount(); i++)
	{
		EMCurve::P2D p;
		cam->iMod.getCurveLuminancePoint(i, p.x, p.y);
		emCurve->pointsL.push_back(p);
	}

	emCurve->activeRGB[0] = cr;
	emCurve->activeRGB[1] = cg;
	emCurve->activeRGB[2] = cb;
	emCurve->activeRGB[3] = cl;
	emCurve->isLuminocityMode = cl;


	emCurve->createLines();
	InvalidateRect(hCurve, NULL, false);

	EMPView * empv = GetEMPViewInstance(hImage);
	empv->imgMod->copyDataFromAnother(&(cam->iMod));

}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillCameraData(Camera * cam)
{
	if (!cam)
		cam = Raytracer::getInstance()->curScenePtr->cameras[ Raytracer::getInstance()->curScenePtr->sscene.activeCamera ];
	if (!cam)
		return;

	HWND hPosX = GetDlgItem(hCamera, IDC_REND_CAMERA_POS_X);
	HWND hPosY = GetDlgItem(hCamera, IDC_REND_CAMERA_POS_Y);
	HWND hPosZ = GetDlgItem(hCamera, IDC_REND_CAMERA_POS_Z);
	HWND hDirX = GetDlgItem(hCamera, IDC_REND_CAMERA_DIR_X);
	HWND hDirY = GetDlgItem(hCamera, IDC_REND_CAMERA_DIR_Y);
	HWND hDirZ = GetDlgItem(hCamera, IDC_REND_CAMERA_DIR_Z);
	HWND hUpX  = GetDlgItem(hCamera, IDC_REND_CAMERA_UP_X);
	HWND hUpY  = GetDlgItem(hCamera, IDC_REND_CAMERA_UP_Y);
	HWND hUpZ  = GetDlgItem(hCamera, IDC_REND_CAMERA_UP_Z);

	char buf[64];
	sprintf_s(buf, 64, "%.3f", cam->position.x);
	SetWindowText(hPosX, buf);
	sprintf_s(buf, 64, "%.3f", cam->position.y);
	SetWindowText(hPosY, buf);
	sprintf_s(buf, 64, "%.3f", cam->position.z);
	SetWindowText(hPosZ, buf);
	sprintf_s(buf, 64, "%.3f", cam->direction.x);
	SetWindowText(hDirX, buf);
	sprintf_s(buf, 64, "%.3f", cam->direction.y);
	SetWindowText(hDirY, buf);
	sprintf_s(buf, 64, "%.3f", cam->direction.z);
	SetWindowText(hDirZ, buf);
	sprintf_s(buf, 64, "%.3f", cam->upDir.x);
	SetWindowText(hUpX, buf);
	sprintf_s(buf, 64, "%.3f", cam->upDir.y);
	SetWindowText(hUpY, buf);
	sprintf_s(buf, 64, "%.3f", cam->upDir.z);
	SetWindowText(hUpZ, buf);

	HWND hISO			= GetDlgItem(hCamera, IDC_REND_CAMERA_ISO);
	HWND hAperture		= GetDlgItem(hCamera, IDC_REND_CAMERA_APERTURE);
	HWND hApertureCopy	= GetDlgItem(hCamera, IDC_REND_CAMERA_APERTURE_COPY);
	HWND hShutter		= GetDlgItem(hCamera, IDC_REND_CAMERA_SHUTTER);
	HWND hFocusDist		= GetDlgItem(hCamera, IDC_REND_CAMERA_FOCUS_DIST);
	HWND hFocal			= GetDlgItem(hCamera, IDC_REND_CAMERA_FOCAL_LENGTH);
	HWND hFovDiag		= GetDlgItem(hCamera, IDC_REND_CAMERA_FOV_DIAG);
	HWND hFovHori		= GetDlgItem(hCamera, IDC_REND_CAMERA_FOV_HORI);
	HWND hFovVert		= GetDlgItem(hCamera, IDC_REND_CAMERA_FOV_VERT);
	HWND hBlAngle		= GetDlgItem(hCamera, IDC_REND_CAMERA_ANGLEBLADES);
	HWND hBlNum			= GetDlgItem(hCamera, IDC_REND_CAMERA_NUMBLADES);
	HWND hBlRound		= GetDlgItem(hCamera, IDC_REND_CAMERA_BLADES_ROUND);
	HWND hBlRadius		= GetDlgItem(hCamera, IDC_REND_CAMERA_BLADERADIUS);
	HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
	HWND hRingBalance	= GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_RING_BALANCE);
	HWND hRingSize		= GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_RING_SIZE);
	HWND hFlatten		= GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_FLATTEN);
	HWND hVignette		= GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_VIGNETTE);
	HWND hDOF			= GetDlgItem(hCamera, IDC_REND_CAMERA_DOF_TEMP);
	HWND hMBON			= GetDlgItem(hCamera, IDC_REND_CAMERA_MB_ENABLED);
	HWND hMBTime		= GetDlgItem(hCamera, IDC_REND_CAMERA_MB_TIME);

	EMEditSpin * emISO			= GetEMEditSpinInstance(hISO);
	EMEditSpin * emAperture		= GetEMEditSpinInstance(hAperture);
	EMEditSpin * emApertureCopy	= GetEMEditSpinInstance(hApertureCopy);
	EMEditSpin * emShutter		= GetEMEditSpinInstance(hShutter);
	EMEditSpin * emFocusDist	= GetEMEditSpinInstance(hFocusDist);
	EMEditSpin * emFocal		= GetEMEditSpinInstance(hFocal);
	EMEditSpin * emBlNum		= GetEMEditSpinInstance(hBlNum);
	EMEditSpin * emBlAngle		= GetEMEditSpinInstance(hBlAngle);
	EMEditSpin * emBlRadius		= GetEMEditSpinInstance(hBlRadius);
	EMCheckBox * emBlRound		= GetEMCheckBoxInstance(hBlRound);
	EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
	EMEditSpin * emRingBalance	= GetEMEditSpinInstance(hRingBalance);
	EMEditSpin * emRingSize		= GetEMEditSpinInstance(hRingSize);
	EMEditSpin * emFlatten		= GetEMEditSpinInstance(hFlatten);
	EMEditSpin * emVignette		= GetEMEditSpinInstance(hVignette);
	EMCheckBox * emDOF			= GetEMCheckBoxInstance(hDOF);
	EMCheckBox * emMBON			= GetEMCheckBoxInstance(hMBON);
	EMEditSpin * emMBTime		= GetEMEditSpinInstance(hMBTime);

	emMBON->selected = Raytracer::getInstance()->curScenePtr->sscene.motion_blur_on;
	InvalidateRect(emMBON->hwnd, NULL, false);
	emMBTime->setValuesToFloat(Raytracer::getInstance()->curScenePtr->sscene.motion_blur_time, 0, Raytracer::getInstance()->curScenePtr->sscene.motion_blur_max_time, 0.01f, 0.001f);
	emISO->setValuesToInt((int)cam->ISO, NOX_CAM_ISO_MIN, NOX_CAM_ISO_MAX,1, 5);
	emAperture->setValuesToFloat(cam->aperture, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 1, 0.1f);
	emApertureCopy->setValuesToFloat(cam->aperture, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 1, 0.1f);
	emShutter->setValuesToFloat(cam->shutter, NOX_CAM_SHUTTER_MIN, NOX_CAM_SHUTTER_MAX, 10, 1);
	emFocusDist->setValuesToFloat(cam->focusDist, NOX_CAM_FOCUS_MIN, NOX_CAM_FOCUS_MAX, 1, 10);
	float f = 0.5f * 36 / tan(cam->angle * PI / 360.0f);
	setCameraFocalLength(f, true, false);
	emBlNum->setValuesToInt(cam->apBladesNum, NOX_CAM_BLADES_NUM_MIN, NOX_CAM_BLADES_NUM_MAX, 1, 0.02f);
	emBlAngle->setValuesToFloat(cam->apBladesAngle, NOX_CAM_BLADES_ANGLE_MIN, NOX_CAM_BLADES_ANGLE_MAX, 1, 0.02f);
	emBlRadius->setValuesToFloat(cam->apBladesRadius, NOX_CAM_BLADES_RADIUS_MIN, NOX_CAM_BLADES_RADIUS_MAX, 1, 0.02f);
	emBlRound->selected = cam->apBladesRound;
	InvalidateRect(emBlRound->hwnd, NULL, false);
	cam->createApertureShapePreview(emDiaph->byteBuff);
	InvalidateRect(emDiaph->hwnd, NULL, false);
	emRingBalance->setValuesToInt(cam->diaphSampler.intBokehRingBalance, NOX_CAM_BOKEH_BALANCE_MIN, NOX_CAM_BOKEH_BALANCE_MAX, 1, 0.1f);
	emRingSize->setValuesToInt(cam->diaphSampler.intBokehRingSize, NOX_CAM_BOKEH_RING_SIZE_MIN, NOX_CAM_BOKEH_RING_SIZE_MAX, 1, 0.1f);
	emFlatten->setValuesToInt(cam->diaphSampler.intBokehFlatten, NOX_CAM_BOKEH_FLATTEN_MIN, NOX_CAM_BOKEH_FLATTEN_MAX, 1, 0.1f);
	emVignette->setValuesToInt(cam->diaphSampler.intBokehVignette, NOX_CAM_BOKEH_VIGNETTE_MIN, NOX_CAM_BOKEH_VIGNETTE_MAX, 1, 0.1f);
	emDOF->selected = cam->dofOnTemp;
	InvalidateRect(emDOF->hwnd, NULL, false);
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::notifyProgressBarCallback(char * message, float progress)
{
	EMProgressBar * empb = GetEMProgressBarInstance(hProgress1);
	empb->setPos(progress);
	UpdateWindow(hProgress1);
	if (message)
	{
		EMStatusBar * emsb = GetEMStatusBarInstance(hStatus1);
		if (emsb)
			emsb->setCaption(message);
		UpdateWindow(hStatus1);
	}
}

//----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::updateEngineSettingsBeforeRender()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
}

//-----------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::updateDimensionsFromActiveCamera()
{
	Camera * ccam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
	if (!ccam)
		return;

	if (ccam->imgBuff   &&   ccam->imgBuff->imgBuf   &&   ccam->imgBuff->hitsBuf)
	{
		if (ccam->aa < 1)
			ccam->aa = 1;
		EMEditSpin * emesw = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_WIDTH));
		EMEditSpin * emesh = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_HEIGHT));
		EMEditSpin * emesa = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_AA));
		emesw->setValuesToInt(ccam->imgBuff->width/ccam->aa,  1, NOX_IMAGE_WIDTH_MAX, 1, 1);
		emesh->setValuesToInt(ccam->imgBuff->height/ccam->aa, 1, NOX_IMAGE_HEIGHT_MAX, 1, 1);
		emesa->setValuesToInt(ccam->aa, NOX_IMAGE_AA_MIN, NOX_IMAGE_AA_MAX, 1, 1);
	}
	else
	{
		EMEditSpin * emesw = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_WIDTH));
		EMEditSpin * emesh = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_HEIGHT));
		emesw->setValuesToInt(ccam->width,  1, NOX_IMAGE_WIDTH_MAX, 1, 1);
		emesh->setValuesToInt(ccam->height, 1, NOX_IMAGE_HEIGHT_MAX, 1, 1);
	}
}

//-----------------------------------------------------------------------------------------------------------------------

Camera * RendererMainWindow::getSelectedCamera()
{
	HWND hList = GetDlgItem(hCamera, IDC_REND_CAMERA_LISTVIEW);
	EMListView * emlv = GetEMListViewInstance(hList);
	if (!emlv)
		return NULL;
	int sel = emlv->selected;
	Raytracer * rtr = Raytracer::getInstance();
	if (sel >= rtr->curScenePtr->cameras.objCount)
		return NULL;
	Camera * cam = rtr->curScenePtr->cameras[sel];
	return cam;
}

//-----------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::setCameraFocalLength(float focal, bool changeSpinner, bool changeInCamera)
{
	EMEditSpin * emes1 = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_WIDTH));
	EMEditSpin * emes2 = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_HEIGHT));
	int ww = emes1->intValue;
	int hh = emes2->intValue;
	float ssh = 18.0f*hh/ww;
	float ssd = sqrt(ssh*ssh + 18.0f*18.0f);

	float f = focal;
	char buf[64];
	sprintf_s(buf, 64, "%.2f�", (360.0f/PI*atan(18.0f / f)));
	SetWindowText(GetDlgItem(hCamera, IDC_REND_CAMERA_FOV_HORI), buf);
	sprintf_s(buf, 64, "%.2f�", (360.0f/PI*atan(ssh / f)));
	SetWindowText(GetDlgItem(hCamera, IDC_REND_CAMERA_FOV_VERT), buf);
	sprintf_s(buf, 64, "%.2f�", (360.0f/PI*atan(ssd / f)));
	SetWindowText(GetDlgItem(hCamera, IDC_REND_CAMERA_FOV_DIAG), buf);

	if (changeSpinner)
	{
		HWND hFocal = GetDlgItem(hCamera, IDC_REND_CAMERA_FOCAL_LENGTH);
		EMEditSpin * eFocal = GetEMEditSpinInstance(hFocal);
		eFocal->setValuesToFloat(focal, NOX_CAM_FOCAL_MIN, NOX_CAM_FOCAL_MAX, 1, 1);
	}

	HWND hList = GetDlgItem(hCamera, IDC_REND_CAMERA_LISTVIEW);
	EMListView * emlv = GetEMListViewInstance(hList);
	if (!emlv)
		return;
	int sel = emlv->selected;
	Raytracer * rtr = Raytracer::getInstance();
	if (sel >= rtr->curScenePtr->cameras.objCount)
		return;
	if (changeInCamera)
	{
		Camera * cam = rtr->curScenePtr->cameras[sel];
		cam->setLengthHori(focal);
	}
}

//-----------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::startStoppingTimer()
{
	EMCheckBox * emcb  = GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_ON));
	EMEditSpin * emesh = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_HH));
	EMEditSpin * emesm = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_MM));
	EMEditSpin * emess = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_SS));
	unsigned int tt;
	tt = 500+1000*( emess->intValue + 60*(emesm->intValue + 60*emesh->intValue ) );
	tt = max(tt, 5000);
	SetTimer(hOutputConf, TIMER_STOP_ID, tt, NULL);
	if (emcb->selected)
		stoppingTimerOn = true;
	else
		stoppingTimerOn = false;
}

//-----------------------------------------------------------------------------------------------------------------------

bool RendererMainWindow::refreshRenderedImage(RedrawRegion * rgn)
{
	EMPView * empv = GetEMPViewInstance(hImage);
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(rtr->curScenePtr);
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	CHECK(cam);
	static bool nowRefreshing = false;
	if(nowRefreshing)
		return false;
	nowRefreshing = true;
	updateRenderedImage(empv, true, false, rgn, false);
	nowRefreshing = false;
	if (!rgn)
	{
		char * tst = createTimeStamp();
		if (tst)
		{
			if (empv->byteBuff)  
			{
				if (empv->byteBuff->timestamp)
					free(empv->byteBuff->timestamp);
				empv->byteBuff->timestamp = tst;
			}
		}
		#ifndef DEBUG_DISABLE_STAMP
			if (showTimeStamp  &&  empv->byteBuff)
				empv->byteBuff->applyTimeStamp();
			if (showLogoStamp  &&  empv->byteBuff)
				empv->byteBuff->applyLogoStamp(&rtr->logoStamp);
		#endif
	}

	InvalidateRect(hImage, NULL, false);

	refreshTimeInStatus();

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

DWORD WINAPI regionRefreshThreadProc(LPVOID lpParameter)
{
	if (!lpParameter)
		return 0;

	EMPView * empv = GetEMPViewInstance(((RedrawRegion *)lpParameter)->hwnd);
	if (!empv)
		return 0;
	updateRenderedImage(empv, true, false, (RedrawRegion *)lpParameter, false);
	InvalidateRect(empv->hwnd, NULL, false);

	return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMainWindow::RefreshThreadProc(LPVOID lpParameter)
{
	clock_t lTime = clock();
	clock_t cTime;
	double diff;

	while (refreshThrRunning)
	{
		Sleep(200);
		cTime = clock();
		diff  = (cTime - lTime)/(double)CLOCKS_PER_SEC;
		if (diff > refreshTime)
		{
			if (refreshTimeOn)
				refreshRenderedImage();
			lTime = clock();
		}
	}

	return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

DWORD WINAPI RendererMainWindow::CounterRefreshThreadProc(LPVOID lpParameter)
{
	Raytracer * rtr = Raytracer::getInstance();
	while (rtr->curScenePtr->nowRendering)
	{
		refreshCounterInStatus();
		Sleep(1000);
	}
	return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::runRefreshThread()
{
	refreshThrRunning = true;
	DWORD threadID, threadIDc;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(RefreshThreadProc), (LPVOID)0, 0, &threadID);
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(CounterRefreshThreadProc), (LPVOID)0, 0, &threadIDc);
}

//-----------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::updateTimersFromGUI()
{
	EMCheckBox * emcbStopOn				= GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_ON));
	EMCheckBox * emcbRefreshOn			= GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_REFRESH_ON));
	EMCheckBox * emcbAutoSaveOn			= GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_AUTOSAVE_ON));
	EMEditSpin * emesStopHours			= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_HH));
	EMEditSpin * emesStopMinutes		= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_MM));
	EMEditSpin * emesStopSeconds		= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_SS));
	EMEditSpin * emesRefreshSeconds		= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_REFRESH_SECONDS));
	EMEditSpin * emesAutoSaveMinutes	= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_AUTOSAVE_MINUTES));

	stoppingTimerOn			= emcbStopOn->selected;
	autosaveTimerOn			= emcbAutoSaveOn->selected;
	refreshTimeOn			= emcbRefreshOn->selected;
	refreshTime				= emesRefreshSeconds->intValue;
	stopTimerHours			= emesStopHours->intValue;
	stopTimerMinutes		= emesStopMinutes->intValue;
	stopTimerSeconds		= emesStopSeconds->intValue;
	autosaveTimerMinutes	= emesAutoSaveMinutes->intValue;

	Raytracer * rtr = Raytracer::getInstance();
	rtr->gui_timers.stoppingTimerOn			= stoppingTimerOn;
	rtr->gui_timers.autosaveTimerOn			= autosaveTimerOn;
	rtr->gui_timers.refreshTimeOn			= refreshTimeOn;
	rtr->gui_timers.refreshTime				= refreshTime;
	rtr->gui_timers.stopTimerHours			= stopTimerHours;
	rtr->gui_timers.stopTimerMinutes		= stopTimerMinutes;
	rtr->gui_timers.stopTimerSeconds		= stopTimerSeconds;
	rtr->gui_timers.autosaveTimerMinutes	= autosaveTimerMinutes;

	rtr->gui_timers.rn_active			= runNext.active;
	rtr->gui_timers.rn_nextCam			= runNext.nextCam;
	rtr->gui_timers.rn_nCamDesc			= runNext.nCamDesc;
	rtr->gui_timers.rn_copyPost			= runNext.copyPost;
	rtr->gui_timers.rn_delBuffs			= runNext.delBuffs;
	rtr->gui_timers.rn_saveImg			= runNext.saveImg;
	rtr->gui_timers.rn_changeSunsky		= runNext.changeSunsky;
	rtr->gui_timers.rn_sunskyMins		= runNext.sunskyMins;
	rtr->gui_timers.rn_fileFormat		= runNext.fileFormat;
	if (rtr->gui_timers.rn_saveFolder)
		free(rtr->gui_timers.rn_saveFolder);
	rtr->gui_timers.rn_saveFolder = copyString(runNext.saveFolder);
}

//-----------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::updateTimersAndRunNext()
{
	Raytracer * rtr = Raytracer::getInstance();
	stoppingTimerOn			= rtr->gui_timers.stoppingTimerOn;
	autosaveTimerOn			= rtr->gui_timers.autosaveTimerOn;
	refreshTimeOn			= rtr->gui_timers.refreshTimeOn;
	refreshTime				= rtr->gui_timers.refreshTime;
	stopTimerHours			= rtr->gui_timers.stopTimerHours;
	stopTimerMinutes		= rtr->gui_timers.stopTimerMinutes;
	stopTimerSeconds		= rtr->gui_timers.stopTimerSeconds;
	autosaveTimerMinutes	= rtr->gui_timers.autosaveTimerMinutes;

	runNext.active			= rtr->gui_timers.rn_active;
	runNext.nextCam			= rtr->gui_timers.rn_nextCam;
	runNext.nCamDesc		= rtr->gui_timers.rn_nCamDesc;
	runNext.copyPost		= rtr->gui_timers.rn_copyPost;
	runNext.delBuffs		= rtr->gui_timers.rn_delBuffs;
	runNext.saveImg			= rtr->gui_timers.rn_saveImg;
	runNext.changeSunsky	= rtr->gui_timers.rn_changeSunsky;
	runNext.sunskyMins		= rtr->gui_timers.rn_sunskyMins;
	runNext.fileFormat		= rtr->gui_timers.rn_fileFormat;

	if (runNext.saveFolder)
		free(runNext.saveFolder);
	runNext.saveFolder = copyString(rtr->gui_timers.rn_saveFolder);
}

//-----------------------------------------------------------------------------------------------------------------------

void rendererOutputShowControls(HWND hWnd, int engine, int sampling);

void RendererMainWindow::fillRenderSettings()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;

	EMComboBox * eEng = GetEMComboBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_ENGINE));
	switch (sc->sscene.giMethod)
	{
		case Raytracer::GI_METHOD_PATH_TRACING: 				eEng->selected = 0;		break;
		case Raytracer::GI_METHOD_BIDIRECTIONAL_PATH_TRACING:	eEng->selected = 1;		break;
		case Raytracer::GI_METHOD_METROPOLIS:					eEng->selected = 2;		break;
		case Raytracer::GI_METHOD_INTERSECTIONS: 				eEng->selected = 3;		break;
		default:					 							eEng->selected = 0;		break;
	}
	EMComboBox * eFill = GetEMComboBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_RANDOM_TYPE));
	switch (sc->sscene.random_type)
	{
		case Camera::RND_TYPE_SCANLINE:		eFill->selected = 0;		break;
		case Camera::RND_TYPE_HALTON:		eFill->selected = 1;		break;
		case Camera::RND_TYPE_MARSAGLIA:	eFill->selected = 2;		break;
		case Camera::RND_TYPE_RAND:			eFill->selected = 3;		break;
		case Camera::RND_TYPE_RAND_S:		eFill->selected = 4;		break;
		case Camera::RND_TYPE_LATTICE:		eFill->selected = 5;		break;
		case Camera::RND_TYPE_BUCKETS: 		eFill->selected = 6;		break;
		default:					 		eFill->selected = 0;		break;
	}
	InvalidateRect(eEng->hwnd, NULL, false);
	InvalidateRect(eFill->hwnd, NULL, false);

	// BUCKETS
	EMEditSpin * emBx  = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_BUCKET_SIZE_X));
	EMEditSpin * emBy  = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_BUCKET_SIZE_Y));
	EMEditSpin * emSpp = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_SAMPLES));
	EMCheckBox * emSAP = GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOP_AFTER_PASS));
	emSAP->selected = sc->sscene.stop_after_pass;
	InvalidateRect(emSAP->hwnd, NULL, false);
	emBx->setValuesToInt(sc->sscene.bucket_size_x, NOX_REND_BUCKET_SIZE_MIN, NOX_REND_BUCKET_SIZE_MAX, 1, 0.1f);
	emBy->setValuesToInt(sc->sscene.bucket_size_y, NOX_REND_BUCKET_SIZE_MIN, NOX_REND_BUCKET_SIZE_MAX, 1, 0.1f);
	emSpp->setValuesToInt(sc->sscene.samples_per_pixel_in_pass, NOX_REND_SAMPLES_MIN, NOX_REND_SAMPLES_MAX, 1, 0.1f);

	// PATH TRACING
	EMEditSpin * emGIS  = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_GI_SAMPLES));
	emGIS->setValuesToInt(sc->sscene.pt_gi_samples, NOX_REND_GI_SAMPLES_MIN, NOX_REND_GI_SAMPLES_MAX, 1, 0.1f);
	EMEditSpin * emRIS  = GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_RIS_SAMPLES));
	emRIS->setValuesToInt(sc->sscene.pt_ris_samples, NOX_REND_RIS_SAMPLES_MIN, NOX_REND_RIS_SAMPLES_MAX, 1, 0.1f);
	EMCheckBox * emFC = GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_FAKE_CAUSTIC));
	EMCheckBox * emD2C = GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_DISCARD_SECONDARY_CAUSTIC));
	emFC->selected = sc->sscene.causticFake;
	emD2C->selected = sc->sscene.disableSecondaryCaustic;
	InvalidateRect(emFC->hwnd, NULL, false);
	InvalidateRect(emD2C->hwnd, NULL, false);
	EMEditTrackBar * emMR = GetEMEditTrackBarInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_CAUSTIC_MAX_ROUGH));
	emMR->setValuesToFloat(sc->sscene.causticMaxRough, NOX_RAYTR_CAUSTIC_ROUGH_MIN, NOX_RAYTR_CAUSTIC_ROUGH_MAX, 1.0f);

	rendererOutputShowControls(hOutputConf, eEng->selected, eFill->selected);

}

//-----------------------------------------------------------------------------------------------------------------------

void RendererMainWindow::fillTimersData()
{
	EMCheckBox * emcbStopOn				= GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_ON));
	EMCheckBox * emcbRefreshOn			= GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_REFRESH_ON));
	EMCheckBox * emcbAutoSaveOn			= GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_AUTOSAVE_ON));
	EMCheckBox * emcbRunNextOn			= GetEMCheckBoxInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_RUN_NEXT_ON));
	EMEditSpin * emesStopHours			= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_HH));
	EMEditSpin * emesStopMinutes		= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_MM));
	EMEditSpin * emesStopSeconds		= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_STOPAFTER_SS));
	EMEditSpin * emesRefreshSeconds		= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_REFRESH_SECONDS));
	EMEditSpin * emesAutoSaveMinutes	= GetEMEditSpinInstance(GetDlgItem(hOutputConf, IDC_REND_OUTPUT_AUTOSAVE_MINUTES));

	emcbStopOn->selected = stoppingTimerOn;
	emcbAutoSaveOn->selected = autosaveTimerOn;
	emcbRefreshOn->selected = refreshTimeOn;
	emcbRunNextOn->selected = runNext.active;
	InvalidateRect(emcbStopOn->hwnd, NULL, false);
	InvalidateRect(emcbAutoSaveOn->hwnd, NULL, false);
	InvalidateRect(emcbRefreshOn->hwnd, NULL, false);
	InvalidateRect(emcbRunNextOn->hwnd, NULL, false);
	emesRefreshSeconds->setValuesToInt(refreshTime, NOX_REND_AUTOREFRESH_TIME_MIN, NOX_REND_AUTOREFRESH_TIME_MAX, 1, 0.1f);
	emesStopHours->setValuesToInt(stopTimerHours, 0, 23, 1, 0.1f);
	emesStopMinutes->setValuesToInt(stopTimerMinutes, 0, 59, 1, 0.1f);
	emesStopSeconds->setValuesToInt(stopTimerSeconds, 0, 59, 1, 0.1f);
	emesAutoSaveMinutes->setValuesToInt(autosaveTimerMinutes, NOX_REND_AUTOSAVE_TIME_MIN,    NOX_REND_AUTOSAVE_TIME_MAX   , 1, 0.1f);
}

//-----------------------------------------------------------------------------------------------------------------------

