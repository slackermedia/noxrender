#include <Windows.h>
#include "dialogs.h"
#include "EM2Controls.h"
#include "resource.h"
#include "noxfonts.h"
#include <tchar.h>
#include "log.h"

// new gui version - not finished

extern HMODULE hDllModule;

HBITMAP loadPNGfromResource(WORD idRes);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
char * analyzeSceneAndGetText();

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK SceneInfo2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;
	static char * memTxt = NULL;

	int mt_marginup = 160;
	int mt_margindown = 60;
	static int mt_height = 100;
	static int mt_pos = 0;
	static int mt_vis = 100;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				SetFocus(hWnd);
				RECT crect;

				GetClientRect(hWnd, &crect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(crect.right, crect.bottom, 0,0, NGCOL_BG_LIGHT, NGCOL_BG_DARK);
				NOXFontManager * fonts = NOXFontManager::getInstance();
				
				EM2Button * embclose = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_SI2_CLOSE));
				embclose->bgImage = hBgBMP;
				embclose->setFont(fonts->em2button, false);
				updateControlBgShift(embclose->hwnd, 0, 0, embclose->bgShiftX, embclose->bgShiftY);

				Raytracer * rtr = Raytracer::getInstance();
				Scene * sc = rtr->curScenePtr;
				char buf[256];


				EM2Text * emttScenename = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXT_SCENE_NAME));
				emttScenename->bgImage = hBgBMP;
				emttScenename->setFont(fonts->em2text, false);
				updateControlBgShift(emttScenename->hwnd, 0, 0, emttScenename->bgShiftX, emttScenename->bgShiftY);

				EM2Text * emtscenename = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_SCENENAME));
				emtscenename->bgImage = hBgBMP;
				emtscenename->setFont(fonts->em2text, false);
				updateControlBgShift(emtscenename->hwnd, 0, 0, emtscenename->bgShiftX, emtscenename->bgShiftY);
				emtscenename->changeCaption(sc->scene_name ? sc->scene_name : "NONE");

				EM2Text * emttAuthor = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXT_AUTHOR));
				emttAuthor->bgImage = hBgBMP;
				emttAuthor->setFont(fonts->em2text, false);
				updateControlBgShift(emttAuthor->hwnd, 0, 0, emttAuthor->bgShiftX, emttAuthor->bgShiftY);

				EM2Text * emtAuthor = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_AUTHOR));
				emtAuthor->bgImage = hBgBMP;
				emtAuthor->setFont(fonts->em2text, false);
				updateControlBgShift(emtAuthor->hwnd, 0, 0, emtAuthor->bgShiftX, emtAuthor->bgShiftY);
				emtAuthor->changeCaption(sc->scene_author_name ? sc->scene_author_name : "NONE");

				EM2Text * emtEmail = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_EMAIL));
				emtEmail->bgImage = hBgBMP;
				emtEmail->setFont(fonts->em2text, false);
				updateControlBgShift(emtEmail->hwnd, 0, 0, emtEmail->bgShiftX, emtEmail->bgShiftY);
				emtEmail->changeCaption(sc->scene_author_email ? sc->scene_author_email : "NONE");

				EM2Text * emtWWW = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_WWW));
				emtWWW->bgImage = hBgBMP;
				emtWWW->setFont(fonts->em2text, false);
				updateControlBgShift(emtWWW->hwnd, 0, 0, emtWWW->bgShiftX, emtWWW->bgShiftY);
				emtWWW->changeCaption(sc->scene_author_www ? sc->scene_author_www : "NONE");

				EM2Text * emttObjects = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXT_OBJECTS));
				emttObjects->bgImage = hBgBMP;
				emttObjects->setFont(fonts->em2text, false);
				updateControlBgShift(emttObjects->hwnd, 0, 0, emttObjects->bgShiftX, emttObjects->bgShiftY);

				EM2Text * emtObjects = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_OBJECTS_COUNT));
				emtObjects->bgImage = hBgBMP;
				emtObjects->setFont(fonts->em2text, false);
				updateControlBgShift(emtObjects->hwnd, 0, 0, emtObjects->bgShiftX, emtObjects->bgShiftY);
				sprintf_s(buf, 256, "%d", sc->meshes.objCount);
				emtObjects->changeCaption(buf);

				EM2Text * emttInstances = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXT_INSTANCES));
				emttInstances->bgImage = hBgBMP;
				emttInstances->setFont(fonts->em2text, false);
				updateControlBgShift(emttInstances->hwnd, 0, 0, emttInstances->bgShiftX, emttInstances->bgShiftY);

				EM2Text * emtInstances = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_INSTANCES_COUNT));
				emtInstances->bgImage = hBgBMP;
				emtInstances->setFont(fonts->em2text, false);
				updateControlBgShift(emtInstances->hwnd, 0, 0, emtInstances->bgShiftX, emtInstances->bgShiftY);
				sprintf_s(buf, 256, "%d", sc->instances.objCount);
				emtInstances->changeCaption(buf);

				EM2Text * emttTriangles = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXT_TRIANGLES));
				emttTriangles->bgImage = hBgBMP;
				emttTriangles->setFont(fonts->em2text, false);
				updateControlBgShift(emttTriangles->hwnd, 0, 0, emttTriangles->bgShiftX, emttTriangles->bgShiftY);

				EM2Text * emtTriangles = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TRIANGLES_COUNT));
				emtTriangles->bgImage = hBgBMP;
				emtTriangles->setFont(fonts->em2text, false);
				updateControlBgShift(emtTriangles->hwnd, 0, 0, emtTriangles->bgShiftX, emtTriangles->bgShiftY);
				sprintf_s(buf, 256, "%d", sc->triangles.objCount);
				emtTriangles->changeCaption(buf);

				EM2Text * emttMaterials = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXT_MATERIALS));
				emttMaterials->bgImage = hBgBMP;
				emttMaterials->setFont(fonts->em2text, false);
				updateControlBgShift(emttMaterials->hwnd, 0, 0, emttMaterials->bgShiftX, emttMaterials->bgShiftY);

				EM2Text * emtMaterials = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_MATERIALS_COUNT));
				emtMaterials->bgImage = hBgBMP;
				emtMaterials->setFont(fonts->em2text, false);
				updateControlBgShift(emtMaterials->hwnd, 0, 0, emtMaterials->bgShiftX, emtMaterials->bgShiftY);
				sprintf_s(buf, 256, "%d", sc->mats.objCount);
				emtMaterials->changeCaption(buf);

				EM2Text * emttTextures = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXT_TEXTURES));
				emttTextures->bgImage = hBgBMP;
				emttTextures->setFont(fonts->em2text, false);
				updateControlBgShift(emttTextures->hwnd, 0, 0, emttTextures->bgShiftX, emttTextures->bgShiftY);

				EM2Text * emtTextures = GetEM2TextInstance(GetDlgItem(hWnd, IDC_SI2_TEXTURES_COUNT));
				emtTextures->bgImage = hBgBMP;
				emtTextures->setFont(fonts->em2text, false);
				updateControlBgShift(emtTextures->hwnd, 0, 0, emtTextures->bgShiftX, emtTextures->bgShiftY);
				sprintf_s(buf, 256, "%d", sc->texManager.textures.objCount);
				emtTextures->changeCaption(buf);

				memTxt = analyzeSceneAndGetText();
				if (!memTxt)
					memTxt = copyString("NO MORE INFO");

				// eval text size for scroll size
				HDC tthdc = GetWindowDC(hWnd);
				HFONT oldFont = (HFONT)SelectObject(tthdc, fonts->em2text);
				RECT rectmt;
				DrawText(tthdc, memTxt, (int)strlen(memTxt), &rectmt, DT_CALCRECT);
				mt_height = rectmt.bottom-rectmt.top;
				SelectObject(tthdc, oldFont);
				ReleaseDC(hWnd, tthdc);

				EM2ScrollBar * emsc = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_SI2_SCROLLBAR));
				emsc->bgImage = hBgBMP;
				updateControlBgShift(emsc->hwnd, 0, 0, emsc->bgShiftX, emsc->bgShiftY);
				mt_vis = crect.bottom-mt_marginup-mt_margindown;
				emsc->setWorkArea(mt_vis, mt_height);
				emsc->workarea_pos = 0;
				emsc->updateSliderFromWorkarea();

			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_DESTROY:
			{
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (memTxt)
					free(memTxt);
				memTxt = NULL;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;

				GetClientRect(hWnd, &rect);
				hdc = BeginPaint(hWnd, &ps);

				// back buffer
				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				// copy background image
				HDC thdc = CreateCompatibleDC(hdc);			// bg image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, 0, 0, SRCCOPY);
				
				
				///  ----- text hdc
				int text_pos_x = 20;
				int text_pos_y = mt_marginup;
				int text_width = rect.right - 29 - text_pos_x;
				int text_height = rect.bottom - text_pos_y - mt_margindown;
				HDC thdc2 = CreateCompatibleDC(hdc);			// text img buffer hdc
				HBITMAP backBMP2 = CreateCompatibleBitmap(hdc, text_width, text_height);
				HBITMAP oldBitmap2 = (HBITMAP)SelectObject(thdc2, backBMP2);
				BitBlt(thdc2, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, text_pos_x, text_pos_y, SRCCOPY);
				SetBkMode(thdc2, TRANSPARENT);
				SetTextColor(thdc2, RGB(207, 207, 207));
				HFONT hOldFont = (HFONT)SelectObject(thdc2, NOXFontManager::getInstance()->em2text);
				RECT trect;
				GetClientRect(hWnd, &trect);
				trect.left = 0;
				trect.right = text_width;
				trect.top = -mt_pos;//-300;//-license_pos;
				DrawText(thdc2, memTxt, (int)strlen(memTxt), &trect, DT_NOCLIP);
				BitBlt(bhdc, text_pos_x, text_pos_y, text_width, text_height, thdc2, 0, 0, SRCCOPY);
				SelectObject(thdc2, hOldFont);
				SelectObject(thdc2, oldBitmap2);
				DeleteDC(thdc2);
				
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				// done
				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_MOUSEWHEEL:
			{
				RECT crect;
				GetClientRect(hWnd, &crect);
				int zDelta = (int)wParam;
				if (zDelta>0)
				{
					mt_pos = max(mt_pos-20, 0);
				}
				else
				{
					mt_pos = min(mt_pos+20, mt_height-mt_vis);
				}
				InvalidateRect(hWnd, NULL, false);

				EM2ScrollBar * emsc = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_SI2_SCROLLBAR));
				emsc->workarea_pos = mt_pos;
				emsc->updateSliderFromWorkarea();
				InvalidateRect(emsc->hwnd, NULL, false);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_SI2_CLOSE:
						{
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;
					case IDC_SI2_SCROLLBAR:	// scrollbar
						{
							if (wmEvent!=SB2_POS_CHANGED)
								break;
							EM2ScrollBar * emsc = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_SI2_SCROLLBAR));

							mt_pos = emsc->workarea_pos;

							InvalidateRect(hWnd, NULL, false);

						}
						break;

				}
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool addTextureInfo2(Scene * sc, Texture * tex, unsigned long long &total_size, char * chname, int txtsize, int &left, char * txtbuf, char * &txtptr, int num_tabs)
{
	total_size = 0;
	CHECK(sc);
	CHECK(tex);
	CHECK(txtbuf);

	Logger::add("tex");

	int w = tex->bmp.getWidth();
	int h = tex->bmp.getHeight();

	
	num_tabs = min(10, max(0, num_tabs));
	char tabs_str[64];
	tabs_str[0] = 0;
	for (int i=0; i<num_tabs; i++)
	{
		int l = (int)strlen(tabs_str);
		sprintf_s(tabs_str+l, 64-l, "	");
	}

	if (tex->bmp.idata)
	{
		Logger::add("idata");
		unsigned long long i_size = w * h * 4;
		sprintf_s(txtptr, left, "%s%s texture 32-bit buffer (%d x %d) size: %.2f MB\r\n", tabs_str, chname, w, h, ((float)i_size/1024.0f/1024.0f) );
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
		total_size += i_size;
	}

	if (tex->bmp.fdata)
	{
		Logger::add("fdata");
		unsigned long long f_size = w * h * sizeof(Color4);
		sprintf_s(txtptr, left, "%s%s texture HDR buffer (%d x %d) size: %.2f MB\r\n", tabs_str, chname, w, h, ((float)f_size/1024.0f/1024.0f) );
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
		total_size += f_size;
	}

	Logger::add("tex done");
	return true;
}

//------------------------------------------------------------------------------------------------

char * analyzeSceneAndGetText()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);
	int txtsize = 512000;
	int left;
	char * txtbuf = (char*)malloc(txtsize);
	CHECK(txtbuf);
	char * txtptr = txtbuf;

	// ----- CAMERAS --------

	unsigned long long whole_scene_size = 0;
	unsigned long long cameras_size = 0;

	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
	sprintf_s(txtptr, left, "Cameras found: %d.\r\n", sc->cameras.objCount);
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
	for (int i=0; i<sc->cameras.objCount; i++)
	{
		Camera * cam = sc->cameras[i];
		unsigned long long cam_size = 0;
		if (!cam)
		{
			sprintf_s(txtptr, left, "	Camera %d is NULL\r\n", (i+1));
			txtptr = strlen(txtbuf)+txtbuf;
			left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			continue;
		}

		sprintf_s(txtptr, left, "	Camera %d : %s\r\n", (i+1), cam->name);
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

		sprintf_s(txtptr, left, "		Res: %d x %d   AA x%d\r\n", cam->width, cam->height, cam->aa);
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

		for (int j=0; j<16; j++)
		{
			unsigned long long direct_size = 0;
			unsigned long long gi_size = 0;
			if (cam->blendBuffDirect[j].imgBuf)
			{
				direct_size = (sizeof(Color4)+sizeof(int)) * cam->blendBuffDirect[j].width * cam->blendBuffDirect[j].height;
				sprintf_s(txtptr, left, "		Blend layer %d direct: (%d x %d): %.2f MB\r\n", 
								(j+1), cam->blendBuffDirect[j].width, cam->blendBuffDirect[j].height, ((float)direct_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}

			if (cam->blendBuffGI[j].imgBuf)
			{
				gi_size = (sizeof(Color4)+sizeof(int)) * cam->blendBuffGI[j].width * cam->blendBuffGI[j].height;
				sprintf_s(txtptr, left, "		Blend layer %d GI: (%d x %d): %.2f MB\r\n", 
								(j+1), cam->blendBuffGI[j].width, cam->blendBuffGI[j].height, ((float)gi_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}

			cam_size += direct_size;
			cam_size += gi_size;
		}

		if (cam->imgBuff)
		{
			unsigned long long img_size = 0;
			if (cam->imgBuff->imgBuf)
			{
				img_size = (sizeof(Color4)+sizeof(int)) * cam->imgBuff->width * cam->imgBuff->height;
				sprintf_s(txtptr, left, "		Accumulation buffer: (%d x %d): %.2f MB\r\n", 
								cam->imgBuff->width, cam->imgBuff->height, ((float)img_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}
			cam_size += img_size;
		}

		if (cam->depthBuf)
		{
			unsigned long long depth_size = 0;
			if (cam->depthBuf->fbuf)
			{
				depth_size = (sizeof(float)+sizeof(int)) * cam->depthBuf->width * cam->depthBuf->height;
				sprintf_s(txtptr, left, "		Depth buffer: (%d x %d): %.2f MB\r\n", 
								cam->depthBuf->width, cam->depthBuf->height, ((float)depth_size/1024.0f/1024.0f));
				txtptr = strlen(txtbuf)+txtbuf;
				left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);
			}
			cam_size += depth_size;
		}

		Texture * tex = &(cam->texDiaphragm);
		char chname[64];
		sprintf_s(chname, 64, "Camera diaphragm");
		unsigned long long diaph_size = 0;
		addTextureInfo2(sc, tex, diaph_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += diaph_size;

		tex = &(cam->texObstacle);
		sprintf_s(chname, 64, "Camera obstacle");
		unsigned long long obst_size = 0;
		addTextureInfo2(sc, tex, obst_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += obst_size;

		tex = &(cam->texDiapObst);
		sprintf_s(chname, 64, "Camera diaphragm and obstacle");
		unsigned long long diaphobst_size = 0;
		addTextureInfo2(sc, tex, diaphobst_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += diaphobst_size;

		tex = &(cam->texFourier);
		sprintf_s(chname, 64, "Camera glare");
		unsigned long long glare_size = 0;
		addTextureInfo2(sc, tex, glare_size, chname, txtsize, left, txtbuf, txtptr, 2);
		cam_size += glare_size;

		sprintf_s(txtptr, left, "		Total camera size : %.2f MB\r\n", ((float)cam_size/1024.0f/1024.0f));
		txtptr = strlen(txtbuf)+txtbuf;
		left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

		cameras_size += cam_size;
	}

	sprintf_s(txtptr, left, "	All cameras total size : %.2f MB\r\n\r\n", ((float)cameras_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	// ----- GEOMETRY --------

	unsigned int num_tris = sc->triangles.objCount;
	unsigned long long geom_size = num_tris * sizeof(Triangle);

	sprintf_s(txtptr, left, "Triangles: %d   Size : %.2f MB\r\n", num_tris, ((float)geom_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	unsigned long long kdtree_size = sc->sscene.bsp.kdtree_buf_size;
	sprintf_s(txtptr, left, "kd-tree size : %.2f MB\r\n\r\n", ((float)kdtree_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);


	// ----- TEXTURE MANAGER ------
	sprintf_s(txtptr, left, "Textures. Found : %d\r\n", sc->texManager.textures.objCount);
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	unsigned long long textures_size = 0;
	for (int i=0; i<sc->texManager.textures.objCount; i++)
	{
		if (!sc->texManager.textures[i])
			continue;
		if (!sc->texManager.textures[i]->isValid())
			continue;

		Texture * tex = sc->texManager.textures[i];
		char chname[512];
		sprintf_s(chname, 512, "Texture no. %d - %s - ", (i+1), tex->filename);
		unsigned long long tex_size = 0;
		addTextureInfo2(sc, tex, tex_size, chname, txtsize, left, txtbuf, txtptr, 1);
		textures_size += tex_size;
	}

	sprintf_s(txtptr, left, "	All textures size: %.2f MB\r\n\r\n", ((float)textures_size/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	// ----- SUMMARY ------

	unsigned long long total_scene_buffers = 0;
	total_scene_buffers += cameras_size;
	total_scene_buffers += geom_size;
	total_scene_buffers += kdtree_size;
	total_scene_buffers += textures_size;

	sprintf_s(txtptr, left, "Total scene size: %.2f MB\r\n", ((float)total_scene_buffers/1024.0f/1024.0f));
	txtptr = strlen(txtbuf)+txtbuf;
	left = txtsize - ((int)(INT_PTR)txtptr - (int)(INT_PTR)txtbuf);

	return txtbuf;
}

//------------------------------------------------------------------------------------------------
