#include <windows.h>
#include <stdio.h>
#include "EMControls.h"

extern HMODULE hDllModule;

EMButton::EMButton(HWND hWnd) 
{
	colText = RGB(0,0,0); 
	colBackGnd = RGB(96,160,255);
	colBackGndSelected = RGB(128,160,255);
	colBackGndClicked = RGB(112,176,255);
	colBorderLight = RGB(144, 208, 255);
	colBorderDark = RGB(32,96,208);
	colDisabledBackground = RGB(192,192,192);
	colDisabledBorderTopLeft = RGB(224,224,224);
	colDisabledBorderBottomRight = RGB(160,160,160);
	colDisabledText = RGB(96,96,96);
	clicked = false;
	mouseIn = false;
	selected = false;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hwnd = hWnd;
	fontWasChanged = false;

	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
	{
		captionSize = 0;
	}	
}

EMButton::~EMButton() 
{
	if (caption)
		free(caption);
	if (hFont  &&  !fontWasChanged)
		DeleteObject(hFont);
	hFont = 0;
	caption = NULL;
}

void InitEMButton()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMButton";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMButtonProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMButton *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMButton * GetEMButtonInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMButton * emb = (EMButton *)GetWindowLongPtr(hwnd, 0);
	#else
		EMButton * emb = (EMButton *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emb;
}

void SetEMButtonInstance(HWND hwnd, EMButton *emb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emb);
	#endif
}

LRESULT CALLBACK EMButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMButton * emb;
	emb = GetEMButtonInstance(hwnd);
	static HRGN rgn;

    switch(msg)
    {
	case WM_CREATE:
		{
			EMButton * emb1 = new EMButton(hwnd);
			SetEMButtonInstance(hwnd, (EMButton *)emb1);			

			RECT rect;
			GetClientRect(hwnd, &rect);
			POINT rp[] = {	{0, rect.bottom-2}, {0,2}, 
							{2,0}, {rect.right-2,0}, 
							{rect.right, 2}, {rect.right, rect.bottom-3}, 
							{rect.right-3, rect.bottom}, {3, rect.bottom},
							{0, rect.bottom-3}   };
			rgn = CreatePolygonRgn(rp, 9, WINDING);			
			SetWindowRgn(hwnd, rgn, TRUE);
		}
	break;
	case WM_SIZE:
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			POINT rp[] = {	{0, rect.bottom-2}, {0,2}, 
							{2,0}, {rect.right-2,0}, 
							{rect.right, 2}, {rect.right, rect.bottom-3}, 
							{rect.right-3, rect.bottom}, {3, rect.bottom},
							{0, rect.bottom-3}   };
			rgn = CreatePolygonRgn(rp, 9, WINDING);			
			SetWindowRgn(hwnd, rgn, TRUE);
		}
		break;
	case WM_NCDESTROY:
		{
			EMButton * emb1;
			emb1 = GetEMButtonInstance(hwnd);
			delete emb1;
			SetEMButtonInstance(hwnd, 0);
			DeleteObject(rgn);
		}
	break;
	case WM_PAINT:
	{
		if (!emb) 
			break;

		RECT rect, inner;
		HDC hdc;
		PAINTSTRUCT ps;
		SIZE sz;
		HANDLE hOldFont;
		HANDLE hOldPen;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);

		hdc = BeginPaint(hwnd, &ps);

		hOldFont = SelectObject(hdc, emb->hFont);
		if (disabled)
			SetTextColor(hdc, emb->colDisabledText);
		else
			SetTextColor(hdc, emb->colText);

		if (disabled)
			SetBkColor(hdc, emb->colDisabledBackground);
		else
			if (emb->clicked && emb->mouseIn)
				SetBkColor  (hdc, emb->colBackGndClicked);
			else
				if (emb->selected)
					SetBkColor  (hdc, emb->colBackGndSelected);
				else
					SetBkColor  (hdc, emb->colBackGnd);

		GetClientRect(hwnd, &rect);
		inner = rect;
		inner.left++;
		inner.top++;
		inner.right--;
		inner.bottom--;

		
		POINT border1[] = {	{0, rect.bottom-3}, {0,2}, 
							{2,0}, {rect.right-3,0} };	// 4

		POINT border2[] = {	{rect.right-3,0}, {rect.right-1, 2}, 
							{rect.right-1, rect.bottom-3}, {rect.right-3, rect.bottom-1}, 
							{2, rect.bottom-1}, {0, rect.bottom-3},	 {0, rect.bottom-4}};	// 6
		
		GetTextExtentPoint32(hdc, emb->caption, emb->captionSize, &sz);
		int posx = (rect.right-rect.left-sz.cx)/2;
		int posy = (rect.bottom-rect.top-sz.cy)/2;
		if (emb->clicked && emb->mouseIn)
		{
			posx++;
			posy++;
		}

		ExtTextOut(hdc, posx, posy, ETO_OPAQUE, &inner, emb->caption, emb->captionSize, 0);

		if (disabled)
		{
			hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emb->colDisabledBorderTopLeft));
			Polyline(hdc, border1, 4);
			DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emb->colDisabledBorderBottomRight)));
			Polyline(hdc, border2, 7);
			DeleteObject(SelectObject(hdc, hOldPen));
		}
		else
			if (emb->clicked && emb->mouseIn)
			{
				hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emb->colBorderDark));
				Polyline(hdc, border1, 4);
				DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emb->colBorderLight)));
				Polyline(hdc, border2, 7);
				DeleteObject(SelectObject(hdc, hOldPen));
			}
			else
			{
				hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emb->colBorderLight));
				Polyline(hdc, border1, 4);
				DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emb->colBorderDark)));
				Polyline(hdc, border2, 7);
				DeleteObject(SelectObject(hdc, hOldPen));
			}
		
		SelectObject(hdc, hOldFont);

		EndPaint(hwnd, &ps);

	}
	break;
	break;
	case WM_MOUSEMOVE:
	{
		if (GetCapture() == hwnd)
		{
			RECT rect;
			GetClientRect(hwnd, &rect);

			if (emb->mouseIn)
			{
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				if (!PtInRect(&rect, pt))
				{
					emb->mouseIn = false;
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}
			}
			else
			{
				POINT pt = { LOWORD(lParam), HIWORD(lParam) };
				if (PtInRect(&rect, pt))
				{
					emb->mouseIn = true;
					GetClientRect(hwnd, &rect);
					InvalidateRect(hwnd, &rect, false);
				}
			}
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		if (!GetCapture())
		{
			emb->clicked = true;
			emb->mouseIn = true;
			SetCapture(hwnd);
			RECT rect;
			GetClientRect(hwnd, &rect);
			InvalidateRect(hwnd, &rect, false);
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		bool wasClicked = false;
		if (GetCapture() != hwnd)
			emb->clicked = false;

		if (emb->clicked)
		{
			emb->clicked = false;
			emb->mouseIn = true;
			RECT rect;
			GetClientRect(hwnd, &rect);
			
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (PtInRect(&rect, pt))
			{
				wasClicked = true;
			}

			InvalidateRect(hwnd, &rect, false);

		}
		ReleaseCapture();
		emb->clicked = false;
		emb->mouseIn = true;
		if (wasClicked)
		{
			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), 
					(LPARAM)hwnd);
		}
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

bool EMButton::changeCaption(char *newCaption)
{
	int ss = (int)strlen(newCaption);
	char * newBuff = (char *)malloc(ss+1);
	if (!newBuff)
		return false;
	SetWindowText(hwnd, newCaption);
	GetWindowText(hwnd, newBuff, ss+1);
	free(caption);
	caption = newBuff;
	captionSize = ss;
	InvalidateRect(hwnd, NULL, false);
	return true;
}

void EMButton::setFont(HFONT hNewFont)
{
	if (hNewFont)
	{
		hFont = hNewFont;
		fontWasChanged = true;
	}
}

