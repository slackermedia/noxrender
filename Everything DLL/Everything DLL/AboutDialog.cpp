#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

// old gui version

extern HMODULE hDllModule;

HBITMAP loadPNGfromResource(WORD idRes);


typedef LRESULT (WINAPI * STATICTEXTPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
STATICTEXTPROC OldSTProc = NULL;

INT_PTR CALLBACK HyperTextDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
		case WM_MOUSEMOVE:
			{
				if (GetCapture() != hWnd)
				{
					InvalidateRect(hWnd, NULL, FALSE);
					SetCapture(hWnd);
					SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW)) );
				}
				else
				{
					RECT rect;
					GetWindowRect(hWnd, &rect);
					POINT pt = { LOWORD(lParam), HIWORD(lParam) };
					ClientToScreen(hWnd, &pt);

					if (!PtInRect(&rect, pt))
					{
						InvalidateRect(hWnd, NULL, FALSE);
						ReleaseCapture();
						SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW)) );
					}
					else
					{
						SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_HAND)) );
					}
				}
			}
			break;
	}

	return OldSTProc(hWnd, message, wParam, lParam);
}



INT_PTR CALLBACK AboutDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBRUSH trBrush = 0;
	static HBITMAP hLogoBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (!bgBrush)
					bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				if (!trBrush)
				{
					LOGBRUSH lgb;
					lgb.lbStyle = BS_NULL;
					trBrush = CreateBrushIndirect(&lgb);
				}
				
				EMButton * embclose = GetEMButtonInstance(GetDlgItem(hWnd, IDC_ABOUT_CLOSE));
				EMButton * emblicense = GetEMButtonInstance(GetDlgItem(hWnd, IDC_ABOUT_SHOW_LICENSE));
				EMButton * embcredits = GetEMButtonInstance(GetDlgItem(hWnd, IDC_ABOUT_CREDITS));
				EMText * emtextautoupdate = GetEMTextInstance(GetDlgItem(hWnd, IDC_ABOUT_AUTOUPDATE_TEXT));
				EMCheckBox * emcbautoupdate = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_ABOUT_AUTOUPDATE_ON));

				GlobalWindowSettings::colorSchemes.apply(embclose);
				GlobalWindowSettings::colorSchemes.apply(emblicense);
				GlobalWindowSettings::colorSchemes.apply(embcredits);
				GlobalWindowSettings::colorSchemes.apply(emcbautoupdate);
				GlobalWindowSettings::colorSchemes.apply(emtextautoupdate, true);


				int vMajor = Raytracer::getInstance()->getVersionMajor();
				int vMinor = Raytracer::getInstance()->getVersionMinor();
				int vBuild = Raytracer::getInstance()->getVersionBuild();
				char vertext[128];
				#ifdef _WIN64
					sprintf_s(vertext, "version %d.%d.%d beta 64-bit", vMajor, vMinor, vBuild);
				#else
					sprintf_s(vertext, "version %d.%d.%d beta 32-bit", vMajor, vMinor, vBuild);
				#endif
				SetWindowText(GetDlgItem(hWnd, IDC_ABOUT_VERSION), vertext);


				HWND hLogo = GetDlgItem(hWnd, IDC_ABOUT_LOGO);
				hLogoBMP = loadPNGfromResource(IDB_NOX_LOGO_ABOUT);
				if (hLogoBMP)
					SendMessage(hLogo, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hLogoBMP);

				bool autoupdateOn = checkAutoupdateStatusFromRegistry();
				emcbautoupdate->selected = autoupdateOn;
				InvalidateRect(emcbautoupdate->hwnd, NULL, false);

				HWND hLink = GetDlgItem(hWnd, IDC_ABOUT_TEST_TEXT);
				#ifdef _WIN64
					OldSTProc = (STATICTEXTPROC) SetWindowLongPtr(hLink, GWLP_WNDPROC, (LONG_PTR)HyperTextDlgProc) ;
				#else
					OldSTProc = (STATICTEXTPROC)(HANDLE)(LONG_PTR) SetWindowLong(hLink, GWL_WNDPROC, (LONG)(LONG_PTR)HyperTextDlgProc) ;
				#endif


			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_DESTROY:
			{
				if (hLogoBMP)
					DeleteObject(hLogoBMP);
				hLogoBMP = 0;
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (trBrush)
					DeleteObject(trBrush);
				trBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_ABOUT_CLOSE:
						{
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;

					case IDC_ABOUT_AUTOUPDATE_ON:
						{
							EMCheckBox * emcbautoupdate = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_ABOUT_AUTOUPDATE_ON));
							setAutoupdateStatusInRegistry(emcbautoupdate->selected);
						}
						break;
					case IDC_ABOUT_SHOW_LICENSE:
						{
							int res = (int)DialogBoxParam(hDllModule,
									MAKEINTRESOURCE(IDD_LICENSE_DIALOG), hWnd, LicenseDlgProc,(LPARAM)(0));
						}
						break;

					case IDC_ABOUT_CREDITS:
						{
							int res = (int)DialogBoxParam(hDllModule,
									MAKEINTRESOURCE(IDD_CREDITS), hWnd, CreditsDlgProc,(LPARAM)(0));
						}
						break;

					case IDC_ABOUT_TEST_TEXT:
						{
							if (wmEvent == STN_CLICKED)
							{
							   ShellExecute(hWnd, "open", "http://www.evermotion.org", NULL, NULL, SW_SHOWNORMAL);
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				if ((HWND)lParam == GetDlgItem(hWnd, IDC_ABOUT_TEST_TEXT))
					SetTextColor(hdc1, RGB(0,160,210));
				else
					SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)trBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

HBITMAP loadPNGfromResource(WORD idRes)
{
	HRSRC res = FindResource(hDllModule, MAKEINTRESOURCE(idRes), "BINARY");
	if (!res)
		return 0;

	HGLOBAL mem = LoadResource(hDllModule, res);
	void *data = LockResource(mem);
	if (!data)
		return 0;
	size_t sz = SizeofResource(hDllModule, res);
	if (sz < 1)
		return 0;

	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, sz);
	if (!hGlobal)
		return 0;
	LPVOID pvData = GlobalLock(hGlobal);
	CopyMemory(pvData,data, sz);
	LPSTREAM pStream = NULL;
	HRESULT hr = CreateStreamOnHGlobal( hGlobal, false, &pStream);
	if (hr != S_OK   ||  !pStream)
	{
		if (pStream)
			pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		return 0;
	}


	// activate gdi+
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// make bitmap
	Gdiplus::Bitmap * bm = new Gdiplus::Bitmap(pStream);
	if (!bm  ||  bm->GetLastStatus() != Gdiplus::Ok  ||  bm->GetWidth()<1  ||  bm->GetHeight()<1)
	{
		pStream->Release();
		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		if (bm)
			delete bm;
		Gdiplus::GdiplusShutdown(gdiplusToken);
		return 0;
	}

	HBITMAP hBmpRes = 0;
	Gdiplus::Color bgcol = Gdiplus::Color(0,0,0);
	if (Gdiplus::Ok != bm->GetHBITMAP(bgcol, &hBmpRes) )
	{
		if (hBmpRes)
			DeleteObject(hBmpRes);
	}

	pStream->Release();
	GlobalUnlock(hGlobal);
	GlobalFree(hGlobal);
	delete bm;
	Gdiplus::GdiplusShutdown(gdiplusToken);
	return hBmpRes;

}


