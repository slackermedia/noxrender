#include <windows.h>
#include "MersenneTwister.h"
#include "raytracer.h"

float Raytracer::getRandomFloatThread()
{
	RandomGenerator * rg = (RandomGenerator *)TlsGetValue(tlsIndexRandom);
	if (rg)
		return rg->getRandomFloat();
	else
		return rgMain.getRandomFloat();
}

unsigned int Raytracer::getRandomIntThread(unsigned int maxval)
{
	RandomGenerator * rg = (RandomGenerator *)TlsGetValue(tlsIndexRandom);
	if (rg)
		return rg->getRandomInt(maxval);
	else
		return rgMain.getRandomInt(maxval);
}

float RandomGenerator::getRandomFloat()
{
	unsigned int r = mt.randUInt();
	float f = (float)r/(float)0xffffffff;
	return f;
}

double RandomGenerator::getRandomDouble()
{
	unsigned int r = mt.randUInt();
	double f = (double)r/(double)0xffffffff;
	return f;
}

unsigned int RandomGenerator::getRandomInt(unsigned int maxval)
{
	unsigned int r = mt.randUInt();
	unsigned long long l = (unsigned long long)r * (unsigned long long)maxval / (unsigned long long)0xffffffff;
	return (unsigned int)l;
}

RandomGenerator * Raytracer::getRandomGeneratorForThread()
{
	RandomGenerator * rg = (RandomGenerator *)TlsGetValue(tlsIndexRandom);
	if (!rg)
	{
		return &(getInstance()->rgMain);
	}
	return rg;
}
