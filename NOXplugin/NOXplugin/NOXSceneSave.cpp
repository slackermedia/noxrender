#include "max2013predef.h"
#include "NOXExporter.h"
#include "NOXMaterial.h"
#include "NOXCamera.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <delayimp.h>
#include "NOXSunLight.h"
#include "NOXRenderer.h"
#pragma comment(lib, "libxml2.lib")
#pragma comment(lib, "DelayImp.lib")
#define ROUND(a) (int)floor(a+0.5)

#include "noxdebug.h"

#define LAYER_TEX_SLOT_COLOR0 0
#define LAYER_TEX_SLOT_COLOR90 1
#define LAYER_TEX_SLOT_ROUGHNESS 2
#define LAYER_TEX_SLOT_WEIGHT 3
#define LAYER_TEX_SLOT_LIGHT 4
#define LAYER_TEX_SLOT_NORMAL 5
#define LAYER_TEX_SLOT_TRANSM 6
#define LAYER_TEX_SLOT_ANISO 7
#define LAYER_TEX_SLOT_ANISO_ANGLE 8
#define MAT_TEX_SLOT_OPACITY 100
#define MAT_TEX_SLOT_DISPLACEMENT 101


XMLScene::XMLScene(bool asRenderer)
{
	ADD_LOG_CONSTR("XMLScene constructor");

	numTris = 0;
	meshes.clear();
	cameras.clear();
	mats.clear();
	matTypes.clear();
	hInfoWindow = NULL;
	locale = _create_locale(LC_NUMERIC, "English");
	exportAndRender = asRenderer;
	prog = NULL;
	sunsky = NULL;

	unitScale = (float)GetMasterScale(UNITS_METERS);
}

XMLScene::~XMLScene()
{
	meshes.clear();
	cameras.clear();
	mats.clear();
	matTypes.clear();
}


void XMLScene::resetMe()
{
	ADD_LOG_PARTS_MAIN("XMLScene::resetMe");
	meshes.clear();
	cameras.clear();
	mats.clear();
	matTypes.clear();
	numTris = 0;
	hInfoWindow = NULL;
}


//-------------------------------------------------------------------------------------------------------------

bool XMLScene::saveScene(const TCHAR * filename)
{
	if (!filename)
		return false;

	if (FAILED(__HrLoadAllImportsForDll("libxml2.dll"))) 
		MessageBox(0, _T("There might be a problem. Dll not found."),_T(""),0);

	unitScale = (float)GetMasterScale(UNITS_METERS);

	setProgInfo(_T("Checking cameras..."));

	ValidateCameras vc;
	camerasOK = vc.check(GetCOREInterface()->GetRootNode());

	if (cameras.size() < 1)
	{
		int bbb = MessageBox(0, _T("No cameras found. Do you want to continue exporting?"), _T("Warning"),MB_YESNO);
		if (IDNO == bbb)
		{
			ADD_LOG_PARTS_MAIN("XMLScene::saveScene aborted - no cameras");
			return false;
		}
	}

	setProgInfo(_T("Creating xml structure..."));

	numDoneTris = 0;
	char buf[128];
	xmlDocPtr xmlDoc;
	xmlDoc = xmlNewDoc((xmlChar*)"1.0");
	if (!xmlDoc)
	{
		return false;
	}

	xmlNodePtr xRoot = xmlNewDocNode(xmlDoc, NULL, (xmlChar*)"scene", NULL);
	if (!xRoot)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}
	xmlDocSetRootElement(xmlDoc, xRoot);

	// add objects
	xmlNodePtr xObjects = xmlNewChild(xRoot, NULL, (xmlChar*)"objects", NULL);
	if (!xObjects)
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	// add totalTris
	xmlAttrPtr xAttr;
	sprintf_s(buf, 128, "%d", numTris);
	xAttr = xmlNewProp(xObjects, (xmlChar*)"totalTris", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add scale
	xmlAttrPtr xScale;
	float scale = 1.0f;
	_sprintf_s_l(buf, 128, "%.3f", locale, scale);
	xScale = xmlNewProp(xObjects, (xmlChar*)"scale", (xmlChar*)buf);
	if (!xScale)
		return false;

	for (unsigned int i=0; i<meshsources.size(); i++)
	{
		if (!insertInstSource(xObjects, i))
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
	}

	for (unsigned int i=0; i<meshinstances.size(); i++)
	{
		if (!insertInstance(xObjects, i))
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
	}

	for (unsigned int i=0; i<cameras.size(); i++)
	{
		if (!insertCamera(xRoot, cameras[i]))
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
	}

	if (!insertMaterials(xRoot))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (!insertUserColors(xRoot))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (!insertBlendSettings(xRoot))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (sunsky)
	{
		if (!insertSunSky(xRoot, sunsky))
		{
			xmlFreeDoc(xmlDoc);
			return false;
		}
	}

	if (!insertSceneInfo(xRoot))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (!insertRendererSettings(xRoot))
	{
		xmlFreeDoc(xmlDoc);
		return false;
	}


	// save
	setProgInfo(_T("Saving to file... please wait"));
	char * filename_ansi = getAnsiCopy1(filename);
	int written = xmlSaveFormatFile(filename_ansi, xmlDoc, 1);
	if (filename_ansi)
		free(filename_ansi);

	setProgInfo(_T("Releasing xml..."));
	xmlFreeDoc(xmlDoc);
	setProgInfo(_T("Done"));

	if (written < 1)
		return false;

	ADD_LOG_PARTS_MAIN("XMLScene::saveScene - file closed successfully");

	bool thereIsLight = false;
	// check for sunsky
	if (sunsky)
	{
		bool sunsky_on = false;
		NOXSunLight * sun = NULL;
		ObjectState objSt = sunsky->EvalWorldState(GetCOREInterface()->GetTime());
		if (objSt.obj != NULL)
			sun = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
		if (sun)
		sunsky_on = sun->trsunsky.getSunSkyON();
		if (sunsky_on)
			thereIsLight = true;
	}
	// check for emissive material
	unsigned int numMats = (unsigned int)mats.size();
	for (unsigned int i=0; i<numMats; i++)
	{
		bool isNOXmat = matTypes[i];
		if (!isNOXmat)
			continue;

		NOXMaterial * nmat = (NOXMaterial*)mats[i];
		int nLayers = nmat->mat.getLayersNumber();
		for (int j=0; j<nLayers; j++)
		{
			int type = nmat->mat.getLayerType(j);
			if (type == 1)
				thereIsLight = true;
		}
	}
	// give a warning
	if (!thereIsLight)
	{
		MessageBox(0, _T("Scene exported successfully, however no emitter found."), _T("Warning"), 0);
		//if (IDNO == bbb)
		//	return false;
	}

	ADD_LOG_PARTS_MAIN("XMLScene::saveScene done");

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertInstSource(xmlNodePtr curNode, int mNum)
{
	if (!curNode)
		return false;
	if (mNum<0  ||  mNum>=(int)meshsources.size())
		return false;

	// add mesh_src
	xmlNodePtr xMeshSrc = xmlNewChild(curNode, NULL, (xmlChar*)"mesh_src", NULL);
	if (!xMeshSrc)
		return false;

	char strID[64];
	sprintf_s(strID, 64, "%d", mNum);
	xmlAttrPtr xAttr;
	xAttr = xmlNewProp(xMeshSrc, (xmlChar*)"id", (xmlChar*)strID);
	if (!xAttr)
		return false;

	INode * node = meshsources[mNum].node; 


	const TCHAR * tname = node->GetObjectRef()->GetObjectName();
	char * name_ansi = tname ? getAnsiCopy1(tname) : copyStringMax1("Unnamed");

	// add name
	xmlNodePtr xName = xmlNewChild(xMeshSrc, NULL, (xmlChar*)"name", (xmlChar*)name_ansi);
	if (!xName)
		return false;

	free(name_ansi);
	name_ansi = NULL;

	// -------- triangles from here

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (!objSt.obj)
		return false;

	TriObject * tobj = (TriObject *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), triObjectClassID);
	if (!tobj)
		return false;

	Mesh mesh = tobj->GetMesh();
	unsigned int numFaces = mesh.numFaces;

	Point3 * normalsArray;
	MNormal * normals;
	Matrix3 m2;
	m2.IdentityMatrix();
	normals = ComputeMyNormals(&mesh, &normalsArray, m2);

	Mtl * mmtl = node->GetMtl();
	bool mattype;

	for (unsigned int i=0; i<numFaces; i++)
	{
		int matID = 0;
		int matInInst = 0;
		if (mmtl)
		{
			if (mmtl->IsMultiMtl())
			{
				int mod = mmtl->NumSubMtls();
				if (mod == 0)
				{
					mod = 1;
					MessageBox(0,_T("Empty multimaterial. Export may fail."),_T(""),0);
					mattype = false;
					matID = addMat(0, mattype);
				}
				else
				{
					int intID = mesh.getFaceMtlIndex(i) % mod;
					matInInst = intID;
					Mtl * submtl = mmtl->GetSubMtl(intID);
					mattype = (NOXMaterial_CLASS_ID == submtl->ClassID()) ? true : false;
					matID = addMat(submtl, mattype);
				}
			}
			else
			{
				mattype = (mmtl->ClassID() == NOXMaterial_CLASS_ID) ? true : false;
				matID = addMat(mmtl, mattype);
			}
		}
		else
		{
			mattype = false;
			matID = addMat(mmtl, mattype);
		}

		if (!insertTriangleSrc(xMeshSrc, &mesh, i, matID, matInInst, normals))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			return false;
		}
	}

	int a = (int)(numDoneTris/(float)numTris*1000);
	setProgress(a);

	deleteSecondIfCopied(objSt.obj, tobj);

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertInstance(xmlNodePtr curNode, int iNum)
{
	if (!curNode)
		return false;
	if (iNum<0  ||  iNum>=(int)meshinstances.size())
		return false;

	INode * node = meshinstances[iNum].node;
	if (!node)
		return false;
	Matrix3 m1 = node->GetObjTMAfterWSM(GetCOREInterface()->GetTime(), NULL);

	// add instance
	xmlNodePtr xInstance= xmlNewChild(curNode, NULL, (xmlChar*)"instance", NULL);
	if (!xInstance)
		return false;

	// id attribute
	unsigned int instID	= meshinstances[iNum].id;
	char strID[64];
	sprintf_s(strID, 64, "%d", instID);
	xmlAttrPtr xAttr;
	xAttr = xmlNewProp(xInstance, (xmlChar*)"id", (xmlChar*)strID);
	if (!xAttr)
		return false;

	const TCHAR * name = node->GetName();
	char * name_ansi = name ? getAnsiCopy1(name) : copyStringMax1("Unnamed");

	// add name
	xmlNodePtr xName = xmlNewChild(xInstance, NULL, (xmlChar*)"name", (xmlChar*)name_ansi);
	if (!xName)
		return false;

	free(name_ansi);
	name_ansi = NULL;

	// matrix
	Point4 p1 = m1.GetColumn(0);
	Point4 p2 = m1.GetColumn(1);
	Point4 p3 = m1.GetColumn(2);
	Point4 p4 = Point4(0.0f, 0.0f, 0.0f, 1.0f);
	p1[3] *= unitScale;
	p2[3] *= unitScale;
	p3[3] *= unitScale;
	float mm[] = {
		 p1[0],  p1[2], -p1[1],  p1[3], 
		 p3[0],  p3[2], -p3[1],  p3[3], 
		-p2[0], -p2[2],  p2[1], -p2[3], 
		 p4[0],  p4[2], -p4[1],  p4[3]};
	unsigned char buf[256];
	for (int i=0; i<16; i++)
		DecHex::floatToHex(mm[i], buf+i*8);
	buf[128] = 0;

	// add matrix
	xmlNodePtr xMatrix = xmlNewChild(xInstance, NULL, (xmlChar*)"matrix", (xmlChar*)buf);
	if (!xMatrix)
		return false;


	// materials
	unsigned char mbuf[1024];
	int nummats = 1;
	bool isMultiMat = false;
	Mtl * mmtl = node->GetMtl();
	if (mmtl)
		if (isMultiMat = (TRUE==mmtl->IsMultiMtl()) )	// no ==
			nummats = max(1, mmtl->NumSubMtls());

	if (isMultiMat)
	{
		if (mmtl->NumSubMtls()==0)
		{
			unsigned int matID = addMat(NULL, false);
			DecHex::uintToHex(matID, mbuf);
			mbuf[8] = 0;
		}
		else
		{
			for (int i=0; i<nummats; i++)
			{
				Mtl * submtl = mmtl->GetSubMtl(i);
				bool mattype = (NOXMaterial_CLASS_ID == submtl->ClassID()) ? true : false;
				unsigned int matID = addMat(submtl, mattype);
				DecHex::uintToHex(matID, mbuf+i*8);
			}
			mbuf[nummats*8] = 0;
		}
	}
	else
	{
		unsigned int matID = 0;
		if (mmtl)
		{
			bool mattype = (NOXMaterial_CLASS_ID == mmtl->ClassID()) ? true : false;
			matID = addMat(mmtl, mattype);
		}
		else
			matID = addMat(NULL, false);

		DecHex::uintToHex(matID, mbuf);
		mbuf[8] = 0;
	}

	// add materials
	xmlNodePtr xMats = xmlNewChild(xInstance, NULL, (xmlChar*)"materials", mbuf);
	if (!xMats)
		return false;

	// count attribute
	char strCount[64];
	sprintf_s(strCount, 64, "%d", nummats);
	xmlAttrPtr xAttr2;
	xAttr2 = xmlNewProp(xMats, (xmlChar*)"count", (xmlChar*)strCount);
	if (!xAttr2)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertMesh(xmlNodePtr curNode, INode * node)
{
	if (!curNode   ||   !node)
		return false;

	Matrix3 m1 = node->GetObjTMAfterWSM(GetCOREInterface()->GetTime(), NULL);

	// add mesh
	xmlNodePtr xMesh = xmlNewChild(curNode, NULL, (xmlChar*)"mesh", NULL);
	if (!xMesh)
		return false;

	// add name
	const TCHAR * name = node->GetName();
	if (name)
	{
		char * name_ansi = getAnsiCopy1(name);
		xmlNodePtr xName = xmlNewChild(xMesh, NULL, (xmlChar*)"name", (xmlChar*)name_ansi);
		if (!xName)
			return false;
		free(name_ansi);
		name_ansi = NULL;
	}

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return false;

	TriObject * tobj = (TriObject *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), triObjectClassID);
	if (!tobj)
		return false;

	Mesh mesh = tobj->GetMesh();

	Point3 * normalsArray;
	MNormal * normals;
	normals = ComputeMyNormals(&mesh, &normalsArray, m1);

	Mtl * mmtl = node->GetMtl();
	bool mattype;
		
		
	for (int i=0; i<mesh.numFaces; i++)
	{
		int matID = 0;
		if (mmtl)
		{
			if (mmtl->IsMultiMtl())
			{
				int mod = mmtl->NumSubMtls();
				if (mod == 0)
				{
					mod = 1;
					MessageBox(0,_T("Empty multimaterial. Export may fail."), _T(""),0);
					mattype = false;
					matID = addMat(0, mattype);
				}
				else
				{
					int intID = mesh.getFaceMtlIndex(i) % mod;
					Mtl * submtl = mmtl->GetSubMtl(intID);
					mattype = (NOXMaterial_CLASS_ID == submtl->ClassID()) ? true : false;
					matID = addMat(submtl, mattype);
				}
			}
			else
			{
				mattype = (mmtl->ClassID() == NOXMaterial_CLASS_ID) ? true : false;
				matID = addMat(mmtl, mattype);
			}
		}
		else
		{
			mattype = false;
			matID = addMat(mmtl, mattype);
		}
		if (!insertTriangle(xMesh, &mesh, i, matID, normals, m1))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			return false;
		}
	}

	int a = (int)(numDoneTris/(float)numTris*1000);
	setProgress(a);

	deleteSecondIfCopied(objSt.obj, tobj);
	
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertTriangleSrc(xmlNodePtr curNode, Mesh * mesh, int triIndex, int matID, int matInLib, MNormal * normals)
{

	if (!curNode   ||   !mesh)
		return false;

	numDoneTris++;

	int i = triIndex;
	Point3 v1,v2,v3;
	Point3 n1,n2,n3;
	Point3 uv1,uv2,uv3;

	// vertices
	if (mesh->faces[i].Direction(mesh->faces[i].getVert(0), mesh->faces[i].getVert(1))<0)
	{
		v3 = mesh->verts[mesh->faces[i].getVert(0)];
		v2 = mesh->verts[mesh->faces[i].getVert(1)];
		v1 = mesh->verts[mesh->faces[i].getVert(2)];
	}
	else
	{
		v1 = mesh->verts[mesh->faces[i].getVert(0)];
		v2 = mesh->verts[mesh->faces[i].getVert(1)];
		v3 = mesh->verts[mesh->faces[i].getVert(2)];
	}

	Point3 normalna = (v2-v1)^(v3-v2);
	if (normalna.Length() == 0)
		return true;

	v1 *= unitScale;
	v2 *= unitScale;
	v3 *= unitScale;

	// normals
	DWORD smoothgr = mesh->faces[i].smGroup;
	if (smoothgr == 0)
	{
		n1 = (v2-v1)^(v3-v2);
		n1 = -n1.Normalize();
		n2=n3=n1;
	}
	else
	{
		if (mesh->faces[i].Direction(mesh->faces[i].getVert(0), mesh->faces[i].getVert(1))<0)
		{
			n3 = (normals[mesh->faces[i].getVert(0)].GetNormal(smoothgr));
			n2 = (normals[mesh->faces[i].getVert(1)].GetNormal(smoothgr));
			n1 = (normals[mesh->faces[i].getVert(2)].GetNormal(smoothgr));
		}
		else
		{
			n1 = (normals[mesh->faces[i].getVert(0)].GetNormal(smoothgr));
			n2 = (normals[mesh->faces[i].getVert(1)].GetNormal(smoothgr));
			n3 = (normals[mesh->faces[i].getVert(2)].GetNormal(smoothgr));
		}
		n1 = -n1;
		n2 = -n2;
		n3 = -n3;
	}


	// UV
	if (mesh->tvFace)
	{
		if (mesh->tvFace[i].Direction(mesh->tvFace[i].getTVert(0), mesh->tvFace[i].getTVert(1))<0)
		{
			uv3 = mesh->tVerts[mesh->tvFace[i].getTVert(0)];
			uv2 = mesh->tVerts[mesh->tvFace[i].getTVert(1)];
			uv1 = mesh->tVerts[mesh->tvFace[i].getTVert(2)];
		}
		else
		{
			uv1 = mesh->tVerts[mesh->tvFace[i].getTVert(0)];
			uv2 = mesh->tVerts[mesh->tvFace[i].getTVert(1)];
			uv3 = mesh->tVerts[mesh->tvFace[i].getTVert(2)];
		}
	}
	else
	{
			uv1 = Point3(1,1,1);
			uv2 = Point3(1,1,1);
			uv3 = Point3(1,1,1);
	}


	char temp[512];

	#ifdef NOX_SWAP_YZ
		printTriangleSrc((unsigned char *) temp, matID,	matInLib, 
					 v1.x,  v1.z,  -v1.y,  v2.x,  v2.z,  -v2.y,  v3.x,  v3.z,  -v3.y, 
					 n1.x,  n1.z,  -n1.y,  n2.x,  n2.z,  -n2.y,  n3.x,  n3.z,  -n3.y,
					uv1.x, uv1.y, uv2.x, uv2.y, uv3.x, uv3.y);
	#else
		printTriangleSrc((unsigned char *) temp, matID,	matInLib,	 
					 v1.x,  v1.y,  v1.z,  v2.x,  v2.y,  v2.z,  v3.x,  v3.y,  v3.z, 
					 n1.x,  n1.y,  n1.z,  n2.x,  n2.y,  n2.z,  n3.x,  n3.y,  n3.z,
					uv1.x, uv1.y, uv2.x, uv2.y, uv3.x, uv3.y);
	#endif

	// add tri
	xmlNodePtr xTri = xmlNewChild(curNode, NULL, (xmlChar*)"tri", (xmlChar*)temp);
	if (!xTri)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::printTriangleSrc(unsigned char * buf, unsigned int matID, unsigned int matInLib, 
								float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, float v3x, float v3y, float v3z, 
								float n1x, float n1y, float n1z, float n2x, float n2y, float n2z, float n3x, float n3y, float n3z, 
								float uv1x, float uv1y, float uv2x, float uv2y, float uv3x, float uv3y)
{
	if (!buf)
		return false;

	DecHex::uintToHex((unsigned int)matID, buf);	// mat in lib
	DecHex::uintToHex((unsigned int)matID, buf+8);	// mat id

	DecHex::floatToHex(v1x, buf+16);
	DecHex::floatToHex(v1y, buf+24);
	DecHex::floatToHex(v1z, buf+32);
	DecHex::floatToHex(v2x, buf+40);
	DecHex::floatToHex(v2y, buf+48);
	DecHex::floatToHex(v2z, buf+56);
	DecHex::floatToHex(v3x, buf+64);
	DecHex::floatToHex(v3y, buf+72);
	DecHex::floatToHex(v3z, buf+80);

	DecHex::floatToHex(-n1x, buf+88);
	DecHex::floatToHex(-n1y, buf+96);
	DecHex::floatToHex(-n1z, buf+104);
	DecHex::floatToHex(-n2x, buf+112);
	DecHex::floatToHex(-n2y, buf+120);
	DecHex::floatToHex(-n2z, buf+128);
	DecHex::floatToHex(-n3x, buf+136);
	DecHex::floatToHex(-n3y, buf+144);
	DecHex::floatToHex(-n3z, buf+152);

	DecHex::floatToHex(uv1x, buf+160);
	DecHex::floatToHex(uv1y, buf+168);
	DecHex::floatToHex(uv2x, buf+176);
	DecHex::floatToHex(uv2y, buf+184);
	DecHex::floatToHex(uv3x, buf+192);
	DecHex::floatToHex(uv3y, buf+200);

	buf[208] = 0;
	return true;
}


//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertTriangle(xmlNodePtr curNode, Mesh * mesh, int triIndex, int matID, MNormal * normals, Matrix3 matr)
{
	if (!curNode   ||   !mesh)
		return false;

	numDoneTris++;

	int i = triIndex;
	Point3 v1,v2,v3;
	Point3 n1,n2,n3;
	Point3 uv1,uv2,uv3;

	Matrix3 nmatr = matr;
	nmatr.SetRow(3, Point3(0,0,0));

	// vertices
	if (mesh->faces[i].Direction(mesh->faces[i].getVert(0), mesh->faces[i].getVert(1))<0)
	{
		v3 = mesh->verts[mesh->faces[i].getVert(0)]*matr;
		v2 = mesh->verts[mesh->faces[i].getVert(1)]*matr;
		v1 = mesh->verts[mesh->faces[i].getVert(2)]*matr;
	}
	else
	{
		v1 = mesh->verts[mesh->faces[i].getVert(0)]*matr;
		v2 = mesh->verts[mesh->faces[i].getVert(1)]*matr;
		v3 = mesh->verts[mesh->faces[i].getVert(2)]*matr;
	}

	Point3 normalna = (v2-v1)^(v3-v2);
	if (normalna.Length() == 0)
	{
		return true;
	}


	v1 *= unitScale;
	v2 *= unitScale;
	v3 *= unitScale;

	// normals
	DWORD smoothgr = mesh->faces[i].smGroup;
	if (smoothgr == 0)
	{
		n1 = (v2-v1)^(v3-v2);
		n1 = -n1.Normalize();
		n2=n3=n1;
	}
	else
	{
		if (mesh->faces[i].Direction(mesh->faces[i].getVert(0), mesh->faces[i].getVert(1))<0)
		{
			n3 = (normals[mesh->faces[i].getVert(0)].GetNormal(smoothgr));
			n2 = (normals[mesh->faces[i].getVert(1)].GetNormal(smoothgr));
			n1 = (normals[mesh->faces[i].getVert(2)].GetNormal(smoothgr));
		}
		else
		{
			n1 = (normals[mesh->faces[i].getVert(0)].GetNormal(smoothgr));
			n2 = (normals[mesh->faces[i].getVert(1)].GetNormal(smoothgr));
			n3 = (normals[mesh->faces[i].getVert(2)].GetNormal(smoothgr));
		}
		n1 = -n1;
		n2 = -n2;
		n3 = -n3;
	}


	// UV
	if (mesh->tvFace)
	{
		if (mesh->tvFace[i].Direction(mesh->tvFace[i].getTVert(0), mesh->tvFace[i].getTVert(1))<0)
		{
			uv3 = mesh->tVerts[mesh->tvFace[i].getTVert(0)];
			uv2 = mesh->tVerts[mesh->tvFace[i].getTVert(1)];
			uv1 = mesh->tVerts[mesh->tvFace[i].getTVert(2)];
		}
		else
		{
			uv1 = mesh->tVerts[mesh->tvFace[i].getTVert(0)];
			uv2 = mesh->tVerts[mesh->tvFace[i].getTVert(1)];
			uv3 = mesh->tVerts[mesh->tvFace[i].getTVert(2)];
		}
	}
	else
	{
		uv1 = Point3(1,1,1);
		uv2 = Point3(1,1,1);
		uv3 = Point3(1,1,1);
	}


	char temp[512];

	#ifdef NOX_SWAP_YZ
		printTriangle((unsigned char *) temp, matID,	 
					 v1.x,  v1.z,  -v1.y,  v2.x,  v2.z,  -v2.y,  v3.x,  v3.z,  -v3.y, 
					 n1.x,  n1.z,  -n1.y,  n2.x,  n2.z,  -n2.y,  n3.x,  n3.z,  -n3.y,
					uv1.x, uv1.y, uv2.x, uv2.y, uv3.x, uv3.y);
	#else
		printTriangle((unsigned char *) temp, matID,	 
					 v1.x,  v1.y,  v1.z,  v2.x,  v2.y,  v2.z,  v3.x,  v3.y,  v3.z, 
					 n1.x,  n1.y,  n1.z,  n2.x,  n2.y,  n2.z,  n3.x,  n3.y,  n3.z,
					uv1.x, uv1.y, uv2.x, uv2.y, uv3.x, uv3.y);
	#endif

	// add tri
	xmlNodePtr xTri = xmlNewChild(curNode, NULL, (xmlChar*)"tri", (xmlChar*)temp);
	if (!xTri)
		return false;



	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::printTriangle(unsigned char * buf, unsigned int matID, float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, float v3x, float v3y, float v3z, 
								float n1x, float n1y, float n1z, float n2x, float n2y, float n2z, float n3x, float n3y, float n3z, 
								float uv1x, float uv1y, float uv2x, float uv2y, float uv3x, float uv3y)
{
	if (!buf)
		return false;

	DecHex::uintToHex((unsigned int)matID, buf);	// mat id

	DecHex::floatToHex(v1x, buf+8);
	DecHex::floatToHex(v1y, buf+16);
	DecHex::floatToHex(v1z, buf+24);
	DecHex::floatToHex(v2x, buf+32);
	DecHex::floatToHex(v2y, buf+40);
	DecHex::floatToHex(v2z, buf+48);
	DecHex::floatToHex(v3x, buf+56);
	DecHex::floatToHex(v3y, buf+64);
	DecHex::floatToHex(v3z, buf+72);

	DecHex::floatToHex(-n1x, buf+80);
	DecHex::floatToHex(-n1y, buf+88);
	DecHex::floatToHex(-n1z, buf+96);
	DecHex::floatToHex(-n2x, buf+104);
	DecHex::floatToHex(-n2y, buf+112);
	DecHex::floatToHex(-n2z, buf+120);
	DecHex::floatToHex(-n3x, buf+128);
	DecHex::floatToHex(-n3y, buf+136);
	DecHex::floatToHex(-n3z, buf+144);

	DecHex::floatToHex(uv1x, buf+152);
	DecHex::floatToHex(uv1y, buf+160);
	DecHex::floatToHex(uv2x, buf+168);
	DecHex::floatToHex(uv2y, buf+176);
	DecHex::floatToHex(uv3x, buf+184);
	DecHex::floatToHex(uv3y, buf+192);

	buf[200] = 0;
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertNOXCameraSettings(xmlNodePtr camNode, INode * node)
{
	if (!camNode   ||   !node)
		return false;

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return false;

	if (!objSt.obj->CanConvertToType(NOXCamera_CLASS_ID))
		return true;

	char buf[64];
	NOXCamera * cam = (NOXCamera*)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXCamera_CLASS_ID);
	if (!cam)
	{
		return false;
	}

	// add <dof_on>
	char * dofon = cam->trCam.getDofON() ? "yes" : "no";
	xmlNodePtr xDOF = xmlNewChild(camNode, NULL, (xmlChar*)"dof_on", (xmlChar*)dofon);
	if (!xDOF)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <iso>
	_sprintf_s_l(buf, 64, "%.2f", locale, cam->trCam.getISO());
	xmlNodePtr xISO = xmlNewChild(camNode, NULL, (xmlChar*)"iso", (xmlChar*)buf);
	if (!xISO)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <aperture>
	_sprintf_s_l(buf, 64, "%.2f", locale, cam->trCam.getAperture());
	xmlNodePtr xAperture = xmlNewChild(camNode, NULL, (xmlChar*)"aperture", (xmlChar*)buf);
	if (!xAperture)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <shutter>
	_sprintf_s_l(buf, 64, "%.3f", locale, cam->trCam.getShutter());
	xmlNodePtr xShutter = xmlNewChild(camNode, NULL, (xmlChar*)"shutter", (xmlChar*)buf);
	if (!xShutter)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <autoexposure>
	char * autoexp = cam->trCam.getAutoExpOn() ? "yes" : "no";
	xmlNodePtr xAutoExp = xmlNewChild(camNode, NULL, (xmlChar*)"autoexposure", (xmlChar*)autoexp);
	if (!xAutoExp)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <autoexptype>
	sprintf_s(buf, 64, "%d", cam->trCam.getAutoExpType());
	xmlNodePtr xAutoExpType = xmlNewChild(camNode, NULL, (xmlChar*)"autoexptype", (xmlChar*)buf);
	if (!xAutoExpType)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <autofocus>
	char * autofocus = cam->trCam.getAutoFocusOn() ? "yes" : "no";
	xmlNodePtr xAutoFocus = xmlNewChild(camNode, NULL, (xmlChar*)"autofocus", (xmlChar*)autofocus);
	if (!xAutoFocus)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <focusdist>
	_sprintf_s_l(buf, 64, "%.3f", locale, cam->trCam.getFocusDistance()*unitScale);
	xmlNodePtr xFocusDist = xmlNewChild(camNode, NULL, (xmlChar*)"focusdist", (xmlChar*)buf);
	if (!xFocusDist)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <bladesnumber>
	sprintf_s(buf, 64, "%d", cam->trCam.getBladesNumber());
	xmlNodePtr xBlNum= xmlNewChild(camNode, NULL, (xmlChar*)"bladesnumber", (xmlChar*)buf);
	if (!xBlNum)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <bladesangle>
	_sprintf_s_l(buf, 64, "%.3f", locale, cam->trCam.getBladesAngle());
	xmlNodePtr xBlAngle = xmlNewChild(camNode, NULL, (xmlChar*)"bladesangle", (xmlChar*)buf);
	if (!xBlAngle)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <bladesround>
	char * blround = cam->trCam.getBladesRound() ? "yes" : "no";
	xmlNodePtr xBlRound = xmlNewChild(camNode, NULL, (xmlChar*)"bladesround", (xmlChar*)blround);
	if (!xBlRound)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	// add <bladesradius>
	_sprintf_s_l(buf, 64, "%.3f", locale, cam->trCam.getBladesRadius());
	xmlNodePtr xBlRadius = xmlNewChild(camNode, NULL, (xmlChar*)"bladesradius", (xmlChar*)buf);
	if (!xBlRadius)
	{
		deleteSecondIfCopied(objSt.obj, cam);
		return false;
	}

	deleteSecondIfCopied(objSt.obj, cam);
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertCamera(xmlNodePtr curNode, INode * node)
{
	if (!curNode   ||   !node)
		return false;

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return false;

	char buf[64];
	CameraState cs;
	CameraObject *cam = (CameraObject *)objSt.obj;
	Interval iv;
	iv.SetInfinite();
	cam->EvalCameraState(GetCOREInterface()->GetTime(),iv,&cs);

	Matrix3 cammat = (node->GetObjTMAfterWSM(GetCOREInterface()->GetTime()));
	
	Point4 r1 = cammat.GetRow(0);
	Point4 r2 = cammat.GetRow(1);
	Point4 r3 = cammat.GetRow(2);
	Point4 r4 = cammat.GetRow(3);

	// add <camera>
	xmlNodePtr xCamera = xmlNewChild(curNode, NULL, (xmlChar*)"camera", NULL);
	if (!xCamera)
		return false;

	// add active
	char * activeText = NULL;
	if (!camerasOK)
		activeText= "yes";
	else
	{
		if (objSt.obj->CanConvertToType(NOXCamera_CLASS_ID))
		{
			NOXCamera * tcam = (NOXCamera *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXCamera_CLASS_ID);
			if (!tcam)
				activeText = "no";
			else
			{
				if (tcam->trCam.getIsActive())
					activeText = "yes";
				else
					activeText = "no";
			}
			deleteSecondIfCopied(objSt.obj, tcam);
		}
		else
			activeText = "no";

	}
	xmlAttrPtr xAttr;
	xAttr = xmlNewProp(xCamera, (xmlChar*)"active", (xmlChar*)activeText);
	if (!xAttr)
		return false;

	// add width
	sprintf_s(buf,64, "%d", GetCOREInterface()->GetRendWidth());
	xAttr = xmlNewProp(xCamera, (xmlChar*)"width", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add height
	sprintf_s(buf,64, "%d", GetCOREInterface()->GetRendHeight());
	xAttr = xmlNewProp(xCamera, (xmlChar*)"height", (xmlChar*)buf);
	if (!xAttr)
		return false;

	// add name
	const TCHAR * name = node->GetName();
	if (name)
	{
		char * name_ansi = getAnsiCopy1(name);
		xmlNodePtr xName = xmlNewChild(xCamera, NULL, (xmlChar*)"name", (xmlChar*)name_ansi);
		if (!xName)
			return false;
		free(name_ansi);
	}

	
	// add pos
	r4 *= unitScale;
	#ifdef NOX_SWAP_YZ
		DecHex::floatToHex(r4.x, (unsigned char*)buf+0);
		DecHex::floatToHex(r4.z, (unsigned char*)buf+8);
		DecHex::floatToHex(-r4.y, (unsigned char*)buf+16);
	#else
		DecHex::floatToHex(r4.x, (unsigned char*)buf+0);
		DecHex::floatToHex(r4.y, (unsigned char*)buf+8);
		DecHex::floatToHex(r4.z, (unsigned char*)buf+16);
	#endif
	buf[24]=0;
	xmlNodePtr xPos = xmlNewChild(xCamera, NULL, (xmlChar*)"pos", (xmlChar*)buf);
	if (!xPos)
		return false;

	// add dir
	#ifdef NOX_SWAP_YZ
		DecHex::floatToHex(-r3.x, (unsigned char*)buf+0);
		DecHex::floatToHex(-r3.z, (unsigned char*)buf+8);
		DecHex::floatToHex( r3.y, (unsigned char*)buf+16);
	#else
		DecHex::floatToHex(-r3.x, (unsigned char*)buf+0);
		DecHex::floatToHex(-r3.y, (unsigned char*)buf+8);
		DecHex::floatToHex(-r3.z, (unsigned char*)buf+16);
	#endif
	buf[24]=0;
	xmlNodePtr xDir = xmlNewChild(xCamera, NULL, (xmlChar*)"dir", (xmlChar*)buf);
	if (!xDir)
		return false;

	// add updir
	#ifdef NOX_SWAP_YZ
		DecHex::floatToHex(r2.x, (unsigned char*)buf+0);
		DecHex::floatToHex(r2.z, (unsigned char*)buf+8);
		DecHex::floatToHex(-r2.y, (unsigned char*)buf+16);
	#else
		DecHex::floatToHex(r2.x, (unsigned char*)buf+0);
		DecHex::floatToHex(r2.y, (unsigned char*)buf+8);
		DecHex::floatToHex(r2.z, (unsigned char*)buf+16);
	#endif
	buf[24]=0;
	xmlNodePtr xUp = xmlNewChild(xCamera, NULL, (xmlChar*)"updir", (xmlChar*)buf);
	if (!xUp)
		return false;

	// add rightdir
	#ifdef NOX_SWAP_YZ
		DecHex::floatToHex(r1.x, (unsigned char*)buf+0);
		DecHex::floatToHex(r1.z, (unsigned char*)buf+8);
		DecHex::floatToHex(-r1.y, (unsigned char*)buf+16);
	#else
		DecHex::floatToHex(r1.x, (unsigned char*)buf+0);
		DecHex::floatToHex(r1.y, (unsigned char*)buf+8);
		DecHex::floatToHex(r1.z, (unsigned char*)buf+16);
	#endif
	buf[24]=0;
	xmlNodePtr xRight = xmlNewChild(xCamera, NULL, (xmlChar*)"rightdir", (xmlChar*)buf);
	if (!xRight)
		return false;

	// add angle
	float angle = cs.fov * 180 / PI;
	DecHex::floatToHex(angle, (unsigned char*)buf);
	buf[8]=0;
	xmlNodePtr xAngle = xmlNewChild(xCamera, NULL, (xmlChar*)"angle", (xmlChar*)buf);
	if (!xAngle)
		return false;

	if (!insertNOXCameraSettings(xCamera, node))
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertMaterials(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	// add <materials>
	xmlNodePtr xMaterials = xmlNewChild(curNode, NULL, (xmlChar*)"materials", NULL);
	if (!xMaterials)
		return false;

	for (unsigned int i=0; i<mats.size(); i++)
	{
		if (!insertMaterial(xMaterials, i))
			return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertSunSky(xmlNodePtr curNode, INode * node)
{
	if (!node  ||  !curNode)
		return false;

	NOXSunLight * sun = NULL;
	char buf[64];
	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return false;
	sun = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
	if (!sun)
		return false;

	float azi, alt;
	sun->evalAltitudeAzimuth(alt, azi);

	sun->trsunsky.setPosFromDir(alt, azi);

	// add <sky>
	xmlNodePtr xSun = xmlNewChild(curNode, NULL, (xmlChar*)"sky", NULL);
	if (!xSun)
		return false;

	// add <use_sunsky>
	char * use = sun->trsunsky.getSunSkyON() ? "yes" : "no";
	xmlNodePtr xUse = xmlNewChild(xSun, NULL, (xmlChar*)"use_sunsky", (xmlChar*)use);
	if (!xUse)
		return false;

	// add <sky_model>
	sprintf_s(buf, 64, "%d", sun->trsunsky.getModel());
	xmlNodePtr xModel = xmlNewChild(xSun, NULL, (xmlChar*)"sky_model", (xmlChar*)buf);
	if (!xModel)
		return false;

	// add <month>
	sprintf_s(buf, 64, "%d", sun->trsunsky.getMonth());
	xmlNodePtr xMonth = xmlNewChild(xSun, NULL, (xmlChar*)"month", (xmlChar*)buf);
	if (!xMonth)
		return false;

	// add <day>
	sprintf_s(buf, 64, "%d", sun->trsunsky.getDay());
	xmlNodePtr xDay = xmlNewChild(xSun, NULL, (xmlChar*)"day", (xmlChar*)buf);
	if (!xDay)
		return false;

	// add <hour>
	sprintf_s(buf, 64, "%d", sun->trsunsky.getHour());
	xmlNodePtr xHour= xmlNewChild(xSun, NULL, (xmlChar*)"hour", (xmlChar*)buf);
	if (!xHour)
		return false;

	// add <minute>
	sprintf_s(buf, 64, "%d", sun->trsunsky.getMinute());
	xmlNodePtr xMinute = xmlNewChild(xSun, NULL, (xmlChar*)"minute", (xmlChar*)buf);
	if (!xMinute)
		return false;

	// add <gmt>
	sprintf_s(buf, 64, "%d", sun->trsunsky.getGMT());
	xmlNodePtr xGMT = xmlNewChild(xSun, NULL, (xmlChar*)"gmt", (xmlChar*)buf);
	if (!xGMT)
		return false;

	// add <longitude>
	sprintf_s(buf, 64, "%d", ROUND(-sun->trsunsky.getLongitude()*180/PI));
	xmlNodePtr xLong = xmlNewChild(xSun, NULL, (xmlChar*)"longitude", (xmlChar*)buf);
	if (!xLong)
		return false;

	// add <latitude>
	sprintf_s(buf, 64, "%d", ROUND(-sun->trsunsky.getLatitude()*180/PI));
	xmlNodePtr xLat = xmlNewChild(xSun, NULL, (xmlChar*)"latitude", (xmlChar*)buf);
	if (!xLat)
		return false;

	// add <turbidity>
	_sprintf_s_l(buf, 64, "%.3f", locale, sun->trsunsky.getTurbidity()*10);
	xmlNodePtr xTurb = xmlNewChild(xSun, NULL, (xmlChar*)"turbidity", (xmlChar*)buf);
	if (!xTurb)
		return false;

	// add <aerosol>
	_sprintf_s_l(buf, 64, "%.3f", locale, sun->trsunsky.getAerosol());
	xmlNodePtr xAero = xmlNewChild(xSun, NULL, (xmlChar*)"aerosol", (xmlChar*)buf);
	if (!xAero)
		return false;

	// add <albedo>
	_sprintf_s_l(buf, 64, "%.3f", locale, sun->trsunsky.getAlbedo());
	xmlNodePtr xAlbedo = xmlNewChild(xSun, NULL, (xmlChar*)"albedo", (xmlChar*)buf);
	if (!xAlbedo)
		return false;

	// add <sun_size>
	_sprintf_s_l(buf, 64, "%.6f", locale, sun->trsunsky.getSunSize());
	xmlNodePtr xSunSize = xmlNewChild(xSun, NULL, (xmlChar*)"sun_size", (xmlChar*)buf);
	if (!xSunSize)
		return false;

	// add <sun_on>
	char * usesun = sun->trsunsky.getSunON() ? "yes" : "no";
	xmlNodePtr xSunON = xmlNewChild(xSun, NULL, (xmlChar*)"sun_on", (xmlChar*)usesun);
	if (!xSunON)
		return false;

	// add <sun_other_layer>
	char * uselayer = sun->trsunsky.getSunOtherLayerON() ? "yes" : "no";
	xmlNodePtr xOtherLayer = xmlNewChild(xSun, NULL, (xmlChar*)"sun_other_layer", (xmlChar*)uselayer);
	if (!xOtherLayer)
		return false;


	NOXSunTarget * targ = sun->getTargetObject();
	if (targ)
	{
		Point3 dirN, dirE;
		targ->evalDirections(dirN, dirE);

		char kurwa[256];
		sprintf_s(kurwa, 256, "dirN: %f %f %f\ndirE: %f %f %f", 
			dirN.x, dirN.y, dirN.z, 
			dirE.x, dirE.y, dirE.z
			);


		char buf1[64];
		#ifdef NOX_SWAP_YZ
			DecHex::floatToHex(dirN.x, (unsigned char*)buf1+0);
			DecHex::floatToHex(dirN.z, (unsigned char*)buf1+8);
			DecHex::floatToHex(-dirN.y, (unsigned char*)buf1+16);
			DecHex::floatToHex(dirE.x, (unsigned char*)buf1+24);
			DecHex::floatToHex(dirE.z, (unsigned char*)buf1+32);
			DecHex::floatToHex(-dirE.y, (unsigned char*)buf1+40);
		#else
			DecHex::floatToHex(dirN.x, (unsigned char*)buf1+0);
			DecHex::floatToHex(dirN.y, (unsigned char*)buf1+8);
			DecHex::floatToHex(dirN.z, (unsigned char*)buf1+16);
			DecHex::floatToHex(dirE.x, (unsigned char*)buf1+24);
			DecHex::floatToHex(dirE.y, (unsigned char*)buf1+32);
			DecHex::floatToHex(dirE.z, (unsigned char*)buf1+40);
		#endif
		buf1[48]=0;

		// add <directions>
		xmlNodePtr xDirs = xmlNewChild(xSun, NULL, (xmlChar*)"directions", (xmlChar*)buf1);
		if (!xDirs)
			return false;
	}


	if (sun != objSt.obj)
		sun->DeleteMe();
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertMaterial(xmlNodePtr curNode, int i)
{
	if (!curNode  ||  i<0  ||  i>=(int)mats.size())
		return false;

	Mtl * mat = mats[i];
	bool isNOXmat = matTypes[i];
	if (isNOXmat)
	{
		return insertNOXMaterial(curNode, mat, i);
	}

	char * matname = NULL;
	char * layertype = NULL;
	float ior = 30;
	float dr,dg,db;
	float sr,sg,sb;
	float rough;
	float er,eg,eb;
	float power;
	bool shade;
	if (!mat)
	{	// empty - so default material
		matname = copyStringMax1("Unnamed");
		layertype = "shade";
		ior = 30;
		dr = dg = db = 1;
		sr = sg = sb = 1;
		rough = 100;
		er = eg = eb = 0;
		power = 0;
		shade = true;
	}
	else
	{
		const TCHAR * tmatname = mat->GetName();
		if (tmatname)
			matname = getAnsiCopy1(tmatname);
		if (mat->GetSelfIllumColorOn())
		{
			layertype = "emission";
			er = mat->GetSelfIllumColor().r;
			eg = mat->GetSelfIllumColor().g;
			eb = mat->GetSelfIllumColor().b;
			power = 100;
			dr = 0;
			dg = 0;
			db = 0;
			sr = 0;
			sg = 0;
			sb = 0;
			rough = 100;
			shade = false;
		}
		else
		{
			layertype = "shade";
			dr = mat->GetDiffuse().r;
			dg = mat->GetDiffuse().g;
			db = mat->GetDiffuse().b;
			sr = mat->GetSpecular().r;
			sg = mat->GetSpecular().g;
			sb = mat->GetSpecular().b;
			rough = 100 - 100*mat->GetShininess();
			er = 0;
			eg = 0;
			eb = 0;
			power = 0;
			shade = true;
		}
	}

	// add <material>
	xmlNodePtr xMaterial = xmlNewChild(curNode, NULL, (xmlChar*)"material", NULL);
	if (!xMaterial)
		return false;

	char buf[128];
	DecHex::uintToHex(i, (unsigned char*)buf);
	buf[8] = 0;
	xmlAttrPtr xAttr;
	xAttr = xmlNewProp(xMaterial, (xmlChar*)"id", (xmlChar*)buf);
	if (!xAttr)
		return false;

	if (matname)
	{
		// add <name>
		xmlNodePtr xName = xmlNewChild(xMaterial, NULL, (xmlChar*)"name", (xmlChar*)matname);
		if (!xName)
			return false;
		free(matname);
		matname = NULL;
	}

	// add blend layer
	xmlNodePtr xBlend = xmlNewChild(xMaterial, NULL, (xmlChar*)"blendlayer", (xmlChar *)"1");
	if (!xBlend)
		return false;

	// add <layer>
	xmlNodePtr xLayer = xmlNewChild(xMaterial, NULL, (xmlChar*)"layer", NULL);
	if (!xLayer)
		return false;

	// add type
	xAttr = xmlNewProp(xLayer, (xmlChar*)"type", (xmlChar*)layertype);
	if (!xAttr)
		return false;

	// add contribution
	xAttr = xmlNewProp(xLayer, (xmlChar*)"contribution", (xmlChar*)"100");
	if (!xAttr)
		return false;


	if (shade)
	{
		// add <diffuse>
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dr * 255))), (unsigned char *)buf);
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dg * 255))), (unsigned char *)buf+2);
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, db * 255))), (unsigned char *)buf+4);
		buf[6] = 0;
		xmlNodePtr xDiff = xmlNewChild(xLayer, NULL, (xmlChar*)"diffuse", (xmlChar *)buf);
		if (!xDiff)
			return false;

		// add <diffuse90>
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, sr * 255))), (unsigned char *)buf);
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, sg * 255))), (unsigned char *)buf+2);
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, sb * 255))), (unsigned char *)buf+4);
		buf[6] = 0;
		xmlNodePtr xDiff90 = xmlNewChild(xLayer, NULL, (xmlChar*)"diffuse90", (xmlChar *)buf);
		if (!xDiff90)
			return false;

		// add <roughness>
		_sprintf_s_l(buf, 128, "%.2f", locale, min(100, max(0, rough*100.0f)));
		xmlNodePtr xRough = xmlNewChild(xLayer, NULL, (xmlChar*)"roughness", (xmlChar *)buf);
		if (!xRough)
			return false;

		// add <fresnel>
		xmlNodePtr xFres = xmlNewChild(xLayer, NULL, (xmlChar*)"fresnel", NULL);
		if (!xFres)
			return false;

		// add <ior>
		_sprintf_s_l(buf, 128, "%.2f", locale, min(100, max(0, ior)));
		xmlNodePtr xIOR= xmlNewChild(xFres, NULL, (xmlChar*)"ior", (xmlChar *)buf);
		if (!xIOR)
			return false;

	}
	else
	{
		// add <emission>
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, er * 255))), (unsigned char *)buf);
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, eg * 255))), (unsigned char *)buf+2);
		DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, eb * 255))), (unsigned char *)buf+4);
		buf[6] = 0;
		xmlNodePtr xEmission = xmlNewChild(xLayer, NULL, (xmlChar*)"emission", (xmlChar *)buf);
		if (!xEmission)
			return false;

		// add <power>
		DecHex::floatToHex(power, (unsigned char *)buf);
		buf[8] = 0;
		xmlNodePtr xPower = xmlNewChild(xLayer, NULL, (xmlChar*)"power", (xmlChar *)buf);
		if (!xPower)
			return false;
	}


	return true;
}

bool XMLScene::insertNOXMaterial(xmlNodePtr curNode, Mtl * mat, int id)
{
	NOXMaterial * nmat = (NOXMaterial*)mat;

	xmlNodePtr xMaterial = xmlNewChild(curNode, NULL, (xmlChar*)"material", NULL);
	if (!xMaterial)
		return false;

	char buf[128];
	DecHex::uintToHex(id, (unsigned char*)buf);
	buf[8] = 0;
	xmlAttrPtr xAttr;
	xAttr = xmlNewProp(xMaterial, (xmlChar*)"id", (xmlChar*)buf);
	if (!xAttr)
		return false;

	if (nmat->GetName())
	{
		// add <name>
		#ifdef ISMAX2013
			const TCHAR * tname = nmat->GetName().ToMCHAR();
		#else
			const TCHAR * tname = nmat->GetName();
		#endif
		char * name_ansi = getAnsiCopy1(tname);
		xmlNodePtr xName = xmlNewChild(xMaterial, NULL, (xmlChar*)"name", (xmlChar*)name_ansi);
		if (!xName)
			return false;
		free(name_ansi);
	}

	// add <blendlayer>
	sprintf_s(buf, 128, "%d", (nmat->mat.getBlendLayerNumber()+1));
	xmlNodePtr xBlend = xmlNewChild(xMaterial, NULL, (xmlChar*)"blendlayer", (xmlChar *)buf);
	if (!xBlend)
		return false;

	if (!insertPreview(xMaterial, mat))
		return false;

	// add <opacity>
	float opacity = nmat->mat.getOpacity();
	_sprintf_s_l(buf, 128, "%.2f", locale, min(100, max(0, opacity*100.0f)));
	xmlNodePtr xOpac = xmlNewChild(xMaterial, NULL, (xmlChar*)"opacity", (xmlChar *)buf);
	if (!xOpac)
		return false;

	// add <displacement_depth>
	float displ_depth = nmat->mat.getDisplacementDepth();
	_sprintf_s_l(buf, 128, "%.3f", locale, displ_depth);
	xmlNodePtr xDisplDepth = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_depth", (xmlChar *)buf);
	if (!xDisplDepth)
		return false;

	// add <displacement_subdivs>
	int displ_subdivs = nmat->mat.getDisplacementSubdivs();
	_sprintf_s_l(buf, 128, "%d", locale, displ_subdivs);
	xmlNodePtr xDisplSubdivs = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_subdivs", (xmlChar *)buf);
	if (!xDisplSubdivs)
		return false;

	// add <displacement_normal_mode>
	int displ_normal = nmat->mat.getDisplacementNormalMode();
	_sprintf_s_l(buf, 128, "%d", locale, displ_normal);
	xmlNodePtr xDisplNormals = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_normal_mode", (xmlChar *)buf);
	if (!xDisplNormals)
		return false;

	// add <displacement_shift>
	float displ_shift = nmat->mat.getDisplacementShift();
	_sprintf_s_l(buf, 128, "%.3f", locale, displ_shift);
	xmlNodePtr xDisplShift = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_shift", (xmlChar *)buf);
	if (!xDisplShift)
		return false;

	// add <displacement_continuity>
	xmlNodePtr xDisplCont = NULL;
	if (nmat->mat.getDisplacementContinuity())
		xDisplCont = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_continuity", (xmlChar *)"yes");
	else
		xDisplCont = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_continuity", (xmlChar *)"no");
	if (!xDisplCont)
		return false;

	// add <displacement_ignore_scale>
	xmlNodePtr xDisplIgnSc = NULL;
	if (nmat->mat.getDisplacementIgnoreScale())
		xDisplIgnSc = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_ignore_scale", (xmlChar *)"yes");
	else
		xDisplIgnSc = xmlNewChild(xMaterial, NULL, (xmlChar*)"displacement_ignore_scale", (xmlChar *)"no");
	if (!xDisplIgnSc)
		return false;


	if (!insertTexture(xMaterial, mat, 0, MAT_TEX_SLOT_DISPLACEMENT))
		return false;
	if (!insertTexture(xMaterial, mat, 0, MAT_TEX_SLOT_OPACITY))
		return false;


	for (int i=0; i<nmat->mat.getLayersNumber(); i++)
	{
		// add <layer>
		xmlNodePtr xLayer = xmlNewChild(xMaterial, NULL, (xmlChar*)"layer", NULL);
		if (!xLayer)
			return false;

		bool shade = (nmat->mat.getLayerType(i) == 2);

		// add type
		xAttr = xmlNewProp(xLayer, (xmlChar*)"type", (xmlChar*)(shade?"shade":"emission"));
		if (!xAttr)
			return false;

		// add contribution
		int contr = nmat->mat.getLayerContribution(i);
		sprintf_s(buf, 128, "%d", contr);
		xAttr = xmlNewProp(xLayer, (xmlChar*)"contribution", (xmlChar*)buf);
		if (!xAttr)
			return false;


		if (shade)
		{
			// add <diffuse>
			float dr,dg,db,da;
			nmat->mat.getLayerColor0(i, dr,dg,db,da);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dr * 255))), (unsigned char *)buf);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dg * 255))), (unsigned char *)buf+2);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, db * 255))), (unsigned char *)buf+4);
			buf[6] = 0;
			xmlNodePtr xDiff = xmlNewChild(xLayer, NULL, (xmlChar*)"diffuse", (xmlChar *)buf);
			if (!xDiff)
				return false;

			// add <diffuse90>
			nmat->mat.getLayerColor90(i, dr,dg,db,da);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dr * 255))), (unsigned char *)buf);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dg * 255))), (unsigned char *)buf+2);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, db * 255))), (unsigned char *)buf+4);
			buf[6] = 0;
			xmlNodePtr xDiff90 = xmlNewChild(xLayer, NULL, (xmlChar*)"diffuse90", (xmlChar *)buf);
			if (!xDiff90)
				return false;

			// add connect90
			xmlNodePtr xC90 = xmlNewChild(xLayer, NULL, (xmlChar*)"connect90", (xmlChar *)NULL);
			if (nmat->mat.getLayerConnect90(i))
				xAttr = xmlNewProp(xC90, (xmlChar*)"active", (xmlChar*)"yes");
			else
				xAttr = xmlNewProp(xC90, (xmlChar*)"active", (xmlChar*)"no");
			if (!xAttr)
				return false;

			// add roughness
			float rough = nmat->mat.getLayerRoughness(i);
			_sprintf_s_l(buf, 128, "%.2f", locale, min(100, max(0, rough*100.0f)));
			xmlNodePtr xRough = xmlNewChild(xLayer, NULL, (xmlChar*)"roughness", (xmlChar *)buf);
			if (!xRough)
				return false;

			// add anisotropy
			float aniso = nmat->mat.getLayerAniso(i);
			_sprintf_s_l(buf, 128, "%.2f", locale, min(100, max(0, aniso*100.0f)));
			xmlNodePtr xAniso = xmlNewChild(xLayer, NULL, (xmlChar*)"anisotropy", (xmlChar *)buf);
			if (!xAniso)
				return false;

			// add anisotropy_angle
			float anisoangle = nmat->mat.getLayerAnisoAngle(i);
			_sprintf_s_l(buf, 128, "%.2f", locale, min(180, max(-180, anisoangle)));
			xmlNodePtr xAnisoAngle = xmlNewChild(xLayer, NULL, (xmlChar*)"anisotropy_angle", (xmlChar *)buf);
			if (!xAnisoAngle)
				return false;

			// add fresnel
			xmlNodePtr xFres = xmlNewChild(xLayer, NULL, (xmlChar*)"fresnel", NULL);
			if (!xFres)
				return false;

			// add ior
			float ior = nmat->mat.getLayerFresnelIOR(i);
			_sprintf_s_l(buf, 128, "%.2f", locale, min(100, max(0, ior)));
			xmlNodePtr xIOR= xmlNewChild(xFres, NULL, (xmlChar*)"ior", (xmlChar *)buf);
			if (!xIOR)
				return false;


			// add transmission
			xmlNodePtr xTr = xmlNewChild(xLayer, NULL, (xmlChar*)"transmission", (xmlChar *)NULL);
			if (nmat->mat.getLayerTransmOn(i))
				xAttr = xmlNewProp(xTr, (xmlChar*)"active", (xmlChar*)"yes");
			else
				xAttr = xmlNewProp(xTr, (xmlChar*)"active", (xmlChar*)"no");
			if (!xAttr)
				return false;

			// add <transmcolor>
			nmat->mat.getLayerTransmColor(i, dr,dg,db,da);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dr * 255))), (unsigned char *)buf);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dg * 255))), (unsigned char *)buf+2);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, db * 255))), (unsigned char *)buf+4);
			buf[6] = 0;
			xmlNodePtr xTrCol = xmlNewChild(xTr, NULL, (xmlChar*)"transmcolor", (xmlChar *)buf);
			if (!xTrCol)
				return false;

			// add fake glass
			xmlNodePtr xFakeGlass = NULL;
			if (nmat->mat.getLayerFakeGlass(i))
				xFakeGlass = xmlNewChild(xTr, NULL, (xmlChar*)"fakeglass", (xmlChar *)"yes");
			else
				xFakeGlass = xmlNewChild(xTr, NULL, (xmlChar*)"fakeglass", (xmlChar *)"no");
			if (!xFakeGlass)
				return false;

			// add absorption
			xmlNodePtr xAbs = xmlNewChild(xTr, NULL, (xmlChar*)"absorption", (xmlChar *)NULL);
			if (nmat->mat.getLayerAbsOn(i))
				xAttr = xmlNewProp(xAbs, (xmlChar*)"active", (xmlChar*)"yes");
			else
				xAttr = xmlNewProp(xAbs, (xmlChar*)"active", (xmlChar*)"no");
			if (!xAttr)
				return false;

			// add abscolor
			nmat->mat.getLayerAbsColor(i, dr,dg,db,da);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dr * 255))), (unsigned char *)buf);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dg * 255))), (unsigned char *)buf+2);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, db * 255))), (unsigned char *)buf+4);
			buf[6] = 0;
			xmlNodePtr xAbsc= xmlNewChild(xAbs, NULL, (xmlChar*)"abscolor", (xmlChar *)buf);
			if (!xAbsc)
				return false;

			// add distance
			float adist = nmat->mat.getLayerAbsDist(i);
			_sprintf_s_l(buf, 128, "%1.3f", locale, adist);
			xmlNodePtr xAbsd = xmlNewChild(xAbs, NULL, (xmlChar*)"distance", (xmlChar *)buf);
			if (!xAbsd)
				return false;

			// add dispersion_on
			xmlNodePtr xDispOn = NULL;
			if (nmat->mat.getLayerDispersionOn(i))
				xDispOn = xmlNewChild(xTr, NULL, (xmlChar*)"dispersion_on", (xmlChar *)"yes");
			else
				xDispOn = xmlNewChild(xTr, NULL, (xmlChar*)"dispersion_on", (xmlChar *)"no");
			if (!xDispOn)
				return false;

			// add dispersion_val
			float disp = nmat->mat.getLayerDispersionValue(i);
			_sprintf_s_l(buf, 128, "%1.3f", locale, disp);
			xmlNodePtr xDispVal = xmlNewChild(xTr, NULL, (xmlChar*)"dispersion_val", (xmlChar *)buf);
			if (!xDispVal)
				return false;

			// add sss
			xmlNodePtr xSSS = xmlNewChild(xLayer, NULL, (xmlChar*)"sss", (xmlChar *)NULL);
			if (nmat->mat.getLayerSSSOn(i))
				xAttr = xmlNewProp(xSSS, (xmlChar*)"active", (xmlChar*)"yes");
			else
				xAttr = xmlNewProp(xSSS, (xmlChar*)"active", (xmlChar*)"no");
			if (!xAttr)
				return false;

			// add sss_density
			float sss_dens = nmat->mat.getLayerSSSDensity(i);
			_sprintf_s_l(buf, 128, "%1.3f", locale, sss_dens);
			xmlNodePtr xSssDens = xmlNewChild(xSSS, NULL, (xmlChar*)"sss_density", (xmlChar *)buf);
			if (!xSssDens)
				return false;

			// add sss_coll_dir
			float sss_coll_dir = nmat->mat.getLayerSSSCollDirection(i);
			_sprintf_s_l(buf, 128, "%1.3f", locale, sss_coll_dir);
			xmlNodePtr xSssCollDir = xmlNewChild(xSSS, NULL, (xmlChar*)"sss_coll_dir", (xmlChar *)buf);
			if (!xSssCollDir)
				return false;

			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_COLOR0))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_COLOR90))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_ROUGHNESS))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_WEIGHT))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_NORMAL))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_TRANSM))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_ANISO))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_ANISO_ANGLE))
				return false;

		}
		else
		{
			// add <emission>
			float dr,dg,db,da;
			nmat->mat.getLayerEmissionColor(i, dr,dg,db,da);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dr * 255))), (unsigned char *)buf);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, dg * 255))), (unsigned char *)buf+2);
			DecHex::ucharToHex2BigEndian(unsigned char(min(255, max(0, db * 255))), (unsigned char *)buf+4);
			buf[6] = 0;
			xmlNodePtr xEmission = xmlNewChild(xLayer, NULL, (xmlChar*)"emission", (xmlChar *)buf);
			if (!xEmission)
				return false;

			// add <temperature>
			int temperature = nmat->mat.getLayerEmissionTemperature(i);
			sprintf_s(buf, 128, "%d", temperature);
			xmlNodePtr xTemperature = xmlNewChild(xLayer, NULL, (xmlChar*)"temperature", (xmlChar *)buf);
			if (!xTemperature)
				return false;

			// add <power>
			float power = nmat->mat.getLayerEmissionPower(i);
			DecHex::floatToHex(power, (unsigned char *)buf);
			buf[8] = 0;
			xmlNodePtr xPower = xmlNewChild(xLayer, NULL, (xmlChar*)"power", (xmlChar *)buf);
			if (!xPower)
				return false;

			// add <unit>
			int unit = nmat->mat.getLayerEmissionUnit(i);
			buf[0] = 0;
			switch (unit)
			{
				case 1:  sprintf_s(buf, 128, "w"); break;
				case 2:  sprintf_s(buf, 128, "w/m2"); break;
				case 3:  sprintf_s(buf, 128, "lm"); break;
				case 4:  sprintf_s(buf, 128, "lx"); break;
				default: sprintf_s(buf, 128, "w"); break;
			}
			xmlNodePtr xUnit = xmlNewChild(xLayer, NULL, (xmlChar*)"unit", (xmlChar *)buf);
			if (!xUnit)
				return false;

			// add <ies>
			char * iesfname = nmat->mat.getLayerEmissionIes(i);
			if (iesfname)
			{
				xmlNodePtr xIES= xmlNewChild(xLayer, NULL, (xmlChar*)"ies", (xmlChar *)iesfname);
				if (!xIES)
					return false;
			}

			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_WEIGHT))
				return false;
			if (!insertTexture(xLayer, mat, i, LAYER_TEX_SLOT_LIGHT))
				return false;
		}
	}

	return true;
}

bool XMLScene::insertTexture(xmlNodePtr curNode, Mtl * mat, int layer, int slot)
{
	NOXMaterial * nmat = (NOXMaterial*)mat;
	if (!nmat)
		return false;

	int numLayers = nmat->mat.getLayersNumber();
	if (layer<0 || layer>=numLayers)
		return false;

	char * tex = NULL;
	bool slot_ok = false;

	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			slot_ok = true;		tex = nmat->mat.getLayerTexRefl0(layer);		break;
		case LAYER_TEX_SLOT_COLOR90:		slot_ok = true;		tex = nmat->mat.getLayerTexRefl90(layer);		break;
		case LAYER_TEX_SLOT_ROUGHNESS:		slot_ok = true;		tex = nmat->mat.getLayerTexRough(layer);		break;
		case LAYER_TEX_SLOT_WEIGHT:			slot_ok = true;		tex = nmat->mat.getLayerTexWeight(layer);		break;
		case LAYER_TEX_SLOT_LIGHT:			slot_ok = true;		tex = nmat->mat.getLayerTexLight(layer);		break;
		case LAYER_TEX_SLOT_NORMAL:			slot_ok = true;		tex = nmat->mat.getLayerTexNormal(layer);		break;
		case LAYER_TEX_SLOT_TRANSM:			slot_ok = true;		tex = nmat->mat.getLayerTexTransm(layer);		break;
		case LAYER_TEX_SLOT_ANISO:			slot_ok = true;		tex = nmat->mat.getLayerTexAniso(layer);		break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:	slot_ok = true;		tex = nmat->mat.getLayerTexAnisoAngle(layer);	break;
		case MAT_TEX_SLOT_OPACITY:			slot_ok = true;		tex = nmat->mat.getTexOpacity();				break;
		case MAT_TEX_SLOT_DISPLACEMENT:		slot_ok = true;		tex = nmat->mat.getTexDisplacement();			break;
	}

	if (!slot_ok)
		return false;

	// check if tex filename exists
	if (!tex)
		return true;

	if (!insertTextureTypeFile(curNode, mat, layer, slot))
		return false;

	return true;
}

bool XMLScene::insertTextureTypeFile(xmlNodePtr curNode, Mtl * mat, int layer, int slot)
{
	if (!curNode)
		return false;
	NOXMaterial * nmat = (NOXMaterial*)mat;
	if (!nmat)
		return false;

	int numLayers = nmat->mat.getLayersNumber();
	if (layer<0 || layer>=numLayers)
		return false;

	char buf[64];
	char * tex = NULL;
	char * tagname = NULL;
	bool enabled = false;
	bool texfilter = false;
	bool texprecalc = false;
	float brightness = 0.0f;
	float contrast = 1.0f;
	float saturation = 0.0f;
	float hue = 0.0f;
	float red = 0.0f;
	float green = 0.0f;
	float blue = 0.0f;
	float gamma = 2.2f;
	bool invert = false;
	bool invert_x = false;
	bool invert_y = false;
	float powerEV = 0.0f;

	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:		
			tagname = "tex_refl0";		
			tex = nmat->mat.getLayerTexRefl0(layer);
			enabled = nmat->mat.getLayerUseTexRefl0(layer);
			texfilter = nmat->mat.getLayerTexPostFilterRefl0(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateRefl0(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessRefl0(layer);
			contrast = nmat->mat.getLayerTexPostContrastRefl0(layer);
			saturation = nmat->mat.getLayerTexPostSaturationRefl0(layer);
			hue = nmat->mat.getLayerTexPostHueRefl0(layer);
			red = nmat->mat.getLayerTexPostRedRefl0(layer);
			green = nmat->mat.getLayerTexPostGreenRefl0(layer);
			blue = nmat->mat.getLayerTexPostBlueRefl0(layer);
			gamma = nmat->mat.getLayerTexPostGammaRefl0(layer);
			invert = nmat->mat.getLayerTexPostInvertRefl0(layer);
			break;
		case LAYER_TEX_SLOT_COLOR90:	
			tagname = "tex_refl90";		
			tex = nmat->mat.getLayerTexRefl90(layer);
			enabled = nmat->mat.getLayerUseTexRefl90(layer);
			texfilter = nmat->mat.getLayerTexPostFilterRefl90(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateRefl90(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessRefl90(layer);
			contrast = nmat->mat.getLayerTexPostContrastRefl90(layer);
			saturation = nmat->mat.getLayerTexPostSaturationRefl90(layer);
			hue = nmat->mat.getLayerTexPostHueRefl90(layer);
			red = nmat->mat.getLayerTexPostRedRefl90(layer);
			green = nmat->mat.getLayerTexPostGreenRefl90(layer);
			blue = nmat->mat.getLayerTexPostBlueRefl90(layer);
			gamma = nmat->mat.getLayerTexPostGammaRefl90(layer);
			invert = nmat->mat.getLayerTexPostInvertRefl90(layer);
			break;
		case LAYER_TEX_SLOT_TRANSM:	
			tagname = "tex_transm";		
			tex = nmat->mat.getLayerTexTransm(layer);
			enabled = nmat->mat.getLayerUseTexTransm(layer);
			texfilter = nmat->mat.getLayerTexPostFilterTransm(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateTransm(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessTransm(layer);
			contrast = nmat->mat.getLayerTexPostContrastTransm(layer);
			saturation = nmat->mat.getLayerTexPostSaturationTransm(layer);
			hue = nmat->mat.getLayerTexPostHueTransm(layer);
			red = nmat->mat.getLayerTexPostRedTransm(layer);
			green = nmat->mat.getLayerTexPostGreenTransm(layer);
			blue = nmat->mat.getLayerTexPostBlueTransm(layer);
			gamma = nmat->mat.getLayerTexPostGammaTransm(layer);
			invert = nmat->mat.getLayerTexPostInvertTransm(layer);
			break;
		case LAYER_TEX_SLOT_ROUGHNESS:	
			tagname = "tex_rough";		
			tex = nmat->mat.getLayerTexRough(layer);
			enabled = nmat->mat.getLayerUseTexRough(layer);
			texfilter = nmat->mat.getLayerTexPostFilterRough(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateRough(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessRough(layer);
			contrast = nmat->mat.getLayerTexPostContrastRough(layer);
			saturation = nmat->mat.getLayerTexPostSaturationRough(layer);
			hue = nmat->mat.getLayerTexPostHueRough(layer);
			red = nmat->mat.getLayerTexPostRedRough(layer);
			green = nmat->mat.getLayerTexPostGreenRough(layer);
			blue = nmat->mat.getLayerTexPostBlueRough(layer);
			gamma = nmat->mat.getLayerTexPostGammaRough(layer);
			invert = nmat->mat.getLayerTexPostInvertRough(layer);
			break;
		case LAYER_TEX_SLOT_ANISO:	
			tagname = "tex_aniso";		
			tex = nmat->mat.getLayerTexAniso(layer);
			enabled = nmat->mat.getLayerUseTexAniso(layer);
			texfilter = nmat->mat.getLayerTexPostFilterAniso(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateAniso(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessAniso(layer);
			contrast = nmat->mat.getLayerTexPostContrastAniso(layer);
			saturation = nmat->mat.getLayerTexPostSaturationAniso(layer);
			hue = nmat->mat.getLayerTexPostHueAniso(layer);
			red = nmat->mat.getLayerTexPostRedAniso(layer);
			green = nmat->mat.getLayerTexPostGreenAniso(layer);
			blue = nmat->mat.getLayerTexPostBlueAniso(layer);
			gamma = nmat->mat.getLayerTexPostGammaAniso(layer);
			invert = nmat->mat.getLayerTexPostInvertAniso(layer);
			break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:	
			tagname = "tex_aniso_angle";		
			tex = nmat->mat.getLayerTexAnisoAngle(layer);
			enabled = nmat->mat.getLayerUseTexAnisoAngle(layer);
			texfilter = nmat->mat.getLayerTexPostFilterAnisoAngle(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateAnisoAngle(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessAnisoAngle(layer);
			contrast = nmat->mat.getLayerTexPostContrastAnisoAngle(layer);
			saturation = nmat->mat.getLayerTexPostSaturationAnisoAngle(layer);
			hue = nmat->mat.getLayerTexPostHueAnisoAngle(layer);
			red = nmat->mat.getLayerTexPostRedAnisoAngle(layer);
			green = nmat->mat.getLayerTexPostGreenAnisoAngle(layer);
			blue = nmat->mat.getLayerTexPostBlueAnisoAngle(layer);
			gamma = nmat->mat.getLayerTexPostGammaAnisoAngle(layer);
			invert = nmat->mat.getLayerTexPostInvertAnisoAngle(layer);
			break;
		case LAYER_TEX_SLOT_WEIGHT:		
			tagname = "tex_weight";		
			tex = nmat->mat.getLayerTexWeight(layer);
			enabled = nmat->mat.getLayerUseTexWeight(layer);
			texfilter = nmat->mat.getLayerTexPostFilterWeight(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateWeight(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessWeight(layer);
			contrast = nmat->mat.getLayerTexPostContrastWeight(layer);
			saturation = nmat->mat.getLayerTexPostSaturationWeight(layer);
			hue = nmat->mat.getLayerTexPostHueWeight(layer);
			red = nmat->mat.getLayerTexPostRedWeight(layer);
			green = nmat->mat.getLayerTexPostGreenWeight(layer);
			blue = nmat->mat.getLayerTexPostBlueWeight(layer);
			gamma = nmat->mat.getLayerTexPostGammaWeight(layer);
			invert = nmat->mat.getLayerTexPostInvertWeight(layer);
			break;
		case LAYER_TEX_SLOT_LIGHT:		
			tagname = "tex_light";		
			tex = nmat->mat.getLayerTexLight(layer);	
			enabled = nmat->mat.getLayerUseTexLight(layer);
			texfilter = nmat->mat.getLayerTexPostFilterLight(layer);
			texprecalc = nmat->mat.getLayerTexPrecalculateLight(layer);
			brightness = nmat->mat.getLayerTexPostBrightnessLight(layer);
			contrast = nmat->mat.getLayerTexPostContrastLight(layer);
			saturation = nmat->mat.getLayerTexPostSaturationLight(layer);
			hue = nmat->mat.getLayerTexPostHueLight(layer);
			red = nmat->mat.getLayerTexPostRedLight(layer);
			green = nmat->mat.getLayerTexPostGreenLight(layer);
			blue = nmat->mat.getLayerTexPostBlueLight(layer);
			gamma = nmat->mat.getLayerTexPostGammaLight(layer);
			invert = nmat->mat.getLayerTexPostInvertLight(layer);
			break;
		case MAT_TEX_SLOT_OPACITY:		
			tagname = "tex_opacity";	
			tex = nmat->mat.getTexOpacity();			
			enabled = nmat->mat.getUseTexOpacity();
			texfilter = nmat->mat.getTexPostFilterOpacity();
			texprecalc = nmat->mat.getTexPrecalculateOpacity();
			brightness = nmat->mat.getTexPostBrightnessOpacity();
			contrast = nmat->mat.getTexPostContrastOpacity();
			saturation = nmat->mat.getTexPostSaturationOpacity();
			hue = nmat->mat.getTexPostHueOpacity();
			red = nmat->mat.getTexPostRedOpacity();
			green = nmat->mat.getTexPostGreenOpacity();
			blue = nmat->mat.getTexPostBlueOpacity();
			gamma = nmat->mat.getTexPostGammaOpacity();
			invert = nmat->mat.getTexPostInvertOpacity();
			break;
		case MAT_TEX_SLOT_DISPLACEMENT:		
			tagname = "tex_displacement";	
			tex = nmat->mat.getTexDisplacement();			
			enabled = nmat->mat.getUseTexDisplacement();
			texfilter = nmat->mat.getTexPostFilterDisplacement();
			texprecalc = 0;//nmat->mat.getTexPrecalculate();
			brightness = nmat->mat.getTexPostBrightnessDisplacement();
			contrast = nmat->mat.getTexPostContrastDisplacement();
			saturation = nmat->mat.getTexPostSaturationDisplacement();
			hue = nmat->mat.getTexPostHueDisplacement();
			red = nmat->mat.getTexPostRedDisplacement();
			green = nmat->mat.getTexPostGreenDisplacement();
			blue = nmat->mat.getTexPostBlueDisplacement();
			gamma = nmat->mat.getTexPostGammaDisplacement();
			invert = nmat->mat.getTexPostInvertDisplacement();
			break;
		case LAYER_TEX_SLOT_NORMAL:		
			invert_x = nmat->mat.getLayerTexPostNormalInvertX(layer);
			invert_y = nmat->mat.getLayerTexPostNormalInvertY(layer);
			powerEV = nmat->mat.getLayerTexPostNormalPower(layer);
			break;
	}

	if (!tagname)
		return false;

	// add <texture_slot_dependent_name>
	xmlNodePtr xTex = xmlNewChild(curNode, NULL, (xmlChar*)tagname, NULL);
	if (!xTex)
		return false;

	// add attribute type
	xmlAttrPtr xAttrType;
	xAttrType = xmlNewProp(xTex, (xmlChar*)"type", (xmlChar*)"file");
	if (!xAttrType)
		return false;

	// add attribute enabled
	xmlAttrPtr xAttrEnabled;
	xAttrEnabled = xmlNewProp(xTex, (xmlChar*)"enabled", (xmlChar*)(enabled ? "yes" : "no"));
	if (!xAttrEnabled)
		return false;

	// add <filename>
	if (tex)
	{
		xmlNodePtr xFilename = xmlNewChild(xTex, NULL, (xmlChar*)"filename", (xmlChar *)tex);
		if (!xFilename)
			return false;
	}

	// add <filter>
	char * ffilter = texfilter ? "1" : "0";
	xmlNodePtr xFilter = xmlNewChild(xTex, NULL, (xmlChar*)"filter", (xmlChar *)ffilter);
	if (!xFilter)
		return false;

	// add <precalc>	// deprecated
	char * pprecalc = texprecalc ? "1" : "0";
	xmlNodePtr xPrecalc = xmlNewChild(xTex, NULL, (xmlChar*)"precalc", (xmlChar *)pprecalc);
	if (!xPrecalc)
		return false;

	// add <post>
	xmlNodePtr xPost = xmlNewChild(xTex, NULL, (xmlChar*)"post", (xmlChar *)NULL);
	if (!xPost)
		return false;

		// add <brightness>
		_sprintf_s_l(buf, 64, "%.4f", locale, brightness);
		xmlNodePtr xBrightness = xmlNewChild(xPost, NULL, (xmlChar*)"brightness", (xmlChar *)buf);
		if (!xBrightness)
			return false;

		// add <contrast>
		_sprintf_s_l(buf, 64, "%.4f", locale, contrast);
		xmlNodePtr xContrast = xmlNewChild(xPost, NULL, (xmlChar*)"contrast", (xmlChar *)buf);
		if (!xContrast)
			return false;

		// add <saturation>
		_sprintf_s_l(buf, 64, "%.4f", locale, saturation);
		xmlNodePtr xSaturation = xmlNewChild(xPost, NULL, (xmlChar*)"saturation", (xmlChar *)buf);
		if (!xSaturation)
			return false;

		// add <hue>
		_sprintf_s_l(buf, 64, "%.4f", locale, hue);
		xmlNodePtr xHue = xmlNewChild(xPost, NULL, (xmlChar*)"hue", (xmlChar *)buf);
		if (!xHue)
			return false;

		// add <red>
		_sprintf_s_l(buf, 64, "%.4f", locale, red);
		xmlNodePtr xRed = xmlNewChild(xPost, NULL, (xmlChar*)"red", (xmlChar *)buf);
		if (!xRed)
			return false;

		// add <green>
		_sprintf_s_l(buf, 64, "%.4f", locale, green);
		xmlNodePtr xGreen = xmlNewChild(xPost, NULL, (xmlChar*)"green", (xmlChar *)buf);
		if (!xGreen)
			return false;

		// add <blue>
		_sprintf_s_l(buf, 64, "%.4f", locale, blue);
		xmlNodePtr xBlue = xmlNewChild(xPost, NULL, (xmlChar*)"blue", (xmlChar *)buf);
		if (!xBlue)
			return false;

		// add <gamma>
		_sprintf_s_l(buf, 64, "%.4f", locale, gamma);
		xmlNodePtr xGamma = xmlNewChild(xPost, NULL, (xmlChar*)"gamma", (xmlChar *)buf);
		if (!xGamma)
			return false;

		// add <invert>
		sprintf_s(buf, 64, "%s", (invert ? "yes" : "no"));
		xmlNodePtr xInvert = xmlNewChild(xPost, NULL, (xmlChar*)"invert", (xmlChar *)buf);
		if (!xInvert)
			return false;

	// add <post_normal>
	xmlNodePtr xPostNorm = xmlNewChild(xTex, NULL, (xmlChar*)"post_normal", (xmlChar *)NULL);
	if (!xPostNorm)
		return false;

		// add <pn_power>
		_sprintf_s_l(buf, 64, "%.4f", locale, powerEV);
		xmlNodePtr xPower = xmlNewChild(xPostNorm, NULL, (xmlChar*)"pn_power", (xmlChar *)buf);
		if (!xPower)
			return false;

		// add <pn_invert_x>
		sprintf_s(buf, 64, "%s", (invert_x ? "yes" : "no"));
		xmlNodePtr xInvert_x = xmlNewChild(xPostNorm, NULL, (xmlChar*)"pn_invert_x", (xmlChar *)buf);
		if (!xInvert_x)
			return false;

		// add <pn_invert_y>
		sprintf_s(buf, 64, "%s", (invert_y ? "yes" : "no"));
		xmlNodePtr xInvert_y = xmlNewChild(xPostNorm, NULL, (xmlChar*)"pn_invert_y", (xmlChar *)buf);
		if (!xInvert_y)
			return false;

	return true;
}


bool XMLScene::insertPreview(xmlNodePtr curNode, Mtl * mat)
{
	NOXMaterial * nmat = (NOXMaterial*)mat;
	if (!nmat)
		return false;

	if (!nmat->mat.getPreviewExists())
		return true;

	int w1 = nmat->mat.getPreviewWidth();
	int h1 = nmat->mat.getPreviewHeight();
	int bs = nmat->mat.getPreviewBufSize();
	if (w1*h1*sizeof(COLORREF) != bs)
		return false;

	int bsize = 6*w1+1;
	unsigned char * lbuf = (unsigned char *)malloc(bsize);
	if (!lbuf)
		return false;

	unsigned char * buf = (unsigned char*)nmat->mat.getPreviewBuffer();
	if (!buf)
	{
		free(lbuf);
		return false;
	}

	// add <preview>
	xmlNodePtr xPreview = xmlNewChild(curNode, NULL, (xmlChar*)"preview", NULL);
	if (!xPreview)
	{
		free(lbuf);
		free(buf);
		return false;
	}

	// add width
	xmlAttrPtr xAttr;
	char rbuf[32];
	sprintf_s(rbuf, 32, "%d", w1);
	xAttr = xmlNewProp(xPreview, (xmlChar*)"width", (xmlChar*)rbuf);
	if (!xAttr)
	{
		free(lbuf);
		free(buf);
		return false;
	}

	// add height
	sprintf_s(rbuf, 32, "%d", h1);
	xAttr = xmlNewProp(xPreview, (xmlChar*)"height", (xmlChar*)rbuf);
	if (!xAttr)
	{
		free(lbuf);
		free(buf);
		return false;
	}

	char tbuf[32];
	COLORREF cr;
	for (int i=0; i<h1; i++)
	{
		sprintf_s(tbuf, 32, "l%d", i);
		for (int j=0; j<w1; j++)
		{
			cr = *((COLORREF*)&(buf[((i*w1)+j)*sizeof(COLORREF)]));
			DecHex::ucharToHex2BigEndian(GetRValue(cr), &(lbuf[j*6+0]));
			DecHex::ucharToHex2BigEndian(GetGValue(cr), &(lbuf[j*6+2]));
			DecHex::ucharToHex2BigEndian(GetBValue(cr), &(lbuf[j*6+4]));
		}

		lbuf[bsize-1] = 0;

		// add line
		xmlNodePtr xLine = xmlNewChild(xPreview, NULL, (xmlChar*)tbuf, (xmlChar*)lbuf);
		if (!xLine)
		{
			free(lbuf);
			free(buf);
			return false;
		}
	}

	free(lbuf);
	free(buf);
	return true;

}

bool XMLScene::insertBlendSettings(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	// add <blend>
	xmlNodePtr xBlend = xmlNewChild(curNode, NULL, (xmlChar*)"blend", NULL);
	if (!xBlend)
		return false;

	char bufname[64];
	for (int i=0; i<16; i++)
	{
		sprintf_s(bufname,  64, "name%d", (i+1));
		TrMaterial tmat;
		char * tname = tmat.getBlendLayerName(i);
		if (tname)
		{
			xmlNodePtr xName = xmlNewChild(xBlend, NULL, (xmlChar*)bufname, (xmlChar*)tname);
			if (!xName)
				return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertUserColors(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	// add <user_colors>
	xmlNodePtr xUser = xmlNewChild(curNode, NULL, (xmlChar*)"user_colors", NULL);
	if (!xUser)
		return false;

	char name[32];
	unsigned char buf[32];
	for (int i=0; i<18; i++)
	{
		sprintf_s(name, "color%d", (i+1));
		float r,g,b;
		TrRaytracer::getUserColors(i, r,g,b);
		DecHex::floatToHex(r, buf+0);
		DecHex::floatToHex(g, buf+8);
		DecHex::floatToHex(b, buf+16);
		buf[24] = 0;

		xmlNodePtr xColor = xmlNewChild(xUser, NULL, (xmlChar*)name, (xmlChar*)buf);
		if (!xColor)
			return false;
	}
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertRendererSettings(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	Renderer * rend = GetCOREInterface()->GetRenderer(RS_Production);		
	if (rend->ClassID() != NOXRenderer_CLASS_ID)
		return 0;

	NOXRenderer * noxrend = (NOXRenderer*)rend;
	char buf[64];

	// add <renderer_settings>
	xmlNodePtr xSettings = xmlNewChild(curNode, NULL, (xmlChar*)"renderer_settings", (xmlChar*)NULL);
	if (!xSettings)
		return false;

	// add <engine>
	unsigned short engn = 0;
	switch (noxrend->engine)
	{
		case 0:  engn = 1;	break;
		case 1:  engn = 2;	break;
		case 2:  engn = 10;	break;
		default: engn = 1;	break;
	}
	sprintf_s(buf, 64, "%d", engn);
	xmlNodePtr xEngine = xmlNewChild(xSettings, NULL, (xmlChar*)"engine", (xmlChar*)buf);
	if (!xEngine)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool XMLScene::insertSceneInfo(xmlNodePtr curNode)
{
	if (!curNode)
		return false;

	Renderer * rend = GetCOREInterface()->GetRenderer(RS_Production);		
	if (rend->ClassID() != NOXRenderer_CLASS_ID)
		return 0;

	char pswd[64];
	sprintf_s(pswd, 64, "%d", numTris);
	unsigned int lpswd = (unsigned int)strlen(pswd);

	NOXRenderer * noxrend = (NOXRenderer*)rend;
	unsigned int size_scene_name   = (noxrend->scene_name)			?	(unsigned int)strlen(noxrend->scene_name)			: 0;
	unsigned int size_scene_author = (noxrend->scene_author_name)	?	(unsigned int)strlen(noxrend->scene_author_name)	: 0;
	unsigned int size_scene_email  = (noxrend->scene_author_email)	?	(unsigned int)strlen(noxrend->scene_author_email)	: 0;
	unsigned int size_scene_www    = (noxrend->scene_author_www)	?	(unsigned int)strlen(noxrend->scene_author_www)		: 0;

	// xor string, so noone can change it in file later
	char * decSceneName		= xorString(noxrend->scene_name,		 pswd, size_scene_name,		lpswd);
	char * decSceneAuthor	= xorString(noxrend->scene_author_name,	 pswd, size_scene_author,	lpswd);
	char * decSceneEmail	= xorString(noxrend->scene_author_email, pswd, size_scene_email,	lpswd);
	char * decSceneWWW		= xorString(noxrend->scene_author_www,	 pswd, size_scene_www,		lpswd);
	char * hexSceneName		= decToHexString(decSceneName,		size_scene_name);
	char * hexSceneAuthor	= decToHexString(decSceneAuthor,	size_scene_author);
	char * hexSceneEmail	= decToHexString(decSceneEmail,		size_scene_email);
	char * hexSceneWWW		= decToHexString(decSceneWWW,		size_scene_www);


	// add <scene_info>
	xmlNodePtr xSceneInfo = xmlNewChild(curNode, NULL, (xmlChar*)"scene_info", NULL);
	if (!xSceneInfo)
		return false;

	if (hexSceneName)
	{
		// add <scene_name>
		xmlNodePtr xSceneName = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"scene_name", (xmlChar*)hexSceneName);
		if (!xSceneName)
			return false;
	}

	if (hexSceneAuthor)
	{
		// add <scene_author>
		xmlNodePtr xSceneAuthor = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"scene_author", (xmlChar*)hexSceneAuthor);
		if (!xSceneAuthor)
			return false;
	}

	if (hexSceneEmail)
	{
		// add <author_email>
		xmlNodePtr xSceneAuthorEmail = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"author_email", (xmlChar*)hexSceneEmail);
		if (!xSceneAuthorEmail)
			return false;
	}

	if (hexSceneWWW)
	{
		// add <author_www>
		xmlNodePtr xSceneAuthorWWW = xmlNewChild(xSceneInfo, NULL, (xmlChar*)"author_www", (xmlChar*)hexSceneWWW);
		if (!xSceneAuthorWWW)
			return false;
	}

	if (decSceneName)
		free(decSceneName);
	if (decSceneAuthor)
		free(decSceneAuthor);
	if (decSceneEmail)
		free(decSceneEmail);
	if (decSceneWWW)
		free(decSceneWWW);
	if (hexSceneName)
		free(hexSceneName);
	if (hexSceneAuthor)
		free(hexSceneAuthor);
	if (hexSceneEmail)
		free(hexSceneEmail);
	if (hexSceneWWW)
		free(hexSceneWWW);
	
	return true;
}

//-------------------------------------------------------------------------------------------------------------

int XMLScene::addMat(Mtl * mat, bool isNOX)
{	
	// add mat (or not if exists) and return index where it lies in array
	int n = (int)mats.size();
	for (int i=0; i<n; i++)
	{
		if (mats[i] == mat)
			return i;
	}
	mats.push_back(mat);
	matTypes.push_back(isNOX);
	return n;
}

//-------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK NOXExportInfoDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			HWND hProgress = GetDlgItem(hWnd, IDC_NOX_EXP_PROGRESS);
			if (hProgress)
			{
				SendMessage(hProgress, PBM_SETRANGE, 0, (LPARAM) MAKELPARAM (0, 1000));
				SendMessage(hProgress, PBM_SETPOS, 0, 0);
			}
		}
		break;
	case WM_DESTROY:
		{
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			//switch (wmId)
			//{
			//}
		}	// end WM_COMMAND
		break;
	}
	return false;
}

void XMLScene::setProgress(int p)
{
	if (exportAndRender)
	{
		if (prog)
		{
			prog->Progress(p, 1000);
		}
		return;
	}

	if (!hInfoWindow)
		return;
	HWND hProgress = GetDlgItem(hInfoWindow, IDC_NOX_EXP_PROGRESS);
	if (!hProgress)
		return;
	SendMessage(hProgress, PBM_SETPOS, p, 0);
	InvalidateRect(hProgress, NULL, false);
	UpdateWindow(hProgress);
}

void XMLScene::setProgInfo(TCHAR * info)
{
	if (exportAndRender)
	{
		if (prog)
		{
			prog->SetTitle(info);
		}
		return;
	}

	if (!hInfoWindow)
		return;
	HWND hText = GetDlgItem(hInfoWindow, IDC_NOX_EXP_INFO);
	if (!hText)
		return;
	SetWindowText(hText, info);
	InvalidateRect(hText, NULL, false);
	UpdateWindow(hText);
}

//-------------------------------------------------------------------------------------------

void ValidateCameras::deactivateAll(INode * rootNode)
{
	int i;
	for (i=0; i<rootNode->NumberOfChildren(); i++)
	{
		deactivateAll(rootNode->GetChildNode(i));
	}

	ObjectState objSt = rootNode->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return;

	if (objSt.obj->SuperClassID() != CAMERA_CLASS_ID)
		return;

	if (objSt.obj->CanConvertToType(NOXCamera_CLASS_ID))
	{
		NOXCamera * tcam = (NOXCamera *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXCamera_CLASS_ID);
		if (!tcam)
			return;
		tcam->trCam.setActive(false);
		deleteSecondIfCopied(objSt.obj, tcam);
	}
}

//-------------------------------------------------------------------------------------------

void ValidateCameras::checkDeeper(INode * node)
{
	int i;
	for (i=0; i<node->NumberOfChildren(); i++)
	{
		checkDeeper(node->GetChildNode(i));
	}

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return;

	if (objSt.obj->SuperClassID() != CAMERA_CLASS_ID)
		return;

	if (objSt.obj->CanConvertToType(NOXCamera_CLASS_ID))
	{
		NOXCamera * tcam = (NOXCamera *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXCamera_CLASS_ID);
		if (!tcam)
			return;

		if (tcam->trCam.getIsActive())
		{
			if (foundActive)
				foundTooMany = true;
			else
				foundActive = true;
		}
		deleteSecondIfCopied(objSt.obj, tcam);

	}
}

//-------------------------------------------------------------------------------------------

bool ValidateCameras::check(INode * rootNode)
{
	foundActive = false;
	foundTooMany = false;
	checkDeeper(rootNode);
	return (foundActive && !foundTooMany);
}

//-------------------------------------------------------------------------------------------

char * XMLScene::xorString(char * src, char * pswd, unsigned int lsrc, unsigned int lpswd)
{
	if (!src)
		return NULL;
	if (!pswd)
		return NULL;
	if (!lsrc)
		return NULL;
	if (!lpswd)
		return NULL;

	char * data = (char *)malloc(lsrc+1);
	if (!data)
		return false;
	data[lsrc] = 0;

	for (unsigned int i=0; i<lsrc; i++)
	{
		data[i] = src[i] ^ pswd[i%lpswd];
	}

	return data;
}

char * XMLScene::decToHexString(char * src, unsigned int lsrc)
{
	if (!src)
		return NULL;
	if (!lsrc)
		return NULL;

	char * res = (char*)malloc(2*lsrc+1);
	if (!res)
		return false;
	for (unsigned int i=0; i<lsrc; i++)
	{
		DecHex::ucharToHex2(src[i], (unsigned char*)(&(res[2*i])));
	}
	res[2*lsrc] = 0;

	return res;
}

//-------------------------------------------------------------------------------------------


