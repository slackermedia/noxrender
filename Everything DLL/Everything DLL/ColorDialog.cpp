#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

// old gui version

Color4 UserColors::colors[18];

INT_PTR CALLBACK ColorDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		{
			EMColorPicker * emcp = GetEMColorPickerInstance(GetDlgItem(hWnd, IDC_COLOR_PICKER));
			GlobalWindowSettings::colorSchemes.apply(emcp);
			Color4 * col = (Color4*)lParam;
			Color4 c;
			if (col)
			{
				emcp->result = col;
				c = *col;
				emcp->setRGBandRedraw(c);
			}
			else
			{
				emcp->result = new Color4(0,0,0);
				c = Color4(0,0,0);
				emcp->setRGBandRedraw(c);
			}
			EMColorShow * emcs = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_COLOR_CURRENT));
			GlobalWindowSettings::colorSchemes.apply(emcs);
			emcs->redraw(c);
			ColorHSV hsv = c.toColorHSV();


			HWND hR,hG,hB,hH,hS,hV;
			hR = GetDlgItem(hWnd, IDC_COLOR_TRACK_R);
			hG = GetDlgItem(hWnd, IDC_COLOR_TRACK_G);
			hB = GetDlgItem(hWnd, IDC_COLOR_TRACK_B);
			hH = GetDlgItem(hWnd, IDC_COLOR_TRACK_H);
			hS = GetDlgItem(hWnd, IDC_COLOR_TRACK_S);
			hV = GetDlgItem(hWnd, IDC_COLOR_TRACK_V);
			EMEditTrackBar * emetbr = GetEMEditTrackBarInstance(hR);
			EMEditTrackBar * emetbg = GetEMEditTrackBarInstance(hG);
			EMEditTrackBar * emetbb = GetEMEditTrackBarInstance(hB);
			EMEditTrackBar * emetbh = GetEMEditTrackBarInstance(hH);
			EMEditTrackBar * emetbs = GetEMEditTrackBarInstance(hS);
			EMEditTrackBar * emetbv = GetEMEditTrackBarInstance(hV);
			emetbr->setEditBoxWidth(32);
			emetbg->setEditBoxWidth(32);
			emetbb->setEditBoxWidth(32);
			emetbh->setEditBoxWidth(32);
			emetbs->setEditBoxWidth(32);
			emetbv->setEditBoxWidth(32);
			emetbr->colPathBackgroundBefore = RGB(255,0,0);
			emetbg->colPathBackgroundBefore = RGB(0,255,0);
			emetbb->colPathBackgroundBefore = RGB(0,0,255);
			emetbr->setValuesToInt((int)(c.r*255),0,255,10);
			emetbg->setValuesToInt((int)(c.g*255),0,255,10);
			emetbb->setValuesToInt((int)(c.b*255),0,255,10);
			emetbh->setValuesToInt((int)(hsv.h*255),0,255,10);
			emetbs->setValuesToInt((int)(hsv.s*255),0,255,10);
			emetbv->setValuesToInt((int)(hsv.v*255),0,255,10);
			emetbr->colBackground = RGB(127,192,255);
			emetbg->colBackground = RGB(127,192,255);
			emetbb->colBackground = RGB(127,192,255);
			emetbh->colBackground = RGB(127,192,255);
			emetbs->colBackground = RGB(127,192,255);
			emetbv->colBackground = RGB(127,192,255);
			emetbr->pathHeight = 4;
			emetbg->pathHeight = 4;
			emetbb->pathHeight = 4;
			emetbh->pathHeight = 4;
			emetbs->pathHeight = 4;
			emetbv->pathHeight = 4;
			EMButton * bOK = GetEMButtonInstance(GetDlgItem(hWnd, IDC_COLOR_OK));
			EMButton * bCancel = GetEMButtonInstance(GetDlgItem(hWnd, IDC_COLOR_CANCEL));
			bOK->colBackGnd = RGB(112,176,255);
			bOK->colBackGndClicked = RGB(160,224,255);
			bOK->colBorderDark = RGB(96,160,224);
			bOK->colBorderLight = RGB(128,192,255);
			bOK->colText = RGB(0,0,0);
			bCancel->colBackGnd = RGB(112,176,255);
			bCancel->colBackGndClicked = RGB(160,224,255);
			bCancel->colBorderDark = RGB(96,160,224);
			bCancel->colBorderLight = RGB(128,192,255);
			bCancel->colText = RGB(0,0,0);
			GlobalWindowSettings::colorSchemes.apply(emetbr);
			GlobalWindowSettings::colorSchemes.apply(emetbg);
			GlobalWindowSettings::colorSchemes.apply(emetbb);
			GlobalWindowSettings::colorSchemes.apply(emetbh);
			GlobalWindowSettings::colorSchemes.apply(emetbs);
			GlobalWindowSettings::colorSchemes.apply(emetbv);
			GlobalWindowSettings::colorSchemes.apply(bOK);
			GlobalWindowSettings::colorSchemes.apply(bCancel);

			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_COLOR_TEXT_RGB)), true);
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_COLOR_TEXT_HSV)), true);
		
			emetbr->colPathBackgroundBefore = RGB(255,0,0);
			emetbg->colPathBackgroundBefore = RGB(0,255,0);
			emetbb->colPathBackgroundBefore = RGB(0,0,255);

			HWND hC1, hC2, hC3, hC4, hC5, hC6, hC7, hC8, hC9, hC10, hC11, hC12, hC13, hC14, hC15, hC16, hC17, hC18;
			hC1  = GetDlgItem(hWnd, IDC_COLOR_C1);
			hC2  = GetDlgItem(hWnd, IDC_COLOR_C2);
			hC3  = GetDlgItem(hWnd, IDC_COLOR_C3);
			hC4  = GetDlgItem(hWnd, IDC_COLOR_C4);
			hC5  = GetDlgItem(hWnd, IDC_COLOR_C5);
			hC6  = GetDlgItem(hWnd, IDC_COLOR_C6);
			hC7  = GetDlgItem(hWnd, IDC_COLOR_C7);
			hC8  = GetDlgItem(hWnd, IDC_COLOR_C8);
			hC9  = GetDlgItem(hWnd, IDC_COLOR_C9);
			hC10 = GetDlgItem(hWnd, IDC_COLOR_C10);
			hC11 = GetDlgItem(hWnd, IDC_COLOR_C11);
			hC12 = GetDlgItem(hWnd, IDC_COLOR_C12);
			hC13 = GetDlgItem(hWnd, IDC_COLOR_C13);
			hC14 = GetDlgItem(hWnd, IDC_COLOR_C14);
			hC15 = GetDlgItem(hWnd, IDC_COLOR_C15);
			hC16 = GetDlgItem(hWnd, IDC_COLOR_C16);
			hC17 = GetDlgItem(hWnd, IDC_COLOR_C17);
			hC18 = GetDlgItem(hWnd, IDC_COLOR_C18);
			EMColorShow * emcs1  = GetEMColorShowInstance(hC1);
			EMColorShow * emcs2  = GetEMColorShowInstance(hC2);
			EMColorShow * emcs3  = GetEMColorShowInstance(hC3);
			EMColorShow * emcs4  = GetEMColorShowInstance(hC4);
			EMColorShow * emcs5  = GetEMColorShowInstance(hC5);
			EMColorShow * emcs6  = GetEMColorShowInstance(hC6);
			EMColorShow * emcs7  = GetEMColorShowInstance(hC7);
			EMColorShow * emcs8  = GetEMColorShowInstance(hC8);
			EMColorShow * emcs9  = GetEMColorShowInstance(hC9);
			EMColorShow * emcs10 = GetEMColorShowInstance(hC10);
			EMColorShow * emcs11 = GetEMColorShowInstance(hC11);
			EMColorShow * emcs12 = GetEMColorShowInstance(hC12);
			EMColorShow * emcs13 = GetEMColorShowInstance(hC13);
			EMColorShow * emcs14 = GetEMColorShowInstance(hC14);
			EMColorShow * emcs15 = GetEMColorShowInstance(hC15);
			EMColorShow * emcs16 = GetEMColorShowInstance(hC16);
			EMColorShow * emcs17 = GetEMColorShowInstance(hC17);
			EMColorShow * emcs18 = GetEMColorShowInstance(hC18);
			GlobalWindowSettings::colorSchemes.apply(emcs1);
			GlobalWindowSettings::colorSchemes.apply(emcs2);
			GlobalWindowSettings::colorSchemes.apply(emcs3);
			GlobalWindowSettings::colorSchemes.apply(emcs4);
			GlobalWindowSettings::colorSchemes.apply(emcs5);
			GlobalWindowSettings::colorSchemes.apply(emcs6);
			GlobalWindowSettings::colorSchemes.apply(emcs7);
			GlobalWindowSettings::colorSchemes.apply(emcs8);
			GlobalWindowSettings::colorSchemes.apply(emcs9);
			GlobalWindowSettings::colorSchemes.apply(emcs10);
			GlobalWindowSettings::colorSchemes.apply(emcs11);
			GlobalWindowSettings::colorSchemes.apply(emcs12);
			GlobalWindowSettings::colorSchemes.apply(emcs13);
			GlobalWindowSettings::colorSchemes.apply(emcs14);
			GlobalWindowSettings::colorSchemes.apply(emcs15);
			GlobalWindowSettings::colorSchemes.apply(emcs16);
			GlobalWindowSettings::colorSchemes.apply(emcs17);
			GlobalWindowSettings::colorSchemes.apply(emcs18);
			emcs1->redraw( UserColors::colors[0]);
			emcs2->redraw( UserColors::colors[1]);
			emcs3->redraw( UserColors::colors[2]);
			emcs4->redraw( UserColors::colors[3]);
			emcs5->redraw( UserColors::colors[4]);
			emcs6->redraw( UserColors::colors[5]);
			emcs7->redraw( UserColors::colors[6]);
			emcs8->redraw( UserColors::colors[7]);
			emcs9->redraw( UserColors::colors[8]);
			emcs10->redraw(UserColors::colors[9]);
			emcs11->redraw(UserColors::colors[10]);
			emcs12->redraw(UserColors::colors[11]);
			emcs13->redraw(UserColors::colors[12]);
			emcs14->redraw(UserColors::colors[13]);
			emcs15->redraw(UserColors::colors[14]);
			emcs16->redraw(UserColors::colors[15]);
			emcs17->redraw(UserColors::colors[16]);
			emcs18->redraw(UserColors::colors[17]);


		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
			case IDC_COLOR_OK:
				{
					if (wmEvent == BN_CLICKED)
					{
						EMColorPicker * emcp = GetEMColorPickerInstance(GetDlgItem(hWnd, IDC_COLOR_PICKER));
						*emcp->result = emcp->rgb;
						EndDialog(hWnd, (INT_PTR)(emcp->result));
					}
				}
				break;
			case IDC_COLOR_CANCEL:
				{
					if (wmEvent == BN_CLICKED)
					{
						EndDialog(hWnd, (INT_PTR)NULL);
					}
				}
				break;
			case IDC_COLOR_PICKER:
				{
					if (wmEvent == WM_VSCROLL)
					{
						HWND hPicker = GetDlgItem(hWnd, wmId);
						EMColorPicker * emcp = GetEMColorPickerInstance(hPicker);
						Color4 rgb = emcp->rgb;
						Color4 frgb = emcp->fullSatRGB;
						ColorHSV hsv = emcp->hsv;
						Color4 rgbS;
						rgbS = frgb*hsv.s + Color4(1,1,1)*(1-hsv.s);
						HWND hR,hG,hB,hH,hS,hV;
						RECT rect;
						hR = GetDlgItem(hWnd, IDC_COLOR_TRACK_R);
						hG = GetDlgItem(hWnd, IDC_COLOR_TRACK_G);
						hB = GetDlgItem(hWnd, IDC_COLOR_TRACK_B);
						hH = GetDlgItem(hWnd, IDC_COLOR_TRACK_H);
						hS = GetDlgItem(hWnd, IDC_COLOR_TRACK_S);
						hV = GetDlgItem(hWnd, IDC_COLOR_TRACK_V);
						GetEMEditTrackBarInstance(hR)->setValuesToInt((int)(rgb.r*255),0,255,10);
						GetEMEditTrackBarInstance(hG)->setValuesToInt((int)(rgb.g*255),0,255,10);
						GetEMEditTrackBarInstance(hB)->setValuesToInt((int)(rgb.b*255),0,255,10);
						GetEMEditTrackBarInstance(hH)->setValuesToInt((int)(hsv.h*255),0,255,10);
						GetEMEditTrackBarInstance(hS)->setValuesToInt((int)(hsv.s*255),0,255,10);
						GetEMEditTrackBarInstance(hV)->setValuesToInt((int)(hsv.v*255),0,255,10);

						GetEMEditTrackBarInstance(hH)->colPathBackgroundBefore = RGB((int)(frgb.r*255), (int)(frgb.g*255), (int)(frgb.b*255));
						GetEMEditTrackBarInstance(hS)->colPathBackgroundBefore = RGB((int)(rgbS.r*255), (int)(rgbS.g*255), (int)(rgbS.b*255));
						GetEMEditTrackBarInstance(hV)->colPathBackgroundBefore = RGB((int)(hsv.v*255), (int)(hsv.v*255), (int)(hsv.v*255));

						GetClientRect(hR, &rect);
						InvalidateRect(hR, &rect, false);
						GetClientRect(hG, &rect);
						InvalidateRect(hG, &rect, false);
						GetClientRect(hB, &rect);
						InvalidateRect(hB, &rect, false);
						GetClientRect(hH, &rect);
						InvalidateRect(hH, &rect, false);
						GetClientRect(hS, &rect);
						InvalidateRect(hS, &rect, false);
						GetClientRect(hV, &rect);
						InvalidateRect(hV, &rect, false);						

						GetEMColorShowInstance(GetDlgItem(hWnd, IDC_COLOR_CURRENT))->redraw(rgb);
					}
				}
				break;
			case IDC_COLOR_TRACK_R:
			case IDC_COLOR_TRACK_G:
			case IDC_COLOR_TRACK_B:
				{
					if (wmEvent == WM_HSCROLL)
					{
						int tr,tg,tb;
						HWND hR,hG,hB,hH,hS,hV, hPicker;
						RECT rect;
						hR = GetDlgItem(hWnd, IDC_COLOR_TRACK_R);
						hG = GetDlgItem(hWnd, IDC_COLOR_TRACK_G);
						hB = GetDlgItem(hWnd, IDC_COLOR_TRACK_B);
						hH = GetDlgItem(hWnd, IDC_COLOR_TRACK_H);
						hS = GetDlgItem(hWnd, IDC_COLOR_TRACK_S);
						hV = GetDlgItem(hWnd, IDC_COLOR_TRACK_V);
						hPicker = GetDlgItem(hWnd, IDC_COLOR_PICKER);
						EMColorPicker * emcp = GetEMColorPickerInstance(hPicker);
						tr = GetEMEditTrackBarInstance(hR)->intValue;
						tg = GetEMEditTrackBarInstance(hG)->intValue;
						tb = GetEMEditTrackBarInstance(hB)->intValue;
						Color4 rgb = Color4(tr/255.0f,tg/255.0f,tb/255.0f);
						ColorHSV hsv = rgb.toColorHSV();
						emcp->setRGBandRedraw(rgb);
					
						Color4 frgb = emcp->fullSatRGB;
						Color4 rgbS;
						rgbS = frgb*hsv.s + Color4(1,1,1)*(1-hsv.s);

						GetEMEditTrackBarInstance(hH)->colPathBackgroundBefore = RGB((int)(frgb.r*255), (int)(frgb.g*255), (int)(frgb.b*255));
						GetEMEditTrackBarInstance(hS)->colPathBackgroundBefore = RGB((int)(rgbS.r*255), (int)(rgbS.g*255), (int)(rgbS.b*255));
						GetEMEditTrackBarInstance(hV)->colPathBackgroundBefore = RGB((int)(hsv.v*255), (int)(hsv.v*255), (int)(hsv.v*255));

						GetEMEditTrackBarInstance(hH)->setValuesToInt((int)(hsv.h*255),0,255,10);
						GetEMEditTrackBarInstance(hS)->setValuesToInt((int)(hsv.s*255),0,255,10);
						GetEMEditTrackBarInstance(hV)->setValuesToInt((int)(hsv.v*255),0,255,10);

						GetClientRect(hH, &rect);
						InvalidateRect(hH, &rect, false);
						GetClientRect(hS, &rect);
						InvalidateRect(hS, &rect, false);
						GetClientRect(hV, &rect);
						InvalidateRect(hV, &rect, false);						

						GetEMColorShowInstance(GetDlgItem(hWnd, IDC_COLOR_CURRENT))->redraw(rgb);
					}
				}
				break;
			case IDC_COLOR_TRACK_H:
			case IDC_COLOR_TRACK_S:
			case IDC_COLOR_TRACK_V:
				{
					if (wmEvent == WM_HSCROLL)
					{
						int th,ts,tv;
						HWND hR,hG,hB,hH,hS,hV, hPicker;
						RECT rect;
						hR = GetDlgItem(hWnd, IDC_COLOR_TRACK_R);
						hG = GetDlgItem(hWnd, IDC_COLOR_TRACK_G);
						hB = GetDlgItem(hWnd, IDC_COLOR_TRACK_B);
						hH = GetDlgItem(hWnd, IDC_COLOR_TRACK_H);
						hS = GetDlgItem(hWnd, IDC_COLOR_TRACK_S);
						hV = GetDlgItem(hWnd, IDC_COLOR_TRACK_V);
						hPicker = GetDlgItem(hWnd, IDC_COLOR_PICKER);
						EMColorPicker * emcp = GetEMColorPickerInstance(hPicker);
						th = GetEMEditTrackBarInstance(hH)->intValue;
						ts = GetEMEditTrackBarInstance(hS)->intValue;
						tv = GetEMEditTrackBarInstance(hV)->intValue;

						ColorHSV hsv = ColorHSV(th/255.0f,ts/255.0f,tv/255.0f);
						Color4 rgb = hsv.toColor4();
						emcp->setHSVandRedraw(hsv);
					
						Color4 frgb = emcp->fullSatRGB;
						Color4 rgbS;
						rgbS = frgb*hsv.s + Color4(1,1,1)*(1-hsv.s);

						GetEMEditTrackBarInstance(hH)->colPathBackgroundBefore = RGB((int)(frgb.r*255), (int)(frgb.g*255), (int)(frgb.b*255));
						GetEMEditTrackBarInstance(hS)->colPathBackgroundBefore = RGB((int)(rgbS.r*255), (int)(rgbS.g*255), (int)(rgbS.b*255));
						GetEMEditTrackBarInstance(hV)->colPathBackgroundBefore = RGB((int)(hsv.v*255), (int)(hsv.v*255), (int)(hsv.v*255));

						GetEMEditTrackBarInstance(hR)->setValuesToInt((int)(rgb.r*255),0,255,10);
						GetEMEditTrackBarInstance(hG)->setValuesToInt((int)(rgb.g*255),0,255,10);
						GetEMEditTrackBarInstance(hB)->setValuesToInt((int)(rgb.b*255),0,255,10);

						GetClientRect(hH, &rect);
						InvalidateRect(hH, &rect, false);
						GetClientRect(hS, &rect);
						InvalidateRect(hS, &rect, false);
						GetClientRect(hV, &rect);
						InvalidateRect(hV, &rect, false);						
						GetClientRect(hR, &rect);
						InvalidateRect(hR, &rect, false);
						GetClientRect(hG, &rect);
						InvalidateRect(hG, &rect, false);
						GetClientRect(hB, &rect);
						InvalidateRect(hB, &rect, false);	

						GetEMColorShowInstance(GetDlgItem(hWnd, IDC_COLOR_CURRENT))->redraw(rgb);
					}
				}
				break;
			case IDC_COLOR_C1:
			case IDC_COLOR_C2:
			case IDC_COLOR_C3:
			case IDC_COLOR_C4:
			case IDC_COLOR_C5:
			case IDC_COLOR_C6:
			case IDC_COLOR_C7:
			case IDC_COLOR_C8:
			case IDC_COLOR_C9:
			case IDC_COLOR_C10:
			case IDC_COLOR_C11:
			case IDC_COLOR_C12:
			case IDC_COLOR_C13:
			case IDC_COLOR_C14:
			case IDC_COLOR_C15:
			case IDC_COLOR_C16:
			case IDC_COLOR_C17:
			case IDC_COLOR_C18:
				{
					if (wmEvent == BN_CLICKED  ||  wmEvent == CL_RCLICKED)
					{
						EMColorShow * emcs = GetEMColorShowInstance(GetDlgItem(hWnd, wmId));
						if (!emcs)
							break;
						int nCol = 0;
						switch (wmId)
						{
							case IDC_COLOR_C1:  nCol = 0;  break;
							case IDC_COLOR_C2:  nCol = 1;  break;
							case IDC_COLOR_C3:  nCol = 2;  break;
							case IDC_COLOR_C4:  nCol = 3;  break;
							case IDC_COLOR_C5:  nCol = 4;  break;
							case IDC_COLOR_C6:  nCol = 5;  break;
							case IDC_COLOR_C7:  nCol = 6;  break;
							case IDC_COLOR_C8:  nCol = 7;  break;
							case IDC_COLOR_C9:  nCol = 8;  break;
							case IDC_COLOR_C10: nCol = 9;  break;
							case IDC_COLOR_C11: nCol = 10; break;
							case IDC_COLOR_C12: nCol = 11; break;
							case IDC_COLOR_C13: nCol = 12; break;
							case IDC_COLOR_C14: nCol = 13; break;
							case IDC_COLOR_C15: nCol = 14; break;
							case IDC_COLOR_C16: nCol = 15; break;
							case IDC_COLOR_C17: nCol = 16; break;
							case IDC_COLOR_C18: nCol = 17; break;
						}

						HWND hPicker = GetDlgItem(hWnd, IDC_COLOR_PICKER);
						EMColorPicker * emcp = GetEMColorPickerInstance(hPicker);

						if (wmEvent == BN_CLICKED)		// take from lib
						{
							Color4 rgb = UserColors::colors[nCol];
							ColorHSV hsv = rgb.toColorHSV();
							emcp->setRGBandRedraw(rgb);

							EMEditTrackBar * th = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_COLOR_TRACK_H));
							EMEditTrackBar * ts = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_COLOR_TRACK_S));
							EMEditTrackBar * tv = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_COLOR_TRACK_V));
							EMEditTrackBar * tr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_COLOR_TRACK_R));
							EMEditTrackBar * tg = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_COLOR_TRACK_G));
							EMEditTrackBar * tb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_COLOR_TRACK_B));

							Color4 frgb = emcp->fullSatRGB;
							Color4 rgbS;
							rgbS = frgb*hsv.s + Color4(1,1,1)*(1-hsv.s);

							th->colPathBackgroundBefore = RGB((int)(frgb.r*255), (int)(frgb.g*255), (int)(frgb.b*255));
							ts->colPathBackgroundBefore = RGB((int)(rgbS.r*255), (int)(rgbS.g*255), (int)(rgbS.b*255));
							tv->colPathBackgroundBefore = RGB((int)(hsv.v*255), (int)(hsv.v*255), (int)(hsv.v*255));

							tr->setValuesToInt((int)(rgb.r*255),0,255,10);
							tg->setValuesToInt((int)(rgb.g*255),0,255,10);
							tb->setValuesToInt((int)(rgb.b*255),0,255,10);

							th->setValuesToInt((int)(hsv.h*255),0,255,10);
							ts->setValuesToInt((int)(hsv.s*255),0,255,10);
							tv->setValuesToInt((int)(hsv.v*255),0,255,10);

							GetEMColorShowInstance(GetDlgItem(hWnd, IDC_COLOR_CURRENT))->redraw(rgb);
						}

						if (wmEvent == CL_RCLICKED)		// save to lib
						{
							emcs->redraw(emcp->rgb);
							UserColors::colors[nCol] = emcp->rgb;
						}
					}

				}
				break;
			}
		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			EMColorShow * emcs = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_COLOR_CURRENT));
			return (INT_PTR)emcs->dialogBrush;
		}
	case WM_CTLCOLORDLG:
		{
			EMColorShow * emcs = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_COLOR_CURRENT));
			return (INT_PTR)emcs->dialogBrush;
		}
		break;
	}
	return false;
}




