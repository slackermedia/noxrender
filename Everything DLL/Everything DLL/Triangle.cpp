#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "raytracer.h"
#include "log.h"

Triangle::Triangle(	const Point3d &v1, const Point3d &v2, const Point3d &v3, 
						const Vector3d &n1, const Vector3d &n2, const Vector3d &n3, 
						const Point2d &uv1, const Point2d &uv2, const Point2d &uv3, 
						const unsigned int &owner)
{
	flags = 0;
	V1 = v1;
	V2 = v2;
	V3 = v3;
	N1 = n1;
	N2 = n2;
	N3 = n3;
	UV1 = uv1;
	UV2 = uv2;
	UV3 = uv3;

	Vector3d vv1, vv2, vv3;
	vv1 = v2-v1;
	vv1.normalize();
	vv2 = v3-v1;
	vv2.normalize();
	vv3 = vv1^vv2;
	vv3.normalize();

	meshOwn = owner;
	matNum = -1;
	matInInst = 0;

	normal_geom = vv3;
	if (normal_geom*n1 <= 0   ||    normal_geom*n2 <= 0   ||    normal_geom*n3 <= 0)
	{
		Logger::add("Warning. Part of shade normals opposite to geometry normals.");
	}

	evaluateArea();
}

Triangle::~Triangle()
{
}

float Triangle::getArea(const Point3d &p1, const Point3d &p2, const Point3d &p3)
{
	float res;
	res = p1.x*p2.y*p3.z + p2.x*p3.y*p1.z + p3.x*p1.y*p2.z - p3.x*p2.y*p1.z - p2.x*p1.y*p3.z - p1.x*p3.y*p2.z;
	res = fabs(res);
	return res;
}

Point3d Triangle::getPointFromUV(const float &u, const float &v)
{
	Point3d res;
	float uv1 = 1.0f-u-v;
	res.x = u*V2.x + v*V3.x + (uv1)*V1.x;
	res.y = u*V2.y + v*V3.y + (uv1)*V1.y;
	res.z = u*V2.z + v*V3.z + (uv1)*V1.z;
	return res;
}

float Triangle::intersection(const Point3d &origin, const Vector3d &direction, const float &maxDist)
{
	float u,v;
	Vector3d tv,pv,qv;
	float det,d1;
	Vector3d e1 = V2-V1;
	Vector3d e2 = V3-V1;
	pv = direction^e2;
	det = e1*pv;
	if (det < 0.0000001f   &&   det > -0.0000001f)
		return -1;
	det = 1.0f/det;
	tv = origin - V1;
	qv = tv^e1;
	d1 = (e2*qv)*det;
	if (d1<=0.0f)
		return -1;
	if (maxDist>0)
		if (d1>maxDist)
			return -1;

	u = (tv * pv)*det;
	if (u>1 || u<0)
		return -1;
	v = (direction*qv)*det;
	if (v<0 || u+v>1)
		return -1;
	return d1;

}

float Triangle::intersection(const Point3d &origin, const Vector3d &direction, float &u, float &v)
{
	Vector3d tv,pv,qv;
	float det;
	Vector3d e1 = V2-V1;
	Vector3d e2 = V3-V1;

	pv = direction^e2;
	det = e1*pv;
	if (fabs(det) < 0.0000001f)
		return -1;
	det = 1.0f/det;
	tv = origin - V1;
	u = (tv * pv)*det;
	if (u>1 || u<0)
		return -1;
	qv = tv^e1;
	v = (direction*qv)*det;
	if (v<0 || u+v>1)
		return -1;
	return (e2*qv)*det;
}

bool Triangle::getUV(const Point3d &q, float &u, float &v)
{
	Vector3d rr,aa;
	Matrix3d pp;
	Vector3d mat1,mat2,mat3;
	mat1 = Vector3d(1,1,1);
	mat2 = V2-V1;
	mat3 = V3-V1;
	pp = Matrix3d(mat1.toPoint(), mat2.toPoint(), mat3.toPoint(), Matrix3d::BY_COLS);
	if (!pp.invert())
		return false;
	aa = q - V1;
	rr = pp*aa;

	if ((rr.y>=0) && (rr.z>=0) && (rr.y+rr.z<=1.0f))
	{
		u = rr.y;
		v = rr.z;
		return true;
	}
	return false;
}

bool Triangle::evalTexUV(const float &u, const float &v, float &tU, float &tV) const
{
	tU = UV1.x*(1-u-v) + UV2.x*u + UV3.x*v;
	tV = UV1.y*(1-u-v) + UV2.y*u + UV3.y*v;
	return true;
}

Vector3d Triangle::evalNormal(const float &u, const float &v)	const
{
	Vector3d res;
	res = N1*(1.0f-u-v) + N2*u + N3*v;
	res.normalize();
	return res;
}

void Triangle::randomPoint(float &u, float &v, Point3d &point)
{
	#ifdef RAND_PER_THREAD
		u = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
		v = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		u = rand()/(float)RAND_MAX;
		v = rand()/(float)RAND_MAX;
	#endif

	if (u+v>1)
	{
		u = 1-u;
		v = 1-v;
	}

	point = getPointFromUV(u, v);
}

float Triangle::getArea()
{
	return area;
}

float Triangle::evaluateArea()
{
	Vector3d e1 = V2-V1;
	Vector3d e2 = V3-V1;
	Vector3d tempv = (e1^e2);
	area = tempv.length() * 0.5f;
	return area;
}

void Triangle::getBadUVdirSmooth(Vector3d normal_shade, Vector3d &uDir, Vector3d &vDir)
{
	uDir = normal_shade ^ Vector3d(0,1,0);	// try perpend to updir Y
	if (uDir.lengthSqr()<0.00001f)
		uDir = normal_shade ^ Vector3d(0,0,1);	// try perpend to Z dir
	uDir.normalize();
	vDir = uDir ^ normal_shade;
	vDir.normalize();
}

void Triangle::getBadUVdir(Matrix4d &matr, Vector3d &uDir, Vector3d &vDir)
{
	Vector3d normal = matr.getMatrixForNormals() * normal_geom;
	uDir = normal ^ Vector3d(0,1,0);	// try perpend to updir Y
	if (uDir.lengthSqr()<0.00001f)
		uDir = normal ^ Vector3d(0,0,1);	// try perpend to Z dir
	uDir.normalize();
	vDir = uDir ^ normal;
	vDir.normalize();
}

//#define TANGENT_SPACE_SMOOTH

bool Triangle::getTangentSpaceSmooth(const Matrix4d &matr, const float &u, const float &v, Vector3d &dirU, Vector3d &dirV)
{
	dirU = dirU1*(1-u-v) + dirU2*u + dirU3*v;
	dirV = dirV1*(1-u-v) + dirV2*u + dirV3*v;
	dirU = matr * dirU;
	dirV = matr * dirV;
	dirU.normalize();
	dirV.normalize();
	return true;
}

bool Triangle::evalTangentSpace(Matrix4d &matr, const float &u, const float &v, Vector3d &U, Vector3d &V)
{
	Point3d TV1 = matr * V1;
	Point3d TV2 = matr * V2;
	Point3d TV3 = matr * V3;

	#ifdef TANGENT_SPACE_SMOOTH
		Vector3d nsm1 = matr.getMatrixForNormals() * N1;
		Vector3d nsm2 = matr.getMatrixForNormals() * N2;
		Vector3d nsm3 = matr.getMatrixForNormals() * N3;
		nsm1.normalize();
		nsm2.normalize();
		nsm3.normalize();
	#endif

	float a1,b1,c1,d1, f1,g1,h1, l1,m1,n1;
	float a2,b2,c2,d2, f2,g2,h2, l2,m2,n2;
	float a3,b3,c3,d3, f3,g3,h3, l3,m3,n3;
	a1 = UV2.x - UV1.x;		// uv edge 12x
	a2 = UV3.x - UV2.x;		// uv edge 23x
	a3 = UV1.x - UV3.x;		// uv edge 31x
	b1 = UV2.y - UV1.y;		// uv edge 12x
	b2 = UV3.y - UV2.y;		// uv edge 23x
	b3 = UV1.y - UV3.y;		// uv edge 31x
	c1 = UV3.x - UV1.x;		// uv edge 12x
	c2 = UV1.x - UV2.x;		// uv edge 23x
	c3 = UV2.x - UV3.x;		// uv edge 31x
	d1 = UV3.y - UV1.y;		// uv edge 12x
	d2 = UV1.y - UV2.y;		// uv edge 23x
	d3 = UV2.y - UV3.y;		// uv edge 31x

	Vector3d V21_1 = TV2 - TV1;
	Vector3d V31_1 = TV3 - TV1;
	Vector3d V21_2 = TV3 - TV2;
	Vector3d V31_2 = TV1 - TV2;
	Vector3d V21_3 = TV1 - TV3;
	Vector3d V31_3 = TV2 - TV3;

	#ifdef TANGENT_SPACE_SMOOTH
		Vector3d tempv1;
		float s1, s2;

		s1 = V21_1.length();
		s2 = V31_1.length();
		tempv1 = V21_1 ^ nsm1;
		V21_1 = nsm1 ^ tempv1;
		V21_1.normalize();
		tempv1 = V31_1 ^ nsm1;
		V31_1 = nsm1 ^ tempv1;
		V31_1.normalize();
		V21_1 *= s1;
		V31_1 *= s2;

		s1 = V21_2.length();
		s2 = V31_2.length();
		tempv1 = V21_2 ^ nsm2;
		V21_2 = nsm2 ^ tempv1;
		V21_2.normalize();
		tempv1 = V31_2 ^ nsm2;
		V31_2 = nsm2 ^ tempv1;
		V31_2.normalize();
		V21_2 *= s1;
		V31_2 *= s2;

		s1 = V21_3.length();
		s2 = V31_3.length();
		tempv1 = V21_3 ^ nsm3;
		V21_3 = nsm3 ^ tempv1;
		V21_3.normalize();
		tempv1 = V31_3 ^ nsm3;
		V31_3 = nsm3 ^ tempv1;
		V31_3.normalize();
		V21_3 *= s1;
		V31_3 *= s2;
	#endif

	f1 = V21_1.x;
	f2 = V21_2.x;
	f3 = V21_3.x;
	g1 = V21_1.y;
	g2 = V21_2.y;
	g3 = V21_3.y;
	h1 = V21_1.z;
	h2 = V21_2.z;
	h3 = V21_3.z;
	l1 = V31_1.x;
	l2 = V31_2.x;
	l3 = V31_3.x;
	m1 = V31_1.y;
	m2 = V31_2.y;
	m3 = V31_3.y;
	n1 = V31_1.z;
	n2 = V31_2.z;
	n3 = V31_3.z;

	float mn1 = d1*a1 - b1*c1;
	float mn2 = d2*a2 - b2*c2;
	float mn3 = d3*a3 - b3*c3;
	if (mn1 == 0.0f)
		return false;
	if (mn2 == 0.0f)
		return false;
	if (mn3 == 0.0f)
		return false;
	float pmn1 = 1.0f / mn1;
	float pmn2 = 1.0f / mn2;
	float pmn3 = 1.0f / mn3;

	Vector3d RU1, RU2, RU3, RV1, RV2, RV3;

	if (fabs(a1) > fabs(b1))
	{
		float pa1 = 1.0f / a1;
		RV1.x = ( l1 * a1 - f1 * c1 ) * pmn1;
		RV1.y = ( m1 * a1 - g1 * c1 ) * pmn1;
		RV1.z = ( n1 * a1 - h1 * c1 ) * pmn1;
		RU1.x = ( f1 - RV1.x * b1 ) * pa1;
		RU1.y = ( g1 - RV1.y * b1 ) * pa1;
		RU1.z = ( h1 - RV1.z * b1 ) * pa1;
	}
	else
	{
		pmn1 *= -1;
		float pb1 = 1.0f / b1;
		RU1.x = ( l1 * b1 - f1 * d1 ) * pmn1;
		RU1.y = ( m1 * b1 - g1 * d1 ) * pmn1;
		RU1.z = ( n1 * b1 - h1 * d1 ) * pmn1;
		RV1.x = ( f1 - RU1.x * a1 ) * pb1;
		RV1.y = ( g1 - RU1.y * a1 ) * pb1;
		RV1.z = ( h1 - RU1.z * a1 ) * pb1;
	}

	if (fabs(a2) > fabs(b2))
	{
		float pa2 = 1.0f / a2;
		RV2.x = ( l2 * a2 - f2 * c2 ) * pmn2;
		RV2.y = ( m2 * a2 - g2 * c2 ) * pmn2;
		RV2.z = ( n2 * a2 - h2 * c2 ) * pmn2;
		RU2.x = ( f2 - RV2.x * b2 ) * pa2;
		RU2.y = ( g2 - RV2.y * b2 ) * pa2;
		RU2.z = ( h2 - RV2.z * b2 ) * pa2;
	}
	else
	{
		pmn2 *= -1;
		float pb2 = 1.0f / b2;
		RU2.x = ( l2 * b2 - f2 * d2 ) * pmn2;
		RU2.y = ( m2 * b2 - g2 * d2 ) * pmn2;
		RU2.z = ( n2 * b2 - h2 * d2 ) * pmn2;
		RV2.x = ( f2 - RU2.x * a2 ) * pb2;
		RV2.y = ( g2 - RU2.y * a2 ) * pb2;
		RV2.z = ( h2 - RU2.z * a2 ) * pb2;
	}

	if (fabs(a3) > fabs(b3))
	{
		float pa3 = 1.0f / a3;
		RV3.x = ( l3 * a3 - f3 * c3 ) * pmn3;
		RV3.y = ( m3 * a3 - g3 * c3 ) * pmn3;
		RV3.z = ( n3 * a3 - h3 * c3 ) * pmn3;
		RU3.x = ( f3 - RV3.x * b3 ) * pa3;
		RU3.y = ( g3 - RV3.y * b3 ) * pa3;
		RU3.z = ( h3 - RV3.z * b3 ) * pa3;
	}
	else
	{
		pmn3 *= -1;
		float pb3 = 1.0f / b3;
		RU3.x = ( l3 * b3 - f3 * d3 ) * pmn3;
		RU3.y = ( m3 * b3 - g3 * d3 ) * pmn3;
		RU3.z = ( n3 * b3 - h3 * d3 ) * pmn3;
		RV3.x = ( f3 - RU3.x * a3 ) * pb3;
		RV3.y = ( g3 - RU3.y * a3 ) * pb3;
		RV3.z = ( h3 - RU3.z * a3 ) * pb3;
	}

	RU1.normalize();
	RU2.normalize();
	RU3.normalize();
	RV1.normalize();
	RV2.normalize();
	RV3.normalize();

	U.x = RU1.x * (1-u-v) + RU2.x * u + RU3.x * v;
	U.y = RU1.y * (1-u-v) + RU2.y * u + RU3.y * v;
	U.z = RU1.z * (1-u-v) + RU2.z * u + RU3.z * v;
	V.x = RV1.x * (1-u-v) + RV2.x * u + RV3.x * v;
	V.y = RV1.y * (1-u-v) + RV2.y * u + RV3.y * v;
	V.z = RV1.z * (1-u-v) + RV2.z * u + RV3.z * v;

	return true;
}


bool Triangle::evalUVvectors(Vector3d &TU1, Vector3d &TU2, Vector3d &TU3, Vector3d &TV1, Vector3d &TV2, Vector3d &TV3) const
{
	// result not normalized !!!!!
	float u12 = UV2.x-UV1.x;
	float u13 = UV3.x-UV1.x;
	float u23 = UV3.x-UV2.x;
	float u21 = UV1.x-UV2.x;
	float u31 = UV1.x-UV3.x;
	float u32 = UV2.x-UV3.x;
	float v12 = UV2.y-UV1.y;
	float v13 = UV3.y-UV1.y;
	float v23 = UV3.y-UV2.y;
	float v21 = UV1.y-UV2.y;
	float v31 = UV1.y-UV3.y;
	float v32 = UV2.y-UV3.y;

	if (u13*v12 == u12*v13  ||  u21*v23 == u23*v21  ||  u31*v32 == u32*v31)
	{
		TU1.x = TU1.y = TU1.z = 0.0f;
		TV1.x = TV1.y = TV1.z = 0.0f;
		TU2.x = TU2.y = TU2.z = 0.0f;
		TV2.x = TV2.y = TV2.z = 0.0f;
		TU3.x = TU3.y = TU3.z = 0.0f;
		TV3.x = TV3.y = TV3.z = 0.0f;
		return false;
	}
		
	// at V1
	if (fabs(u12)>=fabs(v12))
	{
		float u13pu12 = u13/u12;
		float pmn = 1.0f / (u13pu12*v12-v13);
		TV1 = ( (V1-V3) + (V2-V1)*u13pu12 ) * pmn;
		TU1 = (-TV1*v12 + (V2 - V1) ) * (1.0f/u12);
	}
	else
	{
		float v13pv12 = v13/v12;
		float pmn = 1.0f / (v13pv12*u12-u13);
		TU1 = ( (V1-V3) + (V2-V1)*v13pv12 ) * pmn;
		TV1 = (-TU1*u12 + (V2 - V1) ) * (1.0f/v12);
	}

	// at V2
	if (fabs(u23)>=fabs(v23))
	{
		float u21pu23 = u21/u23;
		float pmn = 1.0f / (u21pu23*v23-v21);
		TV2 = ( (V2-V1) + (V3-V2)*u21pu23 ) * pmn;
		TU2 = (-TV2*v23 + (V3 - V2) ) * (1.0f/u23);
	}
	else
	{
		float v21pv23 = v21/v23;
		float pmn = 1.0f / (v21pv23*u23-u21);
		TU2 = ( (V2-V1) + (V3-V2)*v21pv23 ) * pmn;
		TV2 = (-TU2*u23 + (V3 - V2) ) * (1.0f/v23);
	}

	// at V3
	if (fabs(u31)>=fabs(v31))
	{
		float u32pu31 = u32/u31;
		float pmn = 1.0f / (u32pu31*v31-v32);
		TV3 = ( (V3-V2) + (V1-V3)*u32pu31 ) * pmn;
		TU3 = (-TV3*v31 + (V1 - V3) ) * (1.0f/u31);
	}
	else
	{
		float v32pv31 = v32/v31;
		float pmn = 1.0f / (v32pv31*u31-u32);
		TU3 = ( (V3-V2) + (V1-V3)*v32pv31 ) * pmn;
		TV3 = (-TU3*u31 + (V1 - V3) ) * (1.0f/v31);
	}

	return true;
}
