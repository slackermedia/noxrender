#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "raytracer.h"
#include "QuasiRandom.h"
#include "PathTracing.h"
#include "Metropolis.h"
#include "log.h"
#include "embree.h"

bool Scene::postLoadInitialize(bool toMatEditor)
{
	sscene.bounces = 5;
	bool use_embree = Raytracer::getInstance()->use_embree;

	notifyProgress("Creating arrays", 0);
	triangles.createArray();
	mats.createArray();
	cameras.createArray();
	assignMaterials();
	updateUsedMaterials();

	assignMeshIDs();
	createDisplacementMeshes();

	notifyProgress("Evaluating UV dirs...", 0);
	char buf[512];
	for (int i=0; i<meshes.objCount; i++)
	{
		sprintf_s(buf, 512, "Evaluating UV dirs for %s", meshes[i].name);
		notifyProgress(buf, 0);
		meshes[i].evalUVdirs();
	}

	notifyProgress("Creating kd-tree", 0);

	sscene.bvh.meshes.createArray();
	if (!use_embree)
	{
		Logger::add("Adding meshes to BVH");
		for (int i=0; i<meshes.objCount; i++)
			sscene.bvh.addMesh(&meshes[i]);

		Logger::add("Creating BVH");
		bool bvhok = sscene.bvh.createTree();
		if (!bvhok)
			return false;
		Logger::add("BVH done");
	}

	bool bbok = evalSceneBoundingBox();
	if (!bbok)
		return false;

	updateSkyportals();
	createLightMeshes();

	if (use_embree)
	{
		Logger::add("Filling embree scene geometry");
		createEmbreeSceneInstances(this);
	}

	return true;
}


bool Scene::postRenderReleaseAll()
{
	return true;
}

bool Scene::updateLightMeshes()
{
	createLightMeshes();
	return true;
}

bool Scene::updateUsedMaterials()
{
	if (usedMaterials)
		free(usedMaterials);
	usedMaterials = NULL;
	int numMats = mats.objCount;
	usedMaterials = (bool*)malloc(sizeof(bool)*numMats);
	if (!usedMaterials)
		return false;
	for (int i=0; i<numMats; i++)
		usedMaterials[i] = false;

	for (int i=0; i<instances.objCount; i++)
	{
		MeshInstance * mInst = &(instances[i]);
		for (int j=0; j<mInst->materials.objCount; j++)
			usedMaterials[mInst->materials[j]] = true;
	}

	return true;
}

bool Scene::assignMaterials()
{
	int i,j;
	for (j=0; j<mats.objCount; j++)
	{
		if (mats[j] == 0)
			sscene.editableMaterial = j;
	}

	for (i=0; i<instances.objCount; i++)
	{
		MeshInstance * inst = &(instances[i]);
		inst->materials.freeList();
		for (j=0; j<inst->matsFromFile.objCount; j++)
		{
			int imatID = inst->matsFromFile[j];
			int imatNum = 0;
			for (int k=0; k<mats.objCount; k++)
			{
				if (mats[k]->id == imatID)
				{
					imatNum = k;
					break;
				}
			}
			inst->materials.add(imatNum);
		}
		inst->materials.createArray();
	}

	return true;
}


void Scene::deleteSceneStaticPointers(SceneStatic * ss)
{
	// dirty clean, without it destructor would delete original bsps
	ss->bvh.meshes.objCount = 0;
	ss->bvh.meshes.bheaders.buffer = 0;
	ss->bvh.meshes.bheaders.next = 0;
	ss->bvh.meshes.curNode = 0;
	ss->bvh.meshes.headers_array = 0;

	// deal with materials
	ss->defaultMat->deleteLayers();
	for (int i=0; i<ss->mats->objCount; i++)
		(*ss->mats)[i]->deleteLayers();
	ss->mats->destroyArray();
	delete ss->mats;

	// deal with lightmeshes
	for (int i=0; i<ss->lightMeshes->objCount; i++)
		(*ss->lightMeshes)[i]->freeArrays();	
	ss->lightMeshes->destroyArray();
	delete ss->lightMeshes;
}

bool canInstanceUseSameSource(MeshInstance * inst_new, MeshInstance * inst_old)
{
	Scene * sc = Raytracer::getInstance()->curScenePtr;
	if (!inst_new  ||  !inst_old) 
		return false;
	if (inst_new->materials.objCount != inst_new->materials.objCount)
		return false;
	int mc = inst_new->materials.objCount;
	for (int i=0; i<mc; i++)
	{
		MaterialNox * matnew = sc->mats[inst_new->materials[i]];
		MaterialNox * matold = sc->mats[inst_old->materials[i]];
		if (matnew == matold)
			continue;

		if (matnew->use_tex_displacement != matold->use_tex_displacement)
			return false;
		if (matnew->displ_continuity != matold->displ_continuity)
			return false;
		if (matnew->displ_shift != matold->displ_shift)
			return false;
		if (matnew->displ_normal_mode != matold->displ_normal_mode)
			return false;
		if (matnew->displ_depth != matold->displ_depth)
			return false;
		if (matnew->displ_subdivs_lvl != matold->displ_subdivs_lvl)
			return false;

		if (matnew->tex_displacement.managerID != matold->tex_displacement.managerID)
			return false;
		if (matnew->tex_displacement.texMod.blue != matold->tex_displacement.texMod.blue)
			return false;
		if (matnew->tex_displacement.texMod.brightness != matold->tex_displacement.texMod.brightness)
			return false;
		if (matnew->tex_displacement.texMod.contrast != matold->tex_displacement.texMod.contrast)
			return false;
		if (matnew->tex_displacement.texMod.gamma != matold->tex_displacement.texMod.gamma)
			return false;
		if (matnew->tex_displacement.texMod.green != matold->tex_displacement.texMod.green)
			return false;
		if (matnew->tex_displacement.texMod.hue != matold->tex_displacement.texMod.hue)
			return false;
		if (matnew->tex_displacement.texMod.interpolateProbe != matold->tex_displacement.texMod.interpolateProbe)
			return false;
		if (matnew->tex_displacement.texMod.invert != matold->tex_displacement.texMod.invert)
			return false;
		if (matnew->tex_displacement.texMod.red != matold->tex_displacement.texMod.red)
			return false;
		if (matnew->tex_displacement.texMod.saturation != matold->tex_displacement.texMod.saturation)
			return false;

		Vector3d scale_new = inst_new->matrix_transform.getScaleComponent();
		Vector3d scale_old = inst_old->matrix_transform.getScaleComponent();
		if ((scale_new.x==1 && scale_new.y==1 && scale_new.z==1)   ||   matnew->displ_ignore_scale)
		{
			if ((scale_old.x!=1 || scale_old.y!=1 || scale_old.z!=1)   &&   !matold->displ_ignore_scale)
				return false;
		}
		else
		{
			if (scale_new.x!=scale_old.x   ||   scale_new.y!=scale_old.y   ||   scale_new.z!=scale_old.z)
				return false;
		}
	}

	return true;
}

bool Scene::removeDisplacementMeshes()
{
	int num_instances = instances.objCount;
	int num_meshes = meshes.objCount;
	bool candelmesh = true;
	for (int j=num_meshes-1; j>=0; j--)
	{
		Mesh * mesh = &(meshes[j]);
		if (mesh->displacement_instance_id<0)
		{
			candelmesh = false;
			continue;
		}

		// find all instances of displaced mesh
		for (int i=0; i<num_instances; i++)
		{
			MeshInstance * inst = &(instances[i]);
			if (inst->meshID != j)
				continue;
			if (inst->displace_orig_ID>-1)
			{
				inst->meshID = inst->displace_orig_ID;
			}
			else
				inst->meshID = 0;
			inst->displace_orig_ID = -1;
			inst->has_displacement = false;
		}

		// del mesh tris - only if were last on list
		if (mesh->lastTri+1 == triangles.objCount)
		{
			triangles.trimLastObjects(mesh->lastTri+1-mesh->firstTri);
			triangles.createArray();
		}

		if (candelmesh)
		{
			//Logger::add("Deleting displaced mesh");
			meshes.trimLastObjects(1);
			meshes.createArray();
		}
	}

	return true;
}

bool Scene::createDisplacementMeshes()
{
	for (int i=0; i<instances.objCount; i++)
	{
		MeshInstance * inst = &(instances[i]);
		bool has_displacement = false;
		for (int j=0; j<inst->materials.objCount; j++)
		{
			MaterialNox * mat = mats[ inst->materials[j] ];
			if (!mat)
				continue;
			if (mat->tex_displacement.isValid()  &&  mat->use_tex_displacement  &&  mat->displ_depth>0.0f)
				has_displacement = true;
		}
		if (!has_displacement)
			continue;

		bool inst_exists = false;
		for (int j=0; j<instances.objCount; j++)
		{
			MeshInstance * inst_old = &(instances[j]);
			if (inst_old->displace_orig_ID < 0)
				continue;
			if (inst_old->displace_orig_ID != inst->meshID)
				continue;
			if (!canInstanceUseSameSource(inst, inst_old))
				continue;

			inst->has_displacement = true;
			inst->displace_orig_ID = inst->meshID;
			inst->meshID = inst_old->meshID;

			inst_exists = true;
			break;
		}

		if (inst_exists)
			continue;

		int meshID = createDisplacedMesh(i);
	}

	return true;
}

int Scene::createDisplacedMesh(int instID)
{
	if (instID<0 || instID>=instances.objCount)
		return -1;
	MeshInstance * inst = &(instances[instID]);

	int lastTri = -1;
	if (meshes.objCount>0)
		lastTri = meshes[meshes.objCount-1].lastTri;

	Mesh * meshsrc = &(meshes[inst->meshID]);
	Mesh tmesh;
	meshes.add(tmesh);
	meshes.createArray();
	Mesh * meshdst = &(meshes[meshes.objCount-1]);

	inst->displace_orig_ID = inst->meshID;
	inst->meshID = meshes.objCount-1;
	inst->has_displacement = true;

	meshdst->name = copyString(meshsrc->name, 20);
	int ln = (int)strlen(meshdst->name);
	sprintf_s(&(meshdst->name[ln]), 20, " displacement");

	meshdst->displacement_instance_id = instID;
	meshdst->instSrcID = -1;
	meshdst->firstTri = lastTri+1;
	meshdst->lastTri = lastTri;

	char prText[1024];
	sprintf_s(prText, 1024, "Creating displaced geometry for %s", meshsrc->name);

	int numtris = meshsrc->lastTri - meshsrc->firstTri + 1;
	if (numtris==0)
		Logger::add("sth wrong: src num tris = 0");
	for (int i=0; i<numtris; i++)
	{
		Triangle * tri = &(triangles[i+meshsrc->firstTri]);
		MaterialNox * mat = mats[inst->materials[tri->matInInst]];

		if (!mat->tex_displacement.isValid()  ||  mat->displ_depth==0.0f)
		{
			triangles.add(*tri);
			triangles.createArray();
			meshdst->lastTri++;
			lastTri++;
			continue;
		}

		// so tesselate
		int numtr = (1<<mat->displ_subdivs_lvl);
		bool ok = tri->addDisplacedTris(mat, inst);

		notifyProgress(prText, i*100.0f/numtris);

		meshdst->lastTri += numtr*numtr;
		lastTri += numtr*numtr;
	}

	int hole_added = fillDisplacementHoles(instID);
	meshdst->lastTri += hole_added;
	lastTri += hole_added;

	return meshes.objCount-1;
}

bool Scene::assignMeshIDs()
{
	for (int i=0; i<instances.objCount; i++)
	{
		MeshInstance * inst = &(instances[i]);
		if (inst->meshID>=0  &&  inst->meshID<meshes.objCount  &&  inst->instSrcID==meshes[inst->meshID].instSrcID)
		{
		}
		else
		{
			int id=-1;
			for (int i=0; i<meshes.objCount; i++)
			{
				if (meshes[i].instSrcID==inst->instSrcID)
					id = i;
			}
			if (id<0)
			{
				char errbuf[256];
				sprintf_s(errbuf, 256, "Error. Can't find mesh with instance src id = %d for %s", inst->instSrcID, inst->name);
				MessageBox(0, errbuf, "Error", 0);
			}
			else
			{
				inst->meshID = id;
			}
		}
	}

	return true;
}


