#define _CRT_RAND_S
#include "raytracer.h"
#include <math.h>
#include "log.h"
#include <float.h>
#include "ArHosekSkyModelData.h"

SunSky::SunSky()
{
	dirU = Vector3d( 0, 1, 0);
	dirN = Vector3d( 0, 0, 1);
	dirS = Vector3d( 0, 0,-1);
	dirE = Vector3d(-1, 0, 0);
	dirW = Vector3d( 1, 0, 0);

	month = 3;
	day = 21;
	hour = 12;
	minute = 0;
	aerosol = 0.1f;
	turbidity = 3.0f;
	setPosition(0,0);
	albedo = 0.5f;
	use_hosek = true;

	sunOn = false;
	sunOtherBlendLayer = true;
	sunSize = 32.0f/60.0f;
	maxCosSunSize = cos(sunSize/2*PI/180.0f);
	sunSizeSteradians = 2*PI*(1-cos(sunSize/2.0f *PI/180.0f));
	standardMeridianForTimeZone = 0;

	setDate(3,21,12,0.0f);
}

SunSky::~SunSky()
{
}

bool SunSky::copyFrom(SunSky * other)
{
	if (!other)
		return false;

	sunDir = other->sunDir;
	dirN = other->dirN;
	dirS = other->dirS;
	dirW = other->dirW;
	dirE = other->dirE;
	dirU = other->dirU;
	Ax = other->Ax;   Ay = other->Ay;   AY = other->AY;
	Bx = other->Bx;   By = other->By;   BY = other->BY;
	Cx = other->Cx;   Cy = other->Cy;   CY = other->CY;
	Dx = other->Dx;   Dy = other->Dy;   DY = other->DY;
	Ex = other->Ex;   Ey = other->Ey;   EY = other->EY;
	zx = other->zx;   zy = other->zy;   zY = other->zY;
	cosThetaS = other->cosThetaS;
	month = other->month;
	day = other->day;
	hour = other->hour;
	minute = other->minute;
	longitude = other->longitude;
	latitude = other->latitude;
	sinlat = other->sinlat;
	sinlong = other->sinlong;
	coslat = other->coslat;
	coslong = other->coslong;

	earthTimeInHours = other->earthTimeInHours;
	standardMeridianForTimeZone = other->standardMeridianForTimeZone;
	solarTimeInHours = other->solarTimeInHours;
	solarDeclination = other->solarDeclination;
	solarGlobalTimeInHours = other->solarGlobalTimeInHours;
	realPhiS = other->realPhiS;
	realThetaS = other->realThetaS;
	realSunDir = other->realSunDir;
	turbidity = other->turbidity;
	aerosol = other->aerosol;
	thetaS = other->thetaS;
	phiS = other->phiS;

	sunOn = other->sunOn;
	sunOtherBlendLayer = other->sunOtherBlendLayer;
	sunSize = other->sunSize;
	maxCosSunSize = other->maxCosSunSize;
	sunColor = other->sunColor;
	sunSizeSteradians = other->sunSizeSteradians;

	use_hosek = other->use_hosek;
	albedo = other->albedo;
	for (int i=0; i<3; i++)
		hosek_radiances[i] = other->hosek_radiances[i];
	for (int i=0; i<3; i++)
		for (int j=0; j<9; j++)
			hosek_abc[i][j] = other->hosek_abc[i][j];

	return true;
}

void SunSky::setSunDir(const Vector3d &sDir, const float &turb)
{
	turbidity = turb;
	sunDir = sDir;
	sunDir.normalize();
	cosThetaS = dirU * sunDir;
	thetaS = acos(cosThetaS);
	zx = getZenithx();
	zy = getZenithy();
	zY = getZenithY();
	Vector3d t1,t2;
	if (cosThetaS > 0)
		t1 = dirU^sDir;
	else
		t1 = sDir^dirU;
	t2 = sDir^t1;
	float c1,c2;
	c1 = t2*dirW;
	c2 = t2*dirS;
	phiS = atan2(-c1,-c2);
	//zx=zy=zY=1;
	Ax = getAx();
	Bx = getBx();
	Cx = getCx();
	Dx = getDx();
	Ex = getEx();
	Ay = getAy();
	By = getBy();
	Cy = getCy();
	Dy = getDy();
	Ey = getEy();
	AY = getAY();
	BY = getBY();
	CY = getCY();
	DY = getDY();
	EY = getEY();

	evalSunColor();
	updateHosekData();
}

//-----------------------------------------------------------------

void SunSky::setDate(int Month, int Day, int Hour, float Minute)
{
	month = Month;
	day = Day;
	hour = Hour;
	minute = Minute;
	evalEarthTimeInHours();
	calcSunPosition();

}

void SunSky::setPosition(float Longitude, float Latitude)
{
	longitude = Longitude;
	latitude = Latitude;
	coslat = cos(latitude);
	sinlat = sin(latitude);
	coslong = cos(longitude);
	sinlong = sin(longitude);
	calcSunPosition();
}

void SunSky::evalEarthTimeInHours()
{
	float t = 0;
	t = (float)getDayOfYear();
	t *= 24;
	t += hour;
	t += minute/60.0f;
	earthTimeInHours = t;
}

void SunSky::setStandardMeridianForTimeZone(float inRadians)
{
	standardMeridianForTimeZone = inRadians;
}

int SunSky::getDayOfYear()
{
	int m = 1;
	int t = 0;
	while (m < month)
	{
		switch (m)
		{
			case 1: t += 31; break;
			case 2: t += 28; break;
			case 3: t += 31; break;
			case 4: t += 30; break;
			case 5: t += 31; break;
			case 6: t += 30; break;
			case 7: t += 31; break;
			case 8: t += 31; break;
			case 9: t += 30; break;
			case 10: t += 31; break;
			case 11: t += 30; break;
		}
		m++;
	}
	t += day;
	return t;
}

void SunSky::calcSunPosition()
{	
	int J = getDayOfYear();
	float a = 0.170f*sin(4*PI*(J-80)/373.0f);
	float b = 0.129f*sin(2*PI*(J-8)/355.0f);
	float c1 = 12*(standardMeridianForTimeZone-longitude)/PI;
	float c2 = 12*(standardMeridianForTimeZone)/PI;
	solarTimeInHours = (float)(earthTimeInHours + a - b + c1);
	solarGlobalTimeInHours = (float)(earthTimeInHours + a - b + c2);
	solarDeclination = (float)(0.4093*sin(2*PI*(J-81)/368.0f));
	float sindec = sin(solarDeclination);
	float cosdec = cos(solarDeclination);

	solarTimeInHours -= 12;
	thetaS =  PI/2 - asin(sinlat*sindec + coslat*cosdec*cos(PI*solarTimeInHours/12));
	cosThetaS = cos(thetaS);
	float m,l;
	m = coslat*sindec - sinlat*cosdec*cos(PI*solarTimeInHours/12);
	l = -cosdec*sin(PI*solarTimeInHours/12.0f);
	phiS = atan2(l,m);

	Vector3d dir_thetaS_ = dirE*sin(phiS) + dirN*cos(phiS);
	Vector3d _sunDir;
	_sunDir = dir_thetaS_*sin(thetaS) + dirU*cosThetaS;
	setSunDir(_sunDir, turbidity);
	realPhiS =  -(getSolarGlobalTimeInHours()/24 - (int)(getSolarGlobalTimeInHours()/24)) * 2*PI + 2*PI;
	realThetaS = -PI/2 + getSolarDeclination();
	realSunDir.getFromAngles(realThetaS, realPhiS);
	evalSunColor();

	updateHosekData();
}

Vector3d SunSky::getSunDirection()
{
	return sunDir;
}

Vector3d SunSky::getRandomPerturbedSunDirection()
{
	#ifdef RAND_PER_THREAD
		float rtheta = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float rtheta = getMarsaglia()/(float)(unsigned int)(MAXMARS);
	#endif
	rtheta = sqrt(rtheta) * sunSize * PI/180.0f/2;

	float psin = sin(rtheta);

	Vector3d rvect = Vector3d::random();
	Vector3d prvect = rvect^sunDir;
	Vector3d res = sunDir + prvect * psin;
	res.normalize();
	return res;
}

float SunSky::getSunSizeSteradians()
{
	return sunSizeSteradians;
	float result = 2*PI*(1-cos(sunSize/2.0f *PI/180.0f));

	return result;
	return 2*PI*(1-cos(sunSize/2.0f *PI/180.0f));
	return sunSize*sunSize*PI/180*PI/180*PI/4;
}

//-------------------------------------------------------------------------------

float SunSky::Fx(float costheta, float gamma)
{
	return Fx(costheta, gamma, cos(gamma));
}

float SunSky::Fx(float costheta, float gamma, float cosGamma)
{
	float cosT = costheta;
	if (cosT < 0.00001f)
		cosT = 0.00001f;
	return (1 + Ax*exp(Bx/cosT)) * (1 + Cx*exp(Dx*gamma) + Ex*cosGamma*cosGamma);
}

float SunSky::Fy(float costheta, float gamma)
{
	return Fy(costheta, gamma, cos(gamma));
}

float SunSky::Fy(float costheta, float gamma, float cosGamma)
{
	float cosT = costheta;
	if (cosT < 0.00001f)
		cosT = 0.00001f;
	return (1 + Ay*exp(By/cosT)) * (1 + Cy*exp(Dy*gamma) + Ey*cosGamma*cosGamma);
}

float SunSky::FY(float costheta, float gamma)
{
	return FY(costheta, gamma, cos(gamma));
}

float SunSky::FY(float costheta, float gamma, float cosGamma)
{
	float cosT = costheta;
	if (cosT < 0.00001f)
		cosT = 0.00001f;
	return (1 + AY*exp(BY/cosT)) * (1 + CY*exp(DY*gamma) + EY*cosGamma*cosGamma);
}

float SunSky::getZenithx()
{
	float t1,t2;
	float s1,s2,s3;
	float tp0, tp1, tp2;
	t1 = turbidity;
	t2 = turbidity*turbidity;
	s1 = (thetaS);
	s2 = thetaS*thetaS;
	s3 = (thetaS*thetaS*thetaS);
	tp2 =  0.00166f*s3 - 0.00375f*s2 + 0.00209f*s1;
	tp1 = -0.02903f*s3 + 0.06377f*s2 - 0.03202f*s1 + 0.00394f;
	tp0 =  0.11693f*s3 - 0.21296f*s2 + 0.06052f*s1 + 0.25886f;
	return tp0 + t1*tp1 + t2*tp2;
}

float SunSky::getZenithy()
{
	float t1,t2;
	float s1,s2,s3;
	float tp0, tp1, tp2;
	t1 = turbidity;
	t2 = turbidity*turbidity;
	s1 = (thetaS);
	s2 = thetaS*thetaS;
	s3 = (thetaS*thetaS*thetaS);
	tp2 =  0.00275f*s3 - 0.00610f*s2 + 0.00317f*s1;
	tp1 = -0.04214f*s3 + 0.08970f*s2 - 0.04153f*s1 + 0.00516f;
	tp0 =  0.15346f*s3 - 0.26756f*s2 + 0.06670f*s1 + 0.26688f;
	return tp0 + t1*tp1 + t2*tp2;
}

float SunSky::getZenithY()
{
	return 1000.0f*((4.0453f*turbidity - 4.9710f) * tan((4.0f/9.0f-turbidity/120.0f)*(PI-2*thetaS))   - 0.2155f*turbidity + 2.4192f);
}

float SunSky::getx(float costheta, float gamma)
{
	return zx*Fx(costheta, gamma)/Fx(1, thetaS, cosThetaS);
}

float SunSky::gety(float costheta, float gamma)
{
	return zy*Fy(costheta, gamma)/Fy(1, thetaS, cosThetaS);
}

float SunSky::getY(float costheta, float gamma)
{
	return zY*FY(costheta, gamma)/FY(1, thetaS, cosThetaS);
}

Color4 SunSky::getSunOutsideColor()
{
	return Color4(2000, 2500, 3000);
}

Color4 SunSky::getColor(const Vector3d &direction)
{
	if (use_hosek)
	{
		return getHosekSkyColor(direction);
	}

	float gamma;
	float x,y,Y;

	if (cosThetaS < - 0.7f)
		return Color4(0,0,0);

	float costheta = dirU * direction;
	float cosgamma = direction*sunDir;
	gamma = acos(cosgamma);

	x = getx(costheta, gamma);
	y = gety(costheta, gamma);
	Y = getY(costheta, gamma);

	float XX = x*Y/y;
	float YY = Y;
	float ZZ = (1-x-y)*Y/y;
	float RR = (3.24156f*XX) - (1.53767f*YY) - (0.49870f*ZZ);
	float GG = -(0.96920f*XX) + (1.87589f*YY) + (0.04155f*ZZ);
	float BB = (0.05562f*XX) - (0.20396f*YY) + (1.05686f*ZZ);

	Color4 result = Color4(RR,GG,BB);

	if (_isnan(result.r)  ||  _isnan(result.g)  ||  _isnan(result.b))
	{
		char buf[512];
		sprintf_s(buf, 512, "x=%f   y=%f   Y=%f    costheta=%f   cosgamma=%f   zx=%f  zy=%f  zY=%f... returning 0",
				x,y,Y,costheta, cosgamma, zx, zy, zY);
		Logger::add(buf);
		return Color4(0,0,0);
	}

	return result;
}

Color4 SunSky::getSunColor()
{
	return sunColor;
}

void SunSky::evalSunColor()
{
	if (cosThetaS < 0)
		sunColor = Color4(0,0,0);
	float m = cos(thetaS) + 0.15f*pow(93.885f-(thetaS*180.0f/PI), -1.253f);
	m = 1/m;
	float tr,tg,tb;
	
	// rayleigh scattering
	tr = exp(-0.008735f*m*pow(0.700f, -4.08f));
	tg = exp(-0.008735f*m*pow(0.520f, -4.08f));
	tb = exp(-0.008735f*m*pow(0.460f, -4.08f));

	// aerosol 
	float beta = 0.04608f*turbidity - 0.04586f;
	float alfa = aerosol;
	alfa = 1.3f;
	tr *= exp(-beta*m*pow(0.7f , -alfa));
	tg *= exp(-beta*m*pow(0.52f, -alfa));
	tb *= exp(-beta*m*pow(0.46f, -alfa));

	// ozone
	float l = 0.35f;
	tr *= exp(-0.023f*(l*m));
	tg *= exp(-0.048f*(l*m));
	tb *= exp(-0.006f*(l*m));

	float mn = 120000.0f / getSunSizeSteradians();                     
	tr*=mn; tg*=mn; tb*=mn; 

	sunColor = Color4(tr, tg, tb);
}

bool SunSky::isEnlighted(float ttheta, float tphi)
{
	Vector3d ttdir;
	ttdir.getFromAngles(ttheta, tphi);
	if (ttdir * realSunDir > 0)
		return true;
	else
		return false;
}

unsigned char SunSky::getEnlightedAlpha(float ttheta, float tphi)
{
	Vector3d ttdir;
	ttdir.getFromAngles(ttheta, tphi);
	
	return (unsigned char)(max(0.5f, min((ttdir*realSunDir+1.0f/30.0)*15,1.0f))*255);
}


Vector3d SunSky::randomDirOnHemisphere()
{
	Vector3d rdir = Vector3d::random();
	Vector3d res = dirE * rdir.x  +  dirN * rdir.y  +  dirU * rdir.z;
	res.normalize();
	if (rdir.z < 0)
		res.invert();
	return res;
}

bool SunSky::updateHosekData()
{
	float sunangle = acos(sunDir * dirU);
	sunangle = max(0, PI/2-sunangle);
	if (sunangle>=0)
		sunangle = pow(sunangle / (PI/2.0f), (1.0f/3.0f));
	else
		sunangle = -pow(-sunangle / (PI/2.0f), (1.0f/3.0f));
	int int_turbidity = (int)turbidity;

	double * elev_matrix;
	float albedovec[3] = { albedo/0.95047176f,albedo/0.99995720f, albedo/1.0889783f };

    for (unsigned int ch=0; ch<3; ch++)
	{
		elev_matrix = datasetsXYZ[ch] + ( 9 * 6 * (int_turbidity-1) );
		float turb_remain = turbidity - int_turbidity;
    
		for( unsigned int i = 0; i < 9; ++i )
		{
			hosek_abc[ch][i] = (float) (
			(1.0f-albedovec[ch]) * (1.0 - turb_remain) 
			* (        pow(1.0f-sunangle, 5.0f)                       * elev_matrix[i]  + 
			   5.0f  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[i+9] +
			   10.0f * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[i+18] +
			   10.0f * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[i+27] +
			   5.0f  *    (1.0f-sunangle)       * pow(sunangle, 4.0f) * elev_matrix[i+36] +
			                                      pow(sunangle, 5.0f) * elev_matrix[i+45]) );
		}

		// alb 1 low turb
		elev_matrix = datasetsXYZ[ch] + (9*6*10 + 9*6*(int_turbidity-1));
		for(unsigned int i = 0; i < 9; ++i)
		{
			hosek_abc[ch][i] += (float) (
			(albedovec[ch]) * (1.0 - turb_remain)
			* (       pow(1.0f-sunangle, 5.0f)                       * elev_matrix[i]  + 
			   5.0  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[i+9] +
			   10.0 * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[i+18] +
			   10.0 * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[i+27] +
			   5.0  *    (1.0f-sunangle)       * pow(sunangle, 4.0f) * elev_matrix[i+36] +
			                                     pow(sunangle, 5.0f) * elev_matrix[i+45]) );
		}

		if(int_turbidity == 10)
			continue;

		// alb 0 high turb
		elev_matrix = datasetsXYZ[ch] + (9*6*(int_turbidity));
		for(unsigned int i = 0; i < 9; ++i)
		{
			hosek_abc[ch][i] += (float) (
			(1.0-albedovec[ch]) * (turb_remain)
			* (        pow(1.0f-sunangle, 5.0f)                       * elev_matrix[i]  + 
			   5.0f  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[i+9] +
			   10.0f * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[i+18] +
			   10.0f * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[i+27] +
			   5.0f  *    (1.0f-sunangle)       * pow(sunangle, 4.0f) * elev_matrix[i+36] +
			                                      pow(sunangle, 5.0f) * elev_matrix[i+45]) );
		}

		// alb 1 high turb
		elev_matrix = datasetsXYZ[ch] + (9*6*10 + 9*6*(int_turbidity));
		for(unsigned int i = 0; i < 9; ++i)
		{
			hosek_abc[ch][i] += (float) (
			(albedovec[ch]) * (turb_remain)
			* (        pow(1.0f-sunangle, 5.0f)                       * elev_matrix[i]  + 
			   5.0f  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[i+9] +
			   10.0f * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[i+18] +
			   10.0f * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[i+27] +
			   5.0f  *    (1.0-sunangle)        * pow(sunangle, 4.0f) * elev_matrix[i+36] +
			                                      pow(sunangle, 5.0f) * elev_matrix[i+45]) );
		}
	}

	// radiances
    for (unsigned int ch=0; ch<3; ch++)
	{
		elev_matrix = datasetsXYZRad[ch] + (6*(int_turbidity-1));
		float turb_remain = turbidity - int_turbidity;
    
		hosek_radiances[ch] = (float) (
		(1.0-albedovec[ch]) * (1.0 - turb_remain) 
		* (        pow(1.0f-sunangle, 5.0f)                       * elev_matrix[0] + 
		   5.0f  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[1] +
		   10.0f * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[2] +
		   10.0f * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[3] +
		   5.0f  *    (1.0f-sunangle)       * pow(sunangle, 4.0f) * elev_matrix[4] +
		                                      pow(sunangle, 5.0f) * elev_matrix[5]) );

		// alb 1 low turb
		elev_matrix = datasetsXYZRad[ch] + (6*10 + 6*(int_turbidity-1));
		hosek_radiances[ch] += (float) (
		(albedovec[ch]) * (1.0 - turb_remain)
		* (       pow(1.0f-sunangle, 5.0f)                       * elev_matrix[0] + 
		   5.0  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[1] +
		   10.0 * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[2] +
		   10.0 * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[3] +
		   5.0  *    (1.0f-sunangle)       * pow(sunangle, 4.0f) * elev_matrix[4] +
		                                     pow(sunangle, 5.0f) * elev_matrix[5]) );

		if(int_turbidity == 10)
			continue;

		// alb 0 high turb
		elev_matrix = datasetsXYZRad[ch] + (6*(int_turbidity));
		hosek_radiances[ch] += (float) (
		(1.0-albedovec[ch]) * (turb_remain)
		* (        pow(1.0f-sunangle, 5.0f)                       * elev_matrix[0] + 
		   5.0f  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[1] +
		   10.0f * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[2] +
		   10.0f * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[3] +
		   5.0f  *    (1.0f-sunangle)       * pow(sunangle, 4.0f) * elev_matrix[4] +
		                                      pow(sunangle, 5.0f) * elev_matrix[5]) );

		// alb 1 high turb
		elev_matrix = datasetsXYZRad[ch] + (6*10 + 6*(int_turbidity));
		hosek_radiances[ch] += (float) (
		(albedovec[ch]) * (turb_remain)
		* (        pow(1.0f-sunangle, 5.0f)                       * elev_matrix[0] + 
		   5.0f  * pow(1.0f-sunangle, 4.0f) *     sunangle        * elev_matrix[1] +
		   10.0f * pow(1.0f-sunangle, 3.0f) * pow(sunangle, 2.0f) * elev_matrix[2] +
		   10.0f * pow(1.0f-sunangle, 2.0f) * pow(sunangle, 3.0f) * elev_matrix[3] +
		   5.0f  *    (1.0-sunangle)        * pow(sunangle, 4.0f) * elev_matrix[4] +
		                                      pow(sunangle, 5.0f) * elev_matrix[5]) );
	}

	return true;
}

Color4 SunSky::getHosekSkyColor(Vector3d dir)
{
	Color4 res;
	dir.normalize();

	float costheta = dir * dirU;
	if (costheta < 0)
	{
		Vector3d tdir = dir ^ dirU;
		dir = dirU ^ tdir;
		dir.normalize();
		costheta = dir * dirU;
	}

	float cosgamma = sunDir * dir;
	float gamma = acos(cosgamma);
	float theta = acos(costheta);
	float xyz[3];
	for (int ch=0; ch<3; ch++)
	{
		float expM = exp(hosek_abc[ch][4] * gamma);
		float rayM = cosgamma*cosgamma;
		float mieM = (1.0f + cosgamma*cosgamma) / pow((1.0f + hosek_abc[ch][8]*hosek_abc[ch][8] - 2.0f*hosek_abc[ch][8]*cosgamma), 1.5f);
		float zenith = sqrt(fabs(costheta));

		xyz[ch] = (1.0f + hosek_abc[ch][0] * exp(hosek_abc[ch][1] / (cos(theta) + 0.01f))) *
				  (hosek_abc[ch][2] + hosek_abc[ch][3] * expM + hosek_abc[ch][5] * rayM + hosek_abc[ch][6] * mieM + hosek_abc[ch][7] * zenith);

		xyz[ch] *= hosek_radiances[ch];
	}

	res.r =  (3.24156f*xyz[0]) - (1.53767f*xyz[1]) - (0.49870f*xyz[2]);
	res.g = -(0.96920f*xyz[0]) + (1.87589f*xyz[1]) + (0.04155f*xyz[2]);
	res.b =  (0.05562f*xyz[0]) - (0.20396f*xyz[1]) + (1.05686f*xyz[2]);

	return res * 1000;
}

void SunSky::makeLog()
{
	char buf[256];
	Logger::add("Sunsky whole log");

	sprintf_s(buf, 256, "  turbidity: %f,  albedo: %f,  aerosol: %f", turbidity, albedo, aerosol);
	Logger::add(buf);
	sprintf_s(buf, 256, "  N: %.3f  %.3f  %.3f    S: %.3f  %.3f  %.3f    W: %.3f  %.3f  %.3f    E: %.3f  %.3f  %.3f    U: %.3f  %.3f  %.3f    ",
				dirN.x, dirN.y, dirN.z,   dirS.x, dirS.y, dirS.z,   dirW.x, dirW.y, dirW.z,   dirE.x, dirE.y, dirE.z,   dirU.x, dirU.y, dirU.z);
	Logger::add(buf);
	sprintf_s(buf, 256, "  sun %s at layer %s  model %s", sunOn?"on":"off", sunOtherBlendLayer?"2":"1", use_hosek?"hosek-wilkie":"preetham");
	Logger::add(buf);
	sprintf_s(buf, 256, "  Date: %d-%d  %d:%.2f", month, day, hour, minute);
	Logger::add(buf);
	sprintf_s(buf, 256, "  long: %.4f, lat: %.4f ... sin/cos:  %.4f %.4f %.4f %.4f", longitude, latitude, sinlat, sinlong, coslat, coslong);
	Logger::add(buf);
	sprintf_s(buf, 256, "  solartime h: %f   global: %f   declination: %f", solarTimeInHours, solarGlobalTimeInHours, solarDeclination);
	Logger::add(buf);
	sprintf_s(buf, 256, "sunDir: %.4f %.4f %.4f     real:  %.4f %.4f %.4f   phi: %.4f  theta: %.4f   rphi: %.4f  rtheta: %.4f", 
				sunDir.x, sunDir.y, sunDir.z, realSunDir.x, realSunDir.y, realSunDir.z, phiS, thetaS, realPhiS, realThetaS);
	Logger::add(buf);
	sprintf_s(buf, 256, "sun color:  %.4f %.4f %.4f", sunColor.r, sunColor.g, sunColor.b);
	Logger::add(buf);
	sprintf_s(buf, 256, "sun size: %.4f   in steradians: %.6f   maxcos: %.4f", sunSize, sunSizeSteradians, maxCosSunSize);
	Logger::add(buf);


}

bool SunSky::evalAerialPerspective(Vector3d dir, float dist, Color4 &atten, Color4 &skyIn, Color4 &sunIn)
{
	skyIn = Color4(0,0,0);
	sunIn = Color4(0,0,0);
	atten = Color4(1,1,1);
	// red : 700 nm 
	// green : 520 nm
	// blue : 460 nm

	float costheta = dir * dirU;
	float alpha_haze = 0.8333f/1000, alpha_mol = 0.1136f/1000;	// 1/km !!!!
	Color4 beta_haze, beta_mol;
	beta_mol.r = 8*PI*PI*PI*pow(1.0003f*1.0003f-1, 2)/3/2.545e25f/pow(0.700e-6f,4)*(6+3*0.035f)/(6-7*0.035f);
	beta_mol.g = 8*PI*PI*PI*pow(1.0003f*1.0003f-1, 2)/3/2.545e25f/pow(0.520e-6f,4)*(6+3*0.035f)/(6-7*0.035f);
	beta_mol.b = 8*PI*PI*PI*pow(1.0003f*1.0003f-1, 2)/3/2.545e25f/pow(0.460e-6f,4)*(6+3*0.035f)/(6-7*0.035f);

	float cc = (0.6544f*turbidity - 0.6510f) * 1.0e-16f;
	beta_haze.r = 0.434f * cc * PI * pow(2.0f * PI / 700.0e-9f , 2) * 0.687112f;
	beta_haze.g = 0.434f * cc * PI * pow(2.0f * PI / 520.0e-9f , 2) * 0.674462f;
	beta_haze.b = 0.434f * cc * PI * pow(2.0f * PI / 460.0e-9f , 2) * 0.667276f;

	float h0 = 20.0f;
	float H_haze = exp(-alpha_haze * h0);
	float H_mol  = exp(-alpha_mol  * h0);
	Color4 K_haze = beta_haze*(-1/alpha_haze/costheta);
	Color4 K_mol  = beta_mol *(-1/alpha_mol /costheta);
	float u_haze = exp(-alpha_haze*(h0+dist*costheta));
	float u_mol  = exp(-alpha_mol *(h0+dist*costheta));

	Color4 Tau_haze, Tau_mol;
	Tau_haze.r = exp(K_haze.r*(H_haze-u_haze));
	Tau_haze.g = exp(K_haze.g*(H_haze-u_haze));
	Tau_haze.b = exp(K_haze.b*(H_haze-u_haze));
	Tau_mol.r  = exp(K_mol.r *(H_mol -u_mol ));
	Tau_mol.g  = exp(K_mol.g *(H_mol -u_mol ));
	Tau_mol.b  = exp(K_mol.b *(H_mol -u_mol ));

	Tau_haze.r = exp(-beta_haze.r * dist*alpha_haze);
	Tau_haze.g = exp(-beta_haze.g * dist*alpha_haze);
	Tau_haze.b = exp(-beta_haze.b * dist*alpha_haze);

	atten = Tau_mol * Tau_haze;

	Color4 suncol = getSunColor() * getSunSizeSteradians();
	float c2 = getSunDirection()*dir;
	Color4 scadd = suncol*0.75*(1+c2*c2)*(1/PI);
	sunIn.r = scadd.r * (1-atten.r);
	sunIn.g = scadd.g * (1-atten.g);
	sunIn.b = scadd.b * (1-atten.b);

	return true;
}

