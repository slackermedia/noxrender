#include <windows.h>
#include <stdio.h>
#include "EMControls.h"
#include <math.h>
#include "log.h"
extern HMODULE hDllModule;


EMCurve::EMCurve(HWND hWnd) 
{
	hwnd = hWnd;
	margin = 1;
	SetWindowPos(hwnd, HWND_TOP, 0, 0, 256+2*margin,256+2*margin, SWP_NOMOVE | SWP_NOZORDER);
	colBackGnd   = RGB(40,40,40);
	colDotLines  = RGB(80,80,80);
	colSelOuter  = RGB(255,255,255);
	colBorder    = RGB(0,0,0);
	colLineRed   = RGB(255,0,0);
	colLineGreen = RGB(0,255,0);
	colLineBlue  = RGB(0,0,255);
	colLineLight = RGB(224,224,224);

	isLuminocityMode = false;
	activeRGB[0] = true;
	activeRGB[1] = true;
	activeRGB[2] = true;
	activeRGB[3] = false;
	lastIgnoredRGB[0] = -1;
	lastIgnoredRGB[1] = -1;
	lastIgnoredRGB[2] = -1;
	lastIgnoredRGB[3] = -1;
	reset();

	mOverRGB[0] = false;
	mOverRGB[1] = false;
	mOverRGB[2] = false;
	mOverRGB[4] = false;
	mOverIndexRGB[0] = -1;
	mOverIndexRGB[1] = -1;
	mOverIndexRGB[2] = -1;
	mOverIndexRGB[3] = -1;
	nowDragging = false;

	createLines();

}

EMCurve::~EMCurve() 
{
	pointsR.clear();
	pointsG.clear();
	pointsB.clear();
	pointsL.clear();
}

void InitEMCurve()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMCurve";
    wc.hInstance      = hDllModule;
	wc.lpfnWndProc    = EMCurveProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMCurve *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMCurve * GetEMCurveInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMCurve * emc = (EMCurve *)GetWindowLongPtr(hwnd, 0);
	#else
		EMCurve * emc = (EMCurve *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emc;
}

void SetEMCurveInstance(HWND hwnd, EMCurve *emc)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emc);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emc);
	#endif
}

LRESULT CALLBACK EMCurveProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMCurve * emc;
	emc = GetEMCurveInstance(hwnd);

    switch(msg)
    {
	case WM_CREATE:
		{
			EMCurve * emc1 = new EMCurve(hwnd);
			SetEMCurveInstance(hwnd, (EMCurve *)emc1);
		}
	break;
	case WM_NCDESTROY:
		{
			EMCurve * emc1;
			emc1 = GetEMCurveInstance(hwnd);
			delete emc1;
			SetEMCurveInstance(hwnd, 0);
		}
	break;
	case WM_PAINT:
	{
		if (!emc) 
			break;

		RECT rect;
		HDC hdc, ohdc;
		PAINTSTRUCT ps;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);

		COLORREF bgcol = emc->colBackGnd;

		ohdc = BeginPaint(hwnd, &ps);

		// create back buffer
		hdc = CreateCompatibleDC(ohdc);
		GetClientRect(hwnd, &rect);
		HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
		SelectObject(hdc, backBMP);

		// draw background
		HBRUSH bgbr = CreateSolidBrush(bgcol);
		HBRUSH oldbr = (HBRUSH)SelectObject(hdc, bgbr);
		Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
		SelectObject(hdc, oldbr);
		DeleteObject(bgbr);

		// draw dotted lines
		HPEN dPen = CreatePen(PS_DOT, 1 , emc->colDotLines);
		HPEN oldpen = (HPEN)SelectObject(hdc, dPen);
		SetBkColor(hdc, bgcol);
		POINT d1[] = { {1,64}  , {257,64}  };
		POINT d2[] = { {1,128} , {257,128} };
		POINT d3[] = { {1,192} , {257,192} };
		POINT d4[] = { {64,1}  , {64,257}  };
		POINT d5[] = { {128,1} , {128,257} };
		POINT d6[] = { {192,1} , {192,257} };
		POINT d7[] = { {1,256} , {257,0}   };
		Polyline(hdc, d1, 2);
		Polyline(hdc, d2, 2);
		Polyline(hdc, d3, 2);
		Polyline(hdc, d4, 2);
		Polyline(hdc, d5, 2);
		Polyline(hdc, d6, 2);
		Polyline(hdc, d7, 2);
		SelectObject(hdc, oldpen);
		DeleteObject(dPen);


		int ist, ien;
		if (emc->isLuminocityMode)
		{	ist = 3;	ien = 4;	}
		else
		{	ist = 0;	ien = 3;	}

		COLORREF cols[] = { emc->colLineRed, emc->colLineGreen, emc->colLineBlue, emc->colLineLight};

		// draw lines
		for (int i=ist; i<ien; i++)
		{
			HPEN bPen = CreatePen(PS_SOLID, emc->activeRGB[i] ? 2 : 1 , cols[i]);
			oldpen = (HPEN)SelectObject(hdc, bPen);

			SetROP2(hdc, R2_MERGEPEN);

			Polyline(hdc, emc->getLine(i), 257);
			SelectObject(hdc, oldpen);
			DeleteObject(bPen);
			SetROP2(hdc, R2_COPYPEN);
		}

		// draw inactive points 
		for (int i=ist; i<ien; i++)
		{
			if (emc->activeRGB[i])
				continue;
			oldbr = (HBRUSH)SelectObject(hdc, CreateSolidBrush( //cols[i]));
					RGB(255-GetRValue(cols[i]), 255-GetGValue(cols[i]), 255-GetBValue(cols[i]))));
			oldpen = (HPEN)SelectObject(hdc, CreatePen(PS_NULL, 1, RGB(255,255,255)));
			for (unsigned int j=0; j<emc->getPoints(i).size(); j++)
			{
				Rectangle(hdc, 
					emc->margin +     emc->getPoints(i)[j].x-1,
					emc->margin + 255-emc->getPoints(i)[j].y-1,
					emc->margin +     emc->getPoints(i)[j].x+3,
					emc->margin + 255-emc->getPoints(i)[j].y+3 );
				SetPixel(hdc,
					emc->margin +     emc->getPoints(i)[j].x,
					emc->margin + 255-emc->getPoints(i)[j].y,
					RGB(GetRValue(cols[i]), GetGValue(cols[i]), GetBValue(cols[i])));
			}
			DeleteObject(SelectObject(hdc, oldpen));
			DeleteObject(SelectObject(hdc, oldbr));
		}

		// draw active points
		for (int i=ist; i<ien; i++)
		{
			if (!emc->activeRGB[i])
				continue;
			oldbr = (HBRUSH)SelectObject(hdc, CreateSolidBrush(cols[i]));
			oldpen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emc->colSelOuter));
			for (unsigned int j=0; j<emc->getPoints(i).size(); j++)
			{
				Ellipse(hdc,
					emc->margin +     emc->getPoints(i)[j].x-3,
					emc->margin + 255-emc->getPoints(i)[j].y-3,
					emc->margin +     emc->getPoints(i)[j].x+4,
					emc->margin + 255-emc->getPoints(i)[j].y+4 );
			}
			DeleteObject(SelectObject(hdc, oldpen));
			DeleteObject(SelectObject(hdc, oldbr));
		}

		// draw point that mouse is over
		for (int i=ist; i<ien; i++)
			if (emc->mOverIndexRGB[i] > -1)
			{
				int rx, ry;
				rx = emc->getPoints(i)[emc->mOverIndexRGB[i]].x; 
				ry = emc->getPoints(i)[emc->mOverIndexRGB[i]].y;
				
				HPEN sPen = CreatePen(PS_SOLID, 1, 
					RGB(255-GetRValue(cols[i]), 255-GetGValue(cols[i]), 255-GetBValue(cols[i])));
				oldpen = (HPEN)SelectObject(hdc, sPen);
				HBRUSH sbr = CreateSolidBrush(emc->colSelOuter);
				oldbr = (HBRUSH)SelectObject(hdc, sbr);
				Ellipse(hdc,
						emc->margin +     rx-3,
						emc->margin + 255-ry-3,
						emc->margin +     rx+4,
						emc->margin + 255-ry+4 );
				SelectObject(hdc, oldpen);
				DeleteObject(sPen);
				SelectObject(hdc, oldbr);
				DeleteObject(sbr);
			}

		// draw a border
		HPEN sPen = CreatePen(PS_SOLID, 1, emc->colBorder);
		oldpen = (HPEN)SelectObject(hdc, sPen);
		LOGBRUSH lb;
		lb.lbColor = RGB(0,0,0);
		lb.lbHatch = HS_VERTICAL;
		lb.lbStyle = BS_NULL;
		HBRUSH sbr = CreateBrushIndirect(&lb);
		oldbr = (HBRUSH)SelectObject(hdc, sbr);
		Rectangle(hdc, 0,0, 256+2*emc->margin, 256+2*emc->margin);
		SelectObject(hdc, oldpen);
		DeleteObject(sPen);
		SelectObject(hdc, oldbr);
		DeleteObject(sbr);


		// copy from back buffer
		GetClientRect(hwnd, &rect);
		BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
		DeleteDC(hdc); 
		DeleteObject(backBMP);
		EndPaint(hwnd, &ps);

	}
	break;
	case WM_MOUSEMOVE:
	{
		POINT pt, pt1;
		pt.x = (short)LOWORD(lParam);
		pt.y = (short)HIWORD(lParam);
		pt1.x = min(255, max(0, pt.x-emc->margin));
		pt1.y = min(255, max(0, 255-pt.y-emc->margin));

		int ist, ien;
		if (emc->isLuminocityMode)
		{	ist = 3;	ien = 4;	}
		else
		{	ist = 0;	ien = 3;	}

		if (emc->nowDragging)
		{
			for (int i=ist; i<ien; i++)
			{
				if (!emc->mOverRGB[i])
					continue;
				if (!emc->activeRGB[i])
					continue;
				int already = emc->xPointExists(pt1.x, emc->getPoints(i), emc->mOverIndexRGB[i]);
				emc->getPoints(i)[emc->mOverIndexRGB[i]].x = pt1.x;
				emc->getPoints(i)[emc->mOverIndexRGB[i]].y = pt1.y;
				emc->createLine(i, already);
				InvalidateRect(emc->hwnd, NULL, false);
				emc->lastIgnoredRGB[i] = already;
			}
			emc->notifyChanged();
		}
		else
		{
			bool repaint = false;
			for (int i=ist; i<ien; i++)
			{
				if (!emc->activeRGB[i])
					continue;
				int a = emc->findPointAround(emc->getPoints(i), pt1.x, pt1.y);
				if (emc->mOverIndexRGB[i] != a)
					repaint = true;
				emc->mOverIndexRGB[i] = a;
				emc->mOverRGB[i] = (a>=0);
			}
			if (repaint)
				InvalidateRect(emc->hwnd, NULL, false);
		}


	}
	break;
	case WM_RBUTTONDOWN:
	{
		POINT pt, pt1;
		pt.x = (short)LOWORD(lParam);
		pt.y = (short)HIWORD(lParam);
		pt1.x = min(255, max(0, pt.x-emc->margin));
		pt1.y = min(255, max(0, 255-pt.y-emc->margin));

		int ist, ien;
		if (emc->isLuminocityMode)
		{	ist = 3;	ien = 4;	}
		else
		{	ist = 0;	ien = 3;	}

		for (int i=ist; i<ien; i++)
		{
			if (!emc->activeRGB[i])
				continue;
			if (!emc->mOverRGB[i])
				continue;
			int o = emc->mOverIndexRGB[i];
			if (o < 0)
				continue;
			emc->getPoints(i).erase(emc->getPoints(i).begin()+o);
			emc->mOverRGB[i] = false;
			emc->mOverIndexRGB[i] = -1;
			emc->createLine(i);
		}
		InvalidateRect(emc->hwnd, NULL, false);
		emc->notifyChanged();
	}
	break;
	case WM_LBUTTONDOWN:
	{
		if (!GetCapture())
			SetCapture(hwnd);
		if (GetCapture() != hwnd)
			break;

		POINT pt, pt1;
		pt.x = (short)LOWORD(lParam);
		pt.y = (short)HIWORD(lParam);
		pt1.x = min(255, max(0, pt.x-emc->margin));
		pt1.y = min(255, max(0, 255-pt.y-emc->margin));

		int ist, ien;
		if (emc->isLuminocityMode)
		{	ist = 3;	ien = 4;	}
		else
		{	ist = 0;	ien = 3;	}

		if ((!emc->isLuminocityMode && (emc->mOverIndexRGB[0] > -1   ||   emc->mOverIndexRGB[1] > -1   ||   emc->mOverIndexRGB[2] > -1))   ||
			(emc->isLuminocityMode  &&  emc->mOverIndexRGB[3] > -1))
		{
			emc->nowDragging = true;
		}
		else
		{
			EMCurve::P2D p;
			p.x = pt1.x;
			p.y = pt1.y;
			for (int i=ist; i<ien; i++)
			{
				if (!emc->activeRGB[i])
					continue;
				if (emc->getPoints(i).size() >= EMCMAXPOINTS)
					continue;
				emc->getPoints(i).push_back(p);
				emc->mOverIndexRGB[i] = (int)emc->getPoints(i).size()-1;
				int already = emc->xPointExists(pt1.x, emc->getPoints(i), emc->mOverIndexRGB[i]);
				emc->mOverRGB[i] = true;
				emc->nowDragging = true;
				emc->createLine(i, already);
				emc->lastIgnoredRGB[i] = already;
			}
			InvalidateRect(emc->hwnd, NULL, false);
			emc->notifyChanged();
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		int ist, ien;
		if (emc->isLuminocityMode)
		{	ist = 3;	ien = 4;	}
		else
		{	ist = 0;	ien = 3;	}

		if (GetCapture() == hwnd)
		{
			for (int i=ist; i<ien; i++)
			{
				if (emc->lastIgnoredRGB[i] > -1)
				{
					emc->getPoints(i).erase(emc->getPoints(i).begin()+emc->lastIgnoredRGB[i]);
					emc->mOverIndexRGB[i] = -1;
					emc->lastIgnoredRGB[i] = -1;

					POINT pt, pt1;
					pt.x = (short)LOWORD(lParam);
					pt.y = (short)HIWORD(lParam);
					pt1.x = min(255, max(0, pt.x-emc->margin));
					pt1.y = min(255, max(0, 255-pt.y-emc->margin));
					emc->mOverIndexRGB[i] = emc->findPointAround(emc->getPoints(i), pt1.x, pt1.y);

					emc->createLine(i);
				}
			}
			InvalidateRect(emc->hwnd, NULL, false);

		}
		emc->nowDragging = false;
		ReleaseCapture();
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}


void EMCurve::reset()
{
	pointsR.clear();
	pointsG.clear();
	pointsB.clear();
	pointsL.clear();
	createLines();
	InvalidateRect(hwnd, NULL, false);
}


int compareP2D(const void * elem1, const void * elem2)
{
	EMCurve::P2D * a = (EMCurve::P2D *)elem1;
	EMCurve::P2D * b = (EMCurve::P2D *)elem2;
	if (a->x == b->x)
		return 0;
	if (a->x > b->x)
		return 1;
	else
		return -1;
}

void TridiagonalSolve(const float *a, const float *b, float *c, float *d, float*x, unsigned int n)
{
    int i;
    c[0] /= b[0];
    d[0] /= b[0];
    for(i = 1; i < (int)n; i++)
	{
		float id = (b[i] - c[i - 1]*a[i]);
		id = 1.0f / id;
		c[i] *= id;
		d[i] = (d[i] - d[i - 1]*a[i])*id;
    }

    x[n - 1] = d[n - 1];
    for(i = n - 2; i >= 0; i--)
         x[i] = d[i] - c[i]*x[i + 1];
}

bool EMCurve::prepareSplineEquation(int whichRGB, const int ignoreIndex)
{
	P2D * sortedPts = getSortedPoints(whichRGB); 
	float *a = getFRGBABCD(whichRGB, 0);
	float *b = getFRGBABCD(whichRGB, 1); 
	float *c = getFRGBABCD(whichRGB, 2);
	float *d = getFRGBABCD(whichRGB, 3);
	vector<P2D> points = getPoints(whichRGB);

	// determine if are there points on left and right borders, if not create them
	bool isFirst = false;
	bool isLast  = false;
	for (unsigned int i=0; i<points.size(); i++)
	{
		if (points[i].x == 0)
			isFirst = true;
		if (points[i].x == 255)
			isLast = true;
	}

	if (!isFirst)
	{
		P2D a;
		a.x = a.y = 0;
		points.push_back(a);
	}

	if (!isLast)
	{
		P2D a;
		a.x = a.y = 255;
		points.push_back(a);
	}

	// copy points
	int num_points = (int)points.size();
	for (int i=0; i<num_points; i++)
		sortedPts[i] = points[i];
	if (ignoreIndex > -1)
		sortedPts[ignoreIndex].x = 1000;

	// sort it
	qsort((void*)sortedPts, num_points, sizeof(P2D), compareP2D);

	if (ignoreIndex > -1)
		num_points--;

	// alloc some arrays
	int n = num_points - 1;
	float * h  = (float*)malloc(sizeof(float)*num_points);
	float * BB = (float*)malloc(sizeof(float)*num_points);
	float * m1 = (float*)malloc(sizeof(float)*num_points);
	float * m2 = (float*)malloc(sizeof(float)*num_points);
	float * m3 = (float*)malloc(sizeof(float)*num_points);

	for (int i=0; i<n; i++)	// n-2?
	{
		h[i] = fabs((float)sortedPts[i+1].x - (float)sortedPts[i].x);
		a[i] = (float)sortedPts[i].y;
	}
	a[n-1] = (float)sortedPts[n-1].y;
	a[n] = (float)sortedPts[n].y;
	h[n] = 0;
	a[num_points] = 0;
	b[num_points] = 0;
	c[num_points] = 0;
	d[num_points] = 0;


	BB[0] = BB[n] = 0;
	m1[0] = m1[n] = 0;
	m2[0] = m2[n] = 1;
	m3[0] = m3[n] = 0;
	for (int i=1; i<n; i++)
	{
		BB[i] = 3.0f/h[i]*(a[i+1]-a[i]) - 3.0f/h[i-1]*(a[i]-a[i-1]);
		m1[i] = h[i-1];
		m2[i] = 2*(h[i-1]+h[i]);
		m3[i] = h[i];
	}

	for (int i=0; i<=n; i++)
		c[i] = 0;
	TridiagonalSolve(m1,m2,m3, BB, c, n);
	for (int i=0; i<n; i++)
		b[i] = (a[i+1]-a[i])/h[i] - h[i]/3.0f*(2*c[i]+c[i+1]);
	for (int i=0; i<n; i++)
		d[i] = (c[i+1]-c[i])/3/h[i];

	float r = (float)sortedPts[num_points-1].x - (float)sortedPts[num_points-2].x;
	a[num_points-1] = (float)sortedPts[num_points-1].y;
	b[num_points-1] = b[num_points-2] + 2*c[num_points-2]*r + 3*d[num_points-2]*r*r;
	c[num_points-1] = 0;
	d[num_points-1] = 0;
	sortedPts[num_points].x = 1<<30;

	free(m1);
	free(m2);
	free(m3);
	free(h);
	free(BB);

	// delete temporary border points
	if (!isLast)
		points.pop_back();
	if (!isFirst)
		points.pop_back();

	return true;
}

void EMCurve::createLine(int whichRGB, int ignoreIndex)
{
	P2D * sortedPts = getSortedPoints(whichRGB); 
	float *a = getFRGBABCD(whichRGB, 0);
	float *b = getFRGBABCD(whichRGB, 1); 
	float *c = getFRGBABCD(whichRGB, 2);
	float *d = getFRGBABCD(whichRGB, 3);
	vector<P2D> points = getPoints(whichRGB);
	POINT * line = getLine(whichRGB);

	prepareSplineEquation(whichRGB, ignoreIndex);

	int i = 0;
	for (int k=0; k<256; k++)
	{
		while (k > sortedPts[i+1].x)
			i++;
		float r = fabs((float)k - (float)sortedPts[i].x);
		float S = a[i] + b[i]*r + c[i]*r*r + d[i]*r*r*r;
		
		line[k].x = margin + k;
		line[k].y = margin + 255 - min( 255 , max( 0 , (int)S ) );
	}

	// one more point on curve
	line[256].x = margin + 256;
	line[256].y = line[255].y;


}

void EMCurve::createLine(vector<P2D> points, POINT * line, int ignoreIndex)
{
	if (points.size() < 4)
	{
		for (int k=0; k<257; k++)
		{
			line[k].x = margin + k;
			line[k].y = margin + 255 - k;
		}
		return;
	}

	bool isFirst = false;
	bool isLast  = false;
	for (unsigned int i=0; i<points.size(); i++)
	{
		if (points[i].x == 0)
			isFirst = true;
		if (points[i].x == 255)
			isLast = true;
	}

	if (!isFirst)
	{
		P2D a;
		a.x = a.y = 0;
		points.push_back(a);
	}

	if (!isLast)
	{
		P2D a;
		a.x = a.y = 255;
		points.push_back(a);
	}


#define CUBIC_SPLINE
#ifdef CUBIC_SPLINE
	// make a copy of points in new buffer
	int num_points = (int)points.size();
	P2D * pts = (P2D*)malloc(sizeof(P2D)*num_points);
	for (int i=0; i<num_points; i++)
		pts[i] = points[i];
	// sort it
	qsort((void*)pts, num_points, sizeof(P2D), compareP2D);

	int n = num_points-1;
	float * h  = (float*)malloc(sizeof(float)*num_points);
	float * a  = (float*)malloc(sizeof(float)*num_points);
	float * b  = (float*)malloc(sizeof(float)*num_points);
	float * c  = (float*)malloc(sizeof(float)*num_points);
	float * d  = (float*)malloc(sizeof(float)*num_points);
	float * BB = (float*)malloc(sizeof(float)*num_points);
	float * m1 = (float*)malloc(sizeof(float)*num_points);
	float * m2 = (float*)malloc(sizeof(float)*num_points);
	float * m3 = (float*)malloc(sizeof(float)*num_points);

	for (int i=0; i<n; i++)
	{
		h[i] = fabs((float)pts[i+1].x - (float)pts[i].x);
		a[i] = (float)pts[i].y;
	}
	a[n-1] = (float)pts[n-1].y;
	a[n] = (float)pts[n].y;
	h[n] = 0;

	BB[0] = 0;
	BB[n] = 0;
	m1[0] = 0;
	m2[0] = 1;
	m3[0] = 0;
	m1[n] = 0;
	m2[n] = 1;
	m3[n] = 0;
	for (int i=1; i<n; i++)
	{
		BB[i] = 3.0f/h[i]*(a[i+1]-a[i]) - 3.0f/h[i-1]*(a[i]-a[i-1]);
		m1[i] = h[i-1];
		m2[i] = 2*(h[i-1]+h[i]);
		m3[i] = h[i];
	}

	for (int i=0; i<=n; i++)
		c[i] = 0;
	TridiagonalSolve(m1,m2,m3, BB, c, n);
	for (int i=0; i<n; i++)
		b[i] = (a[i+1]-a[i])/h[i] - h[i]/3.0f*(2*c[i]+c[i+1]);
	for (int i=0; i<n; i++)
		d[i] = (c[i+1]-c[i])/3/h[i];

	int i = 0;
	for (int k=0; k<256; k++)
	{
		while (k > pts[i+1].x)
			i++;
		float r = fabs((float)k - (float)pts[i].x);
		float S = a[i] + b[i]*r + c[i]*r*r + d[i]*r*r*r;
		
		line[k].x = margin + k;
		line[k].y = margin + 255 - min( 255 , max( 0 , (int)S ) );
	}

	free(m1);
	free(m2);
	free(m3);
	free(a);
	free(b);
	free(c);
	free(d);
	free(h);
	free(BB);
	free(pts);
#else
	for (int k=0; k<256; k++)
	{
		float sum = 0;
		for (int j=0; j<points.size(); j++)	// suma
		{
			if (j == ignoreIndex)
				continue;
			float l = 1;
			for (int i=0; i<points.size(); i++)	// iloczyn
			{
				if (i==j)
					continue;
				if (i==ignoreIndex)
					continue;
				float d = (k - points[i].x) / (float)(points[j].x - points[i].x);
				l *= d;
			}
			sum += l * points[j].y;
		}
		line[k].x = margin + k;
		line[k].y = margin + 255 - min( 255 , max( 0 , (int)sum ) );
	}
#endif
	line[256].x = margin + 256;
	line[256].y = line[255].y;

	if (!isLast)
		points.pop_back();
	if (!isFirst)
		points.pop_back();

}

void EMCurve::createLines()
{
	createLine(0);
	createLine(1);
	createLine(2);
	createLine(3);
}

int EMCurve::xPointExists(int x, vector<P2D> points, int excIndex)
{
	for (unsigned int i=0; i<points.size(); i++)
	{
		if (points[i].x == x  &&  excIndex != i)
			return i;
	}
	return -1;
}

int EMCurve::findPointAround(vector<P2D> points, int x, int y)
{
	int r = 4;
	for (unsigned int i=0; i<points.size(); i++)
	{
		if (points[i].x > x-r  &&  points[i].x < x+r  &&  points[i].y > y-r  &&  points[i].y < y+r)
			return i;
	}
	return -1;
}

vector<EMCurve::P2D> & EMCurve::getPoints(int numRGB)
{
	switch (numRGB)
	{
		case 1:  return pointsG;
		case 2:  return pointsB;
		case 3:  return pointsL;
		default: return pointsR;
	}
}

POINT * EMCurve::getLine(int numRGB)
{
	switch (numRGB)
	{
		case 1:  return lineG;
		case 2:  return lineB;
		case 3:  return lineL;
		default: return lineR;
	}
}

EMCurve::P2D * EMCurve::getSortedPoints(int whichRGB)
{
	switch (whichRGB)
	{
		case 1:  return sortedPtsG;
		case 2:  return sortedPtsB;
		case 3:  return sortedPtsL;
		default: return sortedPtsR;
	}
}

float * EMCurve::getFRGBABCD(int whichRGB, int whichABCD)
{
	switch (whichRGB)
	{
		case 0:
			switch (whichABCD)
			{
				case 0: return fra;
				case 1: return frb;
				case 2: return frc;
				case 3: return frd;
			}
			break;
		case 1:
			switch (whichABCD)
			{
				case 0: return fga;
				case 1: return fgb;
				case 2: return fgc;
				case 3: return fgd;
			}
			break;
		case 2:
			switch (whichABCD)
			{
				case 0: return fba;
				case 1: return fbb;
				case 2: return fbc;
				case 3: return fbd;
			}
			break;
		case 3:
			switch (whichABCD)
			{
				case 0: return fla;
				case 1: return flb;
				case 2: return flc;
				case 3: return fld;
			}
			break;
	}
	return fra;
}

bool EMCurve::hasAnyPoint()
{
	if (isLuminocityMode)
	{
		if (pointsL.size()>0)
			return true;
		else
			return false;
	}

	if (pointsR.size()>0)
		return true;
	if (pointsG.size()>0)
		return true;
	if (pointsB.size()>0)
		return true;

	return false;
}


Color4 EMCurve::processColor(const Color4 &col)
{
	if (isLuminocityMode)
		return processColorLuminosity(col);
	else
		return processColorRGB(col);
}

Color4 EMCurve::processColorLuminosity(const Color4 &col)
{

	Color4 col2 = col;
	float gamma = 2.4f;	// with changes below looks almost like 2.2
	float pergamma = 1.0f/gamma;

	if ( col2.r > 0.04045 ) 
		col2.r = pow( ( col2.r + 0.055f ) / 1.055f , gamma);
	else                   
		col2.r = col2.r / 12.92f;
	if ( col2.g > 0.04045 ) 
		col2.g = pow( ( col2.g + 0.055f ) / 1.055f , gamma);
	else                   
		col2.g = col2.g / 12.92f;
	if ( col2.b > 0.04045 ) 
		col2.b = pow( ( col2.b + 0.055f ) / 1.055f , gamma);
	else                   
		col2.b = col2.b / 12.92f;

	//Observer. = 2�, Illuminant = D65
	float X = col2.r * 0.4124f + col2.g * 0.3576f + col2.b * 0.1805f;
	float Y = col2.r * 0.2126f + col2.g * 0.7152f + col2.b * 0.0722f;
	float Z = col2.r * 0.0193f + col2.g * 0.1192f + col2.b * 0.9505f;
		
	X *= 255;
	Y *= 255;
	Z *= 255;
	float x = X/(X+Y+Z);
	float y = Y/(X+Y+Z);

	int i;
	float r,S;
	
	float L = Y;

	i=0;
	while (L > sortedPtsL[i+1].x)
		i++;
	r = (L - (float)sortedPtsL[i].x);
	S = fla[i] + flb[i]*r + flc[i]*r*r + fld[i]*r*r*r;

	Y = S;

	X = x*(Y/y);
	Z = (1-x-y)*(Y/y);

	X *= 1.0f/255;
	Y *= 1.0f/255;
	Z *= 1.0f/255;

	// back to rgb
	col2.r = X *  3.2406f + Y * -1.5372f + Z * -0.4986f;
	col2.g = X * -0.9689f + Y *  1.8758f + Z *  0.0415f;
	col2.b = X *  0.0557f + Y * -0.2040f + Z *  1.0570f;

	if ( col2.r > 0.0031308f ) 
		col2.r = 1.055f * pow( col2.r , ( pergamma ) ) - 0.055f;
	else                     
		col2.r = 12.92f * col2.r;
	if ( col2.g > 0.0031308f ) 
		col2.g = 1.055f * pow( col2.g , ( pergamma ) ) - 0.055f;
	else                     
		col2.g = 12.92f * col2.g;
	if ( col2.b > 0.0031308f ) 
		col2.b = 1.055f * pow( col2.b , ( pergamma ) ) - 0.055f;
	else                     
		col2.b = 12.92f * col2.b;

	return col2;
}

Color4 EMCurve::processColorRGB(const Color4 &col)
{
	Color4 col1 = col;
	col1.r *= 255;
	col1.g *= 255;
	col1.b *= 255;
	int i;
	float r,S;

	i=0;
	while (col1.r > sortedPtsR[i+1].x)
		i++;
	r = (col1.r - (float)sortedPtsR[i].x);
	S = fra[i] + frb[i]*r + frc[i]*r*r + frd[i]*r*r*r;
	col1.r = S;
		
	i=0;
	while (col1.g > sortedPtsG[i+1].x)
		i++;
	r = (col1.g - (float)sortedPtsG[i].x);
	S = fga[i] + fgb[i]*r + fgc[i]*r*r + fgd[i]*r*r*r;
	col1.g = S;

	i=0;
	while (col1.b > sortedPtsB[i+1].x)
		i++;
	r = (col1.b- (float)sortedPtsB[i].x);
	S = fba[i] + fbb[i]*r + fbc[i]*r*r + fbd[i]*r*r*r;
	col1.b = S;

	col1.r *= 1/255.0f;
	col1.g *= 1/255.0f;
	col1.b *= 1/255.0f;

	return col1;
}

void EMCurve::notifyChanged()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, CU_CHANGED), (LPARAM)hwnd);
}



