#include <windows.h>
#include <stdio.h>
#include "EMControls.h"
#include <commctrl.h>

extern HMODULE hDllModule;

EMImgButton::EMImgButton(HWND hWnd)
{
	colBorder = RGB(0,0,0);
	colBorderMouseOver = RGB(160,160,160);
	colBorderClick = RGB(255,255,255);
	colBorderDisabled = RGB(128,128,128);
	showBorder = true;
	clicked = false;
	mouseIn = false;
	hwnd = hWnd;
	hToolTip = 0;
	pNx = pNy = pMx = pMy = pCx = pCy = pDx = pDy = 0;

	bmNormal = 0;
	bmMouseOver = 0;
	bmClicked = 0;
	bmDisabled = 0;
	bmAll = NULL;
}

EMImgButton::~EMImgButton()
{
	if (hToolTip)
		DestroyWindow(hToolTip);
	hToolTip = 0;
}

void InitEMImgButton()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMImgButton";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMImgButtonProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMImgButton *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}


EMImgButton * GetEMImgButtonInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMImgButton * emib = (EMImgButton *)GetWindowLongPtr(hwnd, 0);
	#else
		EMImgButton * emib = (EMImgButton *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emib;
}

void SetEMImgButtonInstance(HWND hwnd, EMImgButton *emib)
{
	#ifdef _WIN64
	    SetWindowLongPtr(hwnd, 0, (LONG_PTR)emib);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emib);
	#endif
}

LRESULT CALLBACK EMImgButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMImgButton * emib;
	emib = GetEMImgButtonInstance(hwnd);

    switch(msg)
    {
	case WM_CREATE:
		{
			EMImgButton * emib1 = new EMImgButton(hwnd);
			SetEMImgButtonInstance(hwnd, (EMImgButton *)emib1);			
		}
	break;
	case WM_NCDESTROY:
		{
			EMImgButton * emib1;
			emib1 = GetEMImgButtonInstance(hwnd);
			delete emib1;
			SetEMImgButtonInstance(hwnd, 0);
		}
	break;
	case WM_PAINT:
	{
		if (!emib) 
			break;

		RECT rect;
		HDC hdc;
		PAINTSTRUCT ps;
		HANDLE hOldPen;

		hdc = BeginPaint(hwnd, &ps);

		GetClientRect(hwnd, &rect);
		
		POINT border[] = {	{0, rect.bottom-1}, 
							{0,0}, 
							{rect.right-1,0}, 
							{rect.right-1,rect.bottom-1}, 
							{0, rect.bottom-1} };	// 5
		
		int rx, ry;
		rx = rect.right;
		ry = rect.bottom;

		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		if (style & WS_DISABLED)
		{
			if (emib->bmAll)
			{
				HDC bmpdc = CreateCompatibleDC(hdc); 
				SelectObject(bmpdc, *(emib->bmAll)); 
				BitBlt(hdc, 0, 0, rx, ry, bmpdc, emib->pDx, emib->pDy, SRCCOPY);
				DeleteDC(bmpdc); 
			}
			if (emib->showBorder)
			{
				hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emib->colBorderDisabled));
				Polyline(hdc, border, 5);
				DeleteObject(SelectObject(hdc, hOldPen));
			}
		}
		else
		{
			if (emib->mouseIn)
			{
				if (emib->clicked)
				{
					if (emib->bmAll)
					{
						HDC bmpdc = CreateCompatibleDC(hdc); 
						SelectObject(bmpdc, *(emib->bmAll)); 
						BitBlt(hdc, 0, 0, rx, ry, bmpdc, emib->pCx, emib->pCy, SRCCOPY);
						DeleteDC(bmpdc); 
					}
					if (emib->showBorder)
					{
						hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emib->colBorderClick));
						Polyline(hdc, border, 5);
						DeleteObject(SelectObject(hdc, hOldPen));
					}
				}
				else
				{
					if (emib->bmAll)
					{
						HDC bmpdc = CreateCompatibleDC(hdc); 
						SelectObject(bmpdc, *(emib->bmAll)); 
						BitBlt(hdc, 0, 0, rx, ry, bmpdc, emib->pMx, emib->pMy, SRCCOPY);
						DeleteDC(bmpdc); 
					}
					if (emib->showBorder)
					{
						hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emib->colBorderMouseOver));
						Polyline(hdc, border, 5);
						DeleteObject(SelectObject(hdc, hOldPen));
					}
				}
			}
			else
			{
				if (emib->bmAll)
				{
					HDC bmpdc = CreateCompatibleDC(hdc); 
					SelectObject(bmpdc, *(emib->bmAll)); 
					BitBlt(hdc, 0, 0, rx, ry, bmpdc, emib->pNx, emib->pNy, SRCCOPY);
					DeleteDC(bmpdc); 
				}
				if (emib->showBorder)
				{
					hOldPen = SelectObject(hdc, CreatePen(PS_SOLID, 1, emib->colBorder));
					Polyline(hdc, border, 5);
					DeleteObject(SelectObject(hdc, hOldPen));
				}
			}
		}

		EndPaint(hwnd, &ps);
	}
	break;
	case WM_MOUSEMOVE:
	{
		if (!GetCapture())
			SetCapture(hwnd);
		if (GetCapture() == hwnd)
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (PtInRect(&rect, pt))
			{
				if (emib->mouseIn == false)
				{
					emib->mouseIn = true;
					InvalidateRect(hwnd, &rect, true);
				}
				emib->mouseIn = true;
			}
			else
			{
				emib->mouseIn = false;
				if (!emib->clicked)
					ReleaseCapture();
				InvalidateRect(hwnd, &rect, true);
			}
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		if (GetCapture() == hwnd)
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (PtInRect(&rect, pt))
			{
				emib->clicked = true;
				InvalidateRect(hwnd, &rect, true);
			}
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		RECT rect;
		GetClientRect(hwnd, &rect);
		POINT pt = { LOWORD(lParam), HIWORD(lParam) };
		if (PtInRect(&rect, pt) && emib->clicked)
		{
			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), 
					(LPARAM)hwnd);
		}
		if (!PtInRect(&rect, pt))
		{
			ReleaseCapture();
		}
		emib->clicked = false;
		emib->mouseIn = false;
		InvalidateRect(hwnd, &rect, true);
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMImgButton::setToolTip(char * text)
{
	if (!hToolTip)
	{
		hToolTip = CreateWindowEx (WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,     
				    CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwnd, NULL, hDllModule, NULL);
	}

	SetWindowPos (hToolTip, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	TOOLINFO ti;
	ti.cbSize = sizeof (TOOLINFO);
	ti.uFlags = TTF_SUBCLASS | TTF_IDISHWND;
	ti.hwnd = hToolTip;
	ti.hinst = NULL;
	ti.uId = (UINT_PTR) hwnd;
	ti.lpszText = (LPSTR) text;

	RECT rect;
	GetClientRect (hwnd, &rect);

	ti.rect.left = rect.left;    
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
	ti.rect.bottom = rect.bottom;

	SendMessage (hToolTip, TTM_ADDTOOL, 0, (LPARAM)&ti);
}

