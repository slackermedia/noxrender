#include "raytracer.h"
#include "log.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h.>

unsigned long long par = 0;
unsigned long long nonpar = 0;

float BSPTree::naiveNode::intervalSAH = 0.02f;
int BSPTree::naiveNode::intervalNumSAH = 50;
int BSPTree::maxDivs;
int BSPTree::maxTris;
int BSPTree::counterTris;
int BSPTree::counterNodes;
int BSPTree::counter5;
int BSPTree::naiveNode::maxLevel = 0;
unsigned long long BSPTree::naiveNode::nextAddr;
unsigned long long BSPTree::naiveNode::rOffset = sizeof(BSPTree::BSPNode);
unsigned long long BSPTree::kdtree_buf_size = 0;

clock_t bspticks = 0;
//#define BSP_BENCHMARK

unsigned int upTo4(unsigned int num)
{
	if (num%4)
		num += 4-num%4;
	return num;
}

BSPTree::BSPTree()
{
	nRoot = new naiveNode();
	nRoot->intervalNumSAH = 16;
	nRoot->intervalSAH = 1.0f / nRoot->intervalNumSAH;
	maxDivs = 32;
	maxTris = 16;
	name = NULL;
}

BSPTree::~BSPTree()
{
	if (name)
		free(name);
	name = NULL;
}

BSPTree::naiveNode::naiveNode()
{
	left  = NULL;
	right = NULL;
	bspOwner = NULL;
	split = 0.0f;
	tris = NULL;
	level = 0;
	offset = 0;
	tOffset = 0;
	dimension = 0;
	isLeaf = true;
	amILeft = true;
	minX = minY = minZ = BIGFLOAT;
	maxX = maxY = maxZ = -BIGFLOAT;
}

BSPTree::naiveNode::~naiveNode()
{
	if (tris)
	{
		tris->freeList();
	}
	delete tris;
	delete left;
	delete right;
}

BSPTree::naiveNode::naiveNode(const naiveNode & node)
{
	left  = NULL;
	right = NULL;
	tris = NULL;
	split = 0.5f;
	level = node.level;
	offset = 0;
	tOffset = 0;
	dimension = 0;
	isLeaf = true;
	amILeft = true;
	minX = node.minX;
	minY = node.minY;
	minZ = node.minZ;
	maxX = node.maxX;
	maxY = node.maxY;
	maxZ = node.maxZ;
	bspOwner = node.bspOwner;
}

void BSPTree::naiveNode::copyFrom(const naiveNode & node)
{
	left  = NULL;
	right = NULL;
	tris = NULL;
	split = 0.5f;
	level = node.level;
	offset = 0;
	tOffset = 0;
	dimension = 0;
	isLeaf = true;
	amILeft = true;
	minX = node.minX;
	minY = node.minY;
	minZ = node.minZ;
	maxX = node.maxX;
	maxY = node.maxY;
	maxZ = node.maxZ;
}

void BSPTree::addTrisMesh(Mesh * mesh)
{
	Scene * scene = Raytracer::getInstance()->curScenePtr;
	nRoot->tris = new bList<int, 32>(0);
	for (int i=mesh->firstTri; i<=mesh->lastTri; i++)
		addTriangle(i);
	nRoot->maxX += 0.01f * (nRoot->maxX - nRoot->minX) + 0.0001f;
	nRoot->maxY += 0.01f * (nRoot->maxY - nRoot->minY) + 0.0001f;
	nRoot->maxZ += 0.01f * (nRoot->maxZ - nRoot->minZ) + 0.0001f;
	nRoot->minX -= 0.01f * (nRoot->maxX - nRoot->minX) + 0.0001f;
	nRoot->minY -= 0.01f * (nRoot->maxY - nRoot->minY) + 0.0001f;
	nRoot->minZ -= 0.01f * (nRoot->maxZ - nRoot->minZ) + 0.0001f;
}

void BSPTree::addTrisRoot()
{
	Scene * scene = Raytracer::getInstance()->curScenePtr;
	nRoot->tris = new bList<int, 32>(0);
	for (int i=0; i<scene->triangles.objCount; i++)
		addTriangle(i);
	nRoot->maxX += 0.01f * (nRoot->maxX - nRoot->minX) + 0.0001f;
	nRoot->maxY += 0.01f * (nRoot->maxY - nRoot->minY) + 0.0001f;
	nRoot->maxZ += 0.01f * (nRoot->maxZ - nRoot->minZ) + 0.0001f;
	nRoot->minX -= 0.01f * (nRoot->maxX - nRoot->minX) + 0.0001f;
	nRoot->minY -= 0.01f * (nRoot->maxY - nRoot->minY) + 0.0001f;
	nRoot->minZ -= 0.01f * (nRoot->maxZ - nRoot->minZ) + 0.0001f;
}

void BSPTree::addTriangle(int index)
{
	nRoot->tris->add(index);
	Triangle * tri = &(Raytracer::getInstance()->curScenePtr->triangles[index]);
	float minX = min(min(tri->V1.x, tri->V2.x), tri->V3.x);
	float minY = min(min(tri->V1.y, tri->V2.y), tri->V3.y);
	float minZ = min(min(tri->V1.z, tri->V2.z), tri->V3.z);
	float maxX = max(max(tri->V1.x, tri->V2.x), tri->V3.x);
	float maxY = max(max(tri->V1.y, tri->V2.y), tri->V3.y);
	float maxZ = max(max(tri->V1.z, tri->V2.z), tri->V3.z);
	nRoot->maxX = max(maxX, nRoot->maxX);
	nRoot->maxY = max(maxY, nRoot->maxY);
	nRoot->maxZ = max(maxZ, nRoot->maxZ);
	nRoot->minX = min(minX, nRoot->minX);
	nRoot->minY = min(minY, nRoot->minY);
	nRoot->minZ = min(minZ, nRoot->minZ);
	
}

float BSPTree::naiveNode::areaSAH()
{
	float x,y,z;
	x = maxX - minX;
	y = maxY - minY;
	z = maxZ - minZ;
	return 2*(x*y + x*z + y*z);
}

bool BSPTree::naiveNode::triInside(Triangle * tri)
{
	if (tri->V1.x < minX   &&   tri->V2.x < minX   &&   tri->V3.x < minX)	return false;
	if (tri->V1.y < minY   &&   tri->V2.y < minY   &&   tri->V3.y < minY)	return false;
	if (tri->V1.z < minZ   &&   tri->V2.z < minZ   &&   tri->V3.z < minZ)	return false;
	if (tri->V1.x > maxX   &&   tri->V2.x > maxX   &&   tri->V3.x > maxX)	return false;
	if (tri->V1.y > maxY   &&   tri->V2.y > maxY   &&   tri->V3.y > maxY)	return false;
	if (tri->V1.z > maxZ   &&   tri->V2.z > maxZ   &&   tri->V3.z > maxZ)	return false;
	return true;
}


void BSPTree::naiveNode::divideSAH()
{
	tris->createArray();

	BSPTree::counterNodes++;

	if (level>maxDivs-2)
	{
		char buf[256];
		sprintf_s(buf, 256, "level %d   tris: %d", level, tris->objCount);
		Logger::add(buf);
	}

	if (tris->objCount <= BSPTree::maxTris    ||    level >= BSPTree::maxDivs)
	{
		int count = upTo4(tris->objCount);
		BSPTree::counterTris += count;
		isLeaf = true;
		return;
	}

	isLeaf = false;

	Scene * scene = Raytracer::getInstance()->curScenePtr;

	int bestDim = -1;
	float bestSpl = 0.5f;
	float bestcost = BIGFLOAT;
	float pArea = 1.0f / areaSAH() ;
	float noDivCost = (float)tris->objCount;

	Triangle * tri = (Triangle*)malloc(sizeof Triangle);
	int * numAL = (int *)malloc(sizeof(int)*3*intervalNumSAH);
	int * numAR = (int *)malloc(sizeof(int)*3*intervalNumSAH);
	float * borders = (float*)malloc(sizeof(float)*intervalNumSAH*3);

	// initialize arrays
	for (int dim=0; dim<3; dim++)
		for (int i=0; i<intervalNumSAH; i++)
		{
			int a = 3*i+dim;
			numAL[a] = 0;
			numAR[a] = 0;

			float spl = intervalSAH*(i+0.5f);
			switch(dim)
			{
				case 0:	borders[i*3+dim] =  spl*maxX+(1-spl)*minX;   break;
				case 1:	borders[i*3+dim] =  spl*maxY+(1-spl)*minY;   break;
				case 2:	borders[i*3+dim] =  spl*maxZ+(1-spl)*minZ;   break;
			}
		}

	
	for (int j=0; j<tris->objCount; j++)
	{
		scene->triangles.copyTo((*tris)[j], tri);		// get tri
		for (int dim=0; dim<3; dim++)
			for (int i=0; i<intervalNumSAH; i++)
			{
				int a = 3*i+dim;
				float splitBorder = borders[a];

				if (tri->V1.V[dim]<=splitBorder || tri->V2.V[dim]<=splitBorder || tri->V3.V[dim]<=splitBorder)
					numAL[a]++;
				if (tri->V1.V[dim]>=splitBorder || tri->V2.V[dim]>=splitBorder || tri->V3.V[dim]>=splitBorder)
					numAR[a]++;
			}
	}		


	for (int dim=0; dim<3; dim++)
		for (int i=0; i<intervalNumSAH; i++)
		{
			int a = 3*i+dim;
			float areaL = 0;
			float areaR = 0;
			float splitBorder = borders[a];

			float bx,by,bz;
			bx = maxX - minX;
			by = maxY - minY;
			bz = maxZ - minZ;

			switch (dim)
			{
				case 0: 
				{
					float blx = splitBorder - minX;
					float brx = maxX - splitBorder;
					areaL = 2*(blx*by + blx*bz + by*bz);
					areaR = 2*(brx*by + brx*bz + by*bz);
				}
				break;
				case 1: 
				{
					float bly = splitBorder - minY;
					float bry = maxY - splitBorder;
					areaL = 2*(bx*bly + bx*bz + bly*bz);
					areaR = 2*(bx*bry + bx*bz + bry*bz);
				}
				break;
				case 2: 
				{
					float blz = splitBorder - minZ;
					float brz = maxZ - splitBorder;
					areaL = 2*(bx*by + bx*blz + by*blz);
					areaR = 2*(bx*by + bx*brz + by*brz);
				}
				break;
			}

			float cost  = (areaL*numAL[a] + areaR*numAR[a]) * pArea + 5;
			if (cost < bestcost)
			{
				float spl = intervalSAH*(i+0.5f);
				bestcost = cost;
				bestSpl = spl;
				bestDim = dim;
			}
		}

	free(borders);
	free(tri);
	free(numAL);
	free(numAR);

	int maxnow = (1<<(level/3));
	if ((bestcost > noDivCost * 0.95f  &&   tris->objCount<=maxnow)   ||    tris->objCount<=16)
	{	// stop here
		int count = upTo4(tris->objCount);
		counterTris += count;
		isLeaf = true;
		return;
	}

	left  = new naiveNode(*this);
	right = new naiveNode(*this);
	switch(bestDim)
	{
		case 0: split = bestSpl*maxX+(1-bestSpl)*minX;	 left->maxX = split;   right->minX = split;       break;
		case 1:	split = bestSpl*maxY+(1-bestSpl)*minY;   left->maxY = split;   right->minY = split;       break;
		case 2:	split = bestSpl*maxZ+(1-bestSpl)*minZ;   left->maxZ = split;   right->minZ = split;       break;
	}
	left->level++;
	left->amILeft = true;
	right->level++;
	right->amILeft = false;
	left->tris = new bList<int, 32>(0);
	right->tris = new bList<int, 32>(0);
	left->bspOwner = bspOwner;
	right->bspOwner = bspOwner;

	dimension = bestDim;
	maxLevel = max(maxLevel, left->level);

	for (int i=0; i<tris->objCount; i++)
	{
		int t = (*tris)[i];
		Triangle tri = scene->triangles[t];

		if (tri.V1.V[dimension]<=split || tri.V2.V[dimension]<=split || tri.V3.V[dimension]<=split)
			left->tris->add(t);
		if (tri.V1.V[dimension]>=split || tri.V2.V[dimension]>=split || tri.V3.V[dimension]>=split)
			right->tris->add(t);
	}

	delete tris;
	tris = 0;

	if (level == 6)
	{
		BSPTree::counter5++;
		char * nname = NULL;
		if (bspOwner)
		{
			if (bspOwner->name)
				nname = bspOwner->name;
			else
				nname = "scene";
		}
		else
			nname = "scene";
		char buf[512];
		sprintf_s(buf, 512, "Creating kd-tree for %s", nname);
		scene->notifyProgress(buf, BSPTree::counter5*100.0f/64.0f);
	}

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return;
	}
	
	left->divideSAH();
	right->divideSAH();
}

void BSPTree::naiveNode::createOffsets(int lvl)
{
	if (lvl > level)
	{
		if (!isLeaf)
		{
			left->createOffsets(lvl);
			right->createOffsets(lvl);
		}
		return;
	}

	if (isLeaf)
	{
		if (amILeft)
		{
			offset = nextAddr;
			tOffset = offset + 2*sizeof(BSPNode);
			rOffset = offset + sizeof(BSPNode);
			nextAddr += 2*sizeof(BSPNode) + upTo4(tris->objCount)*sizeof(Triangle4)/4;
		}
		else
		{
			offset = rOffset;
			tOffset = nextAddr;
			nextAddr += upTo4(tris->objCount)*sizeof(Triangle4)/4;
		}
	}
	else
	{
		if (amILeft)
		{
			offset = nextAddr;
			rOffset = offset + sizeof(BSPNode);
			nextAddr += 2*sizeof(BSPNode);
		}
		else
		{
			offset = rOffset;
		}
	}
}

void BSPTree::naiveNode::makeTris4(void * addr)
{
	if (!isLeaf)
		return;

	Scene * scene = Raytracer::getInstance()->curScenePtr;

	int tt = tris->objCount;
	if (tt < 1)
		return;
	int num4 = upTo4(tt)/4;
	int rr = num4*4 - tt;
	for (int i=0; i<rr; i++)
		tris->add((*tris)[tt-1]);
	tris->createArray();

	char * taddr = (char*)addr + tOffset;
	int i1,i2,i3,i4;
	for (int i=0; i<num4; i++)
	{
		i1 = (*tris)[i*4+0];
		i2 = (*tris)[i*4+1];
		i3 = (*tris)[i*4+2];
		i4 = (*tris)[i*4+3];
		*((Triangle4 *)taddr) = Triangle4(scene->triangles[i1],scene->triangles[i2],scene->triangles[i3],scene->triangles[i4], i1,i2,i3,i4);
		taddr += sizeof(Triangle4);
	}
}

void BSPTree::naiveNode::addToBSPBuffer(void * addr)
{
	if (isLeaf)
	{
		BSPTree::BSPLeaf leafNode;
		leafNode.numTris = upTo4(tris->objCount)/4;

		unsigned long long diffOffsetLong = tOffset - offset;
		if (diffOffsetLong > 8589934591)		// 33 bits
		{
			char errMsg[512];
			sprintf_s(errMsg, 512, "Warning!\nKD-Tree offset is too large. Geometry will probably be bugged.\nLeaf\n%llx = %llx - %llx\nat level %d", 
									diffOffsetLong, tOffset, offset, level);
			Logger::add(errMsg);
			MessageBox(0, errMsg, "Warning", 0);
		}
		unsigned int diffOffset = (unsigned int)(diffOffsetLong>>1);

		leafNode.offset = diffOffset | 0x3;

		*((BSPTree::BSPLeaf*)((char*)addr+offset)) = leafNode;

		makeTris4(addr);
	}
	else
	{
		BSPTree::BSPSplit splitNode;
		splitNode.split = split;

		unsigned long long diffOffsetLong = left->offset - offset;
		if (diffOffsetLong > 8589934591)		// 33 bits
		{
			char errMsg[512];
			sprintf_s(errMsg, 512, "Warning!\nKD-Tree offset is too large. Geometry will probably be bugged.\nBranch\n%llx = %llx - %llx\nat level %d", 
									diffOffsetLong, left->offset, offset, level);
			Logger::add(errMsg);
			MessageBox(0, errMsg, "Warning", 0);
		}
		unsigned int diffOffset = (unsigned int)(diffOffsetLong>>1);
		splitNode.offset = (diffOffset&0xFFFFFFFC) | (dimension&0x3); 

		*((BSPTree::BSPSplit*)((char*)addr+offset)) = splitNode;

		left->addToBSPBuffer(addr);
		right->addToBSPBuffer(addr);
	}
}


void BSPTree::makeBSP()
{
	Scene * scene = Raytracer::getInstance()->curScenePtr;

	counterNodes = 1;	// first node will be empty for cache align
	counterTris = 0;

	nRoot->amILeft = false;
	nRoot->isLeaf = false;
	int num_tris_bsp = nRoot->tris->objCount;

	counter5 = 0;
	nRoot->bspOwner = this;
	nRoot->divideSAH();

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return;
	}

	naiveNode::rOffset = sizeof(BSPNode);
	naiveNode::nextAddr = 2*sizeof(BSPNode);
	naiveNode::maxLevel = maxDivs;
	nRoot->offset = sizeof(BSPNode);
	
	for (int i=0; i<=naiveNode::maxLevel; i++)
		nRoot->createOffsets(i);

	size_t bsize = sizeof(Triangle4)*counterTris/4 + sizeof(BSPNode)*counterNodes;
	root = (BSPNode *)_aligned_malloc(bsize, 16);
	if (!root)
	{
		char buf[128];
		sprintf_s(buf, 128, "System did not let me allocate %lld bytes! Kd-tree not created!", bsize);
		MessageBox(0, buf, "Grrr", 0);
		Logger::add(buf);
		return;
	}

	char buf[512];
	sprintf_s(buf, 512, "BSP: Out of %d tris created %d tris in %d nodes. %lld bytes allocated. (nextAddr=%lld), nnode size=%d", 
			num_tris_bsp, counterTris, counterNodes, bsize,  naiveNode::nextAddr, sizeof(naiveNode));
	Logger::add(buf);

	kdtree_buf_size = bsize;

	nRoot->addToBSPBuffer(root);

	maxx = nRoot->maxX;
	maxy = nRoot->maxY;
	maxz = nRoot->maxZ;
	minx = nRoot->minX;
	miny = nRoot->minY;
	minz = nRoot->minZ;

	delete nRoot;
	nRoot = 0;
}

//-----------------------------------------------
//               TRAVERSE

inline void TransposeSSE(__m128 &A, __m128 &B, __m128 &C, __m128 &D);

float BSPTree::intersectTris(const Point3d &origin, const Vector3d &direction, const Vector3d &invdirection, int &tri, float &u, float &v, float maxDist, size_t offset, unsigned int num4)
{
	int size4 = sizeof(Triangle4);
	Triangle4 * tri4;
	Ray4 ray4(origin.x, origin.y, origin.z, direction.x, direction.y, direction.z);
	Float4 mdist,U,V, inum, dist;
	#ifdef OWN_SSE
		mdist.sse = _mm_set1_ps(maxDist);
		U.sse = _mm_setzero_ps();
		V.sse = _mm_setzero_ps();
	#else
		mdist.v1 = mdist.v2 = mdist.v3 = mdist.v4 = maxDist;
		U.v1 = U.v2 = U.v3 = U.v4 = 0;
		V.v1 = V.v2 = V.v3 = V.v4 = 0;
	#endif
	inum.s_int[0] = inum.s_int[1] = inum.s_int[2] = inum.s_int[3] = -1;
	
	for (unsigned int i=0; i<num4; i++)
	{
		tri4 = (Triangle4*)((char*)root+offset+i*size4);
		_mm_prefetch((char *)(tri4+1), _MM_HINT_T0);
		_mm_prefetch((char *)(tri4+1)+64, _MM_HINT_T0);
		_mm_prefetch((char *)(tri4+1)+128, _MM_HINT_T0);
		_mm_prefetch((char *)(tri4+1)+192, _MM_HINT_T1);
		_mm_prefetch((char *)(tri4+1)+256, _MM_HINT_T1);
		_mm_prefetch((char *)(tri4+1)+320, _MM_HINT_T1);
		tri4->intersection(ray4, dist, U, V, mdist, inum);
	}

	int cl = -1;
	if (mdist.v1 >= 0.0f)
		cl = 0;
	if (mdist.v2 >= 0.0f   &&    (mdist.v2 < mdist.V[cl]  ||  cl<0) )
		cl = 1;
	if (mdist.v3 >= 0.0f   &&    (mdist.v3 < mdist.V[cl]  ||  cl<0) )
		cl = 2;
	if (mdist.v4 >= 0.0f   &&    (mdist.v4 < mdist.V[cl]  ||  cl<0) )
		cl = 3;
	if (cl > -1)
	{
		u = U.V[cl];
		v = V.V[cl];
		tri = inum.s_int[cl];
		return mdist.V[cl];
	}

	return maxDist;
}

//---------------------------------------

bool BSPTree::intersection(const Point3d &origin, const Vector3d &direction, const Vector3d &invdir, const float &maxDist, float &near1, float &far1)
{
	float nearx, neary, nearz;
	float farx, fary, farz;
	float tfar, tnear, t1, t2;

	if ((fabs(direction.x) < SFLOAT)     &&     ((origin.x < minx) || (origin.x > maxx)))		return false;
	if ((fabs(direction.y) < SFLOAT)     &&     ((origin.y < miny) || (origin.y > maxy)))		return false;
	if ((fabs(direction.z) < SFLOAT)     &&     ((origin.z < minz) || (origin.z > maxz)))		return false;

	t1 = (minx - origin.x) * invdir.x;
	t2 = (maxx - origin.x) * invdir.x;
	nearx = min(t1,t2);
	farx  = max(t1,t2);

	t1 = (miny - origin.y) * invdir.y;
	t2 = (maxy - origin.y) * invdir.y;
	neary = min(t1,t2);
	fary  = max(t1,t2);

	t1 = (minz - origin.z) * invdir.z;
	t2 = (maxz - origin.z) * invdir.z;
	nearz = min(t1,t2);
	farz  = max(t1,t2);

	tfar  = min(min(maxDist, farx) , min(fary, farz));
	tnear = max(max(0, nearx) , max(neary, nearz));

	near1 = tnear;
	far1 = tfar;

	return (tnear <= tfar);
}

float BSPTree::intersectForTris(const Point3d &origin, const Vector3d &direction, float &maxDist, int &tri, float &u, float &v)
{
	sfStack<BSPTree::nodeStack, 40> stack;

	Vector3d invDir;
	invDir.x = 1.0f/direction.x;
	invDir.y = 1.0f/direction.y;
	invDir.z = 1.0f/direction.z;

	float near1, far1;
	if (!root  ||  !intersection(origin, direction, invDir, maxDist, near1, far1))
	{
		return -1;
	}

	int backSide[3]; 
	int frontSide[3];
	for (int i=0; i<3; i++)
	{
		if (direction.V[i] == 0.0f)
		{
			if ((int&)direction.V[i]==0x80000000)
			{
				backSide[i]  = 0;
				frontSide[i] = sizeof(BSPNode);
			}
			else
			{
				backSide[i]  = sizeof(BSPNode);
				frontSide[i] = 0;
			}
		}
		else
		{
			if (direction.V[i] >= 0.0f)
			{
				backSide[i]  = sizeof(BSPNode);
				frontSide[i] = 0;
			}
			else
			{
				backSide[i]  = 0;
				frontSide[i] = sizeof(BSPNode);
			}
		}
	}

	BSPNode * curNode;
	unsigned long long curOffset, tempOffset;
	curNode = &root[1];
	curOffset = sizeof(BSPNode);
	nodeStack nstack;
	float d;
	int dim;

	while (1)
	{
		while (!BSP_ISLEAF(curNode->splitNode))
		{
			_mm_prefetch((char *)curNode + BSP_OFFSET(curNode->splitNode), _MM_HINT_T0);
			dim = BSP_DIMENSION(curNode->splitNode);
			d = ( curNode->splitNode.split - origin.V[dim] ) * invDir.V[dim] ;
			if (d <= near1)		// from inside -> near = 0
			{
				curOffset += BSP_OFFSET(curNode->splitNode) +  backSide[dim];
			}
			else if (d >= far1)
				{
					curOffset += BSP_OFFSET(curNode->splitNode) +  frontSide[dim];
				}
				else
				{
					tempOffset = curOffset + BSP_OFFSET(curNode->splitNode) +  backSide[dim];
					nstack.tnear = d;
					nstack.tfar = far1;
					nstack.offset = tempOffset;
					stack.push(nstack);
					curOffset += BSP_OFFSET(curNode->splitNode) +  frontSide[dim];
					far1 = d;
				}
			curNode = (BSPNode*)((char*)root+curOffset);
		}

		// so that one is a leaf
		int ttri;
		float tu, tv, dist=1000000000000000000.0f;
		if (curNode->leafNode.numTris > 0)
		{
			unsigned long long offs = curOffset + BSP_OFFSET(curNode->leafNode);
			int numtris = curNode->leafNode.numTris;
				dist = intersectTris(origin, direction, invDir, ttri, tu, tv, far1, (size_t)offs, numtris);
		}

		bool stosOK = (stack.pop(nstack));
		_mm_prefetch((char*)root+nstack.offset, _MM_HINT_T0);
		if (dist < far1)
		{
			stack.freeStack();
			u = tu;
			v = tv;
			tri = ttri;
			return dist;
		}

		if (!stosOK)
		{
			return -1.0f;
		}

		// get back data from stack
		curOffset = nstack.offset;
		curNode = (BSPNode*)((char*)root+curOffset);
		near1 = nstack.tnear;
		far1 = nstack.tfar;
	}

	return -1;
}

void BSPTree::deleteAllStuff()
{
	if (root)
		_aligned_free(root);
	root = NULL;

	if (name)
		free(name);
	name = NULL;
}

