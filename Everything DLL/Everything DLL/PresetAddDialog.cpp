#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "EM2Controls.h"
#include "noxfonts.h"

extern HMODULE hDllModule;

HBITMAP generateBackgroundPattern(int w, int h, int shx, int shy, COLORREF colLight, COLORREF colDark);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
bool initPresetAddControlPositions(HWND hWnd);
bool updatePresetAddControlBackgrounds(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK PresetNewDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = 450 + wrect.right-wrect.left-crect.right;
				tmprect.bottom = 82 + wrect.bottom-wrect.top-crect.bottom;

				RECT rect;
				GetClientRect(hWnd, &rect);
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(rect.right, rect.bottom, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();

				EM2Text * emt = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NEWPRESET_TEXT_NAME));
				emt->bgImage = hBgBMP;
				emt->setFont(fonts->em2text, false);

				EM2EditSimple * emes = GetEM2EditSimpleInstance(GetDlgItem(hWnd, IDC_NEWPRESET_EDIT_NAME));
				emes->bgImage = hBgBMP;
				emes->setFontEdit(fonts->em2editsimpleedit, false);
				emes->setFontMain(fonts->em2editsimpletext, false);
				emes->margin = 0;

				if (lParam)
				{
					emes->setText((char*)lParam);
					SetWindowText(hWnd, "Rename preset");
				}

				EM2Button * embok= GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NEWPRESET_OK));
				embok->bgImage = hBgBMP;
				embok->setFont(fonts->em2button, false);

				EM2Button * embcancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NEWPRESET_CANCEL));
				embcancel->bgImage = hBgBMP;
				embcancel->setFont(fonts->em2button, false);

				initPresetAddControlPositions(hWnd);
				updatePresetAddControlBackgrounds(hWnd);

				SetFocus(emes->hwnd);

			}	// end WM_INITDIALOG
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;

			}
			break;
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_NEWPRESET_OK:
						{
							if (wmEvent == BN_CLICKED)
							{
								EM2EditSimple * emesname = GetEM2EditSimpleInstance(GetDlgItem(hWnd, IDC_NEWPRESET_EDIT_NAME));
								char * res = emesname->getText();
								if (!res || strlen(res)<1)
									res = copyString("Unnamed");
								EndDialog(hWnd, (INT_PTR)(res)); 
							}
						}
						break;
					case IDC_NEWPRESET_CANCEL:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)(0)); 
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool initPresetAddControlPositions(HWND hWnd)
{
	return true;
}

//------------------------------------------------------------------------------------------------

bool updatePresetAddControlBackgrounds(HWND hWnd)
{
	EM2Text * emttext = GetEM2TextInstance(GetDlgItem(hWnd, IDC_NEWPRESET_TEXT_NAME));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NEWPRESET_TEXT_NAME), 0, 0, emttext->bgShiftX, emttext->bgShiftY);

	EM2EditSimple * emesname = GetEM2EditSimpleInstance(GetDlgItem(hWnd, IDC_NEWPRESET_EDIT_NAME));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NEWPRESET_EDIT_NAME), 0, 0, emesname->bgShiftX, emesname->bgShiftY);

	EM2Button * embok = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NEWPRESET_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NEWPRESET_OK), 0, 0, embok->bgShiftX, embok->bgShiftY);

	EM2Button * embcancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_NEWPRESET_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_NEWPRESET_CANCEL), 0, 0, embcancel->bgShiftX, embcancel->bgShiftY);

	return true;
}

//------------------------------------------------------------------------------------------------

char * runPresetAddDialog(HWND hWnd, char * name)
{
	char * res = (char *)DialogBoxParam(hDllModule,
							MAKEINTRESOURCE(IDD_NEW_PRESET_DIALOG), hWnd, PresetNewDlgProc,(LPARAM)( name ));
	return res;
}

//------------------------------------------------------------------------------------------------
