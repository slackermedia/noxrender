#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "raytracer.h"
#include "valuesMinMax.h"

void setPositionsOnBottomPanel(HWND hWnd, int width, bool regionMode);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcBottomPanel(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static COLORREF bgCol = RGB(0,0,0);
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(bgCol);

				RECT rect;
				GetClientRect(hWnd, &rect);

				setPositionsOnBottomPanel(hWnd, rect.right, false);

				EM2CheckBox * emcAutorefresh = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_AUTOREFRESH));
				emcAutorefresh->colBackground = bgCol;
				emcAutorefresh->setFont(rMain->fonts->em2checkbox, false);
				emcAutorefresh->selected = rMain->timers.refreshOn;

				EM2EditSpin * emesRefrTime = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_REFRESH_TIME));
				emesRefrTime->setFont(rMain->fonts->em2editspin, false);
				emesRefrTime->colBackground = bgCol;
				emesRefrTime->setValuesToInt(rMain->timers.refreshSec, NOX_REND_AUTOREFRESH_TIME_MIN, NOX_REND_AUTOREFRESH_TIME_MAX, NOX_REND_AUTOREFRESH_TIME_DEF, 1, 0.2f);

				EM2Text * emtSeconds = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_SECONDS));
				emtSeconds->setFont(rMain->fonts->em2text, false);
				emtSeconds->colBackGnd = bgCol;

				EM2Line * emtLine1 = GetEM2LineInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE1));
				emtLine1->colBackground = bgCol;
				EM2Line * emtLine3 = GetEM2LineInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE3));
				emtLine3->colBackground = bgCol;
				EM2Line * emtLine4 = GetEM2LineInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE4));
				emtLine4->colBackground = bgCol;

				EM2Button * embRefresh = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_REFRESH_NOW));
				embRefresh->colBackGnd = bgCol;
				embRefresh->setFont(rMain->fonts->em2button, false);
				embRefresh->alphaBg = 0;
				embRefresh->drawBorder = false;

				EM2Text * emtRefreshing = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_REFRESHING));
				emtRefreshing->setFont(rMain->fonts->em2text, false);
				emtRefreshing->colBackGnd = bgCol;
				emtRefreshing->colText = RGB(255,0,0);
				emtRefreshing->align = EM2Text::ALIGN_CENTER;

				EM2Text * emtSamples1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_SAMPLES_SEC));
				emtSamples1->setFont(rMain->fonts->em2text, false);
				emtSamples1->colBackGnd = bgCol;
				emtSamples1->align = EM2Text::ALIGN_RIGHT;

				EM2Text * emtSamples2 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_SAMPLES));
				emtSamples2->setFont(rMain->fonts->em2text, false);
				emtSamples2->colBackGnd = bgCol;
				emtSamples2->align = EM2Text::ALIGN_RIGHT;

				EM2Text * emtRendTime = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_RENDER_TIME));
				emtRendTime->setFont(rMain->fonts->em2text, false);
				emtRendTime->colBackGnd = bgCol;

				EM2Text * emtRegionMode = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_REGION_MODE));
				emtRegionMode->setFont(rMain->fonts->em2text, false);
				emtRegionMode->colBackGnd = bgCol;
				emtRegionMode->colText = RGB(255,0,0);
				emtRegionMode->align = EM2Text::ALIGN_CENTER;

			}
			break;
		case WM_SIZE:
			{
				bool regions = false;
				EMPView * empv = GetEMPViewInstance(rMain->hViewport);
				if (empv->mode == EMPView::MODE_REGION_RECT)
					regions = true;
				if (empv->mode == EMPView::MODE_REGION_AIRBRUSH)
					regions = true;
				if (!rMain->regionsPanelOpened)
					regions = false;
				setPositionsOnBottomPanel(hWnd, LOWORD(lParam), regions);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);
				RECT irect = { 0, 0, rect.right-rect.left, rect.bottom-rect.top };

				FillRect(bhdc, &irect, bgBrush);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_BOTTOM_AUTOREFRESH:
					{
						CHECK(wmEvent==BN_CLICKED);
						EM2CheckBox * emcARefr = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_AUTOREFRESH));
						rMain->timers.refreshOn = emcARefr->selected;
					}
					break;
					case IDC_REND2_BOTTOM_REFRESH_TIME:
					{
						CHECK(wmEvent==WM_VSCROLL);
						EM2EditSpin * emesRefrTime = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_REFRESH_TIME));
						rMain->timers.refreshSec = emesRefrTime->intValue;

						EM2CheckBox * emcARefr = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_BOTTOM_AUTOREFRESH));
						rMain->timers.refreshOn = emcARefr->selected = true;
						InvalidateRect(emcARefr->hwnd, NULL, false);
					}
					break;
					case IDC_REND2_BOTTOM_REFRESH_NOW:
					{
						CHECK(wmEvent==BN_CLICKED);
						rMain->refreshRenderOnThread();
					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelBottom()
{
	// nothing here in current version
}

//------------------------------------------------------------------------------------------------

void setPositionsOnBottomPanel(HWND hWnd, int width, bool regionMode)
{
	if (width<0)
	{
		RECT rect;
		GetClientRect(hWnd, &rect);
		width = rect.right;
	}

	int x = 0, y = 0;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_AUTOREFRESH),		HWND_TOP,	x+9,	y+9,			110,		10,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_REFRESH_TIME),		HWND_TOP,	x+120,	y+4,			56,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_SECONDS),		HWND_TOP,	x+182,	y+6,			56,		16,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE1),				HWND_TOP,	x+240,	y+4,			5,		19,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_REFRESH_NOW),		HWND_TOP,	x+245,	y+3,			90,		22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_REFRESHING),	HWND_TOP,	x+245,	y+6,			90,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE4),				HWND_TOP,	x+335,	y+4,			5,		19,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_REGION_MODE),	HWND_TOP,	x+340,	y+3,			80,		22,			0);

	int rtw = 125;
	int s1w = 110;
	int s2w = 32;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_SAMPLES_SEC),	HWND_TOP,	width-rtw-s2w-s1w-20,	y+6,		s1w,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_SAMPLES),			HWND_TOP,	width-rtw-s2w-15,		y+6,		s2w,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE3),				HWND_TOP,	width-rtw-10,			y+4,		5,		19,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_BOTTOM_RENDER_TIME),		HWND_TOP,	width-rtw,				y+6,		rtw,	16,			0);

	int minwARefr = 240;
	int minwRefr = 335;
	int minwClear = 0;
	int minwSamples = 350 +rtw+s1w+s2w+20;
	int minwTime = 350+rtw;
	int minwRegion = 350;//515;

	int showARefr = width > minwARefr ? SW_SHOW : SW_HIDE;
	int showRefr = width > minwRefr ? SW_SHOW : SW_HIDE;
	int showClear = width > minwClear ? SW_SHOW : SW_HIDE;
	int showSamples = width > minwSamples ? SW_SHOW : SW_HIDE;
	int showTime = width > minwTime ? SW_SHOW : SW_HIDE;
	int showRegion = width > minwRegion && regionMode ? SW_SHOW : SW_HIDE;

	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_AUTOREFRESH), showARefr);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_REFRESH_TIME), showARefr);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_SECONDS), showARefr);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE1), showRefr);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_REFRESH_NOW), showRefr);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_SAMPLES_SEC), showSamples);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_SAMPLES), showSamples);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE3), showSamples);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_RENDER_TIME), showTime);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_LINE4), showRegion);
	ShowWindow(GetDlgItem(hWnd, IDC_REND2_BOTTOM_TEXT_REGION_MODE), showRegion);
}

//------------------------------------------------------------------------------------------------

void RendererMain2::showRegionModeInStatus(bool on)
{
	HWND hText = GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_TEXT_REGION_MODE);
	SetWindowText(hText, "REGION MODE");
	setPositionsOnBottomPanel(hBottomPanel, -1, on);
}

//------------------------------------------------------------------------------------------------

void RendererMain2::showPickModeInStatus(bool on)
{
	HWND hText = GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_TEXT_REGION_MODE);
	SetWindowText(hText, "PICK MODE");
	setPositionsOnBottomPanel(hBottomPanel, -1, on);
}

//------------------------------------------------------------------------------------------------

void RendererMain2::showRefreshingInStatus(bool show)
{
	HWND hRefreshNow = GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_REFRESH_NOW);
	HWND hRefreshing = GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_TEXT_REFRESHING);
	RECT rect;
	GetClientRect(hBottomPanel, &rect);
	int minwRefr = 335;
	bool canbeshown = rect.right>minwRefr;
	int showRenderNow = (canbeshown && !show) ? SW_SHOW : SW_HIDE;
	int showRendering = (canbeshown &&  show) ? SW_SHOW : SW_HIDE;
	ShowWindow(hRefreshNow, showRenderNow);
	ShowWindow(hRefreshing, showRendering);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillStatusBar()
{
	EM2CheckBox * emcAutorefresh = GetEM2CheckBoxInstance(GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_AUTOREFRESH));
	EM2EditSpin * emesRefrTime = GetEM2EditSpinInstance(GetDlgItem(hBottomPanel, IDC_REND2_BOTTOM_REFRESH_TIME));
	emcAutorefresh->selected = timers.refreshOn;
	emesRefrTime->setIntValue(timers.refreshSec);
	InvalidateRect(emcAutorefresh->hwnd, NULL, FALSE);
	return true;
}

//------------------------------------------------------------------------------------------------
