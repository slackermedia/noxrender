#include "RendererMain2.h"
#include <stdio.h>
#include "log.h"

extern char  * defaultDirectory;
NOXFontManager * NOXFontManager::oInstance = NULL;

//------------------------------------------------------------------------------------------------

NOXFontManager * NOXFontManager::getInstance()
{
	if (!oInstance)
		oInstance = new NOXFontManager();
	return oInstance;
}

//------------------------------------------------------------------------------------------------

NOXFontManager::NOXFontManager()
{
	opensans8 = 0;
	opensans9 = 0;
	opensans10 = 0;
	opensans11 = 0;
	opensans12 = 0;

	em2text = 0;
	em2editspin = 0;
	em2edittrack = 0;
	em2editsimpletext = 0;
	em2editsimpleedit = 0;
	em2paneltitle = 0;
	em2checkbox = 0;
	em2groupbar = 0;
	em2button = 0;
	em2combomain = 0;
	em2combounwrap = 0;
	em2tabs = 0;
	em2listentry = 0;
	em2listheader = 0;
	em2topbuttons = 0;
	em2matprops = 0;

	loadAllFonts();
}

//------------------------------------------------------------------------------------------------

NOXFontManager::~NOXFontManager()
{
	deleteAllFonts();
}

//------------------------------------------------------------------------------------------------

HFONT NOXFontManager::loadFont(char * name, int size)
{
	LOGFONT lf;
	memset(&lf, 0, sizeof(lf));
	lf.lfHeight = -MulDiv(size, 96, 72);
	lf.lfWeight = FW_NORMAL;
	lf.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	sprintf_s(lf.lfFaceName, LF_FACESIZE, name);

	HFONT result = CreateFontIndirect(&lf);
	return result;
}

//------------------------------------------------------------------------------------------------

bool NOXFontManager::loadAllFonts()
{
	char filename[2048];
	sprintf_s(filename, 2048, "%s\\settings\\OpenSans-Light.ttf", defaultDirectory);
	int nResults = AddFontResourceEx(filename, FR_PRIVATE, NULL);

	HFONT standardFont = loadFont("Arial", 8);

	opensans8  = loadFont("Open Sans Light", 8);
	opensans9  = loadFont("Open Sans Light", 9);
	opensans10 = loadFont("Open Sans Light", 10);
	opensans11 = loadFont("Open Sans Light", 11);
	opensans12 = loadFont("Open Sans Light", 12);

	em2text = standardFont;
	em2editspin = standardFont;
	em2edittrack = standardFont;
	em2editsimpletext = standardFont;
	em2editsimpleedit = standardFont;
	em2paneltitle = opensans11;
	em2checkbox = standardFont;
	em2groupbar = standardFont;
	em2button = standardFont;
	em2combomain = standardFont;
	em2combounwrap = standardFont;
	em2tabs = standardFont;
	em2listentry = standardFont;
	em2listheader = standardFont;
	em2topbuttons = standardFont;
	em2matprops = loadFont("Arial", 7);

	return true;
}

//------------------------------------------------------------------------------------------------

bool NOXFontManager::deleteAllFonts()
{
	DeleteObject(opensans8);
	DeleteObject(opensans9);
	DeleteObject(opensans10);
	DeleteObject(opensans11);
	DeleteObject(opensans12);

	DeleteObject(em2matprops);

	return true;
}

//------------------------------------------------------------------------------------------------

int CALLBACK EnumFontFamProc1(ENUMLOGFONT *lpelf, NEWTEXTMETRIC *lpntm, DWORD FontType, LPARAM lParam)
{
	Logger::add((char *)lpelf->elfFullName);
	return 1;
}

//------------------------------------------------------------------------------------------------

bool NOXFontManager::logFonts(HWND hWnd)
{
	Logger::add("Enumerating fonts...");
	LOGFONT lf;
	lf.lfCharSet = DEFAULT_CHARSET;
	lf.lfFaceName[0] = 0;
	lf.lfPitchAndFamily = 0;
	HDC hdc = GetDC(hWnd);
	EnumFontFamiliesEx(hdc, &lf, (FONTENUMPROC)&EnumFontFamProc1, 0, 0);
	ReleaseDC(hWnd, hdc);
	return true;
}

//------------------------------------------------------------------------------------------------
