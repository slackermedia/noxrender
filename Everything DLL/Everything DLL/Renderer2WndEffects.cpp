#include "RendererMain2.h"
#include "EM2Controls.h"
#include "resource.h"
#include "valuesMinMax.h"
#include "raytracer.h"
#include "dialogs.h"

extern HMODULE hDllModule;

void setPositionsOnStartEffectsTab(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcEffects(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));
				setPositionsOnStartEffectsTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = RGB(0xcf, 0xcf, 0xcf); 
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2Button * embRedraw = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_EFF_REDRAW));
				embRedraw->bgImage = rMain->hBmpBg;
				embRedraw->setFont(rMain->fonts->em2button, false);

				EM2GroupBar * emgr_bloom = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GR_BLOOM));
				emgr_bloom->bgImage = rMain->hBmpBg;
				emgr_bloom->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_glare = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GR_GLARE));
				emgr_glare->bgImage = rMain->hBmpBg;
				emgr_glare->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_fog = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GR_FOG));
				emgr_fog->bgImage = rMain->hBmpBg;
				emgr_fog->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_aerial = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GR_AERIAL));
				emgr_aerial->bgImage = rMain->hBmpBg;
				emgr_aerial->setFont(rMain->fonts->em2groupbar, false);
				EM2GroupBar * emgr_grain = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GR_GRAIN));
				emgr_grain->bgImage = rMain->hBmpBg;
				emgr_grain->setFont(rMain->fonts->em2groupbar, false);

				EM2CheckBox * emcBloomOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ON));
				emcBloomOn->bgImage = rMain->hBmpBg;
				emcBloomOn->setFont(rMain->fonts->em2checkbox, false);
				EM2EditTrackBar * emetBloomPower = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_POWER));
				emetBloomPower->setFont(rMain->fonts->em2edittrack, false);
				emetBloomPower->bgImage = rMain->hBmpBg;
				emetBloomPower->setEditBoxWidth(48, 5);
				emetBloomPower->setValuesToFloat(NOX_FINAL_BLOOM_POWER_DEF, NOX_FINAL_BLOOM_POWER_MIN, NOX_FINAL_BLOOM_POWER_MAX, NOX_FINAL_BLOOM_POWER_DEF, 0.5f);
				EM2Text * emtArea1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_AREA_BLOOM));
				emtArea1->bgImage = rMain->hBmpBg;
				emtArea1->setFont(rMain->fonts->em2text, false);
				EM2Text * emtThres1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_THRESHOLD_BLOOM));
				emtThres1->bgImage = rMain->hBmpBg;
				emtThres1->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emesBloomArea = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_AREA));
				emesBloomArea->setFont(rMain->fonts->em2editspin, false);
				emesBloomArea->bgImage = rMain->hBmpBg;
				emesBloomArea->setValuesToInt(NOX_FINAL_BLOOM_AREA_DEF, NOX_FINAL_BLOOM_AREA_MIN, NOX_FINAL_BLOOM_AREA_MAX, NOX_FINAL_BLOOM_AREA_DEF, 32, 0.5f);
				EM2EditSpin * emesBloomThres = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_THRESHOLD));
				emesBloomThres->setFont(rMain->fonts->em2editspin, false);
				emesBloomThres->bgImage = rMain->hBmpBg;
				emesBloomThres->setValuesToFloat(NOX_FINAL_BLOOM_THRES_DEF, NOX_FINAL_BLOOM_THRES_MIN, NOX_FINAL_BLOOM_THRES_MAX, NOX_FINAL_BLOOM_THRES_DEF, 0.5f, 0.01f);
				EM2CheckBox * emcBloomAttOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ATT_ON));
				emcBloomAttOn->bgImage = rMain->hBmpBg;
				emcBloomAttOn->setFont(rMain->fonts->em2checkbox, false);
				EM2ColorShow * emcBloomAttCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ATT_COLOR));
				emcBloomAttCol->bgImage = rMain->hBmpBg;
				emcBloomAttCol->setAllowedDragDrop();

				EM2CheckBox * emcGlareOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_ON));
				emcGlareOn->bgImage = rMain->hBmpBg;
				emcGlareOn->setFont(rMain->fonts->em2checkbox, false);
				EM2EditTrackBar * emetGlarePower = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_POWER));
				emetGlarePower->setFont(rMain->fonts->em2edittrack, false);
				emetGlarePower->bgImage = rMain->hBmpBg;
				emetGlarePower->setEditBoxWidth(48, 5);
				emetGlarePower->setValuesToFloat(NOX_FINAL_GLARE_POWER_DEF, NOX_FINAL_GLARE_POWER_MIN, NOX_FINAL_GLARE_POWER_MAX, NOX_FINAL_GLARE_POWER_DEF, 0.5f);
				EM2Text * emtArea2 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_AREA_GLARE));
				emtArea2->bgImage = rMain->hBmpBg;
				emtArea2->setFont(rMain->fonts->em2text, false);
				EM2Text * emtThres2 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_THRESHOLD_GLARE));
				emtThres2->bgImage = rMain->hBmpBg;
				emtThres2->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emesGlareArea = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_AREA));
				emesGlareArea->setFont(rMain->fonts->em2editspin, false);
				emesGlareArea->bgImage = rMain->hBmpBg;
				emesGlareArea->setValuesToInt(NOX_FINAL_GLARE_AREA_DEF, NOX_FINAL_GLARE_AREA_MIN, NOX_FINAL_GLARE_AREA_MAX, NOX_FINAL_GLARE_AREA_DEF, 32, 0.5f);
				EM2EditSpin * emesGlareThres = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_THRESHOLD));
				emesGlareThres->setFont(rMain->fonts->em2editspin, false);
				emesGlareThres->bgImage = rMain->hBmpBg;
				emesGlareThres->setValuesToFloat(NOX_FINAL_GLARE_THRES_DEF, NOX_FINAL_GLARE_THRES_MIN, NOX_FINAL_GLARE_THRES_MAX, NOX_FINAL_GLARE_THRES_DEF, 0.5f, 0.01f);
				EM2Text * emtDispersion = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_DISPERSION));
				emtDispersion->bgImage = rMain->hBmpBg;
				emtDispersion->setFont(rMain->fonts->em2text, false);
				EM2Text * emtApShape = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_APERTURE_SHAPE));
				emtApShape->bgImage = rMain->hBmpBg;
				emtApShape->setFont(rMain->fonts->em2text, false);
				EM2Text * emtApObstacle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_APERTURE_OBSTACLE));
				emtApObstacle->bgImage = rMain->hBmpBg;
				emtApObstacle->setFont(rMain->fonts->em2text, false);
				EM2EditTrackBar * emetDisp = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_DISPERSION));
				emetDisp->setFont(rMain->fonts->em2edittrack, false);
				emetDisp->bgImage = rMain->hBmpBg;
				emetDisp->setEditBoxWidth(48, 5);
				emetDisp->setValuesToFloat(NOX_FINAL_GLARE_DISP_DEF, NOX_FINAL_GLARE_DISP_MIN, NOX_FINAL_GLARE_DISP_MAX, NOX_FINAL_GLARE_DISP_DEF, 0.1f);
				EM2CheckBox * emcTxOnApShape = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_SHAPE));
				emcTxOnApShape->bgImage = rMain->hBmpBg;emcTxOnApShape->setFont(rMain->fonts->em2checkbox, false);
				EM2CheckBox * emcTxOnApObst = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_OBSTACLE));
				emcTxOnApObst->bgImage = rMain->hBmpBg;
				emcTxOnApObst->setFont(rMain->fonts->em2checkbox, false);
				EM2Button * embTxApShape = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TX_APERT_SHAPE));
				embTxApShape->bgImage = rMain->hBmpBg;
				embTxApShape->setFont(rMain->fonts->em2button, false);
				EM2Button * embTxApObst = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TX_APERT_OBSTACLE));
				embTxApObst->bgImage = rMain->hBmpBg;
				embTxApObst->setFont(rMain->fonts->em2button, false);

				EM2CheckBox * emcFogOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_ON));
				emcFogOn->bgImage = rMain->hBmpBg;
				emcFogOn->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emtColor1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_COLOR_FOG));
				emtColor1->bgImage = rMain->hBmpBg;
				emtColor1->setFont(rMain->fonts->em2text, false);
				EM2ColorShow * emcFogCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_COLOR));
				emcFogCol->bgImage = rMain->hBmpBg;
				emcFogCol->setAllowedDragDrop();
				EM2CheckBox * emcFogExclBg = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_EXCL_BG));
				emcFogExclBg->bgImage = rMain->hBmpBg;
				emcFogExclBg->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emtDensity1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_DENSITY));
				emtDensity1->bgImage = rMain->hBmpBg;
				emtDensity1->setFont(rMain->fonts->em2text, false);
				EM2Text * emtDistance1 = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_DISTANCE_FOG));
				emtDistance1->bgImage = rMain->hBmpBg;
				emtDistance1->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emesFogDens = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DENSITY));
				emesFogDens->setFont(rMain->fonts->em2editspin, false);
				emesFogDens->bgImage = rMain->hBmpBg;
				emesFogDens->setValuesToFloat(NOX_FINAL_FOG_SPEED_DEF, NOX_FINAL_FOG_SPEED_MIN, NOX_FINAL_FOG_SPEED_MAX, NOX_FINAL_FOG_SPEED_DEF, 0.1f, 0.01f);
				EM2EditSpin * emesFogDist = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DIST));
				emesFogDist->setFont(rMain->fonts->em2editspin, false);
				emesFogDist->bgImage = rMain->hBmpBg;
				emesFogDist->setValuesToFloat(NOX_FINAL_FOG_DIST_DEF, NOX_FINAL_FOG_DIST_MIN, NOX_FINAL_FOG_DIST_MAX, NOX_FINAL_FOG_DIST_DEF, 10.0f, 0.1f);

				EM2CheckBox * emcAerialOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_ON));
				emcAerialOn->bgImage = rMain->hBmpBg;
				emcAerialOn->setFont(rMain->fonts->em2checkbox, false);
				EM2Text * emtAScale = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_SCALE_AERIAL));
				emtAScale->bgImage = rMain->hBmpBg;
				emtAScale->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emesAerialScale = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_SCALE));
				emesAerialScale->setFont(rMain->fonts->em2editspin, false);
				emesAerialScale->bgImage = rMain->hBmpBg;
				emesAerialScale->setValuesToFloat(NOX_FINAL_AERIAL_SCALE_DEF, NOX_FINAL_AERIAL_SCALE_MIN, NOX_FINAL_AERIAL_SCALE_MAX, NOX_FINAL_AERIAL_SCALE_DEF, 0.1f, 0.01f);

				EM2Text * emtGrain = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_GRAIN));
				emtGrain->bgImage = rMain->hBmpBg;
				emtGrain->setFont(rMain->fonts->em2text, false);
				EM2EditTrackBar * emetGrain = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GRAIN));
				emetGrain->setFont(rMain->fonts->em2edittrack, false);
				emetGrain->bgImage = rMain->hBmpBg;
				emetGrain->setEditBoxWidth(48, 5);
				emetGrain->setValuesToFloat(NOX_FINAL_GRAIN_DEF, NOX_FINAL_GRAIN_MIN, NOX_FINAL_GRAIN_MAX, NOX_FINAL_GRAIN_DEF, 0.1f);

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_EFF_REDRAW:
						{
							rMain->runRedrawFinalThread();
						}
						break;
					case IDC_REND2_EFF_BLOOM_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcBloomOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ON));
							if (cam)
								cam->fMod.bloomEnabled = emcBloomOn->selected;
							rMain->updateLocksTabEffects(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_EFF_BLOOM_POWER:
						{
							CHECK(wmEvent==WM_HSCROLL);
							EM2EditTrackBar * emetBloomPower = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_POWER));
							if (cam)
								cam->fMod.bloomPower = emetBloomPower->floatValue;
						}
						break;
					case IDC_REND2_EFF_BLOOM_AREA:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesBloomArea = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_AREA));
							if (cam)
								cam->fMod.bloomArea = emesBloomArea->intValue;
						}
						break;
					case IDC_REND2_EFF_BLOOM_THRESHOLD:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesBloomThres = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_THRESHOLD));
							if (cam)
								cam->fMod.bloomThreshold = emesBloomThres->floatValue;
						}
						break;
					case IDC_REND2_EFF_BLOOM_ATT_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcBloomAttOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ATT_ON));
							if (cam)
								cam->fMod.bloomAttenuationOn = emcBloomAttOn->selected;
							rMain->updateLocksTabEffects(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_EFF_BLOOM_ATT_COLOR:
						{
							EM2ColorShow * emcBloomAttCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ATT_COLOR));
							if (wmEvent==BN_CLICKED)
							{
								if (emcBloomAttCol)
								{
									Color4 col = emcBloomAttCol->color;
									Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));

									emcBloomAttCol->isMouseOver = false;
									emcBloomAttCol->isClicked = false;
									InvalidateRect(emcBloomAttCol->hwnd, NULL, false);
									if (res)
									{
										if (cam)
											cam->fMod.bloomAttenuationColor = *res;
										emcBloomAttCol->redraw(*res);
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								if (cam)
									cam->fMod.bloomAttenuationColor = emcBloomAttCol->color;
							}

						}
						break;
					case IDC_REND2_EFF_GLARE_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcGlareOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_ON));
							if (cam)
								cam->fMod.glareEnabled = emcGlareOn->selected;
							rMain->updateLocksTabEffects(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_EFF_GLARE_POWER:
						{
							CHECK(wmEvent==WM_HSCROLL);
							EM2EditTrackBar * emetGlarePower = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_POWER));
							if (cam)
								cam->fMod.glarePower = emetGlarePower->floatValue;
						}
						break;
					case IDC_REND2_EFF_GLARE_AREA:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesGlareArea = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_AREA));
							if (cam)
							{
								cam->fMod.glareArea = emesGlareArea->intValue;
								cam->glareOK = false;
							}
						}
						break;
					case IDC_REND2_EFF_GLARE_THRESHOLD:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesGlareThres = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_THRESHOLD));
							if (cam)
								cam->fMod.glareThreshold = emesGlareThres->floatValue;
						}
						break;
					case IDC_REND2_EFF_GLARE_DISPERSION:
						{
							CHECK(wmEvent==WM_HSCROLL);
							EM2EditTrackBar * emetDisp = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_DISPERSION));
							if (cam)
							{
								cam->fMod.glareDispersion = emetDisp->floatValue;
								cam->glareOK = false;
							}
}
						break;
					case IDC_REND2_EFF_TXON_APERT_SHAPE:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcTxOnApShape = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_SHAPE));
							if (cam)
							{
								cam->fMod.glareTextureApertureOn = emcTxOnApShape->selected;
								cam->glareOK = false;
							}
						}
						break;
					case IDC_REND2_EFF_TXON_APERT_OBSTACLE:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcTxOnApObst = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_OBSTACLE));
							if (cam)
							{
								cam->fMod.glareTextureObstacleOn = emcTxOnApObst->selected;
								cam->glareOK = false;
							}
						}
						break;
					case IDC_REND2_EFF_TX_APERT_SHAPE:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							EM2Button * embTxApShape = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TX_APERT_SHAPE));
							Texture *t = new Texture();
							if (cam->texDiaphragm.isValid())
								t->copyFromOther(&cam->texDiaphragm);
							Texture *tex = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE2_DIALOG), hWnd, Tex2DlgProc, (LPARAM)(t));
							if (tex)
							{
								bool texOK = tex->isValid()  &&  tex->filename!=NULL;
								cam->texDiaphragm.copyFromOther(tex);
								tex->freeAllBuffers();
								delete tex;
								cam->glareOK = false;
								EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_SHAPE));
								emc->selected = texOK;
								InvalidateRect(emc->hwnd, NULL, false);
								cam->fMod.glareTextureApertureOn = texOK;
								embTxApShape->changeCaption(texOK ? cam->texDiaphragm.filename : "NO MAP LOADED");
							}
						}
						break;
					case IDC_REND2_EFF_TX_APERT_OBSTACLE:
						{
							CHECK(wmEvent==BN_CLICKED);
							CHECK(cam);
							EM2Button * embTxApObst = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TX_APERT_OBSTACLE));
							Texture *t = new Texture();
							if (cam->texObstacle.isValid())
								t->copyFromOther(&cam->texObstacle);
							Texture *tex = (Texture *)DialogBoxParam(hDllModule, 
													MAKEINTRESOURCE(IDD_TEXTURE2_DIALOG), hWnd, Tex2DlgProc, (LPARAM)(t));
							if (tex)
							{
								bool texOK = tex->isValid()  &&  tex->filename!=NULL;
								cam->texObstacle.copyFromOther(tex);
								tex->freeAllBuffers();
								delete tex;
								cam->glareOK = false;
								EM2CheckBox * emc = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_OBSTACLE));
								emc->selected = texOK;
								InvalidateRect(emc->hwnd, NULL, false);
								cam->fMod.glareTextureObstacleOn = texOK;
								embTxApObst->changeCaption(texOK ? cam->texObstacle.filename : "NO MAP LOADED");
							}

						}
						break;
					case IDC_REND2_EFF_FOG_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcFogOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_ON));
							if (cam)
								cam->fMod.fogEnabled = emcFogOn->selected;
							rMain->updateLocksTabEffects(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_EFF_FOG_COLOR:
						{
							EM2ColorShow * emcFogCol = GetEM2ColorShowInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_COLOR));
							if (wmEvent==BN_CLICKED)
							{
								if (emcFogCol)
								{
									Color4 col = emcFogCol->color;
									Color4 * res = (Color4 *)DialogBoxParam(hDllModule,
												MAKEINTRESOURCE(IDD_COLOR_DIALOG2), hWnd, ColorDlg2Proc,(LPARAM)( &col ));

									emcFogCol->isMouseOver = false;
									emcFogCol->isClicked = false;
									InvalidateRect(emcFogCol->hwnd, NULL, false);
									if (res)
									{
										if (cam)
											cam->fMod.fogColor = *res;
										emcFogCol->redraw(*res);
									}
								}
							}
							if (wmEvent==CS_IGOT_DRAG)
							{
								if (cam)
									cam->fMod.fogColor = emcFogCol->color;
							}
						}
						break;
					case IDC_REND2_EFF_FOG_EXCL_BG:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcFogExclBg = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_EXCL_BG));
							if (cam)
								cam->fMod.fogExclSky = emcFogExclBg->selected;
						}
						break;
					case IDC_REND2_EFF_FOG_DENSITY:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesFogDens = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DENSITY));
							if (cam)
								cam->fMod.fogSpeed = emesFogDens->floatValue;
						}
						break;
					case IDC_REND2_EFF_FOG_DIST:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesFogDist = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DIST));
							if (cam)
								cam->fMod.fogDist = emesFogDist->floatValue;
						}
						break;
					case IDC_REND2_EFF_AERIAL_PERSP_ON:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcAerialOn = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_ON));
							if (cam)
								cam->fMod.aerialEnabled = emcAerialOn->selected;
							rMain->updateLocksTabEffects(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_EFF_AERIAL_PERSP_SCALE:
						{
							CHECK(wmEvent==WM_VSCROLL);
							EM2EditSpin * emesAerialScale = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_SCALE));
							if (cam)
								cam->fMod.aerialScale = emesAerialScale->floatValue;
						}
						break;
					case IDC_REND2_EFF_GRAIN:
						{
							CHECK(wmEvent==WM_HSCROLL);
							EM2EditTrackBar * emetGrain = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_EFF_GRAIN));
							if (cam)
								cam->fMod.grain = emetGrain->floatValue;
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelEffects()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TITLE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2Button * embRedraw = GetEM2ButtonInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_REDRAW));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_REDRAW), bgShiftXRightPanel, 0, embRedraw->bgShiftX, embRedraw->bgShiftY);

	EM2GroupBar * emgrBloom = GetEM2GroupBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_BLOOM));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_BLOOM), bgShiftXRightPanel, 0, emgrBloom->bgShiftX, emgrBloom->bgShiftY);
	EM2GroupBar * emgrGlare = GetEM2GroupBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_GLARE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_GLARE), bgShiftXRightPanel, 0, emgrGlare->bgShiftX, emgrGlare->bgShiftY);
	EM2GroupBar * emgrFog = GetEM2GroupBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_FOG));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_FOG), bgShiftXRightPanel, 0, emgrFog->bgShiftX, emgrFog->bgShiftY);
	EM2GroupBar * emgrAerial = GetEM2GroupBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_AERIAL));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_AERIAL), bgShiftXRightPanel, 0, emgrAerial->bgShiftX, emgrAerial->bgShiftY);
	EM2GroupBar * emgrGrain = GetEM2GroupBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_GRAIN));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GR_GRAIN), bgShiftXRightPanel, 0, emgrGrain->bgShiftX, emgrGrain->bgShiftY);

	EM2CheckBox * emcBloomOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ON));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ON), bgShiftXRightPanel, 0, emcBloomOn->bgShiftX, emcBloomOn->bgShiftY);


	EM2EditTrackBar * emetBloomPower = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_POWER));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_POWER), bgShiftXRightPanel, 0, emetBloomPower->bgShiftX, emetBloomPower->bgShiftY);
	EM2Text * emtArea1 = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_AREA_BLOOM));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_AREA_BLOOM), bgShiftXRightPanel, 0, emtArea1->bgShiftX, emtArea1->bgShiftY);
	EM2Text * emtThres1 = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_THRESHOLD_BLOOM));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_THRESHOLD_BLOOM), bgShiftXRightPanel, 0, emtThres1->bgShiftX, emtThres1->bgShiftY);
	EM2EditSpin * emesBloomArea = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_AREA));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_AREA), bgShiftXRightPanel, 0, emesBloomArea->bgShiftX, emesBloomArea->bgShiftY);
	EM2EditSpin * emesBloomThres = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_THRESHOLD));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_THRESHOLD), bgShiftXRightPanel, 0, emesBloomThres->bgShiftX, emesBloomThres->bgShiftY);
	EM2CheckBox * emcBloomAttOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ATT_ON));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ATT_ON), bgShiftXRightPanel, 0, emcBloomAttOn->bgShiftX, emcBloomAttOn->bgShiftY);
	EM2ColorShow * emcBloomAttCol = GetEM2ColorShowInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ATT_COLOR));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ATT_COLOR), bgShiftXRightPanel, 0, emcBloomAttCol->bgShiftX, emcBloomAttCol->bgShiftY);

	EM2CheckBox * emcGlareOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_ON));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_ON), bgShiftXRightPanel, 0, emcGlareOn->bgShiftX, emcGlareOn->bgShiftY);
	EM2EditTrackBar * emetGlarePower = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_POWER));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_POWER), bgShiftXRightPanel, 0, emetGlarePower->bgShiftX, emetGlarePower->bgShiftY);
	EM2Text * emtArea2 = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_AREA_GLARE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_AREA_GLARE), bgShiftXRightPanel, 0, emtArea2->bgShiftX, emtArea2->bgShiftY);
	EM2Text * emtThres2 = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_THRESHOLD_GLARE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_THRESHOLD_GLARE), bgShiftXRightPanel, 0, emtThres2->bgShiftX, emtThres2->bgShiftY);
	EM2EditSpin * emesGlareArea = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_AREA));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_AREA), bgShiftXRightPanel, 0, emesGlareArea->bgShiftX, emesGlareArea->bgShiftY);
	EM2EditSpin * emesGlareThres = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_THRESHOLD));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_THRESHOLD), bgShiftXRightPanel, 0, emesGlareThres->bgShiftX, emesGlareThres->bgShiftY);
	EM2Text * emtDispersion = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_DISPERSION));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_DISPERSION), bgShiftXRightPanel, 0, emtDispersion->bgShiftX, emtDispersion->bgShiftY);
	EM2Text * emtApShape = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_APERTURE_SHAPE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_APERTURE_SHAPE), bgShiftXRightPanel, 0, emtApShape->bgShiftX, emtApShape->bgShiftY);
	EM2Text * emtApObstacle = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_APERTURE_OBSTACLE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_APERTURE_OBSTACLE), bgShiftXRightPanel, 0, emtApObstacle->bgShiftX, emtApObstacle->bgShiftY);
	EM2EditTrackBar * emetDisp = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_DISPERSION));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_DISPERSION), bgShiftXRightPanel, 0, emetDisp->bgShiftX, emetDisp->bgShiftY);
	EM2CheckBox * emcTxOnApShape = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TXON_APERT_SHAPE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TXON_APERT_SHAPE), bgShiftXRightPanel, 0, emcTxOnApShape->bgShiftX, emcTxOnApShape->bgShiftY);
	EM2CheckBox * emcTxOnApObst = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TXON_APERT_OBSTACLE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TXON_APERT_OBSTACLE), bgShiftXRightPanel, 0, emcTxOnApObst->bgShiftX, emcTxOnApObst->bgShiftY);
	EM2Button * embTxApShape = GetEM2ButtonInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TX_APERT_SHAPE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TX_APERT_SHAPE), bgShiftXRightPanel, 0, embTxApShape->bgShiftX, embTxApShape->bgShiftY);
	EM2Button * embTxApObst = GetEM2ButtonInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TX_APERT_OBSTACLE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TX_APERT_OBSTACLE), bgShiftXRightPanel, 0, embTxApObst->bgShiftX, embTxApObst->bgShiftY);

	EM2CheckBox * emcFogOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_ON));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_ON), bgShiftXRightPanel, 0, emcFogOn->bgShiftX, emcFogOn->bgShiftY);
	EM2Text * emtColor1 = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_COLOR_FOG));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_COLOR_FOG), bgShiftXRightPanel, 0, emtColor1->bgShiftX, emtColor1->bgShiftY);
	EM2ColorShow * emcFogCol = GetEM2ColorShowInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_COLOR));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_COLOR), bgShiftXRightPanel, 0, emcFogCol->bgShiftX, emcFogCol->bgShiftY);
	EM2CheckBox * emcFogExclBg = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_EXCL_BG));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_EXCL_BG), bgShiftXRightPanel, 0, emcFogExclBg->bgShiftX, emcFogExclBg->bgShiftY);
	EM2Text * emtDensity1 = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_DENSITY));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_DENSITY), bgShiftXRightPanel, 0, emtDensity1->bgShiftX, emtDensity1->bgShiftY);
	EM2Text * emtDistance1 = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_DISTANCE_FOG));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_DISTANCE_FOG), bgShiftXRightPanel, 0, emtDistance1->bgShiftX, emtDistance1->bgShiftY);
	EM2EditSpin * emesFogDens = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_DENSITY));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_DENSITY), bgShiftXRightPanel, 0, emesFogDens->bgShiftX, emesFogDens->bgShiftY);
	EM2EditSpin * emesFogDist = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_DIST));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_DIST), bgShiftXRightPanel, 0, emesFogDist->bgShiftX, emesFogDist->bgShiftY);

	EM2CheckBox * emcAerialOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_ON));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_ON), bgShiftXRightPanel, 0, emcAerialOn->bgShiftX, emcAerialOn->bgShiftY);
	EM2Text * emtAScale = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_SCALE_AERIAL));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_SCALE_AERIAL), bgShiftXRightPanel, 0, emtAScale->bgShiftX, emtAScale->bgShiftY);
	EM2EditSpin * emesAerialScale = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_SCALE));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_SCALE), bgShiftXRightPanel, 0, emesAerialScale->bgShiftX, emesAerialScale->bgShiftY);

	EM2Text * emtGrain = GetEM2TextInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_GRAIN));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_TEXT_GRAIN), bgShiftXRightPanel, 0, emtGrain->bgShiftX, emtGrain->bgShiftY);
	EM2EditTrackBar * emetGrain = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GRAIN));
	updateControlBgShift(GetDlgItem(hTabEffects, IDC_REND2_EFF_GRAIN), bgShiftXRightPanel, 0, emetGrain->bgShiftX, emetGrain->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartEffectsTab(HWND hWnd)
{
	int x, y;
	x = 8;	y = 1;
	int ex = 80;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TITLE),					HWND_TOP,	130+x,	11, 100, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_REDRAW),				HWND_TOP,	128+x,	39, 103, 22, 0);

	x = 8; y = 75;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GR_BLOOM),				HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ON),				HWND_TOP,	x,		y+33,	70,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_POWER),			HWND_TOP,	x+ex,	y+28,	360-ex,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_AREA_BLOOM),		HWND_TOP,	x,		y+60,	50,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_THRESHOLD_BLOOM),	HWND_TOP,	x+225,	y+60,	70,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_AREA),			HWND_TOP,	x+ex,	y+58,	65,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_THRESHOLD),		HWND_TOP,	x+295,	y+58,	65,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ATT_ON),			HWND_TOP,	x,		y+93,	90,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_ATT_COLOR),		HWND_TOP,	x+98,	y+92,	14,		14,			0);

	x = 8; y = 195;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GR_GLARE),				HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_ON),				HWND_TOP,	x,		y+33,	70,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_POWER),			HWND_TOP,	x+ex,	y+28,	360-ex,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_AREA_GLARE),		HWND_TOP,	x,		y+60,	50,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_THRESHOLD_GLARE),	HWND_TOP,	x+225,	y+60,	70,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_AREA),			HWND_TOP,	x+ex,	y+58,	65,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_THRESHOLD),		HWND_TOP,	x+295,	y+58,	65,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_DISPERSION),		HWND_TOP,	x,		y+90,	70,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_DISPERSION),		HWND_TOP,	x+ex,	y+88,	360-ex,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_APERTURE_SHAPE),	HWND_TOP,	x,		y+120,	100,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_APERTURE_OBSTACLE),HWND_TOP,	x,		y+150,	120,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_SHAPE),		HWND_TOP,	x+209,	y+123,	10,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TXON_APERT_OBSTACLE),	HWND_TOP,	x+209,	y+153,	10,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TX_APERT_SHAPE),		HWND_TOP,	x+228,	y+117,	132,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TX_APERT_OBSTACLE),		HWND_TOP,	x+228,	y+147,	132,	22,			0);

	x = 8; y = 375;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GR_FOG),				HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_ON),				HWND_TOP,	x,		y+33,	70,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_COLOR_FOG),		HWND_TOP,	x+120,	y+30,	48,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_COLOR),				HWND_TOP,	x+168,	y+32,	14,		14,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_EXCL_BG),			HWND_TOP,	x+220,	y+33,	140,	10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_DENSITY),			HWND_TOP,	x,		y+60,	60,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_DISTANCE_FOG),		HWND_TOP,	x+197,	y+60,	60,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DENSITY),			HWND_TOP,	x+ex,	y+58,	65,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DIST),				HWND_TOP,	x+257,	y+58,	103,	20,			0);
	
	x = 8; y = 465;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GR_AERIAL),				HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_ON),		HWND_TOP,	x,		y+33,	70,		10,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_SCALE_AERIAL),		HWND_TOP,	x+250,	y+30,	45,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_SCALE),	HWND_TOP,	x+295,	y+28,	65,		20,			0);

	x = 8; y = 525;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GR_GRAIN),				HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_TEXT_GRAIN),			HWND_TOP,	x,		y+30,	45,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GRAIN),					HWND_TOP,	x+ex,	y+28,	360-ex,	20,			0);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_POWER),			HWND_TOP,											0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_AREA),			GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_POWER),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_THRESHOLD),		GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_AREA),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_POWER),			GetDlgItem(hWnd, IDC_REND2_EFF_BLOOM_THRESHOLD),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_AREA),			GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_POWER),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_THRESHOLD),		GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_AREA),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_DISPERSION),		GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_THRESHOLD),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DENSITY),			GetDlgItem(hWnd, IDC_REND2_EFF_GLARE_DISPERSION),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DIST),				GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DENSITY),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_SCALE),	GetDlgItem(hWnd, IDC_REND2_EFF_FOG_DIST),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_EFF_GRAIN),					GetDlgItem(hWnd, IDC_REND2_EFF_AERIAL_PERSP_SCALE),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);


}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillEffectsTabAll(Camera * cam, FinalModifier * fMod)
{
	CHECK(fMod);
	CHECK(hTabEffects);

	EM2CheckBox * emcBloomOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ON));
	EM2EditTrackBar * emetBloomPower = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_POWER));
	EM2EditSpin * emesBloomArea = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_AREA));
	EM2EditSpin * emesBloomThres = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_THRESHOLD));
	EM2CheckBox * emcBloomAttOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ATT_ON));
	EM2ColorShow * emcBloomAttCol = GetEM2ColorShowInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ATT_COLOR));

	EM2CheckBox * emcGlareOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_ON));
	EM2EditTrackBar * emetGlarePower = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_POWER));
	EM2EditSpin * emesGlareArea = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_AREA));
	EM2EditSpin * emesGlareThres = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_THRESHOLD));
	EM2EditTrackBar * emetDisp = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_DISPERSION));
	EM2CheckBox * emcTxOnApShape = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TXON_APERT_SHAPE));
	EM2CheckBox * emcTxOnApObst = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TXON_APERT_OBSTACLE));
	EM2Button * embTxApShape = GetEM2ButtonInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TX_APERT_SHAPE));
	EM2Button * embTxApObst = GetEM2ButtonInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_TX_APERT_OBSTACLE));

	EM2CheckBox * emcFogOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_ON));
	EM2ColorShow * emcFogCol = GetEM2ColorShowInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_COLOR));
	EM2CheckBox * emcFogExclBg = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_EXCL_BG));
	EM2EditSpin * emesFogDens = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_DENSITY));
	EM2EditSpin * emesFogDist = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_DIST));

	EM2CheckBox * emcAerialOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_ON));
	EM2EditSpin * emesAerialScale = GetEM2EditSpinInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_SCALE));

	EM2EditTrackBar * emetGrain = GetEM2EditTrackBarInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GRAIN));


	emcBloomOn->selected = fMod->bloomEnabled;
	InvalidateRect(emcBloomOn->hwnd, NULL, false);
	emetBloomPower->setFloatValue(fMod->bloomPower);
	emesBloomArea->setIntValue(fMod->bloomArea);
	emesBloomThres->setFloatValue(fMod->bloomThreshold);
	emcBloomAttOn->selected = fMod->bloomAttenuationOn;
	InvalidateRect(emcBloomAttOn->hwnd, NULL, false);
	emcBloomAttCol->redraw(fMod->bloomAttenuationColor);

	emcGlareOn->selected = fMod->glareEnabled;
	InvalidateRect(emcGlareOn->hwnd, NULL, false);
	emetGlarePower->setFloatValue(fMod->glarePower);
	emesGlareArea->setIntValue(fMod->glareArea);
	emesGlareThres->setFloatValue(fMod->glareThreshold);
	emetDisp->setFloatValue(fMod->glareDispersion);

	emcTxOnApShape->selected = fMod->glareTextureApertureOn;
	InvalidateRect(emcTxOnApShape->hwnd, NULL, false);
	emcTxOnApObst->selected = fMod->glareTextureObstacleOn;
	InvalidateRect(emcTxOnApObst->hwnd, NULL, false);

	if (cam)
	{
		if (cam->texDiaphragm.filename  &&  cam->texDiaphragm.isValid())
			SetWindowText(embTxApShape->hwnd, cam->texDiaphragm.filename);
		else
			SetWindowText(embTxApShape->hwnd, "NO MAP LOADED");

		if (cam->texObstacle.filename  &&  cam->texObstacle.isValid())
			SetWindowText(embTxApObst->hwnd, cam->texObstacle.filename);
		else
			SetWindowText(embTxApObst->hwnd, "NO MAP LOADED");
	}

	emcFogOn->selected = fMod->fogEnabled;
	InvalidateRect(emcFogOn->hwnd, NULL, false);
	emcFogCol->redraw(fMod->fogColor);
	emcFogExclBg->selected = fMod->fogExclSky;
	InvalidateRect(emcFogExclBg->hwnd, NULL, false);
	emesFogDens->setFloatValue(fMod->fogSpeed);
	emesFogDist->setFloatValue(fMod->fogDist);

	emcAerialOn->selected = fMod->aerialEnabled;
	InvalidateRect(emcAerialOn->hwnd, NULL, false);
	emesAerialScale->setFloatValue(fMod->aerialScale);

	emetGrain->setFloatValue(fMod->grain);

	updateLocksTabEffects(Raytracer::getInstance()->curScenePtr->nowRendering);

	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabEffects(bool nowrendering)
{
	setControlEnabled(!nowrendering, hTabEffects, IDC_REND2_EFF_REDRAW);

	EM2CheckBox * emcBloomOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ON));
	EM2CheckBox * emcBloomAttOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_BLOOM_ATT_ON));
	EM2CheckBox * emcGlareOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_GLARE_ON));
	EM2CheckBox * emcFogOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_FOG_ON));
	EM2CheckBox * emcAerialOn = GetEM2CheckBoxInstance(GetDlgItem(hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_ON));

	setControlEnabled(emcBloomOn->selected, hTabEffects, IDC_REND2_EFF_BLOOM_POWER);
	setControlEnabled(emcBloomOn->selected, hTabEffects, IDC_REND2_EFF_BLOOM_AREA);
	setControlEnabled(emcBloomOn->selected, hTabEffects, IDC_REND2_EFF_BLOOM_THRESHOLD);
	setControlEnabled(emcBloomOn->selected, hTabEffects, IDC_REND2_EFF_BLOOM_ATT_ON);
	setControlEnabled(emcBloomOn->selected  &&  emcBloomAttOn->selected, hTabEffects, IDC_REND2_EFF_BLOOM_ATT_COLOR);
	setControlEnabled(emcBloomOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_AREA_BLOOM);
	setControlEnabled(emcBloomOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_THRESHOLD_BLOOM);

	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_GLARE_POWER);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_GLARE_AREA);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_GLARE_THRESHOLD);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_GLARE_DISPERSION);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TXON_APERT_OBSTACLE);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TXON_APERT_SHAPE);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TX_APERT_OBSTACLE);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TX_APERT_SHAPE);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_AREA_GLARE);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_THRESHOLD_GLARE);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_DISPERSION);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_APERTURE_SHAPE);
	setControlEnabled(emcGlareOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_APERTURE_OBSTACLE);

	setControlEnabled(emcFogOn->selected, hTabEffects, IDC_REND2_EFF_FOG_COLOR);
	setControlEnabled(emcFogOn->selected, hTabEffects, IDC_REND2_EFF_FOG_EXCL_BG);
	setControlEnabled(emcFogOn->selected, hTabEffects, IDC_REND2_EFF_FOG_DENSITY);
	setControlEnabled(emcFogOn->selected, hTabEffects, IDC_REND2_EFF_FOG_DIST);
	setControlEnabled(emcFogOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_COLOR_FOG);
	setControlEnabled(emcFogOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_DENSITY);
	setControlEnabled(emcFogOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_DISTANCE_FOG);

	setControlEnabled(emcAerialOn->selected, hTabEffects, IDC_REND2_EFF_AERIAL_PERSP_SCALE);
	setControlEnabled(emcAerialOn->selected, hTabEffects, IDC_REND2_EFF_TEXT_SCALE_AERIAL);

}

//------------------------------------------------------------------------------------------------
