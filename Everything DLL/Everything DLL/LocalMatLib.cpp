#define _CRT_RAND_S
#include "MatEditor.h"
#include "resource.h"
#include "XML_IO.h"
#include "log.h"
#include <stdio.h>
#include <mbstring.h>
#include"MaterialArchiver.h"

extern char  * defaultDirectory;

#define TEXINLAY 9

//---------------------------------------------------------------------------------------------------------------

LocalMatLib::LocalMatLib() 
{
	localList = NULL;
	queryMode = MODE_CATEGORY;
	currentCategory = 0;
	currentPage = 0;
}

//---------------------------------------------------------------------------------------------------------------

LocalMatLib::~LocalMatLib()
{
	for (int i=0; i<23; i++)
	{
		for (int j=0; j<localList[i].objCount; j++)
			localList[i][j].releaseEverything();
		localList[i].freeList();
		localList[i].destroyArray();
	}
	free(localList);
	localList = NULL;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::initialize()
{
	localList = (bList<LocalMatLib::MatInfo, 8> *)malloc(23*sizeof(bList<LocalMatLib::MatInfo, 8>));
	for (int i=0; i<23; i++)
	{
		new (&(localList[i]))  bList<LocalMatLib::MatInfo, 8>(0);
		localList[i].createArray();
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

LocalMatLib::MatInfo::MatInfo()
{
	unique = -1;
	layers = 0;
	textures = 0;
	name = NULL;
	filename = NULL;
}

LocalMatLib::MatInfo::~MatInfo()
{
}

//---------------------------------------------------------------------------------------------------------------

char * LocalMatLib::getLocalMaterialsDirectoryName()
{
	if (!defaultDirectory)
		return NULL;
	int l = (int)strlen(defaultDirectory)+32;
	char * res = (char*)malloc(l);
	if (res)
		sprintf_s(res, l, "%s\\materials", defaultDirectory);
	return res;
}

//---------------------------------------------------------------------------------------------------------------

char * LocalMatLib::getLocalCategoryDirectoryName(int cat)
{
	if (!defaultDirectory)
		return NULL;
	int l = (int)strlen(defaultDirectory)+128;
	char * res = (char*)malloc(l);
	if (res)
		sprintf_s(res, l, "%s\\materials\\%s", defaultDirectory, MatCategory::getCategoryName(cat));
	return res;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::refreshCategory(int cat)
{
	CHECK(cat<23 && cat>=0);

	for (int j=0; j<localList[cat].objCount; j++)
	{
		localList[cat][j].releaseEverything();
	}
	localList[cat].freeList();
	localList[cat].destroyArray();

	char * catDir = getLocalCategoryDirectoryName(cat);
	CHECK(catDir);
	CHECK(dirExists(catDir));

	int cl = (int)strlen(catDir)+16;
	char * wCatDir = (char*)malloc(cl);
	sprintf_s(wCatDir, cl, "%s\\*", catDir);

	HANDLE hDir;
	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hDir = FindFirstFile(wCatDir, &fd);
	
	do
	{
		if (hDir != INVALID_HANDLE_VALUE   &&   (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			if (!strcmp(fd.cFileName, "."))
				continue;
			if (!strcmp(fd.cFileName, ".."))
				continue;

			char fname[2048];
			sprintf_s(fname, 2048, "%s\\%s", catDir, fd.cFileName);

			MatInfo iinfo;
			
			if (iinfo.processFromDir(fname))
			{
				localList[cat].add(iinfo);
			}
		}
	} while (FindNextFile(hDir, &fd));

	localList[cat].createArray();

	FindClose(hDir);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::createMaterialsList()
{
	for (int i=0; i<23; i++)
		if (!refreshCategory(i))
			return false;
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::MatInfo::processFromDir(char * dir)
{
	CHECK(dir);
	CHECK(dirExists(dir));

	char wCard[2048];
	sprintf_s(wCard, 2048, "%s\\*.nxm", dir);

	HANDLE hFile;
	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));

	hFile = FindFirstFile(wCard, &fd);
	if (hFile == INVALID_HANDLE_VALUE   ||   (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		return false;

	char * fname = copyString(fd.cFileName);

	FindClose(hFile);

	int l = (int)strlen(fname);
	int ss = 1;
	if (l > 13)
		ss = _mbsnicmp((unsigned char *)fname, (unsigned char *)"material ", 9);
	if (!ss)
	{
		if (!_mbsnicmp((unsigned char *)&(fname[l-4]), (unsigned char *)".nxm", 4))
		{
			char * addr;
			__int64 a = _strtoi64(&(fname[9]), &addr, 10);
			if (addr == &(fname[l-4]))
				unique = (int)a;
			else
				unique = -1;
		}
		else
		{
			unique = -1;
		}
	}
	else
		unique = -1;

	int ll = (int)strlen(dir) + (int)strlen(fname) + 16;
	char * fullfilename = (char *)malloc(ll);
	sprintf_s(fullfilename, ll, "%s\\%s", dir, fname);


	XMLlocalMatInfo xmlinfo;
	bool xmlOK = xmlinfo.loadFile(fullfilename);


	if (!xmlOK)
	{
		free(fname);
		return false;
	}

	free(fname);

	layers = xmlinfo.info.layers;
	textures = xmlinfo.info.textures;
	name = copyString(xmlinfo.info.name);
	preview.copyImageFrom(&xmlinfo.info.preview);
	filename = fullfilename;

	return true;
}

//---------------------------------------------------------------------------------------------------------------

void LocalMatLib::MatInfo::releaseEverything()
{
	if (filename)
		free(filename);
	filename = NULL;
	if (name)
		free(name);
	name = NULL;
	preview.freeBuffer();
	layers = 0;
	textures = 0;
	unique = 0;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::showByCategory(int cat, int page)
{
	CHECK(page>=0);
	CHECK(cat>=0);
	CHECK(cat<23);

	MatEditorWindow::onlinematlib.clearPreviewControls();

	int m1 = page*7;
	int m2 = min(page*7+7, localList[cat].objCount);

	if (localList[cat].objCount > (page+1)*7)		// check NEXT button
		enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_RIGHT);
	else
		disableControl(MatEditorWindow::hMain, IDC_MATEDIT_LIB_RIGHT);
	if (page > 0)									// check PREV button
		enableControl (MatEditorWindow::hMain, IDC_MATEDIT_LIB_LEFT);
	else
		disableControl(MatEditorWindow::hMain, IDC_MATEDIT_LIB_LEFT);

	MatEditorWindow::setPageControl(page, max(0, localList[cat].objCount-1)/7+1);

	currentCategory = cat;
	currentPage = page;
	queryMode = MODE_CATEGORY;

	for (int i=m1; i<m2; i++)
	{
		EMPView * empv = GetEMPViewInstance(GetDlgItem(MatEditorWindow::hMain, MatEditorWindow::libIDs[i%7]));
		if (!empv)
			continue;
		for (int y=0; y<96; y++)
			for (int x=0; x<96; x++)
				empv->byteBuff->imgBuf[y][x] = localList[cat][i].preview.imgBuf[y][x];
		InvalidateRect(empv->hwnd, NULL, false);


		char tempstring[2048];
		sprintf_s(tempstring, 2048, "Name: %s\nLayers: %d\nTextures: %d", 
				localList[cat][i].name,
				localList[cat][i].layers,
				localList[cat][i].textures);
		empv->setToolTip(tempstring);

	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::saveToLibrary()
{
	MaterialNox * mat = MatEditorWindow::getEditedMaterial();
	CHECK(mat);

	int cat = MatEditorWindow::autoCategory;
	unsigned int nr;
	if (MatEditorWindow::autoID <= 0)
	{
		cat = MatCategory::CAT_USER;
		while(rand_s(&nr));
	}
	else
		nr = MatEditorWindow::autoID;

	// get material name
	char * matName = copyString(mat->name);
	if (!matName)
	{
		matName = (char*)malloc(32);
		sprintf_s(matName, 32, "Unnamed");
	}

	MatEditorWindow::copyBufferWindowToMaterial();

	correctFilename(matName);

	char dirMats[2048];
	char dirCategory[2048];
	char dirSavedMat[2048];
	char dirTextures[2048];
	sprintf_s(dirMats, 2048, "%s\\materials", defaultDirectory);
	sprintf_s(dirCategory, 2048, "%s\\materials\\%s", defaultDirectory, MatCategory::getCategoryName(cat));
	sprintf_s(dirSavedMat, 2048, "%s\\%s_%u", dirCategory, matName, nr);
	sprintf_s(dirTextures, 2048, "%s\\textures", dirSavedMat);

	if (!dirExists(dirMats))
	{
		Logger::add("Creating materials dir.");
		if (!CreateDirectory(dirMats, NULL))
		{
			Logger::add("Failed");
			MessageBox(0, "Materials dir doesn't exist and can't create it.\nMaterial not added.", "Error", 0);
			return false;
		}
	}

	if (!dirExists(dirCategory))
	{
		Logger::add("Creating category dir.");
		if (!CreateDirectory(dirCategory, NULL))
		{
			Logger::add("Failed");
			MessageBox(0, "Category dir doesn't exist and can't create it.\nMaterial not added.", "Error", 0);
			return false;
		}
	}

	if (!dirExists(dirSavedMat))
	{
		Logger::add("Creating saved material dir.");
		if (!CreateDirectory(dirSavedMat, NULL))
		{
			Logger::add("Failed");
			MessageBox(0, "Can't create new material dir.\nMaterial not added.", "Error", 0);
			return false;
		}
	}

	if (!dirExists(dirTextures))
	{
		Logger::add("Creating textures dir.");
		if (!CreateDirectory(dirTextures, NULL))
		{
			Logger::add("Failed");
			MessageBox(0, "Can't create new material textures dir.\nMaterial not added.", "Error", 0);
			return false;
		}
	}

	// create list of textures
	bList<char *, 7> toDel(0);
	bList<RenamedTextures, 7> textures(0);
	for (int i=0; i<mat->layers.objCount; i++)
	{
		char * actual = NULL;
		TextureInstance * texI = NULL;
		for (int j=0; j<TEXINLAY; j++)
		{
			switch (j)
			{
				case 0: texI = &(mat->layers[i].tex_col0); break;
				case 1: texI = &(mat->layers[i].tex_col90); break;
				case 2: texI = &(mat->layers[i].tex_rough); break;
				case 3: texI = &(mat->layers[i].tex_weight); break;
				case 4: texI = &(mat->layers[i].tex_light); break;
				case 5: texI = &(mat->layers[i].tex_normal); break;
				case 6: texI = &(mat->layers[i].tex_transm); break;
				case 7: texI = &(mat->layers[i].tex_aniso); break;
				case 8: texI = &(mat->layers[i].tex_aniso_angle); break;
				default: texI = NULL;
			}
			
			if (texI)
			{
				if (!texI->isValid()  ||  !texI->filename   ||   !texI->fullfilename)
					continue;
				RenamedTextures rtex;
				rtex.filename = texI->filename;
				rtex.fullfilename = texI->fullfilename;
				rtex.layer = i;
				rtex.type = j;
				textures.add(rtex);
				textures.createArray();
			}

		}
	}

	if (mat->tex_opacity.isValid()  &&  mat->tex_opacity.filename   &&   mat->tex_opacity.fullfilename)
	{
		RenamedTextures rtex;
		rtex.filename = mat->tex_opacity.filename;
		rtex.fullfilename = mat->tex_opacity.fullfilename;
		rtex.layer = 0;
		rtex.type = 20;

		textures.add(rtex);
		textures.createArray();
	}

	if (mat->tex_displacement.isValid()  &&  mat->tex_displacement.filename   &&   mat->tex_displacement.fullfilename)
	{
		RenamedTextures rtex;
		rtex.filename = mat->tex_displacement.filename;
		rtex.fullfilename = mat->tex_displacement.fullfilename;
		rtex.layer = 0;
		rtex.type = 21;

		textures.add(rtex);
		textures.createArray();
	}

	// check for duplicates and copy to result dir
	for (int k=0; k<textures.objCount; k++)
	{
		textures[k].newfilename = copyString(textures[k].filename);
		bool copying = true;
		for (int j=0; j<k; j++)
		{
			if (strcmp(textures[k].fullfilename, textures[j].fullfilename) == 0)
			{	// the same
				if (textures[k].newfilename)
					free(textures[k].newfilename);
				textures[k].newfilename = copyString(textures[j].newfilename);
				copying = false;
			}
			else
			{
				textures[k].newfilename = checkDuplicates(textures[k].newfilename, textures[j].newfilename);
			}
		}

		int ll = (int)strlen(textures[k].newfilename) + (int)strlen(textures[k].fullfilename) + 256;
		char * llog = (char *)malloc(ll);
		char * type = NULL;
		switch (textures[k].type)
		{
			case 0:  type="color0      "; break;
			case 1:  type="color90     "; break;
			case 2:  type="roughness   "; break;
			case 3:  type="weight      "; break;
			case 4:  type="light       "; break;
			case 5:  type="normal      "; break;
			case 6:  type="transmission"; break;
			case 7:  type="aniso       "; break;
			case 8:  type="aniso angle "; break;
			case 20: type="opacity     "; break;
			case 21: type="displacement"; break;
			default: type="ERROR       "; break;
		}

		if (textures[k].type<20)
			sprintf_s(llog, ll, " Layer[%d] %s texture %s : \"%s\" => \"%s\"", textures[k].layer, copying?"copying":"using  ", type, textures[k].fullfilename, textures[k].newfilename);
		else
			sprintf_s(llog, ll, " Material  %s texture %s : \"%s\" => \"%s\"", copying?"copying":"using  ", type, textures[k].fullfilename, textures[k].newfilename);
		Logger::add(llog);

		if (copying)
		{
			int ld = (int)strlen(textures[k].newfilename) + (int)strlen(dirSavedMat) + 32;
			char * fullDst = (char *)malloc(ld);
			sprintf_s(fullDst, ld, "%s\\textures\\%s", dirSavedMat, textures[k].newfilename);
			if (copyTexFile(textures[k].fullfilename, fullDst))
			{
				toDel.add(fullDst);
				toDel.createArray();
			}
			else
				Logger::add(" copy failed :(");
		}

		// change texture names in material...
		TextureInstance * texI = NULL;
		switch (textures[k].type)
		{
			case 0:		texI = &(mat->layers[textures[k].layer].tex_col0);  break;
			case 1:		texI = &(mat->layers[textures[k].layer].tex_col90);  break;
			case 2:		texI = &(mat->layers[textures[k].layer].tex_rough);  break;
			case 3:		texI = &(mat->layers[textures[k].layer].tex_weight);  break;
			case 4:		texI = &(mat->layers[textures[k].layer].tex_light);  break;
			case 5:		texI = &(mat->layers[textures[k].layer].tex_normal);  break;
			case 6:		texI = &(mat->layers[textures[k].layer].tex_transm);  break;
			case 7:		texI = &(mat->layers[textures[k].layer].tex_aniso);  break;
			case 8:		texI = &(mat->layers[textures[k].layer].tex_aniso_angle);  break;
			case 20:	texI = &(mat->tex_opacity);  break;
			case 21:	texI = &(mat->tex_displacement);  break;
		}

		if (texI)
		{
			texI->filename = textures[k].newfilename;
			int tl = (int)strlen(textures[k].newfilename)+32;
			texI->fullfilename = (char *)malloc(tl);
			sprintf_s(texI->fullfilename, tl, "textures\\%s", textures[k].newfilename);
		}
	}

	// save xml
	char materialFilename[2048];
	if (MatEditorWindow::autoID <= 0)
		sprintf_s(materialFilename, 2048, "%s\\material.nxm", dirSavedMat);
	else
		sprintf_s(materialFilename, 2048, "%s\\material %d.nxm", dirSavedMat, MatEditorWindow::autoID);


	Logger::add(" Saving NOX material...");
	Raytracer * rtr = Raytracer::getInstance();
	XMLScene xscene;
	bool saved = xscene.saveMaterial(materialFilename, mat);


	for (int k=0; k<textures.objCount; k++)
	{
		// change back texture names in material...
		TextureInstance * texI = NULL;
		switch (textures[k].type)
		{
			case 0:		texI = &(mat->layers[textures[k].layer].tex_col0);  break;
			case 1:		texI = &(mat->layers[textures[k].layer].tex_col90);  break;
			case 2:		texI = &(mat->layers[textures[k].layer].tex_rough);  break;
			case 3:		texI = &(mat->layers[textures[k].layer].tex_weight);  break;
			case 4:		texI = &(mat->layers[textures[k].layer].tex_light); break;
			case 5:		texI = &(mat->layers[textures[k].layer].tex_normal); break;
			case 6:		texI = &(mat->layers[textures[k].layer].tex_transm); break;
			case 7:		texI = &(mat->layers[textures[k].layer].tex_aniso); break;
			case 8:		texI = &(mat->layers[textures[k].layer].tex_aniso_angle); break;
			case 20:	texI = &(mat->tex_opacity);  break;
			case 21:	texI = &(mat->tex_displacement);  break;
		}

		if (texI)
		{
			free(texI->fullfilename);
			free(texI->filename);
			texI->filename = textures[k].filename;
			texI->fullfilename = textures[k].fullfilename;
		}
	}
	textures.freeList();
	textures.destroyArray();

	refreshCategory(cat);

	if (MatEditorWindow::libMode == 1)
	{
		switch (queryMode)
		{
			case MODE_CATEGORY:
				{
					if (currentCategory > 22   ||   currentCategory < 0)
						return true;
					showByCategory(currentCategory, currentPage);
				}
				break;
			case MODE_MY_MATS:
				{
				}
				break;
			case MODE_SEARCH:
				{
				}
				break;
		}
	}

	if (!saved)
	{
		Logger::add(" Failed to save material.");
		MessageBox(0, "Adding material failed.", "Error", 0);
		return false;
	}

	char mesg[512];
	sprintf_s(mesg, 512, "Succesfully added material to category %s", MatCategory::getCategoryName(cat)); 
	Logger::add(mesg);
	MessageBox(0, mesg, "Success", 0);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::loadMaterial(int cat, int num)
{
	CHECK(cat<23 && cat>=0);
	CHECK(num>=0);
	CHECK(num < localList[cat].objCount);

	MatEditorWindow::loadMaterial(localList[cat][num].filename);
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool LocalMatLib::removeFromLibrary(int cat, int num)
{
	CHECK(cat<23 && cat>=0);
	CHECK(num>=0);
	CHECK(num < localList[cat].objCount);

	char question[512];
	sprintf_s(question, 512, "Do you want to delete material named \"%s\" from category %s?", localList[cat][num].name, MatCategory::getCategoryName(cat));
	int res = MessageBox(MatEditorWindow::hMain, question, "Are you sure?", MB_YESNO);
	if (res != IDYES)
		return false;

	if (fileExists(localList[cat][num].filename))
	{
		char delinfobuf[2048];
		sprintf_s(delinfobuf, 2048, " Deleting %s", localList[cat][num].filename);
		Logger::add(delinfobuf);
		DeleteFile(localList[cat][num].filename);
	}

	refreshCategory(cat);

	if (MatEditorWindow::libMode == 1)
	{
		switch (queryMode)
		{
			case MODE_CATEGORY:
				{
					if (currentCategory > 22   ||   currentCategory < 0)
						return true;
					showByCategory(currentCategory, currentPage);
				}
				break;
			case MODE_MY_MATS:
				{
				}
				break;
			case MODE_SEARCH:
				{
				}
				break;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

