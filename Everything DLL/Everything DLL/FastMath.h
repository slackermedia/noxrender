#ifndef __FAST_MATH__H
#define __FAST_MATH__H

#include "DLL.h"

#ifndef BIGFLOAT
	#define BIGFLOAT (float(1.0e30))
#endif

#ifndef PI
	#define PI 3.1415926535897932384626433832795f
#endif

#ifndef TWOPI
	#define TWOPI 6.283185307179586476925286766559f
#endif

#define PI_PER_TWO 1.5707963267948966192313216916398f
#define RAD_TO_DEG(x) ((x)*57.295779513082320876798154814105f)
#define DEG_TO_RAD(x) ((x)*0.017453292519943295769236907684886f)
#define ARRAY_ANGLES_COUNT 629
#define ARRAY_ARC_COUNT 201

class FastMath
{
public:
	static float sinArray[ARRAY_ANGLES_COUNT];
	static float cosArray[ARRAY_ANGLES_COUNT];
	static float tanArray[ARRAY_ANGLES_COUNT];
	static float ctanArray[ARRAY_ANGLES_COUNT];
	static float asinArray[ARRAY_ANGLES_COUNT];
	static float acosArray[ARRAY_ANGLES_COUNT];

	static bool initialized;
	static void initialize();

	static float sin(float x);
	static float cos(float x);
	static float tan(float x);
	static float cot(float x);
	static float asin(float x);
	static float acos(float x);

	FastMath();
	~FastMath();
};




#endif

