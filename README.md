# NOX Renderer

## What is NOX

This is code archived from a 2014 release of the NOX 3d renderer, with plugins for Blender.

If you're a user, this probably is not usable to you in this format. This Git repository is geared toward developers.


## Open source

According to Evermotion in a post on 2014-08-26 at 12:00:

    After years of development we decided to give NOX for the community.

    NOX physically based renderer, fully integrated with Blender and 3ds Max (and with C4D support) is now Open Source. We release it on Apache license - free to commercial use and modifications.

    You can freely improve and modify this render engine, integrate it with any 3d software, write plugins for NOX, use it in your commercial works and / or sell it. The possibilities are endless and depend only on you.

    Main features of NOX:

    - physical based engine
    - enhanced postprocesing
    - rendering to layers
    - real and fake dof
    - instancing and displacement
    - subsurface scattering

Read the original post at https://evermotion.org/articles/show/8845/nox-renderer-is-now-open-source-software-

## Development

I'm not now developing, nor have I ever developed, the NOX renderer. This Git repository is purely for archival purposes.

This is almost entirely unmodified from what Evermotion posted when they open sourced it: it's Windows code, so it needs porting to Linux.

The only thing I've done is add a LICENSE file and added the Apache License text to the LicenceDialog.cpp file.