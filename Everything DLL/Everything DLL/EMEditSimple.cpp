#include <windows.h>
#include <stdio.h>
#include "EMControls.h"
#include "log.h"
extern HMODULE hDllModule;

void InitEMEditSimple()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMEditSimple";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMEditSimpleProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMEditSimple *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);


    WNDCLASSEX wcp;
    
    wcp.cbSize         = sizeof(wcp);
    wcp.lpszClassName  = "EMEditPassword";
    wcp.hInstance      = hDllModule;
    wcp.lpfnWndProc    = EMEditSimpleProc;
    wcp.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wcp.hIcon          = 0;
    wcp.lpszMenuName   = 0;
    wcp.hbrBackground  = (HBRUSH)0;
    wcp.style          = 0;
    wcp.cbClsExtra     = 0;
    wcp.cbWndExtra     = sizeof(EMEditSimple *);
    wcp.hIconSm        = 0;

    RegisterClassEx(&wcp);

}

EMEditSimple::EMEditSimple(HWND hWnd)
{
	colBorderUpLeft = RGB(48,48,48);
	colBorderDownRight = RGB(112,112,112);
	colBackground = RGB(80,80,80);
	colText = RGB(224,224,224);
	colDisabledBorderUpLeft = RGB(64,64,64);
	colDisabledBorderDownRight = RGB(112,112,112);
	colDisabledBackground = RGB(96,96,96);
	colDisabledText = RGB(160,160,160);

	hwnd = hWnd;
	hEdit = 0;

	hBrush = CreateSolidBrush(colBackground);
	hDisabledBrush = CreateSolidBrush(colDisabledBackground);
}

EMEditSimple::~EMEditSimple()
{
	if (hBrush)
		DeleteObject(hBrush);
	hBrush = NULL;
	if (hDisabledBrush)
		DeleteObject(hDisabledBrush);
	hDisabledBrush = NULL;
}

EMEditSimple * GetEMEditSimpleInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMEditSimple * emes = (EMEditSimple *)GetWindowLongPtr(hwnd, 0);
	#else
		EMEditSimple * emes = (EMEditSimple *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emes;
}

void SetEMEditSimpleInstance(HWND hwnd, EMEditSimple *emes)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emes);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emes);
	#endif
}

typedef LRESULT (WINAPI * EDITPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EDITPROC OldEditSimpleProc;


LRESULT CALLBACK EMEditSimpleEditProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_CHAR:
		//case WM_KEYDOWN:
			{
				if (wParam == VK_RETURN)
				{
					LONG wID;
					wID = GetWindowLong(hwnd, GWL_ID);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_RETURN), (LPARAM)hwnd);
					SendMessage(hwnd, EM_SETSEL, 0, -1);
					return TRUE;
				}
				if (wParam == VK_ESCAPE)
				{
					SendMessage(hwnd, EM_SETSEL, -1, 0);
					return TRUE;
				}
				if (wParam == VK_TAB)
				{
					LONG wID = GetWindowLong(hwnd, GWL_ID);
					if (GetKeyState(VK_SHIFT)&0x8000)
						SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB_SHIFT), (LPARAM)hwnd);
					else
						SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB), (LPARAM)hwnd);
					return TRUE;
				}
			}
			break;
		case WM_KILLFOCUS:
			{
				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_LOST_FOCUS), (LPARAM)hwnd);
			}
			break;
		case WM_SETFOCUS:
			{
				SendMessage(hwnd, EM_SETSEL, 0, -1);
			}
			break;
	}
	return CallWindowProc(OldEditSimpleProc, hwnd, msg, wParam, lParam);
}

LRESULT CALLBACK EMEditSimpleProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMEditSimple * emes;
	emes = GetEMEditSimpleInstance(hwnd);
	static HRGN rgn = 0;

    switch(msg)
    {
		case WM_CREATE:
			{
				EMEditSimple * emes1 = new EMEditSimple(hwnd);
				SetEMEditSimpleInstance(hwnd, (EMEditSimple *)emes1);
				RECT rect;
				GetClientRect(emes1->hwnd, &rect);

				bool isPass = false;
				char cname[256];
				if (GetClassName(hwnd, cname, 256))
				{
					if (!strcmp(cname, "EMEditPassword"))
						isPass = true;
				}

				if (isPass)
					emes1->hEdit = CreateWindow("EDIT", "", 
								WS_CHILD | WS_TABSTOP | WS_VISIBLE | ES_AUTOHSCROLL 
								| ES_PASSWORD | ES_WANTRETURN,
								1, 1, rect.right-2, rect.bottom-2,
								hwnd, (HMENU)0, NULL, 0);
				else
					emes1->hEdit = CreateWindow("EDIT", "", 
								WS_CHILD | WS_TABSTOP | WS_VISIBLE | ES_AUTOHSCROLL 
								| ES_MULTILINE | ES_WANTRETURN,
								1, 1, rect.right-2, rect.bottom-2,
								hwnd, (HMENU)0, NULL, 0);

				SendMessage(emes1->hEdit, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

				#ifdef _WIN64
					OldEditSimpleProc = (EDITPROC) SetWindowLongPtr(emes1->hEdit, GWLP_WNDPROC, (LONG_PTR)EMEditSimpleEditProc) ;
				#else
					OldEditSimpleProc = (EDITPROC) (HANDLE)(LONG_PTR)SetWindowLong(emes1->hEdit, GWL_WNDPROC, (LONG)(LONG_PTR)EMEditSimpleEditProc) ;
				#endif
			}
			break;
		case WM_ERASEBKGND: 
			return FALSE;
		case WM_CLOSE:
			{
				return TRUE;
			}
			break;
		case WM_NCDESTROY:
			{
				EMEditSimple * emes1;
				emes1 = GetEMEditSimpleInstance(hwnd);
				delete emes1;
				SetEMEditSimpleInstance(hwnd, 0);
			}
		break;
		case WM_CTLCOLOREDIT:
			{
				HDC hdc1 = (HDC)wParam;
				DWORD style;
				style = GetWindowLong(hwnd, GWL_STYLE);
				bool disabled = ((style & WS_DISABLED) > 0);
				if (!disabled)
				{
					SetTextColor(hdc1, emes->colText);
					SetBkColor(hdc1, emes->colBackground);
				}
				else
				{
					SetTextColor(hdc1, emes->colDisabledText);
					SetBkColor(hdc1, emes->colDisabledBackground);
				}
				if (disabled)
					return (INT_PTR)(emes->hDisabledBrush);
				else
					return (INT_PTR)(emes->hBrush);
			}
			break;
		case WM_PAINT:
			{
				if (!emes) 
					break;

				RECT rect;
				HDC hdc;
				HPEN hOldPen;
				PAINTSTRUCT ps;

				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) > 0);

				hdc = BeginPaint(hwnd, &ps);
				GetClientRect(hwnd, &rect);

				// draw editbox border
				POINT eBorder1[] = {
					{0,   rect.bottom-1 },
					{0,   0},
					{rect.right,   0}
				};

				POINT eBorder2[] = {
					{rect.right-1,   1},
					{rect.right-1,   rect.bottom-1},
					{0, rect.bottom-1}
				};

				if (disabled)
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emes->colDisabledBorderUpLeft));
				else
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emes->colBorderUpLeft));
				Polyline(hdc, eBorder1, 3);
				if (disabled)
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emes->colDisabledBorderDownRight)));
				else
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emes->colBorderDownRight)));
				Polyline(hdc, eBorder2, 3);
				DeleteObject(SelectObject(hdc, hOldPen));

				EndPaint(hwnd, &ps);

				InvalidateRect(emes->hEdit, NULL, false);
			}
			break;
		case WM_SETFOCUS:
			{
				SetFocus(emes->hEdit);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				
				LONG editID = GetWindowLong(emes->hEdit, GWL_ID);
				if (wmId == editID  &&   wmEvent == ES_RETURN)
				{
					LONG wID;
					wID = GetWindowLong(hwnd, GWL_ID);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_RETURN), (LPARAM)hwnd);
					break;
				}
				if (wmId == editID  &&   wmEvent == ES_LOST_FOCUS)
				{
					LONG wID;
					wID = GetWindowLong(hwnd, GWL_ID);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_LOST_FOCUS), (LPARAM)hwnd);
					break;
				}
				if (wmId == editID  &&   wmEvent == ES_TAB)
				{
					HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, false);
					if (hNext != hwnd)
						SetFocus(hNext);
				}
				if (wmId == editID  &&   wmEvent == ES_TAB_SHIFT)
				{
					HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, true);
					if (hNext != hwnd)
						SetFocus(hNext);
				}
			}
			break;

		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

char * EMEditSimple::getText()
{
	if (!hEdit)
		return NULL;
	int l = GetWindowTextLength(hEdit);
	char * res = (char *)malloc(l+2);
	int ok = GetWindowText(hEdit, res, l+2);
	if (ok<1)
		return NULL;

	return res;
}

bool EMEditSimple::setText(char * text)
{
	if(!hEdit)
		return false;
	if (!text)
		return false;
	int ok = SetWindowText(hEdit, text);
	if (!ok)
		return false;

	return true;
}


