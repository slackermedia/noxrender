#define _WIN32_WINNT 0x0502
#include <windows.h>
#include "resource.h"
#include "../NOXUpdate/nox_update.h"
#include "../../Everything DLL/Everything DLL/RendererMainWindow.h"
#include "../../Everything DLL/Everything DLL/raytracer.h"

char * noxDir = NULL;
char * maxNames[] = {		"3ds max 9 32 bit", 
							"3ds max 9 64 bit", 
							"3ds max 2008 32 bit", 
							"3ds max 2008 64 bit", 
							"3ds max 2009 32 bit", 
							"3ds max 2009 64 bit", 
							"3ds max 2010 32 bit", 
							"3ds max 2010 64 bit", 
							"3ds max 2011 32 bit", 
							"3ds max 2011 64 bit",
							"3ds max 2012 32 bit", 
							"3ds max 2012 64 bit",
							"3ds max 2013 32 bit", 
							"3ds max 2013 64 bit",
							"3ds max 2014 64 bit" };

char * maxPluginFilenames[] = {		"max9 plugin\\32\\NOXplugin.dlr", 
									"max9 plugin\\64\\NOXplugin.dlr", 
									"max2008 plugin\\32\\NOXplugin.dlr", 
									"max2008 plugin\\64\\NOXplugin.dlr", 
									"max2009 plugin\\32\\NOXplugin.dlr", 
									"max2009 plugin\\64\\NOXplugin.dlr", 
									"max2010 plugin\\32\\NOXplugin.dlr", 
									"max2010 plugin\\64\\NOXplugin.dlr", 
									"max2010 plugin\\32\\NOXplugin.dlr", 
									"max2010 plugin\\64\\NOXplugin.dlr",
									"max2012 plugin\\32\\NOXplugin.dlr", 
									"max2012 plugin\\64\\NOXplugin.dlr",
									"max2013 plugin\\32\\NOXplugin.dlr", 
									"max2013 plugin\\64\\NOXplugin.dlr",
									"max2013 plugin\\64\\NOXplugin.dlr" };


HINSTANCE hInst;
HWND hMainPlugins;
HWND hPlInstall = 0;
HWND hPlExit = 0;
HWND hPlStatusText = 0;
HWND hCh_9_32 = 0;
HWND hCh_9_64 = 0;
HWND hCh_2008_32 = 0;
HWND hCh_2008_64 = 0;
HWND hCh_2009_32 = 0;
HWND hCh_2009_64 = 0;
HWND hCh_2010_32 = 0;
HWND hCh_2010_64 = 0;
HWND hCh_2011_32 = 0;
HWND hCh_2011_64 = 0;
HWND hCh_2012_32 = 0;
HWND hCh_2012_64 = 0;
HWND hCh_2013_32 = 0;
HWND hCh_2013_64 = 0;
HWND hCh_2014_64 = 0;
HWND hEdit_9_32 = 0;
HWND hEdit_9_64 = 0;
HWND hEdit_2008_32 = 0;
HWND hEdit_2008_64 = 0;
HWND hEdit_2009_32 = 0;
HWND hEdit_2009_64 = 0;
HWND hEdit_2010_32 = 0;
HWND hEdit_2010_64 = 0;
HWND hEdit_2011_32 = 0;
HWND hEdit_2011_64 = 0;
HWND hEdit_2012_32 = 0;
HWND hEdit_2012_64 = 0;
HWND hEdit_2013_32 = 0;
HWND hEdit_2013_64 = 0;
HWND hEdit_2014_64 = 0;
HWND hDirCh_9_32 = 0;
HWND hDirCh_9_64 = 0;
HWND hDirCh_2008_32 = 0;
HWND hDirCh_2008_64 = 0;
HWND hDirCh_2009_32 = 0;
HWND hDirCh_2009_64 = 0;
HWND hDirCh_2010_32 = 0;
HWND hDirCh_2010_64 = 0;
HWND hDirCh_2011_32 = 0;
HWND hDirCh_2011_64 = 0;
HWND hDirCh_2012_32 = 0;
HWND hDirCh_2012_64 = 0;
HWND hDirCh_2013_32 = 0;
HWND hDirCh_2013_64 = 0;
HWND hDirCh_2014_64 = 0;
HWND hStatus_9_32 = 0;
HWND hStatus_9_64 = 0;
HWND hStatus_2008_32 = 0;
HWND hStatus_2008_64 = 0;
HWND hStatus_2009_32 = 0;
HWND hStatus_2009_64 = 0;
HWND hStatus_2010_32 = 0;
HWND hStatus_2010_64 = 0;
HWND hStatus_2011_32 = 0;
HWND hStatus_2011_64 = 0;
HWND hStatus_2012_32 = 0;
HWND hStatus_2012_64 = 0;
HWND hStatus_2013_32 = 0;
HWND hStatus_2013_64 = 0;
HWND hStatus_2014_64 = 0;

bool isSystem64 = false;

extern bool m3ds9_32;
extern bool m3ds9_64;
extern bool m3ds2008_32;
extern bool m3ds2008_64;
extern bool m3ds2009_32;
extern bool m3ds2009_64;
extern bool m3ds2010_32;
extern bool m3ds2010_64;
extern bool m3ds2011_32;
extern bool m3ds2011_64;
extern bool m3ds2012_32;
extern bool m3ds2012_64;
extern bool m3ds2013_32;
extern bool m3ds2013_64;
extern bool m3ds2014_64;
extern char * dir_3dsmax_9_32;
extern char * dir_3dsmax_9_64;
extern char * dir_3dsmax_2008_32;
extern char * dir_3dsmax_2008_64;
extern char * dir_3dsmax_2009_32;
extern char * dir_3dsmax_2009_64;
extern char * dir_3dsmax_2010_32;
extern char * dir_3dsmax_2010_64;
extern char * dir_3dsmax_2011_32;
extern char * dir_3dsmax_2011_64;
extern char * dir_3dsmax_2012_32;
extern char * dir_3dsmax_2012_64;
extern char * dir_3dsmax_2013_32;
extern char * dir_3dsmax_2013_64;
extern char * dir_3dsmax_2014_64;



LRESULT CALLBACK NOXPluginsWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

//-------------------------------------------------------------------------------------------------------------

ATOM MyRegisterPluginsClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	HICON sIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= NOXPluginsWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= sIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "nox_max_plugins";
	wcex.hIconSm		= sIcon; 
	return RegisterClassEx(&wcex);
}

//-------------------------------------------------------------------------------------------------------------

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	MyRegisterPluginsClass(hInstance);

	noxDir = getRendererDirectory();
	setDir();

	SYSTEM_INFO sinfo;
	GetNativeSystemInfo(&sinfo);
	if (sinfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
	{
		isSystem64 = true;
	}
	else
		if (sinfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_INTEL)
		{
			isSystem64 = false;
		}
		else
		{
			MessageBox(hMainPlugins, "Unfortunately I can't determine your system version.", "", 0);
			return 0;
		}

	recognize3dsmaxVersions(isSystem64);

	hMainPlugins = CreateWindow("nox_max_plugins", "NOX 3ds Max plugins installer", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN ,
		  200,200,   600,435,   0, NULL, hInstance, NULL);

	if (hMainPlugins)
		ShowWindow(hMainPlugins, SW_SHOW);

	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	hCh_9_32    = CreateWindow("BUTTON", "3ds Max 9 32 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,10,   125,20,   hMainPlugins, (HMENU)201, hInstance, NULL);
	hCh_9_64    = CreateWindow("BUTTON", "3ds Max 9 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,30,   125,20,   hMainPlugins, (HMENU)202, hInstance, NULL);
	hCh_2008_32 = CreateWindow("BUTTON", "3ds Max 2008 32 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,50,   125,20,   hMainPlugins, (HMENU)203, hInstance, NULL);
	hCh_2008_64 = CreateWindow("BUTTON", "3ds Max 2008 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,70,   125,20,   hMainPlugins, (HMENU)204, hInstance, NULL);
	hCh_2009_32 = CreateWindow("BUTTON", "3ds Max 2009 32 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,90,   125,20,   hMainPlugins, (HMENU)205, hInstance, NULL);
	hCh_2009_64 = CreateWindow("BUTTON", "3ds Max 2009 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,110,  125,20,   hMainPlugins, (HMENU)206, hInstance, NULL);
	hCh_2010_32 = CreateWindow("BUTTON", "3ds Max 2010 32 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,130,  125,20,   hMainPlugins, (HMENU)207, hInstance, NULL);
	hCh_2010_64 = CreateWindow("BUTTON", "3ds Max 2010 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,150,  125,20,   hMainPlugins, (HMENU)208, hInstance, NULL);
	hCh_2011_32 = CreateWindow("BUTTON", "3ds Max 2011 32 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,170,  125,20,   hMainPlugins, (HMENU)209, hInstance, NULL);
	hCh_2011_64 = CreateWindow("BUTTON", "3ds Max 2011 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,190,  125,20,   hMainPlugins, (HMENU)210, hInstance, NULL);
	hCh_2012_32 = CreateWindow("BUTTON", "3ds Max 2012 32 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,210,  125,20,   hMainPlugins, (HMENU)211, hInstance, NULL);
	hCh_2012_64 = CreateWindow("BUTTON", "3ds Max 2012 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,230,  125,20,   hMainPlugins, (HMENU)212, hInstance, NULL);
	hCh_2013_32 = CreateWindow("BUTTON", "3ds Max 2013 32 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,250,  125,20,   hMainPlugins, (HMENU)213, hInstance, NULL);
	hCh_2013_64 = CreateWindow("BUTTON", "3ds Max 2013 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,270,  125,20,   hMainPlugins, (HMENU)214, hInstance, NULL);
	hCh_2014_64 = CreateWindow("BUTTON", "3ds Max 2014 64 bit", WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		  10,290,  125,20,   hMainPlugins, (HMENU)215, hInstance, NULL);
	SendMessage(hCh_9_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_9_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2008_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2008_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2009_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2009_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2010_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2010_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2011_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2011_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2012_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2012_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2013_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2013_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hCh_2014_64, WM_SETFONT, (WPARAM)hFont, TRUE);


	hEdit_9_32 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 9 32 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,10,  315,20, hMainPlugins, (HMENU)261, hInstance, 0);
	hEdit_9_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 9 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,30,  315,20, hMainPlugins, (HMENU)262, hInstance, 0);
	hEdit_2008_32 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2008 32 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,50,  315,20, hMainPlugins, (HMENU)263, hInstance, 0);
	hEdit_2008_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2008 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,70,  315,20, hMainPlugins, (HMENU)264, hInstance, 0);
	hEdit_2009_32 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2009 32 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,90,  315,20, hMainPlugins, (HMENU)265, hInstance, 0);
	hEdit_2009_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2009 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,110, 315,20, hMainPlugins, (HMENU)266, hInstance, 0);
	hEdit_2010_32 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2010 32 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,130, 315,20, hMainPlugins, (HMENU)267, hInstance, 0);
	hEdit_2010_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2010 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,150, 315,20, hMainPlugins, (HMENU)268, hInstance, 0);
	hEdit_2011_32 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2011 32 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,170, 315,20, hMainPlugins, (HMENU)269, hInstance, 0);
	hEdit_2011_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2011 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,190, 315,20, hMainPlugins, (HMENU)270, hInstance, 0);
	hEdit_2012_32 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2012 32 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,210, 315,20, hMainPlugins, (HMENU)271, hInstance, 0);
	hEdit_2012_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2012 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,230, 315,20, hMainPlugins, (HMENU)272, hInstance, 0);
	hEdit_2013_32 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2013 32 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,250, 315,20, hMainPlugins, (HMENU)273, hInstance, 0);
	hEdit_2013_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2013 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,270, 315,20, hMainPlugins, (HMENU)274, hInstance, 0);
	hEdit_2014_64 = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "3ds Max 2014 64 bit path...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
				140,290, 315,20, hMainPlugins, (HMENU)275, hInstance, 0);

	SendMessage(hEdit_9_32,    WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_9_64,    WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2008_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2008_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2009_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2009_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2010_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2010_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2011_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2011_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2012_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2012_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2013_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2013_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hEdit_2014_64, WM_SETFONT, (WPARAM)hFont, TRUE);

	hDirCh_9_32 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,11,   30,18,   hMainPlugins, (HMENU)221, hInstance, NULL);
	hDirCh_9_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,31,   30,18,   hMainPlugins, (HMENU)222, hInstance, NULL);
	hDirCh_2008_32 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,51,   30,18,   hMainPlugins, (HMENU)223, hInstance, NULL);
	hDirCh_2008_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,71,   30,18,   hMainPlugins, (HMENU)224, hInstance, NULL);
	hDirCh_2009_32 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,91,   30,18,   hMainPlugins, (HMENU)225, hInstance, NULL);
	hDirCh_2009_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,111,   30,18,  hMainPlugins, (HMENU)226, hInstance, NULL);
	hDirCh_2010_32 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,131,   30,18,  hMainPlugins, (HMENU)227, hInstance, NULL);
	hDirCh_2010_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,151,   30,18,  hMainPlugins, (HMENU)228, hInstance, NULL);
	hDirCh_2011_32 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,171,   30,18,  hMainPlugins, (HMENU)229, hInstance, NULL);
	hDirCh_2011_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,191,   30,18,  hMainPlugins, (HMENU)230, hInstance, NULL);
	hDirCh_2012_32 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,211,   30,18,  hMainPlugins, (HMENU)231, hInstance, NULL);
	hDirCh_2012_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,231,   30,18,  hMainPlugins, (HMENU)232, hInstance, NULL);
	hDirCh_2013_32 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,251,   30,18,  hMainPlugins, (HMENU)233, hInstance, NULL);
	hDirCh_2013_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,271,   30,18,  hMainPlugins, (HMENU)234, hInstance, NULL);
	hDirCh_2014_64 = CreateWindow("BUTTON", "...", WS_CHILD | WS_VISIBLE,
		  460,291,   30,18,  hMainPlugins, (HMENU)235, hInstance, NULL);

	SendMessage(hDirCh_9_32,    WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_9_64,    WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2008_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2008_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2009_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2009_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2010_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2010_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2011_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2011_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2012_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2012_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2013_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2013_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDirCh_2014_64, WM_SETFONT, (WPARAM)hFont, TRUE);

	hStatus_9_32    = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 13,    185,20,   hMainPlugins, (HMENU)254, hInstance, NULL);
	hStatus_9_64    = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 33,    185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hStatus_2008_32 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 53,    185,20,   hMainPlugins, (HMENU)254, hInstance, NULL);
	hStatus_2008_64 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 73,    185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hStatus_2009_32 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 93,    185,20,   hMainPlugins, (HMENU)254, hInstance, NULL);
	hStatus_2009_64 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 113,   185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hStatus_2010_32 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 133,   185,20,   hMainPlugins, (HMENU)254, hInstance, NULL);
	hStatus_2010_64 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 153,   185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hStatus_2011_32 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 173,   185,20,   hMainPlugins, (HMENU)254, hInstance, NULL);
	hStatus_2011_64 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 193,   185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hStatus_2012_32 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 213,   185,20,   hMainPlugins, (HMENU)254, hInstance, NULL);
	hStatus_2012_64 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 233,   185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hStatus_2013_32 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 253,   185,20,   hMainPlugins, (HMENU)254, hInstance, NULL);
	hStatus_2013_64 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 273,   185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hStatus_2014_64 = CreateWindow("static", "status", WS_CHILD | WS_VISIBLE,
		  500, 293,   185,20,   hMainPlugins, (HMENU)253, hInstance, NULL);

	SendMessage(hStatus_9_32,    WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_9_64,    WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2008_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2008_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2009_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2009_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2010_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2010_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2011_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2011_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2012_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2012_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2013_32, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2013_64, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hStatus_2014_64, WM_SETFONT, (WPARAM)hFont, TRUE);


	hPlStatusText = CreateWindow("static", "Choose versions and set paths for plugin...", WS_CHILD | WS_VISIBLE,
		  10,330,   580,20,   hMainPlugins, (HMENU)253, hInstance, NULL);
	hPlInstall = CreateWindow("BUTTON", "Install", WS_CHILD | WS_VISIBLE,
		  180,360,   100,30,   hMainPlugins, (HMENU)251, hInstance, NULL);
	hPlExit = CreateWindow("BUTTON", "Exit", WS_CHILD | WS_VISIBLE,
		  320,360,   100,30,   hMainPlugins, (HMENU)252, hInstance, NULL);
	SendMessage(hPlInstall, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hPlExit, WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hPlStatusText, WM_SETFONT, (WPARAM)hFont, TRUE);

	findAndFill3dsmaxDirs(isSystem64);

	if (!noxDir)
	{
		EnableWindow(hPlInstall, false);
		EnableWindow(hPlStatusText, false);
		EnableWindow(hCh_9_32, false);
		EnableWindow(hCh_9_64, false);
		EnableWindow(hCh_2008_32, false);
		EnableWindow(hCh_2008_64, false);
		EnableWindow(hCh_2009_32, false);
		EnableWindow(hCh_2009_64, false);
		EnableWindow(hCh_2010_32, false);
		EnableWindow(hCh_2010_64, false);
		EnableWindow(hCh_2011_32, false);
		EnableWindow(hCh_2011_64, false);
		EnableWindow(hCh_2012_32, false);
		EnableWindow(hCh_2012_64, false);
		EnableWindow(hCh_2013_32, false);
		EnableWindow(hCh_2013_64, false);
		EnableWindow(hCh_2014_64, false);
		EnableWindow(hEdit_9_32, false);
		EnableWindow(hEdit_9_64, false);
		EnableWindow(hEdit_2008_32, false);
		EnableWindow(hEdit_2008_64, false);
		EnableWindow(hEdit_2009_32, false);
		EnableWindow(hEdit_2009_64, false);
		EnableWindow(hEdit_2010_32, false);
		EnableWindow(hEdit_2010_64, false);
		EnableWindow(hEdit_2011_32, false);
		EnableWindow(hEdit_2011_64, false);
		EnableWindow(hEdit_2012_32, false);
		EnableWindow(hEdit_2012_64, false);
		EnableWindow(hEdit_2013_32, false);
		EnableWindow(hEdit_2013_64, false);
		EnableWindow(hEdit_2014_64, false);
		EnableWindow(hDirCh_9_32, false);
		EnableWindow(hDirCh_9_64, false);
		EnableWindow(hDirCh_2008_32, false);
		EnableWindow(hDirCh_2008_64, false);
		EnableWindow(hDirCh_2009_32, false);
		EnableWindow(hDirCh_2009_64, false);
		EnableWindow(hDirCh_2010_32, false);
		EnableWindow(hDirCh_2010_64, false);
		EnableWindow(hDirCh_2011_32, false);
		EnableWindow(hDirCh_2011_64, false);
		EnableWindow(hDirCh_2012_32, false);
		EnableWindow(hDirCh_2012_64, false);
		EnableWindow(hDirCh_2013_32, false);
		EnableWindow(hDirCh_2013_64, false);
		EnableWindow(hDirCh_2014_64, false);
		EnableWindow(hStatus_9_32, false);
		EnableWindow(hStatus_9_64, false);
		EnableWindow(hStatus_2008_32, false);
		EnableWindow(hStatus_2008_64, false);
		EnableWindow(hStatus_2009_32, false);
		EnableWindow(hStatus_2009_64, false);
		EnableWindow(hStatus_2010_32, false);
		EnableWindow(hStatus_2010_64, false);
		EnableWindow(hStatus_2011_32, false);
		EnableWindow(hStatus_2011_64, false);
		EnableWindow(hStatus_2012_32, false);
		EnableWindow(hStatus_2012_64, false);
		EnableWindow(hStatus_2013_32, false);
		EnableWindow(hStatus_2013_64, false);
		EnableWindow(hStatus_2014_64, false);

		MessageBox(hMainPlugins, "Error!\n\nCan't determine NOX directory.\nRun VBS file from NOX directory and then install plugins again.", "Error", 0);
	}


	// Main message loop:
	MSG msg;
	HACCEL hAccelTable;
	hAccelTable = 0;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

//-------------------------------------------------------------------------------------------------------------

void findAndFill3dsmaxDirs(bool is64system)
{
	recognize3dsmaxVersions(is64system);

	char temp[2048];
	char * curStr;
	HWND curHwnd;
	HWND curCBhwnd;
	HWND curChDirBtn;
	HWND curStatus;
	bool curCBon;

	curStr = dir_3dsmax_9_32;
	curHwnd = hEdit_9_32;
	curCBhwnd = hCh_9_32;
	curCBon = m3ds9_32;
	curChDirBtn = hDirCh_9_32;
	curStatus = hStatus_9_32;


	for (int j=0; j<NUM_PLUGINS_ALL; j++)
	{
		switch (j)
		{
			case 0: 
				curStr = dir_3dsmax_9_32;
				curHwnd = hEdit_9_32;
				curCBon = m3ds9_32;
				curCBhwnd = hCh_9_32;
				curChDirBtn = hDirCh_9_32;
				curStatus = hStatus_9_32;
				break;
			case 1: 
				curStr = dir_3dsmax_9_64;
				curHwnd = hEdit_9_64;
				curCBon = m3ds9_64;
				curCBhwnd = hCh_9_64;
				curChDirBtn = hDirCh_9_64;
				curStatus = hStatus_9_64;
				break;
			case 2: 
				curStr = dir_3dsmax_2008_32;
				curHwnd = hEdit_2008_32;
				curCBon = m3ds2008_32;
				curCBhwnd = hCh_2008_32;
				curChDirBtn = hDirCh_2008_32;
				curStatus = hStatus_2008_32;
				break;
			case 3: 
				curStr = dir_3dsmax_2008_64;
				curHwnd = hEdit_2008_64;
				curCBon = m3ds2008_64;
				curCBhwnd = hCh_2008_64;
				curChDirBtn = hDirCh_2008_64;
				curStatus = hStatus_2008_64;
				break;
			case 4: 
				curStr = dir_3dsmax_2009_32;
				curHwnd = hEdit_2009_32;
				curCBon = m3ds2009_32;
				curCBhwnd = hCh_2009_32;
				curChDirBtn = hDirCh_2009_32;
				curStatus = hStatus_2009_32;
				break;
			case 5: 
				curStr = dir_3dsmax_2009_64;
				curHwnd = hEdit_2009_64;
				curCBon = m3ds2009_64;
				curCBhwnd = hCh_2009_64;
				curChDirBtn = hDirCh_2009_64;
				curStatus = hStatus_2009_64;
				break;
			case 6: 
				curStr = dir_3dsmax_2010_32;
				curHwnd = hEdit_2010_32;
				curCBon = m3ds2010_32;
				curCBhwnd = hCh_2010_32;
				curChDirBtn = hDirCh_2010_32;
				curStatus = hStatus_2010_32;
				break;
			case 7: 
				curStr = dir_3dsmax_2010_64;
				curHwnd = hEdit_2010_64;
				curCBon = m3ds2010_64;
				curCBhwnd = hCh_2010_64;
				curChDirBtn = hDirCh_2010_64;
				curStatus = hStatus_2010_64;
				break;
			case 8: 
				curStr = dir_3dsmax_2011_32;
				curHwnd = hEdit_2011_32;
				curCBon = m3ds2011_32;
				curCBhwnd = hCh_2011_32;
				curChDirBtn = hDirCh_2011_32;
				curStatus = hStatus_2011_32;
				break;
			case 9: 
				curStr = dir_3dsmax_2011_64;
				curHwnd = hEdit_2011_64;
				curCBon = m3ds2011_64;
				curCBhwnd = hCh_2011_64;
				curChDirBtn = hDirCh_2011_64;
				curStatus = hStatus_2011_64;
				break;
			case 10: 
				curStr = dir_3dsmax_2012_32;
				curHwnd = hEdit_2012_32;
				curCBon = m3ds2012_32;
				curCBhwnd = hCh_2012_32;
				curChDirBtn = hDirCh_2012_32;
				curStatus = hStatus_2012_32;
				break;
			case 11: 
				curStr = dir_3dsmax_2012_64;
				curHwnd = hEdit_2012_64;
				curCBon = m3ds2012_64;
				curCBhwnd = hCh_2012_64;
				curChDirBtn = hDirCh_2012_64;
				curStatus = hStatus_2012_64;
				break;
			case 12: 
				curStr = dir_3dsmax_2013_32;
				curHwnd = hEdit_2013_32;
				curCBon = m3ds2013_32;
				curCBhwnd = hCh_2013_32;
				curChDirBtn = hDirCh_2013_32;
				curStatus = hStatus_2013_32;
				break;
			case 13: 
				curStr = dir_3dsmax_2013_64;
				curHwnd = hEdit_2013_64;
				curCBon = m3ds2013_64;
				curCBhwnd = hCh_2013_64;
				curChDirBtn = hDirCh_2013_64;
				curStatus = hStatus_2013_64;
				break;
			case 14: 
				curStr = dir_3dsmax_2014_64;
				curHwnd = hEdit_2014_64;
				curCBon = m3ds2014_64;
				curCBhwnd = hCh_2014_64;
				curChDirBtn = hDirCh_2014_64;
				curStatus = hStatus_2014_64;
				break;

		}
		
		SendMessage(curCBhwnd, BM_SETCHECK, (curCBon ? BST_CHECKED : BST_UNCHECKED), 0);
		EnableWindow(curHwnd, curCBon);
		EnableWindow(curChDirBtn, curCBon);

		if (curStr)
		{
			int ll = (int)strlen(curStr);
			for (int i=ll-1; i>=0; i--)
			{
				if (curStr[i] == '\\'  ||  curStr[i]=='/')
					curStr[i] = 0;
				else
					break;
			}
			sprintf_s(temp, 2048, "%s\\plugins", curStr);
			SetWindowText(curHwnd, temp);
			SetWindowText(curStatus, "install");

			bool dirOK = doesDirExist(temp);
			if (!dirOK)
			{
				SendMessage(curCBhwnd, BM_SETCHECK, BST_UNCHECKED, 0);
				EnableWindow(curHwnd, false);
				SetWindowText(curStatus, "ignored");
			}
		}
		else
		{
			SetWindowText(curHwnd, "");
			SetWindowText(curStatus, "ignored");
		}
	}
}

//-------------------------------------------------------------------------------------------------------------

bool copyFile(char * dst, char * src)
{
	if (!dst  ||  !src)
		return false;

	if (!doesFileExist(src))
		return false;

	char * dir = getDirectoryFromFilePath(dst);
	if (!dir)
		return false;
	if (!doesDirExist(dir))
	{
		free(dir);
		return false;
	}

	BOOL res = CopyFile(src, dst, false);
	return (res==TRUE);
}

//-------------------------------------------------------------------------------------------------------------

void installPlugins()
{
	char * dirs[NUM_PLUGINS_ALL];
	bool installCh[NUM_PLUGINS_ALL];
	HWND hChb = hCh_9_32;
	HWND hDir = hEdit_9_32;
	HWND hStats[NUM_PLUGINS_ALL];

	for (int k=0; k<NUM_PLUGINS_ALL; k++)
	{
		switch (k)
		{
			case 0:
				hChb = hCh_9_32;
				hDir = hEdit_9_32;
				hStats[k] = hStatus_9_32;
				break;
			case 1:
				hChb = hCh_9_64;
				hDir = hEdit_9_64;
				hStats[k] = hStatus_9_64;
				break;
			case 2:
				hChb = hCh_2008_32;
				hDir = hEdit_2008_32;
				hStats[k] = hStatus_2008_32;
				break;
			case 3:
				hChb = hCh_2008_64;
				hDir = hEdit_2008_64;
				hStats[k] = hStatus_2008_64;
				break;
			case 4:
				hChb = hCh_2009_32;
				hDir = hEdit_2009_32;
				hStats[k] = hStatus_2009_32;
				break;
			case 5:
				hChb = hCh_2009_64;
				hDir = hEdit_2009_64;
				hStats[k] = hStatus_2009_64;
				break;
			case 6:
				hChb = hCh_2010_32;
				hDir = hEdit_2010_32;
				hStats[k] = hStatus_2010_32;
				break;
			case 7:
				hChb = hCh_2010_64;
				hDir = hEdit_2010_64;
				hStats[k] = hStatus_2010_64;
				break;
			case 8:
				hChb = hCh_2011_32;
				hDir = hEdit_2011_32;
				hStats[k] = hStatus_2011_32;
				break;
			case 9:
				hChb = hCh_2011_64;
				hDir = hEdit_2011_64;
				hStats[k] = hStatus_2011_64;
				break;
			case 10:
				hChb = hCh_2012_32;
				hDir = hEdit_2012_32;
				hStats[k] = hStatus_2012_32;
				break;
			case 11:
				hChb = hCh_2012_64;
				hDir = hEdit_2012_64;
				hStats[k] = hStatus_2012_64;
				break;
			case 12:
				hChb = hCh_2013_32;
				hDir = hEdit_2013_32;
				hStats[k] = hStatus_2013_32;
				break;
			case 13:
				hChb = hCh_2013_64;
				hDir = hEdit_2013_64;
				hStats[k] = hStatus_2013_64;
				break;
			case 14:
				hChb = hCh_2014_64;
				hDir = hEdit_2014_64;
				hStats[k] = hStatus_2014_64;
				break;
		}
		installCh[k] =  (BST_CHECKED == SendMessage(hChb,    BM_GETCHECK, 0, 0) );
		char sdir[2048];
		GetWindowText(hDir, sdir, 2048);
		int ll = (int)strlen(sdir)+1;
		dirs[k] = (char*)malloc(ll);
		if (ll == 1)
			dirs[k][0] = 0;
		else
		{
			sprintf_s(dirs[k],ll, "%s", sdir);
			for (int i=ll-2; i>=2; i--)
			{
				if (dirs[k][i]=='\\'  ||  dirs[k][i]=='/')
					dirs[k][i]=0;
				else
					break;
			}
		}
	}

	for (int k=0; k<NUM_PLUGINS_ALL; k++)
	{
		if (!installCh[k])
			continue;
		SetWindowText(hStats[k], "waiting...");
	}

	bool wereErrors = false;

	for (int k=0; k<NUM_PLUGINS_ALL; k++)
	{
		if (!installCh[k])
			continue;

		char installingText[2048];
		sprintf_s(installingText, 2048, "Installing plugin for %s ...", maxNames[k]);
		SetWindowText(hPlStatusText, installingText);
		SetWindowText(hStats[k], "Installing...");

		char src[2048];
		char dst[2048];
		char * fname = getFilenameFromFilePath(maxPluginFilenames[k]);
		if (!fname)
		{
			char errbuff[2048];
			sprintf_s(errbuff, 2048, "Error!\n\nCan't determine filename from subdir:\n%s", maxPluginFilenames[k]);
			MessageBox(hMainPlugins, errbuff, "Error", 0);
			wereErrors = true;
			SetWindowText(hStats[k], "FAILED");
			continue;
		}
		sprintf_s(src, 2048, "%s\\%s", noxDir, maxPluginFilenames[k]);
		sprintf_s(dst, 2048, "%s\\%s", dirs[k], fname);

		if (!doesFileExist(src))
		{
			char errbuff[2048];
			sprintf_s(errbuff, 2048, "Error!\n\nSource file does not exist:\n%s", src);
			MessageBox(hMainPlugins, errbuff, "Error", 0);
			SetWindowText(hStats[k], "FAILED");
			wereErrors = true;
			continue;
		}

		bool doneOK = false;
		bool ignored = false;
		do
		{
			bool dstDirOK = doesDirExist(dirs[k]);
			if (dstDirOK)
				doneOK = true;
			else
			{
				char errbuff[2048];
				sprintf_s(errbuff, 2048, "Error!\n\nDestination directory does not exist:\n%s", dirs[k]);
				int mbRes = MessageBox(hMainPlugins, errbuff, "Error",MB_ABORTRETRYIGNORE);
				switch (mbRes)
				{
					case IDRETRY:
						break;
					case IDIGNORE:
						doneOK = true;
						wereErrors = true;
						SetWindowText(hStats[k], "FAILED");
						ignored = true;
						break;
					case IDABORT:
						{
							SetWindowText(hStats[k], "ABORTED");
							SetWindowText(hPlStatusText, "Installation aborted!");
							for (int k=0; k<10; k++)
							{
								if (dirs[k])
									free(dirs[k]);
								dirs[k] = NULL;
							}
						}
						return;
				}
			}
		}
		while (!doneOK);
		if (ignored)
			continue;

		doneOK = false;
		do
		{
			bool copiedOK = copyFile(dst, src);
			if (copiedOK)
				doneOK = true;
			else
			{
				char errbuff[2048];
				sprintf_s(errbuff, 2048, "Error!\n\nCopying file failed:\n%s\n\nProbably 3d studio is running.", dirs[k]);
				int mbRes = MessageBox(hMainPlugins, errbuff, "Error",MB_ABORTRETRYIGNORE);
				switch (mbRes)
				{
					case IDRETRY:
						break;
					case IDIGNORE:
						doneOK = true;
						SetWindowText(hStats[k], "FAILED");
						ignored = true;
						wereErrors = true;
						break;
					case IDABORT:
						{
							SetWindowText(hStats[k], "ABORTED");
							SetWindowText(hPlStatusText, "Installation aborted!");
							for (int k=0; k<10; k++)
							{
								if (dirs[k])
									free(dirs[k]);
								dirs[k] = NULL;
							}
						}
						return;
				}
			}
		}
		while (!doneOK);

		if (!ignored)
			SetWindowText(hStats[k], "INSTALLED");
	}

	if (wereErrors)
		SetWindowText(hPlStatusText, "Installation finished but some errors happened.");
	else
		SetWindowText(hPlStatusText, "Installation finished successfully.");

	// free dir names
	for (int k=0; k<NUM_PLUGINS_ALL; k++)
	{
		if (dirs[k])
			free(dirs[k]);
		dirs[k] = NULL;
	}
}

//-------------------------------------------------------------------------------------------------------------

