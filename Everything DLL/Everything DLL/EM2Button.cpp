#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include "EM2Controls.h"
#include "log.h"

char * copyString(char * src, unsigned int addSpace);
extern HMODULE hDllModule;

//-------------------------------------------------------------------------------------------------

EM2Button::EM2Button(HWND hWnd) 
{
	colText = NGCOL_LIGHT_GRAY;
    colTextMouseOver = NGCOL_YELLOW;
    colTextSelected = NGCOL_YELLOW;
    colTextDisabled = RGB(60,60,60);
	colTextDisabled = NGCOL_TEXT_DISABLED;
	colTextShadow = RGB(25,25,25);

    colBackGnd = RGB(64,64,64);
    colBackGndMouseOver = RGB(112,112,112);
    colBackGndSelected = RGB(112,112,112);
	colBackGndDisabled = RGB(64,64,64);

	colBorderLeft = RGB(158,158,158);
	colBorderTop = RGB(255,255,255);
	colBorderRight = RGB(2,2,2);
	colBorderBottom = RGB(0,0,0);

	colBorderLeftMouseOver = RGB(158,158,158);
	colBorderTopMouseOver = RGB(255,255,255);
	colBorderRightMouseOver = RGB(2,2,2);
	colBorderBottomMouseOver = RGB(0,0,0);

	colBorderLeftSelected = RGB(2,2,2);
	colBorderTopSelected = RGB(0,0,0);
	colBorderRightSelected = RGB(158,158,158);
	colBorderBottomSelected = RGB(255,255,255);

	colBorderLeftDisabled = RGB(96,96,96);
	colBorderTopDisabled = RGB(150,150,150);
	colBorderRightDisabled = RGB(10,10,10);
	colBorderBottomDisabled = RGB(0,0,0);

	alphaBg = 0;
	alphaBgMouseOver = 24;
	alphaBgSelected = 24;
	alphaBgDisabled = 0;

	alphaBorder = 64;
	alphaBorderMouseOver = 64;
	alphaBorderSelected = 64;
	alphaBorderDisabled = 64;

	drawBorder = true;

	mouseClicked = false;
	mouseOver = false;
	selected = false;
	#ifdef GUI2_DRAW_TEXT_SHADOWS
		drawTextShadow = true;
	#else
		drawTextShadow = false;
	#endif

	bgImage = 0;
	bgShiftX = bgShiftY = 0;

	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfont = true;
	hwnd = hWnd;

	twoState = false;
	ts_allow_unclick = true;
	ellipsis = false;
	useCursors = true;



	captionSize = GetWindowTextLength(hwnd);
	caption = (char *) malloc(captionSize+1);
	if (caption)
	{
		GetWindowText(hwnd, caption, captionSize+1);
		caption[captionSize] = 0;
	}
	else
	{
		captionSize = 0;
	}	
}

//-------------------------------------------------------------------------------------------------

EM2Button::~EM2Button() 
{
	if (caption)
		free(caption);
	if  (hFont  &&  autodelfont)
		DeleteObject(hFont);
	hFont = 0;
	caption = NULL;
}

//-------------------------------------------------------------------------------------------------

void InitEM2Button()
{
    WNDCLASSEX wc;
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2Button";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2ButtonProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2Button *);
    wc.hIconSm        = 0;
    ATOM a = RegisterClassEx(&wc);
}

//-------------------------------------------------------------------------------------------------

EM2Button * GetEM2ButtonInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2Button * emb = (EM2Button *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2Button * emb = (EM2Button *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emb;
}

//-------------------------------------------------------------------------------------------------

void SetEM2ButtonInstance(HWND hwnd, EM2Button *emb)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emb);
	#endif
}

//-------------------------------------------------------------------------------------------------

LRESULT CALLBACK EM2ButtonProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2Button * emb = GetEM2ButtonInstance(hwnd);

    switch(msg)
    {
		case WM_CREATE:
			{
				EM2Button * emb1 = new EM2Button(hwnd);
				SetEM2ButtonInstance(hwnd, emb1);			
			}
			break;
		case WM_DESTROY:
			{
				EM2Button * emb1;
				emb1 = GetEM2ButtonInstance(hwnd);
				delete emb1;
				SetEM2ButtonInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
			{
				if (!emb) 
					break;
				RECT rect;
				PAINTSTRUCT ps;
				SIZE sz;
				HANDLE hOldFont;

				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) != 0);

				HDC ohdc = BeginPaint(hwnd, &ps);

				GetClientRect(hwnd, &rect);

				HDC hdc = CreateCompatibleDC(ohdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(hdc, backBMP);

				if (emb->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emb->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emb->bgShiftX, emb->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emb->colBackGnd));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emb->colBackGnd));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, TRANSPARENT);
				}

				COLORREF bgc, bcl, bcr, bct, bcb;
				unsigned char abg, abd;
				if (disabled)
				{
					bgc = emb->colBackGndDisabled;
					bcl = emb->colBorderLeftDisabled;
					bcr = emb->colBorderRightDisabled;
					bct = emb->colBorderTopDisabled;
					bcb = emb->colBorderBottomDisabled;
					abg = emb->alphaBgDisabled;
					abd = emb->alphaBorderDisabled;
				}
				else
					if (emb->mouseClicked  ||  emb->selected)
					{
						bgc = emb->colBackGndSelected;
						bcl = emb->colBorderLeftSelected;
						bcr = emb->colBorderRightSelected;
						bct = emb->colBorderTopSelected;
						bcb = emb->colBorderBottomSelected;
						abg = emb->alphaBgSelected;
						abd = emb->alphaBorderSelected;
					}
					else
						if (emb->mouseOver)
						{
							bgc = emb->colBackGndMouseOver;
							bcl = emb->colBorderLeftMouseOver;
							bcr = emb->colBorderRightMouseOver;
							bct = emb->colBorderTopMouseOver;
							bcb = emb->colBorderBottomMouseOver;
							abg = emb->alphaBgMouseOver;
							abd = emb->alphaBorderMouseOver;
						}
						else
						{
							bgc = emb->colBackGnd;
							bcl = emb->colBorderLeft;
							bcr = emb->colBorderRight;
							bct = emb->colBorderTop;
							bcb = emb->colBorderBottom;
							abg = emb->alphaBg;
							abd = emb->alphaBorder;
						}

				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);
						Gdiplus::SolidBrush brush1(Gdiplus::Color(abg, GetRValue(bgc), GetGValue(bgc), GetBValue(bgc)));
						Gdiplus::Pen bpenl(Gdiplus::Color(abd, GetRValue(bcl), GetGValue(bcl), GetBValue(bcl)));
						Gdiplus::Pen bpenr(Gdiplus::Color(abd, GetRValue(bcr), GetGValue(bcr), GetBValue(bcr)));
						Gdiplus::Pen bpent(Gdiplus::Color(abd, GetRValue(bct), GetGValue(bct), GetBValue(bct)));
						Gdiplus::Pen bpenb(Gdiplus::Color(abd, GetRValue(bcb), GetGValue(bcb), GetBValue(bcb)));

						if (emb->drawBorder)
							graphics.FillRectangle(&brush1, rect.left+1, rect.top+1, rect.right-2, rect.bottom-2);
						else
							graphics.FillRectangle(&brush1, rect.left, rect.top, rect.right, rect.bottom);

						if (emb->drawBorder)
						{
							// drawline draws including ending point
							graphics.DrawLine(&bpenl, Gdiplus::Point(0,0), Gdiplus::Point(0,rect.bottom-1));
							graphics.DrawLine(&bpenr, Gdiplus::Point(rect.right-1,0), Gdiplus::Point(rect.right-1,rect.bottom-1));
							graphics.DrawLine(&bpent, Gdiplus::Point(0,0), Gdiplus::Point(rect.right-1,0));
							graphics.DrawLine(&bpenb, Gdiplus::Point(0,rect.bottom-1), Gdiplus::Point(rect.right-1,rect.bottom-1));
						}
					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else

					HPEN hPenL = CreatePen(PS_SOLID, 1, getMixedColor(NGCOL_BG_MEDIUM, abd, bcl));
					HPEN hPenR = CreatePen(PS_SOLID, 1, getMixedColor(NGCOL_BG_MEDIUM, abd, bcr));
					HPEN hPenT = CreatePen(PS_SOLID, 1, getMixedColor(NGCOL_BG_MEDIUM, abd, bct));
					HPEN hPenB = CreatePen(PS_SOLID, 1, getMixedColor(NGCOL_BG_MEDIUM, abd, bcb));

					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(getMixedColor(NGCOL_BG_MEDIUM, abg, bgc)));
					HPEN hOldPen = (HPEN)SelectObject(hdc, GetStockObject(NULL_PEN));

					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);

					DeleteObject(SelectObject(hdc, hPenL));
					MoveToEx(hdc, 0,0, NULL);
					LineTo(hdc, 0, rect.bottom);
					DeleteObject(SelectObject(hdc, hPenR));
					MoveToEx(hdc, rect.right-1, 0, NULL);
					LineTo(hdc, rect.right-1, rect.bottom);
					DeleteObject(SelectObject(hdc, hPenT));
					MoveToEx(hdc, 0,0, NULL);
					LineTo(hdc, rect.right, 0);
					DeleteObject(SelectObject(hdc, hPenB));
					MoveToEx(hdc, 0,rect.bottom-1, NULL);
					LineTo(hdc, rect.right, rect.bottom-1);

					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen));
				#endif


				COLORREF txtcol = disabled ? emb->colTextDisabled: (emb->mouseOver ? emb->colTextMouseOver : (emb->selected ? emb->colTextSelected : emb->colText));

				GetClientRect(hwnd, &rect);
	
				hOldFont = SelectObject(hdc, emb->getFont());

				char * captionCopy = copyString(emb->caption, 5);
				if (emb->ellipsis)
					cutTextToFitWidthAddEllipsis(hdc, captionCopy, rect.right-rect.left-8);
				int ll = captionCopy ? (int)strlen(captionCopy) : 0;

				GetTextExtentPoint32(hdc, captionCopy, ll, &sz);
				int posx = (rect.right-rect.left-sz.cx)/2;
				int posy = (rect.bottom-rect.top-sz.cy)/2;

				SetBkColor(hdc, NGCOL_TEXT_BACKGROUND);
				if (emb->drawTextShadow)
				{
					SetTextColor(hdc, emb->colTextShadow);
					ExtTextOut(hdc, posx+1, posy, ETO_CLIPPED, &rect, captionCopy, ll, 0);
					ExtTextOut(hdc, posx-1, posy, ETO_CLIPPED, &rect, captionCopy, ll, 0);
					ExtTextOut(hdc, posx, posy+1, ETO_CLIPPED, &rect, captionCopy, ll, 0);
					ExtTextOut(hdc, posx, posy-1, ETO_CLIPPED, &rect, captionCopy, ll, 0);
				}

				SetTextColor(hdc, txtcol);
				ExtTextOut(hdc, posx, posy, ETO_CLIPPED, &rect, captionCopy, ll, 0);

				if (captionCopy)
					free(captionCopy);
				captionCopy = NULL;

				SelectObject(hdc, hOldFont);

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY);

				SelectObject(hdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(hdc); 

				EndPaint(hwnd, &ps);

			}
			break;
		case WM_MOUSEMOVE:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				bool nowMouseOver = (PtInRect(&rect, pt)==TRUE);

				if (!GetCapture())
					SetCapture(hwnd);

				if (nowMouseOver!=emb->mouseOver)
				{
					if (emb->useCursors)
						SetCursor(LoadCursor (NULL, nowMouseOver ? IDC_HAND : IDC_ARROW));
					InvalidateRect(hwnd, NULL, false);
				}
				emb->mouseOver = nowMouseOver;

				if (GetCapture()==hwnd  &&  !nowMouseOver  &&  !emb->mouseClicked)
				{
					emb->mouseOver = false;
					InvalidateRect(hwnd, NULL, false);
					ReleaseCapture();
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emb->mouseOver = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				emb->mouseClicked = true;
				emb->mouseOver = true;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONUP:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				bool nowMouseOver = (PtInRect(&rect, pt)==TRUE);

				InvalidateRect(hwnd, NULL, false);

				bool wasClicked = false;
				if (GetCapture() != hwnd)
					emb->mouseClicked = false;

				if (emb->mouseClicked)
				{
					emb->mouseClicked = false;
					emb->mouseOver = true;
					if (nowMouseOver)
						wasClicked = true;
				}
				ReleaseCapture();
				emb->mouseClicked = false;
				emb->mouseOver = false;
				if (wasClicked)
				{
					if (emb->twoState)
					{
						if (emb->selected)
						{
							if (emb->ts_allow_unclick)
								emb->selected = false;
						}
						else
							emb->selected = true;
					}
					LONG wID;
					wID = GetWindowLong(hwnd, GWL_ID);
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), 
							(LPARAM)hwnd);
				}
			}
			break;
		default:
			break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//-------------------------------------------------------------------------------------------------

bool EM2Button::changeCaption(char *newCaption)
{
	if (!newCaption)
		return false;
	char * newbuf = copyString(newCaption, 0);
	if (!newbuf)
		return false;
	if (caption)
		free(caption);
	caption = newbuf;
	captionSize = (int)strlen(caption);
	SetWindowText(hwnd, caption);
	InvalidateRect(hwnd, NULL, false);
	return true;
}

//-------------------------------------------------------------------------------------------------

bool EM2Button::setFont(HFONT hNewFont, bool autodel)
{
	if (autodelfont)
		DeleteObject(hFont);
	hFont = hNewFont;
	autodelfont = autodel;
	return true;
}

//-------------------------------------------------------------------------------------------------

HFONT EM2Button::getFont()
{
	return hFont;
}

//-------------------------------------------------------------------------------------------------
