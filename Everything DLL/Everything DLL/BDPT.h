#ifndef __BDPT__h__
#define __BDPT__h__

#include "raytracer.h"

//----------------------------------------------------------------

class SunBoundingBox
{
public:
	Triangle tris[6];
	float areas[6];
	float sumareas[6];
	float totalarea;
	float A, B, C, D;

	Point3d randomStartPoint(float &pdf);
	bool createProjectedPlane(Vector3d sundir, SceneStatic * ss);
	float evalDistance(Point3d origin, Vector3d sdir);
};

//----------------------------------------------------------------

class BDPTNode
{
public:
	Color4 incoming;
	Color4 brdf;

	HitData hd;
	Point3d int_point;
	float u,v;

	MaterialNox * mat;
	MatLayer * mlay;
	Triangle * tri;
	Matrix4d inst_mat;
	float rough;
	float pdfArea;		// pdf in space domain
	float pdfAngle;		// pdf of incoming (opposite) direction in angle domain
	float pdfBackAngle;	// pdf of outgoing direction in angle domain
	float pdfMatLayer;
	float rr;
	float rrBack;
	float lastDistance;
	bool sssIn, sssOut;
	bool fakeTransm, fakeRefl;
	
	void evalNormalFromMap();
	bool updatePdfsBrdfMultilayered(bool fromCam, bool correctPdf);
	bool isDeltaDistr(float tu, float tv);	//slow as hell
	bool isDeltaDistrCurLayer();
	inline void clearNode() { mat=NULL; mlay=NULL; tri=NULL; sssIn=false; sssOut=false; incoming=Color4(0,0,0); brdf=Color4(0,0,0); lastDistance = -1.0f; fakeRefl=fakeTransm=false; }
	BDPTNode() { clearNode(); }
};

//----------------------------------------------------------------

#define BDPT_MAX_NODES 30

class BDPT
{
public:
	SceneStatic * ss;
	Camera * cam;
	bool * working;
	bool finished;
	int threadID;
	SunBoundingBox sbb;
	MatLayer portalmlay;
	int etl_size;

	
	float sample_time;
	float pdfBackAreaHitEmitter;
	Color4 att_to_sky;
	bool connect_to_cam;
	bool can_hit_emitter;
	bool ray_in_sky;
	int triIDHitEmitter;
	int instanceIDHitEmitter;
	int cur_emitter_type;
	float spectr_pos_cam;
	float spectr_pos_emitter;

	BDPTNode ePath[BDPT_MAX_NODES];
	BDPTNode lPath[BDPT_MAX_NODES];
	int eSize, lSize;
	int max_refl;
	float cX, cY;

	void renderingLoop();
	bool createPath(int start_type);
	bool visibilityTest(int eI, int lI, Color4 &attenuation);
	float evalWeight(int eI, int lI, bool make_log=false);
	float evalAttenuationForInvisibleLight(int eI, int lI);
	Color4 evalPathContributionUnweighted(int eI, int lI);
	void makeLog(int iE, int iL);


	bool isShadowOpacFakeGlass(const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, Color4 &attenuation, bool stopOnPortal);
	float findNonOpacFakeTri(const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, float &uu, float &vv, Color4 &attenuation, bool stopOnPortal);
	float findTri(const Point3d &origin, Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instanceID, int &iitri, float &uu, float &vv, 
				Color4 &attenuation, bool stopOnPortal, float invWgt, bool &fakeRefl, bool &fakeTransm);

	BDPT();
	~BDPT();
};

//----------------------------------------------------------------

#endif

