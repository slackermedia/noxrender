#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>
#include "CameraResponse.h"

extern HMODULE hDllModule;

bool redrawFakeDofOnThread(HWND hImg);	// for final tab


INT_PTR CALLBACK RendererMainWindow::PostWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				HWND hISO		= GetDlgItem(hWnd, IDC_REND_TONE_ISO);
				HWND hTone		= GetDlgItem(hWnd, IDC_REND_TONE_TONE);
				HWND hGamma		= GetDlgItem(hWnd, IDC_REND_TONE_GAMMA);
				EMEditTrackBar * emiso		= GetEMEditTrackBarInstance(hISO);
				EMEditTrackBar * emtone		= GetEMEditTrackBarInstance(hTone);
				EMEditTrackBar * emgamma	= GetEMEditTrackBarInstance(hGamma);
				emiso->setEditBoxWidth(32);
				emtone->setEditBoxWidth(32);
				emgamma->setEditBoxWidth(32);
				emiso->setValuesToFloat(0, NOX_POST_EV_MIN, NOX_POST_EV_MAX, 0.5f);
				emtone->setValuesToFloat(0.85f, NOX_POST_TONE_MIN, NOX_POST_TONE_MAX, 0.01f);
				emgamma->setValuesToFloat(2.2f, NOX_POST_GAMMA_MIN, NOX_POST_GAMMA_MAX, 0.1f);
				theme.apply(emiso);
				theme.apply(emtone);
				theme.apply(emgamma);

				HWND hHideStamp = GetDlgItem(hWnd, IDC_REND_POST_SHOW_TIMESTAMP);
				HWND hHideLogo  = GetDlgItem(hWnd, IDC_REND_POST_SHOW_LOGO);
				HWND hRstIso    = GetDlgItem(hWnd, IDC_REND_POST_RESET_ISO);
				HWND hRstTone   = GetDlgItem(hWnd, IDC_REND_POST_RESET_TONE);
				HWND hRstGamma  = GetDlgItem(hWnd, IDC_REND_POST_RESET_GAMMA);
				HWND hRstBright = GetDlgItem(hWnd, IDC_REND_POST_RESET_BRIGHTNESS);
				HWND hRstContr  = GetDlgItem(hWnd, IDC_REND_POST_RESET_CONTRAST);
				HWND hRstSat    = GetDlgItem(hWnd, IDC_REND_POST_RESET_SATURATION);
				HWND hRstRed    = GetDlgItem(hWnd, IDC_REND_POST_RESET_RED);
				HWND hRstGreen  = GetDlgItem(hWnd, IDC_REND_POST_RESET_GREEN);
				HWND hRstBlue   = GetDlgItem(hWnd, IDC_REND_POST_RESET_BLUE);
				HWND hRstTemp   = GetDlgItem(hWnd, IDC_REND_POST_RESET_TEMPERATURE);
				HWND hRstHideSt = GetDlgItem(hWnd, IDC_REND_POST_SHOW_TIMESTAMP);
				HWND hRstVign	= GetDlgItem(hWnd, IDC_REND_POST_RESET_VIGNETTE);
				HWND hRstGI		= GetDlgItem(hWnd, IDC_REND_POST_RESET_GI_COMP);

				HWND hGr1		= GetDlgItem(hWnd, IDC_REND_POST_GR1);
				HWND hGr2		= GetDlgItem(hWnd, IDC_REND_POST_GR2);
				HWND hGr3		= GetDlgItem(hWnd, IDC_REND_POST_GR3);
				HWND hGr4		= GetDlgItem(hWnd, IDC_REND_POST_GR4);

				theme.apply(GetEMButtonInstance(hRstIso));
				theme.apply(GetEMButtonInstance(hRstTone));
				theme.apply(GetEMButtonInstance(hRstGamma));
				theme.apply(GetEMButtonInstance(hRstBright));
				theme.apply(GetEMButtonInstance(hRstContr));
				theme.apply(GetEMButtonInstance(hRstSat));
				theme.apply(GetEMButtonInstance(hRstRed));
				theme.apply(GetEMButtonInstance(hRstGreen));
				theme.apply(GetEMButtonInstance(hRstBlue));
				theme.apply(GetEMButtonInstance(hRstTemp));
				theme.apply(GetEMButtonInstance(hRstVign));
				theme.apply(GetEMButtonInstance(hRstGI));
				theme.apply(GetEMGroupBarInstance(hGr1));
				theme.apply(GetEMGroupBarInstance(hGr2));
				theme.apply(GetEMGroupBarInstance(hGr3));
				theme.apply(GetEMGroupBarInstance(hGr4));


				EMCheckBox * emHide1 = GetEMCheckBoxInstance(hHideStamp);
				EMCheckBox * emHide2 = GetEMCheckBoxInstance(hHideLogo);
				theme.apply(emHide1);
				theme.apply(emHide2);
				COLORREF hideCol = theme.WindowBackground;
				emHide1->selected = false;
				emHide2->selected = true;
				InvalidateRect(hHideStamp, NULL, false);
				InvalidateRect(hHideLogo, NULL, false);


				POINT p1 = {0,0};
				ClientToScreen(hRstIso, &p1);
				ScreenToClient(hWnd, &p1);
				SetWindowPos(hRstIso, HWND_TOP, p1.x, p1.y-1, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
				POINT p2 = {0,0};
				ClientToScreen(hRstTone, &p2);
				ScreenToClient(hWnd, &p2);
				SetWindowPos(hRstTone, HWND_TOP, p2.x, p2.y+1, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

				HWND hBri = GetDlgItem(hWnd, IDC_REND_POST_BRIGHTNESS);
				HWND hCon = GetDlgItem(hWnd, IDC_REND_POST_CONTRAST);
				HWND hSat = GetDlgItem(hWnd, IDC_REND_POST_SATURATION);
				HWND hCur = GetDlgItem(hWnd, IDC_REND_POST_CURVE);
				HWND hCR  = GetDlgItem(hWnd, IDC_REND_POST_CURVE_RED);
				HWND hCG  = GetDlgItem(hWnd, IDC_REND_POST_CURVE_GREEN);
				HWND hCB  = GetDlgItem(hWnd, IDC_REND_POST_CURVE_BLUE);
				HWND hCL  = GetDlgItem(hWnd, IDC_REND_POST_CURVE_LIGHTNESS);
				HWND hRes = GetDlgItem(hWnd, IDC_REND_POST_CURVE_RESET);
				HWND hRed = GetDlgItem(hWnd, IDC_REND_POST_RED);
				HWND hGre = GetDlgItem(hWnd, IDC_REND_POST_GREEN);
				HWND hBlu = GetDlgItem(hWnd, IDC_REND_POST_BLUE);
				HWND hTem = GetDlgItem(hWnd, IDC_REND_POST_TEMPERATURE);
				HWND hFlt = GetDlgItem(hWnd, IDC_REND_POST_FILTER);
				HWND hVgn = GetDlgItem(hWnd, IDC_REND_POST_VIGNETTE);
				HWND hGIC = GetDlgItem(hWnd, IDC_REND_POST_GI_COMPENSATE);
				HWND hRsp = GetDlgItem(hWnd, IDC_REND_POST_RESPONSE);
				HWND hSGr = GetDlgItem(hWnd, IDC_REND_POST_RESPONSE_SHOW_GRAPH);
				HWND hRPr = GetDlgItem(hWnd, IDC_REND_POST_RESPONSE_SHOW_PREVIEWS);

				HWND hTD  = GetDlgItem(hWnd, IDC_REND_POST_TEMP_DIRECT);
				HWND hTG  = GetDlgItem(hWnd, IDC_REND_POST_TEMP_GI);
				HWND hHot = GetDlgItem(hWnd, IDC_REND_POST_HOT_PIXELS_ON);
				EMCheckBox* emcTD = GetEMCheckBoxInstance(hTD);
				EMCheckBox* emcTG = GetEMCheckBoxInstance(hTG);
				EMCheckBox* emcHP = GetEMCheckBoxInstance(hHot);
				emcTD->selected = true;
				emcTG->selected = true;
				emcHP->selected = false;
				theme.apply(emcTD);
				theme.apply(emcTG);
				theme.apply(emcHP);

				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_SHOW_HISTOGRAM)));

				EMEditTrackBar * embri = GetEMEditTrackBarInstance(hBri);
				EMEditTrackBar * emcon = GetEMEditTrackBarInstance(hCon);
				EMEditTrackBar * emsat = GetEMEditTrackBarInstance(hSat);
				EMCurve * emcur	= GetEMCurveInstance(hCur);
				EMCheckBox* emcR = GetEMCheckBoxInstance(hCR);
				EMCheckBox* emcG = GetEMCheckBoxInstance(hCG);
				EMCheckBox* emcB = GetEMCheckBoxInstance(hCB);
				EMCheckBox* emcL = GetEMCheckBoxInstance(hCL);
				EMButton * emRes = GetEMButtonInstance(hRes);
				EMEditTrackBar * emR = GetEMEditTrackBarInstance(hRed);
				EMEditTrackBar * emG = GetEMEditTrackBarInstance(hGre);
				EMEditTrackBar * emB = GetEMEditTrackBarInstance(hBlu);
				EMEditTrackBar * emTemp = GetEMEditTrackBarInstance(hTem);
				EMEditTrackBar * emVgn = GetEMEditTrackBarInstance(hVgn);
				EMEditTrackBar * emGIC = GetEMEditTrackBarInstance(hGIC);
				EMComboBox	   * emRsp = GetEMComboBoxInstance(hRsp);
				EMButton       * emSGr = GetEMButtonInstance(hSGr);
				EMButton       * emRPr = GetEMButtonInstance(hRPr);

				EMComboBox * emcFlt = GetEMComboBoxInstance(hFlt);
				emcFlt->addItem("No AA");
				emcFlt->addItem("Box");
				emcFlt->addItem("Box2");
				emcFlt->addItem("AA weighted");
				emcFlt->addItem("Mitchell-Netravali");
				emcFlt->addItem("Catmull-Rom");
				emcFlt->addItem("B-spline");
				emcFlt->addItem("Gauss");
				emcFlt->addItem("Gauss very smooth");
				emcFlt->selected = 0;
				theme.apply(emcFlt);

				emRsp->deleteItems();
				for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
					emRsp->addItem(getResponseFunctionName(i));
				emRsp->selected = 0;
				theme.apply(emRsp);

				emcR->selected = true;
				emcG->selected = true;
				emcB->selected = true;
				emcL->selected = false;
				theme.apply(emR);
				theme.apply(emG);
				theme.apply(emB);
				theme.apply(embri);
				theme.apply(emcon);
				theme.apply(emsat);
				theme.apply(emRes);
				theme.apply(emcur);
				theme.apply(emcR);
				theme.apply(emcG);
				theme.apply(emcB);
				theme.apply(emcL);
				theme.apply(emTemp);
				theme.apply(emVgn);
				theme.apply(emGIC);
				theme.apply(emRsp);
				theme.apply(emSGr);
				theme.apply(emRPr);

				embri->setEditBoxWidth(32);
				emcon->setEditBoxWidth(32);
				emsat->setEditBoxWidth(32);
				emR->setEditBoxWidth(32);
				emG->setEditBoxWidth(32);
				emB->setEditBoxWidth(32);
				emTemp->setEditBoxWidth(32);
				emVgn->setEditBoxWidth(32);
				emGIC->setEditBoxWidth(32);
				embri->setValuesToFloat(0, -1, 1, 0.05f);
				emcon->setValuesToFloat(1, 0, 2, 0.05f);
				emsat->setValuesToFloat(0, -1, 1, 0.05f);
				emR->setValuesToFloat(0, -1, 1, 0.05f);
				emG->setValuesToFloat(0, -1, 1, 0.05f);
				emB->setValuesToFloat(0, -1, 1, 0.05f);
				emTemp->setValuesToInt(6000, 1000, 11000, 10);
				emVgn->setValuesToInt(20, NOX_POST_VIGNETTE_MIN, NOX_POST_VIGNETTE_MAX, 10);
				emGIC->setValuesToFloat(0, NOX_POST_EV_MIN, NOX_POST_EV_MAX, 0.5f);

				HWND hLoad = GetDlgItem(hWnd, IDC_REND_POST_LOAD);
				HWND hSave = GetDlgItem(hWnd, IDC_REND_POST_SAVE);
				HWND hResetAll = GetDlgItem(hWnd, IDC_REND_POST_RESET_ALL);
				EMButton * emload = GetEMButtonInstance(hLoad);
				EMButton * emsave = GetEMButtonInstance(hSave);
				EMButton * emresetall = GetEMButtonInstance(hResetAll);
				theme.apply(emload);
				theme.apply(emsave);
				theme.apply(emresetall);

				theme.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_REND_POST_REDRAW_FINAL)));
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_POST_SHOW_TIMESTAMP:
					{
						EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_SHOW_TIMESTAMP));
						CHECK(emc);
						bool hide = emc->selected;
						RendererMainWindow::showTimeStamp = hide;
						SetTimer(hPost, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_SHOW_LOGO:
					{
						EMCheckBox * emc = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_SHOW_LOGO));
						CHECK(emc);
						bool hide = emc->selected;
						RendererMainWindow::showLogoStamp = hide;
						SetTimer(hPost, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_TONE_ISO:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emiso = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_TONE_ISO));
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						float ev = emiso->floatValue;
						ev = min(NOX_POST_EV_MAX, max(NOX_POST_EV_MIN, ev));
						cam->iMod.setISO_EV_compensation(ev);
						cam->iMod.evalMultiplierFromISO_and_EV();
						empv->imgMod->setISO_camera(cam->iMod.getISO_camera());
						empv->imgMod->setISO_EV_compensation(ev);
						empv->imgMod->evalMultiplierFromISO_and_EV();
						SetTimer(hPost, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_GI_COMPENSATE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emgi = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_GI_COMPENSATE));
						empv->imgMod->setGICompensation(emgi->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setGICompensation(emgi->floatValue);
						SetTimer(hPost, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_TONE_TONE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emtone = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_TONE_TONE));
						empv->imgMod->setToneMappingValue(emtone->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setToneMappingValue(emtone->floatValue);
						SetTimer(hPost, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_TONE_GAMMA:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emgamma = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_TONE_GAMMA));
						empv->imgMod->setGamma(emgamma->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setGamma(emgamma->floatValue);
						SetTimer(hPost, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_BRIGHTNESS:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * embri = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_BRIGHTNESS));
						empv->imgMod->setBrightness(embri->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setBrightness(embri->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_CONTRAST:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emcon = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_CONTRAST));
						empv->imgMod->setContrast(emcon->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setContrast(emcon->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_SATURATION:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emsat = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_SATURATION));
						empv->imgMod->setSaturation(emsat->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setSaturation(emsat->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RED:
					case IDC_REND_POST_GREEN:
					case IDC_REND_POST_BLUE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emR = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_RED));
						EMEditTrackBar * emG = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_GREEN));
						EMEditTrackBar * emB = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_BLUE));
						empv->imgMod->setRGBAdd(emR->floatValue, emG->floatValue, emB->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setRGBAdd(emR->floatValue, emG->floatValue, emB->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_TEMPERATURE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emtemp = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_TEMPERATURE));
						empv->imgMod->setTemperature((float)emtemp->intValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setTemperature((float)emtemp->intValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_VIGNETTE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emtemp = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_VIGNETTE));
						empv->imgMod->setVignette(emtemp->intValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setVignette(emtemp->intValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_CURVE_RED:
					{
						EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE));
						EMCheckBox * emcbr = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_RED));
						EMCheckBox * emcbg = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_GREEN));
						EMCheckBox * emcbb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_BLUE));
						EMCheckBox * emcbl = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_LIGHTNESS));
						emc->activeRGB[3] = false;
						bool recalc = emc->isLuminocityMode;
						emc->isLuminocityMode = false;
						emc->activeRGB[0] = emcbr->selected;
						emcbl->selected = false;
						InvalidateRect(emcbl->hwnd, NULL, false);
						if (recalc)
							refreshRenderedImage();
						InvalidateRect(emc->hwnd, NULL, false);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setCurveCheckboxes(emcbr->selected, emcbg->selected, emcbb->selected, emcbl->selected);
					}
					break;
					case IDC_REND_POST_CURVE_GREEN:
					{
						EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE));
						EMCheckBox * emcbr = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_RED));
						EMCheckBox * emcbg = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_GREEN));
						EMCheckBox * emcbb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_BLUE));
						EMCheckBox * emcbl = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_LIGHTNESS));
						emc->activeRGB[3] = false;
						bool recalc = emc->isLuminocityMode;
						emc->isLuminocityMode = false;
						emc->activeRGB[1] = emcbg->selected;
						emcbl->selected = false;
						InvalidateRect(emcbl->hwnd, NULL, false);
						if (recalc)
							refreshRenderedImage();
						InvalidateRect(emc->hwnd, NULL, false);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setCurveCheckboxes(emcbr->selected, emcbg->selected, emcbb->selected, emcbl->selected);
					}
					break;
					case IDC_REND_POST_CURVE_BLUE:
					{
						EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE));
						EMCheckBox * emcbr = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_RED));
						EMCheckBox * emcbg = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_GREEN));
						EMCheckBox * emcbb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_BLUE));
						EMCheckBox * emcbl = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_LIGHTNESS));
						emc->activeRGB[3] = false;
						bool recalc = emc->isLuminocityMode;
						emc->isLuminocityMode = false;
						emc->activeRGB[2] = emcbb->selected;
						emcbl->selected = false;
						InvalidateRect(emcbl->hwnd, NULL, false);
						if (recalc)
							refreshRenderedImage();
						InvalidateRect(emc->hwnd, NULL, false);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setCurveCheckboxes(emcbr->selected, emcbg->selected, emcbb->selected, emcbl->selected);
					}
					break;
					case IDC_REND_POST_CURVE_LIGHTNESS:
					{
						EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE));
						EMCheckBox * emcr = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_RED));
						EMCheckBox * emcg = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_GREEN));
						EMCheckBox * emcb = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_BLUE));
						EMCheckBox * emcl = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE_LIGHTNESS));
						if (emcl->selected)
						{
							emc->activeRGB[0] = false;
							emc->activeRGB[1] = false;
							emc->activeRGB[2] = false;
							emcr->selected = false;
							emcg->selected = false;
							emcb->selected = false;
						}
						else
						{
							emc->activeRGB[0] = true;
							emc->activeRGB[1] = true;
							emc->activeRGB[2] = true;
							emcr->selected = true;
							emcg->selected = true;
							emcb->selected = true;
						}
						emc->isLuminocityMode = emcl->selected;
						emc->activeRGB[3] = emcl->selected;
						InvalidateRect(emcr->hwnd, NULL, false);
						InvalidateRect(emcg->hwnd, NULL, false);
						InvalidateRect(emcb->hwnd, NULL, false);
						refreshRenderedImage();
						InvalidateRect(emc->hwnd, NULL, false);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setCurveCheckboxes(emcr->selected, emcg->selected, emcb->selected, emcl->selected);
					}
					break;
					case IDC_REND_POST_CURVE_RESET:
					{
						EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE));
						emc->reset();
						refreshRenderedImage();
						InvalidateRect(emc->hwnd, NULL, false);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.delCurveAllPoints();
					}
					break;
					case IDC_REND_POST_CURVE:
					{
						if (wmEvent == CU_CHANGED)
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.delCurveAllPoints();
						EMCurve * emc = GetEMCurveInstance(GetDlgItem(hWnd, IDC_REND_POST_CURVE));
						for (unsigned int i=0; i<emc->pointsR.size(); i++)
							cam->iMod.addRedPoint(emc->pointsR[i].x, emc->pointsR[i].y);
						for (unsigned int i=0; i<emc->pointsG.size(); i++)
							cam->iMod.addGreenPoint(emc->pointsG[i].x, emc->pointsG[i].y);
						for (unsigned int i=0; i<emc->pointsB.size(); i++)
							cam->iMod.addBluePoint(emc->pointsB[i].x, emc->pointsB[i].y);
						for (unsigned int i=0; i<emc->pointsL.size(); i++)
							cam->iMod.addLuminancePoint(emc->pointsL[i].x, emc->pointsL[i].y);
					}
					break;
					case IDC_REND_POST_RESET_ISO:
					{
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_TONE_ISO));
						emetb->setValuesToFloat(0, NOX_POST_EV_MIN, NOX_POST_EV_MAX, 0.5f);

						float ev = 0.0f;
						cam->iMod.setISO_EV_compensation(ev);
						cam->iMod.evalMultiplierFromISO_and_EV();
						empv->imgMod->setISO_camera(cam->iMod.getISO_camera());
						empv->imgMod->setISO_EV_compensation(ev);
						empv->imgMod->evalMultiplierFromISO_and_EV();

						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_GI_COMP:
					{
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_GI_COMPENSATE));
						emetb->setValuesToFloat(0, NOX_POST_EV_MIN, NOX_POST_EV_MAX, 0.5f);
						float ev = 0.0f;
						cam->iMod.setGICompensation(ev);
						empv->imgMod->setGICompensation(ev);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_TONE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_TONE_TONE));
						emetb->setValuesToFloat(0.85f, NOX_POST_TONE_MIN, NOX_POST_TONE_MAX, 0.01f);
						empv->imgMod->setToneMappingValue(emetb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setToneMappingValue(emetb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_GAMMA:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_TONE_GAMMA));
						emetb->setValuesToFloat(2.2f, NOX_POST_GAMMA_MIN, NOX_POST_GAMMA_MAX, 0.1f);
						empv->imgMod->setGamma(emetb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setGamma(emetb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_BRIGHTNESS:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_BRIGHTNESS));
						emetb->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.05f);
						empv->imgMod->setBrightness(emetb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setBrightness(emetb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_CONTRAST:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_CONTRAST));
						emetb->setValuesToFloat(1.0f, 0.0f, 2.0f, 0.05f);
						empv->imgMod->setContrast(emetb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setContrast(emetb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_SATURATION:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_SATURATION));
						emetb->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.05f);
						empv->imgMod->setSaturation(emetb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setSaturation(emetb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_RED:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetbr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_RED));
						EMEditTrackBar * emetbg = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_GREEN));
						EMEditTrackBar * emetbb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_BLUE));
						emetbr->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.05f);
						empv->imgMod->setRGBAdd(emetbr->floatValue, emetbg->floatValue, emetbb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setRGBAdd(emetbr->floatValue, emetbg->floatValue, emetbb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_GREEN:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetbr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_RED));
						EMEditTrackBar * emetbg = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_GREEN));
						EMEditTrackBar * emetbb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_BLUE));
						emetbg->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.05f);
						empv->imgMod->setRGBAdd(emetbr->floatValue, emetbg->floatValue, emetbb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setRGBAdd(emetbr->floatValue, emetbg->floatValue, emetbb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_BLUE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetbr = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_RED));
						EMEditTrackBar * emetbg = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_GREEN));
						EMEditTrackBar * emetbb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_BLUE));
						emetbb->setValuesToFloat(0.0f, -1.0f, 1.0f, 0.05f);
						empv->imgMod->setRGBAdd(emetbr->floatValue, emetbg->floatValue, emetbb->floatValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setRGBAdd(emetbr->floatValue, emetbg->floatValue, emetbb->floatValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_TEMPERATURE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_TEMPERATURE));
						emetb->setValuesToInt(6000, 1000, 11000, 10);
						empv->imgMod->setTemperature((float)emetb->intValue);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setTemperature((float)emetb->intValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESET_VIGNETTE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMEditTrackBar * emetb = GetEMEditTrackBarInstance(GetDlgItem(hWnd, IDC_REND_POST_VIGNETTE));
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						emetb->setValuesToInt(cam->diaphSampler.intBokehVignette, NOX_POST_VIGNETTE_MIN, NOX_POST_VIGNETTE_MAX, 10);
						empv->imgMod->setVignette(emetb->intValue);
						cam->iMod.setVignette(emetb->intValue);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_FILTER:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						EMComboBox * emc = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_FILTER));
						if (empv)
							empv->imgMod->setFilter(emc->selected);
						if (cam)
							cam->iMod.setFilter(emc->selected);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_LOAD:
					{
						loadPostprocess();
					}
					break;
					case IDC_REND_POST_SAVE:
					{
						savePostprocess();
					}
					break;
					case IDC_REND_POST_RESET_ALL:
					{
						ImageModifier imod;

						EMPView * empv = GetEMPViewInstance(hImage);
						CHECK(empv);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						CHECK(cam);

						int dRes = MessageBox(hWnd, "All post settings will be lost.\nAre you sure?", "Warning.", MB_YESNO);
						if (dRes==IDYES)
						{
							*(empv->imgMod) = imod;
							cam->iMod = imod;
							fillCameraPostData(cam);
							SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
						}
					}
					break;
					case IDC_REND_POST_REDRAW_FINAL:
					{
						Raytracer * rtr = Raytracer::getInstance();
						if (rtr->curScenePtr->nowRendering)
						{
							MessageBox(0, "To finalize image, stop renderer first.", "", 0);
							break;
						}
						Camera * cam = rtr->curScenePtr->getActiveCamera();
						if (!cam)
							break;
						redrawFakeDofOnThread(hImage);
					}
					break;
					case IDC_REND_POST_TEMP_DIRECT:
					{
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_TEMP_GI:
					{
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESPONSE:
					{
						EMComboBox * emcombo = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_RESPONSE));
						EMPView * empv = GetEMPViewInstance(hImage);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						if (emcombo)
							empv->imgMod->setResponseFunctionNumber(emcombo->selected);
						if (cam)
							cam->iMod.setResponseFunctionNumber(emcombo->selected);

						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
					case IDC_REND_POST_RESPONSE_SHOW_GRAPH:
					{
						if (wmEvent == BN_CLICKED)
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EMComboBox * emcombo = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_RESPONSE));
							EMPView * empv = GetEMPViewInstance(hImage);
							int num = 0;
							if (emcombo)
								num = emcombo->selected;
							int newNum = (int)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_RESPONSE_GRAPH_DIALOG), hWnd, ResponseGraphDlgProc,(LPARAM)(num));
							if (newNum > -1   &&   newNum!=num)
							{
								if (empv)
									empv->imgMod->setResponseFunctionNumber(newNum);
								if (cam)
									cam->iMod.setResponseFunctionNumber(newNum);
								emcombo->selected = newNum;
								InvalidateRect(emcombo->hwnd, NULL, false);
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
							}
						}
					}
					break;
					case IDC_REND_POST_RESPONSE_SHOW_PREVIEWS:
					{
						if (wmEvent == BN_CLICKED)
						{
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EMComboBox * emcombo = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_POST_RESPONSE));
							EMPView * empv = GetEMPViewInstance(hImage);
							int num = 0;
							if (emcombo)
								num = emcombo->selected;

							int newNum = (int)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_RESPONSE_PREVIEWS_2_DIALOG), hWnd, ResponsePreviewDlg2Proc,(LPARAM)(num));

							if (newNum > -1   &&   newNum!=num)
							{
								if (empv)
									empv->imgMod->setResponseFunctionNumber(newNum);
								if (cam)
									cam->iMod.setResponseFunctionNumber(newNum);
								emcombo->selected = newNum;
								InvalidateRect(emcombo->hwnd, NULL, false);
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
							}
						}
					}
					break;
					case IDC_REND_POST_SHOW_HISTOGRAM:
					{
						HWND hHist  = GetDlgItem(hWnd, IDC_REND_POST_SHOW_HISTOGRAM);
						EMCheckBox* emSH = GetEMCheckBoxInstance(hHist);
						EMPView * empv = GetEMPViewInstance(hImage);
						empv->show_histogram = emSH->selected;
						InvalidateRect(hImage, NULL, false);
					}
					break;
					case IDC_REND_POST_HOT_PIXELS_ON:
					{
						HWND hHot  = GetDlgItem(hWnd, IDC_REND_POST_HOT_PIXELS_ON);
						EMCheckBox* emHP = GetEMCheckBoxInstance(hHot);
						EMPView * empv = GetEMPViewInstance(hImage);
						empv->imgMod->setDotsEnabled(emHP->selected);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->iMod.setDotsEnabled(emHP->selected);
						SetTimer(hWnd, TIMER_POSTPROCESS_ID, POSTPROCESS_TIMEOUT, NULL);
					}
					break;
				}
			}
			break;
		case WM_TIMER:
			{
				if (wParam == TIMER_POSTPROCESS_ID)
				{
					KillTimer(hWnd, TIMER_POSTPROCESS_ID);
					refreshRenderedImage();
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}
