#include "RendererMain2.h"
#include "EM2Controls.h"
#include "newgui_coords.h"


//------------------------------------------------------------------------------------------------

LONG_PTR RendererMain2::processResizeMessage(WPARAM wParam, LPARAM lParam)
{
	int tabssize = TABS_WIDTH;
	int rpanelsize = RIGHT_PANEL_WIDTH;

	RECT mcRect;
	GetClientRect(getHWND(), &mcRect);
	mcRect.right = (int)(short)LOWORD(lParam);
	mcRect.bottom = (int)(short)HIWORD(lParam);

	int tabsPosX = mcRect.right-TABS_WIDTH-5-RIGHT_PANEL_WIDTH;
	if (!rightPanelOpened)
		tabsPosX = mcRect.right-TABS_WIDTH-3;
	SetWindowPos(hTabs, HWND_TOP, tabsPosX,0, TABS_WIDTH, 260, SWP_NOZORDER);
	EM2Tabs * emtabs = GetEM2TabsInstance(hTabs);
	if (!emtabs)
		return 0;
	emtabs->bgShiftX = tabsPosX;
	emtabs->bgShiftY = 0;

	int rightPanelPosX = mcRect.right-RIGHT_PANEL_WIDTH;
	bgShiftXRightPanel = rightPanelPosX;
	bgShiftXTopPanel = (mcRect.right-170-TABS_WIDTH-(rightPanelOpened?RIGHT_PANEL_WIDTH:0))/2;

	SetWindowPos(hTabRender,	HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabCamera,	HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabMats,		HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabEnv,		HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabBlend,		HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabPost,		HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabCorrection,HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabFakeDof,	HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabEffects,	HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	SetWindowPos(hTabOptions,	HWND_TOP, rightPanelPosX, 0, RIGHT_PANEL_WIDTH,mcRect.bottom, SWP_NOZORDER);
	
	SetWindowPos(hTopPanel,		HWND_TOP, bgShiftXTopPanel, 0, 300, 28, SWP_NOZORDER  );
	SetWindowPos(hViewport,		HWND_TOP, 11, 30,  tabsPosX-8-11, mcRect.bottom-30-11-28, SWP_NOZORDER);
	SetWindowPos(hBottomPanel,	HWND_TOP, 11, mcRect.bottom-11-28, tabsPosX-8-11, 28, SWP_NOZORDER);
	vpRect.left = 10;
	vpRect.right = tabsPosX-7;
	vpRect.top = 29;
	vpRect.bottom = mcRect.bottom-10;

	EM2Tabs * emt = GetEM2TabsInstance(hTabs);
	switch (emt->selected)
	{
		case 0:
			updateBgShiftsPanelRender();
			break;
		case 1:
			updateBgShiftsPanelCamera();
			break;
		case 2:
			updateBgShiftsPanelMats();
			break;
		case 3:
			updateBgShiftsPanelEnv();
			break;
		case 4:
			updateBgShiftsPanelBlend();
			break;
		case 5:
			updateBgShiftsPanelPost();
			break;
		case 6:
			updateBgShiftsPanelCorrection();
			break;
		case 7:
			updateBgShiftsPanelFakeDof();
			break;
		case 8:
			updateBgShiftsPanelEffects();
			break;
		case 9:
			updateBgShiftsPanelOptions();
			break;
	}
	updateBgShiftsPanelTop();
	InvalidateRect(hTopPanel, NULL, false);

	int bmposx = mcRect.right - RIGHT_PANEL_WIDTH - MBTN_W - 3;
	if (!rightPanelOpened)
		bmposx = mcRect.right - MBTN_W;
	int bmposy = 371;
	SetWindowPos(hButtonMain, HWND_TOP, bmposx, bmposy, 0,0, SWP_NOSIZE);
	EM2ImgButton * emibMain = GetEM2ImgButtonInstance(hButtonMain);
	updateControlBgShift(hButtonMain, 0, 0, emibMain->bgShiftX, emibMain->bgShiftY);

	int brposx = tabsPosX-7;
	if (regionsPanelOpened)
		brposx += 31;
	int brposy = bmposy;
	SetWindowPos(hButtonRegions, HWND_TOP, brposx, brposy, 0,0, SWP_NOSIZE);
	EM2ImgButton * emibReg = GetEM2ImgButtonInstance(hButtonRegions);
	updateControlBgShift(hButtonRegions, 0, 0, emibReg->bgShiftX, emibReg->bgShiftY);

	bgShiftXRegionsPanel = tabsPosX-7;
	bgShiftYRegionsPanel = 269;
	SetWindowPos(hRegionsPanel, HWND_TOP, bgShiftXRegionsPanel, bgShiftYRegionsPanel, 30, 240, SWP_NOZORDER);
	updateBgShiftsRegions();

	if (showstartgui)		// start texts
	{
		int cx = mcRect.right/2;
		int cy = mcRect.bottom/2;
		SetWindowPos(start_texts.ht1_welcome,		HWND_TOP, cx-100,    cy+9,    80,  16, SWP_NOZORDER);
		SetWindowPos(start_texts.ht2_noxname,		HWND_TOP, cx-100+83, cy+9,    110, 16, SWP_NOZORDER);
		SetWindowPos(start_texts.ht3_loadLink,		HWND_TOP, cx-56,    cy+47,   40,  16, SWP_NOZORDER);
		SetWindowPos(start_texts.ht4_noxScene,		HWND_TOP, cx-56+43, cy+47,   70,  16, SWP_NOZORDER);
		SetWindowPos(start_texts.ht7_or,			HWND_TOP, cx-15,    cy+67,   30,  16, SWP_NOZORDER);
		SetWindowPos(start_texts.ht5_openLink,		HWND_TOP, cx-92,    cy+87,   40,  16, SWP_NOZORDER);
		SetWindowPos(start_texts.ht6_SampleScenes,	HWND_TOP, cx-92+43, cy+87,   150, 16, SWP_NOZORDER);
		EM2Text * emtst1 = GetEM2TextInstance(start_texts.ht1_welcome);
		EM2Text * emtst2 = GetEM2TextInstance(start_texts.ht2_noxname);
		EM2Text * emtst3 = GetEM2TextInstance(start_texts.ht3_loadLink);
		EM2Text * emtst4 = GetEM2TextInstance(start_texts.ht4_noxScene);
		EM2Text * emtst5 = GetEM2TextInstance(start_texts.ht5_openLink);
		EM2Text * emtst6 = GetEM2TextInstance(start_texts.ht6_SampleScenes);
		EM2Text * emtst7 = GetEM2TextInstance(start_texts.ht7_or);
		updateControlBgShift(start_texts.ht1_welcome,		0, 0, emtst1->bgShiftX, emtst1->bgShiftY);
		updateControlBgShift(start_texts.ht2_noxname,		0, 0, emtst2->bgShiftX, emtst2->bgShiftY);
		updateControlBgShift(start_texts.ht3_loadLink,		0, 0, emtst3->bgShiftX, emtst3->bgShiftY);
		updateControlBgShift(start_texts.ht4_noxScene,		0, 0, emtst4->bgShiftX, emtst4->bgShiftY);
		updateControlBgShift(start_texts.ht5_openLink,		0, 0, emtst5->bgShiftX, emtst5->bgShiftY);
		updateControlBgShift(start_texts.ht6_SampleScenes,	0, 0, emtst6->bgShiftX, emtst6->bgShiftY);
		updateControlBgShift(start_texts.ht7_or,			0, 0, emtst7->bgShiftX, emtst7->bgShiftY);
		RECT updRect;
		updRect.left = cx - 100;
		updRect.top = cy - 150;
		updRect.right = cx + 100;
		updRect.bottom = cy + 50;
		InvalidateRect(getHWND(), &updRect, FALSE);
	}

	return 0;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::tabsWereChanged()
{
	EM2Tabs * emt = GetEM2TabsInstance(hTabs);
	if (!emt)
		return;

	bool rpo = rightPanelOpened;
	ShowWindow(hTabRender,		rpo && emt->selected==0 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabCamera,		rpo && emt->selected==1 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabMats,		rpo && emt->selected==2 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabEnv,			rpo && emt->selected==3 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabBlend,		rpo && emt->selected==4 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabPost,		rpo && emt->selected==5 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabCorrection,	rpo && emt->selected==6 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabFakeDof,		rpo && emt->selected==7 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabEffects,		rpo && emt->selected==8 ? SW_SHOW : SW_HIDE);
	ShowWindow(hTabOptions,		rpo && emt->selected==9 ? SW_SHOW : SW_HIDE);

	if (emt->last_selected!=emt->selected   ||   (!rightPanelOpened  &&  emt->last_selected==emt->selected))
		switch (emt->last_selected)
		{
			case 0: SendMessage(hTabRender,		EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 1: SendMessage(hTabCamera,		EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 2: SendMessage(hTabMats,		EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 3: SendMessage(hTabEnv,		EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 4: SendMessage(hTabBlend,		EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 5: SendMessage(hTabPost,		EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 6: SendMessage(hTabCorrection,	EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 7: SendMessage(hTabFakeDof,	EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 8: SendMessage(hTabEffects,	EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
			case 9: SendMessage(hTabOptions,	EMT_TAB_CLOSED, (WPARAM)0, (LPARAM)0);	break;
		}
	if (emt->last_selected!=emt->selected)
		switch (emt->selected)
		{
			case 0: SendMessage(hTabRender,		EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 1: SendMessage(hTabCamera,		EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 2: SendMessage(hTabMats,		EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 3: SendMessage(hTabEnv,		EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 4: SendMessage(hTabBlend,		EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 5: SendMessage(hTabPost,		EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 6: SendMessage(hTabCorrection,	EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 7: SendMessage(hTabFakeDof,	EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 8: SendMessage(hTabEffects,	EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
			case 9: SendMessage(hTabOptions,	EMT_TAB_OPENED, (WPARAM)0, (LPARAM)0);	break;
		}

	emt->last_selected = emt->selected;
	switch (emt->selected)
	{
		case 0:
			updateBgShiftsPanelRender();
			break;
		case 1:
			updateBgShiftsPanelCamera();
			break;
		case 2:
			updateBgShiftsPanelMats();
			break;
		case 3:
			updateBgShiftsPanelEnv();
			break;
		case 4:
			updateBgShiftsPanelBlend();
			break;
		case 5:
			updateBgShiftsPanelPost();
			break;
		case 6:
			updateBgShiftsPanelCorrection();
			break;
		case 7:
			updateBgShiftsPanelFakeDof();
			break;
		case 8:
			updateBgShiftsPanelEffects();
			break;
		case 9:
			updateBgShiftsPanelOptions();
			break;
	}
	InvalidateRect(getHWND(), NULL, false);
}

//------------------------------------------------------------------------------------------------

bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY)
{
	if (!hControl)
		return false;
	HWND hParent = GetParent(hControl);
	POINT p = {0, 0};
	MapWindowPoints(hControl, hParent, &p, 1);
	shCtrlX = shParX + p.x;
	shCtrlY = shParY + p.y;
	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::openCloseRightPanel(bool open)
{
	rightPanelOpened = open;

	tabsWereChanged();

	RECT rect;
	GetClientRect(getHWND(), &rect);
	processResizeMessage(0, MAKELONG(rect.right, rect.bottom));

	InvalidateRect(getHWND(), NULL, false);

	EM2ImgButton * btnMain = GetEM2ImgButtonInstance(hButtonMain);
	if (btnMain)
	{
		btnMain->bgImage = hBmpBg;
		btnMain->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
		if (open)
		{
			btnMain->setToolTip("Hide tab panel");
			btnMain->pNx = MBTN_RN_X;
			btnMain->pNy = MBTN_RN_Y;
			btnMain->pMx = MBTN_RM_X;
			btnMain->pMy = MBTN_RM_Y;
			btnMain->pCx = MBTN_RC_X;
			btnMain->pCy = MBTN_RC_Y;
			btnMain->pDx = MBTN_RD_X;
			btnMain->pDy = MBTN_RD_Y;
		}
		else
		{
			btnMain->setToolTip("Open tab panel");
			btnMain->pNx = MBTN_LN_X;
			btnMain->pNy = MBTN_LN_Y;
			btnMain->pMx = MBTN_LM_X;
			btnMain->pMy = MBTN_LM_Y;
			btnMain->pCx = MBTN_LC_X;
			btnMain->pCy = MBTN_LC_Y;
			btnMain->pDx = MBTN_LD_X;
			btnMain->pDy = MBTN_LD_Y;
		}
		InvalidateRect(btnMain->hwnd, NULL, FALSE);
	}

	EM2Tabs * emt = GetEM2TabsInstance(hTabs);
	if (emt)
	{
	}

}

//------------------------------------------------------------------------------------------------

void RendererMain2::openCloseRegionsPanel(bool open)
{
	regionsPanelOpened = open;

	RECT rect;
	GetClientRect(getHWND(), &rect);
	processResizeMessage(0, MAKELONG(rect.right, rect.bottom));
	ShowWindow(hRegionsPanel, open ? SW_SHOW : SW_HIDE);

	InvalidateRect(getHWND(), NULL, false);

	EM2ImgButton * btnReg = GetEM2ImgButtonInstance(hButtonRegions);
	if (btnReg)
	{
		btnReg->bgImage = hBmpBg;
		btnReg->bmImage = EM2Elements::getInstance()->getHBitmapPtr();
		if (open)
		{
			btnReg->setToolTip("Hide navigation tools");
			btnReg->pNx = RBTN_RN_X;
			btnReg->pNy = RBTN_RN_Y;
			btnReg->pMx = RBTN_RM_X;
			btnReg->pMy = RBTN_RM_Y;
			btnReg->pCx = RBTN_RC_X;
			btnReg->pCy = RBTN_RC_Y;
			btnReg->pDx = RBTN_RD_X;
			btnReg->pDy = RBTN_RD_Y;
		}
		else
		{
			btnReg->setToolTip("Open navigation tools");
			btnReg->pNx = RBTN_LN_X;
			btnReg->pNy = RBTN_LN_Y;
			btnReg->pMx = RBTN_LM_X;
			btnReg->pMy = RBTN_LM_Y;
			btnReg->pCx = RBTN_LC_X;
			btnReg->pCy = RBTN_LC_Y;
			btnReg->pDx = RBTN_LD_X;
			btnReg->pDy = RBTN_LD_Y;
		}
	}

	EM2Tabs * emt = GetEM2TabsInstance(hTabs);
	if (emt)
	{
	}

}

//------------------------------------------------------------------------------------------------
