#include <windows.h>
#include <stdio.h>
#include "EMControls.h"

extern HMODULE hDllModule;

void MyLine(HDC hdc, int X1, int Y1, int X2, int Y2, float w, COLORREF c, COLORREF b);
void MyEllipse(HDC hdc, int X1, int Y1, int X2, int Y2, COLORREF c, COLORREF b);
void MyEllipse2(HDC hdc, int X1, int Y1, int X2, int Y2, float a, COLORREF c, COLORREF b);
void MyEllipseSSE(HDC hdc, int X1, int Y1, int X2, int Y2, COLORREF c, COLORREF b);

//------------------------------------------------------------------------------------------------------------

EMAberration::EMAberration(HWND hWnd) 
{
	hwnd = hWnd;

	abbShiftXY = 0.0f;
	abbShiftZ = 0.0f;
	abbAchromatic = 0.0f;
	distPattern = 1.0f;

    colBackground = RGB(255,0,0);
    colLens = RGB(0,255,0);
    colAxis = RGB(128,128,0);
    colDepthLine = RGB(0,128,128);
    colRay = RGB(255,255,255);
    colBorderLeftUp = RGB(0,255,0);
    colBorderDownRight = RGB(0,0,255);

    colBackground = RGB(24,24,24);
    colLens = RGB(240,240,240);
    colAxis = RGB(255,255,255);
    colDepthLine = RGB(255,255,255);
    colRay = RGB(255,255,255);
    colBorderLeftUp = RGB(48,48,48);
    colBorderDownRight = RGB(80,80,80);

	drawNice = true;
}

EMAberration::~EMAberration() 
{
}

//------------------------------------------------------------------------------------------------------------

void InitEMAberration()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMAberration";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EMAberrationProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMAberration *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//------------------------------------------------------------------------------------------------------------

EMAberration * GetEMAberrationInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMAberration * emab = (EMAberration *)GetWindowLongPtr(hwnd, 0);
	#else
		EMAberration * emab = (EMAberration *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emab;
}

void SetEMAberrationInstance(HWND hwnd, EMAberration *emab)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emab);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emab);
	#endif
}

//------------------------------------------------------------------------------------------------------------

LRESULT CALLBACK EMAberrationProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMAberration * emab;
	emab = GetEMAberrationInstance(hwnd);

	switch(msg)
    {
		case WM_CREATE:
			{
				EMAberration * emab1 = new EMAberration(hwnd);
				SetEMAberrationInstance(hwnd, (EMAberration *)emab1);
			}
			break;
		case WM_SIZE:
			{
			}
			break;
		case WM_NCDESTROY:
			{
				delete emab;
				SetEMAberrationInstance(hwnd, 0);
			}
			break;
		case WM_LBUTTONDOWN:
		case WM_MOUSEMOVE:
			{
				if (msg==WM_MOUSEMOVE)
				{
					if (!(wParam&MK_LBUTTON))
						break;
				}
				RECT rect;
				GetClientRect(hwnd, &rect);
				POINT pt;
				pt.x = (short)LOWORD(lParam);
				pt.y = (short)HIWORD(lParam);
				float ptx = min(rect.right*0.95f, pt.x);
				float d1 = rect.right*0.65f;
				float d0 = rect.right*0.1f;
				float d = (ptx - d0)/(d1-d0);
				d = max(d, 0.1f);
				emab->distPattern = d;
				InvalidateRect(hwnd, NULL, false);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, EMA_DIST_CHANGED), (LPARAM)hwnd);

			}
			break;
		case WM_PAINT:
			{
				float abbShiftXY = emab->abbShiftXY;
				float abbShiftZ = emab->abbShiftZ;
				float abbAchromatic = emab->abbAchromatic;
				float distPattern = emab->distPattern;
				bool drawNice = emab->drawNice;

				RECT rect;
				HDC hdc, thdc;
				PAINTSTRUCT ps;

				hdc = BeginPaint(hwnd, &ps);
				GetClientRect(hwnd, &rect);
				thdc = CreateCompatibleDC(hdc); 
				HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
				HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

				int ellX = (int)(rect.right*0.1f);
				int ellY = (int)(rect.bottom*0.5f);
				int ellH = (int)(rect.bottom*0.4f);
				int ellW = (int)(ellX*0.2f);

				COLORREF ellCol = emab->colLens;
				COLORREF centerCol = emab->colAxis;
				COLORREF bckgCol = emab->colBackground;


				HBRUSH bkgBrush = CreateSolidBrush(bckgCol);
				HPEN bkgPen = CreatePen(PS_SOLID, 1, bckgCol);
				HBRUSH hOldBrush = (HBRUSH)SelectObject(thdc, bkgBrush);
				HPEN hOldPen = (HPEN)SelectObject(thdc, bkgPen);
				Rectangle(thdc, rect.left, rect.top, rect.right, rect.bottom);

				HBRUSH ellBrush = CreateSolidBrush(ellCol);
				HPEN ellPen = CreatePen(PS_SOLID, 1, ellCol);
				SelectObject(thdc, ellBrush);
				SelectObject(thdc, ellPen);
				if (drawNice)
					MyEllipseSSE(thdc, ellX-ellW, ellY-ellH, ellX+ellW, ellY+ellH, ellCol, bckgCol);
				else
					Ellipse(thdc, ellX-ellW, ellY-ellH, ellX+ellW, ellY+ellH);

				// main line
				SetBkMode(thdc, TRANSPARENT);
				HPEN hCenterPen = CreatePen(PS_DASH, 1, centerCol);
				SelectObject(thdc, hCenterPen);
				MoveToEx(thdc, rect.right/20, rect.bottom/2, NULL);
				LineTo(thdc, rect.right*19/20, rect.bottom/2);


				COLORREF raysCol = emab->colRay;
				int linewidth = 1;
				COLORREF tcols[3];
				tcols[0] = RGB(GetRValue(raysCol),0,0);
				tcols[1] = RGB(0,GetGValue(raysCol),0);
				tcols[2] = RGB(0,0,GetBValue(raysCol));
				HPEN pRed   = CreatePen(PS_SOLID, linewidth, tcols[0]);
				HPEN pGreen = CreatePen(PS_SOLID, linewidth, tcols[1]);
				HPEN pBlue  = CreatePen(PS_SOLID, linewidth, tcols[2]);
				HPEN lPens[] = { pRed, pGreen, pBlue };
				float lShiftsXY[] = {-1, 0, 1};
				float lShiftsZ[]  = {-1, 0, -abbAchromatic*2+1};

				SetROP2(thdc, R2_MERGEPEN);

				float d1 = rect.right*0.65f;
				float d0 = (float)ellX;
				int ppos = (int)((d1-d0)*distPattern+d0);

				HPEN pDepth = CreatePen(PS_SOLID, 1, emab->colDepthLine);
				SelectObject(thdc, pDepth);
				MoveToEx(thdc, ppos, rect.bottom*2/20, NULL);
				LineTo(  thdc, ppos, rect.bottom*18/20);

				for (int i=0; i<3; i++)
				{
					int lStartX = ellX;
					int lStartY = (int)( ellY - rect.bottom*0.3f - rect.bottom*0.08*lShiftsXY[i]*abbShiftXY);
					int lHalfX = (int)(rect.right*0.65f + rect.right*0.08f*lShiftsZ[i]*abbShiftZ);
					int lHalfY = ellY;
					int lStopX = (int)(rect.right*0.95f);
					float lRatio = (float)(lHalfY-lStartY)/(float)(lHalfX-lStartX);
					int lStopY = (int)(lStartY+(lStopX-lStartX)*lRatio);

					SelectObject(thdc, lPens[i]);
					if (drawNice)
					{
						MyLine(thdc, lStartX, lStartY,				lStopX, lStopY,				1.25f, tcols[i], bckgCol);
						MyLine(thdc, lStartX, ellY-(lStartY-ellY),	lStopX, ellY-(lStopY-ellY),	1.25f, tcols[i], bckgCol);
					}
					else
					{
						MoveToEx(thdc, lStartX, lStartY, NULL);
						LineTo(thdc, lStopX, lStopY);
						MoveToEx(thdc, lStartX, ellY-(lStartY-ellY), NULL);
						LineTo(thdc, lStopX, ellY-(lStopY-ellY));
					}
				}

				SetROP2(thdc, R2_COPYPEN);

				// draw borders
				POINT eBorder1[] = { {0,   rect.bottom-1 }, {0,   0}, {rect.right,   0} };
				POINT eBorder2[] = { {rect.right-1,   1}, {rect.right-1,   rect.bottom-1}, {0, rect.bottom-1} };
				HPEN luPen = CreatePen(PS_SOLID, 1, emab->colBorderLeftUp);
				HPEN drPen = CreatePen(PS_SOLID, 1, emab->colBorderDownRight);
				SelectObject(thdc, luPen);
				Polyline(thdc, eBorder1, 3);
				SelectObject(thdc, drPen);
				Polyline(thdc, eBorder2, 3);

				// clean stuff
				SelectObject(thdc, hOldBrush);
				SelectObject(thdc, hOldPen);
				DeleteObject(ellBrush);
				DeleteObject(ellPen);
				DeleteObject(pRed);
				DeleteObject(pGreen);
				DeleteObject(pBlue);
				DeleteObject(pDepth);
				DeleteObject(hCenterPen);
				DeleteObject(luPen);
				DeleteObject(drPen);
				DeleteObject(bkgBrush);
				DeleteObject(bkgPen);

				// copy from back buffer and free resources
				BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);//0xCC0020); 
				SelectObject(thdc, oldBMP);
				DeleteObject(hbmp);
				DeleteDC(thdc);
				EndPaint(hwnd, &ps);
			}
			break;
		default:
			break;
	}

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//------------------------------------------------------------------------------------------------------------

void MyLine(HDC hdc, int X1, int Y1, int X2, int Y2, float w, COLORREF c, COLORREF b)
{
	int x1 = X1;
	int x2 = X2;
	int y1 = Y1;
	int y2 = Y2;

	float fx1 = (float)x1;
	float fx2 = (float)x2;
	float fy1 = (float)y1;
	float fy2 = (float)y2;

	int ox = abs(x2-x1)+1;
	int oy = abs(y2-y1)+1;

	float wp2 = w;

	unsigned char ocr = GetRValue(c);
	unsigned char ocg = GetGValue(c);
	unsigned char ocb = GetBValue(c);
	unsigned char obr = GetRValue(b);
	unsigned char obg = GetGValue(b);
	unsigned char obb = GetBValue(b);

	if (ox>=oy)
	{
		int xx1 = min(x1,x2);
		int xx2 = max(x1,x2);
		for (int x=xx1; x<=xx2; x++)
		{
			float fx = (float)x;
			float fy = (fx-fx1)/(fx2-fx1)*(fy2-fy1)+fy1;
			int iy = (int)fy;
			float o1 = fabs(fy-iy+1);
			float o2 = fabs(fy-iy);
			float o3 = fabs(fy-iy-1);
			float w1 = max(0, min(1, wp2-o1));
			float w2 = max(0, min(1, wp2-o2));
			float w3 = max(0, min(1, wp2-o3));

			COLORREF c1 = RGB(ocr*w1+obr*(1-w1), ocg*w1+obg*(1-w1), ocb*w1+obb*(1-w1));
			COLORREF c2 = RGB(ocr*w2+obr*(1-w2), ocg*w2+obg*(1-w2), ocb*w2+obb*(1-w2));
			COLORREF c3 = RGB(ocr*w3+obr*(1-w3), ocg*w3+obg*(1-w3), ocb*w3+obb*(1-w3));
			
			SetPixel(hdc, x, iy-1, c1);
			SetPixel(hdc, x, iy  , c2);
			SetPixel(hdc, x, iy+1, c3);
		}
	}
	else
	{
		int yy1 = min(y1,y2);
		int yy2 = max(y1,y2);
		for (int y=yy1; y<=yy2; y++)
		{
			float fy = (float)y;
			float fx = (fy-fy1)/(fy2-fy1)*(fx2-fx1)+fx1;
			int ix = (int)fx;
			float o1 = fabs(fx-ix+1);
			float o2 = fabs(fx-ix);
			float o3 = fabs(fx-ix-1);
			float w1 = max(0, min(1, wp2-o1));
			float w2 = max(0, min(1, wp2-o2));
			float w3 = max(0, min(1, wp2-o3));

			COLORREF c1 = RGB(ocr*w1+obr*(1-w1), ocg*w1+obg*(1-w1), ocb*w1+obb*(1-w1));
			COLORREF c2 = RGB(ocr*w2+obr*(1-w2), ocg*w2+obg*(1-w2), ocb*w2+obb*(1-w2));
			COLORREF c3 = RGB(ocr*w3+obr*(1-w3), ocg*w3+obg*(1-w3), ocb*w3+obb*(1-w3));
			
			SetPixel(hdc, ix-1, y, c1);
			SetPixel(hdc, ix  , y, c2);
			SetPixel(hdc, ix+1, y, c3);
		}
	}
}

//---------------------------------------------------------------------------------------------------------------

void MyEllipse(HDC hdc, int X1, int Y1, int X2, int Y2, COLORREF c, COLORREF b)
{
	int x1 = min(X1,X2);
	int x2 = max(X1,X2);
	int y1 = min(Y1,Y2);
	int y2 = max(Y1,Y2);

	float xr = (x2-x1+1)*0.5f;
	float yr = (y2-y1+1)*0.5f;
	float pxr = 1.0f/xr;
	float pyr = 1.0f/yr;
	float xc = (x1+x2)*0.5f;
	float yc = (y1+y2)*0.5f;

	float mr = min(xr,yr);
	float mr2 = mr*mr;

	unsigned char ocr = GetRValue(c);
	unsigned char ocg = GetGValue(c);
	unsigned char ocb = GetBValue(c);
	unsigned char obr = GetRValue(b);
	unsigned char obg = GetGValue(b);
	unsigned char obb = GetBValue(b);

	#define EAA 5
	float paa = 1.0f/EAA;

	for (int y=y1; y<=y2; y++)
	{
		for (int x=x1; x<=x2; x++)
		{
			int cntr = 0;
			for (int iay=0; iay<EAA; iay++)
				for (int iax=0; iax<EAA; iax++)
				{
					float dx = (x-xc-0.5f+paa*iax)*pxr;
					float dy = (y-yc-0.5f+paa*iay)*pyr;
					float d2 = dx*dx+dy*dy;
					if (d2<=1.0f)
						cntr++;
				}

			if (cntr>0)
			{
				float wgc = cntr*paa*paa;
				float wgb = 1-wgc;

				unsigned char cfr = (unsigned char)(wgc*ocr + wgb*obr);
				unsigned char cfg = (unsigned char)(wgc*ocg + wgb*obg);
				unsigned char cfb = (unsigned char)(wgc*ocb + wgb*obb);

				COLORREF cf = RGB(cfr, cfg, cfb);
				SetPixel(hdc, x,y,cf);
			}
		}
	}


}

//---------------------------------------------------------------------------------------------------------------

void MyEllipseSSE(HDC hdc, int X1, int Y1, int X2, int Y2, COLORREF c, COLORREF b)
{
	int x1 = min(X1,X2);
	int x2 = max(X1,X2);
	int y1 = min(Y1,Y2);
	int y2 = max(Y1,Y2);

	float xr = (x2-x1+1)*0.5f;
	float yr = (y2-y1+1)*0.5f;
	float pxr = 1.0f/xr;
	float pyr = 1.0f/yr;
	float xc = (x1+x2)*0.5f;
	float yc = (y1+y2)*0.5f;

	float mr = min(xr,yr);
	float mr2 = mr*mr;

	unsigned char ocr = GetRValue(c);
	unsigned char ocg = GetGValue(c);
	unsigned char ocb = GetBValue(c);
	unsigned char obr = GetRValue(b);
	unsigned char obg = GetGValue(b);
	unsigned char obb = GetBValue(b);

	__m128 sseOCR = _mm_set1_ps((float)ocr);
	__m128 sseOCG = _mm_set1_ps((float)ocg);
	__m128 sseOCB = _mm_set1_ps((float)ocb);
	__m128 sseOBR = _mm_set1_ps((float)obr);
	__m128 sseOBG = _mm_set1_ps((float)obg);
	__m128 sseOBB = _mm_set1_ps((float)obb);

	#define EAA 5
	float paa = 1.0f/EAA;
	float paa2 = paa*paa;



	int ssex = x2-x1;
	ssex = (ssex/4+1);
	int ssex4 = ssex*4;
	float xc_sse = xc-x1;
	__m128 ssepxr = _mm_set1_ps(pxr);
	__m128 ssepyr = _mm_set1_ps(pyr);
	__m128 ssepaa = _mm_set1_ps(paa);
	__m128 ssepaa2 = _mm_set1_ps(paa2);
	__m128 ssexc = _mm_set1_ps(xc_sse+0.5f);
	__m128 sseyc = _mm_set1_ps(yc+0.5f);
	__m128 sseone = _mm_set1_ps(1.0f);
	__m128 ssezero = _mm_set1_ps(0.0f);
	

	__m128i * tabI = (__m128i*)_aligned_malloc(ssex4*(y2-y1+1)*sizeof(int), 16);

	__m128 xincr = _mm_setr_ps(4.0f, 4.0f, 4.0f, 4.0f);

	for (int y=y1; y<=y2; y++)
	{
		__m128 ssexpos = _mm_setr_ps(0.0f, 1.0f, 2.0f, 3.0f);
		__m128 sseypos = _mm_set1_ps((float)y);

		for (int x=0; x<ssex; x++)
		{
			__m128 ssecntr = _mm_setzero_ps();
			__m128 ssedisty = _mm_sub_ps(sseypos, sseyc);
			
			for (int iay=0; iay<EAA; iay++)
			{
				ssedisty = _mm_add_ps(ssedisty, ssepaa);
				__m128 ssedy = _mm_mul_ps(ssedisty, ssepyr);
				__m128 ssedistx = _mm_sub_ps(ssexpos, ssexc);
				for (int iax=0; iax<EAA; iax++)
				{
					ssedistx = _mm_add_ps(ssedistx, ssepaa);
					__m128 ssedx = _mm_mul_ps(ssedistx, ssepxr);
					__m128 ssed2 = _mm_add_ps(_mm_mul_ps(ssedx, ssedx), _mm_mul_ps(ssedy, ssedy));

					__m128 sseBitmask = _mm_cmple_ps(ssed2, sseone);
					__m128 sseIncrCntr = _mm_and_ps(sseBitmask, sseone);
					__m128 sseotherwise = _mm_andnot_ps(sseBitmask, ssezero);
					sseIncrCntr = _mm_or_ps(sseIncrCntr, sseotherwise);
					ssecntr = _mm_add_ps(ssecntr, sseIncrCntr);
				}
			}
			ssecntr = _mm_mul_ps(ssecntr, ssepaa2);
			
			__m128 oth = _mm_sub_ps(sseone, ssecntr);
			__m128 redF   = _mm_add_ps(_mm_mul_ps(ssecntr, sseOCR), _mm_mul_ps(oth, sseOBR));
			__m128 greenF = _mm_add_ps(_mm_mul_ps(ssecntr, sseOCG), _mm_mul_ps(oth, sseOBG));
			__m128 blueF  = _mm_add_ps(_mm_mul_ps(ssecntr, sseOCB), _mm_mul_ps(oth, sseOBB));

			__m128i redI   = _mm_cvtps_epi32(redF);			// convert to int
			__m128i greenI = _mm_cvtps_epi32(greenF);		// convert to int
			__m128i blueI  = _mm_cvtps_epi32(blueF);		// convert to int
			greenI = _mm_slli_epi32(greenI, 8);				// 
			//blueI  = _mm_slli_epi32(blueI, 16);			// RGB macro version
			redI   = _mm_slli_epi32(redI, 16);				// RGB bitmap version
			__m128i sseres = _mm_or_si128(redI, greenI);
			sseres = _mm_or_si128(sseres, blueI);
			tabI[(y-y1)*ssex+x] = sseres;

			ssexpos = _mm_add_ps(ssexpos, xincr);
		}
	}

	int cx = ssex4;
	int cy = y2-y1+1;
	HDC mydc = CreateCompatibleDC(hdc); 
	HBITMAP hbmp = CreateCompatibleBitmap(hdc, cx, cy);
	HBITMAP oldBmp = (HBITMAP)SelectObject(mydc, hbmp);

	BITMAPINFOHEADER bmih;
	ZeroMemory(&bmih,sizeof(bmih));
    bmih.biSize = sizeof(BITMAPINFOHEADER);
    bmih.biBitCount = 32;
    bmih.biClrImportant = 0;
    bmih.biClrUsed = 0;
	bmih.biCompression = BI_RGB;
    bmih.biHeight = y2-y1+1;
    bmih.biWidth = ssex4;
    bmih.biPlanes = 1;
    bmih.biSizeImage = ssex4*4*cy;
    bmih.biXPelsPerMeter = 0;
    bmih.biYPelsPerMeter = 0;

	BITMAPINFO bmi;
	bmi.bmiHeader = bmih;
	int ccc = SetDIBits(mydc, hbmp, 0, cy, tabI, (BITMAPINFO*)&bmih, DIB_RGB_COLORS);
	BitBlt(hdc, x1, y1, cx, cy, mydc, 0, 0, SRCCOPY);
	SelectObject(mydc, oldBmp);
	DeleteObject(hbmp);
	DeleteDC(mydc);

	_aligned_free(tabI);
}

//---------------------------------------------------------------------------------------------------------------

void MyEllipse2(HDC hdc, int X1, int Y1, int X2, int Y2, float a, COLORREF c, COLORREF b)
{
	unsigned char ocr = GetRValue(c);
	unsigned char ocg = GetGValue(c);
	unsigned char ocb = GetBValue(c);
	unsigned char obr = GetRValue(b);
	unsigned char obg = GetGValue(b);
	unsigned char obb = GetBValue(b);

	int x1 = (int)((X1+X2)/2-a/2-1);
	int x2 = (int)((X1+X2)/2+a/2+1);
	int y1 = (int)((Y1+Y2)/2-a/2-1);
	int y2 = (int)((Y1+Y2)/2+a/2+1);

	for (int y=y1; y<=y2; y++)
	{
		for (int x=x1; x<=x2; x++)
		{
			float dx1 = (float)(x-X1);
			float dx2 = (float)(x-X2);
			float dy1 = (float)(y-Y1);
			float dy2 = (float)(y-Y2);
			float d1_2 = dx1*dx1 + dy1*dy1;
			float d2_2 = dx2*dx2 + dy2*dy2;
			float d1 = sqrt(d1_2);
			float d2 = sqrt(d2_2);
			float wd = d1+d2;

			float wgc = min(1, max(0, a-wd));
			wgc = sqrt(wgc);
			float wgb = 1-wgc;

			unsigned char cfr = (unsigned char)(wgc*ocr + wgb*obr);
			unsigned char cfg = (unsigned char)(wgc*ocg + wgb*obg);
			unsigned char cfb = (unsigned char)(wgc*ocb + wgb*obb);

			COLORREF cf = RGB(cfr, cfg, cfb);
			if (wd<a)
				SetPixel(hdc, x,y,cf);
		}
	}
}

//---------------------------------------------------------------------------------------------------------------
