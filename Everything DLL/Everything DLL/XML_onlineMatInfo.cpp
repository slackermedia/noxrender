#include "XML_IO.h"
#include "DLL.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "log.h"

//------------------------------------------------------------------------------------------------------

XMLonlineMatInfo::XMLonlineMatInfo()
{
	errLine = -1;
	fSize = 0;
	numMats = 0;
	totalResults = -1; 
}

//------------------------------------------------------------------------------------------------------

XMLonlineMatInfo::~XMLonlineMatInfo()
{
	for (int i=0; i<7; i++)
		infos[i].clearAll();
}

//------------------------------------------------------------------------------------------------------

bool XMLonlineMatInfo::loadFile(char *filename)
{
	fSize = fileSize(filename);

	xmlNodePtr cur;
	xmlNodePtr curRoot;

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
		return false;

	curRoot = xmlDocGetRootElement(xmlDoc);
	if (curRoot == NULL) 
	{	// is empty
		xmlFreeDoc(xmlDoc);
		return false;
	}

	numMats = 0;

	if (xmlStrcmp(curRoot->name, (const xmlChar *) "materials")) 
	{	// bad root
		xmlFreeDoc(xmlDoc);
		return false;
	}

	xmlChar * key = xmlGetProp(curRoot, (xmlChar*)"count");
	if (key != NULL  &&  strlen((char*)key)>0)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if (!key   ||   (addr == (char *)key)  || ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			if (key)
				xmlFree(key);
			errLine = xmlGetLineNo(curRoot);
			return false;
		}

		xmlFree(key);
		totalResults = (int)a;
	}


	cur = curRoot->xmlChildrenNode;
	while (cur != NULL) 
	{
		// check <materialinfo>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"materialinfo")))
		{
			if (!parseMaterialInfo(cur, numMats++))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// go to next
		cur = cur->next;
	}

	if (numMats > 7)
		numMats = 7;

	xmlFreeDoc(xmlDoc);

	return true;
}

//------------------------------------------------------------------------------------------------------

bool XMLonlineMatInfo::parseMaterialInfo(xmlNodePtr curNode, int num)
{
	if (num >= 7)
		return true;

	char * pc;
	xmlNodePtr cur = curNode->xmlChildrenNode;
	while (cur != NULL) 
	{
		// check <id>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"id")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			infos[num].id = (int)a;
		}

		// check <name>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
			{
				infos[num].name = copyString(pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// check <author>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"author")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
			{
				infos[num].author = copyString(pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// check <filesize>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"filesize")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			infos[num].filesize = (int)a;
		}

		// check <category>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"category")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			infos[num].category = (int)a;
		}

		// check <layers>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"layers")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			infos[num].layers = (int)a;
		}

		// check <textures>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"textures")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			infos[num].textures = (int)a;
		}

		// check <description>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"description")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
			{
				infos[num].description = copyString(pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// check <filename>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"filename")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
			{
				infos[num].filename = copyString(pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// check <imagename>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"imagename")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 0)
			{}
			else
			{
				infos[num].imagename = copyString(pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// go to next
		cur = cur->next;
	}

	return true;
}

//------------------------------------------------------------------------------------------------------
