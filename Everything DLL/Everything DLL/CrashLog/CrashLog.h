#ifndef __crash_log_h__
#define __crash_log_h__

#include <windows.h>
#include <stdio.h>

class CrashLog
{
public:
	static CrashLog * getInstance();
	static LONG __stdcall CrashLog::exceptionFilter(EXCEPTION_POINTERS* exceptionPtrs);
	bool setCrashLogFolder(char * folder);
	bool setCrashRunApp(char * file, char * args);
	bool setSymbolPath(char * folder);
	bool runSender();
	~CrashLog();
private:
	void printExceptionInfo(FILE * file, DWORD code);
	void printSymbol(FILE * file, PVOID addr);
	INT_PTR getImageBase(char * filename);
	void logAddress(FILE * file, PVOID addr);
	char * crashLogFilename;
	char * symbolPath;
	char * appFilename;
	char * appArgs;
	void Initialize();
	CrashLog();
};

#endif
