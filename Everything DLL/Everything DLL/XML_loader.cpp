#include "XML_IO.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "raytracer.h"
#include <math.h>
#include "log.h"
#include <locale.h>
#include "valuesMinMax.h"

//----------------------------------------------------------------------------------------------------------------

bool getFloatFromString(char * str, float &res)
{
	CHECK(str);
	CHECK(strlen(str)>0);

	char * opc = str;
	char * addr;
	double a = _strtod_l((char *)str, &addr, noxLocale);
	if ( (addr == (char *)str)  ||  ((unsigned int)(addr-(char*)str) < strlen((char *)str)) )
		return false;

	res = (float)(a);

	return true;
}

XMLScene::XMLScene()
{
	errLine = -1;
	curMeshNum = 0;
	fileSize = 0;
	eNumTris = 1;
	cTri = 0;
	directory = NULL;
}

XMLScene::~XMLScene()
{
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::LoadScene(char *filename)
{
	HANDLE hFile = CreateFile(filename, STANDARD_RIGHTS_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		DWORD up;
		DWORD fs = GetFileSize(hFile, &up);
		fileSize = fs;
		CloseHandle(hFile);
	}

	directory = getDirectory(filename);

	xmlNodePtr cur;

	Raytracer::getInstance()->curScenePtr->notifyProgress("Parsing XML...", 100.0f);

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
		return false;

	cur = xmlDocGetRootElement(xmlDoc);
	if (cur == NULL) 
	{	// is empty
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "scene")) 
	{	// bad root
		xmlFreeDoc(xmlDoc);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->sscene.activeCamera = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) 
	{
		// check <objects>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"objects")))
		{
			if (!parseObjects(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <materials>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"materials")))
		{
			if (!parseMaterials(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <camera>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"camera")))
		{
			char * cdir = getDirectory(filename);
			if (!cdir)
				cdir = ".";
			if (!parseCamera(cur, cdir))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <sky>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"sky")))
		{
			if (!parseSky(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <user_colors>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"user_colors")))
		{
			if (!parseUserColors(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <renderer_settings>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"renderer_settings")))
		{
			if (!parseRendererSettings(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <scene_info>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"scene_info")))
		{
			if (!parseSceneInfo(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <blend>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"blend")))
		{
			if (!parseBlend(cur, rtr->curScenePtr->blendSettings))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// check <user_presets>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"user_presets")))
		{
			if (!parseUserPresets(cur))
			{
				xmlFreeDoc(xmlDoc);
				return false;
			}
		}

		// go to next
		cur = cur->next;
	}


	if (rtr->curScenePtr->cameras.objCount < 1)
	{
		MessageBox(0, "No camera found in scene.", "Error", MB_ICONERROR);
		xmlFreeDoc(xmlDoc);
		return false;
	}

	rtr->curScenePtr->notifyProgress("XML processed OK", 100.0f);

	xmlFreeDoc(xmlDoc);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseObjects(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	xmlChar *key;

	eNumTris = fileSize / XML_TRI_ROW_SIZE;
	eNumTris = max(1, eNumTris);
	key = xmlGetProp(curNode, (xmlChar*)"totalTris");
	if ((key) && strlen((char *)key) != 0)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
		}
		else
		{
			eNumTris = (int)a;
			if (eNumTris < 1)
				eNumTris = fileSize / XML_TRI_ROW_SIZE;
			eNumTris = max(1, eNumTris);
		}
	}
	if (key)
		xmlFree(key);


	key = xmlGetProp(curNode, (xmlChar*)"scale");
	if ((key) && strlen((char *)key) != 0)
	{
		char * addr;


		double a = _strtod_l((char *)key, &addr, noxLocale);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to double)
		}
		else
		{
			float scale = (float)a;
			scale = min(NOX_SCALE_GEOM_MAX, max(NOX_SCALE_GEOM_MIN, scale));
			Scene * scene = Raytracer::getInstance()->curScenePtr;
			scene->sscene.scaleScene = scale;
			scene->sscene.perScaleScene = 1.0f/scale;
		}
	}
	if (key)
		xmlFree(key);


	cur = curNode->xmlChildrenNode;
	while (cur != NULL) 
	{
		// search for <mesh>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"mesh")))
		{
			errLine = xmlGetLineNo(cur);
			xmlFree(key);
			return false;
		}
		 
		// search for <mesh_src>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"mesh_src")))
		{
			if (!parseMeshSrc(cur))
				return false;
		}

		// search for <instance>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"instance")))
		{
			if (!parseInstance(cur))
				return false;
		}

		// go to next child
		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

void initMeshInst(void * addr)
{
	new (addr) MeshInstance();
}

bool XMLScene::parseInstance(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	Raytracer * rtr = Raytracer::getInstance();

	// get attribute 'id'
	xmlChar * idkey = xmlGetProp(curNode, (xmlChar*)"id");
	if (!idkey  ||  strlen((char *)idkey) == 0)
	{
		xmlFree(idkey);
		errLine = xmlGetLineNo(curNode);
		return false;
	}

	char * addr;
	__int64 a = _strtoi64((char *)idkey, &addr, 10);
	if ( (addr == (char *)idkey)  ||  ((unsigned int)(addr-(char*)idkey) < strlen((char *)idkey)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(idkey);
		return false;
	}
	xmlFree(idkey);
	if (a<0)
	{	// negative index
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	int instMeshID = (int)a;

	MeshInstance * t_inst = new MeshInstance();
	rtr->curScenePtr->instances.add(*t_inst, initMeshInst);
	rtr->curScenePtr->instances.createArray();
	MeshInstance * inst = &(rtr->curScenePtr->instances[rtr->curScenePtr->instances.objCount-1]);
	new(inst) MeshInstance(); 
	inst->meshID = -1;
	inst->instSrcID = instMeshID;
	free(t_inst);
	t_inst = NULL;

	while (cur != NULL) 
	{
		// search for <name>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			inst->name = copyString(pc);
			if (pc)
				xmlFree(pc);
		}

		// search for <matrix>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"matrix")))
		{
			Matrix4d m;
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc)!=128)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}

			for (int i=0; i<4; i++)
				for (int j=0; j<4; j++)
				{
					float f;
					if (DecHex::hex8ToFloat((unsigned char *)pc+((i*4+j)*8), f))
						m.M[i][j] = f;
					else
					{
						errLine = xmlGetLineNo(cur);
						xmlFree(pc);
						return false;
					}
				}
			if (pc)
				xmlFree(pc);
			inst->matrix_transform = m;
			m.getInverted(inst->matrix_inv);
		}

		// search for <materials>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"materials")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);

			// get attribute 'count'
			xmlChar * ckey = xmlGetProp(cur, (xmlChar*)"count");
			if (!ckey  ||  strlen((char *)ckey) == 0)
			{
				xmlFree(idkey);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char * addr;
			__int64 a = _strtoi64((char *)ckey, &addr, 10);
			if ( (addr == (char *)ckey)  ||  ((unsigned int)(addr-(char*)ckey) < strlen((char *)ckey)) )
			{	// conversion error (to int)
				errLine = xmlGetLineNo(cur);
				xmlFree(ckey);
				return false;
			}
			xmlFree(ckey);
			int count = (int)a;
			if (count<1)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}

			inst->matsFromFile.freeList();

			for (int i=0; i<count; i++)
			{
				unsigned int ii;
				if (DecHex::hex8ToUint((unsigned char *)pc+(i*8), ii))
				{
					inst->matsFromFile.add(ii);
				}
				else
				{
					errLine = xmlGetLineNo(cur);
					xmlFree(pc);
					return false;
				}
			}
			inst->matsFromFile.createArray();

			if (pc)
				xmlFree(pc);
		}

		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseMeshSrc(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	triCounter = 0;
	cbMin = Point3d( BIGFLOAT,  BIGFLOAT,  BIGFLOAT);
	cbMax = Point3d(-BIGFLOAT, -BIGFLOAT, -BIGFLOAT);
	Raytracer * rtr = Raytracer::getInstance();

	Mesh em;
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	// get attribute 'id'
	xmlChar * idkey = xmlGetProp(curNode, (xmlChar*)"id");
	if (!idkey  ||  strlen((char *)idkey) == 0)
	{
		xmlFree(idkey);
		errLine = xmlGetLineNo(curNode);
		return false;
	}

	char * addr;
	__int64 a = _strtoi64((char *)idkey, &addr, 10);
	if ( (addr == (char *)idkey)  ||  ((unsigned int)(addr-(char*)idkey) < strlen((char *)idkey)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(idkey);
		return false;
	}
	xmlFree(idkey);
	if (a<0)
	{	// negative index
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	int instMeshID = (int)a;
	em.instSrcID = (int)a;


	rtr->curScenePtr->meshes.add(em);
	rtr->curScenePtr->meshes.createArray();
	Mesh * mesh = &(rtr->curScenePtr->meshes[rtr->curScenePtr->meshes.objCount-1]);
	bool firstTri = true;

	while (cur != NULL) 
	{
		// search for <name>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			mesh->assignName(pc);
			if (pc)
				xmlFree(pc);
		}

		// search for <tri>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tri")))
		{
			if (firstTri)
			{
				mesh->firstTri = cTri;
				firstTri = false;
			}
			mesh->lastTri = cTri;
			if (!parseTriangle(cur))
				return false;
		}
		 
		// go to next child
		cur = cur->next;
	}

	curMeshNum++;

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseMesh(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	triCounter = 0;
	cbMin = Point3d( BIGFLOAT,  BIGFLOAT,  BIGFLOAT);
	cbMax = Point3d(-BIGFLOAT, -BIGFLOAT, -BIGFLOAT);
	Raytracer * rtr = Raytracer::getInstance();
	Mesh em;

	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	rtr->curScenePtr->meshes.add(em);
	rtr->curScenePtr->meshes.createArray();
	Mesh * mesh = &(rtr->curScenePtr->meshes[rtr->curScenePtr->meshes.objCount-1]);
	bool firstTri = true;

	while (cur != NULL) 
	{
		// search for <name>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			mesh->assignName(pc);
			if (pc)
				xmlFree(pc);
		}

		// search for <tri>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tri")))
		{
			if (firstTri)
			{
				mesh->firstTri = cTri;
				firstTri = false;
			}
			mesh->lastTri = cTri;
			if (!parseTriangle(cur))
				return false;
		}
		 
		// go to next child
		cur = cur->next;
	}

	curMeshNum++;

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseTriangle(xmlNodePtr curNode)
{
	char * pc;
	float f;

	pc = (char *)xmlNodeListGetString(xmlDoc, curNode->xmlChildrenNode, 1);
	char * opc = pc;
	if (!pc   ||  strlen(pc) != XML_TRI_SIZE)
	{
		errLine = xmlGetLineNo(curNode);
		if (pc)
			xmlFree(pc);
		return false;
	}

	int i,j;
	Point3d verts[3];
	Vector3d norms[3];
	Point2d uv[3];

	// first 1 uint -> material ID in instance lib
	unsigned int ui1;
	if (!DecHex::hex8ToUint((unsigned char *)pc, ui1) )
	{
		errLine = xmlGetLineNo(curNode);
		xmlFree(opc);
		return false;
	}
	pc = pc + 8;

	// second 1 uint -> material ID
	unsigned int ui2;
	if (!DecHex::hex8ToUint((unsigned char *)pc, ui2) )
	{
		errLine = xmlGetLineNo(curNode);
		xmlFree(opc);
		return false;
	}
	pc = pc + 8;

	// first 9 floats are vertices
	for(j=0; j<3; j++)
	{
		for(i=0; i<3; i++)
		{
			if (DecHex::hex8ToFloat((unsigned char *)pc+((j*3+i)*8), f))
				verts[j].V[i] = f;
			else
			{
				errLine = xmlGetLineNo(curNode);
				xmlFree(opc);
				return false;
			}
		}
	}

	// next 9 floats are normals
	for(j=3; j<6; j++)
	{
		for(i=0; i<3; i++)
		{
			if (DecHex::hex8ToFloat((unsigned char *)pc+((j*3+i)*8), f))
				norms[j-3].V[i] = f;
			else
			{
				errLine = xmlGetLineNo(curNode);
				xmlFree(opc);
				return false;
			}
		}
	}


	if (norms[0].length() < 0.2f)
		MessageBox(0, "normal 0?", "", 0);
	if (norms[1].length() < 0.2f)
		MessageBox(0, "normal 0?", "", 0);
	if (norms[2].length() < 0.2f)
		MessageBox(0, "normal 0?", "", 0);

	norms[0].normalize();
	norms[1].normalize();
	norms[2].normalize();

	// next 6 floats are uv
	for(j=0; j<3; j++)
	{
		for(i=0; i<2; i++)
		{
			if (DecHex::hex8ToFloat((unsigned char *)pc+((18+j*2+i)*8), f))
				uv[j].V[i] = f;
			else
			{
				errLine = xmlGetLineNo(curNode);
				xmlFree(opc);
				return false;
			}
		}
	}

	xmlFree(opc);

	// add triangle to engine
	#ifdef FORCE_SSE
		Triangle * tri = (Triangle *)_aligned_malloc(sizeof(Triangle), 16);
		*tri = Triangle(verts[0], verts[1], verts[2], norms[0], norms[1], norms[2], uv[0], uv[1], uv[2], 0);
	#else
		Triangle * tri = new Triangle(verts[0], verts[1], verts[2], norms[0], norms[1], norms[2], uv[0], uv[1], uv[2], curMeshNum);
	#endif
	tri->matInInst = ui1;
	Raytracer * rtr = Raytracer::getInstance();

	rtr->curScenePtr->triangles.add(*tri);
	delete tri;

	triCounter++;

	float maxx, maxy, maxz, minx, miny, minz;
	maxx = max(max(verts[0].x, verts[1].x), verts[2].x);
	maxy = max(max(verts[0].y, verts[1].y), verts[2].y);
	maxz = max(max(verts[0].z, verts[1].z), verts[2].z);
	minx = min(min(verts[0].x, verts[1].x), verts[2].x);
	miny = min(min(verts[0].y, verts[1].y), verts[2].y);
	minz = min(min(verts[0].z, verts[1].z), verts[2].z);
	if (cbMin.x > minx)
		cbMin.x = minx;
	if (cbMin.y > miny)
		cbMin.y = miny;
	if (cbMin.z > minz)
		cbMin.z = minz;
	if (cbMax.x < maxx)
		cbMax.x = maxx;
	if (cbMax.y < maxy)
		cbMax.y = maxy;
	if (cbMax.z < maxz)
		cbMax.z = maxz;

	cTri++;
	static clock_t lastticks = clock();
	clock_t curticks = clock();
	clock_t diff = curticks - lastticks;
	if (diff > 100)
	{
		rtr->curScenePtr->notifyProgress("Loading polygons", cTri/(float)eNumTris*100.0f);
		lastticks = curticks;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseCamera(xmlNodePtr curNode, char * curPath)
{
	xmlNodePtr cur;
	xmlChar * key;
	cur = curNode->xmlChildrenNode;
	char * pc;
	char * opc;
	float f;
	bool there_was_rightdir = false;

	Camera * cam = new Camera();

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	// get width
	key = xmlGetProp(curNode, (xmlChar*)"width");
	if (key)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			errLine = xmlGetLineNo(curNode);
			xmlFree(key);
			return false;
		}
		xmlFree(key);
		if (a < 1)
		{
			errLine = xmlGetLineNo(curNode);
			return false;
		}
		cam->width = (int)a;
	}

	// get height
	key = xmlGetProp(curNode, (xmlChar*)"height");
	if (key)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			errLine = xmlGetLineNo(curNode);
			xmlFree(key);
			return false;
		}
		xmlFree(key);
		if (a < 1)
		{
			errLine = xmlGetLineNo(curNode);
			return false;
		}
		cam->height = (int)a;
	}

	// get antialiasing
	key = xmlGetProp(curNode, (xmlChar*)"antialiasing");
	if (key)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			errLine = xmlGetLineNo(curNode);
			xmlFree(key);
			return false;
		}
		xmlFree(key);
		if (a < 1)
		{
			errLine = xmlGetLineNo(curNode);
			return false;
		}
		cam->aa = (int)a;
	}

	while (cur != NULL) 
	{
		// search for <name>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			cam->assignName(pc);
			if (pc)
				xmlFree(pc);
		}

		// search for <pos>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"pos")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			opc = pc;
			if (!pc  ||  strlen(pc) != 24)
			{
				errLine = xmlGetLineNo(cur);
				delete cam;
				if (pc)
					xmlFree(opc);
				return false;
			}
			bool OK = true;
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
				OK = false;
			cam->position.x = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+8, f))
				OK = false;
			cam->position.y = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+16, f))
				OK = false;
			cam->position.z = f;
			xmlFree(opc);
			if (!OK)
			{
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
		}
		 
		// search for <dir>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"dir")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			opc = pc;
			if (!pc  ||  strlen(pc) != 24)
			{
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			bool OK = true;
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
				OK = false;
			cam->direction.x = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+8, f))
				OK = false;
			cam->direction.y = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+16, f))
				OK = false;
			cam->direction.z = f;
			xmlFree(opc);
			if (!OK)
			{
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			cam->direction.normalize();
		}

		// search for <updir>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"updir")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			opc = pc;
			if (!opc  ||  strlen(pc) != 24)
			{
				if (opc)
					xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			bool OK = true;
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
				OK = false;
			cam->upDir.x = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+8, f))
				OK = false;
			cam->upDir.y = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+16, f))
				OK = false;
			cam->upDir.z = f;
			xmlFree(opc);
			if (!OK)
			{
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			cam->upDir.normalize();
		}

		// search for <rightdir>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"rightdir")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			opc = pc;
			if (!opc  ||  strlen(pc) != 24)
			{
				if (opc)
					xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			bool OK = true;
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
				OK = false;
			cam->rightDir.x = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+8, f))
				OK = false;
			cam->rightDir.y = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+16, f))
				OK = false;
			cam->rightDir.z = f;
			xmlFree(opc);
			if (!OK)
			{
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			cam->rightDir.normalize();
			there_was_rightdir = true;
		}

		// search for <angle>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"angle")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) != 8)
			{
				xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
			{
				xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				delete cam;
				return false;
			}
			xmlFree(pc);
			cam->angle = f;
		}

		// search for <iso>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"iso")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				cam->ISO = (float)a;
				cam->iMod.setISO_camera((float)a);
			}
			xmlFree(opc);
		}

		// search for <aperture>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"aperture")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				cam->aperture = (float)a;
			}
			xmlFree(opc);
		}

		// search for <shutter>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"shutter")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				cam->shutter = (float)a;
			}
			xmlFree(opc);
		}

		// search for <focusdist>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"focusdist")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				cam->focusDist = (float)a;
			}
			xmlFree(opc);
		}

		// search for <autoexposure>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"autoexposure")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cam->autoexposure = true;
			else
				cam->autoexposure = false;
			xmlFree(pc);
		}

		// search for <autofocus>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"autofocus")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cam->autoFocus= true;
			else
				cam->autoFocus= false;
			xmlFree(pc);
		}

		// search for <autoexptype>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"autoexptype")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			cam->autoexposureType = (int)a;
			xmlFree(pc);
		}

		// search for <bladesnumber>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bladesnumber")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			cam->apBladesNum = min(16, max(5, (int)a));
			xmlFree(pc);
		}

		// search for <bladesangle>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bladesangle")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				cam->apBladesAngle = min(360, max(0, (float)a));
			}
			xmlFree(opc);
		}

		// search for <bladesround>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bladesround")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cam->apBladesRound = true;
			else
				cam->apBladesRound = false;
			xmlFree(pc);
		}

		// search for <bladesradius>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bladesradius")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				cam->apBladesRadius = min(10, max(1, (float)a));
			}
			xmlFree(opc);
		}

		// search for <bokeh_ring_balance>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_ring_balance")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			int ring_balance = min(NOX_CAM_BOKEH_BALANCE_MAX, max(NOX_CAM_BOKEH_BALANCE_MIN, (int)a));
			cam->diaphSampler.intBokehRingBalance = ring_balance;
				if (ring_balance>=0)
				cam->diaphSampler.bokehRingBalance = ring_balance+1.0f;
			else
				cam->diaphSampler.bokehRingBalance = 1.0f + ring_balance/21.0f;

			xmlFree(pc);
		}

		// search for <bokeh_ring_size>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_ring_size")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			int ring_size = min(NOX_CAM_BOKEH_RING_SIZE_MAX, max(NOX_CAM_BOKEH_RING_SIZE_MIN, (int)a));
			cam->diaphSampler.intBokehRingSize = ring_size;
			cam->diaphSampler.bokehRingSize = ring_size*0.01f;

			xmlFree(pc);
		}

		// search for <bokeh_vignette>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_vignette")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			int bokeh_vignette = min(NOX_CAM_BOKEH_VIGNETTE_MAX, max(NOX_CAM_BOKEH_VIGNETTE_MIN, (int)a));
			cam->diaphSampler.intBokehVignette= bokeh_vignette;
			cam->diaphSampler.bokehVignette = bokeh_vignette*0.01f;

			xmlFree(pc);
		}

		// search for <bokeh_flatten>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_flatten")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			int bokeh_flatten = min(NOX_CAM_BOKEH_FLATTEN_MAX, max(NOX_CAM_BOKEH_FLATTEN_MIN, (int)a));
			cam->diaphSampler.intBokehFlatten = bokeh_flatten;
			cam->diaphSampler.bokehFlatten = bokeh_flatten*0.01f;

			xmlFree(pc);
		}

		// search for <dof_on>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"dof_on")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cam->dofOnTemp = true;
			else
				cam->dofOnTemp = false;
			xmlFree(pc);
		}

		// search for <final_post>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"final_post")))
		{
			if (!parseCameraFinalValues(cur, cam))
			{
				delete cam;
				return false;
			}
		}


		// search for <imagedata>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"imagedata")))
		{
			if (!parseImageData(cur, curPath, cam))
			{
				delete cam;
				return false;
			}
		}

		// go to next child
		cur = cur->next;
	}

	if (!parseCameraPostValues(curNode, cam))
	{
		delete cam;
		return false;
	}

	if (!there_was_rightdir)
	{
		cam->rightDir = cam->direction ^ cam->upDir;
		cam->rightDir.normalize();
	}
	rtr->curScenePtr->cameras.add(cam);
	rtr->curScenePtr->cameras.createArray();

	key = xmlGetProp(curNode, (xmlChar*)"active");
	if (key)
	{
		if ((!xmlStrcmp(key, (const xmlChar *)"yes")))
		{
			rtr->curScenePtr->sscene.activeCamera = rtr->curScenePtr->cameras.objCount-1;
		}
		xmlFree(key);
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseCameraPostValues(xmlNodePtr curNode, Camera * cam)
{
	CHECK(curNode);
	CHECK(cam);
	return parseCameraPostValues(curNode, &(cam->iMod));
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseCameraPostValues(xmlNodePtr curNode, ImageModifier * iMod)
{
	if (!curNode)
		return false;

	CHECK(iMod);

	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;

	float addR = 0.0f;
	float addG = 0.0f;
	float addB = 0.0f;
	char * pc;

	while (cur != NULL) 
	{
		// search for <iso_ev_correction>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"iso_ev_correction")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				iMod->setISO_EV_compensation((float)a);
				iMod->evalMultiplierFromISO_and_EV();
			}
			xmlFree(opc);
		}

		// search for <gi_ev_correction>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"gi_ev_correction")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				iMod->setGICompensation((float)a);
			}
			xmlFree(opc);
		}

		// search for <tone>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tone")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				iMod->setToneMappingValue((float)a);
			}
			xmlFree(opc);
		}

		// search for <use_tone>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"use_tone")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			// value not used anymore
			xmlFree(pc);
		}

		// search for <hot_pixels>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"hot_pixels")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				iMod->setDotsEnabled(true);
			else
				iMod->setDotsEnabled(false);
			xmlFree(pc);
		}

		// search for <gamma>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"gamma")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				iMod->setGamma((float)a);
			}
			xmlFree(opc);
		}

		// search for <brightness>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"brightness")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				iMod->setBrightness((float)a);
			}
			xmlFree(opc);
		}

		// search for <contrast>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"contrast")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				iMod->setContrast((float)a);
			}
			xmlFree(opc);
		}

		// search for <saturation>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"saturation")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				iMod->setSaturation((float)a);
			}
			xmlFree(opc);
		}

		// search for <addred>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"addred")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				addR = (float)a;
			}
			xmlFree(opc);
		}

		// search for <addgreen>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"addgreen")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				addG = (float)a;
			}
			xmlFree(opc);
		}

		// search for <addblue>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"addblue")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				addB = (float)a;
			}
			xmlFree(opc);
		}

		// search for <temperature>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"temperature")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			iMod->setTemperature((float)(int)a);
			xmlFree(pc);
		}

		// search for <vignette>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"vignette")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			iMod->setVignette((int)a);
			xmlFree(pc);
		}

		// search for <response_function>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"response_function")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			iMod->setResponseFunctionNumber((int)a);
			xmlFree(pc);
		}

		// search for <filter>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"filter")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			iMod->setFilter(max(0, min(6, (int)a)));
			xmlFree(pc);
		}

		// search for <curve>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"curve")))
		{
			//if (!parseCameraPostCurve(cur, cam))
			if (!parseCameraPostCurve(cur, iMod))
				return false;
		}	

		// go to next child
		cur = cur->next;
	}

	iMod->setRGBAdd(addR, addG, addB);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseCameraPostCurve(xmlNodePtr curNode, Camera * cam)
{
	CHECK(curNode);
	CHECK(cam);
	return parseCameraPostCurve(curNode, &cam->iMod);
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseCameraPostCurve(xmlNodePtr curNode, ImageModifier * iMod)
{
	if (!curNode)
		return false;

	xmlNodePtr cur, cur1;
	cur = curNode->xmlChildrenNode;
	char * pc;

	CHECK(iMod);
	iMod->delCurveAllPoints();

	bool cr = false;
	bool cg = false;
	bool cb = false;
	bool cl = false;

	while (cur != NULL) 
	{
		// search for <r> <g> <b> <l>
		if (	(!xmlStrcmp(cur->name, (const xmlChar *)"r"))   ||
				(!xmlStrcmp(cur->name, (const xmlChar *)"g"))   ||
				(!xmlStrcmp(cur->name, (const xmlChar *)"b"))   ||
				(!xmlStrcmp(cur->name, (const xmlChar *)"l"))) 
		{
			int chosen = -1;
			if (!xmlStrcmp(cur->name, (const xmlChar *)"r"))		chosen = 0;
			if (!xmlStrcmp(cur->name, (const xmlChar *)"g"))		chosen = 1;
			if (!xmlStrcmp(cur->name, (const xmlChar *)"b"))		chosen = 2;
			if (!xmlStrcmp(cur->name, (const xmlChar *)"l"))		chosen = 3;

			bool xExists = false;
			bool yExists = false;
			int x,y;

			cur1 = cur->xmlChildrenNode;
			while (cur1 != NULL) 
			{
				// search for <x>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"x")))
				{
					pc = (char *)xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					if (!pc   ||   strlen((char *)pc) == 0)
					{
						if (pc)
							xmlFree(pc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}

					char * addr;
					__int64 a = _strtoi64((char *)pc, &addr, 10);
					if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
					{	// conversion error (to int)
						if (pc)
							xmlFree(pc);
						errLine = xmlGetLineNo(curNode);
						return false;
					}
					x = (int)a;
					xExists = true;
					xmlFree(pc);
				}

				// search for <y>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"y")))
				{
					pc = (char *)xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					if (!pc   ||   strlen((char *)pc) == 0)
					{
						if (pc)
							xmlFree(pc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}

					char * addr;
					__int64 a = _strtoi64((char *)pc, &addr, 10);
					if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
					{	// conversion error (to int)
						if (pc)
							xmlFree(pc);
						errLine = xmlGetLineNo(curNode);
						return false;
					}
					y = (int)a;
					yExists = true;
					xmlFree(pc);
				}
				cur1 = cur1->next;
			}
			if (xExists  &&  yExists)
			{
				if (chosen == 0)
					iMod->addRedPoint(x,y);
				if (chosen == 1)
					iMod->addGreenPoint(x,y);
				if (chosen == 2)
					iMod->addBluePoint(x,y);
				if (chosen == 3)
					iMod->addLuminancePoint(x,y);
			}

		}

		// search for <cr>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"cr")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cr = true;
			else
				cr = false;
			xmlFree(pc);
		}

		// search for <cg>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"cg")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cg = true;
			else
				cg = false;
			xmlFree(pc);
		}

		// search for <cb>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"cb")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cb = true;
			else
				cb = false;
			xmlFree(pc);
		}

		// search for <cl>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"cl")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				cl = true;
			else
				cl = false;
			xmlFree(pc);
		}

		// go to next child
		cur = cur->next;
	}

	if (cl)
		iMod->setCurveCheckboxes(false, false, false, true);
	else
		iMod->setCurveCheckboxes(cr, cg, cb, false);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseImageData(xmlNodePtr curNode, char * curPath, Camera * cam)
{
	if (!curNode   ||   !cam)
		return false;

	int w,h,cseed;
	xmlChar *key;

	// get width
	key = xmlGetProp(curNode, (xmlChar*)"width");
	if (key)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			errLine = xmlGetLineNo(curNode);
			xmlFree(key);
			return false;
		}
		w = (int)a;
		xmlFree(key);
	}
	else
	{
		errLine = xmlGetLineNo(curNode);
		return false;
	}

	// get height
	key = xmlGetProp(curNode, (xmlChar*)"height");
	if (key)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			errLine = xmlGetLineNo(curNode);
			xmlFree(key);
			return false;
		}
		h = (int)a;
		xmlFree(key);
	}
	else
	{
		errLine = xmlGetLineNo(curNode);
		return false;
	}

	// non negative must be
	if (w < 1    ||    h < 1)
	{
		errLine = xmlGetLineNo(curNode);
		return false;
	}

	// get rtime
	key = xmlGetProp(curNode, (xmlChar*)"rtime");
	if (key)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			errLine = xmlGetLineNo(curNode);
			xmlFree(key);
			return false;
		}
		cam->rendTime = (int)a;
		xmlFree(key);
	}

	// get cseed
	key = xmlGetProp(curNode, (xmlChar*)"cseed");
	if (key)
	{
		char * addr;
		__int64 a = _strtoi64((char *)key, &addr, 10);
		if ( (addr == (char *)key)  ||  ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
		{	// conversion error (to int)
			errLine = xmlGetLineNo(curNode);
			xmlFree(key);
			return false;
		}
		cseed = (int)a;
		xmlFree(key);
		cam->maxHalton = cseed;
	}

	xmlNodePtr cur = curNode->xmlChildrenNode;

	while (cur != NULL) 
	{
		int bb = -1;
		// search for <file1d> etc
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file1d")))
			bb = 0;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file2d")))
			bb = 1;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file3d")))
			bb = 2;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file4d")))
			bb = 3;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file5d")))
			bb = 4;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file6d")))
			bb = 5;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file7d")))
			bb = 6;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file8d")))
			bb = 7;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file9d")))
			bb = 8;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file10d")))
			bb = 9;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file11d")))
			bb = 10;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file12d")))
			bb = 11;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file13d")))
			bb = 12;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file14d")))
			bb = 13;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file15d")))
			bb = 14;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file16d")))
			bb = 15;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file1g")))
			bb = 16;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file2g")))
			bb = 17;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file3g")))
			bb = 18;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file4g")))
			bb = 19;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file5g")))
			bb = 20;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file6g")))
			bb = 21;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file7g")))
			bb = 22;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file8g")))
			bb = 23;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file9g")))
			bb = 24;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file10g")))
			bb = 25;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file11g")))
			bb = 26;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file12g")))
			bb = 27;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file13g")))
			bb = 28;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file14g")))
			bb = 29;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file15g")))
			bb = 30;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file16g")))
			bb = 31;
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"file_z")))
			bb = 32;

		if (bb>-1  &&  bb<32)
		{
			// get file name
			key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char fname[2048];
			sprintf_s(fname, 2048, "%s\\%s", curPath, key);

			ImageBuffer * fileBuffer;
			if (bb>15)
				fileBuffer = &(cam->blendBuffGI[bb-16]);
			else
				fileBuffer = &(cam->blendBuffDirect[bb]);
			fileBuffer->allocBuffer(w,h);

			cam->blendBits |= (1<<(bb%16));

			if (!loadImageData(fname, fileBuffer))
			{
				fileBuffer->freeBuffer();
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}
		if (bb==32)
		{
			// get file name
			key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char fname[2048];
			sprintf_s(fname, 2048, "%s\\%s", curPath, key);

			cam->depthBuf->allocBuffer(w,h, true, true);

			if (!loadFloatData(fname, cam->depthBuf))
			{
				cam->depthBuf->freeBuffer();
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		cur = cur->next;
	}

	if (!cam->imgBuff)
		cam->imgBuff = new ImageBuffer();
	cam->imgBuff->allocBuffer(w,h);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseMaterials(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;

	while (cur != NULL) 
	{
		// search for <material>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"material")))
		{
			if (!parseMaterial(cur))
				return false;
		}
		cur = cur->next;
	}
	Raytracer::getInstance()->curScenePtr->mats.createArray();
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseMaterial(xmlNodePtr curNode)
{
	// WARNING loadMaterial does not use this method
	xmlNodePtr cur;
	xmlChar *key;

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->abortThreadPtr  &&  *rtr->curScenePtr->abortThreadPtr)
	{
		Logger::add("Aborted by user!!!");
		return false;
	}

	// get id
	key = xmlGetProp(curNode, (xmlChar*)"id");
	if (!key   ||   strlen((char *)key) != 8)
	{
		errLine = xmlGetLineNo(curNode);
		if (key)
			xmlFree(key);
		return false;
	}

	unsigned int ui;
	if (!DecHex::hex8ToUint((unsigned char *)key, ui) )
	{
		xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	xmlFree(key);

	MaterialNox * mat = new MaterialNox();
	mat->id = ui;
	mat->hasEmitters = false;
	mat->hasShade = false;
	mat->hasFakeGlass = false;
	mat->hasInvisibleEmitter = false;

	cur = curNode->xmlChildrenNode;
	int nLayers = 0;

	while (cur != NULL) 
	{
		// search for <preview>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"preview")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!mat->preview)
				mat->preview = new ImageByteBuffer();
			if (!parsePreview(cur, mat->preview))
			{
				delete mat;
				return false;
			}
		}

		// search for <name>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 1)
			{}
			else
			{
				int ll = (int)strlen(pc);
				mat->name = (char *)malloc(ll+1);
				if (mat->name)
					sprintf_s(mat->name, ll+1, "%s", pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// search for <skyportal>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"skyportal")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->isSkyPortal = true;
			else
				mat->isSkyPortal = false;
			xmlFree(pc);
		}

		// search for <matteshadow>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"matteshadow")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->isMatteShadow = true;
			else
				mat->isMatteShadow = false;
			xmlFree(pc);
		}

		// search for <displacement_subdivs>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_subdivs")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			mat->displ_subdivs_lvl = (int)a;
			xmlFree(pc);
		}

		// search for <displacement_depth>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_depth")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mat->displ_depth = (float)a;
			}
			xmlFree(opc);
		}

		// search for <displacement_shift>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_shift")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mat->displ_shift = (float)a;
			}
			xmlFree(opc);
		}

		// search for <displacement_continuity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_continuity")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->displ_continuity = true;
			else
				mat->displ_continuity = false;
			xmlFree(pc);
		}

		// search for <displacement_ignore_scale>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_ignore_scale")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->displ_ignore_scale = true;
			else
				mat->displ_ignore_scale = false;
			xmlFree(pc);
		}

		// search for <displacement_normal_mode>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_normal_mode")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			mat->displ_normal_mode = (int)a;
			xmlFree(pc);
		}

		// search for <opacity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"opacity")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mat->opacity= (float)(a * 0.01);
			}
			xmlFree(opc);
		}

		// search for <tex_opacity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_opacity")))
		{
			if (!parseTexture(cur, mat, NULL))
				return false;
		}

		// search for <tex_displacement>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_displacement")))
		{
			if (!parseTexture(cur, mat, NULL))
				return false;
		}

		// search for <blendlayer>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"blendlayer")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			mat->blendIndex = min(15, max(0, (int)a-1));
			xmlFree(pc);
		}

		// search for <layer>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"layer")))
		{
			nLayers++;
			key = xmlGetProp(cur, (xmlChar*)"type");
			if (key == NULL)
			{	// no type specified
				errLine = xmlGetLineNo(cur);
				delete mat;
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"shade"))
			{	// type is shade
				if (!parseMaterialShadeLayer(cur, mat) )
				{
					delete mat;
					xmlFree(key);
					return false;
				}
			}
			else
			{
				if ((!xmlStrcmp(key, (const xmlChar *)"emission")))
				{	// type is emission
					if (!parseMaterialEmissionLayer(cur, mat) )
					{
						delete mat;
						xmlFree(key);
						return false;
					}
				}
				else
				{	// another type - bad
					errLine = xmlGetLineNo(cur);
					delete mat;
					xmlFree(key);
					return false;
				}
			}
			xmlFree(key);
		}
		cur = cur->next;
	}


	if (nLayers > 0)
	{
		mat->layers.createArray();
		Raytracer::getInstance()->curScenePtr->mats.add(mat);
		Raytracer::getInstance()->curScenePtr->mats.createArray();
	}
	else
	{
		delete mat;
		return false;
	}

	rtr->curScenePtr->notifyProgress("Loading materials...", 0.0f);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseTexture(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay)
{
	if (!curNode)
		return false;

	Logger::add("parseTexture");

	// get attribute type
	xmlChar * key = xmlGetProp(curNode, (xmlChar*)"type");
	if (!key  ||  strlen((char *)key) == 0)
	{
		xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	if (strcmp((char*)key, "file"))	// nothing more supported
	{
		xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}

	int slot = -1;
	if (!strcmp((char*)curNode->name, "tex_refl0"))
	{
		slot = LAYER_TEX_SLOT_COLOR0; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_refl90"))
	{
		slot = LAYER_TEX_SLOT_COLOR90; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_rough"))
	{
		slot = LAYER_TEX_SLOT_ROUGHNESS; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_weight"))
	{
		slot = LAYER_TEX_SLOT_WEIGHT; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_light"))
	{
		slot = LAYER_TEX_SLOT_LIGHT; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_normal"))
	{
		slot = LAYER_TEX_SLOT_NORMAL; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_transm"))
	{
		slot = LAYER_TEX_SLOT_TRANSM; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_aniso"))
	{
		slot = LAYER_TEX_SLOT_ANISO; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_aniso_angle"))
	{
		slot = LAYER_TEX_SLOT_ANISO_ANGLE; 
		CHECK(mlay);
	}
	if (!strcmp((char*)curNode->name, "tex_opacity"))
	{
		slot = MAT_TEX_SLOT_OPACITY; 
		CHECK(mat);
	}
	if (!strcmp((char*)curNode->name, "tex_displacement"))
	{
		slot = MAT_TEX_SLOT_DISPLACEMENT; 
		CHECK(mat);
	}

	if (!parseTextureTypeFile(curNode, mat, mlay, slot))
	{
		return false;
	}


	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseTextureTypeFile(xmlNodePtr curNode, MaterialNox * mat, MatLayer * mlay, int slot)
{
	if (!curNode)
		return false;

	Logger::add("parseTextureTypeFile");

	// attribute enabled
	xmlChar * key = xmlGetProp(curNode, (xmlChar*)"enabled");
	if (!key  ||  strlen((char *)key) == 0)
	{
		xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	bool enabled = false;
	if (!strcmp((char*)key, "yes"))
		enabled = true;

	TextureInstance * texptr = NULL;
	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:
			CHECK(mlay);
			mlay->use_tex_col0 = enabled;
			texptr = &(mlay->tex_col0);
			break;
		case LAYER_TEX_SLOT_COLOR90:
			CHECK(mlay);
			mlay->use_tex_col90 = enabled;
			texptr = &(mlay->tex_col90);
			break;
		case LAYER_TEX_SLOT_ROUGHNESS:
			CHECK(mlay);
			mlay->use_tex_rough = enabled;
			texptr = &(mlay->tex_rough);
			break;
		case LAYER_TEX_SLOT_WEIGHT:
			CHECK(mlay);
			mlay->use_tex_weight = enabled;
			texptr = &(mlay->tex_weight);
			break;
		case LAYER_TEX_SLOT_LIGHT:
			CHECK(mlay);
			mlay->use_tex_light = enabled;
			texptr = &(mlay->tex_light);
			break;
		case LAYER_TEX_SLOT_NORMAL:
			CHECK(mlay);
			mlay->use_tex_normal = enabled;
			texptr = &(mlay->tex_normal);
			break;
		case LAYER_TEX_SLOT_TRANSM:
			CHECK(mlay);
			mlay->use_tex_transm = enabled;
			texptr = &(mlay->tex_transm);
			break;
		case LAYER_TEX_SLOT_ANISO:
			CHECK(mlay);
			mlay->use_tex_aniso = enabled;
			texptr = &(mlay->tex_aniso);
			break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:
			CHECK(mlay);
			mlay->use_tex_aniso_angle= enabled;
			texptr = &(mlay->tex_aniso_angle);
			break;
		case MAT_TEX_SLOT_OPACITY:
			CHECK(mat);
			mat->use_tex_opacity = enabled;
			texptr = &(mat->tex_opacity);
			break;
		case MAT_TEX_SLOT_DISPLACEMENT:
			CHECK(mat);
			mat->use_tex_displacement = enabled;
			texptr = &(mat->tex_displacement);
			break;
	}

	xmlNodePtr cur = curNode->xmlChildrenNode;
	while (cur != NULL) 
	{
		// <filename>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"filename")))
		{
			xmlChar * key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}

			Raytracer * rtr = Raytracer::getInstance();
			char * fname = getValidTexPath((char*)key);
			int id = rtr->curScenePtr->texManager.addTexture(fname, true, NULL, (LAYER_TEX_SLOT_NORMAL==slot));
			texptr->managerID = id;
			if (id>-1)
			{
				Texture * tex = rtr->curScenePtr->texManager.textures[id];
				texptr->filename = copyString(tex->filename);
				texptr->fullfilename = copyString(tex->fullfilename);
				texptr->texScPtr = tex;
			}

			xmlFree(key);
		}

		// <post>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"post")))
		{
			if (!parseTexturePost(cur, texptr))
				return false;
		}

		// <post_normal>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"post_normal")))
		{
			if (!parseTextureNormalPost(cur, texptr))
				return false;
		}

		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseTexturePost(xmlNodePtr curNode, TextureInstance * texptr)
{
	if (!curNode   ||   !texptr)
		return false;

	Logger::add("parseTexturePost");

	xmlNodePtr cur = curNode->xmlChildrenNode;
	while (cur != NULL) 
	{
		// <brightness>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"brightness")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.brightness);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <contrast>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"contrast")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.contrast);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <saturation>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"saturation")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.saturation);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <hue>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"hue")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.hue);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <red>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"red")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.red);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <green>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"green")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.green);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <blue>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"blue")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.blue);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <gamma>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"gamma")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->texMod.gamma);
			xmlFree(pc);
			texptr->texMod.updateGamma(texptr->texMod.gamma);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// <invert>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"invert")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			if (!strcmp(pc, "yes"))
				texptr->texMod.invert = true;
			else
				texptr->texMod.invert = false;

			xmlFree(pc);
		}

		// <filter>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"filter")))
		{
			xmlChar * key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}

			if (!strcmp((char*)key, "1"))
				texptr->texMod.interpolateProbe = true;
			else
				texptr->texMod.interpolateProbe = false;

			xmlFree(key);
		}

		cur = cur->next;
	}

	texptr->texMod.clipValues();

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseTextureNormalPost(xmlNodePtr curNode, TextureInstance * texptr)
{
	if (!curNode   ||   !texptr)
		return false;

	xmlNodePtr cur = curNode->xmlChildrenNode;
	while (cur != NULL) 
	{
		// <pn_power>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"pn_power")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			bool floatOK = getFloatFromString(pc, texptr->normMod.powerEV);
			xmlFree(pc);

			if (!floatOK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			texptr->normMod.power = pow(2.0f, texptr->normMod.powerEV);
		}

		// <pn_invert_x>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"pn_invert_x")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			if (!strcmp(pc, "yes"))
				texptr->normMod.invertX = true;
			else
				texptr->normMod.invertX = false;

			xmlFree(pc);
		}

		// <pn_invert_y>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"pn_invert_y")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			if (!strcmp(pc, "yes"))
				texptr->normMod.invertY = true;
			else
				texptr->normMod.invertY = false;

			xmlFree(pc);
		}

		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseMaterialShadeLayer(xmlNodePtr curNode, MaterialNox * mat)
{
	char * pc;
	xmlChar *key;
	xmlNodePtr cur;

	cur = curNode->xmlChildrenNode;

	MatLayer mLayer;
	mLayer.type = MatLayer::TYPE_SHADE;
	mat->hasShade = true;

	// get contribution
	key = xmlGetProp(curNode, (xmlChar*)"contribution");
	if (!key  ||  strlen((char *)key) == 0)
	{
		xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	char * addr;
	__int64 a = _strtoi64((char *)key, &addr, 10);
	if ( (addr == (char *)key)  || ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(key);
		return false;
	}
	xmlFree(key);
	if (a<0 || a>100)
	{	// out of bound
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	mLayer.contribution = (int)a;

	while (cur != NULL) 
	{
		// search for <diffuse>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"diffuse")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc  ||  strlen(pc) != 6)
			{	// must be 6 chars
				if (opc)
					xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			// get color
			unsigned char c;
			bool tOK = true;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc,  c) )
				tOK = false;
			mLayer.refl0.r = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+2,  c) )
				tOK = false;
			mLayer.refl0.g = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+4,  c) )
				tOK = false;
			mLayer.refl0.b = c/255.0f;

			xmlFree(opc);

			if (!tOK)
			{	// error, some chars wasn't hex values
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// search for <diffuse90>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"diffuse90")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc  ||  strlen(pc) != 6)
			{	// must be 6 chars
				if (opc)
					xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			// get color
			unsigned char c;
			bool tOK = true;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc,  c) )
				tOK = false;
			mLayer.refl90.r = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+2,  c) )
				tOK = false;
			mLayer.refl90.g = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+4,  c) )
				tOK = false;
			mLayer.refl90.b = c/255.0f;

			xmlFree(opc);

			if (!tOK)
			{	// some chars wasn't hex values
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// search for <connect90>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"connect90")))
		{
			key = xmlGetProp(cur, (xmlChar*)"active");
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"yes"))
				mLayer.connect90 = true;
			else
				mLayer.connect90 = false;
			xmlFree(key);
		}

		// search for <roughness>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"roughness")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mLayer.roughness = (float)(a * 0.01);
			}
			xmlFree(opc);
		}

		// search for <anisotropy>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"anisotropy")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mLayer.anisotropy = (float)(a * 0.01);
			}
			xmlFree(opc);
		}

		// search for <anisotropy_angle>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"anisotropy_angle")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mLayer.aniso_angle = min(NOX_MAT_ANISO_ANGLE_MAX, max(NOX_MAT_ANISO_ANGLE_MIN, (float)(a)));
			}
			xmlFree(opc);
		}

		// search for <tex_refl0>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_refl0")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_refl90>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_refl90")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_rough>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_rough")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_weight>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_weight")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_light>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_light")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_transm>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_transm")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_aniso>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_aniso")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_aniso_angle>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_aniso_angle")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_normal>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_normal")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}


		// search for <transmission>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"transmission")))
		{
			key = xmlGetProp(cur, (xmlChar*)"active");
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"yes"))
				mLayer.transmOn = true;
			else
				mLayer.transmOn = false;
			xmlFree(key);

			xmlNodePtr cur1 = cur->xmlChildrenNode;
			while (cur1 != NULL)	// inside <transmission>
			{
				// search for <transmcolor>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"transmcolor")))
				{
					pc = (char *)xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					char * opc = pc;
					if (!pc  ||  strlen(pc) != 6)
					{	// must be 6 chars
						if (opc)
							xmlFree(opc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}

					// get color
					unsigned char c;
					bool tOK = true;
					if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc,  c) )
						tOK = false;
					mLayer.transmColor.r = c/255.0f;
					if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+2,  c) )
						tOK = false;
					mLayer.transmColor.g = c/255.0f;
					if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+4,  c) )
						tOK = false;
					mLayer.transmColor.b = c/255.0f;

					xmlFree(opc);

					if (!tOK)
					{	// some chars wasn't hex values
						errLine = xmlGetLineNo(cur1);
						return false;
					}
				}

				// search for <fakeglass>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"fakeglass")))
				{
					key = xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					if (!key  ||  strlen((char*)key)<1)
					{
						errLine = xmlGetLineNo(cur1);
						xmlFree(key);
						return false;
					}
					if (!xmlStrcmp(key, (const xmlChar *)"yes"))
					{
						mLayer.fakeGlass = true;
						if (mLayer.transmOn)
							mat->hasFakeGlass = true;
					}
					else
						mLayer.fakeGlass = false;
					xmlFree(key);
				}

				// search for <dispersion_on>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"dispersion_on")))
				{
					key = xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					if (!key  ||  strlen((char*)key)<1)
					{
						errLine = xmlGetLineNo(cur1);
						xmlFree(key);
						return false;
					}
					if (!xmlStrcmp(key, (const xmlChar *)"yes"))
					{
						mLayer.dispersionOn = true;
					}
					else
						mLayer.dispersionOn = false;
					xmlFree(key);
				}

				// search for <dispersion_val>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"dispersion_val")))
				{
					pc = (char *)xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					char * opc = pc;
					if (!pc   ||   strlen((char *)pc) == 0)
					{
						if (pc)
							xmlFree(pc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}

					char * addr;
					double a = _strtod_l((char *)pc, &addr, noxLocale);
					if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
					{	// conversion error (to float)
						xmlFree(opc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}
					else
					{
						mLayer.dispersionValue = (float)a;
					}
					xmlFree(opc);
				}


				// search for <absorption>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"absorption")))
				{
					key = xmlGetProp(cur1, (xmlChar*)"active");
					if (!key  ||  strlen((char*)key)<1)
					{
						errLine = xmlGetLineNo(cur1);
						xmlFree(key);
						return false;
					}
					if (!xmlStrcmp(key, (const xmlChar *)"yes"))
						mLayer.absOn = true;
					else
						mLayer.absOn = false;
					xmlFree(key);

					xmlNodePtr cur2 = cur1->xmlChildrenNode;
					while (cur2 != NULL)		// inside <absorption>
					{
						// search for <abscolor>
						if ((!xmlStrcmp(cur2->name, (const xmlChar *)"abscolor")))
						{
							pc = (char *)xmlNodeListGetString(xmlDoc, cur2->xmlChildrenNode, 1);
							char * opc = pc;
							if (!pc  ||  strlen(pc) != 6)
							{	// must be 6 chars
								if (opc)
									xmlFree(opc);
								errLine = xmlGetLineNo(cur2);
								return false;
							}

							// get color
							unsigned char c;
							bool tOK = true;
							if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc,  c) )
								tOK = false;
							mLayer.absColor.r = c/255.0f;
							if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+2,  c) )
								tOK = false;
							mLayer.absColor.g = c/255.0f;
							if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+4,  c) )
								tOK = false;
							mLayer.absColor.b = c/255.0f;

							xmlFree(opc);

							if (!tOK)
							{	// some chars wasn't hex values
								errLine = xmlGetLineNo(cur2);
								return false;
							}
						}

						// search for <distance>
						if ((!xmlStrcmp(cur2->name, (const xmlChar *)"distance")))
						{
							pc = (char *)xmlNodeListGetString(xmlDoc, cur2->xmlChildrenNode, 1);
							char * opc = pc;
							if (!pc   ||   strlen((char *)pc) == 0)
							{
								if (pc)
									xmlFree(pc);
								errLine = xmlGetLineNo(cur2);
								return false;
							}

							char * addr;
							double a = _strtod_l((char *)pc, &addr, noxLocale);
							if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
							{	// conversion error (to float)
								xmlFree(opc);
								errLine = xmlGetLineNo(cur2);
								return false;
							}
							else
							{
								mLayer.absDist= (float)a;
							}
							xmlFree(opc);
						}

						cur2 = cur2->next;
					}
				}
				cur1 = cur1->next;
			}
		}

		// search for <sss>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"sss")))
		{
			key = xmlGetProp(cur, (xmlChar*)"active");
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"yes"))
				mLayer.sssON = true;
			else
				mLayer.sssON = false;
			xmlFree(key);

			xmlNodePtr cur1 = cur->xmlChildrenNode;
			while (cur1 != NULL)	// inside <sss>
			{
				// search for <sss_density>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"sss_density")))
				{
					pc = (char *)xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					char * opc = pc;
					if (!pc   ||   strlen((char *)pc) == 0)
					{
						if (pc)
							xmlFree(pc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}

					char * addr;
					double a = _strtod_l((char *)pc, &addr, noxLocale);
					if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
					{	// conversion error (to float)
						xmlFree(opc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}
					else
					{
						mLayer.sssDens = (float)a;
					}
					xmlFree(opc);
				}

				// search for <sss_coll_dir>
				if ((!xmlStrcmp(cur1->name, (const xmlChar *)"sss_coll_dir")))
				{
					pc = (char *)xmlNodeListGetString(xmlDoc, cur1->xmlChildrenNode, 1);
					char * opc = pc;
					if (!pc   ||   strlen((char *)pc) == 0)
					{
						if (pc)
							xmlFree(pc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}

					char * addr;
					double a = _strtod_l((char *)pc, &addr, noxLocale);
					if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
					{	// conversion error (to float)
						xmlFree(opc);
						errLine = xmlGetLineNo(cur1);
						return false;
					}
					else
					{
						mLayer.sssCollDir = (float)a;
					}
					xmlFree(opc);
				}

				cur1 = cur1->next;
			}
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"fresnel")))
		{
			if (!parseFresnel(cur, mLayer.fresnel))
				return false;
		}

		cur = cur->next;
	}

	mat->layers.add(mLayer, &(MatLayer::runConstructor));
	mat->layers.createArray();
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseFresnel(xmlNodePtr curNode, Fresnel & fresnel)
{	char * pc;
	xmlNodePtr cur;

	cur = curNode->xmlChildrenNode;

	while (cur != NULL) 
	{
		// search for <ior>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ior")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fresnel.IOR = (float)a;
			}
			xmlFree(opc);
		}

		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseMaterialEmissionLayer(xmlNodePtr curNode, MaterialNox * mat)
{
	char * pc;
	xmlChar *key;
	xmlNodePtr cur;

	cur = curNode->xmlChildrenNode;

	MatLayer mLayer;
	mLayer.type = MatLayer::TYPE_EMITTER;

	// get contribution
	key = xmlGetProp(curNode, (xmlChar*)"contribution");
	if (!key   ||   strlen((char *)key) == 0)
	{
		if (key)
			xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	char * addr;
	__int64 a = _strtoi64((char *)key, &addr, 10);
	if ( (addr == (char *)key)  || ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(key);
		return false;
	}
	xmlFree(key);
	if (a<0 || a>100)
	{	// out of bound
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	mLayer.contribution = (int)a;

	while (cur != NULL) 
	{
		// search for <emission>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"emission")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) != 6)
			{	// must be 6 chars
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			// get color
			unsigned char c;
			bool tOK = true;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc,  c) )
				tOK = false;
			mLayer.color.r = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+2,  c) )
				tOK = false;
			mLayer.color.g = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+4,  c) )
				tOK = false;
			mLayer.color.b = c/255.0f;

			xmlFree(pc);

			if (!tOK)
			{	// some chars wasn't hex values
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// search for <power>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"power")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) != 8)
			{	// must be 8 chars
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			float f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
			{
				xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			xmlFree(pc);
			mLayer.power = f;
		}

		// search for <temperature>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"temperature")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			
			mLayer.emitterTemperature = min(10000, max(1000, (int)a));

			xmlFree(pc);
		}

		// search for <unit>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"unit")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			mLayer.unit = MatLayer::EMISSION_WATT;
			if ((!xmlStrcmp((const xmlChar *)pc, (const xmlChar *)"w")))
				mLayer.unit = MatLayer::EMISSION_WATT;
			if ((!xmlStrcmp((const xmlChar *)pc, (const xmlChar *)"w/m2")))
				mLayer.unit = MatLayer::EMISSION_WATT_PER_SQR_METER;
			if ((!xmlStrcmp((const xmlChar *)pc, (const xmlChar *)"lm")))
				mLayer.unit = MatLayer::EMISSION_LUMEN;
			if ((!xmlStrcmp((const xmlChar *)pc, (const xmlChar *)"lx")))
				mLayer.unit = MatLayer::EMISSION_LUX;


			xmlFree(pc);
		}

		// search for <invisible>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"invisible")))
		{
			key = xmlGetProp(cur, (xmlChar*)"active");
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"yes"))
			{
				mLayer.emissInvisible = true;
				mat->hasInvisibleEmitter = true;
			}
			else
				mLayer.emissInvisible = false;
			xmlFree(key);
		}

		// search for <tex_light>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_light")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <tex_weight>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_weight")))
		{
			if (!parseTexture(cur, mat, &mLayer))
				return false;
		}

		// search for <ies>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ies")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			IesNOX * ies = new IesNOX();

			char * fname = getValidTexPath((char*)pc);

			bool ok = ies->parseFile(fname);
			if (ok)
			{
				mLayer.ies = ies;
			}
			else
			{
				delete ies;
			}
			xmlFree(pc);
		}

		cur = cur->next;
	}

	mat->layers.add(mLayer, &(MatLayer::runConstructor));
	mat->layers.createArray();

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseBlend(xmlNodePtr curNode, BlendSettings &blend)
{
	if (!curNode)
		return false;
	xmlNodePtr cur = curNode->xmlChildrenNode;
	
	while (cur != NULL) 
	{
		int n=-1;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight1"))
			n=0;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight2"))
			n=1;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight3"))
			n=2;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight4"))
			n=3;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight5"))
			n=4;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight6"))
			n=5;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight7"))
			n=6;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight8"))
			n=7;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight9"))
			n=8;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight10"))
			n=9;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight11"))
			n=10;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight12"))
			n=11;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight13"))
			n=12;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight14"))
			n=13;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight15"))
			n=14;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"weight16"))
			n=15;

		if (!xmlStrcmp(cur->name, (const xmlChar *)"red1"))
			n=16;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red2"))
			n=17;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red3"))
			n=18;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red4"))
			n=19;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red5"))
			n=20;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red6"))
			n=21;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red7"))
			n=22;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red8"))
			n=23;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red9"))
			n=24;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red10"))
			n=25;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red11"))
			n=26;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red12"))
			n=27;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red13"))
			n=28;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red14"))
			n=29;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red15"))
			n=30;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"red16"))
			n=31;

		if (!xmlStrcmp(cur->name, (const xmlChar *)"green1"))
			n=32;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green2"))
			n=33;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green3"))
			n=34;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green4"))
			n=35;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green5"))
			n=36;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green6"))
			n=37;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green7"))
			n=38;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green8"))
			n=39;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green9"))
			n=40;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green10"))
			n=41;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green11"))
			n=42;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green12"))
			n=43;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green13"))
			n=44;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green14"))
			n=45;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green15"))
			n=46;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"green16"))
			n=47;

		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue1"))
			n=48;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue2"))
			n=49;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue3"))
			n=50;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue4"))
			n=51;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue5"))
			n=52;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue6"))
			n=53;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue7"))
			n=54;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue8"))
			n=55;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue9"))
			n=56;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue10"))
			n=57;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue11"))
			n=58;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue12"))
			n=59;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue13"))
			n=60;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue14"))
			n=61;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue15"))
			n=62;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"blue16"))
			n=63;

		if (!xmlStrcmp(cur->name, (const xmlChar *)"name1"))
			n=64;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name2"))
			n=65;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name3"))
			n=66;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name4"))
			n=67;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name5"))
			n=68;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name6"))
			n=69;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name7"))
			n=70;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name8"))
			n=71;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name9"))
			n=72;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name10"))
			n=73;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name11"))
			n=74;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name12"))
			n=75;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name13"))
			n=76;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name14"))
			n=77;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name15"))
			n=78;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"name16"))
			n=79;

		if (!xmlStrcmp(cur->name, (const xmlChar *)"on1"))
			n=80;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on2"))
			n=81;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on3"))
			n=82;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on4"))
			n=83;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on5"))
			n=84;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on6"))
			n=85;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on7"))
			n=86;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on8"))
			n=87;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on9"))
			n=88;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on10"))
			n=89;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on11"))
			n=90;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on12"))
			n=91;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on13"))
			n=92;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on14"))
			n=93;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on15"))
			n=94;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"on16"))
			n=95;

		if (n<0 || n>=96)
		{
			cur = cur->next;
			continue;
		}

		int num = n%16;
		int cat = n/16;

		if (cat == 0  ||  cat == 1  ||  cat == 2  ||  cat == 3)	// w r g b
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				switch (cat)
				{
					case 0:
						blend.weights[num] = min(NOX_BLEND_WEIGHT_MAX  , max(NOX_BLEND_WEIGHT_MIN  ,(float)a/100.0f));
						break;
					case 1:
						blend.red[num] = min(NOX_BLEND_RED_MAX  , max(NOX_BLEND_RED_MIN  ,(float)a/100.0f));
						break;
					case 2:
						blend.green[num] = min(NOX_BLEND_GREEN_MAX  , max(NOX_BLEND_GREEN_MIN  ,(float)a/100.0f));
						break;
					case 3:
						blend.blue[num] = min(NOX_BLEND_BLUE_MAX  , max(NOX_BLEND_BLUE_MIN  ,(float)a/100.0f));
						break;
				}
			}
			xmlFree(opc);
		}

		if (cat == 4)	// name
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			blend.setName(num, pc);
			xmlFree(pc);
		}

		if (cat == 5)	// on
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!xmlStrcmp((const xmlChar *)pc, (const xmlChar *)"yes"))
				blend.blendOn[num] = true;
			else
				blend.blendOn[num] = false;
			xmlFree(pc);
		}

		cur = cur->next;
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseUserColors(xmlNodePtr curNode)
{
	if (!curNode)
		return false;
	xmlNodePtr cur = curNode->xmlChildrenNode;
	
	while (cur != NULL) 
	{
		int n = -1;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color1"))
			n=0;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color2"))
			n=1;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color3"))
			n=2;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color4"))
			n=3;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color5"))
			n=4;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color6"))
			n=5;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color7"))
			n=6;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color8"))
			n=7;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color9"))
			n=8;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color10"))
			n=9;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color11"))
			n=10;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color12"))
			n=11;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color13"))
			n=12;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color14"))
			n=13;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color15"))
			n=14;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color16"))
			n=15;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color17"))
			n=16;
		if (!xmlStrcmp(cur->name, (const xmlChar *)"color18"))
			n=17;

		if (n>=0  &&  n<=17)
		{
			float r,g,b;
			bool bad = false;
			char * pc = (char*)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!DecHex::hex8ToFloat((unsigned char *)(pc+0 ), r))
				bad = true;
			if (!DecHex::hex8ToFloat((unsigned char *)(pc+8 ), g))
				bad = true;
			if (!DecHex::hex8ToFloat((unsigned char *)(pc+16), b))
				bad = true;

			if (bad)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}

			UserColors::colors[n] = Color4(r,g,b);
		}

		cur = cur->next;
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseSky(xmlNodePtr curNode)
{

	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	char * pc;
	float f;
	float turbidity;
	float aerosol;
	float albedo = 0.5f;
	Vector3d sdir = Vector3d(0,1,0);
	int month, day, hour, minute, gmt;
	int longitude, latitude;
	int skymodel = 0;

	while (cur != NULL) 
	{
		// search for <sun_on>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"sun_on")))
		{
			xmlChar* key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"yes"))
				Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunOn = true;
			else
				Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunOn = false;
			xmlFree(key);
		}

		// search for <sky_model>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"sky_model")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			xmlFree(pc);
			skymodel = (int)a;
		}

		// search for <sun_other_layer>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"sun_other_layer")))
		{
			xmlChar* key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"yes"))
				Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunOtherBlendLayer = true;
			else
				Raytracer::getInstance()->curScenePtr->sscene.sunsky->sunOtherBlendLayer = false;
			xmlFree(key);
		}

		// search for <sun_size>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"sun_size")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				Raytracer * rtr = Raytracer::getInstance();
				rtr->curScenePtr->sscene.sunsky->sunSize = min(2.0f, max(0.1f, (float)a));
				rtr->curScenePtr->sscene.sunsky->maxCosSunSize = cos(rtr->curScenePtr->sscene.sunsky->sunSize/2*PI/180.0f);
				rtr->curScenePtr->sscene.sunsky->sunSizeSteradians = 2*PI*(1-cos(rtr->curScenePtr->sscene.sunsky->sunSize/2.0f *PI/180.0f)); 
			}
			xmlFree(opc);
		}
		 
		// search for <turbidity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"turbidity")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				turbidity = min(10, max(2, (float)a/10.0f));
			}
			xmlFree(opc);
		}
		 
		// search for <aerosol>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"aerosol")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				aerosol = (float)a;
			}
			xmlFree(opc);
		}

		// search for <albedo>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"albedo")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				albedo = (float)a;
			}
			xmlFree(opc);
		}
		
		// search for <sundir>	
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"sundir")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen(pc) != 24)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			bool OK = true;
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
				OK = false;
			sdir.x = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+8, f))
				OK = false;
			sdir.y = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+16, f))
				OK = false;
			sdir.z = f;
			xmlFree(pc);
			if (!OK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			sdir.normalize();
		}

		// search for <longitude>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"longitude")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			longitude = (int)a;
			xmlFree(pc);
			if (longitude > 180 || longitude < -180)
			{	// out of bound
				errLine = xmlGetLineNo(cur);
				return false;
			}
		}

		// search for <latitude>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"latitude")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			xmlFree(pc);
			latitude = (int)a;
			if (latitude > 89 || latitude < -89)
			{	// out of bound
				errLine = xmlGetLineNo(curNode);
				return false;
			}
		}

		// search for <use_sunsky>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"use_sunsky")))
		{
			xmlChar* key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"yes"))
				Raytracer::getInstance()->curScenePtr->sscene.useSunSky = true;
			else
				Raytracer::getInstance()->curScenePtr->sscene.useSunSky = false;
			xmlFree(key);
		}

		// search for <directions>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"directions")))
		{
			Vector3d dirN, dirE;
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen(pc) != 48)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			bool OK = true;
			if (!DecHex::hex8ToFloat((unsigned char *)pc, f))
				OK = false;
			dirN.x = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+8, f))
				OK = false;
			dirN.y = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+16, f))
				OK = false;
			dirN.z = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+24, f))
				OK = false;
			dirE.x = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+32, f))
				OK = false;
			dirE.y = f;
			if (!DecHex::hex8ToFloat((unsigned char *)pc+40, f))
				OK = false;
			dirE.z = f;
			xmlFree(pc);
			if (!OK)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			dirN.normalize();
			dirE.normalize();
			if (fabs(dirN*dirE) >= 0.99999999f)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}

			Vector3d dirU = dirE ^ dirN;
			dirE = dirN ^ dirU;

			Raytracer::getInstance()->curScenePtr->sscene.sunsky->dirN =  dirN;
			Raytracer::getInstance()->curScenePtr->sscene.sunsky->dirS = -dirN;
			Raytracer::getInstance()->curScenePtr->sscene.sunsky->dirE =  dirE;
			Raytracer::getInstance()->curScenePtr->sscene.sunsky->dirW = -dirE;
			Raytracer::getInstance()->curScenePtr->sscene.sunsky->dirU =  dirU;
		}

		// go to next child
		cur = cur->next;
	}

	if (!parseSkyDate(curNode, month, day, hour, minute, gmt))
		return false;

	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->sscene.sunsky->aerosol = aerosol;
	rtr->curScenePtr->sscene.sunsky->setStandardMeridianForTimeZone(-gmt*PI/12);
	rtr->curScenePtr->sscene.sunsky->setSunDir(sdir, turbidity);
	rtr->curScenePtr->sscene.sunsky->setDate(month, day, hour, (float)minute);
	rtr->curScenePtr->sscene.sunsky->setPosition((float)-longitude*(PI)/180.0f, (float)(-latitude)*(PI)/180.0f);
	rtr->curScenePtr->sscene.sunsky->albedo = albedo;
	rtr->curScenePtr->sscene.sunsky->use_hosek = (skymodel!=0);
	rtr->curScenePtr->sscene.sunsky->updateHosekData();

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseSkyDate(xmlNodePtr curNode, int &Month, int &Day, int &Hour, int &Minute, int &GMT)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	char * pc;
	int month, day, hour, minute, gmt;
	int maxday;

	while (cur != NULL) 
	{
		// search for <month>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"month")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			xmlFree(pc);
			month = (int)a;
			if (month > 12 || month < 1)
			{	// out of bound
				errLine = xmlGetLineNo(curNode);
				return false;
			}
		}

		switch (month)
		{
			case  1: maxday = 31; break;
			case  2: maxday = 28; break;
			case  3: maxday = 31; break;
			case  4: maxday = 30; break;
			case  5: maxday = 31; break;
			case  6: maxday = 30; break;
			case  7: maxday = 31; break;
			case  8: maxday = 31; break;
			case  9: maxday = 30; break;
			case 10: maxday = 31; break;
			case 11: maxday = 30; break;
			case 12: maxday = 31; break;
			default: maxday = 0;
		}

		// search for <day>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"day")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||  (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			xmlFree(pc);
			day = (int)a;
			if (day > maxday || day < 1)
			{	// out of bound
				errLine = xmlGetLineNo(curNode);
				return false;
			}
		}

		// search for <hour>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"hour")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			hour = (int)a;
			xmlFree(pc);
			if (hour > 23 || hour < 0)
			{	// out of bound
				errLine = xmlGetLineNo(curNode);
				return false;
			}
		}

		// search for <minute>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"minute")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			xmlFree(pc);
			minute = (int)a;
			if (minute > 59 || minute < 0)
			{	// out of bound
				errLine = xmlGetLineNo(curNode);
				return false;
			}
		}

		// search for <gmt>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"gmt")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)   ||   ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			gmt = (int)a;
			xmlFree(pc);
			if (gmt > 12 || gmt < -12)
			{	// out of bound
				errLine = xmlGetLineNo(curNode);
				return false;
			}
		}

		// go to next child
		cur = cur->next;
	}

	Month = month;
	Day = day;
	Hour = hour;
	Minute = minute;
	GMT = gmt;

	return true;
}

//================================================================================================================

MaterialNox * XMLScene::loadMaterial(char * filename, int& errorline)//, ImageByteBuffer * image)
{
	if (!filename)
	{
		errorline = -1;
		return NULL;
	}

	directory = getDirectory(filename);

	xmlNodePtr cur, xRoot;
	xmlChar *key;

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
		return NULL;

	xRoot = xmlDocGetRootElement(xmlDoc);
	if (xRoot == NULL) 
	{	// is empty
		xmlFreeDoc(xmlDoc);
		return NULL;
	}

	if (xmlStrcmp(xRoot->name, (const xmlChar *) "material")) 
	{	// bad root
		xmlFreeDoc(xmlDoc);
		return false;
	}


	// get id
	key = xmlGetProp(xRoot, (xmlChar*)"id");
	if (!key   ||   strlen((char *)key) != 8)
	{
		errorline = xmlGetLineNo(xRoot);
		if (key)
			xmlFree(key);
		xmlFreeDoc(xmlDoc);
		return false;
	}

	unsigned int ui;
	if (!DecHex::hex8ToUint((unsigned char *)key, ui) )
	{
		xmlFree(key);
		xmlFreeDoc(xmlDoc);
		errorline = xmlGetLineNo(xRoot);
		return false;
	}
	xmlFree(key);

	MaterialNox * mat = new MaterialNox();
	mat->id = ui;

	cur = xRoot->xmlChildrenNode;
	int nLayers = 0;

	while (cur != NULL) 
	{
		// search for <preview>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"preview")))
		{
			if (!mat->preview)
				mat->preview = new ImageByteBuffer();

			if (!parsePreview(cur, mat->preview))
			{
				mat->preview->freeBuffer();
				errorline = errLine;
				xmlFreeDoc(xmlDoc);
				delete mat;
				return false;
			}
		}

		// search for <tex_opacity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_opacity")))
		{
			if (!parseTexture(cur, mat, NULL))
				return false;
		}

		// search for <name>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc  ||  strlen(pc) < 1)
			{
				mat->name = NULL;
			}
			else
			{
				int ll = (int)strlen(pc);
				mat->name = (char *)malloc(ll+1);
				if (mat->name)
					sprintf_s(mat->name, ll+1, "%s", pc);
			}
			if (pc)
				xmlFree(pc);
		}

		// search for <skyportal>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"skyportal")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->isSkyPortal = true;
			else
				mat->isSkyPortal = false;
			xmlFree(pc);
		}

		// search for <matteshadow>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"matteshadow")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->isMatteShadow = true;
			else
				mat->isMatteShadow = false;
			xmlFree(pc);
		}

		// search for <displacement_subdivs>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_subdivs")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			mat->displ_subdivs_lvl = (int)a;
			xmlFree(pc);
		}

		// search for <displacement_depth>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_depth")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mat->displ_depth = (float)a;
			}
			xmlFree(opc);
		}

		// search for <displacement_shift>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_shift")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mat->displ_shift = (float)a;
			}
			xmlFree(opc);
		}

		// search for <displacement_continuity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_continuity")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->displ_continuity = true;
			else
				mat->displ_continuity = false;
			xmlFree(pc);
		}

		// search for <displacement_ignore_scale>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_ignore_scale")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				mat->displ_ignore_scale = true;
			else
				mat->displ_ignore_scale = false;
			xmlFree(pc);
		}

		// search for <displacement_normal_mode>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"displacement_normal_mode")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			mat->displ_normal_mode = (int)a;
			xmlFree(pc);
		}

		// search for <opacity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"opacity")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				mat->opacity= (float)(a * 0.01);
			}
			xmlFree(opc);
		}

		// search for <tex_opacity>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_opacity")))
		{
			if (!parseTexture(cur, mat, NULL))
				return false;
		}

		// search for <tex_displacement>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"tex_displacement")))
		{
			if (!parseTexture(cur, mat, NULL))
				return false;
		}

		// search for <blendlayer>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"blendlayer")))
		{
			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			mat->blendIndex = min(15, max(0, (int)a-1));
			xmlFree(pc);
		}

		// search for <layer>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"layer")))
		{
			nLayers++;
			key = xmlGetProp(cur, (xmlChar*)"type");
			if (key == NULL)
			{	// no type specified
				errorline = xmlGetLineNo(cur);
				xmlFreeDoc(xmlDoc);
				delete mat;
				return false;
			}
			if (!xmlStrcmp(key, (const xmlChar *)"shade"))
			{	// type is shade
				if (!parseMaterialShadeLayer(cur, mat) )
				{
					delete mat;
					errorline = errLine;
					xmlFreeDoc(xmlDoc);
					xmlFree(key);
					return false;
				}
			}
			else
			{
				if ((!xmlStrcmp(key, (const xmlChar *)"emission")))
				{	// type is emission
					if (!parseMaterialEmissionLayer(cur, mat) )
					{
						delete mat;
						errorline = errLine;
						xmlFreeDoc(xmlDoc);
						xmlFree(key);
						return false;
					}
				}
				else
				{	// another type - bad
					errorline = xmlGetLineNo(cur);
					delete mat;
					xmlFreeDoc(xmlDoc);
					xmlFree(key);
					return false;
				}
			}
			xmlFree(key);
		}
		cur = cur->next;
	}


	if (nLayers > 0)
	{
		mat->layers.createArray();
	}
	else
	{
		delete mat;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	xmlFreeDoc(xmlDoc);

	return mat;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parsePreview(xmlNodePtr curNode, ImageByteBuffer * image)
{
	if (!curNode   ||   !image)
		return false;

	xmlNodePtr cur;
	xmlChar * key;
	int w,h;
	cur = curNode->xmlChildrenNode;

	// get width
	key = xmlGetProp(curNode, (xmlChar*)"width");
	if (!key   ||   strlen((char *)key) == 0)
	{
		if (key)
			xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	char * addr;
	__int64 a = _strtoi64((char *)key, &addr, 10);
	if ( (addr == (char *)key)  || ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(key);
		return false;
	}
	xmlFree(key);
	w = (int)a;

	// get height
	key = xmlGetProp(curNode, (xmlChar*)"height");
	if (!key   ||   strlen((char *)key) == 0)
	{
		if (key)
			xmlFree(key);
		errLine = xmlGetLineNo(curNode);
		return false;
	}
	a = _strtoi64((char *)key, &addr, 10);
	if ( (addr == (char *)key)  || ((unsigned int)(addr-(char*)key) < strlen((char *)key)) )
	{	// conversion error (to int)
		errLine = xmlGetLineNo(curNode);
		xmlFree(key);
		return false;
	}
	xmlFree(key);
	h = (int)a;

	if (w < 1   ||   h < 1)
		return false;

	image->allocBuffer(w,h);
	image->clearBuffer();
	
	while (cur != NULL) 
	{
		if (cur->name[0] == 'l')
		{
			int l = (int)strlen((char*)cur->name);
			int n;
			if (l < 2)
				return false;

			char * addr;
			char * nn = (char*)cur->name+1;
			__int64 a = _strtoi64((char *)nn, &addr, 10);
			if ( (addr == (char *)nn)  || ((unsigned int)(addr-(char*)nn) < strlen((char *)nn)) )
			{	// conversion error (to int)
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			n = (int)a;
			if (n < 0   ||   n >= h)
			{
				errLine = xmlGetLineNo(curNode);
				return false;
			}

			char * pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(curNode);
				return false;
			}

			l = (int)strlen(pc);
			if (l != w*6)
			{
				errLine = xmlGetLineNo(curNode);
				xmlFree(pc);
				return false;
			}

			unsigned char r,g,b;
			for (int i=0; i<w; i++)
			{
				DecHex::hex2ToUcharBigEndian((unsigned char*)&(pc[i*6+0]), r);
				DecHex::hex2ToUcharBigEndian((unsigned char*)&(pc[i*6+2]), g);
				DecHex::hex2ToUcharBigEndian((unsigned char*)&(pc[i*6+4]), b);
				image->imgBuf[n][i] = RGB(r,g,b);
			}

			xmlFree(pc);
		}

		// go to next child
		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::loadPost(char * filename, Camera * cam)
{
	if (!filename)
	{
		errLine = -2;
		return false;
	}

	if (!cam)
	{
		errLine = -3;
		return false;
	}

	xmlNodePtr xRoot;

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
	{
		errLine = -4;
		return false;
	}

	xRoot = xmlDocGetRootElement(xmlDoc);
	if (xRoot == NULL) 
	{	// is empty
		errLine = -5;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (xmlStrcmp(xRoot->name, (const xmlChar *) "postprocess")) 
	{	// bad root
		errLine = -6;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (!parseCameraPostValues(xRoot, cam))
	{
		return false;
		xmlFreeDoc(xmlDoc);
	}

	xmlFreeDoc(xmlDoc);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

char * XMLScene::getValidTexPath(char * fname)
{
	if (!fname)
	{
		return NULL;
	}

	if (strlen(fname) < 2)
	{
		return copyString(fname);
	}

	if (fname[1] == ':')
	{
		return copyString(fname);
	}

	if (!directory)
	{
		return copyString(fname);
	}

	int s = (int)strlen(directory) + (int)strlen(fname) + 16 ;
	char * res = (char *)malloc(s);
	sprintf_s(res, s, "%s\\%s", directory, fname);

	return res;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::loadBlendSettings(char * filename, BlendSettings &blend, bool onlyNames)
{
	if (!filename)
	{
		errLine = -2;
		return false;
	}

	xmlNodePtr xRoot;

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
	{
		errLine = -4;
		return false;
	}

	xRoot = xmlDocGetRootElement(xmlDoc);
	if (xRoot == NULL) 
	{	// is empty
		errLine = -5;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (xmlStrcmp(xRoot->name, (const xmlChar *) "blend")) 
	{	// bad root
		errLine = -6;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	BlendSettings cblend;
	if (!parseBlend(xRoot, cblend))
	{
		return false;
		xmlFreeDoc(xmlDoc);
	}

	bool res;
	if (onlyNames)
		res = blend.copyNamesFrom(&cblend);
	else
		res = blend.copyFrom(&cblend);

	xmlFreeDoc(xmlDoc);

	return res;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseRendererSettings(xmlNodePtr curNode)
{
	if (!curNode)
		return false;
	xmlNodePtr cur = curNode->xmlChildrenNode;
	char * pc;
	
	while (cur != NULL) 
	{
		// search for <timer_stopafter_hours>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_stopafter_hours")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)   ||   ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			Raytracer::getInstance()->gui_timers.stopTimerHours = (int)a;
			xmlFree(pc);
		}

		// search for <timer_stopafter_minutes>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_stopafter_minutes")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)   ||   ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			Raytracer::getInstance()->gui_timers.stopTimerMinutes = (int)a;
			xmlFree(pc);
		}

		// search for <timer_stopafter_seconds>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_stopafter_seconds")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)   ||   ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			Raytracer::getInstance()->gui_timers.stopTimerSeconds = (int)a;
			xmlFree(pc);
		}

		// search for <timer_refresh_seconds>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_refresh_seconds")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)   ||   ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			Raytracer::getInstance()->gui_timers.refreshTime = (int)a;
			xmlFree(pc);
		}

		// search for <timer_autosave_minutes>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_autosave_minutes")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)   ||   ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			Raytracer::getInstance()->gui_timers.autosaveTimerMinutes = (int)a;
			xmlFree(pc);
		}

		// search for <timer_on_refresh>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_on_refresh")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				Raytracer::getInstance()->gui_timers.refreshTimeOn  = true;
			else
				Raytracer::getInstance()->gui_timers.refreshTimeOn  = false;
			xmlFree(pc);
		}

		// search for <timer_on_stop>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_on_stop")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				Raytracer::getInstance()->gui_timers.stoppingTimerOn = true;
			else
				Raytracer::getInstance()->gui_timers.stoppingTimerOn = false;
			xmlFree(pc);
		}

		// search for <timer_on_autosave>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"timer_on_autosave")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				Raytracer::getInstance()->gui_timers.autosaveTimerOn = true;
			else
				Raytracer::getInstance()->gui_timers.autosaveTimerOn = false;
			xmlFree(pc);
		}

		// search for <engine>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"engine")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc  ||  (addr == (char *)pc)   ||   ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}

			xmlFree(pc);

			Raytracer * rtr = Raytracer::getInstance();
			Scene * sc = rtr->curScenePtr;
			switch (a)
			{
				case 1:
					sc->sscene.giMethod = 1;	break;	//pt	
				case 2:
					sc->sscene.giMethod = 2;	break;	//bdpt
				case 3:
					sc->sscene.giMethod = 3;	break;	//mlt
				case 10:
					sc->sscene.giMethod = 10;	break;	//inters
			}
		}

		cur = cur->next;
	}

	return true;
}		

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseSceneInfo(xmlNodePtr curNode)
{
	if (!curNode)
		return false;
	xmlNodePtr cur = curNode->xmlChildrenNode;
	char * pc;
	
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (!sc)
		return true;

	while (cur != NULL) 
	{
		// search for <scene_name>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"scene_name")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
				continue;

			unsigned int lpc = (unsigned int)strlen(pc);
			if (lpc%2)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(pc);
				return false;
			}

			unsigned int hlpc = lpc/2;
			char * res = (char *)malloc(hlpc+1);
			if (!res)
			{
				xmlFree(pc);
				continue;
			}

			for (unsigned int i=0; i<hlpc; i++)
			{
				DecHex::hex2ToUchar((unsigned char *)(&(pc[2*i])), (unsigned char&)res[i]);
			}
			res[hlpc] = 0;
			xmlFree(pc);

			if (sc->scene_name)
				free(sc->scene_name);
			sc->scene_name = res;
			sc->le_sc_name = hlpc;
		}

		// search for <scene_author>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"scene_author")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
				continue;

			unsigned int lpc = (unsigned int)strlen(pc);
			if (lpc%2)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(pc);
				return false;
			}

			unsigned int hlpc = lpc/2;
			char * res = (char *)malloc(hlpc+1);
			if (!res)
			{
				xmlFree(pc);
				continue;
			}

			for (unsigned int i=0; i<hlpc; i++)
			{
				DecHex::hex2ToUchar((unsigned char *)(&(pc[2*i])), (unsigned char&)res[i]);
			}
			res[hlpc] = 0;
			xmlFree(pc);

			if (sc->scene_author_name)
				free(sc->scene_author_name);
			sc->scene_author_name = res;
			sc->le_sc_author_name = hlpc;
		}

		// search for <author_email>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"author_email")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
				continue;

			unsigned int lpc = (unsigned int)strlen(pc);
			if (lpc%2)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(pc);
				return false;
			}

			unsigned int hlpc = lpc/2;
			char * res = (char *)malloc(hlpc+1);
			if (!res)
			{
				xmlFree(pc);
				continue;
			}

			for (unsigned int i=0; i<hlpc; i++)
			{
				DecHex::hex2ToUchar((unsigned char *)(&(pc[2*i])), (unsigned char&)res[i]);
			}
			res[hlpc] = 0;
			xmlFree(pc);

			if (sc->scene_author_email)
				free(sc->scene_author_email);
			sc->scene_author_email = res;
			sc->le_sc_author_email = hlpc;
		}

		// search for <author_www>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"author_www")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
				continue;

			unsigned int lpc = (unsigned int)strlen(pc);
			if (lpc%2)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(pc);
				return false;
			}

			unsigned int hlpc = lpc/2;
			char * res = (char *)malloc(hlpc+1);
			if (!res)
			{
				xmlFree(pc);
				continue;
			}

			for (unsigned int i=0; i<hlpc; i++)
			{
				DecHex::hex2ToUchar((unsigned char *)(&(pc[2*i])), (unsigned char&)res[i]);
			}
			res[hlpc] = 0;
			xmlFree(pc);

			if (sc->scene_author_www)
				free(sc->scene_author_www);
			sc->scene_author_www = res;
			sc->le_sc_author_www = hlpc;
		}

		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseUserPresets(xmlNodePtr curNode)
{
	if (!curNode)
		return false;
	xmlNodePtr cur = curNode->xmlChildrenNode;
	
	while (cur != NULL) 
	{
		// search for <user_preset>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"user_preset")))
		{
			PresetPost * preset = new PresetPost();
			if (!parseUserPreset(cur, preset))
				return false;
			Raytracer::getInstance()->curScenePtr->presets.add(preset);
			Raytracer::getInstance()->curScenePtr->presets.createArray();
		}

		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseUserPreset(xmlNodePtr curNode, PresetPost * pr)
{
	if (!curNode)
		return false;
	if (!pr)
		return false;
	xmlNodePtr cur = curNode->xmlChildrenNode;
	//char * pc;
	
	while (cur != NULL) 
	{
		// search for <name>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"name")))
		{
			xmlChar * key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			pr->name = copyString((char*)key);
			xmlFree(key);
		}

		// search for <blend>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"blend")))
		{
			if (!parseBlend(cur, pr->blend))
				return false;
		}

		// search for <post>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"post")))
		{
			if (!parseCameraPostValues(cur, &pr->iMod))
				return false;
		}

		// search for <final>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"final")))
		{
			if (!parseCameraFinalValues(cur, &pr->fMod, &pr->texGlareObstacle, &pr->texGlareDiaphragm))
				return false;
		}

		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::loadFinal(char * filename, Camera * cam)
{
	if (!filename)
	{
		errLine = -2;
		return false;
	}

	if (!cam)
	{
		errLine = -3;
		return false;
	}

	xmlNodePtr xRoot;

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
	{
		errLine = -4;
		return false;
	}

	xRoot = xmlDocGetRootElement(xmlDoc);
	if (xRoot == NULL) 
	{	// is empty
		errLine = -5;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (xmlStrcmp(xRoot->name, (const xmlChar *) "final_post")) 
	{	// bad root
		errLine = -6;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (!parseCameraFinalValues(xRoot, cam))
	{
		return false;
		xmlFreeDoc(xmlDoc);
	}

	xmlFreeDoc(xmlDoc);

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseCameraFinalValues(xmlNodePtr curNode, Camera * cam)
{
	if (!curNode   ||   !cam)
		return false;

	char * texObst = NULL;
	char * texDiaph = NULL;
	bool ok = parseCameraFinalValues(curNode, &(cam->fMod), &texObst, &texDiaph);
	if (texObst)
	{
		cam->texObstacle.loadFromFile(texObst);
		free(texObst);
	}
	if (texDiaph)
	{
		cam->texDiaphragm.loadFromFile(texDiaph);
		free(texDiaph);
	}
	return ok;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::parseCameraFinalValues(xmlNodePtr curNode, FinalModifier * fMod, char ** texObstacle, char ** texDiaphragm)
{
	if (!curNode)
		return false;

	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;

	float addR = 0.0f;
	float addG = 0.0f;
	float addB = 0.0f;
	char * pc;

	CHECK(fMod);

	while (cur != NULL) 
	{
		// search for <focus_dist>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"focus_dist")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->focusDistance = min(max((float)a, NOX_FINAL_FOCUS_MIN), NOX_FINAL_FOCUS_MAX);
			}
			xmlFree(opc);
		}

		// search for <dof_quality>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"dof_quality")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->quality = min(max((int)a, NOX_FINAL_QUALITY_MIN), NOX_FINAL_QUALITY_MAX);
			xmlFree(pc);
		}

		// search for <dof_algorithm>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"dof_algorithm")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->algorithm = min(max((int)a-1, NOX_FINAL_ALGORITHM_MIN), NOX_FINAL_ALGORITHM_MAX);
			xmlFree(pc);
		}

		// search for <dof_on>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"dof_on")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->dofOn = true;
			else
				fMod->dofOn = false;
			xmlFree(pc);
		}

		// search for <aperture>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"aperture")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->aperture = min(max((float)a, NOX_FINAL_APERTURE_MIN), NOX_FINAL_APERTURE_MAX);
			}
			xmlFree(opc);
		}

		// search for <aperture_num_blades>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"aperture_num_blades")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->bladesNum = min(max((int)a, NOX_FINAL_BLADES_NUM_MIN), NOX_FINAL_BLADES_NUM_MAX);
			xmlFree(pc);
		}

		// search for <aperture_blades_angle>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"aperture_blades_angle")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->bladesAngle = min(max((float)a, NOX_FINAL_BLADES_ANGLE_MIN), NOX_FINAL_BLADES_ANGLE_MAX);
			}
			xmlFree(opc);
		}

		// search for <aperture_blades_radius>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"aperture_blades_radius")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->bladesRadius = min(max((float)a, NOX_FINAL_BLADES_RADIUS_MIN), NOX_FINAL_BLADES_RADIUS_MAX);
			}
			xmlFree(opc);
		}

		// search for <aperture_blades_round>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"aperture_blades_round")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->roundBlades = true;
			else
				fMod->roundBlades = false;
			xmlFree(pc);
		}

		// search for <bokeh_balance>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_balance")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->bokehRingBalance = min(max((int)a, NOX_FINAL_BOKEH_BALANCE_MIN), NOX_FINAL_BOKEH_BALANCE_MAX);
			xmlFree(pc);
		}

		// search for <bokeh_ring_size>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_ring_size")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->bokehRingSize = min(max((int)a, NOX_FINAL_BOKEH_RING_SIZE_MIN), NOX_FINAL_BOKEH_RING_SIZE_MAX);
			xmlFree(pc);
		}

		// search for <bokeh_flatten>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_flatten")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->bokehFlatten = min(max((int)a, NOX_FINAL_BOKEH_FLATTEN_MIN), NOX_FINAL_BOKEH_FLATTEN_MAX);
			xmlFree(pc);
		}

		// search for <bokeh_vignette>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bokeh_vignette")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->bokehVignette = min(max((int)a, NOX_FINAL_BOKEH_VIGNETTE_MIN), NOX_FINAL_BOKEH_VIGNETTE_MAX);
			xmlFree(pc);
		}

		// search for <chr_abb_shift_lens>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"chr_abb_shift_lens")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->chromaticShiftLens = min(max((float)a, NOX_FINAL_CHR_SHIFT_LENS_MIN), NOX_FINAL_CHR_SHIFT_LENS_MAX);
			}
			xmlFree(opc);
		}

		// search for <chr_abb_shift_depth>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"chr_abb_shift_depth")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->chromaticShiftDepth = min(max((float)a, NOX_FINAL_CHR_SHIFT_DEPTH_MIN), NOX_FINAL_CHR_SHIFT_DEPTH_MAX);
			}
			xmlFree(opc);
		}

		// search for <chr_abb_achromatic>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"chr_abb_achromatic")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->chromaticAchromatic = min(max((float)a, NOX_FINAL_CHR_ACHROMATIC_MIN), NOX_FINAL_CHR_ACHROMATIC_MAX);
			}
			xmlFree(opc);
		}

		// search for <chr_abb_red>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"chr_abb_red")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->chromaticPlanarRed = min(max((float)a, NOX_FINAL_CHR_PL_RED_MIN), NOX_FINAL_CHR_PL_RED_MAX);
			}
			xmlFree(opc);
		}

		// search for <chr_abb_green>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"chr_abb_green")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->chromaticPlanarGreen = min(max((float)a, NOX_FINAL_CHR_PL_GREEN_MIN), NOX_FINAL_CHR_PL_GREEN_MAX);
			}
			xmlFree(opc);
		}

		// search for <chr_abb_blue>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"chr_abb_blue")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->chromaticPlanarBlue = min(max((float)a, NOX_FINAL_CHR_PL_BLUE_MIN), NOX_FINAL_CHR_PL_BLUE_MAX);
			}
			xmlFree(opc);
		}

		// search for <hot_pixels_enabled>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"hot_pixels_enabled")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->dotsEnabled = true;
			else
				fMod->dotsEnabled = false;
			xmlFree(pc);
		}

		// search for <hot_pixels_radius>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"hot_pixels_radius")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->dotsRadius = min(max((int)a, NOX_FINAL_DOTS_RADIUS_MIN), NOX_FINAL_DOTS_RADIUS_MAX);
			xmlFree(pc);
		}

		// search for <hot_pixels_threshold>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"hot_pixels_threshold")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->dotsThreshold = min(max((float)a, NOX_FINAL_DOTS_THRESHOLD_MIN), NOX_FINAL_DOTS_THRESHOLD_MAX);
			}
			xmlFree(opc);
		}

		// search for <hot_pixels_density>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"hot_pixels_density")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->dotsDensity = min(max((int)a, NOX_FINAL_DOTS_DENSITY_MIN), NOX_FINAL_DOTS_DENSITY_MAX);
			xmlFree(pc);
		}

		// search for <grain>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"grain")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->grain = min(max((float)a, NOX_FINAL_GRAIN_MIN), NOX_FINAL_GRAIN_MAX);
			}
			xmlFree(opc);
		}

		// search for <fog_enabled>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"fog_enabled")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->fogEnabled = true;
			else
				fMod->fogEnabled = false;
			xmlFree(pc);
		}

		// search for <fog_exclude_background>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"fog_exclude_background")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->fogExclSky = true;
			else
				fMod->fogExclSky = false;
			xmlFree(pc);
		}

		// search for <fog_distance>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"fog_distance")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->fogDist  = min(max((float)a, NOX_FINAL_FOG_DIST_MIN), NOX_FINAL_FOG_DIST_MAX);
			}
			xmlFree(opc);
		}

		// search for <fog_density>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"fog_density")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->fogSpeed = min(max((float)a, NOX_FINAL_FOG_SPEED_MIN), NOX_FINAL_FOG_SPEED_MAX);
			}
			xmlFree(opc);
		}

		// search for <fog_color>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"fog_color")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc  ||  strlen(pc) != 6)
			{	// must be 6 chars
				if (opc)
					xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			// get color
			unsigned char c;
			Color4 c4;
			bool tOK = true;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc,  c) )
				tOK = false;
			c4.r = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+2,  c) )
				tOK = false;
			c4.g = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+4,  c) )
				tOK = false;
			c4.b = c/255.0f;

			xmlFree(opc);

			if (!tOK)
			{	// some chars wasn't hex values
				errLine = xmlGetLineNo(cur);
				return false;
			}

			fMod->fogColor = c4;
		}

		// search for <bloom_enabled>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bloom_enabled")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->bloomEnabled = true;
			else
				fMod->bloomEnabled = false;
			xmlFree(pc);
		}

		// search for <bloom_power>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bloom_power")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->bloomPower = min(max((float)a, NOX_FINAL_BLOOM_POWER_MIN), NOX_FINAL_BLOOM_POWER_MAX);
			}
			xmlFree(opc);
		}

		// search for <bloom_area>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bloom_area")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->bloomArea = min(max((int)a, NOX_FINAL_BLOOM_AREA_MIN), NOX_FINAL_BLOOM_AREA_MAX);
			xmlFree(pc);
		}

		// search for <bloom_threshold>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bloom_threshold")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->bloomThreshold = min(max((float)a, NOX_FINAL_BLOOM_THRES_MIN), NOX_FINAL_BLOOM_THRES_MAX);
			}
			xmlFree(opc);
		}

		// search for <bloom_attenuation_enabled>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bloom_attenuation_enabled")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->bloomAttenuationOn = true;
			else
				fMod->bloomAttenuationOn = false;
			xmlFree(pc);
		}

		// search for <bloom_attenuation_color>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"bloom_attenuation_color")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc  ||  strlen(pc) != 6)
			{	// must be 6 chars
				if (opc)
					xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			// get color
			unsigned char c;
			Color4 c4;
			bool tOK = true;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc,  c) )
				tOK = false;
			c4.r = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+2,  c) )
				tOK = false;
			c4.g = c/255.0f;
			if (!DecHex::hex2ToUcharBigEndian((unsigned char *)pc+4,  c) )
				tOK = false;
			c4.b = c/255.0f;

			xmlFree(opc);

			if (!tOK)
			{	// error, some chars wasn't hex values
				errLine = xmlGetLineNo(cur);
				return false;
			}

			fMod->bloomAttenuationColor = c4;
		}

		// search for <glare_enabled>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_enabled")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->glareEnabled = true;
			else
				fMod->glareEnabled = false;
			xmlFree(pc);
		}

		// search for <glare_power>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_power")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->glarePower = min(max((float)a, NOX_FINAL_GLARE_POWER_MIN), NOX_FINAL_GLARE_POWER_MAX);
			}
			xmlFree(opc);
		}

		// search for <glare_dispersion>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_dispersion")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->glareDispersion = min(max((float)a, NOX_FINAL_GLARE_DISP_MIN), NOX_FINAL_GLARE_DISP_MAX);
			}
			xmlFree(opc);
		}

		// search for <glare_threshold>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_threshold")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			char * opc = pc;
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			double a = _strtod_l((char *)pc, &addr, noxLocale);
			if ( (addr == (char *)pc)  ||  ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to float)
				xmlFree(opc);
				errLine = xmlGetLineNo(cur);
				return false;
			}
			else
			{
				fMod->glareThreshold = min(max((float)a, NOX_FINAL_GLARE_THRES_MIN), NOX_FINAL_GLARE_THRES_MAX);
			}
			xmlFree(opc);
		}

		// search for <glare_area>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_area")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc   ||   strlen((char *)pc) == 0)
			{
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(cur);
				return false;
			}

			char * addr;
			__int64 a = _strtoi64((char *)pc, &addr, 10);
			if (!pc   ||   (addr == (char *)pc)  || ((unsigned int)(addr-(char*)pc) < strlen((char *)pc)) )
			{	// conversion error (to int)
				if (pc)
					xmlFree(pc);
				errLine = xmlGetLineNo(curNode);
				return false;
			}
			fMod->glareArea = min(max((int)a, NOX_FINAL_GLARE_AREA_MIN), NOX_FINAL_GLARE_AREA_MAX);
			xmlFree(pc);
		}


		// search for <glare_diaphragm_on>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_diaphragm_on")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->glareTextureApertureOn = true;
			else
				fMod->glareTextureApertureOn = false;
			xmlFree(pc);
		}

		// search for <glare_obstacle_on>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_obstacle_on")))
		{
			pc = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!pc)
			{
				errLine = xmlGetLineNo(cur);
				return false;
			}
			if ((!xmlStrcmp((xmlChar*)pc, (const xmlChar *)"yes")))
				fMod->glareTextureObstacleOn = true;
			else
				fMod->glareTextureObstacleOn = false;
			xmlFree(pc);
		}

		// search for <diffraction_obstacle>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_obstacle")))
		{
			xmlChar * key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (texObstacle)
				*texObstacle = copyString((char*)key);
			xmlFree(key);
		}

		// search for <diffraction_diaphragm>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"glare_diaphragm")))
		{
			xmlChar * key = xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!key  ||  strlen((char*)key)<1)
			{
				errLine = xmlGetLineNo(cur);
				xmlFree(key);
				return false;
			}
			if (texDiaphragm)
				*texDiaphragm = copyString((char*)key);
			xmlFree(key);
		}

		// go to next child
		cur = cur->next;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------

bool XMLScene::loadPreset(char * filename, ImageModifier * iMod, FinalModifier * fMod, BlendSettings * blend, Texture * texObstacle, Texture * texDiaphragm)
{
	if (!filename)
	{
		errLine = -2;
		return false;
	}

	xmlNodePtr xRoot;

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
	{
		errLine = -4;
		return false;
	}

	xRoot = xmlDocGetRootElement(xmlDoc);
	if (xRoot == NULL) 
	{	// is empty
		errLine = -5;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (xmlStrcmp(xRoot->name, (const xmlChar *) "nox_presets")) 
	{	// bad root
		errLine = -6;
		xmlFreeDoc(xmlDoc);
		return false;
	}

	xmlNodePtr curNode = xRoot;
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	//char * pc;

	while (cur != NULL) 
	{
		// search for <blend>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"blend")))
		{
			if (blend)
			{
				bool ok = parseBlend(cur, *blend);
				if (!ok)
				{
					xmlFreeDoc(xmlDoc);
					return false;
				}
			}
		}

		// search for <post>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"post")))
		{
			if (iMod)
			{
				bool ok = parseCameraPostValues(cur, iMod);
				if (!ok)
				{
					xmlFreeDoc(xmlDoc);
					return false;
				}
			}
		}

		// search for <final>
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"final")))
		{
			if (fMod)
			{
				char * texObst = NULL;
				char * texDiaph = NULL;
				bool ok = parseCameraFinalValues(cur, fMod, &texObst, &texDiaph);
				if (texObst)
				{
					texObstacle->loadFromFile(texObst);
					free(texObst);
				}
				if (texDiaph)
				{
					texDiaphragm->loadFromFile(texDiaph);
					free(texDiaph);
				}
				if (!ok)
				{
					xmlFreeDoc(xmlDoc);
					return false;
				}
			}
		}

		// go to next child
		cur = cur->next;
	}

	xmlFreeDoc(xmlDoc);

	return true;
}

//----------------------------------------------------------------------------------------------------------------
