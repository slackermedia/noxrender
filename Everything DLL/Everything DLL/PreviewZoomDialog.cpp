#include "Dialogs.h"
#include "EMControls.h"
#include "resource.h"
#include <stdio.h>
#include "ColorSchemes.h"
#include "DLL.h"
#include "log.h"

extern HMODULE hDllModule;
extern ThemeManager gTheme;
extern HBITMAP gIcons;

bool startRenderingZoomPreview(HWND hDlg, HWND hImg);
bool stopRenderingZoomPreview(HWND hDlg, HWND hImg);
bool resizePreviewBeforeClosing(HWND hImg);
void stopRefreshThread();
void runRefreshThread(HWND hPrev);


struct RThreadValues
{
	HWND hPreview;
	int seconds;
	bool running;
	bool paused;
};

RThreadValues rtv;

void bigPreviewPreRenderOperations(HWND hwnd);
void bigPreviewPostRenderOperations(HWND hwnd);
DWORD WINAPI bigPreviewRefreshThreadProc(LPVOID lpParameter);
bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);


INT_PTR CALLBACK PreviewZoomDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (!bgBrush)
					bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

				HWND hOK        = GetDlgItem(hWnd, IDC_PREV_OK);
				HWND hCancel    = GetDlgItem(hWnd, IDC_PREV_CANCEL);
				HWND hPrev      = GetDlgItem(hWnd, IDC_PREV_IMAGE);
				HWND hStart     = GetDlgItem(hWnd, IDC_PREV_START);
				HWND hStop      = GetDlgItem(hWnd, IDC_PREV_STOP);
				HWND hRefresh   = GetDlgItem(hWnd, IDC_PREV_REFRESH);
				HWND hSave      = GetDlgItem(hWnd, IDC_PREV_SAVE_IMG);
				HWND hHand      = GetDlgItem(hWnd, IDC_PREV_HANDTOOL);
				HWND hRect      = GetDlgItem(hWnd, IDC_PREV_RECT);
				HWND hAir       = GetDlgItem(hWnd, IDC_PREV_AIRBRUSH);
				HWND hFill      = GetDlgItem(hWnd, IDC_PREV_FILL);
				HWND hClear     = GetDlgItem(hWnd, IDC_PREV_CLEAR);
				HWND hInvert    = GetDlgItem(hWnd, IDC_PREV_INVERT);
				HWND hASize     = GetDlgItem(hWnd, IDC_PREV_AIRBRUSH_SIZE);
				HWND hGB1       = GetDlgItem(hWnd, IDC_PREV_GROUP_REGION);
				
				SetWindowPos(hPrev, HWND_TOP, 0,0, 480, 480, SWP_NOOWNERZORDER | SWP_NOMOVE);

				EMButton * embOK             = GetEMButtonInstance(hOK);
				EMButton * embCancel         = GetEMButtonInstance(hCancel);
				EMPView * empv               = GetEMPViewInstance(hPrev);
				EMImgButton * emibstart      = GetEMImgButtonInstance(hStart);
				EMImgButton * emibstop       = GetEMImgButtonInstance(hStop);
				EMImgButton * emibrefresh    = GetEMImgButtonInstance(hRefresh);
				EMImgButton * emibsave       = GetEMImgButtonInstance(hSave);
				EMImgStateButton * emibhand  = GetEMImgStateButtonInstance(hHand);
				EMImgStateButton * emibair   = GetEMImgStateButtonInstance(hAir);
				EMImgButton * emibfill       = GetEMImgButtonInstance(hFill);
				EMImgButton * emibclear      = GetEMImgButtonInstance(hClear);
				EMImgButton * emibinv        = GetEMImgButtonInstance(hInvert);
				EMEditTrackBar * emetas      = GetEMEditTrackBarInstance(hASize);
				EMGroupBar * emgb1           = GetEMGroupBarInstance(hGB1);

				// Init Start button
				emibstart->bmAll = &(gIcons);
				emibstart->pNx = emibstart->pMx = emibstart->pCx = emibstart->pDx = 0;
				emibstart->pNy = 0;
				emibstart->pMy = 32;
				emibstart->pCy = 64;
				emibstart->pDy = 96;

				// Init Stop button
				emibstop->bmAll = &(gIcons);
				emibstop->pNx = emibstop->pMx = emibstop->pCx = emibstop->pDx = 64;
				emibstop->pNy = 0;
				emibstop->pMy = 32;
				emibstop->pCy = 64;
				emibstop->pDy = 96;

				// Init Refresh button
				emibrefresh->bmAll = &(gIcons);
				emibrefresh->pNx = emibrefresh->pMx = emibrefresh->pCx = emibrefresh->pDx = 448;
				emibrefresh->pNy = 0;
				emibrefresh->pMy = 32;
				emibrefresh->pCy = 64;
				emibrefresh->pDy = 96;

				// Init Save button
				emibsave->bmAll = &(gIcons);
				emibsave->pNx = emibsave->pMx = emibsave->pCx = emibsave->pDx = 96;
				emibsave->pNy = 0;
				emibsave->pMy = 32;
				emibsave->pCy = 64;
				emibsave->pDy = 96;

				// Init Handtool button
				emibhand->bmAll = &(gIcons);
				emibhand->pNx = emibhand->pMx = emibhand->pCx = emibhand->pDx = 416;
				emibhand->pNy = 0;
				emibhand->pMy = 32;
				emibhand->pCy = 64;
				emibhand->pDy = 96;

				// Init Rectangle button
				EMImgStateButton * emibrect = GetEMImgStateButtonInstance(hRect);
				emibrect->bmAll = &(gIcons);
				emibrect->pNx = emibrect->pMx = emibrect->pCx = emibrect->pDx = 288;
				emibrect->pNy = 0;
				emibrect->pMy = 32;
				emibrect->pCy = 64;
				emibrect->pDy = 96;

				// Init Airbrush button
				emibair->bmAll = &(gIcons);
				emibair->pNx = emibair->pMx = emibair->pCx = emibair->pDx = 256;
				emibair->pNy = 0;
				emibair->pMy = 32;
				emibair->pCy = 64;
				emibair->pDy = 96;

				// Init Fill button
				emibfill->bmAll = &(gIcons);
				emibfill->pNx = emibfill->pMx = emibfill->pCx = emibfill->pDx = 320;
				emibfill->pNy = 0;
				emibfill->pMy = 32;
				emibfill->pCy = 64;
				emibfill->pDy = 96;

				// Init Clear button
				emibclear->bmAll = &(gIcons);
				emibclear->pNx = emibclear->pMx = emibclear->pCx = emibclear->pDx = 384;
				emibclear->pNy = 0;
				emibclear->pMy = 32;
				emibclear->pCy = 64;
				emibclear->pDy = 96;

				// Init Invert button
				emibinv->bmAll = &(gIcons);
				emibinv->pNx = emibinv->pMx = emibinv->pCx = emibinv->pDx = 352;
				emibinv->pNy = 0;
				emibinv->pMy = 32;
				emibinv->pCy = 64;
				emibinv->pDy = 96;

				emetas->setEditBoxWidth(32);
				emetas->setValuesToInt(10,2,30,1);

				if (!empv->byteBuff)
					empv->byteBuff = new ImageByteBuffer();
				empv->byteBuff->allocBuffer(480,480);
				empv->byteBuff->clearBuffer();


				GlobalWindowSettings::colorSchemes.apply(embOK);
				GlobalWindowSettings::colorSchemes.apply(embCancel);
				GlobalWindowSettings::colorSchemes.apply(empv);
				GlobalWindowSettings::colorSchemes.apply(emibstart);
				GlobalWindowSettings::colorSchemes.apply(emibstop);
				GlobalWindowSettings::colorSchemes.apply(emibrefresh);
				GlobalWindowSettings::colorSchemes.apply(emibsave);

				GlobalWindowSettings::colorSchemes.apply(emibhand);
				GlobalWindowSettings::colorSchemes.apply(emibrect);
				GlobalWindowSettings::colorSchemes.apply(emibair);
				GlobalWindowSettings::colorSchemes.apply(emibfill);
				GlobalWindowSettings::colorSchemes.apply(emibclear);
				GlobalWindowSettings::colorSchemes.apply(emibinv);
				GlobalWindowSettings::colorSchemes.apply(emetas);
				GlobalWindowSettings::colorSchemes.apply(emgb1);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);

				switch (wmId)
				{
					case IDC_PREV_OK:
						{
							if (wmEvent != BN_CLICKED)
								break;
							
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							resizePreviewBeforeClosing(hPr);

							EndDialog(hWnd, (INT_PTR)(1));
						}
						break;
					case IDC_PREV_CANCEL:
						{
							if (wmEvent != BN_CLICKED)
								break;
							EndDialog(hWnd, (INT_PTR)(NULL));
						}
						break;
					case IDC_PREV_START:
						{
							if (wmEvent != BN_CLICKED)
								break;

							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							startRenderingZoomPreview(hWnd, hPr);
							runRefreshThread(hPr);
						}
						break;
					case IDC_PREV_STOP:
						{
							if (wmEvent != BN_CLICKED)
								break;

							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							stopRenderingZoomPreview(hWnd, hPr);
							stopRefreshThread();
						}
						break;
					case IDC_PREV_PAUSE:
						{
							if (wmEvent != BN_CLICKED)
								break;
						}
						break;
					case IDC_PREV_REFRESH:
						{
							if (wmEvent != BN_CLICKED)
								break;
							
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							updateRenderedImage(empv, true, false, NULL, true);
							InvalidateRect(hPr, NULL, false);
						}
						break;
					case IDC_PREV_SAVE_IMG:
						{
							if (wmEvent != BN_CLICKED)
								break;

							Logger::add("Zoom window: Save image button pressed. Dialog opened.");
							char * filename = saveFileDialog(hWnd, 
								"PNG image (*.png)\0*.png\0", 
								"Save image", "png", 1);

							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							ImageByteBuffer * ibbuf = empv->byteBuff;
							CHECK(ibbuf);
							bool saveOK = ibbuf->saveAsWindowsFormat(filename, 3, NULL);
						}
						break;
					case IDC_PREV_HANDTOOL:
						{
							if (wmEvent != BN_CLICKED)
								break;
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							empv->mode = EMPView::MODE_SHOW;

							HWND h1 = GetDlgItem(hWnd, IDC_PREV_RECT);
							HWND h2 = GetDlgItem(hWnd, IDC_PREV_AIRBRUSH);
							EMImgStateButton * emisb1 = GetEMImgStateButtonInstance(h1);
							EMImgStateButton * emisb2 = GetEMImgStateButtonInstance(h2);
							emisb1->stateOn = false;
							emisb2->stateOn = false;
							InvalidateRect(h1, NULL, false);
							InvalidateRect(h2, NULL, false);
						}
						break;
					case IDC_PREV_RECT:
						{
							if (wmEvent != BN_CLICKED)
								break;
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							empv->mode = EMPView::MODE_REGION_RECT;

							HWND h1 = GetDlgItem(hWnd, IDC_PREV_HANDTOOL);
							HWND h2 = GetDlgItem(hWnd, IDC_PREV_AIRBRUSH);
							EMImgStateButton * emisb1 = GetEMImgStateButtonInstance(h1);
							EMImgStateButton * emisb2 = GetEMImgStateButtonInstance(h2);
							emisb1->stateOn = false;
							emisb2->stateOn = false;
							InvalidateRect(h1, NULL, false);
							InvalidateRect(h2, NULL, false);
						}
						break;
					case IDC_PREV_AIRBRUSH:
						{
							if (wmEvent != BN_CLICKED)
								break;
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							empv->mode = EMPView::MODE_REGION_AIRBRUSH;

							HWND h1 = GetDlgItem(hWnd, IDC_PREV_HANDTOOL);
							HWND h2 = GetDlgItem(hWnd, IDC_PREV_RECT);
							EMImgStateButton * emisb1 = GetEMImgStateButtonInstance(h1);
							EMImgStateButton * emisb2 = GetEMImgStateButtonInstance(h2);
							emisb1->stateOn = false;
							emisb2->stateOn = false;
							InvalidateRect(h1, NULL, false);
							InvalidateRect(h2, NULL, false);
						}
						break;
					case IDC_PREV_FILL:
						{
							if (wmEvent != BN_CLICKED)
								break;
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							if (empv->rgnPtr)
								empv->rgnPtr->fillRegions();
							InvalidateRect(hPr, NULL, false);
						}
						break;
					case IDC_PREV_CLEAR:
						{
							if (wmEvent != BN_CLICKED)
								break;
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							if (empv->rgnPtr)
								empv->rgnPtr->clearRegions();
							InvalidateRect(hPr, NULL, false);
						}
						break;
					case IDC_PREV_INVERT:
						{
							if (wmEvent != BN_CLICKED)
								break;
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							EMPView * empv = GetEMPViewInstance(hPr);
							if (empv->rgnPtr)
								empv->rgnPtr->inverseRegions();
							InvalidateRect(hPr, NULL, false);
						}
						break;
					case IDC_PREV_AIRBRUSH_SIZE:
						{
							if (wmEvent != WM_HSCROLL)
								break;
							HWND hPr = GetDlgItem(hWnd, IDC_PREV_IMAGE);
							HWND hSize = GetDlgItem(hWnd, IDC_PREV_AIRBRUSH_SIZE);
							EMPView * empv = GetEMPViewInstance(hPr);
							EMEditTrackBar * emetb = GetEMEditTrackBarInstance(hSize);
							if (empv->rgnPtr)
								empv->rgnPtr->airbrush_size = emetb->intValue + 4;
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}


void lockBeforeStartRendering(HWND hwnd)
{
	disableControl(hwnd, IDC_PREV_START);
	enableControl (hwnd, IDC_PREV_STOP);
	enableControl (hwnd, IDC_PREV_PAUSE);
	enableControl (hwnd, IDC_PREV_REFRESH);
	disableControl(hwnd, IDC_PREV_SAVE_IMG);
	disableControl(hwnd, IDC_PREV_OK);
	disableControl(hwnd, IDC_PREV_CANCEL);
	enableControl (hwnd, IDC_PREV_HANDTOOL);
	enableControl (hwnd, IDC_PREV_RECT);
	enableControl (hwnd, IDC_PREV_AIRBRUSH);
	enableControl (hwnd, IDC_PREV_FILL);
	enableControl (hwnd, IDC_PREV_CLEAR);
	enableControl (hwnd, IDC_PREV_INVERT);
}

void unlockAfterStopRendering(HWND hwnd)
{
	enableControl (hwnd, IDC_PREV_START);
	disableControl(hwnd, IDC_PREV_STOP);
	disableControl(hwnd, IDC_PREV_PAUSE);
	enableControl (hwnd, IDC_PREV_REFRESH);
	enableControl (hwnd, IDC_PREV_SAVE_IMG);
	enableControl (hwnd, IDC_PREV_OK);
	enableControl (hwnd, IDC_PREV_CANCEL);
}

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

DWORD WINAPI bigPreviewRefreshThreadProc(LPVOID lpParameter)
{
	RThreadValues * rtv = (RThreadValues *)lpParameter;

	clock_t lTime = clock();
	clock_t cTime;
	double diff;
	HWND hPr = rtv->hPreview;
	EMPView * empv = GetEMPViewInstance(hPr);
	int s = rtv->seconds;

	while (rtv->running)
	{
		Sleep(100);
		while (rtv->paused)
		{
			if (!rtv->running)
				return 0;
			Sleep(50);
		}

		cTime = clock();
		diff  = (cTime - lTime)/(double)CLOCKS_PER_SEC;
		if (diff > s)
		{
			updateRenderedImage(empv, true, false, NULL, true);
			InvalidateRect(hPr, NULL, false);
			lTime = clock();
		}
	}

	return 0;
}

//------------------------------------------------------------------------------------------------------

bool startRenderingZoomPreview(HWND hDlg, HWND hImg)
{
	EMPView * empv = GetEMPViewInstance(hImg);
	CHECK(empv);
	Raytracer * rtr = Raytracer::getInstance();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	CHECK(cam);

	rtr->curScenePtr->blendBits = rtr->curScenePtr->evalBlendBits();
	cam->width = 480;
	cam->height = 480;
	empv->rgnPtr = &(cam->staticRegion);
	cam->initializeCamera(true);
	empv->otherSource = cam->imgBuff;
	empv->rgnPtr->overallHits = 0;
	empv->rgnPtr->createRegions(cam->width, cam->height);

	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	int prn = rtr->curScenePtr->thrM->getProcessorsNumber();
	if (prn < 1)
		prn = 1;
	#ifdef DEBUG_ONE_THREAD
		prn = 1;
	#endif

	lockBeforeStartRendering(hDlg);

	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		MaterialNox * m = rtr->curScenePtr->mats[i];
		if (m)
			m->prepareToRender();
	}

	cam->nowRendering = true;

	rtr->curScenePtr->thrM->initThreads(prn, false);
	rtr->curScenePtr->thrM->runThreads();
	rtr->curScenePtr->nowRendering = true;
	return true;

}

//------------------------------------------------------------------------------------------------------

bool stopRenderingZoomPreview(HWND hDlg, HWND hImg)
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->resumeThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	unlockAfterStopRendering(hDlg);
	rtr->curScenePtr->nowRendering = false;
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	cam->nowRendering = false;
	return true;
}

//------------------------------------------------------------------------------------------------------

bool resizePreviewBeforeClosing(HWND hImg)
{
	Raytracer * rtr = Raytracer::getInstance();
	EMPView * empv = GetEMPViewInstance(hImg);
	CHECK(empv);
	if (empv->byteBuff)
	{
		ImageByteBuffer * newbuf = empv->byteBuff->copyWithZoom(0.4);
		if (newbuf)
		{
			Scene * sc = rtr->curScenePtr;
			int edm = sc->sscene.editableMaterial;
			MaterialNox * mat = sc->mats[edm];
			CHECK(mat);
			ImageByteBuffer * oldbuf = mat->preview;
			mat->preview = newbuf;
			if (oldbuf)
				delete oldbuf;
		}
	}
	return true;
}

//------------------------------------------------------------------------------------------------------

void stopRefreshThread()
{
	rtv.running = false;
}

//------------------------------------------------------------------------------------------------------

void runRefreshThread(HWND hPrev)
{
	rtv.hPreview = hPrev;
	rtv.seconds = 10;
	rtv.paused = false;
	rtv.running = true;
	DWORD threadID;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(bigPreviewRefreshThreadProc), (LPVOID)&rtv, 0, &threadID);
}

//------------------------------------------------------------------------------------------------------
