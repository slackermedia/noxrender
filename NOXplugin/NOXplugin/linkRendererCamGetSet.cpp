#include "linkRenderer.h"
#ifdef PORTABLE_COMPILATION
	#include "noxinclude/resource.h"
	#include "noxinclude/Dialogs.h"
#else
	#include "../../Everything DLL/Everything DLL/resource.h"
	#include "../../Everything DLL/Everything DLL/Dialogs.h"
#endif


bool TrCamera::copySettingsFrom(TrCamera * other)
{
	if (!other)
		return false;

	setHorzLineState(other->getHorzLineState());
	setFOVType(other->getFOVType());
	setType(other->getType());
	setOrtho(other->getIsOrtho());
	setFOV(other->getFOV()); 
	setManualClip(other->getManualClip());
	setClipDistHither(other->getClipDistHither());
	setClipDistYon(other->getClipDistYon());
	setEnvRangeNear(other->getEnvRangeNear());
	setEnvRangeFar(other->getEnvRangeFar());

	setISO(other->getISO());
	setAperture(other->getAperture());
	setShutter(other->getShutter());
	setAutoExpOn(other->getAutoExpOn());
	setAutoExpType(other->getAutoExpType());
	setAutoFocusOn(other->getAutoFocusOn());
	setFocusDistance(other->getFocusDistance());
	//setActive(other->getIsActive());
	setBladesNumber(other->getBladesNumber());
	setBladesAngle(other->getBladesAngle());
	setBladesRound(other->getBladesRound());
	setBladesRadius(other->getBladesRadius());
	setDofON(other->getDofON());
	setBokehRingBalance(other->getBokehRingBalance());
	setBokehRingSize(other->getBokehRingSize());
	setBokehFlatten(other->getBokehFlatten());
	setVignette(other->getVignette());

	return true;
}

//===================================================================================================================
// GET stuff

bool TrCamera::getIsOrtho()
{
	return false;
}

float TrCamera::getFOV()
{
	return fov;
}

float TrCamera::getClipDistHither()
{
	return clipDistHither;
}

float TrCamera::getClipDistYon()
{
	return clipDistYon;
}

float TrCamera::getEnvRangeNear()
{
	return envRangeNear;
}

float TrCamera::getEnvRangeFar()
{
	return envRangeFar;
}

int TrCamera::getManualClip()
{
	return manualClip;
}

//int TrCamera::getConeState()
//{
//	return coneState;
//}

int TrCamera::getHorzLineState()
{
	return horLine;
}

int TrCamera::getFOVType()
{
	return fovType;
}

int TrCamera::getType()
{
	return camType;
}

//===================================================================================================================
// SET stuff

void TrCamera::setHorzLineState(int s)
{
	horLine = s;
}

void TrCamera::setFOVType(int fovtype)
{
	fovType = fovtype;
}

void TrCamera::setType(int type)
{
	camType = type;
}

void TrCamera::setOrtho(bool ortho)
{
}

void TrCamera::setFOV(float f)
{
	fov = f;
}

void TrCamera::setManualClip(int onOff)
{
	manualClip = onOff;
}

void TrCamera::setClipDistHither(float val)
{
	clipDistHither = val;
}

void TrCamera::setClipDistYon(float val)
{
	clipDistYon = val;
}

void TrCamera::setEnvRangeNear(float f)
{
	envRangeNear = f;
}

void TrCamera::setEnvRangeFar(float f)
{
	envRangeFar = f;
}

//===================================================================================================================

float TrCamera::getISO()
{
	if (address)
		return ((Camera*)address)->ISO;
	return 100;
}

float TrCamera::getAperture()
{
	if (address)
		return ((Camera*)address)->aperture;
	return 8;
}

float TrCamera::getShutter()
{
	if (address)
		return ((Camera*)address)->shutter;
	return 50;
}

bool TrCamera::getAutoExpOn()
{
	if (address)
		return ((Camera*)address)->autoexposure;
	return false;
}

int TrCamera::getAutoExpType()
{
	if (address)
		return ((Camera*)address)->autoexposureType;
	return 2;
}

bool TrCamera::getAutoFocusOn()
{
	if (address)
		return ((Camera*)address)->autoFocus;
	return false;
}

float TrCamera::getFocusDistance()
{
	if (address)
		return ((Camera*)address)->focusDist;
	return 100;
}

bool TrCamera::getIsActive()
{
	return (active!=0);
}

int TrCamera::getBladesNumber()
{
	if (address)
		return ((Camera*)address)->apBladesNum;
	return 5;
}

float TrCamera::getBladesAngle()
{
	if (address)
		return ((Camera*)address)->apBladesAngle;
	return 0.0f;
}

bool TrCamera::getBladesRound()
{
	if (address)
		return ((Camera*)address)->apBladesRound;
	return false;
}

float TrCamera::getBladesRadius()
{
	if (address)
		return ((Camera*)address)->apBladesRadius;
	return 1.0f;
}

bool  TrCamera::getDofON()
{
	if (address)
		return ((Camera*)address)->dofOnTemp;
	return true;
}

int TrCamera::getBokehRingBalance()
{
	if (address)
		return ((Camera*)address)->diaphSampler.intBokehRingBalance;
	return 0;
}

int  TrCamera::getBokehRingSize()
{
	if (address)
		return ((Camera*)address)->diaphSampler.intBokehRingSize;
	return 0;
}

int  TrCamera::getBokehFlatten()
{
	if (address)
		return ((Camera*)address)->diaphSampler.intBokehFlatten;
	return 0;
}

int  TrCamera::getVignette()
{
	if (address)
		return ((Camera*)address)->diaphSampler.intBokehVignette;
	return 0;
}


//===================================================================================================================

void TrCamera::setISO(float f)
{
	if (address)
		((Camera*)address)->ISO = f;
}

void TrCamera::setAperture(float f)
{
	if (address)
		((Camera*)address)->aperture = f;
}

void TrCamera::setShutter(float f)
{
	if (address)
		((Camera*)address)->shutter = f;
}

void TrCamera::setAutoExpOn(bool b)
{
	if (address)
		((Camera*)address)->autoexposure = b;
}

void TrCamera::setAutoExpType(int i)
{
	if (address)
		((Camera*)address)->autoexposureType = i;
}

void TrCamera::setAutoFocusOn(bool b)
{
	if (address)
		((Camera*)address)->autoFocus = b;
}

void TrCamera::setFocusDistance(float f)
{
	if (address)
		((Camera*)address)->focusDist = f;
}

void TrCamera::setActive(bool b)
{
	active = b;
}

void TrCamera::setBladesNumber(int i)
{
	if (address)
		((Camera*)address)->apBladesNum = i;
}

void TrCamera::setBladesAngle(float f)
{
	if (address)
		((Camera*)address)->apBladesAngle = f;
}

void TrCamera::setBladesRound(bool b)
{
	if (address)
		((Camera*)address)->apBladesRound = b;
}

void TrCamera::setBladesRadius(float f)
{
	if (address)
		((Camera*)address)->apBladesRadius = f;
}

void TrCamera::setDofON(bool b)
{
	if (address)
		((Camera*)address)->dofOnTemp = b;
}

void TrCamera::setBokehRingBalance(int i)
{
	if (address)
		((Camera*)address)->diaphSampler.intBokehRingBalance = i;
}

void TrCamera::setBokehRingSize(int i)
{
	if (address)
		((Camera*)address)->diaphSampler.intBokehRingSize = i;
}

void TrCamera::setBokehFlatten(int i)
{
	if (address)
		((Camera*)address)->diaphSampler.intBokehFlatten = i;
}

void TrCamera::setVignette(int i)
{
	if (address)
		((Camera*)address)->diaphSampler.intBokehVignette = i;
}
	

