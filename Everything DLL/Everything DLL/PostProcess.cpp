#include <Windows.h>
#include "EMControls.h"
#include "log.h"


bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Camera * cam = sc->getActiveCamera();
	if (!cam)
		return false;

	DWORD tickStartUpdate = GetTickCount();
	
	bool doDirect = rtr->curScenePtr->sscene.drawBufDirect;
	bool doGI = rtr->curScenePtr->sscene.drawBufGI;
	bool doGI_normal = (cam->gi_type == Camera::GI_TYPE_PT);

	DWORD tickStartSum = GetTickCount();

	ImageBuffer * ib = cam->blendBuffers(doDirect, doGI, doGI_normal, rgn);

	DWORD tickAfterSum = GetTickCount();

	FinalModifier * fMod = NULL;
	cam->fMod.focal = 0.5f * 36 / tan(cam->angle * PI / 360.0f) * 0.001f;
	if (final)
		fMod = &cam->fMod;
	ImageModifier iModCopy;
	iModCopy.copyDataFromAnother(empv->imgMod);
	if (rgn)
	{
		ib->vig_buck_pos_x = rgn->left;
		ib->vig_buck_pos_y = rgn->top;
		ib->vig_orig_width = cam->width;
		ib->vig_orig_height = cam->height;
	}

	DWORD tickStartPost = GetTickCount();

	ImageBuffer * pp = ib->doPostProcess(cam->aa, &iModCopy, fMod, cam->depthBuf, (rgn?NULL:"Redrawing..."));

	DWORD tickAfterPost = GetTickCount();

	ib->freeBuffer();
	delete ib;

	DWORD tickStartByte= GetTickCount();
	
	if (rgn)
		pp->copyPartToImageByteBuffer(empv->byteBuff, rgn->left, rgn->top);
	else
		pp->copyToImageByteBuffer(empv->byteBuff, RGB(255,0,0));
	
	DWORD tickAfterByte= GetTickCount();

	empv->updateHistogram();
	pp->freeBuffer();
	delete pp;
	pp = NULL;

	DWORD tickDone = GetTickCount();

	char buff[256];
	sprintf_s(buff, 256, "all update took %d ms, summing: %d ms, postpro: %d ms, to byte: %d ms",
		(tickDone-tickStartUpdate), (tickAfterSum-tickStartSum), (tickAfterPost-tickStartPost), (tickAfterByte-tickStartByte));
	Logger::add(buff);

	return true;
}
