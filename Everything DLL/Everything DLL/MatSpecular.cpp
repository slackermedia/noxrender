#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>

#define clamp(x,a,b)   max((a),min((x),(b)))

float getFresnelTemp(float IOR, float kos, bool goingIn);
float getFresnelTemp(float IOR, float kos);

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionIdealReflectionTransmission(HitData & hd, float &probability)
{
	hd.setFlagDelta();

	if (dispersionOn)
		return randomNewDirectionDispersionGlass(hd, probability);

	Vector3d res;

	if (fakeGlass  &&  transmOn)
	{
		float f = fresnel.getValueFresnelFromCos(fabs(hd.normal_shade*hd.in));
		f = getFresnelTemp(fresnel.IOR, fabs(hd.in*hd.normal_shade), true );
		#ifdef RAND_PER_THREAD
			float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
		#else
			float a = ((float)rand()/(float)RAND_MAX);
		#endif
		if (a>f)
		{
			probability = (1-f);
			hd.setFlagGhostReflection();
			return hd.out = hd.in * -1;
		}
		else
		{
			probability = f;
			return hd.out = hd.in.reflect(hd.normal_shade);
		}
	}

	if (!transmOn)
	{
		probability = 1;
		res = hd.in.reflect(hd.normal_shade);
		hd.out = res;
		return res;
	}

	float f = getFresnelTemp(fresnel.IOR, fabs(hd.in*hd.normal_shade), (hd.in*hd.normal_shade > 0) );

	f=sqrt(f);	// more samples on small probability directions

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = ((float)rand()/(float)RAND_MAX);
	#endif
	if (a>f)
	{
		bool inner;
		probability = 1;

		float ior;
		if (hd.in*hd.normal_shade > 0)
			ior = fresnel.IOR;
		else
			ior = 1.0f/fresnel.IOR;

		res = hd.in.refractSnell(hd.normal_shade, ior, inner);
		if (inner)
		{
			hd.setFlagInternalReflection();
			probability = 1;
		}
		else
			probability = 1-f;
	}
	else
	{
		probability = f;
		res = hd.in.reflect(hd.normal_shade);
	}

	res.normalize();
	hd.out = res;
	return res;
}

//-------------------------------------------------------------------------------------------

Color4 MatLayer::getBRDFIdealReflectionTransmission(HitData & hd)
{
	bool inner = false;
	bool transmitted;
	hd.setFlagDelta();

	float cosThetaInShade  = hd.normal_shade*hd.in;
	float cosThetaOutShade = hd.normal_shade*hd.out;
	float cosThetaInGeom   = hd.normal_geom *hd.in;
	float cosThetaOutGeom  = hd.normal_geom *hd.out;

	if (dispersionOn)
		return getBRDFDispersionGlass(hd);

	if (fakeGlass  &&  transmOn)
	{
		transmitted = (cosThetaInShade * cosThetaOutShade < 0);
		float f = getFresnelTemp(fresnel.IOR, fabs(cosThetaInShade), true );
		if (transmitted)
		{
			if (hd.in * hd.out < -0.999)
			{
				hd.setFlagGhostReflection();
				Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
				if ((hd.in*hd.normal_shade < 0)   &&   absOn)
				{	
					absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
					absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
					absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
				}
				return getTransmissionColor(hd.tU, hd.tV) * absorption * (1-f);
			}
			else
				return Color4(0,0,0);
		}
		else
		{
			if (hd.in.reflect(hd.normal_shade)*hd.out > 0.999f)
				return getColor90(hd.tU, hd.tV) * f;
			else
				return Color4(0,0,0);
		}
	}


	float ior;
	if (cosThetaInShade > 0)
		ior = fresnel.IOR;
	else
		ior = 1.0f/fresnel.IOR;

	Vector3d snell = hd.in.refractSnell(hd.normal_shade, ior, inner);
	snell.normalize();

	if (hd.out*snell > 0.999f    && !inner)
		transmitted = true;
	else
	{	
		Vector3d refl = hd.in.reflect(hd.normal_shade);
		refl.normalize();
		hd.out.normalize();
		if (hd.out*refl > 0.999f)
			transmitted = false;	
		else
			return Color4(0,0,0);
	}

	float cos_i = fabs(cosThetaInShade);
	cos_i = min(1.0f, cos_i);
	float f = getFresnelTemp(fresnel.IOR, cos_i, (cosThetaInShade > 0) );

	Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
	if ((hd.in*hd.normal_shade < 0)   &&   absOn   &&   transmOn)
	{	
		absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
		absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
		absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
	}

	if (transmitted  &&   transmOn)
	{
		Color4 trCol = getTransmissionColor(hd.tU, hd.tV);
		return absorption*trCol*(1-f);
	}
	else
	{
		if (inner)
		{
			hd.setFlagInternalReflection();
		}

		float nthI = fabs(FastMath::acos(min(1.0f, fabs(cosThetaInShade))))/PI*2.0f;
		Color4 result = (getColor0(hd.tU, hd.tV)*(1-nthI) + getColor90(hd.tU, hd.tV)*nthI) * absorption * f;

		return result; 
	}
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityIdealReflectionTransmission(HitData & hd)
{
	if (dispersionOn)
		return getProbabilityDispersionGlass(hd);

	bool inner = false;
	float ior;
	bool transmitted;
	Vector3d refl;

	float cosThetaInShade  = hd.normal_shade*hd.in;
	float cosThetaOutShade = hd.normal_shade*hd.out;
	float cosThetaInGeom   = hd.normal_geom *hd.in;
	float cosThetaOutGeom  = hd.normal_geom *hd.out;

	if (fakeGlass  &&  transmOn)
	{
		transmitted = (cosThetaInShade * cosThetaOutShade < 0);
		float f = getFresnelTemp(fresnel.IOR, fabs(cosThetaInShade), true );
		if (transmitted)
		{
			if (hd.in * hd.out < -0.999)
				return (1-f);
			else
				return 0;
		}
		else
		{
			if (hd.in.reflect(hd.normal_shade)*hd.out > 0.999f)
				return f;
			else
				return 0;
		}
	}

	if (cosThetaInShade > 0)
		ior = fresnel.IOR;
	else
		ior = 1.0f/fresnel.IOR;

	if (cosThetaInShade * cosThetaOutShade > 0)
		transmitted = false;
	else
		transmitted = true;

	if (cosThetaInShade < 0)
	{
		if (!transmitted  &&  transmOn)
			refl = hd.in.refractSnell(hd.normal_shade, ior, inner);
	}

	if (transmitted  &&  transmOn)
		refl = hd.in.refractSnell(hd.normal_shade, ior, inner);
	else
		refl = hd.in.reflect(hd.normal_shade);

	if (refl * hd.out > 0.999f)
	{
		float f = getFresnelTemp(fresnel.IOR, fabs(cosThetaInShade), (cosThetaInShade > 0) );

		f=sqrt(f);

		if (!transmitted)
		{
			if (transmOn)
				if (inner)
				{
					hd.setFlagInternalReflection();
					return 1;
				}
				else
					return f;
			else
				return 1;
		}
		else
		{
			if (transmOn)
				return 1-f;
			else
			{
				Logger::add("No transmission, yet almost perfectly refracted direction. This may happen very rarely, otherwise this may be a bug.");
				return 0;
			}

		}
	}
	else
		return 0;

	return 1;
}

//-------------------------------------------------------------------------------------------
#define EQUALS_ROUND(a, b, delta) (fabs((a)-(b))<(delta))

bool MatLayer::getBRDFandPDFsIdealReflectionTransmission(HitData &hd, Color4 &brdf, float &pdf_f, float &pdf_b)
{
	if (dispersionOn)
	{
		brdf = getBRDFDispersionGlass(hd);
		pdf_f = getProbabilityDispersionGlass(hd);
		HitData hd2 = hd;
		hd2.swapDirections();
		pdf_b = getProbabilityDispersionGlass(hd2);
		return true;
	}

	hd.setFlagDelta();

	float cosThetaInShade  = hd.normal_shade*hd.in;
	float cosThetaOutShade = hd.normal_shade*hd.out;
	float cosThetaInGeom   = hd.normal_geom *hd.in;
	float cosThetaOutGeom  = hd.normal_geom *hd.out;
	float F = -1;

	if (fakeGlass  &&  transmOn)	// --- fakeglass ---
	{
		bool transmitted = (cosThetaInShade * cosThetaOutShade < 0);
		float f = getFresnelTemp(fresnel.IOR, fabs(cosThetaInShade), true );
		if (transmitted)
		{
			if (hd.in * hd.out < -0.999)
			{
				hd.setFlagGhostReflection();
				Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
				if ((cosThetaInShade < 0)   &&   absOn)
				{	
					absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
					absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
					absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
				}
				brdf = getTransmissionColor(hd.tU, hd.tV) * absorption * (1-f);
				pdf_f = 1-f;
				pdf_b = 1-f;
			}
			else
			{
				pdf_f = 0.0f;
				pdf_b = 0.0f;
				brdf = Color4(0.0f, 0.0f, 0.0f);
			}
		}
		else
		{
			if (hd.in.reflect(hd.normal_shade)*hd.out > 0.999f)
			{
				pdf_f = f;
				pdf_b = f;
				brdf = getColor90(hd.tU, hd.tV) * f;			// todo: a moze tez troche z col0 ?
			}
			else
			{
				pdf_f = 0.0f;
				pdf_b = 0.0f;
				brdf = Color4(0.0f, 0.0f, 0.0f);
			}
		}
	}
	else	// --- no fakeglass ---
	{
		bool transmitted = (cosThetaInShade * cosThetaOutShade < 0);
		if (transmitted)
		{
			bool inner;
			float ior = (cosThetaInShade > 0) ? fresnel.IOR : 1.0f/fresnel.IOR;
			Vector3d refl = hd.in.refractSnell(hd.normal_shade, ior, inner);
			refl.normalize();
			if (transmOn  &&  (refl * hd.out > 0.995f))
			{
				F = getFresnelTemp(fresnel.IOR, fabs(cosThetaInShade), (cosThetaInShade>0));
				pdf_f = 1-sqrt(F);
				pdf_b = pdf_f;
				float Fb = getFresnelTemp(fresnel.IOR, fabs(cosThetaOutShade), (cosThetaOutShade>0));
				pdf_b = 1-sqrt(Fb);

				Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
				if ((cosThetaInShade < 0)   &&   absOn   &&   transmOn)
				{	
					absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
					absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
					absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
				}
				Color4 trCol = getTransmissionColor(hd.tU, hd.tV);
				brdf = absorption*trCol*(1-F);
			}
			else
			{
				pdf_f = 0.0f;
				pdf_b = 0.0f;
				brdf = Color4(0.0f, 0.0f, 0.0f);
			}
		}
		else
		{
			Vector3d refl = hd.in.reflect(hd.normal_shade);
			if (refl * hd.out > 0.999f)
			{
				F = getFresnelTemp(fresnel.IOR, fabs(cosThetaInShade), (cosThetaInShade>0 || !transmOn));
				pdf_f = transmOn ? sqrt(F) : 1.0f;
				pdf_b = pdf_f;
				Color4 absorption = Color4(1.0f, 1.0f, 1.0f);
				if (absOn   &&   transmOn   &&   cosThetaInShade<0)
				{	
					absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
					absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
					absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
				}
				float nthI = fabs(acos(min(1.0f, fabs(cosThetaInShade))))/PI*2.0f;
				brdf = (getColor0(hd.tU, hd.tV)*(1-nthI) + getColor90(hd.tU, hd.tV)*nthI) * absorption * F;
			}
			else
			{
				pdf_f = 0.0f;
				pdf_b = 0.0f;
				brdf = Color4(0.0f, 0.0f, 0.0f);
			}
		}
	}

	#ifdef VERIFY_MATLAYER_BRDF_PDFS
		Color4 vbrdf = getBRDFIdealReflectionTransmission(hd);
		float vpdf_f = getProbabilityIdealReflectionTransmission(hd);
		HitData hdback = hd;
		hdback.swapDirections();
		float vpdf_b = getProbabilityIdealReflectionTransmission(hdback);
		if (vbrdf.r!=brdf.r  ||  vbrdf.g!=brdf.g  ||  vbrdf.b!=brdf.b)
			Logger::add("brdf verify failed on specular");
		if (!EQUALS_ROUND(vpdf_f, pdf_f, 0.005f))
		{
			char buf[256];
			sprintf_s(buf, 256, "pdf forward verify failed on specular.. cos_in=%f  cos_out=%f  fpdf_new=%f  fpdf_old=%f  f=%f", cosThetaInShade, cosThetaOutShade, pdf_f, vpdf_f, F);
			Logger::add(buf);
		}
		if (!EQUALS_ROUND(vpdf_b, pdf_b, 0.005f))
		{
			char buf[256];
			sprintf_s(buf, 256, "pdf backward verify failed on specular.. cos_in=%f  cos_out=%f  bpdf_new=%f  bpdf_old=%f  f=%f", cosThetaInShade, cosThetaOutShade, pdf_b, vpdf_b, F);
			Logger::add(buf);
		}
	#endif

	return true;
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------


Vector3d MatLayer::randomNewDirectionPureSpecular(HitData & hd, float &probability)
{
	Vector3d res;

	res = hd.in.reflect(hd.normal_shade);
	hd.out = res;
	probability = 1;

	return res;
}

//----------		PURE SPECULAR BRDF

Color4 MatLayer::getBRDFPureSpecular(HitData & hd)		// not used 
{
	Vector3d r = hd.in.reflect(hd.normal_shade);
	if (r*hd.out > 0.999f)
	{
		float c = hd.out*hd.normal_shade;
		if (c>0.0001f)
			c = 1.0f/c;
		else 
			c = 10000.0f;
		float nthI = fabs(FastMath::acos(hd.in*hd.normal_shade))/PI*2.0f;
		return (getColor0(hd.tU, hd.tV)*(1-nthI) + getColor90(hd.tU, hd.tV)*nthI)*c;
	}
	else
		return Color4(0,0,0);
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilitySpecular(HitData & hd)	// not used 
{
	Vector3d r = hd.in.reflect(hd.normal_shade);
	if (r*hd.out > 0.9999f)
		return 1;
	else 
		return 0;
}

//-------------------------------------------------------------------------------------------
//===========================================================================================

Vector3d MatLayer::randomNewDirectionDispersionGlass(HitData & hd, float &probability)
{
	Vector3d res;
	float f = getFresnelTemp(fresnel.IOR, fabs(hd.in*hd.normal_shade), (hd.in*hd.normal_shade > 0) );

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = ((float)rand()/(float)RAND_MAX);
	#endif
	if (a>f)
	{
		bool inner;
		probability = 1;

		if (hd.spectr_pos<0.0f   ||   hd.spectr_pos>1.0f)
		{
			#ifdef RAND_PER_THREAD
				hd.spectr_pos = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
			#else
				hd.spectr_pos = ((float)rand()/(float)RAND_MAX);
			#endif
		}
		float rnd = hd.spectr_pos;
		rnd = rnd*2.0f - 1.0f;
		rnd = 1.0f + rnd*0.02f*dispersionValue;

		float ior;
		if (hd.in*hd.normal_shade > 0)
			ior = fresnel.IOR*rnd;
		else
			ior = 1.0f/fresnel.IOR/rnd;

		res = hd.in.refractSnell(hd.normal_shade, ior, inner);
		if (inner)
		{
			hd.setFlagInternalReflection();
			probability = 1;
		}
		else
			probability = 1-f;
	}
	else
	{
		probability = f;
		res = hd.in.reflect(hd.normal_shade);
	}

	hd.out = res;
	return res;
}

//-------------------------------------------------------------------------------------------

void evalTotalInternalReflectionColor(float l, float &r, float &g, float &b)
{
	float lr = min(0.5f, l);
	r = 1.0f - (4*lr-4*lr*lr);

	float lg1 = max(0.5f, min(0.75f, l));
	float lg2 = max(0.25f, min(0.5f, l));
	float g1 = 0.5f - 12.0f*lg1 + 8.0f*lg1*lg1;
	float g2 = -(2.0f*lg2*lg2-lg2)*4.0f;
	g = g1+g2;

	float lb = max(0.5f, l);
	b = -4.0f*(lb*lb-lb);
}

Color4 MatLayer::getBRDFDispersionGlass(HitData & hd)
{
	float cosIn  = hd.in*hd.normal_shade;
	float cosOut = hd.out*hd.normal_shade;
	bool orefl = ((cosIn)*(cosOut)>0);

	Vector3d a = hd.in^hd.normal_shade;
	Vector3d b = hd.out^hd.normal_shade;
	a.normalize();
	b.normalize();
	b *= -1;
	if (a*b<0.99999f)
		return Color4(0,0,0);

	if (orefl)
	{
		float minIOR = sqrt(1.0f/(1-cosIn*cosIn));
		if (_isnan(minIOR) || !_finite(minIOR))
			minIOR = 10000.0;
		float ior1 = fresnel.IOR*(1.0f-0.02f*dispersionValue);	// red
		float ior2 = fresnel.IOR*(1.0f+0.02f*dispersionValue);	// blue
		float l = (minIOR-ior1)/(ior2-ior1);

		if (fresnel.IOR > minIOR)
			hd.setFlagInternalReflection();

		float f = getFresnelTemp(fresnel.IOR, fabs(cosIn), (cosIn > 0) );
		float nthI = fabs(acos(cosIn))/PI*2.0f;
		if (hd.in.reflect(hd.normal_shade) * hd.out > 0.99999f)
			return (getColor0(hd.tU, hd.tV)*(1-nthI) + getColor90(hd.tU, hd.tV)*nthI) * f ;//* innerMpl;  ///////
		else
			return Color4(0,0,0);
	}

	float ll = 1.0f - cosIn*cosIn;
	float mm = 1.0f - cosOut*cosOut;

	if (fabs(cosIn)>0.99999f  ||  fabs(cosIn)>0.99999f  ||  ll==0.0f  ||  mm==0.0f)
		return getColor0(hd.tU, hd.tV);

	float ior = sqrt(ll/mm);

	float ior1, ior2;
	float l = 0.5f;
	if (cosIn > 0)	// air/vacuum side
	{
		ior1 = fresnel.IOR*(1.0f-0.02f*dispersionValue);	// red
		ior2 = fresnel.IOR*(1.0f+0.02f*dispersionValue);	// blue
		l = (ior-ior1)/(ior2-ior1);

	}
	else			// glass/other shit side
	{
		ior1 = fresnel.IOR*(1.0f-0.02f*dispersionValue);	// red
		ior2 = fresnel.IOR*(1.0f+0.02f*dispersionValue);	// blue
		l = (1.0f/ior-ior1)/(ior2-ior1);
	}

	if (l<0 || l>1)
		return Color4(0,0,0);

	float red = max(0, 1-l*2);
	float blue = max(0, (l-0.5f)*2);
	float green = max(0, 1-fabs(0.5f-l)*4);

	Color4 col = Color4(red,green,blue);

	return col * getTransmissionColor(hd.tU, hd.tV);
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityDispersionGlass(HitData & hd)
{

	Vector3d a = hd.in^hd.normal_shade;
	Vector3d b = hd.out^hd.normal_shade;
	a.normalize();
	b.normalize();
	b *= -1;
	if (a*b<0.99999f)
		return 0.0f;

	float cosIn  = hd.in*hd.normal_shade;
	float cosOut = hd.out*hd.normal_shade;
	bool orefl = ((cosIn)*(cosOut)>0);

	float f = getFresnelTemp(fresnel.IOR, fabs(cosIn), (cosIn > 0) );

	if (orefl)
		return f;
	else
		return 1-f;


	return 1.0f;
}

//-------------------------------------------------------------------------------------------

