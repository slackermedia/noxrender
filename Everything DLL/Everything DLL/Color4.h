#ifndef __COLOR_4_H__
#define __COLOR_4_H__

#define COLOR_SSE
#include <xmmintrin.h>
#include <intrin.h>

class HalfFloat;
class Color4;
class ColorHalf4;
class ColorHSV;
class ColorHSL;
class ColorHSI;

//---------------------------------------------------------------------

class HalfFloat
{
public:
	union
	{		
		unsigned char uc[2];
		char c[2];
		WORD w;
	};

	float toFloat();
	static HalfFloat fromFloat(float &src);

	bool isInf();
	bool isInfPlus();
	bool isInfMinus();
	bool isNaN();
	bool isZero();
	bool isDenorm();


	HalfFloat();
	HalfFloat(float &fsrc);
	~HalfFloat();
};

//---------------------------------------------------------------------

#ifdef COLOR_HALF_SSE
class _CRT_ALIGN(8) ColorHalf4
#else
class ColorHalf4
#endif
{
public:
	union 
	{
		struct
		{
			HalfFloat r,g,b,a;
		};
		#ifdef COLOR_HALF_SSE
			__m64 sse64;
		#endif
	};

	Color4 getColor4();

	ColorHalf4() {}
	ColorHalf4(float R, float G, float B, float A=1.0f) { r=R; g=G; b=B; a=A; }
	~ColorHalf4() {}
};

//---------------------------------------------------------------------

#ifdef COLOR_SSE
class _CRT_ALIGN(16) Color4
#else
class Color4
#endif
{
public:
	union
	{
		#ifdef COLOR_SSE
			__m128 sse;
		#endif
		struct
		{
			float r,g,b,a;
		};
		float V[4];
	};


	Color4() {r=g=b=0.0f; a=1.0f;}
	Color4(float R, float G, float B) {r=R; g=G; b=B; a=1.0f;}
	Color4(float R, float G, float B, float A) {r=R; g=G; b=B; a=A;}

	Color4 operator+(const Color4 &another) const;
	Color4 operator+=(const Color4 &another);
	Color4 operator*(const Color4 &another) const;
	Color4 operator*=(const Color4 &another);
	Color4 operator*(const float s) const;
	Color4 operator*=(const float s);
	bool isNaN();
	bool isInf();


	void clamp();
	COLORREF toColorRef();
	ColorHSV toColorHSV();
	ColorHSL toColorHSL();
	ColorHSI toColorHSI();
	bool isBlack();
	ColorHalf4 toHalfColor() { return ColorHalf4(r,g,b,a); }
	void toCIE_XYZ(float &X, float &Y, float &Z);
	void toCIE_xyY(float &x, float &y, float &Y);
};

//---------------------------------------------------------------------

class ColorHSV
{
public:
	float h,s,v;
	ColorHSV() {h=s=v=0.0f;}
	ColorHSV(float H, float S, float V) {h=H; s=S; v=V;}
	Color4 toColor4();
};

//---------------------------------------------------------------------

class ColorHSL
{
public:
	float h,s,l;
	ColorHSL() {h=s=l=0.0f;}
	ColorHSL(float H, float S, float L) {h=H; s=S; l=L;}
	Color4 toColor4();
};

//---------------------------------------------------------------------

class ColorHSI
{
public:
	float h,s,i;
	ColorHSI() {h=s=i=0.0f;}
	ColorHSI(float H, float S, float I) {h=H; s=S; i=I;}
	Color4 toColor4();
};

//---------------------------------------------------------------------

#endif
