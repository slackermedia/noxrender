#include <windows.h>
#include <stdio.h>
#include "nox_update.h"

extern HWND hCheckUpdate;
extern HWND hDownloadUpdate;
extern HWND hAbort;
extern HWND hExit;
extern HWND hProgress;
extern HWND hTextInfo1;
extern HWND hTextInfo2;
extern HWND hAutoInstall;
extern HWND hAutoUpdate;
extern HWND hDiffDirs;

extern bool autoUpdateOn;
extern bool autoInstall;
extern bool betaTester;
extern bool diffrDirs;
extern bool stopDownloading;
extern bool silentMode;
extern bool stopInstalling;
extern char * downloadFilenameUpdate;

extern int lMajor, lMinor, lBuild;
extern int sMajor, sMinor, sBuild;


int programStatus = PROGRAM_STATUS_BEGIN;

LRESULT CALLBACK NOXUpdateWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		{
		}	// end WM_CREATE
		break;
	case WM_SIZE:
		{
		}
		break;
	case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case 201:	// Check Update / Download Update / Install Update
				{
					if (wmEvent != BN_CLICKED)
						break;

					switch (programStatus)
					{
						case PROGRAM_STATUS_BEGIN:
							break;
						case PROGRAM_STATUS_LOCAL_VERSION_OK:
							{
								stopDownloading = false;
								bool lOK = getNoxVersion(lMajor, lMinor, lBuild);
								programStatus = lOK ? PROGRAM_STATUS_LOCAL_VERSION_OK : PROGRAM_STATUS_LOCAL_VERSION_ERROR;

								SetWindowText(hTextInfo2, "Checking server version. Please wait...");
								
								bool threadOK = downloadNOXserverVersion(betaTester);
								if (!threadOK)
								{
									programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
									unlockControlsAfterDownloadVersion(0);
								}
							}
							break;
						case PROGRAM_STATUS_LOCAL_VERSION_ERROR:
							break;
						case PROGRAM_STATUS_CHECKING_SERVER_VERSION:
							break;
						case PROGRAM_STATUS_SERVER_VERSION_OK_HIGHER:
						case PROGRAM_STATUS_SERVER_VERSION_OK_LOWER:
							{
								stopDownloading = false;
								bool dOK = downloadUpdate(sMajor, sMinor, sBuild, betaTester);
								if (!dOK)
								{
									programStatus = PROGRAM_STATUS_DOWN_UPDATE_ERROR;
									unlockControlsAfterDownloadUpdate(0);
								}
							}
							break;
						case PROGRAM_STATUS_SERVER_VERSION_ERROR:
							break;
						case PROGRAM_STATUS_DOWNLOADING_UPDATE:
							break;
						case PROGRAM_STATUS_DOWN_UPDATE_OK:
							{
								bool iOK = runInstallThread(downloadFilenameUpdate);
								if (!iOK)
								{
									unlockControlsAfterInstallUpdate(0);
								}
							}
							break;
						case PROGRAM_STATUS_DOWN_UPDATE_ERROR:
							break;
						case PROGRAM_STATUS_INSTALLING:
							break;
						case PROGRAM_STATUS_WAIT_FOR_DIR:
							break;
						case PROGRAM_STATUS_UNPACKING:
							break;
						case PROGRAM_STATUS_COPYING:
							break;
						case PROGRAM_STATUS_DELETING_TEMP:
							break;
						case PROGRAM_STATUS_RUN_PLUGINS:
							break;
						case PROGRAM_STATUS_INSTALL_ERROR:
							break;
						case PROGRAM_STATUS_INSTALL_ABORTED:
							break;
						case PROGRAM_STATUS_INSTALL_COMPLETE:
							{
								programStatus = PROGRAM_STATUS_RUN_PLUGINS;
								runPluginInstaller();
								unlockControlsAfterRunPluginsInstaller(1);
							}
							break;
					}


				}
				break;
				case 202:	// Download update
				{
					if (wmEvent != BN_CLICKED)
						break;

					stopDownloading = false;
					bool dOK = downloadUpdate(sMajor, sMinor, sBuild, betaTester);
					if (!dOK)
					{
						programStatus = PROGRAM_STATUS_DOWN_UPDATE_ERROR;
						unlockControlsAfterDownloadUpdate(0);
					}
				}
				break;
				case 203:	// Abort/Exit
				{
					if (wmEvent != BN_CLICKED)
						break;

					switch(programStatus)
					{
						case PROGRAM_STATUS_BEGIN:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_LOCAL_VERSION_OK:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_LOCAL_VERSION_ERROR:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_CHECKING_SERVER_VERSION:
							stopDownloading = true;
							break;
						case PROGRAM_STATUS_SERVER_VERSION_OK_HIGHER:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_SERVER_VERSION_OK_LOWER:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_SERVER_VERSION_ERROR:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_DOWNLOADING_UPDATE:
							stopDownloading = true;
							break;
						case PROGRAM_STATUS_DOWN_UPDATE_OK:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_DOWN_UPDATE_ERROR:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_INSTALLING:
							stopInstalling = true;
							break;
						case PROGRAM_STATUS_WAIT_FOR_DIR:
							break;
						case PROGRAM_STATUS_UNPACKING:
							stopInstalling = true;
							break;
						case PROGRAM_STATUS_COPYING:
							stopInstalling = true;
							break;
						case PROGRAM_STATUS_DELETING_TEMP:
							stopInstalling = true;
							break;
						case PROGRAM_STATUS_RUN_PLUGINS:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_INSTALL_ERROR:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_INSTALL_ABORTED:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
						case PROGRAM_STATUS_INSTALL_COMPLETE:
							SendMessage(hWnd, WM_CLOSE, 0, 0);
							break;
					}
				}
				break;
				case 206:	// Auto update
				{
					if (wmEvent != BN_CLICKED)
						break;
					bool cbON = (BST_CHECKED == SendMessage(hAutoUpdate, BM_GETCHECK, 0, 0) );
					setRegistryAutoUpdate(cbON);
					autoUpdateOn = cbON;
				}
				break;
				case 207:	// Auto install
				{
					if (wmEvent != BN_CLICKED)
						break;
					bool cbON = (BST_CHECKED == SendMessage(hAutoInstall, BM_GETCHECK, 0, 0) );
					setRegistryAutoInstall(cbON);
					autoInstall = cbON;
				}
				break;
				case 208:	// Different dirs
				{
					if (wmEvent != BN_CLICKED)
						break;
					if (wmEvent != BN_CLICKED)
						break;
					bool cbON = (BST_CHECKED == SendMessage(hDiffDirs, BM_GETCHECK, 0, 0) );
					if (betaTester)
					{
						setRegistryDifferentDirs(cbON);
						diffrDirs = cbON;
					}
				}
				break;
			}
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	return 0;
}


