#include "max2013predef.h"
#include "NOXCamera.h"
#include <macrorec.h>
#include <decomp.h>
//#include <target.h>
#include "NOXExporter.h"

HWND NOXCamera::hParams = 0;
static NOXCameraClassDesc NOXCameraDesc;
ClassDesc2* GetNOXCameraDesc() { return &NOXCameraDesc; }
static NOXCameraTargetClassDesc NOXCameraDescTarget;
ClassDesc2* GetNOXCameraTargetDesc() { return &NOXCameraDescTarget; }


//================================================================================================================================

NOXCameraCreateMouseCB mccmcb;

CreateMouseCallBack* NOXCamera::GetCreateMouseCallBack()
{
	mccmcb.cam = this;
	return &mccmcb;
}


void NOXCamera::RenderApertureChanged(TimeValue t)
{
}


RefResult NOXCamera::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message)
{
	return REF_SUCCEED;
}


int NOXCameraCreateMouseCB::proc(ViewExp *vpt, int msg, int point, int flags, IPoint2 m, Matrix3 &mat)
{
	if (msg == MOUSE_FREEMOVE)
	{
		vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
	}

	if (msg==MOUSE_POINT || msg==MOUSE_MOVE) 
	{
		if (point == 0)
		{
			ULONG handle;
			cam->NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
			INode * node = GetCOREInterface()->GetINodeByHandle( handle );
			if ( node ) 
			{
				Point3 color = GetUIColor( COLOR_CAMERA_OBJ );
				node->SetWireColor( RGB( color.x*255.0f, color.y*255.0f, color.z*255.0f ) );
			}
		}

		mat.SetTrans( vpt->SnapPoint(m,m,NULL,SNAP_IN_3D) );	

		if (point == 1 && msg==MOUSE_POINT)
			return 0;

		if (msg==MOUSE_POINT)
		{
		}

	}
	

	if (msg == MOUSE_ABORT)
		return CREATE_ABORT;

	return TRUE;
	return FALSE;
	return CREATE_CONTINUE;
	return CREATE_ABORT;
}


static void RemoveScaling(Matrix3 &tm) 
{
	AffineParts ap;
	decomp_affine(tm, &ap);
	tm.IdentityMatrix();
	tm.SetRotate(ap.q);
	tm.SetTrans(ap.t);
}

void GetMatrix(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm) 
{
	if (inode->GetObjTMAfterWSM(t).IsIdentity()) 
		tm = Inverse(inode->GetObjTMBeforeWSM(t)); 
	else
		tm = inode->GetObjectTM(t); 

	RemoveScaling(tm);
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(tm.GetTrans())/(float)360.0;
	if (scaleFactor!=(float)1.0)
		tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
}


int NOXCamera::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) 
{
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();

	UpdateTargDistance(t, inode);
	GetMatrix(t,inode,vpt,m);
	gw->setTransform(m);
	DWORD rlim = gw->getRndLimits();
	gw->setRndLimits(GW_WIREFRAME|GW_EDGES_ONLY|GW_BACKCULL);
	gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_OBJ) );	

	if (inode->Selected())
	{
		gw->setColor( LINE_COLOR, GetSelColor());
	}
	else if(!inode->IsFrozen() && !inode->Dependent())
	{
		Color color(inode->GetWireColor());
	}
	camMesh.render( gw, gw->getMaterial(), NULL, COMP_ALL);	

	if (GetConeState()  ||  inode->Selected())
	{
		m = inode->GetObjectTM(t);
		float d = (focusConnectedToTarget && Type()) ? tDist : focusLength;
		gw->setTransform(m);
		DrawCone(t, gw, d, COLOR_CAMERA_CLIP, 1,0);
	}
	
	if (Type())
	{
		m = inode->GetObjectTM(t);
		gw->setTransform(m);
		DrawTargetLine(t, gw, tDist, COLOR_CAMERA_CLIP);
	}

	gw->setRndLimits(rlim);
	return(0);
}


int NOXCamera::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) 
{
	HitRegion hitRegion;
	DWORD	savedLimits;
	int res = 0;
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();	
	MakeHitRegion(hitRegion,type,crossing,4,p);	
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	GetMatrix(t,inode,vpt,m);
	gw->setTransform(m);
	gw->clearHitCode();
	res = camMesh.select( gw, gw->getMaterial(), &hitRegion, flags & HIT_ABORTONHIT ); 
	if( !res )	
	{
		// this special case only works with point selection of targeted lights
		if((type != HITTYPE_POINT) || !inode->GetTarget())
			return 0;
		// don't let line be active if only looking at selected stuff and target isn't selected
		if((flags & HIT_SELONLY) && !inode->GetTarget()->Selected() )
			return 0;
		gw->clearHitCode();
		res = gw->checkHitCode();
		if(res != 0)
			inode->SetTargetNodePair(1);
	}
	gw->setRndLimits(savedLimits);
	return res;
}

Interval NOXCamera::ObjectValidity(TimeValue time) 
{
	Interval ivalid;
	ivalid.SetInfinite();
	return ivalid;	
}

void NOXCamera::GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel )
{
	box = camMesh.getBoundingBox(tm);	
}

void NOXCamera::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	Matrix3 m = inode->GetObjectTM(t);
	Point3 pt;
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(m.GetTrans())/(float)360.0;
	box = camMesh.getBoundingBox();
	box.Scale(scaleFactor);
	Point3 q[4];

	float d = (focusConnectedToTarget && Type()) ? tDist : focusLength;
	box += Point3(float(0),float(0),-d);
	GetConePoints(t,q, d);
	box.IncludePoints(q,4);
}

void NOXCamera::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
	{
	int i,nv;
	Matrix3 tm;
	Point3 pt;
	GetMatrix(t,inode,vpt,tm);
	nv = camMesh.getNumVerts();
	box.Init();
	for (i=0; i<nv; i++) 
		box += tm*camMesh.getVert(i);

	tm = inode->GetObjectTM(t);
	Point3 q[4];
	float d = (focusConnectedToTarget && Type()) ? tDist : focusLength;
	GetConePoints(t,q,d);
	box.IncludePoints(q,4,&tm);
}

#define MAXVP_DIST 1.0e8f
void NOXCamera::GetConePoints(TimeValue t, Point3* q, float dist) 
{
	if (dist>MAXVP_DIST)
		dist = MAXVP_DIST;
	float ta = (float)tan(0.5*(double)GetFOV(t));
	float w = dist * ta;
	float asp = GetCOREInterface()->GetRendImageAspect();
	asp = max(asp, 0.01f);
	float h = w / asp;
	q[0] = Point3( w, h,-dist);				
	q[1] = Point3(-w, h,-dist);				
	q[2] = Point3(-w,-h,-dist);				
	q[3] = Point3( w,-h,-dist);				
}

void NOXCamera::DrawTargetLine(TimeValue t, GraphicsWindow *gw, float dist, int colid) 
{
	Point3 u[2];
	//if (colid)	gw->setColor( LINE_COLOR, GetUIColor(colid));
	gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_CONE));
	u[0] = Point3(0.0f, 0.0f, 0.0f);
	u[1] = Point3(0.0f, 0.0f, -dist);
	gw->polyline( 2, u, NULL, NULL, FALSE, NULL );
}

void NOXCamera::DrawCone(TimeValue t, GraphicsWindow *gw, float dist, int colid, BOOL drawSides, BOOL drawDiags) 
{
	Point3 q[5], u[3];
	GetConePoints(t,q,dist);
	if (colid)	gw->setColor( LINE_COLOR, GetUIColor(colid));
	if (drawDiags) 
	{
		u[0] =  q[0];	u[1] =  q[2];	
		gw->polyline( 2, u, NULL, NULL, FALSE, NULL );	
		u[0] =  q[1];	u[1] =  q[3];	
		gw->polyline( 2, u, NULL, NULL, FALSE, NULL );	
	}
	gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_CONE));
	gw->polyline( 4, q, NULL, NULL, TRUE, NULL );	
	if (drawSides) 
	{
		gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_CONE));
		u[0] = Point3(0,0,0);
		for (int i=0; i<4; i++) 
		{
			u[1] =  q[i];	
			gw->polyline( 2, u, NULL, NULL, FALSE, NULL );	
		}
	}
	
}

static int GetTargetPoint(TimeValue t, INode *inode, Point3& p) 
{
	Matrix3 tmat;
	if (inode->GetTargetTM(t,tmat)) 
	{
		p = tmat.GetTrans();
		return 1;
	}
	else 
		return 0;
}

void NOXCamera::UpdateTargDistance(TimeValue t, INode* inode)
{
	Point3 pt,v[2];
	if (GetTargetPoint(t,inode,pt))
	{
		Matrix3 tm = inode->GetObjectTM(t);
		float den = Length(tm.GetRow(2));
		tDist = (den!=0)?Length(tm.GetTrans()-pt)/den : 0.0f;
		SetTDist(t, tDist);
		if (focusConnectedToTarget)
		{
			focusLength = tDist;
			trCam.setFocusDistance(tDist);
		}
	}
}


void NOXCamera::makeActiveOnlyMe(INode * rootNode)
{
	ValidateCameras vc;
	vc.deactivateAll(rootNode);
	trCam.setActive(true);
}