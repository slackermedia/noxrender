#ifndef __LINK_RENDERER_H
#define __LINK_RENDERER_H

#ifndef DONT_INCLUDE_EVER_RENDERER
	#ifdef PORTABLE_COMPILATION
		#include "noxinclude/raytracer.h"
		#include "noxinclude/log.h"
	#else
		#include "../../Everything DLL/Everything DLL/raytracer.h"
		#include "../../Everything DLL/Everything DLL/log.h"
	#endif
#endif

#define NOX_TEX_ROUGHNESS 1
#define NOX_TEX_REFL0 2
#define NOX_TEX_REFL90 3
#define NOX_TEX_WEIGHT 4
#define NOX_TEX_LIGHT 5
#define NOX_TEX_NORMAL 6
#define NOX_TEX_TRANSM 7
#define NOX_TEX_ANISO 8
#define NOX_TEX_ANISO_ANGLE 9
#define NOX_TEX_OPACITY 10
#define NOX_TEX_DISPLACEMENT 11

//---------------------------------------------------------------------------------------------------------------

class TrRaytracer
{
public:
	static bool createMainScene();
	static bool changeSceneToMain();
	static bool loadMatEditorScenes();
	static int getVersionMajor();
	static int getVersionMinor();
	static int getVersionBuild();
	static void checkNOXVersions();
	static void addLog(char * txt);

	static bool getUserColors(const int num, float &r, float &g, float &b);
	static bool setUserColors(const int num, const float &r, const float &g, const float &b);
};

//---------------------------------------------------------------------------------------------------------------

class TrMaterial
{
public:
	void * address;
	void * curLay;
	int llay;

	bool createSimpleMaterial();
	void deleteNOXmaterial();
	bool runEditor(HINSTANCE hInst, HWND hParent);
	bool runEditor2(HINSTANCE hInst, HWND hParent);
	void backFromEditor(bool accept);

	// for material in max
	void SetAmbient(float r, float g, float b);
	void SetDiffuse(float r, float g, float b);
	void SetSpecular(float r, float g, float b);
	void SetShininess(float v);
	bool GetAmbient(float &r, float &g, float &b);
    bool GetDiffuse(float &r, float &g, float &b);
	bool GetSpecular(float &r, float &g, float &b);
	float GetXParency();
	float GetShininess();
	float GetShinStr();

	// to get data from nox
	int   getLayersNumber();
	char* getName();
	bool  getSkyportal();
	bool  getMatteShadow();
	int	  getBlendLayerNumber();
	int   getLayerType(int lNum);
	int   getLayerContribution(int lNum);
	float getLayerRoughness(int lNum);
	bool  getLayerColor0 (int lNum, float &r, float &g, float &b, float &a);
	bool  getLayerColor90(int lNum, float &r, float &g, float &b, float &a);
	bool  getLayerConnect90(int lNum);
	float getLayerAniso(int lNum);
	float getLayerAnisoAngle(int lNum);
	int   getLayerBrdfType(int lNum);
	bool  getLayerTransmOn(int lNum);
	bool  getLayerTransmColor(int lNum, float &r, float &g, float &b, float &a);
	bool  getLayerFakeGlass(int lNum);
	bool  getLayerAbsOn(int lNum);
	bool  getLayerAbsColor(int lNum, float &r, float &g, float &b, float &a);
	float getLayerAbsDist(int lNum);
	bool  getLayerEmissionColor(int lNum, float &r, float &g, float &b, float &a);
	float getLayerEmissionPower(int lNum);
	int   getLayerEmissionTemperature(int lNum);
	int   getLayerEmissionUnit(int lNum);
	char* getLayerEmissionIes(int lNum); 
	float getLayerFresnelIOR(int lNum);
	float getLayerDispersionValue(int lNum);
	bool  getLayerDispersionOn(int lNum);
	bool  getLayerSSSOn(int lNum);
	float getLayerSSSDensity(int lNum);
	float getLayerSSSCollDirection(int lNum);
	float getOpacity();
	float getDisplacementDepth();
	int   getDisplacementSubdivs();
	int   getDisplacementNormalMode();
	float getDisplacementShift();
	bool  getDisplacementContinuity();
	bool  getDisplacementIgnoreScale();
	bool  getLayerUseTexRefl0(int lNum);
	char* getLayerTexRefl0(int lNum);
	bool  getLayerUseTexRefl90(int lNum);
	char* getLayerTexRefl90(int lNum);
	bool  getLayerUseTexTransm(int lNum);
	char* getLayerTexTransm(int lNum);
	bool  getLayerUseTexAniso(int lNum);
	char* getLayerTexAniso(int lNum);
	bool  getLayerUseTexAnisoAngle(int lNum);
	char* getLayerTexAnisoAngle(int lNum);
	bool  getLayerUseTexRough(int lNum);
	char* getLayerTexRough(int lNum);
	bool  getLayerUseTexWeight(int lNum);
	char* getLayerTexWeight(int lNum);
	bool  getLayerUseTexLight(int lNum);
	char* getLayerTexLight(int lNum);
	bool  getLayerUseTexNormal(int lNum);
	char* getLayerTexNormal(int lNum);
	bool  getUseTexOpacity();
	char* getTexOpacity();
	bool  getUseTexDisplacement();
	char* getTexDisplacement();
	bool getLayerTexPostFilterRefl0(int lNum);
	bool getLayerTexPostFilterRefl90(int lNum);
	bool getLayerTexPostFilterRough(int lNum);
	bool getLayerTexPostFilterWeight(int lNum);
	bool getLayerTexPostFilterLight(int lNum);
	bool getLayerTexPostFilterTransm(int lNum);
	bool getLayerTexPostFilterNormal(int lNum);
	bool getLayerTexPostFilterAniso(int lNum);
	bool getLayerTexPostFilterAnisoAngle(int lNum);
	bool getTexPostFilterOpacity();
	bool getTexPostFilterDisplacement();
	bool getLayerTexPrecalculateRefl0(int lNum);
	bool getLayerTexPrecalculateRefl90(int lNum);
	bool getLayerTexPrecalculateRough(int lNum);
	bool getLayerTexPrecalculateWeight(int lNum);
	bool getLayerTexPrecalculateLight(int lNum);
	bool getLayerTexPrecalculateTransm(int lNum);
	bool getLayerTexPrecalculateNormal(int lNum);
	bool getLayerTexPrecalculateAniso(int lNum);
	bool getLayerTexPrecalculateAnisoAngle(int lNum);
	bool getTexPrecalculateOpacity();
	float getLayerTexPostBrightnessRefl0(int lNum);
	float getLayerTexPostBrightnessRefl90(int lNum);
	float getLayerTexPostBrightnessRough(int lNum);
	float getLayerTexPostBrightnessWeight(int lNum);
	float getLayerTexPostBrightnessLight(int lNum);
	float getLayerTexPostBrightnessTransm(int lNum);
	float getLayerTexPostBrightnessNormal(int lNum);
	float getLayerTexPostBrightnessAniso(int lNum);
	float getLayerTexPostBrightnessAnisoAngle(int lNum);
	float getTexPostBrightnessOpacity();
	float getTexPostBrightnessDisplacement();
	float getLayerTexPostContrastRefl0(int lNum);
	float getLayerTexPostContrastRefl90(int lNum);
	float getLayerTexPostContrastRough(int lNum);
	float getLayerTexPostContrastWeight(int lNum);
	float getLayerTexPostContrastLight(int lNum);
	float getLayerTexPostContrastTransm(int lNum);
	float getLayerTexPostContrastNormal(int lNum);
	float getLayerTexPostContrastAniso(int lNum);
	float getLayerTexPostContrastAnisoAngle(int lNum);
	float getTexPostContrastOpacity();
	float getTexPostContrastDisplacement();
	float getLayerTexPostSaturationRefl0(int lNum);
	float getLayerTexPostSaturationRefl90(int lNum);
	float getLayerTexPostSaturationRough(int lNum);
	float getLayerTexPostSaturationWeight(int lNum);
	float getLayerTexPostSaturationLight(int lNum);
	float getLayerTexPostSaturationTransm(int lNum);
	float getLayerTexPostSaturationNormal(int lNum);
	float getLayerTexPostSaturationAniso(int lNum);
	float getLayerTexPostSaturationAnisoAngle(int lNum);
	float getTexPostSaturationOpacity();
	float getTexPostSaturationDisplacement();
	float getLayerTexPostHueRefl0(int lNum);
	float getLayerTexPostHueRefl90(int lNum);
	float getLayerTexPostHueRough(int lNum);
	float getLayerTexPostHueWeight(int lNum);
	float getLayerTexPostHueLight(int lNum);
	float getLayerTexPostHueTransm(int lNum);
	float getLayerTexPostHueNormal(int lNum);
	float getLayerTexPostHueAniso(int lNum);
	float getLayerTexPostHueAnisoAngle(int lNum);
	float getTexPostHueOpacity();
	float getTexPostHueDisplacement();
	float getLayerTexPostRedRefl0(int lNum);
	float getLayerTexPostRedRefl90(int lNum);
	float getLayerTexPostRedRough(int lNum);
	float getLayerTexPostRedWeight(int lNum);
	float getLayerTexPostRedLight(int lNum);
	float getLayerTexPostRedTransm(int lNum);
	float getLayerTexPostRedNormal(int lNum);
	float getLayerTexPostRedAniso(int lNum);
	float getLayerTexPostRedAnisoAngle(int lNum);
	float getTexPostRedOpacity();
	float getTexPostRedDisplacement();
	float getLayerTexPostGreenRefl0(int lNum);
	float getLayerTexPostGreenRefl90(int lNum);
	float getLayerTexPostGreenRough(int lNum);
	float getLayerTexPostGreenWeight(int lNum);
	float getLayerTexPostGreenLight(int lNum);
	float getLayerTexPostGreenTransm(int lNum);
	float getLayerTexPostGreenNormal(int lNum);
	float getLayerTexPostGreenAniso(int lNum);
	float getLayerTexPostGreenAnisoAngle(int lNum);
	float getTexPostGreenOpacity();
	float getTexPostGreenDisplacement();
	float getLayerTexPostBlueRefl0(int lNum);
	float getLayerTexPostBlueRefl90(int lNum);
	float getLayerTexPostBlueRough(int lNum);
	float getLayerTexPostBlueWeight(int lNum);
	float getLayerTexPostBlueLight(int lNum);
	float getLayerTexPostBlueTransm(int lNum);
	float getLayerTexPostBlueNormal(int lNum);
	float getLayerTexPostBlueAniso(int lNum);
	float getLayerTexPostBlueAnisoAngle(int lNum);
	float getTexPostBlueOpacity();
	float getTexPostBlueDisplacement();
	float getLayerTexPostGammaRefl0(int lNum);
	float getLayerTexPostGammaRefl90(int lNum);
	float getLayerTexPostGammaRough(int lNum);
	float getLayerTexPostGammaWeight(int lNum);
	float getLayerTexPostGammaLight(int lNum);
	float getLayerTexPostGammaTransm(int lNum);
	float getLayerTexPostGammaNormal(int lNum);
	float getLayerTexPostGammaAniso(int lNum);
	float getLayerTexPostGammaAnisoAngle(int lNum);
	float getTexPostGammaOpacity();
	float getTexPostGammaDisplacement();
	bool getLayerTexPostInvertRefl0(int lNum);
	bool getLayerTexPostInvertRefl90(int lNum);
	bool getLayerTexPostInvertRough(int lNum);
	bool getLayerTexPostInvertWeight(int lNum);
	bool getLayerTexPostInvertLight(int lNum);
	bool getLayerTexPostInvertTransm(int lNum);
	bool getLayerTexPostInvertNormal(int lNum);
	bool getLayerTexPostInvertAniso(int lNum);
	bool getLayerTexPostInvertAnisoAngle(int lNum);
	bool getTexPostInvertOpacity();
	bool getTexPostInvertDisplacement();
	float getLayerTexPostNormalPower(int lNum);
	bool getLayerTexPostNormalInvertX(int lNum);
	bool getLayerTexPostNormalInvertY(int lNum);


	bool  getPreviewExists();
	int   getPreviewWidth();
	int   getPreviewHeight();
	void* getPreviewBuffer();	// free result manually
	int   getPreviewBufSize();

	// set data from file to nox
	bool addLayer();
	bool setCurLayer(int nID);
	bool setName(char * newname);
	bool setSkyportal(bool b);
	bool setMatteShadow(bool b);
	bool setBlendLayerNumber(int num);
	bool setLayerType(int newtype);
	bool setLayerContribution(int newcontr);
	bool setLayerRoughness(float newrough);
	bool setLayerColor0 (float r, float g, float b, float a);
	bool setLayerColor90(float r, float g, float b, float a);
	bool setLayerConnect90(int newconn);
	bool setLayerAniso(float newaniso);
	bool setLayerAnisoAngle(float newanisoangle);
	bool setLayerBrdfType(int newbrdf);
	bool setLayerTransmOn(int newtransmon);
	bool setLayerTransmColor(float r, float g, float b, float a);
	bool setLayerFakeGlass(int newfakeglass);
	bool setLayerAbsOn(int newabson);
	bool setLayerAbsColor(float r, float g, float b, float a);
	bool setLayerAbsDist(float newdist);
	bool setLayerEmissionColor(float r, float g, float b, float a);
	bool setLayerEmissionPower(float newpower);
	bool setLayerEmissionTemperature(int newtemp);
	bool setLayerEmissionUnit(int newunit);
	bool setLayerEmissionIes(char * fname);
	bool setLayerFresnelIOR(float newIOR);
	bool setLayerDispersionValue(float newDisp);
	bool setLayerDispersionOn(int newdispOn);
	bool setLayerSSSOn(int newSSSOn);
	bool setLayerSSSDensity(float newSSSDens);
	bool setLayerSSSCollDirection(float newSSSCDir);
	bool setOpacity(float newopacity);
	bool setDisplacementDepth(float newdepth);
	bool setDisplacementSubdivs(int newsubdivs);
	bool setDisplacementNormalMode(int newnormalmode);
	bool setDisplacementShift(float newshift);
	bool setDisplacementContinuity(bool on);
	bool setDisplacementIgnoreScale(bool on);
	bool setNewPreview(int w1, int h1, unsigned char * buf);
	bool setLayerUseTexRefl0(bool b);
	bool setLayerTexRefl0(char * newname);
	bool setLayerUseTexRefl90(bool b);
	bool setLayerTexRefl90(char * newname);
	bool setLayerUseTexTransm(bool b);
	bool setLayerTexTransm(char * newname);
	bool setLayerUseTexAniso(bool b);
	bool setLayerTexAniso(char * newname);
	bool setLayerUseTexAnisoAngle(bool b);
	bool setLayerTexAnisoAngle(char * newname);
	bool setLayerUseTexRough(bool b);
	bool setLayerTexRough(char * newname);
	bool setLayerUseTexWeight(bool b);
	bool setLayerTexWeight(char * newname);
	bool setLayerUseTexLight(bool b);
	bool setLayerTexLight(char * newname);
	bool setLayerUseTexNormal(bool b);
	bool setLayerTexNormal(char * newname);
	bool setUseTexOpacity(bool b);
	bool setTexOpacity(char * newname);
	bool setUseTexDisplacement(bool b);
	bool setTexDisplacement(char * newname);

	bool setLayerTexFilterRefl0(bool newON);
	bool setLayerTexFilterRefl90(bool newON);
	bool setLayerTexFilterRough(bool newON);
	bool setLayerTexFilterWeight(bool newON);
	bool setLayerTexFilterLight(bool newON);
	bool setLayerTexFilterTransm(bool newON);
	bool setLayerTexFilterAniso(bool newON);
	bool setLayerTexFilterAnisoAngle(bool newON);
	bool setTexFilterOpacity(bool newON);
	bool setTexFilterDisplacement(bool newON);
	bool setLayerTexPrecalculateRefl0(bool newON);
	bool setLayerTexPrecalculateRefl90(bool newON);
	bool setLayerTexPrecalculateRough(bool newON);
	bool setLayerTexPrecalculateWeight(bool newON);
	bool setLayerTexPrecalculateLight(bool newON);
	bool setLayerTexPrecalculateTransm(bool newON);
	bool setTexPrecalculateOpacity(bool newON);
	bool setTexPrecalculateDisplacement(bool newON);
	bool setLayerTexPostBrightnessRefl0(float newBri);
	bool setLayerTexPostBrightnessRefl90(float newBri);
	bool setLayerTexPostBrightnessRough(float newBri);
	bool setLayerTexPostBrightnessWeight(float newBri);
	bool setLayerTexPostBrightnessLight(float newBri);
	bool setLayerTexPostBrightnessTransm(float newBri);
	bool setLayerTexPostBrightnessNormal(float newBri);
	bool setLayerTexPostBrightnessAniso(float newBri);
	bool setLayerTexPostBrightnessAnisoAngle(float newBri);
	bool setTexPostBrightnessOpacity(float newBri);
	bool setTexPostBrightnessDisplacement(float newBri);
	bool setLayerTexPostContrastRefl0(float newCon);
	bool setLayerTexPostContrastRefl90(float newCon);
	bool setLayerTexPostContrastRough(float newCon);
	bool setLayerTexPostContrastWeight(float newCon);
	bool setLayerTexPostContrastLight(float newCon);
	bool setLayerTexPostContrastTransm(float newCon);
	bool setLayerTexPostContrastNormal(float newCon);
	bool setLayerTexPostContrastAniso(float newCon);
	bool setLayerTexPostContrastAnisoAngle(float newCon);
	bool setTexPostContrastOpacity(float newCon);
	bool setTexPostContrastDisplacement(float newCon);
	bool setLayerTexPostSaturationRefl0(float newSat);
	bool setLayerTexPostSaturationRefl90(float newSat);
	bool setLayerTexPostSaturationRough(float newSat);
	bool setLayerTexPostSaturationWeight(float newSat);
	bool setLayerTexPostSaturationLight(float newSat);
	bool setLayerTexPostSaturationTransm(float newSat);
	bool setLayerTexPostSaturationNormal(float newSat);
	bool setLayerTexPostSaturationAniso(float newSat);
	bool setLayerTexPostSaturationAnisoAngle(float newSat);
	bool setTexPostSaturationOpacity(float newSat);
	bool setTexPostSaturationDisplacement(float newSat);
	bool setLayerTexPostHueRefl0(float newHue);
	bool setLayerTexPostHueRefl90(float newHue);
	bool setLayerTexPostHueRough(float newHue);
	bool setLayerTexPostHueWeight(float newHue);
	bool setLayerTexPostHueLight(float newHue);
	bool setLayerTexPostHueTransm(float newHue);
	bool setLayerTexPostHueNormal(float newHue);
	bool setLayerTexPostHueAniso(float newHue);
	bool setLayerTexPostHueAnisoAngle(float newHue);
	bool setTexPostHueOpacity(float newHue);
	bool setTexPostHueDisplacement(float newHue);
	bool setLayerTexPostRedRefl0(float newRed);
	bool setLayerTexPostRedRefl90(float newRed);
	bool setLayerTexPostRedRough(float newRed);
	bool setLayerTexPostRedWeight(float newRed);
	bool setLayerTexPostRedLight(float newRed);
	bool setLayerTexPostRedTransm(float newRed);
	bool setLayerTexPostRedNormal(float newRed);
	bool setLayerTexPostRedAniso(float newRed);
	bool setLayerTexPostRedAnisoAngle(float newRed);
	bool setTexPostRedOpacity(float newRed);
	bool setTexPostRedDisplacement(float newRed);
	bool setLayerTexPostGreenRefl0(float newGreen);
	bool setLayerTexPostGreenRefl90(float newGreen);
	bool setLayerTexPostGreenRough(float newGreen);
	bool setLayerTexPostGreenWeight(float newGreen);
	bool setLayerTexPostGreenLight(float newGreen);
	bool setLayerTexPostGreenTransm(float newGreen);
	bool setLayerTexPostGreenNormal(float newGreen);
	bool setLayerTexPostGreenAniso(float newGreen);
	bool setLayerTexPostGreenAnisoAngle(float newGreen);
	bool setTexPostGreenOpacity(float newGreen);
	bool setTexPostGreenDisplacement(float newGreen);
	bool setLayerTexPostBlueRefl0(float newBlue);
	bool setLayerTexPostBlueRefl90(float newBlue);
	bool setLayerTexPostBlueRough(float newBlue);
	bool setLayerTexPostBlueWeight(float newBlue);
	bool setLayerTexPostBlueLight(float newBlue);
	bool setLayerTexPostBlueTransm(float newBlue);
	bool setLayerTexPostBlueNormal(float newBlue);
	bool setLayerTexPostBlueAniso(float newBlue);
	bool setLayerTexPostBlueAnisoAngle(float newBlue);
	bool setTexPostBlueOpacity(float newBlue);
	bool setTexPostBlueDisplacement(float newBlue);
	bool setLayerTexPostGammaRefl0(float newGamma);
	bool setLayerTexPostGammaRefl90(float newGamma);
	bool setLayerTexPostGammaRough(float newGamma);
	bool setLayerTexPostGammaWeight(float newGamma);
	bool setLayerTexPostGammaLight(float newGamma);
	bool setLayerTexPostGammaTransm(float newGamma);
	bool setLayerTexPostGammaNormal(float newGamma);
	bool setLayerTexPostGammaAniso(float newGamma);
	bool setLayerTexPostGammaAnisoAngle(float newGamma);
	bool setTexPostGammaOpacity(float newGamma);
	bool setTexPostGammaDisplacement(float newGamma);
	bool setLayerTexPostInvertRefl0(bool newON);
	bool setLayerTexPostInvertRefl90(bool newON);
	bool setLayerTexPostInvertRough(bool newON);
	bool setLayerTexPostInvertWeight(bool newON);
	bool setLayerTexPostInvertLight(bool newON);
	bool setLayerTexPostInvertTransm(bool newON);
	bool setLayerTexPostInvertNormal(bool newON);
	bool setLayerTexPostInvertAniso(bool newON);
	bool setLayerTexPostInvertAnisoAngle(bool newON);
	bool setTexPostInvertOpacity(bool newON);
	bool setTexPostInvertDisplacement(bool newON);
	bool setLayerTexPostNormalPower(float newpower);
	bool setLayerTexPostNormalInvertX(bool newinv);
	bool setLayerTexPostNormalInvertY(bool newinv);





	float * createPreview(int w, int h);
	void * cloneAddress();

	bool isTexLoaded(int which);

	void setBlendLayerName(int num, char * newname);
	char * getBlendLayerName(int num);

	void runBlendLayersDialog(HWND hWnd);
	void runNOXTestDialog(HWND hWnd);
	static void * blendsettings;
	static void createBlendSettings();

	TrMaterial();
	~TrMaterial();
};

//----------------------------------------------------------------------------------------------------------------------

void notifyNOXCameraCallback(float focal, float focus);

class TrCamera
{
public:
	void * address;

	bool createSimpleCamera();
	bool updateNOXfromMAX();
	void runEditor(HINSTANCE hInst, HWND hParent);

	char* getName();

	// GET stuff
	bool getIsOrtho();
	float getFOV();
	float getClipDistHither();
	float getClipDistYon();
	float getEnvRangeNear();
	float getEnvRangeFar();
	int getManualClip();
	int getHorzLineState();
	int getFOVType();
	int getType();

	float getISO();
	float getAperture();
	float getShutter();
	bool  getAutoExpOn();
	int   getAutoExpType();
	bool  getAutoFocusOn();
	float getFocusDistance();
	bool  getIsActive();
	int   getBladesNumber();
	float getBladesAngle();
	bool  getBladesRound();
	float getBladesRadius();
	bool  getDofON();
	int   getBokehRingBalance();
	int   getBokehRingSize();
	int   getBokehFlatten();
	int   getVignette();

	// SET stuff
	void setHorzLineState(int s);
	void setFOVType(int fovtype);
	void setType(int type);
	void setOrtho(bool ortho);
	void setFOV(float f); 
	void setManualClip(int onOff);
	void setClipDistHither(float val);
	void setClipDistYon(float val);
	void setEnvRangeNear(float f);
	void setEnvRangeFar(float f);

	void setISO(float f);
	void setAperture(float f);
	void setShutter(float f);
	void setAutoExpOn(bool b);
	void setAutoExpType(int i);
	void setAutoFocusOn(bool b);
	void setFocusDistance(float f);
	void setActive(bool b);
	void setBladesNumber(int i);
	void setBladesAngle(float f);
	void setBladesRound(bool b);
	void setBladesRadius(float f);
	void setDofON(bool b);
	void setBokehRingBalance(int i);
	void setBokehRingSize(int i);
	void setBokehFlatten(int i);
	void setVignette(int i);

	// GET/SET stored values
	float fov;
	float clipDistHither;
	float clipDistYon;
	float envRangeNear;
	float envRangeFar;
	int manualClip;
	int horLine;
	int fovType;
	int camType;
	int active;

	bool copySettingsFrom(TrCamera * other);

	TrCamera();
	~TrCamera();
};

//---------------------------------------------------------------------------------------------------------

void notifyNOXSunSkyCallback(int month, int day, int hour, int minute, int gmt, float longitude, float latitude, float env_azimuth);

class TrSunSky
{
public:
	bool sunskyOn;
	bool targetHidden;
	bool envMapHidden;
	void * addressSunSky;
	void * addressEnv;
	bool createSunSky();
	bool createEnv();
	bool updateNOXfromMAX();

	float getLongitude();
	float getLatitude();
	float getTurbidity();
	float getAerosol();
	float getSunSize();
	int getMonth();
	int getDay();
	int getHour();
	int getMinute();
	int getGMT();
	bool getSunSkyON();
	bool getTargetHidden();
	bool getEnvHidden();
	bool getSunON();
	bool getSunOtherLayerON();
	int getModel();
	float getAlbedo();

	bool getEnvEnabled();
	float getEnvPower();
	int getEnvBlendLayer();
	float getEnvAzimuth();
	char * getEnvTexFilename();	// NULL on nothing
	float getEnvTexPostBrightness();
	float getEnvTexPostContrast();
	float getEnvTexPostSaturation();
	float getEnvTexPostHue();
	float getEnvTexPostRed();
	float getEnvTexPostGreen();
	float getEnvTexPostBlue();
	float getEnvTexPostGamma();
	bool  getEnvTexPostInvert();
	bool  getEnvTexPostInterpolateProbe();


	void setLongitude(float f);
	void setLatitude(float f);
	void setTurbidity(float f);
	void setAerosol(float f);
	void setMonth(int i);
	void setDay(int i);
	void setHour(int i);
	void setMinute(int i);
	void setGMT(int i);
	void setSunSkyON(bool b);
	void setTargetHidden(bool b);
	void getAziAltFromNOX(float &azi,  float &alt);
	void setSunON(bool b);
	void setSunOtherLayerON(bool b);
	void setSunSize(float f);
	void setModel(int i);
	void setAlbedo(float f);

	void setEnvEnabled(bool b);
	void setEnvPower(float f);
	void setEnvBlendLayer(int i);
	void setEnvAzimuth(float f);
	void setEnvTexFilename(char * cp);	// NULL for nothing
	void setEnvTexPostBrightness(float f);
	void setEnvTexPostContrast(float f);
	void setEnvTexPostSaturation(float f);
	void setEnvTexPostHue(float f);
	void setEnvTexPostRed(float f);
	void setEnvTexPostGreen(float f);
	void setEnvTexPostBlue(float f);
	void setEnvTexPostGamma(float f);
	void setEnvTexPostInvert(bool b);
	void setEnvTexPostInterpolateProbe(bool b);

	void setPosFromDir(float alt, float azi);
	bool prepareForExport();

	void runDialog(HINSTANCE hInst, HWND hParent);
	
	bool correctDirection(float &alt, float &azi);

	bool getEnvTex(BITMAPINFO ** bmi);

	TrSunSky();
	TrSunSky(TrSunSky &src);
	~TrSunSky();
};

//---------------------------------------------------------------------------------------------------------

#endif
