#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "raytracer.h"
#include "log.h"

int BBox::soFarIn = 0;

BBox::BBox(const Point3d &Bmin, const Point3d &Bmax, const int &num, const int &divRecLeft)
{
	triStart = soFarIn;
	triNum = num;
	bmin.x = Bmin.x;
	bmin.y = Bmin.y;
	bmin.z = Bmin.z;
	bmin.w = 0.0f;
	bmax.x = Bmax.x;
	bmax.y = Bmax.y;
	bmax.z = Bmax.z;
	bmax.w = 0.0f;

	soFarIn += num;
	isLeaf = true;
	divLeft = divRecLeft;
	tris4 = NULL;
	num4 = 0;
	last4 = 0;

	children = NULL;
	vchildren = 0;
}

BBox::BBox() 
{
	triStart = triNum = 0;
	isLeaf = true;
	divLeft = 0;
	tris4 = NULL;
	num4 = 0; 
	last4 = 0;
	bmin.x = bmin.y = bmin.z = bmin.w = 0.0f;
	bmax.x = bmax.y = bmax.z = bmax.w = 0.0f;

	children = NULL;
	vchildren = 0;
}

BBox::~BBox() 
{
	if (children)
		_aligned_free(children);	// probably memory leak 
	children = NULL;
	if (tris4)
		_aligned_free(tris4);
	tris4 = NULL;

	tris.destroyArray();
	tris.freeList();
}


bool BBox::intersection(const Point3d &origin, const Vector3d &direction)
{
	float tnear = -BIGFLOAT;
	float tfar = BIGFLOAT;
	float tempnear, tempfar, temp, adir;
	int i;

	for (i=0; i<3; i++)
	{	// for each dimension
		adir = fabs(direction.V[i]);
		if (adir < SFLOAT)
		{	// parallel direction
			if ((origin.V[i] < bmin.V[i]) || (origin.V[i] > bmax.V[i]))
			{
				return false;
			}
		}
		else
		{
			tempnear = (bmin.V[i] - origin.V[i])/direction.V[i];
			tempfar  = (bmax.V[i] - origin.V[i])/direction.V[i];
			if (tempnear > tempfar)
			{
				temp = tempnear;
				tempnear = tempfar;
				tempfar = temp;
			}
			if (tempnear > tnear)
				tnear = tempnear;
			if (tempfar  < tfar)
				tfar = tempfar;
		}
	}

	if (tnear > tfar)
		return false;

	if (tfar < SFLOAT)
		return false;

	return true;
}

void BBox::releaseNonLeaf()
{
	tris.freeList();
	for (int i=0; i<8; i++)
	{
		if (vchildren & (1<<i))
			children[i].releaseNonLeaf();
	}
}

bool BBox::divideVolume()
{
	isLeaf = false;

	int i;
	children = (BBox*)_aligned_malloc(sizeof(BBox)*8, 16);
	for (i=0; i<8; i++)
	{
		children[i] = BBox();
	}

	Point3d center;
	center.x = (bmin.x + bmax.x)*0.5f;
	center.y = (bmin.y + bmax.y)*0.5f;
	center.z = (bmin.z + bmax.z)*0.5f;


	children[0].bmin.x = bmin.x;
	children[0].bmin.y = bmin.y;
	children[0].bmin.z = bmin.z;
	children[0].bmax.x = center.x;
	children[0].bmax.y = center.y;
	children[0].bmax.z = center.z;

	children[1].bmin.x = bmin.x;
	children[1].bmin.y = bmin.y;
	children[1].bmin.z = center.z;
	children[1].bmax.x = center.x;
	children[1].bmax.y = center.y;
	children[1].bmax.z = bmax.z;

	children[2].bmin.x = bmin.x;
	children[2].bmin.y = center.y;
	children[2].bmin.z = bmin.z;
	children[2].bmax.x = center.x;
	children[2].bmax.y = bmax.y;
	children[2].bmax.z = center.z;

	children[3].bmin.x = bmin.x;
	children[3].bmin.y = center.y;
	children[3].bmin.z = center.z;
	children[3].bmax.x = center.x;
	children[3].bmax.y = bmax.y;
	children[3].bmax.z = bmax.z;

	children[4].bmin.x = center.x;
	children[4].bmin.y = bmin.y;
	children[4].bmin.z = bmin.z;
	children[4].bmax.x = bmax.x;
	children[4].bmax.y = center.y;
	children[4].bmax.z = center.z;

	children[5].bmin.x = center.x;
	children[5].bmin.y = bmin.y;
	children[5].bmin.z = center.z;
	children[5].bmax.x = bmax.x;
	children[5].bmax.y = center.y;
	children[5].bmax.z = bmax.z;

	children[6].bmin.x = center.x;
	children[6].bmin.y = center.y;
	children[6].bmin.z = bmin.z;
	children[6].bmax.x = bmax.x;
	children[6].bmax.y = bmax.y;
	children[6].bmax.z = center.z;

	children[7].bmin.x = center.x;
	children[7].bmin.y = center.y;
	children[7].bmin.z = center.z;
	children[7].bmax.x = bmax.x;
	children[7].bmax.y = bmax.y;
	children[7].bmax.z = bmax.z;


	int maxTris = Raytracer::getInstance()->curScenePtr->sscene.maxTrisPerBox;
	for (int j=0; j<8; j++)
	{
		children[j].evalCenter();
		for (i=0; i<tris.objCount; i++)
		{
			//children[j].tryToAdd(tris.objArray[i]);
			if (children[j].tryToAdd(tris.objArray[i]))
				vchildren |= (1<<j);
		}
		children[j].tris.createArray();
		children[j].divLeft = divLeft-1;
		if (divLeft>1)
			if (children[j].tris.objCount > maxTris)
			{
				children[j].divideVolume();
				if (Raytracer::getInstance()->curScenePtr->sscene.maxBoxDivs-divLeft < 1)
					Raytracer::getInstance()->curScenePtr->notifyProgress("Creating octree", (j+1)*12.5f);
			}
	}

	return true;
}

void BBox::evalCenter()
{
}

bool BBox::tryToAdd(int tri)
{
	bool reallyAdd = true;
	Triangle * tr;
	tr = &(Raytracer::getInstance()->curScenePtr->triangles[tri]);

	if ((tr->V1.x > bmax.x) && (tr->V2.x > bmax.x) && (tr->V3.x > bmax.x))
		return false;
	if ((tr->V1.x < bmin.x) && (tr->V2.x < bmin.x) && (tr->V3.x < bmin.x))
		return false;
	if ((tr->V1.y > bmax.y) && (tr->V2.y > bmax.y) && (tr->V3.y > bmax.y))
		return false;
	if ((tr->V1.y < bmin.y) && (tr->V2.y < bmin.y) && (tr->V3.y < bmin.y))
		return false;
	if ((tr->V1.z > bmax.z) && (tr->V2.z > bmax.z) && (tr->V3.z > bmax.z))
		return false;
	if ((tr->V1.z < bmin.z) && (tr->V2.z < bmin.z) && (tr->V3.z < bmin.z))
		return false;

	tris.add(tri);
	return true;
}

void BBox::addTrisRoot()
{
	int i;
	float maxx,maxy,maxz, minx, miny,minz;
	Triangle * tri;
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	for (i=triStart;  i<triStart+triNum;  i++)
	{
		tris.add(i);
		tri = &(scene->triangles[i]);
		maxx = max(max(tri->V1.x, tri->V2.x),tri->V3.x);
		maxy = max(max(tri->V1.y, tri->V2.y),tri->V3.y);
		maxz = max(max(tri->V1.z, tri->V2.z),tri->V3.z);
		minx = min(min(tri->V1.x, tri->V2.x),tri->V3.x);
		miny = min(min(tri->V1.y, tri->V2.y),tri->V3.y);
		minz = min(min(tri->V1.z, tri->V2.z),tri->V3.z);
		bmax.x = max(maxx, bmax.x);
		bmax.y = max(maxy, bmax.y);
		bmax.z = max(maxz, bmax.z);
		bmin.x = min(minx, bmin.x);
		bmin.y = min(miny, bmin.y);
		bmin.z = min(minz, bmin.z);
	}
	bmax.x += 0.1f;
	bmax.y += 0.1f;
	bmax.z += 0.1f;
	bmin.x -= 0.1f;
	bmin.y -= 0.1f;
	bmin.z -= 0.1f;
	tris.createArray();
	divideVolume();
	copyTriangles();
	releaseNonLeaf();
}

void BBox::releaseRoot()
{
	isLeaf = true;
	tris.destroyArray();
	tris.freeList();
	if (children)
		_aligned_free(children);
	children = NULL;
	if (tris4)
		_aligned_free(tris4);
	tris4 = NULL;
	triStart = triNum = 0;
}

void BBox::intersectTrisPureSSE(const Float4 &origin, const Float4 &direction, const Float4 &invdirection, Float4 &isect)
{
	#ifdef OWN_SSE

	if (!isLeaf)
	{
		Float4 nisect(isect);

		for (int i=0; i<8; i++)
		{
			if (!(vchildren & (1<<i)))
				continue;

			if (!children[i].intersectionSSE(origin, invdirection))
				continue;

			children[i].intersectTrisPureSSE(origin, direction, invdirection, nisect);

			__m128 d,d1,zero;
			zero = _mm_setzero_ps();
			d  = _mm_cmpgt_ps(nisect.sse, zero);		// dist > 0 
			d1 = _mm_cmplt_ps(nisect.sse, isect.sse);	// dist < maxdist
			d  = _mm_and_ps(d, d1);

			#ifdef OWN_SSE3
				d = _mm_movehdup_ps(d);
				d = _mm_movehl_ps(d, d);	// d is mask
			#else
				d = _mm_unpackhi_ps(d, d);
				d = _mm_unpackhi_ps(d, d);	// d is mask
			#endif

			__m128 am, bnm;		// isect = d ? nisect : isect
			am  = _mm_and_ps(nisect.sse, d);
			bnm = _mm_andnot_ps(d, isect.sse);
			isect.sse = _mm_or_ps(am, bnm);

		}	// end for

		return;
	}

	Ray4 ray4(origin, direction);
	Float4 mdist,U,V, inum, dist;
	#ifdef OWN_SSE3
		mdist.sse = _mm_movehdup_ps(isect.sse);
		mdist.sse = _mm_movehl_ps(mdist.sse, mdist.sse);
	#else
		mdist.sse = _mm_unpackhi_ps(isect.sse, isect.sse);
		mdist.sse = _mm_unpackhi_ps(mdist.sse, mdist.sse);
	#endif

	U.sse = _mm_setzero_ps();
	V.sse = _mm_setzero_ps();
	dist.sse = _mm_setzero_ps();
	inum.s_int[0] = inum.s_int[1] = inum.s_int[2] = inum.s_int[3] = -1;

	for (int i=0; i<num4; i++)
	{
		tris4[i].intersection(ray4, dist, U, V, mdist, inum);
	}

	//#define TEMP_SSE

	#ifdef TEMP_SSE

		__m128 tmp3, tmp2, tmp1, tmp0;
		__m128 row0, row1, row2, row3;

		tmp0   = _mm_shuffle_ps(U.sse, V.sse, 0x44);
		tmp2   = _mm_shuffle_ps(U.sse, V.sse, 0xEE);
		tmp1   = _mm_shuffle_ps(inum.sse, mdist.sse, 0x44);
		tmp3   = _mm_shuffle_ps(inum.sse, mdist.sse, 0xEE);

		row0 = _mm_shuffle_ps(tmp0, tmp1, 0x88);
		row1 = _mm_shuffle_ps(tmp0, tmp1, 0xDD);
		row2 = _mm_shuffle_ps(tmp2, tmp3, 0x88);
		row3 = _mm_shuffle_ps(tmp2, tmp3, 0xDD);

		__m128 d,d1,zero, iss;
		__m128 am, bnm;		
		zero= _mm_setzero_ps();

		iss = isect.sse;

		// row0 and isect  --->  isect
		d  = _mm_cmpgt_ps(row0, zero);		// dist > 0 
		d1 = _mm_cmplt_ps(row0, iss);	// dist < maxdist
		d  = _mm_and_ps(d, d1);

		d = _mm_unpackhi_ps(d , d);
		d = _mm_unpackhi_ps(d , d);			// d is mask

		am   = _mm_and_ps(row0, d);
		bnm  = _mm_andnot_ps(d, iss);
		iss = _mm_or_ps(am, bnm);		// isect = d ? nisect : isect

		// row1 and isect  --->  isect
		d  = _mm_cmpgt_ps(row1, zero);		// dist > 0 
		d1 = _mm_cmplt_ps(row1, iss);	// dist < maxdist
		d  = _mm_and_ps(d, d1);

		d = _mm_unpackhi_ps(d , d);
		d = _mm_unpackhi_ps(d , d);			// d is mask

		am   = _mm_and_ps(row1, d);
		bnm  = _mm_andnot_ps(d, iss);
		iss = _mm_or_ps(am, bnm);		// isect = d ? nisect : isect

		// row2 and isect  --->  isect
		d  = _mm_cmpgt_ps(row2, zero);		// dist > 0 
		d1 = _mm_cmplt_ps(row2, iss);	// dist < maxdist
		d  = _mm_and_ps(d, d1);

		d = _mm_unpackhi_ps(d , d);
		d = _mm_unpackhi_ps(d , d);			// d is mask

		am   = _mm_and_ps(row2, d);
		bnm  = _mm_andnot_ps(d, iss);
		iss = _mm_or_ps(am, bnm);		// isect = d ? nisect : isect

		// row3 and isect  --->  isect
		d  = _mm_cmpgt_ps(row3, zero);		// dist > 0 
		d1 = _mm_cmplt_ps(row3, iss);	// dist < maxdist
		d  = _mm_and_ps(d, d1);

		d = _mm_unpackhi_ps(d , d);
		d = _mm_unpackhi_ps(d , d);			// d is mask

		am   = _mm_and_ps(row3, d);
		bnm  = _mm_andnot_ps(d, iss);
		iss = _mm_or_ps(am, bnm);		// isect = d ? nisect : isect

		isect.sse = iss;

	#else

		int cl = -1;

		if (mdist.v1 > 0.001f)
			cl = 0;
		if (mdist.v2 > 0.001f   &&    (mdist.v2 < mdist.V[cl]  ||  cl<0) )
			cl = 1;
		if (mdist.v3 > 0.001f   &&    (mdist.v3 < mdist.V[cl]  ||  cl<0) )
			cl = 2;
		if (mdist.v4 > 0.001f   &&    (mdist.v4 < mdist.V[cl]  ||  cl<0) )
			cl = 3;
		if (cl > -1)
		{
			isect.v1 = U.V[cl];
			isect.v2 = V.V[cl];
			isect.s_int[2] = inum.s_int[cl];
			isect.v4 = mdist.V[cl];
		}

	#endif	// TEMP_SSE

	#endif  // OWN_SSE

}


float BBox::intersectTrisSSE(const Float4 &origin, const Float4 &direction, const Float4 &invdirection, int &tri, float &u, float &v)
{
	float best = -1.0f;
	int chosen, another;
	float d;
	int i;
	float tu, tv, bu, bv;

	if (!isLeaf)
	{
		for (i=0; i<8; i++)
		{
			if (!(vchildren & (1<<i)))
				continue;

			if (!children[i].intersectionSSE(origin, invdirection))
				continue;

			d = children[i].intersectTrisSSE(origin, direction, invdirection, another, tu, tv);
			if (d > SFLOAT)
			{
				if (best > SFLOAT)
				{
					if (d < best)
					{
						best = d;
						bu = tu;
						bv = tv;
						chosen = another;
					}
				}
				else
				{
					best = d;
					bu = tu;
					bv = tv;
					chosen = another;
				}
			}	// end positive distance
		}	// end for
		if (best > SFLOAT)
		{
			u = bu;
			v = bv;
			tri = chosen;
			return best;
		}
		else
			return -1.0f;
	}

	Ray4 ray4(origin, direction);
	Float4 mdist,U,V, inum, dist;
	#ifdef OWN_SSE
		mdist.sse = _mm_set1_ps(100000000000.0f);
	#else
		mdist.v1 = mdist.v1 = mdist.v1 = mdist.v1 = 100000000000.0f;
	#endif
	inum.s_int[0] = inum.s_int[1] = inum.s_int[2] = inum.s_int[3] = -1;
	for (i=0; i<num4; i++)
	{
		tris4[i].intersection(ray4, dist, U, V, mdist, inum);
	}

	int cl = -1;
	if (mdist.v1 > 0.001f)
		cl = 0;
	if (mdist.v2 > 0.001f   &&    (mdist.v2 < mdist.V[cl]  ||  cl<0) )
		cl = 1;
	if (mdist.v3 > 0.001f   &&    (mdist.v3 < mdist.V[cl]  ||  cl<0) )
		cl = 2;
	if (mdist.v4 > 0.001f   &&    (mdist.v4 < mdist.V[cl]  ||  cl<0) )
		cl = 3;
	if (cl > -1)
	{
		tri = inum.s_int[cl];
		u = U.V[cl];
		v = V.V[cl];
		return mdist.V[cl];
	}
	else
	{
		return -1;
	}
}

float BBox::intersectTris(const Point3d &origin, const Vector3d &direction, int &tri, float &u, float &v)
{
	float best = -1.0f;
	int chosen, another;
	float d;
	int i;
	float tu, tv, bu, bv;

	if (!isLeaf)
	{
		for (i=0; i<8; i++)
		{
			if (!(vchildren & (1<<i)))
				continue;

			if (children[i].intersection(origin, direction))
			{
				d = children[i].intersectTris(origin, direction, another, tu, tv);
				if (d > SFLOAT)
				{
					if (best > SFLOAT)
					{
						if (d < best)
						{
							best = d;
							bu = tu;
							bv = tv;
							chosen = another;
						}
					}
					else
					{
						best = d;
						bu = tu;
						bv = tv;
						chosen = another;
					}
				}	// end positive distance
			}	// end box intersected
		}	// end for
		if (best > SFLOAT)
		{
			u = bu;
			v = bv;
			tri = chosen;
			return best;
		}
		else
			return -1.0f;
	}

	Ray4 ray4(origin.x, origin.y, origin.z, direction.x, direction.y, direction.z);
	Float4 mdist,U,V, inum, dist;
	mdist.v1 = mdist.v2 = mdist.v3 = mdist.v4 = 100000000000.0f;
	inum.s_int[0] = inum.s_int[1] = inum.s_int[2] = inum.s_int[3] = -1;
	for (i=0; i<num4; i++)
	{
		tris4[i].intersection(ray4, dist, U, V, mdist, inum);
	}

	int cl = -1;
	if (mdist.v1 > 0.001f)
		cl = 0;
	if (mdist.v2 > 0.001f   &&    (mdist.v2 < mdist.V[cl]  ||  cl<0) )
		cl = 1;
	if (mdist.v3 > 0.001f   &&    (mdist.v3 < mdist.V[cl]  ||  cl<0) )
		cl = 2;
	if (mdist.v4 > 0.001f   &&    (mdist.v4 < mdist.V[cl]  ||  cl<0) )
		cl = 3;
	if (cl > -1)
	{
		tri = inum.s_int[cl];
		u = U.V[cl];
		v = V.V[cl];
		return mdist.V[cl];
	}
	else
	{
		return -1;
	}

	Triangle * gtri;

	for (i=0; i<tris.objCount; i++)
	{
		gtri = &(Raytracer::getInstance()->curScenePtr->triangles[tris.objArray[i]]);
		d = gtri->intersection(origin, direction, tu, tv);
		if (d > SFLOAT)
		{
			if (best > SFLOAT)
			{
				if (d < best)
				{
					best = d;
					bu = tu;
					bv = tv;
					chosen = tris.objArray[i];
				}
			}
			else
			{
				best = d;
				bu = tu;
				bv = tv;
				chosen = tris.objArray[i];
			}
		}
	}
	if (best > SFLOAT)
	{
		u = bu;
		v = bv;
		tri = chosen;
		return best;
	}
	return -1.0f;
}


bool BBox::intersection(const Point3d &origin, const Vector3d &direction, const float &maxDist)
{
	float tnear = -BIGFLOAT;
	float tfar = BIGFLOAT;
	float tempnear, tempfar, temp, adir;
	int i;

	for (i=0; i<3; i++)
	{	// for each dimension
		adir = fabs(direction.V[i]);
		if (adir < SFLOAT)
		{	// parallel direction
			if ((origin.V[i] < bmin.V[i]) || (origin.V[i] > bmax.V[i]))
				return false;
		}
		else
		{
			tempnear = (bmin.V[i] - origin.V[i])/direction.V[i];
			tempfar  = (bmax.V[i] - origin.V[i])/direction.V[i];
			if (tempnear > tempfar)
			{
				temp = tempnear;
				tempnear = tempfar;
				tempfar = temp;
			}
			if (tempnear > tnear)
				tnear = tempnear;
			if (tempfar  < tfar)
				tfar = tempfar;
		}
	}

	if (tnear > tfar)
		return false;

	if (tfar < SFLOAT)
		return false;

	if (maxDist > 0)
	{
		if (tnear > maxDist)
			return false;
	}
	return true;
}


float BBox::intersectTris(const Point3d &origin, const Vector3d &direction, int &tri, const float &maxDist)
{
	float best = -1.0f;
	int chosen, another;
	float d;
	int i;

	if (!isLeaf)
	{
		BBox * pBox;
		for (i=0; i<8; i++)
		{
			pBox = &(children[i]);
			if (!pBox)
			{
				continue;
			}
			if (pBox->intersection(origin, direction, maxDist))
			{
				d = pBox->intersectTris(origin, direction, another, maxDist);
				if (d > SFLOAT)
				{
					if (best > SFLOAT)
					{
						if (d < best)
						{
							best = d;
							chosen = another;
						}
					}
					else
					{
						best = d;
						chosen = another;
					}
				}	// end positive distance
			}	// end box intersected
		}	// end for
		if (best > SFLOAT)
		{
			tri = chosen;
			return best;
		}
		else
			return -1.0f;
	}

	Triangle * gtri;
	for (i=0; i<tris.objCount; i++)
	{
		gtri = &(Raytracer::getInstance()->curScenePtr->triangles[tris.objArray[i]]);
		d = gtri->intersection(origin, direction, maxDist);
		if (d > SFLOAT)
		{
			if (best > SFLOAT)
			{
				if (d < best)
				{
					best = d;
					chosen = tris.objArray[i];
				}
			}
			else
			{
				best = d;
				chosen = tris.objArray[i];
			}
		}
	}
	if (best > SFLOAT)
	{
		tri = chosen;
		return best;
	}
	return -1.0f;

}

void BBox::copyTriangles()
{
	if (!isLeaf)
	{
		for (int i=0; i<8; i++)
			children[i].copyTriangles();
		return;
	}

	Raytracer * rtr = Raytracer::getInstance();
	num4 = (int)ceil(tris.objCount/4.0f); 
	int fl = tris.objCount/4;
	last4 = tris.objCount%4;
	tris4 = (Triangle4 *)_aligned_malloc(sizeof(Triangle4)*num4, 16);
	for (int i=0;  i<fl;  i++)
	{
		tris4[i] = Triangle4(	(rtr->curScenePtr->triangles[tris.objArray[4*i  ]]), 
								(rtr->curScenePtr->triangles[tris.objArray[4*i+1]]), 
								(rtr->curScenePtr->triangles[tris.objArray[4*i+2]]), 
								(rtr->curScenePtr->triangles[tris.objArray[4*i+3]]),
								tris.objArray[4*i  ],
								tris.objArray[4*i+1],
								tris.objArray[4*i+2],
								tris.objArray[4*i+3]);
	}

	if (last4)
	{
		int n1,n2,n3,n4;
		Triangle *t1,*t2,*t3,*t4;
		t1 = &(rtr->curScenePtr->triangles[tris.objArray[fl*4]]);
		n1 = tris.objArray[fl*4];
		if (last4>1)
		{
			t2 = &(rtr->curScenePtr->triangles[tris.objArray[fl*4+1]]);
			n2 = tris.objArray[fl*4+1];
		}
		else
		{
			t2 = t1;
			n2 = n1;
		}
		if (last4>2)
		{
			t3 = &(rtr->curScenePtr->triangles[tris.objArray[fl*4+2]]);
			n3 = tris.objArray[fl*4+2];
		}
		else
		{
			t3 = t2;
			n3 = n2;
		}
		t4 = t3;
		n4 = n3;
		tris4[num4-1] = Triangle4(*t1,*t2,*t3,*t4, n1,n2,n3,n4);
	}
}

bool BBox::intersectionSSE(const Float4 &origin, const Float4 &invdirection, const Float4 &maxDist)
{
	#ifdef OWN_SSE
		__m128 infplus  = _mm_set1_ps( BIGFLOAT);
		__m128 infminus = _mm_set1_ps(-BIGFLOAT);
		__m128 tempnear1, tempfar1;

		tempnear1 = _mm_mul_ps(_mm_sub_ps(bmin.sse, origin.sse), invdirection.sse);
		tempfar1  = _mm_mul_ps(_mm_sub_ps(bmax.sse, origin.sse), invdirection.sse);

		__m128 ftn1 = _mm_min_ps(tempnear1, infplus);
		__m128 ftn2 = _mm_min_ps(tempfar1,  infplus);
		__m128 ftf1 = _mm_max_ps(tempnear1, infminus);
		__m128 ftf2 = _mm_max_ps(tempfar1,  infminus);

		__m128 lmax  = _mm_max_ps(ftn1, ftn2);
		__m128 lmin  = _mm_min_ps(ftf1, ftf2);

		__m128 lmax0 = _mm_shuffle_ps(lmax,lmax, 0x39);
		__m128 lmin0 = _mm_shuffle_ps(lmin,lmin, 0x39);
		__m128 lmax1 = _mm_movehl_ps(lmax,lmax);
		__m128 lmin1 = _mm_movehl_ps(lmin,lmin);

		lmax = _mm_min_ss(lmax, lmax0);
		lmin = _mm_max_ss(lmin, lmin0);
		lmax = _mm_min_ss(lmax, lmax1);
		lmin = _mm_max_ss(lmin, lmin1);

		bool ret = (bool)((_mm_comige_ss(maxDist.sse, lmin) & _mm_comige_ss(lmax, _mm_setzero_ps()) & _mm_comige_ss(lmax,lmin)) != 0);
		return ret;
	#else
		return false;
	#endif
}


bool BBox::intersectionSSE(const Float4 &origin, const Float4 &invdirection)
{
	#ifdef OWN_SSE
		__m128 infplus  = _mm_set1_ps( BIGFLOAT);
		__m128 infminus = _mm_set1_ps(-BIGFLOAT);
		__m128 tempnear1, tempfar1;

		tempnear1 = _mm_mul_ps(_mm_sub_ps(bmin.sse, origin.sse), invdirection.sse);
		tempfar1  = _mm_mul_ps(_mm_sub_ps(bmax.sse, origin.sse), invdirection.sse);

		__m128 ftn1 = _mm_min_ps(tempnear1, infplus);
		__m128 ftn2 = _mm_min_ps(tempfar1,  infplus);
		__m128 ftf1 = _mm_max_ps(tempnear1, infminus);
		__m128 ftf2 = _mm_max_ps(tempfar1,  infminus);

		__m128 lmax  = _mm_max_ps(ftn1, ftn2);
		__m128 lmin  = _mm_min_ps(ftf1, ftf2);

		__m128 lmax0 = _mm_shuffle_ps(lmax,lmax, 0x39);
		__m128 lmin0 = _mm_shuffle_ps(lmin,lmin, 0x39);
		__m128 lmax1 = _mm_movehl_ps(lmax,lmax);
		__m128 lmin1 = _mm_movehl_ps(lmin,lmin);

		lmax = _mm_min_ss(lmax, lmax0);
		lmin = _mm_max_ss(lmin, lmin0);
		lmax = _mm_min_ss(lmax, lmax1);
		lmin = _mm_max_ss(lmin, lmin1);

		bool ret = (bool)((_mm_comige_ss(lmax, _mm_setzero_ps()) & _mm_comige_ss(lmax,lmin)) != 0);
		return ret;
	#else
		return false;
	#endif
}
