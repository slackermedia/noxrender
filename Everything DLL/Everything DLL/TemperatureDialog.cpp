#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

// old gui version

INT_PTR CALLBACK TemperatureDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			
			EMColorShow * emcs = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_COLOR));
			EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
			EMButton * embok = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_OK));
			EMButton * embcancel = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_CANCEL));
			EMEditSpin * emspin = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_SPIN));
			EMButton * embd50 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_D50));
			EMButton * embd55 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_D55));
			EMButton * embd65 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_D65));
			EMButton * embd75 = GetEMButtonInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_D75));
			EMText * emtext = GetEMTextInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_TEXT));

			GlobalWindowSettings::colorSchemes.apply(emcs);
			GlobalWindowSettings::colorSchemes.apply(emtp);
			GlobalWindowSettings::colorSchemes.apply(embok);
			GlobalWindowSettings::colorSchemes.apply(embcancel);
			GlobalWindowSettings::colorSchemes.apply(emspin);
			GlobalWindowSettings::colorSchemes.apply(embd50);
			GlobalWindowSettings::colorSchemes.apply(embd55);
			GlobalWindowSettings::colorSchemes.apply(embd65);
			GlobalWindowSettings::colorSchemes.apply(embd75);
			GlobalWindowSettings::colorSchemes.apply(emtext, true);

			int temperature = (int)lParam;
			temperature = min(10000, max(1000, temperature));
			emspin->setValuesToInt(temperature, 1000, 10000, 50, 50);
			emtp->setTemperature(temperature);
			emcs->color = emtp->getGammaColor();
			InvalidateRect(emcs->hwnd, NULL, false);
	
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
			case IDC_TEMPERATURE_OK:
				{
					if (wmEvent == BN_CLICKED)
					{
						EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
						int temperature = emtp->getTemperature();
						EndDialog(hWnd, (INT_PTR)(temperature));
					}
				}
				break;
			case IDC_TEMPERATURE_CANCEL:
				{
					if (wmEvent == BN_CLICKED)
					{
						EndDialog(hWnd, (INT_PTR)NULL);
					}
				}
				break;
			case IDC_TEMPERATURE_BAR:
				{
					if (wmEvent != WM_HSCROLL)
						break;
					EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
					EMEditSpin * emspin = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_SPIN));
					emspin->setValuesToInt(emtp->getTemperature(), 1000, 10000, 50, 50);
					EMColorShow * emc = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_COLOR));
					emc->color = emtp->getGammaColor();
					InvalidateRect(emc->hwnd, NULL, false);
				}
				break;
			case IDC_TEMPERATURE_D50:
				{
					if (wmEvent != BN_CLICKED)
						break;
					EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
					emtp->setTemperature(5000);
					EMEditSpin * emspin = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_SPIN));
					emspin->setValuesToInt(5000, 1000, 10000, 50, 50);
					EMColorShow * emc = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_COLOR));
					emc->color = emtp->getGammaColor();
					InvalidateRect(emc->hwnd, NULL, false);
				}
				break;
			case IDC_TEMPERATURE_D55:
				{
					if (wmEvent != BN_CLICKED)
						break;
					EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
					emtp->setTemperature(5500);
					EMEditSpin * emspin = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_SPIN));
					emspin->setValuesToInt(5500, 1000, 10000, 50, 50);
					EMColorShow * emc = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_COLOR));
					emc->color = emtp->getGammaColor();
					InvalidateRect(emc->hwnd, NULL, false);
				}
				break;
			case IDC_TEMPERATURE_D65:
				{
					if (wmEvent != BN_CLICKED)
						break;
					EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
					emtp->setTemperature(6500);
					EMEditSpin * emspin = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_SPIN));
					emspin->setValuesToInt(6500, 1000, 10000, 50, 50);
					EMColorShow * emc = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_COLOR));
					emc->color = emtp->getGammaColor();
					InvalidateRect(emc->hwnd, NULL, false);
				}
				break;
			case IDC_TEMPERATURE_D75:
				{
					if (wmEvent != BN_CLICKED)
						break;
					EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
					emtp->setTemperature(7500);
					EMEditSpin * emspin = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_SPIN));
					emspin->setValuesToInt(7500, 1000, 10000, 50, 50);
					EMColorShow * emc = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_COLOR));
					emc->color = emtp->getGammaColor();
					InvalidateRect(emc->hwnd, NULL, false);
				}
				break;
			case IDC_TEMPERATURE_SPIN:
				{
					if (wmEvent != WM_VSCROLL)
						break;
					EMTemperaturePicker * emtp = GetEMTemperaturePickerInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_BAR));
					EMEditSpin * emspin = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_SPIN));
					int val = emspin->intValue;
					val = ((val+25)/50)*50;
					emspin->setValuesToInt(val, 1000, 10000, 50, 50);
					emtp->setTemperature(val);
					EMColorShow * emc = GetEMColorShowInstance(GetDlgItem(hWnd, IDC_TEMPERATURE_COLOR));
					emc->color = emtp->getGammaColor();
					InvalidateRect(emc->hwnd, NULL, false);
				}
				break;
			}
		}
		break;
	case WM_CTLCOLORSTATIC:
		{
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)bgBrush;
		}
	case WM_CTLCOLORDLG:
		{
			return (INT_PTR)bgBrush;
		}
		break;
	}
	return false;
}




