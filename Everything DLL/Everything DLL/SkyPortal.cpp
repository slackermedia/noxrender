#include "raytracer.h"
#include "log.h"

//---------------------------------------------------------------------

SkyPortalMesh::SkyPortalMesh() :
	tris(0)
{
	numTris = 0;
	instNum = -1;
	totalArea = 0.0f;
	triAreas = NULL;
	triAreasSum = NULL;
}

//---------------------------------------------------------------------

SkyPortalMesh::~SkyPortalMesh()
{
	freeArrays();
}

//---------------------------------------------------------------------

void SkyPortalMesh::freeArrays()
{
	if (triAreas)
		free(triAreas);
	if (triAreasSum)
		free(triAreasSum);
	triAreas = NULL;
	triAreasSum = NULL;
	tris.freeList();
	tris.destroyArray();
}

//---------------------------------------------------------------------

int SkyPortalMesh::randomTriangle(float &prob, int &meshTriID)
{
	prob = 1;
	meshTriID = -1;
	if (numTris <= 0)
		return -1;

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = getMarsaglia()/(float)(unsigned int)(MAXMARS);
	#endif
	a *= totalArea;
	int chosen = 0;
	float area = 1;

	//if (numTris > 16)
	if (0)
	{
		int tmin = 1;
		int tmax = numTris-2;
		float ta1 = triAreasSum[tmin];
		float ta2 = triAreasSum[tmax];
		if (ta1>=a)
		{
			chosen = 0;
			prob = triAreas[chosen]/totalArea;
			meshTriID = chosen;
			return tris[chosen];
		}
		if (ta2<=a)
		{
			chosen = numTris-1;
			prob = triAreas[chosen]/totalArea;
			meshTriID = chosen;
			return tris[chosen];
		}

		
		for (int i=0; i<40; i++)
		{
			int tavg = (tmin+tmax)/2;
			chosen = tavg;
			float ta3p = triAreasSum[tavg-1];
			float ta3  = triAreasSum[tavg];
			float ta3n = triAreasSum[tavg+1];
			if (ta3<a)
			{
				tmin = tavg;
				ta1 = ta3;
				if (ta3n>=a)
				{
					chosen = tavg+1;
					prob = triAreas[chosen]/totalArea;
					meshTriID = chosen;
					return tris[chosen];
				}
			}
			else
			{
				tmax = tavg;
				ta2 = ta3;
				if (ta3p<=a)
				{
					chosen = tavg;
					prob = triAreas[chosen]/totalArea;
					meshTriID = chosen;
					return tris[chosen];
				}
			}
		}

		prob = triAreas[chosen]/totalArea;
		meshTriID = chosen;
		return tris[chosen];
	}
	else
	{
		float soFar = 0;
		for (int i=0; i<numTris; i++)
		{
			soFar += triAreas[i];
			chosen = i;
			if (a < soFar)
				break;
		}
		
		prob = triAreas[chosen]/totalArea;
		meshTriID = chosen;
		return tris[chosen];
	}
}

//---------------------------------------------------------------------

SkyPortalMesh * SkyPortalMesh::copy()
{
	SkyPortalMesh * spm = new SkyPortalMesh();
	if (!spm)
		return 0;

	spm->totalArea = totalArea;
	spm->numTris = numTris;
	spm->instNum = instNum;

	spm->triAreas = (float *)malloc(sizeof(float)*numTris);
	spm->triAreasSum = (float *)malloc(sizeof(float)*numTris);
	memcpy(spm->triAreas, triAreas, sizeof(float)*numTris);
	memcpy(spm->triAreasSum, triAreasSum, sizeof(float)*numTris);

	for (int i=0; i<tris.objCount; i++)
		spm->tris.add(tris[i]);
	spm->tris.createArray();

	return spm;
}

//---------------------------------------------------------------------

int Scene::randomSkyPortal(float &prob)
{
	prob = 1;
	if (skyportalMeshes.objCount <= 0)
		return -1;

	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = getMarsaglia()/(float)(unsigned int)(MAXMARS);
	#endif

	a *= sscene.skyPortalsTotalArea;
	int chosen = -1;
	int numMeshes = skyportalMeshes.objCount;
	float soFar = 0.0f;

	for (int i=0; i<numMeshes; i++)
	{
		soFar += skyportalMeshes[i]->totalArea;
		chosen = i;
		if (a <= soFar)
			break;
	}
	prob = skyportalMeshes[chosen]->totalArea / sscene.skyPortalsTotalArea;
	return chosen;
}

//---------------------------------------------------------------------

bool Scene::updateSkyportals()
{
	for (int i=0; i<skyportalMeshes.objCount; i++)
	{
		skyportalMeshes[i]->freeArrays();
		delete skyportalMeshes[i];
		skyportalMeshes.freeList();
		skyportalMeshes.destroyArray();
	}

	sscene.useSkyPortals = false;
	sscene.skyPortalsTotalArea = 0.0f;
	for (int i=0; i<mats.objCount; i++)
	{
		if (usedMaterials[i])
			if (mats[i]->isSkyPortal)
				sscene.useSkyPortals = true;
	}

	if (!sscene.useSkyPortals)
		return true;

	for (int i=0; i<instances.objCount; i++)
	{
		MeshInstance * mInst = &(instances[i]);
		bool hasSkyP = false;
		for (int j=0; j<mInst->materials.objCount; j++)
			if (mats[mInst->materials[j]]->isSkyPortal)
				hasSkyP = true;
		if (!hasSkyP)
			continue;

		SkyPortalMesh * tmesh = new SkyPortalMesh();
		skyportalMeshes.add(tmesh);
		skyportalMeshes.createArray();
		SkyPortalMesh * skpMesh = skyportalMeshes[skyportalMeshes.objCount-1];

		Mesh * mesh = &(meshes[mInst->meshID]);
		for (int j=mesh->firstTri; j<=mesh->lastTri; j++)
		{
			Triangle * tri = &(triangles[j]);
			MaterialNox * mat = mats[mInst->materials[tri->matInInst]];
			if (mat->isSkyPortal)
				skpMesh->tris.add(j);
		}
		skpMesh->tris.createArray();

		skpMesh->numTris = skpMesh->tris.objCount;
		skpMesh->triAreas = (float *)malloc(sizeof(float)*skpMesh->numTris);
		skpMesh->triAreasSum = (float *)malloc(sizeof(float)*skpMesh->numTris);
		skpMesh->instNum = i;

		for (int j=0; j<skpMesh->tris.objCount; j++)
		{
			Triangle tri;
			tri.V1 = mInst->matrix_transform * triangles[skpMesh->tris[j]].V1;
			tri.V2 = mInst->matrix_transform * triangles[skpMesh->tris[j]].V2;
			tri.V3 = mInst->matrix_transform * triangles[skpMesh->tris[j]].V3;
			float area = tri.evaluateArea();
			skpMesh->triAreas[j] = area;
			skpMesh->triAreasSum[j] = skpMesh->totalArea;
			skpMesh->totalArea  += skpMesh->triAreas[j];
		}
		sscene.skyPortalsTotalArea += skpMesh->totalArea;

		char buf[256];
		sprintf_s(buf, 256, "Adding sky portal mesh - used %d of %d tris, area: %.4f", skpMesh->numTris, (meshes[i].lastTri-meshes[i].firstTri+1), skpMesh->totalArea);
		Logger::add(buf);
	}

	char buf[256];
	sprintf_s(buf, 256, "Total skyportals: %d", skyportalMeshes.objCount);
	Logger::add(buf);
	
	sscene.useSkyPortals = (sscene.skyPortalsTotalArea>0.0f);

	return true;
}

//---------------------------------------------------------------------

bool Scene::updateSkyportalsOld1()
{
	for (int i=0; i<skyportalMeshes.objCount; i++)
	{
		skyportalMeshes[i]->freeArrays();
		delete skyportalMeshes[i];
		skyportalMeshes.freeList();
		skyportalMeshes.destroyArray();
	}

	sscene.useSkyPortals = false;
	sscene.skyPortalsTotalArea = 0.0f;
	for (int i=0; i<mats.objCount; i++)
	{
		if (usedMaterials[i])
			if (mats[i]->isSkyPortal)
				sscene.useSkyPortals = true;
	}

	if (!sscene.useSkyPortals)
		return true;

	for (int i=0; i<meshes.objCount; i++)
	{
		bool meshportal = false;
		for(int j=meshes[i].firstTri; j<=meshes[i].lastTri; j++)
			if (mats[triangles[j].matNum]->isSkyPortal)
				meshportal = true;
		if (meshportal)
		{
			SkyPortalMesh * pmesh = new SkyPortalMesh();
			for(int j=meshes[i].firstTri; j<=meshes[i].lastTri; j++)
				if (mats[triangles[j].matNum]->isSkyPortal)
					pmesh->tris.add(j);
			pmesh->tris.createArray();
			pmesh->numTris = pmesh->tris.objCount;
			pmesh->triAreas = (float *)malloc(sizeof(float)*pmesh->numTris);
			pmesh->triAreasSum = (float *)malloc(sizeof(float)*pmesh->numTris);
			pmesh->instNum = i;
			float areaSum = 0.0f;
			for (int j=0; j<pmesh->tris.objCount; j++)
			{
				float area = triangles[pmesh->tris[j]].area;
				areaSum += area;
				pmesh->triAreas[j] = area;
				pmesh->triAreasSum[j] = areaSum;
			}
			pmesh->totalArea = areaSum;
			sscene.skyPortalsTotalArea += areaSum;

			skyportalMeshes.add(pmesh);
			skyportalMeshes.createArray();
		}
	}

	char buf[256];
	sprintf_s(buf, 256, "Total skyportals: %d", skyportalMeshes.objCount);
	Logger::add(buf);


	return true;
}

//---------------------------------------------------------------------
