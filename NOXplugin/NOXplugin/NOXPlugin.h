#ifndef __NOXplugin__H
#define __NOXplugin__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

bool dirExists1(TCHAR * dirName);
TCHAR * getRendererDirectory1();
TCHAR * getNoxUserDirectory1();
TCHAR * getNoxUserDirectoryFromReg();

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#endif
