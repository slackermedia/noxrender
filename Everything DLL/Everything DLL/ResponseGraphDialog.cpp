#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "CameraResponse.h"

bool drawResponseGraph(ImageByteBuffer * imgbuf, int respNum, bool compareToGamma);


INT_PTR CALLBACK ResponseGraphDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH hBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				hBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				int rNum = (int)lParam;

				EMComboBox * emCombo	= GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_DLG_RESP_CHOOSE_RESPONSE));
				EMButton   * emOK		= GetEMButtonInstance(GetDlgItem(hWnd, IDC_DLG_RESP_OK));
				EMButton   * emCancel	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_DLG_RESP_CANCEL));
				EMPView    * emGraph	= GetEMPViewInstance(GetDlgItem(hWnd, IDC_DLG_RESP_GRAPH));
				EMCheckBox * emCmpGamma = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_DLG_RESP_COMPARE_GAMMA));
				EMText     * emTxtGamma = GetEMTextInstance(GetDlgItem(hWnd, IDC_DLG_RESP_TEXT_CMP_GAMMA));
				SetWindowPos(emGraph->hwnd, HWND_TOP,  1,  1,  514, 514,   SWP_NOOWNERZORDER | SWP_NOMOVE);
				emCmpGamma->selected = true;
				GlobalWindowSettings::colorSchemes.apply(emCombo);
				GlobalWindowSettings::colorSchemes.apply(emOK);
				GlobalWindowSettings::colorSchemes.apply(emCancel);
				GlobalWindowSettings::colorSchemes.apply(emGraph);
				GlobalWindowSettings::colorSchemes.apply(emCmpGamma);
				GlobalWindowSettings::colorSchemes.apply(emTxtGamma, true);

				emCombo->deleteItems();
				for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
					emCombo->addItem(getResponseFunctionName(i));
				emCombo->selected = rNum;
				InvalidateRect(emCombo->hwnd, NULL, false);

				if (!emGraph->byteBuff)
					emGraph->byteBuff = new ImageByteBuffer();
				emGraph->byteBuff->allocBuffer(512, 512);
				emGraph->byteBuff->clearBuffer();
				emGraph->dontLetUserChangeZoom = true;
				drawResponseGraph(emGraph->byteBuff, rNum, true);
				InvalidateRect(emGraph->hwnd, NULL, false);
			}
			break;
		case WM_DESTROY:
			{
				DeleteObject(hBrush);
				hBrush = 0;
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)(-1));
			}
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
				case IDC_DLG_RESP_OK:
					{
						if (wmEvent == BN_CLICKED)
						{
							EMComboBox * emCombo	= GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_DLG_RESP_CHOOSE_RESPONSE));
							int res = -1;
							if (emCombo)
								res = emCombo->selected;
							EndDialog(hWnd, (INT_PTR)(res));
						}
					}
					break;
				case IDC_DLG_RESP_CANCEL:
					{
						if (wmEvent == BN_CLICKED)
						{
							EndDialog(hWnd, (INT_PTR)(-1));
						}
					}
					break;
				}
				case IDC_DLG_RESP_COMPARE_GAMMA:
				case IDC_DLG_RESP_CHOOSE_RESPONSE:
					{
						if (wmEvent != CBN_SELCHANGE  &&  wmEvent != BN_CLICKED)
							break;

						EMCheckBox * emCmpGamma = GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_DLG_RESP_COMPARE_GAMMA));
						EMComboBox * emCombo = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_DLG_RESP_CHOOSE_RESPONSE));
						EMPView * emGraph = GetEMPViewInstance(GetDlgItem(hWnd, IDC_DLG_RESP_GRAPH));
						if (!emCombo  ||  !emGraph)
							break;

						ImageByteBuffer * imgbuf = emGraph->byteBuff;
						if (!imgbuf)
							break;

						bool showGamma = emCmpGamma->selected;
						bool grDrOK = drawResponseGraph(imgbuf, emCombo->selected, showGamma);

						if (grDrOK)
							InvalidateRect(emGraph->hwnd, NULL, false);

					}
					break;
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)hBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)hBrush;
			}
			break;
	}
	return false;
}

bool drawResponseGraph(ImageByteBuffer * imgbuf, int respNum, bool compareToGamma)
{
	imgbuf->clearBuffer();

	bool showLines = true;
	int llength = 6;
	int ldist = 64;
	COLORREF linesColor = RGB(48,48,48);
	COLORREF cGammaColor = RGB(64,64,64);

	if (showLines)
	{
		for (int y=ldist; y<512; y+=ldist)
		{
			for (int x=0; x<512; x++)
			{
				int a = x%(llength*2)/llength;
				COLORREF col = linesColor*(1-a);
				imgbuf->imgBuf[y][x] = col;
			}
		}
		for (int x=ldist; x<512; x+=ldist)
		{
			for (int y=0; y<512; y++)
			{
				int a = y%(llength*2)/llength;
				COLORREF col = linesColor*(a);
				imgbuf->imgBuf[y][x] = col;
			}
		}

	}

	if (compareToGamma)
	{
		float cgamma = 1.0f/2.2f;
		for (int i=0; i<511; i++)
		{
			int x1 = i;
			int x2 = i+1;
			float fy1 = x1/511.0f;
			float fy2 = x2/511.0f;
			fy1 = pow(fy1, cgamma); 
			fy2 = pow(fy2, cgamma); 
			int y1 = min(511, max(0, 511-(int)(fy1*511)));
			int y2 = min(511, max(0, 511-(int)(fy2*511)));
			if (y1>y2)
				for (int yy=y1; yy>=y2; yy--)
					imgbuf->imgBuf[yy][x1] = cGammaColor;
			else
				for (int yy=y1; yy<=y2; yy++)
					imgbuf->imgBuf[yy][x1] = cGammaColor;
		}
	}


	float rgamma = 1.0f/2.2f;
	if (respNum == 0)
	{
		for (int i=0; i<511; i++)
		{
			int x1 = i;
			int x2 = i+1;
			float fy1 = x1/511.0f;
			float fy2 = x2/511.0f;
			fy1 = pow(fy1, rgamma); 
			fy2 = pow(fy2, rgamma); 
			int y1 = min(511, max(0, 511-(int)(fy1*511)));
			int y2 = min(511, max(0, 511-(int)(fy2*511)));
			if (y1>y2)
				for (int yy=y1; yy>=y2; yy--)
					imgbuf->imgBuf[yy][x1] = RGB(255,255,255);
			else
				for (int yy=y1; yy<=y2; yy++)
					imgbuf->imgBuf[yy][x1] = RGB(255,255,255);
		}
		return true;
	}


	if (respNum<0 || respNum>NOX_NUM_RESPONSE_FILTERS)
		return false;

	float * floatsRed	= getResponseFunctionRed(respNum);
	float * floatsGreen	= getResponseFunctionGreen(respNum);
	float * floatsBlue	= getResponseFunctionBlue(respNum);
	CHECK(floatsRed);
	CHECK(floatsGreen);
	CHECK(floatsBlue);

	for (int i=0; i<511; i++)
	{
		float yr1 = floatsRed[  i*2+0];
		float yr2 = floatsRed[  i*2+2];
		float yg1 = floatsGreen[i*2+0];
		float yg2 = floatsGreen[i*2+2];
		float yb1 = floatsBlue[ i*2+0];
		float yb2 = floatsBlue[ i*2+2];
		int ir1 = min(511, max(0, 511-(int)(yr1*511)));
		int ir2 = min(511, max(0, 511-(int)(yr2*511)));
		int ig1 = min(511, max(0, 511-(int)(yg1*511)));
		int ig2 = min(511, max(0, 511-(int)(yg2*511)));
		int ib1 = min(511, max(0, 511-(int)(yb1*511)));
		int ib2 = min(511, max(0, 511-(int)(yb2*511)));

		if (ir1>ir2)
			for (int yy=ir1; yy>=ir2; yy--)
				imgbuf->imgBuf[yy][i] |= RGB(255,0,0);
		else
			for (int yy=ir1; yy<=ir2; yy++)
				imgbuf->imgBuf[yy][i] |= RGB(255,0,0);

		if (ig1>ig2)
			for (int yy=ig1; yy>=ig2; yy--)
				imgbuf->imgBuf[yy][i] |= RGB(0,255,0);
		else
			for (int yy=ig1; yy<=ig2; yy++)
				imgbuf->imgBuf[yy][i] |= RGB(0,255,0);

		if (ib1>ib2)
			for (int yy=ib1; yy>=ib2; yy--)
				imgbuf->imgBuf[yy][i] |= RGB(0,0,255);
		else
			for (int yy=ib1; yy<=ib2; yy++)
				imgbuf->imgBuf[yy][i] |= RGB(0,0,255);

	}

	return true;
}


