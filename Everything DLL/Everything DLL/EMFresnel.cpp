#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "FastMath.h"

extern HMODULE hDllModule;

void InitEMFresnel()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMFresnel";
    wc.hInstance      = hDllModule;
	wc.lpfnWndProc    = EMFresnelProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EMFresnel *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMFresnel * GetEMFresnelInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMFresnel * emf = (EMFresnel *)GetWindowLongPtr(hwnd, 0);
	#else
		EMFresnel * emf = (EMFresnel *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emf;
}

void SetEMFresnelInstance(HWND hwnd, EMFresnel *emf)
{
	#ifdef _WIN64
	    SetWindowLongPtr(hwnd, 0, (LONG_PTR)emf);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emf);
	#endif
}

EMFresnel::EMFresnel(HWND hWnd)
{
	colBorderLeftUp = RGB(0,0,128);
	colBorderRightDown = RGB(0,0,255);
	colBackgroundUp = RGB(128,192,255);
	colBackgroundDown = RGB(64,128,255);
	colPoint = RGB(0,0,0);
	colPointOver = RGB(255,255,255);
	colPointClicked = RGB(255,0,0);
	colLine = RGB(255,255,128);
	showBorder = true;

	IOR = 1.6f;
	moveChangeIOR = 0.01f;
	maxIOR = 100;
	minIOR = 1.0f;

	mode = MODE_CUSTOM;
	drawSimpleLines = true;
	X1 = 0.3f;
	X2 = 0.6f;
	Y1 = 0.3f;
	Y2 = 0.6f;
	Y0 = 0.0f;
	Y3 = 1.0f;
	overPoint0 = false;
	overPoint1 = false;
	overPoint2 = false;
	overPoint3 = false;

	clicked = false;
	overControl = false;

	hwnd = hWnd;
	hUp = 0;
	hDown = 0;
	pLine = 0;
	numLinePoints = 0;
}

EMFresnel::~EMFresnel()
{
	if (pLine)
		delete [] pLine;
}

LRESULT CALLBACK EMFresnelProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMFresnel* emf;
	emf = GetEMFresnelInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HPEN hOldPen;

	switch (msg)
	{
		case WM_CREATE:
			GetClientRect(hwnd, &rect);
			emf = new EMFresnel(hwnd);
			SetEMFresnelInstance(hwnd, emf);
			break;
		case WM_PAINT:
		{
			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);

			// eval new plot points and regions to fill
			emf->createNewRegions();

			// fill upper and lower part
			HBRUSH brUp = CreateSolidBrush(emf->colBackgroundUp);
			FillRgn(hdc, emf->hUp, brUp);
			DeleteObject(brUp);
			HBRUSH brDown = CreateSolidBrush(emf->colBackgroundDown);
			FillRgn(hdc, emf->hDown, brDown);
			DeleteObject(brDown);

			// draw plot
			hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 2, emf->colLine));
			Polyline(hdc, emf->pLine, emf->numLinePoints);
			DeleteObject(SelectObject(hdc, hOldPen));

			if (emf->showBorder)
			{
				GetClientRect(hwnd, &rect);
				POINT border1[] = {	{0, rect.bottom-1},  {0,0}, {rect.right, 0} };
				POINT border2[] = {	{rect.right-1,1}, {rect.right-1,rect.bottom-1}, 
									{0, rect.bottom-1}};
				hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colBorderLeftUp));
				Polyline(hdc, border1, 3);
				DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colBorderRightDown)));
				Polyline(hdc, border2, 3);
				DeleteObject(SelectObject(hdc, hOldPen));
			}

			// if mouse is over control in custom mode
			if (emf->overControl && emf->mode==EMFresnel::MODE_CUSTOM)
			{
				GetClientRect(hwnd, &rect);
				int x,y;

				// first point
				x = (int)(rect.right*emf->X1 );
				y = (int)(rect.bottom*(1-emf->Y1) );
				POINT pp1[] = { {x-2,y-2}, {x+2,y-2}, {x+2,y+2}, {x-2,y+2}, {x-2,y-2} };

				if (emf->overPoint1)
				{
					if (emf->clicked)
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointClicked));
					else
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointOver));
				}
				else
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPoint));
				Polyline(hdc, pp1, 5);
				DeleteObject(SelectObject(hdc, hOldPen));

				// second point
				x = (int)(rect.right*emf->X2 );
				y = (int)(rect.bottom*(1-emf->Y2) );
				POINT pp2[] = { {x-2,y-2}, {x+2,y-2}, {x+2,y+2}, {x-2,y+2}, {x-2,y-2} };

				if (emf->overPoint2)
				{
					if (emf->clicked)
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointClicked));
					else
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointOver));
				}
				else
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPoint));
				Polyline(hdc, pp2, 5);
				DeleteObject(SelectObject(hdc, hOldPen));

				// left point
				x = 2;
				y = (int)(rect.bottom*(1-emf->Y0) );
				POINT pp3[] = { {x-2,y-2}, {x+2,y-2}, {x+2,y+2}, {x-2,y+2}, {x-2,y-2} };

				if (emf->overPoint0)
				{
					if (emf->clicked)
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointClicked));
					else
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointOver));
				}
				else
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPoint));
				Polyline(hdc, pp3, 5);
				DeleteObject(SelectObject(hdc, hOldPen));

				// right point
				x = rect.right-3;
				y = (int)(rect.bottom*(1-emf->Y3) );
				POINT pp4[] = { {x-2,y-2}, {x+2,y-2}, {x+2,y+2}, {x-2,y+2}, {x-2,y-2} };

				if (emf->overPoint3)
				{
					if (emf->clicked)
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointClicked));
					else
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPointOver));
				}
				else
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emf->colPoint));
				Polyline(hdc, pp4, 5);
				DeleteObject(SelectObject(hdc, hOldPen));
			}

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
		case WM_MOUSEMOVE:
		{
			if (!GetCapture())
				SetCapture(hwnd);
			if (GetCapture() == hwnd)
			{
				POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				GetClientRect(hwnd, &rect);
				switch (emf->mode)
				{
					case EMFresnel::MODE_CUSTOM:
						emf->processMouseMoveCustom((short)pt.x, (short)pt.y);
						break;
					case EMFresnel::MODE_FRESNEL:
						emf->processMouseMoveFresnel((short)pt.x, (short)pt.y);
						break;
				}
			}
		}
		break;
		case WM_LBUTTONDOWN:
		{
			POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
			GetClientRect(hwnd, &rect);
			switch (emf->mode)
			{
				case EMFresnel::MODE_CUSTOM:
					emf->processMouseDownCustom((short)pt.x, (short)pt.y);
					break;
				case EMFresnel::MODE_FRESNEL:
					emf->processMouseDownFresnel((short)pt.x, (short)pt.y);
					break;
			}
		}
		break;
		case WM_LBUTTONUP:
		{
			POINT pt = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
			GetClientRect(hwnd, &rect);
			switch (emf->mode)
			{
				case EMFresnel::MODE_CUSTOM:
					emf->processMouseUpCustom((short)pt.x, (short)pt.y);
					break;
				case EMFresnel::MODE_FRESNEL:
					emf->processMouseUpFresnel((short)pt.x, (short)pt.y);
					break;
			}
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMFresnel::processMouseMoveFresnel(short x, short y)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	POINT pt = {x,y};
	if (clicked)
	{
		int yRes = GetSystemMetrics(SM_CYSCREEN);
		ClientToScreen(hwnd, &pt);
		if (pt.y > yRes-2)
		{
			SetCursorPos(pt.x, 1);
			lastYPos -= yRes;
		}
		else
			if (pt.y < 1)
			{
				SetCursorPos(pt.x, yRes-2);
				lastYPos += yRes;
			}

		IOR = lastIOR + (lastYPos - y)*moveChangeIOR;
		if (IOR < minIOR)
			IOR = minIOR;
		if (IOR > maxIOR)
			IOR = maxIOR;
		InvalidateRect(hwnd, &rect, false);
		notifyScrolling();
	}
	else
	{
		overControl = (TRUE == PtInRect(&rect, pt));
		if (overControl)
		{
		}
		else
		{
			ReleaseCapture();
		}
	}
}

void EMFresnel::processMouseDownFresnel(short x, short y)
{
	clicked = true;
	lastYPos = y;
	lastIOR = IOR;
}

void EMFresnel::processMouseUpFresnel(short x, short y)
{
	clicked = false;
}

void EMFresnel::processMouseMoveCustom(short x, short y)
{
	POINT pt = {x, y};
	RECT rect;
	GetClientRect(hwnd, &rect);

	if (clicked)
	{
		if (overPoint1)
		{
			X1 = pt.x/(float)rect.right;
			Y1 = 1 - pt.y/(float)rect.bottom;
			if (X1 > 0.995f)
				X1 = 0.995f;
			if (X1 < 0.005f)
				X1 = 0.005f;
			if (Y1 > 1)
				Y1 = 1;
			if (Y1 < 0)
				Y1 = 0;
			InvalidateRect(hwnd, &rect, false);
		}
		else
			if (overPoint2)
			{
				X2 = pt.x/(float)rect.right;
				Y2 = 1 - pt.y/(float)rect.bottom;
				if (X2 > 0.995f)
					X2 = 0.995f;
				if (X2 < 0.005f)
					X2 = 0.005f;
				if (Y2 > 1)
					Y2 = 1;
				if (Y2 < 0)
					Y2 = 0;
				InvalidateRect(hwnd, &rect, false);
			}
			else
				if (overPoint0)
				{
					Y0 = 1 - pt.y/(float)rect.bottom;
					if (Y0 > 1)
						Y0 = 1;
					if (Y0 < 0)
						Y0 = 0;
					InvalidateRect(hwnd, &rect, false);
				}
				else
					if (overPoint3)
					{
						Y3 = 1 - pt.y/(float)rect.bottom;
						if (Y3 > 1)
							Y3 = 1;
						if (Y3 < 0)
							Y3 = 0;
						InvalidateRect(hwnd, &rect, false);
					}
		notifyScrolling();
	}
	else
	{
		overControl = (TRUE == PtInRect(&rect, pt));
		if (overControl)
		{
			RECT rp0, rp1, rp2, rp3;
			int x,y;
			x = 2;
			y = (int)(rect.bottom * (1-Y0) );
			rp0.top = y-2;
			rp0.bottom = y+2;
			rp0.left = x-2;
			rp0.right = x+2;
			x = (int)(rect.right * X1 );
			y = (int)(rect.bottom * (1-Y1) );
			rp1.top = y-2;
			rp1.bottom = y+2;
			rp1.left = x-2;
			rp1.right = x+2;
			x = (int)(rect.right * X2 );
			y = (int)(rect.bottom * (1-Y2) );
			rp2.top = y-2;
			rp2.bottom = y+2;
			rp2.left = x-2;
			rp2.right = x+2;
			x = rect.right-3;
			y = (int)(rect.bottom * (1-Y3) );
			rp3.top = y-2;
			rp3.bottom = y+2;
			rp3.left = x-2;
			rp3.right = x+2;
			overPoint0 = (TRUE == PtInRect(&rp0, pt));
			overPoint1 = (TRUE == PtInRect(&rp1, pt));
			overPoint2 = (TRUE == PtInRect(&rp2, pt));
			overPoint3 = (TRUE == PtInRect(&rp3, pt));
			InvalidateRect(hwnd, &rect, false);
		}
		else
		{
			InvalidateRect(hwnd, &rect, false);
			ReleaseCapture();
		}
	}
}

void EMFresnel::processMouseDownCustom(short x, short y)
{
	POINT pt = {x, y};
	RECT rect;
	GetClientRect(hwnd, &rect);
	RECT rp1, rp2;
	int x1, y1;
	x1 = (int)(rect.right * X1 );
	y1 = (int)(rect.bottom * (1-Y1) );
	rp1.top = y1-2;
	rp1.bottom = y1+2;
	rp1.left = x1-2;
	rp1.right = x1+2;
	x1 = (int)(rect.right * X2 );
	y1 = (int)(rect.bottom * (1-Y2) );
	rp2.top = y1-2;
	rp2.bottom = y1+2;
	rp2.left = x1-2;
	rp2.right = x1+2;
	overPoint1 = (TRUE == PtInRect(&rp1, pt));
	overPoint2 = (TRUE == PtInRect(&rp2, pt));
	clicked = true;
}

void EMFresnel::processMouseUpCustom(short x, short y)
{
	clicked = false;
}



void EMFresnel::createNewRegions()
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	int w = rect.right;
	if (w < 2) 
		w = 2;
	int h = rect.bottom;
	POINT * pUp = new POINT[w+2];
	POINT * pDown = new POINT[w+2];
	if (pLine)
		delete [] pLine;
	pLine = new POINT[w];
	numLinePoints = w;
	int i, j;
	float val;
	float x;
	for (i=0; i<w; i++)
	{
		x = i/(float)(w-1);
		switch (mode)
		{
			case EMFresnel::MODE_CUSTOM:
				if (drawSimpleLines)
					val = (1.0f - getCustomValueSimpleLines(x))*0.994f + 0.003f;
				else
					val = (1.0f - getCustomValue(x))*0.994f + 0.003f;
				break;
			case EMFresnel::MODE_FRESNEL:
				val = (1.0f - getFresnelValue(x))*0.994f + 0.003f;
				break;
		}
		j = (int)(val*h);
		pUp[i].x = i;
		pUp[i].y = j;
		pDown[i].x = i;
		pDown[i].y = j;
		pLine[i].x = i;
		pLine[i].y = j;
	}
	pUp[w].x = w;
	pUp[w].y = 0;
	pUp[w+1].x = 0;
	pUp[w+1].y = 0;
	pDown[w].x = w;
	pDown[w].y = h;
	pDown[w+1].x = 0;
	pDown[w+1].y = h;
	if (hUp)
		DeleteObject(hUp);
	hUp = CreatePolygonRgn(pUp, w+2, WINDING);
	if (hDown)
		DeleteObject(hDown);
	hDown = CreatePolygonRgn(pDown, w+2, WINDING);
	delete [] pUp;
	delete [] pDown;
}

float EMFresnel::getCustomValueSimpleLines(float x)
{
	float cc;
	if (abs(X1 - X2) < 0.005f)
	{
		if (x < X2)
		{
			cc = x/X2;
			return (Y0*(1-cc) + Y2*cc);
		}
		else
		{
			cc = (x-X2)/(1-X2);
			return (Y2*(1-cc) + Y3*cc);
		}
	}
	if (X1 < X2)
	{
		if (x < X1)
		{	// (0,X1)
			cc = x/X1;
			return (Y0*(1-cc) + Y1*cc);
		}
		else
		{
			if (x < X2)
			{	// (X1,X2)
				cc = (x-X1)/(X2-X1);
				return (Y1*(1-cc) + Y2*cc);
			}
			else
			{	// (X2,1)
				cc = (x-X2)/(1-X2);
				return (Y2*(1-cc) + Y3*cc);
			}
		}
	}
	else
	{
		if (x < X2)
		{	// (0,X2)
			cc = x/X2;
			return (Y0*(1-cc) + Y2*cc);
		}
		else
		{
			if (x < X1)
			{	// (X2,X1)
				cc = (x-X2)/(X1-X2);
				return (Y2*(1-cc) + Y1*cc);
			}
			else
			{	// (X1,1)
				cc = (x-X1)/(1-X1);
				return (Y1*(1-cc) + Y3*cc);
			}
		}
	}
	return 0.0f;
}

float EMFresnel::getCustomValue(float x)
{
	float res;

	float X[4];
	float Y[4];
	X[0] = 0;
	X[1] = X1;
	X[2] = X2;
	X[3] = 1;
	Y[0] = Y0;
	Y[1] = Y1;
	Y[2] = Y2;
	Y[3] = Y3;

	int i,j;
	res = 0;
	float l;
	if (abs(X[1] - X[2]) < 0.005f)
	{
		for (i=0; i<4; i++)
		{
			if (i==1)
				i++;
			l = 1;
			for (j=0; j<4; j++)
			{
				if (j==1)
					j++;
				if (j == i)
					continue;
				l *= (x-X[j])/(X[i]-X[j]);
			}
			res += l*Y[i];
		}
	}
	else
	{
		for (i=0; i<4; i++)
		{
			l = 1;
			for (j=0; j<4; j++)
			{
				if (j == i)
					continue;
				l *= (x-X[j])/(X[i]-X[j]);
			}
			res += l*Y[i];
		}
	}
	if (res > 1)
		res = 1;
	if (res < 0)
		res = 0;

	return res;
}

float EMFresnel::getFresnelValue(float x)
{
	x = x*1.5707963267948966192313216916398f;	// PI/2
	float f1,f2;
	float sqrt1;
	float sin1, cos1;
	sin1 = FastMath::sin(x);
	cos1 = FastMath::cos(x);

	sqrt1 = IOR*IOR - sin1*sin1;
	if (sqrt1>0)
		sqrt1 = sqrt(sqrt1);
	else 
		sqrt1 = 0;
	f1 = (IOR*IOR*cos1 - sqrt1) / (IOR*IOR*cos1 + sqrt1);
	f2 = (cos1 - sqrt1) / (cos1 + sqrt1);
	return (f2*f2 + f1*f1)*0.5f;
}

void EMFresnel::notifyScrolling()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_VSCROLL), (LPARAM)hwnd);
}

Fresnel EMFresnel::getFresnelClassInstance()
{
	Fresnel res;
	res.IOR = IOR;
	res.X1 = X1;
	res.X2 = X2;
	res.Y0 = Y0;
	res.Y1 = Y1;
	res.Y2 = Y2;
	res.Y3 = Y3;
	res.customPolynomial = !drawSimpleLines;
	switch (mode)
	{
		case MODE_CUSTOM:
			res.mode = Fresnel::MODE_CUSTOM;
			break;
		case MODE_FRESNEL:
			res.mode = Fresnel::MODE_FRESNEL;
			break;
	}

	return res;
}
