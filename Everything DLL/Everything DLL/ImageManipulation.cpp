#include "Colors.h"
#include "EMControls.h"
#include "log.h"
#include <math.h>
#include "CameraResponse.h"

//---------------------------------------------------------------------------------------------------------------

float rgb_r55[][12] = {	// for temperature change
	{
		 6.9389923563552169e-01f, 2.7719388100974670e+03f,
		 2.0999316761104289e+07f,-4.8889434162208414e+09f,
		-1.1899785506796783e+07f,-4.7418427686099203e+04f,
		 1.0000000000000000e+00f, 3.5434394338546258e+03f,
		-5.6159353379127791e+05f, 2.7369467137870544e+08f,
		 1.6295814912940913e+08f, 4.3975072422421846e+05f
	 },{
		 9.5417426141210926e-01f, 2.2041043287098860e+03f,
		-3.0142332673634286e+06f,-3.5111986367681120e+03f,
		-5.7030969525354260e+00f, 6.1810926909962016e-01f,
		 1.0000000000000000e+00f, 1.3728609973644000e+03f,
		 1.3099184987576159e+06f,-2.1757404458816318e+03f,
		-2.3892456292510311e+00f, 8.1079012401293249e-01f
	 },{
		-7.1151622540856201e+10f, 3.3728185802339764e+16f,
		-7.9396187338868539e+19f, 2.9699115135330123e+22f,
		-9.7520399221734228e+22f,-2.9250107732225114e+20f,
		 1.0000000000000000e+00f, 1.3888666482167408e+16f,
		 2.3899765140914549e+19f, 1.4583606312383295e+23f,
		 1.9766018324502894e+22f, 2.9395068478016189e+18f
	}
};

//---------------------------------------------------------------------------------------------------------------
 
bool ImageBuffer::applyBrightness(float val)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float v = val;
			imgBuf[y][x].r += v;
			imgBuf[y][x].g += v;
			imgBuf[y][x].b += v;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyContrast(float val)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float h2 = 0.5f;
			imgBuf[y][x].r = (imgBuf[y][x].r - h2) * val + h2;
			imgBuf[y][x].g = (imgBuf[y][x].g - h2) * val + h2;
			imgBuf[y][x].b = (imgBuf[y][x].b - h2) * val + h2;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applySaturation(float val)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			// HSL
			Color4 rgb = imgBuf[y][x];
			ColorHSL hsl = rgb.toColorHSL();
			hsl.s = min(1, max(0, hsl.s+val));
			rgb = hsl.toColor4();
			imgBuf[y][x] = rgb;

		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyCurve(void * emc)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			Color4 rgb = imgBuf[y][x];
			Color4 newrgb = ((EMCurve*)emc)->processColor(rgb);
			imgBuf[y][x] = newrgb;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyGrain(float grain)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	if (grain <= 0.0001f)
		return true;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			#ifdef RAND_PER_THREAD
				float p = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
			#else
				float p = ((float)rand()/(float)RAND_MAX);
			#endif
			p = 2 * p - 1;
			p *= grain;
			p += 1.0f;
			Color4 col = imgBuf[y][x];
			col.r *= p;
			col.g *= p;
			col.b *= p;
			imgBuf[y][x] = col;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::removeBurnedDots(int radius, float thres, int maxposs)
{
	CHECK(imgBuf);
	CHECK(hitsBuf);
	CHECK(width>0);
	CHECK(height>0);
	int wh = width*height;

	unsigned int * tbuf = (unsigned int*)malloc(sizeof(unsigned int)*wh);
	if (!tbuf)
	{
		Logger::add("Can't run burned dots remover, not enough memory");
		if (tbuf)
			free(tbuf);
		return false;
	}

	unsigned int area = (radius*2+1)*(radius*2+1);

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			int x1 = max(min(width-1, x-radius), 0);
			int x2 = max(min(width-1, x+radius), 0);
			int y1 = max(min(height-1, y-radius), 0);
			int y2 = max(min(height-1, y+radius), 0);

			Color4 c = imgBuf[y][x];
			unsigned int cntr = 0;

			for (int yy=y1; yy<=y2; yy++)
			{
				for (int xx=x1; xx<=x2; xx++)
				{
					Color4 cc = imgBuf[yy][xx];
					if (		c.r > cc.r*thres	||
								c.g > cc.g*thres	||
								c.b > cc.b*thres	)
						cntr++;
				}
			}
			tbuf[y*width+x] = cntr;
		}
	}

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			unsigned int cntr = tbuf[y*width+x];
			if (cntr>area-2-maxposs)
			{
				
				imgBuf[y][x] = imgBuf[y][x-1];
			}
		}
	}

	free(tbuf);
	//free(cbuf);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyRGBCorrection(float r, float g, float b)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			imgBuf[y][x] += Color4(r,g,b);
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

void kelvinToRGB(float temperature, float *rgb)
{
	float nomin, denom;
	int channel, deg;

	for (channel = 0; channel < 3; channel++)
	{
		nomin = rgb_r55[channel][0];
		for (deg = 1; deg < 6; deg++)
		nomin = nomin * temperature + rgb_r55[channel][deg];

		denom = rgb_r55[channel][6];
		for (deg = 1; deg < 6; deg++)
		denom = denom * temperature + rgb_r55[channel][6 + deg];

		rgb[channel] = nomin / denom; 
	}   
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyTemperature(float t)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	float tempSrcRGB[3];
	float tempDstRGB[3];
	Color4 mn;
	kelvinToRGB(6000, tempSrcRGB);
	kelvinToRGB(t, tempDstRGB);
	mn.r = tempDstRGB[0] / tempSrcRGB[0];
	mn.g = tempDstRGB[1] / tempSrcRGB[1];
	mn.b = tempDstRGB[2] / tempSrcRGB[2];


	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			Color4 rgb = imgBuf[y][x];
			Color4 newrgb = rgb * mn;
			imgBuf[y][x] = newrgb;
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applySimpleVignette(int val, float aperture)
{
	if (!this->imgBuf   ||   !this->hitsBuf   ||   this->width<1   ||   this->height<1)
		return false;

	int val1 = (val<0 ? -val : val);
	if (val1 == 0)
		return true;

	float r_1 = 1.0f;
	float r_A = 1.0f/aperture;

	float mpl1[1501];
	for (int i=0; i<=1500; i++)
	{
		float d2 = i/1000.0f * val1/50.0f;
		
		if (d2 <= r_1+r_A)
		{
			float kos1 = (r_A*r_A + d2*d2 - r_1*r_1) / (2*d2*r_A);
			float kos2 = (r_1*r_1 + d2*d2 - r_A*r_A) / (2*d2*r_1);
			float t1 = kos1*r_A;
			float t2 = kos2*r_1;

			float p1_k = (acos(kos1) * r_A*r_A);
			float p1_t = t1*sqrt(r_A*r_A - t1*t1);
			float p1 = p1_k - p1_t;
			float p2_k = (acos(kos2) * r_1*r_1);
			float p2_t = t2*sqrt(r_1*r_1 - t2*t2);
			float p2 = p2_k - p2_t;

			mpl1[i] = max(0.0f, p1+p2)/PI;
		}
		else
			mpl1[i] = 0.0f;
	}
	mpl1[0] = 1.0f;

	int owidth = max(width+vig_buck_pos_x, vig_orig_width);
	int oheight = max(height+vig_buck_pos_y, vig_orig_height);
	int wym = max(owidth, oheight);
	for (int y=0; y<height; y++)
	{
		float yy = (y-oheight/2+vig_buck_pos_y) / (float)wym;
		for (int x=0; x<width; x++)
		{
			float xx = (x-owidth/2+vig_buck_pos_x) / (float)wym;
			float dist2 = xx*xx + yy*yy;
			float dist = sqrt(dist2);
			int dd = (int)(2*dist*1000);
			dd = min(max(dd, 0), 1500);

			imgBuf[y][x] *= mpl1[dd];
		}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyISOToneGamma(float mpl, float tone, float gamma)
{
	if (!imgBuf)
		return false;
	int w = width;
	int h = height;
	if (w < 1   ||   h < 1)
		return false;
	int x,y;
	Color4 d;
	float g = 1.0f/gamma;
	bool useTone= ( tone  != 0.0f );
	bool useGamma   = ( gamma != 1.0f );
	for (y=0; y<h; y++)
	{
		for (x=0; x<w; x++)
		{
			d = imgBuf[y][x] * mpl;
			if (useTone)
			{
				d.r = d.r / pow(1 + d.r, tone);
				d.g = d.g / pow(1 + d.g, tone);
				d.b = d.b / pow(1 + d.b, tone);
			}
			if (useGamma)
			{
				d.r = pow(d.r, g);
				d.g = pow(d.g, g);
				d.b = pow(d.b, g);
			}
			imgBuf[y][x] = d;
		}
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::applyCameraResponseFunction(int numberResp)
{

	ImageBuffer * res = new ImageBuffer();
	CHECK(res);
	float allOK = res->allocBuffer(width,height);
	CHECK(allOK);

	if (numberResp<1 || numberResp>90)
	{
		for (int y=0; y<height; y++)
		{
			memcpy(res->imgBuf[y], imgBuf[y], width*sizeof(Color4));
		}
		return res;
	}

	float * responseRed   = getResponseFunctionRed(numberResp);
	float * responseGreen = getResponseFunctionGreen(numberResp);
	float * responseBlue  = getResponseFunctionBlue(numberResp);

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			Color4 src = imgBuf[y][x];
			int iir = min(1023 , max(0, (int)(src.r*1023)));
			int iig = min(1023 , max(0, (int)(src.g*1023)));
			int iib = min(1023 , max(0, (int)(src.b*1023)));
			Color4 dst = Color4(responseRed[iir], responseGreen[iig], responseBlue[iib]);
			res->imgBuf[y][x] = dst;
		}
	}

	return res;
}

//---------------------------------------------------------------------------------------------------------------

ImageBuffer * ImageBuffer::evalChromaticAberrationPlanar(float valR, float valG, float valB)
{
	if (valR<0.001f   &&   valG<0.001f   &&   valB<0.001f)
		return false;
	ImageBuffer * res = new ImageBuffer();
	CHECK(res);
	bool allok = res->allocBuffer(width, height);
	CHECK(allok);

	float cx = width/2.0f;
	float cy = height/2.0f;
	float tR = 1.0f - min(1.0f, max(0.0f, valR))*0.01f;
	float tG = 1.0f - min(1.0f, max(0.0f, valG))*0.01f;
	float tB = 1.0f - min(1.0f, max(0.0f, valB))*0.01f;

	for (int y=0; y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			float ovx = x-cx;
			float ovy = y-cy;
			float dxr = ovx*tR+cx;
			float dyr = ovy*tR+cy;
			float dxg = ovx*tG+cx;
			float dyg = ovy*tG+cy;
			float dxb = ovx*tB+cx;
			float dyb = ovy*tB+cy;

			float fdxr = floor(dxr);
			float fdyr = floor(dyr);
			float fdxg = floor(dxg);
			float fdyg = floor(dyg);
			float fdxb = floor(dxb);
			float fdyb = floor(dyb);

			int idxr = (int)fdxr;
			int idyr = (int)fdyr;
			int idxg = (int)fdxg;
			int idyg = (int)fdyg;
			int idxb = (int)fdxb;
			int idyb = (int)fdyb;

			float wxr = dxr-fdxr;
			float wyr = dyr-fdyr;
			float wxg = dxg-fdxg;
			float wyg = dyg-fdyg;
			float wxb = dxb-fdxb;
			float wyb = dyb-fdyb;

			float cr =	(1-wxr)*(1-wyr) * (imgBuf[idyr][idxr].r) + 
						(1-wxr)*(  wyr) * (imgBuf[min(height-1,idyr+1)][idxr].r) + 
						(  wxr)*(1-wyr) * (imgBuf[idyr]					[min(width-1,idxr+1)].r) + 
						(  wxr)*(  wyr) * (imgBuf[min(height-1,idyr+1)]	[min(width-1,idxr+1)].r);
			float cg =	(1-wxg)*(1-wyg) * (imgBuf[idyg][idxg].g) + 
						(1-wxg)*(  wyg) * (imgBuf[min(height-1,idyg+1)][idxg].g) + 
						(  wxg)*(1-wyg) * (imgBuf[idyg]					[min(width-1,idxg+1)].g) + 
						(  wxg)*(  wyg) * (imgBuf[min(height-1,idyg+1)]	[min(width-1,idxg+1)].g);
			float cb =	(1-wxb)*(1-wyb) * (imgBuf[idyb][idxb].b) + 
						(1-wxb)*(  wyb) * (imgBuf[min(height-1,idyb+1)]	[idxb].b) + 
						(  wxb)*(1-wyb) * (imgBuf[idyb]					[min(width-1,idxb+1)].b) + 
						(  wxb)*(  wyb) * (imgBuf[min(height-1,idyb+1)]	[min(width-1,idxb+1)].b);

			res->imgBuf[y][x] = Color4(cr,cg,cb);
		}
	}

	res->setHitsValues(1);
	return res;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyFog(FloatBuffer * depthmap, float dist1, float dist2, Color4 &att1, Color4 &att2, int func, float speed, bool exclSky)
{
	if (!imgBuf)
		return false;
	if (width<1  ||  height<1)
		return false;
	if (!depthmap)
		return false;
	if (!depthmap->fbuf)
		return false;

	if (depthmap->width!=width || depthmap->height!=height)
	{
		MessageBox(0, "depth vs buffer diff size", "", 0);
		return false;
	}

	if (func==0)
	{
		if (dist1<=0.0f)
			return false;

		for (int y=0; y<height; y++)
			for (int x=0; x<width; x++)
			{
				Color4 src = imgBuf[y][x];
				int nd = depthmap->hbuf[y][x];
				float depth = nd ? depthmap->fbuf[y][x]/nd : 0;
				if (exclSky  &&  depth==BIGFLOAT)
					continue;
				depth = max(0, min(depth, dist1));
				float ll = depth/dist1;
				ll = pow(ll, speed);
				Color4 dst = att1*ll + src*(1-ll);
				imgBuf[y][x] = dst;
			}
	}

	if (func==1)
	{
		for (int y=0; y<height; y++)
			for (int x=0; x<width; x++)
			{
				Color4 src = imgBuf[y][x];
				int nd = depthmap->hbuf[y][x];
				float depth = nd ? depthmap->fbuf[y][x]/nd : 0;
				depth = max(0, min(depth, dist1));
				float ll = depth/dist1;
				Color4 dst = att1*ll + src*(1-ll);
				imgBuf[y][x] = dst;
			}
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ImageBuffer::applyAerialPerspective(FloatBuffer * depthmap, float scale, float enMpl, Camera * cam, SunSky * sunsky, bool exclSky)
{
	if (!imgBuf)
		return false;
	if (width<1  ||  height<1)
		return false;
	if (!depthmap)
		return false;
	if (!depthmap->fbuf)
		return false;
	if (!cam)
		return false;
	if (!sunsky)
		return false;
	if (depthmap->width!=width || depthmap->height!=height)
	{
		MessageBox(0, "depth vs buffer diff size", "", 0);
		return false;
	}

	float cmpl = cam->mplApSh;

	int TS=20;
	int th = (height+TS-1)/TS;
	int tw = (width+TS-1)/TS;
	for (int ty=0; ty<th; ty++)
		for (int tx=0; tx<tw; tx++)
		{
			Point3d pp;
			Vector3d dir11 = cam->getDirection((float)(tx+0)*TS, (float)(ty+0)*TS, pp, 0);
			Vector3d dir12 = cam->getDirection((float)(tx+0)*TS, (float)(ty+1)*TS, pp, 0);
			Vector3d dir21 = cam->getDirection((float)(tx+1)*TS, (float)(ty+0)*TS, pp, 0);
			Vector3d dir22 = cam->getDirection((float)(tx+1)*TS, (float)(ty+1)*TS, pp, 0);

			Color4 skycol11 = Color4(0,0,0);
			Color4 skycol12 = Color4(0,0,0);
			Color4 skycol21 = Color4(0,0,0);
			Color4 skycol22 = Color4(0,0,0);
			int XXP = 6;
			int YYP = 5;
			for (int yy=0; yy<YYP; yy++)
				for (int xx=0; xx<6; xx++)
				{
					float rx = xx*PI*2/XXP;
					float ry = (yy+0.5f)*PI/YYP;
					Vector3d pdir;
					pdir.x = sin(rx) * sin(ry);
					pdir.y = cos(ry);
					pdir.z = cos(rx) * sin(ry);
					Color4 sc = sunsky->getColor(pdir);

					float c11 = pdir*dir11;
					float c12 = pdir*dir12;
					float c21 = pdir*dir21;
					float c22 = pdir*dir22;

					float g = -0.6f;
					float wr11 = 0.75f*(1+c11*c11)*(1/PI);
					float wr12 = 0.75f*(1+c12*c12)*(1/PI);
					float wr21 = 0.75f*(1+c21*c21)*(1/PI);
					float wr22 = 0.75f*(1+c22*c22)*(1/PI);
					float wm11 = (1-g*g)/( 4*PI* pow(1+g*g-2*g*c11, 1.5f));
					float wm12 = (1-g*g)/( 4*PI* pow(1+g*g-2*g*c12, 1.5f));
					float wm21 = (1-g*g)/( 4*PI* pow(1+g*g-2*g*c21, 1.5f));
					float wm22 = (1-g*g)/( 4*PI* pow(1+g*g-2*g*c22, 1.5f));
					skycol11 += sc*(wr11+wm11);
					skycol12 += sc*(wr12+wm12);
					skycol21 += sc*(wr21+wm21);
					skycol22 += sc*(wr22+wm22);
				}
			skycol11 *= 1.0f/XXP/YYP;
			skycol12 *= 1.0f/XXP/YYP;
			skycol21 *= 1.0f/XXP/YYP;
			skycol22 *= 1.0f/XXP/YYP;

			for (int ity=0; ity<TS; ity++)
			{
				int y = ty*TS+ity;
				if (y>=height)
					continue;
				for (int itx=0; itx<TS; itx++)
				{
					int x = tx*TS+itx;
					if (x>=width)
						continue;

					Color4 src = imgBuf[y][x];
					int nd = depthmap->hbuf[y][x];
					float depth = nd ? depthmap->fbuf[y][x]/nd : 0;
					if (exclSky  &&  depth==BIGFLOAT)
						continue;
					depth *= scale;

					Point3d pp;
					Vector3d dir = cam->getDirection((float)x, (float)y, pp, 0);

					Color4 skyIn, sunIn, att;
					bool ok = sunsky->evalAerialPerspective(dir, depth, att, skyIn, sunIn);
					
					float w11 = (TS-itx)*(TS-ity)/(float)(TS*TS);
					float w12 = (TS-itx)*(ity)/(float)(TS*TS);
					float w21 = (itx)*(TS-ity)/(float)(TS*TS);
					float w22 = (itx)*(ity)/(float)(TS*TS);
					Color4 skycol = skycol11*w11 + skycol12*w12 + skycol21*w21 + skycol22*w22;
					skyIn.r = skycol.r * (1-att.r);
					skyIn.g = skycol.g * (1-att.g);
					skyIn.b = skycol.b * (1-att.b);
					Color4 dst = src;
					if (ok)
						dst = src*att + skyIn*cmpl + sunIn*cmpl;

					imgBuf[y][x] = dst;

				}
			}
		}
	
	return true;
}

//---------------------------------------------------------------------------------------------------------------
