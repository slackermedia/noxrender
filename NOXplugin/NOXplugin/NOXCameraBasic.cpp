#include "max2013predef.h"
#include "NOXCamera.h"
#include <decomp.h>
#include "noxdebug.h"

extern NOXCameraCreateMouseCB mccmcb;
INode* FindNodeRef(ReferenceTarget *rt);

//-----------------------------------------------------------------------------------------------------------------------

NOXCamera::NOXCamera() 
{
	ADD_LOG_CONSTR("NOXCamera constructor");
	tDist = 100;
	horzLineState = 0;
	envDisplay = false;
	displayFlags = 0;

	coneState = 1;
	focusConnectedToTarget = true;
	cntrl = NULL;
	focusLength = 0;
	trCam.setActive(true);

	createMesh();
}

NOXCamera::~NOXCamera() 
{
	ADD_LOG_CONSTR("NOXCamera destructor");
	mccmcb.cam = NULL;
	DeleteAllRefsFromMe();
}

//-----------------------------------------------------------------------------------------------------------------------

NOXCamera * NOXCamera::NewCamera(int type)
{
	ADD_LOG_PARTS_MAIN("NOXCamera::NewCamera");
	return new NOXCamera();
}

//-----------------------------------------------------------------------------------------------------------------------

RefTargetHandle NOXCamera::Clone( RemapDir &remap )
{
	ADD_LOG_PARTS_MAIN("NOXCamera::Clone");
	NOXCamera * newob = new NOXCamera();
	etarg = new NOXTargetObject();
	
	newob->tDist = tDist;
	newob->horzLineState = horzLineState;
	newob->envDisplay = envDisplay;

	newob->displayFlags = displayFlags;
	newob->coneState = coneState;
	newob->trCam.setActive(false);

	BaseClone(this, newob, remap);

	newob->trCam.copySettingsFrom(&trCam);

	ADD_LOG_PARTS_MAIN("NOXCamera::Clone done");

	return newob;
}

//-----------------------------------------------------------------------------------------------------------------------

ObjectState NOXCamera::Eval(TimeValue t)
{
	return ObjectState(this);
}

//-----------------------------------------------------------------------------------------------------------------------

RefResult NOXCamera::EvalCameraState(TimeValue time, Interval& valid, CameraState* cs)
{
	cs->isOrtho = IsOrtho();	
	cs->fov = GetFOV(time,valid);
	cs->tdist = GetTDist(time,valid);
	cs->horzLine = horzLineState;
	cs->manualClip = GetManualClip();
	cs->hither = GetClipDist(time, CAM_HITHER_CLIP, valid);
	cs->yon = GetClipDist(time, CAM_YON_CLIP, valid);
	cs->nearRange = GetEnvRange(time, ENV_NEAR_RANGE, valid);
	cs->farRange = GetEnvRange(time, ENV_FAR_RANGE, valid);

	return REF_SUCCEED;
}

//-----------------------------------------------------------------------------------------------------------------------

