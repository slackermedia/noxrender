#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

//#define DEBUG_LOG2

//--------------------------------------------------------------------------------------------------------------------------------------

bool Metropolis::perturbateLens()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;
	CHECK(scene);

	float r = (float)(getMarsaglia()/(float)(unsigned int)MAXMARS);
	float phi = r * 2 * PI;			// 0 - 2PI
	r = (float)(getMarsaglia()/(float)(unsigned int)MAXMARS);
	float tempplR1 = plR1;
	if (sysRej > 0)
		tempplR1 = plR1 / (float)sysRej;
	tempplR1 = max(1.4f, tempplR1);
	float dist = plR2 * exp(-log(plR2/tempplR1)*r);

	// check if new pixel location is valid
	float sX = dist * sin(phi);
	float sY = dist * cos(phi);
	pCX += sX;
	pCY += sY;
	if (pCX<0  ||  pCY<0   ||   pCX>=cam->width*cam->aa   ||   pCY>=cam->height*cam->aa)
		return false;

	MLTVertex * vrt1 = NULL;
	MLTVertex * vrt2 = NULL;

	bool got2Diff = false;
	bool hitLight = false;
	cInd = 0;

	// analyze current path.... 
	for (int i=1; i<=numEP; i++)
	{
		cInd++;
		vrt1 = &(ePath[i]);
		if (i==numEP)
			vrt2 = &(lPath[numLP]);
		else
			vrt2 = &(ePath[i+1]);

		MatLayer * mlay1 = vrt1->mlay;
		MatLayer * mlay2 = vrt2->mlay;

		if (!mlay1)
			return false;

		if (!mlay2)
		{
			if (i==numEP  &&  (sourceSky  ||  sourceSun))
			{
				got2Diff = true;
				break;
			}
			else
				return false;
		}

		float rough1 = vrt1->rough;
		float rough2 = vrt2->rough;
		if (rough1 > 0)
		{
			if (rough2 > 0   ||   mlay2->type == MatLayer::TYPE_EMITTER)
			{
				got2Diff = true;
				break;			// ES*D(L|D)
			}
			return false;		// ES*DS... path not allowed
		}
	}

	if (!got2Diff)		// ES* path so far... continue searching
	{

		return false;
		
		for (int i=numLP; i>0; i--)
		{
			cInd++;
			vrt1 = &(lPath[i]);
			vrt2 = &(lPath[i-1]);
			MatLayer * mlay1 = vrt1->mlay;
			MatLayer * mlay2 = vrt2->mlay;

			if (!mlay1  ||  !mlay2)
				return false;

			float rough1 = vrt1->rough;
			float rough2 = vrt2->rough;

			if (rough1 > 0)
			{
				if (rough2 > 0   ||   mlay2->type == MatLayer::TYPE_EMITTER)
				{
					got2Diff = true;
					break;			// ES*D(L|D)
				}
				return false;		// ES*DS... path not allowed
			}
		}
	}

	if (!got2Diff   &&   !hitLight)		// no ES*D(L|D) nor ES*L path found
		return false;

	//--------------------------------------------------------------
	// everything checked ... perturbation is possible
	
	Point3d sPos;
	Vector3d eyeDirection = cam->getDirection(pCX,pCY, sPos, 0);

	// fill dirs... rest unchanged
	eCandidate[0].inDir = eCandidate[0].normal = eCandidate[0].outDir = eyeDirection;
	eCandidate[0].pos = sPos;
	Vector3d cDir = eyeDirection;
	Point3d cPoint = sPos;
	MLTVertex * curPath = NULL;
	MLTVertex * origPath = NULL;

	for (int i=1; i<=cInd; i++)
	{
		if (i<=numEP)
		{
			curPath = &(eCandidate[i]);
			origPath = &(ePath[i]);
		}
		else
		{
			curPath = &(lCandidate[numLP-(i-numEP-1)]);
			origPath = &(lPath[numLP-(i-numEP-1)]);
		}

		// send a ray
		float u=0, v=0;
		int chosen = -1;
		float bigf = BIGFLOAT;

		Color4 att = Color4(1,1,1);
		float d = rtr->curScenePtr->intersectBSPforNonOpacFakeGlass(cPoint, cDir, bigf, chosen, u, v, att, ss, true);
		if (chosen < 0   ||   chosen >= scene->triangles.objCount   ||   d < 0.0001f)		// not less than 0.1mm
			return false;	// nothing intersected

		if (i<=numEP)
			eCandidate[i].atten = att;
		if (i==numEP+1)
			connectionAttenuationCandidate = att;
		if (i > numEP+1)
			lCandidate[numLP-(i-numEP)].atten = att;
		
		Triangle * tri  = &(scene->triangles[chosen]);
		MaterialNox * mat  = (scene->mats[tri->matNum]);
		MatLayer * mlay = NULL;

		// partly fill current vertex
		float tu,tv;
		tri->evalTexUV(u,v, tu, tv);
		curPath->normal = tri->evalNormal(u,v);
		curPath->u = u;
		curPath->v = v;
		curPath->mat = mat;
		curPath->mlay = mlay;
		curPath->pos = tri->getPointFromUV(u,v);
		curPath->tri = tri;

		// compare materials and get matlayer
		if (mat == origPath->mat)
		{
			mlay = origPath->mlay;
			
		}
		else
		{	// other material
			float mlpdf;
			mlay = mat->getRandomShadeLayer(tu,tv, mlpdf);
			CHECK(mlay);
		}
		curPath->mlay = mlay;
		if (!curPath->fetchRoughness())
			curPath->rough = 1.0f;

		float rough = curPath->rough;
		float rough_orig = origPath->rough;

		if (rough > maxCausticRough  &&  rough_orig <= maxCausticRough)
			return false;
		if (rough <= maxCausticRough  &&  rough_orig > maxCausticRough)
			return false;
		if (origPath->mlay->type == MatLayer::TYPE_EMITTER)		// don't think it would happen anyway
			return false;

		if (i<=numEP)
		{
			curPath->inDir = cDir * -1;
			curPath->inCosine = fabs(cDir*curPath->normal);//*-1;
		}
		else
		{
			curPath->outDir = cDir * -1;
			curPath->outCosine = fabs(cDir*curPath->normal);//*-1;
		}

		float pdf;
		HitData hd;
		hd.in = cDir * -1;
		hd.normal_shade = curPath->normal;
		hd.tU = tu;
		hd.tV = tv;
		if (curPath->tri)
		{
			hd.normal_geom = curPath->tri->normal_geom;
			hd.adjustNormal();
		}

		Vector3d oldDir = ( (i<=numEP) ? origPath->outDir : origPath->inDir );
		float oldCos = origPath->normal * oldDir;
		bool gotValidDir = false;
		int nTry = 0;
		while (!gotValidDir)
		{
			hd.clearFlagInvalidRandom();
			cDir = mlay->randomNewDirection(hd, pdf);
			float newCos = cDir * curPath->normal;
			if (newCos*oldCos > 0   &&   !hd.isFlagInvalidRandom())
				gotValidDir = true;
			if (nTry++ > 50)
				return false;
		}

		hd.out = cDir;
		float ccos = curPath->normal * cDir;

		if (i<=numEP)
		{
			curPath->outDir = cDir;
			curPath->outCosine = curPath->normal * cDir;
		}
		else
		{
			curPath->inDir = cDir;
			curPath->inCosine = curPath->normal * cDir;
		}

		if (fabs(curPath->inCosine) < 0.0001f   ||   fabs(curPath->outCosine) < 0.0001f)
			return false;

		if (ccos < 0)
			cPoint = curPath->pos + curPath->normal * -0.0001f;
		else
			cPoint = curPath->pos + curPath->normal * 0.0001f;
		
		curPath->brdf = mlay->getBRDF(hd);
	}

	// check visibility and correct dir
	MLTVertex * v1 = NULL;
	MLTVertex * v2 = NULL;

	if (cInd > numEP)
		v1 = &(lCandidate[numLP-(cInd-1-numEP)]);
	else
		v1 = &(eCandidate[cInd]);

	if (cInd+1 > numEP)
		v2 = &(lCandidate[numLP-(cInd-numEP)]);
	else	
		v2 = &(eCandidate[cInd+1]);

	Vector3d ddir;
	ddir = v2->pos - v1->pos;
	ddir.normalize();

	if (cInd > numEP)
	{
		v1->inDir = ddir;
		v1->inCosine = v1->inDir * v1->normal;
	}
	else
	{
		v1->outDir = ddir;
		v1->outCosine = v1->outDir * v1->normal;
	}

	if (cInd+1 > numEP)
	{
		v2->outDir = ddir*-1;
		v2->outCosine = v2->outDir * v2->normal;
	}
	else	
	{
		v2->inDir = ddir*-1;
		v2->inCosine = v2->inDir * v2->normal;
	}

	Color4 visCol = visibilityTestFakeGlassAndOpacity(v1,v2);

	if (cInd<=numCEP)
		eCandidate[cInd].atten = visCol;
	if (cInd==numCEP+1)
		connectionAttenuationCandidate = visCol;
	if (cInd > numEP+1)
		lCandidate[numCLP-(cInd-numCEP)].atten = visCol;

	bool visOK = !visCol.isBlack();

	return visOK;
}

//--------------------------------------------------------------------------------------------------------------------------------------

float Metropolis::tentativeTransitionForPertLens(bool forward)
{	
	MLTVertex * v1 = NULL;
	MLTVertex * v2 = NULL;
	MLTVertex * lCur = NULL;
	MLTVertex * eCur = NULL;
	bool useSun = forward ? cSourceSun : sourceSun;
	bool useSky = forward ? cSourceSky : sourceSky;

	if (forward)
	{
		if (cInd > numEP)
			v1 = &(lCandidate[numLP-(cInd-1-numEP)]);
		else
			v1 = &(eCandidate[cInd]);

		if (cInd+1 > numEP)
			v2 = &(lCandidate[numLP-(cInd-numEP)]);
		else
			v2 = &(eCandidate[cInd+1]);
		lCur = lCandidate;
		eCur = eCandidate;
	}
	else
	{
		if (cInd > numEP)
			v1 = &(lPath[numLP-(cInd-1-numEP)]);
		else
			v1 = &(ePath[cInd]);

		if (cInd+1 > numEP)
			v2 = &(lPath[numLP-(cInd-numEP)]);
		else
			v2 = &(ePath[cInd+1]);
		lCur = lPath;
		eCur = ePath;
	}

	Vector3d dirEL = v2->pos - v1->pos;
	float d2 = dirEL.normalize_return_old_length();
	if (d2<0.0001f)
		return 0;
	d2 = d2*d2;
	Vector3d dirLE = dirEL*-1;
	float cosEL = fabs(dirEL*v1->normal);
	float cosLE = fabs(dirLE*v2->normal);

	if (cosEL < 0.001f    ||   cosLE < 0.001f)
		return 0;


	float den = 1.0f;
	Color4 nom = Color4(1,1,1);

	// denominator first... pdf on all spec refl
	for (int i=1; i<cInd; i++)
	{
		HitData hd;
		float u = eCur[i].u;
		float v = eCur[i].v;
		hd.in = eCur[i].inDir;
		hd.out = eCur[i].outDir;
		hd.normal_shade = eCur[i].normal;
		Triangle * tri  = eCur[i].tri;
		MatLayer * mlay = eCur[i].mlay;
		CHECK(tri);
		CHECK(mlay);
		tri->evalTexUV(u,v, hd.tU, hd.tV);
		hd.normal_geom = tri->normal_geom;
		hd.adjustNormal();	// done
		float pdf = mlay->getProbability(hd);
		if (pdf < 0.001f)
			return 0;

		float rough = eCur[i].rough;
		if (rough > 0)
		{
			float tcos = fabs(hd.out*hd.normal_shade);
			if (tcos<0.001f)
				return 0.0f;
			den *= pdf / tcos;
		}
		else
			den *= pdf;
	}

	if (cInd > numEP)
		Logger::add("whhhaaattt?");

	if (cInd==numEP  &&  (useSky || useSun))
	{
		d2 = 1;
		cosLE = 1;
	}

	// nominator
	float G = cosEL*cosLE/d2;
	if (cInd==numEP)
	{
		HitData hd;
		float u = lCur[numLP].u;
		float v = lCur[numLP].v;
		hd.in = lCur[numLP].inDir;
		hd.out = lCur[numLP].outDir;
		hd.normal_shade = lCur[numLP].normal;
		Triangle * tri  = lCur[numLP].tri;
		MatLayer * mlay = lCur[numLP].mlay;

		Color4 brdf;
		if (tri  &&  mlay)
		{
			hd.normal_geom = tri->normal_geom;
			hd.adjustNormal();	// done
			tri->evalTexUV(u,v, hd.tU, hd.tV);
			if (mlay->type == MatLayer::TYPE_EMITTER)
				brdf = mlay->getLightColor(hd.tU, hd.tV) * mlay->getIlluminance(ss, lTriID);
			else
				brdf = mlay->getBRDF(hd);
		}
		else
		{
			if (useSky || useSun)
			{
				if (useSky)
					brdf = ss->sunsky->getColor(eCur[numEP].outDir);
				if (useSun)
					brdf = ss->sunsky->getSunColor();
			}
			else
				return 0;
		}
		if (brdf.isBlack()  ||  brdf.isInf()  ||  brdf.isNaN())
			return 0;	
		nom *= brdf;
	}
	else
	{
		HitData hd;
		float u = eCur[cInd+1].u;
		float v = eCur[cInd+1].v;
		hd.in = eCur[cInd+1].outDir;	// swap in/out for L->P light dir
		hd.out = eCur[cInd+1].inDir;
		hd.normal_shade = eCur[cInd+1].normal;
		Triangle * tri  = eCur[cInd+1].tri;
		MatLayer * mlay = eCur[cInd+1].mlay;
		CHECK(tri);
		CHECK(mlay);
		tri->evalTexUV(u,v, hd.tU, hd.tV);
		hd.normal_geom = tri->normal_geom;
		hd.adjustNormal();	
		Color4 brdf = mlay->getBRDF(hd);
		if (brdf.isBlack()  ||  brdf.isInf()  ||  brdf.isNaN())
			return 0;
		nom *= brdf;
	}

	for (int i=cInd; i>0; i--)
	{
		HitData hd;
		float u = eCur[i].u;
		float v = eCur[i].v;
		hd.in = eCur[i].outDir;		// swap in/out for L->P light dir
		hd.out = eCur[i].inDir;
		hd.normal_shade = eCur[i].normal;
		Triangle * tri  = eCur[i].tri;
		MatLayer * mlay = eCur[i].mlay;
		CHECK(tri);
		CHECK(mlay);
		tri->evalTexUV(u,v, hd.tU, hd.tV);
		hd.normal_geom = tri->normal_geom;
		hd.adjustNormal();
		Color4 brdf = mlay->getBRDF(hd);
		if (brdf.isBlack()  ||  brdf.isInf()  ||  brdf.isNaN())
			return 0;
		nom *= brdf;
	}
	nom *= G;

	// fake glass and opacity stuff
	Color4 fakeAndOpacityAttenuation = Color4(1,1,1);
	if (forward)
	{
		fakeAndOpacityAttenuation = connectionAttenuationCandidate;
		for (int i=1; i<=numCEP; i++)
			fakeAndOpacityAttenuation *= eCandidate[i].atten;
		for (int i=1; i<=numCLP; i++)
			fakeAndOpacityAttenuation *= lCandidate[i].atten;
	}
	else
	{
		fakeAndOpacityAttenuation = connectionAttenuation;
		for (int i=1; i<=numEP; i++)
			fakeAndOpacityAttenuation *= ePath[i].atten;
		for (int i=1; i<=numLP; i++)
			fakeAndOpacityAttenuation *= lPath[i].atten;
	}

	nom *= fakeAndOpacityAttenuation;


	return (0.299f * nom.r  +  0.587f * nom.g  +  0.114f * nom.b)/den;
}

//--------------------------------------------------------------------------------------------------------------------------------------

