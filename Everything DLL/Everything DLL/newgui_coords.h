#ifndef __new_gui_coordinates_h__
#define __new_gui_coordinates_h__


// show/hide panels arrow button
#define MBTN_W 15
#define MBTN_H 37
#define MBTN_RN_X 10
#define MBTN_RN_Y 60
#define MBTN_RM_X 30
#define MBTN_RM_Y 60
#define MBTN_RC_X 50
#define MBTN_RC_Y 60
#define MBTN_RD_X 70
#define MBTN_RD_Y 60
#define MBTN_LN_X 10
#define MBTN_LN_Y 100
#define MBTN_LM_X 30
#define MBTN_LM_Y 100
#define MBTN_LC_X 50
#define MBTN_LC_Y 100
#define MBTN_LD_X 70
#define MBTN_LD_Y 100


// show/hide regions arrow button
#define RBTN_W 15
#define RBTN_H 37
#define RBTN_RN_X 90
#define RBTN_RN_Y 60
#define RBTN_RM_X 110
#define RBTN_RM_Y 60
#define RBTN_RC_X 130
#define RBTN_RC_Y 60
#define RBTN_RD_X 150
#define RBTN_RD_Y 60
#define RBTN_LN_X 90
#define RBTN_LN_Y 100
#define RBTN_LM_X 110
#define RBTN_LM_Y 100
#define RBTN_LC_X 130
#define RBTN_LC_Y 100
#define RBTN_LD_X 150
#define RBTN_LD_Y 100


// temperature button image
#define BTN_TEMP_W 14
#define BTN_TEMP_H 14
#define BTN_TEMP_N_X 90
#define BTN_TEMP_N_Y 10
#define BTN_TEMP_M_X 110
#define BTN_TEMP_M_Y 10
#define BTN_TEMP_C_X 110
#define BTN_TEMP_C_Y 10
#define BTN_TEMP_D_X 150
#define BTN_TEMP_D_Y 10


#endif