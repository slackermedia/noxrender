#include "FastMath.h"
#include <math.h>

float FastMath::sinArray[];
float FastMath::cosArray[];
float FastMath::tanArray[];
float FastMath::ctanArray[];
float FastMath::asinArray[];
float FastMath::acosArray[];

bool FastMath::initialized = false;

	
FastMath::FastMath()
{
	initialize();
}

FastMath::~FastMath()
{
}

void FastMath::initialize()
{
	if (initialized)
		return;

	int i;
	for (i=0; i<ARRAY_ANGLES_COUNT; i++)
	{
		sinArray[i] = ::sin((float)(i/100.0f));
		cosArray[i] = ::cos((float)(i/100.0f));
		tanArray[i] = ::tan((float)(i/100.0f));
		if (float(tanArray[i] < 0.000001f))
		{
			if (tanArray[i] >= 0)
				ctanArray[i] = BIGFLOAT;
			else
				ctanArray[i] = -BIGFLOAT;
		}
		else
			ctanArray[i] = 1.0f/tanArray[i];
	}

	for (i=0; i<ARRAY_ARC_COUNT; i++)
	{
		asinArray[i] = ::asin((float)((i-100)/100.0f));
		acosArray[i] = ::acos((float)((i-100)/100.0f));
	}

	initialized = true;
}

float FastMath::sin(float x)
{
	if (x < 0)
	{
		int c = (int)((-x)/TWOPI);
		x = x + (c+1)*TWOPI;
	}

	if (x > TWOPI)
	{
		int c = (int)(x/TWOPI);
		x = x - c*TWOPI;
	}

	int c = (int)(x*100.0f);
	float r = x*100.0f - c;
	
	float res = sinArray[c]*(1-r) + sinArray[c+1]*r;

	return res;
}

float FastMath::cos(float x)
{
	if (x < 0)
	{
		int c = (int)((-x)/TWOPI);
		x = x + (c+1)*TWOPI;
	}

	if (x > TWOPI)
	{
		int c = (int)(x/TWOPI);
		x = x - c*TWOPI;
	}

	int c = (int)(x*100.0f);
	float r = x*100.0f - c;
	
	float res = cosArray[c]*(1-r) + cosArray[c+1]*r;

	return res;
}

float FastMath::tan(float x)
{
	return ::tan(x);
}

float FastMath::cot(float x)
{
	float t = ::tan(x);
	if (fabs(t) > 0.00001f)
		return 1/t;
	else
	{
		if (t>0)
			return BIGFLOAT;
		else
			return -BIGFLOAT;
	}
}

float FastMath::asin(float x)
{
	if (x<-1 || x>1)
		return ::asin(x);
	float x1 = x*100.0f + 100.0f;

	int c = (int)(x1);
	float r = x1 - c;
	
	float res = asinArray[c]*(1-r) + asinArray[c+1]*r;

	return res;
}

float FastMath::acos(float x)
{
	if (x<-1 || x>1)
		return ::acos(x);
	float x1 = x*100.0f + 100.0f;

	int c = (int)(x1);
	float r = x1 - c;
	
	float res = acosArray[c]*(1-r) + acosArray[c+1]*r;

	return res;
}


