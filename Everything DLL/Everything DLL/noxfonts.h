#ifndef __NOX_FONTS_H__
#define __NOX_FONTS_H__

// fonts manager for new gui (from v. 0.40)

#include <windows.h>

//------------------------------------------------------------------------------------------------

class NOXFontManager
{
public:
	HFONT opensans8;
	HFONT opensans9;
	HFONT opensans10;
	HFONT opensans11;
	HFONT opensans12;

	HFONT em2text;
	HFONT em2editspin;
	HFONT em2edittrack;
	HFONT em2editsimpletext;
	HFONT em2editsimpleedit;
	HFONT em2paneltitle;
	HFONT em2checkbox;
	HFONT em2groupbar;
	HFONT em2button;
	HFONT em2combomain;
	HFONT em2combounwrap;
	HFONT em2tabs;
	HFONT em2listentry;
	HFONT em2listheader;
	HFONT em2topbuttons;
	HFONT em2matprops;


	HFONT loadFont(char * name, int size);
	bool loadAllFonts();
	bool deleteAllFonts();
	bool logFonts(HWND hWnd);

	static NOXFontManager * getInstance();
	
private:
	NOXFontManager();
	static NOXFontManager * oInstance;
public:
	~NOXFontManager();
};

//------------------------------------------------------------------------------------------------

#endif
