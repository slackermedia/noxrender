#include "max2013predef.h"
#include "NOXSunLight.h"
#include <decomp.h> 
#include <mtl.h>
#include "noxdebug.h"

#include "IViewportManager.h"
#include "IHardwareShader.h"
#include "IHardwareRenderer.h"
#include "IColorCorrectionMgr.h"

#ifdef NITROUS_BLABLA
#include "Graphics\IDisplayManager.h"
#endif

static NOXSunTargetClassDesc NOXSunTargetDesc;
ClassDesc2* GetNOXSunTargetDesc() { return &NOXSunTargetDesc; }
static NOXSunTargetObjectCreateCallback objSunTargCB;

//------------------------------------------------------------------------------------------------------------------------

NOXSunTarget::NOXSunTarget()  
{  
	BuildMesh(); 
	RebuildSphereMesh(1.0f); 
	amIHidden = false; 
	envMapHidden = false;
	hParams = 0; 
	texPtrEnv = NULL;
}

//------------------------------------------------------------------------------------------------------------------------

NOXSunTarget::~NOXSunTarget()
{
	deleteEnvMap();
	deleteMeshes();
}

//------------------------------------------------------------------------------------------------------------------------

RefTargetHandle NOXSunTarget::Clone(RemapDir& remap) 
{
	NOXSunTarget * newob = new NOXSunTarget();
	BaseClone(this, newob, remap);
	return(newob);
}

//------------------------------------------------------------------------------------------------------------------------

int NOXSunTarget::HitTest(TimeValue t, INode *inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) 
{
	if (amIHidden)
		return FALSE;
	HitRegion hitRegion;
	DWORD savedLimits;
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();	
	MakeHitRegion(hitRegion,type,crossing,4,p);	
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	GetMatrix(t,inode,vpt,m);
	gw->setTransform(m);
	if(targMesh.select( gw, gw->getMaterial(), &hitRegion, flags & HIT_ABORTONHIT ))
		return TRUE;
	gw->setRndLimits( savedLimits );
	return FALSE;
}

//------------------------------------------------------------------------------------------------------------------------

int NOXSunTarget::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) 
{
	if (amIHidden && envMapHidden)
		return 0;
	Matrix3 m, m_env;
	GraphicsWindow *gw = vpt->getGW();
	GetMatrix(t,inode,vpt,m);
	GetEnvSphereMatrix(t,inode,vpt,m_env);
	//m_env = m;
	gw->setTransform(m);
	DWORD rlim = gw->getRndLimits();

	bool isNitrous = false;
	#ifdef NITROUS_BLABLA
		MaxSDK::Graphics::IDisplayManager* dmgr = MaxSDK::Graphics::GetIDisplayManager();
		isNitrous = dmgr->IsRetainedModeEnabled();
	#endif


	if (!envMapHidden  &&  texPtrEnv  &&  !isNitrous)
	{
		float tempmatr[4][4];
		Matrix3 tempInvMatr;
		int temppersp = 0;
		gw->getCameraMatrix(tempmatr, &tempInvMatr, &temppersp, &tempHither, &tempYon);
		Point3 translate = tempInvMatr.GetTrans();
		cpx = translate[0];
		cpy = translate[1];
		cpz = translate[2];

		float scale = tempYon/1000 * 0.95f;
		m_env.Scale(Point3(scale, scale, scale));
		Matrix3 m_rot;
		m_rot.IdentityMatrix();
		m_rot.RotateZ(getSunObject()->trsunsky.getEnvAzimuth()*PI/180.0f*-1);
		m_rot *= m_env;
		m_env = m_rot;

		if (temppersp)
		{
			Material mat2;
			mat2.Kd = Point3(0.0f,0.0f,0.0f);
			mat2.Ka = Point3(0.0f,0.0f,0.0f);
			mat2.Ks = Point3(0.0f,0.0f,0.0f);
			mat2.selfIllum = 1.0f;

			TextureInfo texinfo;
			mat2.shadeLimit = GW_TEXTURE;
			texinfo.textHandle = texPtrEnv;
			texinfo.useTex = 1;
			texinfo.faceMap = 0;
			texinfo.mapChannel = 1;
			texinfo.textTM.IdentityMatrix();
			texinfo.uvwSource = UVSOURCE_MESH;
			#ifdef ISMAX2013
			mat2.texture.removeAll();
			mat2.texture.append(texinfo);
			#else
				mat2.texture.Resize(1);
				mat2.texture.Insert(0, 1, &texinfo);
			#endif

			gw->setMaterial(mat2);
			m_env.SetTrans(translate);
			gw->setTransform(m_env);
			sphMesh.render(gw, &mat2, NULL, COMP_ALL);

			// clean up 
			if (texPtrEnv)
				#ifdef ISMAX2013
					mat2.texture.removeAll();
				#else
					mat2.texture.Delete(0,1);
				#endif

		}
	}


	if (amIHidden)
	{
		gw->setRndLimits(rlim);
		return(0);
	}

	gw->setRndLimits(GW_WIREFRAME|GW_EDGES_ONLY);
	if (inode->Selected()) 
		gw->setColor( LINE_COLOR, GetSelColor());
	else if(!inode->IsFrozen() && !inode->Dependent() && inode->GetLookatNode()) {
		const ObjectState& os = inode->GetLookatNode()->EvalWorldState(t);
		Object* ob = os.obj;
		if ( (ob!=NULL) && ( (ob->SuperClassID()==LIGHT_CLASS_ID) ||
							 (ob->SuperClassID()==CAMERA_CLASS_ID) ) )
		{													
			gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_OBJ));
		}
		else
			gw->setColor( LINE_COLOR, GetUIColor(COLOR_CAMERA_OBJ)); // default target color, just use camera targ color
	}

	gw->setTransform(m);

	targMesh.render( gw, gw->getMaterial(), NULL, COMP_ALL);	
	GetLetterMatrix(t,inode,vpt, 0, m);
	gw->setTransform(m);
	meshN.render( gw, gw->getMaterial(), NULL, COMP_ALL);	
	GetLetterMatrix(t,inode,vpt, 1, m);
	gw->setTransform(m);
	meshW.render( gw, gw->getMaterial(), NULL, COMP_ALL);	
	GetLetterMatrix(t,inode,vpt, 2, m);
	gw->setTransform(m);
	meshS.render( gw, gw->getMaterial(), NULL, COMP_ALL);	
	GetLetterMatrix(t,inode,vpt, 3, m);
	gw->setTransform(m);
	meshE.render( gw, gw->getMaterial(), NULL, COMP_ALL);	

    gw->setRndLimits(rlim);
	return(0);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	if (amIHidden  &&  envMapHidden)
		return;
	int i,nv;
	Matrix3 m;
	GetMatrix(t,inode,vpt,m);
	nv = targMesh.getNumVerts();
	box.Init();
	for (i=0; i<nv; i++) 
		box += m*targMesh.getVert(i);
	//nwse
	nv = meshN.getNumVerts();
	GetLetterMatrix(t,inode,vpt,0, m);
	for (i=0; i<nv; i++) 
		box += m*meshN.getVert(i);
	nv = meshW.getNumVerts();
	GetLetterMatrix(t,inode,vpt,1, m);
	for (i=0; i<nv; i++) 
		box += m*meshW.getVert(i);
	nv = meshS.getNumVerts();
	GetLetterMatrix(t,inode,vpt,2, m);
	for (i=0; i<nv; i++) 
		box += m*meshS.getVert(i);
	nv = meshE.getNumVerts();
	GetLetterMatrix(t,inode,vpt,3, m);
	for (i=0; i<nv; i++) 
		box += m*meshE.getVert(i);

	nv = sphMesh.getNumVerts();
	GetEnvSphereMatrix(t, inode, vpt, m);
	for (i=0; i<nv; i++) 
		box += m*sphMesh.getVert(i);

	updateUIAltitudeAzimuth();
	updateUIPositions();
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	if (amIHidden)
		return;
	Matrix3 m = inode->GetObjectTM(t);
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(m.GetTrans())/(float)360.0;
	box = targMesh.getBoundingBox();
	box.Scale(scaleFactor*2);
	return;
}

//------------------------------------------------------------------------------------------------------------------------

ObjectState NOXSunTarget::Eval(TimeValue time) 
{
	return ObjectState(this);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel )
{
	box = targMesh.getBoundingBox(tm);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::GetMatrix(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm) 
{
	tm = inode->GetObjectTM(t);
	tm.NoScale();
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(tm.GetTrans())/(float)360.0;
	if (scaleFactor!=(float)1.0)
		tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::GetEnvSphereMatrix(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm) 
{
	tm = inode->GetObjectTM(t);
	tm.NoScale();
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::GetLetterMatrix(TimeValue t, INode* inode, ViewExp* vpt, int side, Matrix3& tm)
{
	float prz = 50.0f;
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(tm.GetTrans())/(float)360.0;

	Matrix3 om;
	vpt->GetAffineTM(om);
	om = Inverse(om);
	om.SetRow(3, Point3(0.0f, 0.0f, 0.0f));

	tm = inode->GetObjectTM(t);

	Matrix3 itm = Inverse(tm);
	itm.NoScale();
	itm.NoTrans();

	Matrix3 tr = IdentityTM();
	switch (side)
	{
		case 1:  tr.Translate(Point3(-prz, 0.0f, 0.0f)); break;
		case 2:  tr.Translate(Point3(0.0f,  prz, 0.0f)); break;
		case 3:  tr.Translate(Point3( prz, 0.0f, 0.0f)); break;
		default: tr.Translate(Point3(0.0f, -prz, 0.0f)); break;
	}

	tm.NoScale();
	if (scaleFactor!=(float)1.0)
		tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
	tm = tr * tm;
	tm = om * itm * tm ;
}

//------------------------------------------------------------------------------------------------------------------------


CreateMouseCallBack * NOXSunTarget::GetCreateMouseCallBack() 
{
	objSunTargCB.SetObj(this);
	return(&objSunTargCB);
}

//------------------------------------------------------------------------------------------------------------------------

int NOXSunTargetObjectCreateCallback::proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat )
{
	return CREATE_ABORT;

	Point3 c;
	if (msg==MOUSE_POINT||msg==MOUSE_MOVE) 
	{
		switch(point) 
		{
			case 0:
			{
				c = vpt->GetPointOnCP(m);
				mat.SetTrans(c);
				return CREATE_STOP;
			}
			break;
		}
	}
	else
	if (msg == MOUSE_ABORT)
		return CREATE_ABORT;
	return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::updateUIAltitudeAzimuth()
{
	HWND hAlt = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARGET_ALTITUDE);
	HWND hAzi = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARGET_AZIMUTH);

	float alt, azi;
	NOXSunLight * nsl = getSunObject();
	if (!nsl)
		return;
	nsl->evalAltitudeAzimuth(alt, azi);
	int ialt = (int)(180.0f*alt/PI);
	int iazi = (int)(180.0f*azi/PI);
	TCHAR buf[64];
	_stprintf_s(buf, 64, _T("%d�"), (ialt));
	SetWindowText(hAlt, buf);
	_stprintf_s(buf, 64, _T("%d�"), iazi);
	SetWindowText(hAzi, buf);
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::updateUIPositions()
{
	HWND hSx = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_SX);
	HWND hSy = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_SY);
	HWND hSz = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_SZ);
	HWND hTx = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_TX);
	HWND hTy = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_TY);
	HWND hTz = GetDlgItem(hParams, IDC_NOX_SUNSKY_TARG_TZ);

	NOXSunLight * sun = getSunObject();
	if (!sun)
		return;
	Point3 pos, tpos;
	if (!sun->getPositions(pos, tpos))
		return;

	TCHAR buf[64];
	_stprintf_s(buf, 64, _T("%.2f"), pos.x);
	SetWindowText(hSx, buf);
	_stprintf_s(buf, 64, _T("%.2f"), pos.y);
	SetWindowText(hSy, buf);
	_stprintf_s(buf, 64, _T("%.2f"), pos.z);
	SetWindowText(hSz, buf);
	_stprintf_s(buf, 64, _T("%.2f"), tempHither);
	SetWindowText(hTx, buf);
	_stprintf_s(buf, 64, _T("%.2f"), tempYon);
	SetWindowText(hTy, buf);
	_stprintf_s(buf, 64, _T("%.2f"), cpz);
	SetWindowText(hTz, buf);

}

//------------------------------------------------------------------------------------------------------------------------

NOXSunLight * NOXSunTarget::getSunObject()
{
	INode * inode = NULL;
	INode * snode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return NULL;
	snode = inode->GetLookatNode();
	if (!snode)
		return NULL;

	ObjectState objSt = snode->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return NULL;

	if (objSt.obj->SuperClassID() != LIGHT_CLASS_ID)
		return NULL;

	if (objSt.obj && objSt.obj->CanConvertToType(NOXSunLight_CLASS_ID))
	{
		NOXSunLight * sobj = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
		return sobj;
	}

	return NULL;
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::resetRotation()
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return;
	TimeValue t = GetCOREInterface()->GetTime();
	Matrix3 tmat = inode->GetNodeTM(t);
	tmat.NoRot();
	inode->SetNodeTM(t, tmat);
	GetCOREInterface()->ForceCompleteRedraw();
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunTarget::selectSun()
{
	INode * inode = NULL;
	INode * snode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return;
	snode = inode->GetLookatNode();
	if (!snode)
		return;

	GetCOREInterface()->SelectNode(snode);
	GetCOREInterface()->ForceCompleteRedraw();
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunTarget::amISelected()
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return false;
	if (inode->Selected())
		return true;
	else
		return false;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunTarget::setTMfromAltAzi(float alt, float azi)
{
	INode * inode = NULL;
	INode * snode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	snode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!snode)
		return false;
	inode = snode->GetLookatNode();
	if (!inode)
		return false;
	NOXSunLight * sun = getSunObject();
	if (!sun)
		return false;

	Matrix3 mat, tmat, otmat;
	TimeValue t = GetCOREInterface()->GetTime();
	inode->GetTargetTM(t, tmat);
	mat = inode->GetNodeTM(t);
	Point3 spos = mat.GetTrans();
	otmat = tmat;

	tmat.NoTrans();
	tmat.NoScale();

	Point3 dirN  = Point3(0,-1,0) * tmat;
	Point3 dirS  = Point3(0, 1,0) * tmat;
	Point3 dirW  = Point3( 1,0,0) * tmat;
	Point3 dirE  = Point3(-1,0,0) * tmat;
	Point3 dirUp = Point3( 0,0,1) * tmat;

	if (_isnan(azi)  ||  _isnan(alt))
		MessageBox(0, _T("Azimuth correction fail."), _T("Error"), 0);
	Point3 toSun, toSun_, fromSun;
	toSun_ = sin(azi)*dirE + cos(azi)*dirN;
	toSun_ = myNormalize(toSun_);
	toSun = cos(alt)*toSun_ + sin(alt)*dirUp;
	toSun = myNormalize(toSun);
	toSun *= sun->tDist;
	fromSun = -toSun;
	fromSun = fromSun + spos;
	otmat.SetTrans(fromSun);

	snode->SetNodeTM(t, otmat);

	return true;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunTarget::evalDirections(Point3 &dirN, Point3 &dirE)
{
	INode * inode = NULL;
	ULONG handle = 0;
	NotifyDependents( FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE );
	inode = GetCOREInterface()->GetINodeByHandle(handle);
	if (!inode)
		return false;

	Matrix3 mat;
	TimeValue t = GetCOREInterface()->GetTime();
	mat = inode->GetNodeTM(t);

	mat.NoTrans();
	mat.NoScale();

	dirN  = Point3(0,-1,0) * mat;
	dirE  = Point3(-1,0,0) * mat;

	return true;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunTarget::LoadEnvMap(char * filename)
{
	return true;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunTarget::updateEnvMap()
{
	BITMAPINFO * bmi = NULL;
	NOXSunLight * sun = getSunObject();
	if (!sun)
		return false;

	bool ok = sun->trsunsky.getEnvTex(&bmi);

	#ifdef ISMAX2013
		ViewExp * vpt = &(GetCOREInterface()->GetActiveViewExp());
	#else
		ViewExp * vpt = GetCOREInterface()->GetActiveViewport();
	#endif
	GraphicsWindow *gw = vpt->getGW();

	IHardwareRenderer * phr = (IHardwareRenderer *)gw->GetInterface(HARDWARE_RENDERER_INTERFACE_ID);
	if (texPtrEnv  &&  phr)
		phr->FreeTexture(texPtrEnv);
	texPtrEnv = NULL;

	if (!phr)
	{
		ADD_LOG_CONSTR("UpdateEnvMap hardware renderer is null .... go to hell autodesk!!!!!!!!!!!!!!!!!!!!");
	}

	if (bmi  &&  phr)
		texPtrEnv = phr->BuildTexture(bmi,0,0,D3DFMT_R8G8B8);

	if (bmi)
		free(bmi);
	bmi = NULL;

	#ifndef ISMAX2013
		GetCOREInterface()->ReleaseViewport( vpt );
	#endif

	return true;
}

//------------------------------------------------------------------------------------------------------------------------

bool NOXSunTarget::deleteEnvMap()
{
	if (!texPtrEnv)
		return true;

	Interface * intf = GetCOREInterface();
	if (!intf)
		return false;

	#ifdef ISMAX2013
		ViewExp * vpt = &(GetCOREInterface()->GetActiveViewExp());
	#else
		ViewExp * vpt = GetCOREInterface()->GetActiveViewport();
	#endif

	if (!vpt)
		return false;
	GraphicsWindow *gw = vpt->getGW();
	if (!gw)
		return false;
	IHardwareRenderer * phr = (IHardwareRenderer *)gw->GetInterface(HARDWARE_RENDERER_INTERFACE_ID);
	if (phr)
		phr->FreeTexture(texPtrEnv);

	#ifndef ISMAX2013
		GetCOREInterface()->ReleaseViewport( vpt );
	#endif

	texPtrEnv = NULL;
	return true;
}

//------------------------------------------------------------------------------------------------------------------------
