#define _CRT_RAND_S
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "raytracer.h"
#include "QuasiRandom.h"
#include <float.h>
#include "log.h"

Vector3d::Vector3d() 
{
	#ifdef FORCE_SSE
		sse = _mm_setzero_ps();
	#else
		x = y = z = 0;
	#endif
}

Vector3d::Vector3d(const float &X, const float &Y, const float &Z) 
{
	#ifdef FORCE_SSE
		sse = _mm_set_ps(X,Y,Z,0);
	#else
		x=X; y=Y; z=Z;
	#endif
}

Point3d::Point3d() 
{
	#ifdef FORCE_SSE
		sse = _mm_setzero_ps();
	#else
		x = y = z = 0;
	#endif
}

Point3d::Point3d(const float &X, const float &Y, const float &Z) 
{
	#ifdef FORCE_SSE
		sse = _mm_set_ps(X,Y,Z,0);
	#else
		x=X; y=Y; z=Z;
	#endif
}

bool Vector3d::normalize()
{
	#ifdef FORCE_SSE
		__m128 d0 = _mm_load_ps((float*)this);
		__m128 d2 = d0;
		d0 = _mm_mul_ps(d0, d0);

		#ifdef OWN_SSE3
			d0 = _mm_hadd_ps(d0,d0);
			d0 = _mm_hadd_ps(d0,d0);
		#else
			__m128 d1 = d0;
			d0 = _mm_shuffle_ps(d0,d1, 0x4e);
			d0 = _mm_add_ps(d0,d1);
			d1 = d0;
			d1 = _mm_shuffle_ps(d1,d1, 0x11);
			d0 = _mm_add_ps(d0,d1);
		#endif
		d0 = _mm_rsqrt_ps(d0);
		d2 = _mm_mul_ps(d2, d0);
		_mm_store_ps((float*)this, d2);
		return true;
	#else
		float dist = x*x + y*y + z*z;
		if (dist < SFLOAT)
		{
			x = 1.0f;
			y = 0.0f;
			z = 0.0f;
			return false;
		}

		dist = 1.0f / (float)sqrt(dist);
		x *= dist;
		y *= dist;
		z *= dist;
		return true;
	#endif
}

float Vector3d::normalize_return_old_length()	// -1 for error
{
	float dist = x*x + y*y + z*z;
	if (dist < SFLOAT)
	{
		x = 1.0f;
		y = 0.0f;
		z = 0.0f;
		return -1;
	}

	dist = (float)sqrt(dist);
	float d1 = 1.0f / dist;
	x *= d1;
	y *= d1;
	z *= d1;
	return dist;
}

Vector3d Vector3d::getNormalized()
{
	Vector3d res = *this;
	float dist = x*x + y*y + z*z;
	if (dist < SFLOAT)
		return res;

	dist = (float)sqrt(dist);
	float d1 = 1.0f / dist;
	res.x *= d1;
	res.y *= d1;
	res.z *= d1;
	return res;
}

void Vector3d::invert()
{
	#ifdef FORCE_SSE
		sse = _mm_sub_ps(_mm_setzero_ps(), sse);
	#else
		x = -x;
		y = -y;
		z = -z;
	#endif
}

float Vector3d::length()
{
	#ifdef FORCE_SSE
		__m128 d0 = _mm_load_ps((float*)this);
		__m128 d2 = d0;
		d0 = _mm_mul_ps(d0, d0);
		#ifdef OWN_SSE3
			d0 = _mm_hadd_ps(d0,d0);
			d0 = _mm_hadd_ps(d0,d0);
		#else
			__m128 d1 = d0;
			d0 = _mm_shuffle_ps(d0,d1, 0x4e);
			d0 = _mm_add_ps(d0,d1);
			d1 = d0;
			d1 = _mm_shuffle_ps(d1,d1, 0x11);
			d0 = _mm_add_ps(d0,d1);
		#endif
		d0 = _mm_sqrt_ps(d0);
		float res;
		_mm_store_ss(&res, d0);
		return res;
	#else
		return (float)sqrt(x*x + y*y + z*z);
	#endif
}

float Vector3d::lengthSqr()
{
	#ifdef FORCE_SSE
		__m128 d0 = _mm_load_ps((float*)this);
		__m128 d2 = d0;
		d0 = _mm_mul_ps(d0, d0);
		#ifdef OWN_SSE3
			d0 = _mm_hadd_ps(d0,d0);
			d0 = _mm_hadd_ps(d0,d0);
		#else
			__m128 d1 = d0;
			d0 = _mm_shuffle_ps(d0,d1, 0x4e);
			d0 = _mm_add_ps(d0,d1);
			d1 = d0;
			d1 = _mm_shuffle_ps(d1,d1, 0x11);
			d0 = _mm_add_ps(d0,d1);
		#endif
		float res;
		_mm_store_ss(&res, d0);
		return res;
	#else
		return (x*x + y*y + z*z);
	#endif
}


Vector3d Vector3d::random()
{
	Vector3d res;

	int cntr = 0;
	do
	{
		#ifdef RAND_PER_THREAD
			res.x = Raytracer::getRandomGeneratorForThread()->getRandomFloat()*2-1;
			res.y = Raytracer::getRandomGeneratorForThread()->getRandomFloat()*2-1;
			res.z = Raytracer::getRandomGeneratorForThread()->getRandomFloat()*2-1;
		#else
			res.x = getMarsagliaFloat()*2-1;
			res.y = getMarsagliaFloat();
			res.y = getMarsagliaFloat()*2-1;
			res.z = getMarsagliaFloat();
			res.z = getMarsagliaFloat()*2-1;
		#endif


		if (cntr++ > 50)	// a little bit of debug
		{
			unsigned int r1,r2;
			while(rand_s(&r1));
			while(rand_s(&r2));
			getMarsaglia(r1, r2);
		}
	}
	while (res.x*res.x + res.y*res.y + res.z*res.z > 1.0f);
	return res;
}

Vector3d Vector3d::reflect(const Vector3d &normal) const
{
	Vector3d res;
	#ifdef FORCE_SSE
		__m128 d0 = _mm_load_ps((float*)this);
		__m128 d2 = d0;
		d0 = _mm_mul_ps(d0, normal.sse);
		__m128 d1 = d0;
		d0 = _mm_shuffle_ps(d0,d1, 0x4e);
		d0 = _mm_add_ps(d0,d1);
		d1 = d0;
		d1 = _mm_shuffle_ps(d1,d1, 0x11);
		d0 = _mm_add_ps(d0,d1);
		d0 = _mm_mul_ps(d0, normal.sse);
		d0 = _mm_add_ps(d0,d0);
		res.sse = _mm_sub_ps(d0, d2);
		return res;
	#else
		Vector3d rev = *this;
		rev.invert();
		res = normal*((*this * normal)*2)  + rev;
	#endif
	return res;
}


Vector3d refractSnellTemp(const Vector3d &in1, const Vector3d &normal, const float &ior, bool &innerReflected)
{
	Vector3d res;
	float ior2, cosIn, cosOut;

	ior2 = 1.0f/ior;

	cosIn = in1*normal;
	float signCI = 1.0f;
    (int&)signCI |= ((int&)cosIn & 0x80000000);

	cosOut = 1 + ior2*ior2*(cosIn*cosIn - 1);
	if (cosOut < 0)
	{
		innerReflected = true;
		return in1.reflect(normal);
	}

	cosOut = sqrt(cosOut);
	innerReflected = false;

	res = normal*(ior2*cosIn - signCI*cosOut) + in1*(-ior2);

	return res;
}


Vector3d Vector3d::refractSnell(const Vector3d &normal, const float &ior, bool &innerReflected)	// incoming (*this) vector is directed out of medium
{
	Vector3d res;
	float ior2, cosIn, cosOut;

	ior2 = 1.0f/ior;
	cosIn = *this*normal;
	float signCI = 1.0f;
	if (cosIn<0)
		signCI = -1;

	cosOut = 1 + ior2*ior2*(cosIn*cosIn - 1);
	innerReflected = false;
	if (cosOut < 0)
	{
		innerReflected = true;
		return this->reflect(normal);
	}

	cosOut = sqrt(cosOut);
	innerReflected = false;
	res = normal*(ior2*cosIn - signCI*cosOut) + *this*(-ior2);
	res.normalize();
	return res;
}

void Vector3d::toAngles(float &theta, float &phi)
{
	phi = atan2(x, -z);
	theta = acos(y);
}

void Vector3d::getFromAngles(const float &theta, const float &phi)
{
	float s = sin(theta);
	y = cos(theta);
	x = sin(phi)*s;
	z = -cos(phi)*s;
}

Vector3d Point3d::toVec() 
	{return Vector3d(x,y,z);}


Matrix3d::Matrix3d(const Point3d &v1, const Point3d &v2, const Point3d &v3, const int &type)
{
	if (type == BY_ROWS)
	{
		M00 = v1.x;  M01 = v1.y;  M02 = v1.z;  
		M10 = v2.x;  M11 = v2.y;  M12 = v2.z;  
		M20 = v3.x;  M21 = v3.y;  M22 = v3.z;  
	}
	else
	{
		M00 = v1.x;  M01 = v2.x;  M02 = v3.x;  
		M10 = v1.y;  M11 = v2.y;  M12 = v3.y;  
		M20 = v1.z;  M21 = v2.z;  M22 = v3.z;  
	}
}

Matrix3d::Matrix3d(const Vector3d &v1, const Vector3d &v2, const Vector3d &v3, const int &type)
{
	if (type == BY_ROWS)
	{
		M00 = v1.x;  M01 = v1.y;  M02 = v1.z;  
		M10 = v2.x;  M11 = v2.y;  M12 = v2.z;  
		M20 = v3.x;  M21 = v3.y;  M22 = v3.z;  
	}
	else
	{
		M00 = v1.x;  M01 = v2.x;  M02 = v3.x;  
		M10 = v1.y;  M11 = v2.y;  M12 = v3.y;  
		M20 = v1.z;  M21 = v2.z;  M22 = v3.z;  
	}
}

bool Matrix3d::invert()
{
	Matrix3d r;
	float m;
	m = M00*(M11*M22 - M12*M21)  -  M01*(M10*M22 - M12*M20)  +  M02*(M10*M21 - M11*M20);	
	if (fabs(m) < SFLOAT)
	{
		return false;
	}
	m = 1.0f/m;

	r.M00 = (M11*M22 - M12*M21)*m;
	r.M01 = (M02*M21 - M01*M22)*m;
	r.M02 = (M01*M12 - M02*M11)*m;
	r.M10 = (M12*M20 - M10*M22)*m;
	r.M11 = (M00*M22 - M02*M20)*m;
	r.M12 = (M02*M10 - M00*M12)*m;
	r.M20 = (M10*M21 - M11*M20)*m;
	r.M21 = (M01*M20 - M00*M21)*m;
	r.M22 = (M00*M11 - M01*M10)*m;
	*this = r;

	return true;
}


float Matrix4d::getDeterminant()
{
	float res = 0;
	res += M00 * (M11*M22*M33 + M12*M23*M31 + M13*M21*M32 - M13*M22*M31 - M11*M23*M32 - M12*M21*M33);
	res += M10 * (M01*M22*M33 + M02*M23*M31 + M03*M21*M32 - M03*M22*M31 - M01*M23*M32 - M02*M21*M33) * -1.0f;
	res += M20 * (M01*M12*M33 + M02*M13*M31 + M03*M11*M32 - M03*M12*M31 - M01*M13*M32 - M02*M11*M33);
	res += M30 * (M01*M12*M23 + M02*M13*M21 + M03*M11*M22 - M03*M12*M21 - M01*M13*M22 - M02*M11*M23) * -1.0f;
	return res;
}

void Matrix4d::setIdentity()
{
	for (int i=0; i<4; i++)
		for (int j=0; j<4; j++)
			M[i][j] = 0.0f;
	for (int i=0; i<4; i++)
		M[i][i] = 1.0f;
}


bool Matrix4d::getInverted(Matrix4d &m)
{
	float det = getDeterminant();
	if (det==0.0f)
		return false;
	float perdet = 1.0f / det;

	Matrix4d t;
	for (int i=0; i<4; i++)
		for (int j=0; j<4; j++)
			t.M[i][j] = getMinor(i,j) * perdet;

	m = t.getTransposed();
	return true;
}

Matrix4d Matrix4d::getTransposed()
{
	Matrix4d m;
	for (int i=0; i<4; i++)
		for (int j=0; j<4; j++)
			m.M[i][j] = M[j][i];
	return m;
}

Vector3d Matrix4d::getScaleComponent() const
{
	Vector3d res;
	res.x = sqrt( M00*M00 + M10*M10 + M20*M20 );
	res.y = sqrt( M01*M01 + M11*M11 + M21*M21 );
	res.z = sqrt( M02*M02 + M12*M12 + M22*M22 );
	return res;
}

Matrix4d Matrix4d::getMatrixForNormals() const
{
	#define DOITSSE
	#ifdef DOITSSE
		__m128 p1 = _mm_mul_ps(sse[0], sse[0]);
		__m128 p2 = _mm_mul_ps(sse[1], sse[1]);
		__m128 p3 = _mm_mul_ps(sse[2], sse[2]);
		__m128 s = _mm_add_ps(_mm_add_ps(p1,p2),p3);
		__m128 one = _mm_set1_ps(1.0f);
		s = _mm_div_ps(one, s);
		Matrix4d res;
		res.sse[0] = _mm_mul_ps(sse[0], s);
		res.sse[1] = _mm_mul_ps(sse[1], s);
		res.sse[2] = _mm_mul_ps(sse[2], s);
		res.sse[3] = _mm_set_ps(1,0,0,0);
		return res;

	#else
		Matrix4d res;
		float xx = 1.0f / ( M00*M00 + M10*M10 + M20*M20 );
		float yy = 1.0f / ( M01*M01 + M11*M11 + M21*M21 );
		float zz = 1.0f / ( M02*M02 + M12*M12 + M22*M22 );

		res.M00 = M00 * xx;
		res.M01 = M01 * yy;
		res.M02 = M02 * zz;
		res.M03 = 0.0f;
		res.M10 = M10 * xx;
		res.M11 = M11 * yy;
		res.M12 = M12 * zz;
		res.M13 = 0.0f;
		res.M20 = M20 * xx;
		res.M21 = M21 * yy;
		res.M22 = M22 * zz;
		res.M23 = 0.0f;
		res.M30 = 0.0f;
		res.M31 = 0.0f;
		res.M32 = 0.0f;
		res.M33 = 1.0f;

		return res;
	#endif
}

float Matrix4d::getMinor(int y, int x)	
{
	Matrix4d m = *this;
	for (int i=y; i<3; i++)
		for (int j=0; j<4; j++)
			m.M[i][j] = m.M[i+1][j];
	for (int i=x; i<3; i++)
		for (int j=0; j<4; j++)
			m.M[j][i] = m.M[j][i+1];
	float det =	  m.M00*m.M11*m.M22 + m.M01*m.M12*m.M20 + m.M02*m.M10*m.M21
				- m.M02*m.M11*m.M20 - m.M00*m.M12*m.M21 - m.M01*m.M10*m.M22;
	if ((y+x)%2)
		det *= -1;
	return det;
}

bool Matrix4d::setRotation(Vector3d dir, float angle)
{
	dir.normalize();
	float cos_a = cos(angle);
	float sin_a = sin(angle);
    float cos_1_a = 1 - cos_a;

    float u = dir.x;
    float v = dir.y;
    float w = dir.z;

    M00 = cos_1_a * u * u + cos_a;
	M01 = cos_1_a * u * v - sin_a * w;
	M02 = cos_1_a * u * w + sin_a * v;
	M03 = 0;
	M10 = cos_1_a * u * v + sin_a * w;
	M11 = cos_1_a * v * v + cos_a;
	M12 = cos_1_a * v * w - sin_a * u;
	M13 = 0;
	M20 = cos_1_a * u * w - sin_a * v;
	M21 = cos_1_a * v * w + sin_a * u;
	M22 = cos_1_a * w * w + cos_a;
	M23 = 0;
	M30 = 0;
	M31 = 0;
	M32 = 0;
	M33 = 1;

	return true;
}

Matrix4d Matrix4d::operator*(const Matrix4d &m) const
{
	__m128 t0 = _mm_unpacklo_ps(m.sse[0], m.sse[1]);
	__m128 t1 = _mm_unpacklo_ps(m.sse[2], m.sse[3]);
	__m128 t2 = _mm_unpackhi_ps(m.sse[0], m.sse[1]);
	__m128 t3 = _mm_unpackhi_ps(m.sse[2], m.sse[3]);
	__m128 tr1 = _mm_movelh_ps(t0, t1);
	__m128 tr2 = _mm_movehl_ps(t1, t0);
	__m128 tr3 = _mm_movelh_ps(t2, t3);
	__m128 tr4 = _mm_movehl_ps(t3, t2);
	Matrix4d res;
	for (int i=0; i<4; i++)
	{
		__m128 mpl1 = _mm_mul_ps(sse[i], tr1);
		__m128 mpl2 = _mm_mul_ps(sse[i], tr2);
		__m128 mpl3 = _mm_mul_ps(sse[i], tr3);
		__m128 mpl4 = _mm_mul_ps(sse[i], tr4);
		__m128 add1 = _mm_hadd_ps(mpl1, mpl2);
		__m128 add2 = _mm_hadd_ps(mpl3, mpl4);
		res.sse[i] = _mm_hadd_ps(add1, add2);
	}
	return res;
}

Vector3d Matrix4d::operator* (const Vector3d &v) const
{
	Vector3d res;
	res.x = M00*v.x + M01*v.y + M02*v.z;
	res.y = M10*v.x + M11*v.y + M12*v.z;
	res.z = M20*v.x + M21*v.y + M22*v.z;
	return res;
}

Point3d Matrix4d::operator* (const Point3d &v) const
{
	Point3d res;
	res.x = M00*v.x + M01*v.y + M02*v.z + M03;
	res.y = M10*v.x + M11*v.y + M12*v.z + M13;
	res.z = M20*v.x + M21*v.y + M22*v.z + M23;
	return res;
}

void logMatrix(char * name, Matrix4d &m)
{
	char buf[256];
	sprintf_s(buf, 256, "Matrix %s:\n%.4f   %.4f   %.4f   %.4f\n%.4f   %.4f   %.4f   %.4f\n%.4f   %.4f   %.4f   %.4f\n%.4f   %.4f   %.4f   %.4f\n",
		name,
		m.M00, m.M01, m.M02, m.M03, 
		m.M10, m.M11, m.M12, m.M13, 
		m.M20, m.M21, m.M22, m.M23, 
		m.M30, m.M31, m.M32, m.M33	);
	Logger::add(buf);
}

void logMatrixMax(char * name, Matrix4d &m)
{
	char buf[256];
	sprintf_s(buf, 256, "Matrix %s:\n%.4f   %.4f   %.4f   %.4f\n%.4f   %.4f   %.4f   %.4f\n%.4f   %.4f   %.4f   %.4f\n%.4f   %.4f   %.4f   %.4f\n",
		name,
		m.M00, m.M10, m.M20, m.M30, 
		m.M01, m.M11, m.M21, m.M31, 
		m.M02, m.M12, m.M22, m.M32, 
		m.M03, m.M13, m.M23, m.M33 );
	Logger::add(buf);
}

Matrix4dSmall::Matrix4dSmall(const Matrix4d & m4)
{
	M00 = m4.M00;   M01 = m4.M01;   M02 = m4.M02;   M03 = m4.M03;
	M10 = m4.M10;   M11 = m4.M11;   M12 = m4.M12;   M13 = m4.M13;
	M20 = m4.M20;   M21 = m4.M21;   M22 = m4.M22;   M23 = m4.M23;
}

Matrix4dSmall::Matrix4dSmall(const Matrix4dSmall & m4)
{
	M00 = m4.M00;   M01 = m4.M01;   M02 = m4.M02;   M03 = m4.M03;
	M10 = m4.M10;   M11 = m4.M11;   M12 = m4.M12;   M13 = m4.M13;
	M20 = m4.M20;   M21 = m4.M21;   M22 = m4.M22;   M23 = m4.M23;
}

Matrix4d Matrix4dSmall::toFullMatrix()
{
	Matrix4d res;
	res.M00 = M00;   res.M01 = M01;   res.M02 = M02;   res.M03 = M03;
	res.M10 = M10;   res.M11 = M11;   res.M12 = M12;   res.M13 = M13;
	res.M20 = M20;   res.M21 = M21;   res.M22 = M22;   res.M23 = M23;
	res.M30 = 0.0f;  res.M31 = 0.0f;  res.M32 = 0.0f;  res.M33 = 1.0f;
	return res;
}


int Raytracer::getVersionMajor()	 
{ 
	return NOX_VER_MAJOR; 
}

int Raytracer::getVersionMinor()	 
{ 
	return NOX_VER_MINOR; 
}

int Raytracer::getVersionBuild()	 
{ 
	return NOX_VER_BUILD; 
}

