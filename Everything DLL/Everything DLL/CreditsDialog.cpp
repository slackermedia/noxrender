#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"

extern HMODULE hDllModule;

typedef LRESULT (WINAPI * STATICTEXTPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
STATICTEXTPROC OldSTProcCr = NULL;

INT_PTR CALLBACK HyperTextCrDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
		case WM_MOUSEMOVE:
			{
				if (GetCapture() != hWnd)
				{
					InvalidateRect(hWnd, NULL, FALSE);
					SetCapture(hWnd);
					SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW)) );
				}
				else
				{
					RECT rect;
					GetWindowRect(hWnd, &rect);
					POINT pt = { LOWORD(lParam), HIWORD(lParam) };
					ClientToScreen(hWnd, &pt);

					if (!PtInRect(&rect, pt))
					{
						InvalidateRect(hWnd, NULL, FALSE);
						ReleaseCapture();
						SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW)) );
					}
					else
					{
						SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_HAND)) );
					}
					InvalidateRect(hWnd, NULL, FALSE);
				}
			}
			break;
	}

	return OldSTProcCr(hWnd, message, wParam, lParam);
}



INT_PTR CALLBACK CreditsDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBRUSH trBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (!bgBrush)
					bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				if (!trBrush)
				{
					LOGBRUSH lgb;
					lgb.lbStyle = BS_NULL;
					trBrush = CreateBrushIndirect(&lgb);
				}
				
				EMButton * embclose = GetEMButtonInstance(GetDlgItem(hWnd, IDC_CREDITS_CLOSE));
				GlobalWindowSettings::colorSchemes.apply(embclose);

				HWND hLink1 = GetDlgItem(hWnd, IDC_CREDITS_TEXT3);
				HWND hLink2 = GetDlgItem(hWnd, IDC_CREDITS_TEXT6);
				#ifdef _WIN64
					OldSTProcCr = (STATICTEXTPROC) SetWindowLongPtr(hLink1, GWLP_WNDPROC, (LONG_PTR)HyperTextCrDlgProc) ;
					SetWindowLongPtr(hLink2, GWLP_WNDPROC, (LONG_PTR)HyperTextCrDlgProc) ;
				#else
					OldSTProcCr = (STATICTEXTPROC)(HANDLE)(LONG_PTR) SetWindowLong(hLink1, GWL_WNDPROC, (LONG)(LONG_PTR)HyperTextCrDlgProc) ;
					SetWindowLong(hLink2, GWL_WNDPROC, (LONG)(LONG_PTR)HyperTextCrDlgProc) ;
				#endif


			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (trBrush)
					DeleteObject(trBrush);
				trBrush = 0;
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_CREDITS_CLOSE:
						{
							EndDialog(hWnd, (INT_PTR)NULL);
						}
						break;

					case IDC_CREDITS_TEXT3:
						{
							if (wmEvent == STN_CLICKED)
							{
								ShellExecute(hWnd, "open", "mailto:rozwin.skrzydla@gmail.com", NULL, NULL, SW_SHOWNORMAL);
							}
						}
						break;
					case IDC_CREDITS_TEXT6:
						{
							if (wmEvent == STN_CLICKED)
							{
								ShellExecute(hWnd, "open", "mailto:zed@pixelmustdie.com", NULL, NULL, SW_SHOWNORMAL);
							}
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				if ((HWND)lParam == GetDlgItem(hWnd, IDC_CREDITS_TEXT3)  ||   (HWND)lParam == GetDlgItem(hWnd, IDC_CREDITS_TEXT6))
					SetTextColor(hdc1, RGB(0,160,210));
				else
					SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, OPAQUE);
				SetBkColor(hdc1, GlobalWindowSettings::colorSchemes.DialogBackground);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

