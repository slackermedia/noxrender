#ifndef _Xnoxscene_H_
#define _Xnoxscene_H_

enum
{
	NOX_SHOW_INFO = 1010,
	NOX_SHOW_INFO_DETAIL = 1020,
	NOX_SHOW_WARNING = 1030,
	NOX_SHOW_ERROR = 1040,
	NOX_RUN_NOX = 1100,
	NOX_CONSOLE_DEBUG = 1002
};

#endif
