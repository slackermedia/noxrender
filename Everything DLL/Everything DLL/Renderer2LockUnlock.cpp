#include "RendererMain2.h"
#include "resource.h"
#include "EM2Controls.h"

void lockUnlockAllTab2Render(HWND hTabRender, bool unlock);
void lockUnlockAllTab2Camera(HWND hTabCamera, bool unlock);
void lockUnlockAllTab2Environment(HWND hTabEnv, bool unlock);
void lockUnlockAllTab2Layers(HWND hTabBlend, bool unlock);
void lockUnlockAllTab2Post(HWND hTabPost, bool unlock);
void lockUnlockAllTab2Correction(HWND hTabCorr, bool unlock);
void lockUnlockAllTab2FakeDof(HWND hTabFDof, bool unlock);
void lockUnlockAllTab2Effects(HWND hTabEff, bool unlock);
void lockUnlockAllTab2Options(HWND hTabOptions, bool unlock);

//------------------------------------------------------------------------------------------------

void RendererMain2::lockNOXStart()
{
	lockUnlockAllTab2Render(hTabRender,			true);
	lockUnlockAllTab2Camera(hTabCamera,			true);
	lockUnlockAllTab2Environment(hTabEnv,		true);
	lockUnlockAllTab2Layers(hTabBlend,			false);
	lockUnlockAllTab2Post(hTabPost,				false);
	lockUnlockAllTab2Correction(hTabCorrection,	false);
	lockUnlockAllTab2FakeDof(hTabFakeDof,		false);
	lockUnlockAllTab2Effects(hTabEffects,		false);
	lockUnlockAllTab2Options(hTabOptions,		false);

	matControlsLockUnlockAll(false);

	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_RENDER);
	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_RESUME);
	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_STOP);

}

//------------------------------------------------------------------------------------------------

void RendererMain2::unlockAfterSceneLoadFailed()
{
	lockUnlockAllTab2Render(hTabRender,			false);
	lockUnlockAllTab2Camera(hTabCamera,			false);
	lockUnlockAllTab2Environment(hTabEnv,		false);
	lockUnlockAllTab2Layers(hTabBlend,			false);
	lockUnlockAllTab2Post(hTabPost,				false);
	lockUnlockAllTab2Correction(hTabCorrection,	false);
	lockUnlockAllTab2FakeDof(hTabFakeDof,		false);
	lockUnlockAllTab2Effects(hTabEffects,		false);
	lockUnlockAllTab2Options(hTabOptions,		false);

	matControlsLockUnlockAll(false);

	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_RENDER);
	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_RESUME);
	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_STOP);
}

//------------------------------------------------------------------------------------------------

void RendererMain2::unlockAfterSceneLoaded()
{
	lockUnlockAllTab2Render(hTabRender,			true);
	lockUnlockAllTab2Camera(hTabCamera,			true);
	lockUnlockAllTab2Environment(hTabEnv,		true);
	lockUnlockAllTab2Layers(hTabBlend,			true);
	lockUnlockAllTab2Post(hTabPost,				true);
	lockUnlockAllTab2Correction(hTabCorrection,	true);
	lockUnlockAllTab2FakeDof(hTabFakeDof,		true);
	lockUnlockAllTab2Effects(hTabEffects,		true);
	lockUnlockAllTab2Options(hTabOptions,		true);

	matControlsLockUnlockAll(true);
	layers2lockUnlockOnlyUsedLayers(Raytracer::getInstance()->curScenePtr->getActiveCamera());
	updateLocksTabRender(false);
	updateLocksTabCamera(false);
	updateLocksTabMaterials(false);
	updateLocksTabEnvironment(false);
	updateLocksTabLayers(false);
	updateLocksTabPost(false);
	updateLocksTabCorrection(false);
	updateLocksTabFakeDof(false);
	updateLocksTabEffects(false);
	updateLocksTabOptions(false);

	setControlEnabled(true , hTopPanel, IDC_REND2_TOP_RENDER);
	setControlEnabled(true , hTopPanel, IDC_REND2_TOP_RESUME);
	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_STOP);

}

//------------------------------------------------------------------------------------------------

void RendererMain2::lockOnStartRender()
{
	lockUnlockAllTab2Render(hTabRender,			false);
	lockUnlockAllTab2Camera(hTabCamera,			false);
	lockUnlockAllTab2Environment(hTabEnv,		false);
	lockUnlockAllTab2Layers(hTabBlend,			true);
	lockUnlockAllTab2Post(hTabPost,				true);
	lockUnlockAllTab2Correction(hTabCorrection,	true);
	lockUnlockAllTab2FakeDof(hTabFakeDof,		true);
	lockUnlockAllTab2Effects(hTabEffects,		true);
	lockUnlockAllTab2Options(hTabOptions,		true);

	matControlsLockUnlockAll(false);
	layers2lockUnlockOnlyUsedLayers(Raytracer::getInstance()->curScenePtr->getActiveCamera());
	updateLocksTabRender(true);
	updateLocksTabCamera(true);
	updateLocksTabMaterials(true);
	updateLocksTabEnvironment(true);
	updateLocksTabLayers(true);
	updateLocksTabPost(true);
	updateLocksTabCorrection(true);
	updateLocksTabFakeDof(true);
	updateLocksTabEffects(true);
	updateLocksTabOptions(true);

	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_RENDER);
	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_RESUME);
	setControlEnabled(true , hTopPanel, IDC_REND2_TOP_STOP);
	ShowWindow(GetDlgItem(hTopPanel, IDC_REND2_TOP_ANIM), SW_SHOW);
	ShowWindow(GetDlgItem(hTopPanel, IDC_REND2_TOP_RENDER), SW_HIDE);
	EM2Busy * emb = GetEM2BusyInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_ANIM));
	if (emb)
		emb->startAnim();

}

//------------------------------------------------------------------------------------------------

void RendererMain2::unlockOnStopRender()
{
	lockUnlockAllTab2Render(hTabRender,			true);
	lockUnlockAllTab2Camera(hTabCamera,			true);
	lockUnlockAllTab2Environment(hTabEnv,		true);
	lockUnlockAllTab2Layers(hTabBlend,			true);
	lockUnlockAllTab2Post(hTabPost,				true);
	lockUnlockAllTab2Correction(hTabCorrection,	true);
	lockUnlockAllTab2FakeDof(hTabFakeDof,		true);
	lockUnlockAllTab2Effects(hTabEffects,		true);
	lockUnlockAllTab2Options(hTabEffects,		true);

	matControlsLockUnlockAll(true);
	layers2lockUnlockOnlyUsedLayers(Raytracer::getInstance()->curScenePtr->getActiveCamera());
	updateLocksTabRender(false);
	updateLocksTabCamera(false);
	updateLocksTabMaterials(false);
	updateLocksTabEnvironment(false);
	updateLocksTabLayers(false);
	updateLocksTabPost(false);
	updateLocksTabCorrection(false);
	updateLocksTabFakeDof(false);
	updateLocksTabEffects(false);
	updateLocksTabOptions(false);

	setControlEnabled(true , hTopPanel, IDC_REND2_TOP_RENDER);
	setControlEnabled(true , hTopPanel, IDC_REND2_TOP_RESUME);
	setControlEnabled(false, hTopPanel, IDC_REND2_TOP_STOP);
	ShowWindow(GetDlgItem(hTopPanel, IDC_REND2_TOP_ANIM), SW_HIDE);
	ShowWindow(GetDlgItem(hTopPanel, IDC_REND2_TOP_RENDER), SW_SHOW);
	EM2Busy * emb = GetEM2BusyInstance(GetDlgItem(hTopPanel, IDC_REND2_TOP_ANIM));
	if (emb)
		emb->stopAnim();

}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Render(HWND hTabRender, bool unlock)
{
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_WIDTH);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_HEIGHT);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_AA);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_LINK_RES);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_STOP_HOURS);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_STOP_MINUTES);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_STOP_SECONDS);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_STOP_AFTER);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_AUTOSAVE);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_AUTOSAVE_MINUTES);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_STUPID_AFTER_STOP);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_AFTER_STOP_SETTINGS);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_FILL);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_BUCKET_SAMPLES);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_BUCKET_WIDTH);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_BUCKET_HEIGHT);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_ENGINE);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_INT_SHADE);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_BDPT_EMITTER_HIT);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_BDPT_CAMERA_CONNECT);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_BDPT_MAX_REFL);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_PT_GI_SAMPLES);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_PT_RIS);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_PT_CAUSTIC_ROUGH);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_PT_FAKE_CAUSTICS);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_PT_DISABLE_2ND_CAUSTIC);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TIMESTAMP);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_NOX_STAMP);


	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_WIDTH);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_HEIGHT);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_ANTIALIASING);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_FILL);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_BUCKET_SAMPLES);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_BUCKET_WIDTH);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_BUCKET_HEIGHT);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_HOURS1);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_MINUTES1);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_SECONDS1);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_MINUTES2);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_ENGINE);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_GI_SAMPLES);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_RESAMPLE_IS);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_CAUSTICS_ROUGH);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_MAX_REFL);
	setControlEnabled(unlock, hTabRender, IDC_REND2_REND_TEXT_SHADE);

	GROUPBAR_ACTIVATE(GetDlgItem(hTabRender, IDC_REND2_REND_GR_RESOLUTION), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabRender, IDC_REND2_REND_GR_TIMERS), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabRender, IDC_REND2_REND_GR_ENGINE), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabRender, IDC_REND2_REND_GR_MISC), unlock);

}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Camera(HWND hTabCamera, bool unlock)
{
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_MB_ENABLED);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_LIST_CAMS);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_ISO);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_NUM_BLADES);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_MB_DURATION);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_MB_STILL);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_SHUTTER);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_FOCUS_DIST);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_FOCAL);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F15MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F20MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F24MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F28MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F35MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F50MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F85MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_F135MM);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_ROUND_BLADES);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_APERTURE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_BLADES_ANGLE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_BLADES_RADIUS);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_DOF_ON);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_RING_BALANCE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_RING_SIZE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_FLATTEN);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_VIGNETTE);

	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_MB_DURATION);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_ISO);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_SHUTTER);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_DISTANCE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_METERS);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_FOCAL);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_FOV_HORI);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_FOV_VERT);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_FOV_DIAG);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_FOV_HORI);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_FOV_VERT);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_FOV_DIAG);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_BLADES);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_RING_BALANCE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_FLATTEN);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_APERTURE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_RADIUS);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_ANGLE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_RING_SIZE);
	setControlEnabled(unlock, hTabCamera, IDC_REND2_CAM_TEXT_VIGNETTE);


	GROUPBAR_ACTIVATE(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_SELECT_CAMERA), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_MOTION_BLUR), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_EXPOSURE), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_FOCUS_OPTICS), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabCamera, IDC_REND2_CAM_GR_APERTURE_BOKEH), unlock);

}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Environment(HWND hTabEnv, bool unlock)
{
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_EARTH);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_SKY_ENABLED);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_LATITUDE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_LONGITUDE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_MONTH);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_DAY);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_HOUR);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_MINUTE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_GMT);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TURBIDITY);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_ALBEDO);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_SUN_ENABLED);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_SUN_LAYER2);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_SUN_SIZE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_MAP_ENABLED);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_MAP_TEXTURE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_MAP_POWER);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_MAP_LAYER);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_MAP_ROTATION);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_SKY_MODEL);

	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_LONGITUDE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_LATITUDE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_DEGREES1);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_DEGREES2);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_SUN_ALTITUDE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_SUN_AZIMUTH);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_SUN_ALTITUDE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_SUN_AZIMUTH);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_DATE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_TIME);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_GMT);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_MODEL);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_TURBIDITY);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_ALBEDO);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_SUN_SIZE);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_ROTATION);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_POWER);
	setControlEnabled(unlock, hTabEnv, IDC_REND2_ENV_TEXT_BLENDLAYER);

	GROUPBAR_ACTIVATE(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_LONG_LAT), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_DATE_AND_TIME), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_SKY_SYSTEM), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_ATMOSPHERE), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_SUN), unlock);
	GROUPBAR_ACTIVATE(GetDlgItem(hTabEnv, IDC_REND2_ENV_GR_ENV_MAP), unlock);
}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Layers(HWND hTabBlend, bool unlock)
{
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_1);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_2);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_3);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_4);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_5);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_6);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_7);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_8);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_9);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_10);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_11);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_12);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_13);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_14);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_15);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_ENABLED_16);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_1);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_2);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_3);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_4);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_5);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_6);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_7);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_8);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_9);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_10);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_11);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_12);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_13);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_14);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_15);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_MULTIPLIER_16);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_1);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_2);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_3);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_4);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_5);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_6);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_7);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_8);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_9);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_10);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_11);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_12);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_13);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_14);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_15);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_COLOR_16);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_1);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_2);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_3);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_4);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_5);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_6);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_7);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_8);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_9);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_10);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_11);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_12);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_13);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_14);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_15);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_TEXT_NAME_16);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_SHOW_DIRECT);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_SHOW_GI);
	setControlEnabled(unlock, hTabBlend, IDC_REND2_BLEND_RESET_ALL);
}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Post(HWND hTabPost, bool unlock)
{
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_REFRESH_NOW);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_AUTOREFRESH);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_EV);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_GI_COMP);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_VIGNETTE);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_TONE);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_GAMMA);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_CAMERA_RESPONSE);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_CAM_RESP_BROWSE);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_FILTER);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_HP_ENABLED);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_HP_RADIUS);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_HP_THRESHOLD);
	setControlEnabled(unlock, hTabPost, IDC_REND2_POST_HP_DENSITY);
}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Correction(HWND hTabCorr, bool unlock)
{
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_REFRESH_NOW);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_AUTOREFRESH);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_CURVE_CURVE);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_CURVE_RED);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_CURVE_GREEN);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_CURVE_BLUE);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_CURVE_LUMINANCE);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_CURVE_RESET);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_BRIGHTNESS);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_CONTRAST);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_SATURATION);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_RED);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_GREEN);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_BLUE);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_TEMPERATURE);
	setControlEnabled(unlock, hTabCorr, IDC_REND2_CORR_RESET);
}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2FakeDof(HWND hTabFDof, bool unlock)
{
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_REDRAW);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ABB_GRAPH);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ABB_LENS);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ABB_DEPTH);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ABB_ACHROMATIC);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ABB_RED);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ABB_GREEN);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ABB_BLUE);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_FAKEDOF_ON);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ALGORITHM1);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ALGORITHM2);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_ALGORITHM3);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_FOCUS_DIST);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_THREADS);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_QUALITY);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_PICK_FOCUS);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_SHOW_DOF_RANGE);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_SHOW_ZDEPTH);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_FAKEDOF_ON2);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_ROUND_BLADES);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_BLADES);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_RING_BALANCE);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_FLATTEN);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_APERTURE);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_RADIUS);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_ANGLE);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_RING_SIZE);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_VIGNETTE);
	setControlEnabled(unlock, hTabFDof, IDC_REND2_FDOF_AP_LOAD_FROM_CAMERA);
}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Effects(HWND hTabEff, bool unlock)
{
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_REDRAW);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_BLOOM_ON);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_BLOOM_POWER);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_BLOOM_AREA);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_BLOOM_THRESHOLD);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_BLOOM_ATT_ON);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_BLOOM_ATT_COLOR);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_GLARE_ON);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_GLARE_POWER);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_GLARE_AREA);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_GLARE_THRESHOLD);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_GLARE_DISPERSION);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_TXON_APERT_SHAPE);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_TXON_APERT_OBSTACLE);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_TX_APERT_SHAPE);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_TX_APERT_OBSTACLE);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_FOG_ON);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_FOG_COLOR);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_FOG_EXCL_BG);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_FOG_DENSITY);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_FOG_DIST);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_AERIAL_PERSP_ON);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_AERIAL_PERSP_SCALE);
	setControlEnabled(unlock, hTabEff, IDC_REND2_EFF_GRAIN);
}

//------------------------------------------------------------------------------------------------

void lockUnlockAllTab2Options(HWND hTabOptions, bool unlock)
{
	setControlEnabled(unlock, hTabOptions, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO);
	setControlEnabled(unlock, hTabOptions, IDC_REND2_OPT_TEX_RELOAD_NOW);
}

//------------------------------------------------------------------------------------------------

