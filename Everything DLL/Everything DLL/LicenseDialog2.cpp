#include "RendererMain2.h"
#include "noxfonts.h"
#include "EM2Controls.h"
#include "nox_license.h"


bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
extern HMODULE hDllModule;
HWND hLicParent = 0;

//------------------------------------------------------------------------------------------------

bool openLicense2Window(HWND hParent)
{
	int w = 600;
	int h = 700;
	hLicParent = hParent;

	HWND hLicense = CreateWindowEx(WS_EX_TOOLWINDOW, "NoxLicenseWindow", "NOX License", WS_VISIBLE | WS_CLIPCHILDREN,
										(GetSystemMetrics(SM_CXSCREEN)-w)/2, (GetSystemMetrics(SM_CYSCREEN)-h)/2, w, h-5, hParent, (HMENU)0, hDllModule, 0);

	return true;
}

//------------------------------------------------------------------------------------------------
extern char * noxlicensetext;

LRESULT CALLBACK License2WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;

	int tmarginup = 60;
	int tmargindown = 60;
	static int lic_height = 100;
	static int lic_pos = 0;
	static int lic_vis = 100;

	switch (message)
	{
		case WM_CREATE:
			{
				int dx = 412, dy = 512;
				RECT rect;
				GetClientRect(hWnd, &rect);
				dx = rect.right;
				dy = rect.bottom;

				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(dx, dy, 0,0, NGCOL_BG_LIGHT, NGCOL_BG_DARK);
				NOXFontManager * fonts = NOXFontManager::getInstance();

				HWND hTitle = CreateWindow("EM2Text", "NOX LICENSE", WS_CHILD | WS_VISIBLE,
											(rect.right-300)/2, 20, 300, 22, hWnd, (HMENU)12002, hDllModule, 0);
				EM2Text * emtitle = GetEM2TextInstance(hTitle);
				emtitle->setFont(fonts->opensans12, false);
				emtitle->bgImage = hBgBMP;
				emtitle->colText = RGB(210,210,210);
				emtitle->align = EM2Text::ALIGN_CENTER;
				updateControlBgShift(hTitle, 0, 0, emtitle->bgShiftX, emtitle->bgShiftY);

				HWND hOK = CreateWindow("EM2Button", "CLOSE", WS_CHILD | WS_VISIBLE,
											(rect.right-80)/2, 633, 80, 22, hWnd, (HMENU)12001, hDllModule, 0);
				EM2Button * embOK = GetEM2ButtonInstance(hOK);
				embOK->setFont(fonts->em2button, false);
				embOK->bgImage = hBgBMP;
				updateControlBgShift(hOK, 0, 0, embOK->bgShiftX, embOK->bgShiftY);


				// eval text size for scroll size
				HDC tthdc = GetWindowDC(hWnd);
				HFONT oldFont = (HFONT)SelectObject(tthdc, fonts->opensans10);
				RECT rectlic;
				DrawText(tthdc, noxlicensetext, (int)strlen(noxlicensetext), &rectlic, DT_CALCRECT);
				lic_height = rectlic.bottom;
				SelectObject(tthdc, oldFont);
				ReleaseDC(hWnd, tthdc);


				HWND hScroll = CreateWindow("EM2Scrollbar", "", WS_CHILD | WS_VISIBLE,
											dx-10-9, tmarginup, 9, rect.bottom-tmarginup-tmargindown, hWnd, (HMENU)12003, hDllModule, 0);
				EM2ScrollBar * emsc = GetEM2ScrollBarInstance(hScroll);
				lic_vis = rect.bottom-tmarginup-tmargindown;
				emsc->setWorkArea(lic_vis, lic_height);
				emsc->bgImage = hBgBMP;
				updateControlBgShift(hScroll, 0, 0, emsc->bgShiftX, emsc->bgShiftY);
				emsc->workarea_pos = 0;
				emsc->updateSliderFromWorkarea();

				EnableWindow(hLicParent, FALSE);
			}
			break;
		case WM_CLOSE:
			{
				DestroyWindow(hWnd);
			}
			break;
		case WM_DESTROY:
			{
				EnableWindow(hLicParent, TRUE);
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;

				GetClientRect(hWnd, &rect);
				hdc = BeginPaint(hWnd, &ps);

				// back buffer
				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				// copy background image
				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);


				///  ----- text hdc
				int text_pos_x = 20;
				int text_pos_y = tmarginup;
				int text_width = rect.right - 29 - text_pos_x;
				int text_height = rect.bottom - text_pos_y - tmargindown;
				HDC thdc2 = CreateCompatibleDC(hdc);			// text img buffer hdc
				HBITMAP backBMP2 = CreateCompatibleBitmap(hdc, text_width, text_height);
				HBITMAP oldBitmap2 = (HBITMAP)SelectObject(thdc2, backBMP2);

				BitBlt(thdc2, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, text_pos_x, text_pos_y, SRCCOPY);

				SetBkMode(thdc2, TRANSPARENT);
				SetTextColor(thdc2, RGB(207, 207, 207));
				HFONT hOldFont = (HFONT)SelectObject(thdc2, NOXFontManager::getInstance()->opensans10);
				RECT trect;
				GetClientRect(hWnd, &trect);
				trect.left = 0;
				trect.right = text_width;
				trect.top = -lic_pos;//-300;//-license_pos;
				DrawText(thdc2, noxlicensetext, (int)strlen(noxlicensetext), &trect, DT_NOCLIP);
				BitBlt(bhdc, text_pos_x, text_pos_y, text_width, text_height, thdc2, 0, 0, SRCCOPY);
				SelectObject(thdc2, hOldFont);
				SelectObject(thdc2, oldBitmap2);
				DeleteDC(thdc2);

				// release bg image hdc
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				// done
				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_MOUSEWHEEL:
			{
				RECT crect;
				GetClientRect(hWnd, &crect);
				int zDelta = (int)wParam;
				if (zDelta>0)
				{
					lic_pos = max(lic_pos-20, 0);
				}
				else
				{
					lic_pos = min(lic_pos+20, lic_height-lic_vis);
				}
				InvalidateRect(hWnd, NULL, false);

				EM2ScrollBar * emsc = GetEM2ScrollBarInstance(GetDlgItem(hWnd, 12003));
				emsc->workarea_pos = lic_pos;
				emsc->updateSliderFromWorkarea();
				InvalidateRect(emsc->hwnd, NULL, false);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case 12003:	// scrollbar
						{
							if (wmEvent!=SB2_POS_CHANGED)
								break;
							EM2ScrollBar * emsc = GetEM2ScrollBarInstance(GetDlgItem(hWnd, 12003));

							lic_pos = emsc->workarea_pos;

							InvalidateRect(hWnd, NULL, false);

						}
						break;
					case 12001:	// close
						{
							if (wmEvent!=BN_CLICKED)
								break;
							HWND hParent = GetParent(hWnd);
							//hParent = RendererMain2::getInstance()->getHWND();
							hParent = hLicParent;
							DestroyWindow(hWnd);
							SetFocus(hParent);
							ShowWindow(hParent, SW_SHOWNORMAL);
							UpdateWindow(hParent);
						}
						break;
				}
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void InitLicense2WindowClass()
{
	WNDCLASSEX wc;
	wc.cbSize         = sizeof(wc);
	wc.lpszClassName  = "NoxLicenseWindow";
	wc.hInstance      = hDllModule;
	wc.lpfnWndProc    = License2WndProc;
	wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
	wc.hIcon          = 0;
	wc.lpszMenuName   = 0;
	wc.hbrBackground  = (HBRUSH)0;
	wc.style          = 0;
	wc.cbClsExtra     = 0;
	wc.cbWndExtra     = 0;
	wc.hIconSm        = 0;
	RegisterClassEx(&wc);
}

//------------------------------------------------------------------------------------------------
