#ifndef __NOX_RENDERER_H__
#define __NOX_RENDERER_H__

#include "NOXPlugin.h"
#include "NOXExporter.h"

#define NOXRenderer_CLASS_ID	Class_ID(0x63702664, 0x3dba3158)
#define NOXRend_Tab_Class_ID	Class_ID(0x2ae017c8, 0x387a6254)

class NOXRenderer : public Renderer 
{
public:
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	Class_ID ClassID() {return NOXRenderer_CLASS_ID;}		
	SClass_ID SuperClassID() { return RENDERER_CLASS_ID; }
	void GetClassName(TSTR& s) {s = GetString(IDS_NOX_REND_CLASS_NAME);}

	RefTargetHandle Clone(RemapDir &remap);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
		PartID& partID,  RefMessage message);


	int NumSubs() { return 1; }
	TSTR SubAnimName(int i) { return GetString(IDS_PARAMS_SOME_NEEDED_JUNK_REND); }
	Animatable* SubAnim(int i) { return NULL; }

	int NumRefs() { return 1; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);// { }

	int	NumParamBlocks() { return 0; }
	IParamBlock2* GetParamBlock(int i) { return NULL; }
	IParamBlock2* GetParamBlockByID(BlockID id) { return NULL; }

	void DeleteThis() { delete this; }		

	int Open(INode *scene, INode *vnode, ViewParams *viewPar, RendParams &rp, HWND hwnd, 
				DefaultLight* defaultLights=NULL, int numDefLights=0, RendProgressCallback* prog = NULL);
	void Close(	HWND hwnd, RendProgressCallback* prog = NULL );
	int Render(TimeValue t, Bitmap *tobm, FrameRendParams &frp, HWND hwnd, RendProgressCallback *prog=NULL, ViewParams *viewPar=NULL);
	RendParamDlg *CreateParamDialog(IRendParams *ir,BOOL prog=FALSE);
	void ResetParams();

	bool inMatEditor;
	Mtl * matPrevCurMat;

	void renderMaterialPreview(Bitmap *tobm);
	void renderMaterialError(Bitmap *tobm, int error);

	XMLScene xScene;
	BinaryScenePlugin bScene;
	char * filename;
	void generateFilename(bool isXML=false);

	char * scene_name;
	char * scene_author_name;
	char * scene_author_www;
	char * scene_author_email;

	bool overrideMatEnabled;
	Mtl * overrideMaterial;

	float mb_duration;
	bool mb_on;

	char * filename_post;
	char * filename_blend;
	char * filename_final;
	bool use_file_post;
	bool use_file_blend;
	bool use_file_final;
	unsigned short engine;

	NOXRenderer();
	~NOXRenderer();
};


class NOXRendParamDlg : public RendParamDlg 
{
public:
	NOXRenderer *rend;
	IRendParams *ir;
	static HWND hPanel;
	static HWND hRPanel;
	BOOL prog;
	HFONT hFont;

	NOXRendParamDlg(NOXRenderer *r,IRendParams *i,BOOL prog, HINSTANCE hInstance);
	~NOXRendParamDlg();
	void AcceptParams();
	void RejectParams();
	void DeleteThis() {delete this;}
	void InitParamDialog(HWND hWnd);
	void InitProgDialog(HWND hWnd);
};


#endif
