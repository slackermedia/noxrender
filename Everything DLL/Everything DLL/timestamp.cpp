#include <Windows.h>
#include <stdio.h>
#include "raytracer.h"


//---------------------------------------------------------------------------------------

char * formatTime(int sec)
{
	int aa,s,m,h,d;
	aa = sec;
	s = aa % 60;
	aa = aa / 60;
	m = aa % 60;
	aa = aa / 60;
	h = aa % 24;
	d = aa / 24;
	char cd[16];
	char cm[8];
	char cs[8];
	char ch[8];
	if (d > 0)
		sprintf_s(cd, 16,"%d days, ", d);
	else
		cd[0] = 0;
	if (h>9)
		sprintf_s(ch, 8,"%d", h);
	else
		sprintf_s(ch, 8,"0%d", h);
	if (m>9)
		sprintf_s(cm, 8,"%d", m);
	else
		sprintf_s(cm, 8,"0%d", m);
	if (s>9)
		sprintf_s(cs, 8,"%d", s);
	else
		sprintf_s(cs, 8,"0%d", s);

	char * res = (char*)malloc(64);
	if (res)
		sprintf_s(res, 64, "%s%s:%s:%s", cd,ch,cm,cs);
	return res;
}

//---------------------------------------------------------------------------------------

char * createTimeStamp()
{
	char * procinfo = NULL;
	char * meminfo = NULL;
	char * timeinfo = NULL;
	procinfo = getProcessorInfo(true);
	meminfo = getFormatedMemorySize();
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Camera * cam = sc->cameras[sc->sscene.activeCamera];
	int sec = cam->getRendTime(GetTickCount());
	timeinfo = formatTime(sec);

	char * res = (char *)malloc(512);
	if (res)
		#ifdef _WIN64
			sprintf_s(res, 512, "version %d.%.2d beta 64-bit\n%s, %s, time: %s", NOX_VER_MAJOR, NOX_VER_MINOR, procinfo, meminfo, timeinfo); 
		#else
			sprintf_s(res, 512, "version %d.%.2d beta 32-bit\n%s, %s, time: %s", NOX_VER_MAJOR, NOX_VER_MINOR, procinfo, meminfo, timeinfo); 
		#endif

	if (procinfo)
		free(procinfo);
	if (meminfo)
		free(meminfo);
	if (timeinfo)
		free(timeinfo);

	return res;
}

//---------------------------------------------------------------------------------------
