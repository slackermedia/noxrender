#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "raytracer.h"
#include "regions.h"
#include "QuasiRandom.h"
#include "log.h"
#include <float.h>

unsigned int Camera::maxHalton = 0;
extern HWND hRendererMain;

Camera::Camera()
{
	threadID = 0;
	angle = 54;
	width = 640;
	height = 480;
	aa = 2;

	dofOnTemp = true;

	position = Point3d(0,0,0);
	direction = Vector3d(0,0,-1);
	upDir = Vector3d(0,1,0);

	depthBuf = new FloatBuffer();
	imgBuff = new ImageBuffer();
	iReg = NULL;

	clipNear = 0.0001f;
	clipFar = BIGFLOAT;
	startTick = GetTickCount();
	rendTime = 0;
	nowRendering = false;

	maxHalton = 1;
	name = NULL;

	ISO = 100;
	shutter = 50;
	aperture = 4.0f;
	autoexposure = false;
	autoexposureType = 2;
	autoFocus = false;
	focusDist = 100;
	afMode = 0;

	angle = 360.0f/PI*atan(18.0f/35);
	apBladesNum = 5;
	apBladesAngle = 0;
	apBladesRound = false;
	apBladesRadius = 1;
	glareOK = false;
	depthOK = false;
	pinhole = false;
	scaleAperture = 5;
	if (shutter < 1.0f/30.0f)
		shutter = 1.0f/30.0f;
	mplApSh = 1.0f/shutter/aperture/aperture/2.5f;

	blendBits = 0;
	gi_type = GI_TYPE_PT;
	for (int i=0; i<16; i++)
	{
		blendWeights[i] = 1.0f;
		blendBuffDirect[i].dontDeleteMeInDestructor = true;
		blendBuffGI[i].dontDeleteMeInDestructor = true;
	}

	fMod.numThreads = ThreadManager::getProcessorsNumber();
	marsagliaPtr = NULL;
	buckets = NULL;

	mb_on = false;
	mb_velocity = Vector3d(0,0,0);
	mb_rot_vel = Vector3d(0,0,0);
	mb_rot_angle = 0.0f;

	bucket_samples = 50000;
	cur_bucket_sample = 0;
	cur_bucket_i = 0; cur_bucket_j = 0;
	cur_bucket_x = 0; cur_bucket_y = 0;
	cur_bucket_w = 20; cur_bucket_h = 20;
	bucket_hwnd_image = 0;
}

Camera::~Camera()
{
	// dont delete buffers - camera is copied
}

bool Camera::initializeCamera(bool withErase)
{
	if (withErase || !staticRegion.regions)
		staticRegion.createRegions(width, height);

	X2 = width * 0.5f;
	Y2 = height * 0.5f;
	halton = rand();

	depthOK = false;

	cotg = (float)tan(angle*0.5f*PI/180.0f);
	if (cotg < 0.001f)
		cotg = 0.001f;
	if (cotg > 1000)
		cotg = 1000;
	cotg = 1.0f/cotg * X2;
	
	evalConeCosSize();
	float anglex = angle*PI/180.0f;
	float angley = 2*atan(tan(anglex/2)*height/width);
	coneSize = 4*asin(sin(anglex/2)*sin(angley/2));
	float anglepix = 2*atan(tan(anglex/2)/width);
	conePix = 4*asin(sin(anglepix/2)*sin(anglepix/2));

	char buf[256];
	sprintf_s(buf, 256, "cone size: %f  cos size: %f   conepix: %f", coneSize, coneCosSize, conePix);
	Logger::add(buf);

	double testsum = 0;
	double testsum_o = 0;
	for (int i=0; i<width; i++)
		for (int j=0; j<height; j++)
		{
			Point3d temppoint;
			Vector3d tdir = getDirection(i,j, temppoint, 0);
			float kos = tdir * direction;
			testsum += conePix*kos*kos*kos;
			testsum_o += conePix;
		}
	sprintf_s(buf, 256, "cone verification size: %f ... %f", testsum, testsum_o);
	Logger::add(buf);

	if (!imgBuff)
		imgBuff = new ImageBuffer();

	if (withErase  ||   !imgBuff->imgBuf   ||   imgBuff->width!=width*aa   ||   imgBuff->height!=height*aa)
	{
		staticRegion.createRegions(width, height);
		staticRegion.clearRegions();
		if (!imgBuff->allocBuffer(width*aa, height*aa))
			return false;
		imgBuff->clearBuffer();
	}

	Raytracer * rtr = Raytracer::getInstance();
	blendBits = rtr->curScenePtr->evalBlendBits();

	if (!blendBits)
	{
		MessageBox(0, "Error!\nNo emitter found.", "Error", 0);
		return false;
	}

	for (int i=0; i<16; i++)
	{
		if (blendBits&(1<<i))
		{
			if (withErase  ||   !blendBuffDirect[i].imgBuf   ||   blendBuffDirect[i].width!=width*aa   ||   blendBuffDirect[i].height!=height*aa)
			{
				if (!blendBuffDirect[i].allocBuffer(width*aa, height*aa))
					return false;
				blendBuffDirect[i].clearBuffer();
			}
			if (withErase  ||   !blendBuffGI[i].imgBuf   ||   blendBuffGI[i].width!=width*aa   ||   blendBuffGI[i].height!=height*aa)
			{
				if (!blendBuffGI[i].allocBuffer(width*aa, height*aa))
					return false;
				blendBuffGI[i].clearBuffer();
			}
		}
		else
		{
			blendBuffGI[i].freeBuffer();
			blendBuffDirect[i].freeBuffer();
		}
	}


	if (withErase  ||   !depthBuf->fbuf   ||   depthBuf->width!=width*aa   ||   depthBuf->height!=height*aa)
	{
		if (!depthBuf->allocBuffer(width*aa, height*aa, true, true))
			return false;
		depthBuf->clearBuffer();
	}

	buckets = new Bucket();
	bucket_samples = rtr->curScenePtr->sscene.samples_per_pixel_in_pass * rtr->curScenePtr->sscene.bucket_size_x * rtr->curScenePtr->sscene.bucket_size_y;
	buckets->reset(width, height, rtr->curScenePtr->sscene.bucket_size_x, rtr->curScenePtr->sscene.bucket_size_y);
	buckets->stopOnLast = rtr->curScenePtr->sscene.stop_after_pass;

	if (focusDist <= 0.0f)
		pinhole = true;
	else
		pinhole = false;

	float focal = 0.5f * 36 / tan(angle * PI / 360.0f);
	scaleAperture = focal/aperture/1000 /2;

	if (!prepareInvertedMatrix())
	{
		matrixOK = false;
		MessageBox(0, "Error.\nCan't eval inverted matrix for camera perspective.", "Error", 0);
		return false;
	}
	else 
		matrixOK = true;

	if (shutter < 1.0f/30.0f)
		shutter = 1.0f/30.0f;
	mplApSh = 1.0f/shutter/aperture/aperture/2.5f;

	diaphShape.initializeDiaphragm(apBladesNum, aperture, apBladesAngle, apBladesRound, apBladesRadius);

	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_PATH_TRACING)
		gi_type = GI_TYPE_PT;
	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_INTERSECTIONS)
		gi_type = GI_TYPE_PT;
	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_BIDIRECTIONAL_PATH_TRACING)
		gi_type = GI_TYPE_BDPT;
	if (rtr->curScenePtr->sscene.giMethod == Raytracer::GI_METHOD_METROPOLIS)
		gi_type = GI_TYPE_BDPT;

	lastRendTime = 0;

	return true;
}

bool Camera::releaseAllBuffers()
{
	if (depthBuf)
	{
		depthBuf->freeBuffer();
	}
	if (imgBuff)
	{
		imgBuff->freeBuffer();
		delete imgBuff;
		imgBuff = NULL;
	}
	
	for (int i=0; i<16; i++)
	{
		blendBuffGI[i].freeBuffer();
		blendBuffDirect[i].freeBuffer();
	}

	return true;
}


int Camera::getRendTime(DWORD tickCount, bool whole)
{
	int ss = (tickCount - startTick)/1000;
	ss = max(0, ss);
	if (nowRendering)
		if (whole)
			return rendTime + ss;
		else
			return ss;
	else
		return rendTime;
}

float Camera::fovDiagToLength(float fov)
{	// 43.2666 is diagonal length of film
	return 0.5f * 43.266615305567871517430655209646f / tan(fov * PI / 360.0f);
}

float Camera::lengthToFovDiag(float leng)
{
	return 360.0f/PI*atan(21.633307652783935758715327604823f / leng);
}

float Camera::lengthToFovHori(float leng)
{
	return 360.0f/PI*atan(18.0f / leng);
}

bool Camera::setLength(float leng)
{
	angle = lengthToFovDiag(leng);
	return true;
}

bool Camera::setLengthHori(float leng)
{
	angle = lengthToFovHori(leng);
	return true;
}

bool Camera::setLengthfromFovHori36(float fovHori)
{
	angle = fovHori;
	return true;
}

void Camera::evalConeCosSize()
{
	Vector3d tvec;
	float fx, fy;
	float sum1=0.0f;
	float sum2=0.0f;
	float sum3=0.0f;
	float sum4=0.0f;
	for (int y=0; y<height; y++)
	{
		fy = y/(float)height;
		for (int x=0; x<width; x++)
		{
			fx = x/(float)width;
			tvec.x = fx - X2;
			tvec.y = Y2 - fy;
			tvec.z = cotg;
			tvec.normalize();
			sum1 += tvec.z;
			sum2 += tvec.z*tvec.z;
			sum3 += 1.0f/tvec.z;
			sum4 += 1.0f/tvec.z/tvec.z;
		}
	}
	int wh = width * height;
	sum1 /= wh;
	sum2 /= wh;
	sum3 /= wh;
	sum4 /= wh;
	coneCosSize = sum1;
	char buf[64];
	sprintf_s(buf, 64, "s1=%f   s2=%f   s3=%f   s4=%f", sum1, sum2, sum3, sum4);
	Logger::add(buf);
}

Vector3d Camera::getDirection(float x, float y, Point3d &sPos, float sample_time)
{
	Vector3d res;
	Vector3d tvec;

	tvec.x = x/aa - X2;
	tvec.y = Y2 - y/aa;
	tvec.z = cotg;
	if (!tvec.normalize())
		return direction.getNormalized();

	if (0)
	{
		res.normalize();
		float t = -0.1f;
		float fprim = 35;
		tvec.z = tvec.z*(1+t/fprim) + t;
		res.normalize();
	}

	res = direction * tvec.z + rightDir * tvec.x + upDir * tvec.y;
	res.normalize();

	Matrix4d mb_mat;
	mb_mat.setIdentity();
	if (mb_on)
	{
		if (mb_rot_angle!=0.0f)
			mb_mat.setRotation(mb_rot_vel, mb_rot_angle*-sample_time);
	}

	if (!dofOnTemp)
	{
		sPos = position;

		if (mb_on)
		{
			sPos = sPos + direction*-mb_velocity.y*sample_time + rightDir*mb_velocity.x*sample_time + upDir*-mb_velocity.z*sample_time;
			Vector3d tvec2 = mb_mat*tvec;
			res = direction * tvec2.z + rightDir * tvec2.x + upDir * tvec2.y;
		}
		return res;
	}

	if (pinhole)
	{
		sPos = position;
		return res;
	}

	bool vignetteCut = true;
	float u,v;
	int watchdog = 0;

	while (vignetteCut)
	{
		if (watchdog++>100)
			break;
		randomAperturePoint(u,v);
		int wwym = max(width, height);
		float wx = (x/aa - X2) / (float)wwym;
		float wy = (Y2 - y/aa) / (float)wwym;

		float vign = diaphSampler.bokehVignette;
		if (vign!=0)
		{
			float px = u-wx*vign*4;
			float py = v-wy*vign*4;
			if (px*px+py*py<1)
				vignetteCut = false;
			else
				continue;
		}
		else
			vignetteCut = false;

		float flatten = diaphSampler.bokehFlatten;
		if (flatten != 0.0f)
		{
			float sss = sqrt(wx*wx+wy*wy);
			if (_isnan(sss)  ||  sss==0)
				sss = 1;
			float nwx = wx / sss;
			float nwy = wy / sss;
			float mn = (nwx*nwx+nwy*nwy);
			float r = -(nwx*u+nwy*v) / mn;
			if (_isnan(r))
				r = 0;
			r *= flatten;
			u = u + wx*r;
			v = v + wy*r;
		}
	}

	u *= scaleAperture;
	v *= scaleAperture;

	float pscale = Raytracer::getInstance()->curScenePtr->sscene.perScaleScene;
	u *= pscale;
	v *= pscale;

	Point3d focusPoint = position   +   res * focusDist * (1.0f/tvec.z);	// go prosto
	Point3d startPoint = position   +   rightDir * u   +   upDir * v;

	sPos = startPoint;
	Vector3d newres = focusPoint - startPoint;

	newres.normalize();

	return newres;
}

float Camera::evalDistMult(float x, float y)
{
	float xx = x/aa - X2;
	float yy = y/aa - Y2;
	float c = cotg*cotg + xx*xx + yy*yy;
	c = sqrt(c);
	return cotg/c;
}

bool Camera::getCoordsFromDir(const Vector3d &dir, const Point3d &dofPoint, float &x, float &y)
{
	if (!matrixOK)
		return false;

	Vector3d ddir = dir;
	if (dofOnTemp)
	{
		float kkos = dir * direction;
		Point3d pdof = dofPoint + dir*(focusDist/kkos);
		ddir = pdof - position;
		ddir.normalize();
	}

	Vector3d c = iMat*ddir;

	if (fabs(c.z) < 0.001f)
		return false;

	float z = cotg/c.z;
	x = ( c.x * z + X2 );
	y = ( Y2 - c.y * z );

	return true;
}

bool Camera::getCoordsFromDirReturnInFrustum(const Vector3d &dir, const Point3d &dofPoint, float &x, float &y)
{
	if (!matrixOK)
		return false;

	Vector3d ddir = dir;
	if (dofOnTemp)
	{
		float kkos = dir * direction;
		Point3d pdof = dofPoint + dir*(focusDist/kkos);
		ddir = pdof - position;
		ddir.normalize();
	}

	Vector3d c = iMat*ddir;

	if (fabs(c.z) < 0.001f)
		return false;

	if (dir*direction < 0)
		return false;

	float z = cotg/c.z;
	x = ( c.x * z + X2 );
	y = ( Y2 - c.y * z );

	x*=aa;
	y*=aa;

	return (x>=0 && x<width*aa && y>=0 && y<height*aa);
}

bool Camera::prepareInvertedMatrix()
{
	Matrix3d tMat = Matrix3d(rightDir.toPoint(), upDir.toPoint(), direction.toPoint(),  Matrix3d::BY_COLS);
	if (!tMat.invert())
		return false;
	iMat = tMat;
	return true;
}

void Camera::randomNewCoords(float &x, float &y, int type, bool &noMore, bool isDirect)
{
	if (iReg  &&  isDirect)
	{
		if (iReg->regions  &&  iReg->regionsExists)
		{
			float xx,yy;
			if (iReg->randomPointFromRegions(xx,yy))
			{
				x = xx * width  * aa - 0.45f;
				y = yy * height * aa - 0.45f;
				return;
			}
		}
	}

	switch (type)
	{
		case RND_TYPE_SCANLINE:
			{
				scanline.getNextPoint(x, y);
			}
			break;
		case RND_TYPE_HALTON:
			{
				x = (float)(getHalton(halton, 2));
				y = (float)(getHalton(halton, 3));
				x *= width  * aa;
				y *= height * aa;
				halton++;
				if (halton % 100 == 0)
					maxHalton = max(maxHalton, halton);
			}
			break;
		case RND_TYPE_MARSAGLIA:
			{
				if (marsagliaPtr)
				{
					x = (float)marsagliaPtr->getRandom()/(float)(unsigned int)MAXMARS;
					y = (float)marsagliaPtr->getRandom()/(float)(unsigned int)MAXMARS;
					y = (float)marsagliaPtr->getRandom()/(float)(unsigned int)MAXMARS;					/// two times
					float nothing = (float)marsagliaPtr->getRandom()/(float)(unsigned int)MAXMARS;		/// for reset 
					if (x==0.0f  &&  y==0.0f)
						Logger::add("marsaglia stucked");
				}
				else
				{
					x = (float)getMarsaglia()/(float)(unsigned int)MAXMARS;
					y = (float)getMarsaglia()/(float)(unsigned int)MAXMARS;
					y = (float)getMarsaglia()/(float)(unsigned int)MAXMARS;								/// two times
					float nothing = (float)getMarsaglia()/(float)(unsigned int)MAXMARS;					/// for reset 
					float nothing2 = (float)getMarsaglia()/(float)(unsigned int)MAXMARS;				/// for reset 
					float nothing3 = (float)getMarsaglia()/(float)(unsigned int)MAXMARS;				/// for reset 
				}
				x *= width  * aa;
				y *= height * aa;
			}
			break;
		case RND_TYPE_RAND:
			{
				x = rand()/(float)RAND_MAX;
				y = rand()/(float)RAND_MAX;
				x *= width  * aa;
				y *= height * aa;
			}
			break;
		case RND_TYPE_RAND_S:
			{
				unsigned int rrr;
				while(rand_s(&rrr));
				x = rrr / (float)UINT_MAX;
				while(rand_s(&rrr));
				y = rrr / (float)UINT_MAX;
				x *= width  * aa;
				y *= height * aa;
			}
			break;
		case RND_TYPE_LATTICE:
			{
				lattice.randomPoint(x, y);
				x *= width  * aa;
				y *= height * aa;
			}
			break;
		case RND_TYPE_BUCKETS:
			{
				if (!buckets)
				{
					lattice.randomPoint(x, y);
					x *= width  * aa;
					y *= height * aa;
					return;
				}

				if (cur_bucket_sample==0)
				{
					int x1,x2,y1,y2;
					buckets->getCoordinates(cur_bucket_i, cur_bucket_j, x1,y1, x2,y2);
					RedrawRegion rgn;
					rgn.left = x1;
					rgn.right = x2;
					rgn.top = y1;
					rgn.bottom = y2;
					rgn.hwnd = bucket_hwnd_image;

					bool gottile = buckets->getNewTile(cur_bucket_i, cur_bucket_j);
					if (!gottile)
					{
						noMore = true;
						return;
					}
					buckets->getCoordinates(cur_bucket_i, cur_bucket_j, x1,y1, x2,y2);
					cur_bucket_x = x1;
					cur_bucket_y = y1;
					cur_bucket_w = x2 - cur_bucket_x + 1;
					cur_bucket_h = y2 - cur_bucket_y + 1;
					if (buckets->coordsThreads  &&  buckets->nThr>(int)threadID)
					{
						buckets->coordsThreads[threadID*4+0] = x1;
						buckets->coordsThreads[threadID*4+1] = y1;
						buckets->coordsThreads[threadID*4+2] = x2;
						buckets->coordsThreads[threadID*4+3] = y2;
					}

					runRefreshRegionThread(rgn);
					bucket_samples_cur = (int)((long long)bucket_samples*(long long)(x2-x1+1)*(long long)(y2-y1+1)/(long long)buckets->tile_size_x/(long long)buckets->tile_size_y);
				}

				#ifdef RAND_PER_THREAD
					RandomGenerator * rg = Raytracer::getRandomGeneratorForThread();
					x = rg->getRandomFloat()*cur_bucket_w + cur_bucket_x;
					y = rg->getRandomFloat()*cur_bucket_h + cur_bucket_y;
				#else
					x = rand()/(float)RAND_MAX*cur_bucket_w + cur_bucket_x;
					y = rand()/(float)RAND_MAX*cur_bucket_h + cur_bucket_y;
				#endif
				x *= aa;
				y *= aa;

				cur_bucket_sample++;
				if (cur_bucket_sample == bucket_samples_cur)
					cur_bucket_sample = 0;
			}
			break;
	}
}

void Camera::getHaltonCoords(float &x, float &y, bool isDirect)
{
	if (iReg  &&  isDirect)
		if (iReg->regions  &&  iReg->regionsExists)
		{
			float xx,yy;
			if (iReg->randomPointFromRegions(xx,yy))
			{
				x = xx * width  * aa - 0.45f;
				y = yy * height * aa - 0.45f;
				return;
			}
		}

	lattice.randomPoint(x, y);

	x *= width  * aa;
	y *= height * aa;

	halton++;
	if (halton % 100 == 0)
	{
		maxHalton = max(maxHalton, halton);
	}
}


Camera * Camera::copy()
{
	Camera * cam;
	cam = new Camera();
	
	*cam = *this;
	cam->name = NULL;
	cam->assignName(name);
	cam->imgBuff = new ImageBuffer();
	cam->imgBuff->allocBuffer(imgBuff->width, imgBuff->height);
	cam->imgBuff->clearBuffer();

	cam->staticRegion.createRegions(width, height);
	cam->staticRegion.overallHits =  staticRegion.overallHits;
	cam->staticRegion.makeRegionsList();

	return cam;
}

bool Camera::assignName(char * nname)
{
	if (!nname)
		return false;

	int nsize = (int)strlen(nname);
	if (nsize < 1)
		return false;
	
	char * newName = (char*)malloc(nsize+1);
	if (!newName)
		return false;

	sprintf_s(newName, nsize+1, "%s", nname);
	if (name)
		free(name);
	name  = newName;

	return true;
}

void findStraightEquation(float x1, float y1, float x2, float y2, float &A, float &B, float &C)
{
	if (y1==y2)
	{
		A = 0;
		B = 1;
		C = -y1;
		return;
	}
	if (x1==x2)
	{
		A = 1;
		B = 0;
		C = -x1;
		return;
	}

	float m = x1*y2 - x2*y1;
	if (m==0)
	{
		C = 0;
		A = 1;
		B = -x1/y1;
		return;
	}

	B = (x2-x1)/m;
	if (x1==0)
		A = -(B*y2+1)/x2;
	else
		A = -(B*y1+1)/x1;
	C = 1;
}

bool Camera::createApertureShapePreview(ImageByteBuffer * img)
{
	// img must be allocated
	bool smooth = true;
	if (!img)
		return false;
	if (!img->imgBuf)
		return false;
	if (img->width < 1   ||   img->height < 1)
		return false;


	float apert = max(1.0f, fMod.aperture);
	int bladesNum = max(5, min(fMod.bladesNum, 15));
	float bladesRadius = max(1, min(fMod.bladesRadius, 10)) / apert;
	float bladesAngle= max(0, min(fMod.bladesAngle, 360));
	bool bladesRound = fMod.roundBlades;
	float rAngle = bladesAngle;
	float tAngle = 360.0f/bladesNum;
	while (rAngle >= tAngle)
		rAngle -= tAngle;

	int w = img->width;
	int h = img->height;
	float sx = w*0.5f;
	float sy = h*0.5f;
	float R = w>h ? h/2.0f : w/2.0f;
	float r = R/apert;

	float vert[32];
	float vec[32];
	float cx[16];
	float cy[16];
	float angles[16];

	for (int i=0; i<bladesNum; i++)
	{
		float k = PI*2*i/bladesNum + PI*rAngle/180.0f;
		vert[i*2+0] = cos(k)*r + sx; 
		vert[i*2+1] = sin(k)*r + sy; 
	}
	vert[bladesNum*2  ] = vert[0];
	vert[bladesNum*2+1] = vert[1];

	if (bladesRound)
	{
		float c1 = cos(PI/bladesNum) * r;
		float c2 = sin(PI/bladesNum) * r;
		float beta = asin(c2/R/bladesRadius);
		float c3 = cos(beta)*R*bladesRadius;
		float c4 = c3-c1;

		for (int i=0; i<bladesNum; i++)
		{
			float k = PI*2*i/bladesNum + PI*rAngle/180.0f;
			float h = PI*2*(i+0.5f)/bladesNum + PI*rAngle/180.0f;
			angles[i] = k;
			cx[i] = -cos(h)*c4 + sx; 
			cy[i] = -sin(h)*c4 + sy; 
		}
		vert[bladesNum*2  ] = vert[0];
		vert[bladesNum*2+1] = vert[1];
		angles[bladesNum] = angles[0]+2*PI;


		for (int y=0; y<h; y++)
		{
			Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 1) ...", y*100.0f/h);
			for (int x=0; x<w; x++)
			{
				int b = 0;
				double tx = x-sx;
				double ty = y-sy;
				double k = atan2(ty/R,tx/R) ; 
				if (k < 0) 
					k += 2*PI;
				if (k < angles[0])
					k += 2*PI;

				for (int i=0; i<bladesNum; i++)
				{
					if (k>angles[i] && k<angles[i+1])
						b = i;
				}
				float rr = (cx[b]-x)*(cx[b]-x)  +  (cy[b]-y)*(cy[b]-y);
				if (rr < bladesRadius*bladesRadius*R*R)					
					if (smooth   &&   (sqrt(rr) > bladesRadius*R-1))
					{
						float s = bladesRadius*R-sqrt(rr);
						int ss = (int)(s*255.0f);
						img->imgBuf[y][x] = RGB(ss,ss,ss);
					}
					else
						img->imgBuf[y][x] = RGB(255,255,255);
				else
					img->imgBuf[y][x] = RGB(0,0,0);
			}
		}
	}
	else
	{
		float A[16];
		float B[16];
		float C[16];
		float sAB[16];
		for (int i=0; i<bladesNum; i++)
		{
			vec[i*2+0] = vert[i*2+2+0] - vert[i*2+0];
			vec[i*2+1] = vert[i*2+2+1] - vert[i*2+1];
			findStraightEquation(vert[i*2+0], vert[i*2+1], vert[i*2+2+0], vert[i*2+2+1], A[i], B[i], C[i]);
			sAB[i] = sqrt(A[i]*A[i]+B[i]*B[i]);
		}
		A[bladesNum] = A[0];
		B[bladesNum] = B[0];
		sAB[bladesNum] = sAB[0];

		for (int y=0; y<h; y++)
		{
			Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 1) ...", y*100.0f/h);
			for (int x=0; x<w; x++)
			{
				bool inside = true;
				for (int i=0; i<bladesNum; i++)
				{
					float vx = x - vert[i*2+0];
					float vy = y - vert[i*2+1];
					float z = vx*vec[i*2+1] - vy*vec[i*2+0];
					if (z>0)
						inside = false;
				}
				if (inside)
				{
					float m = 10;
					for (int i=0; i<bladesNum; i++)
					{
						float o = fabs(A[i]*x + B[i]*y + C[i])/sAB[i];
						m = min (m, o);
					}
					if (m<1)
					{
						int s = (int)(255.0f*m);
						img->imgBuf[y][x] = RGB(s,s,s);
					}
					else
						img->imgBuf[y][x] = RGB(255,255,255);
				}
				else
					img->imgBuf[y][x] = RGB(0,0,0);
			}
		}
	}

	return true;
}

void Camera::createDiaphragmAndObstacleImage(int widthheight)
{
	int dx,dy,ox,oy;
	dx=dy=ox=oy=0;

	if (texDiaphragm.isValid())
	{
		dx = texDiaphragm.bmp.getWidth();
		dy = texDiaphragm.bmp.getHeight();
	}
	if (texObstacle.isValid())
	{
		ox = texObstacle.bmp.getWidth();
		oy = texObstacle.bmp.getHeight();
	}

	int imx = imgBuff->width/aa*2;
	int imy = imgBuff->height/aa*2;

	int m = max(max(max(dx,dy), ox), oy);
	m = max(max(imx, imy), m);

	if (widthheight<=0)
	{
		if (m < 256)
			m = 256;
	}
	else
	{
		m = max(64, widthheight);
	}
	int d = 64;
	while (d < m)
		d *= 2;

	Texture tDiaph, tObst;
	tDiaph.allocByteBuffer(d,d);
	tObst.allocByteBuffer(d,d);

	if (texDiaphragm.isValid()   &&   fMod.glareTextureApertureOn)
	{
		float x1,x2,y1,y2;
		float wx1,wx2,wy1,wy2;
		float mx = dx/(float)d;
		float my = dy/(float)d;
		
		for (int y=0; y<d; y++)
		{
			for (int x=0; x<d; x++)
			{
				x1 = x*mx;
				x2 = (x+1)*mx;
				y1 = y*my;
				y2 = y1 + my;
				int ix1 = (int)x1;
				int ix2 = (int)x2;
				int iy1 = (int)y1;
				int iy2 = (int)y2;
				wx1 = (1-(x1-ix1))/mx;
				wx2 = (x2-ix2)/mx;
				wy1 = (1-(y1-iy1))/my;
				wy2 = (y2-iy2)/my;
				ix2 = min(ix2, dx-1);
				iy2 = min(iy2, dy-1);

				if (ix1 == ix2)
				{
					float a = wx1 + wx2;
					wx1 = wx1 / a;
					wx2 = wx2 / a;
				}
				if (iy1 == iy2)
				{
					float a = wy1 + wy2;
					wy1 = wy1 / a;
					wy2 = wy2 / a;
				}

				float r=0, g=0, b=0;
				r += texDiaphragm.bmp.idata[(iy1 * dx + ix1)*4+0] * wx1 * wy1;
				r += texDiaphragm.bmp.idata[(iy1 * dx + ix2)*4+0] * wx2 * wy1;
				r += texDiaphragm.bmp.idata[(iy2 * dx + ix1)*4+0] * wx1 * wy2;
				r += texDiaphragm.bmp.idata[(iy2 * dx + ix2)*4+0] * wx2 * wy2;
				g += texDiaphragm.bmp.idata[(iy1 * dx + ix1)*4+1] * wx1 * wy1;
				g += texDiaphragm.bmp.idata[(iy1 * dx + ix2)*4+1] * wx2 * wy1;
				g += texDiaphragm.bmp.idata[(iy2 * dx + ix1)*4+1] * wx1 * wy2;
				g += texDiaphragm.bmp.idata[(iy2 * dx + ix2)*4+1] * wx2 * wy2;
				b += texDiaphragm.bmp.idata[(iy1 * dx + ix1)*4+2] * wx1 * wy1;
				b += texDiaphragm.bmp.idata[(iy1 * dx + ix2)*4+2] * wx2 * wy1;
				b += texDiaphragm.bmp.idata[(iy2 * dx + ix1)*4+2] * wx1 * wy2;
				b += texDiaphragm.bmp.idata[(iy2 * dx + ix2)*4+2] * wx2 * wy2;
				tDiaph.bmp.idata[(y * tDiaph.bmp.getWidth() + x)*4 + 0] = (unsigned char)(int)min(r, 255);
				tDiaph.bmp.idata[(y * tDiaph.bmp.getWidth() + x)*4 + 1] = (unsigned char)(int)min(g, 255);
				tDiaph.bmp.idata[(y * tDiaph.bmp.getWidth() + x)*4 + 2] = (unsigned char)(int)min(b, 255);
				tDiaph.bmp.idata[(y * tDiaph.bmp.getWidth() + x)*4 + 3] = 0;
			}
		}
	}
	else
	{
		ImageByteBuffer bbuf;
		bbuf.allocBuffer(d,d);
		createApertureShapePreview(&bbuf);
		for (int y=0; y<d; y++)
		{
			int tdbgwty = y * tDiaph.bmp.getWidth();
			for (int x=0; x<d; x++)
			{
				COLORREF c = bbuf.imgBuf[y][x];
				tDiaph.bmp.idata[(tdbgwty + x)*4 + 0] = GetRValue(c);
				tDiaph.bmp.idata[(tdbgwty + x)*4 + 1] = GetGValue(c);
				tDiaph.bmp.idata[(tdbgwty + x)*4 + 2] = GetBValue(c);
				tDiaph.bmp.idata[(tdbgwty + x)*4 + 3] = 0;
			}
		}
		bbuf.freeBuffer();
	}

	if (texObstacle.isValid()   &&   fMod.glareTextureObstacleOn)
	{
		float x1,x2,y1,y2;
		float wx1,wx2,wy1,wy2;
		float mx = (float)ox/(float)d;
		float my = (float)oy/(float)d;
		
		for (int y=0; y<d; y++)
		{
			for (int x=0; x<d; x++)
			{
				float xx = x*mx;
				float yy = y*my;

				x1 = xx - 0.5f;
				y1 = yy - 0.5f;
				x2 = xx + 0.5f;
				y2 = yy + 0.5f;

				int ix1 = (int)x1;
				int ix2 = (int)x2;
				int iy1 = (int)y1;
				int iy2 = (int)y2;

				wx1 = 1 - (x1-ix1);
				wx2 = x2-ix2;
				wy1 = 1 - (y1-iy1);
				wy2 = y2-iy2;

				ix2 = min(ix2, ox-1);
				iy2 = min(iy2, oy-1);
				ix1 = max(ix1, 0);
				iy1 = max(iy1, 0);

				if (ix1 == ix2)
				{
					float a = wx1 + wx2;
					wx1 = wx1 / a;
					wx2 = wx2 / a;

				}
				if (iy1 == iy2)
				{
					float a = wy1 + wy2;
					wy1 = wy1 / a;
					wy2 = wy2 / a;
				}

				float r=0, g=0, b=0;
				r += texObstacle.bmp.idata[(iy1 * ox + ix1)*4+0] * wx1 * wy1;
				r += texObstacle.bmp.idata[(iy1 * ox + ix2)*4+0] * wx2 * wy1;
				r += texObstacle.bmp.idata[(iy2 * ox + ix1)*4+0] * wx1 * wy2;
				r += texObstacle.bmp.idata[(iy2 * ox + ix2)*4+0] * wx2 * wy2;
				g += texObstacle.bmp.idata[(iy1 * ox + ix1)*4+1] * wx1 * wy1;
				g += texObstacle.bmp.idata[(iy1 * ox + ix2)*4+1] * wx2 * wy1;
				g += texObstacle.bmp.idata[(iy2 * ox + ix1)*4+1] * wx1 * wy2;
				g += texObstacle.bmp.idata[(iy2 * ox + ix2)*4+1] * wx2 * wy2;
				b += texObstacle.bmp.idata[(iy1 * ox + ix1)*4+2] * wx1 * wy1;
				b += texObstacle.bmp.idata[(iy1 * ox + ix2)*4+2] * wx2 * wy1;
				b += texObstacle.bmp.idata[(iy2 * ox + ix1)*4+2] * wx1 * wy2;
				b += texObstacle.bmp.idata[(iy2 * ox + ix2)*4+2] * wx2 * wy2;
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 0] = (unsigned char)(int)min(r, 255);
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 1] = (unsigned char)(int)min(g, 255);
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 2] = (unsigned char)(int)min(b, 255);
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 3] = 0;
			}
		}
	}
	else
	{
		for (int y=0; y<d; y++)
			for (int x=0; x<d; x++)
			{
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 0] = 255;
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 1] = 255;
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 2] = 255;
				tObst.bmp.idata[(y * tObst.bmp.getWidth() + x)*4 + 3] = 0;
			}
	}


	texDiapObst.freeByteBuffer();
	texDiapObst.allocByteBuffer(d,d);

	int tdobgw = texDiapObst.bmp.getWidth();
	int tobgw = tObst.bmp.getWidth();
	int tdbgw = tDiaph.bmp.getWidth();
	for (int y=0; y<d; y++)
	{
		int tdobgwty = tdobgw*y;
		int tobgwty = tobgw*y;
		int tdbgwty = tdbgw*y;
		for (int x=0; x<d; x++)
		{
			texDiapObst.bmp.idata[(tdobgwty + x)*4 + 0] = tObst.bmp.idata[(tobgwty + x)*4 + 0] * tDiaph.bmp.idata[(tdbgwty + x)*4 + 0] / 255;
			texDiapObst.bmp.idata[(tdobgwty + x)*4 + 1] = tObst.bmp.idata[(tobgwty + x)*4 + 1] * tDiaph.bmp.idata[(tdbgwty + x)*4 + 1] / 255;
			texDiapObst.bmp.idata[(tdobgwty + x)*4 + 2] = tObst.bmp.idata[(tobgwty + x)*4 + 2] * tDiaph.bmp.idata[(tdbgwty + x)*4 + 2] / 255;
			texDiapObst.bmp.idata[(tdobgwty + x)*4 + 3] = 0;
		}
	}

	tDiaph.freeAllBuffers();
	tObst.freeAllBuffers();

}

bool Camera::createGlarePattern(int widthheight)
{
	if (!texDiapObst.isValid())
		return false;
	int w,h;
	
	int d;
	if (widthheight > 0)
	{
		w = h = max(64, widthheight);
		d = 64;
		while (d < w)
			d *= 2;
	}
	else
	{
		w = texDiapObst.bmp.getWidth();
		h = texDiapObst.bmp.getHeight();

		if (w!=h)
			return false;
		d = 256;
		while (d < w)
			d *= 2;
		if (d != w)
			return false;
	}

	// disperse diaphragm shape
	float pr, pg, pb,  t;
	t = fMod.glareDispersion;
	pr = 1.0f;
	pg = 0.868f;
	pb = 0.676f;
	float sx = d/2.0f;
	float sy = d/2.0f;
	float C[] = {1+(pr-1)*t, 1+(pg-1)*t, 1+(pb-1)*t};
	float * sRe;
	float * sIm;
	float * dRe;
	float * dIm;
	sRe = (float*)malloc(sizeof(float)*3*d*d);
	sIm = (float*)malloc(sizeof(float)*3*d*d);
	dRe = (float*)malloc(sizeof(float)*3*d*d);
	dIm = (float*)malloc(sizeof(float)*3*d*d);
	for (int y=0; y<d; y++)
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 2) ...", 0+y*20.0f/d);
		for (int x=0; x<d; x++)
		{
			for (int c=0; c<3; c++)
			{
				float fx = (x-sx) / C[c] + sx;
				float fy = (y-sy) / C[c] + sy;				
				int ix1 = (int)fx;
				int iy1 = (int)fy;
				int ix2 = ix1 + 1;
				int iy2 = iy1 + 1;
				float rx = fx - ix1;
				float ry = fy - iy1;
				sIm[(y*d+x)*3+c] = 0.0f;
				if (ix1>=d || ix2>=d || iy1>=d || iy2>=d || ix1<0 || ix2<0 || iy1<0 || iy2<0)
					sRe[(y*d+x)*3+c] = 0.0f;
				else
					sRe[(y*d+x)*3+c] = 
						  texDiapObst.bmp.idata[(iy1*d + ix1)*4 + c] * (1-rx) * (1-ry)
						+ texDiapObst.bmp.idata[(iy2*d + ix1)*4 + c] * (1-rx) * (  ry)
						+ texDiapObst.bmp.idata[(iy1*d + ix2)*4 + c] * (  rx) * (1-ry)
						+ texDiapObst.bmp.idata[(iy2*d + ix2)*4 + c] * (  rx) * (  ry);
			}
		}
	}

	//----------------- eval FFT

	bool inverse = false;
	int l2d = 0, p = 1; //l2n will become log_2(n)
	while(p < d) 
		{	p *= 2; l2d++;	}

	// copy values
	for (int a=0; a<d*d*3; a++)
	{
		dRe[a] = sRe[a];
		dIm[a] = sIm[a];
	}

	//Bit reversal of each row
	for(int y=0; y<d; y++) //for each row
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 2) ...", 20+y*70.0f/d);
		for(int c=0; c<3; c++) //for each color component
		{
			int j = 0;
			for(int i=0; i<d-1; i++)
			{
				dRe[3*d*i + 3*y + c] = sRe[3*d*j + 3*y + c];
				dIm[3*d*i + 3*y + c] = sIm[3*d*j + 3*y + c];
				int k = d / 2;
				while (k <= j) 
				{
					j -= k; 
					k /= 2;
				}
				j += k;
			}
		}
	}

	//Bit reversal of each column
	float tx = 0, ty = 0; 
	for(int x=0; x<d; x++) //for each column
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 2) ...", 90+x*10.0f/d);
		for(int c=0; c<3; c++) //for each color component
		{
			int j = 0;
			for(int i=0; i<d-1; i++)
			{
				if(i < j)
				{
					tx = dRe[3*d*x + 3*i + c];
					ty = dIm[3*d*x + 3*i + c];
					dRe[3*d*x + 3*i + c] = dRe[3*d*x + 3*j + c];
					dIm[3*d*x + 3*i + c] = dIm[3*d*x + 3*j + c];
					dRe[3*d*x + 3*j + c] = tx;
					dIm[3*d*x + 3*j + c] = ty;            
				}  
				int k = d / 2;
				while (k <= j) 
				{
					j -= k; 
					k /= 2;
				}
				j += k;
			}
		}     
	}

	//Calculate the FFT of the columns
	for(int x=0; x<d; x++) //for each column
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 3) ...", 0+x*30.0f/d);
		for(int c=0; c<3; c++) //for each color component
		{  
			//This is the 1D FFT:
			float ca = -1.0;
			float sa = 0.0;
			int l1 = 1, l2 = 1;
			for(int l=0; l<l2d; l++)
			{
				l1 = l2;
				l2 *= 2;
				float u1 = 1.0;
				float u2 = 0.0;
				for(int j=0; j < l1; j++)
				{
					for(int i=j; i<d; i+=l2)
					{
						int i1 = i + l1;
						float t1 = u1 * dRe[3*d*x + 3*i1 + c] - u2 * dIm[3*d*x + 3*i1 + c];
						float t2 = u1 * dIm[3*d*x + 3*i1 + c] + u2 * dRe[3*d*x + 3*i1 + c];
						dRe[3*d*x + 3*i1 + c] = dRe[3*d*x + 3*i + c] - t1;
						dIm[3*d*x + 3*i1 + c] = dIm[3*d*x + 3*i + c] - t2;
						dRe[3*d*x + 3*i + c] += t1;
						dIm[3*d*x + 3*i + c] += t2;
					}
					float z =  u1 * ca - u2 * sa;
					u2 = u1 * sa + u2 * ca;
					u1 = z;
				}
				sa = sqrt((1.0f - ca) / 2.0f);
				if(!inverse) sa = -sa;
				ca = sqrt((1.0f + ca) / 2.0f);
			}
		}
	}

	// transpose 
	float * sReTr = (float*)malloc(sizeof(float)*3*d*d);
	float * sImTr = (float*)malloc(sizeof(float)*3*d*d);
	float * dReTr = (float*)malloc(sizeof(float)*3*d*d);
	float * dImTr = (float*)malloc(sizeof(float)*3*d*d);

	for (int j=0; j<d; j++)
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 3) ...", 30+j*20.0f/d);
		for (int i=0; i<d; i++)
		{
			sReTr[3*i*d + 3*j + 0] = sRe[3*j*d + 3*i + 0];
			sReTr[3*i*d + 3*j + 1] = sRe[3*j*d + 3*i + 1];
			sReTr[3*i*d + 3*j + 2] = sRe[3*j*d + 3*i + 2];
			sImTr[3*i*d + 3*j + 0] = sIm[3*j*d + 3*i + 0];
			sImTr[3*i*d + 3*j + 1] = sIm[3*j*d + 3*i + 1];
			sImTr[3*i*d + 3*j + 2] = sIm[3*j*d + 3*i + 2];
			dReTr[3*i*d + 3*j + 0] = dRe[3*j*d + 3*i + 0];
			dReTr[3*i*d + 3*j + 1] = dRe[3*j*d + 3*i + 1];
			dReTr[3*i*d + 3*j + 2] = dRe[3*j*d + 3*i + 2];
			dImTr[3*i*d + 3*j + 0] = dIm[3*j*d + 3*i + 0];
			dImTr[3*i*d + 3*j + 1] = dIm[3*j*d + 3*i + 1];
			dImTr[3*i*d + 3*j + 2] = dIm[3*j*d + 3*i + 2];
		}
	}

	//Calculate the FFT of the columns (transposed rows)
	for(int x=0; x<d; x++) //for each column
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 3) ...", 50+x*30.0f/d);
		for(int c=0; c<3; c++) //for each color component
		{  
			float ca = -1.0;
			float sa = 0.0;
			int l1 = 1, l2 = 1;
			for(int l=0; l<l2d; l++)
			{
				l1 = l2;
				l2 *= 2;
				float u1 = 1.0;
				float u2 = 0.0;
				for(int j=0; j < l1; j++)
				{
					for(int i=j; i<d; i+=l2)
					{
						int i1 = i + l1;
						float t1 = u1 * dReTr[3*d*x + 3*i1 + c] - u2 * dImTr[3*d*x + 3*i1 + c];
						float t2 = u1 * dImTr[3*d*x + 3*i1 + c] + u2 * dReTr[3*d*x + 3*i1 + c];
						dReTr[3*d*x + 3*i1 + c] = dReTr[3*d*x + 3*i + c] - t1;
						dImTr[3*d*x + 3*i1 + c] = dImTr[3*d*x + 3*i + c] - t2;
						dReTr[3*d*x + 3*i + c] += t1;
						dImTr[3*d*x + 3*i + c] += t2;
					}
					float z =  u1 * ca - u2 * sa;
					u2 = u1 * sa + u2 * ca;
					u1 = z;
				}
				sa = sqrt((1.0f - ca) / 2.0f);
				if(!inverse) sa = -sa;
				ca = sqrt((1.0f + ca) / 2.0f);
			}
		}
	}

	// transpose back
	for (int j=0; j<d; j++)
	{
		Raytracer::getInstance()->curScenePtr->notifyProgress("Evaluating glare pattern (part 3) ...", 80+j*20.0f/d);
		for (int i=0; i<d; i++)
		{
			sRe[3*i*d + 3*j + 0] = sReTr[3*j*d + 3*i + 0];
			sRe[3*i*d + 3*j + 1] = sReTr[3*j*d + 3*i + 1];
			sRe[3*i*d + 3*j + 2] = sReTr[3*j*d + 3*i + 2];
			sIm[3*i*d + 3*j + 0] = sImTr[3*j*d + 3*i + 0];
			sIm[3*i*d + 3*j + 1] = sImTr[3*j*d + 3*i + 1];
			sIm[3*i*d + 3*j + 2] = sImTr[3*j*d + 3*i + 2];
			dRe[3*i*d + 3*j + 0] = dReTr[3*j*d + 3*i + 0];
			dRe[3*i*d + 3*j + 1] = dReTr[3*j*d + 3*i + 1];
			dRe[3*i*d + 3*j + 2] = dReTr[3*j*d + 3*i + 2];
			dIm[3*i*d + 3*j + 0] = dImTr[3*j*d + 3*i + 0];
			dIm[3*i*d + 3*j + 1] = dImTr[3*j*d + 3*i + 1];
			dIm[3*i*d + 3*j + 2] = dImTr[3*j*d + 3*i + 2];
		}
	}

	for(int x=0; x<d; x++) 
		for(int y=0; y<d; y++) 
			for(int c=0; c<3; c++) //for every value of the buffers
			{
				dRe[3*d*x + 3*y + c] /= d;
				dIm[3*d*x + 3*y + c] /= d;
			}  

	ImageBuffer * imgt = new ImageBuffer();
	bool aok = imgt->allocBuffer(d,d);
	if (!aok)
		return false;
	imgt->setHitsValues(1);

	int dd2 = d/2;
	for (int y=0; y<d; y++)
		for (int x=0; x<d; x++)
		{
			int r = (x-dd2)*(x-dd2) + (y-dd2)*(y-dd2);
			float ss = min(PI/2, r/(float)(dd2*dd2)*PI/2);
			ss = cos(ss);
			int x1 = (x+d/2)%d;
			int y1 = (y+d/2)%d;
			float rRe = dRe[(y1*d+x1)*3+0];
			float gRe = dRe[(y1*d+x1)*3+1];
			float bRe = dRe[(y1*d+x1)*3+2];
			float rIm = dIm[(y1*d+x1)*3+0];
			float gIm = dIm[(y1*d+x1)*3+1];
			float bIm = dIm[(y1*d+x1)*3+2];
			imgt->imgBuf[y][x].r = sqrt(rRe*rRe + rIm*rIm)/255.0f * ss;
			imgt->imgBuf[y][x].g = sqrt(gRe*gRe + gIm*gIm)/255.0f * ss;
			imgt->imgBuf[y][x].b = sqrt(bRe*bRe + bIm*bIm)/255.0f * ss;
		}

	ImageBuffer * imgd = imgt->copyResize(w,h);
	if (imgd)
	{
		imgt->freeBuffer();
		delete imgt;
	}
	else
	{
		imgd = imgt;
	}
	imgt = NULL;

	texFourier.freeByteBuffer();
	texFourier.freeFloatBuffer();
	texFourier.allocFloatBuffer(imgd->width, imgd->height);

	for (int y=0; y<imgd->height; y++)
		for (int x=0; x<imgd->width; x++)
		{
			texFourier.bmp.fdata[(y*imgd->width + x)].r = imgd->imgBuf[y][x].r;
			texFourier.bmp.fdata[(y*imgd->width + x)].g = imgd->imgBuf[y][x].g;
			texFourier.bmp.fdata[(y*imgd->width + x)].b = imgd->imgBuf[y][x].b;
		}

	imgd->freeBuffer();
	delete imgd;

	free(sRe);
	free(sIm);
	free(dRe);
	free(dIm);
	free(sReTr);
	free(sImTr);
	free(dReTr);
	free(dImTr);

	Raytracer::getInstance()->curScenePtr->notifyProgress("", 0);
	glareOK = true;

	return true;
}

void Camera::randomAperturePoint(float &x, float &y)	//[-1..1][-1..1]
{
	int ms = diaphSampler.getNumPoints();
	if (pinhole  ||  ms<1)
	{
		x = 0;
		y = 0;
		return;
	}

	static int m = 0;
	m = (m+1)%ms;
	diaphSampler.getThat(m, x, y);
}

bool Camera::copyDiaphFromShapeToSampler()
{
	diaphSampler.initializeDiaphragm(diaphShape.numBlades, 1.0f, diaphShape.angle, diaphShape.roundBlades, diaphShape.radius);
	return true;
}

bool DiaphragmShape::initializeDiaphragm(int NumBlades, float Aperture, float Angle, bool RoundBlades, float Radius)
{
	float R = 1.0f;
	float r = R/Aperture;

	numBlades    = NumBlades;
	aperture     = Aperture;
	angle        = Angle;
	radius	     = Radius;
	roundBlades  = RoundBlades;

	for (int i=0; i<numBlades; i++)
	{
		float k = PI*2*i/numBlades+ PI*angle/180.0f;
		vert[i*2+0] = cos(k)*r;
		vert[i*2+1] = sin(k)*r;
	}
	vert[numBlades*2  ] = vert[0];
	vert[numBlades*2+1] = vert[1];
	for (int i=0; i<numBlades; i++)
	{
		vec[i*2+0] = vert[i*2+2+0] - vert[i*2+0];
		vec[i*2+1] = vert[i*2+2+1] - vert[i*2+1];
	}

	if (roundBlades)
	{
		float c1 = cos(PI/numBlades) * r;
		float c2 = sin(PI/numBlades) * r;
		float beta = asin(c2/R/radius);
		float c3 = cos(beta)*R*radius;
		float c4 = c3-c1;

		for (int i=0; i<numBlades; i++)
		{
			float k = PI*2*i/numBlades+ PI*angle/180.0f;
			float h = PI*2*(i+0.5f)/numBlades + PI*angle/180.0f;
			angles[i] = k;
			cx[i] = -cos(h)*c4;
			cy[i] = -sin(h)*c4;
		}
		vert[numBlades*2  ] = vert[0];
		vert[numBlades*2+1] = vert[1];
		angles[numBlades] = angles[0]+2*PI;
	}
	return true;
}

bool DiaphragmShape::isPointInDiaphragm(float u, float v)
{
	float R = 1.0f/aperture;

	if (numBlades>15)
		return true; 

	if (roundBlades)
	{
		int b = 0;
		double tx = u;
		double ty = v;
		double k = atan2(ty/R,tx/R);
		if (k < 0) 
			k += 2*PI;
		if (k < angles[0])
			k += 2*PI;
		
		for (int i=0; i<numBlades; i++)
		{
			if (k>angles[i] && k<angles[i+1])
				b = i;
		}
		float rr = (cx[b]-u)*(cx[b]-u)  +  (cy[b]-v)*(cy[b]-v);
		if (rr < radius*radius)
			return true;
		else
			return false;
	}
	else
	{
		bool inside = true;
		for (int i=0; i<numBlades; i++)
		{
			float vx = u - vert[i*2+0];
			float vy = v - vert[i*2+1];
			float z = vx*vec[i*2+1] - vy*vec[i*2+0];
			if (z>0)
				inside = false;
		}
		return inside;
	}
}


void Camera::tempShowBufferAddress()
{
	char buf[64];
	sprintf_s(buf, 64, "camera: %d", (int)(INT_PTR)this);
	Logger::add(buf);
	sprintf_s(buf, 64, "imgBuff: %d", (int)(INT_PTR)imgBuff);
	Logger::add(buf);
	if (imgBuff)
	{
	sprintf_s(buf, 64, "imgBuff->hitsBuf: %d", (int)(INT_PTR)imgBuff->hitsBuf);
	Logger::add(buf);
	sprintf_s(buf, 64, "imgBuff->imgBuf: %d", (int)(INT_PTR)imgBuff->imgBuf);
	Logger::add(buf);
	sprintf_s(buf, 64, "imgBuff: res: %d x %d", imgBuff->width, imgBuff->height);
	Logger::add(buf);
	}

	return;
	for (int i=0; i<imgBuff->height ; i++)
	{
		sprintf_s(buf, 64, "line %d: %d", i, (int)(INT_PTR)imgBuff->imgBuf[i]);
		Logger::add(buf);
	}

	for (int i=0; i<imgBuff->height ; i++)
	{
		sprintf_s(buf, 64, "hits %d: %d", i, (int)(INT_PTR)imgBuff->hitsBuf[i]);
		Logger::add(buf);
	}

}

ImageBuffer * Camera::blendBuffers(bool addDirect, bool addGI, bool giHitsPerPixel, RedrawRegion * rgn, ImageModifier * imod, bool smartGI)
{
	int rx1 = 0;
	int rx2 = width*aa;
	int ry1 = 0;
	int ry2 = height*aa;
	if (rgn)
	{
		rx1 = max(0, rgn->left*aa);
		rx2 = min(width*aa, (rgn->right+1)*aa);
		ry1 = max(0, rgn->top*aa);
		ry2 = min(height*aa, (rgn->bottom+1)*aa);
		if (rx2-rx1<=0  ||  ry2-ry1<=0)
			return NULL;
	}
	int dy = ry2-ry1;
	int dx = rx2-rx1;

	ImageBuffer * res = new ImageBuffer();
	CHECK(res);
	bool allocOK = res->allocBuffer(dx, dy);
	CHECK(allocOK);
	if (!res->imgBuf  ||  !res->hitsBuf)
		return NULL;
	res->clearBuffer();

	unsigned long long curHits = Raytracer::getInstance()->curScenePtr->thrM->collectGISamplesCount();
	double hits = (staticRegion.overallHits + curHits) / (double)(aa*aa) * 64;
	Raytracer * rtr = Raytracer::getInstance();

	ImageModifier * imgmod = imod ? imod : &iMod;
	float gicompev = imgmod->getGICompensation();
	gicompev = min(10.0f, max(-10.0f, gicompev));
	float gicomp = pow(2.0f, gicompev);

	for (int i=0; i<16; i++)
	{
		if (!(blendBits & (1<<i)))
			continue;
		if (blendBuffDirect[i].imgBuf==0  ||  blendBuffDirect[i].hitsBuf==0)
			continue;
		if (rtr->curScenePtr->blendSettings.weights[i] == 0.0f)
			continue;
		if (blendBuffDirect[i].width != imgBuff->width  ||  blendBuffDirect[i].height != imgBuff->height)
			continue;
		if (blendBuffGI[i].width != imgBuff->width  ||  blendBuffGI[i].height != imgBuff->height)
			continue;
		if (!rtr->curScenePtr->blendSettings.blendOn[i])
			continue;


		ImageBuffer * srcDirect = &(blendBuffDirect[i]);
		ImageBuffer * srcGI     = &(blendBuffGI[i]);
		Color4 w = Color4(1,1,1);
		w *= rtr->curScenePtr->blendSettings.weights[i];
		w.r *= pow(rtr->curScenePtr->blendSettings.red[i], 2.2f);
		w.g *= pow(rtr->curScenePtr->blendSettings.green[i], 2.2f);
		w.b *= pow(rtr->curScenePtr->blendSettings.blue[i], 2.2f);
		w *= mplApSh;

		__m128 w128 = w.sse;
		__m128 zero128 = _mm_set1_ps(0.0f);
		__m128 one128 = _mm_set1_ps(1.0f);
		__m128 gicomp128 = _mm_mul_ps(_mm_set1_ps(gicomp), w128);
		unsigned int mAll = 0xffffffff;
		__m128 maskNotSmartGi = _mm_set1_ps(!smartGI ? (float&)mAll : 0.0f);
		__m128 phits64 = _mm_set1_ps(64.0f/hits);
			

		if (addDirect)
			for (int y=0; y<dy; y++)
				for (int x=0; x<dx; x++)
				{
					int dHits = srcDirect->hitsBuf[y+ry1][x+rx1];
					__m128 h128 = _mm_set1_ps((float)dHits);
					__m128 mask = _mm_cmpgt_ps(h128, zero128);
					__m128 ph128 = _mm_div_ps(one128, h128);
					__m128 bnm = _mm_andnot_ps(mask, zero128);
					__m128 am = _mm_and_ps(ph128, mask);
					__m128 r = _mm_or_ps(am, bnm);
					__m128 mpl = _mm_mul_ps(r, w128);
					__m128 mpl2 = _mm_mul_ps(srcDirect->imgBuf[y+ry1][x+rx1].sse, mpl);
					res->imgBuf[y][x].sse = _mm_add_ps(res->imgBuf[y][x].sse, mpl2);
				}
		if  (addGI  &&  giHitsPerPixel)
			for (int y=0; y<dy; y++)
				for (int x=0; x<dx; x++)
				{
					int dHits = srcGI->hitsBuf[y+ry1][x+rx1];
					__m128 h128 = _mm_set1_ps((float)dHits);
					__m128 mask = _mm_cmpgt_ps(h128, zero128);
					__m128 ph128 = _mm_div_ps(one128, h128);
					__m128 bnm = _mm_andnot_ps(mask, zero128);
					__m128 am = _mm_and_ps(ph128, mask);
					__m128 r = _mm_or_ps(am, bnm);
					__m128 mpl = _mm_mul_ps(r, gicomp128);
					__m128 mpl2 = _mm_mul_ps(srcGI->imgBuf[y+ry1][x+rx1].sse, mpl);
					res->imgBuf[y][x].sse = _mm_add_ps(res->imgBuf[y][x].sse, mpl2);
				}
		if  (addGI  &&  !giHitsPerPixel  &&  hits>0)
			for (int y=0; y<dy; y++)
				for (int x=0; x<dx; x++)
				{
					int dHits = srcDirect->hitsBuf[y+ry1][x+rx1];
					__m128 h128 = _mm_set1_ps((float)dHits);
					__m128 mask = _mm_cmpgt_ps(h128, zero128);
					mask = _mm_or_ps(mask, maskNotSmartGi);
					__m128 bnm = _mm_andnot_ps(mask, zero128);
					__m128 am = _mm_and_ps(phits64, mask);
					__m128 r = _mm_or_ps(am, bnm);
					__m128 mpl = _mm_mul_ps(r, gicomp128);
					__m128 mpl2 = _mm_mul_ps(srcGI->imgBuf[y+ry1][x+rx1].sse, mpl);
					res->imgBuf[y][x].sse = _mm_add_ps(res->imgBuf[y][x].sse, mpl2);
				}
	}

	for (int y=0; y<dy; y++)
		for (int x=0; x<dx; x++)
			res->hitsBuf[y][x] = 1;
	return res;
}

void Camera::resetFinalValues()
{
	fMod.focusDistance = focusDist;
	fMod.aperture = aperture;
	fMod.bladesNum = apBladesNum;
	fMod.bladesAngle = apBladesAngle;
	fMod.bladesRadius = apBladesRadius;
	fMod.bokehRingBalance = diaphSampler.intBokehRingBalance;
	fMod.bokehRingSize = diaphSampler.intBokehRingSize;
	fMod.bokehFlatten = diaphSampler.intBokehFlatten;
	fMod.bokehVignette = diaphSampler.intBokehVignette;
	fMod.focal = 0.5f * 36 / tan(angle * PI / 360.0f) * 0.001f;
	fMod.roundBlades = apBladesRound;
}

bool Camera::mergeDataFromOtherCameraFile(Camera * cam)
{
	char logtxt[512];
	Raytracer * rtr = Raytracer::getInstance();

	// direct illumination
	for (int i=0; i<16; i++)
	{
		if (*rtr->curScenePtr->abortThreadPtr)
			return true;

		if (!cam->blendBuffDirect[i].imgBuf   ||   cam->blendBuffDirect[i].width==0   ||   cam->blendBuffDirect[i].height==0)
			continue;

		if (!blendBuffDirect[i].imgBuf   ||   blendBuffDirect[i].width==0   ||   blendBuffDirect[i].height==0)
		{
			sprintf_s(logtxt, 512, "Direct buffer on blend layer %d was empty. Allocating %d x %d", (i+1), cam->blendBuffDirect[i].width, cam->blendBuffDirect[i].height);
			Logger::add(logtxt);

			bool aok = blendBuffDirect[i].allocBuffer(cam->blendBuffDirect[i].width, cam->blendBuffDirect[i].height);
			if (aok)
				Logger::add("Alloc OK");
			else
			{
				Logger::add("Alloc failed.");
				continue;
			}
		}

		if ( (cam->blendBuffDirect[i].width != blendBuffDirect[i].width)
			||  (cam->blendBuffDirect[i].height != blendBuffDirect[i].height) )
		{
			sprintf_s(logtxt, 512, "Direct buffer on blend layer %d in merged file has resolution %d x %d while on base file %d x %d", (i+1), 
						cam->blendBuffDirect[i].width, cam->blendBuffDirect[i].height, blendBuffDirect[i].width, blendBuffDirect[i].height);
			Logger::add(logtxt);
			sprintf_s(logtxt, 512, "Direct buffer on blend layer %d in merged file has resolution %d x %d\nwhile on base file %d x %d\nDo you want to merge that one?", (i+1), 
						cam->blendBuffGI[i].width, cam->blendBuffGI[i].height, blendBuffGI[i].width, blendBuffGI[i].height);
			int mres = MessageBox(0, logtxt, "Warning", MB_YESNOCANCEL);
			switch (mres)
			{
				case IDCANCEL:
					return true;
				case IDNO:
					continue;
			}
		}

		int w = min(cam->blendBuffDirect[i].width, blendBuffDirect[i].width);
		int h = min(cam->blendBuffDirect[i].height, blendBuffDirect[i].height);

		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++)
				blendBuffDirect[i].imgBuf[y][x] += cam->blendBuffDirect[i].imgBuf[y][x];
		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++)
				blendBuffDirect[i].hitsBuf[y][x] += cam->blendBuffDirect[i].hitsBuf[y][x];
	}

	// GI part
	for (int i=0; i<16; i++)
	{
		if (*rtr->curScenePtr->abortThreadPtr)
			return true;

		if (!cam->blendBuffGI[i].imgBuf   ||   cam->blendBuffGI[i].width==0   ||   cam->blendBuffGI[i].height==0)
			continue;

		if (!blendBuffGI[i].imgBuf   ||   blendBuffGI[i].width==0   ||   blendBuffGI[i].height==0)
		{
			sprintf_s(logtxt, 512, "GI buffer on blend layer %d was empty. Allocating %d x %d", (i+1), cam->blendBuffGI[i].width, cam->blendBuffGI[i].height);
			Logger::add(logtxt);

			bool aok = blendBuffGI[i].allocBuffer(cam->blendBuffGI[i].width, cam->blendBuffGI[i].height);
			if (aok)
				Logger::add("Alloc OK");
			else
			{
				Logger::add("Alloc failed.");
				continue;
			}
		}

		if ( (cam->blendBuffGI[i].width != blendBuffGI[i].width)
			||  (cam->blendBuffGI[i].height != blendBuffGI[i].height) )
		{
			sprintf_s(logtxt, 512, "GI buffer on blend layer %d in merged file has resolution %d x %d while on base file %d x %d", (i+1), 
						cam->blendBuffGI[i].width, cam->blendBuffGI[i].height, blendBuffGI[i].width, blendBuffGI[i].height);
			Logger::add(logtxt);
			sprintf_s(logtxt, 512, "GI buffer on blend layer %d in merged file has resolution %d x %d\nwhile on base file %d x %d\nDo you want to merge that one?", (i+1), 
						cam->blendBuffGI[i].width, cam->blendBuffGI[i].height, blendBuffGI[i].width, blendBuffGI[i].height);
			int mres = MessageBox(0, logtxt, "Warning", MB_YESNOCANCEL);
			switch (mres)
			{
				case IDCANCEL:
					return true;
				case IDNO:
					continue;
			}
		}

		int w = min(cam->blendBuffGI[i].width, blendBuffGI[i].width);
		int h = min(cam->blendBuffGI[i].height, blendBuffGI[i].height);

		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++)
				blendBuffGI[i].imgBuf[y][x] += cam->blendBuffGI[i].imgBuf[y][x];
		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++)
				blendBuffGI[i].hitsBuf[y][x] += cam->blendBuffGI[i].hitsBuf[y][x];
	}

	staticRegion.overallHits += cam->staticRegion.overallHits;

	return true;
}

bool Camera::deleteStuffAfterMerge()
{
	for (int i=0; i<16; i++)
	{
		blendBuffDirect[i].freeBuffer();
		blendBuffGI[i].freeBuffer();
	}

	if (depthBuf)
	{
		depthBuf->freeBuffer();
		free(depthBuf);
	}
	depthBuf = NULL;
	if (imgBuff)
	{
		imgBuff->freeBuffer();
		free(imgBuff);
	}
	imgBuff = NULL;

	if (name)
		free(name);
	name = NULL;

	texDiaphragm.freeAllBuffers();
	texObstacle.freeAllBuffers();
	texDiapObst.freeAllBuffers();
	texFourier.freeAllBuffers();

	return true;
}


DWORD WINAPI regionRefreshThreadProc(LPVOID lpParameter);

bool Camera::runRefreshRegionThread(RedrawRegion rgn)
{
	DWORD threadID;
	RedrawRegion * prgn = new RedrawRegion();
	*prgn = rgn;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(regionRefreshThreadProc), (LPVOID)prgn, 0, &threadID);

	return true;
}


