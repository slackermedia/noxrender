#include "EM2Controls.h"
#include "resource.h"

EM2Elements * EM2Elements::elInstance = NULL;
HBITMAP loadHBitmapFromBinaryResource(HMODULE hMod, WORD idRes);
extern HMODULE hDllModule;
char * copyString(char * src, unsigned int addSpace=0);

//----------------------------------------------------------------------

EM2Elements::EM2Elements()
{
	hImage = 0;
	hCircleProgress = 0;
	hImage = loadHBitmapFromBinaryResource(hDllModule, IDB_NOX_NEW_GUI_ELEMENTS);
	hCircleProgress = loadHBitmapFromBinaryResource(hDllModule, IDB_NOX_NEW_GUI_CIRCLE);
}

//----------------------------------------------------------------------

EM2Elements::~EM2Elements()
{
	if (hImage)
		DeleteObject(hImage);
	hImage = 0;
	if (hCircleProgress)
		DeleteObject(hCircleProgress);
	hCircleProgress = 0;
}

//----------------------------------------------------------------------

EM2Elements * EM2Elements::getInstance()
{
	if (!elInstance)
		elInstance = new EM2Elements();
	return elInstance;
}

//----------------------------------------------------------------------

HBITMAP EM2Elements::getCircleBitmap()
{
	return hCircleProgress;
}

//----------------------------------------------------------------------

HBITMAP EM2Elements::getHBitmap()
{
	return hImage;
}

//----------------------------------------------------------------------

HBITMAP * EM2Elements::getHBitmapPtr()
{
	return &hImage;
}

//----------------------------------------------------------------------

int cutTextToFitWidthAddEllipsis(HDC hdc, char * txt, int width)
{
	if (!hdc  ||  !txt  ||  width<5)
		return 0;

	SIZE sz;
	int ll = (int)strlen(txt);
	if (ll<3)
		return 0;

	GetTextExtentPoint32(hdc, txt, ll, &sz);
	int n = sz.cx;
	if (n>width)
		txt[ll-1] = txt[ll-2] = txt[ll-3] = '.';

	GetTextExtentPoint32(hdc, txt, ll, &sz);
	n = sz.cx;

	while (n>width  &&  ll>3)
	{
		ll--;
		txt[ll-3] = '.';
		txt[ll] = 0;
		GetTextExtentPoint32(hdc, txt, ll, &sz);
		n = sz.cx;
	}
	return n;
}

//----------------------------------------------------------------------

char * getShortPathEllipsis(HDC hdc, char * otxt, int width,int minfirst)
{
	if (!hdc  ||  !otxt  ||  width<5)
		return copyString(otxt);
	
	minfirst = max(minfirst, 0);
	char * txt = copyString(otxt);

	SIZE sz;
	int ll = (int)strlen(txt);
	if (ll<minfirst+4)
		return txt;

	GetTextExtentPoint32(hdc, txt, ll, &sz);
	int n = sz.cx;
	if (n<=width)
		return txt;

	txt[minfirst] = txt[minfirst+1] = txt[minfirst+2] = '.';

	GetTextExtentPoint32(hdc, txt, ll, &sz);
	n = sz.cx;

	while (n>width  &&  ll>minfirst+3)
	{
		for (int i=minfirst+3; i<ll; i++)
			txt[i] = txt[i+1];
		ll--;
		GetTextExtentPoint32(hdc, txt, ll, &sz);
		n = sz.cx;
	}
	
	return txt;
}

//----------------------------------------------------------------------
