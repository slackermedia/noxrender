#include "max2013predef.h"
#include "NOXCamera.h"
#include <3dsmaxport.h>

INT_PTR CALLBACK NOXCamParamDialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
static NOXCamera * editedCam = NULL;
static IObjParam * currentInterface = NULL;
//static INode* FindNodeRef(ReferenceTarget *rt);
INode* FindNodeRef(ReferenceTarget *rt);


//----------------------------------------------------------------------------------------------------------

void NOXCamCreateMode::EnterMode() 
{ 
}

void NOXCamCreateMode::ExitMode()  
{
	proc.End();
}

//----------------------------------------------------------------------------------------------------------

void NOXCamera::BeginEditParams (IObjParam * ip, ULONG flags, Animatable * prev)
{
	currentInterface = ip;
	if (!hParams)
	{
		hParams = ip->AddRollupPage(hInstance, MAKEINTRESOURCE(IDD_NOX_CAM_PANEL), NOXCamParamDialogProc, GetString(IDS_NOX_CAM_PARAMS_TITLE), (LPARAM)this);
		ip->RegisterDlgWnd(hParams);
	}
	else
	{
		DLSetWindowLongPtr(hParams, this);
	}
}

//----------------------------------------------------------------------------------------------------------

void NOXCamera::EndEditParams(IObjParam * ip, ULONG flags, Animatable * next)
{
	if (flags & END_EDIT_REMOVEUI)
	{
		if (hParams)
		{
			ip->UnRegisterDlgWnd(hParams);
			ip->DeleteRollupPage(hParams);
		}
		hParams = NULL;				
	}
	else
	{
		if (hParams)
			DLSetWindowLongPtr(hParams, 0);
	}
	currentInterface = NULL;
}

//----------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK NOXCamParamDialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static NOXCamera * cam1 = NULL;
	switch (message)
	{
	case WM_INITDIALOG:
		{
			NOXCamera* cam1 = (NOXCamera *)lParam;
			editedCam = cam1;
			if (cam1)
			{
				HWND hTargeted = GetDlgItem(hDlg, IDC_NOX_CAM_TARGETED);
				HWND hCone = GetDlgItem(hDlg, IDC_NOX_CAM_SHOW_CONE);
				HWND hFocus = GetDlgItem(hDlg, IDC_NOX_CAM_FOCUSTARGET);
				if (cam1->GetConeState())
					SendMessage(hCone, BM_SETCHECK, BST_CHECKED,   0);
				else
					SendMessage(hCone, BM_SETCHECK, BST_UNCHECKED, 0);
				if (cam1->focusConnectedToTarget)
					SendMessage(hFocus, BM_SETCHECK, BST_CHECKED,   0);
				else
					SendMessage(hFocus, BM_SETCHECK, BST_UNCHECKED, 0);
				if (cam1->Type())
				{
					SendMessage(hTargeted, BM_SETCHECK, BST_CHECKED,   0);
					EnableWindow(hFocus, true);
				}
				else
				{
					SendMessage(hTargeted, BM_SETCHECK, BST_UNCHECKED, 0);
					EnableWindow(hFocus, false);
				}
			}
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_CAMERA_SETFOCUSDIST:
				{
					HWND hButton = GetDlgItem(hDlg, IDC_CAMERA_SETFOCUSDIST);
					LONG style = GetWindowLong(hButton, GWL_STYLE);
					SetWindowLong(hButton, GWL_STYLE, style | BS_PUSHLIKE);

					NOXCamFocusDistCommandMode* cmode = &NOXCamFocusDistCommandMode::GetInstance();
					cmode->hButton = hButton;
					cmode->cam = editedCam;
					GetCOREInterface()->SetCommandMode(cmode);

				}
				break;
				case IDC_NOX_CAM_RUN_CAMERA_DIALOG:
				{
					cam1 = editedCam;
					if (cam1)
						cam1->trCam.runEditor(hInstance, hDlg);
				}
				break;
				case IDC_NOX_CAM_FOCUSTARGET:
				{
					if (wmEvent != BN_CLICKED)
						break;
					cam1 = editedCam;
					if (!cam1)
						break;
					HWND hFocus = GetDlgItem(hDlg, IDC_NOX_CAM_FOCUSTARGET);
					if (BST_CHECKED == SendMessage(hFocus, BM_GETCHECK, 0, 0))
						cam1->focusConnectedToTarget = true;
					else
						cam1->focusConnectedToTarget = false;
					if (currentInterface)
					{
						TimeValue t = currentInterface->GetTime();
						if (cam1->focusConnectedToTarget)
						{
							cam1->focusLength = cam1->GetTDist(t);
							cam1->trCam.setFocusDistance(cam1->GetTDist(t));
						}
						currentInterface->RedrawViews(t);
					}
				}
				break;
				case IDC_NOX_CAM_TARGETED:
				{
					if (wmEvent != BN_CLICKED)
						break;
					cam1 = editedCam;
					if (!cam1)
						break;

					HWND hFocus = GetDlgItem(hDlg, IDC_NOX_CAM_FOCUSTARGET);
					HWND hTargeted = GetDlgItem(hDlg, IDC_NOX_CAM_TARGETED);
					if (BST_CHECKED == SendMessage(hTargeted, BM_GETCHECK, 0, 0))
					{
						cam1->SetType(1);
						EnableWindow(hFocus, true);
					}
					else
					{
						cam1->SetType(0);
						EnableWindow(hFocus, false);
					}

					if (currentInterface)
					{
						TimeValue t = currentInterface->GetTime();
						if (cam1->focusConnectedToTarget)
						{
							cam1->focusLength = cam1->GetTDist(t);
							cam1->trCam.setFocusDistance(cam1->GetTDist(t));
						}
						currentInterface->RedrawViews(t);
					}
				}
				break;
				case IDC_NOX_CAM_MAKE_DEFAULT:
				{
					if (wmEvent != BN_CLICKED)
						break;

					cam1 = editedCam;
					if (!cam1)
						break;

					cam1->makeActiveOnlyMe(GetCOREInterface()->GetRootNode());

				}
				break;
				case IDC_NOX_CAM_SHOW_CONE:
				{
					if (wmEvent != BN_CLICKED)
						break;
					cam1 = editedCam;
					if (!cam1)
						break;

					HWND hCone = GetDlgItem(hDlg, IDC_NOX_CAM_SHOW_CONE);
					if (BST_CHECKED == SendMessage(hCone, BM_GETCHECK, 0, 0))
						cam1->SetConeState(1);
					else
						cam1->SetConeState(0);
					if (currentInterface)
					{
						TimeValue t = currentInterface->GetTime();
						currentInterface->RedrawViews(t);
					}
				}
				break;
				case IDC_BUTTON2:
				{
					if (wmEvent != BN_CLICKED)
						break;
					cam1 = editedCam;
					if (!cam1)
						break;
					cam1->tiltON = !cam1->tiltON;
				}
				break;
				case IDC_BUTTON1:
				{
					if (wmEvent != BN_CLICKED)
						break;
					cam1 = editedCam;
					if (!cam1)
						break;

					INode *nd = FindNodeRef(cam1);
					Matrix3 mt = nd->GetObjTMAfterWSM(0);
					
					Point3 p1 = mt.GetRow(0);
					Point3 p2 = mt.GetRow(1);
					Point3 p3 = mt.GetRow(2);
					Point3 p4 = mt.GetRow(3);
					TCHAR buf[512];
					_stprintf_s(buf, 512, _T("%.4f  %.4f  %.4f\n%.4f  %.4f  %.4f\n%.4f  %.4f  %.4f\n%.4f  %.4f  %.4f"),
						p1[0], p1[1], p1[2], 
						p2[0], p2[1], p2[2], 
						p3[0], p3[1], p3[2], 
						p4[0], p4[1], p4[2]  );
					MessageBox(0, buf, _T(""), 0);
				}
				break;
			}
		}
		break;
	}
	return FALSE;
}

//----------------------------------------------------------------------------------------------------------

void notifyNOXCameraCallback(float focal, float focus)
{
	if (!currentInterface   ||   !editedCam)
		return;
	TimeValue t = currentInterface->GetTime();
	editedCam->SetFOV(t, focal);
	if (focus > 0)
		if (editedCam->focusConnectedToTarget  &&  editedCam->Type())
			editedCam->SetTDist(t, focus);
		else
			editedCam->focusLength = focus;
	currentInterface->RedrawViews(t);
}

//----------------------------------------------------------------------------------------------------------

