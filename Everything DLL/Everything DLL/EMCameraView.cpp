#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <stdio.h>
#include "EMControls.h"
#include <math.h>

extern HMODULE hDllModule;

EMCameraView::EMCameraView(HWND hWnd) 
{
	active = false;
	hwnd = hWnd;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	lButton = mButton = rButton = false;
	sceneTextures = 0;
	showTextures = true;
	wireframe = false;

	dirU = Vector3d( 0, 1, 0);
	dirN = Vector3d( 0, 0, 1);
	dirS = Vector3d( 0, 0,-1);
	dirE = Vector3d(-1, 0, 0);
	dirW = Vector3d( 1, 0, 0);
	ctg = 1;
}

EMCameraView::~EMCameraView() 
{
	if  (hFont)
		DeleteObject(hFont);
}

void InitEMCameraView()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
	wc.lpszClassName  = "EMCameraView";
    wc.hInstance      = hDllModule;
	wc.lpfnWndProc    = EMCameraViewProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMCameraView *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMCameraView * GetEMCameraViewInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMCameraView* emcv = (EMCameraView *)GetWindowLongPtr(hwnd, 0);
	#else
		EMCameraView* emcv = (EMCameraView *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcv;
}

void SetEMCameraViewInstance(HWND hwnd, EMCameraView *emcv)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcv);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcv);
	#endif
}

LRESULT CALLBACK EMCameraViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMCameraView * emcv;
	emcv = GetEMCameraViewInstance(hwnd);
	static HRGN rgn;
	static float lx, ly;			// mouse coords			 at l/m/r clicked event
	static float lphi, ltheta;		// forward dir angles	 at l/m/r clicked event
	static Point3d lposition;		// position of camera	 at l/m/r clicked event
	static Vector3d lUp, lRight, lDir;
	float x,y;
	float theta, phi;


    switch(msg)
    {
		case WM_CREATE:
			{
				EMCameraView * emcv1 = new EMCameraView(hwnd);
				SetEMCameraViewInstance(hwnd, (EMCameraView *)emcv1);			
				emcv1->compass_texture = 0;
				emcv1->compass_letters_texture = 0;
			}
		break;
		case WM_NCDESTROY:
			{
				EMCameraView * emcv1;
				emcv1 = GetEMCameraViewInstance(hwnd);
				delete emcv1;
				SetEMCameraViewInstance(hwnd, 0);
			}
		break;
		case WM_PAINT:
		{
			if (!emcv)
				break;

			if (emcv->active)
			{
				emcv->GLRender();
				SwapBuffers(emcv->hDC);
				ValidateRect(hwnd,NULL);
			}
			else
			{
				HDC hdc;
				PAINTSTRUCT ps;
				hdc = BeginPaint(hwnd, &ps);

				HFONT hOldFont = (HFONT)SelectObject(hdc, emcv->hFont);

				RECT rect;
				GetClientRect(hwnd, &rect);
			
				SIZE sz;
				GetTextExtentPoint32(hdc, "GL preview deactivated", 22, &sz);
				int posx = (rect.right-rect.left-sz.cx)/2;
				int posy = (rect.bottom-rect.top-sz.cy)/2;

				SetBkColor(hdc, RGB(160,160,160));
				ExtTextOut(hdc, posx, posy, ETO_OPAQUE, &rect, "GL preview deactivated", 22, 0);

				DeleteObject(SelectObject(hdc, hOldFont));
		
				EndPaint(hwnd, &ps);
			}
		}
		break;
		case WM_MOUSEMOVE:
		{
			if (!emcv->active)
				break;
			if (GetCapture() != hwnd)
				break;

			x = (short)LOWORD(lParam);
			y = (short)HIWORD(lParam);

			bool mShift = ((wParam & MK_SHIFT) != 0);
			bool mCtrl  = ((wParam & MK_CONTROL) != 0);

			float mult;
			if (mShift)
				mult = 0.05f;
			else 
				mult = 0.005f;

			// cursor moving to other edge at screen edges
			int yRes = GetSystemMetrics(SM_CYSCREEN);
			int xRes = GetSystemMetrics(SM_CXSCREEN);
			POINT pt1 = {(LONG)x, (LONG)y};
			ClientToScreen(hwnd, &pt1);
			// for Y
			if (pt1.y > yRes-2)
			{
				SetCursorPos(pt1.x, 1);
				ly -= yRes;
				break;
			}
			if (pt1.y < 1)
			{
				SetCursorPos(pt1.x, yRes-2);
				ly += yRes;
				break;
			}
			// for X
			if (pt1.x > xRes-2)
			{
				SetCursorPos(1, pt1.y);
				lx -= xRes;
				break;
			}
			if (pt1.x < 1)
			{
				SetCursorPos(xRes-2, pt1.y);
				lx += xRes;
				break;
			}
			
			// left mouse button -> direction changing
			if (emcv->lButton)
			{
				theta = (ly-y)*mult*0.4f;
				phi = (x-lx)*mult*0.4f;

				emcv->tcam.direction = lDir + lUp*sin(theta) + lRight*sin(phi);
				emcv->tcam.direction.normalize();
				emcv->tcam.rightDir = emcv->tcam.direction^emcv->tcam.upDir;
				emcv->tcam.rightDir.normalize();
				emcv->tcam.upDir = emcv->tcam.rightDir^emcv->tcam.direction;
				emcv->tcam.upDir.normalize();

				lDir = emcv->tcam.direction;
				lRight = emcv->tcam.rightDir;
				lUp = emcv->tcam.upDir;
				lx = x;
				ly = y;

				emcv->createPerspectiveMatrix();
			}

			// middle mouse button -> camera position moving
			if (emcv->mButton)
			{
				emcv->tcam.position = lposition + emcv->tcam.rightDir*(lx-x)*mult + emcv->tcam.upDir*(y-ly)*mult;
				lposition = emcv->tcam.position;
				lx = x;
				ly = y;
				emcv->createPerspectiveMatrix();
			}

			// right mouse button -> distance changing and rotating around direction
			if (emcv->rButton)
			{
				if (mCtrl)
				{
					float rcos, rsin;
					rcos = cos((x-lx)*mult*0.2f);
					rsin = sin((x-lx)*mult*0.2f);
					emcv->tcam.upDir = lRight*rsin + lUp*rcos;
					emcv->tcam.rightDir = lRight*rcos + lUp*(-rsin);
					emcv->tcam.upDir.normalize();
					emcv->tcam.rightDir.normalize();
				}
				else
					emcv->tcam.position = lposition + emcv->tcam.direction*(ly-y)*mult;
			
				lposition = emcv->tcam.position;
				lDir = emcv->tcam.direction;
				lRight = emcv->tcam.rightDir;
				lUp = emcv->tcam.upDir;
				lx = x;
				ly = y;

				emcv->createPerspectiveMatrix();
			}

			// repaint and notify dialog
			emcv->refreshViewport();
			emcv->notifyParent();
		}
		break;
		case WM_LBUTTONDOWN:
		{
			SetFocus(hwnd);
			if (!emcv->active)
				break;
			if (emcv->mButton || emcv->rButton)
				break;
			if (!GetCapture())
			{
				SetCapture(hwnd);
			}
			if (GetCapture() != hwnd)
				break;
		
			emcv->lButton = true;
			emcv->tcam.direction.toAngles(ltheta, lphi);
			x = (short)LOWORD(lParam);
			y = (short)HIWORD(lParam);
			lx = x;
			ly = y;
			lDir = emcv->tcam.direction;
			lRight = emcv->tcam.rightDir;
			lUp = emcv->tcam.upDir;
		}
		break;
		case WM_LBUTTONUP:
		{
			if (GetCapture() != hwnd)
				break;
			ReleaseCapture();
			emcv->lButton = false;
		}
		break;
		case WM_MBUTTONDOWN:
		{
			if (!emcv->active)
				break;
			if (emcv->lButton || emcv->rButton)
				break;
			if (!GetCapture())
			{
				SetCapture(hwnd);
			}
			if (GetCapture() != hwnd)
				break;
		
			emcv->mButton = true;
			lposition = emcv->tcam.position;
			emcv->tcam.rightDir = emcv->tcam.direction^emcv->tcam.upDir;
			emcv->tcam.upDir = emcv->tcam.rightDir^emcv->tcam.direction;
			lUp = emcv->tcam.upDir;
			lRight = emcv->tcam.rightDir;
			x = (short)LOWORD(lParam);
			y = (short)HIWORD(lParam);
			lx = x;
			ly = y;
		}
		break;
		case WM_MBUTTONUP:
		{
			if (GetCapture() != hwnd)
			{
			}
			ReleaseCapture();
			emcv->mButton = false;
		}
		break;
		case WM_RBUTTONDOWN:
		{
			if (!emcv->active)
				break;
			if (emcv->mButton || emcv->lButton)
				break;
			if (!GetCapture())
			{
				SetCapture(hwnd);
			}
			if (GetCapture() != hwnd)
				break;
		
			emcv->rButton = true;
			x = (short)LOWORD(lParam);
			y = (short)HIWORD(lParam);
			lx = x;
			ly = y;
			lposition = emcv->tcam.position;
			emcv->tcam.rightDir = emcv->tcam.direction^emcv->tcam.upDir;
			emcv->tcam.upDir = emcv->tcam.rightDir^emcv->tcam.direction;
			lUp = emcv->tcam.upDir;
			lRight = emcv->tcam.rightDir;

		}
		break;
		case WM_RBUTTONUP:
		{
			if (GetCapture() != hwnd)
			{
			}
			ReleaseCapture();
			emcv->rButton = false;
		}
		break;

		case WM_MOUSEWHEEL:
		{
			bool lShift = ((wParam & MK_SHIFT) != 0);
			int zDelta = (int)wParam;
			if (zDelta>0)
			{
				if (lShift)
					emcv->tcam.angle += 1.0f;
				else
					emcv->tcam.angle += 0.1f;
			}
			else
			{
				if (lShift)
					emcv->tcam.angle -= 1.0f;
				else
					emcv->tcam.angle -= 0.1f;
			}

			emcv->ctg = tan(emcv->tcam.angle*PI/180.0f/2);
			if (emcv->ctg < 0.01f)
				emcv->ctg = 100;
			else
				emcv->ctg = 1.0f / emcv->ctg;

			emcv->createPerspectiveMatrix();
			emcv->refreshViewport();
			emcv->notifyParent();
		}
		break;


		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
			break;
	}
	return 0;
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMCameraView::GLRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

	if (wireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		
	glCallList((GLuint)sceneList);

	if (compass_textures_loaded && showCompass)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		float size = 0.5f;
		float dist = 1 * ctg;
		if (dist < 1.8f)
			dist = 1.8f;
		float down = 0.6f;
		float ldist = 0.5f;
		float lsize = 0.07f;
		Point3d p, p1;
		p1 = tcam.position + tcam.direction*dist + tcam.upDir*(-down);

		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_LESS,  0.5f);
		glBindTexture(GL_TEXTURE_2D, compass_texture);
	
		glColor3f(1,1,1);

		glBegin(GL_QUADS);
			p = p1 + dirN * size;
			glTexCoord2f(0,0);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirW * size;
			glTexCoord2f(1,0);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirS * size;
			glTexCoord2f(1,1);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirE * size;
			glTexCoord2f(0,1);
			glVertex3f(p.x, p.y, p.z);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, compass_letters_texture);

		glBegin(GL_QUADS);
			p = p1 + dirN * ldist + tcam.rightDir*-lsize + tcam.upDir*(-lsize);
						glTexCoord2f(0.5f,   0);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirN * ldist + tcam.rightDir*lsize + tcam.upDir*(-lsize);
						glTexCoord2f(1,   0);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirN * ldist + tcam.rightDir*lsize + tcam.upDir*(lsize);
						glTexCoord2f(1,   0.5f);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirN * ldist + tcam.rightDir*-lsize + tcam.upDir*(lsize);
						glTexCoord2f(0.5f,   0.5f);
			glVertex3f(p.x, p.y, p.z);

			
			p = p1 + dirS * ldist + tcam.rightDir*-lsize + tcam.upDir*(-lsize);
						glTexCoord2f(0.5f,   0.5f);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirS * ldist + tcam.rightDir*lsize + tcam.upDir*(-lsize);
						glTexCoord2f(1,   0.5f);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirS * ldist + tcam.rightDir*lsize + tcam.upDir*(lsize);
						glTexCoord2f(1,   1);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirS * ldist + tcam.rightDir*-lsize + tcam.upDir*(lsize);
						glTexCoord2f(0.5f,   1);
			glVertex3f(p.x, p.y, p.z);
			
			
			
			p = p1 + dirE * ldist + tcam.rightDir*-lsize + tcam.upDir*(-lsize);
						glTexCoord2f(0,0);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirE * ldist + tcam.rightDir*lsize + tcam.upDir*(-lsize);
						glTexCoord2f(0.5f,0);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirE * ldist + tcam.rightDir*lsize + tcam.upDir*(lsize);
						glTexCoord2f(0.5f,0.5f);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirE * ldist + tcam.rightDir*-lsize + tcam.upDir*(lsize);
						glTexCoord2f(0,0.5f);
			glVertex3f(p.x, p.y, p.z);




			p = p1 + dirW * ldist + tcam.rightDir*-lsize + tcam.upDir*(-lsize);
						glTexCoord2f(0,   0.5f);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirW * ldist + tcam.rightDir*lsize + tcam.upDir*(-lsize);
						glTexCoord2f(0.5f,   0.5f);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirW * ldist + tcam.rightDir*lsize + tcam.upDir*(lsize);
						glTexCoord2f(0.5f,   1);
			glVertex3f(p.x, p.y, p.z);
			p = p1 + dirW * ldist + tcam.rightDir*-lsize + tcam.upDir*(lsize);
						glTexCoord2f(0,   1);
			glVertex3f(p.x, p.y, p.z);

		glEnd();

		glDisable(GL_ALPHA_TEST);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glEnable(GL_LIGHTING);

	}
}

void EMCameraView::refreshViewport()
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, false);
}

void EMCameraView::activateGL()
{
	HDC hdc;
	RECT rect;
	GetClientRect(hwnd, &rect);
	w = rect.right;
	h = rect.bottom;
	hdc = GetDC(hwnd);
	SetDCPixelFormat(hdc);
	hRC = wglCreateContext(hdc);
	wglMakeCurrent(hdc, hRC);
	hDC = hdc;
	createPerspectiveMatrix();
	setupGL();
	active = true;
}

void EMCameraView::deactivateGL()
{
	wglMakeCurrent(hDC,NULL);
	wglDeleteContext(hRC);
	active = false;
}

void EMCameraView::createSceneList()
{
	Raytracer * rtr = Raytracer::getInstance();
	Triangle * tri;
	MaterialNox * mat;
	MatLayer * mlay;
	int i,j;

	float norm[3];
	float vert[3];
	float col[3];

	GLuint currentTexture = 0;
	bool texEnabled = false;

	int cTex = 0;
	MatLayer * ml;
	int numTex = 0;

	for (i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		mat = rtr->curScenePtr->mats[i];
		for (j=0; j<mat->layers.objCount; j++)
		{
			ml = &(mat->layers[0]);
			if (ml->use_tex_col0 && ml->tex_col0.isValid())
				numTex++;
		}
	}

	numTextures = numTex;
	sceneTextures = (unsigned int *)malloc(sizeof(unsigned int) * numTex);

	if (sceneTextures)
	{
		for (i=0; i<rtr->curScenePtr->mats.objCount; i++)
		{
			mat = rtr->curScenePtr->mats[i];
			for (j=0; j<mat->layers.objCount; j++)
			{
				ml = &(mat->layers[0]);
				if (ml->use_tex_col0 && ml->tex_col0.isValid())
				{
					glGenTextures(1, &(sceneTextures[cTex]));
					glBindTexture(GL_TEXTURE_2D, sceneTextures[cTex]);
					if (ml->tex_col0.texMod.interpolateProbe)
						glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);	
					else
						glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);	
					glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR); 
					glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
					gluBuild2DMipmaps(GL_TEXTURE_2D, 4, ml->tex_col0.texScPtr->bmp.getWidth(), ml->tex_col0.texScPtr->bmp.getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, ml->tex_col0.texScPtr->bmp.idata);
					ml->tex_col0.texScPtr->reserved = cTex;
				}
			}
		}
	}

	sceneList = glGenLists(1);
	glNewList((GLuint)sceneList, GL_COMPILE);

	glEnable(GL_LIGHTING);
	glBegin(GL_TRIANGLES);

	for (i=0; i<rtr->curScenePtr->triangles.objCount; i++)
	{
		tri = &(rtr->curScenePtr->triangles[i]);
		mat = rtr->curScenePtr->mats[tri->matNum];
		col[0] = col[1] = col[2] = 0;
		bool useTex = false;
		for (j=0; j<mat->layers.objCount; j++)
		{
			mlay = &(mat->layers[j]);
			col[0] += mlay->refl0.r;
			col[1] += mlay->refl0.g;
			col[2] += mlay->refl0.b;
			if (mlay->use_tex_col0 && mlay->tex_col0.isValid())
			{
				useTex = true;
				if (!texEnabled)
				{
					if (showTextures)
					{
						glEnd();
						glEnable(GL_TEXTURE_2D);
						glBegin(GL_TRIANGLES);
					}
					texEnabled = true;
				}
				if (currentTexture != sceneTextures[mlay->tex_col0.texScPtr->reserved])
				{
					glEnd();
					currentTexture = sceneTextures[mlay->tex_col0.texScPtr->reserved];
					glBindTexture(GL_TEXTURE_2D, currentTexture);
					glBegin(GL_TRIANGLES);
				}
			}
			else
			{
				useTex = false;
				if (texEnabled)
				{
					glEnd();
					glDisable(GL_TEXTURE_2D);
					texEnabled = false;
					glBegin(GL_TRIANGLES);
				}
			}
		}
		
		if (showTextures && useTex)
			glColor3f(1,1,1);
		else
			glColor3fv(col);

		vert[0] = tri->V1.x;  vert[1] = tri->V1.y;  vert[2] = tri->V1.z;  
		norm[0] = tri->N1.x;  norm[1] = tri->N1.y;  norm[2] = tri->N1.z;  
		glTexCoord2f(tri->UV1.x, tri->UV1.y);
		glNormal3fv(norm);
		glVertex3fv(vert);
		vert[0] = tri->V2.x;  vert[1] = tri->V2.y;  vert[2] = tri->V2.z;  
		norm[0] = tri->N2.x;  norm[1] = tri->N2.y;  norm[2] = tri->N2.z;  
		glTexCoord2f(tri->UV2.x, tri->UV2.y);
		glNormal3fv(norm);
		glVertex3fv(vert);
		vert[0] = tri->V3.x;  vert[1] = tri->V3.y;  vert[2] = tri->V3.z;  
		norm[0] = tri->N3.x;  norm[1] = tri->N3.y;  norm[2] = tri->N3.z;  
		glTexCoord2f(tri->UV3.x, tri->UV3.y);
		glNormal3fv(norm);
		glVertex3fv(vert);
	}
	glEnd();

	// draw normals
	glDisable(GL_LIGHTING);
	glColor3f(1,1,0);
	float dd = 3;
	glBegin(GL_LINES);
	for (i=0; i<rtr->curScenePtr->triangles.objCount; i++)
	{
		tri = &(rtr->curScenePtr->triangles[i]);
		vert[0] = tri->V1.x;  vert[1] = tri->V1.y;  vert[2] = tri->V1.z;  
		norm[0] = tri->V1.x+tri->N1.x*dd;  norm[1] = tri->V1.y+tri->N1.y*dd;  norm[2] = tri->N1.z*dd+tri->V1.z;  
		glVertex3fv(vert);
		glVertex3fv(norm);
		vert[0] = tri->V2.x;  vert[1] = tri->V2.y;  vert[2] = tri->V2.z;  
		norm[0] = tri->V2.x+tri->N2.x*dd;  norm[1] = tri->N2.y*dd+tri->V2.y;  norm[2] = tri->N2.z*dd+tri->V2.z;  
		glTexCoord2f(tri->UV2.x, tri->UV2.y);
		glVertex3fv(vert);
		glVertex3fv(norm);
		vert[0] = tri->V3.x;  vert[1] = tri->V3.y;  vert[2] = tri->V3.z;  
		norm[0] = tri->N3.x*dd+tri->V3.x;  norm[1] = tri->N3.y*dd+tri->V3.y;  norm[2] = tri->N3.z*dd+tri->V3.z;  
		glTexCoord2f(tri->UV3.x, tri->UV3.y);
		glVertex3fv(norm);
		glVertex3fv(vert);
	}
	glEnd();

	glEndList();
}

void EMCameraView::disposeSceneList()
{
	glDeleteLists((GLuint)sceneList, 1);
	if (sceneTextures)
	{
		for (int i=0; i<numTextures; i++)
			glDeleteTextures(1, &(sceneTextures[i]));
		free(sceneTextures);
		sceneTextures = 0;
	}

}

void EMCameraView::setupGL()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->curScenePtr->sscene.useSunSky)
	{
		Vector3d ldir = rtr->curScenePtr->sscene.sunsky->sunDir*100000;
		lightPos[0] = ldir.x;  lightPos[1] = ldir.y;  lightPos[2] = ldir.z;  lightPos[3] = 1;
	}
	else 
	{
		lightPos[0] = tcam.position.x;   
		lightPos[1] = tcam.position.y;   
		lightPos[2] = tcam.position.z;   
	}

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_NORMALIZE);
	glEnable(GL_SMOOTH);
	
	GLfloat ambientLight[]   = {0.2f, 0.2f,  0.2f, 1.0f};
	//GLfloat diffuseLight[]   = {0.9f, 0.9f,  0.9f, 0.0f};
	GLfloat diffuseLight[]   = {0.8f, 0.8f,  0.8f, 1.0f};
	GLfloat specularLight[]  = {0.0f, 0.0f,  0.0f, 1.0f};
	
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	
	
	GLfloat mat_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat mat_specular[] = {0.0f, 0.0f, 0.0f, 1.0f};
	GLfloat mat_shininess[] = {64.0f};
	
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, 96.0);

	glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);



}

void EMCameraView::createPerspectiveMatrix()
{
	if (h == 0)
		h = 1;
	glViewport(0,0,w,h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(tcam.angle, 1.0*w/h , 1.0, 2000.0);
	gluLookAt(
		tcam.position.x,   tcam.position.y,   tcam.position.z,  
		tcam.position.x+tcam.direction.x,   tcam.position.y+tcam.direction.y,   tcam.position.z+tcam.direction.z,  
		tcam.upDir.x,      tcam.upDir.y,      tcam.upDir.z);
	glMatrixMode(GL_MODELVIEW);

}

void EMCameraView::initializeCamera()
{
	tcam.angle = cam->angle;
	tcam.clipNear = cam->clipNear;
	tcam.clipFar = cam->clipFar;
	tcam.direction = cam->direction;
	tcam.direction.normalize();
	tcam.position = cam->position;
	tcam.upDir = cam->upDir;
	tcam.upDir.normalize();
	tcam.direction.toAngles(ctheta, cphi);
	tcam.rightDir = tcam.direction^tcam.upDir;
	tcam.upDir = tcam.rightDir^tcam.direction;
	ctg = tan(tcam.angle*PI/180.0f/2);
	if (ctg < 0.01f)
		ctg = 100;
	else
		ctg = 1.0f / ctg;
}

void EMCameraView::SetDCPixelFormat(HDC hDC)
{
	int nPixelFormat;
	static PIXELFORMATDESCRIPTOR pfd = {
			sizeof(PIXELFORMATDESCRIPTOR),  // Size of this structure
			1,                              // Version of this
			PFD_DRAW_TO_WINDOW |            // Draw to window
			PFD_SUPPORT_OPENGL |            // Support OpenGL calls
			PFD_DOUBLEBUFFER,               // Double-buffered mode
			PFD_TYPE_RGBA,                  // RGBA Color mode
			24,                             // Want 24bit color
			0,0,0,0,0,0,                    // Not used to select mode
			0,0,                            // Not used to select mode
			0,0,0,0,0,                      // Not used to select mode
			32,                             // Size of depth buffer
			0,                              // Not used to select mode
			0,                              // Not used to select mode
			PFD_MAIN_PLANE,                 // Draw in main plane
			0,                              // Not used to select mode
			0,0,0 };						// Not used to select mode

	nPixelFormat = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, nPixelFormat, &pfd);
}

void EMCameraView::notifyParent()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, CN_CAMERA_CHANGED), (LPARAM)hwnd);
}

bool EMCameraView::loadTexture32(unsigned int * texture, HINSTANCE hInst, unsigned int resNumber)
{
	bool result = true;
	HBITMAP hBMP;												
	BITMAP	BMP;												
	glGenTextures(1, texture);
	hBMP=(HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(resNumber), IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBMP)												
	{														
		GetObject(hBMP,sizeof(BMP), &BMP);
		if (!BMP.bmBits)
			result = false;
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);	
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR); 
		glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 4, BMP.bmWidth, BMP.bmHeight, GL_BGRA_EXT, GL_UNSIGNED_BYTE, BMP.bmBits);
		DeleteObject(hBMP);									
	}
	else
		result = false;

	return result;
}

void EMCameraView::deleteTexture(unsigned int * texture)
{
	glDeleteTextures(1, texture);
}

