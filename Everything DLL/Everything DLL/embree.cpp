#include "embree.h"

#include <math.h>
#include "log.h"

//---------------------------------------------------------------------

bool embreeSystemMinimum()
{
	#ifndef _WIN64
		return false;
	#endif
    OSVERSIONINFO osvi;
    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx(&osvi);
    bool isWin7 = 
        ((osvi.dwMajorVersion> 6) ||
        ((osvi.dwMajorVersion==6) && (osvi.dwMinorVersion>=1)));
	return isWin7;
}

//---------------------------------------------------------------------

#ifdef USE_EMBREE

#include "C:\Program Files\embree\embree-bin-2.2_win\include\embree2\rtcore.h"
#include "C:\Program Files\embree\embree-bin-2.2_win\include\embree2\rtcore_ray.h"
#pragma comment(lib, "C:\\Program Files\\embree\\embree-bin-2.2_win\\lib\\x64\\embree.lib")

//---------------------------------------------------------------------

void initEmbree()
{
	rtcInit(NULL);
}

//---------------------------------------------------------------------

void releaseEmbree()
{
	rtcExit();
}

//---------------------------------------------------------------------

float intersectEmbree(Scene * scene, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instID, int &tri, float &u, float &v)
{
	if (!scene || !scene->embree_scene)
		return -1.0f;
	RTCRay ray;
	ray.org[0] = origin.x;
	ray.org[1] = origin.y;
	ray.org[2] = origin.z;
	ray.dir[0] = direction.x;
	ray.dir[1] = direction.y;
	ray.dir[2] = direction.z;
	ray.tnear = 0.0f;
	ray.tfar = maxDist;
	ray.time = 0.0f;
	ray.mask = 0xFFFFFFFF;
	ray.geomID = RTC_INVALID_GEOMETRY_ID;
	ray.primID = RTC_INVALID_GEOMETRY_ID;
	ray.instID = RTC_INVALID_GEOMETRY_ID;

	rtcIntersect((RTCScene)scene->embree_scene, ray);

	if (ray.geomID != RTC_INVALID_GEOMETRY_ID)
	{
		instID = ray.instID;
		instMatrix = scene->instances[instID].matrix_transform;
		tri = ray.primID + scene->meshes[scene->instances[instID].meshID].firstTri;
		u = ray.u;
		v = ray.v;
		return ray.tfar;
	}

	return -1.0f;
}

//---------------------------------------------------------------------

struct EVertex { float x,y,z,a; };
struct ETriangle { int v0, v1, v2; };

//---------------------------------------------------------------------

bool createEmbreeSceneInstances(Scene * sc)
{
	if (!sc)
		return false;
	if (sc->embree_scene)
		rtcDeleteScene(*(RTCScene *)sc->embree_scene);
	sc->embree_scene = NULL;

	RTCScene mainscene = rtcNewScene(RTC_SCENE_STATIC, RTC_INTERSECT1);

	int nm = sc->meshes.objCount;
	RTCScene * iscenes = (RTCScene *)malloc(sizeof(RTCScene)*nm);
	for (int i=0; i<nm; i++)
	{
		Mesh * nmesh = &sc->meshes[i];
		RTCScene scene = rtcNewScene(RTC_SCENE_STATIC, RTC_INTERSECT1);
		sc->notifyProgress("Optimizing geometry", 100.0f * i/nm);

		int ntris = nmesh->lastTri - nmesh->firstTri + 1;
		unsigned int emesh = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, ntris, ntris*3, 1);
		EVertex * everts = (EVertex *)rtcMapBuffer(scene, emesh, RTC_VERTEX_BUFFER);
		ETriangle * etris = (ETriangle *)rtcMapBuffer(scene, emesh, RTC_INDEX_BUFFER);
		for (int j=0; j<ntris; j++)
		{
			everts[3*j+0].x = sc->triangles[nmesh->firstTri+j].V1.x;
			everts[3*j+0].y = sc->triangles[nmesh->firstTri+j].V1.y;
			everts[3*j+0].z = sc->triangles[nmesh->firstTri+j].V1.z;
			everts[3*j+0].a = 0.0f;				 
			everts[3*j+1].x = sc->triangles[nmesh->firstTri+j].V2.x;
			everts[3*j+1].y = sc->triangles[nmesh->firstTri+j].V2.y;
			everts[3*j+1].z = sc->triangles[nmesh->firstTri+j].V2.z;
			everts[3*j+1].a = 0.0f;				 
			everts[3*j+2].x = sc->triangles[nmesh->firstTri+j].V3.x;
			everts[3*j+2].y = sc->triangles[nmesh->firstTri+j].V3.y;
			everts[3*j+2].z = sc->triangles[nmesh->firstTri+j].V3.z;
			everts[3*j+2].a = 0.0f;
			etris[j].v0 = 3*j+0;
			etris[j].v1 = 3*j+1;
			etris[j].v2 = 3*j+2;
		}
		rtcUnmapBuffer(scene, emesh, RTC_VERTEX_BUFFER);
		rtcUnmapBuffer(scene, emesh, RTC_INDEX_BUFFER);
		rtcCommit(scene);
		iscenes[i] = scene;
	}

	int ni = sc->instances.objCount;
	for (int i=0; i<ni; i++)
	{
		MeshInstance ninst = sc->instances[i];
		Matrix4d nmatr = ninst.matrix_transform;

		unsigned int instID = rtcNewInstance(mainscene, iscenes[ninst.meshID]);
		float ematr[] = {	nmatr.M00, nmatr.M10, nmatr.M20, 
							nmatr.M01, nmatr.M11, nmatr.M21, 
							nmatr.M02, nmatr.M12, nmatr.M22, 
							nmatr.M03, nmatr.M13, nmatr.M23 };
		rtcSetTransform(mainscene, instID, RTC_MATRIX_COLUMN_MAJOR, ematr);

		char buf[64];
		sprintf_s(buf, 64, "embree instance id: %d", instID);
		Logger::add(buf);
	}

	rtcCommit(mainscene);
	sc->embree_scene = mainscene;
	return true;
}

//---------------------------------------------------------------------

bool createEmbreeScene(Scene * sc)
{
	if (!sc)
		return false;
	if (sc->embree_scene)
		rtcDeleteScene(*(RTCScene *)sc->embree_scene);
	sc->embree_scene = NULL;
	RTCScene scene = rtcNewScene(RTC_SCENE_STATIC, RTC_INTERSECT1);

	for (int i=0; i<sc->instances.objCount; i++)
	{
		MeshInstance inst = sc->instances[i];
		Mesh nmesh = sc->meshes[inst.meshID];
		Matrix4d matr = inst.matrix_transform;

		int ntris = nmesh.lastTri - nmesh.firstTri + 1;
		unsigned int emesh = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, ntris, ntris*3, 1);

		EVertex * everts = (EVertex *)rtcMapBuffer(scene, emesh, RTC_VERTEX_BUFFER);
		ETriangle * etris = (ETriangle *)rtcMapBuffer(scene, emesh, RTC_INDEX_BUFFER);

		for (int j=0; j<ntris; j++)
		{
			Point3d v1 = matr * sc->triangles[nmesh.firstTri+j].V1;
			Point3d v2 = matr * sc->triangles[nmesh.firstTri+j].V2;
			Point3d v3 = matr * sc->triangles[nmesh.firstTri+j].V3;
			everts[3*j+0].x = v1.x;
			everts[3*j+0].y = v1.y;
			everts[3*j+0].z = v1.z;
			everts[3*j+0].a = 0.0f;
			everts[3*j+1].x = v2.x;
			everts[3*j+1].y = v2.y;
			everts[3*j+1].z = v2.z;
			everts[3*j+1].a = 0.0f;
			everts[3*j+2].x = v3.x;
			everts[3*j+2].y = v3.y;
			everts[3*j+2].z = v3.z;
			everts[3*j+2].a = 0.0f;
			etris[j].v0 = 3*j+0;
			etris[j].v1 = 3*j+1;
			etris[j].v2 = 3*j+2;
		}

		rtcUnmapBuffer(scene, emesh, RTC_VERTEX_BUFFER);
		rtcUnmapBuffer(scene, emesh, RTC_INDEX_BUFFER);
	}

	rtcCommit(scene);
	sc->embree_scene = scene;

	return true;
}

//---------------------------------------------------------------------


#else
	void initEmbree() {}
	void releaseEmbree() {}
	float intersectEmbree(Scene * scene, const Point3d &origin, const Vector3d &direction, Matrix4d &instMatrix, float &maxDist, int &instID, int &tri, float &u, float &v) { return -1; }
	bool createEmbreeScene(Scene * sc) { return false; }
	bool createEmbreeSceneInstances(Scene * sc) { return false; }
#endif

