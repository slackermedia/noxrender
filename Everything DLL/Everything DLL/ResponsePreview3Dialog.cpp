#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "EM2Controls.h"
#include "noxfonts.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "log.h"
#include "CameraResponse.h"
#include "RendererMain2.h"

extern HMODULE hDllModule;
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
bool initResponse3ControlPositions(HWND hWnd);
bool updateResponse3ControlBackgrounds(HWND hWnd);
bool updateResponse3View(HWND hWnd, int startline);
bool response3ChangeSelection(HWND hWnd, int newId);
bool drawResponse3Graph(ImageByteBuffer * imgbuf, int respNum, bool compareToGamma, int numLinesX, int numLinesY);
bool response3GoToSelectionLine(HWND hWnd, int id);
bool response3MakePreview(HWND hWnd, int id);
bool response3CreatePrePostBuffer();


HWND hRespPrevs[NOX_NUM_RESPONSE_FILTERS];
HWND hRespNames[NOX_NUM_RESPONSE_FILTERS];
int curResponse3 = 0;

#define NCOLS 3
#define MAXROWS 3
#define GR_N_LINES_X 5
#define GR_N_LINES_Y 5

ImageBuffer * resp3PrePostImage = NULL;

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK ResponsePreviewDlg3Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				SetFocus(hWnd);

				int dx = 960, dy = 511;
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = dx + wrect.right-wrect.left-crect.right;
				tmprect.bottom = dy + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);
	
				bgBrush = CreateSolidBrush(NGCOL_BG_MEDIUM);
				hBgBMP = generateBackgroundPattern(dx, dy, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();
				int curFunc = (int)lParam;
				curResponse3 = (int)lParam;

				EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_RESP3_TITLE));
				emtTitle->align = EM2Text::ALIGN_CENTER;
				emtTitle->bgImage = hBgBMP;
				emtTitle->setFont(fonts->em2paneltitle, false);
				emtTitle->colText = NGCOL_LIGHT_GRAY;

				EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_RESP3_GRAPH));
				if (empv)
				{
					if (!empv->byteBuff)
						empv->byteBuff = new ImageByteBuffer();
					empv->byteBuff->allocBuffer(360, 350);
					empv->byteBuff->clearBuffer();
					empv->dontLetUserChangeZoom = true;
				}

				EM2GroupBar * emgrGraph = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_RESP3_GR_GRAPH));
				emgrGraph->bgImage = hBgBMP;
				emgrGraph->setFont(fonts->em2groupbar, false);
				EM2GroupBar * emgrList= GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_RESP3_GR_LIST));
				emgrList->bgImage = hBgBMP;
				emgrList->setFont(fonts->em2groupbar, false);

				EM2Text * emtSelResp = GetEM2TextInstance(GetDlgItem(hWnd, IDC_RESP3_TEXT_SELECTED_RESPONSE));
				emtSelResp->bgImage = hBgBMP;
				emtSelResp->setFont(fonts->em2text, false);


				EM2ComboBox * emcResp = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_RESP3_RESPONSE));
				emcResp->bgImage = hBgBMP;
				emcResp->align = EM2ComboBox::ALIGN_LEFT;
				emcResp->moveArrow = true;
				emcResp->setFontMain(fonts->em2combomain, false);
				emcResp->setFontUnwrapped(fonts->em2combounwrap, false);
				emcResp->rowWidth = 190;
				emcResp->setNumItems(NOX_NUM_RESPONSE_FILTERS);
				emcResp->maxShownRows = 12;
				for (int i=0; i<NOX_NUM_RESPONSE_FILTERS; i++)
					emcResp->setItem(i, getResponseFunctionName(i), true);
				emcResp->selected = 0;

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_RESP3_OK));
				embOK->bgImage = hBgBMP;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_RESP3_CANCEL));
				embCancel->bgImage = hBgBMP;
				embCancel->setFont(fonts->em2button, false);

				EM2ScrollBar * emScr = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_RESP3_SCROLLBAR));
				emScr->bgImage = hBgBMP;
				int totalrows = (NOX_NUM_RESPONSE_FILTERS + NCOLS-1)/NCOLS;
				emScr->setWorkArea(MAXROWS*130, totalrows*130);

				initResponse3ControlPositions(hWnd);
				updateResponse3ControlBackgrounds(hWnd);

				HINSTANCE hInst = RendererMain2::getInstance()->hInstance;
				for (int i=0; i<NOX_NUM_RESPONSE_FILTERS; i++)
				{
					hRespPrevs[i] = CreateWindow("EMPView", "", WS_CHILD|WS_VISIBLE, 400, i*10, 163, 90, hWnd, (HMENU)(21100+i), hDllModule, 0);
					EMPView * empv = GetEMPViewInstance(hRespPrevs[i]);
					empv->dontLetUserChangeZoom = true;

					char * name = getResponseFunctionName(i);

					hRespNames[i] = CreateWindow("EM2Text", name, WS_CHILD|WS_VISIBLE, 600, i*16, 163, 16, hWnd, (HMENU)(21500+i), hDllModule, 0);
					EM2Text * emName = GetEM2TextInstance(hRespNames[i]);
					emName->bgImage = hBgBMP;
					emName->setFont(fonts->em2text, false);
					emName->align = EM2Text::ALIGN_CENTER;
					emName->isItHyperLink = true;
				}

				response3ChangeSelection(hWnd, curResponse3);
				drawResponse3Graph(empv->byteBuff, curResponse3, true, GR_N_LINES_X, GR_N_LINES_Y);
				response3GoToSelectionLine(hWnd, curResponse3);
				InvalidateRect(empv->hwnd, NULL, false);

				response3CreatePrePostBuffer();
				for (int i=0; i<NOX_NUM_RESPONSE_FILTERS; i++)
					response3MakePreview(hWnd, i);

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
				if (hBgBMP)
					DeleteObject(hBgBMP);
				hBgBMP = 0;
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)(-1));
			}
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);

				if ((wmId>=21100 && wmId<21100+NOX_NUM_RESPONSE_FILTERS)   ||   (wmId>=21500 && wmId<21500+NOX_NUM_RESPONSE_FILTERS))
				{
					int id = wmId>=21500 ? wmId-21500 : wmId-21100;
					response3ChangeSelection(hWnd, id);
					EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_RESP3_GRAPH));
					drawResponse3Graph(empv->byteBuff, id, true, GR_N_LINES_X, GR_N_LINES_Y);
					InvalidateRect(empv->hwnd, NULL, false);
					if (wmEvent==PV_DOUBLECLICKED)
					{
						EndDialog(hWnd, (INT_PTR)(curResponse3));
					}

				}

				switch (wmId)
				{
					case IDC_RESP3_OK:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)(curResponse3));
							}
						}
						break;
					case IDC_RESP3_CANCEL:
						{
							if (wmEvent == BN_CLICKED)
							{
								EndDialog(hWnd, (INT_PTR)(-1));
							}
						}
						break;
					case IDC_RESP3_RESPONSE:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EM2ComboBox * emcResp = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_RESP3_RESPONSE));
							int id = emcResp->selected;
							response3ChangeSelection(hWnd, id);
							EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_RESP3_GRAPH));
							drawResponse3Graph(empv->byteBuff, id, true, GR_N_LINES_X, GR_N_LINES_Y);
							response3GoToSelectionLine(hWnd, id);
							InvalidateRect(empv->hwnd, NULL, false);
						}
						break;
					case IDC_RESP3_SCROLLBAR:
						{
							CHECK(wmEvent==SB2_POS_CHANGED);
							EM2ScrollBar * emsb = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_RESP3_SCROLLBAR));
							updateResponse3View(hWnd, emsb->workarea_pos/130);
						}
						break;
				}
			}
			break;
		case WM_MOUSEWHEEL:
			{
				EM2ScrollBar * emsb = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_RESP3_SCROLLBAR));
				if (!emsb)
					break;

				int zDelta = (int)wParam;
				if (zDelta>0)	
					emsb->workarea_pos = max(0, emsb->workarea_pos-130); 
				else
					emsb->workarea_pos = min(emsb->size_work-emsb->size_screen, emsb->workarea_pos+130); 
				emsb->updateSliderFromWorkarea();
				InvalidateRect(emsb->hwnd, NULL, false);
				updateResponse3View(hWnd, emsb->workarea_pos/130);
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool initResponse3ControlPositions(HWND hWnd)
{
	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_TITLE),						HWND_TOP,	350, 10,	260, 20, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_GR_GRAPH),					HWND_TOP,	18, 40,		360, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_GR_LIST),					HWND_TOP,	398, 40,	543, 16, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_GRAPH),						HWND_TOP,	18, 74,		360, 350, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_TEXT_SELECTED_RESPONSE),	HWND_TOP,	18, 430,	115, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_RESPONSE),					HWND_TOP,	133, 427,	245, 22, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_SCROLLBAR),					HWND_TOP,	932, 74,	9, 368, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_OK),						HWND_TOP,	398, 467,	72, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_RESP3_CANCEL),					HWND_TOP,	491, 467,	72, 22, 0);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateResponse3ControlBackgrounds(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_RESP3_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2Text * emtSelResp = GetEM2TextInstance(GetDlgItem(hWnd, IDC_RESP3_TEXT_SELECTED_RESPONSE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_TEXT_SELECTED_RESPONSE), 0, 0, emtSelResp->bgShiftX, emtSelResp->bgShiftY);

	EM2ComboBox * emcResponse = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_RESP3_RESPONSE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_RESPONSE), 0, 0, emcResponse->bgShiftX, emcResponse->bgShiftY);

	EM2GroupBar * emgGraph = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_RESP3_GR_GRAPH));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_GR_GRAPH), 0, 0, emgGraph->bgShiftX, emgGraph->bgShiftY);
	EM2GroupBar * emgList = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_RESP3_GR_LIST));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_GR_LIST), 0, 0, emgList->bgShiftX, emgList->bgShiftY);

	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_RESP3_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);
	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_RESP3_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);

	EM2ScrollBar * emScr = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_RESP3_SCROLLBAR));
	updateControlBgShift(GetDlgItem(hWnd, IDC_RESP3_SCROLLBAR), 0, 0, emScr->bgShiftX, emScr->bgShiftY);

	return true;
}

//------------------------------------------------------------------------------------------------

bool updateResponse3View(HWND hWnd, int startline)
{
	CHECK(hWnd);
	int numResps = NOX_NUM_RESPONSE_FILTERS;
	int numRows = (numResps + NCOLS-1) / NCOLS;

	startline = max(0, min(numRows-MAXROWS, startline));

	int sx = 399;
	int sy = 74;
	int gx = 180;
	int gy = 130;
	for (int y=0; y<MAXROWS; y++)
	{
		int y1 = y + startline;
		for (int x=0; x<NCOLS; x++)
		{
			int r = x+y1*NCOLS;
			if (r>=numResps)
				continue;
			int px = sx+x*gx;
			int py = sy+y*gy;
			int py2 = sy+y*gy+96;
			HWND hPrev = hRespPrevs[r];
			HWND hText = hRespNames[r];
			SetWindowPos(hPrev, HWND_TOP, px, py,	160, 90, SWP_NOZORDER);
			SetWindowPos(hText, HWND_TOP, px, py2,	160, 16, SWP_NOZORDER);

			EM2Text * emtName = GetEM2TextInstance(hText);
			emtName->bgShiftX = px;
			emtName->bgShiftY = py2;
		}
	}

	int first = startline * NCOLS;
	int last = max(0, min(numResps-1, first + NCOLS*MAXROWS-1));

	for (int i=0; i<numResps; i++)
	{
		int show = i<first || i>last ? SW_HIDE : SW_SHOW;
		HWND hPrev = hRespPrevs[i];
		HWND hText = hRespNames[i];
		ShowWindow(hPrev, show);
		ShowWindow(hText, show);
	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool response3ChangeSelection(HWND hWnd, int newId)
{
	EMPView * empv = GetEMPViewInstance(hRespPrevs[newId]);
	EM2Text * emname = GetEM2TextInstance(hRespNames[newId]);
	EMPView * empvold = GetEMPViewInstance(hRespPrevs[curResponse3]);
	EM2Text * emnameold = GetEM2TextInstance(hRespNames[curResponse3]);

	if (empvold)
	{
		empvold->colBorder = NGCOL_LIGHT_GRAY;
		empvold->showBorder = false;
	}
	if (emnameold)
	{
		emnameold->colText = NGCOL_LIGHT_GRAY;
		emnameold->colLink = NGCOL_LIGHT_GRAY;
	}
	InvalidateRect(hRespPrevs[curResponse3], NULL, false);
	InvalidateRect(hRespNames[curResponse3], NULL, false);

	if (empv)
	{
		empv->colBorder = NGCOL_YELLOW;
		empv->showBorder = true;
	}
	if (emname)
	{
		emname->colText = NGCOL_YELLOW;
		emname->colLink = NGCOL_YELLOW;
	}
	InvalidateRect(hRespPrevs[newId], NULL, false);
	InvalidateRect(hRespNames[newId], NULL, false);

	EM2ComboBox * emcResp = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_RESP3_RESPONSE));
	if (emcResp)
	{
		emcResp->selected = newId;
		InvalidateRect(emcResp->hwnd, NULL, false);
	}

	curResponse3 = newId;
	return true;
}

//------------------------------------------------------------------------------------------------

bool drawResponse3Graph(ImageByteBuffer * imgbuf, int respNum, bool compareToGamma, int numLinesX, int numLinesY)
{
	CHECK(imgbuf);
	imgbuf->clearBuffer();
	int w = imgbuf->width;
	int h = imgbuf->height;
	int ww = w-1;
	int hh = h-1;


	bool showLines = true;
	int llength = 6;
	int ldist = 64;
	COLORREF linesColor = RGB(48,48,48);
	COLORREF cGammaColor = RGB(64,64,64);
	numLinesX = max(0, min(32, numLinesX));
	numLinesY = max(0, min(32, numLinesY));

	if (showLines)
	{
		for (int yy=0; yy<numLinesY; yy++)
		{
			int y = (yy+1)*h/(numLinesY+1);
			for (int x=0; x<w; x++)
			{
				int a = x%(llength*2)/llength;
				COLORREF col = linesColor*(1-a);
				imgbuf->imgBuf[y][x] = col;
			}
		}
		for (int xx=0; xx<numLinesX; xx++)
		{
			int x = (xx+1)*w/(numLinesX+1);
			for (int y=0; y<h; y++)
			{
				int a = y%(llength*2)/llength;
				COLORREF col = linesColor*(a);
				imgbuf->imgBuf[y][x] = col;
			}
		}

	}


	if (compareToGamma)
	{
		float cgamma = 1.0f/2.2f;
		for (int i=0; i<ww; i++)
		{
			int x1 = i;
			int x2 = i+1;
			float fy1 = x1/(float)ww;
			float fy2 = x2/(float)ww;
			fy1 = pow(fy1, cgamma); 
			fy2 = pow(fy2, cgamma); 
			int y1 = min(hh, max(0, hh-(int)(fy1*hh)));
			int y2 = min(hh, max(0, hh-(int)(fy2*hh)));
			if (y1>y2)
				for (int yy=y1; yy>=y2; yy--)
					imgbuf->imgBuf[yy][x1] = cGammaColor;
			else
				for (int yy=y1; yy<=y2; yy++)
					imgbuf->imgBuf[yy][x1] = cGammaColor;
		}
	}


	float rgamma = 1.0f/2.2f;
	if (respNum == 0)
	{
		for (int i=0; i<ww; i++)
		{
			int x1 = i;
			int x2 = i+1;
			float fy1 = x1/(float)ww;
			float fy2 = x2/(float)ww;
			fy1 = pow(fy1, rgamma); 
			fy2 = pow(fy2, rgamma); 
			int y1 = min(hh, max(0, hh-(int)(fy1*hh)));
			int y2 = min(hh, max(0, hh-(int)(fy2*hh)));
			if (y1>y2)
				for (int yy=y1; yy>=y2; yy--)
					imgbuf->imgBuf[yy][x1] = RGB(255,255,255);
			else
				for (int yy=y1; yy<=y2; yy++)
					imgbuf->imgBuf[yy][x1] = RGB(255,255,255);
		}
		return true;
	}


	if (respNum<0 || respNum>NOX_NUM_RESPONSE_FILTERS)
		return false;

	float * floatsRed	= getResponseFunctionRed(respNum);
	float * floatsGreen	= getResponseFunctionGreen(respNum);
	float * floatsBlue	= getResponseFunctionBlue(respNum);
	CHECK(floatsRed);
	CHECK(floatsGreen);
	CHECK(floatsBlue);

	for (int i=0; i<ww; i++)
	{
		float yr1 = floatsRed[  (i+0)*1023/ww];
		float yr2 = floatsRed[  (i+1)*1023/ww];
		float yg1 = floatsGreen[(i+0)*1023/ww];
		float yg2 = floatsGreen[(i+1)*1023/ww];
		float yb1 = floatsBlue[ (i+0)*1023/ww];
		float yb2 = floatsBlue[ (i+1)*1023/ww];
		int ir1 = min(hh, max(0, hh-(int)(yr1*hh)));
		int ir2 = min(hh, max(0, hh-(int)(yr2*hh)));
		int ig1 = min(hh, max(0, hh-(int)(yg1*hh)));
		int ig2 = min(hh, max(0, hh-(int)(yg2*hh)));
		int ib1 = min(hh, max(0, hh-(int)(yb1*hh)));
		int ib2 = min(hh, max(0, hh-(int)(yb2*hh)));

		if (ir1>ir2)
			for (int yy=ir1; yy>=ir2; yy--)
				imgbuf->imgBuf[yy][i] |= RGB(255,0,0);
		else
			for (int yy=ir1; yy<=ir2; yy++)
				imgbuf->imgBuf[yy][i] |= RGB(255,0,0);

		if (ig1>ig2)
			for (int yy=ig1; yy>=ig2; yy--)
				imgbuf->imgBuf[yy][i] |= RGB(0,255,0);
		else
			for (int yy=ig1; yy<=ig2; yy++)
				imgbuf->imgBuf[yy][i] |= RGB(0,255,0);

		if (ib1>ib2)
			for (int yy=ib1; yy>=ib2; yy--)
				imgbuf->imgBuf[yy][i] |= RGB(0,0,255);
		else
			for (int yy=ib1; yy<=ib2; yy++)
				imgbuf->imgBuf[yy][i] |= RGB(0,0,255);

	}

	return true;
}

//------------------------------------------------------------------------------------------------

bool response3GoToSelectionLine(HWND hWnd, int id)
{
	CHECK(hWnd);
	int line = id / NCOLS - MAXROWS/2;
	bool ok = updateResponse3View(hWnd, line);

	EM2ScrollBar * emsb = GetEM2ScrollBarInstance(GetDlgItem(hWnd, IDC_RESP3_SCROLLBAR));
	if (emsb)
	{
		emsb->workarea_pos = max(0, min(emsb->size_work-emsb->size_screen, line*130)); 
		emsb->updateSliderFromWorkarea();
		InvalidateRect(emsb->hwnd, NULL, false);
	}

	return ok;
}

//------------------------------------------------------------------------------------------------

bool response3CreatePrePostBuffer()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Camera * cam = sc->getActiveCamera();
	CHECK(cam);
	bool giperpixel = (cam->gi_type == Camera::GI_TYPE_PT);
	ImageBuffer * srcBigImage = cam->blendBuffers(true, true, giperpixel);
	CHECK(srcBigImage);

	int XX = 160, YY = 90;
	float rtX = XX / (float)srcBigImage->width;
	float rtY = YY / (float)srcBigImage->height;
	int w = XX;
	int h = YY;
	if (rtX > rtY)
		w = (int)(YY * srcBigImage->width/srcBigImage->height);
	else
		h = (int)(XX * srcBigImage->height/srcBigImage->width);

	if (resp3PrePostImage)
	{
		resp3PrePostImage->freeBuffer();
		delete resp3PrePostImage;
	}
	resp3PrePostImage = srcBigImage->copyResize(w, h);

	srcBigImage->freeBuffer();
	delete srcBigImage;
	srcBigImage = NULL;

	CHECK(resp3PrePostImage);

	return true;
}

//------------------------------------------------------------------------------------------------

bool response3MakePreview(HWND hWnd, int id)
{
	CHECK(hWnd);
	CHECK(id>=0 && id<NOX_NUM_RESPONSE_FILTERS);

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	Camera * cam = sc->getActiveCamera();
	CHECK(cam);
	ImageModifier iMod;
	iMod.copyDataFromAnother(&cam->iMod);
	iMod.setResponseFunctionNumber(id);

	EMPView * empv = GetEMPViewInstance(hRespPrevs[id]);
	CHECK(empv);
	if (!empv->byteBuff)
	{
		empv->byteBuff = new ImageByteBuffer();
		empv->byteBuff->clearBuffer();
	}

	CHECK(resp3PrePostImage);
	CHECK(resp3PrePostImage->imgBuf);
	int w = resp3PrePostImage->width;
	int h = resp3PrePostImage->height;
	if (empv->byteBuff->width!=w  ||    empv->byteBuff->height!=h)
		empv->byteBuff->allocBuffer(w,h);

	ImageBuffer * pp = resp3PrePostImage->doPostProcess(1, &iMod, NULL, cam->depthBuf, "Creating previews...");
	CHECK(pp);

	pp->copyToImageByteBuffer(empv->byteBuff, RGB(0,0,0));
	

	InvalidateRect(empv->hwnd, NULL, false);
	return true;
}

//------------------------------------------------------------------------------------------------
