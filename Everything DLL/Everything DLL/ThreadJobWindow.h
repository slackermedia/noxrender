#ifndef __thread_job_window_h
#define __thread_job_window_h

#include <windows.h>

//------------------------------------------------------------------------------------------------

// for old gui

class ThreadWindow
{
public:
	HWND hTWindow;
	bool opened;
	bool canAbort;
	bool aborted;

	static ThreadWindow * getCurrentInstance();
	bool createWindow(HWND hParent, HINSTANCE hInstance);	
	bool closeWindow();
	bool setText(char * newText);
	bool setWindowTitle(char * newText);
	bool setProgress(float pos);	// 0-100
	static void notifyProgressBarCallback(char * message, float progress);
	void allowAbort();
	void denyAbort();
	void abort();

	ThreadWindow();
	~ThreadWindow();
};

//------------------------------------------------------------------------------------------------

// for new gui

class ThreadWindow2
{
public:
	HWND hTWindow;
	bool opened;
	bool canAbort;
	bool aborted;

	static ThreadWindow2 * getCurrentInstance();
	bool createWindow(HWND hParent, HINSTANCE hInstance);	
	bool closeWindow();
	bool setText(char * newText);
	bool setWindowTitle(char * newText);
	bool setProgress(float pos);	// 0-100
	static void notifyProgressBarCallback(char * message, float progress);
	void allowAbort();
	void denyAbort();
	void abort();

	ThreadWindow2();
	~ThreadWindow2();
};

//------------------------------------------------------------------------------------------------

#endif
