#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "CameraResponse.h"


HWND getPrevWindow(HWND hDlg, int col, int row);
HWND getNameWindow(HWND hDlg, int col, int row);
bool createPreviews();
bool showRespPrevPage(HWND hDlg, int page);

ImageByteBuffer responsePreviews[91];
int responsePage = 0;
int currentFunction = 0;


INT_PTR CALLBACK ResponsePreviewDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH hBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				hBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				currentFunction = (int)lParam;
				responsePage = currentFunction/12;

				EMButton   * emOK		= GetEMButtonInstance(GetDlgItem(hWnd, IDC_RESP_PREV_OK));
				EMButton   * emCancel	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_RESP_PREV_CANCEL));
				EMButton   * emPrevious	= GetEMButtonInstance(GetDlgItem(hWnd, IDC_RESP_PREV_PREVIOUS));
				EMButton   * emNext		= GetEMButtonInstance(GetDlgItem(hWnd, IDC_RESP_PREV_NEXT));
				GlobalWindowSettings::colorSchemes.apply(emOK);
				GlobalWindowSettings::colorSchemes.apply(emCancel);
				GlobalWindowSettings::colorSchemes.apply(emPrevious);
				GlobalWindowSettings::colorSchemes.apply(emNext);

				RECT crect, wrect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				int bx = (wrect.right-wrect.left) - (crect.right-crect.left);
				int by = (wrect.bottom-wrect.top) - (crect.bottom-crect.top);

				SetWindowPos(hWnd, HWND_TOP,  0, 0,  1010+bx, 690+by,   SWP_NOOWNERZORDER | SWP_NOMOVE);

				SetWindowPos(emPrevious->hwnd,	HWND_TOP,  10,  650,  100, 26,   SWP_NOOWNERZORDER);
				SetWindowPos(emOK->hwnd,		HWND_TOP,  400, 650,  100, 26,   SWP_NOOWNERZORDER);
				SetWindowPos(emCancel->hwnd,	HWND_TOP,  510, 650,  100, 26,   SWP_NOOWNERZORDER);
				SetWindowPos(emNext->hwnd,		HWND_TOP,  900, 650,  100, 26,   SWP_NOOWNERZORDER);

				for (int y=0; y<3; y++)
					for (int x=0; x<4; x++)
					{
						HWND hFPrev = getPrevWindow(hWnd, x,y);
						SetWindowPos(hFPrev, HWND_TOP,  x*250+10,  y*210+10,  240, 180,   SWP_NOOWNERZORDER);
						EMPView * emFPrev = GetEMPViewInstance(hFPrev);
						if (emFPrev)
						{
							if (!emFPrev->byteBuff)
								emFPrev->byteBuff = new ImageByteBuffer();
							emFPrev->dontLetUserChangeZoom = true;

						}
						GlobalWindowSettings::colorSchemes.apply(emFPrev);

						HWND hName = getNameWindow(hWnd, x,y);
						SetWindowPos(hName, HWND_TOP,  x*250+10,  y*210+194,  240, 18,   SWP_NOOWNERZORDER);
						EMText * emt = GetEMTextInstance(hName);
						emt->changeCaption("filter name");
						emt->align = EMText::ALIGN_CENTER;
						GlobalWindowSettings::colorSchemes.apply(emt, true);
					}

				bool prevOK = createPreviews();
				if (prevOK)
				{
					showRespPrevPage(hWnd, responsePage);
				}
			}
			break;
		case WM_DESTROY:
			{
				DeleteObject(hBrush);
				hBrush = 0;
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)(-1));
			}
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
				case IDC_RESP_PREV_OK:
					{
						if (wmEvent == BN_CLICKED)
						{
							EndDialog(hWnd, (INT_PTR)(currentFunction));
						}
					}
					break;
				case IDC_RESP_PREV_CANCEL:
					{
						if (wmEvent == BN_CLICKED)
						{
							EndDialog(hWnd, (INT_PTR)(-1));
						}
					}
					break;
				case IDC_RESP_PREV_NEXT:
					{
						if (wmEvent == BN_CLICKED)
						{
							responsePage++;
							if (responsePage>((NOX_NUM_RESPONSE_FILTERS+1)/12))
								responsePage--;
							showRespPrevPage(hWnd, responsePage);
						}
					}
					break;
				case IDC_RESP_PREV_PREVIOUS:
					{
						if (wmEvent == BN_CLICKED)
						{
							responsePage--;
							if (responsePage<0)
								responsePage=0;
							showRespPrevPage(hWnd, responsePage);
						}
					}
					break;
				case IDC_RESP_PREV_IMG1:
				case IDC_RESP_PREV_IMG2:
				case IDC_RESP_PREV_IMG3:
				case IDC_RESP_PREV_IMG4:
				case IDC_RESP_PREV_IMG5:
				case IDC_RESP_PREV_IMG6:
				case IDC_RESP_PREV_IMG7:
				case IDC_RESP_PREV_IMG8:
				case IDC_RESP_PREV_IMG9:
				case IDC_RESP_PREV_IMG10:
				case IDC_RESP_PREV_IMG11:
				case IDC_RESP_PREV_IMG12:
					{
						int i=10000;
						switch (wmId)
						{
							case IDC_RESP_PREV_IMG1: i=0; break;
							case IDC_RESP_PREV_IMG2: i=1; break;
							case IDC_RESP_PREV_IMG3: i=2; break;
							case IDC_RESP_PREV_IMG4: i=3; break;
							case IDC_RESP_PREV_IMG5: i=4; break;
							case IDC_RESP_PREV_IMG6: i=5; break;
							case IDC_RESP_PREV_IMG7: i=6; break;
							case IDC_RESP_PREV_IMG8: i=7; break;
							case IDC_RESP_PREV_IMG9: i=8; break;
							case IDC_RESP_PREV_IMG10: i=9; break;
							case IDC_RESP_PREV_IMG11: i=10; break;
							case IDC_RESP_PREV_IMG12: i=11; break;
						}
						CHECK(wmEvent==PV_CLICKED);
						int n = responsePage*12 + i;
						if (n>=0 && n<=NOX_NUM_RESPONSE_FILTERS)
						{
							currentFunction = n;
							showRespPrevPage(hWnd, responsePage);
						}
					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)hBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)hBrush;
			}
			break;
	}
	return false;
}

HWND getPrevWindow(HWND hDlg, int col, int row)
{
	switch (row)
	{
		case 0:
		{
			switch (col)
			{
				case 0: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG1);
				case 1: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG2);
				case 2: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG3);
				case 3: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG4);
			}
		}
		break;
		case 1:
		{
			switch (col)
			{
				case 0: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG5);
				case 1: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG6);
				case 2: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG7);
				case 3: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG8);
			}
		}
		break;
		case 2:
		{
			switch (col)
			{
				case 0: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG9);
				case 1: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG10);
				case 2: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG11);
				case 3: return GetDlgItem(hDlg, IDC_RESP_PREV_IMG12);
			}
		}
		break;
	}
	return (HWND) 0;
}


HWND getNameWindow(HWND hDlg, int col, int row)
{
	switch (row)
	{
		case 0:
		{
			switch (col)
			{
				case 0: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME1);
				case 1: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME2);
				case 2: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME3);
				case 3: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME4);
			}
		}
		break;
		case 1:
		{
			switch (col)
			{
				case 0: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME5);
				case 1: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME6);
				case 2: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME7);
				case 3: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME8);
			}
		}
		break;
		case 2:
		{
			switch (col)
			{
				case 0: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME9);
				case 1: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME10);
				case 2: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME11);
				case 3: return GetDlgItem(hDlg, IDC_RESP_PREV_NAME12);
			}
		}
		break;
	}
	return (HWND) 0;
}

bool createPreviews()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	CHECK(sc);
	
	Camera * cam = sc->getActiveCamera();
	CHECK(cam);
	ImageBuffer * srcBigImage = cam->blendBuffers();
	CHECK(srcBigImage);

	float rtX = 240.0f / srcBigImage->width;
	float rtY = 180.0f / srcBigImage->height;
	int w = 240;
	int h = 180;
	if (rtX > rtY)
		w = (int)(180.0f * srcBigImage->width/srcBigImage->height);
	else
		h = (int)(240.0f * srcBigImage->height/srcBigImage->width);

	ImageBuffer * srcImage = srcBigImage->copyResize(w, h);

	ImageModifier iMod;
	iMod.copyDataFromAnother(&cam->iMod);

	for (int i=0; i<=90; i++)
	{
		iMod.setResponseFunctionNumber(i);
		ImageBuffer * pp = srcImage->doPostProcess(1, &iMod, NULL, cam->depthBuf, "Creating previews...");
		if (!pp)
		{
			MessageBox(0, "Can't repaint. Not enough memory.", "Error", 0);
			return false;
		}

		responsePreviews[i].allocBuffer(w,h);
		pp->copyToImageByteBuffer(&(responsePreviews[i]), RGB(0,0,0));
		pp->freeBuffer();
		delete pp;
		pp = NULL;
	}

	return true;
}

bool showRespPrevPage(HWND hDlg, int page)
{
	if (page<0 || page>((NOX_NUM_RESPONSE_FILTERS+1)/12))
		return false;

	if (page == 0)
		disableControl(hDlg, IDC_RESP_PREV_PREVIOUS);
	else
		enableControl(hDlg, IDC_RESP_PREV_PREVIOUS);
	if (page == (NOX_NUM_RESPONSE_FILTERS+1)/12)
		disableControl(hDlg, IDC_RESP_PREV_NEXT);
	else
		enableControl(hDlg, IDC_RESP_PREV_NEXT);

	for (int y=0; y<3; y++)
		for (int x=0; x<4; x++)
		{
			int n = page*12 + y*4+x;
			HWND hFPrev = getPrevWindow(hDlg, x,y);
			EMPView * emFPrev = GetEMPViewInstance(hFPrev);
			if (emFPrev)
			{
				emFPrev->colBorder = GlobalWindowSettings::colorSchemes.EMPViewBorder;
				if (n==currentFunction)
					emFPrev->colBorder = RGB(255,0,0);

				if (n<=NOX_NUM_RESPONSE_FILTERS  &&  n>=0)
					emFPrev->byteBuff->copyImageFrom(&(responsePreviews[n]));
				else
					emFPrev->byteBuff->clearBuffer();
			}
			InvalidateRect(emFPrev->hwnd, NULL, false);

			HWND hName = getNameWindow(hDlg, x,y);
			EMText * emt = GetEMTextInstance(hName);
			char * fname = NULL;
			if (n<=NOX_NUM_RESPONSE_FILTERS  &&  n>=0)
				fname = getResponseFunctionName(n);
			else 
				fname = "";
			emt->changeCaption(fname);
		}

	return true;
}

