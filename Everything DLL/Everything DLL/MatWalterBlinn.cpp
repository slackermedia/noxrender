#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>

#define clamp(x,a,b)   max((a),min((x),(b)))

float getFresnelTemp(float IOR, float kos, bool goingIn);
float getFresnelTemp(float IOR, float kos);

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionWalterMicrofacetBlinn(HitData & hd, float &probability)
{
	float rough = getRough(hd.tU, hd.tV);
	float alphap = 2/(rough*rough)-2;
	#ifdef RAND_PER_THREAD
		float a = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float a = ((float)rand()/(float)RAND_MAX);
	#endif
	float cosThetaH = pow(a, 1.0f/(alphap+2));
	float sinThetaH = sqrt(1-max(0, cosThetaH*cosThetaH));
	
	Vector3d temp = Vector3d::random();
	Vector3d H_ = temp^hd.normal_shade;
	H_.normalize();
	Vector3d H = H_*sinThetaH + hd.normal_shade*cosThetaH;
	H.normalize();


	if (transmOn)
	{	
		#ifdef RAND_PER_THREAD
			float b = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
		#else
			float b = ((float)rand()/(float)RAND_MAX);
		#endif
		float f;
		if (hd.in*hd.normal_shade > 0)
			f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H));
		else
			f = getFresnelTemp(1.0f/fresnel.IOR, fabs(hd.in*H));

		if (b > f)
		{
			float ior;
			ior = fresnel.IOR;
			bool inner;
			Vector3d res2 = hd.in.refractSnell(H, ior, inner);
			hd.out = res2;
			probability = getProbabilityWalterMicrofacetBlinn(hd);
			return res2;
		}
	}

	Vector3d res1 = hd.in.reflect(H);
	res1.normalize();
	if ((res1*hd.normal_shade)*(hd.in*hd.normal_shade) < 0)
		return randomNewDirectionWalterMicrofacetBlinn(hd, probability);

	hd.out = res1;

	probability = getProbabilityWalterMicrofacetBlinn(hd);

	return res1;
}

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilityWalterMicrofacetBlinn(HitData & hd)
{
	float rough = getRough(hd.tU, hd.tV);
	float alphap = 2/(rough*rough)-2;
	bool goingOut = (hd.in*hd.normal_shade < 0   &&   hd.out*hd.normal_shade > 0);
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);
	Vector3d H;
	if (sameSide)
	{
		H = hd.in + hd.out;
		H.normalize();	
	}
	else
	{
		H = hd.in + hd.out * fresnel.IOR;
		H.normalize();
		if (H*hd.normal_shade < 0)
			H.invert();
	}
	float cosThetaH = fabs(H*hd.normal_shade);
	float D = (alphap+2)*0.5f*PER_PI * pow(cosThetaH, alphap);
	
	float jacobian = 1;
	if (transmOn && !sameSide)
		if (goingOut)
			jacobian = fabs(hd.out*H)/pow((hd.in*H * fresnel.IOR + (hd.out*H)),2);
		else
			jacobian = fresnel.IOR*fresnel.IOR*fabs(hd.out*H)/pow((hd.in*H + fresnel.IOR*(hd.out*H)), 2);
	else
		jacobian = 0.25f/(hd.out * H);

	return D*cosThetaH*jacobian;
}

//-------------------------------------------------------------------------------------------

Color4 MatLayer::getBRDFWalterMicrofacetBlinn(HitData & hd)
{
	float rough = getRough(hd.tU, hd.tV);
	float alphap = 2/(rough*rough)-2;
	bool goingOut = (hd.in*hd.normal_shade < 0   &&   hd.out*hd.normal_shade > 0);
	bool sameSide = ((hd.in*hd.normal_shade)*(hd.out*hd.normal_shade) > 0);
	Vector3d H;
	if (sameSide)
	{
		H = hd.in + hd.out;
		H.normalize();	
	}
	else
	{
		H = hd.in + hd.out * fresnel.IOR;
		H.normalize();
		if (H*hd.normal_shade < 0)
			H.invert();
	}
	float cosThetaH = fabs(H*hd.normal_shade);
	float sin2ThetaH = (1-cosThetaH*cosThetaH);
	float tan2ThetaH = sin2ThetaH/(cosThetaH*cosThetaH);

	float D = max(0, cosThetaH) * (alphap+2)*0.5f*PER_PI * pow(cosThetaH, alphap);
	float a = sqrt(alphap*0.5f+1.0f);
	float ai,ao,gg;

	float cosTheta_i = fabs(hd.in *hd.normal_shade);
	float cosTheta_o = fabs(hd.out*hd.normal_shade);
	float tan2Theta_i = (1-cosTheta_i*cosTheta_i)/(cosTheta_i*cosTheta_i);
	float tan2Theta_o = (1-cosTheta_o*cosTheta_o)/(cosTheta_o*cosTheta_o);
	float sinTheta_i = sqrt(1-cosTheta_i*cosTheta_i);
	float sinTheta_o = sqrt(1-cosTheta_o*cosTheta_o);
	float ctg_i = cosTheta_i/sinTheta_i;
	float ctg_o = cosTheta_o/sinTheta_o;

	ai = a*ctg_i;
	ao = a*ctg_o;

	//G_i
	if (ai>1.6f)
		gg = 1;
	else
		gg = (3.535f*ai + 2.181f*ai*ai)/(1 + 2.276f*ai + 2.577f*ai*ai);

	float kk,G_i,G_o;
	kk  = max(0, (H*hd.in/cosTheta_i));
	kk=1;
	G_i = kk * gg;

	//G_o
	if (ao>1.6f)
		gg = 1;
	else
		gg = (3.535f*ao + 2.181f*ao*ao)/(1 + 2.276f*ao + 2.577f*ao*ao);

	kk  = max(0, (H*hd.out/cosTheta_o));
	kk=1;
	G_o = kk * gg;

	float f;
	if (hd.in*hd.normal_shade>0)
		f = getFresnelTemp(fresnel.IOR, fabs(hd.in*H));
	else
		f = getFresnelTemp(1.0f/fresnel.IOR, fabs(hd.in*H));

	float FF = f;
	if (transmOn)
	{
		if (sameSide)
			FF = f;
		else
		{
			FF = 1-f;
			Color4 absorption = Color4(1,1,1);
			if ((hd.in*hd.normal_shade < 0)   &&   (absOn))
			{	// going out of object - absorb part of energy
				absorption.r = pow(1.0f/max(0.01f, gAbsColor.r), -hd.lastDist/absDist);
				absorption.g = pow(1.0f/max(0.01f, gAbsColor.g), -hd.lastDist/absDist);
				absorption.b = pow(1.0f/max(0.01f, gAbsColor.b), -hd.lastDist/absDist);
			}
			float brdf;
			if (goingOut)
				brdf = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
						FF*G_i*G_o*D/(pow((hd.in*H) * fresnel.IOR + (hd.out*H),2));
			else
				brdf = fabs(hd.in*H)*fabs(hd.out*H)/fabs(hd.in*hd.normal_shade)/fabs(hd.out*hd.normal_shade) * 
						fresnel.IOR*fresnel.IOR*FF*G_i*G_o*D/(pow((hd.in*H) + fresnel.IOR * (hd.out*H),2));
			return getColor0(hd.tU, hd.tV)*brdf*absorption;
		}
	}

	float brdf = G_i*G_o * D * f * 0.25f / cosTheta_i / cosTheta_o;
	return getColor0(hd.tU, hd.tV)* brdf;
}

//-------------------------------------------------------------------------------------------
