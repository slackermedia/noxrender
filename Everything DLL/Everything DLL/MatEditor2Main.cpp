#include "raytracer.h"
#include "MatEditor2.h"
#include "resource.h"
#include "EMControls.h"
#include "EM2Controls.h"
#include "log.h"
#include "dialogs.h"
#include "XML_IO.h"

MatEditor2 * MatEditor2::mewInstance = NULL;
bool MatEditor2::isWindowClassRegistered = false;
extern HMODULE hDllModule;


//----------------------------------------------------------------------

MatEditor2::MatEditor2()
{
	autoLoadFilename = NULL;
	fonts = NOXFontManager::getInstance();
	hBmpBg = generateBackgroundPattern(1920, 1200, 0,0, RGB(40,40,40), RGB(35,35,35));
	isMatLibOpened = false;
	refreshThreadRunning = false;
	isMatEditOpened = false;
	saveOnOK = false;
}

//----------------------------------------------------------------------

MatEditor2::~MatEditor2()
{
	if (hBmpBg)
		DeleteObject(hBmpBg);
	hBmpBg = 0;
}

//----------------------------------------------------------------------

bool MatEditor2::createWindow(HINSTANCE hInst, HWND parent, int mode, char * fileToLoad)
{
	autoLoadFilename = copyString(fileToLoad);

	Raytracer * rtr = Raytracer::getInstance();
	if (rtr->runDialogsUnicode)
	{
		return ( 0 != DialogBoxParamW(hDllModule, MAKEINTRESOURCEW(IDD_MAT_EDITOR2), parent, MatEditor2::MatEditor2WndProc,(LPARAM)(mode)) );
	}
	else
	{
		return ( 0 != DialogBoxParam(hDllModule, MAKEINTRESOURCE(IDD_MAT_EDITOR2), parent, MatEditor2::MatEditor2WndProc,(LPARAM)(mode)) );
	}

	return true;
}

//----------------------------------------------------------------------

MatEditor2 * MatEditor2::getInstance()
{
	if (!mewInstance)
		mewInstance = new MatEditor2();
	return mewInstance;
}

//----------------------------------------------------------------------

ATOM MatEditor2::registerMyClass(HINSTANCE hInst)	// not used
{
	WNDCLASSEX wcex;
	HICON sIcon = LoadIcon(hDllModule, MAKEINTRESOURCE(IDI_ICON1));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)MatEditor2::MatEditor2WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInst;
	wcex.hIcon			= sIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= 0;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "NOXMatEditor2";
	wcex.hIconSm		= sIcon;
	return RegisterClassEx(&wcex);
}

//----------------------------------------------------------------------

HWND MatEditor2::getHWND()
{
	return hMain;
}

//----------------------------------------------------------------------

bool MatEditor2::acceptMaterial()
{


	return true;
}

//----------------------------------------------------------------------

void MatEditor2::setSaveMode(bool saveOnOKButton)
{
	saveOnOK = saveOnOKButton;
}

//----------------------------------------------------------------------

void MatEditor2::setLegacyMode(bool legacy)
{
	Logger::add("Setting engine to legacy mode.");
	Raytracer::getInstance()->use_embree = !legacy;
}

//----------------------------------------------------------------------

MaterialNox * MatEditor2::getEditedMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	int edm = sc->sscene.editableMaterial;
	MaterialNox * mat = sc->mats[edm];
	return mat;
}

//----------------------------------------------------------------------

MatLayer * MatEditor2::getEditedMatLayer()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	HWND hCurrent = GetDlgItem (hMain, IDC_MATEDIT2_LAYERS_LIST);
	EM2ListView * emlv = GetEM2ListViewInstance(hCurrent);
	CHECK(emlv);
	int n = emlv->selected;
	if (n >= mat->layers.objCount  ||  n < 0)
		return NULL;
	return &(mat->layers[n]);
}

//----------------------------------------------------------------------

bool MatEditor2::fillLayersList()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	HWND hCurrent = GetDlgItem (hMain, IDC_MATEDIT2_LAYERS_LIST);
	EM2ListView * emlv = GetEM2ListViewInstance(hCurrent);

	emlv->freeEntries();

	char buf[64];
	for (int i=0; i<mat->layers.objCount; i++)
	{
		MatLayer * mlay = &(mat->layers[i]);
		emlv->addEmptyRow();

		// name
		sprintf_s(buf, 64, "LAYER %d", (i+1));
		emlv->setData(0, i, buf);

		// weight
		if (mlay->tex_weight.isValid()  &&   mlay->use_tex_weight)
			sprintf_s(buf, 64, "TEX");
		else
			sprintf_s(buf, 64, "%d", mlay->contribution);
		emlv->setData(1, i, buf);

		// rough
		if (mlay->tex_rough.isValid()  &&  mlay->use_tex_rough)
			sprintf_s(buf, 64, "TEX");
		else
			sprintf_s(buf, 64, "%d", (int)(mlay->roughness*100+0.01f));
		emlv->setData(2, i, buf);

		// normals
		if (mlay->use_tex_normal  &&  mlay->tex_normal.isValid())
			emlv->setData(3, i, "YES");

		// emission
		if (mlay->type == MatLayer::TYPE_EMITTER)
			emlv->setData(4, i, "YES");

		// transmission
		if (mlay->transmOn)
			emlv->setData(5, i, "YES");

		// sss
		if (mlay->sssON)
			emlv->setData(6, i, "YES");
	}

	if (emlv->selected>=emlv->getNumRows())
		emlv->selected = emlv->getNumRows()-1;
	if (emlv->selected<0)
		emlv->selected = 0;

	InvalidateRect(hCurrent, NULL, false);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::updateCurrentLayerPropsOnList()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	HWND hCurrent = GetDlgItem (hMain, IDC_MATEDIT2_LAYERS_LIST);
	EM2ListView * emlv = GetEM2ListViewInstance(hCurrent);
	CHECK(emlv);

	int i = emlv->selected;
	if (i<0 || i>=mat->layers.objCount)
		return false;
	char buf[64];
	MatLayer * mlay = &(mat->layers[i]);

	// name
	sprintf_s(buf, 64, "LAYER %d", (i+1));
	emlv->setData(0, i, buf);

	// weight
	if (mlay->tex_weight.isValid()  &&   mlay->use_tex_weight)
		sprintf_s(buf, 64, "TEX");
	else
		sprintf_s(buf, 64, "%d", mlay->contribution);
	emlv->setData(1, i, buf);

	// rough
	if (mlay->tex_rough.isValid()  &&  mlay->use_tex_rough)
		sprintf_s(buf, 64, "TEX");
	else
		sprintf_s(buf, 64, "%d", (int)(mlay->roughness*100+0.01f));
	emlv->setData(2, i, buf);

	emlv->setData(3, i, (mlay->use_tex_normal  &&  mlay->tex_normal.isValid()) ? "YES" : "");
	emlv->setData(4, i, (mlay->type == MatLayer::TYPE_EMITTER) ? "YES" : "");
	emlv->setData(5, i, mlay->transmOn ? "YES" : "");
	emlv->setData(6, i, mlay->sssON ? "YES" : "");

	InvalidateRect(hCurrent, NULL, false);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::layerAdd()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (mat->layers.objCount < 16)
	{
		MatLayer newmat;
		mat->layers.add(newmat, &(MatLayer::runConstructor));
		mat->layers.createArray();
	}
	else
	{
		MessageBox(hMain, "Sorry, no more than 16 layers.", "Sorry...", 0);
		return false;
	}

	fillLayersList();
	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST));
	CHECK(emlv);
	emlv->selected = mat->layers.objCount-1;
	emlv->gotoEnd();
	InvalidateRect(emlv->hwnd, NULL, false);
	layerToGUI(emlv->selected);
	
	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::layerDel()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (mat->layers.objCount <= 1)
	{
		MessageBox(hMain, "Sorry, not less than 1 layer.", "Sorry...", 0);
		return false;
	}

	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST));
	CHECK(emlv);
	int toDel = emlv->selected;

	for (int i=toDel; i<mat->layers.objCount-1; i++)
	{
		mat->layers[i] = mat->layers[i+1];
	}

	mat->layers.objCount--;
	mat->layers.available++;
	mat->layers.createArray();

	if (emlv->selected >= mat->layers.objCount)
		emlv->selected = mat->layers.objCount-1;
	fillLayersList();
	layerToGUI(emlv->selected);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::layerClone()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (mat->layers.objCount < 16)
	{
		MatLayer newmat;
		mat->layers.add(newmat, &(MatLayer::runConstructor));
		mat->layers.createArray();
	}
	else
	{
		MessageBox(hMain, "Sorry, no more than 16 layers.", "Sorry...", 0);
		return false;
	}

	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST));
	CHECK(emlv);
	int toClone = emlv->selected;

	for (int i=mat->layers.objCount-1; i>toClone; i--)
	{
		mat->layers[i] = mat->layers[i-1];
	}

	// clone textures
	mat->layers[toClone+1].tex_col0 = mat->layers[toClone].tex_col0;
	mat->layers[toClone+1].tex_col90 = mat->layers[toClone].tex_col90;
	mat->layers[toClone+1].tex_rough = mat->layers[toClone].tex_rough;
	mat->layers[toClone+1].tex_weight = mat->layers[toClone].tex_weight;
	mat->layers[toClone+1].tex_light = mat->layers[toClone].tex_light;
	mat->layers[toClone+1].tex_normal = mat->layers[toClone].tex_normal;
	mat->layers[toClone+1].tex_transm = mat->layers[toClone].tex_transm;
	mat->layers[toClone+1].tex_aniso = mat->layers[toClone].tex_aniso;
	mat->layers[toClone+1].tex_aniso_angle = mat->layers[toClone].tex_aniso_angle;

	emlv->selected++;
	fillLayersList();
	layerToGUI(emlv->selected);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::layerCurrentToGUI()
{
	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST));
	int n = emlv->selected;
	return layerToGUI(n);
}

//----------------------------------------------------------------------

bool MatEditor2::guiToLayerCurrent()
{
	EM2ListView * emlv = GetEM2ListViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_LAYERS_LIST));
	int n = emlv->selected;
	return guiToLayer(n);
}

//----------------------------------------------------------------------

bool MatEditor2::layerToGUI(int num)
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (num >= mat->layers.objCount  ||  num < 0)
		return false;

	MatLayer * mlay = &(mat->layers[num]);
	CHECK(mlay);

	// ---- GEOMETRY ----

	EM2CheckBox * emcTxOnNormBump = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_NORMALBUMP));
	CHECK(emcTxOnNormBump);
	emcTxOnNormBump->selected = mlay->use_tex_normal;
	InvalidateRect(emcTxOnNormBump->hwnd, NULL, false);

	EM2Button * embTxNormBump = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_NORMALBUMP));
	CHECK(embTxNormBump);
	embTxNormBump->changeCaption(mlay->tex_normal.filename ? mlay->tex_normal.filename : "NO MAP LOADED");

	// ---- EMISSION ----

	EM2CheckBox * emcEmissionON = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISSION));
	CHECK(emcEmissionON);
	emcEmissionON->selected = (mlay->type==MatLayer::TYPE_EMITTER);
	InvalidateRect(emcEmissionON->hwnd, NULL, false);

	EM2CheckBox * emcEmissInvisible = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_INVISIBLE));
	CHECK(emcEmissInvisible);
	emcEmissInvisible->selected = mlay->emissInvisible;
	InvalidateRect(emcEmissInvisible->hwnd, NULL, false);

	EM2ColorShow * emcsEmissionColor = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_COLOR));
	CHECK(emcsEmissionColor);
	emcsEmissionColor->redraw(mlay->color);

	EM2EditSpin * emesEmissPower = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER));
	CHECK(emesEmissPower);
	emesEmissPower->setFloatValue(mlay->power);

	EM2ComboBox * emcEUnit = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_UNITS));
	CHECK(emcEUnit);
	emcEUnit->selected = mlay->unit-1;
	InvalidateRect(emcEUnit->hwnd, NULL, false);

	EM2CheckBox * emcTxOnEmission = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_EMISS));
	CHECK(emcTxOnEmission);
	emcTxOnEmission->selected = mlay->use_tex_light;
	InvalidateRect(emcTxOnEmission->hwnd, NULL, false);

	EM2Button * embTxEmission = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_EMISS));
	CHECK(embTxEmission);
	embTxEmission->changeCaption(mlay->tex_light.filename ? mlay->tex_light.filename : "NO MAP LOADED");

	EM2Button * embIES = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_IES));
	CHECK(embIES);
	char * iesfname = "NO MAP LOADED";
	if (mlay->ies  &&  mlay->ies->filename)
		iesfname = getOnlyFile(mlay->ies->filename);

	embIES->changeCaption(iesfname);

	// ---- SSS ----

	EM2CheckBox * emcSssOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS));
	CHECK(emcSssOn);
	emcSssOn->selected = mlay->sssON;
	InvalidateRect(emcSssOn->hwnd, NULL, false);

	EM2EditSpin * emesSssDens= GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY));
	CHECK(emesSssDens);
	emesSssDens->setFloatValue(mlay->sssDens);

	EM2EditSpin * emesSssDir = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION));
	CHECK(emesSssDir);
	emesSssDir->setFloatValue(mlay->sssCollDir);

	// ---- LAYERS ----

	EM2EditSpin * emesWeight = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT));
	CHECK(emesWeight);
	emesWeight->setIntValue(mlay->contribution);

	EM2CheckBox * emcTxOnWeight= GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_WEIGHT));
	CHECK(emcTxOnWeight);
	emcTxOnWeight->selected = mlay->use_tex_weight;
	InvalidateRect(emcTxOnWeight->hwnd, NULL, false);

	EM2Button * embTxWeight = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_WEIGHT));
	CHECK(embTxWeight);
	embTxWeight->changeCaption(mlay->tex_weight.filename ? mlay->tex_weight.filename : "NO MAP LOADED");

	// ---- REFLECTANCE ----

	EM2EditSpin * emesRough = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ROUGH));
	CHECK(emesRough);
	emesRough->setFloatValue(mlay->roughness*100);

	EM2ColorShow * emcsRefl0 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL0));
	CHECK(emcsRefl0);
	emcsRefl0->redraw(mlay->refl0);

	EM2ColorShow * emcsRefl90 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL90));
	CHECK(emcsRefl90);
	if (mlay->connect90)
		emcsRefl90->redraw(mlay->refl0);
	else
		emcsRefl90->redraw(mlay->refl90);

	EM2CheckBox * emcReflLink = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL_LINK));
	CHECK(emcReflLink);
	emcReflLink->selected = mlay->connect90;
	InvalidateRect(emcReflLink->hwnd, NULL, false);

	EM2EditSpin * emesAnisotropy = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY));
	CHECK(emesAnisotropy);
	emesAnisotropy->setFloatValue(mlay->anisotropy);

	EM2EditSpin * emesAnisoAngle = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE));
	CHECK(emesAnisoAngle);
	emesAnisoAngle->setFloatValue(mlay->aniso_angle);


	EM2CheckBox * emcTxOnRough = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ROUGH));
	CHECK(emcTxOnRough);
	emcTxOnRough->selected = mlay->use_tex_rough;
	InvalidateRect(emcTxOnRough->hwnd, NULL, false);

	EM2Button * embTxRough = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ROUGH));
	CHECK(embTxRough);
	embTxRough->changeCaption(mlay->tex_rough.filename ? mlay->tex_rough.filename : "NO MAP LOADED");


	EM2CheckBox * emcTxOnRefl0 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL0));
	CHECK(emcTxOnRefl0);
	emcTxOnRefl0->selected = mlay->use_tex_col0;
	InvalidateRect(emcTxOnRefl0->hwnd, NULL, false);

	EM2Button * embTxRefl0 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL0));
	CHECK(embTxRefl0);
	embTxRefl0->changeCaption(mlay->tex_col0.filename ? mlay->tex_col0.filename : "NO MAP LOADED");


	EM2CheckBox * emcTxOnRefl90 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL90));
	CHECK(emcTxOnRefl90);
	emcTxOnRefl90->selected = mlay->use_tex_col90;
	InvalidateRect(emcTxOnRefl90->hwnd, NULL, false);

	EM2Button * embTxRefl90 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL90));
	CHECK(embTxRefl90);
	embTxRefl90->changeCaption(mlay->tex_col90.filename ? mlay->tex_col90.filename : "NO MAP LOADED");


	EM2CheckBox * emcTxOnAniso = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO));
	CHECK(emcTxOnAniso);
	emcTxOnAniso->selected = mlay->use_tex_aniso;
	InvalidateRect(emcTxOnAniso->hwnd, NULL, false);

	EM2Button * embTxAniso = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO));
	CHECK(embTxAniso);
	embTxAniso->changeCaption(mlay->tex_aniso.filename ? mlay->tex_aniso.filename : "NO MAP LOADED");

	EM2CheckBox * emcTxOnAnisoAngle = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO_ANGLE));
	CHECK(emcTxOnAnisoAngle);
	emcTxOnAnisoAngle->selected = mlay->use_tex_aniso_angle;
	InvalidateRect(emcTxOnAnisoAngle->hwnd, NULL, false);

	EM2Button * embTxAnisoAngle = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO_ANGLE));
	CHECK(embTxAnisoAngle);
	embTxAnisoAngle->changeCaption(mlay->tex_aniso_angle.filename ? mlay->tex_aniso_angle.filename : "NO MAP LOADED");

	ShowWindow(emcTxOnRefl90->hwnd, mlay->connect90 ? SW_HIDE : SW_SHOW);
	ShowWindow(embTxRefl90->hwnd, mlay->connect90 ? SW_HIDE : SW_SHOW);
	ShowWindow(GetDlgItem(hMain, IDC_MATEDIT2_TEXT_LINKED) , mlay->connect90 ? SW_SHOW : SW_HIDE);


	// ---- TRANSMISSION ----

	EM2CheckBox * emcTransmOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION));
	CHECK(emcTransmOn);
	emcTransmOn->selected = mlay->transmOn;
	InvalidateRect(emcTransmOn->hwnd, NULL, false);

	EM2ColorShow * emcsTransmCol = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION_COLOR));
	CHECK(emcsTransmCol);
	emcsTransmCol->redraw(mlay->transmColor);

	EM2CheckBox * emcAbsOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION));
	CHECK(emcAbsOn);
	emcAbsOn->selected = mlay->absOn;
	InvalidateRect(emcAbsOn->hwnd, NULL, false);

	EM2ColorShow * emcsAbsCol = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_COLOR));
	CHECK(emcsAbsCol);
	emcsAbsCol->redraw(mlay->absColor);

	EM2CheckBox * emcDispOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION));
	CHECK(emcDispOn);
	emcDispOn->selected = mlay->dispersionOn;
	InvalidateRect(emcDispOn->hwnd, NULL, false);

	EM2CheckBox * emcFakeGlass = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_FAKE_GLASS));
	CHECK(emcFakeGlass);
	emcFakeGlass->selected = mlay->fakeGlass;
	InvalidateRect(emcFakeGlass->hwnd, NULL, false);

	EM2CheckBox * emcTxOnTransm = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_TRANSM));
	CHECK(emcTxOnTransm);
	emcTxOnTransm->selected = mlay->use_tex_transm;
	InvalidateRect(emcTxOnTransm->hwnd, NULL, false);

	EM2Button * embTxTransm = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_TRANSM));
	CHECK(embTxTransm);
	embTxTransm->changeCaption(mlay->tex_transm.filename ? mlay->tex_transm.filename : "NO MAP LOADED");

	EM2EditSpin * emesAbsDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH));
	CHECK(emesAbsDepth);
	emesAbsDepth->setFloatValue(mlay->absDist*100.0f);

	EM2EditSpin * emesDispSize = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE));
	CHECK(emesDispSize);
	emesDispSize->setFloatValue(mlay->dispersionValue);

	// ---- FRESNEL ----

	EM2EditSpin * emesIOR = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_IOR));
	CHECK(emesIOR);
	emesIOR->setFloatValue(mlay->fresnel.IOR);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::guiToLayer(int num)
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	if (num >= mat->layers.objCount  ||  num < 0)
		return false;

	MatLayer * mlay = &(mat->layers[num]);
	CHECK(mlay);


	// ---- GEOMETRY ----

	EM2CheckBox * emcTxOnNormBump = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_NORMALBUMP));
	CHECK(emcTxOnNormBump);
	mlay->use_tex_normal = emcTxOnNormBump->selected;

	// ---- EMISSION ----

	EM2CheckBox * emcEmissionON = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISSION));
	CHECK(emcEmissionON);
	mlay->type = emcEmissionON->selected ? MatLayer::TYPE_EMITTER : MatLayer::TYPE_SHADE;

	EM2CheckBox * emcEmissInvisible = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_INVISIBLE));
	CHECK(emcEmissInvisible);
	mlay->emissInvisible = emcEmissInvisible->selected;

	EM2ColorShow * emcsEmissionColor = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_COLOR));
	CHECK(emcsEmissionColor);
	mlay->color = emcsEmissionColor->color;

	EM2EditSpin * emesEmissPower = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_POWER));
	CHECK(emesEmissPower);
	mlay->power = emesEmissPower->floatValue;

	EM2ComboBox * emcEUnit = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_UNITS));
	CHECK(emcEUnit);
	mlay->unit = emcEUnit->selected+1;

	EM2CheckBox * emcTxOnEmission = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_EMISS));
	CHECK(emcTxOnEmission);
	mlay->use_tex_light = emcTxOnEmission->selected;

	// ---- SSS ----

	EM2CheckBox * emcSssOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS));
	CHECK(emcSssOn);
	mlay->sssON = emcSssOn->selected;

	EM2EditSpin * emesSssDens= GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DENSITY));
	CHECK(emesSssDens);
	mlay->sssDens = emesSssDens->floatValue;

	EM2EditSpin * emesSssDir = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_SSS_DIRECTION));
	CHECK(emesSssDir);
	mlay->sssCollDir = emesSssDir->floatValue;


	// ---- LAYERS ----

	EM2EditSpin * emesWeight = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_WEIGHT));
	CHECK(emesWeight);
	mlay->contribution = emesWeight->intValue;

	EM2CheckBox * emcTxOnWeight= GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_WEIGHT));
	CHECK(emcTxOnWeight);
	mlay->use_tex_weight = emcTxOnWeight->selected;


	// ---- REFLECTANCE ----

	EM2EditSpin * emesRough = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ROUGH));
	CHECK(emesRough);
	mlay->roughness = emesRough->floatValue * 0.01f;

	EM2CheckBox * emcReflLink = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL_LINK));
	CHECK(emcReflLink);
	mlay->connect90 = emcReflLink->selected;

	EM2ColorShow * emcsRefl0 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL0));
	CHECK(emcsRefl0);
	mlay->refl0 = emcsRefl0->color;

	if (!mlay->connect90)
	{
		EM2ColorShow * emcsRefl90 = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_REFL90));
		CHECK(emcsRefl90);
		mlay->refl90 = emcsRefl90->color;
	}

	EM2EditSpin * emesAnisotropy = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISOTROPY));
	CHECK(emesAnisotropy);
	mlay->anisotropy = emesAnisotropy->floatValue;

	EM2EditSpin * emesAnisoAngle = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ANISO_ANGLE));
	CHECK(emesAnisoAngle);
	mlay->aniso_angle = emesAnisoAngle->floatValue;


	EM2CheckBox * emcTxOnRough = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ROUGH));
	CHECK(emcTxOnRough);
	mlay->use_tex_rough = emcTxOnRough->selected;

	EM2CheckBox * emcTxOnRefl0 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL0));
	CHECK(emcTxOnRefl0);
	mlay->use_tex_col0 = emcTxOnRefl0->selected;

	EM2CheckBox * emcTxOnRefl90 = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_REFL90));
	CHECK(emcTxOnRefl90);
	mlay->use_tex_col90 = emcTxOnRefl90->selected;

	EM2CheckBox * emcTxOnAniso = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO));
	CHECK(emcTxOnAniso);
	mlay->use_tex_aniso = emcTxOnAniso->selected;

	EM2CheckBox * emcTxOnAnisoAngle = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_ANISO_ANGLE));
	CHECK(emcTxOnAnisoAngle);
	mlay->use_tex_aniso_angle = emcTxOnAnisoAngle->selected;


	// ---- TRANSMISSION ----

	EM2CheckBox * emcTransmOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION));
	CHECK(emcTransmOn);
	mlay->transmOn = emcTransmOn->selected;

	EM2ColorShow * emcsTransmCol = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_TRANSMISSION_COLOR));
	CHECK(emcsTransmCol);
	mlay->transmColor = emcsTransmCol->color;

	EM2CheckBox * emcAbsOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION));
	CHECK(emcAbsOn);
	mlay->absOn = emcAbsOn->selected;

	EM2ColorShow * emcsAbsCol = GetEM2ColorShowInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_COLOR));
	CHECK(emcsAbsCol);
	mlay->absColor = emcsAbsCol->color;

	EM2CheckBox * emcDispOn = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION));
	CHECK(emcDispOn);
	mlay->dispersionOn = emcDispOn->selected;

	EM2CheckBox * emcFakeGlass = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_FAKE_GLASS));
	CHECK(emcFakeGlass);
	mlay->fakeGlass = emcFakeGlass->selected;

	EM2CheckBox * emcTxOnTransm = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_TRANSM));
	CHECK(emcTxOnTransm);
	mlay->use_tex_transm = emcTxOnTransm->selected;

	EM2EditSpin * emesAbsDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_ABSORPTION_DEPTH));
	CHECK(emesAbsDepth);
	mlay->absDist = emesAbsDepth->floatValue/100.0f;

	EM2EditSpin * emesDispSize = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPERSION_SIZE));
	CHECK(emesDispSize);
	mlay->dispersionValue = emesDispSize->floatValue;

	// ---- FRESNEL ----

	EM2EditSpin * emesIOR = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_IOR));
	CHECK(emesIOR);
	mlay->fresnel.IOR = emesIOR->floatValue;


	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::matToGUI()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);

	// ---- PREVIEW ----

	EM2EditSimple * emesMatName = GetEM2EditSimpleInstance(GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME));
	CHECK(emesMatName);
	emesMatName->setText(mat->name);

	EM2CheckBox * emcSkyportal = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SKYPORTAL));
	CHECK(emcSkyportal);
	emcSkyportal->selected = mat->isSkyPortal;
	InvalidateRect(emcSkyportal->hwnd, NULL, false);

	EM2CheckBox * emcMatteShadow = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_MATTE_SHADOW));
	CHECK(emcMatteShadow);
	emcMatteShadow->selected = mat->isMatteShadow;
	InvalidateRect(emcMatteShadow->hwnd, NULL, false);

	EM2ComboBox * emcScene = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREV_SCENE));
	CHECK(emcScene);
	emcScene->selected = mat->prSceneIndex;
	InvalidateRect(emcScene->hwnd, NULL, false);

	// ---- GEOMETRY ----

	EM2CheckBox * emcDisplIgnScale = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_IGNORE_SCALE));
	CHECK(emcDisplIgnScale);
	emcDisplIgnScale->selected = mat->displ_ignore_scale;
	InvalidateRect(emcDisplIgnScale->hwnd, NULL, false);

	EM2ComboBox * emcDisplSubdivs = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SUBDIVS));
	CHECK(emcDisplSubdivs);
	emcDisplSubdivs->selected = mat->displ_subdivs_lvl-1;
	InvalidateRect(emcDisplSubdivs->hwnd, NULL, false);

	EM2CheckBox * emcDisplContinuity = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_CONTINUITY));
	CHECK(emcDisplContinuity);
	emcDisplContinuity->selected = mat->displ_continuity;
	InvalidateRect(emcDisplContinuity->hwnd, NULL, false);

	EM2EditSpin * emesDisplDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH));
	CHECK(emesDisplDepth);
	emesDisplDepth->setFloatValue(mat->displ_depth);

	EM2EditSpin * emesDisplShift = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT));
	CHECK(emesDisplShift);
	emesDisplShift->setFloatValue(mat->displ_shift);

	EM2ComboBox * emcDisplNormals = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_NORMALS));
	CHECK(emcDisplNormals);
	emcDisplNormals->selected = mat->displ_normal_mode;
	InvalidateRect(emcDisplNormals->hwnd, NULL, false);

	EM2CheckBox * emcTxOnDispl = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_DISPLACEMENT));
	CHECK(emcTxOnDispl);
	emcTxOnDispl->selected = mat->use_tex_displacement;
	InvalidateRect(emcTxOnDispl->hwnd, NULL, false);

	EM2Button * embTxDispl = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_DISPLACEMENT));
	CHECK(embTxDispl);
	embTxDispl->changeCaption(mat->tex_displacement.filename ? mat->tex_displacement.filename : "NO MAP LOADED");

	// ---- EMISSION ----

	EM2ComboBox * emcELayer = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_LAYER_COMBO));
	CHECK(emcELayer);
	emcELayer->selected = mat->blendIndex;
	InvalidateRect(emcELayer->hwnd, NULL, false);

	// ---- TRANSMISSION ----

	EM2EditSpin * emesOpacity = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_OPACITY));
	CHECK(emesOpacity);
	emesOpacity->setIntValue((int)(mat->opacity*100.0f+0.5f));

	EM2CheckBox * emcTxOnOpacity = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_OPACITY));
	CHECK(emcTxOnOpacity);
	emcTxOnOpacity->selected = mat->use_tex_opacity;
	InvalidateRect(emcTxOnOpacity->hwnd, NULL, false);

	EM2Button * embTxOpacity = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_OPACITY));
	CHECK(embTxOpacity);
	embTxOpacity->changeCaption(mat->tex_opacity.filename ? mat->tex_opacity.filename : "NO MAP LOADED");

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::guiToMat()
{
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);

	// ---- PREVIEW ----

	EM2EditSimple * emesMatName = GetEM2EditSimpleInstance(GetDlgItem(hMain, IDC_MATEDIT2_MATERIAL_NAME));
	CHECK(emesMatName);
	mat->setName(emesMatName->getText());

	EM2CheckBox * emcSkyportal = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_SKYPORTAL));
	CHECK(emcSkyportal);
	mat->isSkyPortal = emcSkyportal->selected;

	EM2CheckBox * emcMatteShadow = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_MATTE_SHADOW));
	CHECK(emcMatteShadow);
	mat->isMatteShadow = emcMatteShadow->selected;

	// ---- GEOMETRY ----

	EM2CheckBox * emcDisplIgnScale = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_IGNORE_SCALE));
	CHECK(emcDisplIgnScale);
	mat->displ_ignore_scale = emcDisplIgnScale->selected;

	EM2ComboBox * emcDisplSubdivs = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SUBDIVS));
	CHECK(emcDisplSubdivs);
	mat->displ_subdivs_lvl = emcDisplSubdivs->selected+1;

	EM2CheckBox * emcDisplContinuity = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_CONTINUITY));
	CHECK(emcDisplContinuity);
	mat->displ_continuity = emcDisplContinuity->selected;

	EM2EditSpin * emesDisplDepth = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_DEPTH));
	CHECK(emesDisplDepth);
	mat->displ_depth = emesDisplDepth->floatValue;

	EM2EditSpin * emesDisplShift = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_SHIFT));
	CHECK(emesDisplShift);
	mat->displ_shift = emesDisplShift->floatValue;

	EM2ComboBox * emcDisplNormals = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_DISPL_NORMALS));
	CHECK(emcDisplNormals);
	mat->displ_normal_mode = emcDisplNormals->selected;

	EM2CheckBox * emcTxOnDispl = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_DISPLACEMENT));
	CHECK(emcTxOnDispl);
	mat->use_tex_displacement = emcTxOnDispl->selected;

	// ---- EMISSION ----

	EM2ComboBox * emcELayer = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_EMISS_LAYER_COMBO));
	CHECK(emcELayer);
	mat->blendIndex = emcELayer->selected;

	// ---- TRANSMISSION ----

	EM2EditSpin * emesOpacity = GetEM2EditSpinInstance(GetDlgItem(hMain, IDC_MATEDIT2_OPACITY));
	CHECK(emesOpacity);
	mat->opacity = (float)emesOpacity->intValue*0.01f;

	EM2CheckBox * emcTxOnOpacity = GetEM2CheckBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_TXON_OPACITY));
	CHECK(emcTxOnOpacity);
	mat->use_tex_opacity = emcTxOnOpacity->selected;

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::postWindowOpen()
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * edMat = rtr->editedMaterial;

	// check material editor scenes indices
	if (rtr->matScenesIndices.objCount < 1)
	{
		MessageBox(hMain, "No test scene for material editor found. May crash.", "", 0);
		return false;
	}

	// find scene index
	int scIndex = 0;
	if (edMat)
	{
		int emInd = edMat->prSceneIndex;
		scIndex = max(0, min(rtr->matScenesIndices.objCount-1, emInd));
	}

	// change scene
	changeScene(scIndex);

	// fill scenes combo
	EM2ComboBox * emscenes = GetEM2ComboBoxInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREV_SCENE));
	emscenes->selected = scIndex;
	InvalidateRect(emscenes->hwnd, NULL, false);

	// set material stuff on gui
	matToGUI();
	fillLayersList();
	layerToGUI(0);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::updateMaterialToEditableMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	
	MaterialNox * oldmat = NULL;
	if (rtr->editedMaterial)
		oldmat = rtr->editedMaterial;

	MaterialNox * mat = getEditedMaterial()->copy();
	mat->updateSceneTexturePointers(rtr->curScenePtr);
	CHECK(mat);
	rtr->editedMaterial = mat;
	if (oldmat)
	{
		oldmat->deleteBuffers(true, true, true);
		oldmat->layers.destroyArray();
		oldmat->layers.freeList();
		delete oldmat;
		oldmat = NULL;
	}

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::changeScene(int id)
{

	Raytracer * rtr = Raytracer::getInstance();
	if (id < 0 || id>=rtr->matScenesIndices.objCount)
		return false;

	rtr->curSceneInd = rtr->matScenesIndices[id];
	rtr->curScenePtr = rtr->scenes[rtr->curSceneInd];

	if (rtr->editedMaterial)
	{
		int m = rtr->curScenePtr->sscene.editableMaterial;
		MaterialNox * oldmat = rtr->curScenePtr->mats[m];
		MaterialNox * newmat = rtr->editedMaterial->copy();
		rtr->curScenePtr->mats.setElement(m, newmat);
		rtr->curScenePtr->mats.createArray();
		newmat->id = oldmat->id;
		newmat->layers.createArray();
		
		newmat->tex_opacity.managerID = rtr->curScenePtr->texManager.addTexture(newmat->tex_opacity.fullfilename, false, newmat->tex_opacity.texScPtr, false);
		newmat->tex_displacement.managerID = rtr->curScenePtr->texManager.addTexture(newmat->tex_displacement.fullfilename, false, newmat->tex_displacement.texScPtr, false);
		for (int i=0; i<newmat->layers.objCount; i++)
		{
			MatLayer * mlay = &(newmat->layers[i]);
			mlay->tex_weight.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_weight.fullfilename,		false, mlay->tex_weight.texScPtr,		false);
			mlay->tex_col0.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_col0.fullfilename,			false, mlay->tex_col0.texScPtr,			false);
			mlay->tex_col90.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_col90.fullfilename,			false, mlay->tex_col90.texScPtr,		false);
			mlay->tex_light.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_light.fullfilename,			false, mlay->tex_light.texScPtr,		false);
			mlay->tex_normal.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_normal.fullfilename,		false, mlay->tex_normal.texScPtr,		true);
			mlay->tex_transm.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_transm.fullfilename,		false, mlay->tex_transm.texScPtr,		false);
			mlay->tex_rough.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_rough.fullfilename,			false, mlay->tex_rough.texScPtr,		false);
			mlay->tex_aniso.managerID			= rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso.fullfilename,			false, mlay->tex_aniso.texScPtr,		false);
			mlay->tex_aniso_angle.managerID		= rtr->curScenePtr->texManager.addTexture(mlay->tex_aniso_angle.fullfilename,	false, mlay->tex_aniso_angle.texScPtr,	false);
		}
		rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);
		newmat->updateSceneTexturePointers(rtr->curScenePtr);

		if (oldmat)
		{
			oldmat->deleteBuffers(true, true, true, true);
			oldmat->layers.destroyArray();
			oldmat->layers.freeList();
			delete oldmat;
			oldmat = NULL;
		}
	}

	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREVIEW));
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	if (cam && empv)
		empv->imgMod->copyDataFromAnother(&(cam->iMod));

	empv->rgnPtr = &(cam->staticRegion);
	cam->staticRegion.createRegions(cam->width, cam->height);
	cam->staticRegion.overallHits = 0;

	rtr->curScenePtr->createLightMeshes();

	return true;
}

//----------------------------------------------------------------------

int MatEditor2::whichSceneIsBestForMat(MaterialNox * mat)
{
	return 0;
}

//----------------------------------------------------------------------

bool MatEditor2::copyBufferWindowToMaterial()
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREVIEW));
	CHECK(empv);
	CHECK(empv->byteBuff);

	if (!mat->preview)
		mat->preview = new ImageByteBuffer();

	bool ok = mat->preview->copyImageFrom(empv->byteBuff);
	if (!ok)
		return false;

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::copyBufferMaterialToWindow()
{
	Raytracer * rtr = Raytracer::getInstance();
	MaterialNox * mat = getEditedMaterial();
	CHECK(mat);
	CHECK(mat->preview);
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREVIEW));
	CHECK(empv);

	if (!empv->byteBuff)
		empv->byteBuff = new ImageByteBuffer();

	bool ok = empv->byteBuff->copyImageFrom(mat->preview);
	if (!ok)
		return false;

	InvalidateRect(empv->hwnd, NULL, false);

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::startRendering()
{
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREVIEW));
	CHECK(empv);
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->blendBits = rtr->curScenePtr->evalBlendBits();
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	CHECK(cam);
	cam->width = 192;
	cam->height = 192;
	bool ok = cam->initializeCamera(true);
	if (!ok)
		return false;
	empv->otherSource = cam->imgBuff;

	cam->staticRegion.createRegions(cam->width, cam->height);
	cam->staticRegion.overallHits = 0;
	empv->rgnPtr = &(cam->staticRegion);

	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	int prn = rtr->curScenePtr->thrM->getProcessorsNumber();
	prn = max(1, prn);
	#ifdef DEBUG_ONE_THREAD
		prn = 1;
	#endif

	for (int i=0; i<rtr->curScenePtr->mats.objCount; i++)
	{
		MaterialNox * m = rtr->curScenePtr->mats[i];
		if (m)
			m->prepareToRender();
	}

	cam->nowRendering = true;

	rtr->curScenePtr->thrM->initThreads(prn, true);
	rtr->curScenePtr->thrM->runThreads();
	rtr->curScenePtr->nowRendering = true;

	startRefreshThread(0.25f);
	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::stopRendering()
{
	Raytracer * rtr = Raytracer::getInstance();
	rtr->curScenePtr->thrM->stopThreads();
	rtr->curScenePtr->thrM->resumeThreads();
	rtr->curScenePtr->thrM->disposeThreads();
	rtr->curScenePtr->nowRendering = false;
	Camera * cam = rtr->curScenePtr->getActiveCamera();
	cam->nowRendering = false;
	stopRefreshThread();
	return true;
}

//----------------------------------------------------------------------

bool updateRenderedImage(EMPView * empv, bool repaint, bool final, RedrawRegion * rgn, bool smartGI);

bool MatEditor2::refreshPreview()
{
	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREVIEW));
	updateRenderedImage(empv, true, false, NULL, false);
	InvalidateRect(empv->hwnd, NULL, false);
	return true;
}

//----------------------------------------------------------------------

void MatEditor2::startRefreshThread(float rtime)
{
	refreshThreadRunning = true;
	static float rt = rtime;
	DWORD threadID;
	HANDLE hRefreshThread = CreateThread( 
			   NULL,
			   0,
			   (LPTHREAD_START_ROUTINE)(RefreshThreadProc),
			   (LPVOID)&rt,
			   0,
			   &threadID);
}

//----------------------------------------------------------------------

void MatEditor2::stopRefreshThread()
{
	refreshThreadRunning = false;
}

//----------------------------------------------------------------------

DWORD WINAPI MatEditor2::RefreshThreadProc(LPVOID lpParameter)
{
	MatEditor2 * matedit = MatEditor2::getInstance();
	CHECK(matedit);

	float s = *((float*)lpParameter);

	clock_t lTime = clock();
	clock_t cTime;
	double diff;
	HWND hPr = GetDlgItem(matedit->hMain, IDC_MATEDIT2_PREVIEW);
	EMPView * empv = GetEMPViewInstance(hPr);

	while (matedit->refreshThreadRunning)
	{
		Sleep(50);

		cTime = clock();
		diff  = (cTime - lTime)/(double)CLOCKS_PER_SEC;
		if (diff > s)
		{
			matedit->refreshPreview();
			lTime = clock();
		}
	}

	return 0;
}

//----------------------------------------------------------------------

DWORD WINAPI MatEditor2::TextureWatcherThreadProc(LPVOID lpParameter)
{
	MatEditor2 * matedit = MatEditor2::getInstance();
	Raytracer * rtr = Raytracer::getInstance();
	CHECK(matedit);

	while (1)
	{
		Sleep(1000);
		if (!matedit->isMatEditOpened)
			return 0;
		if (rtr->curScenePtr->nowRendering)
			continue;

		rtr->curScenePtr->texManager.checkReloadModifiedTextures(matedit->getHWND(), true);
	}

	return 0;
}

//----------------------------------------------------------------------
 
void MatEditor2::startTextureWatcherThread()
{
	DWORD threadID;
	HANDLE hThread = CreateThread( NULL,  0,
			   (LPTHREAD_START_ROUTINE)(TextureWatcherThreadProc),
			   (LPVOID)0, 0, &threadID);
}

//----------------------------------------------------------------------

bool MatEditor2::runTextureDialog(TextureInstance * tex, bool normalsmode)
{
	CHECK(tex);

	Raytracer * rtr = Raytracer::getInstance();

	TextureInstance * t = tex;
	Texture * orig = NULL;
	if (t->managerID>=0)
		orig = rtr->curScenePtr->texManager.textures[t->managerID];
	Texture * tcopy = new Texture();
	tcopy->copyFromOther(orig);
	tcopy->texMod = t->texMod;
	tcopy->normMod = t->normMod;

	Texture * texres = NULL;
	if (rtr->runDialogsUnicode)
	{
		if (normalsmode)
			texres = (Texture *)DialogBoxParamW(hDllModule, 
								MAKEINTRESOURCEW(IDD_NORMALMAP2_DIALOG), hMain, Normalmap2DlgProc,(LPARAM)( tcopy ));
		else
			texres = (Texture *)DialogBoxParamW(hDllModule, 
								MAKEINTRESOURCEW(IDD_TEXTURE2_DIALOG), hMain, Tex2DlgProc,(LPARAM)( tcopy ));
	}
	else
	{
		if (normalsmode)
			texres = (Texture *)DialogBoxParam(hDllModule, 
								MAKEINTRESOURCE(IDD_NORMALMAP2_DIALOG), hMain, Normalmap2DlgProc,(LPARAM)( tcopy ));
		else
			texres = (Texture *)DialogBoxParam(hDllModule, 
								MAKEINTRESOURCE(IDD_TEXTURE2_DIALOG), hMain, Tex2DlgProc,(LPARAM)( tcopy ));
	}

	bool ok = false;
	if (texres)
	{
		ok = true;
		if (t->filename)
			free(t->filename);
		if (t->fullfilename)
			free(t->fullfilename);
		t->filename = copyString(texres->filename);
		t->fullfilename = copyString(texres->fullfilename);
		
		rtr->curScenePtr->texManager.updateTextures(rtr->curScenePtr);

		int id = rtr->curScenePtr->texManager.addTexture(texres->fullfilename, false, texres, normalsmode);
		t->managerID = id;

		t->texMod = texres->texMod;
		t->normMod = texres->normMod;
		texres->freeAllBuffers();
		delete texres;
	}

	return ok;
}

//----------------------------------------------------------------------

bool MatEditor2::updateTexButtons()
{
	MaterialNox * mat = getEditedMaterial();
	MatLayer * mlay = getEditedMatLayer();
	CHECK(mat);
	CHECK(mlay);

	EM2Button * tbDispl = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_DISPLACEMENT));
	if (mat->tex_displacement.filename)
		tbDispl->changeCaption(mat->tex_displacement.filename);
	else
		tbDispl->changeCaption("NO MAP LOADED");

	EM2Button * tbNormal = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_NORMALBUMP));
	if (mlay->tex_normal.filename)
		tbNormal->changeCaption(mlay->tex_normal.filename);
	else
		tbNormal->changeCaption("NO MAP LOADED");

	EM2Button * tbEmiss = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_EMISS));
	if (mlay->tex_light.filename)
		tbEmiss->changeCaption(mlay->tex_light.filename);
	else
		tbEmiss->changeCaption("NO MAP LOADED");

	EM2Button * tbIES = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_IES));
	if (mlay->ies  &&  mlay->ies->filename)
	{
		char * onlyfilename = getOnlyFile(mlay->ies->filename);
		tbIES->changeCaption(onlyfilename ? onlyfilename : mlay->ies->filename);
		if (onlyfilename)
			free(onlyfilename);
	}
	else
		tbIES->changeCaption("NO MAP LOADED");

	EM2Button * tbWeight = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_WEIGHT));
	if (mlay->tex_weight.filename)
		tbWeight->changeCaption(mlay->tex_weight.filename);
	else
		tbWeight->changeCaption("NO MAP LOADED");

	EM2Button * tbRough = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ROUGH));
	if (mlay->tex_rough.filename)
		tbRough->changeCaption(mlay->tex_rough.filename);
	else
		tbRough->changeCaption("NO MAP LOADED");

	EM2Button * tbRefl0 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL0));
	if (mlay->tex_col0.filename)
		tbRefl0->changeCaption(mlay->tex_col0.filename);
	else
		tbRefl0->changeCaption("NO MAP LOADED");

	EM2Button * tbRefl90 = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_REFL90));
	if (mlay->tex_col90.filename)
		tbRefl90->changeCaption(mlay->tex_col90.filename);
	else
		tbRefl90->changeCaption("NO MAP LOADED");

	EM2Button * tbAniso = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO));
	if (mlay->tex_aniso.filename)
		tbAniso->changeCaption(mlay->tex_aniso.filename);
	else
		tbAniso->changeCaption("NO MAP LOADED");

	EM2Button * tbAnisoAngle = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_ANISO_ANGLE));
	if (mlay->tex_aniso_angle.filename)
		tbAnisoAngle->changeCaption(mlay->tex_aniso_angle.filename);
	else
		tbAnisoAngle->changeCaption("NO MAP LOADED");

	EM2Button * tbTransm = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_TRANSM));
	if (mlay->tex_transm.filename)
		tbTransm->changeCaption(mlay->tex_transm.filename);
	else
		tbTransm->changeCaption("NO MAP LOADED");



	EM2Button * tbOpac = GetEM2ButtonInstance(GetDlgItem(hMain, IDC_MATEDIT2_TX_OPACITY));
	if (mat->tex_opacity.filename)
		tbOpac->changeCaption(mat->tex_opacity.filename);
	else
		tbOpac->changeCaption("NO MAP LOADED");


	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::updateDisplacement()
{
	HWND hScene = GetDlgItem(hMain, IDC_MATEDIT2_PREV_SCENE);
	EM2ComboBox * emscene = GetEM2ComboBoxInstance(hScene);
	CHECK(emscene);
	if (emscene->selected!=4)
	{
		MaterialNox * mat = getEditedMaterial();
		if (mat->displ_depth!=0  &&  mat->tex_displacement.isValid()  &&  mat->use_tex_displacement)
		{
			MessageBox(hMain, "Displacement materials can have preview only on displacement scene.", "Error", 0);
			return false;
		}
		return true;
	}

	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	sc->removeDisplacementMeshes();
	sc->createDisplacementMeshes();

	sc->sscene.bvh.deleteAllStuff();
	for (int i=0; i<sc->meshes.objCount; i++)
		sc->sscene.bvh.addMesh(&sc->meshes[i]);
	bool bvhok = sc->sscene.bvh.createTree();

	sc->updateSkyportals();
	sc->createLightMeshes();

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::loadMaterial(char * filename)
{
	CHECK(filename);

	int error;
	XMLScene xscene;
	MaterialNox * mat = xscene.loadMaterial(filename, error);//, img);

	if (!mat)
	{
		if (error>0)
		{
			char buf[128];
			sprintf_s(buf, 128, "Not loaded\nWatch line %d", error);
			MessageBox(hMain, buf, "Error", 0);
		}
		else
			MessageBox(hMain, "Not loaded", "Error", 0);
		return false;
	}

	Raytracer * rtr = Raytracer::getInstance();

	if (!mat->preview)
	{
		mat->preview = new ImageByteBuffer();
		mat->preview->allocBuffer(RES_PR_X, RES_PR_Y);
		mat->preview->clearBuffer();
	}

	int n = rtr->curScenePtr->sscene.editableMaterial;
	if (n < 0   ||   n >= rtr->curScenePtr->mats.objCount)
	{
		MessageBox(0, "No material marked for edit.\nScene probably broken.", "", 0);
		delete mat->preview;
		mat->deleteBuffers(true, true, true);
		delete mat;
		return false;
	}

	MaterialNox * oldmat = rtr->curScenePtr->mats[n];
	mat->id = oldmat->id;
	mat->hasEmitters  = false;

	rtr->curScenePtr->mats[n] = mat;

	myNode<MaterialNox *> * mm = rtr->curScenePtr->mats.first;
	for (int i=0; i<n; i++)
		mm = mm->next;

	mm->obj = mat;
	rtr->curScenePtr->mats.createArray();

	matToGUI();
	fillLayersList();
	layerToGUI(0);
	updateCurrentLayerPropsOnList();


	EMPView * empv = GetEMPViewInstance(GetDlgItem(hMain, IDC_MATEDIT2_PREVIEW));
	if (empv->byteBuff == oldmat->preview)
		empv->byteBuff = NULL;
	oldmat->deleteBuffers(true, true, true);
	delete oldmat;
	
	copyBufferMaterialToWindow();

	return true;
}

//----------------------------------------------------------------------

bool MatEditor2::loadMaterial()
{
	char * filename;
	filename = openFileDialog(hMain, "NXM material (*.nxm)\0*.nxm\0", "Load material", "nxm", 1);
	if (!filename)
		return false;
	return loadMaterial(filename);
}

//----------------------------------------------------------------------

bool MatEditor2::saveMaterial()
{
	char * filename;
	filename = saveFileDialog(hMain, "NXM material (*.nxm)\0*.nxm\0", "Save material", "nxm", 1);
	if (!filename)
		return false;

	copyBufferWindowToMaterial();

	XMLScene xscene;
	Raytracer * rtr = Raytracer::getInstance();
	bool saved = xscene.saveMaterial(filename, rtr->curScenePtr->mats[0]);
	
	if (saved)
		MessageBox(hMain, "Material Saved", "", 0);
	else
		MessageBox(hMain, "Error!\nMaterial not saved", "Error", 0);

	return saved;
}

//----------------------------------------------------------------------

bool MatEditor2::updateControlLocks(bool nowrendering)
{
	bool skyportal = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_SKYPORTAL))->selected;
	if (nowrendering  ||  skyportal)
	{
		// PREVIEW GROUP
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_MATERIAL_NAME);
		setControlEnabled(!nowrendering, getHWND(), IDC_MATEDIT2_SKYPORTAL);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_PREV_SCENE);
		setControlEnabled(true , getHWND(), IDC_MATEDIT2_PREV_RENDER);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_LIBRARY_OPEN);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_PREVIEW);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_NAME);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_LOAD);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_SAVE);

		// GEOMETRY GROUP
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_DISPLACEMENT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_DISPLACEMENT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPL_IGNORE_SCALE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPL_DEPTH);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPL_SHIFT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPL_SUBDIVS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPL_NORMALS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPL_CONTINUITY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_NORMALBUMP);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_NORMALBUMP);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DISPLACEMENT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DEPTH);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_SHIFT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_NORMALS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_SUBDIVISIONS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_NORMAL_BUMP_MAP);

		
		// EMISSION GROUP
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISSION);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_COLOR);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_TEMPERATURE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_EMISS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_EMISS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_POWER);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_UNITS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_LAYER_COMBO);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_IES);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_POWER);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_EMITTER_LAYER);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_IES);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_INVISIBLE);

		// SSS GROUP
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_SSS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_SSS_DENSITY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_SSS_DIRECTION);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DENSITY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DIRECTION);

		// LAYERS GROUP
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_LAYERS_LIST);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_LAYER_ADD);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_LAYER_REMOVE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_LAYER_CLONE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_LAYER_RESET);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_WEIGHT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_WEIGHT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_WEIGHT);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_WEIGHT);

		// REFLECTANCE GROUP
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_ROUGH);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_ROUGH);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_ROUGH);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_REFL0);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_REFL90);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_REFL_LINK);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_REFL0);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_REFL90);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_REFL0);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_REFL90);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_ANISOTROPY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_ANISO_ANGLE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_ANISO);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_ANISO_ANGLE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_ANISO);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_ANISO_ANGLE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_ROUGH);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_REFL0);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_REFL90);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_LINKED);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_ANISOTROPY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_ANISOANGLE);


		// TRANSMISSION GROUP
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TRANSMISSION);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TRANSMISSION_COLOR);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_TRANSM);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_TRANSM);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_ABSORPTION);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_ABSORPTION_COLOR);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_ABSORPTION_DEPTH);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPERSION);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPERSION_SIZE);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_FAKE_GLASS);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_OPACITY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_OPACITY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_OPACITY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_IOR);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DEPTH2);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_OPACITY);
		setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_IOR);

		setControlEnabled(true , getHWND(), IDC_MATEDIT2_OK);
		setControlEnabled(true , getHWND(), IDC_MATEDIT2_CANCEL);

		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_PREVIEW), false);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_GEOMETRY), false);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_EMISSION), false);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_SSS), false);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_MAT_LAYERS), false);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_REFLECTANCE), false);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_TRANSMISSION), false);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_FRESNEL), false);

	}
	else			// not rendering
	{
		bool emission = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_EMISSION))->selected;
		if (emission)
		{
			// PREVIEW GROUP
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_MATERIAL_NAME);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_SKYPORTAL);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_PREV_SCENE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_PREV_RENDER);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LIBRARY_OPEN);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_PREVIEW);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_NAME);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LOAD);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_SAVE);
						  
			// GEOMETRY GROUP 
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_DISPLACEMENT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_DISPLACEMENT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_IGNORE_SCALE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_DEPTH);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_SHIFT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_SUBDIVS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_NORMALS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_CONTINUITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_NORMALBUMP);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_NORMALBUMP);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_DISPLACEMENT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_DEPTH);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_SHIFT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_NORMALS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_SUBDIVISIONS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_NORMAL_BUMP_MAP);
						  
			// EMISSION GROUP 
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISSION);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISS_COLOR);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISS_TEMPERATURE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_EMISS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_EMISS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISS_POWER);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISS_UNITS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISS_LAYER_COMBO);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_IES);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_POWER);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_EMITTER_LAYER);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_IES);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISS_INVISIBLE);
						  
			// SSS GROUP	  
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_SSS);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_SSS_DENSITY);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_SSS_DIRECTION);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DENSITY);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DIRECTION);
						  
			// LAYERS GROUP	  
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYERS_LIST);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_ADD);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_REMOVE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_CLONE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_RESET);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_WEIGHT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_WEIGHT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_WEIGHT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_WEIGHT);
						  
			// REFLECTANCE GROUP
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_ROUGH);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_ROUGH);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_ROUGH);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_REFL0);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_REFL90);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_REFL_LINK);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_REFL0);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_REFL90);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_REFL0);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_REFL90);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_ANISOTROPY);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_ANISO_ANGLE);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_ANISO);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_ANISO);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_ANISO);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_ANISO_ANGLE);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_ROUGH);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_REFL0);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_REFL90);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_LINKED);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_ANISOTROPY);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_ANISOANGLE);
						  
			// TRANSMISSION GROUP
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TRANSMISSION);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TRANSMISSION_COLOR);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_TRANSM);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_TRANSM);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_ABSORPTION);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_ABSORPTION_COLOR);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_ABSORPTION_DEPTH);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPERSION);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_DISPERSION_SIZE);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_FAKE_GLASS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_OPACITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_OPACITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_OPACITY);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_IOR);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_DEPTH2);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_OPACITY);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_IOR);

			setControlEnabled(true , getHWND(), IDC_MATEDIT2_OK);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_CANCEL);
		}
		else	// no rendering, no emission
		{
			bool sssOn = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_SSS))->selected;
			bool transmOn = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_TRANSMISSION))->selected;
			bool fakeOn = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_FAKE_GLASS))->selected;
			bool absOn = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_ABSORPTION))->selected;
			bool dispOn = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_DISPERSION))->selected;
			bool roughTexOn = GetEM2CheckBoxInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_TXON_ROUGH))->selected;
			float rough = GetEM2EditSpinInstance(GetDlgItem(getHWND(), IDC_MATEDIT2_ROUGH))->floatValue;

			// PREVIEW GROUP
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_MATERIAL_NAME);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_SKYPORTAL);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_PREV_SCENE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_PREV_RENDER);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LIBRARY_OPEN);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_PREVIEW);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_NAME);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LOAD);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_SAVE);
						  
			// GEOMETRY GROUP 
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_DISPLACEMENT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_DISPLACEMENT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_IGNORE_SCALE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_DEPTH);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_SHIFT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_SUBDIVS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_NORMALS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_DISPL_CONTINUITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_NORMALBUMP);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_NORMALBUMP);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_DISPLACEMENT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_DEPTH);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_SHIFT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_NORMALS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_SUBDIVISIONS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_NORMAL_BUMP_MAP);
						  
			// EMISSION GROUP 
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_EMISSION);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_COLOR);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_TEMPERATURE);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TXON_EMISS);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TX_EMISS);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_POWER);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_UNITS);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_LAYER_COMBO);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_IES);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_POWER);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_EMITTER_LAYER);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_TEXT_IES);
			setControlEnabled(false, getHWND(), IDC_MATEDIT2_EMISS_INVISIBLE);
						  
			// SSS GROUP	  
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_SSS);
			setControlEnabled(sssOn , getHWND(), IDC_MATEDIT2_SSS_DENSITY);
			setControlEnabled(sssOn , getHWND(), IDC_MATEDIT2_SSS_DIRECTION);
			setControlEnabled(sssOn, getHWND(), IDC_MATEDIT2_TEXT_DENSITY);
			setControlEnabled(sssOn, getHWND(), IDC_MATEDIT2_TEXT_DIRECTION);
						  
			// LAYERS GROUP	  
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYERS_LIST);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_ADD);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_REMOVE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_CLONE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_LAYER_RESET);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_WEIGHT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_WEIGHT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_WEIGHT);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_WEIGHT);
						  
			// REFLECTANCE GROUP
			setControlEnabled(!sssOn && !(fakeOn && transmOn) , getHWND(), IDC_MATEDIT2_ROUGH);
			setControlEnabled(!sssOn && !(fakeOn && transmOn) , getHWND(), IDC_MATEDIT2_TXON_ROUGH);
			setControlEnabled(!sssOn && !(fakeOn && transmOn) , getHWND(), IDC_MATEDIT2_TX_ROUGH);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_REFL0);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_REFL90);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_REFL_LINK);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_REFL0);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_REFL90);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_REFL0);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_REFL90);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_ANISOTROPY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_ANISO_ANGLE);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_ANISO);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_ANISO);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_ANISO);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_ANISO_ANGLE);

			setControlEnabled(!sssOn && !(fakeOn && transmOn), getHWND(), IDC_MATEDIT2_TEXT_ROUGH);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_REFL0);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_REFL90);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_LINKED);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_ANISOTROPY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_ANISOANGLE);
						  
			// TRANSMISSION GROUP
			setControlEnabled(!sssOn		, getHWND(), IDC_MATEDIT2_TRANSMISSION);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TRANSMISSION_COLOR);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_TRANSM);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_TRANSM);
			setControlEnabled(sssOn || transmOn, getHWND()		, IDC_MATEDIT2_ABSORPTION);
			setControlEnabled((sssOn || transmOn) && absOn		, getHWND(), IDC_MATEDIT2_ABSORPTION_COLOR);
			setControlEnabled((sssOn || transmOn) && absOn		, getHWND(), IDC_MATEDIT2_ABSORPTION_DEPTH);
			setControlEnabled(!sssOn && transmOn && !fakeOn		, getHWND(), IDC_MATEDIT2_DISPERSION);
			setControlEnabled(!sssOn && dispOn && transmOn && !fakeOn		, getHWND(), IDC_MATEDIT2_DISPERSION_SIZE);
			setControlEnabled(!sssOn && transmOn		, getHWND(), IDC_MATEDIT2_FAKE_GLASS);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_OPACITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TXON_OPACITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TX_OPACITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_IOR);
			setControlEnabled((sssOn || transmOn) && absOn, getHWND(), IDC_MATEDIT2_TEXT_DEPTH2);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_OPACITY);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_TEXT_IOR);

			setControlEnabled(true , getHWND(), IDC_MATEDIT2_OK);
			setControlEnabled(true , getHWND(), IDC_MATEDIT2_CANCEL);
		}

		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_PREVIEW), true);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_GEOMETRY), true);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_EMISSION), true);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_SSS), true);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_MAT_LAYERS), true);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_REFLECTANCE), true);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_TRANSMISSION), true);
		GROUPBAR_ACTIVATE(GetDlgItem(getHWND(), IDC_MATEDIT2_GR_FRESNEL), true);
	}

	SetFocus(0);

	return true;
}

//----------------------------------------------------------------------
