#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <gdiplus.h>
#include "EM2Controls.h"

extern HMODULE hDllModule;

char * copyString(char * src, unsigned int addSpace=0);

//#define GUI2_USE_GDIPLUS

//----------------------------------------------------------------------

void InitEM2Tabs()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2Tabs";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2TabsProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EM2Tabs *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2Tabs * GetEM2TabsInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2Tabs * emt = (EM2Tabs *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2Tabs * emt = (EM2Tabs *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emt;
}

//----------------------------------------------------------------------

void SetEM2TabsInstance(HWND hwnd, EM2Tabs *emt)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emt);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emt);
	#endif
}

//----------------------------------------------------------------------

EM2Tabs::EM2Tabs(HWND hWnd)
{
	hwnd = hWnd;

    colText					= NGCOL_LIGHT_GRAY;
    colBackGnd				= NGCOL_TEXT_BACKGROUND;
	colSelText				= NGCOL_YELLOW;
	colDisText				= NGCOL_DARK_GRAY;
	colTextShadow			= RGB(15,15,15);

	last_selected = -1;
	selected = -1;
	mouseover = -1;
	useCursors = true;
	allowNone = false;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfont = true;
	numTabs = 0;
	tabnames = NULL;
	#ifdef GUI2_DRAW_TEXT_SHADOWS
		drawTextShadow = true;
	#else
		drawTextShadow = false;
	#endif

	bgImage = 0;
	bgShiftX = bgShiftY = 0;
}

//----------------------------------------------------------------------

EM2Tabs::~EM2Tabs()
{
	if  (hFont  &&  autodelfont)
		DeleteObject(hFont);
	hFont = 0;
	releaseTabs();
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2TabsProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2Tabs* emt = GetEM2TabsInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			emt = new EM2Tabs(hwnd);
			SetEM2TabsInstance(hwnd, emt);
			break;
		case WM_DESTROY:
			{
				delete emt;
				SetEM2TabsInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
		{
			bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) != 0);

			HDC ohdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);

			if (emt->bgImage)
			{
				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP * curBitmap = &emt->bgImage;
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, *curBitmap);
				BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emt->bgShiftX, emt->bgShiftY, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				SetBkMode(hdc, TRANSPARENT);
			}
			else
			{
				HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emt->colBackGnd));
				HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(emt->colBackGnd));
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, hOldBrush));
				DeleteObject(SelectObject(hdc, hOldPen2));
				SetBkMode(hdc, OPAQUE);
				SetBkColor  (hdc, emt->colBackGnd);
			}

			HFONT hOldFont = (HFONT)SelectObject(hdc, emt->getFont());
			for (int i=0; i<emt->numTabs; i++)
			{
				int py = i*23 + 28;
				bool isSelected = (emt->selected==i);

				char * txt = emt->tabnames[i];
				if (!txt)
					continue;
				int ts = (int)strlen(txt);

				int posx = 2;
				int posy = py;

				if (emt->drawTextShadow)
				{
					SetTextColor(hdc, emt->colTextShadow);
					ExtTextOut(hdc, posx+1, posy, ETO_CLIPPED, &rect, txt, ts, 0);
					ExtTextOut(hdc, posx-1, posy, ETO_CLIPPED, &rect, txt, ts, 0);
					ExtTextOut(hdc, posx, posy+1, ETO_CLIPPED, &rect, txt, ts, 0);
					ExtTextOut(hdc, posx, posy-1, ETO_CLIPPED, &rect, txt, ts, 0);
				}

				if (disabled)
					SetTextColor(hdc, emt->colDisText);
				else
					if (emt->mouseover==i  ||  emt->selected==i)
						SetTextColor(hdc, emt->colSelText);
					else
						SetTextColor(hdc, emt->colText);
				ExtTextOut(hdc, posx, posy, ETO_CLIPPED, &rect, txt, ts, 0);
			}
			SelectObject(hdc, hOldFont);

			int line_start = 2;
			///int line_end = 81;
			int line_end = rect.right - 2;

			#ifdef GUI2_USE_GDIPLUS
			{
				ULONG_PTR gdiplusToken;		// activate gdi+
				Gdiplus::GdiplusStartupInput gdiplusStartupInput;
				Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
				{
					Gdiplus::Graphics graphics(hdc);

					Gdiplus::Pen s1pen(Gdiplus::Color(48, 0xc0,0xc0,0xc0));
					Gdiplus::Pen s2pen(Gdiplus::Color(64, 0x19,0x19,0x19));
					Gdiplus::Pen s3pen(Gdiplus::Color(48, 0x19,0x19,0x19));

					for (int i=0; i<emt->numTabs; i++)
					{
						int py = i*23+1 + 27;
						graphics.DrawLine(&s1pen, line_start, py+18, line_end-1, py+18);
						Gdiplus::Point pts1[] = {
									Gdiplus::Point(line_start,   py+17), 
									Gdiplus::Point(line_end-1,   py+17), 
									Gdiplus::Point(line_end,     py+18), 
									Gdiplus::Point(line_end-1,   py+19), 
									Gdiplus::Point(line_start,   py+19), 
									Gdiplus::Point(line_start-1, py+18), 
									Gdiplus::Point(line_start,   py+17) };
						graphics.DrawLines(&s2pen, pts1, 7);
						Gdiplus::Point pts2[] = {
									Gdiplus::Point(line_start,   py+16), 
									Gdiplus::Point(line_end-1,   py+16), 
									Gdiplus::Point(line_end+1,   py+18), 
									Gdiplus::Point(line_end-1,   py+20), 
									Gdiplus::Point(line_start,   py+20), 
									Gdiplus::Point(line_start-2, py+18), 
									Gdiplus::Point(line_start,   py+16) };
						graphics.DrawLines(&s3pen, pts2, 7);
					}
				}	// gdi+ variables destructors here
				Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
			}
			#else
				HPEN holdpen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(0x41,0x41,0x41)));
				for (int i=0; i<emt->numTabs; i++)
				{
					int py = i*23+1 + 27;
					MoveToEx(hdc, line_start, py+18, NULL);
					LineTo(hdc, line_end, py+18);
				}
				DeleteObject((HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(0x1e,0x1e,0x1e))));
				for (int i=0; i<emt->numTabs; i++)
				{
					int py = i*23+1 + 27;

					MoveToEx(hdc, line_start,   py+17, NULL);
					LineTo(hdc,   line_end-1,   py+17);
					LineTo(hdc,   line_end,     py+18);
					LineTo(hdc,   line_end-1,   py+19);
					LineTo(hdc,   line_start,   py+19);
					LineTo(hdc,   line_start-1, py+18);
					LineTo(hdc,   line_start,   py+17);

					MoveToEx(hdc, line_start,   py+16, NULL);
					LineTo(hdc,   line_end-1,   py+16);
					LineTo(hdc,   line_end+1,   py+18);
					LineTo(hdc,   line_end-1,   py+20);
					LineTo(hdc,   line_start,   py+20);
					LineTo(hdc,   line_start-2, py+18);
					LineTo(hdc,   line_start,   py+16);

				}

				DeleteObject(SelectObject(hdc, holdpen));
			#endif

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
		case WM_MOUSEMOVE:
		{
			if (!GetCapture())
				SetCapture(hwnd);
			POINT p = {(short)LOWORD(lParam), (short)HIWORD(lParam) }; 
			int cl = emt->getUnderMouseTabNumber(p.x, p.y);
			if (cl!=emt->mouseover)
			{
				emt->mouseover = cl;
				InvalidateRect(hwnd, NULL, false);
				if (emt->useCursors)
					if (cl>-1 && cl<emt->numTabs)
						SetCursor(LoadCursor (NULL, IDC_HAND));
					else
						SetCursor(LoadCursor (NULL, IDC_ARROW));
			}
			RECT rect;
			GetClientRect(hwnd , &rect);
			if (!PtInRect(&rect, p))
				ReleaseCapture();

		}
		break;
		case WM_CAPTURECHANGED:
			{
				emt->mouseover = -1;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
		{
			int xPos = (short)LOWORD(lParam);
			int yPos = (short)HIWORD(lParam);
			int cl = emt->getUnderMouseTabNumber(xPos, yPos);
			
			if (emt->selected == cl)
			{
				if (emt->allowNone)
					emt->selected = -1;
			}
			else
			{
				if (cl>-1  ||  emt->allowNone)
				{
					emt->last_selected = emt->selected;
					emt->selected = cl;
				}
			}

			InvalidateRect(hwnd, NULL, false);

			LONG wID;
			wID = GetWindowLong(hwnd, GWL_ID);
			SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, BN_CLICKED), (LPARAM)hwnd);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

int EM2Tabs::getUnderMouseTabNumber(int x, int y)
{
	if (x<0 || x>80)
		return -1;

	if (y%23==0)
		return -1;
	int r = y/23-1;
	if (r>=numTabs)
		return -1;
	return r;
}

//----------------------------------------------------------------------

bool EM2Tabs::setTabsCount(int newcount)
{
	if (newcount<1)
		return false;
	newcount = min(newcount, 64);

	char ** newtabnames = (char **)malloc(sizeof(char *)*newcount);
	if (!newtabnames)
		return false;

	for (int i=0; i<newcount; i++)
		newtabnames[i] = NULL;

	releaseTabs();

	tabnames = newtabnames;
	numTabs = newcount;

	return true;
}

//----------------------------------------------------------------------

bool EM2Tabs::setTabText(int id, char * txt)
{
	if (id>=numTabs || id<0)
		return false;
	if (!tabnames)
		return false;
	if (!txt)
		return false;
	if (strlen(txt)<1)
		return false;
	char * oldname = tabnames[id];
	tabnames[id] = copyString(txt);
	if (oldname)
		free(oldname);
	return true;
}

//----------------------------------------------------------------------

void EM2Tabs::releaseTabs()
{

	if (tabnames)
	{
		for (int i=0; i<numTabs; i++)
		{
			if (tabnames[i])
				free(tabnames[i]);
			tabnames[i] = NULL;
		}
		free(tabnames);
		tabnames = NULL;
	}
	numTabs = 0;
}

//----------------------------------------------------------------------

bool EM2Tabs::setFont(HFONT hNewFont, bool autodel)
{
	if (autodelfont)
		DeleteObject(hFont);
	hFont = hNewFont;
	autodelfont = autodel;
	return true;
}

//----------------------------------------------------------------------

HFONT EM2Tabs::getFont()
{
	return hFont;
}

//----------------------------------------------------------------------
