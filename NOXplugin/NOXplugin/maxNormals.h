#ifndef __MAX_NORMALS_H
#define __MAX_NORMALS_H


class MNormal
{
public:
	Point3 normal;
	DWORD smgrp;
	MNormal * next;
	int number;
	bool empty;

	static int global_number;

	MNormal();
	~MNormal();
	void DisposeAll();
	void AddNormal(Point3 n, DWORD s);
	Point3 GetNormal(DWORD s);
	int GetNormalNumber(DWORD s);
	void AssignNumber();
	void Normalize();
	int CountNormals();
	void JoinNormals();
};

MNormal * ComputeMyNormals(Mesh *mesh, Point3 **list, Matrix3 m1);

#endif

