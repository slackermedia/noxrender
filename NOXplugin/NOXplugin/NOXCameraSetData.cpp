#include "max2013predef.h"
#include "NOXCamera.h"
#include <decomp.h>
#include "ILinkTMCtrl.h"

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetOrtho(BOOL b)
{
	trCam.setOrtho(b==TRUE);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetFOV(TimeValue t, float f)
{
	trCam.setFOV(f);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetTDist(TimeValue t, float f)
{
	tDist = f;
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetManualClip(int onOff)
{
	trCam.setManualClip(onOff);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetClipDist(TimeValue t, int which, float val)
{
	if (which == CAM_HITHER_CLIP)
		trCam.setClipDistHither(val);
	if (which == CAM_YON_CLIP)
		trCam.setClipDistYon(val);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetEnvRange(TimeValue time, int which, float f)
{
	if (which == ENV_NEAR_RANGE)
		trCam.setEnvRangeNear(f);
	if (which == ENV_FAR_RANGE)
		trCam.setEnvRangeFar(f);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetEnvDisplay(BOOL b, int notify)
{
	envDisplay = (b==TRUE);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetConeState(int s)
{
	coneState = s;
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetHorzLineState(int s)
{
	horzLineState = s;
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::Enable(int enab)
{
	enabled = (enab!=0);
}

//-----------------------------------------------------------------------------------------------------------------------

BOOL NOXCamera::SetFOVControl(Control *c)
{
	cntrl = c;
	return TRUE;
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetFOVType(int ft)
{
	trCam.setFOVType(ft);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//-----------------------------------------------------------------------------------------------------------------------

INode* FindNodeRef(ReferenceTarget *rt);

static INode* GetNodeRef(ReferenceMaker *rm) 
{
	if (rm->SuperClassID()==BASENODE_CLASS_ID) return (INode *)rm;
	else return rm->IsRefTarget()?FindNodeRef((ReferenceTarget *)rm):NULL;
}

INode* FindNodeRef(ReferenceTarget *rt) 
{
	DependentIterator di(rt);
	ReferenceMaker *rm = NULL;
	INode *nd = NULL;
	while ((rm=di.Next()) != NULL) 
	{
		nd = GetNodeRef(rm);
		if (nd) return nd;
	}
	return NULL;
}

static ISubTargetCtrl* findSubCtrl(Control* ctrl, Control*& subCtrl)
{
	ISubTargetCtrl* assign = NULL;
	ISubTargetCtrl* next;
	Control* child;

	subCtrl = NULL;
	for ( next = GetSubTargetInterface(ctrl); next != NULL; next = GetSubTargetInterface(child)) {
		child = next->GetTMController();
		if (child == NULL)
			break;
		if (next->CanAssignTMController()) {
			assign = next;
			subCtrl = child;
		}
	}

	return assign;
}

static bool replaceSubLookatController(Control* old)
{
	Control* child = NULL;
	ISubTargetCtrl* assign = findSubCtrl(old, child);
	if (assign == NULL)
		return false;
	DbgAssert(assign->CanAssignTMController() && child != NULL);

	Control *tmc = NewDefaultMatrix3Controller();
	tmc->Copy(child); // doesn't copy rotation, only scale and position.
	assign->AssignTMController(tmc);

	return true;
}

static void clearTargetController(INode* node)
{
	Control* old = node->GetTMController();

	if (!replaceSubLookatController(old)) 
	{
		Control *tmc = NewDefaultMatrix3Controller();
		tmc->Copy(old); // doesn't copy rotation, only scale and position.
		node->SetTMController(tmc);
	}
}

static bool replaceSubPRSController(Control* old, INode* targNode)
{
	Control* child = NULL;
	ISubTargetCtrl* assign = findSubCtrl(old, child);
	if (assign == NULL)
		return false;
	DbgAssert(assign->CanAssignTMController() && child != NULL);

	Control *laControl = CreateLookatControl();
	laControl->SetTarget(targNode);
	laControl->Copy(child);
	assign->AssignTMController(laControl);

	return true;
}

static void setTargetController(INode* node, INode* targNode)
{
	Control* old = node->GetTMController();
	if (!replaceSubPRSController(old, targNode)) {
		Control *laControl= CreateLookatControl();
		laControl->SetTarget(targNode);
		laControl->Copy(old);
		node->SetTMController(laControl);
	}
}

//-----------------------------------------------------------------------------------------------------------------------

void NOXCamera::SetType(int tp)
{
	int oldType = trCam.getType();
	if (oldType == tp)
		return;
	trCam.setType(tp);

	Interface *iface = GetCOREInterface();
	TimeValue t = iface->GetTime();
	INode *nd = FindNodeRef(this);
	if (nd==NULL) 
		return;

	if (oldType)
	{	
		// was a targeted so delete target
		UpdateTargDistance(t, nd);

		DependentIterator di(this);
		ReferenceMaker *rm = NULL;
		// iterate through the instances
		while ((rm=di.Next()) != NULL) 
		{
			nd = GetNodeRef(rm);
			if (nd) 
			{
				INode* tn = nd->GetTarget(); 
				Matrix3 tm = nd->GetNodeTM(0);
				if (tn) iface->DeleteNode(tn);
				clearTargetController(nd);
				nd->SetNodeTM(0,tm);
			}
		}
	}
	else
	{	
		// was free so add target
		DependentIterator di(this);
		ReferenceMaker *rm = NULL;
		// iterate through the instances
		while ((rm=di.Next()) != NULL) 
		{
			nd = GetNodeRef(rm);
			if (nd) 
			{
				// create a target, assign lookat controller
				Matrix3 tm = nd->GetNodeTM(t);
				Matrix3 targtm = tm;
				targtm.PreTranslate(Point3(0.0f,0.0f,-tDist));
				Object *targObject = new NOXTargetObject;
				INode *targNode = iface->CreateObjectNode( targObject);
				TSTR targName;
				targName = nd->GetName();
				targName += GetString(IDS_NOX_CAM_DOT_TARGET);
				targNode->SetName(targName);
				targNode->SetNodeTM(0,targtm);
				setTargetController(nd, targNode);
				targNode->SetIsTarget(1);   
			}
		}
	}

	NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
	NotifyDependents(FOREVER, PART_OBJ, REFMSG_NUM_SUBOBJECTTYPES_CHANGED);
	iface->RedrawViews(iface->GetTime());
}

//-----------------------------------------------------------------------------------------------------------------------
