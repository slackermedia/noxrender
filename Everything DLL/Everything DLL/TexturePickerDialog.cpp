#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <Shlobj.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "ColorSchemes.h"
#include "log.h"
#include "DLL.h"
#include "RendererMainWindow.h"
#include "MatEditor.h"

extern HMODULE hDllModule;

//----------------------------------------------------------------------------------------------

bList<PickedTexture *, 16> loadedTexturesListPT(0);
void createTexturesListPT(unsigned int prevSizeX, unsigned int prevSizeY);
void fillGUITexturesListPT(HWND hDlg);
void releaseTexturesListPT();

//----------------------------------------------------------------------------------------------

INT_PTR CALLBACK TexPickerDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			if (!bgBrush)
				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);

			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_PTEX_OK)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_PTEX_CANCEL)));
			GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(GetDlgItem(hWnd, IDC_PTEX_SHOW_FOLDER)));
			GlobalWindowSettings::colorSchemes.apply(GetEMListViewInstance(GetDlgItem(hWnd, IDC_PTEX_LIST)));
			GlobalWindowSettings::colorSchemes.apply(GetEMPViewInstance(GetDlgItem(hWnd, IDC_PTEX_PREVIEW)));
			GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(GetDlgItem(hWnd, IDC_PTEX_FULLFILENAME)), true);

			HWND hList= GetDlgItem(hWnd, IDC_PTEX_LIST);
			EMListView * emlv = GetEMListViewInstance(hList);
			RECT lrect, prect;
			GetClientRect(hList, &lrect);
			emlv->setColNumAndClearData(1, lrect.right);
			emlv->headers.add((char*)malloc(64));
			emlv->headers.createArray();
			sprintf_s(emlv->headers[0], 64, "Texture name");

			GetClientRect(GetDlgItem(hWnd, IDC_PTEX_PREVIEW), &prect);
			createTexturesListPT(prect.right, prect.bottom);
			fillGUITexturesListPT(hWnd);

			EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_PTEX_PREVIEW));
			SetWindowText(GetDlgItem(hWnd, IDC_PTEX_FULLFILENAME), "none");

			if (lParam)
			{
				char * srcfname = (char *)lParam;
				int n = loadedTexturesListPT.objCount;
				for (int i=0; i<n; i++)
				{
					if (!loadedTexturesListPT[i])
						continue;
					if (!loadedTexturesListPT[i]->fullfilename)
						continue;
					if (strcmp(srcfname, loadedTexturesListPT[i]->fullfilename))
						continue;
					emlv->selected = i;
					InvalidateRect(hList, NULL, false);

					char fname2[2048];
					fname2[0] = 0;
					char * fptr = NULL;
					GetFullPathName(srcfname, 2048, fname2, &fptr);
					SetWindowText(GetDlgItem(hWnd, IDC_PTEX_FULLFILENAME), fname2);

					if (empv)
					{
						empv->byteBuff = &loadedTexturesListPT[i]->preview;
						InvalidateRect(empv->hwnd, NULL, false);
					}
					break;
				}
			}
		}
		break;
	case WM_DESTROY:
		{
			HWND hPrev = GetDlgItem(hWnd, IDC_PTEX_PREVIEW);
			EMPView * empv = GetEMPViewInstance(hPrev);
			empv->byteBuff = NULL;

			releaseTexturesListPT();
		}
		break;
	case WM_PAINT:
		{
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rect;
			hdc = BeginPaint(hWnd, &ps);
			GetClientRect(hWnd, &rect);
			HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
			HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
			HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
			HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
			Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
			DeleteObject(SelectObject(hdc, oldPen));
			DeleteObject(SelectObject(hdc, oldBrush));
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId, wmEvent;
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			switch (wmId)
			{
				case IDC_PTEX_OK:
				{
					char * result = NULL;
					EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_PTEX_LIST));
					if (emlv  &&  emlv->selected>-1  &&  emlv->selected<loadedTexturesListPT.objCount)
						result = copyString(loadedTexturesListPT[emlv->selected]->fullfilename);

					EndDialog(hWnd, (INT_PTR)result);
				}
				break;
				case IDC_PTEX_CANCEL:
				{
					EndDialog(hWnd, (INT_PTR)(NULL));
				}
				break;
				case IDC_PTEX_SHOW_FOLDER:
				{
					HWND hList= GetDlgItem(hWnd, IDC_PTEX_LIST);
					EMListView * emlv = GetEMListViewInstance(hList);
					if (!emlv)
						break;
					if (emlv->selected<0  ||  emlv->selected>=loadedTexturesListPT.objCount)
						break;

					char * fname = loadedTexturesListPT[emlv->selected]->fullfilename;
					if (!fname)
						break;
					Logger::add(fname);

					char fname2[2048];
					fname2[0] = 0;
					char * fptr = NULL;
					GetFullPathName(fname, 2048, fname2, &fptr);
					Logger::add(fname2);

					ITEMIDLIST __unaligned *pIDL = ILCreateFromPath(fname2) ;
					if(NULL != pIDL)
					{
						SHOpenFolderAndSelectItems(pIDL, 0, 0, 0) ;
						ILFree(pIDL) ;
					}
				}
				break;
				
				case IDC_PTEX_LIST:
				{
					HWND hList= GetDlgItem(hWnd, IDC_PTEX_LIST);
					EMListView * emlv = GetEMListViewInstance(hList);
					if (!emlv)
						break;
					if (wmEvent==LV_SELECTION_CHANGED)
					{
					}
					if (wmEvent==LV_MOUSEOVER_CHANGED)
					{
						int id = emlv->mouseOver;
						if (id<0)
							id = emlv->selected;
						if (id<0  ||  id>=loadedTexturesListPT.objCount)
							break;

						PickedTexture * pt = loadedTexturesListPT[id];
						if (!pt)
							break;
						EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_PTEX_PREVIEW));
						if (empv)
						{
							empv->byteBuff = &(pt->preview);
							InvalidateRect(empv->hwnd, NULL, false);
						}

						HWND hftext = GetDlgItem(hWnd, IDC_PTEX_FULLFILENAME);
						char fname2[2048];
						fname2[0] = 0;
						char * fptr = NULL;
						GetFullPathName(pt->fullfilename, 2048, fname2, &fptr);
						SetWindowText(hftext, fname2);
					}
				}
				break;
			}
		}	// end WM_COMMAND
		break;

	case WM_CTLCOLORSTATIC:
		{	// colors for text background
			HDC hdc1 = (HDC)wParam;
			SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
			SetBkMode(hdc1, TRANSPARENT);
			return (INT_PTR)bgBrush;
		}
	case WM_CTLCOLORDLG:
		{	// dialog background
			return (INT_PTR)bgBrush;
		}
		break;
	}
	return false;
}

//----------------------------------------------------------------------------------------------

void fillGUITexturesListPT(HWND hDlg)
{
	if (!hDlg)
		return;
	HWND hList= GetDlgItem(hDlg, IDC_PTEX_LIST);
	EMListView * emlv = GetEMListViewInstance(hList);
	if (!emlv)
		return;
	emlv->freeEntries();

	int ntex = loadedTexturesListPT.objCount;
	for (int i=0; i<ntex; i++)
	{
		emlv->addEmptyRow();
		emlv->setData(0, i, loadedTexturesListPT[i]->filename);
	}

}

//----------------------------------------------------------------------------------------------

bool addTextureToListPT(Texture *tex, unsigned int prevSizeX, unsigned int prevSizeY)
{
	CHECK(tex);
	CHECK(tex->isValid());
	char * ffname = tex->fullfilename;
	CHECK(ffname);
	int n = loadedTexturesListPT.objCount;
	for (int i=0; i<n; i++)
	{
		if (!loadedTexturesListPT[i])
			continue;
		if (!loadedTexturesListPT[i]->fullfilename)
			continue;
		if (!strcmp(ffname, loadedTexturesListPT[i]->fullfilename))
			return false;	// already have that tex on list, no copy needed
	}

	PickedTexture * pt = new PickedTexture();
	pt->filename = copyString(tex->filename);
	pt->fullfilename = copyString(tex->fullfilename);

	int ow = tex->bmp.getWidth();
	int oh = tex->bmp.getHeight();
	int dw, dh;
	if (ow*prevSizeY>oh*prevSizeX)
	{
		dw = prevSizeX;
		dh = min(prevSizeY, prevSizeX*oh/ow);
	}
	else
	{
		dw = min(prevSizeX, prevSizeY*ow/oh);
		dh = prevSizeY;
	}
	pt->preview.allocBuffer(dw, dh);
	for (int y=0; y<dh; y++)
		for (int x=0; x<dw; x++)
		{
			int ox = min(x*ow/(int)dw, ow-1);
			int oy = min(y*oh/(int)dh, oh-1);
			Color4 oc = tex->bmp.getColorUnprocessed(ox, oy);
			unsigned char cr = (unsigned char)max(0.0f, min(oc.r*255, 255.0f));
			unsigned char cg = (unsigned char)max(0.0f, min(oc.g*255, 255.0f));
			unsigned char cb = (unsigned char)max(0.0f, min(oc.b*255, 255.0f));
			pt->preview.imgBuf[y][x] = RGB(cr, cg, cb);
		}

	loadedTexturesListPT.add(pt);
	loadedTexturesListPT.createArray();

	return true;
}

//----------------------------------------------------------------------------------------------

void createTexturesListPT(unsigned int prevSizeX, unsigned int prevSizeY)
{
	Raytracer * rtr = Raytracer::getInstance();
	int mainSceneId = -1;
	if (RendererMainWindow::sceneNumber > -1)
		mainSceneId = RendererMainWindow::sceneNumber;	// renderer main window loaded scene 
	else
	{		//find max plugin scene
		if (rtr->pluginMaxSceneId>-1)
			mainSceneId = rtr->pluginMaxSceneId;
	}

	// main scene textures
	if (mainSceneId > -1)
	{
		Scene * sc = rtr->scenes[mainSceneId];
		if (!sc)
		{
			Logger::add("Error: TexturePicker: NULL scene on raytracer list.");
			return;
		}
		int ntex = sc->texManager.textures.objCount;
		for (int i=0; i<ntex; i++)
			addTextureToListPT(sc->texManager.textures[i], prevSizeX, prevSizeY);
	}

	// Material from Mat Editor
	MaterialNox * mat = NULL;
	Scene * sc = rtr->curScenePtr;
	if (sc)
	{
		int edm = sc->sscene.editableMaterial;
		if (edm>-1)
			mat = sc->mats[edm];
	}
	if (mat)
	{
		int id;
		id = mat->tex_displacement.managerID;
		addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
		id = mat->tex_opacity.managerID;
		addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
		
		int nl = mat->layers.objCount;
		for (int i=0; i<nl; i++)
		{
			MatLayer * mlay = &(mat->layers[i]);

			id = mlay->tex_col0.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_col90.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_rough.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_transm.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_weight.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_normal.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_light.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_aniso.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
			id = mlay->tex_aniso_angle.managerID;
			addTextureToListPT( (id>-1 ? sc->texManager.textures[id] : NULL ), prevSizeX, prevSizeY);
		}
	}
}

//----------------------------------------------------------------------------------------------

void releaseTexturesListPT()
{
	int n = loadedTexturesListPT.objCount;
	for (int i=0; i<n; i++)
	{
		PickedTexture * pt = loadedTexturesListPT[i];
		if (!pt)
			continue;
		if (pt->fullfilename)
			free(pt->fullfilename);
		if (pt->filename)
			free(pt->filename);
		pt->fullfilename = NULL;
		pt->filename = NULL;
		pt->preview.freeBuffer();
		delete pt;
	}
	loadedTexturesListPT.freeList();
	loadedTexturesListPT.createArray();
}

//----------------------------------------------------------------------------------------------

