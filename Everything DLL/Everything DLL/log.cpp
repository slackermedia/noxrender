#include "log.h"
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

//#define NOX_LOG_NOTHING

char * Logger::dir = "C:\\logs";
bool Logger::opened = false;
HANDLE Logger::hFile;
bool Logger::do_not_log = false;

bool Logger::setLogDir(char * newDir)
{
	if (!newDir)
		return false;
	size_t l = strlen(newDir);
	if (l < 1)
		return false;
	char * nd = (char *)malloc(l+1);
	sprintf_s(nd, l+1, "%s", newDir);
	dir = nd;

	initialize();
	return true;
}

bool Logger::initialize()
{
	#ifdef NOX_LOG_NOTHING
		return true;
	#endif
	if (do_not_log)
		return true;

	char filename[1024];

	SYSTEMTIME st;
	GetLocalTime(&st);

	sprintf_s(filename,1024, "%s\\log__%d_%d_%d__%d_%d_%d.txt", dir, st.wYear, st.wMonth, st.wDay,  st.wHour, st.wMinute, st.wSecond);

	hFile = CreateFile(filename, FILE_WRITE_DATA ,  FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile)
	{
		opened = true;
		return true;
	}

	return false;
}

bool Logger::add(char * what)
{
	#ifdef NOX_LOG_NOTHING
		return true;
	#endif
	if (do_not_log)
		return true;

	if (!what)
		what = "[NULL PTR PASSED TO LOGGER]";

	if (!opened)
		if (!initialize())
			return false;

	SYSTEMTIME st;
	GetLocalTime(&st);

	unsigned int ss = (unsigned int)strlen(what) + 40;
	char * buf = (char *)malloc(ss);
	if (!buf)
		return false;

	sprintf_s(buf, ss, "%2d:%2d:%2d   %s\r\n", st.wHour, st.wMinute, st.wSecond, what);

	int w;
	DWORD tw;
	tw = (DWORD)strlen(buf);

	bool res = (TRUE == WriteFile(hFile, buf, tw, (LPDWORD)&w, NULL));

	free(buf);

	if (res && tw==w)
		return true;

	return false;
}

bool Logger::closeFile()
{
	#ifdef NOX_LOG_NOTHING
		return true;
	#endif
	if (do_not_log)
		return true;

	bool res = (TRUE == CloseHandle(hFile));
	if (res)
		opened = false;
	return res;
}

void Logger::setLogsOn(bool enabled)
{
	do_not_log = !enabled;
}
