#ifndef WINVER                // Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT        // Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0501        // Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif                        

#ifndef _WIN32_WINDOWS        // Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0501 // Change this to the appropriate value to target Windows Me or later.
#endif

//#define GUI2_USE_GDIPLUS

#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"
#include "resource.h"
#include"log.h"

extern HMODULE hDllModule;

#define MINSBH 32

//----------------------------------------------------------------------

void InitEM2ListView()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2ListView";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2ListViewProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = CS_DBLCLKS;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EM2ListView *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2ListView * GetEM2ListViewInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2ListView * emlv = (EM2ListView *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2ListView * emlv = (EM2ListView *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emlv;
}

//----------------------------------------------------------------------

void SetEM2ListViewInstance(HWND hwnd, EM2ListView *emlv)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emlv);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emlv);
	#endif
}

//----------------------------------------------------------------------

EM2ListView::EM2ListView(HWND hWnd) :
	data(0)
{
	hwnd = hWnd;
	hFontData = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	hFontHeader = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfontdata = true;
	autodelfontdata = false;
	useCursors = true;

	hRegion = 0;
	hRegSlider = 0;
	hRegUnderSlider = 0;
	hRegOverSlider = 0;
	hRegEmpty = 0;

	headers = NULL;
	colWidths = NULL;
	align = NULL;
	alignHeader = NULL;
	margins = NULL;
	marginsHeader = NULL;


	alphaBgHeader = 0;
	alphaBgDataOdd = 0;
	alphaBgDataEven = 0;
	alphaBgDataSelected = 255;
	alphaBgDataMouseOver = 0;
	alphaBorderMouseOver = 128;

	colBorderMouseOver			= NGCOL_YELLOW;

	colBackgroundHeader			= RGB(128,128,128);
	colBackgroundOdd			= RGB(128,128,128);
	colBackgroundEven			= RGB(140,140,140);
	colBackgroundSelected		= RGB(30,30,30);
	colBackgroundMouseOver		= RGB(38,38,38);

	colText						= NGCOL_DARK_GRAY;
	colTextHeader				= RGB(0x9d,0x97,0x94);
	colTextSelected				= NGCOL_YELLOW;
	colTextMouseOver			= NGCOL_LIGHT_GRAY;
	colTextShadow				= RGB(15,15,15);

	colTextDisabled				= NGCOL_TEXT_DISABLED;
	colTextDisabledSelected		= NGCOL_DARK_GRAY;//RGB(255,0,0);
	colTextDisabledHeader		= RGB(0x9d,0x97,0x94);

	alphaSBSlider = 255;
	alphaSBBackground = 255;
	colSBSlider					= RGB(87,87,87);
	colSBSliderMouseOver		= RGB(95,95,95);
	colSBSliderClicked			= RGB(95,95,95);
	colSBBackground				= RGB(49,49,49);

	numCols = 0;
	headerHeight = 18;
	rowHeight = 18;
	selected = 0;
	mouseOver = -1;
	firstShown = 0;
	spaceToSB = 5;
	showHeader = true;
	forceDrawScrollbar = true;
	#ifdef GUI2_DRAW_TEXT_SHADOWS
		drawTextShadow = true;
	#else
		drawTextShadow = false;
	#endif

	sbWidth = 9;
	sliding = false;
	minSbH = 32;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;

	prepareWindow();
	calcSB();
}

//----------------------------------------------------------------------

EM2ListView::~EM2ListView()
{
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2ListViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2ListView* emlv;
	emlv = GetEM2ListViewInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	HFONT hOldFont;
	static int ly=0;
	static int lsbst=0;

	switch (msg)
	{
		case WM_CREATE:
			{
				EM2ListView * emlv1 = new EM2ListView(hwnd);
				SetEM2ListViewInstance(hwnd, emlv1);
			}
			break;
		case WM_DESTROY:
			{
				delete emlv;
				SetEM2ListViewInstance(hwnd, 0);
			}
			break;
		case WM_SIZE:
			{
				emlv->prepareWindow();
				emlv->updateSliderRegions();
			}
			break;
		case WM_PAINT:
			{
				DWORD style;
				style = GetWindowLong(hwnd, GWL_STYLE);
				bool disabled = ((style & WS_DISABLED) > 0);

				HDC ohdc = BeginPaint(hwnd, &ps);
				GetClientRect(hwnd, &rect);

				// create back buffer
				hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
				SelectObject(hdc, backBMP);

				// copy background image
				if (emlv->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emlv->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emlv->bgShiftX, emlv->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
					SetBkMode(hdc, TRANSPARENT);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
					SetBkMode(hdc, OPAQUE);
					SetBkColor(hdc, emlv->colBackgroundOdd);
				}

				int hht = emlv->showHeader ? emlv->getHeightHeader() : 0;
				int w = rect.right-rect.left;
				int h = (emlv->maxShownRowsAndHeader)*emlv->getHeightData()+2;
				int fs = emlv->firstShown;

				int sbwneeded = 0;
				if (emlv->forceDrawScrollbar  ||  emlv->getNumRows()>emlv->maxShownRows)
					sbwneeded = emlv->sbWidth + emlv->spaceToSB;

				//#define GUI2_USE_GDIPLUS
				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);

						// fill header bg
						if (emlv->showHeader  &&  emlv->alphaBgHeader>0)
						{
							Gdiplus::SolidBrush brush1(Gdiplus::Color(emlv->alphaBgHeader, GetRValue(emlv->colBackgroundHeader), GetGValue(emlv->colBackgroundHeader), GetBValue(emlv->colBackgroundHeader)));
							graphics.FillRectangle(&brush1, 0,0, rect.right, emlv->getHeightHeader());
						}

						for (int j=0; j<emlv->maxShownRows; j++)
						{
							RECT crect = {0, j*emlv->getHeightData()+hht, w-sbwneeded, (j+1)*emlv->getHeightData()+hht};
							unsigned char bgAlpha;
							COLORREF bgColor;
							int cc = (fs+j);
							if (cc==emlv->selected)
							{
								SetTextColor(hdc, emlv->colTextSelected);
								bgAlpha = emlv->alphaBgDataSelected;
								bgColor = emlv->colBackgroundSelected;
							}
							else
								if (cc%2)
								{
									SetTextColor(hdc, emlv->colText);
									bgAlpha = emlv->alphaBgDataOdd;
									bgColor = emlv->colBackgroundOdd;
								}
								else
								{
									SetTextColor(hdc, emlv->colText);
									bgAlpha = emlv->alphaBgDataEven;
									bgColor = emlv->colBackgroundEven;
								}

							if (cc==emlv->mouseOver)
								SetTextColor(hdc, emlv->colTextMouseOver);

							if (bgAlpha>0)
							{
								Gdiplus::SolidBrush brush1(Gdiplus::Color(bgAlpha, GetRValue(bgColor), GetGValue(bgColor), GetBValue(bgColor)));
								graphics.FillRectangle(&brush1, crect.left, crect.top, crect.right-crect.left, crect.bottom-crect.top);
							}
							if (cc==emlv->mouseOver  &&  emlv->alphaBorderMouseOver>0)	// mouse over border
							{
								Gdiplus::Pen bpen(Gdiplus::Color(emlv->alphaBorderMouseOver, GetRValue(emlv->colBorderMouseOver), GetGValue(emlv->colBorderMouseOver), GetBValue(emlv->colBorderMouseOver)));
								graphics.DrawRectangle(&bpen, crect.left, crect.top, crect.right-crect.left-1, crect.bottom-crect.top-1);
							}
						}

					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					// fill header bg
					if (emlv->showHeader  &&  emlv->alphaBgHeader>0)
					{
						COLORREF c_bg = getMixedColor(NGCOL_TEXT_BACKGROUND, emlv->alphaBgHeader, emlv->colBackgroundHeader);
						HBRUSH hOBr = (HBRUSH)SelectObject(hdc, CreateSolidBrush(c_bg));
						HPEN hOPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, c_bg));
						Rectangle(hdc, 0, 0, rect.right, emlv->getHeightHeader());
						DeleteObject(SelectObject(hdc, hOPen));
						DeleteObject(SelectObject(hdc, hOBr));
					}

					for (int j=0; j<emlv->maxShownRows; j++)
					{
						RECT crect = {0, j*emlv->getHeightData()+hht, w-sbwneeded, (j+1)*emlv->getHeightData()+hht};
						unsigned char bgAlpha;
						COLORREF bgColor;
						int cc = (fs+j);
						SetBkColor(hdc, NGCOL_TEXT_BACKGROUND);
						if (cc==emlv->selected)
						{
							SetTextColor(hdc, emlv->colTextSelected);
							bgAlpha = emlv->alphaBgDataSelected;
							bgColor = emlv->colBackgroundSelected;
						}
						else
							if (cc%2)
							{
								SetTextColor(hdc, emlv->colText);
								bgAlpha = emlv->alphaBgDataOdd;
								bgColor = emlv->colBackgroundOdd;
							}
							else
							{
								SetTextColor(hdc, emlv->colText);
								bgAlpha = emlv->alphaBgDataEven;
								bgColor = emlv->colBackgroundEven;
							}

						if (cc==emlv->mouseOver)
							SetTextColor(hdc, emlv->colTextMouseOver);
						if (bgAlpha>0)
						{
							COLORREF destBG = getMixedColor(NGCOL_TEXT_BACKGROUND, bgAlpha, bgColor);
							HBRUSH destBrush = CreateSolidBrush(destBG);
							FillRect(hdc, &crect, destBrush);
							DeleteObject(destBrush);
						}
						if (cc==emlv->mouseOver  &&  emlv->alphaBorderMouseOver>0)	// mouse over border
						{
							COLORREF destBorder = getMixedColor(NGCOL_TEXT_BACKGROUND, emlv->alphaBorderMouseOver, emlv->colBorderMouseOver);
							HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
							HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, destBorder));
							Rectangle(hdc, crect.left, crect.top, crect.right, crect.bottom);
							DeleteObject(SelectObject(hdc, oldPen));
							SelectObject(hdc, oldBrush);
						}

					}
				#endif


				SIZE sz;
				SetBkMode(hdc, TRANSPARENT);
				hOldFont = (HFONT)SelectObject(hdc, emlv->getFontHeader());
				if (emlv->showHeader)
				{
					SetBkColor  (hdc, emlv->colBackgroundHeader);
					RECT crect = {1,1,rect.right-1,emlv->getHeightData()+1};
					int lCur = 1;
					for (int i=0; i<emlv->numCols; i++)
					{
						char * header = emlv->getHeaderName(i);// headers[i];
						if (!header)
							break;
						int lng = (int)strlen(header);
						crect.top = 1;
						crect.bottom = emlv->getHeightHeader();
						crect.left = lCur;
						crect.right = lCur + emlv->colWidths[i];

						GetTextExtentPoint32(hdc, header, lng, &sz);

						int px, py;
						py = (emlv->getHeightHeader()-sz.cy)/2;
						switch (emlv->alignHeader[i])
						{
							case EM2ListView::ALIGN_RIGHT:
								px = lCur + emlv->colWidths[i] - emlv->marginsHeader[i] - 2 - sz.cx;
								break;
							case EM2ListView::ALIGN_CENTER:
								px = lCur + (emlv->colWidths[i] - sz.cx)/2;
								break;
							case EM2ListView::ALIGN_LEFT:
							default:
								px = lCur + 1 + emlv->marginsHeader[i];
								break;
						}

						if (emlv->drawTextShadow)
						{
							SetTextColor(hdc, emlv->colTextShadow);
							ExtTextOut(hdc, px+1, py,   ETO_CLIPPED, &crect, header, lng, 0);
							ExtTextOut(hdc, px-1, py,   ETO_CLIPPED, &crect, header, lng, 0);
							ExtTextOut(hdc, px, py+1,   ETO_CLIPPED, &crect, header, lng, 0);
							ExtTextOut(hdc, px, py-1,   ETO_CLIPPED, &crect, header, lng, 0);
						}

						SetTextColor(hdc, disabled ? emlv->colTextDisabledHeader :  emlv->colTextHeader);
						ExtTextOut(hdc, px, py, ETO_CLIPPED, &crect, header, lng, 0);

						lCur += emlv->colWidths[i] ;
					}
				}

				SelectObject(hdc, emlv->getFontData());
				for (int j=0; j<emlv->maxShownRows; j++)
				{
					if (j+fs >= emlv->getNumRows())
						break;

					RECT crect = {0, j*emlv->getHeightData()+hht, w-emlv->sbWidth, (j+1)*emlv->getHeightData()+hht};
					unsigned char bgAlpha;
					COLORREF bgColor;
					COLORREF txtColor;
					int cc = (fs+j);
					SetBkColor(hdc, NGCOL_TEXT_BACKGROUND);
					if (disabled)
					{
						if (cc==emlv->selected)
						{
							txtColor = emlv->colTextDisabledSelected;
							bgAlpha = emlv->alphaBgDataSelected;
							bgColor = emlv->colBackgroundSelected;
						}
						else
						{
							txtColor = emlv->colTextDisabled;
							bgAlpha = emlv->alphaBgDataSelected;
							bgColor = emlv->colBackgroundSelected;
						}
					}
					else
					{
						if (cc==emlv->selected)
						{
							txtColor = emlv->colTextSelected;
							bgAlpha = emlv->alphaBgDataSelected;
							bgColor = emlv->colBackgroundSelected;
						}
						else
							if (cc==emlv->mouseOver)
							{
								txtColor = emlv->colTextMouseOver;
								bgAlpha = emlv->alphaBgDataMouseOver;
								bgColor = emlv->colBackgroundMouseOver;
							}
							else
								if (cc%2)
								{
									txtColor = emlv->colText;
									bgAlpha = emlv->alphaBgDataOdd;
									bgColor = emlv->colBackgroundOdd;
								}
								else
								{
									txtColor = emlv->colText;
									bgAlpha = emlv->alphaBgDataEven;
									bgColor = emlv->colBackgroundEven;
								}
					}
					if (bgAlpha>0)
					{
						COLORREF destBG = getMixedColor(NGCOL_TEXT_BACKGROUND, bgAlpha, bgColor);
						SetBkColor(hdc, destBG);
					}

					int curL = 1;
					for (int i=0; i<emlv->numCols; i++)
					{
						char * ddata = emlv->getData(i, j+fs);
						int cLen = 0;
						if (ddata)
							cLen = (int)strlen(ddata);
						if (cLen < 1)
						{
							curL += emlv->colWidths[i] ;
							continue;
						}
					
						RECT cellrect = {curL+1, j*emlv->getHeightData()+hht+0, curL+emlv->colWidths[i]-2, (j+1)*emlv->getHeightData()+hht};
						GetTextExtentPoint32(hdc, ddata, cLen, &sz);
						
						int px, py;
						switch (emlv->align[i])
						{
							case EM2ListView::ALIGN_LEFT:
								px = curL+1 + emlv->margins[i];
								py = (emlv->getHeightData()-sz.cy)/2+cellrect.top;
								break;
							case EM2ListView::ALIGN_CENTER:
								px = curL+(emlv->colWidths[i]-sz.cx-2)/2;
								py = (emlv->getHeightData()-sz.cy)/2+cellrect.top;
								break;
							case EM2ListView::ALIGN_RIGHT:
								px = curL+emlv->colWidths[i]-sz.cx-2 - emlv->margins[i];
								py = (emlv->getHeightData()-sz.cy)/2+cellrect.top;
								break;
						}

						if (emlv->drawTextShadow)
						{
							SetTextColor(hdc, emlv->colTextShadow);
							ExtTextOut(hdc, px+1, py, ETO_CLIPPED, &cellrect, ddata, cLen, 0);
							ExtTextOut(hdc, px-1, py, ETO_CLIPPED, &cellrect, ddata, cLen, 0);
							ExtTextOut(hdc, px, py+1, ETO_CLIPPED, &cellrect, ddata, cLen, 0);
							ExtTextOut(hdc, px, py-1, ETO_CLIPPED, &cellrect, ddata, cLen, 0);
						}

						SetTextColor(hdc, txtColor);
						ExtTextOut(hdc, px, py, ETO_CLIPPED, &cellrect, ddata, cLen, 0);

						curL += emlv->colWidths[i] ;
					}
				}

				if (emlv->forceDrawScrollbar  ||  emlv->maxShownRows<emlv->getNumRows())
					emlv->drawScrollBar(hdc);

				SelectObject(hdc, hOldFont);

				// copy from back buffer
				GetClientRect(hwnd, &rect);
				BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);

				EndPaint(hwnd, &ps);
			}	// end WM_PAINT
			break;
		case WM_MOUSEMOVE:
			{
				if (!GetCapture())
					SetCapture(hwnd);
				if (emlv->sliding)
				{
					if (emlv->useCursors)
						SetCursor(LoadCursor (NULL, IDC_ARROW));
					int my = (short)HIWORD(lParam);
					emlv->sbStart = lsbst + my-ly;
					emlv->calcFP();
					InvalidateRect(hwnd, NULL, false);
				}
				else
				{
					int newMouseOver = emlv->mouseOver;
					int mx = (short)LOWORD(lParam);
					int my = (short)HIWORD(lParam);
					GetClientRect(hwnd, &rect);
					int w = rect.right - rect.left;
					int hht = emlv->showHeader ? emlv->getHeightHeader() : 0;
					int h = (emlv->maxShownRows)*emlv->getHeightData()+hht;

					int sls = (emlv->maxShownRows<emlv->getNumRows()) ? emlv->sbWidth : 0;
					if (mx < 1   ||   mx > w-2   ||   my < 1   ||   my > h-1) 
					{	// outside and borders
						newMouseOver = -1;
						if (emlv->useCursors)
							SetCursor(LoadCursor (NULL, IDC_ARROW));
						if (!emlv->sliding)
							ReleaseCapture();
					}
					else
					{
						if (mx > w-2-sls)
						{	// on scrollbar however not sliding
							newMouseOver = -1;
							if (emlv->useCursors)
								SetCursor(LoadCursor (NULL, IDC_ARROW));
							if (PtInRegion(emlv->hRegSlider, mx, my))
								newMouseOver = -2;
						}
						else
						{	
							if (my < hht)
							{	// on header
								if (emlv->useCursors)
									SetCursor(LoadCursor (NULL, IDC_ARROW));
								newMouseOver = -1;
							}
							else
							{	// on data
								newMouseOver = (my-hht)/emlv->getHeightData()+emlv->firstShown;
								if (emlv->useCursors)
									SetCursor(LoadCursor (NULL, IDC_HAND));
								if (newMouseOver >= emlv->getNumRows())
								{
									newMouseOver = -1;
								}
							}
						}

					}

					if (newMouseOver != emlv->mouseOver)
					{
						emlv->mouseOver = newMouseOver;
						InvalidateRect(hwnd, NULL, false);
						emlv->notifyMouseOverChanged();
					}
				}
			}
			break;
		case WM_CAPTURECHANGED:
			{
				emlv->mouseOver = -1;
				emlv->notifyMouseOverChanged();
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				SetFocus(hwnd);
				int mx = (short)LOWORD(lParam);
				int my = (short)HIWORD(lParam);
				GetClientRect(hwnd, &rect);
				int w = rect.right - rect.left;
				int h = emlv->maxShownRowsAndHeader * emlv->getHeightData() + 2;

				if (mx < 1   ||   mx > w-2   ||   my < 1   ||   my > h-1)
					break;

				int sls = (emlv->maxShownRows<emlv->getNumRows()) ? emlv->sbWidth : 0;
				if (mx > w-2-sls)
				{	// on trackbar
					if (PtInRegion(emlv->hRegSlider, mx, my))
					{
						emlv->sliding = true;
						ly = my;
						lsbst = emlv->sbStart;
						InvalidateRect(hwnd, NULL, false);
					}
					else
					{
						if (PtInRegion(emlv->hRegOverSlider, mx, my))
						{
							emlv->firstShown -= emlv->maxShownRows;
							emlv->firstShown = max(0, emlv->firstShown);
							emlv->calcSB();
						}
						else
							if (PtInRegion(emlv->hRegUnderSlider, mx, my))
							{
								emlv->firstShown += emlv->maxShownRows;
								emlv->firstShown = min(emlv->firstShown,  emlv->getNumRows()-emlv->maxShownRows);
								emlv->calcSB();
							}
						InvalidateRect(hwnd, NULL, false);
					}
				}
				else
					if (emlv->mouseOver>-1)
					{	// data selection
						emlv->selected = emlv->mouseOver;
						InvalidateRect(hwnd, NULL, false);
						emlv->notifySelectionChanged();
					}
			}
			break;
		case WM_LBUTTONUP:
			{
				emlv->sliding = false;
				int mx = (short)LOWORD(lParam);
				int my = (short)HIWORD(lParam);
				GetClientRect(hwnd, &rect);
				int w = rect.right-rect.left;
				int h = (emlv->maxShownRowsAndHeader)*emlv->getHeightData()+2;

				if (mx < 0   ||   mx > w   ||   my < 0   ||   my > h)
					ReleaseCapture();
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONDBLCLK:
			{
				if (emlv->mouseOver>-1)
				{
					emlv->selected = emlv->mouseOver;
					InvalidateRect(hwnd, NULL, false);
					emlv->notifyDoubleClick();
				}
			}
			break;
		case WM_MOUSEWHEEL:
			{
				int zDelta = (int)wParam;
				if (zDelta > 0)
				{
					emlv->firstShown--;
					emlv->firstShown = max(0, emlv->firstShown);
					if (emlv->getNumRows() > 0)
						emlv->calcSB();
				}
				else
				{
					emlv->firstShown++;
					emlv->firstShown = min(emlv->firstShown,  emlv->getNumRows()-emlv->maxShownRows);
					if (emlv->getNumRows() > 0)
						emlv->calcSB();
				}
				POINT p = { (short)LOWORD(lParam), (short)HIWORD(lParam) }; 
				ScreenToClient(hwnd, &p);
				LPARAM lParam2 = MAKELONG(p.x, p.y);
				SendMessage(hwnd, WM_MOUSEMOVE, wParam&0xffff, lParam2);
				InvalidateRect(hwnd, NULL, false);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

void EM2ListView::calcSB()
{
	// calc scrollbar from list pos
	int h = (maxShownRows)*rowHeight;
	if (data.objCount < 1   ||   data.objCount <= maxShownRows)
	{
		sbStart = 0;
		sbEnd = h;
		firstShown = 0;
		updateSliderRegions();
		return;
	}
	int bar = (h*maxShownRows)/data.objCount;
	bar = max(minSbH, bar);
	int rest = h - bar;
	sbStart = firstShown * rest / max(1, data.objCount - maxShownRows);
	sbEnd = sbStart + bar;
	updateSliderRegions();
}

//----------------------------------------------------------------------

void EM2ListView::calcFP()
{
	// calc list pos from scrollbar
	int h = maxShownRows*rowHeight;
	if (data.objCount < 1)
	{
		firstShown = 0;
		sbStart = 0;
		sbEnd = h;
		updateSliderRegions();
		return;
	}
	int bar = (h*maxShownRows)/data.objCount;
	bar = max(minSbH, bar);
	int rest = h - bar;
	firstShown = sbStart*(data.objCount-maxShownRows)/max(1,rest);
	firstShown = max(0, min(firstShown, data.objCount - maxShownRows));
	sbStart = max(0,min(sbStart, h-bar));
	sbEnd = sbStart + bar;
	updateSliderRegions();
}

//----------------------------------------------------------------------

void EM2ListView::drawScrollBar(HDC hdc)
{
	//#define GUI2_USE_GDIPLUS
	#ifdef GUI2_USE_GDIPLUS
	{
		ULONG_PTR gdiplusToken;		// activate gdi+
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		{
			Gdiplus::Graphics graphics(hdc);
			if (data.objCount <= maxShownRows)
			{
				firstShown = 0;
				Gdiplus::SolidBrush brush1(Gdiplus::Color(alphaSBBackground, GetRValue(colSBBackground), GetGValue(colSBBackground), GetBValue(colSBBackground)));
				Gdiplus::Region * regionEmpty = Gdiplus::Region::FromHRGN(hRegEmpty);
				graphics.FillRegion(&brush1, regionEmpty);
				delete regionEmpty;
				return;
			}
			Gdiplus::SolidBrush brush1(Gdiplus::Color(		alphaSBSlider,		GetRValue(colSBSlider),				GetGValue(colSBSlider),				GetBValue(colSBSlider)));
			Gdiplus::SolidBrush brush1mo(Gdiplus::Color(	alphaSBSlider,		GetRValue(colSBSliderMouseOver),	GetGValue(colSBSliderMouseOver),	GetBValue(colSBSliderMouseOver)));
			Gdiplus::SolidBrush brush1mc(Gdiplus::Color(	alphaSBSlider,		GetRValue(colSBSliderClicked),		GetGValue(colSBSliderClicked),		GetBValue(colSBSliderClicked)));
			Gdiplus::SolidBrush brush2(Gdiplus::Color(		alphaSBBackground,	GetRValue(colSBBackground),			GetGValue(colSBBackground),			GetBValue(colSBBackground)));

			Gdiplus::Region * regionSlider = Gdiplus::Region::FromHRGN(hRegSlider);
			if (sliding)
				graphics.FillRegion(&brush1mc, regionSlider);
			else
				if (mouseOver==-2)
					graphics.FillRegion(&brush1mo, regionSlider);
				else
					graphics.FillRegion(&brush1, regionSlider);
			delete regionSlider;
			Gdiplus::Region * regionOverSlider = Gdiplus::Region::FromHRGN(hRegOverSlider);
			Gdiplus::Region * regionUnderSlider = Gdiplus::Region::FromHRGN(hRegUnderSlider);
			graphics.FillRegion(&brush2, regionOverSlider);
			graphics.FillRegion(&brush2, regionUnderSlider);
			delete regionOverSlider;
			delete regionUnderSlider;
		}	// gdi+ variables destructors here
		Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
	}
	#else
		if (data.objCount <= maxShownRows)
		{
			firstShown = 0;
			HBRUSH hbrs3 = CreateSolidBrush(getMixedColor(NGCOL_TEXT_BACKGROUND, alphaSBBackground, colSBBackground));
			FillRgn(hdc, hRegEmpty, hbrs3);
			DeleteObject(hbrs3);
			return;
		}
		HBRUSH hbrs1 = 0;
		if (sliding)
			hbrs1 = CreateSolidBrush(getMixedColor(NGCOL_TEXT_BACKGROUND, alphaSBSlider, colSBSliderClicked));
		else
			if (mouseOver==-2)
				hbrs1 = CreateSolidBrush(getMixedColor(NGCOL_TEXT_BACKGROUND, alphaSBSlider, colSBSliderMouseOver));
			else
				hbrs1 = CreateSolidBrush(getMixedColor(NGCOL_TEXT_BACKGROUND, alphaSBSlider, colSBSlider));
		FillRgn(hdc, hRegSlider, hbrs1);
		DeleteObject(hbrs1);
		HBRUSH hbrs2 = CreateSolidBrush(getMixedColor(NGCOL_TEXT_BACKGROUND, alphaSBBackground, colSBBackground));
		FillRgn(hdc, hRegUnderSlider, hbrs2);
		FillRgn(hdc, hRegOverSlider, hbrs2);
		DeleteObject(hbrs2);
	#endif
}

//----------------------------------------------------------------------

bool EM2ListView::setHeightHeader(int h)
{
	if (h<8)
		return false;
	headerHeight = h;
	prepareWindow();
	return true;
}

//----------------------------------------------------------------------

bool EM2ListView::setHeightData(int h)
{
	if (h<8)
		return false;
	rowHeight = h;
	prepareWindow();
	return true;
}

//----------------------------------------------------------------------

int EM2ListView::getHeightHeader()
{
	return headerHeight;
}

//----------------------------------------------------------------------

int EM2ListView::getHeightData()
{
	return rowHeight;
}

//----------------------------------------------------------------------

void EM2ListView::prepareWindow()
{
	// should be called on create and each change of size or row/header height
	RECT crect;
	GetClientRect(hwnd, &crect);
	int cHeight = crect.bottom - crect.top - 2;
	
	if (showHeader)
		maxShownRows = (cHeight-headerHeight)/rowHeight;
	else
		maxShownRows = cHeight/rowHeight;
	maxShownRowsAndHeader = maxShownRows;
	if (showHeader)
		maxShownRowsAndHeader++;

	int hh = showHeader ? headerHeight : 0;
	HRGN nReg = CreateRectRgn(0,0, crect.right-crect.left,  (maxShownRows)*rowHeight+hh);
	if (nReg)
	{
		SetWindowRgn(hwnd, nReg, TRUE);
		if (hRegion)
			DeleteObject(hRegion);
		hRegion = nReg;		
	}
}

//----------------------------------------------------------------------

bool EM2ListView::addEmptyRow()
{
	char ** row = (char **)malloc(sizeof(char*)*numCols);
	if (!row)
		return false;
	
	data.add(row);
	data.createArray();
	int last = data.objCount-1;
	for (int i=0; i<numCols; i++)
		data[last][i] = NULL;

	calcSB();

	return true;
}

//----------------------------------------------------------------------

bool EM2ListView::setData(int col, int row, char * str)
{
	if (row >= data.objCount  ||  row<0)
		return false;
	if (col >= numCols  ||  col<0)
		return false;
	if (!data[row])
		return false;

	char * buf = copyString(str);

	char * olddata = data[row][col];
	data[row][col] = buf;
	if (olddata)
		free(olddata);
	
	return true;
}

//----------------------------------------------------------------------

bool EM2ListView::setHeaderName(int col, char * str)
{
	if (col >= numCols  ||  col<0)
		return false;
	char * buf = copyString(str);

	char * oldname = headers[col];
	headers[col] = buf;
	if (oldname)
		free(oldname);
	return true;
}

//----------------------------------------------------------------------

char * EM2ListView::getData(int col, int row)
{
	if (row<0 || row>=data.objCount)
		return NULL;
	if (col<0 || col>=numCols)
		return NULL;
	if (!data[row])
		return NULL;
	return data[row][col];
}

//----------------------------------------------------------------------

char * EM2ListView::getHeaderName(int col)
{
	if (col<0 || col>=numCols)
		return NULL;
	if (!headers)
		return NULL;
	return headers[col];
}

//----------------------------------------------------------------------

void EM2ListView::freeEntries()
{
	for (int i=0; i<data.objCount; i++)
	{
		if (data[i])
		{
			for (int j=0; j<numCols; j++)
			{
				if (data[i][j])
				{
					free(data[i][j]);
					data[i][j] = NULL;
				}
			}
		}
	}
	data.freeList();
	data.createArray();
}

//----------------------------------------------------------------------

bool EM2ListView::setColNumAndClearData(int num, int width)
{
	if (num<1)
		return false;

	int * newColWidth = (int *)malloc(sizeof(int)*num);
	if (!newColWidth)
		return false;
	for (int i=0; i<num; i++)
		newColWidth[i] = width;
	char * newAlign = (char *)malloc(sizeof(char)*num);
	if (!newAlign)
		return false;
	for (int i=0; i<num; i++)
		newAlign[i] = ALIGN_LEFT;
	int * newMargins = (int *)malloc(sizeof(int)*num);
	if (!newMargins)
		return false;
	for (int i=0; i<num; i++)
		newMargins[i] = 0;
	int * newMarginsHeader = (int *)malloc(sizeof(int)*num);
	if (!newMarginsHeader)
		return false;
	for (int i=0; i<num; i++)
		newMarginsHeader[i] = 0;
	char * newAlignHeader = (char *)malloc(sizeof(char)*num);
	if (!newAlignHeader)
		return false;
	for (int i=0; i<num; i++)
		newAlignHeader[i] = ALIGN_LEFT;
	char ** newHeaders= (char **)malloc(sizeof(char*)*num);
	if (!newHeaders)
		return false;
	for (int i=0; i<num; i++)
		newHeaders[i] = NULL;

	freeEntries();

	if (colWidths)
		free(colWidths);
	if (align)
		free(align);
	if (alignHeader)
		free(alignHeader);
	if (headers)
	{
		for (int i=0; i<numCols; i++)
		{
			if (headers[i])
				free(headers[i]);
			headers[i] = NULL;
		}
		free(headers);
	}
	if (margins)
		free(margins);
	if (marginsHeader)
		free(marginsHeader);

	headers = newHeaders;
	colWidths = newColWidth;
	align = newAlign;
	alignHeader = newAlignHeader;
	margins = newMargins;
	marginsHeader = newMarginsHeader;

	numCols = num;

	return true;
}

//----------------------------------------------------------------------

void EM2ListView::notifySelectionChanged()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, LV_SELECTION_CHANGED), (LPARAM)hwnd);
}

//----------------------------------------------------------------------

void EM2ListView::notifyMouseOverChanged()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, LV_MOUSEOVER_CHANGED), (LPARAM)hwnd);
}

//----------------------------------------------------------------------

void EM2ListView::notifyDoubleClick()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, LV_DOUBLECLIKED), (LPARAM)hwnd);
}

//----------------------------------------------------------------------

void EM2ListView::gotoEnd()
{
	firstShown = data.objCount - maxShownRows;
	firstShown = max(0, firstShown);
}

//----------------------------------------------------------------------

void EM2ListView::Refresh()
{
	InvalidateRect(hwnd, NULL, false);
}

//----------------------------------------------------------------------

void EM2ListView::slideToSelected()
{
	if (selected<0)
		return;
	if (firstShown>selected)
		firstShown = selected;
	if (firstShown+maxShownRows-1<selected)
		firstShown = selected-maxShownRows+1;
	calcSB();
}

//----------------------------------------------------------------------

bool EM2ListView::setFontData(HFONT hNewFont, bool autodel)
{
	if (autodelfontdata)
		DeleteObject(hFontData);
	hFontData = hNewFont;
	autodelfontdata = autodel;
	return true;
}

//----------------------------------------------------------------------

bool EM2ListView::setFontHeader(HFONT hNewFont, bool autodel)
{
	if (autodelfontheader)
		DeleteObject(hFontHeader);
	hFontHeader = hNewFont;
	autodelfontheader = autodel;
	return true;
}

//----------------------------------------------------------------------

HFONT EM2ListView::getFontData()
{
	return hFontData;
}

//----------------------------------------------------------------------

HFONT EM2ListView::getFontHeader()
{
	return hFontHeader;
}

//----------------------------------------------------------------------

void EM2ListView::updateSliderRegions()
{
	RECT rect;
	GetClientRect(hwnd, &rect);

	int l = rect.right-sbWidth;
	int y1 = showHeader ? headerHeight : 0;
	int y2 = showHeader ? headerHeight + sbEnd : sbEnd;
	int y3 = showHeader ? headerHeight + maxShownRows*rowHeight : maxShownRows*rowHeight;
	//y1 = 0;
	//y2 = sbEnd;

	HRGN hNewSliderRgn = 0;
	HRGN hNewUnderSliderRgn = 0;
	HRGN hNewOverSliderRgn = 0;
	HRGN hNewEmptyRgn = 0;
	if (sbWidth%2)	
	{
		if (data.objCount<=maxShownRows)
		{
			POINT sle[] = {
				{l+sbWidth/2,	y1-1}, 
				{l+sbWidth,		y1+sbWidth/2}, 
				{l+sbWidth,		y3-sbWidth/2-1}, 
				{l+sbWidth/2,	y3}, 
				{l,				y3-sbWidth/2-1}, 
				{l,				y1+sbWidth/2}, 
			};
			hNewEmptyRgn = CreatePolygonRgn(sle, 6, WINDING);
		}
		else
		{
			POINT slp[] = {
				{l+sbWidth/2,	y1+sbStart-1}, 
				{l+sbWidth,		y1+sbStart+sbWidth/2}, 
				{l+sbWidth,		y1+sbEnd-sbWidth/2-1}, 
				{l+sbWidth/2,	y1+sbEnd}, 
				{l,				y1+sbEnd-sbWidth/2-1}, 
				{l,				y1+sbStart+sbWidth/2}, 
			};
			hNewSliderRgn = CreatePolygonRgn(slp, 6, WINDING);

			POINT slu[] = {
				{l+sbWidth/2,	y2}, 
				{l+sbWidth,		y2-sbWidth/2-1}, 
				{l+sbWidth,		y3-sbWidth/2-1}, 
				{l+sbWidth/2,	y3}, 
				{l,				y3-sbWidth/2-1}, 
				{l,				y2-sbWidth/2-1}, 
			};
			hNewUnderSliderRgn = CreatePolygonRgn(slu, 6, WINDING);

			POINT slo[] = {
				{l+sbWidth/2,	y1-1}, 
				{l+sbWidth,		y1+sbWidth/2+1}, 
				{l+sbWidth,		y1+sbStart+sbWidth/2+1}, 
				{l+sbWidth/2,	y1+sbStart-1}, 
				{l,				y1+sbStart+sbWidth/2}, 
				{l,				y1+sbWidth/2}, 
			};
			hNewOverSliderRgn = CreatePolygonRgn(slo, 6, WINDING);
		}
	}
	else
	{
		if (data.objCount<=maxShownRows)
		{
			POINT sle[] = {
				{l+sbWidth/2-1,	y1}, 
				{l+sbWidth/2,	y1-1}, 
				{l+sbWidth,		y1+sbWidth/2}, 
				{l+sbWidth,		y3-sbWidth/2-1}, 
				{l+sbWidth/2,	y3}, 
				{l+sbWidth/2-1,	y3}, 
				{l,				y3-sbWidth/2}, 
				{l,				y1+sbWidth/2-1}, 
			};
			hNewEmptyRgn = CreatePolygonRgn(sle, 8, WINDING);
		}
		else
		{
			POINT slp[] = {
				{l+sbWidth/2-1,	y1+sbStart}, 
				{l+sbWidth/2,	y1+sbStart-1}, 
				{l+sbWidth,		y1+sbStart+sbWidth/2}, 
				{l+sbWidth,		y1+sbEnd-sbWidth/2-1}, 
				{l+sbWidth/2,	y1+sbEnd}, 
				{l+sbWidth/2-1, y1+sbEnd}, 
				{l,				y1+sbEnd-sbWidth/2}, 
				{l,				y1+sbStart+sbWidth/2-1}, 
			};
			hNewSliderRgn = CreatePolygonRgn(slp, 8, WINDING);

			POINT slu[] = {
				{l+sbWidth/2-1,	y2}, 
				{l+sbWidth/2,	y2}, 
				{l+sbWidth,		y2-sbWidth/2-1}, 
				{l+sbWidth,		y3-sbWidth/2-1}, 
				{l+sbWidth/2,	y3}, 
				{l+sbWidth/2-1,	y3}, 
				{l,				y3-sbWidth/2}, 
				{l,				y2-sbWidth/2}, 
			};
			hNewUnderSliderRgn = CreatePolygonRgn(slu, 8, WINDING);

			POINT slo[] = {
				{l+sbWidth/2-1,	y1}, 
				{l+sbWidth/2,	y1-1}, 
				{l+sbWidth,		y1+sbWidth/2}, 
				{l+sbWidth,		y1+sbStart+sbWidth/2},
				{l+sbWidth/2,	y1+sbStart-1}, 
				{l+sbWidth/2-1,	y1+sbStart}, 
				{l,				y1+sbStart+sbWidth/2-1}, 
				{l,				y1+sbWidth/2-1}, 
			};
			hNewOverSliderRgn = CreatePolygonRgn(slo, 8, WINDING);
		}
	}

	if (hRegSlider)
		DeleteObject(hRegSlider);
	hRegSlider = hNewSliderRgn;

	if (hRegUnderSlider)
		DeleteObject(hRegUnderSlider);
	hRegUnderSlider = hNewUnderSliderRgn;

	if (hRegOverSlider)
		DeleteObject(hRegOverSlider);
	hRegOverSlider = hNewOverSliderRgn;

	if (hRegEmpty)
		DeleteObject(hRegEmpty);
	hRegEmpty = hNewEmptyRgn;
}

//----------------------------------------------------------------------

COLORREF getMixedColor(COLORREF bgCol, unsigned char alpha, COLORREF fgCol)
{
	int ir = (int)GetRValue(fgCol)*(int)alpha + (int)GetRValue(bgCol)*(255-(int)alpha);
	int ig = (int)GetGValue(fgCol)*(int)alpha + (int)GetGValue(bgCol)*(255-(int)alpha);
	int ib = (int)GetBValue(fgCol)*(int)alpha + (int)GetBValue(bgCol)*(255-(int)alpha);
	unsigned char cr = (unsigned char)min(255, max(0, ir/255));
	unsigned char cg = (unsigned char)min(255, max(0, ig/255));
	unsigned char cb = (unsigned char)min(255, max(0, ib/255));
	return RGB(cr, cg, cb);
}

//----------------------------------------------------------------------
