#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "valuesMinMax.h"
#include "raytracer.h"
#include "dialogs.h"

extern HMODULE hDllModule;
void setPositionsOnStartOptionsTab(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcOptions(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnStartOptionsTab(hWnd);


				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_OPT_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = NGCOL_LIGHT_GRAY;
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2GroupBar * emg_textures = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_OPT_GR_TEXTURES));
				emg_textures->bgImage = rMain->hBmpBg;
				emg_textures->setFont(rMain->fonts->em2groupbar, false);

				EM2GroupBar * emg_misc = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_OPT_GR_MISC));
				emg_misc->bgImage = rMain->hBmpBg;
				emg_misc->setFont(rMain->fonts->em2groupbar, false);

				EM2Text * emtAutoreload = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_OPT_TEXT_AUTORELOAD_CHANGED));
				emtAutoreload->bgImage = rMain->hBmpBg;
				emtAutoreload->setFont(rMain->fonts->em2text, false);

				EM2ComboBox * emcbAutoreload = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO));
				emcbAutoreload->setFontMain(rMain->fonts->em2combomain, false);
				emcbAutoreload->setFontUnwrapped(rMain->fonts->em2combounwrap, false);
				emcbAutoreload->bgImage = rMain->hBmpBg;
				emcbAutoreload->maxShownRows = 3;
				emcbAutoreload->rowWidth = 80;
				emcbAutoreload->ellipsis = false;
				emcbAutoreload->moveArrow = true;
				emcbAutoreload->align = EM2ComboBox::ALIGN_LEFT;
				emcbAutoreload->setNumItems(3);
				emcbAutoreload->setItem(0, "NEVER", true);
				emcbAutoreload->setItem(1, "ASK", true);
				emcbAutoreload->setItem(2, "ALWAYS", true);

				EM2Button * embReloadNow = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_OPT_TEX_RELOAD_NOW));
				embReloadNow->setFont(rMain->fonts->em2button, false);
				embReloadNow->bgImage = rMain->hBmpBg;

				EM2CheckBox* emcOpenCL = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_OPT_OPENCL_POST));
				emcOpenCL->setFont(rMain->fonts->em2checkbox, false);
				emcOpenCL->bgImage = rMain->hBmpBg;

				rMain->hTabOptions = hWnd;	// dirty, but below will crash otherwise
				rMain->fillOptionsTabAll();
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_OPT_TEX_RELOAD_NOW:
						{
							CHECK(wmEvent==BN_CLICKED);
							Raytracer::getInstance()->curScenePtr->texManager.checkReloadModifiedTextures(rMain->getHWND(), true);
						}
						break;
					case IDC_REND2_OPT_AUTORELOAD_TEX_COMBO:
						{
							CHECK(wmEvent==CBN_SELCHANGE);
							EM2ComboBox * emc = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO));
							Raytracer * rtr = Raytracer::getInstance();
							rtr->options.texAutoReloadMode = emc->selected;
						}
						break;
					case IDC_REND2_OPT_OPENCL_POST:
						{
							EM2CheckBox * emcOpenCL = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_OPT_OPENCL_POST));
							Raytracer::getInstance()->options.useOpenCLonPost = emcOpenCL->selected;
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelOptions()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_TITLE));
	updateControlBgShift(GetDlgItem(hTabOptions, IDC_REND2_OPT_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgExposure = GetEM2GroupBarInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_GR_TEXTURES));
	updateControlBgShift(GetDlgItem(hTabOptions, IDC_REND2_OPT_GR_TEXTURES), bgShiftXRightPanel, 0, emgExposure->bgShiftX, emgExposure->bgShiftY);
	EM2GroupBar * emgMisc = GetEM2GroupBarInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_GR_MISC));
	updateControlBgShift(GetDlgItem(hTabOptions, IDC_REND2_OPT_GR_MISC), bgShiftXRightPanel, 0, emgMisc->bgShiftX, emgMisc->bgShiftY);

	EM2Text * emtEV = GetEM2TextInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_TEXT_AUTORELOAD_CHANGED));
	updateControlBgShift(GetDlgItem(hTabOptions, IDC_REND2_OPT_TEXT_AUTORELOAD_CHANGED), bgShiftXRightPanel, 0, emtEV->bgShiftX, emtEV->bgShiftY);
	EM2ComboBox * emcbCamResp = GetEM2ComboBoxInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO));
	updateControlBgShift(GetDlgItem(hTabOptions, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO), bgShiftXRightPanel, 0, emcbCamResp->bgShiftX, emcbCamResp->bgShiftY);
	EM2Button * embBrowse = GetEM2ButtonInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_TEX_RELOAD_NOW));
	updateControlBgShift(GetDlgItem(hTabOptions, IDC_REND2_OPT_TEX_RELOAD_NOW), bgShiftXRightPanel, 0, embBrowse->bgShiftX, embBrowse->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartOptionsTab(HWND hWnd)
{
	// srodek panela x=188
	int x, y;
	x = 8;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_OPT_TITLE), HWND_TOP, 130+x,11, 100, 16, 0);

	x=8; y=39;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_OPT_GR_TEXTURES),				HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_OPT_TEXT_AUTORELOAD_CHANGED),	HWND_TOP,	x,		y+30,	135,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO),		HWND_TOP,	x+135,	y+27,	 65,	22,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_OPT_TEX_RELOAD_NOW),			HWND_TOP,	x+260,	y+27,	100,	22,			0);


	x=8; y=99;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_OPT_GR_MISC),					HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_OPT_OPENCL_POST),				HWND_TOP,	x,		y+33,	175,	10,			0);

}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabOptions(bool nowrendering)
{
	setControlEnabled(true, hTabOptions, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO);
	setControlEnabled(!nowrendering, hTabOptions, IDC_REND2_OPT_TEX_RELOAD_NOW);
}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillOptionsTabAll()
{
	Raytracer * rtr = Raytracer::getInstance();

	EM2ComboBox * emc = GetEM2ComboBoxInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_AUTORELOAD_TEX_COMBO));
	emc->selected = rtr->options.texAutoReloadMode;
	InvalidateRect(emc->hwnd, NULL, false);

	EM2CheckBox * emcOpenCL = GetEM2CheckBoxInstance(GetDlgItem(hTabOptions, IDC_REND2_OPT_OPENCL_POST));
	emcOpenCL->selected = rtr->options.useOpenCLonPost  &&  rtr->options.openCLsupported;
	EnableWindow(emcOpenCL->hwnd, rtr->options.openCLsupported ? TRUE : FALSE);
	InvalidateRect(emcOpenCL->hwnd, NULL, false);

	return true;
}

//------------------------------------------------------------------------------------------------
