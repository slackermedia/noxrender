#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include "raytracer.h"
#include "QuasiRandom.h"
#include "PathTracing.h"
#include "Metropolis.h"
#include "log.h"

bool Scene::preRenderInitialize()
{
	return true;
}

void Scene::preEvalFresnels()
{
	int i,j;
	for (i=0; i<mats.objCount; i++)
	{
		MaterialNox * m = mats[i];
		for (j=0; j<m->layers.objCount; j++)
		{
			MatLayer * l = &(m->layers[j]);
			l->fresnel.preEval();
			l->preRenderProcess();
		}
	}
	MaterialNox * m = sscene.defaultMat;
	for (j=0; j<m->layers.objCount; j++)
	{
		MatLayer * l = &(m->layers[j]);
		l->fresnel.preEval();
		l->preRenderProcess();
	}
}


void Scene::copySceneStatic(SceneStatic * src, SceneStatic * dst)
{
	*dst = *src;
	dst->defaultMat = src->defaultMat->copy();

	// copy materials
	dst->mats = new myList<MaterialNox*>;
	for (int i=0; i<src->mats->objCount; i++)
	{
		MaterialNox * t = (*src->mats)[i]->copy();
		void * addr = dst->mats->add(t);
		if (!addr)
		if (!dst->mats->add(t))
			MessageBox(0, "Data corrupted", "", 0);
	}
	dst->mats->createArray();

	// copy LightMeshes
	dst->lightMeshes = new myList<LightMesh *>;
	for (int i=0; i<src->lightMeshes->objCount; i++)
	{
		LightMesh * lm = (*src->lightMeshes)[i]->copy();
		dst->lightMeshes->add(lm);
	}
	dst->lightMeshes->createArray();
}

