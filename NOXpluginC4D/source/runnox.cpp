#include <Windows.h>
#include <tchar.h>
#include "c4d.h"
#include "noxexporter.h"

TCHAR * getNOXDirectory();

//---------------------------------------------------------------------------------------------------------------------

bool NOXexporter::runNox(TCHAR * filename)
{
	TCHAR * ndir = getNOXDirectory();
	if (!ndir)
		return false;

	TCHAR rc[2048];
	TCHAR options[4096];
	#ifdef _WIN64
		_stprintf_s(rc, 2048, _T("%s\\bin\\64\\Renderer64.exe"), ndir);
		_stprintf_s(options, 4096, _T("%s\\bin\\64\\Renderer64.exe -start \"%s\""), ndir, filename);
	#else
		_stprintf_s(rc, 2048, _T("%s\\bin\\32\\Renderer32.exe"), ndir);
		_stprintf_s(options, 4096, _T("%s\\bin\\32\\Renderer32.exe -start \"%s\""), ndir, filename);
	#endif
	free(ndir);

	STARTUPINFO si = {	sizeof(STARTUPINFO),	NULL,	_T(""),	NULL,
				0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0 };

	PROCESS_INFORMATION pinfo;
	bool ok1 = (CreateProcess(rc, options, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS , NULL, NULL, &si, &pinfo) == TRUE);

	return ok1;
}

//---------------------------------------------------------------------------------------------------------------------

TCHAR * getNOXDirectory()
{
	// get default directory
	DWORD rsize = 2048;
	TCHAR buffer[2048];
	LONG regres;
	HKEY key1, key2, key3;
	DWORD type;

	// get renderer key
	regres = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software"), 0, KEY_READ, &key1);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key1, _T("Evermotion"), 0, KEY_READ, &key2);
	if (regres == ERROR_SUCCESS)
		regres = RegOpenKeyEx(key2, _T("Renderer"), 0, KEY_READ, &key3);

	// try to load "directory" value
	if (regres == ERROR_SUCCESS)
	{
		buffer[0] = 0;
		rsize = 2048 * sizeof(TCHAR);
		regres = RegQueryValueEx(key3, _T("directory"), NULL, &type, (BYTE*)buffer, &rsize);
	}

	// close reg keys
	RegCloseKey(key3);
	RegCloseKey(key2);
	RegCloseKey(key1);

	if (regres == ERROR_SUCCESS)
	{	// if OK
	}
	else
	{
		return NULL;
	}

	size_t ss = (_tcsclen(buffer)+1);
	TCHAR * res = (TCHAR *)malloc(ss*sizeof(TCHAR));
	if (res)
		_stprintf_s(res, ss, _T("%s"), buffer);

	return res;
}

//---------------------------------------------------------------------------------------------------------------------
