#include "max2013predef.h"
#include "NOXExporter.h"
#include "NOXMaterial.h"
#include "NOXCamera.h"
#include "NOXSunLight.h"
#include "NOXRenderer.h"
#ifdef PORTABLE_COMPILATION
	#include "noxinclude/BinaryValues.h"
#else
	#include "../../Everything DLL/Everything DLL/BinaryValues.h"
#endif
#define ROUND(a) (int)floor(a+0.5)

#include "noxdebug.h"

void printMatrix(Matrix3 m, char * title);
void logMatrix(Matrix3 m, char * title);
void logPoint(Point3 p, char * title);
void logQuaternion(Quat q, char * title);

#define FWRITE_ULONG8(a)		fwrite((&a), 8, 1, file)
#define FWRITE_UINT(a)			fwrite((&a), 4, 1, file)
#define FWRITE_FLOAT(a)			fwrite((&a), 4, 1, file)
#define FWRITE_USHORT(a) 		fwrite((&a), 2, 1, file)
#define FWRITE_UCHAR(a)			fwrite((&a), 1, 1, file)
#define FWRITE_STRING(a, size)	fwrite((a), (size), 1, file)

//---------------------------------------------------------------------------------------

BinaryScenePlugin::BinaryScenePlugin(bool asRenderer)
{
	ADD_LOG_CONSTR("BinaryScenePlugin Constructor");
	numTris = 0;
	meshinstances.clear();
	meshsources.clear();
	meshes.clear();
	cameras.clear();
	mats.clear();
	matTypes.clear();
	hInfoWindow = NULL;
	overrideMat = NULL;
	overrideMatIsNOX = false;
	exportAndRender = asRenderer;
	prog = NULL;
	sunsky = NULL;
	mb_on = false;
	mb_duration = 0.5f;

	unitScale = (float)GetMasterScale(UNITS_METERS);
}

BinaryScenePlugin::~BinaryScenePlugin()
{
	ADD_LOG_CONSTR("BinaryScenePlugin Destructor");
	meshinstances.clear();
	meshsources.clear();
	meshes.clear();
	cameras.clear();
	mats.clear();
	matTypes.clear();
	sunsky = NULL;
	overrideMat = NULL;
}

void BinaryScenePlugin::resetMe()
{
	ADD_LOG_PARTS_MAIN("BinaryScenePlugin::resetMe");

	meshinstances.clear();
	meshsources.clear();
	meshes.clear();
	cameras.clear();
	mats.clear();
	matTypes.clear();
	sunsky = NULL;
	numTris = 0;
	overrideMat = NULL;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::saveScene(const TCHAR * filename)
{
	ADD_LOG_PARTS_MAIN("BinaryScenePlugin::saveScene");

	if (!filename)
		return false;

	unitScale = (float)GetMasterScale(UNITS_METERS);

	Renderer * rend = GetCOREInterface()->GetRenderer(RS_Production);		
	if (rend->ClassID() == NOXRenderer_CLASS_ID)
	{
		NOXRenderer * noxrend = (NOXRenderer*)rend;
		if (noxrend->overrideMatEnabled  &&  noxrend->overrideMaterial)
		{
			overrideMat = noxrend->overrideMaterial;
			overrideMatIsNOX = (NOXMaterial_CLASS_ID == overrideMat->ClassID()) ? true : false;
		}
		else
			overrideMatIsNOX = false;
		mb_on = noxrend->mb_on;
		mb_duration = noxrend->mb_duration;
	}

	if (cameras.size() < 1)
	{
		int bbb = MessageBox(0, _T("No cameras found. Do you want to continue exporting?"), _T("Warning"),MB_YESNO);
		if (IDNO == bbb)
		{
			ADD_LOG_PARTS_MAIN("BinaryScenePlugin::saveScene aborted - no cameras");
			return false;
		}
	}

    FILE *file;
    file = _tfopen(filename, _T("wb"));
    if (!file)
		return false;

	rGeometrySize = 0;
	rSceneSize = 0;

	unsigned int nox_start_header = NOX_BIN_HEADER;
	if (1!=FWRITE_UINT(nox_start_header))
	{
		MessageBox(0, _T("Writing header error."), _T("Error"), 0);
		return false;
	}

	setProgInfo(_T("Checking cameras..."));

	ValidateCameras vc;
	camerasOK = vc.check(GetCOREInterface()->GetRootNode());

	numDoneTris = 0;

	setProgInfo(_T("Evaluating geometry size..."));

	unsigned long long sceneSize = evalSizeScene() + 16;
	if (1!=FWRITE_ULONG8(sceneSize))
	{
		MessageBox(0, _T("Writing file size error."), _T("Error"), 0);
		return false;
	}
	
	unsigned int noxversion;
	((char*)(&noxversion))[0] = 0;
	((char*)(&noxversion))[1] = (unsigned char)TrRaytracer::getVersionMajor();
	((char*)(&noxversion))[2] = (unsigned char)TrRaytracer::getVersionMinor();
	((char*)(&noxversion))[3] = (unsigned char)TrRaytracer::getVersionBuild();
	if (1!=FWRITE_UINT(noxversion))
	{
		MessageBox(0, _T("Writing NOX version error."), _T("Error"), 0);
		return false;
	}

	if (!insertScenePart(file))
	{
		fclose(file);
		return false;
	}

	fclose(file);

	ADD_LOG_PARTS_MAIN("BinaryScenePlugin::saveScene - file successfully written");

	bool thereIsLight = false;
	// check for sunsky
	if (sunsky)
	{
		bool sunsky_on = false;
		NOXSunLight * sun = NULL;
		ObjectState objSt = sunsky->EvalWorldState(GetCOREInterface()->GetTime());
		if (objSt.obj != NULL)
			sun = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
		if (sun)
			sunsky_on = sun->trsunsky.getSunSkyON();
		if (sunsky_on)
			thereIsLight = true;
		if (sun->trsunsky.getSunON())
			thereIsLight = true;
		if (sun->trsunsky.getEnvEnabled()  &&   sun->trsunsky.getEnvTexFilename())
			thereIsLight = true;

	}
	// check for emissive material
	unsigned int numMats = (unsigned int)mats.size();
	for (unsigned int i=0; i<numMats; i++)
	{
		bool isNOXmat = matTypes[i];
		if (!isNOXmat)
			continue;

		NOXMaterial * nmat = (NOXMaterial*)mats[i];
		int nLayers = nmat->mat.getLayersNumber();
		for (int j=0; j<nLayers; j++)
		{
			int type = nmat->mat.getLayerType(j);
			if (type == 1)
				thereIsLight = true;
		}
	}


	// give a warning
	if (!thereIsLight)
	{
		int bbb = MessageBox(0, _T("Scene exported successfully, however no emitter found.\nDo you want to continue?"), _T("Warning"),MB_YESNO);
		if (IDNO == bbb)
		{
			ADD_LOG_PARTS_MAIN("BinaryScenePlugin::saveScene - aborted at the end - no emitter");
			resetMe();
			return false;
		}
	}

	resetMe();

	ADD_LOG_PARTS_MAIN("BinaryScenePlugin::saveScene done");

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertScenePart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_scene_tag = NOX_BIN_SCENE;
	if (1!=FWRITE_USHORT(nox_scene_tag))
	{
		MessageBox(0, _T("Writing scene tag error."), _T("Error"), 0);
		return false;
	}

	unsigned long long sceneSize = rSceneSize;
	if (sceneSize == 0)
		sceneSize = evalSizeScene();

	if (1!=FWRITE_ULONG8(sceneSize))
	{
		MessageBox(0, _T("Writing scene size error."), _T("Error"), 0);
		return false;
	}

	setProgInfo(_T("Exporting geometry..."));

	if (!insertGeometryPart(file))
	{
		return false;
	}

	for (unsigned int i=0; i<(unsigned int)cameras.size(); i++)
	{
		if (!insertCameraPart(file, i))
		{
			return false;
		}
	}

	setProgInfo(_T("Exporting materials..."));
	if (!insertMaterialsAllPart(file))
	{
		return false;
	}

	setProgInfo(_T("Exporting blend layers..."));
	if (!insertBlendLayersPart(file))
	{
		return false;
	}

	setProgInfo(_T("Exporting User Colors..."));
	if (!insertUserColorsPart(file))
	{
		return false;
	}

	if (sunsky)
	{
		setProgInfo(_T("Exporting sunsky..."));
		if (!insertSunskyPart(file))
		{
			return false;
		}
		setProgInfo(_T("Exporting environment map..."));
		if (!insertEnvironmentPart(file))
		{
			return false;
		}
	}

	setProgInfo(_T("Exporting scene info..."));
	if (!insertSceneInfoPart(file))
	{
		return false;
	}

	setProgInfo(_T("Exporting scene settings..."));
	if (!insertSceneSettingsPart(file))
	{
		return false;
	}

	setProgInfo(_T("Exporting renderer settings..."));
	if (!insertRendererSettingsPart(file))
	{
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;

	unsigned long long matSize = evalSizeMaterials();
	sceneSize += matSize;
	rSceneSize += matSize;

	if (realSize != sceneSize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Scene chunk size is incorrect.\nShould be %llu instead of %llu bytes."), sceneSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	// correct size (with materials)
	// scene first
	_fseeki64(file, (chunkStart+2), SEEK_SET);
	if (1!=FWRITE_ULONG8(sceneSize))
	{
		MessageBox(0, _T("Writing scene size error."), _T("Error"), 0);
		return false;
	}
	// whole file next
	_fseeki64(file, (chunkStart+2), SEEK_SET);
	if (1!=FWRITE_ULONG8(sceneSize))
	{
		MessageBox(0, _T("Writing corrected scene size error."), _T("Error"), 0);
		return false;
	}
	unsigned long long fileSize = sceneSize+16;
	_fseeki64(file, 4, SEEK_SET);
	if (1!=FWRITE_ULONG8(fileSize))
	{
		MessageBox(0, _T("Writing corrected file size error."), _T("Error"), 0);
		return false;
	}
	
	_fseeki64(file, 0, SEEK_END);
	unsigned long long fileEnd = _ftelli64(file);
	if (fileEnd != fileSize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. File size is incorrect.\nShould be %llu instead of %llu bytes."), fileSize, fileEnd);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}


	setProgInfo(_T("Done."));

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertGeometryPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	unsigned int numMeshes = (int)meshes.size();
	unsigned int numInstances = (int)meshinstances.size();
	unsigned int numMeshSources = (int)meshsources.size();
	unsigned int numberTris = numTris;	// numTris count in callback
	unsigned int triSize = (unsigned int)evalSizeTriangle();

	unsigned long long chunkStart = _ftelli64(file);


	unsigned short nox_geom_tag = NOX_BIN_GEOMETRY;
	if (1!=FWRITE_USHORT(nox_geom_tag))
	{
		MessageBox(0, _T("Writing scene tag error."), _T("Error"), 0);
		return false;
	}

	unsigned long long geomSize = rGeometrySize;
	if (geomSize == 0)
		geomSize = evalSizeGeometry();

	if (1!=FWRITE_ULONG8(geomSize))
	{
		MessageBox(0, _T("Writing geometry size error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(numInstances))
	{
		MessageBox(0, _T("Writing number of meshes error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(triSize))
	{
		MessageBox(0, _T("Writing size of triangle error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(numberTris))
	{
		MessageBox(0, _T("Writing total number of triangles error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_scale_tag = NOX_BIN_SCALE_SCENE;
	if (1!=FWRITE_USHORT(nox_scale_tag))
	{
		MessageBox(0, _T("Writing geometry scale tag error."), _T("Error"), 0);
		return false;
	}

	float scale = 1.0f;
	if (1!=FWRITE_FLOAT(scale))
	{
		MessageBox(0, _T("Writing geometry scale error."), _T("Error"), 0);
		return false;
	}

	for (unsigned int i=0; i<numMeshSources; i++)
	{
		if (!insertInstSource(file, i))
		{
			return false;
		}
	}

	for (unsigned int i=0; i<numInstances; i++)
	{
		if (!insertInstance(file, i))
		{
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != geomSize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Geometry chunk size is incorrect.\nShould be %llu instead of %llu bytes."), geomSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertInstance(FILE * file, unsigned int numInst)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numInst < 0   ||   numInst >= (int)meshinstances.size())
	{
		MessageBox(0, _T("Error. Tried to save instance that doesn't exist."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);

	INode * node = meshinstances[numInst].node;
	if (!node)
	{
		MessageBox(0, _T("Error. Instance is NULL node."), _T("Error"), 0);
		return false;
	}

	const TCHAR * name = node->GetName();
	char * name_ansi = name ? getAnsiCopy1(name) : copyStringMax1("Unnamed");
	unsigned int lname = (unsigned int)strlen(name_ansi)+6;

	Matrix3 m1 = node->GetObjTMAfterWSM(GetCOREInterface()->GetTime(), NULL);
	Matrix3 m2 = node->GetObjTMAfterWSM(GetCOREInterface()->GetTime()+GetTicksPerFrame(), NULL);

	// offset matrix
	Matrix3 m_offset(TRUE);
	Point3 offs_pos = node->GetObjOffsetPos();
	m_offset.PreTranslate(offs_pos);
	Quat offs_quat = node->GetObjOffsetRot();
	PreRotateMatrix(m_offset, offs_quat);
	ScaleValue offs_scaleValue = node->GetObjOffsetScale();
	ApplyScaling(m_offset, offs_scaleValue);
	Matrix3 m_offset_inv = m_offset;
	m_offset_inv.Invert();

	bool motion_blur = true;
	if (m1.Equals(m2))
		motion_blur = false;
	Point3 mb_pos = Point3(0.0f,0.0f,0.0f);
	Quat mb_quat;
	mb_quat.Identity();
	if (motion_blur)
	{
		Matrix3 m1i = m1;
		m1i.Invert();

		Matrix3 mdelta = m_offset_inv*m2*m1i*m_offset;
		Quat qdelta = Quat(mdelta);
		if (!qdelta.IsIdentity())
		{
			Point3 tp(qdelta.x, qdelta.y, qdelta.z);
			mb_quat.w = qdelta.w;
			tp = tp.Normalize();
			mb_quat.x = tp.x;  mb_quat.y = tp.y;  mb_quat.z = tp.z;
		}

		Point3 pp = Point3(mb_quat.x, mb_quat.y, mb_quat.z);
		logPoint(pp, "rot vec before m1");
		pp = pp * m1;
		logPoint(pp, "rot vec after m1");
		logMatrix(mdelta, "delta matrix");
		logMatrix(m1, "m1 matrix");

		mb_pos = mdelta.GetRow(3);
		mb_pos *= (float)GetFrameRate();

		// ObjectTM = Offset Scale * Offset Rotation * Offset Position * Controller Scale * Controller Rotation * Controllers Position * Parent Transformation.
	}

	int nummats = 1;
	bool isMultiMat = false;
	Mtl * mmtl = node->GetMtl();
	if (mmtl)
		if (isMultiMat = (TRUE==mmtl->IsMultiMtl()) )	// no == here
			nummats = max(1, mmtl->NumSubMtls());


	unsigned short nox_instance_tag = NOX_BIN_INSTANCE;
	if (1!=FWRITE_USHORT(nox_instance_tag))
	{
		MessageBox(0, _T("Writing instance tag error."), _T("Error"), 0);
		return false;
	}

	unsigned long long instSize = evalSizeInstance(numInst);
	if (1!=FWRITE_ULONG8(instSize))
	{
		MessageBox(0, _T("Writing instance size error."), _T("Error"), 0);
		return false;
	}

	unsigned int instID	= meshinstances[numInst].id;
	if (1!=FWRITE_UINT(instID))
	{
		MessageBox(0, _T("Writing instance ID error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_instname_tag = NOX_BIN_INSTANCE_NAME;
	if (1!=FWRITE_USHORT(nox_instname_tag))
	{
		MessageBox(0, _T("Writing instance name tag error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(lname))
	{
		MessageBox(0, _T("Writing instance name size error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_STRING(name_ansi, (lname-6)))
	{
		MessageBox(0, _T("Writing instance name error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_instmatrix_tag = NOX_BIN_INSTANCE_MATRIX;
	if (1!=FWRITE_USHORT(nox_instmatrix_tag))
	{
		MessageBox(0, _T("Writing instance matrix tag error."), _T("Error"), 0);
		return false;
	}
	
	Point4 p1 = m1.GetColumn(0);
	Point4 p2 = m1.GetColumn(1);
	Point4 p3 = m1.GetColumn(2);
	Point4 p4 = Point4(0.0f, 0.0f, 0.0f, 1.0f);
	p1[3] *= unitScale;
	p2[3] *= unitScale;
	p3[3] *= unitScale;
	float mm[] = {
		 p1[0],  p1[2], -p1[1],  p1[3], 
		 p3[0],  p3[2], -p3[1],  p3[3], 
		-p2[0], -p2[2],  p2[1], -p2[3], 
		 p4[0],  p4[2], -p4[1],  p4[3]};
	for (int i=0; i<16; i++)
	{
		float v = mm[i];
		if (1!=FWRITE_FLOAT(v))
		{
			MessageBox(0, _T("Writing instance matrix error."), _T("Error"), 0);
			return false;
		}
	}

	unsigned short nox_instmats_tag = NOX_BIN_INSTANCE_MATERIALS;
	if (1!=FWRITE_USHORT(nox_instmats_tag))
	{
		MessageBox(0, _T("Writing instance materials tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short nmats = nummats;
	if (1!=FWRITE_USHORT(nmats))
	{
		MessageBox(0, _T("Writing instance materials count error."), _T("Error"), 0);
		return false;
	}
	if (isMultiMat)
	{
		if (mmtl->NumSubMtls()==0)
		{
			unsigned int matID;
			if (overrideMat)
				matID = addMat(overrideMat, overrideMatIsNOX);
			else
				matID = addMat(NULL, false);
			if (1!=FWRITE_UINT(matID))
			{
				MessageBox(0, _T("Writing instance material error."), _T("Error"), 0);
				return false;
			}
		}
		else
		{
			for (int i=0; i<nummats; i++)
			{
				Mtl * submtl = mmtl->GetSubMtl(i);
				bool mattype = (NOXMaterial_CLASS_ID == submtl->ClassID()) ? true : false;
				unsigned int matID;// = addMat(submtl, mattype);

				if (overrideMat)
				{
					if (mattype)
					{
						NOXMaterial * noxmat = dynamic_cast<NOXMaterial*>(submtl);
						if (noxmat  &&  noxmat->isNOXEmitter())
							matID = addMat(submtl, mattype);
						else
							matID = addMat(overrideMat, overrideMatIsNOX);
					}
					else
						matID = addMat(overrideMat, overrideMatIsNOX);
				}
				else
					matID = addMat(submtl, mattype);

				if (1!=FWRITE_UINT(matID))
				{
					MessageBox(0, _T("Writing instance material error."), _T("Error"), 0);
					return false;
				}
			}
		}
	}
	else
	{
		unsigned int matID = 0;
		if (mmtl)
		{
			bool mattype = (NOXMaterial_CLASS_ID == mmtl->ClassID()) ? true : false;
			if (mattype)
			{
				if (overrideMat)
				{
					NOXMaterial * noxmat = dynamic_cast<NOXMaterial*>(mmtl);
					if (noxmat  &&  noxmat->isNOXEmitter())
						matID = addMat(mmtl, mattype);
					else
						matID = addMat(overrideMat, overrideMatIsNOX);
				}
				else
					matID = addMat(mmtl, mattype);
			}
			else
				if (overrideMat)
					matID = addMat(overrideMat, overrideMatIsNOX);
				else
					matID = addMat(mmtl, mattype);
		}
		else
		{
			if (overrideMat)
				matID = addMat(overrideMat, overrideMatIsNOX);
			else
				matID = addMat(NULL, false);
		}

		if (1!=FWRITE_UINT(matID))
		{
			MessageBox(0, _T("Writing instance material error."), _T("Error"), 0);
			return false;
		}
	}

	unsigned short nox_motion_blur_tag = NOX_BIN_INSTANCE_MOTION_BLUR;
	if (1!=FWRITE_USHORT(nox_motion_blur_tag))
	{
		MessageBox(0, _T("Writing instance motion blur tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short mb_on = motion_blur ? 1 : 0;
	if (1!=FWRITE_USHORT(mb_on))
	{
		MessageBox(0, _T("Writing instance motion blur error."), _T("Error"), 0);
		return false;
	}

	mb_pos *= unitScale;
	unsigned short nox_mb_pos_tag = NOX_BIN_INSTANCE_MB_POS;
	if (1!=FWRITE_USHORT(nox_mb_pos_tag))
	{
		MessageBox(0, _T("Writing instance motion blur pos tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_pos.x))
	{
		MessageBox(0, _T("Writing instance motion blur pos error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_pos.z))
	{
		MessageBox(0, _T("Writing instance motion blur pos error."), _T("Error"), 0);
		return false;
	}
	float pminusy = -mb_pos.y;
	if (1!=FWRITE_FLOAT(pminusy))
	{
		MessageBox(0, _T("Writing instance motion blur pos error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mb_angle_tag = NOX_BIN_INSTANCE_MB_ANGLE;
	if (1!=FWRITE_USHORT(nox_mb_angle_tag))
	{
		MessageBox(0, _T("Writing instance motion blur angle tag error."), _T("Error"), 0);
		return false;
	}
	float mb_a = acos(mb_quat.w)*-2 * GetFrameRate();
	if (1!=FWRITE_FLOAT(mb_a))
	{
		MessageBox(0, _T("Writing instance motion blur angle error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_quat.x))
	{
		MessageBox(0, _T("Writing instance motion blur angle error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_quat.z))
	{
		MessageBox(0, _T("Writing instance motion blur angle error."), _T("Error"), 0);
		return false;
	}
	float rminusy = -mb_quat.y;
	if (1!=FWRITE_FLOAT(rminusy))
	{
		MessageBox(0, _T("Writing instance motion blur angle error."), _T("Error"), 0);
		return false;
	}



	unsigned short nox_inst_offset_matrix_tag = NOX_BIN_INSTANCE_OFFSET_MATRIX;
	if (1!=FWRITE_USHORT(nox_inst_offset_matrix_tag))
	{
		MessageBox(0, _T("Writing instance offset matrix tag error."), _T("Error"), 0);
		return false;
	}
	Point4 p1o = m_offset.GetColumn(0);
	Point4 p2o = m_offset.GetColumn(1);
	Point4 p3o = m_offset.GetColumn(2);
	Point4 p4o = Point4(0.0f, 0.0f, 0.0f, 1.0f);
	p1o[3] *= unitScale;
	p2o[3] *= unitScale;
	p3o[3] *= unitScale;
	float mmo[] = {
		 p1o[0],  p1o[2], -p1o[1],  p1o[3], 
		 p3o[0],  p3o[2], -p3o[1],  p3o[3], 
		-p2o[0], -p2o[2],  p2o[1], -p2o[3], 
		 p4o[0],  p4o[2], -p4o[1],  p4o[3]};
	for (int i=0; i<16; i++)
	{
		float v = mmo[i];
		if (1!=FWRITE_FLOAT(v))
		{
			MessageBox(0, _T("Writing instance offset matrix error."), _T("Error"), 0);
			return false;
		}
	}


	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != instSize)
	{
		TCHAR errmsg[512];
		_stprintf_s(errmsg, 512, _T("Export error. Instance chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n\n%s"), instSize, realSize, name);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	free(name_ansi);

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertInstSource(FILE * file, unsigned int numSrc)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numSrc < 0   ||   numSrc >= (int)meshsources.size())
	{
		MessageBox(0, _T("Error. Tried to save instance source that doesn't exist."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);

	INode * node = meshsources[numSrc].node;
	if (!node)
	{
		MessageBox(0, _T("Error. Instance source is NULL node."), _T("Error"), 0);
		return false;
	}


	unsigned short nox_instance_src_tag = NOX_BIN_INSTANCE_SOURCE;
	if (1!=FWRITE_USHORT(nox_instance_src_tag))
	{
		MessageBox(0, _T("Writing instance source tag error."), _T("Error"), 0);
		return false;
	}

	unsigned long long instSize = evalSizeInstSrc(numSrc);
	if (1!=FWRITE_ULONG8(instSize))
	{
		MessageBox(0, _T("Writing instance source size error."), _T("Error"), 0);
		return false;
	}

	unsigned int instID	= numSrc;
	if (1!=FWRITE_UINT(instID))
	{
		MessageBox(0, _T("Writing instance source ID error."), _T("Error"), 0);
		return false;
	}

	if (!insertMeshSrcPart(file, numSrc))
		return false;

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != instSize)
	{
		TCHAR errmsg[512];
		_stprintf_s(errmsg, 512, _T("Export error. Instance Source chunk size is incorrect.\nShould be %llu instead of %llu bytes."), instSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMeshPart(FILE * file, unsigned int numMesh)	// DEPRECATED
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numMesh < 0   ||   numMesh >= (int)meshes.size())
	{
		MessageBox(0, _T("Error. Tried to save mesh that doesn't exist."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);

	INode * node = meshes[numMesh];
	if (!node)
	{
		MessageBox(0, _T("Error. Mesh is NULL node."), _T("Error"), 0);
		return false;
	}

	Matrix3 m1 = node->GetObjTMAfterWSM(GetCOREInterface()->GetTime(), NULL);

	const TCHAR * name = node->GetName();
	char * name_ansi = name ? getAnsiCopy1(name) : copyStringMax1("Unnamed");
	unsigned int lname = (unsigned int)strlen(name_ansi)+6;

	TCHAR progressInfoMesh[512];
	_stprintf_s(progressInfoMesh, 512, _T("Adding mesh %s"), name);
	setProgInfo(progressInfoMesh);



	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
	{
		MessageBox(0, _T("Error (1) while extracting mesh."), _T("Error"), 0);
		return false;
	}


	TriObject * tobj = (TriObject *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), triObjectClassID);
	if (!tobj)
	{
		MessageBox(0, _T("Error (2) while extracting mesh."), _T("Error"), 0);
		return false;
	}

	{
		char buf[256];
		const TCHAR * name = node->GetName();
		char * name_ansi = name ? getAnsiCopy1(name) : copyStringMax1("Unnamed");
		const TCHAR * rname =  node->GetObjectRef()->GetObjectName();
		char * rname_ansi = rname ? getAnsiCopy1(rname) : copyStringMax1("Unnamed");
		
		sprintf_s(buf, 256, "adding %s (%s) at %lld   obj ref %lld    wsmref: %lld   tobj: %lld", name_ansi, rname_ansi,
						(INT_PTR)node, (INT_PTR)node->GetObjectRef(), (INT_PTR)node->GetObjOrWSMRef(),  (INT_PTR)tobj);
		TrRaytracer::addLog(buf);
		free(name_ansi);
		free(rname_ansi);
	}

	if (!_tcscmp(name, _T("MNode")))
	{
		MSTR clsname;
		node->GetClassName(clsname);

		Class_ID mnCID = node->ClassID();
		ULONG la = mnCID.PartA();
		ULONG lb = mnCID.PartB();

		SClass_ID scID = node->SuperClassID();

		char * mnRend = node->Renderable() ? "yes" : "no";
		char * mnIsObjHidden = node->IsObjectHidden () ? "yes" : "no";
		char * mnIsHidden = node->IsHidden() ? "yes" : "no";
		char * mnIsNodeHidden = node->IsNodeHidden() ? "yes" : "no";

		TCHAR loggg[512];
		_stprintf_s(loggg, 512, _T("MNode object\nClass ID: %x %x\nSClassID: %x\nClass name: %s\nRenderable: %s\nIsObjectHidden: %s\nIsHidden: %s\nIsNodeHidden: %s"), 
									la, lb,  scID, clsname.data(), mnRend, mnIsObjHidden, mnIsHidden, mnIsNodeHidden);
		MessageBox(0, loggg, _T(""), 0);
	}


	Mesh mesh = tobj->GetMesh();
	unsigned int numFaces = mesh.numFaces;

	unsigned short nox_mesh_tag = NOX_BIN_MESH;
	if (1!=FWRITE_USHORT(nox_mesh_tag))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh tag error."), _T("Error"), 0);
		return false;
	}

	unsigned long long meshSize = evalSizeMesh(numMesh);
	if (1!=FWRITE_ULONG8(meshSize))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh size error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(numFaces))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing number of triangles in mesh error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_meshname_tag = NOX_BIN_MESHNAME;
	if (1!=FWRITE_USHORT(nox_meshname_tag))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh name tag error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(lname))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh name size error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_STRING(name_ansi, (lname-6)))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh name error."), _T("Error"), 0);
		return false;
	}

	// TRIANGLES FROM HERE ---------------------------

	Point3 * normalsArray;
	MNormal * normals;
	normals = ComputeMyNormals(&mesh, &normalsArray, m1);

	Mtl * mmtl = node->GetMtl();
	bool mattype;


	for (unsigned int i=0; i<numFaces; i++)
	{
		int matID = 0;
		if (mmtl)
		{
			if (mmtl->IsMultiMtl())
			{
				int mod = mmtl->NumSubMtls();
				if (mod == 0)
				{
					mod = 1;
					MessageBox(0, _T("Empty multimaterial. Export may fail."),_T(""),0);
					mattype = false;
					matID = addMat(0, mattype);
				}
				else
				{
					int intID = mesh.getFaceMtlIndex(i) % mod;
					Mtl * submtl = mmtl->GetSubMtl(intID);
					mattype = (NOXMaterial_CLASS_ID == submtl->ClassID()) ? true : false;
					matID = addMat(submtl, mattype);

					char buf[256];
					sprintf_s(buf, 256, "mm   %d", mesh.getFaceMtlIndex(i));
					TrRaytracer::addLog(buf);

				}
			}
			else
			{
				mattype = (mmtl->ClassID() == NOXMaterial_CLASS_ID) ? true : false;
				matID = addMat(mmtl, mattype);

				char buf[256];
				sprintf_s(buf, 256, "nmm  %d", mesh.getFaceMtlIndex(i));
				TrRaytracer::addLog(buf);

			}
		}
		else
		{
			mattype = false;
			matID = addMat(mmtl, mattype);

			char buf[256];
			sprintf_s(buf, 256, "null %d", mesh.getFaceMtlIndex(i));
			TrRaytracer::addLog(buf);
		}

		// TRIANGLE 

		numDoneTris++;

		Point3 v1,v2,v3;
		Point3 n1,n2,n3;
		Point3 uv1,uv2,uv3;

		Matrix3 nmatr = m1;
		nmatr.SetRow(3, Point3(0,0,0));

		// vertices
		if (mesh.faces[i].Direction(mesh.faces[i].getVert(0), mesh.faces[i].getVert(1))<0)
		{
			v3 = mesh.verts[mesh.faces[i].getVert(0)]*m1;
			v2 = mesh.verts[mesh.faces[i].getVert(1)]*m1;
			v1 = mesh.verts[mesh.faces[i].getVert(2)]*m1;
		}
		else
		{
			v1 = mesh.verts[mesh.faces[i].getVert(0)]*m1;
			v2 = mesh.verts[mesh.faces[i].getVert(1)]*m1;
			v3 = mesh.verts[mesh.faces[i].getVert(2)]*m1;
		}

		Point3 normalna = (v2-v1)^(v3-v2);
		v1 *= unitScale;
		v2 *= unitScale;
		v3 *= unitScale;

		// normals
		DWORD smoothgr = mesh.faces[i].smGroup;
		if (smoothgr == 0)
		{
			n1 = (v2-v1)^(v3-v2);
			n1 = -n1.Normalize();
			n2=n3=n1;
		}
		else
		{
			if (mesh.faces[i].Direction(mesh.faces[i].getVert(0), mesh.faces[i].getVert(1))<0)
			{
				n3 = (normals[mesh.faces[i].getVert(0)].GetNormal(smoothgr));
				n2 = (normals[mesh.faces[i].getVert(1)].GetNormal(smoothgr));
				n1 = (normals[mesh.faces[i].getVert(2)].GetNormal(smoothgr));
			}
			else
			{
				n1 = (normals[mesh.faces[i].getVert(0)].GetNormal(smoothgr));
				n2 = (normals[mesh.faces[i].getVert(1)].GetNormal(smoothgr));
				n3 = (normals[mesh.faces[i].getVert(2)].GetNormal(smoothgr));
			}
			n1 = -n1;
			n2 = -n2;
			n3 = -n3;
		}



		#ifdef NOX_SWAP_YZ
			float t;
			t    =  v1.z;
			v1.z = -v1.y;
			v1.y =  t;
			t    =  v2.z;
			v2.z = -v2.y;
			v2.y =  t;
			t    =  v3.z;
			v3.z = -v3.y;
			v3.y =  t;
			t    =  n1.z;
			n1.z = -n1.y;
			n1.y =  t;
			t    =  n2.z;
			n2.z = -n2.y;
			n2.y =  t;
			t    =  n3.z;
			n3.z = -n3.y;
			n3.y =  t;
		#endif

	
		// UV
		if (mesh.tvFace)
		{
			if (mesh.tvFace[i].Direction(mesh.tvFace[i].getTVert(0), mesh.tvFace[i].getTVert(1))<0)
			{
				uv3 = mesh.tVerts[mesh.tvFace[i].getTVert(0)];
				uv2 = mesh.tVerts[mesh.tvFace[i].getTVert(1)];
				uv1 = mesh.tVerts[mesh.tvFace[i].getTVert(2)];
			}
			else
			{
				uv1 = mesh.tVerts[mesh.tvFace[i].getTVert(0)];
				uv2 = mesh.tVerts[mesh.tvFace[i].getTVert(1)];
				uv3 = mesh.tVerts[mesh.tvFace[i].getTVert(2)];
			}
		}
		else
		{
			uv1 = Point3(1,1,1);
			uv2 = Point3(1,1,1);
			uv3 = Point3(1,1,1);
		}

		n1 = -n1;
		n2 = -n2;
		n3 = -n3;

		// add to file
		unsigned short nox_triangle_tag = NOX_BIN_TRIANGLE;
		if (1!=FWRITE_USHORT(nox_triangle_tag))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle tag error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_UINT(matID))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v1.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v1.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v1.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}
		
		if (1!=FWRITE_FLOAT(v2.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v2.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v2.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v3.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v3.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v3.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n1.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n1.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n1.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}
		
		if (1!=FWRITE_FLOAT(n2.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n2.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n2.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n3.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n3.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n3.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv1.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv1.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}
		
		if (1!=FWRITE_FLOAT(uv2.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv2.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv3.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv3.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

	}

	for (int i=0; i<mesh.getNumVerts(); i++)
		normals[i].DisposeAll();
	free(normals);
	normals = NULL;
	delete normalsArray;
	normalsArray = NULL;


	deleteSecondIfCopied(objSt.obj, tobj);

	int a = (int)(numDoneTris/(float)numTris*1000);
	setProgress(a);

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != meshSize)
	{
		TCHAR errmsg[512];
		_stprintf_s(errmsg, 512, _T("Export error. Mesh chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n\n%s"), meshSize, realSize, name);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMeshSrcPart(FILE * file, unsigned int numSrc)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numSrc< 0   ||   numSrc>= (int)meshsources.size())
	{
		MessageBox(0, _T("Error. Tried to save source mesh that doesn't exist."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStart = _ftelli64(file);

	INode * node = meshsources[numSrc].node;
	if (!node)
	{
		MessageBox(0, _T("Error. Mesh source is NULL node."), _T("Error"), 0);
		return false;
	}

	const TCHAR * name = node->GetObjectRef()->GetObjectName();
	char * name_ansi = name ? getAnsiCopy1(name) : copyStringMax1("Unnamed");
	unsigned int lname = (unsigned int)strlen(name_ansi)+6;

	TCHAR progressInfoMesh[512];
	_stprintf_s(progressInfoMesh, 512, _T("Adding mesh %s"), name);
	setProgInfo(progressInfoMesh);



	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
	{
		MessageBox(0, _T("Error (1) while extracting mesh."), _T("Error"), 0);
		return false;
	}


	TriObject * tobj = (TriObject *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), triObjectClassID);
	if (!tobj)
	{
		MessageBox(0, _T("Error (2) while extracting mesh."), _T("Error"), 0);
		return false;
	}

	{
		char buf[256];
		const TCHAR * name = node->GetName();
		const TCHAR * rname = node->GetObjectRef()->GetObjectName();
		char * name_ansi = name ? getAnsiCopy1(name) : copyStringMax1("Unnamed");
		char * rname_ansi = name ? getAnsiCopy1(rname) : copyStringMax1("Unnamed");
		
		sprintf_s(buf, 256, "adding %s (%s) at %lld   obj ref %lld    wsmref: %lld   tobj: %lld", name_ansi, rname_ansi,
						(INT_PTR)node, (INT_PTR)node->GetObjectRef(), (INT_PTR)node->GetObjOrWSMRef(),  (INT_PTR)tobj);
		TrRaytracer::addLog(buf);
		free(name_ansi);
		free(rname_ansi);
	}


	if (!_tcscmp(name, _T("MNode")))
	{
		MSTR clsname;
		node->GetClassName(clsname);

		Class_ID mnCID = node->ClassID();
		ULONG la = mnCID.PartA();
		ULONG lb = mnCID.PartB();

		SClass_ID scID = node->SuperClassID();

		TCHAR * mnRend = node->Renderable() ? _T("yes") : _T("no");
		TCHAR * mnIsObjHidden = node->IsObjectHidden () ? _T("yes") : _T("no");
		TCHAR * mnIsHidden = node->IsHidden() ? _T("yes") : _T("no");
		TCHAR * mnIsNodeHidden = node->IsNodeHidden() ? _T("yes") : _T("no");

		TCHAR loggg[512];
		_stprintf_s(loggg, 512, _T("MNode object\nClass ID: %x %x\nSClassID: %x\nClass name: %s\nRenderable: %s\nIsObjectHidden: %s\nIsHidden: %s\nIsNodeHidden: %s"), 
									la, lb,  scID, clsname.data(), mnRend, mnIsObjHidden, mnIsHidden, mnIsNodeHidden);
		MessageBox(0, loggg, _T(""), 0);
	}


	Mesh mesh = tobj->GetMesh();
	unsigned int numFaces = mesh.numFaces;

	unsigned short nox_mesh_tag = NOX_BIN_MESH;
	if (1!=FWRITE_USHORT(nox_mesh_tag))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh tag error."), _T("Error"), 0);
		return false;
	}

	unsigned long long meshSize = evalSizeMeshSrc(numSrc);
	if (1!=FWRITE_ULONG8(meshSize))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh size error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(numFaces))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing number of triangles in mesh error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_meshname_tag = NOX_BIN_MESHNAME;
	if (1!=FWRITE_USHORT(nox_meshname_tag))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh name tag error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(lname))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh name size error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_STRING(name_ansi, (lname-6)))
	{
		deleteSecondIfCopied(objSt.obj, tobj);
		MessageBox(0, _T("Writing mesh name error."), _T("Error"), 0);
		return false;
	}

	free(name_ansi);

	// TRIANGLES FROM HERE ---------------------------

	Point3 * normalsArray;
	MNormal * normals;
	Matrix3 m2;
	m2.IdentityMatrix();
	normals = ComputeMyNormals(&mesh, &normalsArray, m2);//m1);

	Mtl * mmtl = node->GetMtl();
	bool mattype;

	for (unsigned int i=0; i<numFaces; i++)
	{
		int matID = 0;
		int matInInst = 0;
		if (mmtl)
		{
			if (mmtl->IsMultiMtl())
			{
				int mod = mmtl->NumSubMtls();
				if (mod == 0)
				{
					mod = 1;
					MessageBox(0, _T("Empty multimaterial. Export may fail."), _T(""),0);
					mattype = false;
					if (overrideMat)
						matID = addMat(overrideMat, overrideMatIsNOX);
					else
						matID = addMat(0, mattype);
				}
				else
				{
					int intID = mesh.getFaceMtlIndex(i) % mod;
					matInInst = intID;
					Mtl * submtl = mmtl->GetSubMtl(intID);
					mattype = (NOXMaterial_CLASS_ID == submtl->ClassID()) ? true : false;

					if (mattype)
					{
						NOXMaterial * noxmat = dynamic_cast<NOXMaterial*>(submtl);
						if (noxmat  &&  noxmat->isNOXEmitter())
							matID = addMat(submtl, mattype);
						else
							matID = addMat(overrideMat, overrideMatIsNOX);
					}
					else
						matID = addMat(overrideMat, overrideMatIsNOX);
				}
			}
			else
			{
				mattype = (mmtl->ClassID() == NOXMaterial_CLASS_ID) ? true : false;
				if (mattype)
				{
					if (overrideMat)
					{
						NOXMaterial * noxmat = dynamic_cast<NOXMaterial*>(mmtl);
						if (noxmat  &&  noxmat->isNOXEmitter())
							matID = addMat(mmtl, mattype);
						else
							matID = addMat(overrideMat, overrideMatIsNOX);
					}
					else
						matID = addMat(mmtl, mattype);
				}
				else
					if (overrideMat)
						matID = addMat(overrideMat, overrideMatIsNOX);
					else
						matID = addMat(mmtl, mattype);
			}
		}
		else
		{
			if (overrideMat)
				matID = addMat(overrideMat, overrideMatIsNOX);
			else
				matID = addMat(NULL, false);
		}

		// TRIANGLE 

		numDoneTris++;

		Point3 v1,v2,v3;
		Point3 n1,n2,n3;
		Point3 uv1,uv2,uv3;


		// vertices
		if (mesh.faces[i].Direction(mesh.faces[i].getVert(0), mesh.faces[i].getVert(1))<0)
		{
			v3 = mesh.verts[mesh.faces[i].getVert(0)];//*m1;
			v2 = mesh.verts[mesh.faces[i].getVert(1)];//*m1;
			v1 = mesh.verts[mesh.faces[i].getVert(2)];//*m1;
		}
		else
		{
			v1 = mesh.verts[mesh.faces[i].getVert(0)];//*m1;
			v2 = mesh.verts[mesh.faces[i].getVert(1)];//*m1;
			v3 = mesh.verts[mesh.faces[i].getVert(2)];//*m1;
		}

		Point3 normalna = (v2-v1)^(v3-v2);

		v1 *= unitScale;
		v2 *= unitScale;
		v3 *= unitScale;

		// normals
		DWORD smoothgr = mesh.faces[i].smGroup;
		if (smoothgr == 0)
		{
			n1 = (v2-v1)^(v3-v2);
			n1 = -n1.Normalize();
			n2=n3=n1;
		}
		else
		{
			if (mesh.faces[i].Direction(mesh.faces[i].getVert(0), mesh.faces[i].getVert(1))<0)
			{
				n3 = (normals[mesh.faces[i].getVert(0)].GetNormal(smoothgr));
				n2 = (normals[mesh.faces[i].getVert(1)].GetNormal(smoothgr));
				n1 = (normals[mesh.faces[i].getVert(2)].GetNormal(smoothgr));
			}
			else
			{
				n1 = (normals[mesh.faces[i].getVert(0)].GetNormal(smoothgr));
				n2 = (normals[mesh.faces[i].getVert(1)].GetNormal(smoothgr));
				n3 = (normals[mesh.faces[i].getVert(2)].GetNormal(smoothgr));
			}
			n1 = -n1;
			n2 = -n2;
			n3 = -n3;
		}



		#ifdef NOX_SWAP_YZ
			float t;
			t    =  v1.z;
			v1.z = -v1.y;
			v1.y =  t;
			t    =  v2.z;
			v2.z = -v2.y;
			v2.y =  t;
			t    =  v3.z;
			v3.z = -v3.y;
			v3.y =  t;
			t    =  n1.z;
			n1.z = -n1.y;
			n1.y =  t;
			t    =  n2.z;
			n2.z = -n2.y;
			n2.y =  t;
			t    =  n3.z;
			n3.z = -n3.y;
			n3.y =  t;
		#endif

	
		// UV
		if (mesh.tvFace)
		{
			if (mesh.tvFace[i].Direction(mesh.tvFace[i].getTVert(0), mesh.tvFace[i].getTVert(1))<0)
			{
				uv3 = mesh.tVerts[mesh.tvFace[i].getTVert(0)];
				uv2 = mesh.tVerts[mesh.tvFace[i].getTVert(1)];
				uv1 = mesh.tVerts[mesh.tvFace[i].getTVert(2)];
			}
			else
			{
				uv1 = mesh.tVerts[mesh.tvFace[i].getTVert(0)];
				uv2 = mesh.tVerts[mesh.tvFace[i].getTVert(1)];
				uv3 = mesh.tVerts[mesh.tvFace[i].getTVert(2)];
			}
		}
		else
		{
			uv1 = Point3(1,1,1);
			uv2 = Point3(1,1,1);
			uv3 = Point3(1,1,1);
		}

		n1 = -n1;
		n2 = -n2;
		n3 = -n3;

		// add to file
		unsigned short nox_triangle_tag = NOX_BIN_TRIANGLE;
		if (1!=FWRITE_USHORT(nox_triangle_tag))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle tag error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_UINT(matInInst))	// mat in inst
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(matID))	// mat in whole lib - deprecated
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v1.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v1.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v1.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}
		
		if (1!=FWRITE_FLOAT(v2.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v2.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v2.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v3.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v3.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(v3.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n1.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n1.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n1.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}
		
		if (1!=FWRITE_FLOAT(n2.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n2.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n2.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n3.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n3.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(n3.z))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv1.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv1.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}
		
		if (1!=FWRITE_FLOAT(uv2.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv2.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv3.x))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

		if (1!=FWRITE_FLOAT(uv3.y))
		{
			deleteSecondIfCopied(objSt.obj, tobj);
			MessageBox(0, _T("Writing triangle error."), _T("Error"), 0);
			return false;
		}

	}

	for (int i=0; i<mesh.getNumVerts(); i++)
		normals[i].DisposeAll();
	free(normals);
	normals = NULL;
	delete normalsArray;
	normalsArray = NULL;


	deleteSecondIfCopied(objSt.obj, tobj);

	int a = (int)(numDoneTris/(float)numTris*1000);
	setProgress(a);

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != meshSize)
	{
		TCHAR errmsg[512];
		_stprintf_s(errmsg, 512, _T("Export error. Mesh chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n\n%s"), meshSize, realSize, name);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}


	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertTrianglePart(FILE * file, unsigned int numMesh, unsigned int numTri)
{
	/// NOT USED ANYMORE
	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertCameraPart(FILE * file, unsigned int numCam)
{
	unsigned int width;
	unsigned int height;
	unsigned short aa = NOX_CAMERA_DEFAULT_AA;
	float posx, posy, posz;
	float dirx, diry, dirz;
	float crx, cry, crz;
	float upx, upy, upz;
	float angle;
	float iso = 100;
	float aperture = 8;
	float shutter = 50;
	bool autoexposure = false;
	bool autofocus = false;
	unsigned short autoexp_type = 0;
	unsigned short bladesnumber = 15;
	float bladesangle = 0;
	bool bladesround = false;
	float bladesradius = 2;
	unsigned short bokehRingSize = 20;
	unsigned short bokehFlatten = 0;
	short bokehRingBalance = 0;
	short bokehVignette = 0;
	unsigned short dofON = 1;
	float focusdist = 2;

	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numCam < 0   ||   numCam >= (unsigned int)cameras.size())
	{
		MessageBox(0, _T("Error. Tried to save camera that doesn't exist."), _T("Error"), 0);
		return false;
	}



	unsigned long long camerasize = evalSizeCamera(numCam);

	unsigned long long chunkStart = _ftelli64(file);

	INode * node = cameras[numCam];
	if (!node)
	{
		MessageBox(0, _T("Error. Camera is NULL node."), _T("Error"), 0);
		return false;
	}

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
	{
		MessageBox(0, _T("Error (1) while extracting camera."), _T("Error"), 0);
		return false;
	}

	CameraState cs;
	CameraObject *cam = (CameraObject *)objSt.obj;
	Interval iv;
	iv.SetInfinite();
	cam->EvalCameraState(GetCOREInterface()->GetTime(),iv,&cs);

	Matrix3 cammat = (node->GetObjTMAfterWSM(GetCOREInterface()->GetTime()));
	
	Matrix3 m2 = node->GetObjTMAfterWSM(GetCOREInterface()->GetTime()+GetTicksPerFrame(), NULL);
	bool motion_blur = true;
	if (cammat.Equals(m2))
		motion_blur = false;
	Point3 mb_pos = Point3(0.0f,0.0f,0.0f);
	Quat mb_quat;
	mb_quat.Identity();
	if (motion_blur)
	{
		Matrix3 m1i = cammat;
		Point3 moveOffset = node->GetObjOffsetPos();
		Quat rotOffset = node->GetObjOffsetRot();

		m1i.Invert();
		Matrix3 mdelta = m2*m1i;
		Quat qdelta = Quat(mdelta);
		if (!qdelta.IsIdentity())
		{
			Point3 tp(qdelta.x, qdelta.y, qdelta.z);
			mb_quat.w = qdelta.w;
			tp = tp.Normalize();
			mb_quat.x = tp.x;  mb_quat.y = tp.y;  mb_quat.z = tp.z;
		}

		mb_pos = mdelta.GetRow(3);		// <------------
		mb_pos *= (float)GetFrameRate();

		//logPoint(mb_pos, "cam mb pos");
		//logQuaternion(mb_quat, "cam mb quat");
	}
	
	Point4 r1 = cammat.GetRow(0);
	Point4 r2 = cammat.GetRow(1);
	Point4 r3 = cammat.GetRow(2);
	Point4 r4 = cammat.GetRow(3);

	r4 *= unitScale;

	posx =  r4.x;
	dirx = -r3.x;
	upx  =  r2.x;
	crx  = r1.x;

	#ifdef NOX_SWAP_YZ
		posy =  r4.z;
		posz = -r4.y;
		diry = -r3.z;
		dirz =  r3.y;
		upy  =  r2.z;
		upz  = -r2.y;
		cry  =  r1.z;
		crz  = -r1.y;
	#else
		posy =  r4.y;
		posz =  r4.z;
		diry = -r3.y;
		dirz = -r3.z;
		upy  =  r2.y;
		upz  =  r2.z;
		cry  =  r1.y;
		crz  =  r1.z;
	#endif
	
	angle = cs.fov * 180 / PI;


	bool isActive;
	char * activeText = NULL;
	if (!camerasOK)
		isActive = true;
	else
	{
		if (objSt.obj->CanConvertToType(NOXCamera_CLASS_ID))
		{
			NOXCamera * tcam = (NOXCamera *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXCamera_CLASS_ID);
			if (!tcam)
				isActive = false;
			else
			{
				if (tcam->trCam.getIsActive())
					isActive = true;
				else
					isActive = false;
			}
			deleteSecondIfCopied(objSt.obj, tcam);
		}
		else
			isActive = false;
	}

	const TCHAR * name = node->GetName();
	char * name_ansi = name ? getAnsiCopy1(name) : copyStringMax1("Unnamed");
	unsigned int lname = (unsigned int)strlen(name_ansi)+6;


	width = GetCOREInterface()->GetRendWidth();
	height = GetCOREInterface()->GetRendHeight();

	if (objSt.obj->CanConvertToType(NOXCamera_CLASS_ID))
	{
		NOXCamera * cam = (NOXCamera*)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXCamera_CLASS_ID);
		if (!cam)
		{
			MessageBox(0, _T("Error (2) while extracting camera."), _T("Error"), 0);
			return false;
		}

		iso = cam->trCam.getISO();
		aperture = cam->trCam.getAperture();
		shutter = cam->trCam.getShutter();
		autoexposure = cam->trCam.getAutoExpOn();
		focusdist = cam->trCam.getFocusDistance() * unitScale;
		autofocus = cam->trCam.getAutoFocusOn();
		autoexp_type = cam->trCam.getAutoExpType();
		bladesnumber = cam->trCam.getBladesNumber();
		bladesangle = cam->trCam.getBladesAngle();
		bladesround = cam->trCam.getBladesRound();
		bladesradius = cam->trCam.getBladesRadius();
		bokehRingSize = cam->trCam.getBokehRingSize();
		bokehFlatten = cam->trCam.getBokehFlatten();
		bokehRingBalance = cam->trCam.getBokehRingBalance();
		bokehVignette = cam->trCam.getVignette();
		dofON = cam->trCam.getDofON() ? 1 : 0;

		deleteSecondIfCopied(objSt.obj, cam);
	}


	unsigned short nox_camera_tag = NOX_BIN_CAMERA;
	if (1!=FWRITE_USHORT(nox_camera_tag))
	{
		MessageBox(0, _T("Writing camera tag error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_ULONG8(camerasize))
	{
		MessageBox(0, _T("Writing camera size error."), _T("Error"), 0);
		return false;
	}

	unsigned short activeCamera = isActive ? 1 : 0;
	unsigned short nox_cam_active_tag = NOX_BIN_CAM_ACTIVE;
	if (1!=FWRITE_USHORT(nox_cam_active_tag))
	{
		MessageBox(0, _T("Writing camera active tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(activeCamera))
	{
		MessageBox(0, _T("Writing camera active error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_width_tag = NOX_BIN_CAM_WIDTH;
	if (1!=FWRITE_USHORT(nox_cam_width_tag))
	{
		MessageBox(0, _T("Writing camera width tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(width))
	{
		MessageBox(0, _T("Writing camera width error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_height_tag = NOX_BIN_CAM_HEIGHT;
	if (1!=FWRITE_USHORT(nox_cam_height_tag))
	{
		MessageBox(0, _T("Writing camera height tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(height))
	{
		MessageBox(0, _T("Writing camera height error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_name_tag = NOX_BIN_CAM_NAME;
	if (1!=FWRITE_USHORT(nox_cam_name_tag))
	{
		MessageBox(0, _T("Writing camera name tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(lname))
	{
		MessageBox(0, _T("Writing camera name size error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_STRING(name_ansi, lname-6))
	{
		MessageBox(0, _T("Writing camera name error."), _T("Error"), 0);
		return false;
	}

	free(name_ansi);

	unsigned short nox_cam_pos_tag = NOX_BIN_CAM_POS;
	if (1!=FWRITE_USHORT(nox_cam_pos_tag))
	{
		MessageBox(0, _T("Writing camera position tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(posx))
	{
		MessageBox(0, _T("Writing camera position error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(posy))
	{
		MessageBox(0, _T("Writing camera position error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(posz))
	{
		MessageBox(0, _T("Writing camera position error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_dir_tag = NOX_BIN_CAM_DIR;
	if (1!=FWRITE_USHORT(nox_cam_dir_tag))
	{
		MessageBox(0, _T("Writing camera direction tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirx))
	{
		MessageBox(0, _T("Writing camera direction error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(diry))
	{
		MessageBox(0, _T("Writing camera direction error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirz))
	{
		MessageBox(0, _T("Writing camera direction error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_updir_tag = NOX_BIN_CAM_UPDIR;
	if (1!=FWRITE_USHORT(nox_cam_updir_tag))
	{
		MessageBox(0, _T("Writing camera up direction tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(upx))
	{
		MessageBox(0, _T("Writing camera up direction error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(upy))
	{
		MessageBox(0, _T("Writing camera up direction error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(upz))
	{
		MessageBox(0, _T("Writing camera up direction error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_rdir_tag = NOX_BIN_CAM_RIGHTDIR;
	if (1!=FWRITE_USHORT(nox_cam_rdir_tag))
	{
		MessageBox(0, _T("Writing camera right direction tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(crx))
	{
		MessageBox(0, _T("Writing camera right direction error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(cry))
	{
		MessageBox(0, _T("Writing camera right direction error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(crz))
	{
		MessageBox(0, _T("Writing camera right direction error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_angle_tag = NOX_BIN_CAM_ANGLE;
	if (1!=FWRITE_USHORT(nox_cam_angle_tag))
	{
		MessageBox(0, _T("Writing camera angle tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(angle))
	{
		MessageBox(0, _T("Writing camera angle error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_iso_tag = NOX_BIN_CAM_ISO;
	if (1!=FWRITE_USHORT(nox_cam_iso_tag))
	{
		MessageBox(0, _T("Writing camera ISO tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(iso))
	{
		MessageBox(0, _T("Writing camera ISO error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_aperture_tag = NOX_BIN_CAM_APERTURE;
	if (1!=FWRITE_USHORT(nox_cam_aperture_tag))
	{
		MessageBox(0, _T("Writing camera aperture tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(aperture))
	{
		MessageBox(0, _T("Writing camera aperture error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_shutter_tag = NOX_BIN_CAM_SHUTTER;
	if (1!=FWRITE_USHORT(nox_cam_shutter_tag))
	{
		MessageBox(0, _T("Writing camera shutter tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(shutter))
	{
		MessageBox(0, _T("Writing camera shutter error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_autoexp_on_tag = NOX_BIN_CAM_AUTOEXPOSURE_ON;
	if (1!=FWRITE_USHORT(nox_cam_autoexp_on_tag))
	{
		MessageBox(0, _T("Writing camera autoexposure on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short aexpON = autoexposure ? 1 : 0;
	if (1!=FWRITE_USHORT(aexpON))
	{
		MessageBox(0, _T("Writing camera autoexposure on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_autoexp_type_tag = NOX_BIN_CAM_AUTOEXPOSURE_TYPE;
	if (1!=FWRITE_USHORT(nox_cam_autoexp_type_tag))
	{
		MessageBox(0, _T("Writing camera autoexposure type tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(autoexp_type))
	{
		MessageBox(0, _T("Writing camera autoexposure type error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_autofocus_on_tag = NOX_BIN_CAM_AUTOFOCUS;
	if (1!=FWRITE_USHORT(nox_cam_autofocus_on_tag))
	{
		MessageBox(0, _T("Writing camera autofocus on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short afocusON = autofocus ? 1 : 0;
	if (1!=FWRITE_USHORT(afocusON))
	{
		MessageBox(0, _T("Writing camera autofocus on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_focusdist_tag = NOX_BIN_CAM_FOCUSDIST;
	if (1!=FWRITE_USHORT(nox_cam_focusdist_tag))
	{
		MessageBox(0, _T("Writing camera focus distance tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(focusdist))
	{
		MessageBox(0, _T("Writing camera focus distance error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bladesnumber_on_tag = NOX_BIN_CAM_BLADESNUMBER;
	if (1!=FWRITE_USHORT(nox_cam_bladesnumber_on_tag))
	{
		MessageBox(0, _T("Writing camera aperture blades number tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bladesnumber))
	{
		MessageBox(0, _T("Writing camera aperture blades number error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bladesangle_tag = NOX_BIN_CAM_BLADESANGLE;
	if (1!=FWRITE_USHORT(nox_cam_bladesangle_tag))
	{
		MessageBox(0, _T("Writing camera aperture blades angle tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(bladesangle))
	{
		MessageBox(0, _T("Writing camera aperture blades angle error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bladesround_on_tag = NOX_BIN_CAM_BLADESROUND;
	if (1!=FWRITE_USHORT(nox_cam_bladesround_on_tag))
	{
		MessageBox(0, _T("Writing camera aperture blades round tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short blRound = bladesround ? 1 : 0;
	if (1!=FWRITE_USHORT(blRound))
	{
		MessageBox(0, _T("Writing camera aperture blades round error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bladesradius_tag = NOX_BIN_CAM_BLADESRADIUS;
	if (1!=FWRITE_USHORT(nox_cam_bladesradius_tag))
	{
		MessageBox(0, _T("Writing camera aperture blades radius tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(bladesradius))
	{
		MessageBox(0, _T("Writing camera aperture blades radius error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bokehringsize_tag = NOX_BIN_CAM_BOKEH_RING_SIZE;
	if (1!=FWRITE_USHORT(nox_cam_bokehringsize_tag))
	{
		MessageBox(0, _T("Writing camera bokeh ring size tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bokehRingSize))
	{
		MessageBox(0, _T("Writing camera bokeh ring size error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bokehflatten_tag = NOX_BIN_CAM_BOKEH_FLATTEN;
	if (1!=FWRITE_USHORT(nox_cam_bokehflatten_tag))
	{
		MessageBox(0, _T("Writing camera bokeh flatten tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bokehFlatten))
	{
		MessageBox(0, _T("Writing camera bokeh flatten error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bokehringbalance_tag = NOX_BIN_CAM_BOKEH_RING_BALANCE;
	if (1!=FWRITE_USHORT(nox_cam_bokehringbalance_tag))
	{
		MessageBox(0, _T("Writing camera bokeh ring balance tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bokehRingBalance))
	{
		MessageBox(0, _T("Writing camera bokeh ring balance error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_bokehvignette_tag = NOX_BIN_CAM_BOKEH_VIGNETTE;
	if (1!=FWRITE_USHORT(nox_cam_bokehvignette_tag))
	{
		MessageBox(0, _T("Writing camera bokeh vignette tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(bokehVignette))
	{
		MessageBox(0, _T("Writing camera bokeh vignette error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_dofOn_tag = NOX_BIN_CAM_DOF_ON;
	if (1!=FWRITE_USHORT(nox_cam_dofOn_tag))
	{
		MessageBox(0, _T("Writing camera DOF on tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(dofON))
	{
		MessageBox(0, _T("Writing camera DOF on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_aa_tag = NOX_BIN_CAM_AA;
	if (1!=FWRITE_USHORT(nox_cam_aa_tag))
	{
		MessageBox(0, _T("Writing camera antialiasing tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(aa))
	{
		MessageBox(0, _T("Writing camera antialiasing error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_cam_mb_on_tag = NOX_BIN_CAM_MB_ON;
	if (1!=FWRITE_USHORT(nox_cam_mb_on_tag))
	{
		MessageBox(0, _T("Writing camera motion blur on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short mb_enabled = motion_blur ? 1 : 0;
	if (1!=FWRITE_USHORT(mb_enabled))
	{
		MessageBox(0, _T("Writing camera motion blur on error."), _T("Error"), 0);
		return false;
	}

	mb_pos *= unitScale;
	unsigned short nox_cam_mb_pos_tag = NOX_BIN_CAM_MB_POS;
	if (1!=FWRITE_USHORT(nox_cam_mb_pos_tag))
	{
		MessageBox(0, _T("Writing camera motion blur pos tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_pos.x))
	{
		MessageBox(0, _T("Writing camera motion blur pos error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_pos.z))
	{
		MessageBox(0, _T("Writing camera motion blur pos error."), _T("Error"), 0);
		return false;
	}
	float mbposminusy = -mb_pos.y;
	if (1!=FWRITE_FLOAT(mbposminusy))
	{
		MessageBox(0, _T("Writing camera motion blur pos error."), _T("Error"), 0);
		return false;
	}
	logPoint(mb_pos, "cam mb pos save2");

	unsigned short nox_cam_mb_angle_tag = NOX_BIN_CAM_MB_ANGLE;
	if (1!=FWRITE_USHORT(nox_cam_mb_angle_tag))
	{
		MessageBox(0, _T("Writing camera motion blur angle tag error."), _T("Error"), 0);
		return false;
	}
	float mb_a = acos(mb_quat.w)*-2 * GetFrameRate();
	if (1!=FWRITE_FLOAT(mb_a))
	{
		MessageBox(0, _T("Writing camera motion blur angle error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_quat.x))
	{
		MessageBox(0, _T("Writing camera motion blur angle error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(mb_quat.z))
	//if (1!=FWRITE_FLOAT(mb_quat.y))
	{
		MessageBox(0, _T("Writing camera motion blur angle error."), _T("Error"), 0);
		return false;
	}
	float rminusy = -mb_quat.y;
	if (1!=FWRITE_FLOAT(rminusy))
	//if (1!=FWRITE_FLOAT(mb_quat.z))
	{
		MessageBox(0, _T("Writing camera motion blur angle error."), _T("Error"), 0);
		return false;
	}


	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != camerasize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Geometry chunk size is incorrect.\nShould be %llu instead of %llu bytes."), camerasize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMaterialsAllPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	unsigned long long matsSize = evalSizeMaterials();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_materials_tag = NOX_BIN_MATERIALS;
	if (1!=FWRITE_USHORT(nox_materials_tag))
	{
		MessageBox(0, _T("Writing materials tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(matsSize))
	{
		MessageBox(0, _T("Writing materials size error."), _T("Error"), 0);
		return false;
	}
	unsigned int numMats = (unsigned int)mats.size();
	if (1!=FWRITE_UINT(numMats))
	{
		MessageBox(0, _T("Writing materials number error."), _T("Error"), 0);
		return false;
	}

	ADD_LOG_PARTS_MAIN("Exporting materials");

	for (unsigned int i=0; i<numMats; i++)
	{
		if (!insertMaterialSinglePart(file, i))
			return false;
	}

	ADD_LOG_PARTS_MAIN("Materials exported");

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matsSize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Materials chunk size is incorrect.\nShould be %llu instead of %llu bytes."), matsSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMaterialPreviewPart(FILE * file, unsigned int numMat)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numMat < 0   ||   numMat >= (unsigned int)mats.size())
	{
		MessageBox(0, _T("Error. Tried to save material preview from material that doesn't exist."), _T("Error"), 0);
		return false;
	}

	Mtl * mat = mats[numMat];
	bool isNOXmat = matTypes[numMat];
	if (!mat)
		isNOXmat = false;
	if (!isNOXmat)
	{
		MessageBox(0, _T("Error. Tried to save material preview from nonNOX material."), _T("Error"), 0);
		return false;
	}

	NOXMaterial * nmat = (NOXMaterial*)mat;

	bool prevExists = nmat->mat.getPreviewExists();
	if (!prevExists)
	{
		MessageBox(0, _T("Error. Tried to save material preview but material has no preview."), _T("Error"), 0);
		return false;
	}

	unsigned int bufsize = nmat->mat.getPreviewBufSize();
	unsigned int pwidth  = nmat->mat.getPreviewWidth();
	unsigned int pheight = nmat->mat.getPreviewHeight();

	unsigned char * prevBuf = (unsigned char *)nmat->mat.getPreviewBuffer();
	if (!prevBuf)
	{
		MessageBox(0, _T("Error. Tried to save material preview but preview buffer is empty."), _T("Error"), 0);
		return false;
	}

	unsigned long long matPrevSize = bufsize + 18;
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_matprev_tag = NOX_BIN_MAT_PREVIEW;
	if (1!=FWRITE_USHORT(nox_matprev_tag))
	{
		free(prevBuf);
		MessageBox(0, _T("Writing material preview tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(matPrevSize))
	{
		free(prevBuf);
		MessageBox(0, _T("Writing material preview size error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(pwidth))
	{
		free(prevBuf);
		MessageBox(0, _T("Writing material preview width error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_UINT(pheight))
	{
		free(prevBuf);
		MessageBox(0, _T("Writing material preview height error."), _T("Error"), 0);
		return false;
	}


	if (1!=fwrite(prevBuf, bufsize, 1, file))
	{
		free(prevBuf);
		MessageBox(0, _T("Writing material preview buffer error."), _T("Error"), 0);
		return false;
	}

	free(prevBuf);

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matPrevSize)
	{
		TCHAR errmsg[512];
		_stprintf_s(errmsg, 512, _T("Export error. Material preview chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n"), matPrevSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMaterialSinglePart(FILE * file, unsigned int numMat)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numMat < 0   ||   numMat >= (unsigned int)mats.size())
	{
		MessageBox(0, _T("Error. Tried to save material that doesn't exist."), _T("Error"), 0);
		return false;
	}

	Mtl * mat = mats[numMat];
	bool isNOXmat = matTypes[numMat];
	if (!mat)
		isNOXmat = false;
	unsigned int nLayers;
	unsigned int blend;
	unsigned short skyportal;
	unsigned short matteshadow;
	float opacity;
	unsigned short use_tex_opacity;
	char * tex_opacity = NULL;
	if (isNOXmat)
	{
		NOXMaterial * nmat = (NOXMaterial*)mat;
		nLayers = nmat->mat.getLayersNumber();
		blend = nmat->mat.getBlendLayerNumber();
		opacity = nmat->mat.getOpacity();
		use_tex_opacity = nmat->mat.getUseTexOpacity() ? 1 : 0;
		tex_opacity = nmat->mat.getTexOpacity();	// don't release buffer
		skyportal = nmat->mat.getSkyportal() ? 1 : 0;
		matteshadow = nmat->mat.getMatteShadow() ? 1 : 0;
		//return insertNOXMaterial(curNode, mat, i);
	}
	else
	{
		nLayers = 1;
		blend = 0;
		opacity = 1;
		use_tex_opacity = 0;
		tex_opacity = NULL;
		skyportal = 0;
		matteshadow = 0;
	}

	const TCHAR * matname = mat ? mat->GetName() : (TCHAR *)NULL;
	char * matname_ansi = (matname && _tcsclen(matname)>0)? getAnsiCopy1(matname) : copyStringMax1("Unnamed");
	int matnamelength = (int)strlen(matname_ansi)+6;

	TCHAR infotxt[512];
	_stprintf_s(infotxt, 512, _T("Exporting material %s ..."), matname);
	setProgInfo(infotxt);

	unsigned long long matSize = evalSizeMaterial(numMat);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_material_tag = NOX_BIN_MATERIAL;
	if (1!=FWRITE_USHORT(nox_material_tag))
	{
		MessageBox(0, _T("Writing material tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(matSize))
	{
		MessageBox(0, _T("Writing material size error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(numMat))
	{
		MessageBox(0, _T("Writing material ID error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(blend))
	{
		MessageBox(0, _T("Writing material blend layer error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(nLayers))
	{
		MessageBox(0, _T("Writing material layers number error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_matname_tag = NOX_BIN_MATNAME;
	if (1!=FWRITE_USHORT(nox_matname_tag))
	{
		MessageBox(0, _T("Writing material name tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_UINT(matnamelength))
	{
		MessageBox(0, _T("Writing material name length error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_STRING(matname_ansi, (matnamelength-6)))
	{
		MessageBox(0, _T("Writing material name error."), _T("Error"), 0);
		return false;
	}

	if (matname_ansi)
		free(matname_ansi);

	unsigned short nox_matskyportal_tag = NOX_BIN_MAT_SKYPORTAL;
	if (1!=FWRITE_USHORT(nox_matskyportal_tag))
	{
		MessageBox(0, _T("Writing material sky portal tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(skyportal))
	{
		MessageBox(0, _T("Writing material sky portal error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_matmatteshadow_tag = NOX_BIN_MAT_MATTE_SHADOW;
	if (1!=FWRITE_USHORT(nox_matmatteshadow_tag))
	{
		MessageBox(0, _T("Writing material matte shadow tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(matteshadow))
	{
		MessageBox(0, _T("Writing material matte shadow error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_matopacity_tag = NOX_BIN_MAT_OPACITY;
	if (1!=FWRITE_USHORT(nox_matopacity_tag))
	{
		MessageBox(0, _T("Writing material opacity tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(opacity))
	{
		MessageBox(0, _T("Writing material opacity error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_matopacity_usetex_tag = NOX_BIN_MAT_USE_TEX_OPACITY;
	if (1!=FWRITE_USHORT(nox_matopacity_usetex_tag))
	{
		MessageBox(0, _T("Writing material opacity use texture tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(use_tex_opacity))
	{
		MessageBox(0, _T("Writing material opacity use texture error."), _T("Error"), 0);
		return false;
	}


	if (isNOXmat)
	{
		if (!insertTexturePart(file, numMat, 0, MAT_TEX_SLOT_OPACITY))
			return false;

		if (!insertTexturePart(file, numMat, 0, MAT_TEX_SLOT_DISPLACEMENT))
			return false;


		NOXMaterial * nmat = (NOXMaterial*)mat;
		if (nmat->mat.getPreviewExists())
		{
			if (!insertMaterialPreviewPart(file, numMat))
			{
				return false;
			}
		}		

		unsigned short nox_matdispl_subdivs_tag = NOX_BIN_MAT_DISPLACEMENT_SUBDIVS;
		if (1!=FWRITE_USHORT(nox_matdispl_subdivs_tag))
		{
			MessageBox(0, _T("Writing material displacement subdivs tag error."), _T("Error"), 0);
			return false;
		}
		unsigned short displsubdivs = nmat->mat.getDisplacementSubdivs();
		if (1!=FWRITE_USHORT(displsubdivs))
		{
			MessageBox(0, _T("Writing material displacement subdivs error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_matdispl_depth_tag = NOX_BIN_MAT_DISPLACEMENT_DEPTH;
		if (1!=FWRITE_USHORT(nox_matdispl_depth_tag))
		{
			MessageBox(0, _T("Writing material displacement depth tag error."), _T("Error"), 0);
			return false;
		}
		float displdepth = nmat->mat.getDisplacementDepth();
		if (1!=FWRITE_FLOAT(displdepth))
		{
			MessageBox(0, _T("Writing material displacement depth error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_matdispl_shift_tag = NOX_BIN_MAT_DISPLACEMENT_SHIFT;
		if (1!=FWRITE_USHORT(nox_matdispl_shift_tag))
		{
			MessageBox(0, _T("Writing material displacement shift tag error."), _T("Error"), 0);
			return false;
		}
		float displshift = nmat->mat.getDisplacementShift();
		if (1!=FWRITE_FLOAT(displshift))
		{
			MessageBox(0, _T("Writing material displacement shift error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_matdispl_normals_tag = NOX_BIN_MAT_DISPLACEMENT_NORMAL_MODE;
		if (1!=FWRITE_USHORT(nox_matdispl_normals_tag))
		{
			MessageBox(0, _T("Writing material displacement normal mode tag error."), _T("Error"), 0);
			return false;
		}
		unsigned int displnormals = nmat->mat.getDisplacementNormalMode();
		if (1!=FWRITE_UINT(displnormals))
		{
			MessageBox(0, _T("Writing material displacement normal mode error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_matdispl_cont_tag = NOX_BIN_MAT_DISPLACEMENT_CONTINUITY;
		if (1!=FWRITE_USHORT(nox_matdispl_cont_tag))
		{
			MessageBox(0, _T("Writing material displacement continuity tag error."), _T("Error"), 0);
			return false;
		}
		unsigned short displcont = nmat->mat.getDisplacementContinuity() ? 1 : 0;
		if (1!=FWRITE_USHORT(displcont))
		{
			MessageBox(0, _T("Writing material displacement continuity error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_matdispl_ignsc_tag = NOX_BIN_MAT_DISPLACEMENT_IGNORE_SCALE;
		if (1!=FWRITE_USHORT(nox_matdispl_ignsc_tag))
		{
			MessageBox(0, _T("Writing material displacement ignore scale tag error."), _T("Error"), 0);
			return false;
		}
		unsigned short displign = nmat->mat.getDisplacementIgnoreScale() ? 1 : 0;
		if (1!=FWRITE_USHORT(displign))
		{
			MessageBox(0, _T("Writing material displacement ignore scale error."), _T("Error"), 0);
			return false;
		}

		// LAYERS ----- 
		for (unsigned int i=0; i<nLayers; i++)
		{
			if (!insertMatLayerPart(file, numMat, i))
				return false;
		}
	}
	else
	{
		if (!insertMatLayerNonNOXPart(file, numMat))
			return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != matSize)
	{
		TCHAR errmsg[512];
		_stprintf_s(errmsg, 512, _T("Export error. Material chunk size is incorrect.\nShould be %llu instead of %llu bytes.\n%s"), matSize, realSize, matname);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMatLayerPart(FILE * file, unsigned int numMat, unsigned int numLay)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numMat < 0   ||   numMat >= (unsigned int)mats.size())
	{
		MessageBox(0, _T("Error. Tried to save layer from material that doesn't exist."), _T("Error"), 0);
		return false;
	}

	bool isNOXmat = matTypes[numMat];
	if (!isNOXmat)
	{
		MessageBox(0, _T("Error. Tried to save layer from non-NOX material."), _T("Error"), 0);
		return false;
	}

	Mtl * mat = mats[numMat];
	if (!mat)
	{
		MessageBox(0, _T("Error. Tried to save NULL material as NOX material."), _T("Error"), 0);
		return false;
	}

	NOXMaterial * nmat = (NOXMaterial*)mat;
	int nLayers = nmat->mat.getLayersNumber();

	if (numLay < 0   ||   numLay >= (unsigned int)nLayers)
	{
		MessageBox(0, _T("Error. Tried to save material layer that doesn't exist."), _T("Error"), 0);
		return false;
	}

	unsigned long long layerSize = evalSizeMaterialLayer(numMat, numLay);
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_layer_tag = NOX_BIN_MATLAYER;
	if (1!=FWRITE_USHORT(nox_layer_tag))
	{
		MessageBox(0, _T("Writing material layer tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(layerSize))
	{
		MessageBox(0, _T("Writing material layer size error."), _T("Error"), 0);
		return false;
	}
	unsigned short layType = nmat->mat.getLayerType(numLay);
	if (1!=FWRITE_USHORT(layType))
	{
		MessageBox(0, _T("Writing material layer type error."), _T("Error"), 0);
		return false;
	}
	unsigned int layContr = nmat->mat.getLayerContribution(numLay);
	if (1!=FWRITE_UINT(layContr))
	{
		MessageBox(0, _T("Writing material layer contribution error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_refl0_tag = NOX_BIN_MLAY_REFL0;
	if (1!=FWRITE_USHORT(nox_mlay_refl0_tag))
	{
		MessageBox(0, _T("Writing material layer color0 tag error."), _T("Error"), 0);
		return false;
	}
	float r,g,b,a;
	nmat->mat.getLayerColor0(numLay, r,g,b,a);
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing material layer color0 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing material layer color0 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing material layer color0 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing material layer color0 error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_refl90_tag = NOX_BIN_MLAY_REFL90;
	if (1!=FWRITE_USHORT(nox_mlay_refl90_tag))
	{
		MessageBox(0, _T("Writing material layer color90 tag error."), _T("Error"), 0);
		return false;
	}
	//float r,g,b,a;
	nmat->mat.getLayerColor90(numLay, r,g,b,a);
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing material layer color90 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing material layer color90 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing material layer color90 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing material layer color90 error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_c90_tag = NOX_BIN_MLAY_CONNECT90;
	if (1!=FWRITE_USHORT(nox_mlay_c90_tag))
	{
		MessageBox(0, _T("Writing material layer connect 90 tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short c90 = nmat->mat.getLayerConnect90(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(c90))
	{
		MessageBox(0, _T("Writing material layer connect 90 error."), _T("Error"), 0);
		return false;
	}


	unsigned short nox_mlay_trcol_tag = NOX_BIN_MLAY_TRANSM_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_trcol_tag))
	{
		MessageBox(0, _T("Writing material layer transmission color tag error."), _T("Error"), 0);
		return false;
	}
	nmat->mat.getLayerTransmColor(numLay, r,g,b,a);
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing material layer transmission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing material layer transmission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing material layer transmission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing material layer transmission color error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_rough_tag = NOX_BIN_MLAY_ROUGH;
	if (1!=FWRITE_USHORT(nox_mlay_rough_tag))
	{
		MessageBox(0, _T("Writing material layer roughness tag error."), _T("Error"), 0);
		return false;
	}
	float rough = nmat->mat.getLayerRoughness(numLay);
	if (1!=FWRITE_FLOAT(rough))
	{
		MessageBox(0, _T("Writing material layer roughness error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_aniso_tag = NOX_BIN_MLAY_ANISOTROPY;
	if (1!=FWRITE_USHORT(nox_mlay_aniso_tag))
	{
		MessageBox(0, _T("Writing material layer anisotropy tag error."), _T("Error"), 0);
		return false;
	}
	float aniso = nmat->mat.getLayerAniso(numLay);
	if (1!=FWRITE_FLOAT(aniso))
	{
		MessageBox(0, _T("Writing material layer anisotropy error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_anisoangle_tag = NOX_BIN_MLAY_ANISOTROPY;
	if (1!=FWRITE_USHORT(nox_mlay_anisoangle_tag))
	{
		MessageBox(0, _T("Writing material layer anisotropy angle tag error."), _T("Error"), 0);
		return false;
	}
	float anisoangle = nmat->mat.getLayerAnisoAngle(numLay);
	if (1!=FWRITE_FLOAT(anisoangle))
	{
		MessageBox(0, _T("Writing material layer anisotropy angle error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_fresnel_tag = NOX_BIN_MLAY_FRESNEL;
	if (1!=FWRITE_USHORT(nox_mlay_fresnel_tag))
	{
		MessageBox(0, _T("Writing material layer fresnel tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int freslen = 12;
	if (1!=FWRITE_UINT(freslen))
	{
		MessageBox(0, _T("Writing material layer fresnel size error."), _T("Error"), 0);
		return false;
	}
	unsigned short nox_mlay_fresnel_ior_tag = NOX_BIN_MLAY_FRES_IOR;
	if (1!=FWRITE_USHORT(nox_mlay_fresnel_ior_tag))
	{
		MessageBox(0, _T("Writing material layer fresnel ior tag error."), _T("Error"), 0);
		return false;
	}
	float ior = nmat->mat.getLayerFresnelIOR(numLay);
	if (1!=FWRITE_FLOAT(ior))
	{
		MessageBox(0, _T("Writing material layer fresnel ior error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_transm_tag = NOX_BIN_MLAY_TRANSM_ON;
	if (1!=FWRITE_USHORT(nox_mlay_transm_tag))
	{
		MessageBox(0, _T("Writing material layer transmission on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short transmON = nmat->mat.getLayerTransmOn(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(transmON))
	{
		MessageBox(0, _T("Writing material layer transmission on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_fakeglass_tag = NOX_BIN_MLAY_FAKEGLASS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_fakeglass_tag))
	{
		MessageBox(0, _T("Writing material layer fake glass on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short fakeglassON = nmat->mat.getLayerFakeGlass(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(fakeglassON))
	{
		MessageBox(0, _T("Writing material layer fake glass on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_on_tag = NOX_BIN_MLAY_ABS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_abs_on_tag))
	{
		MessageBox(0, _T("Writing material layer absorption on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short absON = nmat->mat.getLayerAbsOn(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(absON))
	{
		MessageBox(0, _T("Writing material layer absorption on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_color_tag = NOX_BIN_MLAY_ABS_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_abs_color_tag))
	{
		MessageBox(0, _T("Writing material layer absorption color tag error."), _T("Error"), 0);
		return false;
	}
	nmat->mat.getLayerAbsColor(numLay, r,g,b,a);
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing material layer absorption color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing material layer absorption color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing material layer absorption color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing material layer absorption color error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_dist_tag = NOX_BIN_MLAY_ABS_DIST;
	if (1!=FWRITE_USHORT(nox_mlay_abs_dist_tag))
	{
		MessageBox(0, _T("Writing material layer absorption distance tag error."), _T("Error"), 0);
		return false;
	}
	float absDist = nmat->mat.getLayerAbsDist(numLay);
	if (1!=FWRITE_FLOAT(absDist))
	{
		MessageBox(0, _T("Writing material layer absorption distance error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_disp_on_tag = NOX_BIN_MLAY_DISP_ON;
	if (1!=FWRITE_USHORT(nox_mlay_disp_on_tag))
	{
		MessageBox(0, _T("Writing material layer dispersion on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short dispON = nmat->mat.getLayerDispersionOn(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(dispON))
	{
		MessageBox(0, _T("Writing material layer dispersion on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_disp_val_tag = NOX_BIN_MLAY_DISP_VALUE;
	if (1!=FWRITE_USHORT(nox_mlay_abs_disp_val_tag))
	{
		MessageBox(0, _T("Writing material layer dispersion value tag error."), _T("Error"), 0);
		return false;
	}
	float dispVal = nmat->mat.getLayerDispersionValue(numLay);
	if (1!=FWRITE_FLOAT(dispVal))
	{
		MessageBox(0, _T("Writing material layer dispersion value error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_sss_on_tag = NOX_BIN_MLAY_SSS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_sss_on_tag))
	{
		MessageBox(0, _T("Writing material layer sss on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short sssON = nmat->mat.getLayerSSSOn(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(sssON))
	{
		MessageBox(0, _T("Writing material layer sss on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_sss_dens_tag = NOX_BIN_MLAY_SSS_DENSITY;
	if (1!=FWRITE_USHORT(nox_mlay_sss_dens_tag))
	{
		MessageBox(0, _T("Writing material layer sss density tag error."), _T("Error"), 0);
		return false;
	}
	float sssDens = nmat->mat.getLayerSSSDensity(numLay);
	if (1!=FWRITE_FLOAT(sssDens))
	{
		MessageBox(0, _T("Writing material layer sss density error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_sss_cdir_tag = NOX_BIN_MLAY_SSS_DIRECTION ;
	if (1!=FWRITE_USHORT(nox_mlay_sss_cdir_tag))
	{
		MessageBox(0, _T("Writing material layer sss collision direction tag error."), _T("Error"), 0);
		return false;
	}
	float sssCDir = nmat->mat.getLayerSSSCollDirection(numLay);
	if (1!=FWRITE_FLOAT(sssCDir))
	{
		MessageBox(0, _T("Writing material layer sss collision direction error."), _T("Error"), 0);
		return false;
	}


	unsigned short nox_mlay_em_color_tag = NOX_BIN_MLAY_EMISS_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_em_color_tag))
	{
		MessageBox(0, _T("Writing material layer emission color tag error."), _T("Error"), 0);
		return false;
	}
	nmat->mat.getLayerEmissionColor(numLay, r,g,b,a);
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing material layer emission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing material layer emission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing material layer emission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing material layer emission color error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_em_temp_tag = NOX_BIN_MLAY_EMISS_TEMP;
	if (1!=FWRITE_USHORT(nox_mlay_em_temp_tag))
	{
		MessageBox(0, _T("Writing material layer emission temperature tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int emTemp = nmat->mat.getLayerEmissionTemperature(numLay);
	if (1!=FWRITE_UINT(emTemp))
	{
		MessageBox(0, _T("Writing material layer emission temperature error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_em_pow_tag = NOX_BIN_MLAY_EMISS_POWER;
	if (1!=FWRITE_USHORT(nox_mlay_em_pow_tag))
	{
		MessageBox(0, _T("Writing material layer emission power tag error."), _T("Error"), 0);
		return false;
	}
	float emPow = nmat->mat.getLayerEmissionPower(numLay);
	if (1!=FWRITE_FLOAT(emPow))
	{
		MessageBox(0, _T("Writing material layer emission power error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_em_unit_tag = NOX_BIN_MLAY_EMISS_UNIT;
	if (1!=FWRITE_USHORT(nox_mlay_em_unit_tag))
	{
		MessageBox(0, _T("Writing material layer emission unit tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int emUnit = nmat->mat.getLayerEmissionUnit(numLay);
	if (1!=FWRITE_UINT(emUnit))
	{
		MessageBox(0, _T("Writing material layer emission unit error."), _T("Error"), 0);
		return false;
	}

	char * iesFName = nmat->mat.getLayerEmissionIes(numLay);
	if (iesFName)
	{
		int lIes = (int)strlen(iesFName)+6;
		unsigned short nox_ies_filename_tag = NOX_BIN_MLAY_EMISS_IES;
		if (1!=FWRITE_USHORT(nox_ies_filename_tag))
		{
			MessageBox(0, _T("Writing IES filename tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(lIes))
		{
			MessageBox(0, _T("Writing IES filename size error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_STRING(iesFName, (lIes-6)))
		{
			MessageBox(0, _T("Writing IES filaname error."), _T("Error"), 0);
			return false;
		}
	}


	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_COLOR0))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_COLOR90))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_ROUGHNESS))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_WEIGHT))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_LIGHT))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_NORMAL))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_TRANSM))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_ANISOTROPY))
		return false;
	if (!insertTexturePart(file, numMat, numLay, LAYER_TEX_SLOT_ANISO_ANGLE))
		return false;


	unsigned short nox_mlay_tex_on_refl0_tag = NOX_BIN_MLAY_REFL0_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_refl0_tag))
	{
		MessageBox(0, _T("Writing material layer color0 texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short r0texOn = nmat->mat.getLayerUseTexRefl0(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(r0texOn))
	{
		MessageBox(0, _T("Writing material layer color0 texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_refl90_tag = NOX_BIN_MLAY_REFL90_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_refl90_tag))
	{
		MessageBox(0, _T("Writing material layer color90 texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short r90texOn = nmat->mat.getLayerUseTexRefl90(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(r90texOn))
	{
		MessageBox(0, _T("Writing material layer color90 texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_rough_tag = NOX_BIN_MLAY_ROUGH_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_rough_tag))
	{
		MessageBox(0, _T("Writing material layer roughness texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short roughTexOn = nmat->mat.getLayerUseTexRough(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(roughTexOn))
	{
		MessageBox(0, _T("Writing material layer roughness texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_weight_tag = NOX_BIN_MLAY_WEIGHT_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_weight_tag))
	{
		MessageBox(0, _T("Writing material layer contribution texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short weightTexOn = nmat->mat.getLayerUseTexWeight(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(weightTexOn))
	{
		MessageBox(0, _T("Writing material layer contribution texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_light_tag = NOX_BIN_MLAY_LIGHT_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_light_tag))
	{
		MessageBox(0, _T("Writing material layer emission texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short lightTexOn = nmat->mat.getLayerUseTexLight(numLay) ? 1 : 0;
	if (1!=FWRITE_USHORT(lightTexOn))
	{
		MessageBox(0, _T("Writing material layer emission texture on error."), _T("Error"), 0);
		return false;
	}

	// VERIFY
	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != layerSize)
	{
		const TCHAR * matname = mat->GetName();
		if (!matname)
			matname = _T("Unnamed");
		TCHAR errmsg[1024];
		_stprintf_s(errmsg, 1024, _T("Export error. Layer (%d) chunk in material (%s) size is incorrect.\nShould be %llu instead of %llu bytes."), (numLay+1), matname, layerSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMatLayerNonNOXPart(FILE * file, unsigned int numMat)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (numMat < 0   ||   numMat >= (unsigned int)mats.size())
	{
		MessageBox(0, _T("Error. Tried to save layer from non-NOX material that doesn't exist."), _T("Error"), 0);
		return false;
	}

	bool isNOXmat = matTypes[numMat];
	if (isNOXmat)
	{
		MessageBox(0, _T("Error. Tried to save NOX material layer as non-NOX one."), _T("Error"), 0);
		return false;
	}

	Mtl * mat = mats[numMat];
	if (!mat)
		isNOXmat = false;

	unsigned long long layerSize = 172;
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_layer_tag = NOX_BIN_MATLAYER;
	if (1!=FWRITE_USHORT(nox_layer_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(layerSize))
	{
		MessageBox(0, _T("Writing non-NOX material layer size error."), _T("Error"), 0);
		return false;
	}
	unsigned short layType = 2;
	if (mat)
		layType = (mat->GetSelfIllumColorOn()) ? 1 : 2;
	if (1!=FWRITE_USHORT(layType))
	{
		MessageBox(0, _T("Writing non-NOX material layer type error."), _T("Error"), 0);
		return false;
	}
	unsigned int layContr = 100;
	if (1!=FWRITE_UINT(layContr))
	{
		MessageBox(0, _T("Writing non-NOX material layer contribution error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_refl0_tag = NOX_BIN_MLAY_REFL0;
	if (1!=FWRITE_USHORT(nox_mlay_refl0_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer color0 tag error."), _T("Error"), 0);
		return false;
	}
	float r=1,g=1,b=1,a=1;
	if (mat)
	{
		r = mat->GetDiffuse().r;
		g = mat->GetDiffuse().g;
		b = mat->GetDiffuse().b;
		a = 1.0f;
	}
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing non-NOX material layer color0 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing non-NOX material layer color0 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing non-NOX material layer color0 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing non-NOX material layer color0 error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_refl90_tag = NOX_BIN_MLAY_REFL90;
	if (1!=FWRITE_USHORT(nox_mlay_refl90_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer color90 tag error."), _T("Error"), 0);
		return false;
	}
	r = g = b = a = 1.0f;
	if (mat)
	{
		r = mat->GetSpecular().r;
		g = mat->GetSpecular().g;
		b = mat->GetSpecular().b;
		a = 1.0f;
	}
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing non-NOX material layer color90 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing non-NOX material layer color90 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing non-NOX material layer color90 error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing non-NOX material layer color90 error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_rough_tag = NOX_BIN_MLAY_ROUGH;
	if (1!=FWRITE_USHORT(nox_mlay_rough_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer roughness tag error."), _T("Error"), 0);
		return false;
	}
	float rough = 1.0f;
	if (mat)
	{
		rough =  1.0f - mat->GetShininess();
		if (rough>=0.989999f)
			rough = 1.0f;
	}
	if (1!=FWRITE_FLOAT(rough))
	{
		MessageBox(0, _T("Writing non-NOX material layer roughness error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_fresnel_tag = NOX_BIN_MLAY_FRESNEL;
	if (1!=FWRITE_USHORT(nox_mlay_fresnel_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer fresnel tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int freslen = 12;
	if (1!=FWRITE_UINT(freslen))
	{
		MessageBox(0, _T("Writing non-NOX material layer fresnel size error."), _T("Error"), 0);
		return false;
	}
	unsigned short nox_mlay_fresnel_ior_tag = NOX_BIN_MLAY_FRES_IOR;
	if (1!=FWRITE_USHORT(nox_mlay_fresnel_ior_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer fresnel ior tag error."), _T("Error"), 0);
		return false;
	}
	float ior = 1.6f;
	if (1!=FWRITE_FLOAT(ior))
	{
		MessageBox(0, _T("Writing non-NOX material layer fresnel ior error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_transm_tag = NOX_BIN_MLAY_TRANSM_ON;
	if (1!=FWRITE_USHORT(nox_mlay_transm_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer transmission on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short transmON = 0;
	if (1!=FWRITE_USHORT(transmON))
	{
		MessageBox(0, _T("Writing non-NOX material layer transmission on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_fakeglass_tag = NOX_BIN_MLAY_FAKEGLASS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_fakeglass_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer fake glass on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short fakeglassON = 0;
	if (1!=FWRITE_USHORT(fakeglassON))
	{
		MessageBox(0, _T("Writing non-NOX material layer fake glass on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_on_tag = NOX_BIN_MLAY_ABS_ON;
	if (1!=FWRITE_USHORT(nox_mlay_abs_on_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short absON = 0;
	if (1!=FWRITE_USHORT(fakeglassON))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_color_tag = NOX_BIN_MLAY_ABS_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_abs_color_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption color tag error."), _T("Error"), 0);
		return false;
	}
	r = g = b = a = 1.0f;
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption color error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_dist_tag = NOX_BIN_MLAY_ABS_DIST;
	if (1!=FWRITE_USHORT(nox_mlay_abs_dist_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption distance tag error."), _T("Error"), 0);
		return false;
	}
	float absDist = 1.0f;
	if (1!=FWRITE_FLOAT(absDist))
	{
		MessageBox(0, _T("Writing non-NOX material layer absorption distance error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_disp_on_tag = NOX_BIN_MLAY_DISP_ON;
	if (1!=FWRITE_USHORT(nox_mlay_disp_on_tag))
	{
		MessageBox(0, _T("Writing material layer dispersion on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short dispON = 0;
	if (1!=FWRITE_USHORT(dispON))
	{
		MessageBox(0, _T("Writing material layer dispersion on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_abs_disp_val_tag = NOX_BIN_MLAY_DISP_VALUE;
	if (1!=FWRITE_USHORT(nox_mlay_abs_disp_val_tag))
	{
		MessageBox(0, _T("Writing material layer dispersion value tag error."), _T("Error"), 0);
		return false;
	}
	float dispVal = 0.0f;
	if (1!=FWRITE_FLOAT(dispVal))
	{
		MessageBox(0, _T("Writing material layer dispersion value error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_em_color_tag = NOX_BIN_MLAY_EMISS_COLOR;
	if (1!=FWRITE_USHORT(nox_mlay_em_color_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission color tag error."), _T("Error"), 0);
		return false;
	}
	r = g = b = a = 1.0f;
	if (mat)
	{
		r = mat->GetSelfIllumColor().r;
		g = mat->GetSelfIllumColor().g;
		b = mat->GetSelfIllumColor().b;
		a = 1.0f;
	}
	if (1!=FWRITE_FLOAT(r))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(g))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(b))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission color error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(a))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission color error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_em_temp_tag = NOX_BIN_MLAY_EMISS_TEMP;
	if (1!=FWRITE_USHORT(nox_mlay_em_temp_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission temperature tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int emTemp = 6000;
	if (1!=FWRITE_UINT(emTemp))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission temperature error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_em_pow_tag = NOX_BIN_MLAY_EMISS_POWER;
	if (1!=FWRITE_USHORT(nox_mlay_em_pow_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission power tag error."), _T("Error"), 0);
		return false;
	}
	float emPow = 100;
	if (1!=FWRITE_FLOAT(emPow))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission power error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_em_unit_tag = NOX_BIN_MLAY_EMISS_UNIT;
	if (1!=FWRITE_USHORT(nox_mlay_em_unit_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission unit tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int emUnit = 1;
	if (1!=FWRITE_UINT(emUnit))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission unit error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_refl0_tag = NOX_BIN_MLAY_REFL0_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_refl0_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer color0 texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short r0texOn = 0;
	if (1!=FWRITE_USHORT(r0texOn))
	{
		MessageBox(0, _T("Writing non-NOX material layer color0 texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_refl90_tag = NOX_BIN_MLAY_REFL90_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_refl90_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer color90 texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short r90texOn = 0;
	if (1!=FWRITE_USHORT(r90texOn))
	{
		MessageBox(0, _T("Writing non-NOX material layer color90 texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_rough_tag = NOX_BIN_MLAY_ROUGH_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_rough_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer roughness texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short roughTexOn = 0;
	if (1!=FWRITE_USHORT(roughTexOn))
	{
		MessageBox(0, _T("Writing non-NOX material layer roughness texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_weight_tag = NOX_BIN_MLAY_WEIGHT_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_weight_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer contribution texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short weightTexOn = 0;
	if (1!=FWRITE_USHORT(weightTexOn))
	{
		MessageBox(0, _T("Writing non-NOX material layer contribution texture on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mlay_tex_on_light_tag = NOX_BIN_MLAY_LIGHT_TEX_ON;
	if (1!=FWRITE_USHORT(nox_mlay_tex_on_light_tag))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission texture on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short lightTexOn = 0;
	if (1!=FWRITE_USHORT(lightTexOn))
	{
		MessageBox(0, _T("Writing non-NOX material layer emission texture on error."), _T("Error"), 0);
		return false;
	}


	// VERIFY
	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != layerSize)
	{
		const TCHAR * matname = NULL;
		if (mat)
			matname = mat->GetName();
		if (!matname)
			matname = _T("Unnamed");
		TCHAR errmsg[1024];
		_stprintf_s(errmsg, 1024, _T("Export error. Layer chunk in non-NOX material (%s) size is incorrect.\nShould be %llu instead of %llu bytes."), matname, layerSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertTexturePart(FILE * file, unsigned int nummat, unsigned int numlayer, unsigned int slot)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	Mtl * mat = NULL;
	
	if (slot != TEX_SLOT_ENV)
	{
		if (nummat>=(int)mats.size() || nummat<0)
		{
			MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
			return false;
		}

		mat = mats[nummat];
		if (!mat)
		{
			MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
			return false;
		}

		bool isNOXmat = matTypes[nummat];
		if (!isNOXmat)
		{
			MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
			return false;
		}
	}

	NOXMaterial * nmat = (NOXMaterial*)mat;
	char * texffname = NULL;
	unsigned short enabled = 0;
	unsigned short filter = 0;
	unsigned short precalculate = 0;
	float post_brightness = 0.0f;
	float post_contrast = 1.0f;
	float post_saturation = 0.0f;
	float post_hue = 0.0f;
	float post_red = 0.0f;
	float post_green = 0.0f;
	float post_blue = 0.0f;
	float post_gamma = 0.0f;
	unsigned short post_invert = 0;
	float post_normal_power = 0.0f;
	unsigned short post_normal_invert_x = 0;
	unsigned short post_normal_invert_y = 0;

	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			
			texffname = nmat->mat.getLayerTexRefl0(numlayer);	
			enabled = nmat->mat.getLayerUseTexRefl0(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterRefl0(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateRefl0(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessRefl0(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastRefl0(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationRefl0(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueRefl0(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedRefl0(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenRefl0(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueRefl0(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaRefl0(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertRefl0(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_COLOR90:		
			texffname = nmat->mat.getLayerTexRefl90(numlayer);	
			enabled = nmat->mat.getLayerUseTexRefl90(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterRefl90(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateRefl90(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessRefl90(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastRefl90(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationRefl90(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueRefl90(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedRefl90(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenRefl90(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueRefl90(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaRefl90(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertRefl90(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_TRANSM:
			texffname = nmat->mat.getLayerTexTransm(numlayer);
			enabled = nmat->mat.getLayerUseTexTransm(numlayer) ? 1 : 0;
			filter = nmat->mat.getLayerTexPostFilterTransm(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateTransm(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessTransm(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastTransm(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationTransm(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueTransm(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedTransm(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenTransm(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueTransm(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaTransm(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertTransm(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_ROUGHNESS:		
			texffname = nmat->mat.getLayerTexRough(numlayer);	
			enabled = nmat->mat.getLayerUseTexRough(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterRough(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateRough(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessRough(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastRough(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationRough(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueRough(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedRough(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenRough(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueRough(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaRough(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertRough(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_ANISOTROPY:
			texffname = nmat->mat.getLayerTexAniso(numlayer);	
			enabled = nmat->mat.getLayerUseTexAniso(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterAniso(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateAniso(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessAniso(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastAniso(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationAniso(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueAniso(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedAniso(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenAniso(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueAniso(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaAniso(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertAniso(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:
			texffname = nmat->mat.getLayerTexAnisoAngle(numlayer);	
			enabled = nmat->mat.getLayerUseTexAnisoAngle(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterAnisoAngle(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateAnisoAngle(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessAnisoAngle(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastAnisoAngle(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationAnisoAngle(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueAnisoAngle(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedAnisoAngle(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenAnisoAngle(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueAnisoAngle(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaAnisoAngle(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertAnisoAngle(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_WEIGHT:			
			texffname = nmat->mat.getLayerTexWeight(numlayer);	
			enabled = nmat->mat.getLayerUseTexWeight(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterWeight(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateWeight(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessWeight(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastWeight(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationWeight(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueWeight(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedWeight(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenWeight(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueWeight(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaWeight(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertWeight(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_LIGHT:			
			texffname = nmat->mat.getLayerTexLight(numlayer);	
			enabled = nmat->mat.getLayerUseTexLight(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterLight(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateLight(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessLight(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastLight(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationLight(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueLight(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedLight(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenLight(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueLight(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaLight(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertLight(numlayer) ? 1 : 0;
			break;
		case LAYER_TEX_SLOT_NORMAL:
			texffname = nmat->mat.getLayerTexNormal(numlayer);	
			enabled = nmat->mat.getLayerUseTexNormal(numlayer) ? 1 : 0;	
			filter = nmat->mat.getLayerTexPostFilterNormal(numlayer) ? 1 : 0;
			precalculate = nmat->mat.getLayerTexPrecalculateNormal(numlayer) ? 1 : 0;
			post_brightness = nmat->mat.getLayerTexPostBrightnessNormal(numlayer);
			post_contrast	= nmat->mat.getLayerTexPostContrastNormal(numlayer);
			post_saturation = nmat->mat.getLayerTexPostSaturationNormal(numlayer);
			post_hue		= nmat->mat.getLayerTexPostHueNormal(numlayer);
			post_red		= nmat->mat.getLayerTexPostRedNormal(numlayer);
			post_green		= nmat->mat.getLayerTexPostGreenNormal(numlayer);
			post_blue		= nmat->mat.getLayerTexPostBlueNormal(numlayer);
			post_gamma		= nmat->mat.getLayerTexPostGammaNormal(numlayer);
			post_invert		= nmat->mat.getLayerTexPostInvertNormal(numlayer) ? 1 : 0;
			post_normal_power = nmat->mat.getLayerTexPostNormalPower(numlayer);
			post_normal_invert_x = nmat->mat.getLayerTexPostNormalInvertX(numlayer) ? 1 : 0;
			post_normal_invert_y = nmat->mat.getLayerTexPostNormalInvertX(numlayer) ? 1 : 0;
			break;
		case MAT_TEX_SLOT_OPACITY:			
			texffname = nmat->mat.getTexOpacity();				
			enabled = nmat->mat.getUseTexOpacity() ? 1 : 0;
			filter = nmat->mat.getTexPostFilterOpacity() ? 1 : 0;
			precalculate = nmat->mat.getTexPrecalculateOpacity() ? 1 : 0;
			post_brightness = nmat->mat.getTexPostBrightnessOpacity();
			post_contrast	= nmat->mat.getTexPostContrastOpacity();
			post_saturation = nmat->mat.getTexPostSaturationOpacity();
			post_hue		= nmat->mat.getTexPostHueOpacity();
			post_red		= nmat->mat.getTexPostRedOpacity();
			post_green		= nmat->mat.getTexPostGreenOpacity();
			post_blue		= nmat->mat.getTexPostBlueOpacity();
			post_gamma		= nmat->mat.getTexPostGammaOpacity();
			post_invert		= nmat->mat.getTexPostInvertOpacity() ? 1 : 0;
			break;
		case MAT_TEX_SLOT_DISPLACEMENT:			
			texffname = nmat->mat.getTexDisplacement();				
			enabled = nmat->mat.getUseTexDisplacement() ? 1 : 0;
			filter = nmat->mat.getTexPostFilterDisplacement() ? 1 : 0;
			precalculate = 0;//nmat->mat.getTexPrecalculated() ? 1 : 0;
			post_brightness = nmat->mat.getTexPostBrightnessDisplacement();
			post_contrast	= nmat->mat.getTexPostContrastDisplacement();
			post_saturation = nmat->mat.getTexPostSaturationDisplacement();
			post_hue		= nmat->mat.getTexPostHueDisplacement();
			post_red		= nmat->mat.getTexPostRedDisplacement();
			post_green		= nmat->mat.getTexPostGreenDisplacement();
			post_blue		= nmat->mat.getTexPostBlueDisplacement();
			post_gamma		= nmat->mat.getTexPostGammaDisplacement();
			post_invert		= nmat->mat.getTexPostInvertDisplacement() ? 1 : 0;
			break;
		case TEX_SLOT_ENV:		
			{
				if (!sunsky)
					return true;

				NOXSunLight * sun = NULL;
				ObjectState objSt = sunsky->EvalWorldState(GetCOREInterface()->GetTime());
				if (objSt.obj == NULL)
				{
					MessageBox(0, _T("Error (1) while extracting SunSky."), _T("Error"), 0);
					return false;
				}
				sun = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
				if (!sun)
				{
					MessageBox(0, _T("Error (2) while extracting SunSky."), _T("Error"), 0);
					return false;
				}

				texffname = sun->trsunsky.getEnvTexFilename();
				enabled = sun->trsunsky.getEnvEnabled() ? 1 : 0;
				filter = sun->trsunsky.getEnvTexPostInterpolateProbe() ? 1 : 0;
				precalculate = 0;
				post_brightness = sun->trsunsky.getEnvTexPostBrightness();
				post_contrast	= sun->trsunsky.getEnvTexPostContrast();
				post_saturation = sun->trsunsky.getEnvTexPostSaturation();
				post_hue		= sun->trsunsky.getEnvTexPostHue();
				post_red		= sun->trsunsky.getEnvTexPostRed();
				post_green		= sun->trsunsky.getEnvTexPostGreen();
				post_blue		= sun->trsunsky.getEnvTexPostBlue();
				post_gamma		= sun->trsunsky.getEnvTexPostGamma();
				post_invert		= sun->trsunsky.getEnvTexPostInvert() ? 1 : 0;
			}
			break;
	}

	unsigned int ltex = texffname ? ((int)strlen(texffname)+6) : 0;
	unsigned long long texSize = evalSizeTexture(nummat, numlayer, slot);
	unsigned long long chunkStart = _ftelli64(file);


	unsigned short nox_texture_tag = NOX_BIN_MLAY_TEXTURE;
	if (1!=FWRITE_USHORT(nox_texture_tag))
	{
		MessageBox(0, _T("Writing texture tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(texSize))
	{
		MessageBox(0, _T("Writing texture size error."), _T("Error"), 0);
		return false;
	}
	unsigned short texType = 0;
	if (1!=FWRITE_USHORT(texType))
	{
		MessageBox(0, _T("Writing texture type error."), _T("Error"), 0);
		return false;
	}
	unsigned short texSlot = slot;
	if (1!=FWRITE_USHORT(texSlot))
	{
		MessageBox(0, _T("Writing texture slot error."), _T("Error"), 0);
		return false;
	}

	if (ltex>0)
	{
		unsigned short nox_tex_filename_tag = NOX_BIN_MLAY_TEX_FILENAME;
		if (1!=FWRITE_USHORT(nox_tex_filename_tag))
		{
			MessageBox(0, _T("Writing texture filename tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(ltex))
		{
			MessageBox(0, _T("Writing texture filename size error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_STRING(texffname, (ltex-6)))
		{
			MessageBox(0, _T("Writing texture filaname error."), _T("Error"), 0);
			return false;
		}
	}
	
	unsigned short nox_tex_enabled_tag = NOX_BIN_MLAY_TEX_ENABLED;
	if (1!=FWRITE_USHORT(nox_tex_enabled_tag))
	{
		MessageBox(0, _T("Writing texture enabled tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(enabled))
	{
		MessageBox(0, _T("Writing texture enabled error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_tex_filter_tag = NOX_BIN_MLAY_TEX_FILTER;
	if (1!=FWRITE_USHORT(nox_tex_filter_tag))
	{
		MessageBox(0, _T("Writing texture filter tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(filter))
	{
		MessageBox(0, _T("Writing texture filter error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_tex_precalc_tag = NOX_BIN_MLAY_TEX_PRECALCULATE;
	if (1!=FWRITE_USHORT(nox_tex_precalc_tag))
	{
		MessageBox(0, _T("Writing texture precalculate tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_USHORT(filter))
	{
		MessageBox(0, _T("Writing texture precalculate error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_tex_post_tag = NOX_BIN_MLAY_TEX_POST;
	if (1!=FWRITE_USHORT(nox_tex_post_tag))
	{
		MessageBox(0, _T("Writing texture post tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int psize = 58;
	if (1!=FWRITE_UINT(psize))
	{
		MessageBox(0, _T("Writing texture post size error."), _T("Error"), 0);
		return false;
	}

		unsigned short nox_tex_post_brightness_tag = NOX_BIN_MLAY_TEX_POST_BRIGHTNESS;
		if (1!=FWRITE_USHORT(nox_tex_post_brightness_tag))
		{
			MessageBox(0, _T("Writing texture post brightness tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_brightness))
		{
			MessageBox(0, _T("Writing texture post brightness error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_contrast_tag = NOX_BIN_MLAY_TEX_POST_CONTRAST;
		if (1!=FWRITE_USHORT(nox_tex_post_contrast_tag))
		{
			MessageBox(0, _T("Writing texture post contrast tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_contrast))
		{
			MessageBox(0, _T("Writing texture post contrast error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_saturation_tag = NOX_BIN_MLAY_TEX_POST_SATURATION;
		if (1!=FWRITE_USHORT(nox_tex_post_saturation_tag))
		{
			MessageBox(0, _T("Writing texture post saturation tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_saturation))
		{
			MessageBox(0, _T("Writing texture post saturation error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_hue_tag = NOX_BIN_MLAY_TEX_POST_HUE;
		if (1!=FWRITE_USHORT(nox_tex_post_hue_tag))
		{
			MessageBox(0, _T("Writing texture post hue tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_hue))
		{
			MessageBox(0, _T("Writing texture post hue error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_red_tag = NOX_BIN_MLAY_TEX_POST_RED;
		if (1!=FWRITE_USHORT(nox_tex_post_red_tag))
		{
			MessageBox(0, _T("Writing texture post red tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_red))
		{
			MessageBox(0, _T("Writing texture post red error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_green_tag = NOX_BIN_MLAY_TEX_POST_GREEN;
		if (1!=FWRITE_USHORT(nox_tex_post_green_tag))
		{
			MessageBox(0, _T("Writing texture post green tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_green))
		{
			MessageBox(0, _T("Writing texture post green error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_blue_tag = NOX_BIN_MLAY_TEX_POST_BLUE;
		if (1!=FWRITE_USHORT(nox_tex_post_blue_tag))
		{
			MessageBox(0, _T("Writing texture post blue tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_blue))
		{
			MessageBox(0, _T("Writing texture post blue error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_gamma_tag = NOX_BIN_MLAY_TEX_POST_GAMMA;
		if (1!=FWRITE_USHORT(nox_tex_post_gamma_tag))
		{
			MessageBox(0, _T("Writing texture post gamma tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_gamma))
		{
			MessageBox(0, _T("Writing texture post gamma error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_tex_post_invert_tag = NOX_BIN_MLAY_TEX_POST_INVERT;
		if (1!=FWRITE_USHORT(nox_tex_post_invert_tag))
		{
			MessageBox(0, _T("Writing texture post invert tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_USHORT(post_invert))
		{
			MessageBox(0, _T("Writing texture post invert error."), _T("Error"), 0);
			return false;
		}

	unsigned short nox_norm_post_tag = NOX_BIN_MLAY_NORM_POST;
	if (1!=FWRITE_USHORT(nox_norm_post_tag))
	{
		MessageBox(0, _T("Writing normal post tag error."), _T("Error"), 0);
		return false;
	}
	unsigned int psize2 = 20;
	if (1!=FWRITE_UINT(psize2))
	{
		MessageBox(0, _T("Writing normal post size error."), _T("Error"), 0);
		return false;
	}

		unsigned short nox_norm_post_power_tag = NOX_BIN_MLAY_NORM_POST_POWER_EV;
		if (1!=FWRITE_USHORT(nox_norm_post_power_tag))
		{
			MessageBox(0, _T("Writing normal post power tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(post_normal_power))
		{
			MessageBox(0, _T("Writing normal post power error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_norm_post_invertx_tag = NOX_BIN_MLAY_NORM_POST_INVERT_X;
		if (1!=FWRITE_USHORT(nox_norm_post_invertx_tag))
		{
			MessageBox(0, _T("Writing normal post invert x tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_USHORT(post_normal_invert_x))
		{
			MessageBox(0, _T("Writing normal post invert x error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_norm_post_inverty_tag = NOX_BIN_MLAY_NORM_POST_INVERT_Y;
		if (1!=FWRITE_USHORT(nox_norm_post_inverty_tag))
		{
			MessageBox(0, _T("Writing normal post invert y tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_USHORT(post_normal_invert_y))
		{
			MessageBox(0, _T("Writing normal post invert y error."), _T("Error"), 0);
			return false;
		}


	// VERIFY
	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != texSize)
	{
		TCHAR errmsg[1024];
		_stprintf_s(errmsg, 1024, _T("Export error. Texture size is incorrect.\nShould be %llu instead of %llu bytes."), texSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertMatPreviewPart(FILE * file, unsigned int numMat)
{

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertSunskyPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (!sunsky)
	{
		MessageBox(0, _T("Error. Trying to insert SunSky, but not found on scene."), _T("Error"), 0);
		return false;
	}

	NOXSunLight * sun = NULL;
	ObjectState objSt = sunsky->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
	{
		MessageBox(0, _T("Error (1) while extracting SunSky."), _T("Error"), 0);
		return false;
	}
	sun = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
	if (!sun)
	{
		MessageBox(0, _T("Error (1) while extracting SunSky."), _T("Error"), 0);
		return false;
	}

	float azi, alt;
	sun->evalAltitudeAzimuth(alt, azi);
	sun->trsunsky.setPosFromDir(alt, azi);


	unsigned long long sskysize = evalSizeSunsky();
	unsigned long long chunkStart = _ftelli64(file);


	unsigned short nox_sunsky_tag = NOX_BIN_SUNSKY;
	if (1!=FWRITE_USHORT(nox_sunsky_tag))
	{
		MessageBox(0, _T("Writing sunsky tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(sskysize))
	{
		MessageBox(0, _T("Writing sunsky size error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_on = NOX_BIN_SS_ON;
	if (1!=FWRITE_USHORT(nox_ss_on))
	{
		MessageBox(0, _T("Writing sunsky on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short useSun = sun->trsunsky.getSunSkyON() ? 1 : 0;
	if (1!=FWRITE_USHORT(useSun))
	{
		MessageBox(0, _T("Writing sunsky on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_month = NOX_BIN_SS_MONTH;
	if (1!=FWRITE_USHORT(nox_ss_month))
	{
		MessageBox(0, _T("Writing sunsky month tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short month = sun->trsunsky.getMonth();
	if (1!=FWRITE_USHORT(month))
	{
		MessageBox(0, _T("Writing sunsky month error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_day = NOX_BIN_SS_DAY;
	if (1!=FWRITE_USHORT(nox_ss_day))
	{
		MessageBox(0, _T("Writing sunsky day tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short day = sun->trsunsky.getDay();
	if (1!=FWRITE_USHORT(day))
	{
		MessageBox(0, _T("Writing sunsky day error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_hour = NOX_BIN_SS_HOUR;
	if (1!=FWRITE_USHORT(nox_ss_hour))
	{
		MessageBox(0, _T("Writing sunsky hour tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short hour = sun->trsunsky.getHour();
	if (1!=FWRITE_USHORT(hour))
	{
		MessageBox(0, _T("Writing sunsky hour error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_minute = NOX_BIN_SS_MINUTE;
	if (1!=FWRITE_USHORT(nox_ss_minute))
	{
		MessageBox(0, _T("Writing sunsky minute tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short minute = sun->trsunsky.getMinute();
	if (1!=FWRITE_USHORT(minute))
	{
		MessageBox(0, _T("Writing sunsky minute error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_gmt = NOX_BIN_SS_GMT;
	if (1!=FWRITE_USHORT(nox_ss_gmt))
	{
		MessageBox(0, _T("Writing sunsky gmt tag error."), _T("Error"), 0);
		return false;
	}
	short gmt = -sun->trsunsky.getGMT();
	if (1!=FWRITE_USHORT((unsigned short &)gmt))
	{
		MessageBox(0, _T("Writing sunsky gmt error."), _T("Error"), 0);
		return false;
	}


	unsigned short nox_ss_longitude = NOX_BIN_SS_LONGITUDE;
	if (1!=FWRITE_USHORT(nox_ss_longitude))
	{
		MessageBox(0, _T("Writing sunsky longitude tag error."), _T("Error"), 0);
		return false;
	}
	float longitude = sun->trsunsky.getLongitude();
	if (1!=FWRITE_FLOAT(longitude))
	{
		MessageBox(0, _T("Writing sunsky longitude error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_latitude = NOX_BIN_SS_LATITUDE;
	if (1!=FWRITE_USHORT(nox_ss_latitude))
	{
		MessageBox(0, _T("Writing sunsky latitude tag error."), _T("Error"), 0);
		return false;
	}
	float latitude = sun->trsunsky.getLatitude();
	if (1!=FWRITE_FLOAT(latitude))
	{
		MessageBox(0, _T("Writing sunsky latitude error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_turbidity = NOX_BIN_SS_TURBIDITY;
	if (1!=FWRITE_USHORT(nox_ss_turbidity))
	{
		MessageBox(0, _T("Writing sunsky turbidity tag error."), _T("Error"), 0);
		return false;
	}
	float turbidity = sun->trsunsky.getTurbidity();
	if (1!=FWRITE_FLOAT(turbidity))
	{
		MessageBox(0, _T("Writing sunsky turbidity error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_aerosol = NOX_BIN_SS_AEROSOL;
	if (1!=FWRITE_USHORT(nox_ss_aerosol))
	{
		MessageBox(0, _T("Writing sunsky aerosol tag error."), _T("Error"), 0);
		return false;
	}
	float aerosol = sun->trsunsky.getAerosol();
	if (1!=FWRITE_FLOAT(aerosol))
	{
		MessageBox(0, _T("Writing sunsky aerosol error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_albedo = NOX_BIN_SS_ALBEDO;
	if (1!=FWRITE_USHORT(nox_ss_albedo))
	{
		MessageBox(0, _T("Writing sunsky albedo tag error."), _T("Error"), 0);
		return false;
	}
	float albedo = sun->trsunsky.getAlbedo();
	if (1!=FWRITE_FLOAT(albedo))
	{
		MessageBox(0, _T("Writing sunsky albedo error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_sunsize = NOX_BIN_SS_SUN_SIZE;
	if (1!=FWRITE_USHORT(nox_ss_sunsize))
	{
		MessageBox(0, _T("Writing sunsky sun diameter tag error."), _T("Error"), 0);
		return false;
	}
	float sunsize = sun->trsunsky.getSunSize();
	if (1!=FWRITE_FLOAT(sunsize))
	{
		MessageBox(0, _T("Writing sunsky sun diameter error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_sunON = NOX_BIN_SS_SUN_ON;
	if (1!=FWRITE_USHORT(nox_ss_sunON))
	{
		MessageBox(0, _T("Writing sunsky sun on tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short sunON = sun->trsunsky.getSunON() ? 1 : 0;
	if (1!=FWRITE_USHORT(sunON))
	{
		MessageBox(0, _T("Writing sunsky sun on error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_sunOL = NOX_BIN_SS_SUN_OTHER_LAYER;
	if (1!=FWRITE_USHORT(nox_ss_sunOL))
	{
		MessageBox(0, _T("Writing sunsky sun other blend layer tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short sunOL = sun->trsunsky.getSunOtherLayerON() ? 1 : 0;
	if (1!=FWRITE_USHORT(sunOL))
	{
		MessageBox(0, _T("Writing sunsky sun other blend layer error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_ss_model = NOX_BIN_SS_MODEL;
	if (1!=FWRITE_USHORT(nox_ss_model))
	{
		MessageBox(0, _T("Writing sunsky model tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short skymodel = sun->trsunsky.getModel();
	if (1!=FWRITE_USHORT(skymodel))
	{
		MessageBox(0, _T("Writing sunsky model error."), _T("Error"), 0);
		return false;
	}



	NOXSunTarget * targ = sun->getTargetObject();
	if (!targ)
	{
		MessageBox(0, _T("Sunsky error. Can't eval cardinal directions."), _T("Error"), 0);
		return false;
	}

	Point3 dirN, dirE;
	targ->evalDirections(dirN, dirE);

	#ifdef NOX_SWAP_YZ
		float temp;
		temp   = dirN.y;
		dirN.y = dirN.z;
		dirN.z = -temp;
		temp   = dirE.y;
		dirE.y = dirE.z;
		dirE.z = -temp;
	#endif

	unsigned short nox_ss_dirs = NOX_BIN_SS_DIRECTIONS;
	if (1!=FWRITE_USHORT(nox_ss_dirs))
	{
		MessageBox(0, _T("Writing sunsky directions tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirN.x))
	{
		MessageBox(0, _T("Writing sunsky directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirN.y))
	{
		MessageBox(0, _T("Writing sunsky directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirN.z))
	{
		MessageBox(0, _T("Writing sunsky directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirE.x))
	{
		MessageBox(0, _T("Writing sunsky directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirE.y))
	{
		MessageBox(0, _T("Writing sunsky directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirE.z))
	{
		MessageBox(0, _T("Writing sunsky directions error."), _T("Error"), 0);
		return false;
	}

	if (sun != objSt.obj)
		sun->DeleteMe();

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sskysize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. SunSky chunk size is incorrect.\nShould be %llu instead of %llu bytes."), sskysize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertBlendLayersPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	unsigned long long allblendssize = evalSizeBlendLayers();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_blends_tag = NOX_BIN_BLEND_LAYERS;
	if (1!=FWRITE_USHORT(nox_blends_tag))
	{
		MessageBox(0, _T("Writing Blend Layers tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(allblendssize))
	{
		MessageBox(0, _T("Writing blend layers size error."), _T("Error"), 0);
		return false;
	}

	TrMaterial tmat;
	for (unsigned int i=0; i<16; i++)
	{
		char * tname = tmat.getBlendLayerName(i);
		if (!tname)
			tname = "Unnamed";
		
		unsigned int tlen = (unsigned int)strlen(tname);
		if (tlen<1)
		{
			tname = "Unnamed";
			tlen = 7;	// "Unnamed"
		}
		tlen +=6;

		unsigned long long layersize = tlen + 42;
		unsigned short nox_blend_tag = NOX_BIN_BLEND_LAYER;
		if (1!=FWRITE_USHORT(nox_blend_tag))
		{
			MessageBox(0, _T("Writing Blend Layer tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_ULONG8(layersize))
		{
			MessageBox(0, _T("Writing blend layer size error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(i))
		{
			MessageBox(0, _T("Writing blend layer ID error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_blend_on_tag = NOX_BIN_BLEND_LAYER_ON;
		if (1!=FWRITE_USHORT(nox_blend_on_tag))
		{
			MessageBox(0, _T("Writing Blend Layer On tag error."), _T("Error"), 0);
			return false;
		}
		unsigned short isOn = 1;
		if (1!=FWRITE_USHORT(isOn))
		{
			MessageBox(0, _T("Writing blend layer on error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_blend_layer_name_tag = NOX_BIN_BLEND_LAYER_NAME;
		if (1!=FWRITE_USHORT(nox_blend_layer_name_tag))
		{
			MessageBox(0, _T("Writing Blend Layer Name tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(tlen))
		{
			MessageBox(0, _T("Writing blend layer name size error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_STRING(tname, (tlen-6)))
		{
			MessageBox(0, _T("Writing blend layer name error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_blend_layer_weight_tag = NOX_BIN_BLEND_LAYER_WEIGHT;
		if (1!=FWRITE_USHORT(nox_blend_layer_weight_tag))
		{
			MessageBox(0, _T("Writing Blend Layer Weight tag error."), _T("Error"), 0);
			return false;
		}
		float weight = 100.0f;
		if (1!=FWRITE_FLOAT(weight))
		{
			MessageBox(0, _T("Writing blend layer weight error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_blend_layer_red_tag = NOX_BIN_BLEND_LAYER_RED;
		if (1!=FWRITE_USHORT(nox_blend_layer_red_tag))
		{
			MessageBox(0, _T("Writing Blend Layer Red tag error."), _T("Error"), 0);
			return false;
		}
		float red = 100.0f;
		if (1!=FWRITE_FLOAT(red))
		{
			MessageBox(0, _T("Writing blend layer red error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_blend_layer_green_tag = NOX_BIN_BLEND_LAYER_GREEN;
		if (1!=FWRITE_USHORT(nox_blend_layer_green_tag))
		{
			MessageBox(0, _T("Writing Blend Layer Green tag error."), _T("Error"), 0);
			return false;
		}
		float green = 100.0f;
		if (1!=FWRITE_FLOAT(green))
		{
			MessageBox(0, _T("Writing blend layer green error."), _T("Error"), 0);
			return false;
		}

		unsigned short nox_blend_layer_blue_tag = NOX_BIN_BLEND_LAYER_BLUE;
		if (1!=FWRITE_USHORT(nox_blend_layer_blue_tag))
		{
			MessageBox(0, _T("Writing Blend Layer Blue tag error."), _T("Error"), 0);
			return false;
		}
		float blue = 100.0f;
		if (1!=FWRITE_FLOAT(blue))
		{
			MessageBox(0, _T("Writing blend layer blue error."), _T("Error"), 0);
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != allblendssize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Blend layers chunk size is incorrect.\nShould be %llu instead of %llu bytes."), allblendssize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertSceneInfoPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	Renderer * rend = GetCOREInterface()->GetRenderer(RS_Production);		
	if (rend->ClassID() != NOXRenderer_CLASS_ID)
		return 0;

	char pswd[64];
	sprintf_s(pswd, 64, "%d", numTris);
	unsigned int lpswd = (unsigned int)strlen(pswd);

	NOXRenderer * noxrend = (NOXRenderer*)rend;
	unsigned int size_scene_name   = (noxrend->scene_name) ?			6+(unsigned int)strlen(noxrend->scene_name)			: 0;
	unsigned int size_scene_author = (noxrend->scene_author_name) ?		6+(unsigned int)strlen(noxrend->scene_author_name)	: 0;
	unsigned int size_scene_email  = (noxrend->scene_author_email) ?	6+(unsigned int)strlen(noxrend->scene_author_email) : 0;
	unsigned int size_scene_www    = (noxrend->scene_author_www) ?		6+(unsigned int)strlen(noxrend->scene_author_www)	: 0;
	
	unsigned long long sceneinfoSize = 10 + size_scene_name + size_scene_author + size_scene_email + size_scene_www;
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_sceneinfo_tag = NOX_BIN_SCENE_INFO;
	if (1!=FWRITE_USHORT(nox_sceneinfo_tag))
	{
		MessageBox(0, _T("Writing Scene Info tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(sceneinfoSize))
	{
		MessageBox(0, _T("Writing Scene Info size error."), _T("Error"), 0);
		return false;
	}

	if (noxrend->scene_name)
	{
		unsigned short nox_scenename_tag = NOX_BIN_SCENEINFO_SCENENAME;
		if (1!=FWRITE_USHORT(nox_scenename_tag))
		{
			MessageBox(0, _T("Writing Scene Name tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_name))
		{
			MessageBox(0, _T("Writing Scene Name size error."), _T("Error"), 0);
			return false;
		}
		char * decString = xorString(noxrend->scene_name, pswd, size_scene_name-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_name-6))
		{
			free(decString);
			MessageBox(0, _T("Writing Scene Name error."), _T("Error"), 0);
			return false;
		}
		free(decString);
	}

	if (noxrend->scene_author_name)
	{
		unsigned short nox_sceneauthor_tag = NOX_BIN_SCENEINFO_AUTHOR;
		if (1!=FWRITE_USHORT(nox_sceneauthor_tag))
		{
			MessageBox(0, _T("Writing Scene Author tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_author))
		{
			MessageBox(0, _T("Writing Scene Author size error."), _T("Error"), 0);
			return false;
		}
		char * decString = xorString(noxrend->scene_author_name, pswd, size_scene_author-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_author-6))
		{
			free(decString);
			MessageBox(0, _T("Writing Scene Author error."), _T("Error"), 0);
			return false;
		}
		free(decString);
	}

	if (noxrend->scene_author_email)
	{
		unsigned short nox_sceneemail_tag = NOX_BIN_SCENEINFO_EMAIL;
		if (1!=FWRITE_USHORT(nox_sceneemail_tag))
		{
			MessageBox(0, _T("Writing Scene Author Email tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_email))
		{
			MessageBox(0, _T("Writing Scene Author Email size error."), _T("Error"), 0);
			return false;
		}
		char * decString = xorString(noxrend->scene_author_email, pswd, size_scene_email-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_email-6))
		{
			free(decString);
			MessageBox(0, _T("Writing Scene Author Email error."), _T("Error"), 0);
			return false;
		}
		free(decString);
	}

	if (noxrend->scene_author_www)
	{
		unsigned short nox_scenewww_tag = NOX_BIN_SCENEINFO_WWW;
		if (1!=FWRITE_USHORT(nox_scenewww_tag))
		{
			MessageBox(0, _T("Writing Scene Author Webpage tag error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_UINT(size_scene_www))
		{
			MessageBox(0, _T("Writing Scene Author Webpage size error."), _T("Error"), 0);
			return false;
		}
		char * decString = xorString(noxrend->scene_author_www, pswd, size_scene_www-6, lpswd);
		if (1!=FWRITE_STRING(decString, size_scene_www-6))
		{
			free(decString);
			MessageBox(0, _T("Writing Scene Author Webpage error."), _T("Error"), 0);
			return false;
		}
		free(decString);
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sceneinfoSize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Scene Info chunk size is incorrect.\nShould be %llu instead of %llu bytes."), sceneinfoSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertSceneSettingsPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	Renderer * rend = GetCOREInterface()->GetRenderer(RS_Production);		
	if (rend->ClassID() != NOXRenderer_CLASS_ID)
		return 0;
	NOXRenderer * noxrend = (NOXRenderer*)rend;
	
	unsigned long long sceneSettingsSize = evalSizeSceneSettings();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_scenesett_tag = NOX_BIN_SCENE_SETTINGS;
	if (1!=FWRITE_USHORT(nox_scenesett_tag))
	{
		MessageBox(0, _T("Writing Scene Settings tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(sceneSettingsSize))
	{
		MessageBox(0, _T("Writing Scene Settings size error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mb_enabled_tag = NOX_BIN_SCENESETT_MOTION_BLUR_ON;
	if (1!=FWRITE_USHORT(nox_mb_enabled_tag))
	{
		MessageBox(0, _T("Writing Motion Blur Enabled tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short mb_enabled = noxrend->mb_on ? 1 : 0;
	if (1!=FWRITE_USHORT(mb_enabled))
	{
		MessageBox(0, _T("Writing Motion Blur Enabled error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mb_time_tag = NOX_BIN_SCENESETT_MOTION_BLUR_TIME;
	if (1!=FWRITE_USHORT(nox_mb_time_tag))
	{
		MessageBox(0, _T("Writing Motion Blur Time tag error."), _T("Error"), 0);
		return false;
	}

	float mb_time = noxrend->mb_duration / (float)GetFrameRate();
	if (1!=FWRITE_FLOAT(mb_time))
	{
		MessageBox(0, _T("Writing Motion Blur Time error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_mb_max_time_tag = NOX_BIN_SCENESETT_MOTION_BLUR_MAX_TIME;
	if (1!=FWRITE_USHORT(nox_mb_max_time_tag))
	{
		MessageBox(0, _T("Writing Motion Blur Max Time tag error."), _T("Error"), 0);
		return false;
	}

	if (1!=FWRITE_FLOAT(mb_time))	// same time
	{
		MessageBox(0, _T("Writing Motion Blur Max Time error."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != sceneSettingsSize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Scene Settings chunk size is incorrect.\nShould be %llu instead of %llu bytes."), sceneSettingsSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertRendererSettingsPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	Renderer * rend = GetCOREInterface()->GetRenderer(RS_Production);		
	if (rend->ClassID() != NOXRenderer_CLASS_ID)
		return 0;
	NOXRenderer * noxrend = (NOXRenderer*)rend;
	
	unsigned long long rendSettingsSize = evalSizeRendererSettings();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_rendsett_tag = NOX_BIN_RENDERER_SETTINGS;
	if (1!=FWRITE_USHORT(nox_rendsett_tag))
	{
		MessageBox(0, _T("Writing Renderer Settings tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(rendSettingsSize))
	{
		MessageBox(0, _T("Writing Renderer Settings size error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_engine_tag = NOX_BIN_RSETT_ENGINE;
	if (1!=FWRITE_USHORT(nox_engine_tag))
	{
		MessageBox(0, _T("Writing Engine tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short engn = 0;
	switch (noxrend->engine)
	{
		case 0:  engn = 1;	break;
		case 1:  engn = 2;	break;
		case 2:  engn = 10;	break;
		default: engn = 1;	break;
	}
	if (1!=FWRITE_USHORT(engn))
	{
		MessageBox(0, _T("Writing Engine error."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != rendSettingsSize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Renderer Settings chunk size is incorrect.\nShould be %llu instead of %llu bytes."), rendSettingsSize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertUserColorsPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	unsigned long long usercolorssize = evalSizeUserColors();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_usercolors_tag = NOX_BIN_USERCOLORS;
	if (1!=FWRITE_USHORT(nox_usercolors_tag))
	{
		MessageBox(0, _T("Writing User Colors tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(usercolorssize))
	{
		MessageBox(0, _T("Writing User Colors size error."), _T("Error"), 0);
		return false;
	}

	for (int i=0; i<18; i++)
	{
		float r=0.0f, g=0.0f, b=0.0f, a=1.0f;
		TrRaytracer::getUserColors(i, r,g,b);
		unsigned short nox_usersinglecolor_tag = NOX_BIN_USER_SINGLE_COLOR;
		if (1!=FWRITE_USHORT(nox_usersinglecolor_tag))
		{
			MessageBox(0, _T("Writing User Color tag error."), _T("Error"), 0);
			return false;
		}
		unsigned short color_id = i;
		if (1!=FWRITE_USHORT(color_id))
		{
			MessageBox(0, _T("Writing User Color ID error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(r))
		{
			MessageBox(0, _T("Writing User Color R error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(g))
		{
			MessageBox(0, _T("Writing User Color G error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(b))
		{
			MessageBox(0, _T("Writing User Color B error."), _T("Error"), 0);
			return false;
		}
		if (1!=FWRITE_FLOAT(a))
		{
			MessageBox(0, _T("Writing User Color A error."), _T("Error"), 0);
			return false;
		}
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != usercolorssize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. User colors chunk size is incorrect.\nShould be %llu instead of %llu bytes."), usercolorssize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------------------

bool BinaryScenePlugin::insertEnvironmentPart(FILE * file)
{
	if (!file)
	{
		MessageBox(0, _T("Internal plugin error."), _T("Error"), 0);
		return false;
	}

	if (!sunsky)
		return true;

	ObjectState objSt = sunsky->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
	{
		MessageBox(0, _T("Error (1) while extracting SunSky."), _T("Error"), 0);
		return false;
	}
	NOXSunLight * sun = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
	if (!sun)
	{
		MessageBox(0, _T("Error (2) while extracting SunSky."), _T("Error"), 0);
		return false;
	}

	NOXSunTarget * targ = sun->getTargetObject();
	if (!targ)
	{
		MessageBox(0, _T("Environment error. Can't eval cardinal directions."), _T("Error"), 0);
		return false;
	}

	Point3 dirN, dirE;
	targ->evalDirections(dirN, dirE);

	#ifdef NOX_SWAP_YZ
		float temp;
		temp   = dirN.y;
		dirN.y = dirN.z;
		dirN.z = -temp;
		temp   = dirE.y;
		dirE.y = dirE.z;
		dirE.z = -temp;
	#endif


	unsigned long long envsize = evalSizeEnvironmentMap();
	unsigned long long chunkStart = _ftelli64(file);

	unsigned short nox_env_tag = NOX_BIN_ENVIRONMENT_MAP;
	if (1!=FWRITE_USHORT(nox_env_tag))
	{
		MessageBox(0, _T("Writing Environment tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_ULONG8(envsize))
	{
		MessageBox(0, _T("Writing Environment size error."), _T("Error"), 0);
		return false;
	}


	if (!insertTexturePart(file, 0, 0, TEX_SLOT_ENV))
		return false;


	unsigned short nox_env_enabled_tag = NOX_BIN_ENV_ENABLED;
	if (1!=FWRITE_USHORT(nox_env_enabled_tag))
	{
		MessageBox(0, _T("Writing Environment Map Enabled tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short enabled = sun->trsunsky.getEnvEnabled() ? 1 : 0;
	if (1!=FWRITE_USHORT(enabled))
	{
		MessageBox(0, _T("Writing Environment Map Enabled error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_env_blendlayer_tag = NOX_BIN_ENV_BLEND_LAYER;
	if (1!=FWRITE_USHORT(nox_env_blendlayer_tag))
	{
		MessageBox(0, _T("Writing Environment Map Blend Layer tag error."), _T("Error"), 0);
		return false;
	}
	unsigned short blendlayer = sun->trsunsky.getEnvBlendLayer();
	if (1!=FWRITE_USHORT(blendlayer))
	{
		MessageBox(0, _T("Writing Environment Map Blend Layer error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_env_powerev_tag = NOX_BIN_ENV_POWER_EV;
	if (1!=FWRITE_USHORT(nox_env_powerev_tag))
	{
		MessageBox(0, _T("Writing Environment Map Power EV tag error."), _T("Error"), 0);
		return false;
	}
	float powerev = sun->trsunsky.getEnvPower();
	if (1!=FWRITE_FLOAT(powerev))
	{
		MessageBox(0, _T("Writing Environment Map Power EV error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_env_azimuth_tag = NOX_BIN_ENV_AZIMUTH;
	if (1!=FWRITE_USHORT(nox_env_azimuth_tag))
	{
		MessageBox(0, _T("Writing Environment Map Azimuth tag error."), _T("Error"), 0);
		return false;
	}
	float azimuth = sun->trsunsky.getEnvAzimuth();
	if (1!=FWRITE_FLOAT(azimuth))
	{
		MessageBox(0, _T("Writing Environment Map Azimuth error."), _T("Error"), 0);
		return false;
	}

	unsigned short nox_env_directions_tag = NOX_BIN_ENV_DIRECTIONS;
	if (1!=FWRITE_USHORT(nox_env_directions_tag))
	{
		MessageBox(0, _T("Writing Environment Map Directions tag error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirN.x))
	{
		MessageBox(0, _T("Writing Environment Map Directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirN.y))
	{
		MessageBox(0, _T("Writing Environment Map Directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirN.z))
	{
		MessageBox(0, _T("Writing Environment Map Directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirE.x))
	{
		MessageBox(0, _T("Writing Environment Map Directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirE.y))
	{
		MessageBox(0, _T("Writing Environment Map Directions error."), _T("Error"), 0);
		return false;
	}
	if (1!=FWRITE_FLOAT(dirE.z))
	{
		MessageBox(0, _T("Writing Environment Map Directions error."), _T("Error"), 0);
		return false;
	}

	unsigned long long chunkStop = _ftelli64(file);
	unsigned long long realSize = chunkStop - chunkStart;
	if (realSize != envsize)
	{
		TCHAR errmsg[128];
		_stprintf_s(errmsg, 128, _T("Export error. Environment chunk size is incorrect.\nShould be %llu instead of %llu bytes."), envsize, realSize);
		MessageBox(0, errmsg, _T("Error"), 0);
		return false;
	}

	return true;
}


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeScene()
{
	unsigned long long geometrySize = evalSizeGeometry();
	unsigned long long camerasSize = 0;
	for (int i=0; i<(int)cameras.size(); i++)
		camerasSize += evalSizeCamera((unsigned int)i);
	unsigned long long materialsSize = evalSizeMaterials();
	unsigned long long sunskySize = evalSizeSunsky();
	unsigned long long envSize = evalSizeEnvironmentMap();
	unsigned long long blendsSize = evalSizeBlendLayers();
	unsigned long long userColorsSize = evalSizeUserColors();
	unsigned long long sceneInfoSize = evalSizeSceneInfo();
	unsigned long long sceneSettingsSize = evalSizeSceneSettings();
	unsigned long long rendererSettingsSize = evalSizeRendererSettings();


	unsigned long long result = 10 + geometrySize + camerasSize + sunskySize + blendsSize + userColorsSize + sceneInfoSize 
			+ envSize + sceneSettingsSize + rendererSettingsSize;

	rGeometrySize = geometrySize;
	rSceneSize = result;

	return result;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeGeometry()
{
	unsigned int numMeshSrc = (unsigned int)meshsources.size();
	unsigned long long sumMeshSrcSize = 0;
	for (unsigned int i=0; i<numMeshSrc; i++)
		sumMeshSrcSize += evalSizeInstSrc(i);

	unsigned int numMeshInst = (unsigned int)meshinstances.size();
	unsigned long long sumMeshInstSize = 0;
	for (unsigned int i=0; i<numMeshInst; i++)
		sumMeshInstSize += evalSizeInstance(i);

	return sumMeshSrcSize + sumMeshInstSize + 28;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeMesh(unsigned int num)
{
	if (num >= (int)meshes.size() || num < 0)
		return 0;

	INode * node = meshes[num];
	if (!node)
		return 0;

	const TCHAR * name = node->GetName();
	int lname = name ? (int)_tcsclen(name) : 7;

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return false;

	TriObject * tobj = (TriObject *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), triObjectClassID);
	if (!tobj)
		return false;
	Mesh mesh = tobj->GetMesh();
	int numFaces = mesh.numFaces;
	deleteSecondIfCopied(objSt.obj, tobj);

	unsigned long long triSize = evalSizeTriangleOld();
	unsigned long long trisSize = triSize * numFaces;

	unsigned long long all = 14 + trisSize + lname+6;
	return all;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeMeshSrc(unsigned int num)
{
	if (num >= (int)meshsources.size() || num < 0)
		return 0;

	INode * node = meshsources[num].node;
	if (!node)
		return 0;

	const TCHAR * name = node->GetObjectRef()->GetObjectName();
	int lname = name ? (int)_tcsclen(name) : 7;

	ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
	if (objSt.obj == NULL)
		return false;

	TriObject * tobj = (TriObject *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), triObjectClassID);
	if (!tobj)
		return false;
	Mesh mesh = tobj->GetMesh();
	int numFaces = mesh.numFaces;
	deleteSecondIfCopied(objSt.obj, tobj);

	unsigned long long triSize = evalSizeTriangle();
	unsigned long long trisSize = triSize * numFaces;

	unsigned long long all = 14 + trisSize + lname+6;
	return all;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeInstSrc(unsigned int num)
{
	if (num >= (int)meshsources.size() || num < 0)
		return 0;

	unsigned long long meshSize = evalSizeMeshSrc(num);

	unsigned long long all = 14 + meshSize;
	return all;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeInstance(unsigned int num)
{
	if (num >= (int)meshinstances.size() || num < 0)
		return 0;

	INode * node = meshinstances[num].node;
	if (!node)
		return 0;

	const TCHAR * name = node->GetName();
	int lname = name ? (int)_tcsclen(name) : 7;

	unsigned long long res = 14 + 66 +(6+lname);

	int nummats = 1;
	Mtl * mmtl = node->GetMtl();
	if (mmtl)
		if (mmtl->IsMultiMtl())
			nummats = max(1, mmtl->NumSubMtls());
	res += 4 + 4 * nummats;

	res += 36;
	res += 66;

	return res;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeTriangleOld()
{
	return 102;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeTriangle()
{
	return 106;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeCamera(unsigned int num)
{
	if (num >= (int)cameras.size() || num < 0)
		return 0;
	INode * node = cameras[num];
	const TCHAR * name = node->GetName();
	int lname = name ? (int)_tcsclen(name) : 7;

	return 144 + lname+6 + 10 + 36 + 14;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeMaterials()
{
	unsigned long long sumsize = 0;
	unsigned int nummats = (unsigned int)mats.size();
	for (unsigned int i=0; i<nummats; i++)
		sumsize += evalSizeMaterial(i);

	return 14 + sumsize;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeMaterialPreview(unsigned int num)
{
	if (num>=(int)mats.size() || num<0)
		return 0;

	Mtl * mat = mats[num];

	bool isNOXmat = matTypes[num];
	if (!isNOXmat)
		return 0;

	NOXMaterial * nmat = (NOXMaterial*)mat;
	if (!nmat->mat.getPreviewExists())
		return 0;
	return nmat->mat.getPreviewBufSize() + 18;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeMaterial(unsigned int num)
{
	if (num>=(int)mats.size() || num<0)
		return 0;

	Mtl * mat = mats[num];

	bool isNOXmat = matTypes[num];

	if (!mat)
		isNOXmat = false;

	const TCHAR * name = mat ? mat->GetName() : (TCHAR*)NULL;
	int matnamelength = name ? (int)_tcsclen(name) : 0;
	if (matnamelength==0)
		matnamelength = 7;
	matnamelength += 6;

	if (!isNOXmat)
	{
		// no textures, only standard length + name
		int simplelayerlength = 172;
		unsigned long long materialsize = 40 + matnamelength + simplelayerlength;
		return materialsize;
	}

	NOXMaterial * nmat = (NOXMaterial*)mat;
	char * texOpac = nmat->mat.getTexOpacity();
	int lOpac = texOpac ? ((int)strlen(texOpac)+6) : 0;
	unsigned long long lsOpac = evalSizeTexture(num, 0, MAT_TEX_SLOT_OPACITY);
	unsigned long long lsDispl = evalSizeTexture(num, 0, MAT_TEX_SLOT_DISPLACEMENT);

	unsigned long long sumlayerssize = 0;
	int numlayers = nmat->mat.getLayersNumber();
	for (int i=0; i<numlayers; i++)
		sumlayerssize += evalSizeMaterialLayer(num, i);
	unsigned long long previewsize = evalSizeMaterialPreview(num);
	unsigned long long materialsize = sumlayerssize + previewsize + matnamelength + 22 + lsOpac+10 + 4 + lsDispl + 10 + 20 + 4;

	return materialsize;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeMaterialLayer(unsigned int num, unsigned int numlayer)
{
	if (num>=(int)mats.size() || num<0)
		return 0;

	Mtl * mat = mats[num];
	if (!mat)
	{
		return 122;
	}

	bool isNOXmat = matTypes[num];
	if (!isNOXmat)
		return 122;

	NOXMaterial * nmat = (NOXMaterial*)mat;
	unsigned int numalllayers = nmat->mat.getLayersNumber();
	if (numlayer < 0   ||   numlayer >= numalllayers)
		return 0;

	unsigned long long lsLight  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_LIGHT);
	unsigned long long lsRefl0  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_COLOR0);
	unsigned long long lsRefl90 = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_COLOR90);
	unsigned long long lsWeight = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_WEIGHT);
	unsigned long long lsRough  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_ROUGHNESS);
	unsigned long long lsNormal = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_NORMAL);
	unsigned long long lsTransm = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_TRANSM);
	unsigned long long lsAniso  = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_ANISOTROPY);
	unsigned long long lsAnisoA = evalSizeTexture(num, numlayer, LAYER_TEX_SLOT_ANISO_ANGLE);
	unsigned long long totalTexNew = lsLight + lsRefl0 + lsRefl90 + lsRough + lsWeight + lsNormal + lsTransm + lsAniso + lsAnisoA;

	char * iesFName = nmat->mat.getLayerEmissionIes(numlayer);
	int iesSize = iesFName ? (int)strlen(iesFName)+6 : 0;

	return totalTexNew + 222 + iesSize;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeTexture(unsigned int nummat, unsigned int numlayer, unsigned int slot)
{
	bool isNOXmat = false;
	Mtl * mat = NULL;

	if (slot!=TEX_SLOT_ENV)
	{
		if (nummat>=(int)mats.size() || nummat<0)
			return 0;

		mat = mats[nummat];
		if (!mat)
			return 0;

		isNOXmat = matTypes[nummat];
		if (!isNOXmat)
			return 0;
	}

	NOXMaterial * nmat = (NOXMaterial*)mat;

	char * texffname = NULL;
	switch (slot)
	{
		case LAYER_TEX_SLOT_COLOR0:			
			texffname = nmat->mat.getLayerTexRefl0(numlayer);		break;
		case LAYER_TEX_SLOT_COLOR90:		
			texffname = nmat->mat.getLayerTexRefl90(numlayer);		break;
		case LAYER_TEX_SLOT_ROUGHNESS:		
			texffname = nmat->mat.getLayerTexRough(numlayer);		break;
		case LAYER_TEX_SLOT_WEIGHT:			
			texffname = nmat->mat.getLayerTexWeight(numlayer);		break;
		case LAYER_TEX_SLOT_LIGHT:			
			texffname = nmat->mat.getLayerTexLight(numlayer);		break;
		case LAYER_TEX_SLOT_NORMAL:
			texffname = nmat->mat.getLayerTexNormal(numlayer);		break;
		case LAYER_TEX_SLOT_TRANSM:
			texffname = nmat->mat.getLayerTexTransm(numlayer);		break;
		case LAYER_TEX_SLOT_ANISOTROPY:
			texffname = nmat->mat.getLayerTexAniso(numlayer);		break;
		case LAYER_TEX_SLOT_ANISO_ANGLE:
			texffname = nmat->mat.getLayerTexAnisoAngle(numlayer);	break;
		case MAT_TEX_SLOT_OPACITY:			
			texffname = nmat->mat.getTexOpacity();					break;
		case MAT_TEX_SLOT_DISPLACEMENT:			
			texffname = nmat->mat.getTexDisplacement();				break;
		case TEX_SLOT_ENV:
			{
				if (!sunsky)
					texffname = NULL;
				else
				{
					NOXSunLight * sun = NULL;
					ObjectState objSt = sunsky->EvalWorldState(GetCOREInterface()->GetTime());
					if (objSt.obj == NULL)
					{
						MessageBox(0, _T("Error (1) while extracting SunSky."), _T("Error"), 0);
						return false;
					}
					sun = (NOXSunLight *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXSunLight_CLASS_ID);
					if (!sun)
					{
						MessageBox(0, _T("Error (1) while extracting SunSky."), _T("Error"), 0);
						return false;
					}
					texffname = sun->trsunsky.getEnvTexFilename();
				}
			}
			break;
	}

	unsigned long long ltex = texffname ? ((int)strlen(texffname)+6) : 0;

	return ltex+84+20;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeSunsky()
{
	if (sunsky)
		return 108;
	else
		return 0;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeBlendLayers()
{
	unsigned int res = 0;
	TrMaterial tmat;
	for (int i=0; i<16; i++)
	{
		char * tname = tmat.getBlendLayerName(i);
		if (!tname)
		{
			res += 7+6;		// "Unnamed"
			continue;
		}
		
		unsigned int tlen = (unsigned int)strlen(tname);
		if (tlen<1)
		{
			res += 7+6;	// "Unnamed"
			continue;
		}

		res += tlen+6;
	}

	res += 16*42;
	res += 10;

	return res;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeUserColors()
{
	return 370;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeSceneInfo()
{
	Renderer * rend = GetCOREInterface()->GetRenderer(RS_Production);		
	if (rend->ClassID() != NOXRenderer_CLASS_ID)
		return 0;

	NOXRenderer * noxrend = (NOXRenderer*)rend;
	unsigned int size_scene_name   = (noxrend->scene_name) ?			6+(unsigned int)strlen(noxrend->scene_name)			: 0;
	unsigned int size_scene_author = (noxrend->scene_author_name) ?		6+(unsigned int)strlen(noxrend->scene_author_name)	: 0;
	unsigned int size_scene_email  = (noxrend->scene_author_email) ?	6+(unsigned int)strlen(noxrend->scene_author_email) : 0;
	unsigned int size_scene_www    = (noxrend->scene_author_www) ?		6+(unsigned int)strlen(noxrend->scene_author_www)	: 0;
	
	unsigned long long result = 10 + size_scene_name + size_scene_author + size_scene_email + size_scene_www;
	return result;
}
//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeSceneSettings()
{
	return 26;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeRendererSettings()
{
	return 14;
}

//---------------------------------------------------------------------------------------

unsigned long long BinaryScenePlugin::evalSizeEnvironmentMap()
{
	if (!sunsky)
		return 0;

	unsigned long long envSize = evalSizeTexture(0, 0, TEX_SLOT_ENV);
	return envSize + 24 + 32;
}

//---------------------------------------------------------------------------------------

int BinaryScenePlugin::addMat(Mtl * mat, bool isNOX)
{	
	// add mat (or not if exists) and return index where it lies in array
	int n = (int)mats.size();
	for (int i=0; i<n; i++)
	{
		if (mats[i] == mat)
			return i;
	}
	mats.push_back(mat);
	matTypes.push_back(isNOX);
	return n;
}

//---------------------------------------------------------------------------------------

void BinaryScenePlugin::setProgress(int p)
{
	if (exportAndRender)
	{
		if (prog)
		{
			prog->Progress(p, 1000);
		}
		return;
	}

	if (!hInfoWindow)
		return;
	HWND hProgress = GetDlgItem(hInfoWindow, IDC_NOX_EXP_PROGRESS);
	if (!hProgress)
		return;
	SendMessage(hProgress, PBM_SETPOS, p, 0);
	InvalidateRect(hProgress, NULL, false);
	UpdateWindow(hProgress);
}

//---------------------------------------------------------------------------------------

void BinaryScenePlugin::setProgInfo(TCHAR * info)
{
	if (exportAndRender)
	{
		if (prog)
		{
			prog->SetTitle(info);
		}
		return;
	}

	if (!hInfoWindow)
		return;
	HWND hText = GetDlgItem(hInfoWindow, IDC_NOX_EXP_INFO);
	if (!hText)
		return;
	SetWindowText(hText, info);
	InvalidateRect(hText, NULL, false);
	UpdateWindow(hText);
}

//-------------------------------------------------------------------------------------------

char * BinaryScenePlugin::xorString(char * src, char * pswd, unsigned int lsrc, unsigned int lpswd)
{
	if (!src)
		return NULL;
	if (!pswd)
		return NULL;
	if (!lsrc)
		return NULL;
	if (!lpswd)
		return NULL;

	char * data = (char *)malloc(lsrc+1);
	data[lsrc] = 0;

	for (unsigned int i=0; i<lsrc; i++)
	{
		data[i] = src[i] ^ pswd[i%lpswd];
	}

	return data;
}


//---------------------------------------------------------------------------------------

void printMatrix(Matrix3 m, TCHAR * title)
{
	TCHAR buf[512];
	_stprintf_s(buf, 512, _T("%.3f  %.3f  %.3f  0.0\n%.3f  %.3f  %.3f  0.0\n%.3f  %.3f  %.3f  0.0\n%.3f  %.3f  %.3f  1.0"),
		m.GetRow(0).x, m.GetRow(0).y, m.GetRow(0).z, 
		m.GetRow(1).x, m.GetRow(1).y, m.GetRow(1).z, 
		m.GetRow(2).x, m.GetRow(2).y, m.GetRow(2).z, 
		m.GetRow(3).x, m.GetRow(3).y, m.GetRow(3).z);
	MessageBox(0, buf, title, 0);
}

//---------------------------------------------------------------------------------------

void logMatrix(Matrix3 m, char * title)
{
	char buf[512];
	sprintf_s(buf, 512, "%s:\n%.3f  %.3f  %.3f  0.0\n%.3f  %.3f  %.3f  0.0\n%.3f  %.3f  %.3f  0.0\n%.3f  %.3f  %.3f  1.0\n", title,
		m.GetRow(0).x, m.GetRow(0).y, m.GetRow(0).z, 
		m.GetRow(1).x, m.GetRow(1).y, m.GetRow(1).z, 
		m.GetRow(2).x, m.GetRow(2).y, m.GetRow(2).z, 
		m.GetRow(3).x, m.GetRow(3).y, m.GetRow(3).z);
	ADD_LOG_PARTS_MAIN(buf);
}

//---------------------------------------------------------------------------------------

void logPoint(Point3 p, char * title)
{
	char buf[512];
	sprintf_s(buf, 512, "%s:\n%f  %f  %f\n", title,
		p.x, p.y, p.z);
	ADD_LOG_PARTS_MAIN(buf);
}

//---------------------------------------------------------------------------------------

void logQuaternion(Quat q, char * title)
{
	char buf[512];
	sprintf_s(buf, 512, "%s:\n%.3f  %.3f  %.3f  with  %.3f\n", title,
		q, q.y, q.z, q.w);
	ADD_LOG_PARTS_MAIN(buf);
}

//---------------------------------------------------------------------------------------

float BinaryScenePlugin::getMaxShutter()
{
	float res = 50;
	for (unsigned int i=0; i <cameras.size(); i++)
	{
		INode * node = cameras[i];
		if (!node)
			continue;

		ObjectState objSt = node->EvalWorldState(GetCOREInterface()->GetTime());
		if (objSt.obj == NULL)
			continue;

		CameraState cs;
		CameraObject *cam = (CameraObject *)objSt.obj;
		Interval iv;
		iv.SetInfinite();
		cam->EvalCameraState(GetCOREInterface()->GetTime(),iv,&cs);

		if (objSt.obj->CanConvertToType(NOXCamera_CLASS_ID))
		{
			NOXCamera * tcam = (NOXCamera *)objSt.obj->ConvertToType(GetCOREInterface()->GetTime(), NOXCamera_CLASS_ID);
			if (tcam)
				res = min(res, tcam->trCam.getShutter());
			deleteSecondIfCopied(objSt.obj, tcam);
		}

	}
	return res;
}

//---------------------------------------------------------------------------------------
