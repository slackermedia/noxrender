#include "max2013predef.h"
#include "NOXSunLight.h"
#include <decomp.h> 

static NOXSunLight * editedSun = NULL; 

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)  
{
	if (!hParams) 
	{
		hParams = ip->AddRollupPage(
					hInstance, 
					MAKEINTRESOURCE(IDD_NOX_SUNSKY),
					NOXSunLightDlgProc, 
					GetString(IDS_NOX_SUN_DLG_TITLE), 
					(LPARAM)this);    
		ip->RegisterDlgWnd(hParams);
	}
	else 
	{
		DLSetWindowLongPtr(hParams, this);
	}
}

//------------------------------------------------------------------------------------------------------------------------

void NOXSunLight::EndEditParams( IObjParam *ip, ULONG flags, Animatable *next)  
{
	if (flags&END_EDIT_REMOVEUI   ||   removeUI) 
	{
		ip->UnRegisterDlgWnd(hParams);
		ip->DeleteRollupPage(hParams);
		hParams = NULL;
		removeUI = false;
	} 
	else 
	{
		DLSetWindowLongPtr(hParams, NULL);
	}
	ip = NULL;
}

//------------------------------------------------------------------------------------------------------------------------

INT_PTR CALLBACK NOXSunLightDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	NOXSunLight * nsl = DLGetWindowLongPtr<NOXSunLight *>(hWnd);
	if (!nsl && message!=WM_INITDIALOG) return FALSE;

	switch (message) 
	{
		case WM_INITDIALOG: 
		{
			nsl = (NOXSunLight*)lParam;
			DLSetWindowLongPtr(hWnd, lParam);
			nsl->hParams = hWnd;

			HWND hCB = GetDlgItem(hWnd, IDC_NOX_SUNSKY_HIDE_TARGET);
			HWND hCB1 = GetDlgItem(hWnd, IDC_NOX_SUNSKY_ACTIVE);
			HWND hCB2 = GetDlgItem(hWnd, IDC_NOX_SUNSKY_HIDE_ENVMAP);
			HWND hBT = GetDlgItem(hWnd, IDC_NOX_SUNSKY_SELECT_TARGET);
			if (nsl->hiddenTarg)
			{
				SendMessage(hCB, BM_SETCHECK, BST_CHECKED, 0);
				EnableWindow(hBT, false);
			}
			else
			{
				SendMessage(hCB, BM_SETCHECK, BST_UNCHECKED, 0);
				EnableWindow(hBT, true);
			}

			bool checked = nsl->trsunsky.getSunSkyON();
			if (checked)
				SendMessage(hCB1, BM_SETCHECK, BST_CHECKED, 0);
			else
				SendMessage(hCB1, BM_SETCHECK, BST_UNCHECKED, 0);

			checked = nsl->trsunsky.getEnvHidden();
			if (checked)
				SendMessage(hCB2, BM_SETCHECK, BST_CHECKED, 0);
			else
				SendMessage(hCB2, BM_SETCHECK, BST_UNCHECKED, 0);

		}
		break;
		case WM_COMMAND:
		{
			switch(LOWORD(wParam)) 
			{
				case IDC_NOX_SUNSKY_RUN_EDITOR:
				{
					float alt, azi;
					editedSun = nsl;
					nsl->evalAltitudeAzimuth(alt, azi);
					nsl->trsunsky.setPosFromDir(alt, azi);
					nsl->trsunsky.runDialog(hInstance, hWnd);
					NOXSunTarget * target = nsl->getTargetObject();
					if (target)
						target->updateEnvMap();
					nsl->updateToParamBlock();
				}
				break;
				case IDC_NOX_SUNSKY_HIDE_TARGET:
				{
					if (HIWORD(wParam) == BN_CLICKED)
					{
						HWND hCB = GetDlgItem(hWnd, IDC_NOX_SUNSKY_HIDE_TARGET);
						HWND hBT = GetDlgItem(hWnd, IDC_NOX_SUNSKY_SELECT_TARGET);
						bool checked = (BST_CHECKED == SendMessage(hCB, BM_GETCHECK, 0, 0));  
						nsl->setTargetHidden(checked);
						EnableWindow(hBT, !checked);
					}
				}
				break;
				case IDC_NOX_SUNSKY_HIDE_ENVMAP:
				{
					if (HIWORD(wParam) == BN_CLICKED)
					{
						HWND hCB = GetDlgItem(hWnd, IDC_NOX_SUNSKY_HIDE_ENVMAP);
						bool checked = (BST_CHECKED == SendMessage(hCB, BM_GETCHECK, 0, 0));  
						nsl->setEnvHidden(checked);
					}
				}
				break;
				case IDC_NOX_SUNSKY_ACTIVE:
				{
					if (HIWORD(wParam) == BN_CLICKED)
					{
						HWND hCB = GetDlgItem(hWnd, IDC_NOX_SUNSKY_ACTIVE);
						bool checked = (BST_CHECKED == SendMessage(hCB, BM_GETCHECK, 0, 0));  
						nsl->trsunsky.setSunSkyON(checked);
					}
				}
				break;
				case IDC_NOX_SUNSKY_SELECT_TARGET:
				{
					if (HIWORD(wParam) == BN_CLICKED)
						nsl->selectTarget();
				}
				break;
			}
			break;
		}
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MOUSEMOVE:
			{
				return FALSE;
			}
			break;
		default:
			return FALSE;
	}
	return TRUE;
}

//------------------------------------------------------------------------------------------------------------------------

void notifyNOXSunSkyCallback(int month, int day, int hour, int minute, int gmt, float longitude, float latitude, float env_azimuth)
{
	if (!editedSun)
		return;

	editedSun->trsunsky.setMonth(month);
	editedSun->trsunsky.setDay(day);
	editedSun->trsunsky.setHour(hour);
	editedSun->trsunsky.setMinute(minute);
	editedSun->trsunsky.setGMT(gmt);
	editedSun->trsunsky.setLongitude(longitude);
	editedSun->trsunsky.setLatitude(latitude);
	editedSun->trsunsky.setEnvAzimuth(env_azimuth);

	float azi, alt;
	editedSun->trsunsky.getAziAltFromNOX(azi, alt);
	alt = PI/2 - alt;
	azi += PI;
	editedSun->setTMfromAltAzi(alt, azi);

	GetCOREInterface()->RedrawViews(GetCOREInterface()->GetTime());
}

//------------------------------------------------------------------------------------------------------------------------
