#define _CRT_RAND_S
#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include "nox_update.h"

size_t write_data (void *ptr, size_t size, size_t nmemb, void *stream) 
{ 
	return fwrite(ptr, size, nmemb, (FILE*)stream);
}

#define FREAD_UCHAR(a)			fread((&a), 1, 1, file)

//#define LOCAL_TEST

#ifndef CHECK
	#define CHECK(a) if(!(a)) return false
#endif

extern HWND hMainUpdate;
extern HWND hTextInfo2;
extern HWND hProgress;
extern bool silentMode;
extern bool stopDownloading;
extern bool autoInstall;
extern char * addressVersion;
extern char * addressUpdate;
extern char * downloadFilenameVersion;
extern char * downloadFilenameUpdate;
extern int lMajor, lMinor, lBuild;
extern int sMajor, sMinor, sBuild;
extern int programStatus;

//-------------------------------------------------------------------------------------------------------------

bool isVersionHigher(int vLocalMajor, int vLocalMinor, int vLocalBuild, int vServerMajor, int vServerMinor, int vServerBuild)
{
	if (vLocalMajor < vServerMajor)
		return true;
	if (vLocalMajor > vServerMajor)
		return false;

	if (vLocalMinor < vServerMinor)
		return true;
	if (vLocalMinor > vServerMinor)
		return false;

	if (vLocalBuild < vServerBuild)
		return true;
	if (vLocalBuild > vServerBuild)
		return false;

	return false; // the same
}

//-------------------------------------------------------------------------------------------------------------

int downloadProgressCallback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
	int prg = (int)(100*dlnow/dltotal);
	SendMessage(hProgress, PBM_SETPOS, prg, 0); 

	int prc = (int)(100*dlnow/dltotal);
	if (prc>100 || prc<0)
		prc = 0;
	char buf[512];
	sprintf_s(buf, 512, "Downloading %d of %d bytes... %d%%", (int)dlnow, (int)dltotal, prc);
	SetWindowText(hTextInfo2, buf);

	if (stopDownloading)
		return 1;

	return 0;
}

//-------------------------------------------------------------------------------------------------------------

bool updaterDownloadFile(char * address, char * filename)
{
	CHECK(address);
	CHECK(filename);

	CURL *curl;
	CURLcode res;


	FILE * file;
	if (fopen_s(&file, filename, "wb"))
		return false;
	CHECK(file);

	curl = curl_easy_init(); 

	curl_easy_setopt(curl, CURLOPT_POST, true); 

	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
	curl_easy_setopt(curl, CURLOPT_URL, address); 
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); 

	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0); 
	curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, 0);
	curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, downloadProgressCallback); 

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
	
	res = curl_easy_perform(curl); 
	
	long http_code = 0;
	curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

	curl_easy_cleanup(curl); 
	fclose(file);

	CHECK(res==0);
	if (http_code>=400)
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------------------

char * randomTempFilename(char * prefix)
{
	char * gprefix = "nox";
	if (prefix)
	{
		int l = (int)strlen(prefix);
		if (l>0 && l<4)
			gprefix = prefix;
	}

	char temppath[2048];
	char tempRes[2048];
	int ll = GetTempPath(2048, temppath);
	if (ll > 2048)
		return NULL;
	if (ll < 3)
		return NULL;

	char temppathlong[2048];
	int ll2 = GetLongPathName(temppath, temppathlong, 2048);
	if (ll2 > 2048)
		return NULL;

	if (temppathlong[ll2-1] == '\\'  ||  temppathlong[ll2-1] == '/')
		temppathlong[ll2-1] = 0;

	if (!doesDirExist(temppathlong))
		return NULL;

	unsigned int unique;
	while(rand_s(&unique));
	GetTempFileName(temppathlong, "nox", unique, tempRes);

	if (strlen(tempRes) < strlen(temppathlong))
		return NULL;

	int ll3 = (int)strlen(tempRes)+1;
	char * result = (char*)malloc(ll3);
	if (!result)
		return NULL;
	sprintf_s(result, ll3, "%s", tempRes);

	return result;
}

//-------------------------------------------------------------------------------------------------------------

char * createTempDirectory()
{
	char * dirname = randomTempFilename("nox");
	if(!dirname)
		return false;

	BOOL cdOK = CreateDirectory(dirname, NULL);
	if (cdOK != TRUE)
	{
		free(dirname);
		return NULL;
	}

	return dirname;
}

//-------------------------------------------------------------------------------------------------------------

DWORD WINAPI DownloadVersionFileThreadProc(LPVOID lpParameter)
{
	programStatus = PROGRAM_STATUS_CHECKING_SERVER_VERSION;

	if (!addressVersion  ||  !downloadFilenameVersion)
	{
		programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
		unlockControlsAfterDownloadVersion(0);
		SetWindowText(hTextInfo2, "Internal error. Sorry.");
		return 0;
	}

	bool dlOK = updaterDownloadFile(addressVersion, downloadFilenameVersion);
	if (!dlOK)
	{
		programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
		SetWindowText(hTextInfo2, "Failed to check server version.");
		unlockControlsAfterDownloadVersion(0);
		if (doesFileExist(downloadFilenameVersion))
			DeleteFile(downloadFilenameVersion);
		return false;
	}

	if (!doesFileExist(downloadFilenameVersion))
	{
		SetWindowText(hTextInfo2, "Failed to check server version.");
		programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
		unlockControlsAfterDownloadVersion(0);
		return false;
	}

	FILE * file;
	if (fopen_s(&file, downloadFilenameVersion, "rb")  ||  !file)
	{
		SetWindowText(hTextInfo2, "Failed to check server version.");
		programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
		unlockControlsAfterDownloadVersion(0);
		if (doesFileExist(downloadFilenameVersion))
			DeleteFile(downloadFilenameVersion);
		return false;
	}

	_fseeki64(file, 0, SEEK_END);
	long long int filesize = _ftelli64(file);
	_fseeki64(file, 0, SEEK_SET);
	
	if (filesize != 3)
	{
		SetWindowText(hTextInfo2, "Failed to check server version.");
		programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
		unlockControlsAfterDownloadVersion(0);
		fclose(file);
		if (doesFileExist(downloadFilenameVersion))
			DeleteFile(downloadFilenameVersion);
		return false;
	}

	unsigned char v1, v2, v3;
	bool fOK = true;
	if (!FREAD_UCHAR(v1))
		fOK = false;
	if (!FREAD_UCHAR(v2))
		fOK = false;
	if (!FREAD_UCHAR(v3))
		fOK = false;

	fclose(file);
	if (doesFileExist(downloadFilenameVersion))
		DeleteFile(downloadFilenameVersion);

	sMajor = v1;
	sMinor = v2;
	sBuild = v3;

	if (fOK)
	{
		bool higher = isVersionHigher(lMajor, lMinor, lBuild, sMajor, sMinor, sBuild);
		char textBuf[512];
		if (higher)
		{
			sprintf_s(textBuf, 512, "Server version: %d.%d.%d. Update recommended.", sMajor, sMinor, sBuild);
			programStatus = PROGRAM_STATUS_SERVER_VERSION_OK_HIGHER;
		}
		else
		{
			sprintf_s(textBuf, 512, "Server version: %d.%d.%d. No need to update.", sMajor, sMinor, sBuild);
			programStatus = PROGRAM_STATUS_SERVER_VERSION_OK_LOWER;
		}
		SetWindowText(hTextInfo2, textBuf);
		unlockControlsAfterDownloadVersion(1);
		
		if (silentMode)
		{
			if (higher)
				ShowWindow(hMainUpdate, SW_SHOW);
			else
				SendMessage(hMainUpdate, WM_CLOSE, 0, 0);
		}
	}
	else
	{
		programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
		unlockControlsAfterDownloadVersion(0);
		SetWindowText(hTextInfo2, "Failed to check server version.");
	}

	return fOK;
}

//-------------------------------------------------------------------------------------------------------------

bool downloadNOXserverVersion(bool alpha)
{

	lockControlsBeforeDownloadVersion();

	char * tempFilename = randomTempFilename("nox");
	if (!tempFilename)
		return false;

	char * address = (char*)malloc(512);
	if (!address)
		return false;

	#ifdef LOCAL_TEST
		if (alpha)
			sprintf_s(address, 512, "http://localhost/files/noxRenderer/update/alpha.ver");
		else
			sprintf_s(address, 512, "http://localhost/files/noxRenderer/update/nox.ver");
	#else
		if (alpha)
			sprintf_s(address, 512, "http://www.evermotion.org/files/noxRenderer/update/alpha.ver");
		else
			sprintf_s(address, 512, "http://www.evermotion.org/files/noxRenderer/update/nox.ver");
	#endif

	if (addressVersion)
		free(addressVersion);
	addressVersion = address;

	if (downloadFilenameVersion)
		free(downloadFilenameVersion);
	downloadFilenameVersion = tempFilename;

	DWORD threadId;
	HANDLE threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DownloadVersionFileThreadProc), (LPVOID)0, 0, &threadId);
	if (!threadHandle)
		return false;
	return true;
}

//-------------------------------------------------------------------------------------------------------------

bool checkNOXserverVersion(int &vMajor, int &vMinor, int &vBuild, bool alpha)
{
	// NOT USED ANYMORE
	char * tempFilename = randomTempFilename("nox");
	if (!tempFilename)
		return false;


	char * address = (char*)malloc(512);
	if (!address)
		return false;

	#ifdef LOCAL_TEST
		if (alpha)
			sprintf_s(address, 512, "http://localhost/files/noxRenderer/update/alpha.ver");
		else
			sprintf_s(address, 512, "http://localhost/files/noxRenderer/update/nox.ver");
	#else
		if (alpha)
			sprintf_s(address, 512, "http://www.evermotion.org/files/noxRenderer/update/alpha.ver");
		else
			sprintf_s(address, 512, "http://www.evermotion.org/files/noxRenderer/update/nox.ver");
	#endif

	if (addressVersion)
		free(addressVersion);
	addressVersion = address;

	if (downloadFilenameVersion)
		free(downloadFilenameVersion);
	downloadFilenameVersion = tempFilename;

	bool dlOK = updaterDownloadFile(address, tempFilename);
	if (!dlOK)
	{
		if (doesFileExist(tempFilename))
			DeleteFile(tempFilename);
		free(tempFilename);
		return false;
	}

	if (!doesFileExist(tempFilename))
	{
		free(tempFilename);
		return false;
	}

	FILE * file;
	if (fopen_s(&file, tempFilename, "rb")  ||  !file)
	{
		if (doesFileExist(tempFilename))
			DeleteFile(tempFilename);
		free(tempFilename);
		return false;
	}

	_fseeki64(file, 0, SEEK_END);
	long long int filesize = _ftelli64(file);
	_fseeki64(file, 0, SEEK_SET);
	
	if (filesize != 3)
	{
		fclose(file);
		if (doesFileExist(tempFilename))
			DeleteFile(tempFilename);
		free(tempFilename);
		return false;
	}

	unsigned char v1, v2, v3;
	bool fOK = true;
	if (!FREAD_UCHAR(v1))
		fOK = false;
	if (!FREAD_UCHAR(v2))
		fOK = false;
	if (!FREAD_UCHAR(v3))
		fOK = false;


	if (fOK)
	{
		vMajor = v1;
		vMinor = v2;
		vBuild = v3;
	}

	fclose(file);
	if (doesFileExist(tempFilename))
		DeleteFile(tempFilename);
	free(tempFilename);
	return fOK;
}

//-------------------------------------------------------------------------------------------------------------

DWORD WINAPI DownloadUpdateFileThreadProc(LPVOID lpParameter)
{
	programStatus = PROGRAM_STATUS_DOWNLOADING_UPDATE;

	if (!addressUpdate   ||   !downloadFilenameUpdate)
	{
		programStatus = PROGRAM_STATUS_DOWN_UPDATE_ERROR;
		return 0;
	}

	bool dlOK = updaterDownloadFile(addressUpdate, downloadFilenameUpdate);
	if (!dlOK)
	{

		unlockControlsAfterDownloadUpdate(0);
		programStatus = PROGRAM_STATUS_DOWN_UPDATE_ERROR;


		if (stopDownloading)
		{
			SetWindowText(hTextInfo2, "Downloading update aborted.");
		}
		else
		{
			SetWindowText(hTextInfo2, "Downloading update failed.");
		}
		if (doesFileExist(downloadFilenameVersion))
			DeleteFile(downloadFilenameVersion);
		return false;
	}
	
	unlockControlsAfterDownloadUpdate(1);
	programStatus = PROGRAM_STATUS_DOWN_UPDATE_OK;

	if (autoInstall)
	{
		bool iOK = runInstallThread(downloadFilenameUpdate);
		if (!iOK)
		{
			unlockControlsAfterInstallUpdate(0);
		}
	}


	return 0;
}

//-------------------------------------------------------------------------------------------------------------

bool downloadUpdate(int vMajor, int vMinor, int vBuild, bool alpha)
{
	lockControlsBeforeDownloadUpdate();

	char * address = (char*)malloc(2048);
	#ifdef LOCAL_TEST
		if (alpha)
			sprintf_s(address, 2048, "http://localhost/files/noxRenderer/alpha/Renderer_%d.%d.%d.zip", vMajor, vMinor, vBuild);
		else
			sprintf_s(address, 2048, "http://localhost/files/noxRenderer/Renderer_%d.%d.%d.zip", vMajor, vMinor, vBuild);
	#else
		if (alpha)
			sprintf_s(address, 2048, "http://www.evermotion.org/files/noxRenderer/alpha/Renderer_%d.%d.%d.zip", vMajor, vMinor, vBuild);
		else
			sprintf_s(address, 2048, "http://www.evermotion.org/files/noxRenderer/Renderer_%d.%d.%d.zip", vMajor, vMinor, vBuild);
	#endif

	char * filename = randomTempFilename("nox");
	if (!filename)
		return false;

	if (addressUpdate)
		free(addressUpdate);
	if (downloadFilenameUpdate)
		free(downloadFilenameUpdate);
	addressUpdate = address;
	downloadFilenameUpdate = filename;

	DWORD threadId;
	HANDLE threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DownloadUpdateFileThreadProc), (LPVOID)0, 0, &threadId);
	if (!threadHandle)
		return false;
	return true;
}

//-------------------------------------------------------------------------------------------------------------

