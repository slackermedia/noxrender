#define _CRT_RAND_S
#include <math.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "raytracer.h"
#include "FastMath.h"
#include "QuasiRandom.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include <time.h>
#include "log.h"
#include <float.h>

#define clamp(x,a,b)   max((a),min((x),(b)))

extern clock_t localticks;

//-------------------------------------------------------------------------------------------

float MatLayer::getProbabilitySchlick(HitData & hd)
{
	if ((hd.normal_shade * hd.out) * (hd.normal_shade*hd.in)  <  0)
		return 0;

	Vector3d H = hd.in + hd.out;
	H.normalize();
	float cosdelta   = max(0.001f, fabs(hd.normal_shade * H));
	float costheta_i = max(0.001f, fabs(hd.normal_shade * hd.in));
	float costheta_o = max(0.001f, fabs(hd.normal_shade * hd.out));
	float r = getRough(hd.tU, hd.tV);
	float Z = (1 + (r-1)*cosdelta*cosdelta);
	Z = Z*Z;
	Z = r/Z;
	float jac = 0.25f/(hd.out * H);
	float D = jac * Z * 0.25f * PER_PI / costheta_o / costheta_i;

	float m = r;
	float a,b,c;
	if (m<0.5f)
	{	b=4*m*(1-m);	a=0;	c=1-b;	}
	else
	{	b=4*m*(1-m);	c=0;	a=1-b;	}
	float delta = (cosdelta>0.999f) ? 1.0f : 0.0f;
	float probability = D;
	return probability;
}

//-------------------------------------------------------------------------------------------

Vector3d MatLayer::randomNewDirectionSchlick(HitData & hd, float &probability)
{
	clock_t lstart, lstop;
	lstart = clock();

	probability = 2;

	Vector3d refl, temp, bi;
	refl = hd.normal_shade*(-2*(hd.normal_shade*hd.in)) + hd.in;
	refl.invert();
	temp = Vector3d::random();
	temp.normalize();

	Vector3d h_ = hd.normal_shade^temp;
	h_.normalize();

	bi = refl^temp;
	bi.normalize();
	#ifdef RAND_PER_THREAD
		float aa = Raytracer::getRandomGeneratorForThread()->getRandomFloat();
	#else
		float aa = ((float)rand()/(float)RAND_MAX);
	#endif
	float r = getRough(hd.tU, hd.tV);
	float m = (r-aa*r+aa);
	if (m > 0) 
		aa = aa/m;
	else
		return randomNewDirectionSchlick(hd, probability);

	float s = sqrt(max(1-aa, 0));	// sin theta
	aa = sqrt(aa);					// cos theta
	if (aa>1) 
		return randomNewDirectionSchlick(hd, probability);
	Vector3d res1 = refl*aa + bi*s;
	if (res1*hd.normal_shade<0)
		res1 = hd.normal_shade*(-2*(hd.normal_shade*res1)) + res1;

	Vector3d H_ = hd.normal_shade*aa + h_*s;
	H_.normalize();
	res1 = hd.in.reflect(H_);
	res1.normalize();

	if ((res1*hd.normal_shade)*(hd.in*hd.normal_shade) < 0)
		return randomNewDirectionSchlick(hd, probability);


	Vector3d H = (hd.in)+(res1);
	H.normalize();
	float cosdelta = max(0.01f,H*hd.normal_shade);
	float costheta_i = max(0.01f,hd.normal_shade*hd.in);
	float costheta_o = max(0.01f,hd.normal_shade*res1);
	float Z = (1 + (r-1)*(cosdelta)*(cosdelta));
	Z = Z*Z;
	Z = r/Z;
	hd.out = res1;

	probability = getProbabilitySchlick(hd);	

	lstop = clock();
	localticks += (lstop-lstart);

	return res1;
}

//----------		SCHLICK BRDF

Color4 MatLayer::getBRDFSchlick(HitData & hd)
{
	Color4 res;
	float Gi, Go;		
	float D;			
	float F;			
	float Z, A;			
	float cosdelta;		
	float costheta_i;	
	float costheta_o;	
	Vector3d H;			
	Vector3d N;         
	float m = getRough(hd.tU, hd.tV);

	hd.in.normalize();
	hd.out.normalize();
	hd.normal_shade.normalize();

	if (hd.normal_shade*hd.in > 0)
		N = hd.normal_shade;
	else
		N = -hd.normal_shade;

	H = (hd.in)+(hd.out);
	H.normalize();

	costheta_i = max(0.001f,N*hd.in);
	costheta_o = max(0.01f,N*hd.out);
	cosdelta   = max(0.0000001f,N*H);

	if (m < 0.0001f)
	{
		if (cosdelta>0.9993f)
			return getColor0(hd.tU, hd.tV)*1;
		else
			return Color4(0,0,0);
	}

	// eval G (schlick approximation)
	float k = m * 0.79788456080286535587989211986876f;
	Gi = costheta_i/(costheta_i*(1-k) + k);
	Go = costheta_o/(costheta_o*(1-k) + k);
	Gi = max(Gi, 0);
	Go = max(Go, 0);
	float G = Gi*Go;

	A = 1;		// no anisotropy

	Z = (1 + (m-1)*cosdelta*cosdelta);
	Z = Z*Z;
	Z = m/Z;

	float a,b,c;
	if (m<0.5f)
	{	b=4*m*(1-m);	a=0;	c=1-b;	}
	else
	{	b=4*m*(1-m);	c=0;	a=1-b;	}
	float delta = (cosdelta > 0.999f) ? 1.0f : 0.0f;

	float spec = 1-c + (c)/fabs(hd.out*hd.normal_shade);
	D = (float) ( G*A*Z/(4*PI*costheta_i*costheta_o) );

	if (Z <= 1)
		c = 0;
	else
		c *= getProbabilitySchlick(hd);

	F = fresnel.getPreEvaled(hd.out*H);

	float brdf = F*D;

	return getColor0(hd.tU, hd.tV)* brdf;
}

//-------------------------------------------------------------------------------------------
