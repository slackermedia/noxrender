#ifndef __NOX_EXPORT_H__
#define __NOX_EXPORT_H__

#define NOX_SWAP_YZ

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

#include "maxNormals.h"
#include "NOXPlugin.h"

#define XML_TRI_SIZE 200
#define XML_TRI_ROW_SIZE 211

using namespace std;

void countDeletedCopies();
void showDeletedCopiesStats();

template <class T>
void deleteSecondIfCopied(Object * orig, T * copy)
{
	if (orig != copy)
	{
		copy->DeleteMe();
		countDeletedCopies();
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------

#define NOXexport_CLASS_ID	Class_ID(0x2c2e4aa1, 0xb943642)

class NOXExport : public SceneExport 
{
public:
		
	static HWND hParams;
		
	int				ExtCount();
	const TCHAR *	Ext(int n);
	const TCHAR *	LongDesc();
	const TCHAR *	ShortDesc();
	const TCHAR *	AuthorName();
	const TCHAR *	CopyrightMessage();
	const TCHAR *	OtherMessage1();
	const TCHAR *	OtherMessage2();
	unsigned int	Version();
	void			ShowAbout(HWND hWnd);

	BOOL SupportsOptions(int ext, DWORD options);
	int	DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);

	NOXExport();
	~NOXExport();		
};

//--------------------------------------------------------------------------------------------------------------------------------------

class NOXExportClassDesc : public ClassDesc2 
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new NOXExport(); }
	const TCHAR *	ClassName() { return GetString(IDS_NOX_EXPORTER_CLASS_NAME); }
	SClass_ID		SuperClassID() { return SCENE_EXPORT_CLASS_ID; }
	Class_ID		ClassID() { return NOXexport_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_NOX_EXPORTER_CATEGORY); }

	const TCHAR*	InternalName() { return _T("everexport"); }
	HINSTANCE		HInstance() { return hInstance; }
	

};

//--------------------------------------------------------------------------------------------------------------------------------------

class DecHex
{
public:
	static bool singleHexToDec(unsigned char hex, unsigned char &dec);
	static unsigned char singleDecToHex(unsigned char dec);
	static void floatToHex(float f, unsigned char *h);
	static bool hex8ToFloat(unsigned char * h, float &f);
	static void uintToHex(unsigned int ui, unsigned char *h);
	static bool hex8ToUint(unsigned char * h, unsigned int &ui);
	static bool hex2ToUchar(unsigned char *h, unsigned char &c);
	static bool hex2ToUcharBigEndian(unsigned char *h, unsigned char &c);
	static bool ucharToHex2(const unsigned char c, unsigned char * h);
	static bool ucharToHex2BigEndian(const unsigned char c, unsigned char * h);
};

//--------------------------------------------------------------------------------------------------------------------------------------

struct MeshInstSrc
{
	Object * objRef;
	INode * node;
	MeshInstSrc() { objRef=NULL; node=NULL; }
	MeshInstSrc(Object * objectRef, INode * inode) { objRef=objectRef; node=inode; }
};

struct MeshInst
{
	int id;
	INode * node;
	MeshInst() { id=-1; node=NULL; }
	MeshInst(int srcID, INode * inode) { id=srcID; node=inode; }
};

//--------------------------------------------------------------------------------------------------------------------------------------

class XMLScene
{
public:
	bool saveScene(const TCHAR * filename);
	bool insertInstSource(xmlNodePtr curNode, int mNum);
	bool insertInstance(xmlNodePtr curNode, int iNum);
	bool insertTriangleSrc(xmlNodePtr curNode, Mesh * mesh, int triIndex, int matID, int matInLib, MNormal * normals);
	bool printTriangleSrc(unsigned char * buf, unsigned int matID, unsigned int matInLib, float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, float v3x, float v3y, float v3z, 
								float n1x, float n1y, float n1z, float n2x, float n2y, float n2z, float n3x, float n3y, float n3z, 
								float uv1x, float uv1y, float uv2x, float uv2y, float uv3x, float uv3y);	// deprecated

	bool insertMesh(xmlNodePtr curNode, INode * node);	// deprecated
	bool insertTriangle(xmlNodePtr curNode, Mesh * mesh, int triIndex, int matID, MNormal * normals, Matrix3 matr);	// deprecated
	bool printTriangle(unsigned char * buf, unsigned int matID, float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, float v3x, float v3y, float v3z, 
								float n1x, float n1y, float n1z, float n2x, float n2y, float n2z, float n3x, float n3y, float n3z, 
								float uv1x, float uv1y, float uv2x, float uv2y, float uv3x, float uv3y);	// deprecated

	bool insertCamera(xmlNodePtr curNode, INode * node);
	bool insertSunSky(xmlNodePtr curNode, INode * node);
	bool insertNOXCameraSettings(xmlNodePtr camNode, INode * node);
	bool insertMaterials(xmlNodePtr curNode);
	bool insertMaterial(xmlNodePtr curNode, int i);
	bool insertNOXMaterial(xmlNodePtr curNode, Mtl * mat, int id);
	bool insertPreview(xmlNodePtr curNode, Mtl * mat);
	bool insertUserColors(xmlNodePtr curNode);
	bool insertBlendSettings(xmlNodePtr curNode);
	bool insertTexture(xmlNodePtr curNode, Mtl * mat, int layer, int slot);
	bool insertTextureTypeFile(xmlNodePtr curNode, Mtl * mat, int layer, int slot);
	bool insertSceneInfo(xmlNodePtr curNode);
	bool insertRendererSettings(xmlNodePtr curNode);
	char * xorString(char * src, char * pswd, unsigned int lsrc, unsigned int lpswd);
	char * decToHexString(char * src, unsigned int lsrc);

	vector<MeshInst> meshinstances;
	vector<MeshInstSrc> meshsources;

	vector<INode*> meshes;
	vector<INode*> cameras;
	vector<Mtl*> mats;
	INode * sunsky;
	vector<bool> matTypes;
	int addMat(Mtl * mat, bool isNOX);
	int numTris;
	int numDoneTris;
	_locale_t locale;

	bool camerasOK;
	bool exportAndRender;
	HWND hInfoWindow;

	void setProgress(int p);	// 0 - 1000
	void setProgInfo(TCHAR * info);
	RendProgressCallback *prog;
	float unitScale;

	void resetMe();	 // will delete all stuff and set everything as after calling constructor

	XMLScene(bool asRenderer);
	~XMLScene();
};

INT_PTR CALLBACK NOXExportInfoDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

//--------------------------------------------------------------------------------------------------------------------------------------

class ValidateCameras
{
private:
	bool foundActive;
	bool foundTooMany;
	void checkDeeper(INode * node);

public:
	bool check(INode * rootNode);
	void deactivateAll(INode * rootNode);
	
	ValidateCameras() { foundActive = false; foundTooMany = false; }
	~ValidateCameras() {}
};

//--------------------------------------------------------------------------------------------------------------------------------------

class BinaryScenePlugin
{
public:
	bool saveScene(const TCHAR * filename);



	unsigned long long evalSizeScene();
	unsigned long long evalSizeGeometry();
	unsigned long long evalSizeMesh(unsigned int num);
	unsigned long long evalSizeMeshSrc(unsigned int num);
	unsigned long long evalSizeInstSrc(unsigned int num);
	unsigned long long evalSizeInstance(unsigned int num);
	unsigned long long evalSizeTriangle();
	unsigned long long evalSizeTriangleOld();
	unsigned long long evalSizeCamera(unsigned int num);
	unsigned long long evalSizeMaterial(unsigned int num);
	unsigned long long evalSizeMaterialPreview(unsigned int num);
	unsigned long long evalSizeMaterials();
	unsigned long long evalSizeMaterialLayer(unsigned int num, unsigned int numlayer);
	unsigned long long evalSizeSunsky();
	unsigned long long evalSizeBlendLayers();
	unsigned long long evalSizeUserColors();
	unsigned long long evalSizeSceneInfo();
	unsigned long long evalSizeSceneSettings();
	unsigned long long evalSizeRendererSettings();
	unsigned long long evalSizeTexture(unsigned int nummat, unsigned int numlayer, unsigned int slot);
	unsigned long long evalSizeEnvironmentMap();
	float getMaxShutter();

	bool insertScenePart(FILE * file);
	bool insertGeometryPart(FILE * file);
	bool insertMeshPart(FILE * file, unsigned int numMesh);	// deprecated
	bool insertMeshSrcPart(FILE * file, unsigned int numSrc);
	bool insertInstance(FILE * file, unsigned int numInst);
	bool insertInstSource(FILE * file, unsigned int numSrc);
	bool insertTrianglePart(FILE * file, unsigned int numMesh, unsigned int numTri);
	bool insertCameraPart(FILE * file, unsigned int numCam);
	bool insertMaterialsAllPart(FILE * file);
	bool insertMaterialSinglePart(FILE * file, unsigned int numMat);
	bool insertMaterialPreviewPart(FILE * file, unsigned int numMat);
	bool insertMatLayerPart(FILE * file, unsigned int numMat, unsigned int numLay);
	bool insertMatLayerNonNOXPart(FILE * file, unsigned int numMat);
	bool insertMatPreviewPart(FILE * file, unsigned int numMat);
	bool insertTexturePart(FILE * file, unsigned int nummat, unsigned int numlayer, unsigned int slot);
	bool insertSunskyPart(FILE * file);
	bool insertBlendLayersPart(FILE * file);
	bool insertSceneInfoPart(FILE * file);
	bool insertSceneSettingsPart(FILE * file);
	bool insertRendererSettingsPart(FILE * file);
	bool insertUserColorsPart(FILE * file);
	bool insertEnvironmentPart(FILE * file);
	char * xorString(char * src, char * pswd, unsigned int lsrc, unsigned int lpswd);


	vector<MeshInst> meshinstances;
	vector<MeshInstSrc> meshsources;

	vector<INode*> meshes;
	vector<INode*> cameras;
	vector<Mtl*> mats;
	INode * sunsky;
	vector<bool> matTypes;
	int addMat(Mtl * mat, bool isNOX);
	int numTris;
	int numDoneTris;
	//_locale_t locale;
	Mtl * overrideMat;
	bool overrideMatIsNOX;
	bool mb_on;
	float mb_duration;

	bool camerasOK;
	bool exportAndRender;
	HWND hInfoWindow;

	unsigned long long rGeometrySize;
	unsigned long long rSceneSize;


	void setProgress(int p);	// 0 - 1000
	void setProgInfo(TCHAR * info);
	RendProgressCallback *prog;
	float unitScale;

	void resetMe();	 // will delete all stuff and set everything as after calling constructor

	BinaryScenePlugin(bool asRenderer);
	~BinaryScenePlugin();
};

//--------------------------------------------------------------------------------------------------------------------------------------

class SceneEnum : public ITreeEnumProc
{
public:
	ExpInterface *expInterface;
	XMLScene * xscene;
	BinaryScenePlugin * bscene;
	int callback(INode *  node);
	bool saveAsXML;
	SceneEnum(ExpInterface *ei, XMLScene * xScene)			{ expInterface = ei; xscene = xScene; bscene = NULL; saveAsXML = true;}
	SceneEnum(ExpInterface *ei, BinaryScenePlugin * bScene) { expInterface = ei; xscene = NULL; bscene = bScene; saveAsXML = false;}
};

//--------------------------------------------------------------------------------------------------------------------------------------

#endif

