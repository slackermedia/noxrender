#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"
#include "log.h"

extern HMODULE hDllModule;
extern _locale_t noxLocale;
extern _locale_t noxLocaleComa;

#define BWIDTH 15
// HINT: control width = edit width + BWIDTH + 2

#define BNU_X 10
#define BNU_Y 8
#define BNU_W 15
#define BNU_H 10
#define BND_X 10
#define BND_Y 18
#define BND_W 15
#define BND_H 10

#define BMU_X 30
#define BMU_Y 8
#define BMU_W 15
#define BMU_H 10
#define BMD_X 30
#define BMD_Y 18
#define BMD_W 15
#define BMD_H 10

#define BCU_X 50
#define BCU_Y 8
#define BCU_W 15
#define BCU_H 10
#define BCD_X 50
#define BCD_Y 18
#define BCD_W 15
#define BCD_H 10

#define BDU_X 70
#define BDU_Y 8
#define BDU_W 15
#define BDU_H 10
#define BDD_X 70
#define BDD_Y 18
#define BDD_W 15
#define BDD_H 10

//----------------------------------------------------------------------

void InitEM2EditSpin()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2EditSpin";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2EditSpinProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2EditSpin *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

//----------------------------------------------------------------------

EM2EditSpin::EM2EditSpin(HWND hWnd)
{
	colBackground = NGCOL_TEXT_BACKGROUND;
	colEditBackground = RGB(0x3f,0x3f,0x3f);
	colEditText = NGCOL_LIGHT_GRAY;
	colDisabledEditBackground = RGB(192,192,192);
	colDisabledEditText = RGB(96,96,96);

	colEditBorderOuter = RGB(0x10,0x10,0x10);
	colEditBorderLeft = RGB(0x3f,0x3f,0x3f);
	colEditBorderTop = RGB(0x3f,0x3f,0x3f);
	colEditBorderRight = RGB(0x47,0x47,0x47);
	colEditBorderBottom = RGB(0x4e,0x4e,0x4e);

	colDisabledEditBorderOuter = RGB(0x17,0x17,0x17);
	colDisabledEditBackground = RGB(0x28,0x28,0x28);
	colDisabledEditText = NGCOL_DARK_GRAY;
	colDisabledEditText = NGCOL_TEXT_DISABLED;

	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	autodelfont = true;

	hEditBrush = CreateSolidBrush(colEditBackground);
	hEditDisabledBrush = CreateSolidBrush(colDisabledEditBackground);
	hwnd = hWnd;
	hEdit = 0;

	bgImage = 0;
	bgShiftX = bgShiftY = 0;

	nowScrolling = false;
 	clicked1 = false;
	clicked2 = false;
	overButton1 = false;
	overButton2 = false;

	allowFloat = true;

	RECT rect;
	GetClientRect(hWnd, &rect);

	buttonToEditSpace = 2;

	updateButtonRect(&rect);

	minIntValue = 0;
	maxIntValue = 100;
	minFloatValue = 0;
	maxFloatValue = 100;
	intDefaultValue = 0;
	floatDefaultValue = 0.0f;
	intValue = minIntValue;
	floatValue = minFloatValue;
	intChangeValue = 1;
	floatChangeValue = 1;
	intMouseChangeValue = 0.1f;
	floatMouseChangeValue = 0.1f;
	floatAfterDot = 2;
}

//----------------------------------------------------------------------

EM2EditSpin::~EM2EditSpin()
{
	if (hEditBrush)
		DeleteObject(hEditBrush);
	hEditBrush = 0;
	if  (hFont  &&  autodelfont)
		DeleteObject(hFont);
	hFont = 0;
}

//----------------------------------------------------------------------

EM2EditSpin * GetEM2EditSpinInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2EditSpin * emes = (EM2EditSpin *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2EditSpin * emes = (EM2EditSpin *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emes;
}

//----------------------------------------------------------------------

void SetEM2EditSpinInstance(HWND hwnd, EM2EditSpin *emes)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emes);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emes);
	#endif
}

//----------------------------------------------------------------------

typedef LRESULT (WINAPI * EDITPROC) (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
EDITPROC OldEditSpin2Proc;

//----------------------------------------------------------------------

void verifyEditSpin2ControlData(HWND hwnd, int &lastIntValue, float &lastFloatValue)
{
	char * addr;
	char * addr1;
	char * addr2;
	char buff[512];
	memset(buff, 0, 512);
	((WORD*)buff)[0] = 512;
	int cpied = (int)SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
	if (cpied>=0)
		buff[cpied]=0;
	HWND hFazia = GetParent(hwnd);
	EM2EditSpin * emes;
	emes = GetEM2EditSpinInstance(hFazia);
	bool notify  = false;
	if (emes->allowFloat)
	{
		float a = (float)_strtod_l(buff, &addr1, noxLocale);
		float b = (float)_strtod_l(buff, &addr2, noxLocaleComa);
		if (addr1 == buff   &&   addr2 == buff)
		{	// conversion error (to float)
			a = lastFloatValue;
			emes->floatToEditbox(a);
		}
		else
		{
			if (addr2>addr1)
				a = b;
			if (a > emes->maxFloatValue)
				a = emes->maxFloatValue;
			if (a < emes->minFloatValue)
				a = emes->minFloatValue;
			if (lastFloatValue != a)
				if (fabs(lastFloatValue-a) > pow(0.1f, emes->floatAfterDot))
					notify = true;
			emes->floatValue = a;
			emes->floatToEditbox(a);
			lastFloatValue = a;
		}
	}
	else
	{
		int a = (int)_strtoi64(buff, &addr, 10);
		if (addr == buff)
		{	// conversion error (to int)
			a = lastIntValue;
			emes->intToEditbox(a);
		}
		else
		{
			if (a > emes->maxIntValue)
				a = emes->maxIntValue;
			if (a < emes->minIntValue)
				a = emes->minIntValue;
			if (lastIntValue != a)
				notify = true;
			emes->intValue = a;
			emes->intToEditbox(a);
			lastIntValue = a;
		}
	}
	if (notify)
		emes->notifyScrolling();
	InvalidateRect(hFazia, NULL, FALSE);
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2EditSpinEditProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static int lastIntValue = 0;
	static float lastFloatValue = 0;

	switch (msg)
	{
	case 65001:
		{
			lastIntValue = (int&)wParam;
			lastFloatValue = (float&)lParam;
		}
		break;
	case WM_CHAR:
	//case WM_KEYDOWN:
		{
			if (wParam == VK_RETURN)
			{
				verifyEditSpin2ControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_RETURN), (LPARAM)hwnd);
				SendMessage(hwnd, EM_SETSEL, 0, -1);
				return TRUE;
			}
			if (wParam == VK_ESCAPE)
			{
				HWND hFazia = GetParent(hwnd);
				EM2EditSpin * emes = GetEM2EditSpinInstance(hFazia);
				if (emes->allowFloat)
				{
					emes->floatValue = lastFloatValue;
					emes->floatToEditbox(lastFloatValue);
				}
				else
				{
					emes->intValue = lastIntValue;
					emes->intToEditbox(lastIntValue);
				}
				return TRUE;
			}
			if (wParam == VK_TAB)
			{
				verifyEditSpin2ControlData(hwnd, lastIntValue, lastFloatValue);
				LONG wID = GetWindowLong(hwnd, GWL_ID);
				if (GetKeyState(VK_SHIFT)&0x8000)
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB_SHIFT), (LPARAM)hwnd);
				else
					SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, ES_TAB), (LPARAM)hwnd);

				return TRUE;
			}
		}
		break;
	case WM_KILLFOCUS:
		{
			verifyEditSpin2ControlData(hwnd, lastIntValue, lastFloatValue);
		}
		break;
	case WM_SETFOCUS:
		{
			char * addr;
			char buff[512];
			((WORD*)buff)[0] = 512;
			SendMessage(hwnd, EM_GETLINE, 0, (LPARAM)buff);
			HWND hFazia = GetParent(hwnd);
			EM2EditSpin * emes;
			emes = GetEM2EditSpinInstance(hFazia);
			if (emes->allowFloat)
			{
				float a = (float)strtod(buff, &addr);
				if (addr == buff)
				{	// conversion error (to float)
				}
				else
				{
					if (a > emes->maxFloatValue)
						a = emes->maxFloatValue;
					if (a < emes->minFloatValue)
						a = emes->minFloatValue;
					lastFloatValue = a;
				}
			}
			else
			{
				int a = (int)_strtoi64(buff, &addr, 10);
				if (addr == buff)
				{	// conversion error (to int)
				}
				else
				{
					if (a > emes->maxIntValue)
						a = emes->maxIntValue;
					if (a < emes->minIntValue)
						a = emes->minIntValue;
					lastIntValue = a;
				}
			}
			SendMessage(hwnd, EM_SETSEL, 0, -1);
		}
		break;
	case WM_NCCALCSIZE:
		{
			if (lParam)
			{
				NCCALCSIZE_PARAMS * params = (NCCALCSIZE_PARAMS *)lParam;
				RECT trect = params->rgrc[0];
				HDC hdc = GetDC(hwnd);

				EM2EditSpin * emes = GetEM2EditSpinInstance(GetParent(hwnd));
				HFONT hfont = emes ? emes->getFont() : (HFONT)GetStockObject(DEFAULT_GUI_FONT);
				HFONT hOldFont = (HFONT)SelectObject(hdc, hfont);
				DrawText(hdc, "Ky", 2, &trect, DT_CALCRECT | DT_LEFT);

				SelectObject(hdc, hOldFont);
				ReleaseDC(hwnd, hdc);

				int p = ((params->rgrc[0].bottom - params->rgrc[0].top) - (trect.bottom-trect.top));
				if (p>0)
					params->rgrc[0].top += p/2;
				if (p<0)
				{
					params->rgrc[0].bottom += -p;
				}
			}
			return WVR_ALIGNBOTTOM;
		}
		break;
	default:
		return CallWindowProc(OldEditSpin2Proc, hwnd, msg, wParam, lParam);
	}

	return CallWindowProc(OldEditSpin2Proc, hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

LRESULT CALLBACK EM2EditSpinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2EditSpin * emes;
	emes = GetEM2EditSpinInstance(hwnd);
	static int lastPosY;
	static int lastIntValue;
	static float lastFloatValue;
	
    switch(msg)
    {
	case WM_CREATE:
		{
			EM2EditSpin * emes1 = new EM2EditSpin(hwnd);
			SetEM2EditSpinInstance(hwnd, (EM2EditSpin *)emes1);
			emes1->hEdit = CreateWindow("EDIT", "", 
				WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VISIBLE | ES_CENTER 
				| ES_MULTILINE | ES_WANTRETURN,
				1, 1, emes1->editRectWidth-2, emes1->height-2,
				hwnd, (HMENU)0, NULL, 0);
			SendMessage(emes1->hEdit, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

			#ifdef _WIN64
				OldEditSpin2Proc = (EDITPROC) SetWindowLongPtr(emes1->hEdit, GWLP_WNDPROC, (LONG_PTR)EM2EditSpinEditProc) ;
			#else
				OldEditSpin2Proc = (EDITPROC) (HANDLE)(LONG_PTR)SetWindowLong(emes1->hEdit, GWL_WNDPROC, (LONG)(LONG_PTR)EM2EditSpinEditProc) ;
			#endif

			if (emes1->allowFloat)
				emes1->floatToEditbox(emes1->floatValue);
			else
				emes1->intToEditbox(emes1->intValue);

			SetWindowPos(emes1->hEdit, HWND_TOP, 2, 2, emes1->editRectWidth-4, emes1->height-4, SWP_NOZORDER | SWP_FRAMECHANGED);
		}
		break;
	case WM_SIZE:
		{
			RECT rect;
			rect.left = 0;
			rect.top = 0;
			rect.right = (int)(short)LOWORD(lParam);
			rect.bottom = (int)(short)HIWORD(lParam);
			emes->updateButtonRect(&rect);
			SetWindowPos(emes->hEdit, HWND_TOP, 2, 2, emes->editRectWidth-4, emes->height-4, SWP_NOZORDER | SWP_FRAMECHANGED);
		}
		break;
	case WM_CLOSE:
		{
			return TRUE;
		}
		break;
	case WM_DESTROY:
		{
			EM2EditSpin * emes1;
			emes1 = GetEM2EditSpinInstance(hwnd);
			delete emes1;
			SetEM2EditSpinInstance(hwnd, 0);
		}
		break;
	case WM_CTLCOLORSTATIC:
	case WM_CTLCOLOREDIT:
	{
		HDC hdc1 = (HDC)wParam;
		DWORD style;
		style = GetWindowLong(hwnd, GWL_STYLE);
		bool disabled = ((style & WS_DISABLED) > 0);
		if (!disabled)
		{
			SetTextColor(hdc1, emes->colEditText);
			SetBkColor(hdc1, emes->colEditBackground);
		}
		else
		{
			SetTextColor(hdc1, emes->colDisabledEditText);
			SetBkColor(hdc1, emes->colDisabledEditBackground);
		}
		if (disabled)
			return (INT_PTR)(emes->hEditDisabledBrush);
		else
			return (INT_PTR)(emes->hEditBrush);
	}
		break;
	case WM_ENABLE:
		{
			if (GetFocus()==emes->hEdit)
				SetFocus(0);
			SendMessage(emes->hEdit, EM_SETREADONLY, wParam?FALSE:TRUE, 0);
		}
		break;
	case WM_PAINT:
		{
			if (!emes) 
				break;

			RECT rect, sRect;
			HDC hdc, thdc;
			PAINTSTRUCT ps;
			HPEN hOldPen;

			bool drawButton1Clicked = emes->clicked1;
			bool drawButton2Clicked = emes->clicked2;

			bool disabled = (((GetWindowLong(hwnd, GWL_STYLE)) & WS_DISABLED) > 0);

			// get DC and create backbuffer DC
			hdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rect);
			thdc = CreateCompatibleDC(hdc); 
			HBITMAP hbmp = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
			HBITMAP oldBMP = (HBITMAP)SelectObject (thdc, hbmp);

			// get spinner part rect
			sRect = rect;
			sRect.left = emes->editRectWidth;

			// draw background
			if (emes->bgImage)
			{
				HDC thdc2 = CreateCompatibleDC(thdc);			// image buffer thdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc2, emes->bgImage);
				BitBlt(thdc, 0, 0, rect.right, rect.bottom, thdc2, emes->bgShiftX, emes->bgShiftY, SRCCOPY);
				SelectObject(thdc2, oldBitmap);
				DeleteDC(thdc2);
				SetBkMode(thdc, TRANSPARENT);
			}
			else
			{
				HPEN hOldPen2 = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colBackground));
				HBRUSH hOldBrush = (HBRUSH)SelectObject(thdc, CreateSolidBrush(emes->colBackground));
				Rectangle(thdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(thdc, hOldBrush));
				DeleteObject(SelectObject(thdc, hOldPen2));
				SetBkMode(thdc, OPAQUE);
				SetBkColor(thdc, emes->colBackground);
			}

			// draw editbox background - its client rect might be shifted
			RECT erect = { 1,1, emes->editRectWidth-1, emes->height-1 };
			HBRUSH bgeBr = CreateSolidBrush(disabled ? emes->colDisabledEditBackground : emes->colEditBackground);
			FillRect(thdc, &erect, bgeBr);
			DeleteObject(bgeBr);


			// draw editbox border
			hOldPen = (HPEN)SelectObject(thdc, CreatePen(PS_SOLID, 1, disabled ? emes->colDisabledEditBorderOuter : emes->colEditBorderOuter));
			HBRUSH hOldBrush = (HBRUSH)SelectObject(thdc, GetStockObject(NULL_BRUSH));
			Rectangle(thdc, 0,0, emes->editRectWidth, rect.bottom);
			DeleteObject(SelectObject(thdc, hOldBrush));
			// inner
			if (!disabled)
			{
				MoveToEx(thdc, 1, rect.bottom-2, NULL);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colEditBorderLeft)));
				LineTo(thdc, 1, 1);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colEditBorderTop)));
				LineTo(thdc, emes->editRectWidth-2, 1);
				LineTo(thdc, emes->editRectWidth-2, 2);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colEditBorderRight)));
				LineTo(thdc, emes->editRectWidth-2, rect.bottom-2);
				DeleteObject(SelectObject(thdc, CreatePen(PS_SOLID, 1, emes->colEditBorderBottom)));
				LineTo(thdc, 1, rect.bottom-2);
				DeleteObject(SelectObject(thdc, hOldPen));
			}


			// draw arrows
			int bpos_u = rect.bottom/2 - BNU_H;
			int bpos_d = (rect.bottom+1)/2;
			int bpos_l = emes->editRectWidth+emes->buttonToEditSpace;
			HBITMAP hBArrrows = EM2Elements::getInstance()->getHBitmap();
			HDC thdc2 = CreateCompatibleDC(thdc);
			HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc2, hBArrrows);
			BLENDFUNCTION blendFunction;
			blendFunction.BlendOp = AC_SRC_OVER;
			blendFunction.BlendFlags = 0;
			blendFunction.SourceConstantAlpha = 255;
			blendFunction.AlphaFormat = AC_SRC_ALPHA;
			if (disabled)
				AlphaBlend(thdc, bpos_l, bpos_u,  BDU_W,BDU_H ,thdc2, BDU_X,BDU_Y,BDU_W,BDU_H, blendFunction);
			else
				if (emes->clicked1)
					AlphaBlend(thdc, bpos_l, bpos_u,  BCU_W,BCU_H ,thdc2, BCU_X,BCU_Y,BCU_W,BCU_H, blendFunction);
				else
					if (emes->overButton1  &&  !emes->nowScrolling)
						AlphaBlend(thdc, bpos_l, bpos_u,  BMU_W,BMU_H ,thdc2, BMU_X,BMU_Y,BMU_W,BMU_H, blendFunction);
					else
						AlphaBlend(thdc, bpos_l, bpos_u,  BNU_W,BNU_H ,thdc2, BNU_X,BNU_Y,BNU_W,BNU_H, blendFunction);
			if (disabled)
				AlphaBlend(thdc, bpos_l, bpos_d, BDD_W,BDD_H ,thdc2, BDD_X,BDD_Y,BDD_W,BDD_H, blendFunction);
			else
				if (emes->clicked2)
					AlphaBlend(thdc, bpos_l, bpos_d, BCD_W,BCD_H ,thdc2, BCD_X,BCD_Y,BCD_W,BCD_H, blendFunction);
				else
					if (emes->overButton2  &&  !emes->nowScrolling)
						AlphaBlend(thdc, bpos_l, bpos_d, BMD_W,BMD_H ,thdc2, BMD_X,BMD_Y,BMD_W,BMD_H, blendFunction);
					else
						AlphaBlend(thdc, bpos_l, bpos_d, BND_W,BND_H ,thdc2, BND_X,BND_Y,BND_W,BND_H, blendFunction);
			SelectObject(thdc2, oldBitmap);
			DeleteDC(thdc2);

			// copy from back buffer and free resources
			BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, 0, 0, SRCCOPY);
			SelectObject(thdc, oldBMP);
			DeleteObject(hbmp);
			DeleteDC(thdc);
			EndPaint(hwnd, &ps);

			InvalidateRect(emes->hEdit, NULL, false);
			ShowWindow(emes->hEdit, SW_SHOW);
			UpdateWindow(emes->hEdit);
		}
		break;
	case WM_MOUSEMOVE:
		{
			if (!GetCapture())
				SetCapture(hwnd);
			if (GetCapture() == hwnd)
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				rect.left = emes->editRectWidth;
				POINT pt;
				pt.x = (short)LOWORD(lParam);
				pt.y = (short)HIWORD(lParam);
				if (PtInRect(&rect, pt) || emes->nowScrolling)
				{
					if (PtInRect(&(emes->rUpper), pt))
						emes->overButton1 = true;
					else
						emes->overButton1 = false;

					if (PtInRect(&(emes->rLower), pt))
						emes->overButton2 = true;
					else
						emes->overButton2 = false;

					if (emes->clicked1 || emes->clicked2)
					{
						int yRes = GetSystemMetrics(SM_CYSCREEN);
						POINT pt1 = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
						ClientToScreen(hwnd, &pt1);
						if (pt1.y > yRes-2)
						{
							SetCursorPos(pt1.x, 1);
							lastPosY -= yRes;
							break;
						}
						if (pt1.y < 1)
						{
							SetCursorPos(pt1.x, yRes-2);
							lastPosY += yRes;
							break;
						}

						if (emes->allowFloat)
						{
							emes->floatValue = lastFloatValue + (lastPosY - (short)HIWORD(lParam))*emes->floatMouseChangeValue;
						}
						else
						{
							emes->intValue = lastIntValue + (int)((lastPosY - (short)HIWORD(lParam))*emes->intMouseChangeValue);
						}
						emes->updateChangedValue();
						emes->notifyScrolling();
					}
				}
				else
				{
					emes->overButton1 = false;
					emes->overButton2 = false;
					emes->clicked1 = false;
					emes->clicked2 = false;
					ReleaseCapture();
				}
				InvalidateRect(hwnd, &rect, FALSE);
			}
		}
		break;
	case WM_CAPTURECHANGED:
		{
			emes->overButton1 = false;
			emes->overButton2 = false;
			emes->nowScrolling = false;
			InvalidateRect(hwnd, NULL, false);
		}
		break;
	case WM_LBUTTONDOWN:
		{
			RECT rect;
			GetClientRect(hwnd, &rect);
			rect.left = emes->editRectWidth;
			POINT pt = { LOWORD(lParam), HIWORD(lParam) };
			if (PtInRect(&rect, pt))
			{
				SetFocus(hwnd);
				if (PtInRect(&(emes->rUpper), pt))
				{	// clicked upper button
					emes->clicked1 = true;
					emes->overButton1 = true;
					emes->clicked2 = false;
					emes->overButton2 = false;
					emes->nowScrolling = true;
					if (emes->allowFloat)
					{
						emes->floatValue += emes->floatChangeValue;
						lastFloatValue = emes->floatValue;

					}
					else
					{
						lastIntValue = emes->intValue;
						emes->intValue += emes->intChangeValue;
					}
					lastPosY = pt.y;
					emes->updateChangedValue();
					emes->notifyScrolling();
				}
				if (PtInRect(&(emes->rLower), pt))
				{	// clicked lower button
					emes->clicked1 = false;
					emes->overButton1 = false;
					emes->clicked2 = true;
					emes->overButton2 = true;
					emes->nowScrolling = true;
					if (emes->allowFloat)
					{
						emes->floatValue -= emes->floatChangeValue;
						lastFloatValue = emes->floatValue;

					}
					else
					{
						lastIntValue = emes->intValue;
						emes->intValue -= emes->intChangeValue;
					}
					lastPosY = pt.y;
					emes->updateChangedValue();
					emes->notifyScrolling();
				}
				RECT rect;
				GetClientRect(hwnd, &rect);
				InvalidateRect(hwnd, &rect, FALSE);
			}

		}
		break;
	case WM_LBUTTONUP:
		{
			emes->clicked1 = false;
			emes->clicked2 = false;
			emes->nowScrolling = false;
			{
				ReleaseCapture();
			}
			RECT rect;
			GetClientRect(hwnd, &rect);
			InvalidateRect(hwnd, &rect, FALSE);
			emes->notifyScrolling();
		}
		break;
	case WM_RBUTTONUP:
		{
			bool notify = false;
			if (emes->allowFloat)
			{
				notify = (emes->floatDefaultValue != emes->floatValue);
				emes->setFloatValue(emes->floatDefaultValue);
			}
			else
			{
				notify = (emes->intDefaultValue != emes->intValue);
				emes->setIntValue(emes->intDefaultValue);
			}
			if (notify)
				emes->notifyScrolling();
		}
		break;
	case WM_SETFOCUS:
		{
			SetFocus(emes->hEdit);
		}
		break;
	case WM_COMMAND:
	{
		int wmId, wmEvent;
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		
		LONG editID = GetWindowLong(emes->hEdit, GWL_ID);
		if (wmId == editID  &&   wmEvent == ES_RETURN)
		{
		}
		if (wmId == editID  &&   wmEvent == ES_TAB)
		{
			HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, false);
			if (hNext != hwnd)
				SetFocus(hNext);
		}
		if (wmId == editID  &&   wmEvent == ES_TAB_SHIFT)
		{
			HWND hNext = GetNextDlgTabItem(GetParent(hwnd), hwnd, true);
			if (hNext != hwnd)
				SetFocus(hNext);
		}
	}
	break;
    default:
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

//----------------------------------------------------------------------

void EM2EditSpin::floatToEditbox(float value)
{
	char eValue[64];
	char temp[64];
	sprintf_s(temp, 64, "%%.%df", floatAfterDot);
	sprintf_s(eValue, 64, temp, value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}

//----------------------------------------------------------------------

void EM2EditSpin::intToEditbox(int value)
{
	char eValue[64];
	sprintf_s(eValue, 64, "%d", value);
	SendMessage(hEdit, (UINT) WM_SETTEXT, 0, (LPARAM)eValue);  
}

//----------------------------------------------------------------------

void EM2EditSpin::setValuesToInt(int value, int minVal, int maxVal, int defaultValue, int changeValue, float mouseChangeValue)
{
	minIntValue = minVal;
	maxIntValue = maxVal;
	value = min(maxIntValue, max(minIntValue, value));
	intDefaultValue = min(maxIntValue, max(minIntValue, defaultValue));

	setIntValue(value);
	allowFloat = false;
	intToEditbox(value);
	intChangeValue = changeValue;
	intMouseChangeValue = mouseChangeValue;
}

//----------------------------------------------------------------------

void EM2EditSpin::setValuesToFloat(float value, float minVal, float maxVal, float defaultValue, float changeValue, float mouseChangeValue)
{
	minFloatValue = minVal;
	maxFloatValue = maxVal;
	value = min(maxFloatValue, max(minFloatValue, value));
	floatDefaultValue = min(maxFloatValue, max(defaultValue, minFloatValue));

	allowFloat = true;
	setFloatValue(value);
	floatToEditbox(value);
	floatChangeValue = changeValue;
	floatMouseChangeValue = mouseChangeValue;
}

//----------------------------------------------------------------------

void EM2EditSpin::setIntValue(int value)
{
	intValue = min(maxIntValue, max(minIntValue, value));
	if (!allowFloat)
		intToEditbox(intValue);
}

//----------------------------------------------------------------------

void EM2EditSpin::setFloatValue(float value)
{
	floatValue = min(maxFloatValue, max(minFloatValue, value));
	if (allowFloat)
		floatToEditbox(floatValue);
}

//----------------------------------------------------------------------

void EM2EditSpin::updateChangedValue()
{
	if (allowFloat)
	{
		if (floatValue > maxFloatValue)
			floatValue = maxFloatValue;
		if (floatValue < minFloatValue)
			floatValue = minFloatValue;
		floatToEditbox(floatValue);
	}
	else
	{
		if (intValue > maxIntValue)
			intValue = maxIntValue;
		if (intValue < minIntValue)
			intValue = minIntValue;
		intToEditbox(intValue);
	}
}

//----------------------------------------------------------------------

void EM2EditSpin::notifyScrolling()
{
	LONG wID;
	wID = GetWindowLong(hwnd, GWL_ID);
	SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, WM_VSCROLL), (LPARAM)hwnd);
}

//----------------------------------------------------------------------

void EM2EditSpin::updateButtonRect(RECT * clientRect)
{
	RECT rect;
	if (clientRect)
		rect = *clientRect;
	else
		GetClientRect(hwnd, &rect);

	height = rect.bottom;

	rEdit = rect;
	editRectWidth = rect.right - BWIDTH - buttonToEditSpace;
	rEdit.right = editRectWidth;

	rUpper = rect;
	rUpper.left = editRectWidth + 1;
	rUpper.bottom = height/2;
	rUpper.top = rUpper.bottom - BNU_H;

	rLower = rect;
	rLower.left = editRectWidth + 1;
	rLower.top = (height+1)/2;
	rLower.bottom = rLower.top + BND_H;
}

//----------------------------------------------------------------------

bool EM2EditSpin::setFont(HFONT hNewFont, bool autodel)
{
	if (autodelfont)
		DeleteObject(hFont);
	hFont = hNewFont;
	autodelfont = autodel;
	SendMessage(hEdit, WM_SETFONT, (WPARAM)(hFont?hFont:GetStockObject(DEFAULT_GUI_FONT)), TRUE);
	SetWindowPos(hEdit, HWND_TOP, 0,0,0,0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED);
	return true;
}

//----------------------------------------------------------------------

HFONT EM2EditSpin::getFont()
{
	return (hFont ? hFont : (HFONT)GetStockObject(DEFAULT_GUI_FONT));
	return hFont;
}

//----------------------------------------------------------------------

void EM2EditSpin::setAlign(int newalign)
{
	DWORD style = GetWindowLong(hEdit, GWL_STYLE);
	style &= ~(ES_CENTER|ES_LEFT|ES_RIGHT);
	switch (newalign)
	{
		case ALIGN_LEFT:	style |= ES_LEFT;	break;
		case ALIGN_CENTER:	style |= ES_CENTER;	break;
		case ALIGN_RIGHT:	style |= ES_RIGHT;	break;
		default :			style |= ES_LEFT;	break;
	}
	SetWindowLong(hEdit, GWL_STYLE, style);
}

//----------------------------------------------------------------------
