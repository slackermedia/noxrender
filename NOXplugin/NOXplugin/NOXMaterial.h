#ifndef __NOX_MATERIAL_H__
#define __NOX_MATERIAL_H__

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

#include "stdmat.h"
//#include "IMaterialViewportShading.h"
#include "imtl.h"
#include "iviewportmanager.h"

#define DONT_INCLUDE_EVER_RENDERER
#include "linkRenderer.h"
#include "NOXPlugin.h"

#define MAT_NOX_NUM_REFS 163
#define MAT_NOX_REF_PARAMBLOCK 162

INT_PTR CALLBACK NOXMatTexUpdateParamDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);

#define NOXMaterial_CLASS_ID	Class_ID(0x2ed06c97, 0x12df189a)

//--------------------------------------------------------------------------------------------------------------

class NOXMaterial: 
	public Mtl
{
public:
	(BitmapTex *) texRough[16];
	(BitmapTex *) texRefl0[16];
	(BitmapTex *) texRefl90[16];
	(BitmapTex *) texWeight[16];
	(BitmapTex *) texLight[16];
	(BitmapTex *) texNormal[16];
	(BitmapTex *) texTransm[16];
	(BitmapTex *) texAniso[16];
	(BitmapTex *) texAnisoAngle[16];
	BitmapTex * texOpacity;
	BitmapTex * texDisplacement;

	BitmapTex * currentTexture;
	bool showTex;
	BOOL MapEnabled(int id)										{ return showTex ? TRUE : FALSE; }
	void changeTexture(int newID, int nLayer);
	void getTexturesFromNOX();

	Interval		ivalid;
		
	ParamDlg *CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
	void Update(TimeValue t, Interval& valid);
	Interval Validity(TimeValue t);
	void Reset();

	// From MtlBase and Mtl
	void SetAmbient(Color c, TimeValue t);
	void SetDiffuse(Color c, TimeValue t);
	void SetSpecular(Color c, TimeValue t);
	void SetShininess(float v, TimeValue t);
	Color GetAmbient(int mtlNum=0, BOOL backFace=FALSE);
	Color GetDiffuse(int mtlNum=0, BOOL backFace=FALSE);
	Color GetSpecular(int mtlNum=0, BOOL backFace=FALSE);
	float GetXParency(int mtlNum=0, BOOL backFace=FALSE);
	float GetShininess(int mtlNum=0, BOOL backFace=FALSE);
	float GetShinStr(int mtlNum=0, BOOL backFace=FALSE);
	float WireSize(int mtlNum=0, BOOL backFace=FALSE);
		
	BOOL  GetSelfIllumColorOn (int mtlNum=0, BOOL backFace=FALSE);
	float  GetSelfIllum (int mtlNum=0, BOOL backFace=FALSE);
	Color  GetSelfIllumColor (int mtlNum=0, BOOL backFace=FALSE);
	float GetOpacity  (  TimeValue  t   )  {return 0.5f;}
	float GetIOR  (  TimeValue  t   )  {return 1.6f;}
	float GetOpacFalloff  (  TimeValue  t   )  {return 0.5f;}
 
	void Shade(ShadeContext& sc);
		
	BOOL SetDlgThing(ParamDlg* dlg);
	NOXMaterial(BOOL loading);
	~NOXMaterial();

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	Class_ID ClassID() {return NOXMaterial_CLASS_ID;}
	SClass_ID SuperClassID() { return MATERIAL_CLASS_ID; }
	void GetClassName(TSTR& s) {s = GetString(IDS_NOX_MAT_CLASS_NAME);}

	RefTargetHandle Clone( RemapDir &remap );
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,
					PartID& partID,  RefMessage message);

	int NumRefs() { return 163; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);

	IParamBlock2 *pb_test;
	int	NumParamBlocks();
	IParamBlock2* GetParamBlock(int i);
	IParamBlock2* GetParamBlockByID(BlockID id);
	void updateFromParamBlock();
	void updateToParamBlock();

	ULONG Requirements(int subMtlNum)							{ return MTLREQ_TRANSP | MTLREQ_TRANSP_IN_VP | (currentTexture ? currentTexture->Requirements(0) : 0); }
	ULONG LocalRequirements(int subMtlNum)						{ return MTLREQ_TRANSP | MTLREQ_TRANSP_IN_VP; }
	int VPDisplaySubMtl()										{ return -1; }

	void DeleteThis() { delete this; }

	bool isNOXEmitter();

	// my own
	TrMaterial mat;
	Color diffuse;
	Color ambient;
	Color specular;
	float shininess;
	float shinStr;
	float xParency;
	float wireSize;
	void setTexUpdateProgress(int nom, int den);
	HWND hTUInfoWindow;
	int lastTextureNumber;
	int lastLayerNumber;
	int curTextureNumber;
	int curLayerNumber;


};

//--------------------------------------------------------------------------------------------------------------

class NOXMaterialClassDesc : public ClassDesc2 
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new NOXMaterial(loading); }
	const TCHAR *	ClassName() { return GetString(IDS_NOX_MAT_CLASS_NAME); }
	SClass_ID		SuperClassID() { return MATERIAL_CLASS_ID; }
	Class_ID		ClassID() { return NOXMaterial_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_NOX_MAT_CATEGORY); }

	const TCHAR*	InternalName() { return _T("NOXMaterial"); }
	HINSTANCE		HInstance() { return hInstance; }
	

};

//--------------------------------------------------------------------------------------------------------------

class NOXMatParamDialog : public ParamDlg
{
public:
	NOXMaterial * eMat;
	HWND hMatPanel;
	HINSTANCE hInst;
	bool reenableAccels;

	Class_ID ClassID();
	void SetThing (ReferenceTarget *m);
	ReferenceTarget * GetThing();
	void SetTime(TimeValue t);
	void ReloadDialog();
	void DeleteThis();
	void ActivateDlg(BOOL onOff);
	void backFromMatEditor();

	NOXMatParamDialog(NOXMaterial * cMat, IMtlParams *imp, HINSTANCE hInstance);
	~NOXMatParamDialog();
};

INT_PTR CALLBACK NOXMatParamDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);

//--------------------------------------------------------------------------------------------------------------

class NOXMatPostLoad : public PostLoadCallback
{
	NOXMaterial* mat;
public:
	NOXMatPostLoad(NOXMaterial* material)  { mat = material; }
	~NOXMatPostLoad()  {}
	void proc(ILoad * iload);
};

//--------------------------------------------------------------------------------------------------------------

#endif
