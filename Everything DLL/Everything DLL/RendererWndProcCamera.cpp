#include "resource.h"
#include "RendererMainWindow.h"
//#include "MaterialEditorMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>




INT_PTR CALLBACK RendererMainWindow::CameraWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				HWND hList  = GetDlgItem(hWnd, IDC_REND_CAMERA_LISTVIEW);
				EMListView * emlv  = GetEMListViewInstance(hList);
				theme.apply(emlv);
				emlv->setColNumAndClearData(3, 100);
				emlv->colWidths[0] = 20;
				emlv->colWidths[2] = 40;
				emlv->headers.add((char*)malloc(64));
				emlv->headers.add((char*)malloc(64));
				emlv->headers.add((char*)malloc(64));
				emlv->headers.createArray();
				sprintf_s(emlv->headers[0], 64, "ID");
				sprintf_s(emlv->headers[1], 64, "Name");
				sprintf_s(emlv->headers[2], 64, "Active");

				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_GROUP_OPTICS)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_GROUP_EXPOSURE)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_GROUP_CAMPOSITION)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_GROUP_FOCUS)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_GROUP_APERTURE)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_GROUP_BOKEH)));
				theme.apply(GetEMGroupBarInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_GROUP_MOTION_BLUR)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_LENGTH)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_ISO)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_APERTURE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_SHUTTER)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_PREVIEW_BUTTON)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_SETACTIVE)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_DELETE_BUFFER)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_15)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_20)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_24)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_28)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_35)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_50)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_85)));
				theme.apply(GetEMButtonInstance  (GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_135)));
				//theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_AUTOEXP_ON)));
				//theme.apply(GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_AUTOEXP_TYPE)));
				//theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_AUTOFOCUS)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_MB_ENABLED)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_MB_STILL)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_MB_TIME)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_FOCUS_DIST)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_NUMBLADES)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_ANGLEBLADES)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BLADERADIUS)));
				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BLADES_ROUND)));
				theme.apply(GetEMPViewInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_DIAPHRAGM_SHAPE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_APERTURE_COPY)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_RING_BALANCE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_RING_SIZE)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_FLATTEN)));
				theme.apply(GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_VIGNETTE)));

				theme.apply(GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_DOF_TEMP)));

				SetWindowPos(GetDlgItem(hWnd, IDC_REND_CAMERA_DIAPHRAGM_SHAPE), HWND_TOP, 0,0, 96,96, SWP_NOMOVE);

				EMEditSpin * eFocal      = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_LENGTH));
				eFocal->setValuesToFloat(35, NOX_CAM_FOCAL_MIN, NOX_CAM_FOCAL_MAX, 1, 1);
				//EMEditSpin * eAngleHoriz = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_ANGLE_HORIZONTAL));
				//EMEditSpin * eAngleVert  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_ANGLE_VERTICAL));
				//EMEditSpin * eAngleDiag  = GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_ANGLE_DIAGONAL));
				//eAngleHoriz->setValuesToFloat(54.432223114614950936887227224536f ,    2.0624253397759056353348615078868f,   121.89079180184570959531537904652f  , 1,1);
				//eAngleVert->setValuesToFloat (37.849288832102469564560282166968f ,    1.3750327092781994843405007945091f,   100.3888578154696119874410203614f   , 1,1);
				//eAngleDiag->setValuesToFloat (63.439966595414576023891329288495f ,    2.4786078362464095286820203001355f,   130.38250389234639806577590558017f  , 1,1);

				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_ISO))->setValuesToInt(400, NOX_CAM_ISO_MIN, NOX_CAM_ISO_MAX,1, 5);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_APERTURE))->setValuesToFloat(8, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 1, 0.1f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_SHUTTER))->setValuesToFloat(40, NOX_CAM_SHUTTER_MIN, NOX_CAM_SHUTTER_MAX, 10, 1);

				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_APERTURE_COPY))->setValuesToFloat(8, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 1, 0.1f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_NUMBLADES))->setValuesToInt(5,5,16, 1 , 0.02f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_ANGLEBLADES))->setValuesToInt(0, 0, 72, 1, 0.2f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BLADERADIUS))->setValuesToFloat(1,1,10, 0.1f, 0.01f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_MB_TIME))->floatAfterDot = 4;
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_MB_TIME))->setValuesToFloat(0, 0, 0, 0.01f, 0.001f);
				//GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_MB_MAX_TIME))->setValuesToFloat(0, 0, Raytracer::getInstance()->curScenePtr->sscene.motion_blur_time, 0.01f, 0.001f);



				GetEMCheckBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BLADES_ROUND))->selected = true;
				InvalidateRect(GetDlgItem(hWnd, IDC_REND_CAMERA_BLADES_ROUND), NULL, false);
				EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_DIAPHRAGM_SHAPE));
				empv->byteBuff->allocBuffer(96,96);
				empv->byteBuff->clearBuffer();
				empv->dontLetUserChangeZoom = true;

				//EMComboBox * emcombo = GetEMComboBoxInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_AUTOEXP_TYPE));
				//emcombo->addItem("Spot");
				//emcombo->addItem("Center weighted");
				//emcombo->addItem("Matrix");
				////emcombo->selected = 

				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_RING_BALANCE))->setValuesToInt(0,NOX_CAM_BOKEH_BALANCE_MIN, NOX_CAM_BOKEH_BALANCE_MAX, 1, 0.1f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_RING_SIZE))->setValuesToInt(20,NOX_CAM_BOKEH_RING_SIZE_MIN, NOX_CAM_BOKEH_RING_SIZE_MAX, 1, 0.1f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_FLATTEN))->setValuesToInt(0,NOX_CAM_BOKEH_FLATTEN_MIN, NOX_CAM_BOKEH_FLATTEN_MAX, 1, 0.1f);
				GetEMEditSpinInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_BOKEH_VIGNETTE))->setValuesToInt(0,NOX_CAM_BOKEH_VIGNETTE_MIN, NOX_CAM_BOKEH_VIGNETTE_MAX, 1, 0.1f);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_CAMERA_LISTVIEW:
					{
						if (wmEvent == LV_SELECTION_CHANGED)
						{
							EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_LISTVIEW));
							if (!emlv)
								break;
							int sel = emlv->selected;
							Raytracer * rtr = Raytracer::getInstance();

							Camera * cam = NULL;
							if (sel < 0   ||   sel >= rtr->curScenePtr->cameras.objCount)
								//cam = NULL;
								break;
							else
								cam = rtr->curScenePtr->cameras[ sel ];

							//fillCameraData(cam);
						}
						if (wmEvent == LV_DOUBLECLIKED)
						{
							HWND hList = GetDlgItem(hWnd, IDC_REND_CAMERA_LISTVIEW);
							EMListView * emlv = GetEMListViewInstance(hList);
							if (!emlv)
								break;
							int sel = emlv->selected;
							Raytracer * rtr = Raytracer::getInstance();
						
							HWND hBPr = GetDlgItem(hCamera, IDC_REND_CAMERA_PREVIEW_BUTTON);
							DWORD style = GetWindowLong(hBPr, GWL_STYLE);
							bool disabled = ((style & WS_DISABLED) > 0);

							if (disabled)
								break;

							rtr->curScenePtr->sscene.activeCamera = sel;
							Camera * cam = rtr->curScenePtr->cameras[sel];

							changeCamera(cam);

						}
					}
					break;
					case IDC_REND_CAMERA_SETACTIVE:
					{
						HWND hList = GetDlgItem(hWnd, IDC_REND_CAMERA_LISTVIEW);
						EMListView * emlv = GetEMListViewInstance(hList);
						if (!emlv)
							break;
						int sel = emlv->selected;
						Raytracer * rtr = Raytracer::getInstance();
					
						rtr->curScenePtr->sscene.activeCamera = sel;
						Camera * cam = rtr->curScenePtr->cameras[sel];

						changeCamera(cam);

					}
					break;
					case IDC_REND_CAMERA_DELETE_BUFFER:
					{
						if (wmEvent != BN_CLICKED)
							break;
						int res = MessageBox(hWnd, "This will delete all rendering results.\nAre you sure?", "Warning!", MB_YESNO);
						if (res!=IDYES)
							break;
						Camera * cam = getSelectedCamera();
						if (!cam)
							break;

						cam->releaseAllBuffers();
						EMPView * empv = GetEMPViewInstance(hImage);
						if (empv)
						{
							empv->otherSource = NULL;
							if (empv->byteBuff)
							{
								empv->byteBuff->freeBuffer();
								delete empv->byteBuff;
								empv->byteBuff = NULL;
							}
						}
						InvalidateRect(hImage, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_ISO:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						HWND hISO      = GetDlgItem(hWnd, IDC_REND_CAMERA_ISO);
						EMEditSpin * eISO      = GetEMEditSpinInstance(hISO);
						float f = (float)eISO->intValue;
						Camera * cam = getSelectedCamera();
						if (cam)
						{
							cam->ISO = f;
							cam->iMod.setISO_camera(f);
							cam->iMod.evalMultiplierFromISO_and_EV();
						}
					}
					break;
					case IDC_REND_CAMERA_APERTURE:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						HWND hAperture = GetDlgItem(hWnd, IDC_REND_CAMERA_APERTURE);
						EMEditSpin * eAperture = GetEMEditSpinInstance(hAperture);
						float f = eAperture->floatValue;
						Camera * cam = getSelectedCamera();
						if (cam)
							cam->aperture = f;						
						HWND hDiaphragm = GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						EMPView * emDiaph = GetEMPViewInstance(hDiaphragm);
						
						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//cam->createApertureShapePreview(emDiaph->byteBuff);
						
						InvalidateRect(emDiaph->hwnd, NULL, false);
						HWND hApCopy = GetDlgItem(hCamera, IDC_REND_CAMERA_APERTURE_COPY);
						EMEditSpin* emApCopy = GetEMEditSpinInstance(hApCopy);
						emApCopy->setValuesToFloat(eAperture->floatValue, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 1, 0.1f);
						cam->glareOK = false;
					}
					break;
					case IDC_REND_CAMERA_SHUTTER:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						HWND hShutter = GetDlgItem(hWnd, IDC_REND_CAMERA_SHUTTER);
						EMEditSpin * eShutter = GetEMEditSpinInstance(hShutter);
						float f = eShutter->floatValue;
						Camera * cam = getSelectedCamera();
						if (cam)
							cam->shutter = f;
					}
					break;
					//case IDC_REND_CAMERA_AUTOEXP_ON:
					//{
					//	if (wmEvent != BN_CLICKED)
					//		break;
					//	HWND hAutoExpOn = GetDlgItem(hWnd, IDC_REND_CAMERA_AUTOEXP_ON);
					//	EMCheckBox * eAutoExpOn = GetEMCheckBoxInstance(hAutoExpOn);
					//	bool b = eAutoExpOn->selected;
					//	Camera * cam = getSelectedCamera();
					//	if (cam)
					//		cam->autoexposure = b;
					//}
					//break;
					//case IDC_REND_CAMERA_AUTOEXP_TYPE:
					//{
					//	if (wmEvent != CBN_SELCHANGE)
					//		break;
					//	HWND hAutoExpType = GetDlgItem(hWnd, IDC_REND_CAMERA_AUTOEXP_TYPE);
					//	EMComboBox * eAutoExpType = GetEMComboBoxInstance(hAutoExpType);
					//	int b = eAutoExpType->selected;
					//	Camera * cam = getSelectedCamera();
					//	if (cam)
					//		cam->autoexposureType = b;
					//}
					//break;
					//case IDC_REND_CAMERA_AUTOFOCUS:
					//{
					//	if (wmEvent != BN_CLICKED)
					//		break;
					//	HWND hAutoFocus = GetDlgItem(hWnd, IDC_REND_CAMERA_AUTOFOCUS);
					//	EMCheckBox * eAutoFocus = GetEMCheckBoxInstance(hAutoFocus);
					//	bool b = eAutoFocus->selected;
					//	Camera * cam = getSelectedCamera();
					//	if (cam)
					//		cam->autoFocus = b;
					//}
					//break;
					case IDC_REND_CAMERA_FOCUS_DIST:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						HWND hFocusDist = GetDlgItem(hWnd, IDC_REND_CAMERA_FOCUS_DIST);
						EMEditSpin * eFocusDist = GetEMEditSpinInstance(hFocusDist);
						float f = eFocusDist->floatValue;
						Camera * cam = getSelectedCamera();
						if (cam)
							cam->focusDist = f;
					}
					break;
					case IDC_REND_CAMERA_FOCAL_LENGTH:
					{
						if (wmEvent != WM_VSCROLL)
							break;
						HWND hFocal      = GetDlgItem(hWnd, IDC_REND_CAMERA_FOCAL_LENGTH);
						EMEditSpin * eFocal      = GetEMEditSpinInstance(hFocal);
						float f = eFocal->floatValue;
						setCameraFocalLength(f, false, true);
					}
					break;
					case IDC_REND_CAMERA_FOCAL_15:  setCameraFocalLength(15, true, true);		break;
					case IDC_REND_CAMERA_FOCAL_20:  setCameraFocalLength(20, true, true);		break;
					case IDC_REND_CAMERA_FOCAL_24:  setCameraFocalLength(24, true, true);		break;
					case IDC_REND_CAMERA_FOCAL_28:  setCameraFocalLength(28, true, true);		break;
					case IDC_REND_CAMERA_FOCAL_35:  setCameraFocalLength(35, true, true);		break;
					case IDC_REND_CAMERA_FOCAL_50:  setCameraFocalLength(50, true, true);		break;
					case IDC_REND_CAMERA_FOCAL_85:  setCameraFocalLength(85, true, true);		break;
					case IDC_REND_CAMERA_FOCAL_135: setCameraFocalLength(135, true, true);		break;
					case IDC_REND_CAMERA_PREVIEW_BUTTON:
					{
						if (wmEvent != BN_CLICKED)
							break;

						EMListView * emlv = GetEMListViewInstance(GetDlgItem(hWnd, IDC_REND_CAMERA_LISTVIEW));
						if (!emlv)
							break;
						int sel = emlv->selected;
						Raytracer * rtr = Raytracer::getInstance();

						Camera * cam = NULL;
						if (sel < 0   ||   sel >= rtr->curScenePtr->cameras.objCount)
							break;
						else
							cam = rtr->curScenePtr->cameras[ sel ];

						DialogBoxParam(hModule, MAKEINTRESOURCE(IDD_PLUGIN_CAMERA), hWnd, PluginCameraDlgProc,(LPARAM)(cam));

					}
					break;
					case IDC_REND_CAMERA_NUMBLADES:
					{
						HWND hBlNum			= GetDlgItem(hCamera, IDC_REND_CAMERA_NUMBLADES);
						EMEditSpin * emBlNum		= GetEMEditSpinInstance(hBlNum);
						HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->apBladesNum = emBlNum->intValue;
						
						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//cam->createApertureShapePreview(emDiaph->byteBuff);

						cam->glareOK = false;
						InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_ANGLEBLADES:
					{
						HWND hBlAngle		= GetDlgItem(hCamera, IDC_REND_CAMERA_ANGLEBLADES);
						EMEditSpin * emBlAngle		= GetEMEditSpinInstance(hBlAngle);
						HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->apBladesAngle = emBlAngle->floatValue;
						
						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//cam->createApertureShapePreview(emDiaph->byteBuff);

						cam->glareOK = false;
						InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_BLADERADIUS:
					{
						HWND hBlRadius		= GetDlgItem(hCamera, IDC_REND_CAMERA_BLADERADIUS);
						EMEditSpin * emBlRadius		= GetEMEditSpinInstance(hBlRadius);
						HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->apBladesRadius = emBlRadius->floatValue;

						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//cam->createApertureShapePreview(emDiaph->byteBuff);

						cam->glareOK = false;
						InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_BLADES_ROUND:
					{
						HWND hBlRound		= GetDlgItem(hCamera, IDC_REND_CAMERA_BLADES_ROUND);
						EMCheckBox * emBlRound		= GetEMCheckBoxInstance(hBlRound);
						HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->apBladesRound = emBlRound->selected;

						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//cam->createApertureShapePreview(emDiaph->byteBuff);

						cam->glareOK = false;
						InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_APERTURE_COPY:
					{
						HWND hApCopy = GetDlgItem(hCamera, IDC_REND_CAMERA_APERTURE_COPY);
						EMEditSpin* emApCopy = GetEMEditSpinInstance(hApCopy);
						HWND hAp = GetDlgItem(hCamera, IDC_REND_CAMERA_APERTURE);
						EMEditSpin* emAp = GetEMEditSpinInstance(hAp);
						HWND hDiaphragm = GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						EMPView * emDiaph = GetEMPViewInstance(hDiaphragm);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						cam->aperture = emApCopy->floatValue;
						emAp->setValuesToFloat(emApCopy->floatValue, NOX_CAM_APERTURE_MIN, NOX_CAM_APERTURE_MAX, 1, 0.1f);

						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//cam->createApertureShapePreview(emDiaph->byteBuff);

						cam->glareOK = false;
						InvalidateRect(emDiaph->hwnd, NULL, false	);
					}
					break;
					case IDC_REND_CAMERA_DOF_TEMP:
					{
						HWND hDof = GetDlgItem(hCamera, IDC_REND_CAMERA_DOF_TEMP);
						EMCheckBox * emc = GetEMCheckBoxInstance(hDof);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						if (!cam)
							break;
						if (!emc)
							break;
						cam->dofOnTemp = emc->selected;
					}
					break;
					case IDC_REND_CAMERA_BOKEH_RING_BALANCE:
					{
						HWND hBokehBalance = GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_RING_BALANCE);
						EMEditSpin * emBokehBalance = GetEMEditSpinInstance(hBokehBalance);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						int val = emBokehBalance->intValue;
						cam->diaphSampler.intBokehRingBalance = val;
						if (val>=0)
							cam->diaphSampler.bokehRingBalance = val+1.0f;
						else
							cam->diaphSampler.bokehRingBalance = 1.0f + val/21.0f;

						EMPView * emDiaph = GetEMPViewInstance(GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE));
						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						//EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						//cam->createApertureShapePreview(emDiaph->byteBuff);
						InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_BOKEH_RING_SIZE:
					{
						HWND hBokehRingSize = GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_RING_SIZE);
						EMEditSpin * emBokehRingSize = GetEMEditSpinInstance(hBokehRingSize);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						int val = emBokehRingSize->intValue;
						cam->diaphSampler.intBokehRingSize = val;
						cam->diaphSampler.bokehRingSize = val*0.01f;

						EMPView * emDiaph = GetEMPViewInstance(GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE));
						cam->diaphSampler.initializeDiaphragm(cam->apBladesNum, cam->aperture, cam->apBladesAngle, cam->apBladesRound, cam->apBladesRadius);
						cam->diaphSampler.createApertureShapePreview(emDiaph->byteBuff);

						//HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						//EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						//cam->createApertureShapePreview(emDiaph->byteBuff);
						InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_BOKEH_FLATTEN:
					{
						HWND hBokehFlatten = GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_FLATTEN);
						EMEditSpin * emBokehFlatten = GetEMEditSpinInstance(hBokehFlatten);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						int val = emBokehFlatten->intValue;
						cam->diaphSampler.intBokehFlatten = val;
						cam->diaphSampler.bokehFlatten = val*0.01f;
						//HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						//EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						//cam->createApertureShapePreview(emDiaph->byteBuff);
						//InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_BOKEH_VIGNETTE:
					{
						HWND hBokehVignette = GetDlgItem(hCamera, IDC_REND_CAMERA_BOKEH_VIGNETTE);
						EMEditSpin * emBokehVignette= GetEMEditSpinInstance(hBokehVignette);
						Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
						int val = emBokehVignette->intValue;
						cam->diaphSampler.intBokehVignette = val;
						cam->diaphSampler.bokehVignette = val*0.01f;
						//HWND hDiaphragm		= GetDlgItem(hCamera, IDC_REND_CAMERA_DIAPHRAGM_SHAPE);
						//EMPView    * emDiaph		= GetEMPViewInstance(hDiaphragm);
						//cam->createApertureShapePreview(emDiaph->byteBuff);
						//InvalidateRect(emDiaph->hwnd, NULL, false);
					}
					break;
					case IDC_REND_CAMERA_MB_ENABLED:
					{
						HWND hMB = GetDlgItem(hCamera, IDC_REND_CAMERA_MB_ENABLED);
						EMCheckBox * emc = GetEMCheckBoxInstance(hMB);
						Raytracer::getInstance()->curScenePtr->sscene.motion_blur_on = emc->selected;
					}
					break;
					case IDC_REND_CAMERA_MB_STILL:
					{
						HWND hMB = GetDlgItem(hCamera, IDC_REND_CAMERA_MB_STILL);
						EMCheckBox * emc = GetEMCheckBoxInstance(hMB);
						Raytracer::getInstance()->curScenePtr->sscene.motion_blur_still = emc->selected;
					}
					break;
					case IDC_REND_CAMERA_MB_TIME:
					{
						HWND hDuration = GetDlgItem(hCamera, IDC_REND_CAMERA_MB_TIME);
						EMEditSpin * emDuration = GetEMEditSpinInstance(hDuration);
						Raytracer::getInstance()->curScenePtr->sscene.motion_blur_time = emDuration->floatValue;
					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------------------------

void RendererMainWindow::changeCamera(Camera * cam)
{
	if (cam)
	{
		if (!cam->imgBuff   ||   !cam->imgBuff->imgBuf   ||   !cam->imgBuff->hitsBuf)
			cam->imgBuff = new ImageBuffer();
		if (!cam->imgBuff->imgBuf   ||   !cam->imgBuff->hitsBuf)
			cam->imgBuff->allocBuffer(cam->width*cam->aa, cam->height*cam->aa);
	}

	if (cam  &&   cam->imgBuff   &&   cam->imgBuff->imgBuf   &&   cam->imgBuff->hitsBuf)
	{
		EMPView * empv = GetEMPViewInstance(hImage);
		empv->otherSource = cam->imgBuff;
		empv->byteBuff->allocBuffer(cam->width, cam->height);
	}

	EMPView * empv = GetEMPViewInstance(hImage);
	if (cam  && empv)
		empv->rgnPtr = &(cam->staticRegion);

	fillCameraList();
	fillFinalSettings(cam);
	fillCameraData(cam);
	updateDimensionsFromActiveCamera();
	fillCameraPostData();
	HWND hList = GetDlgItem(hCamera, IDC_REND_CAMERA_LISTVIEW);
	InvalidateRect(hList, NULL, false);
	refreshRenderedImage();
}

//-----------------------------------------------------------------------------------------------------------------

