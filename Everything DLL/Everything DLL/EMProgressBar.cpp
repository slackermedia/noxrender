#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "EMControls.h"
#include "resource.h"

extern HMODULE hDllModule;

void InitEMProgressBar()
{
    WNDCLASSEX wc;
    
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EMProgressBar";
    wc.hInstance      = hDllModule;
	wc.lpfnWndProc    = EMProgressBarProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
	wc.cbWndExtra     = sizeof(EMProgressBar *);
    wc.hIconSm        = 0;

    RegisterClassEx(&wc);
}

EMProgressBar * GetEMProgressBarInstance(HWND hwnd)
{
	#ifdef _WIN64
		EMProgressBar * empb = (EMProgressBar *)GetWindowLongPtr(hwnd, 0);
	#else
		EMProgressBar * empb = (EMProgressBar *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return empb;
}

void SetEMProgressBarInstance(HWND hwnd, EMProgressBar *empb)
{
	#ifdef _WIN64
	    SetWindowLongPtr(hwnd, 0, (LONG_PTR)empb);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)empb);
	#endif
}

EMProgressBar::EMProgressBar(HWND hWnd)
{
	colBackGnd					= RGB(96,160,224);
	colBorderLight				= RGB(160,204,255);
	colBorderDark				= RGB(64,128,192);
	colPBackGnd					= RGB(128,192,240);
	colPBorderLight				= RGB(160,204,255);
	colPBorderDark				= RGB(64,128,192);
	colDisabledBackground		= RGB(192,192,192);
	colDisabledBorderLight		= RGB(224,224,224);
	colDisabledBorderDark		= RGB(160,160,160);
	colDisabledPBackground		= RGB(192,192,192);
	colDisabledPBorderLight		= RGB(224,224,224);
	colDisabledPBorderDark		= RGB(160,160,160);
	hwnd = hWnd;
	posMin = 0;
	posMax = 100;
	posCur = 0;
	posMain = 0.0f;

	useNewStyle = false;
	hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	colNBgUp = RGB(37,37,37);
	colNBgDown = RGB(47,47,47);
	colNTrackUp = RGB(30,45,74);
	colNTrackDown = RGB(3,18,47);
	colNTrackBorder = RGB(64,64,64);
	colNText = RGB(207, 207, 207);

}

EMProgressBar::~EMProgressBar()
{
}


LRESULT CALLBACK EMProgressBarProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EMProgressBar * empb;
	empb = GetEMProgressBarInstance(hwnd);

	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			empb = new EMProgressBar(hwnd);
			SetEMProgressBarInstance(hwnd, empb);
			empb->refreshSize();
			break;
		case WM_DESTROY:
			{
				delete empb;
				SetEMProgressBarInstance(hwnd, 0);
			}
			break;
		case WM_SIZE:
			{
				empb->refreshSize();
			}
			break;
		case WM_PAINT:
		{
			HDC ohdc = BeginPaint(hwnd, &ps);
			HPEN hOldPen;
			GetClientRect(hwnd, &rect);

			DWORD style;
			style = GetWindowLong(hwnd, GWL_STYLE);
			bool disabled = ((style & WS_DISABLED) > 0);

			// create back buffer
			hdc = CreateCompatibleDC(ohdc);
			HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right, rect.bottom);
			SelectObject(hdc, backBMP);


			if (empb->useNewStyle)
			{
				int ppos = empb->posCur;
				ppos = min(ppos,rect.right);
				drawGradientVertical(hdc, empb->colNBgUp, empb->colNBgDown, 0,0,rect.right,rect.bottom);
				drawGradientVertical(hdc, empb->colNTrackUp, empb->colNTrackDown, 0,0,ppos,rect.bottom);
				COLORREF cc = empb->colNTrackBorder;
				drawBevel(hdc, cc,cc,cc,cc, 0, 0, ppos-1, rect.bottom-1);

				char caption[2048];
				GetWindowText(hwnd, caption, 2048);
				int l = (int)strlen(caption);
				if (l>0)
				{
					HFONT hOldFont = (HFONT)SelectObject(hdc, empb->hFont);
					SIZE sz;
					GetTextExtentPoint32(hdc, caption, l, &sz);
					SetBkMode(hdc, TRANSPARENT);
					SetTextColor(hdc, empb->colNText);

					int posx = 2;
					int posy = (rect.bottom-sz.cy)/2;
					switch (empb->align)
					{
						case EMProgressBar::ALIGN_CENTER_CONTROL:
							posx = (rect.right-sz.cx)/2;
							break;
						case EMProgressBar::ALIGN_RIGHT_CONTROL:
							posx = (rect.right-sz.cx)-3;
							break;
						case EMProgressBar::ALIGN_CENTER_TRACK:
							posx = (ppos-sz.cx)/2;
							break;
						case EMProgressBar::ALIGN_RIGHT_TRACK:
							posx = (ppos-sz.cx)-3;
							break;
					}
					posy = max(0, posy);
					posx = max(2, posx);

					TextOut(hdc, posx, posy, caption, l);
					SelectObject(hdc, hOldFont);
				}
			}
			else
			{
				int ppos = empb->posCur;
				ppos = min(ppos,rect.right-1);
				if (disabled)
				{
					POINT b1[] = { {0, rect.bottom-1}, {0,0}, {rect.right, 0} };
					POINT b2[] = { {rect.right-1, 1}, {rect.right-1, rect.bottom-1}, {0, rect.bottom-1} };
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colDisabledBorderDark));
					Polyline(hdc, b1, 3);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colDisabledBorderLight)));
					Polyline(hdc, b2, 3);
					DeleteObject(SelectObject(hdc, hOldPen));
					// draw background
					POINT inside1[] = {	{1, 1}, {rect.right-1,1}, {rect.right-1,rect.bottom-1}, {1,rect.bottom-1} };	// 4
					HRGN cfhrg = CreatePolygonRgn(inside1, 4, WINDING);
					HBRUSH cfbr = CreateSolidBrush(empb->colDisabledBackground);
					FillRgn(hdc, cfhrg, cfbr);
					DeleteObject(cfbr);
					DeleteObject(cfhrg);
					if (ppos > 1)
					{
						//draw progress boundary
						POINT bp1[] = { {1, rect.bottom-2}, {1,1}, {ppos, 1} };
						POINT bp2[] = { {ppos-1, 2}, {ppos-1, rect.bottom-2}, {1, rect.bottom-2} };
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colDisabledPBorderLight));
						Polyline(hdc, bp1, 3);
						DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colDisabledPBorderDark)));
						Polyline(hdc, bp2, 3);
						DeleteObject(SelectObject(hdc, hOldPen));
						// draw progress background
						POINT inside2[] = {	{2, 2}, {ppos-1,2}, {ppos-1,rect.bottom-2}, {2,rect.bottom-2} };	// 4
						HRGN cphrg = CreatePolygonRgn(inside2, 4, WINDING);
						HBRUSH cpbr = CreateSolidBrush(empb->colDisabledPBackground);
						FillRgn(hdc, cphrg, cpbr);
						DeleteObject(cpbr);
						DeleteObject(cphrg);
					}
				}
				else
				{
					// draw boundary and background
					POINT b1[] = { {0, rect.bottom-1}, {0,0}, {rect.right, 0} };
					POINT b2[] = { {rect.right-1, 1}, {rect.right-1, rect.bottom-1}, {0, rect.bottom-1} };
					hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colBorderDark));
					Polyline(hdc, b1, 3);
					DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colBorderLight)));
					Polyline(hdc, b2, 3);
					DeleteObject(SelectObject(hdc, hOldPen));
					// draw background
					POINT inside1[] = {	{1, 1}, {rect.right-1,1}, {rect.right-1,rect.bottom-1}, {1,rect.bottom-1} };	// 4
					HRGN cfhrg = CreatePolygonRgn(inside1, 4, WINDING);
					HBRUSH cfbr = CreateSolidBrush(empb->colBackGnd);
					FillRgn(hdc, cfhrg, cfbr);
					DeleteObject(cfbr);
					DeleteObject(cfhrg);
					if (ppos > 1)
					{
						//draw progress boundary
						POINT bp1[] = { {1, rect.bottom-2}, {1,1}, {ppos, 1} };
						POINT bp2[] = { {ppos-1, 2}, {ppos-1, rect.bottom-2}, {1, rect.bottom-2} };
						hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colPBorderLight));
						Polyline(hdc, bp1, 3);
						DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, empb->colPBorderDark)));
						Polyline(hdc, bp2, 3);
						DeleteObject(SelectObject(hdc, hOldPen));
						// draw progress background
						POINT inside2[] = {	{2, 2}, {ppos-1,2}, {ppos-1,rect.bottom-2}, {2,rect.bottom-2} };	// 4
						HRGN cphrg = CreatePolygonRgn(inside2, 4, WINDING);
						HBRUSH cpbr = CreateSolidBrush(empb->colPBackGnd);
						FillRgn(hdc, cphrg, cpbr);
						DeleteObject(cpbr);
						DeleteObject(cphrg);
					}
				}
			}

			// copy from back buffer
			GetClientRect(hwnd, &rect);
			BitBlt(ohdc, 0, 0, rect.right, rect.bottom, hdc, 0, 0, SRCCOPY );
			DeleteDC(hdc); 
			DeleteObject(backBMP);

			EndPaint(hwnd, &ps);
		}
		break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EMProgressBar::refreshSize()
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	posMax = rect.right;
	posCur = (int)(posMain*posMax/100.0f);
	InvalidateRect(hwnd, NULL, false);
}

void EMProgressBar::setPos(float pos)
{
	posMain = pos;
	posCur = (int)(pos*posMax/100.0f);
	InvalidateRect(hwnd, NULL, false);
}

bool EMProgressBar::changeCaption(char * newtext)
{
	return true;
}


