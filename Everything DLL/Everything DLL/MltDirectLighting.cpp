#define _CRT_RAND_S
#include "Metropolis.h"
#include <math.h>
#include "log.h"
#include <float.h>

clock_t cfindinters = 0;
clock_t cbrdfsample = 0;
clock_t clightsample = 0;
clock_t csunsample = 0;
clock_t cskysample = 0;
clock_t cupdateimage = 0;

//#define SAMPLE_ALL_LAYERS

Color4 Metropolis::evalLocalIllumStandard(Point3d orig, Vector3d dir, float dx, float dy)
{
	bool sunOn = ss->sunsky->sunOn;
	bool skyOn = ss->useSunSky;
	int nn = ss->sunsky->sunOtherBlendLayer ? 1 : 0;

	int dbx = max(0, min((int)(dx), cam->width*cam->aa-1));
	int dby = max(0, min((int)(dy), cam->height*cam->aa-1));
	float zDistance = 0.0f;
	bool noRefl = true;
	float dMod = cam->evalDistMult(dx, dy);

	Color4 blends[16];

	Color4 res = Color4(0,0,0);
	float gpdf = 1.0f;
	Color4 gbrdf = Color4(1,1,1);
	Raytracer * rtr = Raytracer::getInstance();
	Scene * scene = rtr->curScenePtr;

	Point3d ipoint = orig;
	Vector3d dirOut = dir;
	Point3d intersection;

	MaterialNox * mat = NULL;
	MatLayer * mlay = NULL;
	HitData hd;
	float slpdf = 1.0f;

	Color4 results[16];
	int addBits = 0;

	for (int i=0; i<16; i++)
		results[i] = Color4(0,0,0);

	clock_t cstartfindinters = 0;
	clock_t cstartbrdfsample = 0;
	clock_t cstartlightsample = 0;
	clock_t cstartsunsample = 0;
	clock_t cstartskysample = 0;
	clock_t cstartupdateimage = 0;
	clock_t cstopfindinters = 0;
	clock_t cstopbrdfsample = 0;
	clock_t cstoplightsample = 0;
	clock_t cstopsunsample = 0;
	clock_t cstopskysample = 0;
	clock_t cstopupdateimage = 0;


	cstartfindinters = clock();

	// -------------------------------------------------------------------------------------- LOOP - go through until nonspecular
	bool itIsSpecular = true;
	int watchdog=50;
	while (itIsSpecular)
	{
		if (watchdog-- < 0)
			return Color4(0,0,0);
		float u,v;
		int chosen = -1, instID=-1;
		float bigf = BIGFLOAT;
		Matrix4d tmatr;

		// find intersection
		float d = ss->bvh.intersectForTrisNonSSE(sample_time, ipoint, dirOut, tmatr, bigf, instID, chosen, u,v);
		if (d < 0.0f)
		{	// ------------------------------------------------------------------------------  nothing intersected

			// check sun
			if (skyOn && sunOn)
			{
				Vector3d sdir;
				sdir = ss->sunsky->getSunDirection();
				sdir.normalize();
				if (dirOut * sdir >= ss->sunsky->maxCosSunSize)
				{
					if (cam->blendBits & (1<<nn))
					{
						Color4 c = ss->sunsky->getSunColor();
						results[nn] += c * gbrdf*(1.0f/gpdf);
						addBits |= (1<<nn);
					}
					res += ss->sunsky->getSunColor();	// temporary keep it
				}
			}

			// check sky
			if (skyOn)
			{
				Color4 skycolor = ss->sunsky->getColor(dirOut);
				results[0] += skycolor * gbrdf*(1.0f/gpdf);
				addBits |= (1<<0);
				res += skycolor;	// temporary keep it
			}

			for (int i=0; i<16; i++)
				if ( (cam->blendBits & (1<<i)) )
				{
					cam->blendBuffDirect[i].addPixelColor((int)dx, (int)dy, results[i], true);
				}

			return res;
			return Color4(0,0,0);
		}

		// ------------------------------------------------------------------------------  something intersected - check for specular and if so, reflect
		// get intersected triangle
		Triangle tri = (scene->triangles[chosen]);

		intersection = tmatr * tri.getPointFromUV(u,v);
		Vector3d normal = (tmatr.getMatrixForNormals() * tri.evalNormal(u,v)).getNormalized();
		tri.evalTexUV(u, v, hd.tU, hd.tV);

		// fill hitdata
		hd.lastDist = d;
		hd.in = dirOut;
		hd.in.invert();
		hd.clearFlagsAll();
		hd.normal_shade = normal;
		hd.normal_geom = (tmatr.getMatrixForNormals() * tri.normal_geom).getNormalized();
		hd.adjustNormal();	// done
		tri.getTangentSpaceSmooth(tmatr, u,v, hd.dir_U, hd.dir_V);

		mat = scene->mats[scene->instances[instID].materials[tri.matInInst]];
		if (!mat)
		{
			Logger::add("no mat???");
			return Color4(0,0,0);
		}

		if (mat->hasEmitters)		// ------------------------------------- emission - finish here
		{
			float llpdf;
			mlay = mat->getRandomEmissionLayer(u,v, llpdf);
			if (mlay)
			{
				res = mlay->getLightColor(hd.tU, hd.tV) * mlay->getIlluminance(ss, instID, chosen);						///  <--------------------------- HERE
				res *= gbrdf*(1.0f/gpdf);
				if (normal * dirOut > 0)
					res = Color4(0,0,0);

				int lb = mat->blendIndex;
				if (cam->blendBits & (1<<lb))
				{
					results[lb] += res;
					addBits |= (1<<lb);
				}

				for (int i=0; i<16; i++)
				{
					if ( (cam->blendBits & (1<<i)) )
						cam->blendBuffDirect[i].addPixelColor((int)dx, (int)dy, results[i], true);
				}

				return res;	
			}
			else
			{
				Logger::add("error while choosing random emission layer (direct lighting)");
				return Color4(0,0,0);
			}
		}

		mlay = mat->getRandomShadeLayer(hd.tU, hd.tV, slpdf);
		if (!mlay)
			return Color4(0,0,0);
		float rough = mlay->getRough(hd.tU, hd.tV);

		float opacity = mat->getOpacity(hd.tU, hd.tV);
		float rand_opacity = 0;
		bool go_opacity = false;
		if (opacity < 1.0f)
		{
			rand_opacity = rtr->getRandomFloatThread();
			go_opacity = (rand_opacity >= opacity);
		}
		
		if (rough > 0.0f)
		{
			if (go_opacity)
			{
				Point3d npPos;
				if (dirOut * normal > 0)
					ipoint = intersection + normal * 0.0001f;
				else
					ipoint = intersection + normal * -0.0001f;
			}
			else
				itIsSpecular = false;
		}
		else
		{	// still specular or high spec-gloss
			float nppdf = 1.0f;
			float p1;
			if (go_opacity)
			{
				hd.out = dirOut;
			}
			else
			{
				hd.clearFlagInvalidRandom();
				hd.out = mlay->randomNewDirection(hd, nppdf);
				p1 = mlay->getProbability(hd);	
			}

			Point3d npPos;
			if (hd.out * normal > 0)
				npPos = intersection + normal * 0.0001f;
			else
				npPos = intersection + normal * -0.0001f;

			if ((hd.out*normal) * (hd.in*normal) > 0.0f)
				noRefl = false;

			Color4 tBrdf = mlay->getBRDF(hd);
			float nCos = normal * hd.out;

			if (go_opacity)
				tBrdf = tBrdf * opacity + Color4(1,1,1)*(1-opacity);

			if (tBrdf.isBlack()  ||  tBrdf.isInf()  ||  tBrdf.isNaN()  ||  nppdf<0.0001f)
				return Color4(0,0,0);

			gbrdf *= tBrdf;
			if (hd.isFlagInvalidRandom())
				return Color4(0,0,0);

			gbrdf *= 1.0f/slpdf;
			gpdf *= nppdf;

			ipoint = npPos;
			dirOut = hd.out;
		}
	}

	if (hd.spectr_pos>0)
		gbrdf *= 4.0f;


	cstartbrdfsample = cstopfindinters = clock();

	// check brdf and pdf
	if (gbrdf.isBlack() || gbrdf.isInf() || gbrdf.isNaN() || gpdf==0)
		return Color4(0,0,0);

	// ==============================================================================================================================================================
	// -------------------------------------------------------------------------------------------- so we have intersected nonspecular, it begins here ;)


	//------------------------------------------------------------------------------------------
	// BRDF sampling first - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	Vector3d rdir;
	float rpdf;
	Color4 rbrdf;
	Point3d brdf_intersection = intersection;

	hd.clearFlagInvalidRandom();
	rdir = mlay->randomNewDirection(hd, rpdf);
	Point3d rStPoint;
	if (hd.isFlagInvalidRandom())
		return Color4(0,0,0);

	if (rpdf<0.001f)
		return Color4(0,0,0);

	float bigf = BIGFLOAT;
	int iltri = -1, inst_id_l = -1;
	Triangle ltri;
	Matrix4d lmatr;
	float lu, lv;
	float rcos = hd.normal_shade * rdir;
	float absRcos = fabs(rcos);
	if (rcos > 0.0)
		rStPoint = intersection + hd.normal_shade * 0.0001f;
	else
		rStPoint = intersection + hd.normal_shade *-0.0001f;
	
	float rd = ss->bvh.intersectForTrisNonSSE(sample_time, rStPoint, rdir, lmatr, bigf, inst_id_l, iltri, lu,lv);

	Color4 brdfSamplingFakeGlassColor = Color4(1,1,1);

	if (rd > 0)	// check for fake glass first
	{
		Triangle ftri = (scene->triangles[iltri]);
		MaterialNox * fmat = scene->mats[scene->instances[inst_id_l].materials[ftri.matInInst]];
		float opacity = fmat->getOpacity(hd.tU, hd.tV);
		if (fmat->hasFakeGlass   ||   opacity<1.0f)
		{
			for (int i=0; i<20; i++)
			{
				HitData fhd;
				fhd.in = rdir * -1;
				fhd.out = rdir;
				fhd.normal_shade = (lmatr.getMatrixForNormals() * ftri.evalNormal(lu, lv)).getNormalized();
				ftri.evalTexUV(lu, lv, fhd.tU, fhd.tV);
				fhd.normal_geom = (lmatr.getMatrixForNormals() * ftri.normal_geom).getNormalized();
				fhd.adjustNormal();
				ftri.getTangentSpaceSmooth(lmatr, lu, lv, fhd.dir_U, fhd.dir_V);
				float fpdf;
				MatLayer * flay = fmat->getRandomFakeGlassLayer(fhd.tU, fhd.tV, fpdf);

				float fdirpdf = 1.0f;
				Color4 fbrdf = Color4(0,0,0);
				if (flay)
				{
					fhd.clearFlagInvalidRandom();
					fhd.out = flay->randomNewDirection(fhd, fdirpdf);
					fbrdf = flay->getBRDF(fhd);
				}

				if (fdirpdf < 0.001f)
					break;

				if (fhd.isFlagInvalidRandom())
				{
					brdfSamplingFakeGlassColor = Color4(0,0,0);
					break;
				}

				fbrdf *= (1.0f/fpdf);

				if (fbrdf.isInf()   ||   fbrdf.isNaN())
					break;
	
				if (opacity < 1.0f)
				{
					if (fhd.out * rdir > 0.999f)
						fbrdf = fbrdf * opacity + Color4(1,1,1)*(1.0f-opacity);
					else
						fbrdf = fbrdf * opacity;
				}

				brdfSamplingFakeGlassColor *= fbrdf;

				if (fbrdf.isBlack())
					break;


				rdir = fhd.out;
				bigf = BIGFLOAT;

				brdf_intersection = lmatr * ftri.getPointFromUV(lu,lv);
				rStPoint = brdf_intersection + fhd.normal_shade * ((fhd.out*fhd.normal_shade>0) ? 0.0001f : -0.0001f); 
				rd = ss->bvh.intersectForTrisNonSSE(sample_time, rStPoint, rdir, lmatr, bigf, inst_id_l, iltri, lu,lv);
				if (rd <= 0)
					break;

				ftri = (scene->triangles[iltri]);
				fmat = scene->mats[scene->instances[inst_id_l].materials[ftri.matInInst]];

				ftri.evalTexUV(lu, lv, fhd.tU, fhd.tV);
				float opacity = fmat->getOpacity(fhd.tU, fhd.tV);
				if (!fmat->hasFakeGlass   &&   opacity>=1.0f)
					break;
			}
		}
	}

	if (rd >= 0.0f)
	{
		ltri = (scene->triangles[iltri]);
		MaterialNox * lmat = scene->mats[scene->instances[inst_id_l].materials[ltri.matInInst]];
		if (lmat->hasEmitters)
		{
			//// find lightmesh
			float rlayerpdf;
			MatLayer * llay = lmat->getRandomEmissionLayer(lu, lv, rlayerpdf);
			if (!llay)
			{
				Logger::add("null emitter layer - this should not happen");
				return Color4(0,0,0);
			}

			MeshInstance * mInst = &(scene->instances[inst_id_l]);
			Matrix4d lMatr = mInst->matrix_transform;
			Vector3d lnormal = (lMatr.getMatrixForNormals() * ltri.evalNormal(lu,lv)).getNormalized();
			float tlu, tlv;
			ltri.evalTexUV(lu, lv, tlu, tlv);
			Color4 emc = llay->getLightColor(tlu, tlv) * llay->getIlluminance(ss, inst_id_l, iltri);					// <--------------------------------------- HERE
			Vector3d dirFromLight = rdir;
			dirFromLight.invert();
			float lcos = max(0, dirFromLight * lnormal);
			float lightPdf = ss->evalPDF(inst_id_l, iltri);	
			
			hd.out = rdir;
			#ifdef SAMPLE_ALL_LAYERS
				Color4 rbrdf = mat->getAllLayersBRDF(hd);
			#else
				Color4 rbrdf = mlay->getBRDF(hd);
				rbrdf *= 1.0f/slpdf;
			#endif
			float brdfPdf = mlay->getProbability(hd);
			float dist = (intersection - (lMatr*ltri.getPointFromUV(lu, lv)) ).length();
			float brdfPdfToPointPdf = brdfPdf * fabs(lcos)/(dist*dist);
			float rough = mlay->getRough(hd.tU, hd.tV);
			if (rough==0)
				lightPdf = 0;

			float weight = brdfPdfToPointPdf*brdfPdfToPointPdf / (brdfPdfToPointPdf*brdfPdfToPointPdf + lightPdf*lightPdf);

			Color4 toAdd = rbrdf * brdfSamplingFakeGlassColor * emc * gbrdf * absRcos * weight;
			float p = brdfPdf * gpdf;
			if (p > 0.0000001f   &&   lcos > 0.0001)								// RANDOM BRDF
			{
				res += toAdd * (1 / p);
				int lb = lmat->blendIndex;
				if (cam->blendBits & (1<<lb))
				{
					results[lb] += toAdd * (1/p);
					addBits |= (1<<lb);
				}
			}
		}	// end lmat->hasEmitters
	}		// end rd > 0.001f
	else
	{	// nothing intersected on brdf sampling, try a sky
		if (ss->useSunSky)
		{
			hd.out = rdir;
			#ifdef SAMPLE_ALL_LAYERS
				Color4 skybrdf = mat->getAllLayersBRDF(hd);
			#else
				Color4 skybrdf = mlay->getBRDF(hd);		
			#endif
			Color4 emc = ss->sunsky->getColor(rdir);
			float brdfpdf = mlay->getProbability(hd);
			if (brdfpdf > 0.001f)
			{
				float weight = 1;
				if (!emc.isBlack()  &&  !emc.isInf()  &&  !emc.isNaN())
				{
					Color4 toAdd = emc * skybrdf * fabs(rdir*hd.normal_shade) * brdfSamplingFakeGlassColor * (1.0f/brdfpdf) * weight *gbrdf*(1.0f/gpdf);
					res += toAdd;
					if (cam->blendBits & (1<<0))
					{
						results[0] += toAdd;
						addBits |= (1<<0);
					}
				}
			}
		}
	}

	cstopbrdfsample = cstartlightsample = clock();

	//--------------------------------------------------------------------------------------------------------------
	// now random light sampling - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	float lpdf1, lpdf2, llayerpdf;
	int ilm, ilt;

	// random triangle with emission material
	ilm = ss->randomLightMesh(lpdf1);
	LightMesh *lm = (*ss->lightMeshes)[ilm];
	if (ilm >= 0 && lm)
		ilt = lm->randomTriangle(lpdf2);
	else
		ilt = -1;

	if (ilt >= 0)
	{	// found - get it and take emissive layer
		int temp;
		float lu, lv;
		Point3d lPoint;

		Triangle ltri = (scene->triangles[ilt]);
		MatLayer * lmlay = NULL;
		MaterialNox * lmat  = scene->mats[scene->instances[lm->instNum].materials[ltri.matInInst]];
		ltri.randomPoint(lu, lv, lPoint);
		Matrix4d lmatr = scene->instances[lm->instNum].matrix_transform;
		lPoint = lmatr * lPoint;
		lmlay = lmat->getRandomEmissionLayer(lu, lv, llayerpdf);

		if (lmlay)
		{
			float ttu, ttv, mdist, d1, ldist;
			Point3d pIntLSide;
			Vector3d lnormal = (lmatr.getMatrixForNormals() * ltri.evalNormal(lu, lv)).getNormalized();

			// eval dir to that point
			Vector3d dirToLight = lPoint - intersection;
			ldist = dirToLight.normalize_return_old_length();

			// shift intersection point a little
			if (dirToLight * hd.normal_shade > 0)
				pIntLSide = intersection + hd.normal_shade * 0.0001f;
			else
				pIntLSide = intersection + hd.normal_shade * -0.0001f;
			dirToLight = lPoint - pIntLSide;
			ldist = dirToLight.normalize_return_old_length();

			mdist = ldist - 0.0002f;

			bool gotShadow = true;
			int shadowWatchDog = 50;
			Color4 randomLightFakeGlassAttenuation = Color4(1,1,1);

			// search for obstacle
			Matrix4d tmatr2;
			temp = -1;
			int tInstID = -1;
			d1 = rtr->curScenePtr->intersectBVHforNonOpacFakeGlass(sample_time, pIntLSide, dirToLight, mdist, tmatr2, tInstID, temp, ttu, ttv, randomLightFakeGlassAttenuation, ss, false);
			if (d1>0  ||  randomLightFakeGlassAttenuation.isBlack())
				gotShadow = true;
			else
				gotShadow = false;
			if (!gotShadow)
			{	// not in shadow
				float tlu, tlv;
				ltri.evalTexUV(lu, lv, tlu, tlv);
				Vector3d dirFromLight = dirToLight;
				dirFromLight.invert();
				float lcos = max(0, dirFromLight * lnormal);
				float rcos = fabs(dirToLight * hd.normal_shade);
				float llpdf = ss->lightMeshPowers[ilm]/ss->allLightsPower * 1/(*ss->lightMeshes)[ilm]->totalArea;
				hd.out = dirToLight;
				hd.in = dir * -1;
				#ifdef SAMPLE_ALL_LAYERS
					Color4 rbrdf = mat->getAllLayersBRDF(hd);
				#else
					Color4 rbrdf = mlay->getBRDF(hd);
					rbrdf *= 1.0f/slpdf;
				#endif

				float brdfPdf = mlay->getProbability(hd);
				float brdfPdfToPointPdf = brdfPdf * fabs(lcos)/(ldist*ldist);
				// eval weight - power heuristic 
				float weight = llpdf*llpdf / (brdfPdfToPointPdf*brdfPdfToPointPdf + llpdf*llpdf);
				if (gpdf > 0.00001f   &&   lcos > 0.001f   &&   rcos > 0.001f)				// RANDOM LIGHT
				{	
					Color4 toAdd = rbrdf * randomLightFakeGlassAttenuation * lmlay->getLightColor(tlu, tlv) * (ss->allLightsPower * rcos * lcos / ldist/ldist) *weight ;
					toAdd *= gbrdf*(1/gpdf);
					res += toAdd;
					int lb = lmat->blendIndex;
					if (cam->blendBits & (1<<lb))
					{
						results[lb] += toAdd;
						addBits |= (1<<lb);
					}
				}
			}
		}
	}

	cstoplightsample = cstartsunsample = clock();

	// random dir to sun
	if (ss->useSunSky   &&   ss->sunsky->sunOn)
	{
		Vector3d toSun = ss->sunsky->getRandomPerturbedSunDirection();
		hd.out = toSun;
		#ifdef SAMPLE_ALL_LAYERS
			Color4 sunbrdf = mat->getAllLayersBRDF(hd);
		#else
			Color4 sunbrdf = mlay->getBRDF(hd);
			sunbrdf *= slpdf;
		#endif
		Color4 emc = ss->sunsky->getSunColor();

		float weight = 1.0f;
		float p = 1.0f * gpdf;
		Color4 toAdd = sunbrdf * emc * gbrdf * fabs(toSun*hd.normal_shade/p) * weight * ss->sunsky->getSunSizeSteradians();

		if (!toAdd.isBlack()  &&  !toAdd.isInf()  &&  !toAdd.isNaN()  &&  p > 0.001f)
		{
			Point3d pp;
			if (toSun * hd.normal_shade > 0)
				pp = intersection + hd.normal_shade *  0.0001f;
			else
				pp = intersection + hd.normal_shade * -0.0001f;
			int temp = -2;
			float ttu, ttv;
			float mdist = BIGFLOAT;
			float d1 = -1;

			bool gotShadow = true;
			int shadowWatchDog = 50;
			Color4 randomSunFakeGlassAttenuation = Color4(1,1,1);
			while (gotShadow)
			{
				if (shadowWatchDog-- < 0)
					break;
				temp = -1;
				int t_inst = -1;
				Matrix4d m_inst;
				d1 = ss->bvh.intersectForTrisNonSSE(sample_time, pp, toSun, m_inst, bigf, t_inst, temp, ttu,ttv);
				if (d1 < 0)
				{
					gotShadow = false;
					break;
				}

				Triangle ftri = (scene->triangles[temp]);
				MaterialNox * fmat = scene->mats[scene->instances[t_inst].materials[ftri.matInInst]];
				HitData fhd;
				ftri.evalTexUV(ttu, ttv, fhd.tU, fhd.tV);

				float opacity = fmat->getOpacity(fhd.tU, fhd.tV);
				if (!fmat->hasFakeGlass   &&   opacity>=1.0f)
					break;

				fhd.clearFlagsAll();
				fhd.in = toSun * -1;
				fhd.out = toSun;
				fhd.normal_shade = m_inst.getMatrixForNormals() * ftri.evalNormal(ttu, ttv);
				fhd.normal_geom	 = m_inst.getMatrixForNormals() * ftri.normal_geom;
				fhd.normal_shade.normalize();
				fhd.normal_geom.normalize();
				fhd.adjustNormal();
				ftri.getTangentSpaceSmooth(m_inst, ttu,ttv, fhd.dir_U, fhd.dir_V);

				float fpdf = 1;
				MatLayer * flay = fmat->getRandomFakeGlassLayer(fhd.tU, fhd.tV, fpdf);
				if (fpdf == 0.0f)
					fpdf = 1.0f;

				Color4 fbrdf = Color4(0,0,0);
				if (flay)
					fbrdf = flay->getBRDF(fhd);
				fbrdf = fbrdf * opacity + Color4(1,1,1) * (1.0f-opacity);
				if (fbrdf.isBlack()  ||   fbrdf.isInf()   ||   fbrdf.isNaN())
					break;

				randomSunFakeGlassAttenuation *= fbrdf * (1.0f/fpdf);

				pp = m_inst * ftri.getPointFromUV(ttu, ttv);
				pp = pp + fhd.normal_shade * ((toSun*fhd.normal_shade>0) ? 0.0001f : -0.0001f);
			}

			if (!gotShadow)
			{
				toAdd *= randomSunFakeGlassAttenuation;
				res += toAdd;
				if (cam->blendBits & (1<<nn))
				{
					results[nn] += toAdd;
					addBits |= (1<<nn);
				}
			}
		}
	}

	cstopsunsample = cstartskysample = clock();
	cstopskysample = cstartupdateimage = clock();

	for (int i=0; i<16; i++)
	{
		if ( (cam->blendBits & (1<<i)) )
			cam->blendBuffDirect[i].addPixelColor((int)dx, (int)dy, results[i], true);
	}

	cstopupdateimage = clock();

	cfindinters  += cstopfindinters  - cstartfindinters;
	cbrdfsample  += cstopbrdfsample  - cstartbrdfsample;
	clightsample += cstoplightsample - cstartlightsample;
	csunsample   += cstopsunsample   - cstartsunsample;
	cskysample   += cstopskysample   - cstartskysample;
	cupdateimage += cstopupdateimage - cstartupdateimage;

	return res;
}


