#include "resource.h"
#include "RendererMainWindow.h"
#include "EMControls.h"
#include "ColorSchemes.h"
#include <windows.h>
#include "raytracer.h"
#include "Dialogs.h"
#include <stdio.h>
#include <time.h>
#include "log.h"
#include "valuesMinMax.h"
#include <math.h>
#include <shlwapi.h>
#include "ThreadJobWindow.h"

extern HMODULE hDllModule;
extern ThemeManager gTheme;
extern HBITMAP gIcons;


INT_PTR CALLBACK RendererMainWindow::MainBarWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			{
				HWND hStart      = GetDlgItem(hWnd, IDC_REND_MAINBAR_START);
				HWND hResume     = GetDlgItem(hWnd, IDC_REND_MAINBAR_RESUME);
				HWND hStop       = GetDlgItem(hWnd, IDC_REND_MAINBAR_STOP);
				HWND hLoadScene  = GetDlgItem(hWnd, IDC_REND_MAINBAR_LOADSCENE);
				HWND hSaveScene  = GetDlgItem(hWnd, IDC_REND_MAINBAR_SAVESCENE);
				HWND hSaveScTex  = GetDlgItem(hWnd, IDC_REND_MAINBAR_SAVESCENE_WITH_TEX);
				HWND hMergeScene = GetDlgItem(hWnd, IDC_REND_MAINBAR_MERGE);
				HWND hSaveImage  = GetDlgItem(hWnd, IDC_REND_MAINBAR_SAVEIMAGE);
				HWND hRefresh    = GetDlgItem(hWnd, IDC_REND_MAINBAR_REFRESH);
				HWND hZoomIn     = GetDlgItem(hWnd, IDC_REND_MAINBAR_ZOOMIN);
				HWND hZoomOut    = GetDlgItem(hWnd, IDC_REND_MAINBAR_ZOOMOUT);
				HWND hZoom100    = GetDlgItem(hWnd, IDC_REND_MAINBAR_ZOOM100);
				HWND hZoomFit    = GetDlgItem(hWnd, IDC_REND_MAINBAR_ZOOMFIT);
				HWND hDrawNice   = GetDlgItem(hWnd, IDC_REND_MAINBAR_DRAW_NICE);
				HWND hSceneInfo  = GetDlgItem(hWnd, IDC_REND_MAINBAR_SCENE_INFO);
				HWND hAbout	     = GetDlgItem(hWnd, IDC_REND_MAINBAR_ABOUT);
				EMImgButton * emstart      = GetEMImgButtonInstance(hStart);
				EMImgButton * emresume     = GetEMImgButtonInstance(hResume);
				EMImgButton * emstop       = GetEMImgButtonInstance(hStop);
				EMImgButton * emloadscene  = GetEMImgButtonInstance(hLoadScene);
				EMImgButton * emsavescene  = GetEMImgButtonInstance(hSaveScene);
				EMImgButton * emsavesctex  = GetEMImgButtonInstance(hSaveScTex);
				EMImgButton * emmergesc    = GetEMImgButtonInstance(hMergeScene);
				EMImgButton * emsaveimage  = GetEMImgButtonInstance(hSaveImage);
				EMImgButton * emrefresh    = GetEMImgButtonInstance(hRefresh);
				EMImgButton * emzoomin     = GetEMImgButtonInstance(hZoomIn);
				EMImgButton * emzoomout    = GetEMImgButtonInstance(hZoomOut);
				EMImgButton * emzoom100    = GetEMImgButtonInstance(hZoom100);
				EMImgButton * emzoomfit    = GetEMImgButtonInstance(hZoomFit);
				EMImgStateButton * emdrawnice   = GetEMImgStateButtonInstance(hDrawNice);
				EMImgButton * emsceneinfo  = GetEMImgButtonInstance(hSceneInfo);
				EMImgButton * emabout      = GetEMImgButtonInstance(hAbout);
				emdrawnice->allowUnclick = true;
				theme.apply(emstart);
				theme.apply(emresume);
				theme.apply(emstop);
				theme.apply(emloadscene);
				theme.apply(emsavescene);
				theme.apply(emsavesctex);
				theme.apply(emmergesc);
				theme.apply(emsaveimage);
				theme.apply(emrefresh);
				theme.apply(emzoomin);
				theme.apply(emzoomout);
				theme.apply(emzoom100);
				theme.apply(emzoomfit);
				theme.apply(emdrawnice);
				theme.apply(emsceneinfo);
				theme.apply(emabout);
				emstart->setToolTip("Start rendering");
				emstop->setToolTip("Stop rendering");
				emresume->setToolTip("Resume rendering");
				emloadscene->setToolTip("Load scene");
				emsavescene->setToolTip("Save scene");
				emsavesctex->setToolTip("Save scene with textures");
				emmergesc->setToolTip("Merge scene with another");
				emsaveimage->setToolTip("Save image");
				emrefresh->setToolTip("Refresh");
				emzoomin->setToolTip("Zoom in");
				emzoomout->setToolTip("Zoom out");
				emzoom100->setToolTip("Show 100%");
				emzoomfit->setToolTip("Fit to screen");
				emdrawnice->setToolTip("Draw smooth");
				emsceneinfo->setToolTip("Scene Info");
				emabout->setToolTip("About NOX Renderer");

				HWND hLine1		 = GetDlgItem(hWnd, IDC_REND_MAINBAR_LINE1);
				HWND hLine2		 = GetDlgItem(hWnd, IDC_REND_MAINBAR_LINE2);
				HWND hLine3		 = GetDlgItem(hWnd, IDC_REND_MAINBAR_LINE3);
				HWND hLine4		 = GetDlgItem(hWnd, IDC_REND_MAINBAR_LINE4);
				HWND hLine5		 = GetDlgItem(hWnd, IDC_REND_MAINBAR_LINE5);
				SetWindowPos(hLine1, HWND_TOP, 0,0, 2, 25, SWP_NOMOVE | SWP_NOZORDER);
				SetWindowPos(hLine2, HWND_TOP, 0,0, 2, 25, SWP_NOMOVE | SWP_NOZORDER);
				SetWindowPos(hLine3, HWND_TOP, 0,0, 2, 25, SWP_NOMOVE | SWP_NOZORDER);
				SetWindowPos(hLine4, HWND_TOP, 0,0, 2, 25, SWP_NOMOVE | SWP_NOZORDER);
				SetWindowPos(hLine5, HWND_TOP, 0,0, 2, 25, SWP_NOMOVE | SWP_NOZORDER);
				theme.apply(GetEMLineInstance(hLine1));
				theme.apply(GetEMLineInstance(hLine2));
				theme.apply(GetEMLineInstance(hLine3));
				theme.apply(GetEMLineInstance(hLine4));
				theme.apply(GetEMLineInstance(hLine5));


				// start button
				emstart->bmAll = &(gIcons);
				emstart->pNx = emstart->pMx = emstart->pCx = emstart->pDx = 0;
				emstart->pNy = 0;
				emstart->pMy = 32;
				emstart->pCy = 64;
				emstart->pDy = 96;
				emstart->colBorderClick = emstart->colBorder;

				// resume button
				emresume->bmAll = &(gIcons);
				emresume->pNx = emresume->pMx = emresume->pCx = emresume->pDx = 768;
				emresume->pNy = 0;
				emresume->pMy = 32;
				emresume->pCy = 64;
				emresume->pDy = 96;
				emresume->colBorderClick = emresume->colBorder;

				// stop button
				emstop->bmAll = &(gIcons);
				emstop->pNx = emstop->pMx = emstop->pCx = emstop->pDx = 64;
				emstop->pNy = 0;
				emstop->pMy = 32;
				emstop->pCy = 64;
				emstop->pDy = 96;
				emstop->colBorderClick = emstop->colBorder;

				// load scene button
				emloadscene->bmAll = &(gIcons);
				emloadscene->pNx = emloadscene->pMx = emloadscene->pCx = emloadscene->pDx = 192;
				emloadscene->pNy = 0;
				emloadscene->pMy = 32;
				emloadscene->pCy = 64;
				emloadscene->pDy = 96;
				emloadscene->colBorderClick = emloadscene->colBorder;

				// save scene button
				emsavescene->bmAll = &(gIcons);
				emsavescene->pNx = emsavescene->pMx = emsavescene->pCx = emsavescene->pDx = 544;
				emsavescene->pNy = 0;
				emsavescene->pMy = 32;
				emsavescene->pCy = 64;
				emsavescene->pDy = 96;
				emsavescene->colBorderClick = emsavescene->colBorder;

				// save scene with textures button
				emsavesctex->bmAll = &(gIcons);
				emsavesctex->pNx = emsavesctex->pMx = emsavesctex->pCx = emsavesctex->pDx = 1440;
				emsavesctex->pNy = 0;
				emsavesctex->pMy = 32;
				emsavesctex->pCy = 64;
				emsavesctex->pDy = 96;
				emsavesctex->colBorderClick = emsavesctex->colBorder;

				// merge scene button
				emmergesc->bmAll = &(gIcons);
				emmergesc->pNx = emmergesc->pMx = emmergesc->pCx = emmergesc->pDx = 1408;
				emmergesc->pNy = 0;
				emmergesc->pMy = 32;
				emmergesc->pCy = 64;
				emmergesc->pDy = 96;
				emmergesc->colBorderClick = emmergesc->colBorder;

				// save image button
				emsaveimage->bmAll = &(gIcons);
				emsaveimage->pNx = emsaveimage->pMx = emsaveimage->pCx = emsaveimage->pDx = 96;
				emsaveimage->pNy = 0;
				emsaveimage->pMy = 32;
				emsaveimage->pCy = 64;
				emsaveimage->pDy = 96;
				emsaveimage->colBorderClick = emsaveimage->colBorder;

				// refresh button
				emrefresh->bmAll = &(gIcons);
				emrefresh->pNx = emrefresh->pMx = emrefresh->pCx = emrefresh->pDx = 448;
				emrefresh->pNy = 0;
				emrefresh->pMy = 32;
				emrefresh->pCy = 64;
				emrefresh->pDy = 96;
				emrefresh->colBorderClick = emrefresh->colBorder;

				// zoom in button (+)
				emzoomin->bmAll = &(gIcons);
				emzoomin->pNx = emzoomin->pMx = emzoomin->pCx = emzoomin->pDx = 576;
				emzoomin->pNy = 0;
				emzoomin->pMy = 32;
				emzoomin->pCy = 64;
				emzoomin->pDy = 96;
				emzoomin->colBorderClick = emzoomin->colBorder;

				// zoom out button (-)
				emzoomout->bmAll = &(gIcons);
				emzoomout->pNx = emzoomout->pMx = emzoomout->pCx = emzoomout->pDx = 608;
				emzoomout->pNy = 0;
				emzoomout->pMy = 32;
				emzoomout->pCy = 64;
				emzoomout->pDy = 96;
				emzoomout->colBorderClick = emzoomout->colBorder;

				// zoom 100% button
				emzoom100->bmAll = &(gIcons);
				emzoom100->pNx = emzoom100->pMx = emzoom100->pCx = emzoom100->pDx = 640;
				emzoom100->pNy = 0;
				emzoom100->pMy = 32;
				emzoom100->pCy = 64;
				emzoom100->pDy = 96;
				emzoom100->colBorderClick = emzoom100->colBorder;

				// zoom fit button
				emzoomfit->bmAll = &(gIcons);
				emzoomfit->pNx = emzoomfit->pMx = emzoomfit->pCx = emzoomfit->pDx = 672;
				emzoomfit->pNy = 0;
				emzoomfit->pMy = 32;
				emzoomfit->pCy = 64;
				emzoomfit->pDy = 96;
				emzoomfit->colBorderClick = emzoomfit->colBorder;

				// draw nice button
				emdrawnice->bmAll = &(gIcons);
				emdrawnice->pNx = emdrawnice->pMx = emdrawnice->pCx = emdrawnice->pDx = 800;
				emdrawnice->pNy = 0;
				emdrawnice->pMy = 32;
				emdrawnice->pCy = 64;
				emdrawnice->pDy = 96;
				emdrawnice->colBorderSelected = emdrawnice->colBorder;

				// about button
				emabout->bmAll = &(gIcons);
				emabout->pNx = emabout->pMx = emabout->pCx = emabout->pDx = 1376;
				emabout->pNy = 0;
				emabout->pMy = 32;
				emabout->pCy = 64;
				emabout->pDy = 96;
				emabout->colBorderClick = emabout->colBorder;

				// scene info button
				emsceneinfo->bmAll = &(gIcons);
				emsceneinfo->pNx = emsceneinfo->pMx = emsceneinfo->pCx = emsceneinfo->pDx = 1472;
				emsceneinfo->pNy = 0;
				emsceneinfo->pMy = 32;
				emsceneinfo->pCy = 64;
				emsceneinfo->pDy = 96;
				emsceneinfo->colBorderClick = emsceneinfo->colBorder;

			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND_MAINBAR_LOADSCENE:
					{
						Logger::add("Load Scene button pressed. Dialog opened.");
						char * filename = openFileDialog(hMain, "Scene file (*.nxs *.nox)\0*.nxs;*.nox\0Scene binary file (*.nox)\0*.nox\0Scene XML file (*.nxs)\0*.nxs\0All files\0*.*\0", "Load scene", "nox");

						loadSceneAsync(filename);
					}
					break;
					case IDC_REND_MAINBAR_MERGE:
					{
						Logger::add("Merge Scene button pressed. Dialog opened.");
						char * filename = openFileDialog(hMain, "Scene binary file (*.nox)\0*.nox\0\0", "Merge scene", "nox");
						if (filename)
							mergeSceneAsync(filename);
					}
					break;
					case IDC_REND_MAINBAR_SAVESCENE:
					{
						Logger::add("Save Scene button pressed. Dialog opened.");
						char * filename = saveFileDialog(hMain,
									"Scene file (*.nox *.nxs)\0*.nox;*.nxs\0Scene binary file (*.nox)\0*.nox\0Scene XML file (*.nxs)\0*.nxs\0All files\0*.*\0", "Save scene", "nox");
						saveScene(filename, false, false);
					}
					break;
					case IDC_REND_MAINBAR_SAVESCENE_WITH_TEX:
					{
						Logger::add("Save Scene with Textures button pressed. Dialog opened.");
						char * filename = saveFileDialog(hMain,
									"Scene file (*.nox *.nxs)\0*.nox;*.nxs\0Scene binary file (*.nox)\0*.nox\0Scene XML file (*.nxs)\0*.nxs\0All files\0*.*\0", "Save scene", "nox");
						saveScene(filename, true, false);
					}
					break;
					case IDC_REND_MAINBAR_SAVEIMAGE:
					{
						Logger::add("Save image button pressed. Dialog opened.");
						char * filename = saveFileDialog(hMain, 
							"EXR high dynamic range image (*.exr)\0*.exr\0BMP image (*.bmp)\0*.bmp\0PNG image (*.png)\0*.png\0JPEG image (*.jpg)\0*.jpg\0TIFF image (*.tiff)\0*.tiff\0", 
							"Save image", "png", 3);
						saveImage(filename);
					}
					break;
					case IDC_REND_MAINBAR_REFRESH:
					{
						refreshRenderedImage();
					}
					break;
					case IDC_REND_MAINBAR_START:
					{
						updateEngineSettingsBeforeRender();
						startStoppingTimer();
						startRendering();
						runRefreshThread();
					}
					break;
					case IDC_REND_MAINBAR_RESUME:
					{
						startStoppingTimer();
						startRendering(false);
						runRefreshThread();
					}
					break;
					case IDC_REND_MAINBAR_STOP:
					{
						stopRendering();
						KillTimer(hOutputConf, TIMER_STOP_ID);
						refreshThrRunning = false;
						refreshRenderedImage();
					}
					break;
					case IDC_REND_MAINBAR_ZOOMIN:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						empv->zoomIn();
					}
					break;
					case IDC_REND_MAINBAR_ZOOMOUT:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						empv->zoomOut();
					}
					break;
					case IDC_REND_MAINBAR_ZOOM100:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						empv->zoom100();
					}
					break;
					case IDC_REND_MAINBAR_ZOOMFIT:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						empv->zoomFit();
					}
					break;
					case IDC_REND_MAINBAR_DRAW_NICE:
					{
						EMPView * empv = GetEMPViewInstance(hImage);
						EMImgStateButton * emdn = GetEMImgStateButtonInstance(GetDlgItem(hMainBar, IDC_REND_MAINBAR_DRAW_NICE));
						empv->drawNice = emdn->stateOn;
						InvalidateRect(empv->hwnd, NULL, false);
					}
					break;
					case IDC_REND_MAINBAR_ABOUT:
					{
						int res = (int)DialogBoxParam(hDllModule,
							MAKEINTRESOURCE(IDD_ABOUT_DIALOG), hWnd, AboutDlgProc,(LPARAM)(0));

					}
					break;
					case IDC_REND_MAINBAR_SCENE_INFO:
					{
						int res = (int)DialogBoxParam(hDllModule,
							MAKEINTRESOURCE(IDD_SCENE_INFO_DIALOG), hWnd, SceneInfoDlgProc,(LPARAM)(0));

					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.WindowText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)RendererMainWindow::backgroundBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}
