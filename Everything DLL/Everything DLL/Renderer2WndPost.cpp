#include "RendererMain2.h"
#include "EM2Controls.h"
#include "EMControls.h"
#include "resource.h"
#include "valuesMinMax.h"
#include "raytracer.h"
#include "dialogs.h"

extern HMODULE hDllModule;
void setPositionsOnStartPostTab(HWND hWnd);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK RendererMain2::WndProcPost(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RendererMain2 * rMain = RendererMain2::getInstance();
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				bgBrush = CreateSolidBrush(RGB(35,35,35));

				setPositionsOnStartPostTab(hWnd);

				EM2Text * emtitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TITLE));
				emtitle->bgImage = rMain->hBmpBg;
				emtitle->setFont(rMain->fonts->em2paneltitle, false);
				emtitle->colText = NGCOL_LIGHT_GRAY;
				emtitle->align = EM2Text::ALIGN_CENTER;

				EM2Button * embRefresh = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_POST_REFRESH_NOW));
				embRefresh->setFont(rMain->fonts->em2button, false);
				embRefresh->bgImage = rMain->hBmpBg;
				EM2CheckBox * emcAuto = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_AUTOREFRESH));
				emcAuto->bgImage = rMain->hBmpBg;
				emcAuto->selected = rMain->autoRedrawPost;
				emcAuto->setFont(rMain->fonts->em2text, false);

				// EXPOSURE
				EM2GroupBar * emg_exposure  = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_GR_EXPOSURE));
				emg_exposure->bgImage = rMain->hBmpBg;
				emg_exposure->setFont(rMain->fonts->em2groupbar, false);

				EM2Text * emtEV = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_EV));
				emtEV->bgImage = rMain->hBmpBg;
				emtEV->setFont(rMain->fonts->em2text, false);
				EM2Text * emtGIComp = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_GI_COMP));
				emtGIComp->bgImage = rMain->hBmpBg;
				emtGIComp->setFont(rMain->fonts->em2text, false);
				EM2Text * emtVign = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_VIGNETTE));
				emtVign->bgImage = rMain->hBmpBg;
				emtVign->setFont(rMain->fonts->em2text, false);
				EM2Text * emtBurn = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_BURN));
				emtBurn->bgImage = rMain->hBmpBg;
				emtBurn->setFont(rMain->fonts->em2text, false);
				EM2Text * emtGamma = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_GAMMA));
				emtGamma->bgImage = rMain->hBmpBg;
				emtGamma->setFont(rMain->fonts->em2text, false);
				EM2Text * emtCamResp = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_CAMERA_RESP));
				emtCamResp->bgImage = rMain->hBmpBg;
				emtCamResp->setFont(rMain->fonts->em2text, false);

				int editWidth = 42;
				EM2EditTrackBar * emetEV = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_EV));
				emetEV->setFont(rMain->fonts->em2edittrack, false);
				emetEV->bgImage = rMain->hBmpBg;
				emetEV->setValuesToFloat(NOX_POST_EV_DEF, NOX_POST_EV_MIN, NOX_POST_EV_MAX, NOX_POST_EV_DEF, 0.5f);
				emetEV->setEditBoxWidth(editWidth, 5);
				EM2EditTrackBar * emetGIcomp = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_GI_COMP));
				emetGIcomp->setFont(rMain->fonts->em2edittrack, false);
				emetGIcomp->bgImage = rMain->hBmpBg;
				emetGIcomp->setEditBoxWidth(editWidth, 5);
				emetGIcomp->setValuesToFloat(NOX_POST_GI_EV_DEF, NOX_POST_GI_EV_MIN, NOX_POST_GI_EV_MAX, NOX_POST_GI_EV_DEF, 0.5f);
				EM2EditTrackBar * emetVign = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_VIGNETTE));
				emetVign->setFont(rMain->fonts->em2edittrack, false);
				emetVign->bgImage = rMain->hBmpBg;
				emetVign->setEditBoxWidth(editWidth, 5);
				emetVign->setValuesToInt(NOX_POST_VIGNETTE_DEF, NOX_POST_VIGNETTE_MIN, NOX_POST_VIGNETTE_MAX, NOX_POST_VIGNETTE_DEF, 1);
				EM2EditTrackBar * emetTone = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_TONE));
				emetTone->setFont(rMain->fonts->em2edittrack, false);
				emetTone->bgImage = rMain->hBmpBg;
				emetTone->setEditBoxWidth(editWidth, 5);
				emetTone->setValuesToFloat(NOX_POST_TONE_DEF, NOX_POST_TONE_MIN, NOX_POST_TONE_MAX, NOX_POST_TONE_DEF, 0.5f);
				EM2EditTrackBar * emetGamma = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_GAMMA));
				emetGamma->setFont(rMain->fonts->em2edittrack, false);
				emetGamma->bgImage = rMain->hBmpBg;
				emetGamma->setEditBoxWidth(editWidth, 5);
				emetGamma->setValuesToFloat(NOX_POST_GAMMA_DEF, NOX_POST_GAMMA_MIN, NOX_POST_GAMMA_MAX, NOX_POST_GAMMA_DEF, 0.5f);
				EM2ComboBox * emcbCamResp = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_CAMERA_RESPONSE));
				emcbCamResp->setFontMain(rMain->fonts->em2combomain, false);
				emcbCamResp->setFontUnwrapped(rMain->fonts->em2combounwrap, false);
				emcbCamResp->bgImage = rMain->hBmpBg;
				emcbCamResp->maxShownRows = 15;
				emcbCamResp->rowWidth = 204;
				emcbCamResp->ellipsis = true;
				emcbCamResp->moveArrow = true;
				emcbCamResp->align = EM2ComboBox::ALIGN_LEFT;
				emcbCamResp->setNumItems(NOX_NUM_RESPONSE_FILTERS);
				for (int i=0; i<=NOX_NUM_RESPONSE_FILTERS; i++)
				{
					char * fnname = copyString(getResponseFunctionName(i));
					if (!fnname)
						continue;
					_strupr_s(fnname, 1+strlen(fnname));
					emcbCamResp->setItem(i, fnname, true);
					free(fnname);
				}

				EM2Button * embBrowse = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_REND2_POST_CAM_RESP_BROWSE));
				embBrowse->setFont(rMain->fonts->em2button, false);
				embBrowse->bgImage = rMain->hBmpBg;

				// FILTER
				EM2GroupBar * emg_filter = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_GR_FILTER));
				emg_filter->bgImage = rMain->hBmpBg;
				emg_filter->setFont(rMain->fonts->em2groupbar, false);
				EM2Text * emtFilter = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_FILTER));
				emtFilter->bgImage = rMain->hBmpBg;
				emtFilter->setFont(rMain->fonts->em2text, false);
				EM2ComboBox * emcbFilter = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_FILTER));
				emcbFilter->setFontMain(rMain->fonts->em2combomain, false);
				emcbFilter->setFontUnwrapped(rMain->fonts->em2combounwrap, false);
				emcbFilter->bgImage = rMain->hBmpBg;
				emcbFilter->maxShownRows = 9;
				emcbFilter->rowWidth = 140;
				emcbFilter->moveArrow = true;
				emcbFilter->align = EM2ComboBox::ALIGN_LEFT;
				emcbFilter->setNumItems(9);
				emcbFilter->setItem(0, "NO AA", true);
				emcbFilter->setItem(1, "BOX", true);
				emcbFilter->setItem(2, "BOX 2", true);
				emcbFilter->setItem(3, "AA WEIGHTED", true);
				emcbFilter->setItem(4, "MITCHELL-NETRAVALI", true);
				emcbFilter->setItem(5, "CATMULL-ROM", true);
				emcbFilter->setItem(6, "B-SPLINE", true);
				emcbFilter->setItem(7, "GAUSS", true);
				emcbFilter->setItem(8, "GAUSS SMOOTH", true);
				emcbFilter->selected = 2;


				// HOT PIXELS
				EM2GroupBar * emg_hp = GetEM2GroupBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_GR_HOT_PIXELS));
				emg_hp->bgImage = rMain->hBmpBg;
				emg_hp->setFont(rMain->fonts->em2groupbar, false);

				EM2Text * emtRadius = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_RADIUS));
				emtRadius->bgImage = rMain->hBmpBg;
				emtRadius->setFont(rMain->fonts->em2text, false);
				EM2Text * emtThreshold = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_THRESHOLD));
				emtThreshold->bgImage = rMain->hBmpBg;
				emtThreshold->setFont(rMain->fonts->em2text, false);
				EM2Text * emtDensity = GetEM2TextInstance(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_DENSITY));
				emtDensity->bgImage = rMain->hBmpBg;
				emtDensity->setFont(rMain->fonts->em2text, false);

				EM2CheckBox * emcHPEnabled = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_ENABLED));
				emcHPEnabled->bgImage = rMain->hBmpBg;
				emcHPEnabled->setFont(rMain->fonts->em2text, false);
				EM2EditSpin * emtHPRadius = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_RADIUS));
				emtHPRadius->bgImage = rMain->hBmpBg;
				emtHPRadius->setFont(rMain->fonts->em2editspin, false);
				emtHPRadius->setValuesToInt(NOX_POST_HP_RADIUS_DEF, NOX_POST_HP_RADIUS_MIN, NOX_POST_HP_RADIUS_MAX, NOX_POST_HP_RADIUS_DEF, 1, 0.1f);
				EM2EditSpin * emtHPThreshold = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_THRESHOLD));
				emtHPThreshold->bgImage = rMain->hBmpBg;
				emtHPThreshold->setFont(rMain->fonts->em2editspin, false);
				emtHPThreshold->floatAfterDot = 1;
				emtHPThreshold->setValuesToFloat(NOX_POST_HP_THRESHOLD_DEF, NOX_POST_HP_THRESHOLD_MIN, NOX_POST_HP_THRESHOLD_MAX, NOX_POST_HP_THRESHOLD_DEF, 0.5f, 0.1f);
				EM2EditSpin * emtHPDensity = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_DENSITY));
				emtHPDensity->bgImage = rMain->hBmpBg;
				emtHPDensity->setFont(rMain->fonts->em2editspin, false);
				emtHPDensity->setValuesToInt(NOX_POST_HP_DENSITY_DEF, NOX_POST_HP_DENSITY_MIN, NOX_POST_HP_DENSITY_MAX, NOX_POST_HP_DENSITY_DEF, 1, 0.1f);

			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_ERASEBKGND:
			{
				return 1;
			}
			break;
		case WM_PAINT:
			{
				BOOL ur;
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect, urect;

				ur = GetUpdateRect(hWnd, &urect, TRUE);		// rectangle region supported
				GetClientRect(hWnd, &rect);
				if (ur)
					rect = urect;

				hdc = BeginPaint(hWnd, &ps);

				HDC bhdc = CreateCompatibleDC(hdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(hdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(bhdc, backBMP);

				HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
				HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, rMain->hBmpBg);
				BitBlt(bhdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left+rMain->bgShiftXRightPanel, rect.top, SRCCOPY);
				SelectObject(thdc, oldBitmap);
				DeleteDC(thdc);

				BitBlt(hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, bhdc, 0, 0, SRCCOPY);

				SelectObject(bhdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(bhdc); 

				EndPaint(hWnd, &ps);
			}
			break;
		case WM_TIMER:
			{
				if (wParam == TIMER_POSTPROCESS_ID)
				{
					KillTimer(hWnd, TIMER_POSTPROCESS_ID);
					rMain->refreshRenderOnThread();
				}
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_REND2_POST_REFRESH_NOW:
						{
							CHECK(wmEvent==BN_CLICKED);
							rMain->refreshRenderOnThread();
						}
						break;
					case IDC_REND2_POST_AUTOREFRESH:
						{
							CHECK(wmEvent==BN_CLICKED);
							EM2CheckBox * emcAutoLays = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabBlend,			IDC_REND2_BLEND_AUTOREFRESH));
							EM2CheckBox * emcAutoPost = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabPost,			IDC_REND2_POST_AUTOREFRESH));
							EM2CheckBox * emcAutoCorr = GetEM2CheckBoxInstance(GetDlgItem(rMain->hTabCorrection,	IDC_REND2_CORR_AUTOREFRESH));
							EM2CheckBox * ecur = emcAutoPost;
							CHECK(ecur);
							rMain->autoRedrawPost = ecur->selected;
							if (emcAutoLays)
								emcAutoLays->selected = ecur->selected;
							if (emcAutoPost)
								emcAutoPost->selected = ecur->selected;
							if (emcAutoCorr)
								emcAutoCorr->selected = ecur->selected;
						}
						break;
					case IDC_REND2_POST_EV:
						{
							EM2EditTrackBar * emetEV = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_EV));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
							{
								cam->iMod.setISO_EV_compensation(emetEV->floatValue);
								cam->iMod.evalMultiplierFromISO_and_EV();
							}
							if (empv  &&  empv->imgMod)
							{
								empv->imgMod->setISO_EV_compensation(emetEV->floatValue);
								empv->imgMod->evalMultiplierFromISO_and_EV();
							}
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_GI_COMP:
						{
							EM2EditTrackBar * emetGIEV = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_GI_COMP));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setGICompensation(emetGIEV->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setGICompensation(emetGIEV->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_VIGNETTE:
						{
							EM2EditTrackBar * emetVignette = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_VIGNETTE));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setVignette(emetVignette->intValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setVignette(emetVignette->intValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_TONE:
						{
							EM2EditTrackBar * emetTone = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_TONE));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setToneMappingValue(emetTone->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setToneMappingValue(emetTone->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_GAMMA:
						{
							EM2EditTrackBar * emetGamma = GetEM2EditTrackBarInstance(GetDlgItem(hWnd, IDC_REND2_POST_GAMMA));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setGamma(emetGamma->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setGamma(emetGamma->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_CAMERA_RESPONSE:
						{
							EM2ComboBox * emcResp = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_CAMERA_RESPONSE));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setResponseFunctionNumber(emcResp->selected);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setResponseFunctionNumber(emcResp->selected);
							rMain->updateLocksTabPost(Raytracer::getInstance()->curScenePtr->nowRendering);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_CAM_RESP_BROWSE:
						{
							CHECK(wmEvent==BN_CLICKED);

							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							EM2ComboBox * emcombo = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_CAMERA_RESPONSE));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							int num = 0;
							if (emcombo)
								num = emcombo->selected;

							int newNum = (int)DialogBoxParam(hDllModule, 
										MAKEINTRESOURCE(IDD_RESPONSE_PREVIEWS_3_DIALOG), hWnd, ResponsePreviewDlg3Proc,(LPARAM)(num));

							if (newNum > -1   &&   newNum!=num)
							{
								if (empv)
									empv->imgMod->setResponseFunctionNumber(newNum);
								if (cam)
									cam->iMod.setResponseFunctionNumber(newNum);
								emcombo->selected = newNum;
								InvalidateRect(emcombo->hwnd, NULL, false);
								rMain->updateLocksTabPost(Raytracer::getInstance()->curScenePtr->nowRendering);
								rMain->refreshRenderOnThread();
							}


						}
						break;
					case IDC_REND2_POST_FILTER:
						{
							EM2ComboBox * emcFilter = GetEM2ComboBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_FILTER));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setFilter(emcFilter->selected);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setFilter(emcFilter->selected);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_HP_ENABLED:
						{
							EM2CheckBox * emcHPon = GetEM2CheckBoxInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_ENABLED));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setDotsEnabled(emcHPon->selected);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setDotsEnabled(emcHPon->selected);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
							rMain->updateLocksTabPost(Raytracer::getInstance()->curScenePtr->nowRendering);
						}
						break;
					case IDC_REND2_POST_HP_RADIUS:
						{
							EM2EditSpin * emcHPradius = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_RADIUS));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setDotsRadius(emcHPradius->intValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setDotsRadius(emcHPradius->intValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_HP_THRESHOLD:
						{
							EM2EditSpin * emcHPthres = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_THRESHOLD));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setDotsThreshold(emcHPthres->floatValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setDotsThreshold(emcHPthres->floatValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
					case IDC_REND2_POST_HP_DENSITY:
						{
							EM2EditSpin * emcHPdens = GetEM2EditSpinInstance(GetDlgItem(hWnd, IDC_REND2_POST_HP_DENSITY));
							EMPView * empv = GetEMPViewInstance(rMain->hViewport);
							Camera * cam = Raytracer::getInstance()->curScenePtr->getActiveCamera();
							if (cam)
								cam->iMod.setDotsDensity(emcHPdens->intValue);
							if (empv  &&  empv->imgMod)
								empv->imgMod->setDotsDensity(emcHPdens->intValue);
							if (rMain->autoRedrawPost)
								SetTimer(hWnd, TIMER_POSTPROCESS_ID, rMain->autoRedrawTimer, NULL);
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, RGB(0xcf,0xcf,0xcf));
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
			break;
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return FALSE;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateBgShiftsPanelPost()
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TITLE));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TITLE), bgShiftXRightPanel, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2GroupBar * emgExposure = GetEM2GroupBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_GR_EXPOSURE));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_GR_EXPOSURE), bgShiftXRightPanel, 0, emgExposure->bgShiftX, emgExposure->bgShiftY);
	EM2GroupBar * emgFitler = GetEM2GroupBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_GR_FILTER));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_GR_FILTER), bgShiftXRightPanel, 0, emgFitler->bgShiftX, emgFitler->bgShiftY);
	EM2GroupBar * emgHP = GetEM2GroupBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_GR_HOT_PIXELS));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_GR_HOT_PIXELS), bgShiftXRightPanel, 0, emgHP->bgShiftX, emgHP->bgShiftY);

	EM2Text * emtEV = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_EV));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_EV), bgShiftXRightPanel, 0, emtEV->bgShiftX, emtEV->bgShiftY);
	EM2Text * emtGIcomp = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_GI_COMP));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_GI_COMP), bgShiftXRightPanel, 0, emtGIcomp->bgShiftX, emtGIcomp->bgShiftY);
	EM2Text * emtVign = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_VIGNETTE));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_VIGNETTE), bgShiftXRightPanel, 0, emtVign->bgShiftX, emtVign->bgShiftY);
	EM2Text * emtBurn = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_BURN));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_BURN), bgShiftXRightPanel, 0, emtBurn->bgShiftX, emtBurn->bgShiftY);
	EM2Text * emtGamma = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_GAMMA));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_GAMMA), bgShiftXRightPanel, 0, emtGamma->bgShiftX, emtGamma->bgShiftY);
	EM2Text * emtCamResp = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_CAMERA_RESP));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_CAMERA_RESP), bgShiftXRightPanel, 0, emtCamResp->bgShiftX, emtCamResp->bgShiftY);

	EM2EditTrackBar * emetEV = GetEM2EditTrackBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_EV));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_EV), bgShiftXRightPanel, 0, emetEV->bgShiftX, emetEV->bgShiftY);
	EM2EditTrackBar * emetGIcomp = GetEM2EditTrackBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_GI_COMP));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_GI_COMP), bgShiftXRightPanel, 0, emetGIcomp->bgShiftX, emetGIcomp->bgShiftY);
	EM2EditTrackBar * emetVign = GetEM2EditTrackBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_VIGNETTE));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_VIGNETTE), bgShiftXRightPanel, 0, emetVign->bgShiftX, emetVign->bgShiftY);
	EM2EditTrackBar * emetBurn = GetEM2EditTrackBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TONE));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TONE), bgShiftXRightPanel, 0, emetBurn->bgShiftX, emetBurn->bgShiftY);
	EM2EditTrackBar * emetGamma = GetEM2EditTrackBarInstance(GetDlgItem(hTabPost, IDC_REND2_POST_GAMMA));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_GAMMA), bgShiftXRightPanel, 0, emetGamma->bgShiftX, emetGamma->bgShiftY);
	EM2ComboBox * emcbCamResp = GetEM2ComboBoxInstance(GetDlgItem(hTabPost, IDC_REND2_POST_CAMERA_RESPONSE));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_CAMERA_RESPONSE), bgShiftXRightPanel, 0, emcbCamResp->bgShiftX, emcbCamResp->bgShiftY);
	EM2Button * embBrowse = GetEM2ButtonInstance(GetDlgItem(hTabPost, IDC_REND2_POST_CAM_RESP_BROWSE));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_CAM_RESP_BROWSE), bgShiftXRightPanel, 0, embBrowse->bgShiftX, embBrowse->bgShiftY);

	// FILTER
	EM2Text * emtFilter = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_FILTER));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_FILTER), bgShiftXRightPanel, 0, emtFilter->bgShiftX, emtFilter->bgShiftY);
	EM2ComboBox * emcbFilter = GetEM2ComboBoxInstance(GetDlgItem(hTabPost, IDC_REND2_POST_FILTER));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_FILTER), bgShiftXRightPanel, 0, emcbFilter->bgShiftX, emcbFilter->bgShiftY);

	// HOT PIXELS
	EM2Text * emtRadius = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_RADIUS));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_RADIUS), bgShiftXRightPanel, 0, emtRadius->bgShiftX, emtRadius->bgShiftY);
	EM2Text * emtThreshold = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_THRESHOLD));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_THRESHOLD), bgShiftXRightPanel, 0, emtThreshold->bgShiftX, emtThreshold->bgShiftY);
	EM2Text * emtDensity = GetEM2TextInstance(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_DENSITY));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_TEXT_DENSITY), bgShiftXRightPanel, 0, emtDensity->bgShiftX, emtDensity->bgShiftY);

	EM2CheckBox * emcHPEnabled = GetEM2CheckBoxInstance(GetDlgItem(hTabPost, IDC_REND2_POST_HP_ENABLED));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_HP_ENABLED), bgShiftXRightPanel, 0, emcHPEnabled->bgShiftX, emcHPEnabled->bgShiftY);

	EM2EditSpin * emesHPRadius = GetEM2EditSpinInstance(GetDlgItem(hTabPost, IDC_REND2_POST_HP_RADIUS));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_HP_RADIUS), bgShiftXRightPanel, 0, emesHPRadius->bgShiftX, emesHPRadius->bgShiftY);
	EM2EditSpin * emesHPThreshold = GetEM2EditSpinInstance(GetDlgItem(hTabPost, IDC_REND2_POST_HP_THRESHOLD));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_HP_THRESHOLD), bgShiftXRightPanel, 0, emesHPThreshold->bgShiftX, emesHPThreshold->bgShiftY);
	EM2EditSpin * emesHPDensity = GetEM2EditSpinInstance(GetDlgItem(hTabPost, IDC_REND2_POST_HP_DENSITY));
	updateControlBgShift(GetDlgItem(hTabPost, IDC_REND2_POST_HP_DENSITY), bgShiftXRightPanel, 0, emesHPDensity->bgShiftX, emesHPDensity->bgShiftY);

}

//------------------------------------------------------------------------------------------------

void setPositionsOnStartPostTab(HWND hWnd)
{
	int x, y;
	x = 8;

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TITLE), HWND_TOP, 130+x,11, 100, 16, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_REFRESH_NOW), HWND_TOP, 136,39, 103, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_AUTOREFRESH), HWND_TOP, x+360-46, 45,  46, 10, 0);
	
	x=8; y=75;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_GR_EXPOSURE),		HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_EV),			HWND_TOP,	x,		y+30,	20,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_EV),				HWND_TOP,	x+110,	y+28,	250,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_GI_COMP),		HWND_TOP,	x,		y+60,	104,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_GI_COMP),			HWND_TOP,	x+110,	y+58,	250,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_VIGNETTE),	HWND_TOP,	x,		y+90,	60,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_VIGNETTE),			HWND_TOP,	x+110,	y+88,	250,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_BURN),		HWND_TOP,	x,		y+120,	35,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TONE),				HWND_TOP,	x+110,	y+118,	250,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_GAMMA),		HWND_TOP,	x,		y+150,	50,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_GAMMA),			HWND_TOP,	x+110,	y+148,	250,	20,			0);

	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_CAMERA_RESP),	HWND_TOP,	x,		y+180,	110,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_CAMERA_RESPONSE),	HWND_TOP,	x+110,	y+178,	159,	20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_CAM_RESP_BROWSE),	HWND_TOP,	x+280,	y+177,	80,		22,			0);

	x=8; y=285;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_GR_FILTER),		HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_FILTER),		HWND_TOP,	x,		y+30,	40,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_FILTER),			HWND_TOP,	x+40,	y+28,	130,	20,			0);

	x=8; y=345;
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_GR_HOT_PIXELS),	HWND_TOP,	x,		y,		360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_HP_ENABLED),		HWND_TOP,	x,		y+30,	360,	16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_RADIUS),		HWND_TOP,	x,		y+60,	45,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_HP_RADIUS),		HWND_TOP,	x+47,	y+58,	48,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_THRESHOLD),	HWND_TOP,	x+119,	y+60,	70,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_HP_THRESHOLD),		HWND_TOP,	x+189,	y+58,	48,		20,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TEXT_DENSITY),		HWND_TOP,	x+260,	y+60,	52,		16,			0);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_HP_DENSITY),		HWND_TOP,	x+312,	y+58,	48,		20,			0);

	// tab order
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_EV),			HWND_TOP,										0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_GI_COMP),		GetDlgItem(hWnd, IDC_REND2_POST_EV),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_VIGNETTE),		GetDlgItem(hWnd, IDC_REND2_POST_GI_COMP),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_TONE),			GetDlgItem(hWnd, IDC_REND2_POST_VIGNETTE),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_GAMMA),		GetDlgItem(hWnd, IDC_REND2_POST_TONE),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_HP_RADIUS),	GetDlgItem(hWnd, IDC_REND2_POST_GAMMA),			0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_HP_THRESHOLD),	GetDlgItem(hWnd, IDC_REND2_POST_HP_RADIUS),		0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);
	SetWindowPos(GetDlgItem(hWnd, IDC_REND2_POST_HP_DENSITY),	GetDlgItem(hWnd, IDC_REND2_POST_HP_THRESHOLD),	0,0,0,0,	SWP_NOMOVE | SWP_NOSIZE);

}

//------------------------------------------------------------------------------------------------

bool RendererMain2::fillPostTabAll(ImageModifier * iMod)
{
	CHECK(iMod);

	EM2EditTrackBar * emetEV =		GetEM2EditTrackBarInstance(	GetDlgItem(hTabPost, IDC_REND2_POST_EV));
	EM2EditTrackBar * emetGIcomp =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabPost, IDC_REND2_POST_GI_COMP));
	EM2EditTrackBar * emetVign =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabPost, IDC_REND2_POST_VIGNETTE));
	EM2EditTrackBar * emetBurn =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabPost, IDC_REND2_POST_TONE));
	EM2EditTrackBar * emetGamma =	GetEM2EditTrackBarInstance(	GetDlgItem(hTabPost, IDC_REND2_POST_GAMMA));
	EM2ComboBox * emcbCamResp =		GetEM2ComboBoxInstance(		GetDlgItem(hTabPost, IDC_REND2_POST_CAMERA_RESPONSE));
	EM2ComboBox * emcbFilter =		GetEM2ComboBoxInstance(		GetDlgItem(hTabPost, IDC_REND2_POST_FILTER));
	EM2CheckBox * emcHPEnabled =	GetEM2CheckBoxInstance(		GetDlgItem(hTabPost, IDC_REND2_POST_HP_ENABLED));
	EM2EditSpin * emesHPRadius =	GetEM2EditSpinInstance(		GetDlgItem(hTabPost, IDC_REND2_POST_HP_RADIUS));
	EM2EditSpin * emesHPThreshold =	GetEM2EditSpinInstance(		GetDlgItem(hTabPost, IDC_REND2_POST_HP_THRESHOLD));
	EM2EditSpin * emesHPDensity =	GetEM2EditSpinInstance(		GetDlgItem(hTabPost, IDC_REND2_POST_HP_DENSITY));

	emetEV->setFloatValue(iMod->getISO_EV_compensation());
	emetGIcomp->setFloatValue(iMod->getGICompensation());
	emetVign->setIntValue(iMod->getVignette());
	emetBurn->setFloatValue(iMod->getToneMappingValue());
	emetGamma->setFloatValue(iMod->getGamma());

	emcbFilter->selected = iMod->getFilter();
	InvalidateRect(emcbFilter->hwnd, NULL, false);

	emcbCamResp->selected = iMod->getResponseFunctionNumber();
	InvalidateRect(emcbCamResp->hwnd, NULL, false);

	emcHPEnabled->selected = iMod->getDotsEnabled();
	InvalidateRect(emcHPEnabled->hwnd, NULL, false);
	emesHPRadius->setIntValue(iMod->getDotsRadius());
	emesHPThreshold->setFloatValue(iMod->getDotsThreshold());
	emesHPDensity->setIntValue(iMod->getDotsDensity());

	return true;
}

//------------------------------------------------------------------------------------------------

void RendererMain2::updateLocksTabPost(bool nowrendering)
{
	EM2CheckBox * emcHPon = GetEM2CheckBoxInstance(GetDlgItem(hTabPost, IDC_REND2_POST_HP_ENABLED));
	setControlEnabled(emcHPon->selected, hTabPost, IDC_REND2_POST_HP_RADIUS);
	setControlEnabled(emcHPon->selected, hTabPost, IDC_REND2_POST_HP_THRESHOLD);
	setControlEnabled(emcHPon->selected, hTabPost, IDC_REND2_POST_HP_DENSITY);

	setControlEnabled(emcHPon->selected, hTabPost, IDC_REND2_POST_TEXT_RADIUS);
	setControlEnabled(emcHPon->selected, hTabPost, IDC_REND2_POST_TEXT_THRESHOLD);
	setControlEnabled(emcHPon->selected, hTabPost, IDC_REND2_POST_TEXT_DENSITY);

	EM2ComboBox * emcr = GetEM2ComboBoxInstance(GetDlgItem(hTabPost, IDC_REND2_POST_CAMERA_RESPONSE));
	setControlEnabled(emcr->selected==0, hTabPost, IDC_REND2_POST_TEXT_GAMMA);
	setControlEnabled(emcr->selected==0, hTabPost, IDC_REND2_POST_GAMMA);

}

//------------------------------------------------------------------------------------------------
