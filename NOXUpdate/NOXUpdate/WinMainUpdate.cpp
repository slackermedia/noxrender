#define _WIN32_WINNT 0x0502
#include <windows.h>
#include "../NOX_InstallPlugins/resource.h"
#include "nox_update.h"
#include "../../Everything DLL/Everything DLL/RendererMainWindow.h"
#include "../../Everything DLL/Everything DLL/raytracer.h"
#include <commctrl.h>


HINSTANCE hInst;
HWND hMainUpdate = 0;
bool isSystem64 = false;
bool silentMode = false;
bool autoUpdateOn = false;
bool autoInstall = false;
bool betaTester = false;
bool diffrDirs = false;


char * addressVersion = NULL;
char * addressUpdate = NULL;
char * downloadFilenameVersion = NULL;
char * downloadFilenameUpdate = NULL;

HWND hCheckUpdate = 0;
HWND hDownloadUpdate = 0;
HWND hAbort = 0;
HWND hProgress = 0;
HWND hTextInfo1 = 0;
HWND hTextInfo2 = 0;
HWND hAutoInstall = 0;
HWND hAutoUpdate = 0;
HWND hDiffDirs = 0;

int lMajor=0, lMinor=0, lBuild=0;
int sMajor=0, sMinor=0, sBuild=0;

bool stopDownloading = false;
extern int programStatus;

LRESULT CALLBACK NOXUpdateWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

//-------------------------------------------------------------------------------------------------------------

ATOM MyRegisterUpdateClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	HICON sIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= NOXUpdateWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= sIcon;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "nox_update";
	wcex.hIconSm		= sIcon;
	return RegisterClassEx(&wcex);
}

//-------------------------------------------------------------------------------------------------------------

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	MyRegisterUpdateClass(hInstance);

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	HACCEL hAccelTable;
	hAccelTable = 0;

	INITCOMMONCONTROLSEX InitCtrlEx;
	InitCtrlEx.dwSize = sizeof(INITCOMMONCONTROLSEX);
	InitCtrlEx.dwICC  = ICC_PROGRESS_CLASS;
	InitCommonControlsEx(&InitCtrlEx);

	if (lpCmdLine)
	{
		unsigned int cmdSize = (unsigned int)strlen(lpCmdLine);
		if (strlen(lpCmdLine) > 1)
		{
			if (strcmp(lpCmdLine, "-silent") == 0)
			{
				silentMode = true;
			}
			else
			{
			}
		}
	}

	setDir();
	getAutoUpdateRegistryStatus(autoUpdateOn, autoInstall, betaTester, diffrDirs);
	int betaH = betaTester ? 20 : 0;

	SYSTEM_INFO sinfo;
	GetNativeSystemInfo(&sinfo);
	if (sinfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
	{
		isSystem64 = true;
	}
	else
		if (sinfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_INTEL)
		{
			isSystem64 = false;
		}
		else
		{
			if (!silentMode)
				MessageBox(0, "Unfortunately I can't determine your system version.", "", 0);
			return 0;
		}

	
	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	hMainUpdate = CreateWindow("nox_update", "NOX Update", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN ,
		  200,200,   315,200+betaH,   0, NULL, hInstance, NULL);

	if (hMainUpdate  &&  !silentMode)
		ShowWindow(hMainUpdate, SW_SHOW);
	else
		ShowWindow(hMainUpdate, SW_HIDE);

	RECT wrect, crect;
	GetWindowRect(hMainUpdate, &wrect);
	GetClientRect(hMainUpdate, &crect);
	int diffx = (wrect.right-wrect.left)-(crect.right-crect.left);
	int diffy = (wrect.bottom-wrect.top)-(crect.bottom-crect.top);
	SetWindowPos(hMainUpdate, HWND_TOP, 0,0, 310+diffx, 160+diffy+betaH, SWP_NOZORDER | SWP_NOMOVE);

	hCheckUpdate = CreateWindow("BUTTON", "Check update", WS_CHILD | WS_VISIBLE,
		   10,120+betaH,   140,30,   hMainUpdate, (HMENU)201, hInstance, NULL);
	hAbort = CreateWindow("BUTTON", "Exit", WS_CHILD | WS_VISIBLE,
		  160,120+betaH,   140,30,   hMainUpdate, (HMENU)203, hInstance, NULL);

	hTextInfo1 = CreateWindow("STATIC", "Checking NOX version...", WS_CHILD | WS_VISIBLE,
		  10,10,   335,20,   hMainUpdate, (HMENU)204, hInstance, NULL);
	hTextInfo2 = CreateWindow("STATIC", "Click Check update...", WS_CHILD | WS_VISIBLE,
		  10,30,   335,20,   hMainUpdate, (HMENU)205, hInstance, NULL);
	hAutoUpdate = CreateWindow("BUTTON", "Check for updates on NOX start", WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
		  10,75,   250,20,   hMainUpdate, (HMENU)206, hInstance, NULL);
	hAutoInstall = CreateWindow("BUTTON", "Auto install after download", WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
		  10,95,   250,20,   hMainUpdate, (HMENU)207, hInstance, NULL);
	if (betaTester)
	{
		hDiffDirs = CreateWindow("BUTTON", "Install in different directory", WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
			  10,115,   250,20,   hMainUpdate, (HMENU)208, hInstance, NULL);
		SendMessage(hDiffDirs, WM_SETFONT, (WPARAM)hFont, TRUE);
	}

	if (betaTester)
		SetWindowText(hMainUpdate, "NOX Update [beta tester]");

	SendMessage(hAutoUpdate,	BM_SETCHECK, autoUpdateOn ? BST_CHECKED : BST_UNCHECKED, 0 );
	SendMessage(hAutoInstall,	BM_SETCHECK, autoInstall ? BST_CHECKED : BST_UNCHECKED, 0 );
	if (betaTester)
		SendMessage(hDiffDirs,	BM_SETCHECK, diffrDirs ? BST_CHECKED : BST_UNCHECKED, 0 );

	hProgress = CreateWindowEx(0, PROGRESS_CLASS, NULL,   WS_CHILD | WS_VISIBLE | PBS_SMOOTH,
			      10, 50,  290, 17,  hMainUpdate, NULL, hInst, NULL);

	SendMessage(hProgress, PBM_SETRANGE, 0, MAKELPARAM(0, 100)); 
	SendMessage(hProgress, PBM_SETPOS, 0, 0); 

	SendMessage(hCheckUpdate,		WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hDownloadUpdate,	WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hAbort,				WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hTextInfo1,			WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hTextInfo2,			WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hAutoInstall,		WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hAutoUpdate,		WM_SETFONT, (WPARAM)hFont, TRUE);

	bool vOK = getNoxVersion(lMajor, lMinor, lBuild);
	if (vOK)
	{
		char msgtxt[512];
		sprintf_s(msgtxt, 512, "Current NOX version: %d.%d.%d", lMajor, lMinor, lBuild);
		SetWindowText(hTextInfo1, msgtxt);
		programStatus = PROGRAM_STATUS_LOCAL_VERSION_OK;
	}
	else
	{
		SetWindowText(hTextInfo1, "Error, can't find NOX installed.");
		programStatus = PROGRAM_STATUS_LOCAL_VERSION_ERROR;
	}

	if (silentMode)
	{
		stopDownloading = false;
		bool lOK = getNoxVersion(lMajor, lMinor, lBuild);
		programStatus = lOK ? PROGRAM_STATUS_LOCAL_VERSION_OK : PROGRAM_STATUS_LOCAL_VERSION_ERROR;
		if (!lOK)
			return 0;

		SetWindowText(hTextInfo2, "Checking server version. Please wait...");
		
		bool threadOK = downloadNOXserverVersion(betaTester);
		if (!threadOK)
		{
			programStatus = PROGRAM_STATUS_SERVER_VERSION_ERROR;
			unlockControlsAfterDownloadVersion(0);
		}
	}

	// Main message loop:
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

//-------------------------------------------------------------------------------------------------------------

void lockControlsBeforeDownloadVersion()
{
	EnableWindow(hCheckUpdate, false);
	SetWindowText(hAbort, "Abort");
}

//-------------------------------------------------------------------------------------------------------------

void unlockControlsAfterDownloadVersion(int status)
{
	SetWindowText(hAbort, "Exit");
	if (status)	// no errors
	{
		EnableWindow(hCheckUpdate, true);
		SetWindowText(hCheckUpdate, "Download update");
	}
	else
	{
		if (silentMode)
		{
			SendMessage(hMainUpdate, WM_CLOSE, 0, 0);
		}

	}
}

//-------------------------------------------------------------------------------------------------------------

void lockControlsBeforeDownloadUpdate()
{
	EnableWindow(hCheckUpdate, false);
	SetWindowText(hAbort, "Abort");
}

//-------------------------------------------------------------------------------------------------------------

void unlockControlsAfterDownloadUpdate(int status)
{
	SetWindowText(hAbort, "Exit");
	if (status)	// no errors
	{
		EnableWindow(hCheckUpdate, true);
		SetWindowText(hCheckUpdate, "Install update");
	}
}

//-------------------------------------------------------------------------------------------------------------

void lockControlsBeforeInstallUpdate()
{
	EnableWindow(hCheckUpdate, false);
	SetWindowText(hAbort, "Abort");
}

//-------------------------------------------------------------------------------------------------------------

void unlockControlsAfterInstallUpdate(int status)
{
	SetWindowText(hAbort, "Exit");
	if (status)	// no errors
	{
		EnableWindow(hCheckUpdate, true);
		SetWindowText(hCheckUpdate, "Install plugins");
	}
}

//-------------------------------------------------------------------------------------------------------------

void unlockControlsAfterRunPluginsInstaller(int status)
{
	SetWindowText(hAbort, "Exit");
	if (status)	// no errors
	{
		EnableWindow(hCheckUpdate, false);
		SetWindowText(hCheckUpdate, "Done");
	}
}

//-------------------------------------------------------------------------------------------------------------
