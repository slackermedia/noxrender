#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EM2Controls.h"
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "IES.h"
#include "log.h"
#include "noxfonts.h"

bool drawGraphIES(ImageByteBuffer * imgbuf, IesNOX * ies);
bool updateControlBgShift(HWND hControl, int shParX, int shParY, int &shCtrlX, int &shCtrlY);
bool updateIES2ControlBackgrounds(HWND hWnd);
bool initIES2ControlPositions(HWND hWnd);
bool fillIES2Data(HWND hWnd, IesNOX * ies);

//------------------------------------------------------------------------------------------------

INT_PTR CALLBACK IES2DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH bgBrush = 0;
	static HBITMAP hBgBMP = 0;
	static IesNOX * ies = NULL;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				int dx = 412, dy = 512;
				RECT tmprect, wrect,crect;
				GetWindowRect(hWnd, &wrect);
				GetClientRect(hWnd, &crect);
				tmprect.left = 0;
				tmprect.top = 0;
				tmprect.right = dx + wrect.right-wrect.left-crect.right;
				tmprect.bottom = dy + wrect.bottom-wrect.top-crect.bottom;
				SetWindowPos(hWnd, HWND_TOP, 0,0, tmprect.right, tmprect.bottom, SWP_NOMOVE);

				bgBrush = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				hBgBMP = generateBackgroundPattern(dx, dy, 0,0, RGB(40,40,40), RGB(35,35,35));
				NOXFontManager * fonts = NOXFontManager::getInstance();

				IesNOX * oIES = (IesNOX *)lParam;
				ies = new IesNOX();
				if (oIES)
					*ies = *oIES;

				EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_TITLE));
				emtTitle->align = EM2Text::ALIGN_CENTER;
				emtTitle->bgImage = hBgBMP;
				emtTitle->setFont(fonts->em2paneltitle, false);
				emtTitle->colText = NGCOL_LIGHT_GRAY;

				EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_OK));
				embOK->bgImage = hBgBMP;
				embOK->setFont(fonts->em2button, false);
				EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_CANCEL));
				embCancel->bgImage = hBgBMP;
				embCancel->setFont(fonts->em2button, false);
				EM2Button * embLoad = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_LOAD));
				embLoad->bgImage = hBgBMP;
				embLoad->setFont(fonts->em2button, false);
				EM2Button * embUnload = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_UNLOAD));
				embUnload->bgImage = hBgBMP;
				embUnload->setFont(fonts->em2button, false);

				EM2Text * emtName = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_FILENAME));
				emtName->bgImage = hBgBMP;
				emtName->setFont(fonts->em2text, false);

				EM2Text * emtPower = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_POWER));
				emtPower->align = EM2Text::ALIGN_RIGHT;
				emtPower->bgImage = hBgBMP;
				emtPower->setFont(fonts->em2text, false);

				initIES2ControlPositions(hWnd);
				updateIES2ControlBackgrounds(hWnd);

				EMPView * empv = GetEMPViewInstance(GetDlgItem(hWnd, IDC_IES2_GRAPH));
				if (!empv->byteBuff)
					empv->byteBuff = new ImageByteBuffer();
				empv->byteBuff->allocBuffer(384, 384);
				empv->byteBuff->clearBuffer();
				empv->dontLetUserChangeZoom = true;
				drawGraphIES(empv->byteBuff, ies);
				InvalidateRect(empv->hwnd, NULL, false);

				fillIES2Data(hWnd, ies);
			}
			break;
		case WM_DESTROY:
			{
				if (bgBrush)
					DeleteObject(bgBrush);
				bgBrush = 0;
			}
			break;
		case WM_CLOSE:
			{
				EndDialog(hWnd, (INT_PTR)(-1));
			}
		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				RECT rect, urect;
				GetClientRect(hWnd, &rect);

				BOOL ur = GetUpdateRect(hWnd, &urect, FALSE);
				GetClientRect(hWnd, &rect);
				int orb = rect.bottom;
				int orr = rect.right;
				if (ur)
					rect = urect;
				int xx = rect.left;
				int yy = rect.top;

				HDC ohdc = BeginPaint(hWnd, &ps);
				HDC hdc = CreateCompatibleDC(ohdc);
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				SelectObject(hdc, backBMP);

				if (hBgBMP)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, hBgBMP);
					BitBlt(hdc, 0, 0, rect.right-rect.left, rect.bottom-rect.top, thdc, rect.left, rect.top, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, bgBrush);
					HPEN hPen = CreatePen(PS_SOLID, 1, NGCOL_BG_MEDIUM);
					HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, oldPen));
					SelectObject(hdc, oldBrush);
				}

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY );
				DeleteDC(hdc); 
				DeleteObject(backBMP);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
				case IDC_IES2_OK:
					{
						if (wmEvent == BN_CLICKED)
						{
							if (!ies->isValid())
							{
								ies->releaseStuff();
								delete ies;
								ies = NULL;
								EndDialog(hWnd, (INT_PTR)(0));
							}
							else
							{
								IesNOX * r_ies = ies;
								ies = NULL;
								EndDialog(hWnd, (INT_PTR)(r_ies));
							}
						}
					}
					break;
				case IDC_IES2_CANCEL:
					{
						if (wmEvent == BN_CLICKED)
						{
							EndDialog(hWnd, (INT_PTR)(-1));
						}
					}
					break;
				case IDC_IES2_LOAD:
					{
						if (wmEvent == BN_CLICKED)
						{
							char * fname = openIesFileDialog(hWnd);
							if (fname)
							{
								ies->releaseStuff();
								bool ok = ies->parseFile(fname);
								if (!ok)
								{
									Logger::add("IES parse failed... error:");
									Logger::add(ies->errorBuf);
									MessageBox(0, ies->errorBuf, "Parsing error", 0);
								}
							}

							EMPView * emGraph = GetEMPViewInstance(GetDlgItem(hWnd, IDC_IES2_GRAPH));
							drawGraphIES(emGraph->byteBuff, ies);
							InvalidateRect(emGraph->hwnd, NULL, false);
							
							fillIES2Data(hWnd, ies);
						}
					}
					break;
				case IDC_IES2_UNLOAD:
					{
						ies->releaseStuff();
						EMPView * emGraph = GetEMPViewInstance(GetDlgItem(hWnd, IDC_IES2_GRAPH));
						drawGraphIES(emGraph->byteBuff, ies);
						InvalidateRect(emGraph->hwnd, NULL, false);
						fillIES2Data(hWnd, ies);
					}
					break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//------------------------------------------------------------------------------------------------

bool updateIES2ControlBackgrounds(HWND hWnd)
{
	EM2Text * emtTitle = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_TITLE));
	updateControlBgShift(GetDlgItem(hWnd, IDC_IES2_TITLE), 0, 0, emtTitle->bgShiftX, emtTitle->bgShiftY);

	EM2Text * emtFname = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_FILENAME));
	updateControlBgShift(GetDlgItem(hWnd, IDC_IES2_FILENAME), 0, 0, emtFname->bgShiftX, emtFname->bgShiftY);
	EM2Text * emtPower = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_POWER));
	updateControlBgShift(GetDlgItem(hWnd, IDC_IES2_POWER), 0, 0, emtPower->bgShiftX, emtPower->bgShiftY);

	EM2Button * embLoad = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_LOAD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_IES2_LOAD), 0, 0, embLoad->bgShiftX, embLoad->bgShiftY);
	EM2Button * embUnload = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_UNLOAD));
	updateControlBgShift(GetDlgItem(hWnd, IDC_IES2_UNLOAD), 0, 0, embUnload->bgShiftX, embUnload->bgShiftY);
	EM2Button * embCancel = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_CANCEL));
	updateControlBgShift(GetDlgItem(hWnd, IDC_IES2_CANCEL), 0, 0, embCancel->bgShiftX, embCancel->bgShiftY);
	EM2Button * embOK = GetEM2ButtonInstance(GetDlgItem(hWnd, IDC_IES2_OK));
	updateControlBgShift(GetDlgItem(hWnd, IDC_IES2_OK), 0, 0, embOK->bgShiftX, embOK->bgShiftY);

	return true;
}

//------------------------------------------------------------------------------------------------

bool initIES2ControlPositions(HWND hWnd)
{
	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_TITLE),					HWND_TOP,	0, 10,	412, 19, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_GRAPH),					HWND_TOP,	14, 39,	384, 384, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_LOAD),					HWND_TOP,	14,  437,	72, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_UNLOAD),					HWND_TOP,	96, 437,	72, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_OK),						HWND_TOP,	129, 477,	72, 22, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_CANCEL),					HWND_TOP,	211, 477,	72, 22, 0);

	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_FILENAME),				HWND_TOP,	179, 440,	136, 16, 0);
	SetWindowPos(GetDlgItem(hWnd, IDC_IES2_POWER),					HWND_TOP,	318, 440,	80,  16, 0);

	return true;
}

//------------------------------------------------------------------------------------------------

bool fillIES2Data(HWND hWnd, IesNOX * ies)
{
	if (!ies)
		return false;

	EM2Text * emFilename = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_FILENAME));
	EM2Text * emTextPower = GetEM2TextInstance(GetDlgItem(hWnd, IDC_IES2_POWER));

	if (ies->filename)
	{
		char * ofname = getOnlyFile(ies->filename);
		if (ofname)
		{
			emFilename->changeCaption(ofname);
			free(ofname);
		}
		else
			emFilename->changeCaption("FILENAME ERROR");
	}
	else
		emFilename->changeCaption("NO FILENAME");

	int numL = max(1, ies->numLamps);
	float power = ies->lumensPerLamp * numL;
	char buf[64];
	sprintf_s(buf, "%.2f LM", power);
	emTextPower->changeCaption(buf);

	return true;
}

//------------------------------------------------------------------------------------------------

