#define _CRT_RAND_S
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "EMControls.h"
#include "Dialogs.h"
#include "resource.h"
#include "DLL.h"
#include "ColorSchemes.h"
#include "MaterialArchiver.h"
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include "log.h"
#include "MatEditor.h"

extern char  * defaultDirectory;
HWND downHwnd = 0;
OnlineMatLib::OnlineMatInfo * downMatInfo = NULL;

bool progressCallbackDownloadMat(void * obj, float progress);
void matDownThreadDoneCallback(void * obj, bool OK);
bool prepareAndLaunchDownloadThread();
bool downAbortedByUser = false;
char downLocalFile[2048];
char curmatdir[2048];


//-----------------------------------------------------------------------------------------------------------------------


INT_PTR CALLBACK DownloadMatDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static MaterialNox * mat;
	static HBRUSH bgBrush = 0;

	switch (message)
	{
		case WM_INITDIALOG:
			{
				if (!lParam)
				{
					MessageBox(0, "Internal error\bSorry...", "Error", 0);
					EndDialog(hWnd, (INT_PTR)NULL);
				}
				
				downMatInfo = (OnlineMatLib::OnlineMatInfo *)lParam;
				downHwnd = hWnd;
				GlobalWindowSettings::colorSchemes.apply(GetEMTextInstance(			GetDlgItem(hWnd, IDC_DOWNLOAD_TEXT1)), true);
				GlobalWindowSettings::colorSchemes.apply(GetEMProgressBarInstance(	GetDlgItem(hWnd, IDC_DOWNLOAD_PROGRESS)));
				GlobalWindowSettings::colorSchemes.apply(GetEMButtonInstance(		GetDlgItem(hWnd, IDC_DOWNLOAD_CANCEL)));

				bool ok = prepareAndLaunchDownloadThread();
				if (!ok)
					EndDialog(hWnd, (INT_PTR)NULL);
			}
			break;
		case WM_PAINT:
			{
				HDC hdc;
				PAINTSTRUCT ps;
				RECT rect;
				hdc = BeginPaint(hWnd, &ps);
				GetClientRect(hWnd, &rect);
				HBRUSH hBr = CreateSolidBrush(GlobalWindowSettings::colorSchemes.DialogBackground);
				HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, hBr);
				HPEN hPen = CreatePen(PS_SOLID, 1, GlobalWindowSettings::colorSchemes.DialogBackground);
				HPEN oldPen = (HPEN)SelectObject(hdc, hPen);
				Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
				DeleteObject(SelectObject(hdc, oldPen));
				DeleteObject(SelectObject(hdc, oldBrush));
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_COMMAND:
			{
				int wmId, wmEvent;
				wmId    = LOWORD(wParam);
				wmEvent = HIWORD(wParam);
				switch (wmId)
				{
					case IDC_DOWNLOAD_CANCEL:
						{
							CHECK(wmEvent==BN_CLICKED);
							downAbortedByUser = true;
						}
						break;
				}
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdc1 = (HDC)wParam;
				SetTextColor(hdc1, GlobalWindowSettings::colorSchemes.DialogText);
				SetBkMode(hdc1, TRANSPARENT);
				return (INT_PTR)bgBrush;
			}
		case WM_CTLCOLORDLG:
			{
				return (INT_PTR)bgBrush;
			}
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------------------------------
bool progressCallbackDownloadMat(void * obj, float progress)
{
	HWND hPr = GetDlgItem(downHwnd, IDC_DOWNLOAD_PROGRESS);
	EMProgressBar * empb = GetEMProgressBarInstance(hPr);
	empb->setPos((float)(100*progress));
	InvalidateRect(hPr, NULL, false);

	if (downAbortedByUser)
		return false;

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

void matDownThreadDoneCallback(void * obj, bool OK)
{
	if (!OK)
	{
		if (!downAbortedByUser)
			MessageBox(0, "Unfortunately download failed.", "Error", 0);
		downAbortedByUser = false;
		EndDialog(downHwnd, (INT_PTR)NULL);
	}
	downAbortedByUser = false;


	char args[2048];
	sprintf_s(args, 2048, "%s\\bin\\32\\7zG.exe x \"%s\" -o\"%s\" -y", defaultDirectory, downLocalFile, curmatdir);

	PROCESS_INFORMATION pi;
	STARTUPINFO si = {	sizeof(STARTUPINFO),	NULL,	"",	NULL, 0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0 };

	bool ok1 = (CreateProcess(NULL, args, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS , NULL, NULL, &si, &pi) == TRUE);
	if (ok1)
	{
		WaitForSingleObject(pi.hProcess, INFINITE);
		DWORD exitCode;
		GetExitCodeProcess(pi.hProcess, &exitCode);
		SwitchToThisWindow(MatEditorWindow::hMain, true);
		if (exitCode)	// some error while compressing
		{	
			ok1 = false;
			Logger::add(" 7-zip error!");
		}

		CloseHandle(pi.hThread );
		CloseHandle(pi.hProcess );	
	}

	int s = (int)strlen(curmatdir) + 64;
	char * result = (char *)malloc(s);
	sprintf_s(result, s, "%s\\material.nxm", curmatdir);

	if (!fileExists(result))
	{
		free(result);
		EndDialog(downHwnd, (INT_PTR)NULL);
	}

	EndDialog(downHwnd, (INT_PTR)result);
}

//-----------------------------------------------------------------------------------------------------------------------

bool prepareAndLaunchDownloadThread()
{
	// www.evermotion.org/nox/noxDownloadMaterial/id
	char matdirs[2048];
	char tempmatdir[2048];

	sprintf_s(matdirs, 2048, "%s\\materials", defaultDirectory);
	sprintf_s(tempmatdir, 2048, "%s\\materials\\temporary", defaultDirectory);
	if (downMatInfo->id <= 0)
	{
		unsigned int unique;
		while(rand_s(&unique));
		sprintf_s(curmatdir, 2048, "%s\\materials\\temporary\\material_noID_%d", defaultDirectory, unique);
	}
	else
		sprintf_s(curmatdir, 2048, "%s\\materials\\temporary\\material_%d", defaultDirectory, downMatInfo->id);

	if (!dirExists(matdirs))
	{
		Logger::add("Creating materials dir.");
		if (!CreateDirectory(matdirs, NULL))
		{
			Logger::add("Failed");
			MessageBox(0, "Materials dir doesn't exist and can't create it.", "Error", 0);
			return false;
		}
	}

	if (!dirExists(tempmatdir))
	{
		Logger::add("Creating temp materials dir.");
		if (!CreateDirectory(tempmatdir, NULL))
		{
			Logger::add("Failed");
			MessageBox(0, "Temporary materials dir doesn't exist and can't create it.", "Error", 0);
			return false;
		}
	}

	if (!dirExists(curmatdir))
	{
		Logger::add("Creating current temp material dir.");
		if (!CreateDirectory(curmatdir, NULL))
		{
			Logger::add("Failed");
			MessageBox(0, "Temporary current material dir doesn't exist and can't create it.", "Error", 0);
			return false;
		}
	}

	char * url = (char*)malloc(2048);
	sprintf_s(url, 2048, "www.evermotion.org/nox/noxDownloadMaterial/%d", downMatInfo->id);
	sprintf_s(downLocalFile, 2048, "%s\\%s", curmatdir, downMatInfo->filename);

	MatDownloader * matDownload = new MatDownloader();
	matDownload->registerThreadFinishedCallback(matDownThreadDoneCallback, NULL);
	matDownload->registerDownloadProgressCallback(progressCallbackDownloadMat, NULL);

	Logger::add("Downloading material...");
	matDownload->downloadFileThread(url, downLocalFile);

	return true;
}

//-----------------------------------------------------------------------------------------------------------------------
