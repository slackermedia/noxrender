#include "linkRenderer.h"

#ifdef PORTABLE_COMPILATION
	#include "noxinclude/resource.h"
	#include "noxinclude/Dialogs.h"
#else
	#include "../../Everything DLL/Everything DLL/resource.h"
	#include "../../Everything DLL/Everything DLL/Dialogs.h"
#endif

#include <math.h>
#include <float.h>



//---------------------------------------   GET

float TrSunSky::getLongitude()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->getLongitude();
	return 0.0f;
}

float TrSunSky::getLatitude()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->getLatitude();
	return 0.0f;
}

float TrSunSky::getTurbidity()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->turbidity;
	return 3.0f;
}

float TrSunSky::getAerosol()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->aerosol;
	return 0.0f;
}

int TrSunSky::getMonth()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->getMonth();
	return 1;
}

int TrSunSky::getDay()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->getDay();
	return 1;
}

int TrSunSky::getHour()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->getHour();
	return 12;
}

int TrSunSky::getMinute()
{
	if (addressSunSky)
		return (int)((SunSky*)addressSunSky)->getMinute();
	return 0;
}

int TrSunSky::getGMT()
{
	if (addressSunSky)
		return (int)(((SunSky*)addressSunSky)->getMeridianForTimeZone()*12/PI);
	return 0;
}

bool TrSunSky::getSunSkyON()
{
	return sunskyOn;
}

bool TrSunSky::getTargetHidden()
{
	return targetHidden;
}

bool TrSunSky::getEnvHidden()
{
	return envMapHidden;
}

float TrSunSky::getSunSize()
{
	if (addressSunSky)
		return (float)((SunSky*)addressSunSky)->sunSize;
	return 32.0f/60.0f;
}

bool TrSunSky::getSunON()
{
	if (addressSunSky)
		return (bool)((SunSky*)addressSunSky)->sunOn;
	return true;
}

bool TrSunSky::getSunOtherLayerON()
{
	if (addressSunSky)
		return (bool)((SunSky*)addressSunSky)->sunOtherBlendLayer;
	return false;
}

int TrSunSky::getModel()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->use_hosek ? 1 : 0;
	return 0;
}

float TrSunSky::getAlbedo()
{
	if (addressSunSky)
		return ((SunSky*)addressSunSky)->albedo;
	return 0.0f;
}

bool TrSunSky::getEnvEnabled()
{
	if (addressEnv)
		return (bool)((EnvSphere*)addressEnv)->enabled;
	return false;
}

float TrSunSky::getEnvPower()
{
	if (addressEnv)
		return (float)((EnvSphere*)addressEnv)->powerEV;
	return 0.0f;
}

int TrSunSky::getEnvBlendLayer()
{
	if (addressEnv)
		return (int)((EnvSphere*)addressEnv)->blendlayer;
	return 3;
}

float TrSunSky::getEnvAzimuth()
{
	if (addressEnv)
		return (float)((EnvSphere*)addressEnv)->azimuth_shift;
	return 0.0f;
}

char * TrSunSky::getEnvTexFilename()
{
	if (addressEnv)
		return (char *)((EnvSphere*)addressEnv)->tex.fullfilename;
	return NULL;
}

float TrSunSky::getEnvTexPostBrightness()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.brightness;
	return NULL;
}

float TrSunSky::getEnvTexPostContrast()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.contrast;
	return NULL;
}

float TrSunSky::getEnvTexPostSaturation()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.saturation;
	return NULL;
}

float TrSunSky::getEnvTexPostHue()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.hue;
	return NULL;
}

float TrSunSky::getEnvTexPostRed()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.red;
	return NULL;
}

float TrSunSky::getEnvTexPostGreen()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.green;
	return NULL;
}

float TrSunSky::getEnvTexPostBlue()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.blue;
	return NULL;
}

float TrSunSky::getEnvTexPostGamma()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.gamma;
	return NULL;
}

bool TrSunSky::getEnvTexPostInvert()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.invert;
	return NULL;
}

bool TrSunSky::getEnvTexPostInterpolateProbe()
{
	if (addressEnv)
		return ((EnvSphere*)addressEnv)->tex.texMod.interpolateProbe;
	return NULL;
}

//---------------------------------------   SET

void TrSunSky::setLongitude(float f)
{
	if (addressSunSky)
	{
		float o = getLatitude();
		((SunSky*)addressSunSky)->setPosition(f, o);
	}
}

void TrSunSky::setLatitude(float f)
{
	if (addressSunSky)
	{
		float o = getLongitude();
		((SunSky*)addressSunSky)->setPosition(o, f);
	}
}

void TrSunSky::setTurbidity(float f)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->turbidity = f;
}

void TrSunSky::setAerosol(float f)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->aerosol = f;
}

void TrSunSky::setMonth(int i)
{
	if (addressSunSky)
	{
		int d = getDay();
		int h = getHour();
		int n = getMinute();
		((SunSky*)addressSunSky)->setDate(i,d,h,(float)n);
	}
}

void TrSunSky::setDay(int i)
{
	if (addressSunSky)
	{
		int m = getMonth();
		int h = getHour();
		int n = getMinute();
		((SunSky*)addressSunSky)->setDate(m,i,h,(float)n);
	}
}

void TrSunSky::setHour(int i)
{
	if (addressSunSky)
	{
		int m = getMonth();
		int d = getDay();
		int n = getMinute();
		((SunSky*)addressSunSky)->setDate(m,d,i,(float)n);
	}
}

void TrSunSky::setMinute(int i)
{
	if (addressSunSky)
	{
		int m = getMonth();
		int d = getDay();
		int h = getHour();
		((SunSky*)addressSunSky)->setDate(m,d,h,(float)i);
	}
}

void TrSunSky::setGMT(int i)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->setStandardMeridianForTimeZone(i*PI/12);
}

void TrSunSky::setSunSkyON(bool b)
{
	sunskyOn = b;
}

void TrSunSky::setTargetHidden(bool b)
{
	targetHidden = b;
}

void TrSunSky::setSunON(bool b)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->sunOn = b;
}

void TrSunSky::setSunOtherLayerON(bool b)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->sunOtherBlendLayer = b;
}

void TrSunSky::setSunSize(float f)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->sunSize = f;
}

void TrSunSky::setAlbedo(float f)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->albedo = f;
}

void TrSunSky::setModel(int i)
{
	if (addressSunSky)
		((SunSky*)addressSunSky)->use_hosek = (i!=0);
}

void TrSunSky::setEnvEnabled(bool b)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->enabled = b;
}

void TrSunSky::setEnvPower(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->powerEV = f;
}

void TrSunSky::setEnvBlendLayer(int i)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->blendlayer = i;
}

void TrSunSky::setEnvAzimuth(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->azimuth_shift = f;
}

void TrSunSky::setEnvTexFilename(char * cp)
{
	Raytracer * rtr = Raytracer::getInstance();
	Scene * sc = rtr->curScenePtr;
	if (!addressEnv)
		return;
	EnvSphere * env = ((EnvSphere*)addressEnv);
	if (env->tex.fullfilename)
		free(env->tex.fullfilename);
	if (env->tex.filename)
		free(env->tex.filename);
	env->tex.fullfilename = NULL;
	env->tex.filename = NULL;

	int id = sc->texManager.addTexture(cp, true, NULL, false);
	env->tex.managerID = id;
	if (id>-1)
	{
		env->tex.fullfilename = copyString(sc->texManager.textures[id]->fullfilename);
		env->tex.filename = copyString(sc->texManager.textures[id]->filename);
	}
}

void TrSunSky::setEnvTexPostBrightness(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.brightness = f;
}

void TrSunSky::setEnvTexPostContrast(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.contrast = f;
}

void TrSunSky::setEnvTexPostSaturation(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.saturation = f;
}

void TrSunSky::setEnvTexPostHue(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.hue = f;
}

void TrSunSky::setEnvTexPostRed(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.red = f;
}

void TrSunSky::setEnvTexPostGreen(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.green = f;
}

void TrSunSky::setEnvTexPostBlue(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.blue = f;
}

void TrSunSky::setEnvTexPostGamma(float f)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.gamma = f;
}

void TrSunSky::setEnvTexPostInvert(bool b)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.invert = b;
}

void TrSunSky::setEnvTexPostInterpolateProbe(bool b)
{
	if (addressEnv)
		((EnvSphere*)addressEnv)->tex.texMod.interpolateProbe = b;
}

//---------------------------------------
