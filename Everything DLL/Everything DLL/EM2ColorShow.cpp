#include <windows.h>
#include <GdiPlus.h>
#include <stdio.h>
#include <math.h>
#include "EM2Controls.h"
#include "resource.h"
#include "log.h"

extern HMODULE hDllModule;

void InitEM2ColorShow()
{
    WNDCLASSEX wc;
    wc.cbSize         = sizeof(wc);
    wc.lpszClassName  = "EM2ColorShow";
    wc.hInstance      = hDllModule;
    wc.lpfnWndProc    = EM2ColorShowProc;
    wc.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wc.hIcon          = 0;
    wc.lpszMenuName   = 0;
    wc.hbrBackground  = (HBRUSH)0;
    wc.style          = 0;
    wc.cbClsExtra     = 0;
    wc.cbWndExtra     = sizeof(EM2ColorShow *);
    wc.hIconSm        = 0;
    RegisterClassEx(&wc);
}

EM2ColorShow * GetEM2ColorShowInstance(HWND hwnd)
{
	#ifdef _WIN64
		EM2ColorShow * emcs = (EM2ColorShow *)GetWindowLongPtr(hwnd, 0);
	#else
		EM2ColorShow * emcs = (EM2ColorShow *)(HANDLE)(LONG_PTR)GetWindowLong(hwnd, 0);
	#endif
    return emcs;
}

void SetEM2ColorShowInstance(HWND hwnd, EM2ColorShow *emcs)
{
	#ifdef _WIN64
		SetWindowLongPtr(hwnd, 0, (LONG_PTR)emcs);
	#else
		SetWindowLong(hwnd, 0, (LONG)(LONG_PTR)emcs);
	#endif
}

EM2ColorShow::EM2ColorShow(HWND hWnd)
{
	colBorder = RGB(12,12,12);
	colBorderDrag = RGB(255,0,0);
	colBorderClick = RGB(0,255,0);
	colBorderMouseOver = NGCOL_YELLOW;
	colShadow1 = RGB(0,0,0);
	colShadow2 = RGB(0,0,0);
	alphaShadow1 = 128;
	alphaShadow2 = 64;
	alphaBorder = 96;

	colDisabled = NGCOL_TEXT_DISABLED;
	alphaDisabled = 128;	// for black shadow

	showBorder = false;
	showShadow = true;
	allowDragSrc = false;
	allowDragDst = false;
	showMouseOverOrClick = true;
	isDragOver = false;
	isClicked = false;
	isMouseOver = false;
	useCursors = true;

	bgImage = 0;
	bgShiftX = 0;
	bgShiftY = 0;

	hwnd = hWnd;
	color = Color4(0,0,0);

	hImageCursor     = (HBITMAP)LoadImage(hDllModule, MAKEINTRESOURCE(IDB_CURSOR_COLOR), IMAGE_BITMAP, 0,0,  LR_CREATEDIBSECTION);
	hImageCursorMask = (HBITMAP)LoadImage(hDllModule, MAKEINTRESOURCE(IDB_CURSOR_COLOR_MASK), IMAGE_BITMAP, 0,0,  LR_CREATEDIBSECTION);
	createCursor(RGB(255,255,255));
}

EM2ColorShow::~EM2ColorShow()
{
}

LRESULT CALLBACK EM2ColorShowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EM2ColorShow* emcs;
	emcs = GetEM2ColorShowInstance(hwnd);

	RECT rect;

	switch (msg)
	{
		case WM_CREATE:
			{
				EM2ColorShow * emcs = new EM2ColorShow(hwnd);
				SetEM2ColorShowInstance(hwnd, emcs);
			}
			break;
		case WM_DESTROY:
			{
				delete emcs;
				SetEM2ColorShowInstance(hwnd, 0);
			}
			break;
		case WM_PAINT:
			{
				if (!emcs) 
					break;
				unsigned char cr = (unsigned char)min(255, max(0, (int)(emcs->color.r*255)));
				unsigned char cg = (unsigned char)min(255, max(0, (int)(emcs->color.g*255)));
				unsigned char cb = (unsigned char)min(255, max(0, (int)(emcs->color.b*255)));

				GetClientRect(hwnd, &rect);
				bool disabled = ((GetWindowLong(hwnd, GWL_STYLE) & WS_DISABLED) != 0);

				PAINTSTRUCT ps;
				HDC ohdc = BeginPaint(hwnd, &ps);


				HDC hdc = CreateCompatibleDC(ohdc);			// back buffer hdc
				HBITMAP backBMP = CreateCompatibleBitmap(ohdc, rect.right-rect.left, rect.bottom-rect.top);
				HBITMAP oldBMP = (HBITMAP)SelectObject(hdc, backBMP);

				if (emcs->bgImage)
				{
					HDC thdc = CreateCompatibleDC(hdc);			// image buffer hdc
					HBITMAP oldBitmap = (HBITMAP)SelectObject(thdc, emcs->bgImage);
					BitBlt(hdc, 0, 0, rect.right, rect.bottom, thdc, emcs->bgShiftX, emcs->bgShiftY, SRCCOPY);
					SelectObject(thdc, oldBitmap);
					DeleteDC(thdc);
				}
				else
				{
					HPEN hOldPen2 = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, NGCOL_TEXT_BACKGROUND));
					HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(NGCOL_TEXT_BACKGROUND));
					Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
					DeleteObject(SelectObject(hdc, hOldBrush));
					DeleteObject(SelectObject(hdc, hOldPen2));
				}

				#ifdef GUI2_USE_GDIPLUS
				{
					ULONG_PTR gdiplusToken;		// activate gdi+
					Gdiplus::GdiplusStartupInput gdiplusStartupInput;
					Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
					{
						Gdiplus::Graphics graphics(hdc);

						if (disabled)
						{
							Gdiplus::Pen bpen1(Gdiplus::Color(255, GetRValue(emcs->colDisabled), GetGValue(emcs->colDisabled), GetBValue(emcs->colDisabled)));
							Gdiplus::Pen bpen2(Gdiplus::Color(emcs->alphaDisabled, 0,0,0));
							Gdiplus::SolidBrush brush1(Gdiplus::Color(emcs->alphaDisabled, 0,0,0));
							int m = emcs->showShadow ? 1 : 0;
							int cx = rect.right/2 - m;
							int cy = rect.bottom/2 - m;
							graphics.DrawLine(&bpen1, cx-4, cy-4, cx+3, cy+3);
							graphics.DrawLine(&bpen1, cx-4, cy+3, cx+3, cy-4);
							if (emcs->showShadow)
							{
								graphics.DrawLine(&bpen2, cx-3, cy+4, cx+4, cy-3);
								graphics.FillRectangle(&brush1, cx+4, cy+4, 1, 1);
							}

						}
						else
						{
							Gdiplus::SolidBrush brush1(Gdiplus::Color(255, cr,cg,cb));
							Gdiplus::Pen bpens1(Gdiplus::Color(emcs->alphaShadow1, GetRValue(emcs->colShadow1), GetGValue(emcs->colShadow1), GetBValue(emcs->colShadow1)));
							Gdiplus::Pen bpens2(Gdiplus::Color(emcs->alphaShadow2, GetRValue(emcs->colShadow2), GetGValue(emcs->colShadow2), GetBValue(emcs->colShadow2)));
							Gdiplus::Pen bpens3(Gdiplus::Color(emcs->alphaBorder, GetRValue(emcs->colBorder), GetGValue(emcs->colBorder), GetBValue(emcs->colBorder)));

							if (emcs->showShadow)
							{
								graphics.FillRectangle(&brush1, 0, 0, rect.right-2, rect.bottom-2);
								graphics.DrawLine(&bpens1, Gdiplus::Point(1,rect.bottom-2), Gdiplus::Point(rect.right-3,rect.bottom-2));
								graphics.DrawLine(&bpens1, Gdiplus::Point(rect.right-2,1), Gdiplus::Point(rect.right-2,rect.bottom-2));
								graphics.DrawLine(&bpens2, Gdiplus::Point(2,rect.bottom-1), Gdiplus::Point(rect.right-2,rect.bottom-1));
								graphics.DrawLine(&bpens2, Gdiplus::Point(rect.right-1,2), Gdiplus::Point(rect.right-1,rect.bottom-1));
							}
							else
								if (emcs->showBorder)
								{
									graphics.DrawRectangle(&bpens3, 0,0, rect.right-1, rect.bottom-1);
									graphics.FillRectangle(&brush1, 1, 1, rect.right-2, rect.bottom-2);
								}
								else
									graphics.FillRectangle(&brush1, 0, 0, rect.right, rect.bottom);

							if ((emcs->isClicked && (emcs->allowDragSrc || emcs->showMouseOverOrClick))  ||  
									(emcs->isMouseOver && (emcs->allowDragSrc || emcs->showMouseOverOrClick))  ||  (emcs->isDragOver && emcs->allowDragDst))
							{
								COLORREF bCol;
								if (emcs->isDragOver  &&  emcs->allowDragDst)
									bCol = emcs->colBorderDrag;
								else
									if (emcs->isClicked  &&  (emcs->allowDragSrc || emcs->showMouseOverOrClick))
										bCol = emcs->colBorderClick;
									else
										if (emcs->isMouseOver  &&  (emcs->allowDragSrc || emcs->showMouseOverOrClick))
											bCol = emcs->colBorderMouseOver;
								Gdiplus::Pen bpenborder(Gdiplus::Color(255, GetRValue(bCol), GetGValue(bCol), GetBValue(bCol)));
								if (emcs->showShadow)
									graphics.DrawRectangle(&bpenborder, 0,0,rect.right-3, rect.bottom-3);
								else
									graphics.DrawRectangle(&bpenborder, 0,0,rect.right-1, rect.bottom-1);
							}
						}	// end no disabled

					}	// gdi+ variables destructors here
					Gdiplus::GdiplusShutdown(gdiplusToken);		// release gdi+
				}
				#else
					if (disabled)
					{
						int m = emcs->showShadow ? 1 : 0;
						int cx = rect.right/2 - m;
						int cy = rect.bottom/2 - m;
						HPEN oldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, emcs->colDisabled));
						MoveToEx(hdc, cx-4, cy-4, NULL);
						LineTo(hdc, cx+4, cy+4);
						MoveToEx(hdc, cx-4, cy+3, NULL);
						LineTo(hdc, cx+4, cy-5);
						if (emcs->showShadow)
						{
							COLORREF colsh = getMixedColor(NGCOL_BG_MEDIUM, emcs->alphaDisabled, RGB(0,0,0));
							DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, colsh)));
							MoveToEx(hdc, cx-3, cy+4, NULL);
							LineTo(hdc, cx+5, cy-4);
							SetPixel(hdc, cx+4, cy+4, colsh);
						}
						DeleteObject(SelectObject(hdc, oldPen));
					}
					else
					{

						HBRUSH hBrColor = CreateSolidBrush(RGB(cr,cg,cb));
						RECT colrect = { 0,0,rect.right-2, rect.bottom-2 };
						if (!emcs->showShadow)
							colrect = rect;
						FillRect(hdc, &colrect, hBrColor);
						DeleteObject(hBrColor);

						if (emcs->showShadow)
						{
							COLORREF mSh1Col = getMixedColor(NGCOL_BG_MEDIUM, emcs->alphaShadow1, emcs->colShadow1);
							COLORREF mSh2Col = getMixedColor(NGCOL_BG_MEDIUM, emcs->alphaShadow2, emcs->colShadow2);
							HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, mSh1Col));
							MoveToEx(hdc, 1, rect.bottom-2, NULL);
							LineTo(hdc, rect.right-2, rect.bottom-2);
							LineTo(hdc, rect.right-2, 0);
							DeleteObject(SelectObject(hdc, CreatePen(PS_SOLID, 1, mSh2Col)));
							MoveToEx(hdc, 2, rect.bottom-1, NULL);
							LineTo(hdc, rect.right-1, rect.bottom-1);
							LineTo(hdc, rect.right-1, 1);
							DeleteObject(SelectObject(hdc, hOldPen));
						}
						else 
							if (emcs->showBorder)
							{
								COLORREF bCol = getMixedColor(NGCOL_BG_MEDIUM, emcs->alphaBorder, emcs->colBorder);
								HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, bCol));
								HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
								if (emcs->showShadow)
									Rectangle(hdc, 0,0, rect.right-2, rect.bottom-2);
								else
									Rectangle(hdc, 0,0, rect.right, rect.bottom);
								DeleteObject(SelectObject(hdc, hOldBrush));
								DeleteObject(SelectObject(hdc, hOldPen));
							}

						if ((emcs->isClicked && (emcs->allowDragSrc || emcs->showMouseOverOrClick))  ||  
								(emcs->isMouseOver && (emcs->allowDragSrc || emcs->showMouseOverOrClick))  ||  (emcs->isDragOver && emcs->allowDragDst))
						//if (emcs->isClicked  ||  emcs->isMouseOver  ||  emcs->isDragOver)
						{
							COLORREF bCol;
							if (emcs->isDragOver  &&  emcs->allowDragDst)
								bCol = emcs->colBorderDrag;
							else
								if (emcs->isClicked  &&  (emcs->allowDragSrc || emcs->showMouseOverOrClick))
									bCol = emcs->colBorderClick;
								else
									if (emcs->isMouseOver  &&  (emcs->allowDragSrc || emcs->showMouseOverOrClick))
										bCol = emcs->colBorderMouseOver;
							HPEN hOldPen = (HPEN)SelectObject(hdc, CreatePen(PS_SOLID, 1, bCol));
							HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
							if (emcs->showShadow)
								Rectangle(hdc, 0,0, rect.right-2, rect.bottom-2);
							else
								Rectangle(hdc, 0,0, rect.right, rect.bottom);
							DeleteObject(SelectObject(hdc, hOldBrush));
							DeleteObject(SelectObject(hdc, hOldPen));
						}
					}
				#endif

				BitBlt(ohdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, hdc, 0, 0, SRCCOPY);

				SelectObject(hdc, oldBMP);
				DeleteObject(backBMP);
				DeleteDC(hdc); 

				EndPaint(hwnd, &ps);
			}
			break;
		case WM_LBUTTONDOWN:
			{
				emcs->isClicked = true;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case WM_LBUTTONUP:
			{
				InvalidateRect(hwnd, NULL, false);
				emcs->isClicked = false;
				GetClientRect(hwnd, &rect);
				SetCursor(LoadCursor(NULL, IDC_ARROW));
				if (GetCapture() == hwnd)
				{
					ReleaseCapture();
				
					POINT pos;
					pos.x = (short)LOWORD(lParam); 
					pos.y = (short)HIWORD(lParam); 
					if (PtInRect(&rect, pos))
					{
						emcs->isMouseOver = true; 
						SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), BN_CLICKED), (LPARAM)hwnd);
						break;
					}
					emcs->isMouseOver = false; 
					ClientToScreen(hwnd, &pos);

					if (!emcs->allowDragSrc)
						break;

					HWND hOther = WindowFromPoint(pos);
					if (!hOther)
						break;
					if (hOther == hwnd)
						break;

					char cname[128];
					int ok = GetClassName(hOther, cname, 128);
					if (!ok)
						break;

					if (strcmp(cname, "EM2ColorShow"))
						break;

					EM2ColorShow * emdst = GetEM2ColorShowInstance(hOther);
					emdst->dragNoOver();
					SendMessage(hOther, CS_YOUGOT_DRAG, (WPARAM)hwnd, (LPARAM)&emcs->color);
				}
			}
			break;
		case WM_MOUSEMOVE:
			{
				GetClientRect(hwnd, &rect);
				POINT pos = { (short)LOWORD(lParam), (short)HIWORD(lParam) };
				bool lbutton = ((wParam&MK_LBUTTON)!=0);
				if (PtInRect(&rect, pos))
				{
					if (!lbutton  &&  emcs->useCursors)
						SetCursor(LoadCursor (NULL, IDC_HAND));
					emcs->isMouseOver = true;
				}
				else
				{
					if (!lbutton  &&  emcs->useCursors)
						SetCursor(LoadCursor (NULL, IDC_ARROW));
					emcs->isMouseOver = false;
				}
				InvalidateRect(hwnd, NULL, false);

				if (GetCapture() == 0)
					SetCapture(hwnd);

				if (wParam & MK_LBUTTON)
				{
					if (GetCapture() == hwnd)
					{
						static HWND lastOther = 0;

						if (!emcs->allowDragSrc)
							break;

						if (GetCursor() != emcs->hCursor  &&  !emcs->isMouseOver)
						{
							unsigned char r,g,b;
							r = (unsigned char)min(255, max(0, 255*emcs->color.r));
							g = (unsigned char)min(255, max(0, 255*emcs->color.g));
							b = (unsigned char)min(255, max(0, 255*emcs->color.b));
							emcs->createCursor(RGB(r,g,b));
							HCURSOR oldCursor = SetCursor((HCURSOR)(emcs->hCursor));
							DeleteObject(oldCursor);
						}

						POINT pos;
						pos.x = (short)LOWORD(lParam); 
						pos.y = (short)HIWORD(lParam); 
						ClientToScreen(hwnd, &pos);
					
						HWND hOther = WindowFromPoint(pos);
						if (!hOther)
						{
							if (lastOther)
							{
								EM2ColorShow * emcslast = GetEM2ColorShowInstance(lastOther);
								if (emcslast)
									emcslast->dragNoOver();
							}
							lastOther = 0;
							break;
						}
						if (hOther == hwnd)
						{
							if (lastOther)
							{
								EM2ColorShow * emcslast = GetEM2ColorShowInstance(lastOther);
								if (emcslast)
									emcslast->dragNoOver();
							}
							lastOther = 0;
							break;
						}

						char cname[128];
						int ok = GetClassName(hOther, cname, 128);	
						if (!ok)
						{
							if (lastOther)
							{
								EM2ColorShow * emcslast = GetEM2ColorShowInstance(lastOther);
								if (emcslast)
									emcslast->dragNoOver();
							}
							lastOther = 0;
							break;
						}
						if (strcmp(cname, "EM2ColorShow"))
						{
							if (lastOther)
							{
								EM2ColorShow * emcslast = GetEM2ColorShowInstance(lastOther);
								if (emcslast)
									emcslast->dragNoOver();
							}
							lastOther = 0;
							break;
						}

						if (lastOther != hOther)
						{
							if (lastOther)
							{
								EM2ColorShow * emcslast = GetEM2ColorShowInstance(lastOther);
								if (emcslast)
									emcslast->dragNoOver();
							}
							lastOther = hOther;
							GetEM2ColorShowInstance(lastOther)->dragOver();
						}


					}

				}
				else
					if (!emcs->isMouseOver)
						ReleaseCapture();

			}
			break;
		case WM_CAPTURECHANGED:
			{
				emcs->isMouseOver = false;
				InvalidateRect(hwnd, NULL, false);
			}
			break;
		case CS_YOUGOT_DRAG:
			{
				if (!emcs->allowDragDst)
					break;
				HWND hSrc = (HWND)wParam;
				Color4 * c = (Color4 *)lParam;
				if (!c)
					break;
				emcs->redraw(*c);

				LONG wID;
				wID = GetWindowLong(hwnd, GWL_ID);
				SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(wID, CS_IGOT_DRAG), (LPARAM)hwnd);
			}
			break;
	}
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void EM2ColorShow::redraw(const Color4 &c)
{
	color = c;
	InvalidateRect(hwnd, NULL, false);
}

void EM2ColorShow::dragOver()
{
	if (allowDragDst)
		isDragOver = true;
	InvalidateRect(hwnd, NULL, false);
}

void EM2ColorShow::dragNoOver()
{
	isDragOver = false;
	InvalidateRect(hwnd, NULL, false);
}

void EM2ColorShow::setAllowedDragDrop()
{
	allowDragSrc = true;
	allowDragDst = true;
}

void EM2ColorShow::setDeniedDragDrop()
{
	allowDragSrc = false;
	allowDragDst = false;
}

bool EM2ColorShow::createCursor(COLORREF col)
{
	BITMAP bmp;
	GetObject(hImageCursor,sizeof(bmp), &bmp);
	if (!bmp.bmBits  ||  bmp.bmWidth!=32  ||  bmp.bmHeight!=32)
		return false;

	unsigned char newbuf[32*32*4];
	unsigned char r,g,b;
	r = GetRValue(col);
	g = GetGValue(col);
	b = GetBValue(col);

	for (int y=0; y<32; y++)
		for (int x=0; x<128; x++)
			newbuf[y*128+x] = ((unsigned char*)(bmp.bmBits))[(31-y)*128+x];
	for (int y=12; y<21; y++)
		for (int x=14; x<23; x++)
		{
			newbuf[y*128+x*4 + 0] = b;
			newbuf[y*128+x*4 + 1] = g;
			newbuf[y*128+x*4 + 2] = r;
		}

	HBITMAP tempbmp = CreateBitmap(
		32,32,1,32, newbuf);

	ICONINFO ii;
	ii.fIcon = FALSE;
	ii.xHotspot = 0;
	ii.yHotspot = 0;
	ii.hbmColor = tempbmp;
	ii.hbmMask = hImageCursorMask;
	HICON hicon = CreateIconIndirect(&ii);
	hCursor = hicon;

	DeleteObject(tempbmp);

	return true;
}
