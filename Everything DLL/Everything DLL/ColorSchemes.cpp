#include "ColorSchemes.h"
#include "EMControls.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "XML_IO.h"

//---------------------------------------------------------------------------------------------------------------

ThemeManager::ThemeManager()
{
	MaterialWindowIcons = NULL;

	WindowBackground = RGB(64,64,64);
	WindowText = RGB(223,223,223);
	DialogBackground = RGB(64,64,64);
	DialogBackgroundSecond = RGB(73,73,73);
	DialogText = RGB(223,223,223);

	EMButtonText = RGB(223,223,223);
	EMButtonBackGnd = RGB(96,96,96);
	EMButtonBackGndSelected = RGB(211,106,29);
	EMButtonBackGndClicked = RGB(112,112,112);
	EMButtonBorderLight = RGB(128,128,128);
	EMButtonBorderDark = RGB(48,48,48);
	EMButtonDisabledBackground = RGB(96,96,96);
	EMButtonDisabledBorderTopLeft = RGB(128,128,128);
	EMButtonDisabledBorderBottomRight = RGB(48,48,48);
	EMButtonDisabledText = RGB(160,160,160);

	EMImgButtonBorder = RGB(64,64,64);
	EMImgButtonBorderMouseOver = RGB(64,64,64);
	EMImgButtonBorderClick = RGB(64,64,64);
	EMImgButtonBorderDisabled = RGB(64,64,64);

	EMImgStateButtonBorder = RGB(64,64,64);
	EMImgStateButtonBorderSelected = RGB(64,64,64);

	EMPViewBorder = RGB(0,0,0);
	EMPViewBackground = RGB(16,16,16);

	EMComboBoxBorderTopLeft = RGB(112,112,112);
	EMComboBoxBorderBottomRight = RGB(80,80,80);
	EMComboBoxBorderTopLeftClicked = RGB(80,80,80);
	EMComboBoxBorderBottomRightClicked = RGB(112,112,112);
	EMComboBoxSelected = RGB(80,80,170);
	EMComboBoxMouseOver = RGB(96,96,187);
	EMComboBoxNormal = RGB(96,96,96);
	EMComboBoxText = RGB(223,223,223);
	EMComboBoxArrowNormal = RGB(0,0,0);
	EMComboBoxArrowPressed = RGB(255,255,255);
	EMComboBoxDisabledArrow = RGB(64,64,64);
	EMComboBoxDisabledBackground = RGB(96,96,96);
	EMComboBoxDisabledBorderTopLeft = RGB(112,112,112);
	EMComboBoxDisabledBorderBottomRight = RGB(80,80,80);
	EMComboBoxDisabledText = RGB(144,144,144);

	EMCheckBoxBorderLight = RGB(80,80,80);
	EMCheckBoxBorderDark  = RGB(48,48,48);
	EMCheckBoxBackground  = RGB(239,239,239);
	EMCheckBoxBackgroundDisabled  = RGB(160,160,160);
	EMCheckBoxXCol = RGB(16,16,16);
	EMCheckBoxXColDisabled = RGB(64,64,64);

	EMEditTrackBarBackground = RGB(64,64,64);
	EMEditTrackBarPathBorderUpLeft = RGB(32,32,32);
	EMEditTrackBarPathBorderDownRight = RGB(96,96,96);
	EMEditTrackBarPathBackgroundBefore = RGB(144,144,144);
	EMEditTrackBarPathBackgroundAfter = RGB(112,112,112);
	EMEditTrackBarSliderBorderUpLeft = RGB(112,112,112);
	EMEditTrackBarSliderBorderDownRight = RGB(32,32,32);
	EMEditTrackBarSliderBackground = RGB(64,64,64);
	EMEditTrackBarSliderBackgroundClicked = RGB(80,80,80);
	EMEditTrackBarEditBorderUpLeft = RGB(66,66,66);
	EMEditTrackBarEditBorderDownRight = RGB(66,66,66);
	EMEditTrackBarEditBackground = RGB(72,72,72);
	EMEditTrackBarEditText = RGB(223,223,223);
	EMEditTrackBarDisabledBorderLight = RGB(112,112,112);
	EMEditTrackBarDisabledBorderDark = RGB(32,32,32);
	EMEditTrackBarDisabledBackground = RGB(96,96,96);
	EMEditTrackBarDisabledEditBackground = RGB(72,72,72);
	EMEditTrackBarDisabledEditText = RGB(144,144,144);

	EMEditSpinBackground = RGB(64,64,64);
	EMEditSpinButtonBorderUpLeft = RGB(112,112,112);
	EMEditSpinButtonBorderDownRight = RGB(32,32,32);
	EMEditSpinButtonBackground = RGB(64,64,64);
	EMEditSpinButtonBackgroundClick = RGB(80,80,80);
	EMEditSpinArrow = RGB(0,0,0);
	EMEditSpinArrowClicked = RGB(255,255,255);
	EMEditSpinEditBorderUpLeft = RGB(66,66,66);
	EMEditSpinEditBorderDownRight = RGB(66,66,66);
	EMEditSpinEditBackground = RGB(72,72,72);
	EMEditSpinEditText = RGB(223,223,223);
	EMEditSpinDisabledBorderLight = RGB(112,112,112);
	EMEditSpinDisabledBorderDark = RGB(32,32,32);
	EMEditSpinDisabledBackground = RGB(96,96,96);
	EMEditSpinDisabledEditBackground = RGB(72,72,72);
	EMEditSpinDisabledEditText = RGB(144,144,144);
	EMEditSpinDisabledArrow = RGB(64,64,64);

	EMFresnelBorderLeftUp = RGB(32,32,32);
	EMFresnelBorderRightDown = RGB(96,96,96);
	EMFresnelBackgroundUp = RGB(80,80,80);
	EMFresnelBackgroundDown = RGB(48,48,48);
	EMFresnelPoint = RGB(0,128,255);
	EMFresnelPointOver = RGB(255,255,255);
	EMFresnelPointClicked = RGB(255,0,0);
	EMFresnelLine = RGB(255,255,128);

	EMColorPickerBorderLeftUp = RGB(32,32,32);
	EMColorPickerBorderRightDown = RGB(96,96,96);
	EMColorPickerArrow = RGB(223,223,223);
	EMColorPickerArrowClicked = RGB(255,0,0);
	EMColorPickerBackground = RGB(64,64,64);

	EMColorShowBorderLeftUp = RGB(32,32,32);
	EMColorShowBorderRightDown = RGB(96,96,96);
	EMColorShowDragBorderLeftUp = RGB(80,80,80);
	EMColorShowDragBorderRightDown = RGB(144,144,144);

	EMGroupBarText = RGB(223,223,223);
	EMGroupBarBackGnd = RGB(51,51,51);
	EMGroupBarBorderLight = RGB(112,112,112);
	EMGroupBarBorderDark = RGB(48,48,48);

	EMProgressBarBackGnd				= RGB(64,64,64);
	EMProgressBarBorderLight			= RGB(96,96,96);
	EMProgressBarBorderDark				= RGB(32,32,32);
	EMProgressBarPBackGnd				= RGB(80,80,80);
	EMProgressBarPBorderLight			= RGB(112,112,112);
	EMProgressBarPBorderDark			= RGB(32,32,32);
	EMProgressBarDisabledBackground		= RGB(96,96,96);
	EMProgressBarDisabledBorderLight	= RGB(112,112,112);
	EMProgressBarDisabledBorderDark		= RGB(32,32,32);
	EMProgressBarDisabledPBackground	= RGB(96,96,96);
	EMProgressBarDisabledPBorderLight	= RGB(112,112,112);
	EMProgressBarDisabledPBorderDark	= RGB(32,32,32);

	EMStatusBarText = RGB(223,223,223); 
	EMStatusBarBackGnd= RGB(64,64,64);
	EMStatusBarBorderDownRight = RGB(80,80,80);
	EMStatusBarBorderUpLeft = RGB(48,48,48);

    EMTabsText					= RGB(223,223,223);
    EMTabsBackGnd				= RGB(80,80,80);
	EMTabsSelText				= RGB(255,255,255);
	EMTabsSelBackGnd			= RGB(96,96,96);
	EMTabsDisText				= RGB(207,207,207);
	EMTabsDisBackGnd			= RGB(64,64,64);
	EMTabsDisBorderLight		= RGB(128,128,128);
	EMTabsDisBorderDark			= RGB(32,32,32);
	EMTabsDisBorderBottom		= RGB(32,32,32);
	EMTabsBorderLight			= RGB(128,128,128);
	EMTabsBorderDark			= RGB(48,48,48);
	EMTabsBorderBottom			= RGB(48,48,48);
	EMTabsSelBorderBottom		= RGB(96,96,96);
	EMTabsDisSelBorderBottom	= RGB(64,64,64);

	EMListViewHeaderBackground			= RGB(96,96,96);
	EMListViewHeaderBorderUpLeft		= RGB(128,128,128);
	EMListViewHeaderBorderDownRight		= RGB(64,64,64);
	EMListViewBackgroundOdd				= RGB(80,80,80);
	EMListViewBackgroundEven			= RGB(72,72,72);
	EMListViewBackgroundSelected		= RGB(128,160,160);
	EMListViewText						= RGB(223,223,223);
	EMListViewTextSelected				= RGB(255,255,255);
	EMListViewMainBorderUpLeft			= RGB(48,48,48);
	EMListViewMainBorderDownRight		= RGB(128,128,128);
	EMListViewTextMouseOver				= RGB(255,255,255);
	EMListViewBackgroundMouseOver		= RGB(96,96,96);
	EMListViewSBTrackBackground			= RGB(88,88,88);
	EMListViewSBBackground				= RGB(96,96,96);
	EMListViewSBBackgroundClicked		= RGB(112,112,112);
	EMListViewSBBorderUpLeft			= RGB(128,128,128);
	EMListViewSBBorderDownRight			= RGB(48,48,48);

	EMCurveBackGnd						= RGB(40,40,40);
	EMCurveDotLines						= RGB(80,80,80);
	EMCurveSelOuter						= RGB(255,255,255);
	EMCurveBorder						= RGB(0,0,0);
	EMCurveLineRed						= RGB(255,0,0);
	EMCurveLineGreen					= RGB(0,255,0);
	EMCurveLineBlue						= RGB(0,0,255);
	EMCurveLineLight					= RGB(223,223,223);

	EMLineBorderLeftUp					= RGB(48,48,48);
	EMLineBorderRightDown				= RGB(80,80,80);

	EMEditSimpleBorderUpLeft			= RGB(48,48,48);
	EMEditSimpleBorderDownRight			= RGB(112,112,112);
	EMEditSimpleBackground				= RGB(80,80,80);
	EMEditSimpleText					= RGB(224,224,224);
	EMEditSimpleDisabledBorderUpLeft	= RGB(64,64,64);
	EMEditSimpleDisabledBorderDownRight	= RGB(112,112,112);
	EMEditSimpleDisabledBackground		= RGB(96,96,96);
	EMEditSimpleDisabledText			= RGB(160,160,160);

	EMPagesLinkNormal		= RGB(144,144,144);
	EMPagesLinkCurrent		= RGB(223,223,223);
	EMPagesLinkMouseOver	= RGB(25,255,255);
	EMPagesNoLink			= RGB(160,160,160);
	EMPagesBackground		= RGB(64, 64, 64);

	EMScrollbarTrack = RGB(64,64,64);
	EMScrollbarTrackLight = RGB(96,96,96);
	EMScrollbarTrackDark = RGB(44,44,44);
	EMScrollbarSlider = RGB(64,64,64);
	EMScrollbarSliderMouseOver = RGB(74,74,74);
	EMScrollbarSliderClicked = RGB(88,88,88);
	EMScrollbarSliderLight = RGB(100,100,100);
	EMScrollbarSliderDark = RGB(44,44,44);
	EMScrollbarButton = RGB(64,64,64);
	EMScrollbarButtonMouseOver = RGB(74,74,74);
	EMScrollbarButtonClicked = RGB(88,88,88);
	EMScrollbarButtonLight = RGB(100,100,100);
	EMScrollbarButtonDark = RGB(44,44,44);
	EMScrollbarButtonArrow = RGB(160,160,160);
	EMScrollbarButtonArrowMouseOver = RGB(192,192,192);
	EMScrollbarButtonArrowClicked = RGB(224,224,224);
	EMScrollbarDisabledTrack = RGB(64,64,64);
	EMScrollbarDisabledTrackLight = RGB(72,72,72);
	EMScrollbarDisabledTrackDark = RGB(56,56,56);
	EMScrollbarDisabledSlider = RGB(64,64,64);
	EMScrollbarDisabledSliderLight = RGB(72,72,72);
	EMScrollbarDisabledSliderDark = RGB(56,56,56);
	EMScrollbarDisabledButton = RGB(64,64,64);
	EMScrollbarDisabledButtonLight = RGB(72,72,72);
	EMScrollbarDisabledButtonDark = RGB(56,56,56);
	EMScrollbarDisabledButtonArrow = RGB(48,48,48);

	EMImageListBg = RGB(64,64,64);
	EMImageListBgMouseOver = RGB(72,72,72);
	EMImageListBgSelected = RGB(80,80,80);
	EMImageListControlBorderLeftUp = RGB(48,48,48);
	EMImageListControlBorderRightDown = RGB(80,80,80);
	EMImageListImgBorderLeftUp = RGB(96,96,96);
	EMImageListImgBorderRightDown = RGB(96,96,96);
	EMImageListImgBorderSelectedLeftUp = RGB(112,112,112);
	EMImageListImgBorderSelectedRightDown = RGB(112,112,112);
	EMImageListText1 = RGB(224,224,224);
	EMImageListText2 = RGB(224,224,224);
	EMImageListText1Selected = RGB(255,255,255);
	EMImageListText2Selected = RGB(240,240,240);
	EMImageListDisabledBg = RGB(64,64,64);
	EMImageListDisabledControlBorderLeftUp = RGB(56,56,56);
	EMImageListDisabledControlBorderRightDown = RGB(72,72,72);
	EMImageListDisabledImgBorderLeftUp = RGB(72,72,72);
	EMImageListDisabledImgBorderRightDown = RGB(72,72,72);
	EMImageListDisabledText1 = RGB(160,160,160);
	EMImageListDisabledText2 = RGB(160,160,160);


}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::loadFromFile(char * filename)
{
	xmlNodePtr cur;

	xmlDoc = xmlParseFile(filename);
	if (!xmlDoc)	// not opened
		return false;

	cur = xmlDocGetRootElement(xmlDoc);
	if (cur == NULL) 
	{	// is empty
		xmlFreeDoc(xmlDoc);
		return false;
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "Theme")) 
	{	// bad root
		xmlFreeDoc(xmlDoc);
		return false;
	}


	cur = cur->xmlChildrenNode;
	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMButton")))
		{
			if (!parseEMButton(cur))
				return false;
		}


		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMImgButton")))
		{
			if (!parseEMImgButton(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMImgStateButton")))
		{
			if (!parseEMImgStateButton(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMPView")))
		{
			if (!parseEMPView(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMComboBox")))
		{
			if (!parseEMComboBox(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMCheckBox")))
		{
			if (!parseEMCheckBox(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMEditTrackBar")))
		{
			if (!parseEMEditTrackBar(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMEditSpin")))
		{
			if (!parseEMEditSpin(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMFresnel")))
		{
			if (!parseEMFresnel(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMColorPicker")))
		{
			if (!parseEMColorPicker(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMColorShow")))
		{
			if (!parseEMColorShow(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMGroupBar")))
		{
			if (!parseEMGroupBar(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMProgressBar")))
		{
			if (!parseEMProgressBar(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMStatusBar")))
		{
			if (!parseEMStatusBar(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMTabs")))
		{
			if (!parseEMTabs(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMListView")))
		{
			if (!parseEMListView(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMCurve")))
		{
			if (!parseEMCurve(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMTemperaturePicker")))
		{
			if (!parseEMTemperaturePicker(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMLine")))
		{
			if (!parseEMLine(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMEditSimple")))
		{
			if (!parseEMEditSimple(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMPages")))
		{
			if (!parseEMPages(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMScrollBar")))
		{
			if (!parseEMScrollbar(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EMImageList")))
		{
			if (!parseEMImagelist(cur))
				return false;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Window")))
		{
			if (!parseWindow(cur))
				return false;
		}

		// go to next
		cur = cur->next;
	}

	DialogBackground = WindowBackground;
	DialogText = WindowText;

	xmlFreeDoc(xmlDoc);

	return true;
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::freeStrings()
{
	if (MaterialWindowIcons)
		free(MaterialWindowIcons);
	MaterialWindowIcons = NULL;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::getColor(char * key, COLORREF &c)
{
	if (!key)
		return false;
	if (strlen(key) != 6)
		return false;

	unsigned char r,g,b;
	if (!DecHex::hex2ToUcharBigEndian((unsigned char *)key, r) )
		return false;
	if (!DecHex::hex2ToUcharBigEndian((unsigned char *)key+2, g) )
		return false;
	if (!DecHex::hex2ToUcharBigEndian((unsigned char *)key+4, b) )
		return false;

	c = RGB(r,g,b);
	//c = RGB(255,0,0);
	return true;
}

//-----------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMButton(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundSelected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonBackGndSelected = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonBackGndClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonBorderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonDisabledBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderTopLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonDisabledBorderTopLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderBottomRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonDisabledBorderBottomRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMButtonDisabledText = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMImgButton(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Border")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImgButtonBorder = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImgButtonBorderMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderClick")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImgButtonBorderClick = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderDisabled")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImgButtonBorderDisabled = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMImgStateButton(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Border")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImgStateButtonBorder = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderSelected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImgStateButtonBorderSelected = c;
		}


		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMPView(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Border")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMPViewBorder = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMPViewBackground = c;
		}


		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMComboBox(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderTopLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxBorderTopLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderBottomRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxBorderBottomRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderTopLeftClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxBorderTopLeftClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderBottomRightClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxBorderBottomRightClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Selected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxSelected = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"MouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Normal")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxNormal = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ArrowNormal")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxArrowNormal = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ArrowPressed")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxArrowPressed = c;
		}
///////

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledArrow")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxDisabledArrow = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxDisabledBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderTopLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxDisabledBorderTopLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderBottomRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxDisabledBorderBottomRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMComboBoxDisabledText = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMCheckBox(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCheckBoxBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCheckBoxBorderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCheckBoxBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"XCol")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCheckBoxXCol = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundDisabled")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCheckBoxBackgroundDisabled = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"XColDisabled")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCheckBoxXColDisabled = c;
		}


		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMEditTrackBar(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"PathBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarPathBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"PathBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarPathBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"PathBackgroundBefore")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarPathBackgroundBefore = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"PathBackgroundAfter")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarPathBackgroundAfter = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarSliderBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarSliderBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarSliderBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderBackgroundClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarSliderBackgroundClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarEditBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarEditBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarEditBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarEditText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarDisabledBorderLight = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarDisabledBorderDark = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarDisabledBackground = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledEditBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarDisabledEditBackground = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledEditText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditTrackBarDisabledEditText = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMEditSpin(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinButtonBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinButtonBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinButtonBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonBackgroundClick")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinButtonBackgroundClick = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Arrow")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinArrow = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ArrowClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinArrowClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinEditBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinEditBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinEditBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"EditText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinEditText = c;
		}
		//////

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinDisabledBorderLight = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinDisabledBorderDark = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinDisabledBackground = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledEditBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinDisabledEditBackground = c;
		}
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledEditText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinDisabledEditText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledArrow")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSpinDisabledArrow = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMFresnel(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelBackgroundUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelBackgroundDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Point")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelPoint = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"PointOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelPointOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"PointClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelPointClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Line")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMFresnelLine = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMColorPicker(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorPickerBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorPickerBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorPickerBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Arrow")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorPickerArrow = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ArrowClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorPickerArrowClicked = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMColorShow(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorShowBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorShowBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DragBorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorShowDragBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DragBorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMColorShowDragBorderRightDown = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMGroupBar(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMGroupBarText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMGroupBarBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMGroupBarBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMGroupBarBorderDark = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMProgressBar(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarBorderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ProgressBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarPBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ProgressBorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarPBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ProgressBorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarPBorderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarDisabledBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarDisabledBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarDisabledBorderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledProgressBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarDisabledPBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledProgressBorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarDisabledPBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledProgressBorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMProgressBarDisabledPBorderDark = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMStatusBar(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMStatusBarBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMStatusBarText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMStatusBarBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMStatusBarBorderDownRight = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMTabs(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SelectedText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsSelText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SelectedBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsSelBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsDisText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsDisBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsDisBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsDisBorderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderBottom")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsDisBorderBottom = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsBorderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsBorderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderBottom")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsBorderBottom = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SelectedBorderBottom")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsSelBorderBottom = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SelectedDisabledBorderBottom")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTabsDisSelBorderBottom = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMListView(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"MainBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewMainBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"MainBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewMainBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"HeaderBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewHeaderBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"HeaderBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewHeaderBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"HeaderBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewHeaderBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundOdd")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewBackgroundOdd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundEven")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewBackgroundEven = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundSelected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewBackgroundSelected = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"TextSelected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewTextSelected = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"TextMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewTextMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BackgroundMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewBackgroundMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ScrollBarTrackBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewSBTrackBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ScrollBarBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewSBBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ScrollBarBackgroundClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewSBBackgroundClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ScrollBarBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewSBBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ScrollBarBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMListViewSBBorderDownRight = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMCurve(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveBackGnd = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DotLines")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveDotLines = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SelOuter")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveSelOuter = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Border")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveBorder = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"LineRed")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveLineRed = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"LineGreen")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveLineGreen = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"LineBlue")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveLineBlue = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"LineLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMCurveLineLight = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMTemperaturePicker(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTemperaturePickerBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTemperaturePickerBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Slider")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTemperaturePickerSlider = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMTemperaturePickerText = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMLine(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMLineBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMLineBorderRightDown = c;
		}


		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMEditSimple(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderUpLeft")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleDisabledBorderUpLeft = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBorderDownRight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleDisabledBorderDownRight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleDisabledBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMEditSimpleDisabledText = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMPages(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"LinkNormal")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMPagesLinkNormal = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"LinkCurrent")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMPagesLinkCurrent = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"LinkMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMPagesLinkMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"NoLink")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMPagesNoLink = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMPagesBackground = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMScrollbar(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Track")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarTrack = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"TrackLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarTrackLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"TrackDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarTrackDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Slider")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarSlider = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarSliderMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarSliderClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarSliderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"SliderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarSliderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Button")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButton = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButtonMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButtonClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButtonLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButtonDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonArrow")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButtonArrow = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonArrowMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButtonArrowMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ButtonArrowClicked")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarButtonArrowClicked = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledTrack")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledTrack = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledTrackLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledTrackLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledTrackDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledTrackDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledSlider")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledSlider = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledSliderLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledSliderLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledSliderDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledSliderDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledButton")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledButton = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledButtonLight")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledButtonLight = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledButtonDark")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledButtonDark = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledButtonArrow")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMScrollbarDisabledButtonArrow = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseEMImagelist(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Bg")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListBg = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BgMouseOver")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListBgMouseOver = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"BgSelected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListBgSelected = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ControlBorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListControlBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ControlBorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListControlBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ImgBorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListImgBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ImgBorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListImgBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ImgBorderSelectedLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListImgBorderSelectedLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"ImgBorderSelectedRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListImgBorderSelectedRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text1")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListText1 = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text2")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListText2 = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text1Selected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListText1Selected = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text2Selected")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListText2Selected = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledBg")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListDisabledBg = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledControlBorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListDisabledControlBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledControlBorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListDisabledControlBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledImgBorderLeftUp")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListDisabledImgBorderLeftUp = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledImgBorderRightDown")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListDisabledImgBorderRightDown = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledText1")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListDisabledText1 = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DisabledText2")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			EMImageListDisabledText2 = c;
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------

bool ThemeManager::parseWindow(xmlNodePtr curNode)
{
	xmlNodePtr cur;
	cur = curNode->xmlChildrenNode;
	COLORREF c;
	char * s;

	while (cur != NULL) 
	{
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Background")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			WindowBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DialogBackground")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			DialogBackground = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DialogBackgroundSecond")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			DialogBackgroundSecond = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"Text")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			WindowText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"DialogText")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!getColor(s, c))
				return false;
			DialogText = c;
		}

		if ((!xmlStrcmp(cur->name, (const xmlChar *)"MaterialWindowIcons")))
		{
			s = (char *)xmlNodeListGetString(xmlDoc, cur->xmlChildrenNode, 1);
			if (!s)
				continue;
			if (strlen(s) < 1)
				continue;

			size_t d = strlen(s);
			if (MaterialWindowIcons)
				free(MaterialWindowIcons);
			MaterialWindowIcons = (char *)malloc(d+2);
			sprintf_s(MaterialWindowIcons, d+2, "%s", s);
		}

		cur = cur->next;
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMButton * emb)
{
	if (!emb)
		return;

	emb->colText = EMButtonText;
	emb->colBackGnd = EMButtonBackGnd;
	emb->colBackGndSelected = EMButtonBackGndSelected;
	emb->colBackGndClicked = EMButtonBackGndClicked;
	emb->colBorderLight = EMButtonBorderLight;
	emb->colBorderDark = EMButtonBorderDark;
	emb->colDisabledBackground = EMButtonDisabledBackground;
	emb->colDisabledBorderTopLeft = EMButtonDisabledBorderTopLeft;
	emb->colDisabledBorderBottomRight = EMButtonDisabledBorderBottomRight;
	emb->colDisabledText = EMButtonDisabledText;

	InvalidateRect(emb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMImgButton * emib)
{
	if (!emib)
		return;

	emib->colBorder = EMImgButtonBorder;
	emib->colBorderMouseOver = EMImgButtonBorderMouseOver;
	emib->colBorderClick = EMImgButtonBorderClick;
	emib->colBorderDisabled = EMImgButtonBorderDisabled;

	InvalidateRect(emib->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMImgStateButton * emisb)
{
	if (!emisb)
		return;

	emisb->colBorder = EMImgStateButtonBorder;
	emisb->colBorderSelected = EMImgStateButtonBorderSelected;

	InvalidateRect(emisb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMPView * empv)
{
	if (!empv)
		return;

	empv->colBorder = EMPViewBorder;
	empv->colBackground = EMPViewBackground;

	InvalidateRect(empv->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMComboBox * emcb)
{
	if (!emcb)
		return;

	emcb->colBorderTopLeft = EMComboBoxBorderTopLeft;
	emcb->colBorderBottomRight = EMComboBoxBorderBottomRight;
	emcb->colBorderTopLeftClicked = EMComboBoxBorderTopLeftClicked;
	emcb->colBorderBottomRightClicked = EMComboBoxBorderBottomRightClicked;
	emcb->colSelected = EMComboBoxSelected;
	emcb->colMouseOver = EMComboBoxMouseOver;
	emcb->colNormal = EMComboBoxNormal;
	emcb->colText = EMComboBoxText;
	emcb->colArrowNormal = EMComboBoxArrowNormal;
	emcb->colArrowPressed = EMComboBoxArrowPressed;
	emcb->colDisabledArrow = EMComboBoxDisabledArrow;
	emcb->colDisabledBackground = EMComboBoxDisabledBackground;
	emcb->colDisabledBorderTopLeft = EMComboBoxDisabledBorderTopLeft;
	emcb->colDisabledBorderBottomRight = EMComboBoxDisabledBorderBottomRight;
	emcb->colDisabledText = EMComboBoxDisabledText;
	
	InvalidateRect(emcb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMCheckBox * emcb)
{
	if (!emcb)
		return;

	emcb->colBorderLight = EMCheckBoxBorderLight;
	emcb->colBorderDark = EMCheckBoxBorderDark;
	emcb->colBackground = EMCheckBoxBackground;
	emcb->colX = EMCheckBoxXCol;
	emcb->colBackgroundDisabled = EMCheckBoxBackgroundDisabled;
	emcb->colXDisabled = EMCheckBoxXColDisabled;

	InvalidateRect(emcb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMEditSpin * emes)
{
	if (!emes)
		return;

	emes->colBackground = EMEditSpinBackground;
	emes->colButtonBorderUpLeft = EMEditSpinButtonBorderUpLeft;
	emes->colButtonBorderDownRight = EMEditSpinButtonBorderDownRight;
	emes->colButtonBackground = EMEditSpinButtonBackground;
	emes->colButtonBackgroundClick = EMEditSpinButtonBackgroundClick;
	emes->colArrow = EMEditSpinArrow;
	emes->colArrowClicked = EMEditSpinArrowClicked;
	emes->colEditBorderUpLeft = EMEditSpinEditBorderUpLeft;
	emes->colEditBorderDownRight = EMEditSpinEditBorderDownRight;
	emes->colEditBackground = EMEditSpinEditBackground;
	emes->colEditText = EMEditSpinEditText;
	emes->colDisabledBorderLight = EMEditSpinDisabledBorderLight;
	emes->colDisabledBorderDark = EMEditSpinDisabledBorderDark;
	emes->colDisabledBackground = EMEditSpinDisabledBackground;
	emes->colDisabledEditBackground = EMEditSpinDisabledEditBackground;
	emes->colDisabledEditText = EMEditSpinDisabledEditText;
	emes->colDisabledArrow = EMEditSpinDisabledArrow;

	if (emes->hEditBrush)
		DeleteObject(emes->hEditBrush);
	emes->hEditBrush = CreateSolidBrush(emes->colEditBackground);
	if (emes->hEditDisabledBrush)
		DeleteObject(emes->hEditDisabledBrush);
	emes->hEditDisabledBrush = CreateSolidBrush(emes->colDisabledEditBackground);

	InvalidateRect(emes->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMEditTrackBar * emetb)
{
	if (!emetb)
		return;

	emetb->colBackground = EMEditTrackBarBackground;
	emetb->colPathBorderUpLeft = EMEditTrackBarPathBorderUpLeft;
	emetb->colPathBorderDownRight = EMEditTrackBarPathBorderDownRight;
	emetb->colPathBackgroundBefore = EMEditTrackBarPathBackgroundBefore;
	emetb->colPathBackgroundAfter = EMEditTrackBarPathBackgroundAfter;
	emetb->colSliderBorderUpLeft = EMEditTrackBarSliderBorderUpLeft;
	emetb->colSliderBorderDownRight = EMEditTrackBarSliderBorderDownRight;
	emetb->colSliderBackground = EMEditTrackBarSliderBackground;
	emetb->colSliderBackgroundClicked = EMEditTrackBarSliderBackgroundClicked;
	emetb->colEditBorderUpLeft = EMEditTrackBarEditBorderUpLeft;
	emetb->colEditBorderDownRight = EMEditTrackBarEditBorderDownRight;
	emetb->colEditBackground = EMEditTrackBarEditBackground;
	emetb->colEditText = EMEditTrackBarEditText;
	emetb->colDisabledBorderLight = EMEditTrackBarDisabledBorderLight;
	emetb->colDisabledBorderDark = EMEditTrackBarDisabledBorderDark;
	emetb->colDisabledBackground = EMEditTrackBarDisabledBackground;
	emetb->colDisabledEditBackground = EMEditTrackBarDisabledEditBackground;
	emetb->colDisabledEditText = EMEditTrackBarDisabledEditText;

	if (emetb->hEditBrush)
		DeleteObject(emetb->hEditBrush);
	emetb->hEditBrush = CreateSolidBrush(emetb->colEditBackground);
	if (emetb->hEditDisabledBrush)
		DeleteObject(emetb->hEditDisabledBrush);
	emetb->hEditDisabledBrush = CreateSolidBrush(emetb->colDisabledEditBackground);

	InvalidateRect(emetb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMFresnel * emf)
{
	if (!emf)
		return;

	emf->colBorderLeftUp = EMFresnelBorderLeftUp;
	emf->colBorderRightDown = EMFresnelBorderRightDown;
	emf->colBackgroundUp = EMFresnelBackgroundUp;
	emf->colBackgroundDown = EMFresnelBackgroundDown;
	emf->colPoint = EMFresnelPoint;
	emf->colPointOver = EMFresnelPointOver;
	emf->colPointClicked = EMFresnelPointClicked;
	emf->colLine = EMFresnelLine;

	InvalidateRect(emf->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMColorPicker * emcp)
{
	if (!emcp)
		return;

	emcp->colBorderLeftUp = EMColorPickerBorderLeftUp;
	emcp->colBorderRightDown = EMColorPickerBorderRightDown;
	emcp->colBackground = EMColorPickerBackground;
	emcp->colArrow = EMColorPickerArrow;
	emcp->colArrowClicked = EMColorPickerArrowClicked;

	InvalidateRect(emcp->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMColorShow * emcs)
{
	if (!emcs)
		return;

	emcs->colBorderLeftUp = EMColorShowBorderLeftUp;
	emcs->colBorderRightDown = EMColorShowBorderRightDown;
	emcs->colDragBorderLeftUp = EMColorShowDragBorderLeftUp;
	emcs->colDragBorderRightDown = EMColorShowDragBorderRightDown;
	if (emcs->dialogBrush)
		DeleteObject(emcs->dialogBrush);
	emcs->dialogBrush = CreateSolidBrush(DialogBackground);

	InvalidateRect(emcs->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------
void ThemeManager::apply(EMGroupBar * emgb)
{
	if (!emgb)
		return;

    emgb->colText = EMGroupBarText;
    emgb->colBackGnd = EMGroupBarBackGnd;
	emgb->colBorderLight = EMGroupBarBorderLight;
	emgb->colBorderDark = EMGroupBarBorderDark;

	InvalidateRect(emgb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMProgressBar * empb)
{
	if (!empb)
		return;

	empb->colBackGnd =					EMProgressBarBackGnd;
	empb->colBorderLight =				EMProgressBarBorderLight;
	empb->colBorderDark =				EMProgressBarBorderDark;
	empb->colPBackGnd =					EMProgressBarPBackGnd;
	empb->colPBorderLight =				EMProgressBarPBorderLight;
	empb->colPBorderDark =				EMProgressBarPBorderDark;
	empb->colDisabledBackground =		EMProgressBarDisabledBackground;
	empb->colDisabledBorderLight =		EMProgressBarDisabledBorderLight;
	empb->colDisabledBorderDark =		EMProgressBarDisabledBorderDark;
	empb->colDisabledPBackground =		EMProgressBarDisabledPBackground;
	empb->colDisabledPBorderLight =		EMProgressBarDisabledPBorderLight;
	empb->colDisabledPBorderDark =		EMProgressBarDisabledPBorderDark;

	InvalidateRect(empb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMStatusBar * emsb)
{
	if (!emsb)
		return;

	emsb->colText				= EMStatusBarText;
    emsb->colBackGnd			= EMStatusBarBackGnd;
	emsb->colBorderUpLeft		= EMStatusBarBorderUpLeft;
	emsb->colBorderDownRight	= EMStatusBarBorderDownRight;

	InvalidateRect(emsb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMTabs * emt)
{
	if (!emt)
		return;

	emt->colText				= EMTabsText;
	emt->colBackGnd				= EMTabsBackGnd;
	emt->colSelText				= EMTabsSelText;
	emt->colSelBackGnd			= EMTabsSelBackGnd;
	emt->colDisText				= EMTabsDisText;
	emt->colDisBackGnd			= EMTabsDisBackGnd;
	emt->colDisBorderLight		= EMTabsDisBorderLight;
	emt->colDisBorderDark		= EMTabsDisBorderDark;
	emt->colDisBorderBottom		= EMTabsDisBorderBottom;
	emt->colBorderLight			= EMTabsBorderLight;
	emt->colBorderDark			= EMTabsBorderDark;
	emt->colBorderBottom		= EMTabsBorderBottom;
	emt->colSelBorderBottom		= EMTabsSelBorderBottom;
	emt->colDisSelBorderBottom	= EMTabsDisSelBorderBottom;

	InvalidateRect(emt->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMListView * emlv)
{
	if (!emlv)
		return;

	emlv->colHeaderBackground		= EMListViewHeaderBackground;
	emlv->colHeaderBorderUpLeft		= EMListViewHeaderBorderUpLeft;
	emlv->colHeaderBorderDownRight	= EMListViewHeaderBorderDownRight;
	emlv->colBackgroundOdd			= EMListViewBackgroundOdd;
	emlv->colBackgroundEven			= EMListViewBackgroundEven;
	emlv->colBackgroundSelected		= EMListViewBackgroundSelected;
	emlv->colText					= EMListViewText;	
	emlv->colTextSelected			= EMListViewTextSelected;	
	emlv->colMainBorderUpLeft		= EMListViewMainBorderUpLeft;	
	emlv->colMainBorderDownRight	= EMListViewMainBorderDownRight;	
	emlv->colTextMouseOver			= EMListViewTextMouseOver;
	emlv->colBackgroundMouseOver	= EMListViewBackgroundMouseOver;	
	emlv->colSBTrackBackground		= EMListViewSBTrackBackground;
	emlv->colSBBackground			= EMListViewSBBackground;	
	emlv->colSBBackgroundClicked	= EMListViewSBBackgroundClicked;	
	emlv->colSBBorderUpLeft			= EMListViewSBBorderUpLeft;
	emlv->colSBBorderDownRight		= EMListViewSBBorderDownRight;

	InvalidateRect(emlv->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMText * emt, bool isDialog, bool secondColor)
{
	if (!emt)
		return;

	if (isDialog)
	{
		emt->colText = DialogText;
		emt->colDisabledText= DialogText;
		if (secondColor)
			emt->colBackGnd = DialogBackgroundSecond;
		else
			emt->colBackGnd = DialogBackground;
	}
	else
	{
		emt->colText = WindowText;
		emt->colDisabledText= WindowText;
		if (secondColor)
			emt->colBackGnd = DialogBackgroundSecond;
		else
			emt->colBackGnd = WindowBackground;
	}

	InvalidateRect(emt->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMCurve * emc)
{
	if (!emc)
		return;
	
	emc->colBackGnd		= EMCurveBackGnd;
	emc->colDotLines	= EMCurveDotLines;
	emc->colSelOuter	= EMCurveSelOuter;
	emc->colBorder		= EMCurveBorder;
	emc->colLineRed		= EMCurveLineRed;
	emc->colLineGreen	= EMCurveLineGreen;
	emc->colLineBlue	= EMCurveLineBlue;
	emc->colLineLight	= EMCurveLineLight;

	InvalidateRect(emc->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMTemperaturePicker * emtp)
{
	if (!emtp)
		return;

	emtp->colBorderLeftUp = EMTemperaturePickerBorderLeftUp;
	emtp->colBorderRightDown = EMTemperaturePickerBorderRightDown;
	emtp->colSlider = EMTemperaturePickerSlider;
	emtp->colText = EMTemperaturePickerText;

	InvalidateRect(emtp->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMLine * eml)
{
	if (!eml)
		return;

	eml->colBorderLeftUp = EMLineBorderLeftUp;
	eml->colBorderRightDown = EMLineBorderRightDown;

	InvalidateRect(eml->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMEditSimple * emes)
{
	if (!emes)
		return;

	emes->colBorderUpLeft =				EMEditSimpleBorderUpLeft;
	emes->colBorderDownRight =			EMEditSimpleBorderDownRight;
	emes->colBackground =				EMEditSimpleBackground;
	emes->colText =						EMEditSimpleText;
	emes->colDisabledBorderUpLeft =		EMEditSimpleDisabledBorderUpLeft;
	emes->colDisabledBorderDownRight =	EMEditSimpleDisabledBorderDownRight;
	emes->colDisabledBackground =		EMEditSimpleDisabledBackground;
	emes->colDisabledText =				EMEditSimpleDisabledText;

	InvalidateRect(emes->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMPages * emp)
{
	if (!emp)
		return;

	emp->colLinkNormal		 = EMPagesLinkNormal;
	emp->colLinkCurrent		 = EMPagesLinkCurrent;
	emp->colLinkMouseOver	 = EMPagesLinkMouseOver;
	emp->colNoLink			 = EMPagesNoLink;
	emp->colBackground		 = EMPagesBackground;

	InvalidateRect(emp->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMScrollBar * emsb)
{
	if (!emsb)
		return;

	emsb->colTrack					= EMScrollbarTrack;
	emsb->colTrackLight				= EMScrollbarTrackLight;
	emsb->colTrackDark				= EMScrollbarTrackDark;
	emsb->colSlider					= EMScrollbarSlider;
	emsb->colSliderMouseOver		= EMScrollbarSliderMouseOver;
	emsb->colSliderClicked			= EMScrollbarSliderClicked;
	emsb->colSliderLight			= EMScrollbarSliderLight;
	emsb->colSliderDark				= EMScrollbarSliderDark;
	emsb->colButton					= EMScrollbarButton;
	emsb->colButtonMouseOver		= EMScrollbarButtonMouseOver;
	emsb->colButtonClicked			= EMScrollbarButtonClicked;
	emsb->colButtonLight			= EMScrollbarButtonLight;
	emsb->colButtonDark				= EMScrollbarButtonDark;
	emsb->colButtonArrow			= EMScrollbarButtonArrow;
	emsb->colButtonArrowMouseOver	= EMScrollbarButtonArrowMouseOver;
	emsb->colButtonArrowClicked		= EMScrollbarButtonArrowClicked;
	emsb->colDisabledTrack			= EMScrollbarDisabledTrack;
	emsb->colDisabledTrackLight		= EMScrollbarDisabledTrackLight;
	emsb->colDisabledTrackDark		= EMScrollbarDisabledTrackDark;
	emsb->colDisabledSlider			= EMScrollbarDisabledSlider;
	emsb->colDisabledSliderLight	= EMScrollbarDisabledSliderLight;
	emsb->colDisabledSliderDark		= EMScrollbarDisabledSliderDark;
	emsb->colDisabledButton			= EMScrollbarDisabledButton;
	emsb->colDisabledButtonLight	= EMScrollbarDisabledButtonLight; 
	emsb->colDisabledButtonDark		= EMScrollbarDisabledButtonDark; 
	emsb->colDisabledButtonArrow	= EMScrollbarDisabledButtonArrow; 

	InvalidateRect(emsb->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

void ThemeManager::apply(EMImageList * emil)
{
	if (!emil)
		return;

	emil->colBg									 = EMImageListBg;
	emil->colBgMouseOver						 = EMImageListBgMouseOver;
	emil->colBgSelected							 = EMImageListBgSelected;
	emil->colControlBorderLeftUp				 = EMImageListControlBorderLeftUp;
	emil->colControlBorderRightDown				 = EMImageListControlBorderRightDown;
	emil->colImgBorderLeftUp					 = EMImageListImgBorderLeftUp;
	emil->colImgBorderRightDown					 = EMImageListImgBorderRightDown;
	emil->colImgBorderSelectedLeftUp			 = EMImageListImgBorderSelectedLeftUp;
	emil->colImgBorderSelectedRightDown			 = EMImageListImgBorderSelectedRightDown;
	emil->colText1								 = EMImageListText1;
	emil->colText2								 = EMImageListText2;
	emil->colText1Selected						 = EMImageListText1Selected;
	emil->colText2Selected						 = EMImageListText2Selected;
	emil->colDisabledBg							 = EMImageListDisabledBg;
	emil->colDisabledControlBorderLeftUp		 = EMImageListDisabledControlBorderLeftUp;
	emil->colDisabledControlBorderRightDown		 = EMImageListDisabledControlBorderRightDown;
	emil->colDisabledImgBorderLeftUp			 = EMImageListDisabledImgBorderLeftUp;
	emil->colDisabledImgBorderRightDown			 = EMImageListDisabledImgBorderRightDown;
	emil->colDisabledText1						 = EMImageListDisabledText1;
	emil->colDisabledText2						 = EMImageListDisabledText2;

	InvalidateRect(emil->hwnd, NULL, false);
}

//---------------------------------------------------------------------------------------------------------------

