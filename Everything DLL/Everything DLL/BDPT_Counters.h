#ifndef __bdpt__counters__h__
#define __bdpt__counters__h__

//#define BDPT_USE_COUNTERS

time_t bdpt_time_start;
time_t bdpt_time_stop;
time_t bdpt_eye_creation_time_start;
time_t bdpt_eye_creation_time_total;
time_t bdpt_emitter_creation_time_start;
time_t bdpt_emitter_creation_time_total;
time_t bdpt_weight_eval_time_start;
time_t bdpt_weight_eval_time_total;
time_t bdpt_shadow_rays_time_start;
time_t bdpt_shadow_rays_time_total;
time_t bdpt_multilayers_time_start;
time_t bdpt_multilayers_time_total;


//time_stop = clock();


#ifdef BDPT_USE_COUNTERS
	#define CNT_EYE_CREATE_START bdpt_eye_creation_time_start = clock()
	#define CNT_EYE_CREATE_STOP bdpt_eye_creation_time_total += clock() - bdpt_eye_creation_time_start
	#define CNT_EMITTER_CREATE_START bdpt_emitter_creation_time_start = clock()
	#define CNT_EMITTER_CREATE_STOP bdpt_emitter_creation_time_total += clock() - bdpt_emitter_creation_time_start
	#define CNT_WEIGHT_EVAL_START bdpt_weight_eval_time_start = clock()
	#define CNT_WEIGHT_EVAL_STOP bdpt_weight_eval_time_total += clock() - bdpt_weight_eval_time_start
	#define CNT_SHADOW_RAYS_START bdpt_shadow_rays_time_start = clock()
	#define CNT_SHADOW_RAYS_STOP bdpt_shadow_rays_time_total += clock() - bdpt_shadow_rays_time_start
	#define CNT_MULTILAYERS_START bdpt_multilayers_time_start = clock()
	#define CNT_MULTILAYERS_STOP bdpt_multilayers_time_total += clock() - bdpt_multilayers_time_start

#else
	#define CNT_EYE_CREATE_START
	#define CNT_EYE_CREATE_STOP
	#define CNT_EMITTER_CREATE_START
	#define CNT_EMITTER_CREATE_STOP
	#define CNT_WEIGHT_EVAL_START
	#define CNT_WEIGHT_EVAL_STOP
	#define CNT_SHADOW_RAYS_START
	#define CNT_SHADOW_RAYS_STOP
	#define CNT_MULTILAYERS_START
	#define CNT_MULTILAYERS_STOP

#endif


void initBDPTCounters()
{
	#ifdef BDPT_USE_COUNTERS
		bdpt_time_start = clock();
		bdpt_eye_creation_time_total = 0;
		bdpt_emitter_creation_time_total = 0;
		bdpt_weight_eval_time_total = 0;
		bdpt_shadow_rays_time_total = 0;
		bdpt_multilayers_time_total = 0;
	#endif
}

void logBDPTCounters()
{
	#ifdef BDPT_USE_COUNTERS
		bdpt_time_stop = clock();
		char lcbuf[512];
		sprintf_s(lcbuf, 512, "Bdpt rendering took %d ms", (bdpt_time_stop-bdpt_time_start));
		Logger::add(lcbuf);
		sprintf_s(lcbuf, 512, "Creating camera paths took %d ms, emitter paths took %d ms.", bdpt_eye_creation_time_total, bdpt_emitter_creation_time_total);
		Logger::add(lcbuf);
		sprintf_s(lcbuf, 512, "Weights evaluation took %d ms.", bdpt_weight_eval_time_total);
		Logger::add(lcbuf);
		sprintf_s(lcbuf, 512, "Shadows checking took %d ms.", bdpt_shadow_rays_time_total);
		Logger::add(lcbuf);
		sprintf_s(lcbuf, 512, "Multilayers update took %d ms.", bdpt_multilayers_time_total);
		Logger::add(lcbuf);
	#endif
}




#endif
