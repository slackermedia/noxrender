#include "CrashLog.h"
#include <windows.h>
#include <Shlobj.h>
#include <stdio.h>
#include <psapi.h>
#include "..\nox_version.h"
#include <Dbghelp.h>

char * getProcessorInfo(bool addnumcores);


CrashLog * cl_inst = NULL;

//-----------------------------------------------------------------------------------------

CrashLog::CrashLog()
{
	crashLogFilename = (char*)malloc(MAX_PATH+64);
	char path[MAX_PATH];
    if (SHGetSpecialFolderPath(HWND_DESKTOP, path, CSIDL_DESKTOP, FALSE)) 
		sprintf_s(crashLogFilename, MAX_PATH+64, "%s\\noxcrash.txt", path);
	else
		sprintf_s(crashLogFilename, MAX_PATH+64, "C:\\noxcrash.txt");
	symbolPath = NULL;

	appFilename = NULL;
	appArgs = NULL;
}

//-----------------------------------------------------------------------------------------

CrashLog::~CrashLog()
{
	if (crashLogFilename)
		free(crashLogFilename);
	crashLogFilename = NULL;
}

//-----------------------------------------------------------------------------------------

void CrashLog::printExceptionInfo(FILE * file, DWORD code)
{
	if (!file)
		return;
	switch (code)
	{
		case EXCEPTION_ACCESS_VIOLATION:			fprintf_s(file, "EXCEPTION_ACCESS_VIOLATION - The thread tried to read from or write to a virtual address for which it does not have the appropriate access."); break;
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:		fprintf_s(file, "EXCEPTION_ARRAY_BOUNDS_EXCEEDED - The thread tried to access an array element that is out of bounds and the underlying hardware supports bounds checking."); break;
		case EXCEPTION_BREAKPOINT:					fprintf_s(file, "EXCEPTION_BREAKPOINT - A breakpoint was encountered."); break;
		case EXCEPTION_DATATYPE_MISALIGNMENT:		fprintf_s(file, "EXCEPTION_DATATYPE_MISALIGNMENT - The thread tried to read or write data that is misaligned on hardware that does not provide alignment. For example, 16-bit values must be aligned on 2-byte boundaries; 32-bit values on 4-byte boundaries, and so on."); break;
		case EXCEPTION_FLT_DENORMAL_OPERAND:		fprintf_s(file, "EXCEPTION_FLT_DENORMAL_OPERAND - One of the operands in a floating-point operation is denormal. A denormal value is one that is too small to represent as a standard floating-point value."); break;
		case EXCEPTION_FLT_DIVIDE_BY_ZERO:			fprintf_s(file, "EXCEPTION_FLT_DIVIDE_BY_ZERO- The thread tried to divide a floating-point value by a floating-point divisor of zero."); break;
		case EXCEPTION_FLT_INEXACT_RESULT:			fprintf_s(file, "EXCEPTION_FLT_INEXACT_RESULT - The result of a floating-point operation cannot be represented exactly as a decimal fraction."); break;
		case EXCEPTION_FLT_INVALID_OPERATION:		fprintf_s(file, "EXCEPTION_FLT_INVALID_OPERATION - This exception represents any floating-point exception not included in this list."); break;
		case EXCEPTION_FLT_OVERFLOW:				fprintf_s(file, "EXCEPTION_FLT_OVERFLOW - The exponent of a floating-point operation is greater than the magnitude allowed by the corresponding type."); break;
		case EXCEPTION_FLT_STACK_CHECK:				fprintf_s(file, "EXCEPTION_FLT_STACK_CHECK - The stack overflowed or underflowed as the result of a floating-point operation."); break;
		case EXCEPTION_FLT_UNDERFLOW:				fprintf_s(file, "EXCEPTION_FLT_UNDERFLOW - The exponent of a floating-point operation is less than the magnitude allowed by the corresponding type."); break;
		case EXCEPTION_ILLEGAL_INSTRUCTION:			fprintf_s(file, "EXCEPTION_ILLEGAL_INSTRUCTION - The thread tried to execute an invalid instruction."); break;
		case EXCEPTION_IN_PAGE_ERROR:				fprintf_s(file, "EXCEPTION_IN_PAGE_ERROR - The thread tried to access a page that was not present, and the system was unable to load the page. For example, this exception might occur if a network connection is lost while running a program over the network."); break;
		case EXCEPTION_INT_DIVIDE_BY_ZERO:			fprintf_s(file, "EXCEPTION_INT_DIVIDE_BY_ZERO - The thread tried to divide an integer value by an integer divisor of zero."); break;
		case EXCEPTION_INT_OVERFLOW:				fprintf_s(file, "EXCEPTION_INT_OVERFLOW - The result of an integer operation caused a carry out of the most significant bit of the result."); break;
		case EXCEPTION_INVALID_DISPOSITION:			fprintf_s(file, "EXCEPTION_INVALID_DISPOSITION - An exception handler returned an invalid disposition to the exception dispatcher. Programmers using a high-level language such as C should never encounter this exception."); break;
		case EXCEPTION_NONCONTINUABLE_EXCEPTION:	fprintf_s(file, "EXCEPTION_NONCONTINUABLE_EXCEPTION - The thread tried to continue execution after a noncontinuable exception occurred."); break;
		case EXCEPTION_PRIV_INSTRUCTION:			fprintf_s(file, "EXCEPTION_PRIV_INSTRUCTION - The thread tried to execute an instruction whose operation is not allowed in the current machine mode."); break;
		case EXCEPTION_SINGLE_STEP:					fprintf_s(file, "EXCEPTION_SINGLE_STEP - A trace trap or other single-instruction mechanism signaled that one instruction has been executed."); break;
		case EXCEPTION_STACK_OVERFLOW:				fprintf_s(file, "EXCEPTION_STACK_OVERFLOW - The thread used up its stack."); break;
		default:									fprintf_s(file, "No explanation for that code."); break;
	}
	fprintf_s(file, "\n");
}

//-----------------------------------------------------------------------------------------

INT_PTR CrashLog::getImageBase(char * filename)
{
	HANDLE filehandle = CreateFile((const char*)filename, 
                        GENERIC_READ,FILE_SHARE_READ, 
                        NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,
                        NULL); 

	if (INVALID_HANDLE_VALUE == filehandle) 
		return 0;

	HANDLE mhand =  CreateFileMapping(filehandle,0,PAGE_READONLY,0,0,0);
	IMAGE_DOS_HEADER *mz = (IMAGE_DOS_HEADER *)MapViewOfFile(mhand,FILE_MAP_READ,0,0,0);
	if (mz->e_magic != IMAGE_DOS_SIGNATURE)
	{
		CloseHandle(filehandle);
		return 0;
	}
	IMAGE_DOS_HEADER *dos = (IMAGE_DOS_HEADER *)((ULONG_PTR) mz);
	IMAGE_NT_HEADERS *nt = (IMAGE_NT_HEADERS *)((ULONG_PTR) mz + (mz->e_lfanew));
	CloseHandle(filehandle);
	if (nt->Signature != IMAGE_NT_SIGNATURE)
		return 0;
	return nt->OptionalHeader.ImageBase;
}

//-----------------------------------------------------------------------------------------

void CrashLog::logAddress(FILE * file, PVOID addr)
{
	if (!file)
		return;

	MODULEINFO modinfo;
	HANDLE hProcess = GetCurrentProcess();

    HMODULE hMods[1024];
    DWORD cbNeeded;
	BOOL enumOK = EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded);

	INT_PTR instr_ptr = (INT_PTR)addr;
	bool gotModule = false;
	for (unsigned int i=0; i<(cbNeeded/sizeof(HMODULE)); i++)
	{
		BOOL mOK1 = GetModuleInformation(hProcess, hMods[i], &modinfo, sizeof(modinfo));
		if (instr_ptr >= (long long)modinfo.lpBaseOfDll   &&   instr_ptr < (long long)modinfo.lpBaseOfDll+modinfo.SizeOfImage)
		{
			char szModName[MAX_PATH];
			char line1[MAX_PATH+256];
			BOOL mOK2 = GetModuleFileNameEx( hProcess, hMods[i], szModName, sizeof(szModName));
			unsigned long long laddr = getImageBase(szModName);
			if (laddr>0)
			{
				INT_PTR naddr = instr_ptr - (INT_PTR)(modinfo.lpBaseOfDll) + (INT_PTR)laddr;
				sprintf_s(line1, MAX_PATH+256, "cs=%p   in module %s  (local : %p)", instr_ptr, szModName, naddr);
			}
			else
				sprintf_s(line1, MAX_PATH+256, "cs=%p   in module %s", instr_ptr, szModName);
			fprintf_s(file, line1);
			gotModule = true;
		}
	}
	if (!gotModule)
	{
		char line1[256];
		sprintf_s(line1, 256, "cs=%p  not found in any module", instr_ptr);
		fprintf_s(file, line1);
	}
}

//-----------------------------------------------------------------------------------------

void CrashLog::printSymbol(FILE * file, PVOID addr)
{
	if (!file)
		return;

	HANDLE curProcess = GetCurrentProcess();
	IMAGEHLP_LINE64 line;
	line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);

	SymSetOptions(SYMOPT_LOAD_LINES);
	SymInitialize(curProcess, symbolPath, TRUE);

	DWORD64  dwDisplacement64 = 0;
	DWORD  dwDisplacement = 0;
	DWORD64  dwAddress = (DWORD64)addr;

	char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
	buffer[0] = 0;
	PSYMBOL_INFO pSymbol = (PSYMBOL_INFO)buffer;

	pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
	pSymbol->MaxNameLen = MAX_SYM_NAME;

	if (SymGetLineFromAddr64(curProcess, dwAddress, &dwDisplacement, &line))
		fprintf_s(file, "Line %d - ", line.LineNumber);

	if (SymFromAddr(curProcess, dwAddress, &dwDisplacement64, pSymbol))
		fprintf_s(file, "Symbol: %s", pSymbol->Name);

	fprintf_s(file, "\n");

	SymCleanup(curProcess);
}

//-----------------------------------------------------------------------------------------

LONG __stdcall CrashLog::exceptionFilter(EXCEPTION_POINTERS* exceptionPtrs)
{
	CrashLog * clog = CrashLog::getInstance();
	FILE * file;
	if (fopen_s(&file, clog->crashLogFilename, "w" ))
	{
		char buf[256];
		sprintf_s(buf, 256, "Could not create crash log file:\n", clog->crashLogFilename);
		MessageBox(0, buf, "Error", 0);
		return EXCEPTION_CONTINUE_SEARCH;
	}

	#ifdef _WIN64
		fprintf_s(file, "NOX Renderer 64-bit, version: %d.%d.%d.%d\n", NOX_VER_MAJOR, NOX_VER_MINOR, NOX_VER_BUILD, NOX_VER_COMP_NO);
	#else
		fprintf_s(file, "NOX Renderer 32-bit, version: %d.%d.%d.%d\n", NOX_VER_MAJOR, NOX_VER_MINOR, NOX_VER_BUILD, NOX_VER_COMP_NO);
	#endif

	char * cpuinfo = getProcessorInfo(false);
	fprintf_s(file, "CPU: %s", cpuinfo);


	SYSTEMTIME st;
	GetLocalTime(&st);
	fprintf_s(file, "Date/Time: %02d-%02d-%02d  %02d:%02d:%02d\n\n", st.wYear, st.wMonth, st.wDay,  st.wHour, st.wMinute, st.wSecond);

	fprintf_s(file, "Exception code: 0x%X   at address 0x%X\n", exceptionPtrs->ExceptionRecord->ExceptionCode, exceptionPtrs->ExceptionRecord->ExceptionAddress);
	
	clog->printExceptionInfo(file, exceptionPtrs->ExceptionRecord->ExceptionCode);

	clog->printSymbol(file, exceptionPtrs->ExceptionRecord->ExceptionAddress);

	clog->logAddress(file, exceptionPtrs->ExceptionRecord->ExceptionAddress);

	fclose(file);

	clog->runSender();

	return EXCEPTION_CONTINUE_SEARCH;
}

//-----------------------------------------------------------------------------------------

void CrashLog::Initialize()
{
	SetUnhandledExceptionFilter(CrashLog::exceptionFilter);
}

//-----------------------------------------------------------------------------------------

CrashLog * CrashLog::getInstance()
{
	if (!cl_inst)
	{
		cl_inst = new CrashLog();
		cl_inst->Initialize();
	}
	return cl_inst;
}

//-----------------------------------------------------------------------------------------

bool CrashLog::setCrashLogFolder(char * folder)
{
	if (!folder)
		return false;

	// check if dir exists
	bool exists = true;
	HANDLE hDir;
	WIN32_FIND_DATA fd;
	ZeroMemory(&fd, sizeof(WIN32_FIND_DATA));
	hDir = FindFirstFile(folder, &fd);
	if (hDir == INVALID_HANDLE_VALUE   ||   !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		exists = false;
	if (hDir != INVALID_HANDLE_VALUE)
		FindClose(hDir);
	if (!exists)
		return false;

	int l = (int)strlen(folder);
	char * newfilename = (char*)malloc(l+32);
	if (!newfilename)
		return false;
	sprintf_s(newfilename, l+32, "%s\\noxcrash.txt", folder);

	if (crashLogFilename)
		free(crashLogFilename);
	crashLogFilename = newfilename;
	return true;
}

//-----------------------------------------------------------------------------------------

bool CrashLog::setCrashRunApp(char * file, char * args)
{
	if (file)
	{
		int l = (int)strlen(file);
		char * newfilename = (char*)malloc(l+32);
		if (newfilename)
		{
			sprintf_s(newfilename, l+32, "%s", file);
			if (appFilename)
				free(appFilename);
			appFilename = newfilename;
		}
	}

	if (args)
	{
		int l = (int)strlen(args);
		char * newargs = (char*)malloc(l+32);
		if (newargs)
		{
			sprintf_s(newargs, l+32, "%s", args);
			if (appArgs)
				free(appArgs);
			appArgs = newargs;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------------------

bool CrashLog::setSymbolPath(char * folder)
{
	if (!folder)
		return false;

	int l = (int)strlen(folder);
	char * newfilename = (char*)malloc(l+1);
	if (!newfilename)
		return false;
	sprintf_s(newfilename, l+1, "%s", folder);

	if (symbolPath)
		free(symbolPath);
	symbolPath = newfilename;
	return true;
}

//-----------------------------------------------------------------------------------------

bool CrashLog::runSender()
{
	if (!appFilename)
		return false;

	PROCESS_INFORMATION processInformation;
	STARTUPINFO startupInfo;
	memset(&processInformation, 0, sizeof(processInformation));
	memset(&startupInfo, 0, sizeof(startupInfo));
	startupInfo.cb = sizeof(startupInfo);

	BOOL ok = CreateProcess(appFilename, appArgs, 
			NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, NULL, 
			&startupInfo, &processInformation);

	return true;
}

//-----------------------------------------------------------------------------------------
